package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZoneId;
import java.time.ZonedDateTime;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity PartyFacilityPurpose.
 */

@Entity
@Table(name = "party_facility_purpose")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "partyfacilitypurpose")
public class PartyFacilityPurpose implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idparfacpur", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idPartyFacility;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idparty", referencedColumnName="idparty")
    private Organization organization;

    @ManyToOne
    @JoinColumn(name="idfacility", referencedColumnName="idfacility")
    private Facility facility;

    @ManyToOne
    @JoinColumn(name="idpurposetype", referencedColumnName="idpurposetype")
    private PurposeType purposeType;

    public UUID getIdPartyFacility() {
        return this.idPartyFacility;
    }

    public void setIdPartyFacility(UUID id) {
        this.idPartyFacility = id;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public PartyFacilityPurpose dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public PartyFacilityPurpose dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Organization getOrganization() {
        return organization;
    }

    public PartyFacilityPurpose organization(Organization organization) {
        this.organization = organization;
        return this;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Facility getFacility() {
        return facility;
    }

    public PartyFacilityPurpose facility(Facility facility) {
        this.facility = facility;
        return this;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    public PurposeType getPurposeType() {
        return purposeType;
    }

    public PartyFacilityPurpose purposeType(PurposeType purposeType) {
        this.purposeType = purposeType;
        return this;
    }

    public void setPurposeType(PurposeType purposeType) {
        this.purposeType = purposeType;
    }

    @PrePersist
    public void prePersist() {
        if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
        if (this.dateThru == null)
            this.dateThru = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PartyFacilityPurpose partyFacilityPurpose = (PartyFacilityPurpose) o;
        if (partyFacilityPurpose.idPartyFacility == null || this.idPartyFacility == null) {
            return false;
        }
        return Objects.equals(this.idPartyFacility, partyFacilityPurpose.idPartyFacility);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPartyFacility);
    }

    @Override
    public String toString() {
        return "PartyFacilityPurpose{" +
            "idPartyFacility=" + this.idPartyFacility +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
