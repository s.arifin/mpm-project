package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.util.Objects;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;

/**
 * BeSmart Team
 * Class definition for Entity ItemIssuance.
 */

@Entity
@Table(name = "item_issuance")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "itemissuance")
public class ItemIssuance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "iditeiss", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idItemIssuance;

    @Column(name = "qty")
    private Double qty;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name="idshiite", referencedColumnName="idshiite")
    private ShipmentItem shipmentItem;

    @ManyToOne
    @JoinColumn(name="idinvite", referencedColumnName="idinvite")
    private InventoryItem inventoryItem;

    @ManyToOne
    @JoinColumn(name="idslip", referencedColumnName="idslip")
    private PickingSlip picking;

    public UUID getIdItemIssuance() {
        return this.idItemIssuance;
    }

    public void setIdItemIssuance(UUID id) {
        this.idItemIssuance = id;
    }

    public Double getQty() {
        return qty;
    }

    public ItemIssuance qty(Double qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public ShipmentItem getShipmentItem() {
        return shipmentItem;
    }

    public ItemIssuance shipmentItem(ShipmentItem shipmentItem) {
        this.shipmentItem = shipmentItem;
        return this;
    }

    public void setShipmentItem(ShipmentItem shipmentItem) {
        this.shipmentItem = shipmentItem;
    }

    public InventoryItem getInventoryItem() {
        return inventoryItem;
    }

    public ItemIssuance inventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
        return this;
    }

    public void setInventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
    }

    public PickingSlip getPicking() {
        return picking;
    }

    public ItemIssuance picking(PickingSlip pickingSlip) {
        this.picking = pickingSlip;
        return this;
    }

    public void setPicking(PickingSlip pickingSlip) {
        this.picking = pickingSlip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ItemIssuance itemIssuance = (ItemIssuance) o;
        if (itemIssuance.idItemIssuance == null || this.idItemIssuance == null) {
            return false;
        }
        return Objects.equals(this.idItemIssuance, itemIssuance.idItemIssuance);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idItemIssuance);
    }

    @Override
    public String toString() {
        return "ItemIssuance{" +
            "idItemIssuance=" + this.idItemIssuance +
            ", qty='" + getQty() + "'" +
            '}';
    }
}
