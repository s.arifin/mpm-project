package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity PaymentType.
 */

@Entity
@Table(name = "payment_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "paymenttype")
public class PaymentType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaytyp")
    private Integer idPaymentType;

    @Column(name = "description")
    private String description;

    public Integer getIdPaymentType() {
        return this.idPaymentType;
    }

    public void setIdPaymentType(Integer id) {
        this.idPaymentType = id;
    }

    public String getDescription() {
        return description;
    }

    public PaymentType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PaymentType paymentType = (PaymentType) o;
        if (paymentType.idPaymentType == null || this.idPaymentType == null) {
            return false;
        }
        return Objects.equals(this.idPaymentType, paymentType.idPaymentType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPaymentType);
    }

    @Override
    public String toString() {
        return "PaymentType{" +
            "idPaymentType=" + this.idPaymentType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
