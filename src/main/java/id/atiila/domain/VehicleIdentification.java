package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.time.ZoneId;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity VehicleIdentification.
 */

@Entity
@Table(name = "vehicle_identification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "vehicleidentification")
public class VehicleIdentification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idvehide", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idVehicleIdentification;

    @Column(name = "vehiclenumber")
    private String vehicleNumber;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idvehicle", referencedColumnName="idvehicle")
    private Vehicle vehicle;

    @ManyToOne
    @JoinColumn(name="idcustomer", referencedColumnName="idcustomer")
    private Customer customer;

    public UUID getIdVehicleIdentification() {
        return this.idVehicleIdentification;
    }

    public void setIdVehicleIdentification(UUID id) {
        this.idVehicleIdentification = id;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public VehicleIdentification vehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
        return this;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public VehicleIdentification dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public VehicleIdentification dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public VehicleIdentification vehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
        return this;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Customer getCustomer() {
        return customer;
    }

    public VehicleIdentification customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @PreUpdate
    @PrePersist
    public void preUpdate() {
        if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
        if (this.dateThru == null) this.dateThru = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        VehicleIdentification that = (VehicleIdentification) o;

        return new EqualsBuilder()
            .append(idVehicleIdentification, that.idVehicleIdentification)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idVehicleIdentification)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "VehicleIdentification{" +
            "idVehicleIdentification=" + this.idVehicleIdentification +
            ", vehicleNumber='" + getVehicleNumber() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
