package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


/**
 * atiila consulting
 * Class definition for Entity BillingItemType.
 */

@Entity
@Table(name = "billing_item_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "billingitemtype")
public class BillingItemType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idbilitetyp")
    private Integer idItemType;

    @Column(name = "description")
    private String description;

    public Integer getIdItemType() {
        return this.idItemType;
    }

    public void setIdItemType(Integer id) {
        this.idItemType = id;
    }

    public String getDescription() {
        return description;
    }

    public BillingItemType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BillingItemType billingItemType = (BillingItemType) o;
        if (billingItemType.idItemType == null || this.idItemType == null) {
            return false;
        }
        return Objects.equals(this.idItemType, billingItemType.idItemType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idItemType);
    }

    @Override
    public String toString() {
        return "BillingItemType{" +
            "idItemType=" + this.idItemType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
