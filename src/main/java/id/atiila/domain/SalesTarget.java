package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * A SalesTarget.
 */
@Entity
@Table(name = "sales_target")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "salestarget")
public class SalesTarget implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idslstrgt",  columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idslstrgt;

    @Column(name = "idsalesman")
    private UUID idsalesman;

    @Column(name = "internal")
    private String internal;

    @Column(name = "targetsales")
    private Integer targetsales;

    @Column(name = "dtfrom")
    private ZonedDateTime dtfrom;

    @Column(name = "dtthru")
    private ZonedDateTime dtthru;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove


    public UUID getIdslstrgt() {
        return idslstrgt;
    }

    public SalesTarget idslstrgt(UUID idslstrgt) {
        this.idslstrgt = idslstrgt;
        return this;
    }

    public void setIdslstrgt(UUID idslstrgt) {
        this.idslstrgt = idslstrgt;
    }

    public UUID getIdsalesman() {
        return idsalesman;
    }

    public SalesTarget idsalesman(UUID idsalesman) {
        this.idsalesman = idsalesman;
        return this;
    }

    public void setIdsalesman(UUID idsalesman) {
        this.idsalesman = idsalesman;
    }

    public String getInternal() {
        return internal;
    }

    public SalesTarget internal(String internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(String internal) {
        this.internal = internal;
    }

    public Integer getTargetsales() {
        return targetsales;
    }

    public SalesTarget targetsales(Integer targetsales) {
        this.targetsales = targetsales;
        return this;
    }

    public void setTargetsales(Integer targetsales) {
        this.targetsales = targetsales;
    }

    public ZonedDateTime getDtfrom() {
        return dtfrom;
    }

    public SalesTarget dtfrom(ZonedDateTime dtfrom) {
        this.dtfrom = dtfrom;
        return this;
    }

    public void setDtfrom(ZonedDateTime dtfrom) {
        this.dtfrom = dtfrom;
    }

    public ZonedDateTime getDtthru() {
        return dtthru;
    }

    public SalesTarget dtthru(ZonedDateTime dtthru) {
        this.dtthru = dtthru;
        return this;
    }

    public void setDtthru(ZonedDateTime dtthru) {
        this.dtthru = dtthru;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SalesTarget salesTarget = (SalesTarget) o;
        if (salesTarget.getIdslstrgt() == null || getIdslstrgt() == null) {
            return false;
        }
        return Objects.equals(getIdslstrgt(), salesTarget.getIdslstrgt());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdslstrgt());
    }

    @Override
    public String toString() {
        return "SalesTarget{" +
            ", idslstrgt='" + getIdslstrgt() + "'" +
            ", idsalesman='" + getIdsalesman() + "'" +
            ", internal='" + getInternal() + "'" +
            ", targetsales='" + getTargetsales() + "'" +
            ", dtfrom='" + getDtfrom() + "'" +
            ", dtthru='" + getDtthru() + "'" +
            "}";
    }
}
