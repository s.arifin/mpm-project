package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * BeSmart Team
 * Class definition for Entity PositionType.
 */

@Entity
@Table(name = "position_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "positiontype")
public class PositionType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idpostyp")
    private Integer idPositionType;

    @Column(name = "description")
    private String description;

    @Column(name = "title")
    private String title;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "position_authority",
        joinColumns = {@JoinColumn(name = "idpostyp", referencedColumnName = "idpostyp")},
        inverseJoinColumns = {@JoinColumn(name = "name", referencedColumnName = "name")})
    private Set<Authority> authorities = new HashSet<>();

    public Integer getIdPositionType() {
        return this.idPositionType;
    }

    public void setIdPositionType(Integer id) {
        this.idPositionType = id;
    }

    public String getDescription() {
        return description;
    }

    public PositionType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public PositionType title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PositionType positionType = (PositionType) o;
        if (positionType.idPositionType == null || this.idPositionType == null) {
            return false;
        }
        return Objects.equals(this.idPositionType, positionType.idPositionType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPositionType);
    }

    @Override
    public String toString() {
        return "PositionType{" +
            "idPositionType=" + this.idPositionType +
            ", description='" + getDescription() + "'" +
            ", title='" + getTitle() + "'" +
            '}';
    }
}
