package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;

/**
 * BeSmart Team
 * Class definition for Entity WeServiceType.
 */

@Entity
@Table(name = "we_service_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "weservicetype")
public class WeServiceType extends WorkEffortType {

    private static final long serialVersionUID = 1L;

    @Column(name = "frt")
    private Integer flatRateTime;

    @JsonIgnoreProperties("weType")
    @OneToOne
    @JoinColumn(name="idproduct", referencedColumnName = "idproduct")
    private Service service;

    public Integer getFlatRateTime() {
        return flatRateTime;
    }

    public WeServiceType flatRateTime(Integer flatRateTime) {
        this.flatRateTime = flatRateTime;
        return this;
    }

    public void setFlatRateTime(Integer flatRateTime) {
        this.flatRateTime = flatRateTime;
    }

    public Service getService() {
        return service;
    }

    public WeServiceType service(Service service) {
        this.service = service;
        return this;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "WeServiceType{" +
            "idWeType=" + this.getIdWeType() +
            ", flatRateTime='" + getFlatRateTime() + "'" +
            '}';
    }
}
