package id.atiila.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.*;

import id.atiila.base.BaseConstants;
import id.atiila.base.DomainEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

/**
 * BeSmart Team
 * Class definition for Entity InventoryItem Status.
 */

@Entity
@Table(name = "inventory_item_status")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class InventoryItemStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idstatus", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idStatus;

    @Column(name = "idstatustype")
    private Integer idStatusType;

    @ManyToOne
    @JoinColumn(name="idinvite", referencedColumnName = "idinvite")
    private InventoryItem owner;

	@Column(name = "idreason")
    private Integer idReasonType;

    @Column(name = "reason")
    private String reason;

    @Column(name="dtfrom")
    private LocalDateTime dateFrom;

    @Column(name="dtthru")
    private LocalDateTime dateThru;

    public UUID getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(UUID idStatus) {
		this.idStatus = idStatus;
	}

	public Integer getIdStatusType() {
		return idStatusType;
	}

	public void setIdStatusType(Integer idStatusType) {
		this.idStatusType = idStatusType;
	}

	public InventoryItem getOwner() {
		return owner;
	}

	public void setOwner(InventoryItem owner) {
		this.owner = owner;
	}

	public LocalDateTime getDateFrom() {
		return this.dateFrom;
	}

	public void setDateFrom(LocalDateTime dateFrom) {
		this.dateFrom = dateFrom;
	}

	public LocalDateTime getDateThru() {
		return this.dateThru;
	}

	public void setDateThru(LocalDateTime dateThru) {
		this.dateThru = dateThru;
	}

    public Integer getIdReasonType() {
        return this.idReasonType;
    }

    public void setIdReasonType(Integer idReasonType) {
        this.idReasonType = idReasonType;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

	@PreUpdate
	@PrePersist
	public void preUpdate() {
		if (this.dateFrom == null) this.dateFrom = LocalDateTime.now();
		if (this.dateThru == null) this.dateThru = LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		InventoryItemStatus that = (InventoryItemStatus) o;

		return new EqualsBuilder()
				.append(idStatus, that.idStatus)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idStatus)
				.toHashCode();
	}

    @Override
    public String toString() {
        return "InventoryItemStatus{" +
            "idStatus=" + getIdStatus() +
            ", idStatusType=" + getIdStatusType() +
            ", owner=" + getOwner() +
            ", dateFrom=" + getDateFrom() +
            ", dateThru=" + getDateThru() +
            '}';
    }
}
