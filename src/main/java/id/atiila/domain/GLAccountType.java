package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


/**
 * atiila consulting
 * Class definition for Entity GLAccountType.
 */

@Entity
@Table(name = "gl_account_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "glaccounttype")
public class GLAccountType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idglacctyp")
    private Integer idGLAccountType;

    @Column(name = "description")
    private String description;

    public Integer getIdGLAccountType() {
        return this.idGLAccountType;
    }

    public void setIdGLAccountType(Integer id) {
        this.idGLAccountType = id;
    }

    public String getDescription() {
        return description;
    }

    public GLAccountType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GLAccountType gLAccountType = (GLAccountType) o;
        if (gLAccountType.idGLAccountType == null || this.idGLAccountType == null) {
            return false;
        }
        return Objects.equals(this.idGLAccountType, gLAccountType.idGLAccountType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idGLAccountType);
    }

    @Override
    public String toString() {
        return "GLAccountType{" +
            "idGLAccountType=" + this.idGLAccountType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
