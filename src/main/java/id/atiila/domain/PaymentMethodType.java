package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity PaymentMethodType.
 */

@Entity
@Table(name = "payment_method_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "paymentmethodtype")
public class PaymentMethodType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaymettyp")
    private Integer idPaymentMethodType;

    @Column(name = "refkey")
    private String refKey;

    @Column(name = "description")
    private String description;

    public Integer getIdPaymentMethodType() {
        return this.idPaymentMethodType;
    }

    public void setIdPaymentMethodType(Integer id) {
        this.idPaymentMethodType = id;
    }

    public String getRefKey() {
        return refKey;
    }

    public PaymentMethodType refKey(String refKey) {
        this.refKey = refKey;
        return this;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public String getDescription() {
        return description;
    }

    public PaymentMethodType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PaymentMethodType paymentMethodType = (PaymentMethodType) o;
        if (paymentMethodType.idPaymentMethodType == null || this.idPaymentMethodType == null) {
            return false;
        }
        return Objects.equals(this.idPaymentMethodType, paymentMethodType.idPaymentMethodType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPaymentMethodType);
    }

    @Override
    public String toString() {
        return "PaymentMethodType{" +
            "idPaymentMethodType=" + this.idPaymentMethodType +
            ", refKey='" + getRefKey() + "'" +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
