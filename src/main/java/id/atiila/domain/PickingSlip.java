package id.atiila.domain;

import javax.persistence.*;

import id.atiila.domain.listener.PickingListener;
import org.elasticsearch.common.recycler.Recycler;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * BeSmart Team
 * Class definition for Entity PickingSlip.
 */

@Entity
@Table(name = "picking_slip")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pickingslip")
@EntityListeners(PickingListener.class)
public class PickingSlip extends InventoryMovement {

    private static final long serialVersionUID = 1L;

    @Column(name = "acc1")
    private Boolean acc1;

    @Column(name = "acc2")
    private Boolean acc2;

    @Column(name = "acc3")
    private Boolean acc3;

    @Column(name = "acc4")
    private Boolean acc4;

    @Column(name = "acc5")
    private Boolean acc5;

    @Column(name = "acc6")
    private Boolean acc6;

    @Column(name = "acc7")
    private Boolean acc7;

    @Column(name = "acc8")
    private Boolean acc8;

    @Column(name = "acctambah2")
    private Boolean acctambah2;

    @Column(name = "acctambah1")
    private Boolean acctambah1;

    @Column(name = "promat1")
    private Boolean promat1;

    @Column(name = "promat2")
    private Boolean promat2;

//    @ManyToOne
//    @JoinColumn(name="idinternalmecha", referencedColumnName="idparrol")
//    private Mechanic mechanicinternal;
    @Column(name = "idinternalmecha")
    private UUID mechanicinternal;

//    @ManyToOne
//    @JoinColumn(name="idexternalmecha", referencedColumnName="idparrol")
//    private Mechanic mechanicexternal;
    @Column(name = "idexternalmecha")
    private UUID mechanicexternal;


    @ManyToOne
    @JoinColumn(name="idshipto", referencedColumnName="idshipto")
    private ShipTo shipTo;

    @ManyToOne
    @JoinColumn(name="idinvite", referencedColumnName="idinvite")
    private InventoryItem inventoryItem;

    @ManyToOne
    @JoinColumn(name="idordite", referencedColumnName="idordite")
    private OrderItem orderItem;

    @OneToMany(mappedBy = "picking", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ItemIssuance> issuances = new HashSet<>();

    @Column(name = "qty")
    private Double qty;
//
//    @Transient
//    private Shipment shipment;
//
//    public Shipment getShipment() {
//        if (this.shipment == null && getOrderItem().size() > 0) {
//            OrderItem o = getDetails().iterator().next();
//            BillingItem billingItem = o.getBillingItems().iterator().next();
//            if (billingItem != null) this.billing = billingItem.getBilling();
//        }
//        return this.billing;
//    }


    public Boolean getAcc8() {
        return acc8;
    }

    public void setAcc8(Boolean acc8) {
        this.acc8 = acc8;
    }

    public Boolean getAcctambah2() {
        return acctambah2;
    }

    public void setAcctambah2(Boolean acctambah2) {
        this.acctambah2 = acctambah2;
    }

    public Boolean getAcctambah1() {
        return acctambah1;
    }

    public void setAcctambah1(Boolean acctambah1) {
        this.acctambah1 = acctambah1;
    }

    public UUID getMechanicinternal() {
        return mechanicinternal;
    }

    public void setMechanicinternal(UUID mechanicinternal) {
        this.mechanicinternal = mechanicinternal;
    }

    public UUID getMechanicexternal() {
        return mechanicexternal;
    }

    public void setMechanicexternal(UUID mechanicexternal) {
        this.mechanicexternal = mechanicexternal;
    }

    public ShipTo getShipTo() {
        return shipTo;
    }

    public PickingSlip shipTo(ShipTo shipTo) {
        this.shipTo = shipTo;
        return this;
    }

    public void setShipTo(ShipTo shipTo) {
        this.shipTo = shipTo;
    }

    public InventoryItem getInventoryItem() {
        return inventoryItem;
    }

    public PickingSlip inventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
        return this;
    }

    public void setInventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public PickingSlip orderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
        return this;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public void setQty(Integer qty) {
        this.qty = Double.valueOf(qty);
    }

    public Set<ItemIssuance> getIssuances() {
        return issuances;
    }

    public void setIssuances(Set<ItemIssuance> issuances) {
        this.issuances = issuances;
    }

    public PickingSlip addIssuance(ItemIssuance itemIssuance) {
        this.issuances.add(itemIssuance);
        itemIssuance.setPicking(this);
        return this;
    }

    public PickingSlip removeIssuance(ItemIssuance itemIssuance) {
        this.issuances.remove(itemIssuance);
        itemIssuance.setPicking(null);
        return this;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    public Boolean getAcc1(){
        return acc1;
    }

    public void setAcc1(Boolean acc1) {
        this.acc1 = acc1;
    }

    public Boolean getAcc2() {
        return acc2;
    }
    public void setAcc2(Boolean acc2) {
        this.acc2 = acc2;
    }

    public Boolean getAcc3() {
        return acc3;
    }

    public void setAcc3(Boolean acc3) {
        this.acc3 = acc3;
    }

    public Boolean getAcc4() {
        return acc4;
    }

    public void setAcc4(Boolean acc4) {
        this.acc4 = acc4;
    }

    public Boolean getAcc5() {
        return acc5;
    }

    public void setAcc5(Boolean acc5) {
        this.acc5 = acc5;
    }

    public Boolean getAcc6() {
        return acc6;
    }

    public void setAcc6(Boolean acc6) {
        this.acc6 = acc6;
    }

    public Boolean getAcc7() {
        return acc7;
    }

    public void setAcc7(Boolean acc7) {
        this.acc7 = acc7;
    }

    public Boolean getPromat1() {
        return promat1;
    }

    public void setPromat1(Boolean promat1) {
        this.promat1 = promat1;
    }

    public Boolean getPromat2() {
        return promat2;
    }

    public void setPromat2(Boolean promat2) {
        this.promat2 = promat2;
    }

    @Override
    public String toString() {
        return "PickingSlip{" +
            "idSlip=" + this.getIdSlip() +
            ", dateCreate='" + getDateCreate() + "'" +
            ", acc1='" + getAcc1() + "'"+
            ", acc2='" + getAcc2() + "'"+
            ", acc3='" + getAcc3() + "'"+
            ", acc4='" + getAcc4() + "'"+
            ", acc5='" + getAcc5() + "'"+
            ", acc6='" + getAcc6() + "'"+
            ", acc7='" + getAcc7() + "'"+
            ", promat1='" + getPromat1() + "'"+
            ", promat2='" + getPromat2() + "'"+
            ", mechanicinternal='" + getMechanicinternal() + "'"+
            ", mechanexinternal='" + getMechanicexternal() + "'"+
            '}';
    }
}
