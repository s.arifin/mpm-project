package id.atiila.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import id.atiila.base.DomainEntity;

/**
 * BeSmart Team
 * Class definition for Entity WorkEffort Status.
 */

@Entity
@Table(name = "work_effort_status")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class WorkEffortStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idstatus", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idStatus;

    @Column(name = "idstatustype")
    private Integer idStatusType;

    @ManyToOne
    @JoinColumn(name="idwe", referencedColumnName = "idwe")
    private WorkEffort owner;

    @Column(name="dtfrom")
    private LocalDateTime dateFrom;

    @Column(name="dtthru")
    private LocalDateTime dateThru;

    public UUID getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(UUID idStatus) {
		this.idStatus = idStatus;
	}

	public Integer getIdStatusType() {
		return idStatusType;
	}

	public void setIdStatusType(Integer idStatusType) {
		this.idStatusType = idStatusType;
	}

	public WorkEffort getOwner() {
		return owner;
	}

	public void setOwner(WorkEffort owner) {
		this.owner = owner;
	}

	public LocalDateTime getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(LocalDateTime dateFrom) {
		this.dateFrom = dateFrom;
	}

	public LocalDateTime getDateThru() {
		return dateThru;
	}

	public void setDateThru(LocalDateTime dateThru) {
		this.dateThru = dateThru;
	}

	@PreUpdate
	@PrePersist
	public void preUpdate() {
		if (this.dateFrom == null) this.dateFrom = LocalDateTime.now();
		if (this.dateThru == null) this.dateThru = LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		WorkEffortStatus that = (WorkEffortStatus) o;

		return new EqualsBuilder()
				.append(idStatus, that.idStatus)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idStatus)
				.toHashCode();
	}

    @Override
    public String toString() {
        return "WorkEffortStatus{" +
            "idStatus=" + getIdStatus() +
            ", idStatusType=" + getIdStatusType() +
            ", owner=" + getOwner() +
            ", dateFrom=" + getDateFrom() +
            ", dateThru=" + getDateThru() +
            '}';
    }
}
