package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity RuleType.
 */

@Entity
@Table(name = "rule_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "ruletype")
public class RuleType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idrultyp")
    private Integer idRuleType;

    @Column(name = "description")
    private String description;

    public Integer getIdRuleType() {
        return this.idRuleType;
    }

    public void setIdRuleType(Integer id) {
        this.idRuleType = id;
    }

    public String getDescription() {
        return description;
    }

    public RuleType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RuleType ruleType = (RuleType) o;
        if (ruleType.idRuleType == null || this.idRuleType == null) {
            return false;
        }
        return Objects.equals(this.idRuleType, ruleType.idRuleType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idRuleType);
    }

    @Override
    public String toString() {
        return "RuleType{" +
            "idRuleType=" + this.idRuleType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
