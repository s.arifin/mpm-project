package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.hibernate.annotations.Where;
import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity UnitDocumentMessage.
 */

@Entity
@Table(name = "unit_document_message")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "unitdocumentmessage")
public class UnitDocumentMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmessage")
    private Integer idMessage;

    @Column(name = "idreq")
    private UUID idRequirement;

    @Column(name = "idprospect")
    private String idProspect;

    @Column(name = "content")
    private String content;

    @Column(name = "dtcreated")
    private ZonedDateTime dateCreated;

    @Column(name = "usernamefrom")
    private String usernameFrom;

    @Column(name = "usernameto")
    private String usernameTo;

    @ManyToOne
    @JoinColumn(name="idparent", referencedColumnName="idmessage")
    private UnitDocumentMessage parent;

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name = "idmessage")
    @Where(clause = "current_timestamp between dtfrom and dtthru")
    private List<Approval> approvals = new ArrayList<Approval>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<UnitDocumentMessageStatus> statuses = new ArrayList<UnitDocumentMessageStatus>();

    public List<UnitDocumentMessageStatus> getStatuses() { return statuses; }

    public void setStatuses(List<UnitDocumentMessageStatus> statuses) { this.statuses = statuses; }

    public Integer getStatusDocument() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now().plusNanos(1);

        for (UnitDocumentMessageStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatusDocument(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now();

        for (UnitDocumentMessageStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        UnitDocumentMessageStatus current = new UnitDocumentMessageStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        statuses.add(current);
    }

    //getter and setter

    public List<Approval> getApprovals() {
        return approvals;
    }

    public void setApprovals(List<Approval> approvals) {
        this.approvals = approvals;
    }

    public Integer getIdMessage() {
        return this.idMessage;
    }

    public void setIdMessage(Integer id) {
        this.idMessage = id;
    }

    public UUID getIdRequirement() {
        return idRequirement;
    }

    public UnitDocumentMessage idRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
        return this;
    }

    public void setIdRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
    }

    public String getIdProspect() {
        return idProspect;
    }

    public UnitDocumentMessage idProspect(String idProspect) {
        this.idProspect = idProspect;
        return this;
    }

    public void setIdProspect(String idProspect) {
        this.idProspect = idProspect;
    }

    public String getContent() {
        return content;
    }

    public UnitDocumentMessage content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public UnitDocumentMessage dateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getUsernameFrom() {
        return usernameFrom;
    }

    public UnitDocumentMessage usernameFrom(String usernameFrom) {
        this.usernameFrom = usernameFrom;
        return this;
    }

    public void setUsernameFrom(String usernameFrom) {
        this.usernameFrom = usernameFrom;
    }

    public String getUsernameTo() {
        return usernameTo;
    }

    public UnitDocumentMessage usernameTo(String usernameTo) {
        this.usernameTo = usernameTo;
        return this;
    }

    public void setUsernameTo(String usernameTo) {
        this.usernameTo = usernameTo;
    }

    public UnitDocumentMessage getParent() {
        return parent;
    }

    public void setParent(UnitDocumentMessage parent) {
        this.parent = parent;
    }

    public Integer getApproval() {
        Integer result = BaseConstants.STATUS_WAITING_FOR_APPROVAL;
        LocalDateTime now = LocalDateTime.now().plusNanos(1);

        for (Approval current: getApprovals()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_WAITING_FOR_APPROVAL;
        LocalDateTime now = LocalDateTime.now();

        for (Approval current: getApprovals()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        Approval r = new Approval();
        r.setIdApprovalType(BaseConstants.SALES_MESSAGE_UNIT);
        r.setIdStatusType(value);
        approvals.add(r);
    }

    public Integer getCurrentApprovalStatus() {
        return getApproval();
    }
    public Integer getCurrentStatus() { return getStatusDocument(); }

    @PrePersist
    @PreUpdate
    public void preUpdate(){
        if (this.dateCreated == null) this.dateCreated = ZonedDateTime.now();
        if (getCurrentApprovalStatus().equals(BaseConstants.STATUS_WAITING_FOR_APPROVAL)) setStatus(BaseConstants.STATUS_WAITING_FOR_APPROVAL);
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatusDocument(BaseConstants.STATUS_DRAFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UnitDocumentMessage unitDocumentMessage = (UnitDocumentMessage) o;
        if (unitDocumentMessage.idMessage == null || this.idMessage == null) {
            return false;
        }
        return Objects.equals(this.idMessage, unitDocumentMessage.idMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idMessage);
    }

    @Override
    public String toString() {
        return "UnitDocumentMessage{" +
            "idMessage=" + this.idMessage +
            ", idRequirement='" + getIdRequirement() + "'" +
            ", idProspect='" + getIdProspect() + "'" +
            ", content='" + getContent() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            ", usernameFrom='" + getUsernameFrom() + "'" +
            ", usernameTo='" + getUsernameTo() + "'" +
            '}';
    }
}
