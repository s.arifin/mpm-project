package id.atiila.domain;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.data.elasticsearch.annotations.Document;

import id.atiila.base.BaseConstants;

/**
 * BeSmart Team
 * Class definition for Entity Salesman.
 */

@Entity
@Table(name = "salesman")
@DiscriminatorValue(BaseConstants.ROLE_SALESMAN)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "salesman")
public class Salesman extends PartyRole {

    private static final long serialVersionUID = 1L;

    @Column(name = "idsalesman")
    private String idSalesman;

    @Column(name = "coordinator")
    private Boolean coordinator;

    @Column(name = "teamleader")
    private Boolean teamLeader;

    @Column(name = "ahmcode")
    private String ahmCode;

    @Column(name = "username")
    private String userName;

    @ManyToOne(fetch= FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name="idcoordinator", referencedColumnName="idparrol")
    private Salesman coordinatorSales;

    @ManyToOne(fetch=FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name="idteamleader", referencedColumnName="idparrol")
    private Salesman teamLeaderSales;

    public String getAhmCode() {
        return ahmCode;
    }

    public void setAhmCode(String ahmCode) {
        this.ahmCode = ahmCode;
    }

    public String getIdSalesman() {
        return idSalesman;
    }

    public Salesman idSalesman(String idSalesman) {
        this.idSalesman = idSalesman;
        return this;
    }

    public void setIdSalesman(String idSalesman) {
        this.idSalesman = idSalesman;
    }

    public Boolean isCoordinator() {
        return coordinator;
    }

    public Salesman coordinator(Boolean coordinator) {
        this.coordinator = coordinator;
        return this;
    }

    public void setCoordinator(Boolean coordinator) {
        this.coordinator = coordinator;
    }

    public Boolean isTeamLeader() { return teamLeader; }

    public Salesman teamLeader(Boolean teamLeader) {
        this.teamLeader = teamLeader;
        return this;
    }

    public void setTeamLeader(Boolean teamLeader) { this.teamLeader = teamLeader; }

    public Salesman getCoordinatorSales() {
        return coordinatorSales;
    }

    public Salesman coordinatorSales(Salesman salesman) {
        this.coordinatorSales = salesman;
        return this;
    }

    public void setCoordinatorSales(Salesman salesman) {
        this.coordinatorSales = salesman;
    }

    public Salesman getTeamLeaderSales() { return teamLeaderSales; }

    public Salesman teamLeaderSales(Salesman teamLeaderSales) {
        this.teamLeaderSales = teamLeaderSales;
        return this;
    }

    public void setTeamLeaderSales(Salesman teamLeaderSales) { this.teamLeaderSales = teamLeaderSales; }

    public Person getPerson() {
        return (Person) getParty();
    }

    public void setPerson(Person p) {
        setParty(p);
    }

    public String getUserName() { return userName; }

    public void setUserName(String userName) { this.userName = userName; }

    public Boolean getCoordinator() { return coordinator; }

    public Boolean getTeamLeader() { return teamLeader; }

    @Override
    public String toString() {
        return "Salesman{" +
            "idPartyRole=" + this.getIdPartyRole() +
            ", idSalesman='" + getIdSalesman() + "'" +
            ", coordinator='" + isCoordinator() + "'" +
            ", dateRegister='" + getDateRegister() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
