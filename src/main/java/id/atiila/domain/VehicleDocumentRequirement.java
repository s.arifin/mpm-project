package id.atiila.domain;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.UUID;

import javax.persistence.*;


import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * BeSmart Team
 * Class definition for Entity VehicleDocumentRequirement.
 */

@Entity
@Table(name = "vehicle_document_requirement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "vehicledocumentrequirement")
public class VehicleDocumentRequirement extends Requirement {

    private static final long serialVersionUID = 1L;

    @Column(name = "note")
    private String note;

    @Column(name = "statusfaktur")
    private String statusFaktur;

    @Column(name = "bbn", precision=10, scale=2)
    private BigDecimal bbn;

    @Column(name = "othercost", precision=10, scale=2)
    private BigDecimal otherCost;

    @Column(name = "atpmfaktur")
    private String fakturATPM;

    @Column(name = "atpmfakturdt")
    private ZonedDateTime fakturDate;



    @Column(name = "dtinvoiceprinted")
    private ZonedDateTime invoicePrint;

    @Column(name = "dtfillname")
    private ZonedDateTime dateFillName;

    @Column(name = "policenumber")
    private String requestPoliceNumber;

    @Column(name = "submissionno")
    private String submissionNo;

    @Column(name = "submissiondt")
    private ZonedDateTime dateSubmission;

    @Column(name = "registrationnumber")
    private String registrationNumber;

    @Column(name = "waitstnk")
    private Boolean waitStnk;

    @Column(name = "costhandling", precision=10, scale=2)
    private BigDecimal costHandling;

    @Column(name = "bbnkb", precision=10, scale=2)
    private BigDecimal bbnkb;

    @Column(name = "pkb", precision=10, scale=2)
    private BigDecimal pkb;

    @Column(name = "swdkllj", precision=10, scale=2)
    private BigDecimal swdkllj;

    @Column(name = "tnkb", precision=10, scale=2)
    private BigDecimal tnkb;

    @Column(name = "jasa", precision=10, scale=2)
    private BigDecimal jasa;

    @Column(name = "stnkbpkb", precision=10, scale=2)
    private BigDecimal stnkbpkb;

    @Column(name = "biayaparkir", precision=10, scale=2)
    private BigDecimal biayaParkir;

    @Column(name = "biaya", precision=10, scale=2)
    private BigDecimal biaya;

    @Column(name = "totalcosthandling", precision=10, scale=2)
    private BigDecimal totalCostHandling;

    @Column(name = "idslsreq")
    private UUID idSalesRequirement;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "idsaletype", referencedColumnName = "idsaletype")
    private SaleType saleType;

    @ManyToOne (cascade = {CascadeType.PERSIST})
    @JoinColumn(name="idordite", referencedColumnName="idordite")
    private OrderItem orderItem;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne (cascade = {CascadeType.PERSIST})
    @JoinColumn(name="idshipto", referencedColumnName="idshipto")
    private ShipTo shipTo;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="idvendor", referencedColumnName="idvendor")
    private Vendor vendor;

    @ManyToOne (cascade = {CascadeType.PERSIST})
    @JoinColumn(name="idbillto", referencedColumnName="idbillto")
    private BillTo billTo;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="idpersonowner", referencedColumnName="idparty")
    private Person personOwner;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="idorganizationowner", referencedColumnName="idparty")
    private Organization organizationOwner;

    @ManyToOne //(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name="idvehicle", referencedColumnName="idvehicle")
    private Vehicle vehicle;

    public BigDecimal getBiayaParkir() {
        return biayaParkir;
    }

    public void setBiayaParkir(BigDecimal biayaParkir) {
        this.biayaParkir = biayaParkir;
    }

    public BigDecimal getBiaya() {
        return biaya;
    }

    public void setBiaya(BigDecimal biaya) {
        this.biaya = biaya;
    }

    public BigDecimal getTotalCostHandling() {
        return totalCostHandling;
    }

    public void setTotalCostHandling(BigDecimal totalCostHandling) {
        this.totalCostHandling = totalCostHandling;
    }

    public BigDecimal getBbnkb() {
        return bbnkb;
    }

    public void setBbnkb(BigDecimal bbnkb) {
        this.bbnkb = bbnkb;
    }

    public BigDecimal getPkb() {
        return pkb;
    }

    public void setPkb(BigDecimal pkb) {
        this.pkb = pkb;
    }

    public BigDecimal getSwdkllj() {
        return swdkllj;
    }

    public void setSwdkllj(BigDecimal swdkllj) {
        this.swdkllj = swdkllj;
    }

    public BigDecimal getTnkb() {
        return tnkb;
    }

    public void setTnkb(BigDecimal tnkb) {
        this.tnkb = tnkb;
    }

    public BigDecimal getJasa() {
        return jasa;
    }

    public void setJasa(BigDecimal jasa) {
        this.jasa = jasa;
    }

    public BigDecimal getStnkbpkb() {
        return stnkbpkb;
    }

    public void setStnkbpkb(BigDecimal stnkbpkb) {
        this.stnkbpkb = stnkbpkb;
    }

    public Organization getOrganizationOwner() {
        return organizationOwner;
    }

    public void setOrganizationOwner(Organization organizationOwner) {
        this.organizationOwner = organizationOwner;
    }

    public BigDecimal getCostHandling() {
        return costHandling;
    }

    public void setCostHandling(BigDecimal costHandling) {
        this.costHandling = costHandling;
    }

    public String getNote() {
        return note;
    }

    public ZonedDateTime getInvoicePrint() { return invoicePrint; }

    public void setInvoicePrint(ZonedDateTime invoicePrint) { this.invoicePrint = invoicePrint; }

    public VehicleDocumentRequirement note(String note) {
        this.note = note;
        return this;
    }

    public String getStatusFaktur() {
        return statusFaktur;
    }

    public void setStatusFaktur(String statusFaktur) {
        this.statusFaktur = statusFaktur;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public BigDecimal getBbn() {
        return bbn;
    }

    public VehicleDocumentRequirement bbn(BigDecimal bbn) {
        this.bbn = bbn;
        return this;
    }

    public void setBbn(BigDecimal bbn) {
        this.bbn = bbn;
    }

    public BigDecimal getOtherCost() {
        return otherCost;
    }

    public VehicleDocumentRequirement otherCost(BigDecimal otherCost) {
        this.otherCost = otherCost;
        return this;
    }

    public void setOtherCost(BigDecimal otherCost) {
        this.otherCost = otherCost;
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    public SaleType getSaleType() {
        return saleType;
    }

    public void setSaleType(SaleType saleType) {
        this.saleType = saleType;
    }

    public String getFakturATPM() {
        return fakturATPM;
    }

    public void setFakturATPM(String fakturATPM) {
        this.fakturATPM = fakturATPM;
    }

    public ZonedDateTime getFakturDate() {
        return fakturDate;
    }

    public void setFakturDate(ZonedDateTime fakturDate) {
        this.fakturDate = fakturDate;
    }

    public String getRequestPoliceNumber() {
        return requestPoliceNumber;
    }

    public void setRequestPoliceNumber(String requestPoliceNumber) {
        this.requestPoliceNumber = requestPoliceNumber;
    }

    public String getSubmissionNo() {
        return submissionNo;
    }

    public void setSubmissionNo(String submissionNo) {
        this.submissionNo = submissionNo;
    }

    public ZonedDateTime getDateSubmission() {
        return dateSubmission;
    }

    public void setDateSubmission(ZonedDateTime dateSubmission) {
        this.dateSubmission = dateSubmission;
    }

    public Internal getInternal() {
        return internal;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public Person getPersonOwner() {
        return personOwner;
    }

    public void setPersonOwner(Person personOwner) {
        this.personOwner = personOwner;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public ShipTo getShipTo() {
        return shipTo;
    }

    public void setShipTo(ShipTo shipTo) {
        this.shipTo = shipTo;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public BillTo getBillTo() {
        return billTo;
    }

    public void setBillTo(BillTo billTo) {
        this.billTo = billTo;
    }

    public ZonedDateTime getDateFillName() {
        return dateFillName;
    }

    public void setDateFillName(ZonedDateTime dateFillName) {
        this.dateFillName = dateFillName;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Boolean getWaitStnk() {
        return waitStnk;
    }

    public UUID getIdSalesRequirement() {
        return idSalesRequirement;
    }

    public void setIdSalesRequirement(UUID idSalesRequirement) {
        this.idSalesRequirement = idSalesRequirement;
    }

    public void setWaitStnk(Boolean waitStnk) {
        this.waitStnk = waitStnk;
    }

    //    public ZonedDateTime getDateFaktur() {
//        return dateFaktur;
//    }
//
//    public void setDateFaktur(ZonedDateTime dateFaktur) {
//        this.dateFaktur = dateFaktur;
//    }
//
//    public ZonedDateTime getDateBiroJasa() {
//        return dateBiroJasa;
//    }
//
//    public void setDateBiroJasa(ZonedDateTime dateBiroJasa) {
//        this.dateBiroJasa = dateBiroJasa;
//    }
//
//    public ZonedDateTime getDateStnkBpkb() {
//        return dateStnkBpkb;
//    }
//
//    public void setDateStnkBpkb(ZonedDateTime dateStnkBpkb) {
//        this.dateStnkBpkb = dateStnkBpkb;
//    }

    @Override
    public String toString() {
        return "VehicleDocumentRequirement{" +
            "id='" + getIdRequirement() + '\'' +
            ",note='" + note + '\'' +
            ", bbn=" + bbn +
            ", otherCost=" + otherCost +
            // ", idSaleType=" + saleType.getIdSaleType() +
            ", fakturATPM='" + fakturATPM + '\'' +
            ", requestPoliceNumber='" + requestPoliceNumber + '\'' +
            ", orderItem=" + orderItem +
            ", internal=" + internal +
            ", personOwner=" + personOwner +
            ", vehicle=" + vehicle +
            ", organizationOwner=" + organizationOwner +
            '}';
    }
}
