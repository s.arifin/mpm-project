package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity Product.
 */

@Entity
@Table(name = "product")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "idprotyp")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "product")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idproduct")
    private String idProduct;

    @Column(name = "name")
    private String name;

    @Column(name = "dtintroduction")
    private ZonedDateTime dateIntroduction;

    @Column(name = "description")
    private String description;

    @Column(name = "istaxable")
    private Boolean taxable;

    @Column(name = "pricetype")
    private Integer priceType;

    @Column(name = "dtdiscontinue")
    private ZonedDateTime dateDiscontinue;

    @ManyToOne
    @JoinColumn(name="idprotyp", referencedColumnName="idprotyp", insertable = false, updatable = false)
    private ProductType productType;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "product_feature_impl",
               joinColumns = @JoinColumn(name="products_id", referencedColumnName="idproduct"),
               inverseJoinColumns = @JoinColumn(name="features_id", referencedColumnName="idfeature"))
    private Set<Feature> features = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "product_category_impl",
               joinColumns = @JoinColumn(name="idproduct", referencedColumnName="idproduct"),
               inverseJoinColumns = @JoinColumn(name="idcategory", referencedColumnName="idcategory"))
    private Set<ProductCategory> categories = new HashSet<>();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdProduct() {
        return this.idProduct;
    }

    public void setIdProduct(String id) {
        this.idProduct = id;
    }

    public String getName() {
        return name;
    }

    public Boolean getTaxable() {
        return taxable;
    }

    public void setTaxable(Boolean taxable) {
        this.taxable = taxable;
    }

    public Integer getPriceType() {
        return priceType;
    }

    public void setPriceType(Integer priceType) {
        this.priceType = priceType;
    }

    public Product name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getDateIntroduction() {
        return dateIntroduction;
    }

    public Product dateIntroduction(ZonedDateTime dateIntroduction) {
        this.dateIntroduction = dateIntroduction;
        return this;
    }

    public void setDateIntroduction(ZonedDateTime dateIntroduction) {
        this.dateIntroduction = dateIntroduction;
    }

    public ProductType getProductType() {
        return productType;
    }

    public Product productType(ProductType productType) {
        this.productType = productType;
        return this;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Set<Feature> getFeatures() {
        return features;
    }

    public Product features(Set<Feature> features) {
        this.features = features;
        return this;
    }

    public Product addFeature(Feature feature) {
        this.features.add(feature);
        return this;
    }

    public Product removeFeature(Feature feature) {
        this.features.remove(feature);
        return this;
    }

    public void setFeatures(Set<Feature> features) {
        this.features = features;
    }

    public Set<ProductCategory> getCategories() {
        return categories;
    }

    public Product categories(Set<ProductCategory> productCategories) {
        this.categories = productCategories;
        return this;
    }

    public Product addCategory(ProductCategory productCategory) {
        this.categories.add(productCategory);
        return this;
    }

    public Product removeCategory(ProductCategory productCategory) {
        this.categories.remove(productCategory);
        return this;
    }

    public void setCategories(Set<ProductCategory> productCategories) {
        this.categories = productCategories;
    }

    public ZonedDateTime getDateDiscontinue() {
        return dateDiscontinue;
    }

    public void setDateDiscontinue(ZonedDateTime dateDiscontinue) {
        this.dateDiscontinue = dateDiscontinue;
    }

    @PrePersist
    public void prePersist() {
        if (this.dateIntroduction == null) this.dateIntroduction = ZonedDateTime.now();
        if (this.taxable == null) this.taxable = true;
        if (this.priceType == null) this.priceType = 0;
    }

    @PreUpdate
    public void preUpdate() {
        if (this.taxable == null) this.taxable = true;
        if (this.priceType == null) this.priceType = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Product product = (Product) o;
        if (product.idProduct == null || this.idProduct == null) {
            return false;
        }
        return Objects.equals(this.idProduct, product.idProduct);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idProduct);
    }

    @Override
    public String toString() {
        return "Product{" +
            "idProduct=" + this.idProduct +
            ", name='" + getName() + "'" +
            ", dateIntroduction='" + getDateIntroduction() + "'" +
            '}';
    }
}
