package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


/**
 * atiila consulting
 * Class definition for Entity DimensionType.
 */

@Entity
@Table(name = "dimension_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dimensiontype")
public class DimensionType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iddimtyp")
    private Integer idDimensionType;

    @Column(name = "description")
    private String description;

    public Integer getIdDimensionType() {
        return this.idDimensionType;
    }

    public void setIdDimensionType(Integer id) {
        this.idDimensionType = id;
    }

    public String getDescription() {
        return description;
    }

    public DimensionType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DimensionType dimensionType = (DimensionType) o;
        if (dimensionType.idDimensionType == null || this.idDimensionType == null) {
            return false;
        }
        return Objects.equals(this.idDimensionType, dimensionType.idDimensionType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idDimensionType);
    }

    @Override
    public String toString() {
        return "DimensionType{" +
            "idDimensionType=" + this.idDimensionType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
