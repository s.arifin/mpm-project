package id.atiila.domain;

import javax.persistence.*;
import java.util.UUID;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * BeSmart Team
 * Class definition for Entity ShipmentOutgoing.
 */

@Entity
@Table(name = "shipment_outgoing")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "shipmentoutgoing")
public class ShipmentOutgoing extends Shipment {

    private static final long serialVersionUID = 1L;

    @Column(name = "iddriver")
    private UUID idDriver;

    @Column(name = "othername")
    private String otherName;

    @Column(name = "deliveryopt")
    private String deliveryOpt;

    @Column(name = "deliveryaddress")
    private String deliveryAddress;

    @Column(name = "printcount")
    private Integer jumlahprint;

    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public String getDeliveryOpt() {
        return deliveryOpt;
    }

    public void setDeliveryOpt(String deliveryOpt) {
        this.deliveryOpt = deliveryOpt;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public UUID getIdriver() {
        return idDriver;
    }

    public void setIddriver(UUID idDriver) {
        this.idDriver = idDriver;
    }

    public Integer getJumlahprint() {
        return jumlahprint;
    }

    public void setJumlahprint(Integer jumlahprint) {
        this.jumlahprint = jumlahprint;
    }

    @Override
    public String toString() {
        return "ShipmentOutgoing{" +
            "idShipment=" + this.getIdShipment() + "'" +
            "otherName=" + this.getOtherName() +
            '}';
    }
}
