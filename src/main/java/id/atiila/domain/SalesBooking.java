package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZoneId;
import java.time.ZonedDateTime;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity SalesBooking.
 */

@Entity
@Table(name = "sales_booking")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "salesbooking")
public class SalesBooking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idslsboo", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idSalesBooking;

    @Column(name = "idinternal")
    private String idInternal;

    @Column(name = "idproduct")
    private String idProduct;

    @Column(name = "idfeature")
    private Integer idFeature;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idreq", referencedColumnName="idreq")
    private Requirement requirement;

    private String note;

    @Column(name="yearassembly")
    private Integer yearAssembly;

    @Column(name = "onhand")
    private Boolean onHand;

    @Column(name = "intransit")
    private Boolean inTransit;

    //getter and setter
    public Boolean getOnHand() {
        return onHand;
    }

    public void setOnHand(Boolean onHand) {
        this.onHand = onHand;
    }

    public Boolean getInTransit() {
        return inTransit;
    }

    public void setInTransit(Boolean inTransit) {
        this.inTransit = inTransit;
    }

    public Integer getYearAssembly() {
        return yearAssembly;
    }

    public void setYearAssembly(Integer yearAssembly) {
        this.yearAssembly = yearAssembly;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public UUID getIdSalesBooking() {
        return this.idSalesBooking;
    }

    public void setIdSalesBooking(UUID id) {
        this.idSalesBooking = id;
    }

    public String getIdInternal() {
        return idInternal;
    }

    public SalesBooking idInternal(String idInternal) {
        this.idInternal = idInternal;
        return this;
    }

    public void setIdInternal(String idInternal) {
        this.idInternal = idInternal;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public SalesBooking idProduct(String idProduct) {
        this.idProduct = idProduct;
        return this;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdFeature() {
        return idFeature;
    }

    public SalesBooking idFeature(Integer idFeature) {
        this.idFeature = idFeature;
        return this;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public SalesBooking dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public SalesBooking dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Requirement getRequirement() {
        return requirement;
    }

    public SalesBooking requirement(Requirement requirement) {
        this.requirement = requirement;
        return this;
    }

    public void setRequirement(Requirement requirement) {
        this.requirement = requirement;
    }

    @PrePersist
    public void prePersist() {
        if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
        if (this.dateThru == null)
            this.dateThru = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SalesBooking salesBooking = (SalesBooking) o;
        if (salesBooking.idSalesBooking == null || this.idSalesBooking == null) {
            return false;
        }
        return Objects.equals(this.idSalesBooking, salesBooking.idSalesBooking);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idSalesBooking);
    }

    @Override
    public String toString() {
        return "SalesBooking{" +
            "idSalesBooking=" + this.idSalesBooking +
            ", idInternal='" + getIdInternal() + "'" +
            ", idProduct='" + getIdProduct() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
