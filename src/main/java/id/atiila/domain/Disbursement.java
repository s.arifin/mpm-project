package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity Disbursement.
 */

@Entity
@Table(name = "disbursement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "disbursement")
public class Disbursement extends Payment {

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Disbursement disbursement = (Disbursement) o;
        if (disbursement.getIdPayment() == null || this.getIdPayment() == null) {
            return false;
        }
        return Objects.equals(this.getIdPayment(), disbursement.getIdPayment());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdPayment());
    }

    @Override
    public String toString() {
        return "Disbursement{" +
            "idPayment=" + this.getIdPayment() +
            ", paymentNumber='" + getPaymentNumber() + "'" +
            ", refferenceNumber='" + getRefferenceNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", amount='" + getAmount() + "'" +
            '}';
    }
}
