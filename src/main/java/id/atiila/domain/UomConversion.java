package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity UomConversion.
 */

@Entity
@Table(name = "uom_conversion")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "uomconversion")
public class UomConversion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iduomconversion")
    private Integer idUomConversion;

    @Column(name = "factor")
    private Double factor;

    @ManyToOne
    @JoinColumn(name="iduomto", referencedColumnName="iduom")
    private Uom uomTo;

    @ManyToOne
    @JoinColumn(name="iduomfro", referencedColumnName="iduom")
    private Uom uomFrom;

    public Integer getIdUomConversion() {
        return this.idUomConversion;
    }

    public void setIdUomConversion(Integer id) {
        this.idUomConversion = id;
    }

    public Double getFactor() {
        return factor;
    }

    public UomConversion factor(Double factor) {
        this.factor = factor;
        return this;
    }

    public void setFactor(Double factor) {
        this.factor = factor;
    }

    public Uom getUomTo() {
        return uomTo;
    }

    public UomConversion uomTo(Uom uom) {
        this.uomTo = uom;
        return this;
    }

    public void setUomTo(Uom uom) {
        this.uomTo = uom;
    }

    public Uom getUomFrom() {
        return uomFrom;
    }

    public UomConversion uomFrom(Uom uom) {
        this.uomFrom = uom;
        return this;
    }

    public void setUomFrom(Uom uom) {
        this.uomFrom = uom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UomConversion uomConversion = (UomConversion) o;
        if (uomConversion.idUomConversion == null || this.idUomConversion == null) {
            return false;
        }
        return Objects.equals(this.idUomConversion, uomConversion.idUomConversion);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idUomConversion);
    }

    @Override
    public String toString() {
        return "UomConversion{" +
            "idUomConversion=" + this.idUomConversion +
            ", factor='" + getFactor() + "'" +
            '}';
    }
}
