package id.atiila.domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import id.atiila.base.BaseConstants;

/**
 * BeSmart Team
 * Class definition for Entity Mechanic.
 */

@Entity
@Table(name = "mechanic")
@DiscriminatorValue(BaseConstants.ROLE_MECHANIC)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "mechanic")
public class Mechanic extends PartyRole {

    private static final long serialVersionUID = 1L;

    @Column(name = "idmechanic")
    private String idMechanic;

    @Column(name = "idvendor")
    private String idVendor;

    @Column(name = "isexternal")
    private Boolean external;

    public String getIdMechanic() {
        return idMechanic;
    }

    public void setIdMechanic(String idMechanic) {
        this.idMechanic = idMechanic;
    }

    public Person getPerson() {
        return (Person) getParty();
    }

    public void setPerson(Person p) {
        setParty(p);
    }

    public Boolean getExternal() {
        return external;
    }

    public void setExternal(Boolean external) {
        this.external = external;
    }

    public String getIdVendor() {
        return idVendor;
    }

    public void setIdVendor(String idVendor) {
        this.idVendor = idVendor;
    }

    @Override
    public String toString() {
        return "Mechanic{" +
            "idPartyRole=" + this.getIdPartyRole() +
            ", idMechanic='" + getIdMechanic() + "'" +
            ", external='" + getExternal() + "'" +
            ", idVendor='" + getIdVendor() + "'" +
            '}';
    }
}
