package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity UserMediator.
 */

@Entity
@Table(name = "user_mediator")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "usermediator")
public class UserMediator implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idusrmed", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idUserMediator;

    @Column(name = "username")
    private String userName;

    @Column(name = "email")
    private String email;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name="idperson", referencedColumnName="idparty")
    private Person person;

    @JsonIgnore
    @OneToMany(mappedBy = "owner")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Position> positions = new HashSet<>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<UserMediatorStatus> statuses = new ArrayList<UserMediatorStatus>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<UserMediatorRole> roles = new ArrayList<UserMediatorRole>();

    public UUID getIdUserMediator() {
        return this.idUserMediator;
    }

    public void setIdUserMediator(UUID id) {
        this.idUserMediator = id;
    }

    public String getUserName() {
        return userName;
    }

    public UserMediator userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public UserMediator email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Internal getInternal() {
        return internal;
    }

    public UserMediator internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public Person getPerson() {
        return person;
    }

    public UserMediator person(Person person) {
        this.person = person;
        return this;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Set<Position> getPositions() {
        return positions;
    }

    public UserMediator positions(Set<Position> positions) {
        this.positions = positions;
        return this;
    }

    public UserMediator addPosition(Position position) {
        this.positions.add(position);
        return this;
    }

    public UserMediator removePosition(Position position) {
        this.positions.remove(position);
        return this;
    }

    public void setPositions(Set<Position> positions) {
        this.positions = positions;
    }

    public List<UserMediatorRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<UserMediatorRole> roles) {
        this.roles = roles;
    }

    public void setPartyRole(String user, Integer role)  {
        for (UserMediatorRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) return;
        }

        UserMediatorRole current = new UserMediatorRole();
        current.setOwner(this);
        current.setIdRoleType(role);
		current.setUser(user);
        current.setDateFrom(ZonedDateTime.now());
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        roles.add(current);
    }

    public String getPartyRole(String user, Integer role)  {
        for (UserMediatorRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) return current.getUser();
        }
        return null;
    }

    public void removePartyRole(String user, Integer role)  {
        for (UserMediatorRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) {
				current.setDateThru(ZonedDateTime.now());
				return;
			}
        }
    }

    public List<UserMediatorStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<UserMediatorStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);

        for (UserMediatorStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now();

        for (UserMediatorStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        UserMediatorStatus current = new UserMediatorStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public Boolean getActiveData() {
        switch (getCurrentStatus()) {
            case BaseConstants.STATUS_DRAFT:
            case BaseConstants.STATUS_ACTIVE:
                return true;
        }
        return false;
    }

    @PrePersist
    public void prePersist() {
        if (getStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @PreUpdate
    public void preUpdate() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserMediator userMediator = (UserMediator) o;
        if (userMediator.idUserMediator == null || this.idUserMediator == null) {
            return false;
        }
        return Objects.equals(this.idUserMediator, userMediator.idUserMediator);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idUserMediator);
    }

    @Override
    public String toString() {
        return "UserMediator{" +
            "idUserMediator=" + this.idUserMediator +
            ", userName='" + getUserName() + "'" +
            ", email='" + getEmail() + "'" +
            '}';
    }
}
