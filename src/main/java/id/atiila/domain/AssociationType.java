package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity AssociationType.
 */

@Entity
@Table(name = "association_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "associationtype")
public class AssociationType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idasstyp")
    private Integer idAssociattionType;

    @Column(name = "description")
    private String description;

    public Integer getIdAssociattionType() {
        return this.idAssociattionType;
    }

    public void setIdAssociattionType(Integer id) {
        this.idAssociattionType = id;
    }

    public String getDescription() {
        return description;
    }

    public AssociationType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AssociationType associationType = (AssociationType) o;
        if (associationType.idAssociattionType == null || this.idAssociattionType == null) {
            return false;
        }
        return Objects.equals(this.idAssociattionType, associationType.idAssociattionType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idAssociattionType);
    }

    @Override
    public String toString() {
        return "AssociationType{" +
            "idAssociattionType=" + this.idAssociattionType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
