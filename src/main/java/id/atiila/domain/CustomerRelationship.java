package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.*;
import java.util.Objects;

/**
 * atiila consulting
 * Class definition for Entity CustomerRelationship.
 */

@Entity
@Table(name = "customer_relationship")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "customerrelationship")
public class CustomerRelationship extends PartyRelationship {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idcustomer", referencedColumnName="idcustomer")
    private Customer customer;

    public Internal getInternal() {
        return internal;
    }

    public CustomerRelationship internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public Customer getCustomer() {
        return customer;
    }

    public CustomerRelationship customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CustomerRelationship customerRelationship = (CustomerRelationship) o;
        if (customerRelationship.getIdPartyRelationship() == null || this.getIdPartyRelationship() == null) {
            return false;
        }
        return Objects.equals(this.getIdPartyRelationship(), customerRelationship.getIdPartyRelationship());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdPartyRelationship());
    }

    @Override
    public String toString() {
        return "CustomerRelationship{" +
            "idPartyRelationship=" + this.getIdPartyRelationship() +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
