package id.atiila.domain;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import id.atiila.base.BaseConstants;

/**
 * BeSmart Team
 * Class definition for Entity SalesBroker.
 */

@Entity
@Table(name = "sales_broker")
@DiscriminatorValue(BaseConstants.ROLE_SALES_BROKER)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "salesbroker")
public class SalesBroker extends PartyRole {

    private static final long serialVersionUID = 1L;

    @Column(name = "idbroker")
    private String idBroker;

    @ManyToOne
    @JoinColumn(name="idbrotyp", referencedColumnName="idbrotyp")
    private BrokerType brokerType;

    public String getIdBroker() {
        return idBroker;
    }

    public SalesBroker idBroker(String idBroker) {
        this.idBroker = idBroker;
        return this;
    }

    public void setIdBroker(String idBroker) {
        this.idBroker = idBroker;
    }

    public BrokerType getBrokerType() { return brokerType; }

    public SalesBroker brokerType(BrokerType brokerType) {
        this.brokerType = brokerType;
        return this;
    }

    public void setBrokerType(BrokerType brokerType) { this.brokerType = brokerType; }

    public Person getPerson() {
        return (Person) getParty();
    }

    public void setPerson(Person p) {
        setParty(p);
    }

    @Override
    public String toString() {
        return "SalesBroker{" +
            "idPartyRole=" + this.getIdPartyRole() +
            ", idBroker='" + getIdBroker() + "'" +
            '}';
    }
}
