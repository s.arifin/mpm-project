package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity Container.
 */

@Entity
@Table(name = "container")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "container")
public class Container implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idcontainer", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idContainer;

    @Column(name = "code")
    private String containerCode;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name="idfacility", referencedColumnName="idfacility")
    private Facility facility;

    @ManyToOne
    @JoinColumn(name="idcontyp", referencedColumnName="idcontyp")
    private ContainerType containerType;

    public UUID getIdContainer() {
        return this.idContainer;
    }

    public void setIdContainer(UUID id) {
        this.idContainer = id;
    }

    public String getContainerCode() {
        return containerCode;
    }

    public Container containerCode(String containerCode) {
        this.containerCode = containerCode;
        return this;
    }

    public void setContainerCode(String containerCode) {
        this.containerCode = containerCode;
    }

    public String getDescription() {
        return description;
    }

    public Container description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Facility getFacility() {
        return facility;
    }

    public Container facility(Facility facility) {
        this.facility = facility;
        return this;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    public ContainerType getContainerType() {
        return containerType;
    }

    public Container containerType(ContainerType containerType) {
        this.containerType = containerType;
        return this;
    }

    public void setContainerType(ContainerType containerType) {
        this.containerType = containerType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Container container = (Container) o;
        if (container.idContainer == null || this.idContainer == null) {
            return false;
        }
        return Objects.equals(this.idContainer, container.idContainer);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idContainer);
    }

    @Override
    public String toString() {
        return "Container{" +
            "idContainer=" + this.idContainer +
            ", containerCode='" + getContainerCode() + "'" +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
