package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.*;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity ProductDocument.
 */

@Entity
@Table(name = "product_document")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "productdocument")
public class ProductDocument extends Documents {

    private static final long serialVersionUID = 1L;

    @Lob
    @Column(name = "content")
    private byte[] content;

    @Column(name = "contenttype")
    private String contentContentType;

    @Column(name = "note")
    private String note;

    @Column(name = "datecreate")
    private ZonedDateTime dateCreate;

    @ManyToOne
    @JoinColumn(name="idproduct", referencedColumnName="idproduct")
    private Product product;

    public byte[] getContent() {
        return content;
    }

    public ProductDocument content(byte[] content) {
        this.content = content;
        return this;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentContentType() {
        return contentContentType;
    }

    public ProductDocument contentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
        return this;
    }

    public void setContentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
    }

    public String getNote() {
        return note;
    }

    public ProductDocument note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public ProductDocument dateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
        return this;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Product getProduct() {
        return product;
    }

    public ProductDocument product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "ProductDocument{" +
            "idDocument=" + this.getIdDocument() +
            ", content='" + getContent() + "'" +
            ", contentContentType='" + contentContentType + "'" +
            ", note='" + getNote() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            '}';
    }
}
