package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.time.ZoneId;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity ProductAssociation.
 */

@Entity
@Table(name = "product_association")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "productassociation")
public class ProductAssociation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idproass", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idProductAssociation;

    @Column(name = "qty")
    private Double qty;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idproductto", referencedColumnName="idproduct")
    private Product productTo;

    @ManyToOne
    @JoinColumn(name="idproductfrom", referencedColumnName="idproduct")
    private Product productFrom;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public UUID getIdProductAssociation() {
        return this.idProductAssociation;
    }

    public void setIdProductAssociation(UUID id) {
        this.idProductAssociation = id;
    }

    public Double getQty() {
        return qty;
    }

    public ProductAssociation qty(Double qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public ProductAssociation dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public ProductAssociation dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Product getProductTo() {
        return productTo;
    }

    public ProductAssociation productTo(Product product) {
        this.productTo = product;
        return this;
    }

    public void setProductTo(Product product) {
        this.productTo = product;
    }

    public Product getProductFrom() {
        return productFrom;
    }

    public ProductAssociation productFrom(Product product) {
        this.productFrom = product;
        return this;
    }

    public void setProductFrom(Product product) {
        this.productFrom = product;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @PrePersist
    public void prePersist() {
        if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
        if (this.dateThru == null)
            this.dateThru = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ProductAssociation that = (ProductAssociation) o;

        return new EqualsBuilder()
            .append(idProductAssociation, that.idProductAssociation)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idProductAssociation)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "ProductAssociation{" +
            "idProductAssociation=" + this.idProductAssociation +
            ", qty='" + getQty() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
