package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity PurposeType.
 */

@Entity
@Table(name = "purpose_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "purposetype")
public class PurposeType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idpurposetype")
    private Integer idPurposeType;

    @Column(name = "description")
    private String description;

    public PurposeType() {
    }

    public PurposeType(Integer idPurposeType) {
        this.idPurposeType = idPurposeType;
    }

    public Integer getIdPurposeType() {
        return this.idPurposeType;
    }

    public void setIdPurposeType(Integer id) {
        this.idPurposeType = id;
    }

    public String getDescription() {
        return description;
    }

    public PurposeType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PurposeType purposeType = (PurposeType) o;
        if (purposeType.idPurposeType == null || this.idPurposeType == null) {
            return false;
        }
        return Objects.equals(this.idPurposeType, purposeType.idPurposeType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPurposeType);
    }

    @Override
    public String toString() {
        return "PurposeType{" +
            "idPurposeType=" + this.idPurposeType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
