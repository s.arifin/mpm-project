package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity MotorDueReminder.
 */

@Entity
@Table(name = "motor_due_reminder")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "motorduereminder")
public class MotorDueReminder implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idreminder")
    private Integer idReminder;

    @Column(name = "nservice1")
    private Integer service1;

    @Column(name = "nservice2")
    private Integer service2;

    @Column(name = "nservice3")
    private Integer service3;

    @Column(name = "nservice4")
    private Integer service4;

    @Column(name = "nservice5")
    private Integer service5;

    @Column(name = "km")
    private Integer km;

    @Column(name = "ndays")
    private Integer nDays;

    @ManyToOne
    @JoinColumn(name="idmotor", referencedColumnName="idproduct")
    private Motor motor;

    public Integer getIdReminder() {
        return this.idReminder;
    }

    public void setIdReminder(Integer id) {
        this.idReminder = id;
    }

    public Integer getService1() {
        return service1;
    }

    public MotorDueReminder service1(Integer service1) {
        this.service1 = service1;
        return this;
    }

    public void setService1(Integer service1) {
        this.service1 = service1;
    }

    public Integer getService2() {
        return service2;
    }

    public MotorDueReminder service2(Integer service2) {
        this.service2 = service2;
        return this;
    }

    public void setService2(Integer service2) {
        this.service2 = service2;
    }

    public Integer getService3() {
        return service3;
    }

    public MotorDueReminder service3(Integer service3) {
        this.service3 = service3;
        return this;
    }

    public void setService3(Integer service3) {
        this.service3 = service3;
    }

    public Integer getService4() {
        return service4;
    }

    public MotorDueReminder service4(Integer service4) {
        this.service4 = service4;
        return this;
    }

    public void setService4(Integer service4) {
        this.service4 = service4;
    }

    public Integer getService5() {
        return service5;
    }

    public MotorDueReminder service5(Integer service5) {
        this.service5 = service5;
        return this;
    }

    public void setService5(Integer service5) {
        this.service5 = service5;
    }

    public Motor getMotor() {
        return motor;
    }

    public MotorDueReminder motor(Motor motor) {
        this.motor = motor;
        return this;
    }

    public Integer getKm() {
        return km;
    }

    public void setKm(Integer km) {
        this.km = km;
    }

    public Integer getnDays() {
        return nDays;
    }

    public void setnDays(Integer nDays) {
        this.nDays = nDays;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MotorDueReminder motorDueReminder = (MotorDueReminder) o;
        if (motorDueReminder.idReminder == null || this.idReminder == null) {
            return false;
        }
        return Objects.equals(this.idReminder, motorDueReminder.idReminder);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idReminder);
    }

    @Override
    public String toString() {
        return "MotorDueReminder{" +
            "idReminder=" + this.idReminder +
            ", service1='" + getService1() + "'" +
            ", service2='" + getService2() + "'" +
            ", service3='" + getService3() + "'" +
            ", service4='" + getService4() + "'" +
            ", service5='" + getService5() + "'" +
            '}';
    }
}
