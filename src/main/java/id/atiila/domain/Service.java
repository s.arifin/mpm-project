package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.*;

/**
 * BeSmart Team
 * Class definition for Entity Service.
 */

@Entity
@Table(name = "service")
@DiscriminatorValue(BaseConstants.PRODUCT_TYPE_SERVICE)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "service")
public class Service extends Product {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name="iduom", referencedColumnName="iduom")
    private Uom uom;

    @JsonIgnoreProperties("service")
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "service")
    private WeServiceType weType;

    public Uom getUom() {
        return uom;
    }

    public Service uom(Uom uom) {
        this.uom = uom;
        return this;
    }

    public void setUom(Uom uom) {
        this.uom = uom;
    }

    public WeServiceType getWeType() {
        return weType;
    }

    public Service weType(WeServiceType weServiceType) {
        this.weType = weServiceType;
        this.weType.setService(this);
        return this;
    }

    public void setWeType(WeServiceType weServiceType) {
        this.weType = weServiceType;
        this.weType.setService(this);
    }

    @Override
    public void preUpdate() {
        super.preUpdate();
        if (this.weType == null) {
            setWeType(new WeServiceType());
            this.weType.setFlatRateTime(0);
        }
    }

    @Override
    public String toString() {
        return "Service{" +
            "idProduct=" + this.getIdProduct() +
            ", name='" + getName() + "'" +
            ", dateIntroduction='" + getDateIntroduction() + "'" +
            '}';
    }
}
