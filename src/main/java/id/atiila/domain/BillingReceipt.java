package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity BillingReceipt.
 */

@Entity
@Table(name = "billing_receipt")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "billingreceipt")
public class BillingReceipt extends Billing {

    private static final long serialVersionUID = 1L;

    public BillingItem addMotorCycle(OrderItem o) {
        BillingItem b = new BillingItem();
        b.setIdItemType(BaseConstants.ITEM_TYPE_PRODUCT);
        b.setIdProduct(o.getIdProduct());
        b.setItemDescription(o.getItemDescription());
        b.setSequence(10 + getDetails().size());
        b.setQty(o.getQty());
        b.setUnitPrice(o.getUnitPrice());
        b.setIdFeature(o.getIdFeature());
        b.setDiscount(0);
        b.setTaxAmount(0);
        b.setBasePrice(0);
        b.setTotalAmount(o.getUnitPrice().doubleValue() * o.getQty());
        b.setBilling(this);
        this.getDetails().add(b);
        return b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BillingReceipt b = (BillingReceipt) o;
        if (b.getIdBilling() == null || this.getIdBilling() == null) {
            return false;
        }
        return Objects.equals(this.getIdBilling(), b.getIdBilling());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdBilling());
    }

    @Override
    public String toString() {
        return "BillingReceipt{" +
            "idBilling=" + this.getIdBilling() +
            ", billingNumber='" + getBillingNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", dateDue='" + getDateDue() + "'" +
            '}';
    }
}
