package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity RelationType.
 */

@Entity
@Table(name = "relation_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "relationtype")
public class RelationType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idreltyp")
    private Integer idRelationType;

    @Column(name = "description")
    private String description;

    public Integer getIdRelationType() {
        return this.idRelationType;
    }

    public void setIdRelationType(Integer id) {
        this.idRelationType = id;
    }

    public String getDescription() {
        return description;
    }

    public RelationType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RelationType relationType = (RelationType) o;
        if (relationType.idRelationType == null || this.idRelationType == null) {
            return false;
        }
        return Objects.equals(this.idRelationType, relationType.idRelationType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idRelationType);
    }

    @Override
    public String toString() {
        return "RelationType{" +
            "idRelationType=" + this.idRelationType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
