package id.atiila.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import id.atiila.base.BaseConstants;

/**
 * BeSmart Team
 * Class definition for Entity OrganizationCustomer.
 */

@Entity
@Table(name = "organization_customer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "organizationcustomer")
public class OrganizationCustomer extends Customer {

    private static final long serialVersionUID = 1L;

    public Organization getOrganization() {
        return (Organization) getParty();
    }

    public void setOrganization(Organization o) {
        setParty(o);
    }

    @Override
    public String toString() {
        return "OrganizationCustomer{" +
                "idCustomer='" + getIdCustomer() + "'" +
                '}';
    }
}
