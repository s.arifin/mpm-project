package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.time.ZonedDateTime;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity MovingSlip.
 */

@Entity
@Table(name = "moving_slip")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "movingslip")
public class MovingSlip extends InventoryMovement {

    private static final long serialVersionUID = 1L;

    @Column(name = "qty")
    private Integer qty;

    @Column(name = "dtcreate")
    private ZonedDateTime dateCreate;

    @ManyToOne
    @JoinColumn(name="idinviteto", referencedColumnName="idinvite")
    private InventoryItem inventoryItemTo;

    @ManyToOne
    @JoinColumn(name="idcontainerfrom", referencedColumnName="idcontainer")
    private Container containerFrom;

    @ManyToOne
    @JoinColumn(name="idcontainerto", referencedColumnName="idcontainer")
    private Container containerTo;

    @ManyToOne
    @JoinColumn(name="idfacilityfrom", referencedColumnName="idfacility")
    private Facility facilityFrom;

    @ManyToOne
    @JoinColumn(name="idfacilityto", referencedColumnName="idfacility")
    private Facility facilityTo;

    @ManyToOne
    @JoinColumn(name="idproduct", referencedColumnName="idproduct")
    private Product product;

    @ManyToOne
    @JoinColumn(name="idinvite", referencedColumnName="idinvite")
    private InventoryItem inventoryItem;

    public Integer getQty() {
        return qty;
    }

    public MovingSlip qty(Integer qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public InventoryItem getInventoryItemTo() {
        return inventoryItemTo;
    }

    public MovingSlip inventoryItemTo(InventoryItem inventoryItem) {
        this.inventoryItemTo = inventoryItem;
        return this;
    }

    public void setInventoryItemTo(InventoryItem inventoryItem) {
        this.inventoryItemTo = inventoryItem;
    }

    public Container getContainerFrom() {
        return containerFrom;
    }

    public MovingSlip containerFrom(Container container) {
        this.containerFrom = container;
        return this;
    }

    public void setContainerFrom(Container container) {
        this.containerFrom = container;
    }

    public Container getContainerTo() {
        return containerTo;
    }

    public MovingSlip containerTo(Container container) {
        this.containerTo = container;
        return this;
    }

    public void setContainerTo(Container container) {
        this.containerTo = container;
    }

    public Facility getFacilityFrom() {
        return facilityFrom;
    }

    public MovingSlip facilityFrom(Facility facility) {
        this.facilityFrom = facility;
        return this;
    }

    public void setFacilityFrom(Facility facility) {
        this.facilityFrom = facility;
    }

    public Facility getFacilityTo() {
        return facilityTo;
    }

    public MovingSlip facilityTo(Facility facility) {
        this.facilityTo = facility;
        return this;
    }

    public void setFacilityTo(Facility facility) {
        this.facilityTo = facility;
    }

    public Product getProduct() {
        return product;
    }

    public MovingSlip product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public InventoryItem getInventoryItem() {
        return inventoryItem;
    }

    public MovingSlip inventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
        return this;
    }

    public void setInventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
    }

    @PrePersist
    public void prePresist() {
        if (this.getDateCreate() == null){ this.setDateCreate(ZonedDateTime.now()); };
    }

    @Override
    public String toString() {
        return "MovingSlip{" +
            "idSlip=" + this.getIdSlip() +
            ", qty='" + getQty() + "'" +
            '}';
    }
}
