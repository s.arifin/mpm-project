package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity StockOpname.
 */

@Entity
@Table(name = "stock_opname")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "stockopname")
public class StockOpname implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idstkop", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idStockOpname;

    @Column(name = "stockopnamenumber")
    private String stockOpnameNumber;

    @Column(name = "dtcreated")
    private ZonedDateTime dateCreated;

    @ManyToOne
    @JoinColumn(name="idcalendar", referencedColumnName="idcalendar")
    private StandardCalendar calendar;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idstkopntyp", referencedColumnName="idstkopntyp")
    private StockOpnameType stockOpnameType;

    @OneToMany(mappedBy = "stockOpname", cascade = CascadeType.ALL)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<StockOpnameItem> items = new HashSet<>();

    public UUID getIdStockOpname() {
        return this.idStockOpname;
    }

    public void setIdStockOpname(UUID id) {
        this.idStockOpname = id;
    }

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<StockOpnameStatus> statuses = new ArrayList<StockOpnameStatus>();

    public String getStockOpnameNumber() {
        return stockOpnameNumber;
    }

    public StockOpname stockOpnameNumber(String stockOpnameNumber) {
        this.stockOpnameNumber = stockOpnameNumber;
        return this;
    }

    public void setStockOpnameNumber(String stockOpnameNumber) {
        this.stockOpnameNumber = stockOpnameNumber;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public StockOpname dateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public StandardCalendar getCalendar() {
        return calendar;
    }

    public StockOpname calendar(StandardCalendar standardCalendar) {
        this.calendar = standardCalendar;
        return this;
    }

    public void setCalendar(StandardCalendar standardCalendar) {
        this.calendar = standardCalendar;
    }

    public Internal getInternal() {
        return internal;
    }

    public StockOpname internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public StockOpnameType getStockOpnameType() {
        return stockOpnameType;
    }

    public StockOpname stockOpnameType(StockOpnameType stockOpnameType) {
        this.stockOpnameType = stockOpnameType;
        return this;
    }

    public void setStockOpnameType(StockOpnameType stockOpnameType) {
        this.stockOpnameType = stockOpnameType;
    }

    public Set<StockOpnameItem> getItems() {
        return items;
    }

    public StockOpname items(Set<StockOpnameItem> stockOpnameItems) {
        this.items = stockOpnameItems;
        return this;
    }

    public StockOpname addItems(StockOpnameItem stockOpnameItem) {
        this.items.add(stockOpnameItem);
        stockOpnameItem.setStockOpname(this);
        return this;
    }

    public StockOpname removeItems(StockOpnameItem stockOpnameItem) {
        this.items.remove(stockOpnameItem);
        stockOpnameItem.setStockOpname(null);
        return this;
    }

    public void setItems(Set<StockOpnameItem> stockOpnameItems) {
        this.items = stockOpnameItems;
    }

    public List<StockOpnameStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<StockOpnameStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);

        for (StockOpnameStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now();

        for (StockOpnameStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        StockOpnameStatus current = new StockOpnameStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StockOpname stockOpname = (StockOpname) o;
        if (stockOpname.idStockOpname == null || this.idStockOpname == null) {
            return false;
        }
        return Objects.equals(this.idStockOpname, stockOpname.idStockOpname);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idStockOpname);
    }

    @Override
    public String toString() {
        return "StockOpname{" +
            "idStockOpname=" + this.idStockOpname +
            ", stockOpnameNumber='" + getStockOpnameNumber() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            '}';
    }
}
