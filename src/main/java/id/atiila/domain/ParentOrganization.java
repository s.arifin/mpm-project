package id.atiila.domain;

import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.*;

/**
 * BeSmart Team
 * Class definition for Entity ParentOrganization.
 */

@Entity
@Table(name = "parent_organization")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Document(indexName = "parentorganization")
public class ParentOrganization extends Internal {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "ParentOrganization{" +
            "idInternal='" + getIdInternal() + "'" +
            '}';
    }
}
