package id.atiila.domain;

import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity Branch.
 */

@Entity
@Table(name = "branch")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "branch")
public class Branch extends Internal {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Branch{" +
            "idInternal='" + getIdInternal() + "'" +
            '}';
    }
}
