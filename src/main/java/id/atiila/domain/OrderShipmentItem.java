package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity OrderShipmentItem.
 */

@Entity
@Table(name = "order_shipment_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "ordershipmentitem")
public class OrderShipmentItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idordshiite", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idOrderShipmentItem;

    @Column(name = "qty")
    private Double qty;

    @ManyToOne
    @JoinColumn(name="idordite", referencedColumnName="idordite")
    private OrderItem orderItem;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name="idshiite", referencedColumnName="idshiite")
    private ShipmentItem shipmentItem;

    public UUID getIdOrderShipmentItem() {
        return this.idOrderShipmentItem;
    }

    public void setIdOrderShipmentItem(UUID id) {
        this.idOrderShipmentItem = id;
    }

    public Double getQty() {
        return qty;
    }

    public OrderShipmentItem qty(Double qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public void setQty(Integer qty) {
        this.qty = Double.valueOf(qty);
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public OrderShipmentItem orderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
        return this;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    public ShipmentItem getShipmentItem() {
        return shipmentItem;
    }

    public OrderShipmentItem shipmentItem(ShipmentItem shipmentItem) {
        this.shipmentItem = shipmentItem;
        return this;
    }

    public void setShipmentItem(ShipmentItem shipmentItem) {
        this.shipmentItem = shipmentItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderShipmentItem orderShipmentItem = (OrderShipmentItem) o;
        if (orderShipmentItem.idOrderShipmentItem == null || this.idOrderShipmentItem == null) {
            return false;
        }
        return Objects.equals(this.idOrderShipmentItem, orderShipmentItem.idOrderShipmentItem);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idOrderShipmentItem);
    }

    @Override
    public String toString() {
        return "OrderShipmentItem{" +
            "idOrderShipmentItem=" + this.idOrderShipmentItem +
            ", qty='" + getQty() + "'" +
            '}';
    }
}
