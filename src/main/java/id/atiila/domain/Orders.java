package id.atiila.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import id.atiila.base.BaseConstants;
import id.atiila.base.DomainEntity;

/**
 * BeSmart Team
 * Class definition for Entity Orders.
 */

@Entity
@Table(name = "orders")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
// @DiscriminatorColumn(name = "idordtyp")
@Document(indexName = "orders")
public class Orders extends AuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idorder", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idOrder;

    @Column(name = "ordernumber")
    private String orderNumber;

    @CreationTimestamp
    @Column(name = "dtentry")
    private ZonedDateTime dateEntry;

    @Column(name = "dtorder")
    private ZonedDateTime dateOrder;

    @Formula("(select sum(items.totalamount) from order_item items where items.idorder = idorder)")
    private BigDecimal totalAmount;

    @OneToMany(mappedBy = "orders", cascade = {CascadeType.ALL})
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<OrderItem> details = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="idordtyp", referencedColumnName = "idordtyp")
    private OrderType orderType;

    public UUID getIdOrder() {
        return this.idOrder;
    }

    public void setIdOrder(UUID id) {
        this.idOrder = id;
    }

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy = "owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<OrdersStatus> statuses = new ArrayList<OrdersStatus>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<OrdersRole> roles = new ArrayList<OrdersRole>();

    public String getOrderNumber() {
        return orderNumber;
    }

    public Orders orderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public ZonedDateTime getDateEntry() {
        return dateEntry;
    }

    public Orders dateEntry(ZonedDateTime dateEntry) {
        this.dateEntry = dateEntry;
        return this;
    }

    public void setDateEntry(ZonedDateTime dateEntry) {
        this.dateEntry = dateEntry;
    }

    public ZonedDateTime getDateOrder() {
        return dateOrder;
    }

    public Orders dateOrder(ZonedDateTime dateOrder) {
        this.dateOrder = dateOrder;
        return this;
    }

    public void setDateOrder(ZonedDateTime dateOrder) {
        this.dateOrder = dateOrder;
    }

    public Set<OrderItem> getDetails() {
        return details;
    }

    public Orders details(Set<OrderItem> orderItems) {
        this.details = orderItems;
        return this;
    }

    public Orders addDetail(OrderItem orderItem) {
        this.details.add(orderItem);
        orderItem.setOrders(this);
        return this;
    }

    public Orders removeDetail(OrderItem orderItem) {
        this.details.remove(orderItem);
        orderItem.setOrders(null);
        return this;
    }

    public void setDetails(Set<OrderItem> orderItems) {
        this.details = orderItems;
    }

    public List<OrdersRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<OrdersRole> roles) {
        this.roles = roles;
    }

    public void setPartyRole(Party party, Integer role)  {
        for (OrdersRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) return;
        }

        OrdersRole current = new OrdersRole();
        current.setOwner(this);
        current.setIdRoleType(role);
		current.setParty(party);
        current.setDateFrom(LocalDateTime.now());
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        roles.add(current);
    }

    public Party getPartyRole(Party party, Integer role)  {
        for (OrdersRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) return current.getParty();
        }
        return null;
    }

    public void removePartyRole(Party party, Integer role)  {
        for (OrdersRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) {
				current.setDateThru(LocalDateTime.now());
				return;
			}
        }
    }

    public List<OrdersStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OrdersStatus> statuses) {
        this.statuses = statuses;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);

        for (OrdersStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now();

        for (OrdersStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        OrdersStatus current = new OrdersStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 999, ZoneId.systemDefault()));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public Boolean getActiveData() {
        switch (getCurrentStatus()) {
            case BaseConstants.STATUS_DRAFT:
            case BaseConstants.STATUS_ACTIVE:
                return true;
        }
        return false;
    }

    public OrderItem getOrderItem(Product product, Double qty, BigDecimal unitPrice) {
        OrderItem orderItem = new OrderItem();
        orderItem.setOrders(this);
        orderItem.setIdProduct(product.getIdProduct());
        orderItem.setItemDescription(product.getName());
        orderItem.setQty(qty);
        orderItem.setUnitPrice(unitPrice);
        orderItem.setTaxAmount(unitPrice.doubleValue() * qty * 10/ 100);
        orderItem.setTotalAmount(qty * (unitPrice.doubleValue() + orderItem.getTaxAmount().doubleValue()));
        this.addDetail(orderItem);
        return orderItem;
    }

    @PrePersist
    public void prePersist() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @PreUpdate
    public void preUpdate() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Orders orders = (Orders) o;
        if (orders.idOrder == null || this.idOrder == null) {
            return false;
        }
        return Objects.equals(this.idOrder, orders.idOrder);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idOrder);
    }

    @Override
    public String toString() {
        return "Orders{" +
            "idOrder=" + this.idOrder +
            ", orderNumber='" + getOrderNumber() + "'" +
            ", dateEntry='" + getDateEntry() + "'" +
            ", dateOrder='" + getDateOrder() + "'" +
            '}';
    }
}
