package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity PriceType.
 */

@Entity
@Table(name = "price_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pricetype")
public class PriceType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpricetype")
    private Integer idPriceType;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name="idrultyp", referencedColumnName="idrultyp")
    private RuleType ruleType;

    public Integer getIdPriceType() {
        return this.idPriceType;
    }

    public void setIdPriceType(Integer id) {
        this.idPriceType = id;
    }

    public String getDescription() {
        return description;
    }

    public PriceType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RuleType getRuleType() {
        return ruleType;
    }

    public PriceType ruleType(RuleType ruleType) {
        this.ruleType = ruleType;
        return this;
    }

    public void setRuleType(RuleType ruleType) {
        this.ruleType = ruleType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PriceType priceType = (PriceType) o;
        if (priceType.idPriceType == null || this.idPriceType == null) {
            return false;
        }
        return Objects.equals(this.idPriceType, priceType.idPriceType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPriceType);
    }

    @Override
    public String toString() {
        return "PriceType{" +
            "idPriceType=" + this.idPriceType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
