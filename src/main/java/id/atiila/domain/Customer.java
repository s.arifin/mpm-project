package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity Customer.
 */

@Entity
@Table(name = "customer")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "customer")
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idcustomer")
    private String idCustomer;

    @Column(name = "idroletype")
    private Integer idRoleType;

    @Column(name = "idmpm")
    private String idMPM;

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name="idparty", referencedColumnName="idparty")
    private Party party;

    public String getIdCustomer() {
        return this.idCustomer;
    }

    public void setIdCustomer(String id) {
        this.idCustomer = id;
    }

    public Integer getIdRoleType() {
        return idRoleType;
    }

    public Customer idRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
        return this;
    }

    public void setIdRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
    }

    public Party getParty() {
        return party;
    }

    public Customer party(Party party) {
        this.party = party;
        return this;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public String getIdMPM() {
        return idMPM;
    }

    public void setIdMPM(String idMPM) {
        this.idMPM = idMPM;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Customer customer = (Customer) o;
        if (customer.idCustomer == null || this.idCustomer == null) {
            return false;
        }
        return Objects.equals(this.idCustomer, customer.idCustomer);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idCustomer);
    }

    @Override
    public String toString() {
        return "Customer{" +
            "idCustomer=" + this.idCustomer +
            ", idRoleType='" + getIdRoleType() + "'" +
            '}';
    }
}
