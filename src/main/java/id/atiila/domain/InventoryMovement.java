package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.*;

import org.hibernate.annotations.Cache;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.UUID;
import java.util.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity InventoryMovement.
 */

@Entity
@Table(name = "inventory_movement")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "inventorymovement")
public class InventoryMovement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idslip", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idSlip;

    @Column(name = "dtcreate")
    @CreationTimestamp
    private ZonedDateTime dateCreate;

    @Column(name = "slipnum")
    private String slipNumber;

    @Column(name = "reffnum")
    private String refferenceNumber;

    @Column(name = "idivtmvttyp")
    private Integer idInventoryMovementType;

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<InventoryMovementStatus> statuses = new ArrayList<InventoryMovementStatus>();

    public Integer getIdInventoryMovementType() {
        return idInventoryMovementType;
    }

    public void setIdInventoryMovementType(Integer idInventoryMovementType) {
        this.idInventoryMovementType = idInventoryMovementType;
    }

    public String getSlipNumber() {
        return slipNumber;
    }

    public void setSlipNumber(String slipNumber) {
        this.slipNumber = slipNumber;
    }

    public UUID getIdSlip() {
        return this.idSlip;
    }

    public void setIdSlip(UUID id) {
        this.idSlip = id;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public String getRefferenceNumber() {
        return refferenceNumber;
    }

    public void setRefferenceNumber(String refferenceNumber) {
        this.refferenceNumber = refferenceNumber;
    }

    public InventoryMovement dateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
        return this;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public List<InventoryMovementStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<InventoryMovementStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now().plusNanos(1);

        for (InventoryMovementStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now();

        for (InventoryMovementStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        InventoryMovementStatus current = new InventoryMovementStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public Boolean getActiveData() {
        switch (getCurrentStatus()) {
            case BaseConstants.STATUS_DRAFT:
            case BaseConstants.STATUS_ACTIVE:
                return true;
        }
        return false;
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
        if (getDateCreate() == null)setDateCreate(ZonedDateTime.now());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InventoryMovement inventoryMovement = (InventoryMovement) o;
        if (inventoryMovement.idSlip == null || this.idSlip == null) {
            return false;
        }
        return Objects.equals(this.idSlip, inventoryMovement.idSlip);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idSlip);
    }

    @Override
    public String toString() {
        return "InventoryMovement{" +
            "idSlip=" + this.idSlip +
            ", dateCreate='" + getDateCreate() + "'" +
            '}';
    }
}
