package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity VendorType.
 */

@Entity
@Table(name = "vendor_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "vendortype")
public class VendorType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idvndtyp")
    private Integer idVendorType;

    @Column(name = "description")
    private String description;

    public Integer getIdVendorType() {
        return this.idVendorType;
    }

    public void setIdVendorType(Integer id) {
        this.idVendorType = id;
    }

    public String getDescription() {
        return description;
    }

    public VendorType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VendorType vendorType = (VendorType) o;
        if (vendorType.idVendorType == null || this.idVendorType == null) {
            return false;
        }
        return Objects.equals(this.idVendorType, vendorType.idVendorType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idVendorType);
    }

    @Override
    public String toString() {
        return "VendorType{" +
            "idVendorType=" + this.idVendorType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
