package id.atiila.domain;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import id.atiila.base.BaseConstants;

/**
 * BeSmart Team
 * Class definition for Entity Good.
 */

@Entity
@Table(name = "good")
@DiscriminatorValue(BaseConstants.PRODUCT_TYPE_GOOD)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "good")
public class Good extends Product {

    private static final long serialVersionUID = 1L;

    @Column(name = "serialized")
    private Boolean serialized;

    @ManyToOne
    @JoinColumn(name="iduom", referencedColumnName="iduom")
    private Uom uom;

    public Uom getUom() {
        return uom;
    }

    public void setUom(Uom uom) {
        this.uom = uom;
    }

    public Boolean getSerialized() {
        return serialized;
    }

    public void setSerialized(Boolean serialized) {
        this.serialized = serialized;
    }

    @Override
    public void preUpdate() {
        super.preUpdate();
        if (this.serialized == null) serialized = false;
    }

    @Override
    public void prePersist() {
        super.prePersist();
        if (this.serialized == null) serialized = false;
    }

    @Override
    public String toString() {
        return "Good{" +
            "idProduct=" + this.getIdProduct() +
            ", name='" + getName() + "'" +
            ", dateIntroduction='" + getDateIntroduction() + "'" +
            '}';
    }
}
