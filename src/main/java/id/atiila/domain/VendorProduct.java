package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.time.ZoneId;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity VendorProduct.
 */

@Entity
@Table(name = "vendor_product")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "vendorproduct")
public class VendorProduct implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idvenpro", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idVendorProduct;

    @Column(name = "orderratio")
    private Integer orderRatio;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idproduct", referencedColumnName="idproduct")
    private Product product;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idvendor", referencedColumnName="idvendor")
    private Vendor vendor;

    public UUID getIdVendorProduct() {
        return this.idVendorProduct;
    }

    public void setIdVendorProduct(UUID id) {
        this.idVendorProduct = id;
    }

    public Integer getOrderRatio() {
        return orderRatio;
    }

    public VendorProduct orderRatio(Integer orderRatio) {
        this.orderRatio = orderRatio;
        return this;
    }

    public void setOrderRatio(Integer orderRatio) {
        this.orderRatio = orderRatio;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public VendorProduct dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public VendorProduct dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Product getProduct() {
        return product;
    }

    public VendorProduct product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Internal getInternal() {
        return internal;
    }

    public VendorProduct internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public VendorProduct vendor(Vendor vendor) {
        this.vendor = vendor;
        return this;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    @PreUpdate
    @PrePersist
    public void preUpdate() {
        if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
        if (this.dateThru == null) this.dateThru = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        VendorProduct that = (VendorProduct) o;

        return new EqualsBuilder()
            .append(idVendorProduct, that.idVendorProduct)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idVendorProduct)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "VendorProduct{" +
            "idVendorProduct=" + this.idVendorProduct +
            ", orderRatio='" + getOrderRatio() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
