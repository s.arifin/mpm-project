package id.atiila.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import id.atiila.base.BaseConstants;
import id.atiila.base.DomainEntity;

/**
 * BeSmart Team
 * Class definition for Entity WorkOrderBooking.
 */

@Entity
@Table(name = "work_order_booking")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "workorderbooking")
public class WorkOrderBooking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idbooking", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idBooking;

    @Column(name = "bookingnumber")
    private String bookingNumber;

    @Column(name = "vehiclenumber")
    private String vehicleNumber;

    @Column(name = "dtcreate")
    private ZonedDateTime dateCreate;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name="idcustomer", referencedColumnName="idcustomer")
    private PersonalCustomer customer;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name="idvehicle", referencedColumnName="idvehicle")
    private Vehicle vehicle;

    @ManyToOne
    @JoinColumn(name="idbooslo", referencedColumnName="idbooslo")
    private BookingSlot slot;

    @ManyToOne
    @JoinColumn(name="idboktyp", referencedColumnName="idboktyp")
    private BookingType bookingType;

    @ManyToOne
    @JoinColumn(name="idevetyp", referencedColumnName="idevetyp")
    private EventType eventType;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    public UUID getIdBooking() {
        return this.idBooking;
    }

    public void setIdBooking(UUID id) {
        this.idBooking = id;
    }

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<WorkOrderBookingStatus> statuses = new ArrayList<WorkOrderBookingStatus>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<WorkOrderBookingRole> roles = new ArrayList<WorkOrderBookingRole>();

    public String getBookingNumber() {
        return bookingNumber;
    }

    public WorkOrderBooking bookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
        return this;
    }

    public void setBookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public WorkOrderBooking vehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
        return this;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public WorkOrderBooking dateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
        return this;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public PersonalCustomer getCustomer() {
        return customer;
    }

    public WorkOrderBooking customer(PersonalCustomer personalCustomer) {
        this.customer = personalCustomer;
        return this;
    }

    public void setCustomer(PersonalCustomer personalCustomer) {
        this.customer = personalCustomer;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public WorkOrderBooking vehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
        return this;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public BookingSlot getSlot() {
        return slot;
    }

    public WorkOrderBooking slot(BookingSlot bookingSlot) {
        this.slot = bookingSlot;
        return this;
    }

    public void setSlot(BookingSlot bookingSlot) {
        this.slot = bookingSlot;
    }

    public BookingType getBookingType() {
        return bookingType;
    }

    public WorkOrderBooking bookingType(BookingType bookingType) {
        this.bookingType = bookingType;
        return this;
    }

    public void setBookingType(BookingType bookingType) {
        this.bookingType = bookingType;
    }

    public EventType getEventType() {
        return eventType;
    }

    public WorkOrderBooking eventType(EventType eventType) {
        this.eventType = eventType;
        return this;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public Internal getInternal() {
        return internal;
    }

    public WorkOrderBooking internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public List<WorkOrderBookingRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<WorkOrderBookingRole> roles) {
        this.roles = roles;
    }

    public void setPartyRole(Party party, Integer role)  {
        for (WorkOrderBookingRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) return;
        }

        WorkOrderBookingRole current = new WorkOrderBookingRole();
        current.setOwner(this);
        current.setIdRoleType(role);
		current.setParty(party);
        current.setDateFrom(LocalDateTime.now());
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        roles.add(current);
    }

    public Party getPartyRole(Party party, Integer role)  {
        for (WorkOrderBookingRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) return current.getParty();
        }
        return null;
    }

    public void removePartyRole(Party party, Integer role)  {
        for (WorkOrderBookingRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) {
				current.setDateThru(LocalDateTime.now());
				return;
			}
        }
    }

    public List<WorkOrderBookingStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<WorkOrderBookingStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now().plusNanos(1);
        
        for (WorkOrderBookingStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now();

        for (WorkOrderBookingStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        WorkOrderBookingStatus current = new WorkOrderBookingStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public Boolean getActiveData() {
        switch (getCurrentStatus()) {
            case BaseConstants.STATUS_DRAFT:
            case BaseConstants.STATUS_ACTIVE:
                return true;
        }
        return false;
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
        if (this.dateCreate == null) this.dateCreate = ZonedDateTime.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WorkOrderBooking workOrderBooking = (WorkOrderBooking) o;
        if (workOrderBooking.idBooking == null || this.idBooking == null) {
            return false;
        }
        return Objects.equals(this.idBooking, workOrderBooking.idBooking);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idBooking);
    }

    @Override
    public String toString() {
        return "WorkOrderBooking{" +
            "idBooking=" + this.idBooking +
            ", bookingNumber='" + getBookingNumber() + "'" +
            ", vehicleNumber='" + getVehicleNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            '}';
    }
}
