package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity ShipmentItem.
 */

@Entity
@Table(name = "shipment_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "shipmentitem")
public class ShipmentItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idshiite", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idShipmentItem;

    @Column(name = "idproduct")
    private String idProduct;

    @Column(name = "idfeature")
    private Integer idFeature;

    @Column(name = "itemdescription")
    private String itemDescription;

    @Column(name = "qty")
    private Double qty;

    @Column(name = "contentdescription")
    private String contentDescription;

    @Column(name = "idframe")
    private String idFrame;

    @Column(name = "idmachine")
    private String idMachine;

    @ManyToOne
    @JoinColumn(name="idshipment", referencedColumnName="idshipment")
    private Shipment shipment;

    @ManyToOne
    @JoinColumn(name="idfeature", referencedColumnName="idfeature", insertable = false, updatable = false)
    private Feature feature;

    @OneToMany(mappedBy = "shipmentItem", cascade = CascadeType.ALL)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ShipmentBillingItem> billingItems = new HashSet<>();

    public UUID getIdShipmentItem() {
        return this.idShipmentItem;
    }

    public void setIdShipmentItem(UUID id) {
        this.idShipmentItem = id;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public ShipmentItem idProduct(String idProduct) {
        this.idProduct = idProduct;
        return this;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdFeature() {
        return idFeature;
    }

    public ShipmentItem idFeature(Integer idFeature) {
        this.idFeature = idFeature;
        return this;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public ShipmentItem itemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
        return this;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Double getQty() {
        return qty;
    }

    public ShipmentItem qty(Double qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public String getContentDescription() {
        return contentDescription;
    }

    public ShipmentItem contentDescription(String contentDescription) {
        this.contentDescription = contentDescription;
        return this;
    }

    public void setContentDescription(String contentDescription) {
        this.contentDescription = contentDescription;
    }

    public String getIdFrame() { return idFrame; }

    public void setIdFrame(String idFrame) { this.idFrame = idFrame; }

    public String getIdMachine() { return idMachine; }

    public void setIdMachine(String idMachine) { this.idMachine = idMachine; }

    public Shipment getShipment() {
        return shipment;
    }

    public ShipmentItem shipment(Shipment shipment) {
        this.shipment = shipment;
        return this;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }

    public Feature getFeature() { return feature; }

    public void setFeature(Feature feature) { this.feature = feature; }

    public Set<ShipmentBillingItem> getBillingItems() {
        return billingItems;
    }

    public ShipmentItem billingItems(Set<ShipmentBillingItem> shipmentBillingItems) {
        this.billingItems = shipmentBillingItems;
        return this;
    }

    public ShipmentItem addBillingItem(ShipmentBillingItem shipmentBillingItem) {
        this.billingItems.add(shipmentBillingItem);
        shipmentBillingItem.setShipmentItem(this);
        return this;
    }

    public ShipmentItem addBillingItem(BillingItem bi) {
        ShipmentBillingItem sbi = new ShipmentBillingItem();
        sbi.setShipmentItem(this);
        sbi.setBillingItem(bi);
        sbi.setQty(this.getQty());
        this.billingItems.add(sbi);
        return this;
    }

    public ShipmentItem removeBillingItem(ShipmentBillingItem shipmentBillingItem) {
        this.billingItems.remove(shipmentBillingItem);
        shipmentBillingItem.setShipmentItem(null);
        return this;
    }

    public void setBillingItems(Set<ShipmentBillingItem> shipmentBillingItems) {
        this.billingItems = shipmentBillingItems;
    }

    public void assignFrom(ShipmentReceipt sr) {
        this.setIdProduct(sr.getIdProduct());
        this.setItemDescription(sr.getItemDescription());
        this.setQty(sr.getQtyAccept() - sr.getQtyReject());
        this.setIdFeature(sr.getIdFeature());
        this.contentDescription(sr.getItemDescription());
    }

    public void assignFrom(OrderItem oi, Double qty) {
        this.setIdProduct(oi.getIdProduct());
        this.setItemDescription(oi.getItemDescription());
        this.setQty(qty);
        this.setIdFeature(oi.getIdFeature());
        this.contentDescription(oi.getItemDescription());
        this.setIdFeature(oi.getIdFeature());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShipmentItem shipmentItem = (ShipmentItem) o;
        if (shipmentItem.idShipmentItem == null || this.idShipmentItem == null) {
            return false;
        }
        return Objects.equals(this.idShipmentItem, shipmentItem.idShipmentItem);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idShipmentItem);
    }

    @Override
    public String toString() {
        return "ShipmentItem{" +
            "idShipmentItem=" + this.idShipmentItem +
            ", idProduct='" + getIdProduct() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", itemDescription='" + getItemDescription() + "'" +
            ", qty='" + getQty() + "'" +
            ", contentDescription='" + getContentDescription() + "'" +
            '}';
    }
}
