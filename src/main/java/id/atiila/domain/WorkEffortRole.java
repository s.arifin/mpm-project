package id.atiila.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import id.atiila.base.DomainEntity;

/**
 * BeSmart Team
 * Class definition for Entity WorkEffort Role.
 */

@Entity
@Table(name = "work_effort_role")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class WorkEffortRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idrole", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idRole;

    @Column(name = "idroletype")
    private Integer idRoleType;

    @ManyToOne
    @JoinColumn(name="idwe", referencedColumnName = "idwe")
    private WorkEffort owner;

    @ManyToOne
    @JoinColumn(name="idparty", referencedColumnName = "idparty")
    private Party party;

    @Column(name="username")
    private String userName;

    @Column(name="dtfrom")
    private LocalDateTime dateFrom;

    @Column(name="dtthru")
    private LocalDateTime dateThru;

    public UUID getIdRole() {
		return this.idRole;
	}

	public void setIdRole(UUID idRole) {
		this.idRole = idRole;
	}

	public Integer getIdRoleType() {
		return this.idRoleType;
	}

	public void setIdRoleType(Integer idRoleType) {
		this.idRoleType = idRoleType;
	}

	public WorkEffort getOwner() {
		return this.owner;
	}

	public void setOwner(WorkEffort owner) {
		this.owner = owner;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	public Party getParty() {
		return this.party;
	}

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public LocalDateTime getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(LocalDateTime dateFrom) {
		this.dateFrom = dateFrom;
	}

	public LocalDateTime getDateThru() {
		return dateThru;
	}

	public void setDateThru(LocalDateTime dateThru) {
		this.dateThru = dateThru;
	}

	@PreUpdate
	@PrePersist
	public void preUpdate() {
		if (this.dateFrom == null) this.dateFrom = LocalDateTime.now();
		if (this.dateThru == null) this.dateThru = LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		WorkEffortRole that = (WorkEffortRole) o;

		return new EqualsBuilder()
				.append(this.idRole, that.getIdRole())
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(this.idRole)
				.toHashCode();
	}

    @Override
    public String toString() {
        return "WorkEffortRole {" +
            "idStatus=" + getIdRole() +
            ", idStatusType=" + getIdRoleType() +
            ", owner=" + getOwner() +
            ", dateFrom=" + getDateFrom() +
            ", dateThru=" + getDateThru() +
            '}';
    }
}
