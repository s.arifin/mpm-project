package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity RuleHotItem.
 */
@Entity
@Table(name = "rule_hot_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "rulehotitem")
public class RuleHotItem extends Rules {

    private static final long serialVersionUID = 1L;

    @Column(name = "idproduct")
    private String idProduct;

    @Column(name = "mindownpayment", precision=10, scale=2)
    private BigDecimal minDownPayment;

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public BigDecimal getMinDownPayment() {
        return minDownPayment;
    }

    public void setMinDownPayment(BigDecimal minDownPayment) {
        this.minDownPayment = minDownPayment;
    }
}
