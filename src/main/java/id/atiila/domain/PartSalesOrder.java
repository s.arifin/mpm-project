package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity PartSalesOrder.
 */

@Entity
@Table(name = "part_sales_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "partsalesorder")
public class PartSalesOrder extends SalesOrder {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "PartSalesOrder{" +
            "idOrder=" + this.getIdOrder() +
            '}';
    }
}
