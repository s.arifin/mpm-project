package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity ShipmentBillingItem.
 */

@Entity
@Table(name = "shipment_billing_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "shipmentbillingitem")
public class ShipmentBillingItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idshibilite", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idShipmentBillingItem;

    @Column(name = "qty")
    private Double qty;

    @ManyToOne
    @JoinColumn(name="idshiite", referencedColumnName="idshiite")
    private ShipmentItem shipmentItem;

    @ManyToOne
    @JoinColumn(name="idbilite", referencedColumnName="idbilite")
    private BillingItem billingItem;

    public UUID getIdShipmentBillingItem() {
        return this.idShipmentBillingItem;
    }

    public void setIdShipmentBillingItem(UUID id) {
        this.idShipmentBillingItem = id;
    }

    public Double getQty() {
        return qty;
    }

    public ShipmentBillingItem qty(Double qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public ShipmentItem getShipmentItem() {
        return shipmentItem;
    }

    public ShipmentBillingItem shipmentItem(ShipmentItem shipmentItem) {
        this.shipmentItem = shipmentItem;
        return this;
    }

    public void setShipmentItem(ShipmentItem shipmentItem) {
        this.shipmentItem = shipmentItem;
    }

    public BillingItem getBillingItem() {
        return billingItem;
    }

    public ShipmentBillingItem billingItem(BillingItem billingItem) {
        this.billingItem = billingItem;
        return this;
    }

    public void setBillingItem(BillingItem billingItem) {
        this.billingItem = billingItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShipmentBillingItem shipmentBillingItem = (ShipmentBillingItem) o;
        if (shipmentBillingItem.idShipmentBillingItem == null || this.idShipmentBillingItem == null) {
            return false;
        }
        return Objects.equals(this.idShipmentBillingItem, shipmentBillingItem.idShipmentBillingItem);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idShipmentBillingItem);
    }

    @Override
    public String toString() {
        return "ShipmentBillingItem{" +
            "idShipmentBillingItem=" + this.idShipmentBillingItem +
            ", qty='" + getQty() + "'" +
            '}';
    }
}
