package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.BaseConstants;
import id.atiila.base.DomainEntity;
import id.atiila.domain.listener.OrderItemListener;
import org.hibernate.annotations.*;

import org.hibernate.annotations.Cache;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;


/**
 * BeSmart Team
 * Class definition for Entity OrderItem.
 */

@Entity
@Table(name = "order_item")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "orderitem")
@EntityListeners(OrderItemListener.class)
public class OrderItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idordite", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idOrderItem;

    @Column(name = "idproduct")
    private String idProduct;

    @Column(name = "itemdescription")
    private String itemDescription;

    @Column(name = "idfeature")
    private Integer idFeature;

    @Column(name = "idshipto")
    private String idShipTo;

    @Column(name = "qty")
    private Double qty;

    @Column(name="idmachine", nullable = true, length = 20)
    private String idmachine;

    @Column(name="idframe", nullable = true, length = 20)
    private String idframe;

    @Column(name = "unitprice")
    private BigDecimal unitPrice;

    @Column(name = "discount")
    private BigDecimal discount;

    @Column(name = "discountmd")
    private BigDecimal discountMD;

    @Column(name = "discountfin")
    private BigDecimal discountFinCoy;

    @Column(name = "discountatpm")
    private BigDecimal discountatpm;

    @Column(name = "bbn")
    private BigDecimal bbn;

    @Column(name = "taxamount")
    private BigDecimal taxAmount;

    @Column(name = "totalamount")
    private BigDecimal totalAmount;

    @Formula("(select sum(coalesce(items.qty, 0)) from order_shipment_item items where items.idordite = idordite)")
    private Double qtyFilled;

    @Formula("(select sum(coalesce(items.qtyaccept,0) - coalesce(items.qtyreject,0)) from shipment_receipt items where items.idordite = idordite)")
    private Double qtyReceipt;

    @ManyToOne
    @JoinColumn(name="idorder", referencedColumnName="idorder")
    private Orders orders;

    @OneToMany(mappedBy = "orderItem", cascade = CascadeType.ALL)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<OrderShipmentItem> shipmentItems = new HashSet<>();

    @OneToMany(mappedBy = "orderItem", cascade = CascadeType.ALL)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<OrderBillingItem> billingItems = new HashSet<>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy = "owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<OrdersStatus> statuses = new ArrayList<OrdersStatus>();

    public UUID getIdOrderItem() {
        return this.idOrderItem;
    }

    public void setIdOrderItem(UUID id) {
        this.idOrderItem = id;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public OrderItem idProduct(String idProduct) {
        this.idProduct = idProduct;
        return this;
    }

    public BigDecimal getDiscountatpm() {
        return discountatpm;
    }

    public void setDiscountatpm(BigDecimal discountatpm) {
        this.discountatpm = discountatpm;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public OrderItem itemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
        return this;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Integer getIdFeature() {
        return idFeature;
    }

    public OrderItem idFeature(Integer idFeature) {
        this.idFeature = idFeature;
        return this;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public String getIdShipTo() {
        return idShipTo;
    }

    public OrderItem idShipTo(String idShipTo) {
        this.idShipTo = idShipTo;
        return this;
    }

    public void setIdShipTo(String idShipTo) {
        this.idShipTo = idShipTo;
    }

    public Double getQty() {
        return qty == null ? 0d : qty;
    }

    public OrderItem qty(Double qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public void setQty(Integer qty) {
        this.qty = Double.valueOf(qty);
    }

    public BigDecimal getUnitPrice() {
        return unitPrice == null ? BigDecimal.ZERO : unitPrice;
    }

    public OrderItem unitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public void setUnitPrice(Integer unitPrice) {
        this.unitPrice = BigDecimal.valueOf(unitPrice);
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = BigDecimal.valueOf(unitPrice);
    }

    public BigDecimal getDiscount() {
        return discount == null ? BigDecimal.ZERO : discount;
    }

    public OrderItem discount(BigDecimal discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = BigDecimal.valueOf(discount);
    }

    public BigDecimal getTaxAmount() {
        return taxAmount == null ? BigDecimal.ZERO : taxAmount;
    }

    public OrderItem taxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
        return this;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount == null ? BigDecimal.ZERO : totalAmount;
    }

    public OrderItem totalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
        return this;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = BigDecimal.valueOf(totalAmount);
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = BigDecimal.valueOf(totalAmount);
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = BigDecimal.valueOf(taxAmount);
    }

    public BigDecimal getDiscountMD() {
        return discountMD;
    }

    public void setDiscountMD(BigDecimal discountMD) {
        this.discountMD = discountMD;
    }

    public void setDiscountMD(Integer discountMD) {
        this.discountMD = BigDecimal.valueOf(discountMD);
    }

    public BigDecimal getDiscountFinCoy() {
        return discountFinCoy;
    }

    public void setDiscountFinCoy(BigDecimal discountFinCoy) {
        this.discountFinCoy = discountFinCoy;
    }

    public void setDiscountFinCoy(Integer discountFinCoy) {
        this.discountFinCoy = BigDecimal.valueOf(discountFinCoy);
    }

    public BigDecimal getBbn() {
        return bbn;
    }

    public void setBbn(BigDecimal bbn) {
        this.bbn = bbn;
    }

    public Orders getOrders() {
        return orders;
    }

    public OrderItem orders(Orders orders) {
        this.orders = orders;
        return this;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public Set<OrderShipmentItem> getShipmentItems() {
        return shipmentItems;
    }

    public OrderItem shipmentItems(Set<OrderShipmentItem> orderShipmentItems) {
        this.shipmentItems = orderShipmentItems;
        return this;
    }

    public OrderItem addShipmentItems(OrderShipmentItem orderShipmentItem) {
        this.shipmentItems.add(orderShipmentItem);
        orderShipmentItem.setOrderItem(this);
        return this;
    }

    public OrderItem addShipmentItem(ShipmentItem shipmentItem) {
        for (OrderShipmentItem si: this.getShipmentItems()) {
            if (si.getShipmentItem().equals(shipmentItem)) return this;
        }
        OrderShipmentItem osh = new OrderShipmentItem();
        osh.setOrderItem(this);
        osh.setShipmentItem(shipmentItem);
        osh.setQty(shipmentItem.getQty());
        this.shipmentItems.add(osh);
        return this;
    }

    public OrderItem removeShipmentItems(OrderShipmentItem orderShipmentItem) {
        this.shipmentItems.remove(orderShipmentItem);
        orderShipmentItem.setOrderItem(null);
        return this;
    }

    public void setShipmentItems(Set<OrderShipmentItem> orderShipmentItems) {
        this.shipmentItems = orderShipmentItems;
    }

    public Set<OrderBillingItem> getBillingItems() {
        return billingItems;
    }

    public OrderItem billingItems(Set<OrderBillingItem> orderBillingItems) {
        this.billingItems = orderBillingItems;
        return this;
    }

    public OrderItem addBillingItem(OrderBillingItem orderBillingItem) {
        this.billingItems.add(orderBillingItem);
        orderBillingItem.setOrderItem(this);
        return this;
    }

    public OrderItem addBillingItem(BillingItem billingItem) {
        OrderBillingItem b = new OrderBillingItem();
        b.setOrderItem(this);
        b.setBillingItem(billingItem);
        b.setQty(billingItem.getQty().doubleValue());
        b.setAmount(b.getQty() * (this.getUnitPrice().doubleValue() - this.getDiscount().doubleValue() + this.getTaxAmount().doubleValue()));
        this.billingItems.add(b);
        return this;
    }

    public OrderItem removeBillingItem(OrderBillingItem orderBillingItem) {
        this.billingItems.remove(orderBillingItem);
        orderBillingItem.setOrderItem(null);
        return this;
    }

    public void setBillingItems(Set<OrderBillingItem> orderBillingItems) {
        this.billingItems = orderBillingItems;
    }

    public void setQtyReceipt(Double qtyReceipt) {
        this.qtyReceipt = qtyReceipt;
    }

    public Double getQtyFilled() {
        return qtyFilled == null ? 0d: qtyFilled;
    }

    public void setQtyFilled(Double qtyFilled) {
        this.qtyFilled = qtyFilled;
    }

    public Double getQtyReceipt() {
        return qtyReceipt == null ? 0d : qtyReceipt;
    }

    public void assignFrom(CustomerRequestItem cri) {
        setIdProduct(cri.getProduct().getIdProduct());
        setItemDescription(cri.getDescription());
        if (cri.getFeature() != null) setIdFeature(cri.getFeature().getIdFeature());
        setQty(1d);
        setUnitPrice(cri.getUnitPrice());
        setBbn(cri.getBbn());
        setDiscountFinCoy(0);
        setDiscountMD(0);
        setDiscount(0);
        setTaxAmount(cri.getUnitPrice().doubleValue() * 10/100);
        setUnitPrice(cri.getUnitPrice().doubleValue() - getTaxAmount().doubleValue());
        setTotalAmount(cri.getUnitPrice().doubleValue() - getDiscount().doubleValue() - getDiscountMD().doubleValue());
    }

    public List<OrdersStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OrdersStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);

        for (OrdersStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now();

        for (OrdersStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        OrdersStatus current = new OrdersStatus();
        current.setOrderItem(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 999, ZoneId.systemDefault()));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    @PrePersist
    public void prePersist() {
        if (this.getCurrentStatus() == BaseConstants.STATUS_NOT_DEFINED) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderItem orderItem = (OrderItem) o;
        if (orderItem.idOrderItem == null || this.idOrderItem == null) {
            return false;
        }
        return Objects.equals(this.idOrderItem, orderItem.idOrderItem);
    }

    public String getIdmachine() {
        return idmachine;
    }

    public void setIdmachine(String idmachine) {
        this.idmachine = idmachine;
    }

    public String getIdframe() {
        return idframe;
    }

    public void setIdframe(String idframe) {
        this.idframe = idframe;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idOrderItem);
    }

    @Override
    public String toString() {
        return "OrderItem{" +
            "idOrderItem=" + this.idOrderItem +
            ", idProduct='" + getIdProduct() + "'" +
            ", itemDescription='" + getItemDescription() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", idShipTo='" + getIdShipTo() + "'" +
            ", qty='" + getQty() + "'" +
            ", unitPrice='" + getUnitPrice() + "'" +
            ", discount='" + getDiscount() + "'" +
            ", taxAmount='" + getTaxAmount() + "'" +
            ", totalAmount='" + getTotalAmount() + "'" +
            '}';
    }
}
