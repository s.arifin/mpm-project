package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;


/**
 * atiila consulting
 * Class definition for Entity GLAccount.
 */

@Entity
@Table(name = "gl_account")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "glaccount")
public class GLAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idglacc", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idGLAccount;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name="idglacctyp", referencedColumnName="idglacctyp")
    private GLAccountType accountType;

    public UUID getIdGLAccount() {
        return this.idGLAccount;
    }

    public void setIdGLAccount(UUID id) {
        this.idGLAccount = id;
    }

    public String getName() {
        return name;
    }

    public GLAccount name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public GLAccount description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GLAccountType getAccountType() {
        return accountType;
    }

    public GLAccount accountType(GLAccountType gLAccountType) {
        this.accountType = gLAccountType;
        return this;
    }

    public void setAccountType(GLAccountType gLAccountType) {
        this.accountType = gLAccountType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GLAccount gLAccount = (GLAccount) o;
        if (gLAccount.idGLAccount == null || this.idGLAccount == null) {
            return false;
        }
        return Objects.equals(this.idGLAccount, gLAccount.idGLAccount);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idGLAccount);
    }

    @Override
    public String toString() {
        return "GLAccount{" +
            "idGLAccount=" + this.idGLAccount +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
