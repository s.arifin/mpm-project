package id.atiila.domain;

import id.atiila.base.BaseConstants;
import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity BillingItem.
 */

@Entity
@Table(name = "billing_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "billingitem")
public class BillingItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idbilite", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idBillingItem;

    @Column(name = "idbilitetyp")
    private Integer idItemType;

    @Column(name = "idfeature")
    private Integer idFeature;

    @Column(name = "idproduct")
    private String idProduct;

    @Column(name = "seq")
    private Integer sequence;

    @Column(name = "itemdescription")
    private String itemDescription;

    @Column(name = "qty")
    private Double qty;

    @Column(name = "unitprice", precision=10, scale=2)
    private BigDecimal unitPrice;

    @Column(name = "baseprice", precision=10, scale=2)
    private BigDecimal basePrice;

    @Column(name = "discount", precision=10, scale=2)
    private BigDecimal discount;

    @Column(name = "taxamount", precision=10, scale=2)
    private BigDecimal taxAmount;

    @Column(name = "totalamount", precision=10, scale=2)
    private BigDecimal totalAmount;

    @ManyToOne
    @JoinColumn(name="idfeature", referencedColumnName="idfeature", insertable = false, updatable = false)
    private Feature feature;

    @ManyToOne
    @JoinColumn(name="idbilling", referencedColumnName="idbilling")
    private Billing billing;

    @ManyToOne
    @JoinColumn(name="idinvite", referencedColumnName="idinvite")
    private InventoryItem inventoryItem;

    public UUID getIdBillingItem() {
        return this.idBillingItem;
    }

    public void setIdBillingItem(UUID id) {
        this.idBillingItem = id;
    }

    public Integer getIdItemType() {
        return idItemType;
    }

    public BillingItem idItemType(Integer idItemType) {
        this.idItemType = idItemType;
        return this;
    }

    public void setIdItemType(Integer idItemType) {
        this.idItemType = idItemType;
    }

    public Integer getIdFeature() {
        return idFeature;
    }

    public BillingItem idFeature(Integer idFeature) {
        this.idFeature = idFeature;
        return this;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public BillingItem idProduct(String idProduct) {
        this.idProduct = idProduct;
        return this;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public BillingItem itemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
        return this;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Double getQty() {
        return qty;
    }

    public BillingItem qty(Double qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public void setQty(Integer qty) {
        this.qty = Double.valueOf(qty);
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public BillingItem sequence(Integer sequence) {
        this.sequence = sequence;
        return this;
    }

    public BillingItem unitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = BigDecimal.valueOf(unitPrice);
    }

    public void setUnitPrice(Integer unitPrice) {
        this.unitPrice = BigDecimal.valueOf(unitPrice);
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public BillingItem basePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
        return this;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public void setBasePrice(Integer basePrice) {
        this.basePrice = BigDecimal.valueOf(basePrice);
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public BillingItem discount(BigDecimal discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = BigDecimal.valueOf(discount);
    }


    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public BillingItem taxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
        return this;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public void setTaxAmount(Integer taxAmount) {
        this.taxAmount = BigDecimal.valueOf(taxAmount);
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public BillingItem totalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
        return this;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = BigDecimal.valueOf(totalAmount);
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = BigDecimal.valueOf(totalAmount);
    }

    public Billing getBilling() {
        return billing;
    }

    public BillingItem billing(Billing billing) {
        this.billing = billing;
        return this;
    }

    public void setBilling(Billing billing) {
        this.billing = billing;
    }

    public InventoryItem getInventoryItem() {
        return inventoryItem;
    }

    public BillingItem inventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
        return this;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    public BillingItem addOrderItem(OrderItem oi, Double qty) {
        this.setIdItemType(BaseConstants.ITEM_TYPE_PRODUCT);
        this.setItemDescription(oi.getItemDescription());
        this.setIdProduct(oi.getIdProduct());
        this.setUnitPrice(oi.getUnitPrice());
        this.setDiscount(oi.getDiscount());
        this.setQty(qty);
        this.setTaxAmount(oi.getTaxAmount());
        double totalAmount = this.qty * (this.getUnitPrice().doubleValue() - this.getDiscount().doubleValue() + this.getTaxAmount().doubleValue());
        this.setTotalAmount(totalAmount);
        return this;
    }

    public void setInventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BillingItem billingItem = (BillingItem) o;
        if (billingItem.idBillingItem == null || this.idBillingItem == null) {
            return false;
        }
        return Objects.equals(this.idBillingItem, billingItem.idBillingItem);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idBillingItem);
    }

    @Override
    public String toString() {
        return "BillingItem{" +
            "idBillingItem=" + this.idBillingItem +
            ", idItemType='" + getIdItemType() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", idProduct='" + getIdProduct() + "'" +
            ", itemDescription='" + getItemDescription() + "'" +
            ", qty='" + getQty() + "'" +
            ", unitPrice='" + getUnitPrice() + "'" +
            ", basePrice='" + getBasePrice() + "'" +
            ", discount='" + getDiscount() + "'" +
            ", taxAmount='" + getTaxAmount() + "'" +
            ", totalAmount='" + getTotalAmount() + "'" +
            '}';
    }
}
