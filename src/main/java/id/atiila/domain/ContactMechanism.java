package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;


/**
 * BeSmart Team
 * Class definition for Entity ContactMechanism.
 */

@Entity
@Table(name = "contact_mechanism")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "idcontacttype")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "contactmechanism")
public class ContactMechanism implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idcontact", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idContact;

    @Column(name = "idcontacttype", updatable = false, insertable = false)
    private Integer idContactType;

    public UUID getIdContact() {
        return this.idContact;
    }

    public void setIdContact(UUID id) {
        this.idContact = id;
    }

    public Integer getIdContactType() {
        return idContactType;
    }

    public ContactMechanism idContactType(Integer idContactType) {
        this.idContactType = idContactType;
        return this;
    }

    public void setIdContactType(Integer idContactType) {
        this.idContactType = idContactType;
    }

    public String getDescription() {
        return "";
    }

    public void setDescription(String description) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ContactMechanism that = (ContactMechanism) o;

        return new EqualsBuilder()
            .append(idContact, that.idContact)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idContact);
    }

    @Override
    public String toString() {
        return "ContactMechanism{" +
            "idContact=" + this.idContact +
            ", idContactType='" + getIdContactType() + "'" +
            '}';
    }
}
