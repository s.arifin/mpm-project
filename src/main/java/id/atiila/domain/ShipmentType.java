package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity ShipmentType.
 */

@Entity
@Table(name = "shipment_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "shipmenttype")
public class ShipmentType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idshityp")
    private Integer idShipmentType;

    @Column(name = "description")
    private String description;

    public Integer getIdShipmentType() {
        return this.idShipmentType;
    }

    public void setIdShipmentType(Integer id) {
        this.idShipmentType = id;
    }

    public String getDescription() {
        return description;
    }

    public ShipmentType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShipmentType shipmentType = (ShipmentType) o;
        if (shipmentType.idShipmentType == null || this.idShipmentType == null) {
            return false;
        }
        return Objects.equals(this.idShipmentType, shipmentType.idShipmentType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idShipmentType);
    }

    @Override
    public String toString() {
        return "ShipmentType{" +
            "idShipmentType=" + this.idShipmentType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
