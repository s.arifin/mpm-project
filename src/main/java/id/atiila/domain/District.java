package id.atiila.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity District.
 */

@Entity
@Table(name = "district")
@DiscriminatorValue(BaseConstants.TYPE_DISTRICT)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "district")
public class District extends GeoBoundary {

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        District e = (District) o;
        if (e.getIdGeobou() == null || this.getIdGeobou() == null) {
            return false;
        }
        return Objects.equals(this.getIdGeobou(), e.getIdGeobou());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdGeobou());
    }

    @Override
    public String toString() {
        return "District{" +
            "idGeobou=" + this.getIdGeobou() +
            ", geocode='" + getGeoCode() + "'" +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
