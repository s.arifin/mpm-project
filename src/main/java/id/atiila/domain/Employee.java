package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * atiila consulting
 * Class definition for Entity Employee.
 */

@Entity
@Table(name = "employee")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "employee")
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idemployee")
    private String idEmployee;

    @Column(name = "idroletype")
    private Integer idRoleType;

    @ManyToOne
    @JoinColumn(name="idparty", referencedColumnName="idparty")
    private Person person;

    public String getIdEmployee() {
        return this.idEmployee;
    }

    public void setIdEmployee(String id) {
        this.idEmployee = id;
    }

    public Integer getIdRoleType() {
        return idRoleType;
    }

    public Employee idRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
        return this;
    }

    public void setIdRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
    }

    public Person getPerson() {
        return person;
    }

    public Employee person(Person person) {
        this.person = person;
        return this;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Employee employee = (Employee) o;
        if (employee.idEmployee == null || this.idEmployee == null) {
            return false;
        }
        return Objects.equals(this.idEmployee, employee.idEmployee);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idEmployee);
    }

    @Override
    public String toString() {
        return "Employee{" +
            "idEmployee=" + this.idEmployee +
            ", idRoleType='" + getIdRoleType() + "'" +
            '}';
    }
}
