package id.atiila.domain;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * BeSmart Team
 * Class definition for Entity ProspectOrganization.
 */

@Entity
@Table(name = "organization_prospect")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "prospectorganization")
public class ProspectOrganization extends Prospect {

    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="idparty", referencedColumnName="idparty")
    private Organization organization;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="idpic", referencedColumnName="idparty")
    private Person pic;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="idposaddtdp", referencedColumnName="idcontact")
    private PostalAddress addressTDP;

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="prospectOrganization")
    private List<ProspectOrganizationDetails> details = new ArrayList<>();

    public Organization getOrganization() {
        return organization;
    }

    public ProspectOrganization organization(Organization organization) {
        this.organization = organization;
        return this;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Person getPic() {
        return pic;
    }

    public void setPic(Person pic) {
        this.pic = pic;
    }

    public PostalAddress getAddressTDP() {
        return addressTDP;
    }

    public void setAddressTDP(PostalAddress addressTDP) {
        this.addressTDP = addressTDP;
    }

    public List<ProspectOrganizationDetails> getDetails() {return details; }

    public void setDetails(List<ProspectOrganizationDetails> details) { this.details = details; }

    @Override
    public String toString() {
        return "ProspectOrganization{" +
            "idProspect=" + this.getIdProspect() +
            '}';
    }
}
