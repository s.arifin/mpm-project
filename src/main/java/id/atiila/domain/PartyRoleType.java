package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "party_role_type")
public class PartyRoleType {

    @EmbeddedId
    private PartyRoleTypeId id;

    @JoinColumn(name = "idroletype")
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("idRoleType")
    private RoleType roleType;

    @JoinColumn(name = "idparty")
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("idParty")
    private Party party;

    public PartyRoleType() {
    }

//    public PartyRoleType(Integer role, UUID idparty) {
//        this.id = new PartyRoleTypeId(role, idparty);
//    }

    public PartyRoleType(Party party, RoleType roleType) {
        this.id = new PartyRoleTypeId(roleType.getIdRoleType(), party.getIdParty());
        this.party = party;
        this.roleType = roleType;
    }

    public PartyRoleType(PartyRoleTypeId p) {
        this.id = p;
    }

    public PartyRoleTypeId getId() {
        return id;
    }

    public void setId(PartyRoleTypeId id) {
        this.id = id;
    }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PartyRoleType that = (PartyRoleType) o;

        return new EqualsBuilder()
            .append(id, that.id)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(id)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "PartyRoleType{" +
            "id=" + id +
            '}';
    }
}
