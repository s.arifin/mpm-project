package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity ContainerType.
 */

@Entity
@Table(name = "container_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "containertype")
public class ContainerType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcontyp")
    private Integer idContainerType;

    @Column(name = "description")
    private String description;

    public Integer getIdContainerType() {
        return this.idContainerType;
    }

    public void setIdContainerType(Integer id) {
        this.idContainerType = id;
    }

    public String getDescription() {
        return description;
    }

    public ContainerType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ContainerType containerType = (ContainerType) o;
        if (containerType.idContainerType == null || this.idContainerType == null) {
            return false;
        }
        return Objects.equals(this.idContainerType, containerType.idContainerType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idContainerType);
    }

    @Override
    public String toString() {
        return "ContainerType{" +
            "idContainerType=" + this.idContainerType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
