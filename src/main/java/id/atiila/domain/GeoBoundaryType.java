package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity GeoBoundaryType.
 */

@Entity
@Table(name = "geo_boundary_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "geoboundarytype")
public class GeoBoundaryType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idgeoboutype")
    private Integer idGeobouType;

    @Column(name = "description")
    private String description;

    public Integer getIdGeobouType() {
        return this.idGeobouType;
    }

    public void setIdGeobouType(Integer id) {
        this.idGeobouType = id;
    }

    public String getDescription() {
        return description;
    }

    public GeoBoundaryType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GeoBoundaryType geoBoundaryType = (GeoBoundaryType) o;
        if (geoBoundaryType.idGeobouType == null || this.idGeobouType == null) {
            return false;
        }
        return Objects.equals(this.idGeobouType, geoBoundaryType.idGeobouType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idGeobouType);
    }

    @Override
    public String toString() {
        return "GeoBoundaryType{" +
            "idGeobouType=" + this.idGeobouType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
