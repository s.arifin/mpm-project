package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity Position.
 */

@Entity
@Table(name = "positions")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "position")
public class Position implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idposition", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idPosition;

    @Column(name = "seqnum")
    private Integer sequenceNumber;

    @Column(name = "description")
    private String description;

    @Column(name = "username")
    private String userName;

    @ManyToOne
    @JoinColumn(name="idpostyp", referencedColumnName="idpostyp")
    private PositionType positionType;

    @ManyToOne
    @JoinColumn(name="idorganization", referencedColumnName="idparty")
    private Organization organization;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idusrmed", referencedColumnName="idusrmed")
    private UserMediator owner;

    public UUID getIdPosition() {
        return this.idPosition;
    }

    public void setIdPosition(UUID id) {
        this.idPosition = id;
    }

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<PositionStatus> statuses = new ArrayList<PositionStatus>();

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public Position sequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
        return this;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getDescription() {
        return description;
    }

    public Position description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserName() {
        return userName;
    }

    public Position userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public PositionType getPositionType() {
        return positionType;
    }

    public Position positionType(PositionType positionType) {
        this.positionType = positionType;
        return this;
    }

    public void setPositionType(PositionType positionType) {
        this.positionType = positionType;
    }

    public Organization getOrganization() {
        return organization;
    }

    public Position organization(Organization organization) {
        this.organization = organization;
        return this;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Internal getInternal() {
        return internal;
    }

    public Position internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public UserMediator getOwner() {
        return owner;
    }

    public Position owner(UserMediator userMediator) {
        this.owner = userMediator;
        return this;
    }

    public void setOwner(UserMediator userMediator) {
        this.owner = userMediator;
    }

    public List<PositionStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<PositionStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);
        
        for (PositionStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now();

        for (PositionStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        PositionStatus current = new PositionStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public Boolean getActiveData() {
        switch (getCurrentStatus()) {
            case BaseConstants.STATUS_DRAFT:
            case BaseConstants.STATUS_ACTIVE:
                return true;
        }
        return false;
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Position position = (Position) o;
        if (position.idPosition == null || this.idPosition == null) {
            return false;
        }
        return Objects.equals(this.idPosition, position.idPosition);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPosition);
    }

    @Override
    public String toString() {
        return "Position{" +
            "idPosition=" + this.idPosition +
            ", sequenceNumber='" + getSequenceNumber() + "'" +
            ", description='" + getDescription() + "'" +
            ", userName='" + getUserName() + "'" +
            '}';
    }
}
