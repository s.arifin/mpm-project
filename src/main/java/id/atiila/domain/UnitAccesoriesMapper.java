package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity UnitAccesoriesMapper.
 */

@Entity
@Table(name = "unit_accesories_mapper")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "unitaccesoriesmapper")
public class UnitAccesoriesMapper implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "iduntaccmap", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idUnitAccMapper;

    @Column(name = "acc1")
    private String acc1;

    @Column(name = "qtyacc1")
    private Integer qtyAcc1;

    @Column(name = "acc2")
    private String acc2;

    @Column(name = "qtyacc2")
    private Integer qtyAcc2;

    @Column(name = "acc3")
    private String acc3;

    @Column(name = "qtyacc3")
    private Integer qtyAcc3;

    @Column(name = "acc4")
    private String acc4;

    @Column(name = "qtyacc4")
    private Integer qtyAcc4;

    @Column(name = "acc5")
    private String acc5;

    @Column(name = "qtyacc5")
    private Integer qtyAcc5;

    @Column(name = "acc6")
    private String acc6;

    @Column(name = "qtyacc6")
    private Integer qtyAcc6;

    @Column(name = "acc7")
    private String acc7;

    @Column(name = "qtyacc7")
    private Integer qtyAcc7;

    @Column(name = "promat1")
    private String promat1;

    @Column(name = "qtypromat1")
    private Integer qtyPromat1;

    @Column(name = "promat2")
    private String promat2;

    @Column(name = "qtypromat2")
    private Integer qtyPromat2;

    @ManyToOne
    @JoinColumn(name="idmotor", referencedColumnName="idproduct")
    private Motor motor;

    public UUID getIdUnitAccMapper() {
        return this.idUnitAccMapper;
    }

    public void setIdUnitAccMapper(UUID id) {
        this.idUnitAccMapper = id;
    }

    public String getAcc1() {
        return acc1;
    }

    public UnitAccesoriesMapper acc1(String acc1) {
        this.acc1 = acc1;
        return this;
    }

    public void setAcc1(String acc1) {
        this.acc1 = acc1;
    }

    public Integer getQtyAcc1() {
        return qtyAcc1;
    }

    public UnitAccesoriesMapper qtyAcc1(Integer qtyAcc1) {
        this.qtyAcc1 = qtyAcc1;
        return this;
    }

    public void setQtyAcc1(Integer qtyAcc1) {
        this.qtyAcc1 = qtyAcc1;
    }

    public String getAcc2() {
        return acc2;
    }

    public UnitAccesoriesMapper acc2(String acc2) {
        this.acc2 = acc2;
        return this;
    }

    public void setAcc2(String acc2) {
        this.acc2 = acc2;
    }

    public Integer getQtyAcc2() {
        return qtyAcc2;
    }

    public UnitAccesoriesMapper qtyAcc2(Integer qtyAcc2) {
        this.qtyAcc2 = qtyAcc2;
        return this;
    }

    public void setQtyAcc2(Integer qtyAcc2) {
        this.qtyAcc2 = qtyAcc2;
    }

    public String getAcc3() {
        return acc3;
    }

    public UnitAccesoriesMapper acc3(String acc3) {
        this.acc3 = acc3;
        return this;
    }

    public void setAcc3(String acc3) {
        this.acc3 = acc3;
    }

    public Integer getQtyAcc3() {
        return qtyAcc3;
    }

    public UnitAccesoriesMapper qtyAcc3(Integer qtyAcc3) {
        this.qtyAcc3 = qtyAcc3;
        return this;
    }

    public void setQtyAcc3(Integer qtyAcc3) {
        this.qtyAcc3 = qtyAcc3;
    }

    public String getAcc4() {
        return acc4;
    }

    public UnitAccesoriesMapper acc4(String acc4) {
        this.acc4 = acc4;
        return this;
    }

    public void setAcc4(String acc4) {
        this.acc4 = acc4;
    }

    public Integer getQtyAcc4() {
        return qtyAcc4;
    }

    public UnitAccesoriesMapper qtyAcc4(Integer qtyAcc4) {
        this.qtyAcc4 = qtyAcc4;
        return this;
    }

    public void setQtyAcc4(Integer qtyAcc4) {
        this.qtyAcc4 = qtyAcc4;
    }

    public String getAcc5() {
        return acc5;
    }

    public UnitAccesoriesMapper acc5(String acc5) {
        this.acc5 = acc5;
        return this;
    }

    public void setAcc5(String acc5) {
        this.acc5 = acc5;
    }

    public Integer getQtyAcc5() {
        return qtyAcc5;
    }

    public UnitAccesoriesMapper qtyAcc5(Integer qtyAcc5) {
        this.qtyAcc5 = qtyAcc5;
        return this;
    }

    public void setQtyAcc5(Integer qtyAcc5) {
        this.qtyAcc5 = qtyAcc5;
    }

    public String getAcc6() {
        return acc6;
    }

    public UnitAccesoriesMapper acc6(String acc6) {
        this.acc6 = acc6;
        return this;
    }

    public void setAcc6(String acc6) {
        this.acc6 = acc6;
    }

    public Integer getQtyAcc6() {
        return qtyAcc6;
    }

    public UnitAccesoriesMapper qtyAcc6(Integer qtyAcc6) {
        this.qtyAcc6 = qtyAcc6;
        return this;
    }

    public void setQtyAcc6(Integer qtyAcc6) {
        this.qtyAcc6 = qtyAcc6;
    }

    public String getAcc7() {
        return acc7;
    }

    public UnitAccesoriesMapper acc7(String acc7) {
        this.acc7 = acc7;
        return this;
    }

    public void setAcc7(String acc7) {
        this.acc7 = acc7;
    }

    public Integer getQtyAcc7() {
        return qtyAcc7;
    }

    public UnitAccesoriesMapper qtyAcc7(Integer qtyAcc7) {
        this.qtyAcc7 = qtyAcc7;
        return this;
    }

    public void setQtyAcc7(Integer qtyAcc7) {
        this.qtyAcc7 = qtyAcc7;
    }

    public String getPromat1() {
        return promat1;
    }

    public UnitAccesoriesMapper promat1(String promat1) {
        this.promat1 = promat1;
        return this;
    }

    public void setPromat1(String promat1) {
        this.promat1 = promat1;
    }

    public Integer getQtyPromat1() {
        return qtyPromat1;
    }

    public UnitAccesoriesMapper qtyPromat1(Integer qtyPromat1) {
        this.qtyPromat1 = qtyPromat1;
        return this;
    }

    public void setQtyPromat1(Integer qtyPromat1) {
        this.qtyPromat1 = qtyPromat1;
    }

    public String getPromat2() {
        return promat2;
    }

    public UnitAccesoriesMapper promat2(String promat2) {
        this.promat2 = promat2;
        return this;
    }

    public void setPromat2(String promat2) {
        this.promat2 = promat2;
    }

    public Integer getQtyPromat2() {
        return qtyPromat2;
    }

    public UnitAccesoriesMapper qtyPromat2(Integer qtyPromat2) {
        this.qtyPromat2 = qtyPromat2;
        return this;
    }

    public void setQtyPromat2(Integer qtyPromat2) {
        this.qtyPromat2 = qtyPromat2;
    }

    public Motor getMotor() {
        return motor;
    }

    public UnitAccesoriesMapper motor(Motor motor) {
        this.motor = motor;
        return this;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UnitAccesoriesMapper unitAccesoriesMapper = (UnitAccesoriesMapper) o;
        if (unitAccesoriesMapper.idUnitAccMapper == null || this.idUnitAccMapper == null) {
            return false;
        }
        return Objects.equals(this.idUnitAccMapper, unitAccesoriesMapper.idUnitAccMapper);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idUnitAccMapper);
    }

    @Override
    public String toString() {
        return "UnitAccesoriesMapper{" +
            "idUnitAccMapper=" + this.idUnitAccMapper +
            ", acc1='" + getAcc1() + "'" +
            ", qtyAcc1='" + getQtyAcc1() + "'" +
            ", acc2='" + getAcc2() + "'" +
            ", qtyAcc2='" + getQtyAcc2() + "'" +
            ", acc3='" + getAcc3() + "'" +
            ", qtyAcc3='" + getQtyAcc3() + "'" +
            ", acc4='" + getAcc4() + "'" +
            ", qtyAcc4='" + getQtyAcc4() + "'" +
            ", acc5='" + getAcc5() + "'" +
            ", qtyAcc5='" + getQtyAcc5() + "'" +
            ", acc6='" + getAcc6() + "'" +
            ", qtyAcc6='" + getQtyAcc6() + "'" +
            ", acc7='" + getAcc7() + "'" +
            ", qtyAcc7='" + getQtyAcc7() + "'" +
            ", promat1='" + getPromat1() + "'" +
            ", qtyPromat1='" + getQtyPromat1() + "'" +
            ", promat2='" + getPromat2() + "'" +
            ", qtyPromat2='" + getQtyPromat2() + "'" +
            '}';
    }
}
