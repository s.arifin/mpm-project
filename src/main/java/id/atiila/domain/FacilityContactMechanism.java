package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity FacilityContactMechanism.
 */

@Entity
@Table(name = "facility_contact_mechanism")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "facilitycontactmechanism")
public class FacilityContactMechanism implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idconmecpur", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idContactMechPurpose;

    @Column(name = "idpurposetype")
    private Integer idPurposeType;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idfacility", referencedColumnName="idfacility")
    private Facility facility;

    @ManyToOne
    @JoinColumn(name="idcontact", referencedColumnName="idcontact")
    private ContactMechanism contact;

    public UUID getIdContactMechPurpose() {
        return this.idContactMechPurpose;
    }

    public void setIdContactMechPurpose(UUID id) {
        this.idContactMechPurpose = id;
    }

    public Integer getIdPurposeType() {
        return idPurposeType;
    }

    public FacilityContactMechanism idPurposeType(Integer idPurposeType) {
        this.idPurposeType = idPurposeType;
        return this;
    }

    public void setIdPurposeType(Integer idPurposeType) {
        this.idPurposeType = idPurposeType;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public FacilityContactMechanism dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public FacilityContactMechanism dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Facility getFacility() {
        return facility;
    }

    public FacilityContactMechanism facility(Facility facility) {
        this.facility = facility;
        return this;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    public ContactMechanism getContact() {
        return contact;
    }

    public FacilityContactMechanism contact(ContactMechanism contactMechanism) {
        this.contact = contactMechanism;
        return this;
    }

    public void setContact(ContactMechanism contactMechanism) {
        this.contact = contactMechanism;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FacilityContactMechanism facilityContactMechanism = (FacilityContactMechanism) o;
        if (facilityContactMechanism.idContactMechPurpose == null || this.idContactMechPurpose == null) {
            return false;
        }
        return Objects.equals(this.idContactMechPurpose, facilityContactMechanism.idContactMechPurpose);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idContactMechPurpose);
    }

    @Override
    public String toString() {
        return "FacilityContactMechanism{" +
            "idContactMechPurpose=" + this.idContactMechPurpose +
            ", idPurposeType='" + getIdPurposeType() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
