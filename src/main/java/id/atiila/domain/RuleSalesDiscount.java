package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity RuleSalesDiscount.
 */

@Entity
@Table(name = "rule_sales_discount")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "rulesalesdiscount")
public class RuleSalesDiscount extends Rules {

    private static final long serialVersionUID = 1L;

    @Column(name = "name")
    private String role;

    @Column(name = "maxamount", precision=10, scale=2)
    private BigDecimal maxAmount;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }
}
