package id.atiila.domain;

import javax.persistence.*;

import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * BeSmart Team
 * Class definition for Entity InternalOrder.
 */

@Entity
@Table(name = "internal_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "internalorder")
public class InternalOrder extends Orders {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name="idintfro", referencedColumnName="idinternal")
    private Internal internalFrom;

    @ManyToOne
    @JoinColumn(name="idintto", referencedColumnName="idinternal")
    private Internal internalTo;

    public Internal getInternalFrom() {
        return internalFrom;
    }

    public InternalOrder internalFrom(Internal internal) {
        this.internalFrom = internal;
        return this;
    }

    public void setInternalFrom(Internal internal) {
        this.internalFrom = internal;
    }

    public Internal getInternalTo() {
        return internalTo;
    }

    public InternalOrder internalTo(Internal internal) {
        this.internalTo = internal;
        return this;
    }

    public void setInternalTo(Internal internal) {
        this.internalTo = internal;
    }

    @Override
    public String toString() {
        return "InternalOrder{" +
            "idOrder=" + this.getIdOrder() +
            '}';
    }
}
