package id.atiila.domain.listener;

import id.atiila.base.BeanUtil;
import id.atiila.domain.Motor;
import id.atiila.service.ActivitiProcessor;

import javax.persistence.PostPersist;
import javax.persistence.PreUpdate;

public class MotorListener {

    public static String PROCESS_NAME = "motor-process";

    @PostPersist
    public void postPersist(Motor e) {
        ActivitiProcessor processor = BeanUtil.getBean(ActivitiProcessor.class);
    }

    @PreUpdate
    public void preUpdate(Motor e) {
    }

}
