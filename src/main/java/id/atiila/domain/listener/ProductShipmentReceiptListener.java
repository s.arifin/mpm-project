package id.atiila.domain.listener;

import id.atiila.base.BeanUtil;
import id.atiila.domain.ProductShipmentReceipt;
import id.atiila.service.ActivitiProcessor;

import javax.persistence.PostPersist;
import javax.persistence.PreUpdate;

public class ProductShipmentReceiptListener {

    public static String PROCESS_NAME = "product-shipment-receipt-process";

    @PostPersist
    public void postPersist(ProductShipmentReceipt e) {
        ActivitiProcessor processor = BeanUtil.getBean(ActivitiProcessor.class);
    }

    @PreUpdate
    public void preUpdate(ProductShipmentReceipt e) {
    }

}
