package id.atiila.domain.listener;

import id.atiila.base.BeanUtil;
import id.atiila.domain.ShipmentReceipt;
import id.atiila.service.ActivitiProcessor;

import javax.persistence.PostPersist;
import javax.persistence.PreUpdate;

public class ShipmentReceiptListener {

    public static String PROCESS_NAME = "shipment-receipt-process";

    @PostPersist
    public void postPersist(ShipmentReceipt e) {
        ActivitiProcessor processor = BeanUtil.getBean(ActivitiProcessor.class);
    }

    @PreUpdate
    public void preUpdate(ShipmentReceipt e) {
    }

}
