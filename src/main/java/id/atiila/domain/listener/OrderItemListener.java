package id.atiila.domain.listener;

import id.atiila.base.BeanUtil;
import id.atiila.domain.OrderItem;
import id.atiila.domain.Product;
import id.atiila.repository.ProductRepository;
import id.atiila.service.ActivitiProcessor;

import javax.persistence.PostPersist;
import javax.persistence.PreUpdate;
import java.math.BigDecimal;

public class OrderItemListener {

    public static String PROCESS_NAME = "order-item-process";
    public static String VAR_NAME = "order-item";

    @PostPersist
    public void postPersist(OrderItem e) {
        ActivitiProcessor processor = BeanUtil.getBean(ActivitiProcessor.class);
//        processor.startProcess(PROCESS_NAME, e.getOrderNumber(), processor.getVariables(VAR_NAME, e));
    }

    @PreUpdate
    public void preUpdate(OrderItem e) {
        if (e.getUnitPrice() == null) e.setUnitPrice(0);
        if (e.getTaxAmount() == null) e.setTaxAmount(BigDecimal.ZERO);
        if (e.getQty() == null) e.setQty(0);
        if (e.getDiscount() == null) e.setDiscount(0);
        if (e.getTotalAmount() == null) e.setTotalAmount(0);

        // Set Item Description
        if (e.getIdProduct() != null && e.getItemDescription() == null) {
            ProductRepository productRepository = BeanUtil.getBean(ProductRepository.class);
            Product p = productRepository.findOne(e.getIdProduct());
            if (p != null) e.setItemDescription(p.getName());
        }
    }

}
