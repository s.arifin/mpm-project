package id.atiila.domain.listener;

import id.atiila.base.BeanUtil;
import id.atiila.domain.PickingSlip;
import id.atiila.service.ActivitiProcessor;

import javax.persistence.PostPersist;
import javax.persistence.PreUpdate;

public class PickingListener {

    public static String PROCESS_NAME = "picking-process";

    @PostPersist
    public void postPersist(PickingSlip e) {
        ActivitiProcessor processor = BeanUtil.getBean(ActivitiProcessor.class);
    }

    @PreUpdate
    public void preUpdate(PickingSlip e) {
    }

}
