package id.atiila.domain.listener;

import id.atiila.base.BeanUtil;
import id.atiila.domain.RemPart;
import id.atiila.service.ActivitiProcessor;

import javax.persistence.PostPersist;
import javax.persistence.PreUpdate;

public class RemPartListener {

    public static String PROCESS_NAME = "motor-process";

    @PostPersist
    public void postPersist(RemPart e) {
        ActivitiProcessor processor = BeanUtil.getBean(ActivitiProcessor.class);
    }

    @PreUpdate
    public void preUpdate(RemPart e) {
    }

}
