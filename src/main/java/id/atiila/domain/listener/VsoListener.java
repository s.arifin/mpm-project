package id.atiila.domain.listener;

import id.atiila.base.BeanUtil;
import id.atiila.domain.VehicleSalesOrder;
import id.atiila.service.ActivitiProcessor;

import javax.persistence.PostPersist;
import javax.persistence.PreUpdate;

public class VsoListener {

    public static String PROCESS_NAME = "vso-process";
    public static String VAR_NAME = "vso";

    @PostPersist
    public void postPersist(VehicleSalesOrder e) {
        ActivitiProcessor processor = BeanUtil.getBean(ActivitiProcessor.class);
//        processor.startProcess(PROCESS_NAME, e.getOrderNumber(), processor.getVariables(VAR_NAME, e));
    }

    @PreUpdate
    public void preUpdate(VehicleSalesOrder e) {
        ActivitiProcessor processor = BeanUtil.getBean(ActivitiProcessor.class);
    }

}
