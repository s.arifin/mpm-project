package id.atiila.domain.listener;

import id.atiila.base.BeanUtil;
import id.atiila.domain.ProspectPerson;
import id.atiila.service.ActivitiProcessor;

import javax.persistence.PostPersist;
import javax.persistence.PreUpdate;

public class ProspectPersonListener {

    public static String PROCESS_NAME = "prospect-person-process";

    @PostPersist
    public void postPersist(ProspectPerson e) {
        ActivitiProcessor processor = BeanUtil.getBean(ActivitiProcessor.class);
//        processor.startProcess(PROCESS_NAME, e.getOrderNumber(), processor.getVariables(VAR_NAME, e));
    }

    @PreUpdate
    public void preUpdate(ProspectPerson e) {
    }

}
