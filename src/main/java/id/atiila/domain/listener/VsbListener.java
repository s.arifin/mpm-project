package id.atiila.domain.listener;

import id.atiila.base.BeanUtil;
import id.atiila.domain.VehicleSalesBilling;
import id.atiila.service.ActivitiProcessor;

import javax.persistence.PostPersist;
import java.util.Map;

public class VsbListener {

    public static String PROCESS_NAME = "vsb-process";

    @PostPersist
    public void postPersist(VehicleSalesBilling e) {
        ActivitiProcessor processor = BeanUtil.getBean(ActivitiProcessor.class);
        Map<String, Object> vars = processor.getVariables("vsb", e);
//        processor.startProcess(PROCESS_NAME, e.getOrderNumber(), vars);
    }

}
