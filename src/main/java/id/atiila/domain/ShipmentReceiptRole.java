package id.atiila.domain;

import java.io.Serializable;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;
import javax.persistence.*;
import id.atiila.base.BaseConstants;
import id.atiila.base.DomainEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

/**
 * BeSmart Team
 * Class definition for Entity ShipmentReceipt Role.
 */

@Entity
@Table(name = "shipment_receipt_role")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ShipmentReceiptRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idrole", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idRole;

    @Column(name = "idroletype")
    private Integer idRoleType;

	@Column(name = "username")
    private String user;

    @ManyToOne
    @JoinColumn(name="idreceipt", referencedColumnName = "idreceipt")
    private ShipmentReceipt owner;

    @ManyToOne
    @JoinColumn(name="idparty", referencedColumnName = "idparty")
    private Party party;

    @Column(name="dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name="dtthru")
    private ZonedDateTime dateThru;

    public UUID getIdRole() {
		return this.idRole;
	}

	public void setIdRole(UUID idRole) {
		this.idRole = idRole;
	}

	public Integer getIdRoleType() {
		return this.idRoleType;
	}

	public void setIdRoleType(Integer idRoleType) {
		this.idRoleType = idRoleType;
	}

	public ShipmentReceipt getOwner() {
		return this.owner;
	}

	public void setOwner(ShipmentReceipt owner) {
		this.owner = owner;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	public Party getParty() {
		return this.party;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public ZonedDateTime getDateFrom() {
		return this.dateFrom;
	}

	public void setDateFrom(ZonedDateTime dateFrom) {
		this.dateFrom = dateFrom;
	}

	public ZonedDateTime getDateThru() {
		return this.dateThru;
	}

	public void setDateThru(ZonedDateTime dateThru) {
		this.dateThru = dateThru;
	}

	@PreUpdate
	@PrePersist
	public void preUpdate() {
		if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
		if (this.dateThru == null) this.dateThru = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ShipmentReceiptRole that = (ShipmentReceiptRole) o;

		return new EqualsBuilder()
				.append(this.idRole, that.getIdRole())
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(this.idRole)
				.toHashCode();
	}

    @Override
    public String toString() {
        return "ShipmentReceiptRole {" +
            "idStatus=" + getIdRole() +
            ", idStatusType=" + getIdRoleType() +
            ", owner=" + getOwner() +
            ", dateFrom=" + getDateFrom() +
            ", dateThru=" + getDateThru() +
            '}';
    }
}
