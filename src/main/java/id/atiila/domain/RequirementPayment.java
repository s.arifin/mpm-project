package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;import java.util.UUID;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity RequirementPayment.
 */

@Entity
@Table(name = "order_payment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "requirementpayment")
public class RequirementPayment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idreqpayite", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idReqPayment;

    @Column(name = "amount", precision=10, scale=2)
    private BigDecimal amount;

    @ManyToOne
    @JoinColumn(name="idreq", referencedColumnName="idreq")
    private Requirement requirement;

    @ManyToOne
    @JoinColumn(name="idpayment", referencedColumnName="idpayment")
    private Payment payment;

    public UUID getIdReqPayment() {
        return this.idReqPayment;
    }

    public void setIdReqPayment(UUID id) {
        this.idReqPayment = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public RequirementPayment amount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Requirement getRequirement() {
        return requirement;
    }

    public RequirementPayment requirement(Requirement requirement) {
        this.requirement = requirement;
        return this;
    }

    public void setRequirement(Requirement requirement) {
        this.requirement = requirement;
    }

    public Payment getPayment() {
        return payment;
    }

    public RequirementPayment payment(Payment payment) {
        this.payment = payment;
        return this;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RequirementPayment requirementPayment = (RequirementPayment) o;
        if (requirementPayment.idReqPayment == null || this.idReqPayment == null) {
            return false;
        }
        return Objects.equals(this.idReqPayment, requirementPayment.idReqPayment);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idReqPayment);
    }

    @Override
    public String toString() {
        return "RequirementPayment{" +
            "idReqPayment=" + this.idReqPayment +
            ", amount='" + getAmount() + "'" +
            '}';
    }
}
