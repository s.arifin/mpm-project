package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.time.ZoneId;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity LeasingTenorProvide.
 */

@Entity
@Table(name = "leasing_tenor_provide")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "leasingtenorprovide")
public class LeasingTenorProvide implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idlsgpro", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idLeasingProvide;

    @ManyToOne
    @JoinColumn(name = "idproduct", referencedColumnName = "idproduct")
    private Product product;

    @Column(name = "tenor")
    private Integer tenor;

    @Column(name = "installment", precision=10, scale=2)
    private BigDecimal installment;

    @Column(name = "downpayment", precision=10, scale=2)
    private BigDecimal downPayment;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idleacom", referencedColumnName="idparrol")
    private LeasingCompany leasing;

    public UUID getIdLeasingProvide() {
        return this.idLeasingProvide;
    }

    public void setIdLeasingProvide(UUID id) {
        this.idLeasingProvide = id;
    }

    public Product getProduct() {
        return product;
    }

    public LeasingTenorProvide product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getTenor() {
        return tenor;
    }

    public LeasingTenorProvide tenor(Integer tenor) {
        this.tenor = tenor;
        return this;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

    public BigDecimal getInstallment() {
        return installment;
    }

    public LeasingTenorProvide installment(BigDecimal installment) {
        this.installment = installment;
        return this;
    }

    public void setInstallment(BigDecimal installment) {
        this.installment = installment;
    }

    public BigDecimal getDownPayment() {
        return downPayment;
    }

    public LeasingTenorProvide downPayment(BigDecimal downPayment) {
        this.downPayment = downPayment;
        return this;
    }

    public void setDownPayment(BigDecimal downPayment) {
        this.downPayment = downPayment;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public LeasingTenorProvide dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public LeasingTenorProvide dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public LeasingCompany getLeasing() {
        return leasing;
    }

    public LeasingTenorProvide leasing(LeasingCompany leasingCompany) {
        this.leasing = leasingCompany;
        return this;
    }

    public void setLeasing(LeasingCompany leasingCompany) {
        this.leasing = leasingCompany;
    }

    @PreUpdate
    @PrePersist
    public void preUpdate() {
        if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
        if (this.dateThru == null) this.dateThru = ZonedDateTime.of(9999,12,31,23,59,59, 999, ZoneId.systemDefault());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        LeasingTenorProvide that = (LeasingTenorProvide) o;

        return new EqualsBuilder()
            .append(idLeasingProvide, that.idLeasingProvide)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idLeasingProvide)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "LeasingTenorProvide{" +
            "idLeasingProvide=" + this.idLeasingProvide +
            ", product='" + getProduct() + "'" +
            ", tenor='" + getTenor() + "'" +
            ", installment='" + getInstallment() + "'" +
            ", downPayment='" + getDownPayment() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
