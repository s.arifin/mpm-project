package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity WorkEffortType.
 */

@Entity
@Table(name = "we_type")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "workefforttype")
public class WorkEffortType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idwetyp")
    private Integer idWeType;

    @Column(name = "description")
    private String description;

    public Integer getIdWeType() {
        return this.idWeType;
    }

    public void setIdWeType(Integer id) {
        this.idWeType = id;
    }

    public String getDescription() {
        return description;
    }

    public WorkEffortType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WorkEffortType workEffortType = (WorkEffortType) o;
        if (workEffortType.idWeType == null || this.idWeType == null) {
            return false;
        }
        return Objects.equals(this.idWeType, workEffortType.idWeType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idWeType);
    }

    @Override
    public String toString() {
        return "WorkEffortType{" +
            "idWeType=" + this.idWeType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
