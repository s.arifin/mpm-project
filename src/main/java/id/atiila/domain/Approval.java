package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.hibernate.annotations.Where;
import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity Approval.
 */

@Entity
@Table(name = "approval")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "approval")
public class Approval implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idapproval", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idApproval;

    @Column(name = "idapptyp")
    private Integer idApprovalType;

    @Column(name = "idstatustype")
    private Integer idStatusType;

    @Column(name = "idmessage")
    private Integer idMessage;

    @Column(name = "dtfrom")
    private LocalDateTime dateFrom;

    @Column(name = "idreq")
    private UUID idRequirement;

    @Column(name = "dtthru")
    private LocalDateTime dateThru;

    @Column(name = "idleacom")
    private UUID idLeasingCompany;

    //getter and setter
    public UUID getIdRequirement() {
        return idRequirement;
    }

    public void setIdRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
    }

    public Integer getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(Integer idMessage) {
        this.idMessage = idMessage;
    }

    public UUID getIdApproval() {
        return this.idApproval;
    }

    public void setIdApproval(UUID id) {
        this.idApproval = id;
    }

    public Integer getIdApprovalType() {
        return idApprovalType;
    }

    public Approval idApprovalType(Integer idApprovalType) {
        this.idApprovalType = idApprovalType;
        return this;
    }

    public void setIdApprovalType(Integer idApprovalType) {
        this.idApprovalType = idApprovalType;
    }

    public Integer getIdStatusType() {
        return idStatusType;
    }

    public Approval idStatusType(Integer idStatusType) {
        this.idStatusType = idStatusType;
        return this;
    }

    public void setIdStatusType(Integer idStatusType) {
        this.idStatusType = idStatusType;
    }

    public LocalDateTime getDateFrom() {
        return dateFrom;
    }

    public Approval dateFrom(LocalDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(LocalDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDateTime getDateThru() {
        return dateThru;
    }

    public Approval dateThru(LocalDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(LocalDateTime dateThru) {
        this.dateThru = dateThru;
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (this.dateFrom == null) this.dateFrom = LocalDateTime.now();
        if (this.dateThru == null) this.dateThru = LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Approval approval = (Approval) o;
        if (approval.idApproval == null || this.idApproval == null) {
            return false;
        }
        return Objects.equals(this.idApproval, approval.idApproval);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idApproval);
    }

    @Override
    public String toString() {
        return "Approval{" +
            "idApproval=" + this.idApproval +
            ", idApprovalType='" + getIdApprovalType() + "'" +
            ", idStatusType='" + getIdStatusType() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
