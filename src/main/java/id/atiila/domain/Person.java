package id.atiila.domain;

import java.time.LocalDate;
import java.time.ZonedDateTime;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.WhereJoinTable;
import org.springframework.data.elasticsearch.annotations.Document;

import id.atiila.base.BaseConstants;

/**
 * BeSmart Team
 * Class definition for Entity Person.
 */

@Entity
@Table(name = "person")
@DiscriminatorValue("1")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
//@Document(indexName = "person")
public class Person extends Party {

    private static final long serialVersionUID = 1L;

    @Column(name = "fname")
    private String firstName;

    @Column(name = "lname")
    private String lastName;

    @Column(name = "pob")
    private String pob;

    @Column(name = "bloodtype")
    private String bloodType;

    @Column(name = "gender")
    private String gender;

    @Column(name = "dtob")
    private ZonedDateTime dob;

    @Column(name = "personalidnumber")
    private String personalIdNumber;

    @Column(name = "familyidnumber")
    private String familyIdNumber;

    @Column(name = "taxidnumber")
    private String taxIdNumber;

    @Column(name = "cellphone1")
    private String cellPhone1;

    @Column(name = "cellphone2")
    private String cellPhone2;

    @Column(name = "phone")
    private String phone;

    @Column(name = "privatemail")
    private String privateMail;

    @ManyToOne
    @JoinColumn(name="idreligiontype", referencedColumnName="idreligiontype")
    private ReligionType religionType;

    @ManyToOne
    @JoinColumn(name="idworktype", referencedColumnName="idworktype")
    private WorkType workType;

    public String getFirstName() {
        return firstName;
    }

    public Person firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Person lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPob() {
        return pob;
    }

    public Person pob(String pob) {
        this.pob = pob;
        return this;
    }

    public void setPob(String pob) {
        this.pob = pob;
    }

    public String getBloodType() {
        return bloodType;
    }

    public Person bloodType(String bloodType) {
        this.bloodType = bloodType;
        return this;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getGender() {
        return gender;
    }

    public Person gender(String gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ZonedDateTime getDob() {
        return dob;
    }

    public Person dob(ZonedDateTime dob) {
        this.dob = dob;
        return this;
    }

    public void setDob(ZonedDateTime dob) {
        this.dob = dob;
    }

    public String getPersonalIdNumber() {
        return personalIdNumber;
    }

    public Person personalIdNumber(String personalIdNumber) {
        this.personalIdNumber = personalIdNumber;
        return this;
    }

    public void setPersonalIdNumber(String personalIdNumber) {
        this.personalIdNumber = personalIdNumber;
    }

    public String getFamilyIdNumber() {
        return familyIdNumber;
    }

    public Person familyIdNumber(String familyIdNumber) {
        this.familyIdNumber = familyIdNumber;
        return this;
    }

    public void setFamilyIdNumber(String familyIdNumber) {
        this.familyIdNumber = familyIdNumber;
    }

    public String getTaxIdNumber() {
        return taxIdNumber;
    }

    public void setTaxIdNumber(String taxIdNumber) {
        this.taxIdNumber = taxIdNumber;
    }

    public ReligionType getReligionType() {
        return religionType;
    }

    public Person religionType(ReligionType religionType) {
        this.religionType = religionType;
        return this;
    }

    public void setReligionType(ReligionType religionType) {
        this.religionType = religionType;
    }

    public WorkType getWorkType() {
        return workType;
    }

    public Person workType(WorkType workType) {
        this.workType = workType;
        return this;
    }

    public void setWorkType(WorkType workType) {
        this.workType = workType;
    }

    public String getCellPhone1() {
        return cellPhone1;
    }

    public void setCellPhone1(String cellPhone1) {
        this.cellPhone1 = cellPhone1;
    }

    public String getCellPhone2() {
        return cellPhone2;
    }

    public void setCellPhone2(String cellPhone2) {
        this.cellPhone2 = cellPhone2;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPrivateMail() {
        return privateMail;
    }

    public void setPrivateMail(String privateMail) {
        this.privateMail = privateMail;
    }

    @Override
    public String getName() {
        StringBuilder sb = new StringBuilder();
        if (this.firstName != null) {
            sb.append(this.firstName);
            if (this.lastName != null) {
                sb.append(" ");
                sb.append(this.lastName);
            }
        } else if (this.lastName != null) {
            sb.append(this.lastName);
        };
        return sb.toString();
    }

    @Override
    public void preUpdate() {
    }

    @Override
    public String toString() {
        return "Person{" +
            "idParty=" + this.getIdParty() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", pob='" + getPob() + "'" +
            ", bloodType='" + getBloodType() + "'" +
            ", gender='" + getGender() + "'" +
            ", dob='" + getDob() + "'" +
            ", personalIdNumber='" + getPersonalIdNumber() + "'" +
            ", familyIdNumber='" + getFamilyIdNumber() + "'" +
            ", taxNumber='" + getTaxIdNumber() + "'" +
            '}';
    }
}
