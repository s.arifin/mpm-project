package id.atiila.domain;

import javax.persistence.*;

import id.atiila.domain.listener.MotorListener;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Formula;
import org.springframework.data.elasticsearch.annotations.Document;

import id.atiila.base.BaseConstants;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity Motor.
 */

@Entity
@Table(name = "motor")
@DiscriminatorValue(BaseConstants.PRODUCT_TYPE_MOTOR)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "motor")
@EntityListeners(MotorListener.class)
public class Motor extends Good {

    private static final long serialVersionUID = 1L;

    @Override
    public void prePersist() {
        if (this.getSerialized() == null) setSerialized(true);
        if (this.getTaxable() == null) setTaxable(true);
        if (this.getPriceType() == null) setPriceType(2);
        super.prePersist();
    }

    @Override
    public void preUpdate() {
        if (this.getSerialized() == null) setSerialized(true);
        if (this.getTaxable() == null) setTaxable(true);
        if (this.getPriceType() == null) setPriceType(2);
        super.preUpdate();
    }

    @Override
    public String toString() {
        return "Motor{" +
            "idProduct=" + this.getIdProduct() +
            ", name='" + getName() + "'" +
            ", dateIntroduction='" + getDateIntroduction() + "'" +
            '}';
    }
}
