package id.atiila.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;

/**
 * BeSmart Team
 * Class definition for Entity BookingType.
 */

@Entity
@Table(name = "booking_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "bookingtype")
public class BookingType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idboktyp")
    private Integer idBookingType;

    @Column(name = "description")
    private String description;

    public Integer getIdBookingType() {
        return this.idBookingType;
    }

    public void setIdBookingType(Integer id) {
        this.idBookingType = id;
    }

    public String getDescription() {
        return description;
    }

    public BookingType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        BookingType that = (BookingType) o;

        return new EqualsBuilder()
            .append(idBookingType, that.idBookingType)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idBookingType)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "BookingType{" +
            "idBookingType=" + this.idBookingType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
