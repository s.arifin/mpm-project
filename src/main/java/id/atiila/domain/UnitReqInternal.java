package id.atiila.domain;

import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.*;

/**
 * BeSmart Team
 * Class definition for Entity UnitReqInternal.
 */

@Entity
@Table(name = "unit_requirement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "unitreqinternal")
public class UnitReqInternal extends Requirement {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name="idbranch", referencedColumnName="idinternal")
    private Branch branch;

    @ManyToOne
    @JoinColumn(name="idproduct", referencedColumnName="idproduct")
    private Motor motor;

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Motor getMotor() {
        return motor;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }

    @Override
    public String toString() {
        return "UnitReqInternal{" +
            "idRequirement=" + this.getIdRequirement() +
            ", description='" + getDescription() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", dateRequired='" + getDateRequired() + "'" +
            ", budget='" + getBudget() + "'" +
            ", qty='" + getQty() + "'" +
            '}';
    }
}
