package id.atiila.domain;

import id.atiila.base.BaseConstants;
import id.atiila.domain.listener.VsbListener;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * BeSmart Team
 * Class definition for Entity VehicleSalesBilling.
 */

@Entity
@Table(name = "vehicle_sales_billing")
@DiscriminatorValue(BaseConstants.BILLING_SALES_VEHICLE)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class VehicleSalesBilling extends BillingReceipt {

    private static final long serialVersionUID = 1L;

    @Column(name="idsaletype")
    private Integer idSaleType;

    @ManyToOne
    @JoinColumn(name="idcustomer", referencedColumnName="idcustomer")
    private Customer customer;

    @Column(name = "axunitprice", precision=10, scale=2)
    private BigDecimal axunitprice;

    @Column(name = "axbbn", precision=10, scale=2)
    private BigDecimal axbbn;

    @Column(name = "axsalesamount", precision=10, scale=2)
    private BigDecimal axsalesamount;

    @Column(name = "axdisc", precision=10, scale=2)
    private BigDecimal axdisc;

    @Column(name = "axprice", precision=10, scale=2)
    private BigDecimal axprice;

    @Column(name = "axppn", precision=10, scale=2)
    private BigDecimal axppn;

    @Column(name = "axaramt", precision=10, scale=2)
    private BigDecimal axaramt;

    @Column(name = "axadd1", precision=10, scale=2)
    private BigDecimal axadd1;

    @Column(name = "axadd2", precision=10, scale=2)
    private BigDecimal axadd2;

    @Column(name = "axadd3", precision=10, scale=2)
    private BigDecimal axadd3;

    @Column(name = "irunumber")
    private String iruNumber;

    @Column(name = "requesttaxinvoice")
    private Integer requestTaxInvoice;

    @Column(name = "taxinvoicenote")
    private String taxInvoiceNote;

    public Integer getRequestTaxInvoice() { return requestTaxInvoice; }

    public void setRequestTaxInvoice(Integer requestTaxInvoice) { this.requestTaxInvoice = requestTaxInvoice; }

    public String getTaxInvoiceNote() {
        return taxInvoiceNote;
    }

    public void setTaxInvoiceNote(String taxInvoiceNote) {
        this.taxInvoiceNote = taxInvoiceNote;
    }

    public Customer getCustomer() {
        return customer;
    }

    public VehicleSalesBilling customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Integer getIdSaleType() {
        return idSaleType;
    }

    public void setIdSaleType(Integer idSaleType) {
        this.idSaleType = idSaleType;
    }

    public BigDecimal getAxunitprice() {
        return axunitprice;
    }

    public void setAxunitprice(BigDecimal axunitprice) {
        this.axunitprice = axunitprice;
    }

    public BigDecimal getAxbbn() {
        return axbbn;
    }

    public void setAxbbn(BigDecimal axbbn) {
        this.axbbn = axbbn;
    }

    public BigDecimal getAxsalesamount() {
        return axsalesamount;
    }

    public void setAxsalesamount(BigDecimal axsalesamount) {
        this.axsalesamount = axsalesamount;
    }

    public BigDecimal getAxdisc() {
        return axdisc;
    }

    public void setAxdisc(BigDecimal axdisc) {
        this.axdisc = axdisc;
    }

    public BigDecimal getAxprice() {
        return axprice;
    }

    public void setAxprice(BigDecimal axprice) {
        this.axprice = axprice;
    }

    public BigDecimal getAxppn() {
        return axppn;
    }

    public void setAxppn(BigDecimal axppn) {
        this.axppn = axppn;
    }

    public BigDecimal getAxaramt() {
        return axaramt;
    }

    public void setAxaramt(BigDecimal axaramt) {
        this.axaramt = axaramt;
    }

    public BigDecimal getAxadd1() {
        return axadd1;
    }

    public void setAxadd1(BigDecimal axadd1) {
        this.axadd1 = axadd1;
    }

    public BigDecimal getAxadd2() {
        return axadd2;
    }

    public void setAxadd2(BigDecimal axadd2) {
        this.axadd2 = axadd2;
    }

    public BigDecimal getAxadd3() {
        return axadd3;
    }

    public void setAxadd3(BigDecimal axadd3) {
        this.axadd3 = axadd3;
    }

    public String getIruNumber() { return iruNumber; }

    public void setIruNumber(String iruNumber) { this.iruNumber = iruNumber; }

    @Override
    public String toString() {
        return "VehicleSalesBilling{" +
            "idBilling=" + this.getIdBilling() +
            '}';
    }


}
