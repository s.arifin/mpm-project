package id.atiila.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import id.atiila.base.DomainEntity;

/**
 * BeSmart Team
 * Class definition for Entity Orders Status.
 */

@Entity
@Table(name = "orders_status")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OrdersStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idstatus", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idStatus;

    @Column(name = "idstatustype")
    private Integer idStatusType;

    @ManyToOne
    @JoinColumn(name="idorder", referencedColumnName = "idorder")
    private Orders owner;

    @ManyToOne
    @JoinColumn(name="idordite", referencedColumnName = "idordite")
    private OrderItem orderItem;

    @Column(name="dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name="dtthru")
    private ZonedDateTime dateThru;

    public UUID getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(UUID idStatus) {
		this.idStatus = idStatus;
	}

	public Integer getIdStatusType() {
		return idStatusType;
	}

	public void setIdStatusType(Integer idStatusType) {
		this.idStatusType = idStatusType;
	}

	public Orders getOwner() {
		return owner;
	}

	public void setOwner(Orders owner) {
		this.owner = owner;
	}

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    public ZonedDateTime getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(ZonedDateTime dateFrom) {
		this.dateFrom = dateFrom;
	}

	public ZonedDateTime getDateThru() {
		return dateThru;
	}

	public void setDateThru(ZonedDateTime dateThru) {
		this.dateThru = dateThru;
	}

	@PreUpdate
	@PrePersist
	public void preUpdate() {
		if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
		if (this.dateThru == null) this.dateThru = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 999, ZoneId.systemDefault());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		OrdersStatus that = (OrdersStatus) o;

		return new EqualsBuilder()
				.append(idStatus, that.idStatus)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idStatus)
				.toHashCode();
	}

    @Override
    public String toString() {
        return "OrdersStatus{" +
            "idStatus=" + getIdStatus() +
            ", idStatusType=" + getIdStatusType() +
            ", owner=" + getOwner() +
            ", dateFrom=" + getDateFrom() +
            ", dateThru=" + getDateThru() +
            '}';
    }
}
