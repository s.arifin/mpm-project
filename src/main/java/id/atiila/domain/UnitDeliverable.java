package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity UnitDeliverable.
 */

@Entity
@Table(name = "unit_deliverable")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "unitdeliverable")
public class UnitDeliverable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "iddeliverable", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idDeliverable;

    @Column(name = "iddeltype")
    private Integer idDeliverableType;

    @Column(name = "description")
    private String description;

    @Column(name = "dtreceipt")
    private ZonedDateTime dateReceipt;

    @Column(name = "dtdelivery")
    private ZonedDateTime dateDelivery;

    @Column(name = "bastnumber")
    private String bastNumber;

    @Column(name = "name")
    private String name;

    @Column(name = "identitynumber")
    private String identityNumber;

    @Column(name = "cellphone")
    private String cellPhone;

    @Column(name = "refnumber")
    private String refNumber;

    @Column(name = "refdt")
    private ZonedDateTime refDate;

    @Column(name = "receiptqty")
    private Integer receiptQty;

    @Column(name = "receiptnominal")
    private BigDecimal receiptNominal;

    @Column(name = "dtbastsales")
    private ZonedDateTime dateBastSales;

    @Column(name = "dtbastsalesback")
    private ZonedDateTime dateBastSalesBack;

    @ManyToOne
    @JoinColumn(name="idreq", referencedColumnName="idreq")
    private VehicleDocumentRequirement vehicleDocumentRequirement;

    @ManyToOne
    @JoinColumn(name="idvndstatyp", referencedColumnName="idstatustype")
    private StatusType vndStatusType;

    @ManyToOne
    @JoinColumn(name="idconstatyp", referencedColumnName="idstatustype")
    private StatusType conStatusType;

    @Column(name = "bpkbnumber")
    private String bpkbNumber;

    public Boolean getDiPrint() {
        return diPrint;
    }

    public void setDiPrint(Boolean diPrint) {
        this.diPrint = diPrint;
    }

    @Column(name = "isprinted")
    private Boolean diPrint;

    @Column(name = "iscompleted")
    private Boolean iscompleted;

    public ZonedDateTime getDateBastSales() {
        return dateBastSales;
    }

    public void setDateBastSales(ZonedDateTime dateBastSales) {
        this.dateBastSales = dateBastSales;
    }

    public ZonedDateTime getDateBastSalesBack() {
        return dateBastSalesBack;
    }

    public void setDateBastSalesBack(ZonedDateTime dateBastSalesBack) {
        this.dateBastSalesBack = dateBastSalesBack;
    }


    public Boolean getIscompleted() {
        return iscompleted;
    }

    public void setIscompleted(Boolean iscompleted) {
        this.iscompleted = iscompleted;
    }

    public String getBpkbNumber() {
        return bpkbNumber;
    }

    public void setBpkbNumber(String bpkbNumber) {
        this.bpkbNumber = bpkbNumber;
    }

    public UUID getIdDeliverable() {
        return this.idDeliverable;
    }

    public void setIdDeliverable(UUID id) {
        this.idDeliverable = id;
    }

    public Integer getIdDeliverableType() {
        return idDeliverableType;
    }

    public UnitDeliverable idDeliverableType(Integer idDeliverableType) {
        this.idDeliverableType = idDeliverableType;
        return this;
    }

    public void setIdDeliverableType(Integer idDeliverableType) {
        this.idDeliverableType = idDeliverableType;
    }

    public String getDescription() {
        return description;
    }

    public UnitDeliverable description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getDateReceipt() {
        return dateReceipt;
    }

    public UnitDeliverable dateReceipt(ZonedDateTime dateReceipt) {
        this.dateReceipt = dateReceipt;
        return this;
    }

    public void setDateReceipt(ZonedDateTime dateReceipt) {
        this.dateReceipt = dateReceipt;
    }

    public ZonedDateTime getDateDelivery() {
        return dateDelivery;
    }

    public UnitDeliverable dateDelivery(ZonedDateTime dateDelivery) {
        this.dateDelivery = dateDelivery;
        return this;
    }

    public void setDateDelivery(ZonedDateTime dateDelivery) {
        this.dateDelivery = dateDelivery;
    }

    public String getBastNumber() {
        return bastNumber;
    }

    public UnitDeliverable bastNumber(String bastNumber) {
        this.bastNumber = bastNumber;
        return this;
    }

    public void setBastNumber(String bastNumber) {
        this.bastNumber = bastNumber;
    }

    public String getName() {
        return name;
    }

    public UnitDeliverable name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public UnitDeliverable identityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
        return this;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public UnitDeliverable cellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
        return this;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public VehicleDocumentRequirement getVehicleDocumentRequirement() {
        return vehicleDocumentRequirement;
    }

    public UnitDeliverable vehicleDocumentRequirement(VehicleDocumentRequirement vehicleDocumentRequirement) {
        this.vehicleDocumentRequirement = vehicleDocumentRequirement;
        return this;
    }

    public void setVehicleDocumentRequirement(VehicleDocumentRequirement vehicleDocumentRequirement) {
        this.vehicleDocumentRequirement = vehicleDocumentRequirement;
    }

    public StatusType getVndStatusType() {
        return vndStatusType;
    }

    public UnitDeliverable vndStatusType(StatusType statusType) {
        this.vndStatusType = statusType;
        return this;
    }

    public void setVndStatusType(StatusType statusType) {
        this.vndStatusType = statusType;
    }

    public StatusType getConStatusType() {
        return conStatusType;
    }

    public UnitDeliverable conStatusType(StatusType statusType) {
        this.conStatusType = statusType;
        return this;
    }

    public void setConStatusType(StatusType statusType) {
        this.conStatusType = statusType;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public ZonedDateTime getRefDate() {
        return refDate;
    }

    public void setRefDate(ZonedDateTime refDate) {
        this.refDate = refDate;
    }

    public Integer getReceiptQty() {
        return receiptQty;
    }

    public void setReceiptQty(Integer receiptQty) {
        this.receiptQty = receiptQty;
    }

    public BigDecimal getReceiptNominal() {
        return receiptNominal;
    }

    public void setReceiptNominal(BigDecimal receiptNominal) {
        this.receiptNominal = receiptNominal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UnitDeliverable unitDeliverable = (UnitDeliverable) o;
        if (unitDeliverable.idDeliverable == null || this.idDeliverable == null) {
            return false;
        }
        return Objects.equals(this.idDeliverable, unitDeliverable.idDeliverable);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idDeliverable);
    }

    @Override
    public String toString() {
        return "UnitDeliverable{" +
            "idDeliverable=" + this.idDeliverable +
            ", idDeliverableType='" + getIdDeliverableType() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateReceipt='" + getDateReceipt() + "'" +
            ", dateDelivery='" + getDateDelivery() + "'" +
            ", bastNumber='" + getBastNumber() + "'" +
            ", name='" + getName() + "'" +
            ", identityNumber='" + getIdentityNumber() + "'" +
            ", cellPhone='" + getCellPhone() + "'" +
            '}';
    }
}
