package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.UUID;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity ShipmentPackage.
 */

@Entity
@Table(name = "shipment_package")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "shipmentpackage")
public class ShipmentPackage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idpackage", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idPackage;

    @Column(name = "idshipactyp")
    private Integer idPackageType;

    @Column(name = "dtcreated")
    private ZonedDateTime dateCreated;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<ShipmentPackageStatus> statuses = new ArrayList<ShipmentPackageStatus>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="shipmentPackage")
    private List<ShipmentReceipt> shipmentReceipts = new ArrayList<>();

    public UUID getIdPackage() {
        return this.idPackage;
    }

    public void setIdPackage(UUID id) {
        this.idPackage = id;
    }

    public Integer getIdPackageType() {
        return idPackageType;
    }

    public ShipmentPackage idPackageType(Integer idPackageType) {
        this.idPackageType = idPackageType;
        return this;
    }

    public void setIdPackageType(Integer idPackageType) {
        this.idPackageType = idPackageType;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public ShipmentPackage dateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Internal getInternal() {
        return internal;
    }

    public ShipmentPackage internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public List<ShipmentPackageStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<ShipmentPackageStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now().plusNanos(1);
        
        for (ShipmentPackageStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now();

        for (ShipmentPackageStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        ShipmentPackageStatus current = new ShipmentPackageStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public Boolean getActiveData() {
        switch (getCurrentStatus()) {
            case BaseConstants.STATUS_DRAFT:
            case BaseConstants.STATUS_ACTIVE:
                return true;
        }
        return false;
    }

    public List<ShipmentReceipt> getShipmentReceipts() {
        return shipmentReceipts;
    }

    public void setShipmentReceipts(List<ShipmentReceipt> shipmentReceipts) {
        this.shipmentReceipts = shipmentReceipts;
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
        if (this.getDateCreated() == null) this.setDateCreated(ZonedDateTime.now());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShipmentPackage shipmentPackage = (ShipmentPackage) o;
        if (shipmentPackage.idPackage == null || this.idPackage == null) {
            return false;
        }
        return Objects.equals(this.idPackage, shipmentPackage.idPackage);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPackage);
    }

    @Override
    public String toString() {
        return "ShipmentPackage{" +
            "idPackage=" + this.idPackage +
            ", idPackageType='" + getIdPackageType() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            '}';
    }
}
