package id.atiila.domain;

import id.atiila.base.DomainEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.WhereJoinTable;
import org.springframework.data.elasticsearch.annotations.Document;
import org.hibernate.annotations.GenericGenerator;

import java.util.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity Party.
 */

@Entity
@Table(name = "party")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name="idtype")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "party")
public class Party implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idparty", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idParty;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "party_category_impl",
               joinColumns = @JoinColumn(name="idparty", referencedColumnName="idparty"),
               inverseJoinColumns = @JoinColumn(name="idcategory", referencedColumnName="idcategory"))
    private Set<PartyCategory> categories = new HashSet<>();

    @Column(name="idtype", insertable = false, updatable = false)
    private Integer idType;

    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinTable(name = "party_contact_mechanism",
        joinColumns = @JoinColumn(name="idparty", referencedColumnName="idparty"),
        inverseJoinColumns = @JoinColumn(name="idcontact", referencedColumnName="idcontact"))
    @WhereJoinTable(clause = "(idpurposetype = 1) and (current_timestamp between dtfrom and dtthru)")
    private PostalAddress postalAddress;

    @Transient
    private String name;

    //getter and setter
    public UUID getIdParty() {
        return this.idParty;
    }

    public void setIdParty(UUID id) {
        this.idParty = id;
    }

    public String getName() {
        return this.name;
    }

    public Party name(String name) {
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<PartyCategory> getCategories() {
        return categories;
    }

    public Party categories(Set<PartyCategory> partyCategories) {
        this.categories = partyCategories;
        return this;
    }

    public Party addCategory(PartyCategory partyCategory) {
        this.categories.add(partyCategory);
        return this;
    }

    public Party removeCategory(PartyCategory partyCategory) {
        this.categories.remove(partyCategory);
        return this;
    }

    public void setCategories(Set<PartyCategory> partyCategories) {
        this.categories = partyCategories;
    }

    public Integer getIdType() {
        return idType;
    }

    public void setIdType(Integer idType) {
        this.idType = idType;
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(PostalAddress postalAddress) {
        this.postalAddress = postalAddress;
    }

//    public Set<RoleType> getRoleTypes() {
//        return roleTypes;
//    }

//    public void setRoleTypes(Set<RoleType> roleTypes) {
//        this.roleTypes = roleTypes;
//    }
//
//    public void addRole(Integer roleType) {
//        for (RoleType r: roleTypes) {
//            if (roleType.equals(r.getIdRoleType())) return;
//        }
//        roleTypes.add(new RoleType(roleType));
//    }
//
//    public void removeRole(Integer roleType) {
//        for (RoleType r: roleTypes) {
//            if (roleType.equals(r.getIdRoleType())) {
//                roleTypes.remove(r);
//                return;
//            }
//        }
//    }

    @PrePersist
    public void prePersist() {
    }

    @PreUpdate
    public void preUpdate() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Party party = (Party) o;
        if (party.idParty == null || this.idParty == null) {
            return false;
        }
        return Objects.equals(this.idParty, party.idParty);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idParty);
    }

    @Override
    public String toString() {
        return "Party{" +
            "idParty=" + this.idParty +
            ", name='" + getName() + "'" +
            '}';
    }
}
