package id.atiila.domain;

import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;import java.util.UUID;

import java.util.Objects;



/**
 * atiila consulting
 * Class definition for Entity PartyRelationship.
 */

@Entity
@Table(name = "party_relationship")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "partyrelationship")
public class PartyRelationship implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idparrel", columnDefinition = "BINARY(16)")
    private UUID idPartyRelationship;

    @Column(name = "dtfrom")
    @CreationTimestamp
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idstatustype", referencedColumnName="idstatustype")
    private StatusType statusType;

    @ManyToOne
    @JoinColumn(name="idreltyp", referencedColumnName="idreltyp")
    private RelationType relationType;

    public UUID getIdPartyRelationship() {
        return this.idPartyRelationship;
    }

    public void setIdPartyRelationship(UUID id) {
        this.idPartyRelationship = id;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public PartyRelationship dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public PartyRelationship dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public StatusType getStatusType() {
        return statusType;
    }

    public PartyRelationship statusType(StatusType statusType) {
        this.statusType = statusType;
        return this;
    }

    public void setStatusType(StatusType statusType) {
        this.statusType = statusType;
    }

    public RelationType getRelationType() {
        return relationType;
    }

    public PartyRelationship relationType(RelationType relationType) {
        this.relationType = relationType;
        return this;
    }

    public void setRelationType(RelationType relationType) {
        this.relationType = relationType;
    }

    @PrePersist
    public void prePersist() {
        if (dateThru == null) this.dateThru = BaseConstants.zoneDateEndTime();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PartyRelationship partyRelationship = (PartyRelationship) o;
        if (partyRelationship.idPartyRelationship == null || this.idPartyRelationship == null) {
            return false;
        }
        return Objects.equals(this.idPartyRelationship, partyRelationship.idPartyRelationship);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPartyRelationship);
    }

    @Override
    public String toString() {
        return "PartyRelationship{" +
            "idPartyRelationship=" + this.idPartyRelationship +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
