package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity ShipTo.
 */

@Entity
@Table(name = "ship_to")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "shipto")
public class ShipTo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idshipto")
    private String idShipTo;

    @Column(name = "idroletype")
    private Integer idRoleType;

    @ManyToOne
    @JoinColumn(name="idparty", referencedColumnName="idparty")
    private Party party;

    public String getIdShipTo() {
        return this.idShipTo;
    }

    public void setIdShipTo(String id) {
        this.idShipTo = id;
    }

    public Integer getIdRoleType() {
        return idRoleType;
    }

    public ShipTo idRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
        return this;
    }

    public void setIdRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
    }

    public Party getParty() {
        return party;
    }

    public ShipTo party(Party party) {
        this.party = party;
        return this;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShipTo shipTo = (ShipTo) o;
        if (shipTo.idShipTo == null || this.idShipTo == null) {
            return false;
        }
        return Objects.equals(this.idShipTo, shipTo.idShipTo);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idShipTo);
    }

    @Override
    public String toString() {
        return "ShipTo{" +
            "idShipTo=" + this.idShipTo +
            ", idRoleType='" + getIdRoleType() + "'" +
            '}';
    }
}
