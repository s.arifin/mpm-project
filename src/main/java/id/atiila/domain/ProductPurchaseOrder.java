package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity ProductPurchaseOrder.
 */

@Entity
@Table(name = "product_purchase_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "productpurchaseorder")
public class ProductPurchaseOrder extends PurchaseOrder {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "ProductPurchaseOrder{" +
            "idOrder=" + this.getIdOrder() +
            '}';
    }
}
