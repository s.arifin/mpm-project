package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;

/**
 * BeSmart Team
 * Class definition for Entity BookingSlotStandard.
 */

@Entity
@Table(name = "booking_slot_standard")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "bookingslotstandard")
public class BookingSlotStandard implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idbooslostd", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idBookSlotStd;

    @Column(name = "description")
    private String description;

    @Column(name = "capacity")
    private Integer capacity;

    @Column(name = "starthour")
    private Integer startHour;

    @Column(name = "startminute")
    private Integer startMinute;

    @Column(name = "endhour")
    private Integer endHour;

    @Column(name = "endminute")
    private Integer endMinute;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idboktyp", referencedColumnName="idboktyp")
    private BookingType bookingType;

    public UUID getIdBookSlotStd() {
        return this.idBookSlotStd;
    }

    public void setIdBookSlotStd(UUID id) {
        this.idBookSlotStd = id;
    }

    public String getDescription() {
        return description;
    }

    public BookingSlotStandard description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public BookingSlotStandard capacity(Integer capacity) {
        this.capacity = capacity;
        return this;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Integer getStartHour() {
        return startHour;
    }

    public BookingSlotStandard startHour(Integer startHour) {
        this.startHour = startHour;
        return this;
    }

    public void setStartHour(Integer startHour) {
        this.startHour = startHour;
    }

    public Integer getStartMinute() {
        return startMinute;
    }

    public BookingSlotStandard startMinute(Integer startMinute) {
        this.startMinute = startMinute;
        return this;
    }

    public void setStartMinute(Integer startMinute) {
        this.startMinute = startMinute;
    }

    public Integer getEndHour() {
        return endHour;
    }

    public BookingSlotStandard endHour(Integer endHour) {
        this.endHour = endHour;
        return this;
    }

    public void setEndHour(Integer endHour) {
        this.endHour = endHour;
    }

    public Integer getEndMinute() {
        return endMinute;
    }

    public BookingSlotStandard endMinute(Integer endMinute) {
        this.endMinute = endMinute;
        return this;
    }

    public void setEndMinute(Integer endMinute) {
        this.endMinute = endMinute;
    }

    public Internal getInternal() {
        return internal;
    }

    public BookingSlotStandard internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public BookingType getBookingType() {
        return bookingType;
    }

    public BookingSlotStandard bookingType(BookingType bookingType) {
        this.bookingType = bookingType;
        return this;
    }

    public void setBookingType(BookingType bookingType) {
        this.bookingType = bookingType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        BookingSlotStandard that = (BookingSlotStandard) o;

        return new EqualsBuilder()
            .append(idBookSlotStd, that.idBookSlotStd)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idBookSlotStd)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "BookingSlotStandard{" +
            "idBookSlotStd=" + this.idBookSlotStd +
            ", description='" + getDescription() + "'" +
            ", capacity='" + getCapacity() + "'" +
            ", startHour='" + getStartHour() + "'" +
            ", startMinute='" + getStartMinute() + "'" +
            ", endHour='" + getEndHour() + "'" +
            ", endMinute='" + getEndMinute() + "'" +
            '}';
    }
}
