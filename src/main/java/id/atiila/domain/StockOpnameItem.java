package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity StockOpnameItem.
 */

@Entity
@Table(name = "stock_opname_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "stockopnameitem")
public class StockOpnameItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idstopnite", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idStockopnameItem;

    @Column(name = "itemdescription")
    private String itemDescription;

    @Column(name = "qty")
    private Integer qty;

    @Column(name = "qtycount")
    private Integer qtyCount;

    @Column(name = "tagnumber")
    private String tagNumber;

    @Column(name = "checked")
    private Boolean hasChecked;

    @Column(name = "het", precision=10, scale=2)
    private BigDecimal het;

    @ManyToOne
    @JoinColumn(name="idstkop", referencedColumnName="idstkop")
    private StockOpname stockOpname;

    @ManyToOne
    @JoinColumn(name="idproduct", referencedColumnName="idproduct")
    private Product product;

    @ManyToOne
    @JoinColumn(name="idcontainer", referencedColumnName="idcontainer")
    private Container container;

    @JsonIgnore
    @OneToMany(mappedBy = "inventoryItem", cascade = CascadeType.ALL)
    private List<StockOpnameInventory> inventories = new ArrayList<>();

    public UUID getIdStockopnameItem() {
        return this.idStockopnameItem;
    }

    public void setIdStockopnameItem(UUID id) {
        this.idStockopnameItem = id;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public StockOpnameItem itemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
        return this;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Integer getQty() {
        return qty;
    }

    public StockOpnameItem qty(Integer qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getQtyCount() {
        return qtyCount;
    }

    public StockOpnameItem qtyCount(Integer qtyCount) {
        this.qtyCount = qtyCount;
        return this;
    }

    public void setQtyCount(Integer qtyCount) {
        this.qtyCount = qtyCount;
    }

    public String getTagNumber() {
        return tagNumber;
    }

    public StockOpnameItem tagNumber(String tagNumber) {
        this.tagNumber = tagNumber;
        return this;
    }

    public void setTagNumber(String tagNumber) {
        this.tagNumber = tagNumber;
    }

    public Boolean isHasChecked() {
        return hasChecked;
    }

    public StockOpnameItem hasChecked(Boolean hasChecked) {
        this.hasChecked = hasChecked;
        return this;
    }

    public void setHasChecked(Boolean hasChecked) {
        this.hasChecked = hasChecked;
    }

    public BigDecimal getHet() {
        return het;
    }

    public StockOpnameItem het(BigDecimal het) {
        this.het = het;
        return this;
    }

    public void setHet(BigDecimal het) {
        this.het = het;
    }

    public StockOpname getStockOpname() {
        return stockOpname;
    }

    public StockOpnameItem stockOpname(StockOpname stockOpname) {
        this.stockOpname = stockOpname;
        return this;
    }

    public void setStockOpname(StockOpname stockOpname) {
        this.stockOpname = stockOpname;
    }

    public Product getProduct() {
        return product;
    }

    public StockOpnameItem product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Container getContainer() {
        return container;
    }

    public StockOpnameItem container(Container container) {
        this.container = container;
        return this;
    }

    public Boolean getHasChecked() {
        return hasChecked;
    }

    public List<StockOpnameInventory> getInventories() {
        return inventories;
    }

    public void setInventories(List<StockOpnameInventory> inventories) {
        this.inventories = inventories;
    }

    public void setContainer(Container container) {
        this.container = container;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StockOpnameItem stockOpnameItem = (StockOpnameItem) o;
        if (stockOpnameItem.idStockopnameItem == null || this.idStockopnameItem == null) {
            return false;
        }
        return Objects.equals(this.idStockopnameItem, stockOpnameItem.idStockopnameItem);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idStockopnameItem);
    }

    @Override
    public String toString() {
        return "StockOpnameItem{" +
            "idStockopnameItem=" + this.idStockopnameItem +
            ", itemDescription='" + getItemDescription() + "'" +
            ", qty='" + getQty() + "'" +
            ", qtyCount='" + getQtyCount() + "'" +
            ", tagNumber='" + getTagNumber() + "'" +
            ", hasChecked='" + isHasChecked() + "'" +
            ", het='" + getHet() + "'" +
            '}';
    }
}
