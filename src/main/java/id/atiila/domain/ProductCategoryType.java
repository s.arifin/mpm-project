package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity ProductCategoryType.
 */

@Entity
@Table(name = "product_category_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "productcategorytype")
public class ProductCategoryType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcattyp")
    private Integer idCategoryType;

    @Column(name = "description")
    private String description;

    @Column(name = "refkey")
    private String refKey;

    public Integer getIdCategoryType() {
        return this.idCategoryType;
    }

    public void setIdCategoryType(Integer id) {
        this.idCategoryType = id;
    }

    public String getDescription() {
        return description;
    }

    public ProductCategoryType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefKey() {
        return refKey;
    }

    public ProductCategoryType refKey(String refKey) {
        this.refKey = refKey;
        return this;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProductCategoryType productCategoryType = (ProductCategoryType) o;
        if (productCategoryType.idCategoryType == null || this.idCategoryType == null) {
            return false;
        }
        return Objects.equals(this.idCategoryType, productCategoryType.idCategoryType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idCategoryType);
    }

    @Override
    public String toString() {
        return "ProductCategoryType{" +
            "idCategoryType=" + this.idCategoryType +
            ", description='" + getDescription() + "'" +
            ", refKey='" + getRefKey() + "'" +
            '}';
    }
}
