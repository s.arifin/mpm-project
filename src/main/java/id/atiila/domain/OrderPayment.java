package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity OrderPayment.
 */

@Entity
@Table(name = "order_payment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "orderpayment")
public class OrderPayment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idordpayite", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idOrderPayment;

    @Column(name = "amount", precision=10, scale=2)
    private BigDecimal amount;

    @ManyToOne
    @JoinColumn(name="idorder", referencedColumnName="idorder")
    private Orders orders;

    @ManyToOne
    @JoinColumn(name="idpayment", referencedColumnName="idpayment")
    private Payment payment;

    public UUID getIdOrderPayment() {
        return this.idOrderPayment;
    }

    public void setIdOrderPayment(UUID id) {
        this.idOrderPayment = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public OrderPayment amount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Orders getOrders() {
        return orders;
    }

    public OrderPayment orders(Orders orders) {
        this.orders = orders;
        return this;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public Payment getPayment() {
        return payment;
    }

    public OrderPayment payment(Payment payment) {
        this.payment = payment;
        return this;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderPayment orderPayment = (OrderPayment) o;
        if (orderPayment.idOrderPayment == null || this.idOrderPayment == null) {
            return false;
        }
        return Objects.equals(this.idOrderPayment, orderPayment.idOrderPayment);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idOrderPayment);
    }

    @Override
    public String toString() {
        return "OrderPayment{" +
            "idOrderPayment=" + this.idOrderPayment +
            ", amount='" + getAmount() + "'" +
            '}';
    }
}
