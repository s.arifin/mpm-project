package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity CustomerOrder.
 */

@Entity
@Table(name = "customer_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "customerorder")
public class CustomerOrder extends Orders {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idcustomer", referencedColumnName="idcustomer")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name="idbillto", referencedColumnName="idbillto")
    private BillTo billTo;

    public Internal getInternal() {
        return internal;
    }

    public CustomerOrder internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public Customer getCustomer() {
        return customer;
    }

    public CustomerOrder customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public BillTo getBillTo() {
        return billTo;
    }

    public CustomerOrder billTo(BillTo billTo) {
        this.billTo = billTo;
        return this;
    }

    public void setBillTo(BillTo billTo) {
        this.billTo = billTo;
    }

    @Override
    public String toString() {
        return "CustomerOrder{" +
            "idOrder=" + this.getIdOrder() +
            '}';
    }
}
