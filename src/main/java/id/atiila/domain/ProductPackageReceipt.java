package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity ProductPackageReceipt.
 */

@Entity
@Table(name = "product_package_receipt")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "productpackagereceipt")
public class ProductPackageReceipt extends ShipmentPackage {

    private static final long serialVersionUID = 1L;

    @Column(name = "documentnumber")
    private String documentNumber;

    @ManyToOne
    @JoinColumn(name="idshifro", referencedColumnName="idshipto")
    private ShipTo shipFrom;

    public String getDocumentNumber() {
        return documentNumber;
    }

    public ProductPackageReceipt documentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
        return this;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public ShipTo getShipFrom() {
        return shipFrom;
    }

    public ProductPackageReceipt shipFrom(ShipTo shipTo) {
        this.shipFrom = shipTo;
        return this;
    }

    public void setShipFrom(ShipTo shipTo) {
        this.shipFrom = shipTo;
    }

    @Override
    public String toString() {
        return "ProductPackageReceipt{" +
            "idPackage=" + this.getIdPackage() +
            ", dateCreated='" + getDateCreated() + "'" +
            ", documentNumber='" + getDocumentNumber() + "'" +
            '}';
    }
}
