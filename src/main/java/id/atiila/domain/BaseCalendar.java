package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity BaseCalendar.
 */

@Entity
@Table(name = "base_calendar")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "basecalendar")
public class BaseCalendar implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcalendar")
    private Integer idCalendar;

    @Column(name = "idcaltyp")
    private Integer idCalendarType;

    @Column(name = "idparent")
    private Integer idParent;

    @Column(name = "seqnum")
    private Integer sequence;

    @Column(name = "dtkey")
    private ZonedDateTime dateKey;

    @Column(name = "workday")
    private Boolean workDay;

    @Column(name = "description")
    private String description;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    public Integer getIdCalendar() {
        return this.idCalendar;
    }

    public void setIdCalendar(Integer id) {
        this.idCalendar = id;
    }

    public Integer getIdCalendarType() {
        return idCalendarType;
    }

    public BaseCalendar idCalendarType(Integer idCalendarType) {
        this.idCalendarType = idCalendarType;
        return this;
    }

    public void setIdCalendarType(Integer idCalendarType) {
        this.idCalendarType = idCalendarType;
    }

    public Integer getIdParent() {
        return idParent;
    }

    public BaseCalendar idParent(Integer idParent) {
        this.idParent = idParent;
        return this;
    }

    public void setIdParent(Integer idParent) {
        this.idParent = idParent;
    }

    public ZonedDateTime getDateKey() {
        return dateKey;
    }

    public BaseCalendar dateKey(ZonedDateTime dateKey) {
        this.dateKey = dateKey;
        return this;
    }

    public void setDateKey(ZonedDateTime dateKey) {
        this.dateKey = dateKey;
    }

    public Boolean isWorkDay() {
        return workDay;
    }

    public BaseCalendar workDay(Boolean workDay) {
        this.workDay = workDay;
        return this;
    }

    public void setWorkDay(Boolean workDay) {
        this.workDay = workDay;
    }

    public String getDescription() {
        return description;
    }

    public BaseCalendar description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public BaseCalendar dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public BaseCalendar dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Boolean getWorkDay() {
        return workDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BaseCalendar baseCalendar = (BaseCalendar) o;
        if (baseCalendar.idCalendar == null || this.idCalendar == null) {
            return false;
        }
        return Objects.equals(this.idCalendar, baseCalendar.idCalendar);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idCalendar);
    }

    @Override
    public String toString() {
        return "BaseCalendar{" +
            "idCalendar=" + this.idCalendar +
            ", idCalendarType='" + getIdCalendarType() + "'" +
            ", idParent='" + getIdParent() + "'" +
            ", dateKey='" + getDateKey() + "'" +
            ", workDay='" + isWorkDay() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
