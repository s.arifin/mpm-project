package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity BillingDisbursement.
 */

@Entity
@Table(name = "billing_disbursement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "billingdisbursement")
public class BillingDisbursement extends Billing {

    private static final long serialVersionUID = 1L;

    @Column(name = "vendorinvoice")
    private String vendorInvoice;

    @Column(name = "dtissued")
    private ZonedDateTime dateIssued;

    @Column(name = "discountinvoice")
    private BigDecimal discountLine;

    @Column(name = "freightcost")
    private BigDecimal freightCost;

    @ManyToOne
    @JoinColumn(name="idvendor", referencedColumnName="idvendor")
    private Vendor vendor;

    public BigDecimal getDiscountLine() {
        return discountLine;
    }

    public void setDiscountLine(BigDecimal discountLine) {
        this.discountLine = discountLine;
    }

    public BigDecimal getFreightCost() {
        return freightCost;
    }

    public void setFreightCost(BigDecimal freightCost) {
        this.freightCost = freightCost;
    }

    public ZonedDateTime getDateIssued() {
        return dateIssued;
    }

    public void setDateIssued(ZonedDateTime dateIssued) {
        this.dateIssued = dateIssued;
    }

    public String getVendorInvoice() {
        return vendorInvoice;
    }

    public BillingDisbursement vendorInvoice(String vendorInvoice) {
        this.vendorInvoice = vendorInvoice;
        return this;
    }

    public void setVendorInvoice(String vendorInvoice) {
        this.vendorInvoice = vendorInvoice;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public BillingDisbursement vendor(Vendor vendor) {
        this.vendor = vendor;
        return this;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    @Override
    public String toString() {
        return "BillingDisbursement{" +
            "idBilling=" + this.getIdBilling() +
            ", billingNumber='" + getBillingNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", dateDue='" + getDateDue() + "'" +
            ", vendorInvoice='" + getVendorInvoice() + "'" +
            '}';
    }
}
