package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity PaymentMethod.
 */

@Entity
@Table(name = "payment_method")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "paymentmethod")
public class PaymentMethod implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaymet")
    private Integer idPaymentMethod;

    @Column(name = "refkey")
    private String refKey;

    @Column(name = "description")
    private String description;

    @Column(name = "accountnumber")
    private String accountNumber;

    @ManyToOne
    @JoinColumn(name="idpaymettyp", referencedColumnName="idpaymettyp")
    private PaymentMethodType methodType;

    public Integer getIdPaymentMethod() {
        return this.idPaymentMethod;
    }

    public void setIdPaymentMethod(Integer id) {
        this.idPaymentMethod = id;
    }

    public String getRefKey() {
        return refKey;
    }

    public PaymentMethod refKey(String refKey) {
        this.refKey = refKey;
        return this;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public String getDescription() {
        return description;
    }

    public PaymentMethod description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public PaymentMethod accountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
        return this;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public PaymentMethodType getMethodType() {
        return methodType;
    }

    public PaymentMethod methodType(PaymentMethodType paymentMethodType) {
        this.methodType = paymentMethodType;
        return this;
    }

    public void setMethodType(PaymentMethodType paymentMethodType) {
        this.methodType = paymentMethodType;
    }

    @PrePersist
    public void prePersist() {
        if (this.refKey == null) this.refKey = this.idPaymentMethod.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PaymentMethod paymentMethod = (PaymentMethod) o;
        if (paymentMethod.idPaymentMethod == null || this.idPaymentMethod == null) {
            return false;
        }
        return Objects.equals(this.idPaymentMethod, paymentMethod.idPaymentMethod);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPaymentMethod);
    }

    @Override
    public String toString() {
        return "PaymentMethod{" +
            "idPaymentMethod=" + this.idPaymentMethod +
            ", refKey='" + getRefKey() + "'" +
            ", description='" + getDescription() + "'" +
            ", accountNumber='" + getAccountNumber() + "'" +
            '}';
    }
}
