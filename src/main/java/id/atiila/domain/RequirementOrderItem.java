package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;
import java.util.Objects;

/**
 * atiila consulting
 * Class definition for Entity RequirementOrderItem.
 */

@Entity
@Table(name = "requirement_order_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "requirementorderitem")
public class RequirementOrderItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idreqordite", columnDefinition = "BINARY(16)")
    private UUID idReqOrderItem;

    @Column(name = "qty")
    private Double qty;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name="idordite", referencedColumnName="idordite")
    private OrderItem orderItem;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name="idreq", referencedColumnName="idreq")
    private SalesUnitRequirement requirement;

    public UUID getIdReqOrderItem() {
        return this.idReqOrderItem;
    }

    public void setIdReqOrderItem(UUID id) {
        this.idReqOrderItem = id;
    }

    public Double getQty() {
        return qty;
    }

    public RequirementOrderItem qty(Double qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public void setQty(Integer qty) {
        this.qty = Double.valueOf(qty);
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public RequirementOrderItem orderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
        return this;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    public SalesUnitRequirement getRequirement() {
        return requirement;
    }

    public RequirementOrderItem requirement(SalesUnitRequirement requirement) {
        this.requirement = requirement;
        return this;
    }

    public void setRequirement(SalesUnitRequirement requirement) {
        this.requirement = requirement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RequirementOrderItem requirementOrderItem = (RequirementOrderItem) o;
        if (requirementOrderItem.idReqOrderItem == null || this.idReqOrderItem == null) {
            return false;
        }
        return Objects.equals(this.idReqOrderItem, requirementOrderItem.idReqOrderItem);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idReqOrderItem);
    }

    @Override
    public String toString() {
        return "RequirementOrderItem{" +
            "idReqOrderItem=" + this.idReqOrderItem +
            ", qty='" + getQty() + "'" +
            '}';
    }
}
