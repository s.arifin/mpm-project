package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity PaymentApplication.
 */

@Entity
@Table(name = "payment_application")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "paymentapplication")
public class PaymentApplication implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idpayapp", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idPaymentApplication;

    @Column(name = "amountapplied", precision=10, scale=2)
    private BigDecimal amountApplied;

    @ManyToOne
    @JoinColumn(name="idpayment", referencedColumnName="idpayment")
    private Payment payment;

    @ManyToOne
    @JoinColumn(name="idbilling", referencedColumnName="idbilling")
    private Billing billing;

    public UUID getIdPaymentApplication() {
        return this.idPaymentApplication;
    }

    public void setIdPaymentApplication(UUID id) {
        this.idPaymentApplication = id;
    }

    public BigDecimal getAmountApplied() {
        return amountApplied;
    }

    public PaymentApplication amountApplied(BigDecimal amountApplied) {
        this.amountApplied = amountApplied;
        return this;
    }

    public void setAmountApplied(BigDecimal amountApplied) {
        this.amountApplied = amountApplied;
    }

    public Payment getPayment() {
        return payment;
    }

    public PaymentApplication payment(Payment payment) {
        this.payment = payment;
        return this;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Billing getBilling() {
        return billing;
    }

    public PaymentApplication billing(Billing billing) {
        this.billing = billing;
        return this;
    }

    public void setBilling(Billing billing) {
        this.billing = billing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PaymentApplication paymentApplication = (PaymentApplication) o;
        if (paymentApplication.idPaymentApplication == null || this.idPaymentApplication == null) {
            return false;
        }
        return Objects.equals(this.idPaymentApplication, paymentApplication.idPaymentApplication);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPaymentApplication);
    }

    @Override
    public String toString() {
        return "PaymentApplication{" +
            "idPaymentApplication=" + this.idPaymentApplication +
            ", amountApplied='" + getAmountApplied() + "'" +
            '}';
    }
}
