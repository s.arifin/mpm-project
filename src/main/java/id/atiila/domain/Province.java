package id.atiila.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity Province.
 */

@Entity
@Table(name = "province")
@DiscriminatorValue(BaseConstants.TYPE_PROVINCE)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "province")
public class Province extends GeoBoundary {

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Province e = (Province) o;
        if (e.getIdGeobou() == null || this.getIdGeobou() == null) {
            return false;
        }
        return Objects.equals(this.getIdGeobou(), e.getIdGeobou());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdGeobou());
    }

    @Override
    public String toString() {
        return "Province{" +
            "idGeobou=" + this.getIdGeobou() +
            ", geocode='" + getGeoCode() + "'" +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
