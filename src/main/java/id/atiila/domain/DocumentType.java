package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity DocumentType.
 */

@Entity
@Table(name = "document_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "documenttype")
public class DocumentType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iddoctype")
    private Integer idDocumentType;

    @Column(name = "description")
    private String description;

    @Column(name = "idparent")
    private Integer idParent;

    public Integer getIdDocumentType() {
        return this.idDocumentType;
    }

    public void setIdDocumentType(Integer id) {
        this.idDocumentType = id;
    }

    public String getDescription() {
        return description;
    }

    public DocumentType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIdParent() {
        return idParent;
    }

    public DocumentType idParent(Integer idParent) {
        this.idParent = idParent;
        return this;
    }

    public void setIdParent(Integer idParent) {
        this.idParent = idParent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DocumentType documentType = (DocumentType) o;
        if (documentType.idDocumentType == null || this.idDocumentType == null) {
            return false;
        }
        return Objects.equals(this.idDocumentType, documentType.idDocumentType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idDocumentType);
    }

    @Override
    public String toString() {
        return "DocumentType{" +
            "idDocumentType=" + this.idDocumentType +
            ", description='" + getDescription() + "'" +
            ", idParent='" + getIdParent() + "'" +
            '}';
    }
}
