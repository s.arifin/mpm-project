package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.time.ZoneId;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity PriceComponent.
 */

@Entity
@Table(name = "price_component")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pricecomponent")
public class PriceComponent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idpricom", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idPriceComponent;

    @Column(name = "price", precision=10, scale=2)
    private BigDecimal price;

    @Column(name = "manfucterprice", precision=10, scale=2)
    private BigDecimal manfucterPrice;

    @Column(name = "percentvalue")
    private Float percent;

    @ManyToOne
    @JoinColumn(name="idpricetype", referencedColumnName = "idpricetype")
    private PriceType priceType;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idproduct", referencedColumnName="idproduct")
    private Product product;

    @ManyToOne
    @JoinColumn(name="idparty", referencedColumnName="idparty")
    private Party seller;

    public UUID getIdPriceComponent() {
        return this.idPriceComponent;
    }

    public void setIdPriceComponent(UUID id) {
        this.idPriceComponent = id;
    }

    public BigDecimal getPrice() {
        return this.price == null ? BigDecimal.ZERO : this.price;
    }

    public PriceComponent price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public void setPrice(BigDecimal price){
        this.price = price;
    }

    public void setPrice(Integer price){
        this.price = BigDecimal.valueOf(price);
    }

    public PriceType getPriceType() {
        return priceType;
    }

    public PriceComponent priceType(PriceType priceType){
        this.priceType = priceType;
        return this;
    }

    public void setPriceType(PriceType priceType) {
        this.priceType = priceType;
    }

    public BigDecimal getManfucterPrice() {
        return this.manfucterPrice == null ? BigDecimal.ZERO : this.manfucterPrice;
    }

    public PriceComponent manfucterPrice(BigDecimal manfucterPrice) {
        this.manfucterPrice = manfucterPrice;
        return this;
    }

    public void setManfucterPrice(BigDecimal manfucterPrice) {
        this.manfucterPrice = manfucterPrice;
    }

    public void setManfucterPrice(Integer manfucterPrice) {
        this.manfucterPrice = BigDecimal.valueOf(manfucterPrice);
    }

    public Float getPercent() {
        return this.percent == null ? 0f : this.percent;
    }

    public void setPercent(Float percent) {
        this.percent = percent;
    }

    public void setPercent(Integer percent) {
        this.percent = Float.valueOf(percent);
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public PriceComponent dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public PriceComponent dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Product getProduct() {
        return product;
    }

    public PriceComponent product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Party getSeller() {
        return seller;
    }

    public void setSeller(Party seller) {
        this.seller = seller;
    }

    @PrePersist
    @PreUpdate
    private void preUpdate() {
        if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
        if (this.dateThru == null) this.dateThru = ZonedDateTime.of( 9999, 12, 31, 23, 59, 59, 999, ZoneId.systemDefault());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PriceComponent that = (PriceComponent) o;

        return new EqualsBuilder()
            .append(idPriceComponent, that.idPriceComponent)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idPriceComponent)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "PriceComponent{" +
            "idPriceComponent=" + this.idPriceComponent +
            ", price='" + getPrice() + "'" +
            ", manfucterPrice='" + getManfucterPrice() + "'" +
            ", discount='" + getPercent() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
