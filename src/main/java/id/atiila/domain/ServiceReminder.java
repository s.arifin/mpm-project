package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity ServiceReminder.
 */

@Entity
@Table(name = "service_reminder")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "servicereminder")
public class ServiceReminder implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idreminder")
    private Integer idReminder;

    @Column(name = "days")
    private Integer days;

    @ManyToOne
    @JoinColumn(name="idclaimtype", referencedColumnName="idclaimtype")
    private DealerClaimType claimType;

    @ManyToOne
    @JoinColumn(name="idremindertype", referencedColumnName="idremindertype")
    private DealerReminderType reminderType;

    public Integer getIdReminder() {
        return this.idReminder;
    }

    public void setIdReminder(Integer id) {
        this.idReminder = id;
    }

    public Integer getDays() {
        return days;
    }

    public ServiceReminder days(Integer days) {
        this.days = days;
        return this;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public DealerClaimType getClaimType() {
        return claimType;
    }

    public ServiceReminder claimType(DealerClaimType dealerClaimType) {
        this.claimType = dealerClaimType;
        return this;
    }

    public void setClaimType(DealerClaimType dealerClaimType) {
        this.claimType = dealerClaimType;
    }

    public DealerReminderType getReminderType() {
        return reminderType;
    }

    public ServiceReminder reminderType(DealerReminderType dealerReminderType) {
        this.reminderType = dealerReminderType;
        return this;
    }

    public void setReminderType(DealerReminderType dealerReminderType) {
        this.reminderType = dealerReminderType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ServiceReminder serviceReminder = (ServiceReminder) o;
        if (serviceReminder.idReminder == null || this.idReminder == null) {
            return false;
        }
        return Objects.equals(this.idReminder, serviceReminder.idReminder);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idReminder);
    }

    @Override
    public String toString() {
        return "ServiceReminder{" +
            "idReminder=" + this.idReminder +
            ", days='" + getDays() + "'" +
            '}';
    }
}
