package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity ProductShipmentIncoming.
 */

@Entity
@Table(name = "product_shipment_incoming")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "productshipmentincoming")
public class ProductShipmentIncoming extends Shipment {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "ProductShipmentIncoming{" +
            "idShipment=" + this.getIdShipment() +
            ", shipmentNumber='" + getShipmentNumber() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateSchedulle='" + getDateSchedulle() + "'" +
            ", reason='" + getReason() + "'" +
            '}';
    }
}
