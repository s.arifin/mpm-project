package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity BillTo.
 */

@Entity
@Table(name = "bill_to")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "billto")
public class BillTo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idbillto")
    private String idBillTo;

    @Column(name = "idroletype")
    private Integer idRoleType;

    @ManyToOne
    @JoinColumn(name="idparty", referencedColumnName="idparty")
    private Party party;

    public String getIdBillTo() {
        return this.idBillTo;
    }

    public void setIdBillTo(String id) {
        this.idBillTo = id;
    }

    public Integer getIdRoleType() {
        return idRoleType;
    }

    public BillTo idRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
        return this;
    }

    public void setIdRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
    }

    public Party getParty() {
        return party;
    }

    public BillTo party(Party party) {
        this.party = party;
        return this;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BillTo billTo = (BillTo) o;
        if (billTo.idBillTo == null || this.idBillTo == null) {
            return false;
        }
        return Objects.equals(this.idBillTo, billTo.idBillTo);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idBillTo);
    }

    @Override
    public String toString() {
        return "BillTo{" +
            "idBillTo=" + this.idBillTo +
            ", idRoleType='" + getIdRoleType() + "'" +
            '}';
    }
}
