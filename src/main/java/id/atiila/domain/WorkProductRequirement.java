package id.atiila.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * BeSmart Team
 * Class definition for Entity WorkProductRequirement.
 */

@Entity
@Table(name = "work_product_req")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "workproductrequirement")
public class WorkProductRequirement extends Requirement {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "WorkProductRequirement{" +
            "idRequirement=" + this.getIdRequirement()+
            '}';
    }
}
