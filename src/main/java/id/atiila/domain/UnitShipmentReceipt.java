package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity UnitShipmentReceipt.
 */

@Entity
@Table(name = "unit_shipment_receipt")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "unitshipmentreceipt")
public class UnitShipmentReceipt extends ShipmentReceipt {

    private static final long serialVersionUID = 1L;

    @Column(name = "idframe")
    private String idFrame;

    @Column(name = "idmachine")
    private String idMachine;

    @Column(name = "yearassembly")
    private Integer yearAssembly;

    @Column(name = "unitprice", precision=10, scale=2)
    private BigDecimal unitPrice;

    @Column(name = "acc1")
    private Boolean acc1;

    @Column(name = "acc2")
    private Boolean acc2;

    @Column(name = "acc3")
    private Boolean acc3;

    @Column(name = "acc4")
    private Boolean acc4;

    @Column(name = "acc5")
    private Boolean acc5;

    @Column(name = "acc6")
    private Boolean acc6;

    @Column(name = "acc7")
    private Boolean acc7;

    @Column(name = "isreceipt")
    private Boolean receipt;

    public String getIdFrame() {
        return idFrame;
    }

    public UnitShipmentReceipt idFrame(String idFrame) {
        this.idFrame = idFrame;
        return this;
    }

    public void setIdFrame(String idFrame) {
        this.idFrame = idFrame;
    }

    public String getIdMachine() {
        return idMachine;
    }

    public UnitShipmentReceipt idMachine(String idMachine) {
        this.idMachine = idMachine;
        return this;
    }

    public void setIdMachine(String idMachine) {
        this.idMachine = idMachine;
    }

    public Integer getYearAssembly() {
        return yearAssembly;
    }

    public UnitShipmentReceipt yearAssembly(Integer yearAssembly) {
        this.yearAssembly = yearAssembly;
        return this;
    }

    public void setYearAssembly(Integer yearAssembly) {
        this.yearAssembly = yearAssembly;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public UnitShipmentReceipt unitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Boolean isAcc1() {
        return acc1;
    }

    public UnitShipmentReceipt acc1(Boolean acc1) {
        this.acc1 = acc1;
        return this;
    }

    public void setAcc1(Boolean acc1) {
        this.acc1 = acc1;
    }

    public Boolean isAcc2() {
        return acc2;
    }

    public UnitShipmentReceipt acc2(Boolean acc2) {
        this.acc2 = acc2;
        return this;
    }

    public void setAcc2(Boolean acc2) {
        this.acc2 = acc2;
    }

    public Boolean isAcc3() {
        return acc3;
    }

    public UnitShipmentReceipt acc3(Boolean acc3) {
        this.acc3 = acc3;
        return this;
    }

    public void setAcc3(Boolean acc3) {
        this.acc3 = acc3;
    }

    public Boolean isAcc4() {
        return acc4;
    }

    public UnitShipmentReceipt acc4(Boolean acc4) {
        this.acc4 = acc4;
        return this;
    }

    public void setAcc4(Boolean acc4) {
        this.acc4 = acc4;
    }

    public Boolean isAcc5() {
        return acc5;
    }

    public UnitShipmentReceipt acc5(Boolean acc5) {
        this.acc5 = acc5;
        return this;
    }

    public void setAcc5(Boolean acc5) {
        this.acc5 = acc5;
    }

    public Boolean isAcc6() {
        return acc6;
    }

    public UnitShipmentReceipt acc6(Boolean acc6) {
        this.acc6 = acc6;
        return this;
    }

    public void setAcc6(Boolean acc6) {
        this.acc6 = acc6;
    }

    public Boolean isAcc7() {
        return acc7;
    }

    public UnitShipmentReceipt acc7(Boolean acc7) {
        this.acc7 = acc7;
        return this;
    }

    public void setAcc7(Boolean acc7) {
        this.acc7 = acc7;
    }

    public Boolean getReceipt() { return receipt; }

    public void setReceipt(Boolean receipt) { this.receipt = receipt; }

    @Override
    public String toString() {
        return "UnitShipmentReceipt{" +
            "idReceipt=" + this.getIdReceipt() +
            ", idProduct='" + getIdProduct() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", receiptCode='" + getReceiptCode() + "'" +
            ", qtyAccept='" + getQtyAccept() + "'" +
            ", qtyReject='" + getQtyReject() + "'" +
            ", itemDescription='" + getItemDescription() + "'" +
            ", dateReceipt='" + getDateReceipt() + "'" +
            ", idFrame='" + getIdFrame() + "'" +
            ", idMachine='" + getIdMachine() + "'" +
            ", yearAssembly='" + getYearAssembly() + "'" +
            ", unitPrice='" + getUnitPrice() + "'" +
            ", acc1='" + isAcc1() + "'" +
            ", acc2='" + isAcc2() + "'" +
            ", acc3='" + isAcc3() + "'" +
            ", acc4='" + isAcc4() + "'" +
            ", acc5='" + isAcc5() + "'" +
            ", acc6='" + isAcc6() + "'" +
            ", acc7='" + isAcc7() + "'" +
            '}';
    }
}
