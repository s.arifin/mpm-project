package id.atiila.domain;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.UUID;

import javax.persistence.*;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.BaseConstants;
import org.bouncycastle.jcajce.provider.asymmetric.util.BaseCipherSpi;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * BeSmart Team
 * Class definition for Entity SalesUnitRequirement.
 */

@Entity
@Table(name = "sales_unit_requirement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "salesunitrequirement")
public class SalesUnitRequirement extends Requirement {

    private static final long serialVersionUID = 1L;

    @Column(name = "brokerfee", precision = 10, scale = 2)
    private BigDecimal brokerFee;

    @Column(name = "downpayment", precision=10, scale=2)
    private BigDecimal downPayment;

    @Column(name="idmachine")
    private String idmachine;

    @Column(name="idframe")
    private String idframe;

    @Column(name="requestidmachine", nullable = true, length = 90)
    private String requestIdMachine;

    @Column(name="requestidframe", nullable = true, length = 90)
    private String requestIdFrame;

    @Column(name="requestidmachineandframe")
    private String requestIdMachineAndFrame;

    @Column(name="idprosou", nullable = true)
    private Integer idProspectSource;

    @Column(name="note", nullable = true, length = 300)
    private String note;

    @Column(name="subsfincomp")
    private BigDecimal subsfincomp;

    @Column(name="subsmd")
    private BigDecimal subsmd;

    @Column(name="subsown")
    private BigDecimal subsown;

    @Column(name="subsahm")
    private BigDecimal subsahm;

    @Column(name="unitprice")
    private BigDecimal unitprice;

    @Column(name="bbnprice")
    private BigDecimal bbnprice;

    @Column(name="hetprice")
    private BigDecimal hetprice;

    @Column(name="notepartner", length = 300)
    private String notepartner;

    @Column(name="shipmentnote", nullable = true, length = 300)
    private String noteshipment;

    @Column(name="requestpoliceid", nullable = true, length = 30)
    private String requestpoliceid;

    @Column(name="prodyear", nullable = true)
    private Integer productYear;

    @Column(name="idprospect")
    private UUID idProspect;

    @Column(name="idsalesbroker")
    private UUID brokerId;

    @ManyToOne
    @JoinColumn(name="idcustomer", referencedColumnName="idcustomer")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name="idsalesman", referencedColumnName="idparrol")
    private Salesman salesman;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idsaletype", referencedColumnName="idsaletype")
    private SaleType saleType;

    @ManyToOne
    @JoinColumn(name="idbillto", referencedColumnName="idbillto")
    private BillTo billTo;

    @ManyToOne
    @JoinColumn(name="idcolor", referencedColumnName="idfeature")
    private Feature color;

    @ManyToOne
    @JoinColumn(name="idproduct", referencedColumnName="idproduct")
    private Motor product;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name="idowner", referencedColumnName = "idparty")
    private Person personOwner;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name="idorganizationowner", referencedColumnName = "idparty")
    private Organization organizationOwner;

    @Column(name="minpayment")
    private BigDecimal minPayment;

    @Column(name = "isunithotitem")
    private Boolean unitHotItem;

    @Column(name = "isunitindent")
    private Boolean unitIndent;

    @Column(name = "waitstnk")
    private Boolean waitStnk;

    @Column(name = "creditinstallment")
    private BigDecimal creditInstallment;

    @Column(name = "creditdownpayment")
    private BigDecimal creditDownPayment;

    @Column(name = "credittenor")
    private Integer creditTenor;

    @Column(name ="ponumber", length = 50)
    private String poNumber;

    @Column(name ="podate")
    private ZonedDateTime podate;

    @ManyToOne
    @JoinColumn(name="idleasing", referencedColumnName = "idparrol")
    private LeasingCompany leasingCompany;

    @Column(name ="idrequest")
    private UUID idRequest;

    @Column(name = "salescommissions")
    private BigDecimal komisiSales;

    @Column(name = "leasingnote")
    private String leasingnote;

    @Size(max = 20)
    @Column(name = "reset_key", length = 20)
    @JsonIgnore
    private String resetKey;

    @Column(name = "ipaddress")
    private String ipAddress;

    @Column(name = "facility")
    private UUID idGudang;

    public UUID getIdGudang() { return idGudang; }

    public void setIdGudang(UUID idGudang) { this.idGudang = idGudang; }

    public String getIpAddress() { return ipAddress; }

    public void setIpAddress(String ipAddress) { this.ipAddress = ipAddress; }

    public String getResetKey() { return resetKey; }

    public void setResetKey(String resetKey) { this.resetKey = resetKey; }

    public String getLeasingnote() { return leasingnote; }

    public void setLeasingnote(String leasingnote) { this.leasingnote = leasingnote; }

    public BigDecimal getKomisiSales() {
        return komisiSales;
    }

    public void setKomisiSales(BigDecimal komisiSales) {
        this.komisiSales = komisiSales;
    }

    public UUID getBrokerId() {
        return brokerId;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public ZonedDateTime getPodate() {
        return podate;
    }

    public void setPodate(ZonedDateTime podate) {
        this.podate = podate;
    }

    public void setBrokerId(UUID brokerId) {
        this.brokerId = brokerId;
    }

    public BigDecimal getCreditInstallment() {
        return creditInstallment;
    }

    public void setCreditInstallment(BigDecimal creditInstallment) {
        this.creditInstallment = creditInstallment;
    }

    public BigDecimal getCreditDownPayment() {
        return creditDownPayment;
    }

    public void setCreditDownPayment(BigDecimal creditDownPayment) {
        this.creditDownPayment = creditDownPayment;
    }

    public Integer getCreditTenor() {
        return creditTenor;
    }

    public void setCreditTenor(Integer creditTenor) {
        this.creditTenor = creditTenor;
    }

    public LeasingCompany getLeasingCompany() {
        return leasingCompany;
    }

    public void setLeasingCompany(LeasingCompany leasingCompany) {
        this.leasingCompany = leasingCompany;
    }

    public Boolean getWaitStnk() {
        return waitStnk;
    }

    public void setWaitStnk(Boolean waitStnk) {
        this.waitStnk = waitStnk;
    }

    public Boolean getUnitHotItem() {
        return unitHotItem;
    }

    public void setUnitHotItem(Boolean unitHotItem) {
        this.unitHotItem = unitHotItem;
    }

    public Boolean getUnitIndent() {
        return unitIndent;
    }

    public void setUnitIndent(Boolean unitIndent) {
        this.unitIndent = unitIndent;
    }

    public BigDecimal getMinPayment() {
        return minPayment;
    }

    public void setMinPayment(BigDecimal minPayment) {
        this.minPayment = minPayment;
    }

    public String getRequestIdMachineAndFrame() {
        return requestIdMachineAndFrame;
    }

    public void setRequestIdMachineAndFrame(String requestIdMachineAndFrame) {
        this.requestIdMachineAndFrame = requestIdMachineAndFrame;
    }

    public String getRequestIdMachine() {
        return requestIdMachine;
    }

    public void setRequestIdMachine(String requestIdMachine) {
        this.requestIdMachine = requestIdMachine;
    }

    public String getRequestIdFrame() {
        return requestIdFrame;
    }

    public void setRequestIdFrame(String requestIdFrame) {
        this.requestIdFrame = requestIdFrame;
    }

    public Integer getProductYear() { return productYear;}

    public void setProductYear(Integer productYear) { this.productYear = productYear; }

    public BigDecimal getBbnprice() {
        return bbnprice;
    }

    public String getIdmachine() {
        return idmachine;
    }

    public void setIdmachine(String idmachine) {
        this.idmachine = idmachine;
    }

    public String getIdframe() {
        return idframe;
    }

    public void setIdframe(String idframe) {
        this.idframe = idframe;
    }

    public SalesUnitRequirement bbnprice(BigDecimal bbnprice){
        this.bbnprice = bbnprice;
        return this;
    }

    public void setBbnprice(BigDecimal bbnprice) {
        this.bbnprice = bbnprice;
    }

    public BigDecimal getHetprice() {
        return hetprice;
    }

    public SalesUnitRequirement hetprice(BigDecimal hetprice){
        this.hetprice = hetprice;
        return this;
    }

    public void setHetprice(BigDecimal hetprice) {
        this.hetprice = hetprice;
    }

    public void setHetprice(Integer hetprice) {
        this.hetprice = BigDecimal.valueOf(hetprice);
    }

    //broker fee

    public BigDecimal getBrokerFee() {
        return brokerFee;
    }

    public SalesUnitRequirement brokerFee(BigDecimal brokerFee){
        this.brokerFee = brokerFee;
        return this;
    }

    public void setBrokerFee(BigDecimal brokerFee) {
        this.brokerFee = brokerFee;
    }

//    public LeasingTenorProvide getLeasingTenorProvide() {
//        return leasingTenorProvide;
//    }
//
//    public SalesUnitRequirement leasingTenorProvide(LeasingTenorProvide leasingTenorProvide){
//        this.leasingTenorProvide = leasingTenorProvide;
//        return this;
//    }
//
//    public void setLeasingTenorProvide(LeasingTenorProvide leasingTenorProvide) {
//        this.leasingTenorProvide = leasingTenorProvide;
//    }

    public String getRequestpoliceid() {
        return requestpoliceid;
    }

    public SalesUnitRequirement requestpoliceid(String requestpoliceid){
        this.requestpoliceid = requestpoliceid;
        return this;
    }

    public void setRequestpoliceid(String requestpoliceid) {
        this.requestpoliceid = requestpoliceid;
    }

    //price

    public BigDecimal getUnitPrice() {
        return unitprice;
    }

    public SalesUnitRequirement unitprice(BigDecimal price){
        this.unitprice = price;
        return this;
    }

    public void setUnitPrice(BigDecimal price) {
        this.unitprice = price;
    }

    //note rekanan

    public String getNotePartner() {
        return notepartner;
    }

    public SalesUnitRequirement notepartner(String note){
        this.notepartner = note;
        return this;
    }

    public void setNotePartner(String note) {
        this.notepartner = note;
    }

    // note kirim

    public String getNoteShipment() {
        return noteshipment;
    }

    public SalesUnitRequirement noteshipment(String address){
        this.noteshipment = address;
        return this;
    }

    public void setNoteShipment(String address) {
        this.noteshipment = address;
    }

    //subsahm

    public BigDecimal getSubsahm() {
        return subsahm;
    }

    public SalesUnitRequirement subsahm(BigDecimal subsahm){
        this.subsahm = subsahm;
        return this;
    }

    public void setSubsahm(BigDecimal subsahm) {
        this.subsahm = subsahm;
    }

    //subsown

    public BigDecimal getSubsown() {
        return subsown;
    }

    public SalesUnitRequirement subsown(BigDecimal subsown){
        this.subsown = subsown;
        return this;
    }

    public void setSubsown(BigDecimal subsown) {
        this.subsown = subsown;
    }


    //subsmd

    public BigDecimal getSubsmd() {
        return subsmd;
    }

    public SalesUnitRequirement subsmd(BigDecimal subsmd){
        this.subsmd = subsmd;
        return this;
    }

    public void setSubsmd(BigDecimal subsmd) {
        this.subsmd = subsmd;
    }


    //subsfincomp

    public BigDecimal getSubsfincomp() {
        return subsfincomp;
    }

    public SalesUnitRequirement subsfincomp(BigDecimal subsfincomp){
        this.subsfincomp = subsfincomp;
        return this;
    }

    public void setSubsfincomp(BigDecimal subsfincomp) {
        this.subsfincomp = subsfincomp;
    }

    //prospect

    public UUID getIdProspect() {
        return idProspect;
    }

    public void setIdProspect(UUID idProspect) {
        this.idProspect = idProspect;
    }

    public BigDecimal getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(BigDecimal unitprice) {
        this.unitprice = unitprice;
    }

    public String getNotepartner() {
        return notepartner;
    }

    public void setNotepartner(String notepartner) {
        this.notepartner = notepartner;
    }

    public String getNoteshipment() {
        return noteshipment;
    }

    public void setNoteshipment(String noteshipment) {
        this.noteshipment = noteshipment;
    }

    //note

    public String getNote() {
        return note;
    }

    public SalesUnitRequirement note(String note){
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    //prospect source

    public Integer getIdProspectSource() { return idProspectSource; }

    public SalesUnitRequirement idProspectSource(Integer idProspectSource) {
        this.idProspectSource = idProspectSource;
        return this;
    }

    public void setIdProspectSource(Integer idProspectSource) { this.idProspectSource = idProspectSource; }

    //down payment

    public BigDecimal getDownPayment() {
        return downPayment;
    }

    public SalesUnitRequirement downPayment(BigDecimal downPayment) {
        this.downPayment = downPayment;
        return this;
    }

    public void setDownPayment(BigDecimal downPayment) {
        this.downPayment = downPayment;
    }

    //person owner

    public Person getPersonOwner(){return personOwner;}

    public SalesUnitRequirement personOwner(Person person){
        this.personOwner = person;
        return this;
    }

    public void setPersonOwner(Person personowner) {
        this.personOwner = personowner;
    }

    //customer

    public Customer getCustomer() {
        return customer;
    }

    public SalesUnitRequirement customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    //salesman

    public Salesman getSalesman() {
        return salesman;
    }

    public SalesUnitRequirement salesman(Salesman salesman) {
        this.salesman = salesman;
        return this;
    }

    public void setSalesman(Salesman salesman) {
        this.salesman = salesman;
    }

    //internal

    public Internal getInternal() {
        return internal;
    }

    public SalesUnitRequirement internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public SaleType getSaleType() {
        return saleType;
    }

    public SalesUnitRequirement saleType(SaleType saleType) {
        this.saleType = saleType;
        return this;
    }

    public void setSaleType(SaleType saleType) {
        this.saleType = saleType;
    }

    public BillTo getBillTo() {
        return billTo;
    }

    public SalesUnitRequirement billTo(BillTo billTo) {
        this.billTo = billTo;
        return this;
    }

    public void setBillTo(BillTo billTo) {
        this.billTo = billTo;
    }

//    public SalesBroker getBroker() {
//        return broker;
//    }
//
//    public SalesUnitRequirement broker(SalesBroker salesBroker) {
//        this.broker = salesBroker;
//        return this;
//    }
//
//    public void setBroker(SalesBroker salesBroker) {
//        this.broker = salesBroker;
//    }

    public Feature getColor() {
        return color;
    }

    public SalesUnitRequirement color(Feature feature) {
        this.color = feature;
        return this;
    }

    public void setColor(Feature feature) {
        this.color = feature;
    }

    public Motor getProduct() {
        return product;
    }

    public SalesUnitRequirement product(Motor motor) {
        this.product = motor;
        return this;
    }

    public void setProduct(Motor motor) {
        this.product = motor;
    }

    public Integer getApprovalDiscount() {
        return getApprovalByType(BaseConstants.DISCOUNT_SUR);
    }

    public Integer getApprovalTerritorial(){
        return getApprovalByType(BaseConstants.PELANGGARAN_WILAYAH);
    }

    public Integer getApprovalLeasing(){
        return getApprovalByType(BaseConstants.LEASING);
    }

    public Organization getOrganizationOwner() {
        return organizationOwner;
    }

    public void setOrganizationOwner(Organization organizationOwner) {
        this.organizationOwner = organizationOwner;
    }

    public UUID getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(UUID idRequest) {
        this.idRequest = idRequest;
    }

    @Override
    public void prePersist() {
        super.prePersist();
        if (this.waitStnk == null) this.setWaitStnk(false);
        if (this.downPayment == null) this.setDownPayment(BigDecimal.ZERO);
        if (this.subsahm == null) this.setSubsahm(BigDecimal.ZERO);
        if (this.subsfincomp == null) this.setSubsfincomp(BigDecimal.ZERO);
        if (this.subsmd == null) this.setSubsmd(BigDecimal.ZERO);
        if (this.subsown == null) this.setSubsown(BigDecimal.ZERO);
        if (this.brokerFee == null) this.setBrokerFee(BigDecimal.ZERO);
        if (this.hetprice == null) this.setHetprice(BigDecimal.ZERO);
        if (this.bbnprice == null) this.setBbnprice(BigDecimal.ZERO);

//        if (getApprovalDiscount().equals(BaseConstants.STATUS_NOT_DEFINED))setApproval(BaseConstants.DISCOUNT_SUR, BaseConstants.STATUS_NOT_REQUIRED);
//        if (getApprovalTerritorial().equals(BaseConstants.STATUS_NOT_DEFINED))setApproval(BaseConstants.PELANGGARAN_WILAYAH, BaseConstants.STATUS_NOT_REQUIRED);
    }

    @PreUpdate
    public void preUpdate(){
        if (this.subsahm == null) this.setSubsahm(BigDecimal.ZERO);
        if (this.subsfincomp == null) this.setSubsfincomp(BigDecimal.ZERO);
        if (this.subsmd == null) this.setSubsmd(BigDecimal.ZERO);
        if (this.subsown == null) this.setSubsown(BigDecimal.ZERO);
        if (this.brokerFee == null) this.setBrokerFee(BigDecimal.ZERO);
        if (this.hetprice == null) this.setHetprice(BigDecimal.ZERO);
        if (this.bbnprice == null) this.setBbnprice(BigDecimal.ZERO);
    }

    @Override
    public String toString() {
        return "SalesUnitRequirement{" +
            "idRequirement=" + this.getIdRequirement() +
            ", downPayment='" + getDownPayment() + "'" +
            '}';
    }
}
