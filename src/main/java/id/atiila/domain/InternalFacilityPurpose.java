package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;import java.util.UUID;

import java.util.Objects;

/**
 * atiila consulting
 * Class definition for Entity InternalFacilityPurpose.
 */

@Entity
@Table(name = "internal_facility_purpose")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "internalfacilitypurpose")
public class InternalFacilityPurpose implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idintfacpur", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idPartyFacility;

    @Column(name = "dtfrom")
    @CreationTimestamp
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idfacility", referencedColumnName="idfacility")
    private Facility facility;

    @ManyToOne
    @JoinColumn(name="idpurposetype", referencedColumnName="idpurposetype")
    private PurposeType purposeType;

    public UUID getIdPartyFacility() {
        return this.idPartyFacility;
    }

    public void setIdPartyFacility(UUID id) {
        this.idPartyFacility = id;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public InternalFacilityPurpose dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public InternalFacilityPurpose dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Internal getInternal() {
        return internal;
    }

    public InternalFacilityPurpose internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public Facility getFacility() {
        return facility;
    }

    public InternalFacilityPurpose facility(Facility facility) {
        this.facility = facility;
        return this;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    public PurposeType getPurposeType() {
        return purposeType;
    }

    public InternalFacilityPurpose purposeType(PurposeType purposeType) {
        this.purposeType = purposeType;
        return this;
    }

    public void setPurposeType(PurposeType purposeType) {
        this.purposeType = purposeType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InternalFacilityPurpose internalFacilityPurpose = (InternalFacilityPurpose) o;
        if (internalFacilityPurpose.idPartyFacility == null || this.idPartyFacility == null) {
            return false;
        }
        return Objects.equals(this.idPartyFacility, internalFacilityPurpose.idPartyFacility);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPartyFacility);
    }

    @Override
    public String toString() {
        return "InternalFacilityPurpose{" +
            "idPartyFacility=" + this.idPartyFacility +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
