package id.atiila.domain;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.BaseConstants;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * BeSmart Team
 * Class definition for Entity Internal.
 */

@Entity
@Table(name = "internal")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "internal")
public class Internal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idinternal")
    private String idInternal;

    @Column(name = "iddealercode")
    private String idDealerCode;

    @Column(name = "idmpm")
    private String idMPM;

    @Column(name = "idahm")
    private String idAHM;

    @Column(name = "idroletype")
    private Integer idRoleType;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "idparty", referencedColumnName = "idparty")
    private Organization organization;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idparent", referencedColumnName = "idinternal")
    private Internal parent;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idroot", referencedColumnName = "idinternal")
    private Internal root;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "idinternal", referencedColumnName = "idinternal")
    @Where(clause = "current_timestamp between dtfrom and dtthru")
    private Set<InternalFacilityPurpose> facilities = new HashSet<>();

    @Transient
    private Facility primaryFacility;

    public String getIdInternal() {
        return idInternal;
    }

    public Internal idInternal(String idInternal) {
        this.idInternal = idInternal;
        return this;
    }

    public void setIdInternal(String idInternal) {
        this.idInternal = idInternal;
    }

    public String getIdDealerCode() { return idDealerCode; }

    public void setIdDealerCode(String idDealerCode) { this.idDealerCode = idDealerCode; }

    public String getIdMPM() { return idMPM; }

    public void setIdMPM(String idMPM) { this.idMPM = idMPM; }

    public String getIdAHM() { return idAHM; }

    public void setIdAHM(String idAHM) { this.idAHM = idAHM; }

    public Integer getIdRoleType() {
        return idRoleType;
    }

    public void setIdRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Internal getParent() {
        return parent;
    }

    public void setParent(Internal parent) {
        this.parent = parent;
    }

    public Internal getRoot() {
        return root;
    }

    public void setRoot(Internal root) {
        this.root = root;
    }

    public Facility getPrimaryFacility() {
        if (this.primaryFacility == null && this.facilities.size() > 0) {
            for (InternalFacilityPurpose o: facilities) {
                if (o.getPurposeType().getIdPurposeType().equals(BaseConstants.PURP_TYPE_WAREHOUSE)) {
                    this.primaryFacility = o.getFacility();
                    break;
                }
            }
        }
        return this.primaryFacility;
    }

    public void setPrimaryFacility(Facility primaryFacility) {
        this.primaryFacility = primaryFacility;
    }

    public Set<InternalFacilityPurpose> getFacilities() {
        return facilities;
    }

    public void setFacilities(Set<InternalFacilityPurpose> facilities) {
        this.facilities = facilities;
    }

    public Facility addFacility(PurposeType purp, Facility f) {
        for (InternalFacilityPurpose o: facilities) {
            if (!o.getFacility().equals(f) && o.getPurposeType().equals(purp)) {
                o.setDateThru(ZonedDateTime.now());
            }
            if (o.getFacility().equals(f) && o.getPurposeType().equals(purp)) return f;
        }
        InternalFacilityPurpose o = new InternalFacilityPurpose();
        o.setFacility(f);
        o.setInternal(this);
        o.setPurposeType(purp);
        o.setDateFrom(ZonedDateTime.now());
        o.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        facilities.add(o);

        return f;
    }

    public void removeFacility(Integer purp) {
        for (InternalFacilityPurpose o: facilities) {
            if (o.getPurposeType().getIdPurposeType().equals(purp)) {
                o.setDateThru(ZonedDateTime.now());
                facilities.remove(o);
                return;
            }
        }
    }

    public Facility getFacility(Integer purp) {
        for (InternalFacilityPurpose o: facilities) {
            if (o.getPurposeType().getIdPurposeType().equals(purp)) {
                return o.getFacility();
            }
        }
        return  null;
    }


    @PrePersist
    public void prePersist() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Internal internal = (Internal) o;

        return new EqualsBuilder()
            .append(idInternal, internal.idInternal)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idInternal)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "Internal{" +
              "idInternal='" + getIdInternal() + "'" +
            '}';
    }
}
