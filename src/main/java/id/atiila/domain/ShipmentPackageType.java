package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity ShipmentPackageType.
 */

@Entity
@Table(name = "shipment_package_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "shipmentpackagetype")
public class ShipmentPackageType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    //TODO: Sebabin error init pertama karena di postconstruct insert ID nya sedangkan di domain di auto increment
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idshipactyp")
    private Integer idPackageType;

    @Column(name = "description")
    private String description;

    public Integer getIdPackageType() {
        return this.idPackageType;
    }

    public void setIdPackageType(Integer id) {
        this.idPackageType = id;
    }

    public String getDescription() {
        return description;
    }

    public ShipmentPackageType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShipmentPackageType shipmentPackageType = (ShipmentPackageType) o;
        if (shipmentPackageType.idPackageType == null || this.idPackageType == null) {
            return false;
        }
        return Objects.equals(this.idPackageType, shipmentPackageType.idPackageType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPackageType);
    }

    @Override
    public String toString() {
        return "ShipmentPackageType{" +
            "idPackageType=" + this.idPackageType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
