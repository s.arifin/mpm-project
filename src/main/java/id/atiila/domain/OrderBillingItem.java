package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity OrderBillingItem.
 */

@Entity
@Table(name = "order_billing_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "orderbillingitem")
public class OrderBillingItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idordbilite", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idOrderBillingItem;

    @Column(name = "qty")
    private Double qty;

    @Column(name = "amount", precision=10, scale=2)
    private BigDecimal amount;

    @ManyToOne
    @JoinColumn(name="idordite", referencedColumnName="idordite")
    private OrderItem orderItem;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="idbilite", referencedColumnName="idbilite")
    private BillingItem billingItem;

    public UUID getIdOrderBillingItem() {
        return this.idOrderBillingItem;
    }

    public void setIdOrderBillingItem(UUID id) {
        this.idOrderBillingItem = id;
    }

    public Double getQty() {
        return qty;
    }

    public OrderBillingItem qty(Double qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public OrderBillingItem amount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setAmount(Double amount) {
        this.amount = BigDecimal.valueOf(amount);
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public OrderBillingItem orderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
        return this;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    public BillingItem getBillingItem() {
        return billingItem;
    }

    public OrderBillingItem billingItem(BillingItem billingItem) {
        this.billingItem = billingItem;
        return this;
    }

    public void setBillingItem(BillingItem billingItem) {
        this.billingItem = billingItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderBillingItem orderBillingItem = (OrderBillingItem) o;
        if (orderBillingItem.idOrderBillingItem == null || this.idOrderBillingItem == null) {
            return false;
        }
        return Objects.equals(this.idOrderBillingItem, orderBillingItem.idOrderBillingItem);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idOrderBillingItem);
    }

    @Override
    public String toString() {
        return "OrderBillingItem{" +
            "idOrderBillingItem=" + this.idOrderBillingItem +
            ", qty='" + getQty() + "'" +
            ", amount='" + getAmount() + "'" +
            '}';
    }
}
