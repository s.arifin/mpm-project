package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity StatusType.
 */

@Entity
@Table(name = "status_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "statustype")
public class StatusType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idstatustype")
    private Integer idStatusType;

    @Column(name = "description")
    private String description;

    public Integer getIdStatusType() {
        return this.idStatusType;
    }

    public void setIdStatusType(Integer id) {
        this.idStatusType = id;
    }

    public String getDescription() {
        return description;
    }

    public StatusType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatusType statusType = (StatusType) o;
        if (statusType.idStatusType == null || this.idStatusType == null) return false;
        return Objects.equals(this.idStatusType, statusType.idStatusType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idStatusType);
    }

    @Override
    public String toString() {
        return "StatusType{" +
            "idStatusType=" + this.idStatusType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
