package id.atiila.domain;

import java.io.Serializable;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;

import javax.persistence.*;

import id.atiila.base.BaseConstants;
import id.atiila.base.DomainEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.GenericGenerator;

/**
 * atiila consulting
 * Class definition for Entity GeneralUpload Status.
 */

@Entity
@Table(name = "general_upload_status")
public class GeneralUploadStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idstatus", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idStatus;

    @Column(name = "idstatustype")
    private Integer idStatusType;

    @ManyToOne
    @JoinColumn(name="idgenupl", referencedColumnName = "idgenupl")
    private GeneralUpload owner;

	@Column(name = "idreason")
    private Integer idReasonType;

    @Column(name = "reason")
    private String reason;

    @Column(name="dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name="dtthru")
    private ZonedDateTime dateThru;

    public UUID getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(UUID idStatus) {
		this.idStatus = idStatus;
	}

	public Integer getIdStatusType() {
		return idStatusType;
	}

	public void setIdStatusType(Integer idStatusType) {
		this.idStatusType = idStatusType;
	}

	public GeneralUpload getOwner() {
		return owner;
	}

	public void setOwner(GeneralUpload owner) {
		this.owner = owner;
	}

	public ZonedDateTime getDateFrom() {
		return this.dateFrom;
	}

	public void setDateFrom(ZonedDateTime dateFrom) {
		this.dateFrom = dateFrom;
	}

	public ZonedDateTime getDateThru() {
		return this.dateThru;
	}

	public void setDateThru(ZonedDateTime dateThru) {
		this.dateThru = dateThru;
	}

    public Integer getIdReasonType() {
        return this.idReasonType;
    }

    public void setIdReasonType(Integer idReasonType) {
        this.idReasonType = idReasonType;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

	@PreUpdate
	@PrePersist
	public void preUpdate() {
		if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
		if (this.dateThru == null) this.dateThru = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		GeneralUploadStatus that = (GeneralUploadStatus) o;

		return new EqualsBuilder()
				.append(idStatus, that.idStatus)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idStatus)
				.toHashCode();
	}

    @Override
    public String toString() {
        return "GeneralUploadStatus{" +
            "idStatus=" + getIdStatus() +
            ", idStatusType=" + getIdStatusType() +
            ", owner=" + getOwner() +
            ", dateFrom=" + getDateFrom() +
            ", dateThru=" + getDateThru() +
            '}';
    }
}
