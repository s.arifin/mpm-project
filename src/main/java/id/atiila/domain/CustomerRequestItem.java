package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Formula;
import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * atiila consulting
 * Class definition for Entity CustomerRequestItem.
 */

@Entity
@Table(name = "customer_request_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "customerrequestitem")
public class CustomerRequestItem extends RequestItem {

    private static final long serialVersionUID = 1L;

    @Column(name = "qty")
    private Double qty;

    @Column(name = "unitprice", precision=10, scale=2)
    private BigDecimal unitPrice;

    @Column(name = "bbn")
    private BigDecimal bbn;

    @Column(name = "idframe")
    private String idFrame;

    @Column(name = "idmachine")
    private String idMachine;

    @Column(name = "sequence")
    private Integer sequence;

    @Formula("(select sum(ori.qtydelivered) from order_request_item ori where ori.idreqitem = idreqitem)")
    private Double qtyDelivered;

    @ManyToOne
    @JoinColumn(name="idcustomer", referencedColumnName="idcustomer")
    private Customer customer;

    public Double getQty() {
        return qty;
    }

    public CustomerRequestItem qty(Double qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public CustomerRequestItem unitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getIdFrame() {
        return idFrame;
    }

    public CustomerRequestItem idFrame(String idFrame) {
        this.idFrame = idFrame;
        return this;
    }

    public void setIdFrame(String idFrame) {
        this.idFrame = idFrame;
    }

    public String getIdMachine() {
        return idMachine;
    }

    public CustomerRequestItem idMachine(String idMachine) {
        this.idMachine = idMachine;
        return this;
    }

    public void setIdMachine(String idMachine) {
        this.idMachine = idMachine;
    }

    public Integer getSequence() {
        return sequence;
    }

    public CustomerRequestItem sequence(Integer sequence) {
        this.sequence = sequence;
        return this;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Customer getCustomer() {
        return customer;
    }

    public CustomerRequestItem customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Double getQtyDelivered() {
        return this.qtyDelivered == null ? 0d : this.qtyDelivered;
    }

    public BigDecimal getBbn() {
        return this.bbn == null ? BigDecimal.ZERO : this.bbn;
    }

    public void setBbn(BigDecimal bbn) {
        this.bbn = bbn;
    }

    public void setBbn(Integer bbn) {
        this.bbn = BigDecimal.valueOf(bbn);
    }

    public void setBbn(Double bbn) {
        this.bbn = BigDecimal.valueOf(bbn);
    }

    public void setQtyDelivered(Double qtyDelivered) {
        this.qtyDelivered = qtyDelivered;
    }

    @Override
    public void prePersist() {
        super.prePersist();
        if (this.getQty() == null) this.qty = 1d;
    }

    @Override
    public void preUpdate() {
        super.preUpdate();
        if (this.getDescription() == null && this.getProduct() != null) {
            this.setDescription(this.getProduct().getName());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CustomerRequestItem customerRequestItem = (CustomerRequestItem) o;
        if (customerRequestItem.getIdRequestItem() == null || this.getIdRequestItem() == null) {
            return false;
        }
        return Objects.equals(this.getIdRequestItem(), customerRequestItem.getIdRequestItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdRequestItem());
    }

    @Override
    public String toString() {
        return "CustomerRequestItem{" +
            "idRequestItem=" + this.getIdRequestItem() +
            ", itemDescription='" + getDescription() + "'" +
            ", qty='" + getQty() + "'" +
            ", unitPrice='" + getUnitPrice() + "'" +
            ", idFrame='" + getIdFrame() + "'" +
            ", idMachine='" + getIdMachine() + "'" +
            ", sequence='" + getSequence() + "'" +
            '}';
    }
}
