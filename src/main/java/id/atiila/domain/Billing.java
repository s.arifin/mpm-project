package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.DomainEntity;
import org.hibernate.annotations.*;

import org.hibernate.annotations.Cache;
import org.springframework.data.elasticsearch.annotations.Document;

import java.math.BigDecimal;
import java.util.*;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity Billing.
 */

@Entity
@Table(name = "billing")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "billing")
public class Billing extends AuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idbilling", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idBilling;

    @Column(name = "billingnumber")
    private String billingNumber;

    @Column(name = "description")
    private String description;

    @Column(name = "dtcreate")
    @CreationTimestamp
    private ZonedDateTime dateCreate;

    @Column(name = "dtdue")
    private ZonedDateTime dateDue;

    @Column(name = "refkey")
    private String refKey;

    @OneToMany(mappedBy = "billing", cascade = CascadeType.ALL)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<BillingItem> details = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="idbiltyp", referencedColumnName="idbiltyp")
    private BillingType billingType;

    @OneToMany(mappedBy = "billing")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PaymentApplication> payments = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idbilto", referencedColumnName="idbillto")
    private BillTo billTo;

    @ManyToOne
    @JoinColumn(name="idbilfro", referencedColumnName="idbillto")
    private BillTo billFrom;

    @Column(name = "printcounter")
    private Integer printCounter;

    public Integer getPrintCounter() {
        return printCounter;
    }

    public void setPrintCounter(Integer printCounter) {
        this.printCounter = printCounter;
    }

    public UUID getIdBilling() {
        return this.idBilling;
    }

    public void setIdBilling(UUID id) {
        this.idBilling = id;
    }

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<BillingStatus> statuses = new ArrayList<BillingStatus>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<BillingRole> roles = new ArrayList<BillingRole>();

    public String getBillingNumber() {
        return billingNumber;
    }

    public Billing billingNumber(String billingNumber) {
        this.billingNumber = billingNumber;
        return this;
    }

    public void setBillingNumber(String billingNumber) {
        this.billingNumber = billingNumber;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public Billing dateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
        return this;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public ZonedDateTime getDateDue() {
        return dateDue;
    }

    public Billing dateDue(ZonedDateTime dateDue) {
        this.dateDue = dateDue;
        return this;
    }

    public void setDateDue(ZonedDateTime dateDue) {
        this.dateDue = dateDue;
    }

    public Set<BillingItem> getDetails() {
        return details;
    }

    public Billing details(Set<BillingItem> billingItems) {
        this.details = billingItems;
        return this;
    }

    public Billing addDetail(BillingItem billingItem) {
        this.details.add(billingItem);
        billingItem.setBilling(this);
        return this;
    }

    public Billing removeDetail(BillingItem billingItem) {
        this.details.remove(billingItem);
        billingItem.setBilling(null);
        return this;
    }

    public void setDetails(Set<BillingItem> billingItems) {
        this.details = billingItems;
    }

    public BillingType getBillingType() {
        return billingType;
    }

    public Billing billingType(BillingType billingType) {
        this.billingType = billingType;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBillingType(BillingType billingType) {
        this.billingType = billingType;
    }

    public Set<PaymentApplication> getPayments() {
        return payments;
    }

    public Billing payments(Set<PaymentApplication> paymentApplications) {
        this.payments = paymentApplications;
        return this;
    }

    public Billing addPayment(PaymentApplication paymentApplication) {
        this.payments.add(paymentApplication);
        paymentApplication.setBilling(this);
        return this;
    }

    public Billing removePayment(PaymentApplication paymentApplication) {
        this.payments.remove(paymentApplication);
        paymentApplication.setBilling(null);
        return this;
    }

    public void setPayments(Set<PaymentApplication> paymentApplications) {
        this.payments = paymentApplications;
    }

    public Internal getInternal() {
        return internal;
    }

    public Billing internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public BillTo getBillTo() {
        return billTo;
    }

    public Billing billTo(BillTo billTo) {
        this.billTo = billTo;
        return this;
    }

    public void setBillTo(BillTo billTo) {
        this.billTo = billTo;
    }

    public BillTo getBillFrom() {
        return billFrom;
    }

    public Billing billFrom(BillTo billTo) {
        this.billFrom = billTo;
        return this;
    }

    public void setBillFrom(BillTo billTo) {
        this.billFrom = billTo;
    }

    public List<BillingRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<BillingRole> roles) {
        this.roles = roles;
    }

    public void setPartyRole(String user, Integer role)  {
        for (BillingRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) return;
        }

        BillingRole current = new BillingRole();
        current.setOwner(this);
        current.setIdRoleType(role);
		current.setUser(user);
        current.setDateFrom(ZonedDateTime.now());
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        roles.add(current);
    }

    public String getPartyRole(String user, Integer role)  {
        for (BillingRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) return current.getUser();
        }
        return null;
    }

    public void removePartyRole(String user, Integer role)  {
        for (BillingRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) {
				current.setDateThru(ZonedDateTime.now());
				return;
			}
        }
    }

    public List<BillingStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<BillingStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);

        for (BillingStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now();

        for (BillingStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        BillingStatus current = new BillingStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public BillingItem addProduct(Product product, Double qty, BigDecimal basePrice, BigDecimal unitPrice) {
        BillingItem b = new BillingItem();
        b.setIdItemType(BaseConstants.ITEM_TYPE_PRODUCT);
        b.setIdProduct(product.getIdProduct());
        b.setItemDescription(product.getName());
        b.setSequence(10 + getDetails().size());
        b.setQty(qty);
        b.setDiscount(0);
        b.setTaxAmount(0);
        b.setBasePrice(basePrice);
        b.setBilling(this);
        b.setUnitPrice(unitPrice);
        b.setBilling(this);
        this.getDetails().add(b);
        b.setTotalAmount(b.getQty() * b.getUnitPrice().doubleValue());
        return b;
    }

    public BillingItem addItem(Integer itemType, String desc, BigDecimal value) {
        if (value == null || value.doubleValue() == 0d) return null;

        BillingItem b = null;

        for (BillingItem o: getDetails()) {
            if (itemType.equals(o.getIdItemType())) {
                b = o;
                break;
            }
        }

        if (b == null) {
            b = new BillingItem();
            b.setIdItemType(itemType);
            b.setItemDescription(desc);
            b.setSequence(10000 + getDetails().size());
            b.setQty(1);
            b.setDiscount(0);
            b.setTaxAmount(0);
            b.setBasePrice(0);
            b.setBilling(this);
            b.setUnitPrice(0);
            b.setBilling(this);
            this.getDetails().add(b);
        }

        Double totValue = b.getUnitPrice().doubleValue() + (value == null ? 0d : value.doubleValue());
        b.setUnitPrice(totValue);
        b.setTotalAmount(b.getQty() * b.getUnitPrice().doubleValue());
        return b;
    }

    public BillingItem addItem(Integer itemType, String desc, Integer value) {
        return addItem(itemType, desc, BigDecimal.valueOf(value));
    }

    public BillingItem addItem(Integer itemType, String desc, Double value) {
        return addItem(itemType, desc, BigDecimal.valueOf(value));
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Billing billing = (Billing) o;
        if (billing.idBilling == null || this.idBilling == null) {
            return false;
        }
        return Objects.equals(this.idBilling, billing.idBilling);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idBilling);
    }

    @Override
    public String toString() {
        return "Billing{" +
            "idBilling=" + this.idBilling +
            ", billingNumber='" + getBillingNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", dateDue='" + getDateDue() + "'" +
            '}';
    }
}
