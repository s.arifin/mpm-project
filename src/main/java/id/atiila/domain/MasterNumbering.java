package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.util.Objects;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;

/**
 * BeSmart Team
 * Class definition for Entity MasterNumbering.
 */

@Entity
@Table(name = "master_numbering")
// @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)  // Tidak di cache, untuk menjaga angkanya
@Document(indexName = "masternumbering")
public class MasterNumbering implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idnumbering", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idNumbering;

    @Column(name = "idtag")
    private String idTag;

    @Column(name = "idvalue")
    private String idValue;

    @Column(name = "nextvalue")
    private Long nextValue;

    public UUID getIdNumbering() {
        return this.idNumbering;
    }

    public void setIdNumbering(UUID id) {
        this.idNumbering = id;
    }

    public String getIdTag() {
        return idTag;
    }

    public MasterNumbering idTag(String idTag) {
        this.idTag = idTag;
        return this;
    }

    public void setIdTag(String idTag) {
        this.idTag = idTag;
    }

    public String getIdValue() {
        return idValue;
    }

    public MasterNumbering idValue(String idValue) {
        this.idValue = idValue;
        return this;
    }

    public void setIdValue(String idValue) {
        this.idValue = idValue;
    }

    public Long getNextValue() {
        return nextValue;
    }

    public MasterNumbering nextValue(Long nextValue) {
        this.nextValue = nextValue;
        return this;
    }

    public void setNextValue(Long nextValue) {
        this.nextValue = nextValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MasterNumbering masterNumbering = (MasterNumbering) o;
        if (masterNumbering.idNumbering == null || this.idNumbering == null) {
            return false;
        }
        return Objects.equals(this.idNumbering, masterNumbering.idNumbering);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idNumbering);
    }

    @Override
    public String toString() {
        return "MasterNumbering{" +
            "idNumbering=" + this.idNumbering +
            ", idTag='" + getIdTag() + "'" +
            ", idValue='" + getIdValue() + "'" +
            ", nextValue='" + getNextValue() + "'" +
            '}';
    }
}
