package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.time.ZonedDateTime;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity Memos.
 */

@Entity
@Table(name = "memo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "memos")
public class Memos implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idmemo", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idMemo;

    @Column(name = "memonumber")
    private String memoNumber;

    @Column(name = "bbn", precision=10, scale=2)
    private BigDecimal bbn;

    @Column(name = "discount")
    private Float discount;

    @Column(name = "unitprice", precision=10, scale=2)
    private BigDecimal unitPrice;

    @Column(name = "dpp", precision=10, scale=2)
    private BigDecimal dpp;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal dealer;

    @ManyToOne
    @JoinColumn(name="idbilling", referencedColumnName="idbilling")
    private Billing billing;

    @ManyToOne
    @JoinColumn(name="idmemotype", referencedColumnName="idmemotype")
    private MemoType memoType;

    @Column(name = "ppn", precision=10, scale=2)
    private BigDecimal ppn;

    @Column(name = "aramount", precision=10, scale=2)
    private BigDecimal arAmount;

    @Column(name = "salesamount", precision=10, scale=2)
    private BigDecimal saleSamount;

    @ManyToOne
    //    (cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    //@JsonIgnore
    @JoinColumn(name = "idinvite", referencedColumnName = "idinvite")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    //private Set<InventoryItem> inventoryItems = new HashSet<>();
    private InventoryItem inventoryItem ;

    @ManyToOne
        // (cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
    //@JsonIgnore
    @JoinColumn( name= "idoldinvite", referencedColumnName = "idinvite")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    //private Set<InventoryItem> oldInventoryItems = new HashSet<>();
    private InventoryItem oldInventoryItem;

    @Column(name= "reqidfeature")
    private Integer reqIdFeature;

    @Column(name = "currbillnumb")
    private String currbillnumb;

    @Column(name = "dtcreated")
    private ZonedDateTime dateCreated;

    public BigDecimal getDpp() {
        return dpp;
    }

    public void setDpp(BigDecimal dpp) {
        this.dpp = dpp;
    }

    public BigDecimal getSaleSamount() {
        return saleSamount;
    }

    public void setSaleSamount(BigDecimal saleSamount) {
        this.saleSamount = saleSamount;
    }

    public String getCurrbillnumb() {
        return currbillnumb;
    }

    public void setCurrbillnumb(String currbillnumb) {
        this.currbillnumb = currbillnumb;
    }

    public UUID getIdMemo() {
        return this.idMemo;
    }

    public void setIdMemo(UUID id) {
        this.idMemo = id;
    }

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<MemosStatus> statuses = new ArrayList<MemosStatus>();

    public String getMemoNumber() {
        return memoNumber;
    }

    public Memos memoNumber(String memoNumber) {
        this.memoNumber = memoNumber;
        return this;
    }

    public void setMemoNumber(String memoNumber) {
        this.memoNumber = memoNumber;
    }

    public BigDecimal getBbn() {
        return bbn;
    }

    public Memos bbn(BigDecimal bbn) {
        this.bbn = bbn;
        return this;
    }

    public void setBbn(BigDecimal bbn) {
        this.bbn = bbn;
    }

    public Float getDiscount() {
        return discount;
    }

    public Memos discount(Float discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public Memos unitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Internal getDealer() {
        return dealer;
    }

    public Memos dealer(Internal internal) {
        this.dealer = internal;
        return this;
    }

    public void setDealer(Internal internal) {
        this.dealer = internal;
    }

    public Billing getBilling() {
        return billing;
    }

    public Memos billing(Billing billing) {
        this.billing = billing;
        return this;
    }

    public void setBilling(Billing billing) {
        this.billing = billing;
    }

    public MemoType getMemoType() {
        return memoType;
    }

    public Memos memoType(MemoType memoType) {
        this.memoType = memoType;
        return this;
    }

    public void setMemoType(MemoType memoType) {
        this.memoType = memoType;
    }

    public List<MemosStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<MemosStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now().plusNanos(1);

        for (MemosStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now();

        for (MemosStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        MemosStatus current = new MemosStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public Boolean getActiveData() {
        switch (getCurrentStatus()) {
            case BaseConstants.STATUS_DRAFT:
            case BaseConstants.STATUS_ACTIVE:
                return true;
        }
        return false;
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Memos memos = (Memos) o;
        if (memos.idMemo == null || this.idMemo == null) {
            return false;
        }
        return Objects.equals(this.idMemo, memos.idMemo);
    }

//    public InventoryItem getInventoryItem() {
//        return inventoryItem;
//    }
//
//    public void setInventoryItem(InventoryItem inventoryItem) {
//        this.inventoryItem = inventoryItem;
//    }

//    public InventoryItem getOldInventoryItem() {
//        return oldInventoryItem;
//    }
//
//    public void setOldInventoryItem(InventoryItem oldInventoryItem) {
//        this.oldInventoryItem = oldInventoryItem;
//    }

    public Integer getReqIdFeature() {
        return reqIdFeature;
    }

    public void setReqIdFeature(Integer reqIdFeature) {
        this.reqIdFeature = reqIdFeature;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idMemo);
    }

    public InventoryItem getInventoryItem() {
        return inventoryItem;
    }

    public void setInventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
    }

    public InventoryItem getOldInventoryItem() {
        return oldInventoryItem;
    }

    public void setOldInventoryItem(InventoryItem oldInventoryItem) {
        this.oldInventoryItem = oldInventoryItem;
    }

    public void setPpn(BigDecimal ppn) {
        this.ppn = ppn;
    }

    public BigDecimal getPpn() {
        return ppn;
    }

    public BigDecimal getArAmount() {
        return arAmount;
    }

    public void setArAmount(BigDecimal arAmount) {
        this.arAmount = arAmount;
    }

    public ZonedDateTime getDateCreated() { return dateCreated; }

    public void setDateCreated(ZonedDateTime dateCreated) { this.dateCreated = dateCreated; }

    @Override
    public String toString() {
        return "Memos{" +
            "idMemo=" + this.idMemo +
            ", memoNumber='" + getMemoNumber() + "'" +
            ", bbn='" + getBbn() + "'" +
            ", discount='" + getDiscount() + "'" +
            ", unitPrice='" + getUnitPrice() + "'" +
            '}';
    }
}
