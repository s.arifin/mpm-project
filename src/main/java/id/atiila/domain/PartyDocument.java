package id.atiila.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * BeSmart Team
 * Class definition for Entity PartyDocument.
 */

@Entity
@Table(name = "party_document")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "partydocument")
public class PartyDocument extends Documents {

    private static final long serialVersionUID = 1L;

    @Lob
    @Column(name = "content")
    private byte[] content;

    @Column(name = "contenttype")
    private String contentContentType;

    @ManyToOne
    @JoinColumn(name="idparty", referencedColumnName="idparty")
    private Party party;

    public byte[] getContent() {
        return content;
    }

    public PartyDocument content(byte[] content) {
        this.content = content;
        return this;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentContentType() {
        return contentContentType;
    }

    public PartyDocument contentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
        return this;
    }

    public void setContentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
    }

    public Party getParty() {
        return party;
    }

    public PartyDocument party(Party party) {
        this.party = party;
        return this;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    @Override
    public String toString() {
        return "PartyDocument{" +
            "idDocument=" + this.getIdDocument() +
            ", content='" + getContent() + "'" +
            ", contentContentType='" + contentContentType + "'" +
            '}';
    }
}
