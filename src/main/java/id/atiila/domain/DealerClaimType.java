package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity DealerClaimType.
 */

@Entity
@Table(name = "dealer_claim_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dealerclaimtype")
public class DealerClaimType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idclaimtype")
    private Integer idClaimType;

    @Column(name = "description")
    private String description;

    public Integer getIdClaimType() {
        return this.idClaimType;
    }

    public void setIdClaimType(Integer id) {
        this.idClaimType = id;
    }

    public String getDescription() {
        return description;
    }

    public DealerClaimType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DealerClaimType dealerClaimType = (DealerClaimType) o;
        if (dealerClaimType.idClaimType == null || this.idClaimType == null) {
            return false;
        }
        return Objects.equals(this.idClaimType, dealerClaimType.idClaimType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idClaimType);
    }

    @Override
    public String toString() {
        return "DealerClaimType{" +
            "idClaimType=" + this.idClaimType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
