package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity Feature.
 */

@Entity
@Table(name = "feature")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "feature")
public class Feature implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idfeature")
    private Integer idFeature;

    @Column(name = "description")
    private String description;

    @Column(name = "refkey")
    private String refKey;

    @ManyToOne
    @JoinColumn(name="idfeatyp", referencedColumnName="idfeatyp")
    private FeatureType featureType;

    public Integer getIdFeature() {
        return this.idFeature;
    }

    public void setIdFeature(Integer id) {
        this.idFeature = id;
    }

    public String getDescription() {
        return description;
    }

    public Feature description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefKey() {
        return refKey;
    }

    public Feature refKey(String refKey) {
        this.refKey = refKey;
        return this;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public FeatureType getFeatureType() {
        return featureType;
    }

    public Feature featureType(FeatureType featureType) {
        this.featureType = featureType;
        return this;
    }

    public void setFeatureType(FeatureType featureType) {
        this.featureType = featureType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Feature feature = (Feature) o;
        if (feature.idFeature == null || this.idFeature == null) {
            return false;
        }
        return Objects.equals(this.idFeature, feature.idFeature);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idFeature);
    }

    @Override
    public String toString() {
        return "Feature{" +
            "idFeature=" + this.idFeature +
            ", description='" + getDescription() + "'" +
            ", refKey='" + getRefKey() + "'" +
            '}';
    }
}
