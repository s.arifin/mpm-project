package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


/**
 * atiila consulting
 * Class definition for Entity Dimension.
 */

@Entity
@Table(name = "dimension")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dimension")
public class Dimension implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iddimension")
    private Integer idDimension;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name="iddimtyp", referencedColumnName="iddimtyp")
    private DimensionType dimensionType;

    public Integer getIdDimension() {
        return this.idDimension;
    }

    public void setIdDimension(Integer id) {
        this.idDimension = id;
    }

    public String getDescription() {
        return description;
    }

    public Dimension description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DimensionType getDimensionType() {
        return dimensionType;
    }

    public Dimension dimensionType(DimensionType dimensionType) {
        this.dimensionType = dimensionType;
        return this;
    }

    public void setDimensionType(DimensionType dimensionType) {
        this.dimensionType = dimensionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Dimension dimension = (Dimension) o;
        if (dimension.idDimension == null || this.idDimension == null) {
            return false;
        }
        return Objects.equals(this.idDimension, dimension.idDimension);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idDimension);
    }

    @Override
    public String toString() {
        return "Dimension{" +
            "idDimension=" + this.idDimension +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
