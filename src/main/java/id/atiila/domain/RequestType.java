package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * Be Smart Team
 * Class definition for Entity RequestType.
 */

@Entity
@Table(name = "request_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "requesttype")
public class RequestType implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final int VSO_GROUP = 101;
    public static final int VSO_DIRECT = 102;
    public static final int VSO_INTERNAL = 103;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idreqtype")
    private Integer idRequestType;

    @Column(name = "description")
    private String description;

    public Integer getIdRequestType() {
        return this.idRequestType;
    }

    public void setIdRequestType(Integer id) {
        this.idRequestType = id;
    }

    public String getDescription() {
        return description;
    }

    public RequestType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RequestType requestType = (RequestType) o;
        if (requestType.idRequestType == null || this.idRequestType == null) {
            return false;
        }
        return Objects.equals(this.idRequestType, requestType.idRequestType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idRequestType);
    }

    @Override
    public String toString() {
        return "RequestType{" +
            "idRequestType=" + this.idRequestType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
