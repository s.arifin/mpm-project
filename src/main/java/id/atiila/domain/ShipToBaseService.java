package id.atiila.domain;

import id.atiila.repository.ShipToRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class ShipToBaseService {

    @Autowired
    private ShipToRepository repo;

    public ShipTo findById(String id) {
        return repo.findOne(id);
    }
}
