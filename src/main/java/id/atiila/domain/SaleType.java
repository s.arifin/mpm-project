package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity SaleType.
 */

@Entity
@Table(name = "sale_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "saletype")
public class SaleType implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final int CASH = 20;
    public static final int CREDIT = 21;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idsaletype")
    private Integer idSaleType;

    @Column(name = "description")
    private String description;

    @Column(name="idparent")
    private Integer idParent;

    public Integer getIdSaleType() {
        return this.idSaleType;
    }

    public void setIdSaleType(Integer id) {
        this.idSaleType = id;
    }

    public String getDescription() {
        return description;
    }

    public SaleType description(String description) {
        this.description = description;
        return this;
    }

    public Integer getIdParent() {
        return idParent;
    }

    public void setIdParent(Integer idParent) {
        this.idParent = idParent;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SaleType saleType = (SaleType) o;
        if (saleType.idSaleType == null || this.idSaleType == null) {
            return false;
        }
        return Objects.equals(this.idSaleType, saleType.idSaleType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idSaleType);
    }

    @Override
    public String toString() {
        return "SaleType{" +
            "idSaleType=" + this.idSaleType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
