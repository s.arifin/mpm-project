package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.*;

import java.util.Objects;

/**
 * atiila consulting
 * Class definition for Entity UnitPreparation.
 */

@Entity
@Table(name = "unit_preparation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "unitpreparation")
public class UnitPreparation extends PickingSlip {

    private static final long serialVersionUID = 1L;

    @Column(name = "usermekanik")
    private String userMekanik;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idfacility", referencedColumnName="idfacility")
    private Facility facility;

    public String getUserMekanik() {
        return userMekanik;
    }

    public UnitPreparation userMekanik(String userMekanik) {
        this.userMekanik = userMekanik;
        return this;
    }

    public void setUserMekanik(String userMekanik) {
        this.userMekanik = userMekanik;
    }

    public Internal getInternal() {
        return internal;
    }

    public UnitPreparation internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public Facility getFacility() {
        return facility;
    }

    public UnitPreparation facility(Facility facility) {
        this.facility = facility;
        return this;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UnitPreparation unitPreparation = (UnitPreparation) o;
        if (unitPreparation.getIdSlip() == null || this.getIdSlip() == null) {
            return false;
        }
        return Objects.equals(this.getIdSlip(), unitPreparation.getIdSlip());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdSlip());
    }

    @Override
    public String toString() {
        return "UnitPreparation{" +
            "idSlip=" + this.getIdSlip() +
            ", dateCreate='" + getDateCreate() + "'" +
            ", userMekanik='" + getUserMekanik() + "'" +
            '}';
    }
}
