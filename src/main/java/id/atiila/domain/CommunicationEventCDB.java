package id.atiila.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * BeSmart Team
 * Class definition for Entity CommunicationEventCDB.
 */

@Entity
@Table(name = "communication_event_cdb")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "communicationeventcdb")
public class CommunicationEventCDB extends CommunicationEvent {

    private static final long serialVersionUID = 1L;

    @Column(name = "idcustomer")
    private String idCustomer;

    @Column(name = "hobby")
    private String hobby;

    @Column(name = "homestatus")
    private String homeStatus;

    @Column(name = "exponemonth")
    private String expenditureOneMonth;

    @Column(name = "job")
    private String job;

    @Column(name = "lasteducation")
    private String lastEducation;

    @Column(name = "owncellphone")
    private String mobilePhoneOwnershipStatus;

    @Column(name = "hondarefference")
    private String infoAboutHonda;

    @Column(name = "ownvehiclebrand")
    private String vehicleBrandOwner;

    @Column(name = "ownvehicletype")
    private String vehicleTypeOwner;

    @Column(name = "buyfor")
    private String buyFor;

    @Column(name = "vehicleuser ")
    private String vehicleUser ;

    @Column(name="facebook")
    private String facebook;

    @Column(name="instagram")
    private String instagram;

    @Column(name="twitter")
    private String twitter;

    @Column(name="youtube")
    private String youtube;

    @Column(name="email")
    private String email;

    @Column(name="idreligion")
    private int idReligion;

    @Column(name = "iscityidadd")
    private int isCityzenIdCardAddress;

    @Column(name = "ismailadd")
    private int isMailAddress;

    @Column(name="idprovince")
    private UUID idProvince;

    @Column(name="idcity")
    private UUID idCity;

    @Column(name="iddistrict")
    private UUID idDistrict;

    @Column(name="idvillage")
    private UUID idVillage;

    @Column(name = "address")
    private String address;

    @Column(name="idprovincesurat")
    private UUID idProvinceSurat;

    @Column(name="idcitysurat")
    private UUID idCitySurat;

    @Column(name="iddistrictsurat")
    private UUID idDistrictSurat;

    @Column(name="idvillagesurat")
    private UUID idVillageSurat;

    @Column(name = "addresssurat")
    private String addressSurat;

    @Column (name = "jenispembayaran")
    private String jenisPembayaran;

    @Column (name = "groupcustomer")
    private String groupCustomer;

    @Column (name = "keterangan")
    private String keterangan;

    @Column (name = "citizenship")
    private String citizenship;

    @Column (name = "postalcode")
    private String postalCode;

    @Column (name = "postalcodesurat")
    private String postalCodeSurat;

    @Column(name="idslsreq")
    private UUID idSalesUnitRequirement;

    public UUID getIdSalesUnitRequirement() {
        return idSalesUnitRequirement;
    }

    public void setIdSalesUnitRequirement(UUID idSalesUnitRequirement) {
        this.idSalesUnitRequirement = idSalesUnitRequirement;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCodeSurat() {
        return postalCodeSurat;
    }

    public void setPostalCodeSurat(String postalCodeSurat) {
        this.postalCodeSurat = postalCodeSurat;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public int getIdReligion() {
        return idReligion;
    }

    public void setIdReligion(int idReligion) {
        this.idReligion = idReligion;
    }

    public int getIsCityzenIdCardAddress() {
        return isCityzenIdCardAddress;
    }

    public void setIsCityzenIdCardAddress(int isCityzenIdCardAddress) {
        this.isCityzenIdCardAddress = isCityzenIdCardAddress;
    }

    public int getIsMailAddress() {
        return isMailAddress;
    }

    public void setIsMailAddress(int isMailAddress) {
        this.isMailAddress = isMailAddress;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getHobby() {
        return hobby;
    }

    public CommunicationEventCDB hobby(String hobby) {
        this.hobby = hobby;
        return this;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getHomeStatus() {
        return homeStatus;
    }

    public CommunicationEventCDB homeStatus(String homeStatus) {
        this.homeStatus = homeStatus;
        return this;
    }

    public void setHomeStatus(String homeStatus) {
        this.homeStatus = homeStatus;
    }

    public String getExpenditureOneMonth() {
        return expenditureOneMonth;
    }

    public CommunicationEventCDB expenditureOneMonth(String expenditureOneMonth) {
        this.expenditureOneMonth = expenditureOneMonth;
        return this;
    }

    public void setExpenditureOneMonth(String expenditureOneMonth) {
        this.expenditureOneMonth = expenditureOneMonth;
    }

    public String getJob() {
        return job;
    }

    public CommunicationEventCDB job(String job) {
        this.job = job;
        return this;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getLastEducation() {
        return lastEducation;
    }

    public CommunicationEventCDB lastEducation(String lastEducation) {
        this.lastEducation = lastEducation;
        return this;
    }

    public void setLastEducation(String lastEducation) {
        this.lastEducation = lastEducation;
    }

    public String getMobilePhoneOwnershipStatus() {
        return mobilePhoneOwnershipStatus;
    }

    public CommunicationEventCDB mobilePhoneOwnershipStatus(String mobilePhoneOwnershipStatus) {
        this.mobilePhoneOwnershipStatus = mobilePhoneOwnershipStatus;
        return this;
    }

    public void setMobilePhoneOwnershipStatus(String mobilePhoneOwnershipStatus) {
        this.mobilePhoneOwnershipStatus = mobilePhoneOwnershipStatus;
    }

    public String getInfoAboutHonda() {
        return infoAboutHonda;
    }

    public CommunicationEventCDB infoAboutHonda(String infoAboutHonda) {
        this.infoAboutHonda = infoAboutHonda;
        return this;
    }

    public void setInfoAboutHonda(String infoAboutHonda) {
        this.infoAboutHonda = infoAboutHonda;
    }

    public String getVehicleBrandOwner() {
        return vehicleBrandOwner;
    }

    public CommunicationEventCDB vehicleBrandOwner(String vehicleBrandOwner) {
        this.vehicleBrandOwner = vehicleBrandOwner;
        return this;
    }

    public void setVehicleBrandOwner(String vehicleBrandOwner) {
        this.vehicleBrandOwner = vehicleBrandOwner;
    }

    public String getVehicleTypeOwner() {
        return vehicleTypeOwner;
    }

    public CommunicationEventCDB vehicleTypeOwner(String vehicleTypeOwner) {
        this.vehicleTypeOwner = vehicleTypeOwner;
        return this;
    }

    public void setVehicleTypeOwner(String vehicleTypeOwner) {
        this.vehicleTypeOwner = vehicleTypeOwner;
    }

    public String getBuyFor() {
        return buyFor;
    }

    public CommunicationEventCDB buyFor(String buyFor) {
        this.buyFor = buyFor;
        return this;
    }

    public void setBuyFor(String buyFor) {
        this.buyFor = buyFor;
    }

    public String getVehicleUser () {
        return vehicleUser ;
    }

    public CommunicationEventCDB vehicleUser (String vehicleUser ) {
        this.vehicleUser  = vehicleUser ;
        return this;
    }

    public void setVehicleUser (String vehicleUser ) {
        this.vehicleUser  = vehicleUser ;
    }

    public UUID getIdProvince() { return idProvince; }

    public void setIdProvince(UUID idProvince) { this.idProvince = idProvince; }

    public UUID getIdCity() { return idCity; }

    public void setIdCity(UUID idCity) { this.idCity = idCity; }

    public UUID getIdDistrict() { return idDistrict; }

    public void setIdDistrict(UUID idDistrict) { this.idDistrict = idDistrict; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public UUID getIdProvinceSurat() {
        return idProvinceSurat;
    }

    public void setIdProvinceSurat(UUID idProvinceSurat) {
        this.idProvinceSurat = idProvinceSurat;
    }

    public UUID getIdCitySurat() {
        return idCitySurat;
    }

    public void setIdCitySurat(UUID idCitySurat) {
        this.idCitySurat = idCitySurat;
    }

    public UUID getIdDistrictSurat() {
        return idDistrictSurat;
    }

    public void setIdDistrictSurat(UUID idDistrictSurat) {
        this.idDistrictSurat = idDistrictSurat;
    }

    public String getAddressSurat() {
        return addressSurat;
    }

    public void setAddressSurat(String addressSurat) {
        this.addressSurat = addressSurat;
    }

    public String getJenisPembayaran() {
        return jenisPembayaran;
    }

    public void setJenisPembayaran(String jenisPembayaran) {
        this.jenisPembayaran = jenisPembayaran;
    }

    public String getGroupCustomer() {
        return groupCustomer;
    }

    public void setGroupCustomer(String groupCustomer) {
        this.groupCustomer = groupCustomer;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public UUID getIdVillage() {
        return idVillage;
    }

    public void setIdVillage(UUID idVillage) {
        this.idVillage = idVillage;
    }

    public UUID getIdVillageSurat() {
        return idVillageSurat;
    }

    public void setIdVillageSurat(UUID idVillageSurat) {
        this.idVillageSurat = idVillageSurat;
    }

    @Override
    public String toString() {
        return "CommunicationEventCDB{" +
            "idCommunicationEvent=" + this.getIdCommunicationEvent() +
//            ", idOrder='" + getIdOrder() + "'" +
            ", idCustomer='" + getIdCustomer() + "'" +
            ", hobby='" + getHobby() + "'" +
            ", homeStatus='" + getHomeStatus() + "'" +
            ", expenditureOneMonth='" + getExpenditureOneMonth() + "'" +
            ", job='" + getJob() + "'" +
            ", lastEducation='" + getLastEducation() + "'" +
            ", mobilePhoneOwnershipStatus='" + getMobilePhoneOwnershipStatus() + "'" +
            ", infoAboutHonda='" + getInfoAboutHonda() + "'" +
            ", vehicleBrandOwner='" + getVehicleBrandOwner() + "'" +
            ", vehicleTypeOwner='" + getVehicleTypeOwner() + "'" +
            ", buyFor='" + getBuyFor() + "'" +
            ", vehicleUser ='" + getVehicleUser () + "'" +
            '}';
    }
}
