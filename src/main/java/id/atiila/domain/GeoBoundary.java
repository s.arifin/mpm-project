package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.DomainEntity;
import org.hibernate.annotations.*;

import org.hibernate.annotations.Cache;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.UUID;
import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity GeoBoundary.
 */

@Entity
@Table(name = "geo_boundary")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "idgeoboutype")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "geoboundary")
public class GeoBoundary implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idgeobou", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idGeobou;

    @Column(name = "geocode")
    private String geoCode;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name="idparent", referencedColumnName="idgeobou")
    private GeoBoundary parent;

//    @ManyToOne
//    @JoinColumn(name="idgeoboutype", referencedColumnName="idgeoboutype")
//    private GeoBoundaryType geoBoundaryType;

    @Column(name="idgeoboutype", insertable = false, updatable = false)
    private Integer idGeobouType;

    public UUID getIdGeobou() {
        return this.idGeobou;
    }

    public void setIdGeobou(UUID id) {
        this.idGeobou = id;
    }

    public String getGeoCode() { return geoCode; }

    public GeoBoundary geoCode(String geoCode) {
        this.geoCode = geoCode;
        return this;
    }

    public void setGeoCode(String geoCode) {
        this.geoCode = geoCode;
    }

    public String getDescription() {
        return description;
    }

    public GeoBoundary description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public GeoBoundary getParent() {
        return parent;
    }

    public GeoBoundary parent(GeoBoundary geoBoundary) {
        this.parent = geoBoundary;
        return this;
    }

    public void setParent(GeoBoundary geoBoundary) {
        this.parent = geoBoundary;
    }

    public Integer getIdGeobouType() { return idGeobouType; }

    public void setIdGeobouType(Integer idGeobouType) { this.idGeobouType = idGeobouType; }

    //    public GeoBoundaryType getGeoBoundaryType() {
//        return geoBoundaryType;
//    }
//
//    public GeoBoundary geoBoundaryType(GeoBoundaryType geoBoundaryType) {
//        this.geoBoundaryType = geoBoundaryType;
//        return this;
//    }
//
//    public void setGeoBoundaryType(GeoBoundaryType geoBoundaryType) {
//        this.geoBoundaryType = geoBoundaryType;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GeoBoundary geoBoundary = (GeoBoundary) o;
        if (geoBoundary.idGeobou == null || this.idGeobou == null) {
            return false;
        }
        return Objects.equals(this.idGeobou, geoBoundary.idGeobou);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idGeobou);
    }

    @Override
    public String toString() {
        return "GeoBoundary{" +
            "idGeobou=" + this.idGeobou +
            ", geocode='" + getGeoCode() + "'" +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
