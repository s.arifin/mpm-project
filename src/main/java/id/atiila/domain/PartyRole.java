package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.time.ZoneId;
import java.util.UUID;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity PartyRole.
 */

@Entity
@Table(name = "party_role")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "idroletype")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "partyrole")
public class PartyRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idparrol", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idPartyRole;

    @Column(name = "dtregister")
    private ZonedDateTime dateRegister;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    /*@ManyToOne
    @JoinColumn(name="idroletype", referencedColumnName="idroletype", insertable = false, updatable = false)
    private RoleType roleType;*/

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name="idparty", referencedColumnName="idparty")
    private Party party;

    public UUID getIdPartyRole() {
        return this.idPartyRole;
    }

    public void setIdPartyRole(UUID id) {
        this.idPartyRole = id;
    }

    /*@JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<PartyRoleStatus> statuses = new ArrayList<PartyRoleStatus>();*/

    public ZonedDateTime getDateRegister() {
        return dateRegister;
    }

    public PartyRole dateRegister(ZonedDateTime dateRegister) {
        this.dateRegister = dateRegister;
        return this;
    }

    public void setDateRegister(ZonedDateTime dateRegister) {
        this.dateRegister = dateRegister;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public PartyRole dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public PartyRole dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Party getParty() {
        return party;
    }

    public PartyRole party(Party party) {
        this.party = party;
        return this;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    /*public List<PartyRoleStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<PartyRoleStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);
        
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);
        
        for (PartyRoleStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now();

        for (PartyRoleStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        PartyRoleStatus current = new PartyRoleStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(LocalDateTime.now());
        current.setDateThru(BaseConstants.endDate());
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public String getName() {
        return getParty() != null ? getParty().getName(): "";
    }*/

    @PreUpdate
    @PrePersist
    public void preUpdate() {
        if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
        if (this.dateThru == null) this.dateThru = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PartyRole partyRole = (PartyRole) o;
        if (partyRole.idPartyRole == null || this.idPartyRole == null) {
            return false;
        }
        return Objects.equals(this.idPartyRole, partyRole.idPartyRole);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPartyRole);
    }

    @Override
    public String toString() {
        return "PartyRole{" +
            "idPartyRole=" + this.idPartyRole +
            ", dateRegister='" + getDateRegister() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
