package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity BookingSlot.
 */

@Entity
@Table(name = "booking_slot")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "bookingslot")
public class BookingSlot implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idbooslo", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idBookSlot;

    @Column(name = "slotnumber")
    private String slotNumber;

    @ManyToOne
    @JoinColumn(name="idcalendar", referencedColumnName="idcalendar")
    private BaseCalendar calendar;

    @ManyToOne
    @JoinColumn(name="idbooslostd", referencedColumnName="idbooslostd")
    private BookingSlotStandard standard;

    public UUID getIdBookSlot() {
        return this.idBookSlot;
    }

    public void setIdBookSlot(UUID id) {
        this.idBookSlot = id;
    }

    public String getSlotNumber() {
        return slotNumber;
    }

    public BookingSlot slotNumber(String slotNumber) {
        this.slotNumber = slotNumber;
        return this;
    }

    public void setSlotNumber(String slotNumber) {
        this.slotNumber = slotNumber;
    }

    public BaseCalendar getCalendar() {
        return calendar;
    }

    public BookingSlot calendar(BaseCalendar baseCalendar) {
        this.calendar = baseCalendar;
        return this;
    }

    public void setCalendar(BaseCalendar baseCalendar) {
        this.calendar = baseCalendar;
    }

    public BookingSlotStandard getStandard() {
        return standard;
    }

    public BookingSlot standard(BookingSlotStandard bookingSlotStandard) {
        this.standard = bookingSlotStandard;
        return this;
    }

    public void setStandard(BookingSlotStandard bookingSlotStandard) {
        this.standard = bookingSlotStandard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BookingSlot bookingSlot = (BookingSlot) o;
        if (bookingSlot.idBookSlot == null || this.idBookSlot == null) {
            return false;
        }
        return Objects.equals(this.idBookSlot, bookingSlot.idBookSlot);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idBookSlot);
    }

    @Override
    public String toString() {
        return "BookingSlot{" +
            "idBookSlot=" + this.idBookSlot +
            ", slotNumber='" + getSlotNumber() + "'" +
            '}';
    }
}
