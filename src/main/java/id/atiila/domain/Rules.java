package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Objects;


/**
 * BeSmart Team
 * Class definition for Entity RuleSalesDiscount.
 */

@Entity
@Table(name = "rules")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "rulesalesdiscount")
public class Rules implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idrule")
    private Integer idRule;

    @Column(name = "idrultyp")
    private Integer idRuleType;

    @Column(name = "seqnum")
    private Integer sequenceNumber;

    @Column(name = "description")
    private String description;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @Column(name = "idinternal")
    private String idInternal;

    //getter and setter
    public String getIdInternal() {
        return idInternal;
    }

    public void setIdInternal(String idInternal) {
        this.idInternal = idInternal;
    }

    public Integer getIdRule() {
        return this.idRule;
    }

    public void setIdRule(Integer id) {
        this.idRule = id;
    }

    public Integer getIdRuleType() {
        return idRuleType;
    }

    public Rules idRuleType(Integer idRuleType) {
        this.idRuleType = idRuleType;
        return this;
    }

    public void setIdRuleType(Integer idRuleType) {
        this.idRuleType = idRuleType;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public Rules sequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
        return this;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getDescription() {
        return description;
    }

    public Rules description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public Rules dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public Rules dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    @PrePersist
    public void prePersist() {
        if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
        if (this.dateThru == null)
            this.dateThru = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Rules ruleSalesDiscount = (Rules) o;
        if (ruleSalesDiscount.idRule == null || this.idRule == null) {
            return false;
        }
        return Objects.equals(this.idRule, ruleSalesDiscount.idRule);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idRule);
    }

    @Override
    public String toString() {
        return "RuleSalesDiscount{" +
            "idRule=" + this.idRule +
            ", idRuleType='" + getIdRuleType() + "'" +
            ", sequenceNumber='" + getSequenceNumber() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
