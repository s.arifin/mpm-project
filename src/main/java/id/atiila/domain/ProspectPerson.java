package id.atiila.domain;

import javax.persistence.*;

import id.atiila.domain.listener.ProspectPersonListener;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * BeSmart Team
 * Class definition for Entity ProspectPerson.
 */

@Entity
@Table(name = "person_prospect")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
// @Document(indexName = "prospectperson")
// @EntityListeners(ProspectPersonListener.class)
public class ProspectPerson extends Prospect {

    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name="idparty", referencedColumnName="idparty")
    private Person person;

    @Column(name ="fname" )
    private String fName;

    @Column(name = "lname")
    private String lName;

    @Column(name = "cellphone")
    private String cellphone;

    @Column(name = "facilityname")
    private String facilityName;

    @Column(name = "internname")
    private String internName;

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getInternName() {
        return internName;
    }

    public void setInternName(String internName) {
        this.internName = internName;
    }

    public Person getPerson() {
        return person;
    }

    public ProspectPerson person(Person person) {
        this.person = person;
        return this;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "ProspectPerson{" +
            "idProspect=" + this.getIdProspect() +
            '}';
    }
}
