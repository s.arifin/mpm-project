package id.atiila.domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * BeSmart Team
 * Class definition for Entity TelecomunicationNumber.
 */

@Entity
@Table(name = "telecomunication_number")
@DiscriminatorValue("3")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "telecomunicationnumber")
public class TelecomunicationNumber extends ContactMechanism {

    private static final long serialVersionUID = 1L;

    @Column(name = "number")
    private String number;

    public String getNumber() {
        return number;
    }

    public TelecomunicationNumber number(String number) {
        this.number = number;
        return this;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String getDescription() {
        return this.number;
    }

    @Override
    public String toString() {
        return "TelecomunicationNumber{" +
            "idContact=" + this.getIdContact() +
            ", number='" + getNumber() + "'" +
            '}';
    }
}
