package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity UomType.
 */

@Entity
@Table(name = "uom_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "uomtype")
public class UomType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iduomtyp")
    private Integer idUomType;

    @Column(name = "description")
    private String description;

    public Integer getIdUomType() {
        return this.idUomType;
    }

    public void setIdUomType(Integer id) {
        this.idUomType = id;
    }

    public String getDescription() {
        return description;
    }

    public UomType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UomType uomType = (UomType) o;
        if (uomType.idUomType == null || this.idUomType == null) {
            return false;
        }
        return Objects.equals(this.idUomType, uomType.idUomType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idUomType);
    }

    @Override
    public String toString() {
        return "UomType{" +
            "idUomType=" + this.idUomType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
