package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.UUID;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;

/**
 * BeSmart Team
 * Class definition for Entity CommunicationEvent.
 */

@Entity
@Table(name = "communication_event")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "communicationevent")
public class CommunicationEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idcomevt", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idCommunicationEvent;

    @Column(name = "note")
    private String note;

    @ManyToOne
    @JoinColumn(name="idevetyp", referencedColumnName="idevetyp")
    private EventType eventType;

    @OneToOne
    @JoinTable(name = "communication_event_purpose",
        joinColumns = @JoinColumn(name="idcomevt", referencedColumnName="idcomevt"),
        inverseJoinColumns = @JoinColumn(name="idpurposetype", referencedColumnName="idpurposetype"))
    private PurposeType purpose;

    public UUID getIdCommunicationEvent() {
        return this.idCommunicationEvent;
    }

    public void setIdCommunicationEvent(UUID id) {
        this.idCommunicationEvent = id;
    }

    public EventType getEventType() {
        return eventType;
    }

    public CommunicationEvent eventType(EventType eventType) {
        this.eventType = eventType;
        return this;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<CommunicationEventStatus> statuses = new ArrayList<CommunicationEventStatus>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<CommunicationEventRole> roles = new ArrayList<CommunicationEventRole>();


    public String getNote() {
        return note;
    }

    public CommunicationEvent note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public PurposeType getPurpose() {
        return purpose;
    }

    public CommunicationEvent purpose(PurposeType purposeType) {
        this.purpose = purposeType;
        return this;
    }

    public void setPurpose(PurposeType purposeType) {
        this.purpose = purposeType;
    }

    public List<CommunicationEventRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<CommunicationEventRole> roles) {
        this.roles = roles;
    }

    public void setPartyRole(Party party, Integer role)  {
        for (CommunicationEventRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) return;
        }

        CommunicationEventRole current = new CommunicationEventRole();
        current.setOwner(this);
        current.setIdRoleType(role);
		current.setParty(party);
        current.setDateFrom(LocalDateTime.now());
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        roles.add(current);
    }

    public Party getPartyRole(Party party, Integer role)  {
        for (CommunicationEventRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) return current.getParty();
        }
        return null;
    }

    public void removePartyRole(Party party, Integer role)  {
        for (CommunicationEventRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) {
				current.setDateThru(LocalDateTime.now());
				return;
			}
        }
    }

    public List<CommunicationEventStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<CommunicationEventStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now().plusNanos(1);
        
        for (CommunicationEventStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now();

        for (CommunicationEventStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        CommunicationEventStatus current = new CommunicationEventStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public Boolean getActiveData() {
        switch (getCurrentStatus()) {
            case BaseConstants.STATUS_DRAFT:
            case BaseConstants.STATUS_ACTIVE:
                return true;
        }
        return false;
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommunicationEvent communicationEvent = (CommunicationEvent) o;
        if (communicationEvent.idCommunicationEvent == null || this.idCommunicationEvent == null) {
            return false;
        }
        return Objects.equals(this.idCommunicationEvent, communicationEvent.idCommunicationEvent);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idCommunicationEvent);
    }

    @Override
    public String toString() {
        return "CommunicationEvent{" +
            "idCommunicationEvent=" + this.idCommunicationEvent +
            ", note='" + getNote() + "'" +
            '}';
    }
}
