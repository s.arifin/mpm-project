package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * atiila consulting
 * Class definition for Entity OrderRequestItem.
 */

@Entity
@Table(name = "order_request_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "orderrequestitem")
public class OrderRequestItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idordreqite", columnDefinition = "BINARY(16)")
    private UUID idOrderRequestItem;

    @Column(name = "qty")
    private Double qty;

    @Column(name = "qtydelivered")
    private Double qtyDelivered;

    @ManyToOne
    @JoinColumn(name="idordite", referencedColumnName="idordite")
    private OrderItem orderItem;

    @ManyToOne
    @JoinColumn(name="idreqitem", referencedColumnName="idreqitem")
    private RequestItem requestItem;

    public UUID getIdOrderRequestItem() {
        return this.idOrderRequestItem;
    }

    public void setIdOrderRequestItem(UUID id) {
        this.idOrderRequestItem = id;
    }

    public Double getQty() {
        return qty;
    }

    public OrderRequestItem qty(Double qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Double getQtyDelivered() {
        return qtyDelivered;
    }

    public OrderRequestItem qtyDelivered(Double qtyDelivered) {
        this.qtyDelivered = qtyDelivered;
        return this;
    }

    public void setQtyDelivered(Double qtyDelivered) {
        this.qtyDelivered = qtyDelivered;
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public OrderRequestItem orderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
        return this;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    public RequestItem getRequestItem() {
        return requestItem;
    }

    public OrderRequestItem requestItem(RequestItem requestItem) {
        this.requestItem = requestItem;
        return this;
    }

    public void setRequestItem(RequestItem requestItem) {
        this.requestItem = requestItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderRequestItem orderRequestItem = (OrderRequestItem) o;
        if (orderRequestItem.idOrderRequestItem == null || this.idOrderRequestItem == null) {
            return false;
        }
        return Objects.equals(this.idOrderRequestItem, orderRequestItem.idOrderRequestItem);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idOrderRequestItem);
    }

    @Override
    public String toString() {
        return "OrderRequestItem{" +
            "idOrderRequestItem=" + this.idOrderRequestItem +
            ", qty='" + getQty() + "'" +
            ", qtyDelivered='" + getQtyDelivered() + "'" +
            '}';
    }
}
