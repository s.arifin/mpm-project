package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity BrokerType.
 */

@Entity
@Table(name = "broker_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "brokertype")
public class BrokerType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idbrotyp")
    private Integer idBrokerType;

    @Column(name = "description")
    private String description;

    public Integer getIdBrokerType() {
        return this.idBrokerType;
    }

    public void setIdBrokerType(Integer id) {
        this.idBrokerType = id;
    }

    public String getDescription() {
        return description;
    }

    public BrokerType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BrokerType brokerType = (BrokerType) o;
        if (brokerType.idBrokerType == null || this.idBrokerType == null) {
            return false;
        }
        return Objects.equals(this.idBrokerType, brokerType.idBrokerType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idBrokerType);
    }

    @Override
    public String toString() {
        return "BrokerType{" +
            "idBrokerType=" + this.idBrokerType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
