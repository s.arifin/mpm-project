package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity ReasonType.
 */

@Entity
@Table(name = "reason_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "reasontype")
public class ReasonType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idreason")
    private Integer idReason;

    @Column(name = "description")
    private String description;

    public Integer getIdReason() {
        return this.idReason;
    }

    public void setIdReason(Integer id) {
        this.idReason = id;
    }

    public String getDescription() {
        return description;
    }

    public ReasonType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReasonType reasonType = (ReasonType) o;
        if (reasonType.idReason == null || this.idReason == null) {
            return false;
        }
        return Objects.equals(this.idReason, reasonType.idReason);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idReason);
    }

    @Override
    public String toString() {
        return "ReasonType{" +
            "idReason=" + this.idReason +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
