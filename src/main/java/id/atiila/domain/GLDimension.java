package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


/**
 * atiila consulting
 * Class definition for Entity GLDimension.
 */

@Entity
@Table(name = "gl_dimension")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "gldimension")
public class GLDimension implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idgldim")
    private Integer idGLDimension;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idbillto", referencedColumnName="idbillto")
    private BillTo billTo;

    @ManyToOne
    @JoinColumn(name="idvendor", referencedColumnName="idvendor")
    private Vendor vendor;

    @ManyToOne
    @JoinColumn(name="idparcat", referencedColumnName="idcategory")
    private PartyCategory partyCategory;

    @ManyToOne
    @JoinColumn(name="idprocat", referencedColumnName="idcategory")
    private ProductCategory productCategory;

    @ManyToOne
    @JoinColumn(name="iddimension1", referencedColumnName="iddimension")
    private Dimension dimension1;

    @ManyToOne
    @JoinColumn(name="iddimension2", referencedColumnName="iddimension")
    private Dimension dimension2;

    @ManyToOne
    @JoinColumn(name="iddimension3", referencedColumnName="iddimension")
    private Dimension dimension3;

    @ManyToOne
    @JoinColumn(name="iddimension4", referencedColumnName="iddimension")
    private Dimension dimension4;

    @ManyToOne
    @JoinColumn(name="iddimension5", referencedColumnName="iddimension")
    private Dimension dimension5;

    @ManyToOne
    @JoinColumn(name="iddimension6", referencedColumnName="iddimension")
    private Dimension dimension6;

    public Integer getIdGLDimension() {
        return this.idGLDimension;
    }

    public void setIdGLDimension(Integer id) {
        this.idGLDimension = id;
    }

    public Internal getInternal() {
        return internal;
    }

    public GLDimension internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public BillTo getBillTo() {
        return billTo;
    }

    public GLDimension billTo(BillTo billTo) {
        this.billTo = billTo;
        return this;
    }

    public void setBillTo(BillTo billTo) {
        this.billTo = billTo;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public GLDimension vendor(Vendor vendor) {
        this.vendor = vendor;
        return this;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public PartyCategory getPartyCategory() {
        return partyCategory;
    }

    public GLDimension partyCategory(PartyCategory partyCategory) {
        this.partyCategory = partyCategory;
        return this;
    }

    public void setPartyCategory(PartyCategory partyCategory) {
        this.partyCategory = partyCategory;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public GLDimension productCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
        return this;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public Dimension getDimension1() {
        return dimension1;
    }

    public GLDimension dimension1(Dimension dimension) {
        this.dimension1 = dimension;
        return this;
    }

    public void setDimension1(Dimension dimension) {
        this.dimension1 = dimension;
    }

    public Dimension getDimension2() {
        return dimension2;
    }

    public GLDimension dimension2(Dimension dimension) {
        this.dimension2 = dimension;
        return this;
    }

    public void setDimension2(Dimension dimension) {
        this.dimension2 = dimension;
    }

    public Dimension getDimension3() {
        return dimension3;
    }

    public GLDimension dimension3(Dimension dimension) {
        this.dimension3 = dimension;
        return this;
    }

    public void setDimension3(Dimension dimension) {
        this.dimension3 = dimension;
    }

    public Dimension getDimension4() {
        return dimension4;
    }

    public GLDimension dimension4(Dimension dimension) {
        this.dimension4 = dimension;
        return this;
    }

    public void setDimension4(Dimension dimension) {
        this.dimension4 = dimension;
    }

    public Dimension getDimension5() {
        return dimension5;
    }

    public GLDimension dimension5(Dimension dimension) {
        this.dimension5 = dimension;
        return this;
    }

    public void setDimension5(Dimension dimension) {
        this.dimension5 = dimension;
    }

    public Dimension getDimension6() {
        return dimension6;
    }

    public GLDimension dimension6(Dimension dimension) {
        this.dimension6 = dimension;
        return this;
    }

    public void setDimension6(Dimension dimension) {
        this.dimension6 = dimension;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GLDimension gLDimension = (GLDimension) o;
        if (gLDimension.idGLDimension == null || this.idGLDimension == null) {
            return false;
        }
        return Objects.equals(this.idGLDimension, gLDimension.idGLDimension);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idGLDimension);
    }

    @Override
    public String toString() {
        return "GLDimension{" +
            "idGLDimension=" + this.idGLDimension +
            '}';
    }
}
