package id.atiila.domain;

import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;import java.util.UUID;

import java.util.Objects;


/**
 * atiila consulting
 * Class definition for Entity FeatureApplicable.
 */

@Entity
@Table(name = "feature_applicable")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "featureapplicable")
public class FeatureApplicable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idapplicability", columnDefinition = "BINARY(16)")
    private UUID idApplicability;

    @Column(name = "boolvalue")
    private Boolean boolValue;

    @Column(name = "dtfrom")
    @CreationTimestamp
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idfeatyp", referencedColumnName="idfeatyp")
    private FeatureType featureType;

    @ManyToOne
    @JoinColumn(name="idfeature", referencedColumnName="idfeature")
    private Feature feature;

    @ManyToOne
    @JoinColumn(name="idproduct", referencedColumnName="idproduct")
    private Product product;

    public UUID getIdApplicability() {
        return this.idApplicability;
    }

    public void setIdApplicability(UUID id) {
        this.idApplicability = id;
    }

    public Boolean isBoolValue() {
        return boolValue;
    }

    public FeatureApplicable boolValue(Boolean boolValue) {
        this.boolValue = boolValue;
        return this;
    }

    public void setBoolValue(Boolean boolValue) {
        this.boolValue = boolValue;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public FeatureApplicable dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public FeatureApplicable dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public FeatureType getFeatureType() {
        return featureType;
    }

    public FeatureApplicable featureType(FeatureType featureType) {
        this.featureType = featureType;
        return this;
    }

    public void setFeatureType(FeatureType featureType) {
        this.featureType = featureType;
    }

    public Feature getFeature() {
        return feature;
    }

    public FeatureApplicable feature(Feature feature) {
        this.feature = feature;
        return this;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    public Product getProduct() {
        return product;
    }

    public FeatureApplicable product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @PrePersist
    public void prePersist() {
        if (this.dateThru == null) this.dateThru = BaseConstants.zoneDateEndTime();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FeatureApplicable featureApplicable = (FeatureApplicable) o;
        if (featureApplicable.idApplicability == null || this.idApplicability == null) {
            return false;
        }
        return Objects.equals(this.idApplicability, featureApplicable.idApplicability);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idApplicability);
    }

    @Override
    public String toString() {
        return "FeatureApplicable{" +
            "idApplicability=" + this.idApplicability +
            ", boolValue='" + isBoolValue() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
