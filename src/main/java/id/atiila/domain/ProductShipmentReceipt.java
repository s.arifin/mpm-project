package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * BeSmart Team
 * Class definition for Entity ProductShipmentReceipt.
 */

@Entity
@Table(name = "product_shipment_receipt")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "productshipmentreceipt")
@EntityListeners(ProductShipmentReceipt.class)
public class ProductShipmentReceipt extends ShipmentReceipt {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "ProductShipmentReceipt{" +
            "idReceipt=" + this.getIdReceipt() +
            ", idProduct='" + getIdProduct() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", receiptCode='" + getReceiptCode() + "'" +
            ", qtyAccept='" + getQtyAccept() + "'" +
            ", qtyReject='" + getQtyReject() + "'" +
            ", itemDescription='" + getItemDescription() + "'" +
            ", dateReceipt='" + getDateReceipt() + "'" +
            '}';
    }
}
