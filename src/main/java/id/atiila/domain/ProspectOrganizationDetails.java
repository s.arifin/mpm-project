package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity ProspectOrganizationDetails.
 */

@Entity
@Table(name = "organization_prospect_detail")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "prospectorganizationdetails")
public class ProspectOrganizationDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idetail", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idProspectOrganizationDetail;

    @Column(name = "unitprice", precision=10, scale=2)
    private BigDecimal unitPrice;

    @Column(name = "discount")
    private Float discount;

    @ManyToOne
    @JoinColumn(name="idprospect", referencedColumnName="idprospect")
    private ProspectOrganization prospectOrganization;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="idpersonowner", referencedColumnName="idparty")
    private Person person;

    @ManyToOne
    @JoinColumn(name="idproduct", referencedColumnName="idproduct")
    private Motor product;

    @ManyToOne
    @JoinColumn(name="idfeature", referencedColumnName="idfeature")
    private Feature color;

    public UUID getIdProspectOrganizationDetail() {
        return this.idProspectOrganizationDetail;
    }

    public void setIdProspectOrganizationDetail(UUID id) {
        this.idProspectOrganizationDetail = id;
    }


    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public ProspectOrganizationDetails unitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Float getDiscount() {
        return discount;
    }

    public ProspectOrganizationDetails discount(Float discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public ProspectOrganization getProspectOrganization() {
        return prospectOrganization;
    }

    public ProspectOrganizationDetails prospectOrganization (ProspectOrganization prospectOrganization) {
        this.prospectOrganization = prospectOrganization;
        return this;
    }

    public void setProspectOrganization(ProspectOrganization prospectOrganization) {
        this.prospectOrganization = prospectOrganization;
    }

    public Person getPerson() {
        return person;
    }

    public ProspectOrganizationDetails person(Person person) {
        this.person = person;
        return this;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Motor getProduct() {
        return product;
    }

    public ProspectOrganizationDetails product(Motor motor) {
        this.product = motor;
        return this;
    }

    public void setProduct(Motor motor) {
        this.product = motor;
    }

    public Feature getColor() {
        return color;
    }

    public ProspectOrganizationDetails color(Feature feature) {
        this.color = feature;
        return this;
    }

    public void setColor(Feature feature) {
        this.color = feature;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProspectOrganizationDetails prospectOrganizationDetails = (ProspectOrganizationDetails) o;
        if (prospectOrganizationDetails.idProspectOrganizationDetail == null || this.idProspectOrganizationDetail == null) {
            return false;
        }
        return Objects.equals(this.idProspectOrganizationDetail, prospectOrganizationDetails.idProspectOrganizationDetail);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idProspectOrganizationDetail);
    }

    @Override
    public String toString() {
        return "ProspectOrganizationDetails{" +
            "idProspectOrganizationDetail=" + this.idProspectOrganizationDetail +
            ", unitPrice='" + getUnitPrice() + "'" +
            ", discount='" + getDiscount() + "'" +
            '}';
    }
}
