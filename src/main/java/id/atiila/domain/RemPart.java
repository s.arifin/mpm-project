package id.atiila.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

import id.atiila.domain.listener.RemPartListener;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import id.atiila.base.BaseConstants;

/**
 * BeSmart Team
 * Class definition for Entity RemPart.
 */

@Entity
@Table(name = "rem_part")
@DiscriminatorValue(BaseConstants.PRODUCT_TYPE_REM_PART)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "rempart")
@EntityListeners(RemPartListener.class)
public class RemPart extends Good {

    private static final long serialVersionUID = 1L;

    @Override
    public void prePersist() {
        if (this.getTaxable() == null) setTaxable(true);
        if (this.getPriceType() == null) setPriceType(2);
        super.prePersist();
    }

    @Override
    public void preUpdate() {
        if (this.getTaxable() == null) setTaxable(true);
        if (this.getPriceType() == null) setPriceType(2);
        super.preUpdate();
    }

    @Override
    public String toString() {
        return "RemPart{" +
            "idProduct=" + this.getIdProduct() +
            ", name='" + getName() + "'" +
            ", dateIntroduction='" + getDateIntroduction() + "'" +
            '}';
    }
}
