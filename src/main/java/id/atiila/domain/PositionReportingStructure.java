package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity PositionReportingStructure.
 */

@Entity
@Table(name = "position_reporting_structure")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "positionreportingstructure")
public class PositionReportingStructure implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idposrepstru")
    private Integer idPositionStructure;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idposfro", referencedColumnName="idposition")
    private Position positionFrom;

    @ManyToOne
    @JoinColumn(name="idposto", referencedColumnName="idposition")
    private Position positionTo;

    public Integer getIdPositionStructure() {
        return this.idPositionStructure;
    }

    public void setIdPositionStructure(Integer id) {
        this.idPositionStructure = id;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public PositionReportingStructure dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public PositionReportingStructure dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Position getPositionFrom() {
        return positionFrom;
    }

    public PositionReportingStructure positionFrom(Position position) {
        this.positionFrom = position;
        return this;
    }

    public void setPositionFrom(Position position) {
        this.positionFrom = position;
    }

    public Position getPositionTo() {
        return positionTo;
    }

    public PositionReportingStructure positionTo(Position position) {
        this.positionTo = position;
        return this;
    }

    public void setPositionTo(Position position) {
        this.positionTo = position;
    }

    @PrePersist
    public void prePersist() {
        if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
        if (this.dateThru == null)
            this.dateThru = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PositionReportingStructure positionReportingStructure = (PositionReportingStructure) o;
        if (positionReportingStructure.idPositionStructure == null || this.idPositionStructure == null) {
            return false;
        }
        return Objects.equals(this.idPositionStructure, positionReportingStructure.idPositionStructure);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPositionStructure);
    }

    @Override
    public String toString() {
        return "PositionReportingStructure{" +
            "idPositionStructure=" + this.idPositionStructure +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
