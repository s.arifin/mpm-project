package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity Shipment.
 */

@Entity
@Table(name = "shipment")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "shipment")
public class Shipment extends AuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idshipment", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idShipment;

    @Column(name = "shipmentnumber")
    private String shipmentNumber;

    @Column(name = "description")
    private String description;

    @Column(name = "dtschedulle")
    private ZonedDateTime dateSchedulle;

    @Column(name = "reason")
    private String reason;

    @Column(name = "dtcreatenumber")
    private ZonedDateTime dateCreateNumber;

    @OneToMany(mappedBy = "shipment", cascade = {CascadeType.ALL})
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ShipmentItem> details = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idshityp", referencedColumnName="idshityp")
    private ShipmentType shipmentType;

    @ManyToOne
    @JoinColumn(name="idshifro", referencedColumnName="idshipto")
    private ShipTo shipFrom;

    @ManyToOne
    @JoinColumn(name="idshito", referencedColumnName="idshipto")
    private ShipTo shipTo;

    @ManyToOne
    @JoinColumn(name="idposaddfro", referencedColumnName="idcontact")
    private PostalAddress addressFrom;

    @ManyToOne
    @JoinColumn(name="idposaddto", referencedColumnName="idcontact")
    private PostalAddress addressTo;

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<ShipmentStatus> statuses = new ArrayList<ShipmentStatus>();

    public UUID getIdShipment() {
        return this.idShipment;
    }

    public void setIdShipment(UUID id) {
        this.idShipment = id;
    }

    public String getShipmentNumber() {
        return shipmentNumber;
    }

    public Shipment shipmentNumber(String shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
        return this;
    }

    public void setShipmentNumber(String shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
    }

    public ZonedDateTime getDateCreateNumber() {
        return dateCreateNumber;
    }

    public void setDateCreateNumber(ZonedDateTime dateCreateNumber) {
        this.dateCreateNumber = dateCreateNumber;
    }

    public String getDescription() {
        return description;
    }

    public Shipment description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getDateSchedulle() {
        return dateSchedulle;
    }

    public Shipment dateSchedulle(ZonedDateTime dateSchedulle) {
        this.dateSchedulle = dateSchedulle;
        return this;
    }

    public void setDateSchedulle(ZonedDateTime dateSchedulle) {
        this.dateSchedulle = dateSchedulle;
    }

    public String getReason() {
        return reason;
    }

    public Shipment reason(String reason) {
        this.reason = reason;
        return this;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Set<ShipmentItem> getDetails() {
        return details;
    }

    public Shipment details(Set<ShipmentItem> shipmentItems) {
        this.details = shipmentItems;
        return this;
    }

    public Shipment addDetail(ShipmentItem shipmentItem) {
        this.details.add(shipmentItem);
        shipmentItem.setShipment(this);
        return this;
    }

    public Shipment removeDetail(ShipmentItem shipmentItem) {
        this.details.remove(shipmentItem);
        shipmentItem.setShipment(null);
        return this;
    }

    public void setDetails(Set<ShipmentItem> shipmentItems) {
        this.details = shipmentItems;
    }

    public ShipmentType getShipmentType() {
        return shipmentType;
    }

    public Shipment shipmentType(ShipmentType shipmentType) {
        this.shipmentType = shipmentType;
        return this;
    }

    public void setShipmentType(ShipmentType shipmentType) {
        this.shipmentType = shipmentType;
    }

    public ShipTo getShipFrom() {
        return shipFrom;
    }

    public Shipment shipFrom(ShipTo shipTo) {
        this.shipFrom = shipTo;
        return this;
    }

    public void setShipFrom(ShipTo shipTo) {
        this.shipFrom = shipTo;
    }

    public ShipTo getShipTo() {
        return shipTo;
    }

    public Shipment shipTo(ShipTo shipTo) {
        this.shipTo = shipTo;
        return this;
    }

    public void setShipTo(ShipTo shipTo) {
        this.shipTo = shipTo;
    }

    public PostalAddress getAddressFrom() {
        return this.addressFrom != null? this.addressFrom : null ;
    }

    public Shipment addressFrom(PostalAddress postalAddress) {
        this.addressFrom = postalAddress;
        return this;
    }

    public void setAddressFrom(PostalAddress postalAddress) {
        this.addressFrom = postalAddress;
    }

    public PostalAddress getAddressTo() {
        return addressTo;
    }

    public Shipment addressTo(PostalAddress postalAddress) {
        this.addressTo = postalAddress;
        return this;
    }

    public Internal getInternal() {
        return internal;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public void setAddressTo(PostalAddress postalAddress) {
        this.addressTo = postalAddress;
    }

    public List<ShipmentStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<ShipmentStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);

        for (ShipmentStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now();

        for (ShipmentStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        ShipmentStatus current = new ShipmentStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Shipment shipment = (Shipment) o;
        if (shipment.idShipment == null || this.idShipment == null) {
            return false;
        }
        return Objects.equals(this.idShipment, shipment.idShipment);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idShipment);
    }

    @Override
    public String toString() {
        return "Shipment{" +
            "idShipment=" + this.idShipment +
            ", shipmentNumber='" + getShipmentNumber() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateSchedulle='" + getDateSchedulle() + "'" +
            ", reason='" + getReason() + "'" +
            '}';
    }
}
