package id.atiila.domain;

import id.atiila.base.BaseConstants;
import id.atiila.base.ContextHelper;
import id.atiila.security.SecurityUtils;
import id.atiila.service.PositionUtils;
import id.atiila.service.UserMediatorService;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class RequestListener {

    @PrePersist
    public void prePersist(Request e) {
        UserMediatorService um = ContextHelper.getBean("userMediatorService", UserMediatorService.class);
        PositionUtils positionUtils = ContextHelper.getBean("positionUtils", PositionUtils.class);

        String user = SecurityUtils.getCurrentUserLogin().get();
        e.setPartyRole(user, BaseConstants.ROLE_CREATOR);
    }

    @PreUpdate
    public void preUpdate(Request e) {
    }
}
