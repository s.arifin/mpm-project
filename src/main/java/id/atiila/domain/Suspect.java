package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.time.ZonedDateTime;
import java.util.UUID;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;

/**
 * BeSmart Team
 * Class definition for Entity Suspect.
 */

@Entity
@Table(name = "suspect")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "suspect")
public class Suspect implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idsuspect", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idSuspect;

    @Column(name = "suspectnumber")
    private String suspectNumber;

    @Column(name = "salesdt")
    private ZonedDateTime salesDate;

    @Column(name = "marketname")
    private String marketName;

    @Column(name = "repurchaseprob")
    private Float repurchaseProbability;

    @Column(name = "priority")
    private Float priority;

    @ManyToOne
    @JoinColumn(name="idsuspecttyp", referencedColumnName="idsuspecttyp")
    private SuspectType suspectType;

    @ManyToOne
    @JoinColumn(name="iddealer", referencedColumnName="idinternal")
    private Internal dealer;

    @ManyToOne
    @JoinColumn(name="idsalescoordinator", referencedColumnName="idparrol")
    private Salesman salesCoordinator;

    @ManyToOne
    @JoinColumn(name="idsalesman", referencedColumnName="idparrol")
    private Salesman salesman;

    @ManyToOne
    @JoinColumn(name="idsaletype", referencedColumnName="idsaletype")
    private SaleType saleType;

    public UUID getIdSuspect() {
        return this.idSuspect;
    }

    public void setIdSuspect(UUID id) {
        this.idSuspect = id;
    }

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<SuspectStatus> statuses = new ArrayList<SuspectStatus>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<SuspectRole> roles = new ArrayList<SuspectRole>();

    public String getSuspectNumber() {
        return suspectNumber;
    }

    public Suspect suspectNumber(String suspectNumber) {
        this.suspectNumber = suspectNumber;
        return this;
    }

    public void setSuspectNumber(String suspectNumber) {
        this.suspectNumber = suspectNumber;
    }

    public ZonedDateTime getSalesDate() { return salesDate; }

    public void setSalesDate(ZonedDateTime salesDate) { this.salesDate = salesDate; }

    public String getMarketName() { return marketName; }

    public void setMarketName(String marketName) { this.marketName = marketName; }

    public Float getRepurchaseProbability() { return repurchaseProbability; }

    public void setRepurchaseProbability(Float repurchaseProbability) { this.repurchaseProbability = repurchaseProbability; }

    public Float getPriority() { return priority; }

    public void setPriority(Float priority) { this.priority = priority; }

    public SuspectType getSuspectType() { return suspectType; }

    public Suspect suspectType(SuspectType suspectType) {
        this.suspectType = suspectType;
        return this;
    }

    public void setSuspectType(SuspectType suspectType) { this.suspectType = suspectType; }

    public Internal getDealer() {
        return dealer;
    }

    public Suspect dealer(Internal internal) {
        this.dealer = internal;
        return this;
    }

    public void setDealer(Internal internal) {
        this.dealer = internal;
    }

    public Salesman getSalesCoordinator() {
        return salesCoordinator;
    }

    public Suspect salesCoordinator(Salesman salesman) {
        this.salesCoordinator = salesman;
        return this;
    }

    public void setSalesCoordinator(Salesman salesman) {
        this.salesCoordinator = salesman;
    }

    public Salesman getSalesman() {
        return salesman;
    }

    public Suspect salesman(Salesman salesman) {
        this.salesman = salesman;
        return this;
    }

    public void setSalesman(Salesman salesman) {
        this.salesman = salesman;
    }

    public SaleType getSaleType() { return saleType; }

    public void setSaleType(SaleType saleType) { this.saleType = saleType; }

    public List<SuspectRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<SuspectRole> roles) {
        this.roles = roles;
    }

    public void setPartyRole(Party party, Integer role)  {
        for (SuspectRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) return;
        }

        SuspectRole current = new SuspectRole();
        current.setOwner(this);
        current.setIdRoleType(role);
		current.setParty(party);
        current.setDateFrom(LocalDateTime.now());
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        roles.add(current);
    }

    public Party getPartyRole(Party party, Integer role)  {
        for (SuspectRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) return current.getParty();
        }
        return null;
    }

    public void removePartyRole(Party party, Integer role)  {
        for (SuspectRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) {
				current.setDateThru(LocalDateTime.now());
				return;
			}
        }
    }

    public List<SuspectStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<SuspectStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now().plusNanos(1);
        
        for (SuspectStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now();

        for (SuspectStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        SuspectStatus current = new SuspectStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public Boolean getActiveData() {
        switch (getCurrentStatus()) {
            case BaseConstants.STATUS_DRAFT:
            case BaseConstants.STATUS_ACTIVE:
                return true;
        }
        return false;
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Suspect suspect = (Suspect) o;
        if (suspect.idSuspect == null || this.idSuspect == null) {
            return false;
        }
        return Objects.equals(this.idSuspect, suspect.idSuspect);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idSuspect);
    }

    @Override
    public String toString() {
        return "Suspect{" +
            "idSuspect=" + this.idSuspect +
            ", suspectNumber='" + getSuspectNumber() + "'" +
            '}';
    }
}
