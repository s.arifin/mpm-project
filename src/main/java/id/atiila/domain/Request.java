package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.DomainEntity;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.*;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;

/**
 * Be Smart Team
 * Class definition for Entity Request.
 */

@Entity
@Table(name = "request")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "request")
@EntityListeners({RequestListener.class})
public class Request implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idrequest", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idRequest;

    @Column(name = "reqnum")
    private String requestNumber;

    @Column(name = "dtcreate")
    @CreationTimestamp
    private ZonedDateTime dateCreate;

    @Column(name = "dtrequest")
    private ZonedDateTime dateRequest;

    @Column(name = "description")
    private String description;

    @Column(name = "idinternal")
    private String idInternal;

    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @OneToMany(mappedBy = "request")
    private Set<RequestItem> details = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="idreqtype", referencedColumnName="idreqtype")
    private RequestType requestType;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal", insertable = false, updatable = false)
    private Internal internal;

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<RequestStatus> statuses = new ArrayList<RequestStatus>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<RequestRole> roles = new ArrayList<RequestRole>();

    public UUID getIdRequest() {
        return this.idRequest;
    }

    public void setIdRequest(UUID id) {
        this.idRequest = id;
    }

    public String getIdInternal() {
        return idInternal;
    }

    public void setIdInternal(String idInternal) {
        this.idInternal = idInternal;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public Request requestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
        return this;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public Request dateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
        return this;
    }

    public ZonedDateTime getDateRequest() {
        return dateRequest;
    }

    public void setDateRequest(ZonedDateTime dateRequest) {
        this.dateRequest = dateRequest;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDescription() {
        return description;
    }

    public Request description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<RequestItem> getDetails() {
        return details;
    }

    public Request details(Set<RequestItem> requestItems) {
        this.details = requestItems;
        return this;
    }

    public Request addDetail(RequestItem requestItem) {
        this.details.add(requestItem);
        requestItem.setRequest(this);
        return this;
    }

    public Request removeDetail(RequestItem requestItem) {
        this.details.remove(requestItem);
        requestItem.setRequest(null);
        return this;
    }

    public void setDetails(Set<RequestItem> requestItems) {
        this.details = requestItems;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public Request requestType(RequestType requestType) {
        this.requestType = requestType;
        return this;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public Internal getInternal() {
        return internal;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public List<RequestRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<RequestRole> roles) {
        this.roles = roles;
    }

    public void setPartyRole(String user, Integer role)  {
        for (RequestRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) return;
        }

        RequestRole current = new RequestRole();
        current.setOwner(this);
        current.setIdRoleType(role);
		current.setUser(user);
        current.setDateFrom(ZonedDateTime.now());
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        roles.add(current);
    }

    public String getPartyRole(String user, Integer role)  {
        for (RequestRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) return current.getUser();
        }
        return null;
    }

    public void removePartyRole(String user, Integer role)  {
        for (RequestRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) {
				current.setDateThru(ZonedDateTime.now());
				return;
			}
        }
    }

    public List<RequestStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<RequestStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);

        for (RequestStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now();

        for (RequestStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        RequestStatus current = new RequestStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    @PrePersist
    public void prePersist() {
        if (getDateCreate() == null) setDateCreate(ZonedDateTime.now());
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @PreUpdate
    public void preUpdate() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Request request = (Request) o;
        if (request.idRequest == null || this.idRequest == null) {
            return false;
        }
        return Objects.equals(this.idRequest, request.idRequest);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idRequest);
    }

    @Override
    public String toString() {
        return "Request{" +
            "idRequest=" + this.idRequest +
            ", requestNumber='" + getRequestNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
