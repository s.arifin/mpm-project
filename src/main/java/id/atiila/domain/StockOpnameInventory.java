package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity StockOpnameInventory.
 */

@Entity
@Table(name = "stock_opname_inventory")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "stockopnameinventory")
public class StockOpnameInventory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idstkopninv", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idStockOpnameInventory;

    @Column(name = "qty")
    private Double qty;

    @ManyToOne
    @JoinColumn(name="idinvite", referencedColumnName="idinvite")
    private InventoryItem inventoryItem;

    @ManyToOne
    @JoinColumn(name="idstopnite", referencedColumnName="idstopnite")
    private StockOpnameItem item;

    public UUID getIdStockOpnameInventory() {
        return this.idStockOpnameInventory;
    }

    public void setIdStockOpnameInventory(UUID id) {
        this.idStockOpnameInventory = id;
    }

    public Double getQty() {
        return qty;
    }

    public StockOpnameInventory qty(Double qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public InventoryItem getInventoryItem() {
        return inventoryItem;
    }

    public StockOpnameInventory inventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
        return this;
    }

    public void setInventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
    }

    public StockOpnameItem getItem() {
        return item;
    }

    public StockOpnameInventory item(StockOpnameItem stockOpnameItem) {
        this.item = stockOpnameItem;
        return this;
    }

    public void setItem(StockOpnameItem stockOpnameItem) {
        this.item = stockOpnameItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StockOpnameInventory stockOpnameInventory = (StockOpnameInventory) o;
        if (stockOpnameInventory.idStockOpnameInventory == null || this.idStockOpnameInventory == null) {
            return false;
        }
        return Objects.equals(this.idStockOpnameInventory, stockOpnameInventory.idStockOpnameInventory);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idStockOpnameInventory);
    }

    @Override
    public String toString() {
        return "StockOpnameInventory{" +
            "idStockOpnameInventory=" + this.idStockOpnameInventory +
            ", qty='" + getQty() + "'" +
            '}';
    }
}
