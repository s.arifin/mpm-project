package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * Be Smart Team
 * Class definition for Entity RequestUnitInternal.
 */

@Entity
@Table(name = "request_unit_internal")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "requestunitinternal")
public class RequestUnitInternal extends Request {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name="idparent", referencedColumnName="idrequest")
    private RequestUnitInternal parent;

    @ManyToOne
    @JoinColumn(name="idinternalto", referencedColumnName="idinternal")
    private Internal internalTo;

    public RequestUnitInternal getParent() {
        return parent;
    }

    public void setParent(RequestUnitInternal parent) {
        this.parent = parent;
    }

    public Internal getInternalTo() {
        return internalTo;
    }

    public RequestUnitInternal internalTo(Internal internal) {
        this.internalTo = internal;
        return this;
    }

    public void setInternalTo(Internal internal) {
        this.internalTo = internal;
    }

    @Override
    public String toString() {
        return "RequestUnitInternal{" +
            "idRequest=" + this.getIdRequest() +
            '}';
    }
}
