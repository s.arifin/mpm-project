package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.DomainEntity;
import id.atiila.domain.listener.ShipmentReceiptListener;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity ShipmentReceipt.
 */

@Entity
@Table(name = "shipment_receipt")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "shipmentreceipt")
//@EntityListeners(ShipmentReceiptListener.class)
public class ShipmentReceipt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idreceipt", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idReceipt;

    @Column(name = "idproduct")
    private String idProduct;

    @Column(name = "idfeature")
    private Integer idFeature;

    @Column(name = "code")
    private String receiptCode;

    @Column(name = "qtyaccept")
    private Double qtyAccept;

    @Column(name = "qtyreject")
    private Double qtyReject;

    @Column(name = "itemdescription")
    private String itemDescription;

    @Column(name = "dtreceipt")
    private ZonedDateTime dateReceipt;

    @ManyToOne
    @JoinColumn(name="idfeature", referencedColumnName="idfeature", insertable = false, updatable = false)
    private Feature feature;

    @ManyToOne
    @JoinColumn(name="idpackage", referencedColumnName="idpackage")
    private ShipmentPackage shipmentPackage;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinColumn(name = "idshiite", referencedColumnName="idshiite")
    private ShipmentItem shipmentItem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="idordite", referencedColumnName="idordite")
    private OrderItem orderItem;

    @OneToMany(mappedBy = "shipmentReceipt", cascade = {CascadeType.ALL})
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<InventoryItem> inventoryItems = new HashSet<>();

    public UUID getIdReceipt() {
        return this.idReceipt;
    }

    public void setIdReceipt(UUID id) {
        this.idReceipt = id;
    }

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<ShipmentReceiptStatus> statuses = new ArrayList<ShipmentReceiptStatus>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<ShipmentReceiptRole> roles = new ArrayList<ShipmentReceiptRole>();

    public String getIdProduct() {
        return idProduct;
    }

    public ShipmentReceipt idProduct(String idProduct) {
        this.idProduct = idProduct;
        return this;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdFeature() {
        return idFeature;
    }

    public ShipmentReceipt idFeature(Integer idFeature) {
        this.idFeature = idFeature;
        return this;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public String getReceiptCode() {
        return receiptCode;
    }

    public ShipmentReceipt receiptCode(String receiptCode) {
        this.receiptCode = receiptCode;
        return this;
    }

    public void setReceiptCode(String receiptCode) {
        this.receiptCode = receiptCode;
    }

    public Double getQtyAccept() {
        return qtyAccept;
    }

    public ShipmentReceipt qtyAccept(Double qtyAccept) {
        this.qtyAccept = qtyAccept;
        return this;
    }

    public void setQtyAccept(Double qtyAccept) {
        this.qtyAccept = qtyAccept;
    }

    public void setQtyAccept(Integer qtyAccept) {
        this.qtyAccept = Double.valueOf(qtyAccept);
    }

    public Double getQtyReject() {
        return qtyReject;
    }

    public ShipmentReceipt qtyReject(Double qtyReject) {
        this.qtyReject = qtyReject;
        return this;
    }

    public void setQtyReject(Double qtyReject) {
        this.qtyReject = qtyReject;
    }

    public void setQtyReject(Integer qtyReject) {
        this.qtyReject = Double.valueOf(qtyReject);
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public ShipmentReceipt itemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
        return this;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public ZonedDateTime getDateReceipt() {
        return dateReceipt;
    }

    public ShipmentReceipt dateReceipt(ZonedDateTime dateReceipt) {
        this.dateReceipt = dateReceipt;
        return this;
    }

    public void setDateReceipt(ZonedDateTime dateReceipt) {
        this.dateReceipt = dateReceipt;
    }

    public Feature getFeature() { return feature; }

    public void setFeature(Feature feature) { this.feature = feature; }

    public ShipmentPackage getShipmentPackage() {
        return shipmentPackage;
    }

    public ShipmentReceipt shipmentPackage(ShipmentPackage shipmentPackage) {
        this.shipmentPackage = shipmentPackage;
        return this;
    }

    public void setShipmentPackage(ShipmentPackage shipmentPackage) {
        this.shipmentPackage = shipmentPackage;
    }

    public ShipmentItem getShipmentItem() {
        return shipmentItem;
    }

    public ShipmentReceipt shipmentItem(ShipmentItem shipmentItem) {
        this.shipmentItem = shipmentItem;
        return this;
    }

    public void setShipmentItem(ShipmentItem shipmentItem) {
        this.shipmentItem = shipmentItem;
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public ShipmentReceipt orderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
        return this;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    public Set<InventoryItem> getInventoryItems() {
        return inventoryItems;
    }

    public ShipmentReceipt inventoryItems(Set<InventoryItem> inventoryItems) {
        this.inventoryItems = inventoryItems;
        return this;
    }

    public ShipmentReceipt addInventoryItem(InventoryItem inventoryItem) {
        this.inventoryItems.add(inventoryItem);
        inventoryItem.setShipmentReceipt(this);
        return this;
    }

    public ShipmentReceipt removeInventoryItem(InventoryItem inventoryItem) {
        this.inventoryItems.remove(inventoryItem);
        inventoryItem.setShipmentReceipt(null);
        return this;
    }

    public void setInventoryItems(Set<InventoryItem> inventoryItems) {
        this.inventoryItems = inventoryItems;
    }

    public List<ShipmentReceiptRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<ShipmentReceiptRole> roles) {
        this.roles = roles;
    }

    public void setPartyRole(String user, Integer role)  {
        for (ShipmentReceiptRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) return;
        }

        ShipmentReceiptRole current = new ShipmentReceiptRole();
        current.setOwner(this);
        current.setIdRoleType(role);
		current.setUser(user);
        current.setDateFrom(ZonedDateTime.now());
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        roles.add(current);
    }

    public String getPartyRole(String user, Integer role)  {
        for (ShipmentReceiptRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) return current.getUser();
        }
        return null;
    }

    public void removePartyRole(String user, Integer role)  {
        for (ShipmentReceiptRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) {
				current.setDateThru(ZonedDateTime.now());
				return;
			}
        }
    }

    public List<ShipmentReceiptStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<ShipmentReceiptStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);

        for (ShipmentReceiptStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now();

        for (ShipmentReceiptStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        ShipmentReceiptStatus current = new ShipmentReceiptStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShipmentReceipt shipmentReceipt = (ShipmentReceipt) o;
        if (shipmentReceipt.idReceipt == null || this.idReceipt == null) {
            return false;
        }
        return Objects.equals(this.idReceipt, shipmentReceipt.idReceipt);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idReceipt);
    }

    @Override
    public String toString() {
        return "ShipmentReceipt{" +
            "idReceipt=" + this.idReceipt +
            ", idProduct='" + getIdProduct() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", receiptCode='" + getReceiptCode() + "'" +
            ", qtyAccept='" + getQtyAccept() + "'" +
            ", qtyReject='" + getQtyReject() + "'" +
            ", itemDescription='" + getItemDescription() + "'" +
            ", dateReceipt='" + getDateReceipt() + "'" +
            '}';
    }
}
