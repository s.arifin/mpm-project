package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.hibernate.annotations.Formula;
import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;import java.util.UUID;

import java.util.Objects;



/**
 * atiila consulting
 * Class definition for Entity VehicleCustomerRequest.
 */

@Entity
@Table(name = "vehicle_customer_request")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "vehiclecustomerrequest")
public class VehicleCustomerRequest extends Request {

    private static final long serialVersionUID = 1L;

    @Column(name = "customerorder")
    private String customerOrder;

    @Formula("(select sum(coalesce(cri.qty, 0) - coalesce(ori.qtydelivered, 0)) from request_item ri " +
             "   join customer_request_item cri on (ri.idreqitem = cri.idreqitem) " +
             "   left join order_request_item ori on (ori.idreqitem = ri.idreqitem) " +
             "  where (ri.idrequest = idrequest))")
    private Double qtyToShipment;

    @ManyToOne
    @JoinColumn(name="idcustomer", referencedColumnName="idcustomer")
    private OrganizationCustomer customer;

    @ManyToOne
    @JoinColumn(name="idsalesbroker", referencedColumnName="idparrol")
    private SalesBroker salesBroker;

    @Column (name = "idsalesman")
    private UUID idSalesman;

    public UUID getIdSalesman() {
        return idSalesman;
    }

    public void setIdSalesman(UUID idSalesman) {
        this.idSalesman = idSalesman;
    }

    public String getCustomerOrder() {
        return customerOrder;
    }

    public VehicleCustomerRequest customerOrder(String customerOrder) {
        this.customerOrder = customerOrder;
        return this;
    }

    public void setCustomerOrder(String customerOrder) {
        this.customerOrder = customerOrder;
    }

    public OrganizationCustomer getCustomer() {
        return customer;
    }

    public VehicleCustomerRequest customer(OrganizationCustomer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(OrganizationCustomer customer) {
        this.customer = customer;
    }

    public SalesBroker getSalesBroker() {
        return salesBroker;
    }

    public void setSalesBroker(SalesBroker salesBroker) {
        this.salesBroker = salesBroker;
    }

    public Double getQtyToShipment() {
        return this.qtyToShipment == null ? 0d : this.qtyToShipment;
    }

    public void setQtyToShipment(Double qtyToShipment) {
        this.qtyToShipment = qtyToShipment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VehicleCustomerRequest vehicleCustomerRequest = (VehicleCustomerRequest) o;
        if (vehicleCustomerRequest.getIdRequest() == null || this.getIdRequest() == null) {
            return false;
        }
        return Objects.equals(this.getIdRequest(), vehicleCustomerRequest.getIdRequest());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdRequest());
    }

    @Override
    public String toString() {
        return "VehicleCustomerRequest{" +
            "idRequest=" + this.getIdRequest() +
            ", requestNumber='" + getRequestNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", dateRequest='" + getDateRequest() + "'" +
            ", description='" + getDescription() + "'" +
            ", customerOrder='" + getCustomerOrder() + "'" +
            '}';
    }
}
