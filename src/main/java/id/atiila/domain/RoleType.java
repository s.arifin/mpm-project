package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity RoleType.
 */

@Entity
@Table(name = "role_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "roletype")
public class RoleType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idroletype")
    private Integer idRoleType;

    @Column(name = "description")
    private String description;

    public RoleType() {
    }

    public RoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
    }

    public Integer getIdRoleType() {
        return this.idRoleType;
    }

    public void setIdRoleType(Integer id) {
        this.idRoleType = id;
    }

    public String getDescription() {
        return description;
    }

    public RoleType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RoleType roleType = (RoleType) o;
        if (roleType.idRoleType == null || this.idRoleType == null) {
            return false;
        }
        return Objects.equals(this.idRoleType, roleType.idRoleType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idRoleType);
    }

    @Override
    public String toString() {
        return "RoleType{" +
            "idRoleType=" + this.idRoleType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
