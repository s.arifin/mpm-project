package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity SuspectType.
 */

@Entity
@Table(name = "suspect_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "suspecttype")
public class SuspectType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idsuspecttyp")
    private Integer idSuspectType;

    @Column(name = "description")
    private String description;

    public Integer getIdSuspectType() {
        return this.idSuspectType;
    }

    public void setIdSuspectType(Integer id) {
        this.idSuspectType = id;
    }

    public String getDescription() {
        return description;
    }

    public SuspectType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SuspectType suspectType = (SuspectType) o;
        if (suspectType.idSuspectType == null || this.idSuspectType == null) {
            return false;
        }
        return Objects.equals(this.idSuspectType, suspectType.idSuspectType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idSuspectType);
    }

    @Override
    public String toString() {
        return "SuspectType{" +
            "idSuspectType=" + this.idSuspectType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
