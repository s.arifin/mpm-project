package id.atiila.domain;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.WhereJoinTable;
import org.springframework.data.elasticsearch.annotations.Document;

import id.atiila.base.BaseConstants;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * BeSmart Team
 * Class definition for Entity Organization.
 */

@Entity
@Table(name = "organization")
@DiscriminatorValue("2")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "organization")
public class Organization extends Party {

    private static final long serialVersionUID = 1L;

    @Column(name = "numbertdp")
    private String numberTDP;

    @Column(name = "dtestablised")
    private ZonedDateTime dateOrganizationEstablish;

    @Column(name = "name")
    private String name;

    @Column(name = "officemail")
    private String officeMail;

    @Column(name = "officephone")
    private String officePhone;

    @Column(name = "officefax")
    private String officeFax;

    @Column(name = "idpic")
    private UUID idPic;

    @Column(name = "npwp")
    private String npwp;

    @Column(name = "picname")
    private String picName;

    @Column(name = "picphone")
    private String picPhone;

    @Column(name = "picmail")
    private String picMail;

    public String getPicName() {
        return picName;
    }

    public void setPicName(String picName) {
        this.picName = picName;
    }

    public String getPicPhone() {
        return picPhone;
    }

    public void setPicPhone(String picPhone) {
        this.picPhone = picPhone;
    }

    public String getPicMail() {
        return picMail;
    }

    public void setPicMail(String picMail) {
        this.picMail = picMail;
    }

    public String getName() {
        return name;
    }

    public Organization name(String name) {
        this.name = name;
        return this;
    }

    public String getNumberTDP() {
        return numberTDP;
    }

    public void setNumberTDP(String numberTDP) {
        this.numberTDP = numberTDP;
    }

    public  ZonedDateTime getDateOrganizationEstablish() {
        return dateOrganizationEstablish;
    }

    public void setDateOrganizationEstablish( ZonedDateTime dateOrganizationEstablish) {
        this.dateOrganizationEstablish = dateOrganizationEstablish;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOfficeMail() {
        return officeMail;
    }

    public void setOfficeMail(String officeMail) {
        this.officeMail = officeMail;
    }

    public String getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    public String getOfficeFax() {
        return officeFax;
    }

    public void setOfficeFax(String officeFax) {
        this.officeFax = officeFax;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public UUID getIdPic() {
        return idPic;
    }

    public void setIdPic(UUID idPic) {
        this.idPic = idPic;
    }

    @Override
    public void preUpdate() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Organization party = (Organization) o;
        if (party.getIdParty() == null || this.getIdParty() == null) {
            return false;
        }
        return Objects.equals(this.getIdParty(), party.getIdParty());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdParty());
    }

    @Override
    public String toString() {
        return "Organization{" +
            "idParty=" + this.getIdParty() +
            ", name='" + getName() + "'" +
            '}';
    }
}
