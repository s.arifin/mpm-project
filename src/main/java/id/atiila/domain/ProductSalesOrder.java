package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.*;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity ProductSalesOrder.
 */

@Entity
@Table(name = "regular_sales_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "productsalesorder")
public class ProductSalesOrder extends SalesOrder {

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProductSalesOrder productSalesOrder = (ProductSalesOrder) o;
        if (productSalesOrder.getIdOrder() == null || this.getIdOrder() == null) {
            return false;
        }
        return Objects.equals(this.getIdOrder(), productSalesOrder.getIdOrder());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdOrder());
    }

    @Override
    public String toString() {
        return "ProductSalesOrder{" +
            "idOrder=" + this.getIdOrder() +
            '}';
    }
}
