package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity FeatureType.
 */

@Entity
@Table(name = "feature_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "featuretype")
public class FeatureType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idfeatyp")
    private Integer idFeatureType;

    @Column(name = "description")
    private String description;

    @Column(name = "refkey")
    private String refKey;

    public Integer getIdFeatureType() {
        return this.idFeatureType;
    }

    public void setIdFeatureType(Integer id) {
        this.idFeatureType = id;
    }

    public String getDescription() {
        return description;
    }

    public FeatureType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefKey() {
        return refKey;
    }

    public FeatureType refKey(String refKey) {
        this.refKey = refKey;
        return this;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FeatureType featureType = (FeatureType) o;
        if (featureType.idFeatureType == null || this.idFeatureType == null) {
            return false;
        }
        return Objects.equals(this.idFeatureType, featureType.idFeatureType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idFeatureType);
    }

    @Override
    public String toString() {
        return "FeatureType{" +
            "idFeatureType=" + this.idFeatureType +
            ", description='" + getDescription() + "'" +
            ", refKey='" + getRefKey() + "'" +
            '}';
    }
}
