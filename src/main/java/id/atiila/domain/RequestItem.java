package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;


/**
 * BeSmart Team
 * Class definition for Entity RequestItem.
 */

@Entity
@Table(name = "request_item")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "requestitem")
public class RequestItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idreqitem", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idRequestItem;

    @Column(name = "seq")
    private Integer sequence;

    @ManyToOne
    @JoinColumn(name="idproduct", referencedColumnName = "idproduct")
    private Product product;

    @Column(name = "description")
    private String description;

    @Column(name = "qtyreq")
    private Double qtyReq;

    @Column(name = "qtytransfer")
    private Double qtyTransfer;

    @ManyToOne
    @JoinColumn(name="idrequest", referencedColumnName="idrequest")
    private Request request;

    @ManyToOne
    @JoinColumn(name="idfeature", referencedColumnName="idfeature")
    private Feature feature;

    public UUID getIdRequestItem() {
        return this.idRequestItem;
    }

    public void setIdRequestItem(UUID id) {
        this.idRequestItem = id;
    }

    public Integer getSequence() {
        return sequence;
    }

    public RequestItem sequence(Integer sequence) {
        this.sequence = sequence;
        return this;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Product getProduct() {
        return product;
    }

    public RequestItem product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getDescription() {
        return description;
    }

    public RequestItem description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getQtyReq() {
        return qtyReq;
    }

    public void setQtyReq(Double qtyReq) {
        this.qtyReq = qtyReq;
    }

    public Double getQtyTransfer() {
        return qtyTransfer;
    }

    public void setQtyTransfer(Double qtyTransfer) {
        this.qtyTransfer = qtyTransfer;
    }

    public Request getRequest() {
        return request;
    }

    public RequestItem request(Request request) {
        this.request = request;
        return this;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    @PrePersist
    public void prePersist() {
        if (this.qtyReq == null) qtyReq = 1d;
    }

    @PreUpdate
    public void preUpdate() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RequestItem requestItem = (RequestItem) o;
        if (requestItem.idRequestItem == null || this.idRequestItem == null) {
            return false;
        }
        return Objects.equals(this.idRequestItem, requestItem.idRequestItem);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idRequestItem);
    }

    @Override
    public String toString() {
        return "RequestItem{" +
            "idRequestItem=" + this.idRequestItem +
            ", sequence='" + getSequence() + "'" +
            ", product='" + getProduct() + "'" +
            ", description='" + getDescription() + "'" +
            ", qtyReq='" + getQtyReq() + "'" +
            ", qtyTransfer='" + getQtyTransfer() + "'" +
            '}';
    }
}
