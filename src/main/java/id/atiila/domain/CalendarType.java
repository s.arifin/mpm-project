package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity CalendarType.
 */

@Entity
@Table(name = "calendar_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "calendartype")
public class CalendarType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idcaltyp")
    private Integer idCalendarType;

    @Column(name = "description")
    private String description;

    public CalendarType() {
    }

    public CalendarType(Integer idCalendarType) {
        this.idCalendarType = idCalendarType;
    }

    public CalendarType(Integer id, String description) {
        this.idCalendarType = id;
        this.description = description;
    }

    public Integer getIdCalendarType() {
        return this.idCalendarType;
    }

    public void setIdCalendarType(Integer id) {
        this.idCalendarType = id;
    }

    public String getDescription() {
        return description;
    }

    public CalendarType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CalendarType calendarType = (CalendarType) o;
        if (calendarType.idCalendarType == null || this.idCalendarType == null) {
            return false;
        }
        return Objects.equals(this.idCalendarType, calendarType.idCalendarType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idCalendarType);
    }

    @Override
    public String toString() {
        return "CalendarType{" +
            "idCalendarType=" + this.idCalendarType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
