package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity ProductCategory.
 */

@Entity
@Table(name = "product_category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "productcategory")
public class ProductCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcategory")
    private Integer idCategory;

    @Column(name = "description")
    private String description;

    @Column(name = "refkey")
    private String refKey;

    @ManyToOne
    @JoinColumn(name="idcattyp", referencedColumnName="idcattyp")
    private ProductCategoryType categoryType;

    public Integer getIdCategory() {
        return this.idCategory;
    }

    public void setIdCategory(Integer id) {
        this.idCategory = id;
    }

    public String getDescription() {
        return description;
    }

    public ProductCategory description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefKey() {
        return refKey;
    }

    public ProductCategory refKey(String refKey) {
        this.refKey = refKey;
        return this;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public ProductCategoryType getCategoryType() {
        return categoryType;
    }

    public ProductCategory categoryType(ProductCategoryType productCategoryType) {
        this.categoryType = productCategoryType;
        return this;
    }

    public void setCategoryType(ProductCategoryType productCategoryType) {
        this.categoryType = productCategoryType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProductCategory productCategory = (ProductCategory) o;
        if (productCategory.idCategory == null || this.idCategory == null) {
            return false;
        }
        return Objects.equals(this.idCategory, productCategory.idCategory);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idCategory);
    }

    @Override
    public String toString() {
        return "ProductCategory{" +
            "idCategory=" + this.idCategory +
            ", description='" + getDescription() + "'" +
            ", refKey='" + getRefKey() + "'" +
            '}';
    }
}
