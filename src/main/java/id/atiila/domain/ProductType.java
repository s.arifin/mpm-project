package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity ProductType.
 */

@Entity
@Table(name = "product_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "producttype")
public class ProductType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idprotyp")
    private Integer idProductType;

    @Column(name = "description")
    private String description;

    public Integer getIdProductType() {
        return this.idProductType;
    }

    public void setIdProductType(Integer id) {
        this.idProductType = id;
    }

    public String getDescription() {
        return description;
    }

    public ProductType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProductType productType = (ProductType) o;
        if (productType.idProductType == null || this.idProductType == null) {
            return false;
        }
        return Objects.equals(this.idProductType, productType.idProductType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idProductType);
    }

    @Override
    public String toString() {
        return "ProductType{" +
            "idProductType=" + this.idProductType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
