package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.*;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity PackageReceipt.
 */

@Entity
@Table(name = "package_receipt")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "packagereceipt")
public class PackageReceipt extends ShipmentPackage {

    private static final long serialVersionUID = 1L;

    @Column(name = "documentnumber")
    private String documentNumber;

    @Column(name = "vendorinvoice")
    private String vendorInvoice;

    @Column(name = "dtissued")
    private ZonedDateTime dateIssued;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idshifro", referencedColumnName="idshipto")
    private ShipTo shipFrom;

    @Column(name = "shiptype")
    private  Integer shipType;

    public Integer getShipType() {
        return shipType;
    }

    public void setShipType(Integer shipType) {
        this.shipType = shipType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public PackageReceipt documentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
        return this;
    }

    public ZonedDateTime getDateIssued() {
        return dateIssued;
    }

    public void setDateIssued(ZonedDateTime dateIssued) {
        this.dateIssued = dateIssued;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getVendorInvoice() {
        return vendorInvoice;
    }

    public PackageReceipt vendorInvoice(String vendorInvoice) {
        this.vendorInvoice = vendorInvoice;
        return this;
    }

    public void setVendorInvoice(String vendorInvoice) {
        this.vendorInvoice = vendorInvoice;
    }

    public Internal getInternal() {
        return internal;
    }

    public PackageReceipt internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public ShipTo getShipFrom() {
        return shipFrom;
    }

    public PackageReceipt shipFrom(ShipTo shipTo) {
        this.shipFrom = shipTo;
        return this;
    }

    public void setShipFrom(ShipTo shipTo) {
        this.shipFrom = shipTo;
    }

    @Override
    public String toString() {
        return "PackageReceipt{" +
            "idPackage=" + this.getIdPackage() +
            ", dateCreated='" + getDateCreated() + "'" +
            ", documentNumber='" + getDocumentNumber() + "'" +
            ", vendorInvoice='" + getVendorInvoice() + "'" +
            '}';
    }
}
