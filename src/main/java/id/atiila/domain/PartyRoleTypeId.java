package id.atiila.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
public class PartyRoleTypeId implements Serializable {

    @Column(name = "idroletype")
    private Integer idRoleType;

    @Column(name = "idparty")
    private UUID idParty;

    public PartyRoleTypeId() {
    }

    public PartyRoleTypeId(Integer idRoleType, UUID idParty) {
        this.idRoleType = idRoleType;
        this.idParty = idParty;
    }

    public Integer getIdRoleType() {
        return idRoleType;
    }

    public void setIdRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
    }

    public UUID getIdParty() {
        return idParty;
    }

    public void setIdParty(UUID idParty) {
        this.idParty = idParty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PartyRoleTypeId that = (PartyRoleTypeId) o;

        return new EqualsBuilder()
            .append(idRoleType, that.idRoleType)
            .append(idParty, that.idParty)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idRoleType)
            .append(idParty)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "PartyRoleTypeId{" +
            "idRoleType=" + idRoleType +
            ", idParty=" + idParty +
            '}';
    }
}
