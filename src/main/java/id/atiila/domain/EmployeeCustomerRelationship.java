package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;import java.util.UUID;

import java.util.Objects;



/**
 * atiila consulting
 * Class definition for Entity EmployeeCustomerRelationship.
 */

@Entity
@Table(name = "customer_relationship")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "employeecustomerrelationship")
public class EmployeeCustomerRelationship extends PartyRelationship {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name="idemployee", referencedColumnName="idemployee")
    private Employee employee;

    @ManyToOne
    @JoinColumn(name="idcustomer", referencedColumnName="idcustomer")
    private Customer customer;

    public Employee getEmployee() {
        return employee;
    }

    public EmployeeCustomerRelationship employee(Employee employee) {
        this.employee = employee;
        return this;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Customer getCustomer() {
        return customer;
    }

    public EmployeeCustomerRelationship customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EmployeeCustomerRelationship employeeCustomerRelationship = (EmployeeCustomerRelationship) o;
        if (employeeCustomerRelationship.getIdPartyRelationship() == null || this.getIdPartyRelationship() == null) {
            return false;
        }
        return Objects.equals(this.getIdPartyRelationship(), employeeCustomerRelationship.getIdPartyRelationship());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdPartyRelationship());
    }

    @Override
    public String toString() {
        return "EmployeeCustomerRelationship{" +
            "idPartyRelationship=" + this.getIdPartyRelationship() +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
