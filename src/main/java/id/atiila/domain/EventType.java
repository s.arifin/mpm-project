package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity EventType.
 */

@Entity
@Table(name = "event_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "eventtype")
public class EventType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idevetyp")
    private Integer idEventType;

    @Column(name = "idprneve")
    private Integer idParentEventType;

    @Column(name = "description")
    private String description;

    public Integer getIdEventType() {
        return this.idEventType;
    }

    public void setIdEventType(Integer id) {
        this.idEventType = id;
    }

    public Integer getIdParentEventType() { return idParentEventType; }

    public EventType idParentEventType (Integer idEventType) {
        this.idEventType = idEventType;
        return this;
    }

    public void setIdParentEventType(Integer idParentEventType) { this.idParentEventType = idParentEventType; }

    public String getDescription() {
        return description;
    }

    public EventType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EventType eventType = (EventType) o;
        if (eventType.idEventType == null || this.idEventType == null) {
            return false;
        }
        return Objects.equals(this.idEventType, eventType.idEventType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idEventType);
    }

    @Override
    public String toString() {
        return "EventType{" +
            "idEventType=" + this.idEventType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
