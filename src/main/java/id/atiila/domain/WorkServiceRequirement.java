package id.atiila.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * BeSmart Team
 * Class definition for Entity WorkServiceRequirement.
 */

@Entity
@Table(name = "work_service_requirement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "workservicerequirement")
public class WorkServiceRequirement extends WorkEffort {

    private static final long serialVersionUID = 1L;

    @ManyToMany(mappedBy = "services")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<WorkRequirement> requirements = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="idbooking", referencedColumnName="idbooking")
    private WorkOrderBooking booking;

    public Set<WorkRequirement> getRequirements() {
        return requirements;
    }

    public WorkServiceRequirement requirements(Set<WorkRequirement> workRequirements) {
        this.requirements = workRequirements;
        return this;
    }

    public WorkServiceRequirement addRequirement(WorkRequirement workRequirement) {
        this.requirements.add(workRequirement);
        workRequirement.getServices().add(this);
        return this;
    }

    public WorkServiceRequirement removeRequirement(WorkRequirement workRequirement) {
        this.requirements.remove(workRequirement);
        workRequirement.getServices().remove(this);
        return this;
    }

    public void setRequirements(Set<WorkRequirement> workRequirements) {
        this.requirements = workRequirements;
    }

    public WorkOrderBooking getBooking() {
        return booking;
    }

    public WorkServiceRequirement booking(WorkOrderBooking workOrderBooking) {
        this.booking = workOrderBooking;
        return this;
    }

    public void setBooking(WorkOrderBooking workOrderBooking) {
        this.booking = workOrderBooking;
    }

    @Override
    public String toString() {
        return "WorkServiceRequirement{" +
            "idWe=" + this.getIdWe() +
            '}';
    }
}
