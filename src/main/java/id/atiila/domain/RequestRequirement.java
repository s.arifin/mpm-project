package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity RequestRequirement.
 */

@Entity
@Table(name = "request_requirement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "requestrequirement")
public class RequestRequirement extends Request {

    private static final long serialVersionUID = 1L;

    @Column(name = "qty")
    private Integer qty;

    @Column(name="idrequirement")
    private UUID idRequirement;

    public Integer getQty() {
        return qty;
    }

    public RequestRequirement qty(Integer qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public UUID getIdRequirement() {
        return idRequirement;
    }

    public RequestRequirement idRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
        return this;
    }

    public void setIdRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RequestRequirement requestRequirement = (RequestRequirement) o;
        if (requestRequirement.getIdRequest() == null || this.getIdRequest() == null) {
            return false;
        }
        return Objects.equals(this.getIdRequest(), requestRequirement.getIdRequest());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdRequest());
    }

    @Override
    public String toString() {
        return "RequestRequirement{" +
            "idRequest=" + this.getIdRequest() +
            ", qty='" + getQty() + "'" +
            '}';
    }
}
