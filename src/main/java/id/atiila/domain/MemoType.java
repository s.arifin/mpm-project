package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity MemoType.
 */

@Entity
@Table(name = "memo_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "memotype")
public class MemoType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmemotype")
    private Integer idMemoType;

    @Column(name = "description")
    private String description;

    public Integer getIdMemoType() {
        return this.idMemoType;
    }

    public void setIdMemoType(Integer id) {
        this.idMemoType = id;
    }

    public String getDescription() {
        return description;
    }

    public MemoType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MemoType memoType = (MemoType) o;
        if (memoType.idMemoType == null || this.idMemoType == null) {
            return false;
        }
        return Objects.equals(this.idMemoType, memoType.idMemoType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idMemoType);
    }

    @Override
    public String toString() {
        return "MemoType{" +
            "idMemoType=" + this.idMemoType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
