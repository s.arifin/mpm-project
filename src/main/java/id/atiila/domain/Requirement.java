package id.atiila.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import javax.persistence.*;

import id.atiila.service.SalesUnitRequirementService;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import id.atiila.base.BaseConstants;
import id.atiila.base.DomainEntity;

/**
 * BeSmart Team
 * Class definition for Entity Requirement.
 */

@Entity
@Table(name = "requirement")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "requirement")
public class Requirement extends AuditingEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idreq", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idRequirement;

    @Column(name = "requirementnumber")
    private String requirementNumber;

    @Column(name = "description")
    private String description;

    @Column(name = "dtcreate")
    private ZonedDateTime dateCreate;

    @Column(name = "dtrequired")
    private ZonedDateTime dateRequired;

    @Column(name = "budget", precision=10, scale=2)
    private BigDecimal budget;

    @Column(name = "qty")
    private Integer qty;

    @ManyToOne
    @JoinColumn(name="idreqtyp", referencedColumnName="idreqtyp")
    private RequirementType requirementType;

//    @Column(name="idreqtyp")
//    private Integer idReqTyp;

//    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
//    @ManyToMany
//    @JoinTable(name = "order_item_requirement",
//        joinColumns = @JoinColumn(name="requirements_id", referencedColumnName="idreq"),
//        inverseJoinColumns = @JoinColumn(name="order_items_id", referencedColumnName="idordite"))
//    private Set<OrderItem> orderItems = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="idfacility", referencedColumnName="idfacility")
    private Facility facility;

    @JsonIgnore
    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Requirement> items = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="idparentreq", referencedColumnName="idreq")
    private Requirement parent;

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy = "owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<RequirementStatus> statuses = new ArrayList<RequirementStatus>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<RequirementRole> roles = new ArrayList<RequirementRole>();

    @JsonIgnore
    @OneToMany
    @JoinColumn(name="idreq", insertable = false, updatable = false)
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<Approval> approvals = new ArrayList<Approval>();

    public UUID getIdRequirement() {
        return this.idRequirement;
    }

    public void setIdRequirement(UUID id) {
        this.idRequirement = id;
    }

    public String getRequirementNumber() {
        return requirementNumber;
    }

    public Requirement requirementNumber(String requirementNumber) {
        this.requirementNumber = requirementNumber;
        return this;
    }

    public void setRequirementNumber(String requirementNumber) {
        this.requirementNumber = requirementNumber;
    }

    public String getDescription() {
        return description;
    }

    public Requirement description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public Requirement dateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
        return this;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public ZonedDateTime getDateRequired() {
        return dateRequired;
    }

    public Requirement dateRequired(ZonedDateTime dateRequired) {
        this.dateRequired = dateRequired;
        return this;
    }

    public void setDateRequired(ZonedDateTime dateRequired) {
        this.dateRequired = dateRequired;
    }

    public BigDecimal getBudget() {
        return budget;
    }

    public Requirement budget(BigDecimal budget) {
        this.budget = budget;
        return this;
    }

    public void setBudget(BigDecimal budget) {
        this.budget = budget;
    }

    public Integer getQty() {
        return qty;
    }

    public Requirement qty(Integer qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public RequirementType getRequirementType() {
        return requirementType;
    }

    public Requirement requirementType(RequirementType requirementType) {
        this.requirementType = requirementType;
        return this;
    }

    public void setRequirementType(RequirementType requirementType) {
        this.requirementType = requirementType;
    }

//    public Set<OrderItem> getOrderItems() {
//        return orderItems;
//    }
//
//    public void setOrderItems(Set<OrderItem> orderItems) {
//        this.orderItems = orderItems;
//    }

    public Facility getFacility() {
        return facility;
    }

    public Requirement facility(Facility facility) {
        this.facility = facility;
        return this;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    public Set<Requirement> getItems() {
        return items;
    }

    public Requirement items(Set<Requirement> requirements) {
        this.items = requirements;
        return this;
    }

    public Requirement addItem(Requirement requirement) {
        this.items.add(requirement);
        requirement.setParent(this);
        return this;
    }

    public Requirement removeItem(Requirement requirement) {
        this.items.remove(requirement);
        requirement.setParent(null);
        return this;
    }

    public void setItems(Set<Requirement> requirements) {
        this.items = requirements;
    }

    public Requirement getParent() {
        return parent;
    }

    public Requirement parent(Requirement requirement) {
        this.parent = requirement;
        return this;
    }

    public void setParent(Requirement requirement) {
        this.parent = requirement;
    }

    public List<RequirementRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<RequirementRole> roles) {
        this.roles = roles;
    }

    public void setPartyRole(Party party, Integer role)  {
        for (RequirementRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) return;
        }

        RequirementRole current = new RequirementRole();
        current.setOwner(this);
        current.setIdRoleType(role);
        current.setParty(party);
        current.setDateFrom(LocalDateTime.now());
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        roles.add(current);
    }

    public Party getPartyRole(Party party, Integer role)  {
        for (RequirementRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) return current.getParty();
        }
        return null;
    }

    public void removePartyRole(Party party, Integer role)  {
        for (RequirementRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) {
                current.setDateThru(LocalDateTime.now());
                return;
            }
        }
    }

    public List<Approval> getApprovals() {
        return approvals;
    }

    public void setApprovals(List<Approval> approvals) {
        this.approvals = approvals;
    }

    public List<RequirementStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<RequirementStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);

        for (RequirementStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now();

        for (RequirementStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        RequirementStatus current = new RequirementStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public Boolean getActiveData() {
        switch (getCurrentStatus()) {
            case BaseConstants.STATUS_DRAFT:
            case BaseConstants.STATUS_ACTIVE:
                return true;
        }
        return false;
    }

    protected Integer getApprovalByType(Integer id){
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now().plusNanos(1);

        for (Approval current: getApprovals()) {
            if (current.getIdApprovalType().equals(id)){
                if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                    return current.getIdStatusType();
                }
            }
        }
        return result;
    }

//    protected void setApprovalByType(Integer idapprovaltype, Integer idstatustype){
//        LocalDateTime now = LocalDateTime.now();
//        for (Approval current: getApprovals()) {
//            if (current.getIdApprovalType().equals(idapprovaltype)){
//                if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
//                    if (idstatustype.equals(current.getIdStatusType())) return;
//                    current.setDateThru(now.minusNanos(1));
//                }
//            }
//        }
//
//        Approval current = new Approval();
//        current.setIdApprovalType(idapprovaltype);
//        current.setIdStatusType(idstatustype);
//        approvals.add(current);
//    }

    @PrePersist
    public void prePersist() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
        if (this.dateCreate == null) this.dateCreate = ZonedDateTime.now();
        if (this.dateRequired == null) this.dateRequired = this.dateCreate.truncatedTo(ChronoUnit.DAYS).plusDays(7l);
        if (this.qty == null) this.qty = 1;
        if (this.budget == null) this.budget = BigDecimal.valueOf(0);
    }

    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
        if (this.qty == null) this.qty = 1;
        if (this.budget == null) this.budget = BigDecimal.valueOf(0);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Requirement requirement = (Requirement) o;
        if (requirement.idRequirement == null || this.idRequirement == null) {
            return false;
        }
        return Objects.equals(this.idRequirement, requirement.idRequirement);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idRequirement);
    }

    @Override
    public String toString() {
        return "Requirement{" +
            "idRequirement=" + this.idRequirement +
            ", requirementNumber='" + getRequirementNumber() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", dateRequired='" + getDateRequired() + "'" +
            ", budget='" + getBudget() + "'" +
            ", qty='" + getQty() + "'" +
            '}';
    }
}
