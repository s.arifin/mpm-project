package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.*;

/**
 * BeSmart Team
 * Class definition for Entity VehicleWorkRequirement.
 */

@Entity
@Table(name = "vehicle_work_requirement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "vehicleworkrequirement")
public class VehicleWorkRequirement extends WorkRequirement {

    private static final long serialVersionUID = 1L;

    @Column(name = "vehiclenumber")
    private String vehicleNumber;

    @ManyToOne
    @JoinColumn(name="idcustomer", referencedColumnName="idcustomer")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name="idmechanic", referencedColumnName="idparrol")
    private Mechanic mechanic;

    @ManyToOne
    @JoinColumn(name="idvehide", referencedColumnName="idvehide")
    private VehicleIdentification vehicle;

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public VehicleWorkRequirement vehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
        return this;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public Customer getCustomer() {
        return customer;
    }

    public VehicleWorkRequirement customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Mechanic getMechanic() {
        return mechanic;
    }

    public VehicleWorkRequirement mechanic(Mechanic mechanic) {
        this.mechanic = mechanic;
        return this;
    }

    public void setMechanic(Mechanic mechanic) {
        this.mechanic = mechanic;
    }

    public VehicleIdentification getVehicle() {
        return vehicle;
    }

    public VehicleWorkRequirement vehicle(VehicleIdentification vehicleIdentification) {
        this.vehicle = vehicleIdentification;
        return this;
    }

    public void setVehicle(VehicleIdentification vehicleIdentification) {
        this.vehicle = vehicleIdentification;
    }

    @Override
    public String toString() {
        return "VehicleWorkRequirement{" +
            "idRequirement=" + this.getIdRequirement() +
            ", vehicleNumber='" + getVehicleNumber() + "'" +
            '}';
    }
}
