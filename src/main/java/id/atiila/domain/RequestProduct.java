package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * Be Smart Team
 * Class definition for Entity RequestProduct.
 */

@Entity
@Table(name = "request_product")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "requestproduct")
public class RequestProduct extends Request {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name="idfacilityfrom", referencedColumnName="idfacility")
    private Facility facilityFrom;

    @ManyToOne
    @JoinColumn(name="idfacilityto", referencedColumnName="idfacility")
    private Facility facilityTo;

    public Facility getFacilityFrom() {
        return facilityFrom;
    }

    public RequestProduct facilityFrom(Facility facility) {
        this.facilityFrom = facility;
        return this;
    }

    public void setFacilityFrom(Facility facility) {
        this.facilityFrom = facility;
    }

    public Facility getFacilityTo() {
        return facilityTo;
    }

    public RequestProduct facilityTo(Facility facility) {
        this.facilityTo = facility;
        return this;
    }

    public void setFacilityTo(Facility facility) {
        this.facilityTo = facility;
    }

    @Override
    public String toString() {
        return "RequestProduct{" +
            "idRequest=" + this.getIdRequest() +
            '}';
    }
}
