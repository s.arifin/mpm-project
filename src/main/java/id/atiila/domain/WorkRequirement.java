package id.atiila.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * BeSmart Team
 * Class definition for Entity WorkRequirement.
 */

@Entity
@Table(name = "work_requirement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "workrequirement")
public class WorkRequirement extends Requirement {

    private static final long serialVersionUID = 1L;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "work_requirement_services",
               joinColumns = @JoinColumn(name="work_requirements_id", referencedColumnName="idreq"),
               inverseJoinColumns = @JoinColumn(name="services_id", referencedColumnName="idwe"))
    private Set<WorkServiceRequirement> services = new HashSet<>();

    public Set<WorkServiceRequirement> getServices() {
        return services;
    }

    public WorkRequirement services(Set<WorkServiceRequirement> workServiceRequirements) {
        this.services = workServiceRequirements;
        return this;
    }

    public WorkRequirement addServices(WorkServiceRequirement workServiceRequirement) {
        this.services.add(workServiceRequirement);
        workServiceRequirement.getRequirements().add(this);
        return this;
    }

    public WorkRequirement removeServices(WorkServiceRequirement workServiceRequirement) {
        this.services.remove(workServiceRequirement);
        workServiceRequirement.getRequirements().remove(this);
        return this;
    }

    public void setServices(Set<WorkServiceRequirement> workServiceRequirements) {
        this.services = workServiceRequirements;
    }

    @Override
    public String toString() {
        return "WorkRequirement{" +
            "idRequirement=" + this.getIdRequirement() +
            '}';
    }
}
