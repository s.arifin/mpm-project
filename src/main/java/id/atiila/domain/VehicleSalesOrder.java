package id.atiila.domain;

import javax.persistence.*;

import id.atiila.base.BaseConstants;
import id.atiila.domain.listener.VsoListener;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.criterion.Order;
import org.springframework.data.elasticsearch.annotations.Document;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Iterator;
import java.util.UUID;

/**
 * BeSmart Team
 * Class definition for Entity VehicleSalesOrder.
 */

@Entity
@Table(name = "vehicle_sales_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "vehiclesalesorder")
public class VehicleSalesOrder extends SalesOrder {

    private static final long serialVersionUID = 1L;

    @Column(name="idunitreq")
    private UUID idSalesUnitRequirement;

    @Column(name="dtbastunit")

    private LocalDate dateBASTUnit;

    @Column(name="dtcustreceipt")
    private LocalDate dateCustomerReceipt;

    @Column(name = "bastfinco")
    private String bastFinco;

    @Column (name = "datebastfinco")
    private ZonedDateTime dateBastFinco;

    @Column(name = "name")
    private String name;

    @Column(name = "identitynumber")
    private String identityNumber;

    @Column(name = "iscompleted")
    private Boolean iscompleted;

    @Column(name="dtmachineswipe")
    private LocalDate dateSwipeMachine;

    @Column(name="dtcovernote")
    private LocalDate dateCoverNote;

    @Column(name="dttakepicture")
    private LocalDate dateTakePicture;

    @Column(name="idlsgpaystatus")
    private Integer idLeasingStatusPayment;

    @ManyToOne
    @JoinColumn(name="idleacom", referencedColumnName="idparrol")
    private LeasingCompany leasing;

    @Transient
    private Billing billing;

    @Column(name="dtadmslsverify")
    private LocalDate adminSalesVeriDate;

    // 0 = default -> belum verifikasi
    // 1 = ya
    // 2 = tidak approve
    @Column(name="admslsverifyval")
    private Integer adminSalesVerifyValue;

    @Column(name="dtafcverify")
    private LocalDate afcVeriDate;

    @Column(name="afcverifyval")
    private Integer afcVerifyValue;

    @Column(name="admslsnotapprovnote", nullable = true, length = 300)
    private String adminSalesNotApprvNote;

    @Column(name="afcnotapprovnote", nullable = true, length = 300)
    private String afcNotApprvNote;

    @Column
    private String vrnama1;

    @Column
    private String vrnama2;

    @Column
    private String vrnamamarket;

    @Column
    private String vrwarna;

    @Column
    private String vrtahunproduksi;

    @Column
    private Double vrangsuran;

    @Column
    private Double vrtenor;

    @Column
    private String vrleasing;

    @Column(name="vrtglpengiriman")
    private ZonedDateTime vrtglpengiriman;

    @Column
    private String vrjampengiriman;

    @Column
    private String vrtipepenjualan;

    @Column
    private Double vrdpmurni;

    @Column
    private Double vrtandajadi;

    @Column(name="vrtglpembayaran")
    private ZonedDateTime vrtglpembayaran;

    @Column
    private Double vrsisa;

    @Column
    private Integer vriscod;

    @Column
    private String vrcatatantambahan;

    @Column
    private Integer vrisverified;

    @Column
    private String vrcatatan;

    @Column(nullable = true, length = 300)
    private String kacabnote;

    // 0 = default -> belum verifikasi
    // 1 = ya
    @Column(name = "isreqtaxinv")
    private Integer isRequestTaxInvoice;

    @Column(name = "isdonematching")
    private Boolean doneMatching;

    @Column(name = "idbirojasa")
    private String idBiroJasa;

    @Column(name = "idsalesman")
    private UUID idSalesman;

    @Column(name = "issubsidifincoasdp")
    private Boolean subFincoyAsDp;

    @Column(name = "refundfinco")
    private BigDecimal refundFinco;

    @Column(name = "iscanclosedp")
    private Boolean canCloseDP;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getBastFinco() {
        return bastFinco;
    }

    public void setBastFinco(String bastFinco) {
        this.bastFinco = bastFinco;
    }

    public BigDecimal getRefundFinco() {
        return refundFinco;
    }

    public void setRefundFinco(BigDecimal refundFinco) {
        this.refundFinco = refundFinco;
    }

    public Boolean getSubFincoyAsDp() {
        return subFincoyAsDp;
    }

    public void setSubFincoyAsDp(Boolean subFincoyAsDp) {
        this.subFincoyAsDp = subFincoyAsDp;
    }

    public UUID getIdSalesman() {
        return idSalesman;
    }

    public void setIdSalesman(UUID idSalesman) {
        this.idSalesman = idSalesman;
    }

    public ZonedDateTime getDateBastFinco() {
        return dateBastFinco;
    }

    public void setDateBastFinco(ZonedDateTime dateBastFinco) {
        this.dateBastFinco = dateBastFinco;
    }

    public Boolean getIscompleted() {
        return iscompleted;
    }

    public void setIscompleted(Boolean iscompleted) {
        this.iscompleted = iscompleted;
    }

    @Override
    public void prePersist() {
        super.prePersist();
        if (this.adminSalesVerifyValue == null) this.setAdminSalesVerifyValue(0);
        if (this.afcVerifyValue == null) this.setAfcVerifyValue(0);
        if (this.isRequestTaxInvoice == null ) this.setIsRequestTaxInvoice(0);
        if (this.doneMatching == null ) this.setDoneMatching(false);
        if (this.canCloseDP == null) canCloseDP = true;
    }

    public String getIdBiroJasa() {
        return idBiroJasa;
    }

    public void setIdBiroJasa(String idBiroJasa) {
        this.idBiroJasa = idBiroJasa;
    }

    public Boolean getDoneMatching() {
        return doneMatching;
    }

    public void setDoneMatching(Boolean doneMatching) {
        this.doneMatching = doneMatching;
    }

    public Integer getIsRequestTaxInvoice() {
        return isRequestTaxInvoice;
    }

    public void setIsRequestTaxInvoice(Integer isRequestTaxInvoice) {
        this.isRequestTaxInvoice = isRequestTaxInvoice;
    }

    public String getKacabnote() {
        return kacabnote;
    }

    public void setKacabnote(String kacabnote) {
        this.kacabnote = kacabnote;
    }

    public String getAfcNotApprvNote() {
        return afcNotApprvNote;
    }

    public void setAfcNotApprvNote(String afcNotApprvNote) {
        this.afcNotApprvNote = afcNotApprvNote;
    }

    public void setAdminSalesVerifyValue(Integer adminSalesVerifyValue) {
        this.adminSalesVerifyValue = adminSalesVerifyValue;
    }

    public void setAfcVerifyValue(Integer afcVerifyValue) {
        this.afcVerifyValue = afcVerifyValue;
    }

    public String getAdminSalesNotApprvNote() {
        return adminSalesNotApprvNote;
    }

    public void setAdminSalesNotApprvNote(String adminSalesNotApprvNote) {
        this.adminSalesNotApprvNote = adminSalesNotApprvNote;
    }

    public LocalDate getAfcVeriDate() {
        return afcVeriDate;
    }

    public void setAfcVeriDate(LocalDate afcVeriDate) {
        this.afcVeriDate = afcVeriDate;
    }

    public LocalDate getAdminSalesVeriDate() {
        return adminSalesVeriDate;
    }

    public void setAdminSalesVeriDate(LocalDate adminSalesVeriDate) {
        this.adminSalesVeriDate = adminSalesVeriDate;
    }

    public LocalDate getDateBASTUnit() {
        return dateBASTUnit;
    }

    public void setDateBASTUnit(LocalDate dateBASTUnit) {
        this.dateBASTUnit = dateBASTUnit;
    }

    public LocalDate getDateCustomerReceipt() {
        return dateCustomerReceipt;
    }

    public void setDateCustomerReceipt(LocalDate dateCustomerReceipt) {
        this.dateCustomerReceipt = dateCustomerReceipt;
    }

    public LocalDate getDateSwipeMachine() {
        return dateSwipeMachine;
    }

    public void setDateSwipeMachine(LocalDate dateSwipeMachine) {
        this.dateSwipeMachine = dateSwipeMachine;
    }

    public LocalDate getDateCoverNote() {
        return dateCoverNote;
    }

    public void setDateCoverNote(LocalDate dateCoverNote) {
        this.dateCoverNote = dateCoverNote;
    }

    public LocalDate getDateTakePicture() {
        return dateTakePicture;
    }

    public void setDateTakePicture(LocalDate dateTakePicture) {
        this.dateTakePicture = dateTakePicture;
    }

    public Integer getIdLeasingStatusPayment() {
        return idLeasingStatusPayment;
    }

    public LeasingCompany getLeasing() {
        return leasing;
    }

    public void setLeasing(LeasingCompany leasing) {
        this.leasing = leasing;
    }

    public void setIdLeasingStatusPayment(Integer idLeasingStatusPayment) {
        this.idLeasingStatusPayment = idLeasingStatusPayment;
    }

    public Billing getBilling() {
        if (this.billing == null && getDetails().size() > 0) {
            OrderItem o = getDetails().iterator().next();
            //todo: Masalah disini yang menyebabkan tuple
            if (o.getBillingItems().size() > 0){
                BillingItem billingItem = o.getBillingItems().iterator().next().getBillingItem();
                if (billingItem != null) this.billing = billingItem.getBilling();
            }
        }
        return this.billing;
    }

//    public ShipmentOutgoing getShipmentOutGoing(){
//        ShipmentOutgoing so = this.getShipmentOutGoing();
//
//    }vehicle

    public LocalDate getDateOrderStatusApprove(){
        Iterator<OrdersStatus> oss = getStatuses().iterator();
        while (oss.hasNext()){
            OrdersStatus os = oss.next();
            if (os.getIdStatusType()==72){
                return  os.getDateFrom().toLocalDate();
            }
        }
        return  null;
    }

    public ZonedDateTime getDateSchedulle(){
        return null;
        // return this.getDetails().iterator().next().getShipmentItems().iterator().next().getShipmentItem().getShipment().getDateSchedulle();
    }

    public String getBillingNumber() {
        return getBilling() != null ? getBilling().getBillingNumber() : null;
    }

    public ZonedDateTime getDateBilling() {
        return getBilling() != null ? getBilling().getDateCreate() : null;
    }

    public BigDecimal getBillingAmount() {
        return getBilling() != null ? BigDecimal.ZERO : null;
    }

    @Override
    public String toString() {
        return "VehicleSalesOrder{" +
            "idOrder=" + this.getIdOrder() +
            '}';
    }


    public Integer getAdminSalesVerifyValue() {
        return adminSalesVerifyValue;
    }

    public Integer getAfcVerifyValue() {
        return afcVerifyValue;
    }

    public String getVrnama1() {
        return vrnama1;
    }

    public void setVrnama1(String vrnama1) {
        this.vrnama1 = vrnama1;
    }

    public String getVrnama2() {
        return vrnama2;
    }

    public void setVrnama2(String vrnama2) {
        this.vrnama2 = vrnama2;
    }

    public String getVrnamamarket() {
        return vrnamamarket;
    }

    public void setVrnamamarket(String vrnamamarket) {
        this.vrnamamarket = vrnamamarket;
    }

    public String getVrwarna() {
        return vrwarna;
    }

    public void setVrwarna(String vrwarna) {
        this.vrwarna = vrwarna;
    }

    public String getVrtahunproduksi() {
        return vrtahunproduksi;
    }

    public void setVrtahunproduksi(String vrtahunproduksi) {
        this.vrtahunproduksi = vrtahunproduksi;
    }

    public Double getVrangsuran() {
        return vrangsuran;
    }

    public void setVrangsuran(Double vrangsuran) {
        this.vrangsuran = vrangsuran;
    }

    public Double getVrtenor() {
        return vrtenor;
    }

    public void setVrtenor(Double vrtenor) {
        this.vrtenor = vrtenor;
    }

    public String getVrleasing() {
        return vrleasing;
    }

    public void setVrleasing(String vrleasing) {
        this.vrleasing = vrleasing;
    }

    public ZonedDateTime getVrtglpengiriman() {
        return vrtglpengiriman;
    }

    public void setVrtglpengiriman(ZonedDateTime vrtglpengiriman) {
        this.vrtglpengiriman = vrtglpengiriman;
    }

    public String getVrjampengiriman() {
        return vrjampengiriman;
    }

    public void setVrjampengiriman(String vrjampengiriman) {
        this.vrjampengiriman = vrjampengiriman;
    }

    public String getVrtipepenjualan() {
        return vrtipepenjualan;
    }

    public void setVrtipepenjualan(String vrtipepenjualan) {
        this.vrtipepenjualan = vrtipepenjualan;
    }

    public Double getVrdpmurni() {
        return vrdpmurni;
    }

    public void setVrdpmurni(Double vrdpmurni) {
        this.vrdpmurni = vrdpmurni;
    }

    public Double getVrtandajadi() {
        return vrtandajadi;
    }

    public void setVrtandajadi(Double vrtandajadi) {
        this.vrtandajadi = vrtandajadi;
    }

    public ZonedDateTime getVrtglpembayaran() {
        return vrtglpembayaran;
    }

    public void setVrtglpembayaran(ZonedDateTime vrtglpembayaran) {
        this.vrtglpembayaran = vrtglpembayaran;
    }

    public Double getVrsisa() {
        return vrsisa;
    }

    public void setVrsisa(Double vrsisa) {
        this.vrsisa = vrsisa;
    }

    public Integer getVriscod() {
        return vriscod;
    }

    public void setVriscod(Integer vriscod) {
        this.vriscod = vriscod;
    }

    public String getVrcatatantambahan() {
        return vrcatatantambahan;
    }

    public void setVrcatatantambahan(String vrcatatantambahan) {
        this.vrcatatantambahan = vrcatatantambahan;
    }

    public Integer getVrisverified() {
        return vrisverified;
    }

    public void setVrisverified(Integer vrisverified) {
        this.vrisverified = vrisverified;
    }

    public String getVrcatatan() {
        return vrcatatan;
    }

    public void setVrcatatan(String vrcatatan) {
        this.vrcatatan = vrcatatan;
    }

    public UUID getIdSalesUnitRequirement() {
        return idSalesUnitRequirement;
    }

    public void setIdSalesUnitRequirement(UUID idSalesUnitRequirement) {
        this.idSalesUnitRequirement = idSalesUnitRequirement;
    }

    public Boolean getCanCloseDP() {
        return canCloseDP;
    }

    public void setCanCloseDP(Boolean canCloseDP) {
        this.canCloseDP = canCloseDP;
    }
}
