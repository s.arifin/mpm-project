package id.atiila.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * BeSmart Team
 * Class definition for Entity ShipmentIncoming.
 */

@Entity
@Table(name = "shipment_incoming")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "shipmentincoming")
public class ShipmentIncoming extends Shipment {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "ShipmentIncoming{" +
            "idShipment=" + this.getIdShipment() +
            '}';
    }
}
