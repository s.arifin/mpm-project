package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity OrderType.
 */

@Entity
@Table(name = "order_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "ordertype")
public class OrderType implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final int VSO_GROUP = 200;
    public static final int VSO_DIRECT = 201;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idordtyp")
    private Integer idOrderType;

    @Column(name = "description")
    private String description;

    public Integer getIdOrderType() {
        return this.idOrderType;
    }

    public void setIdOrderType(Integer id) {
        this.idOrderType = id;
    }

    public String getDescription() {
        return description;
    }

    public OrderType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderType orderType = (OrderType) o;
        if (orderType.idOrderType == null || this.idOrderType == null) {
            return false;
        }
        return Objects.equals(this.idOrderType, orderType.idOrderType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idOrderType);
    }

    @Override
    public String toString() {
        return "OrderType{" +
            "idOrderType=" + this.idOrderType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
