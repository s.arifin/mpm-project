package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity DealerReminderType.
 */

@Entity
@Table(name = "dealer_reminder_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dealerremindertype")
public class DealerReminderType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idremindertype")
    private Integer idReminderType;

    @Column(name = "description")
    private String description;

    @Column(name = "messages")
    private String messages;

    public Integer getIdReminderType() {
        return this.idReminderType;
    }

    public void setIdReminderType(Integer id) {
        this.idReminderType = id;
    }

    public String getDescription() {
        return description;
    }

    public DealerReminderType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessages() {
        return messages;
    }

    public DealerReminderType messages(String messages) {
        this.messages = messages;
        return this;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DealerReminderType dealerReminderType = (DealerReminderType) o;
        if (dealerReminderType.idReminderType == null || this.idReminderType == null) {
            return false;
        }
        return Objects.equals(this.idReminderType, dealerReminderType.idReminderType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idReminderType);
    }

    @Override
    public String toString() {
        return "DealerReminderType{" +
            "idReminderType=" + this.idReminderType +
            ", description='" + getDescription() + "'" +
            ", messages='" + getMessages() + "'" +
            '}';
    }
}
