package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.util.Objects;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;

/**
 * BeSmart Team
 * Class definition for Entity Vehicle.
 */

@Entity
@Table(name = "vehicle")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "vehicle")
public class Vehicle implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idvehicle", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idVehicle;

    @Column(name = "idmachine")
    private String idMachine;

    @Column(name = "idframe")
    private String idFrame;

    @Column(name = "yearofass")
    private Integer yearOfAssembly;

    @Column(name = "idproduct")
    private String idProduct;

    @Column(name = "idcolor")
    private Integer idColor;

    @ManyToOne
    @JoinColumn(name="idproduct", referencedColumnName="idproduct", insertable = false, updatable = false)
    private Good product;

    @ManyToOne
    @JoinColumn(name="idcolor", referencedColumnName="idfeature", insertable = false, updatable = false)
    private Feature feature;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    public UUID getIdVehicle() {
        return this.idVehicle;
    }

    public void setIdVehicle(UUID id) {
        this.idVehicle = id;
    }

    public String getIdMachine() {
        return idMachine;
    }

    public Vehicle idMachine(String idMachine) {
        this.idMachine = idMachine;
        return this;
    }

    public void setIdMachine(String idMachine) {
        this.idMachine = idMachine;
    }

    public String getIdFrame() {
        return idFrame;
    }

    public Vehicle idFrame(String idFrame) {
        this.idFrame = idFrame;
        return this;
    }

    public void setIdFrame(String idFrame) {
        this.idFrame = idFrame;
    }

    public Integer getYearOfAssembly() {
        return yearOfAssembly;
    }

    public Vehicle yearOfAssembly(Integer yearOfAssembly) {
        this.yearOfAssembly = yearOfAssembly;
        return this;
    }

    public void setYearOfAssembly(Integer yearOfAssembly) {
        this.yearOfAssembly = yearOfAssembly;
    }

    public Good getProduct() {
        return product;
    }

    public Vehicle product(Good good) {
        this.product = good;
        return this;
    }

    public void setProduct(Good good) {
        this.product = good;
    }

    public Feature getFeature() {
        return feature;
    }

    public Vehicle feature(Feature feature) {
        this.feature = feature;
        return this;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdColor() {
        return idColor;
    }

    public void setIdColor(Integer idColor) {
        this.idColor = idColor;
    }

    public Internal getInternal() {
        return internal;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Vehicle vehicle = (Vehicle) o;
        if (vehicle.idVehicle == null || this.idVehicle == null) {
            return false;
        }
        return Objects.equals(this.idVehicle, vehicle.idVehicle);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idVehicle);
    }

    @Override
    public String toString() {
        return "Vehicle{" +
            "idVehicle=" + this.idVehicle +
            ", idMachine='" + getIdMachine() + "'" +
            ", idFrame='" + getIdFrame() + "'" +
            ", yearOfAssembly='" + getYearOfAssembly() + "'" +
            '}';
    }
}
