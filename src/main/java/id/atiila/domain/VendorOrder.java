package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity VendorOrder.
 */

@Entity
@Table(name = "vendor_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "vendororder")
public class VendorOrder extends Orders {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name="idvendor", referencedColumnName="idvendor")
    private Vendor vendor;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idbillto", referencedColumnName="idbillto")
    private BillTo billTo;

    public Vendor getVendor() {
        return vendor;
    }

    public VendorOrder vendor(Vendor vendor) {
        this.vendor = vendor;
        return this;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public Internal getInternal() {
        return internal;
    }

    public VendorOrder internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public BillTo getBillTo() {
        return billTo;
    }

    public VendorOrder billTo(BillTo billTo) {
        this.billTo = billTo;
        return this;
    }

    public void setBillTo(BillTo billTo) {
        this.billTo = billTo;
    }

    @Override
    public String toString() {
        return "VendorOrder{" +
            "idOrder=" + this.getIdOrder() +
            '}';
    }
}
