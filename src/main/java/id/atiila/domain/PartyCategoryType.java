package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity PartyCategoryType.
 */

@Entity
@Table(name = "party_category_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "partycategorytype")
public class PartyCategoryType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcattyp")
    private Integer idCategoryType;

    @Column(name = "description")
    private String description;

    @Column(name = "refkey")
    private String refKey;

    @ManyToOne
    @JoinColumn(name="idparent", referencedColumnName="idcattyp")
    private PartyCategoryType parent;

    public Integer getIdCategoryType() {
        return this.idCategoryType;
    }

    public void setIdCategoryType(Integer id) {
        this.idCategoryType = id;
    }

    public String getDescription() {
        return description;
    }

    public PartyCategoryType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefKey() {
        return refKey;
    }

    public PartyCategoryType refKey(String refKey) {
        this.refKey = refKey;
        return this;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public PartyCategoryType getParent() {
        return parent;
    }

    public PartyCategoryType parent(PartyCategoryType partyCategoryType) {
        this.parent = partyCategoryType;
        return this;
    }

    public void setParent(PartyCategoryType partyCategoryType) {
        this.parent = partyCategoryType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PartyCategoryType partyCategoryType = (PartyCategoryType) o;
        if (partyCategoryType.idCategoryType == null || this.idCategoryType == null) {
            return false;
        }
        return Objects.equals(this.idCategoryType, partyCategoryType.idCategoryType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idCategoryType);
    }

    @Override
    public String toString() {
        return "PartyCategoryType{" +
            "idCategoryType=" + this.idCategoryType +
            ", description='" + getDescription() + "'" +
            ", refKey='" + getRefKey() + "'" +
            '}';
    }
}
