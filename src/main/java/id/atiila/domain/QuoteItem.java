package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity QuoteItem.
 */

@Entity
@Table(name = "quote_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "quoteitem")
public class QuoteItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idquoteitem", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idQuoteItem;

    @Column(name = "unitprice", precision=10, scale=2)
    private BigDecimal unitPrice;

    @Column(name = "qty")
    private Integer qty;

    @ManyToOne
    @JoinColumn(name="idquote", referencedColumnName="idquote")
    private Quote quote;

    @ManyToOne
    @JoinColumn(name="idproduct", referencedColumnName="idproduct")
    private Product product;

    public UUID getIdQuoteItem() {
        return this.idQuoteItem;
    }

    public void setIdQuoteItem(UUID id) {
        this.idQuoteItem = id;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public QuoteItem unitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getQty() {
        return qty;
    }

    public QuoteItem qty(Integer qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Quote getQuote() {
        return quote;
    }

    public QuoteItem quote(Quote quote) {
        this.quote = quote;
        return this;
    }

    public void setQuote(Quote quote) {
        this.quote = quote;
    }

    public Product getProduct() {
        return product;
    }

    public QuoteItem product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        QuoteItem quoteItem = (QuoteItem) o;
        if (quoteItem.idQuoteItem == null || this.idQuoteItem == null) {
            return false;
        }
        return Objects.equals(this.idQuoteItem, quoteItem.idQuoteItem);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idQuoteItem);
    }

    @Override
    public String toString() {
        return "QuoteItem{" +
            "idQuoteItem=" + this.idQuoteItem +
            ", unitPrice='" + getUnitPrice() + "'" +
            ", qty='" + getQty() + "'" +
            '}';
    }
}
