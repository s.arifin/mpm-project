package id.atiila.domain;

import id.atiila.base.BaseConstants;
import id.atiila.base.DomainEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;

/**
 * BeSmart Team
 * Class definition for Entity PartyContactMechanism.
 */

@Entity
@Table(name = "party_contact_mechanism")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "partycontactmechanism")
public class PartyContactMechanism implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idparcon")
    private Long idPartyContact;

    @Column(name = "idpurposetype")
    private Integer idPurposeType;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name = "idparty", referencedColumnName = "idparty")
    private Party party;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idcontact", referencedColumnName = "idcontact")
    private ContactMechanism contact;

    public Long getIdPartyContact() {
        return this.idPartyContact;
    }

    public void setIdPartyContact(Long id) {
        this.idPartyContact = id;
    }

    public Integer getIdPurposeType() {
        return idPurposeType;
    }

    public PartyContactMechanism idPurposeType(Integer idPurposeType) {
        this.idPurposeType = idPurposeType;
        return this;
    }

    public void setIdPurposeType(Integer idPurposeType) {
        this.idPurposeType = idPurposeType;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public PartyContactMechanism dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public PartyContactMechanism dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Party getParty() {
        return party;
    }

    public PartyContactMechanism party(Party party) {
        this.party = party;
        return this;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public ContactMechanism getContact() {
        return contact;
    }

    public PartyContactMechanism contact(ContactMechanism contactMechanism) {
        this.contact = contactMechanism;
        return this;
    }

    public void setContact(ContactMechanism contactMechanism) {
        this.contact = contactMechanism;
    }

    public String getDescription() {
        return this.contact != null ? contact.getDescription() : "";
    }

    public PostalAddress getPostal() {
        if (this.contact != null && this.contact.getIdContactType() == 1) return (PostalAddress) this.contact;
        return null;
    }

    public void setPostal(PostalAddress postal) {
        if (postal != null && postal.getIdContactType() == 1) this.contact = postal;
    }

    public ElectronicAddress getElectronic() {
        if (this.contact != null && this.contact.getIdContactType() == 1) return (ElectronicAddress) this.contact;
        return null;
    }

    public void setElectronic(ElectronicAddress electronic) {
        if (electronic != null && electronic.getIdContactType() == 2) this.contact = electronic;
    }

    public TelecomunicationNumber getTelco() {
        if (this.contact != null && this.contact.getIdContactType() == 3) return (TelecomunicationNumber) this.contact;
        return null;
    }

    public void setTelco(TelecomunicationNumber telco) {
        if (telco != null && telco.getIdContactType() == 3) this.contact = telco;
    }

    @PrePersist
    public void prePersist() {
        if (this.idPurposeType == null) this.idPurposeType = BaseConstants.CPM_POSTALADDRESS;
        if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
        if (this.dateThru == null)
            this.dateThru = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PartyContactMechanism that = (PartyContactMechanism) o;
        return new EqualsBuilder()
            .append(idPartyContact, that.idPartyContact)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idPartyContact)
            .append(idPurposeType)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "PartyContactMechanism {" +
            "idPartyContact=" + this.idPartyContact +
            ", idPurposeType='" + getIdPurposeType() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
