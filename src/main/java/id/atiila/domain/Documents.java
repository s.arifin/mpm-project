package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity Documents.
 */

@Entity
@Table(name = "documents")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "documents")
public class Documents implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "iddocument", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idDocument;

    @Column(name = "note")
    private String note;

    @Column(name = "dtcreate")
    private ZonedDateTime dateCreate;

    @ManyToOne
    @JoinColumn(name="iddoctype", referencedColumnName="iddoctype")
    private DocumentType documentType;

    public UUID getIdDocument() {
        return this.idDocument;
    }

    public void setIdDocument(UUID id) {
        this.idDocument = id;
    }

    public String getNote() {
        return note;
    }

    public Documents note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public Documents dateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
        return this;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public Documents documentType(DocumentType documentType) {
        this.documentType = documentType;
        return this;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Documents documents = (Documents) o;

        return new EqualsBuilder()
            .append(idDocument, documents.idDocument)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idDocument)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "Documents{" +
            "idDocument=" + this.idDocument +
            ", note='" + getNote() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            '}';
    }
}
