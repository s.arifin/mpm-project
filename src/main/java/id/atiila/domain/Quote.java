package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.UUID;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity Quote.
 */

@Entity
@Table(name = "quote")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "quote")
public class Quote implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idquote", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idQuote;

    @Column(name = "dtissued")
    private ZonedDateTime dateIssued;

    @Column(name = "description")
    private String description;

    public UUID getIdQuote() {
        return this.idQuote;
    }

    public void setIdQuote(UUID id) {
        this.idQuote = id;
    }

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<QuoteStatus> statuses = new ArrayList<QuoteStatus>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<QuoteRole> roles = new ArrayList<QuoteRole>();

    public ZonedDateTime getDateIssued() {
        return dateIssued;
    }

    public Quote dateIssued(ZonedDateTime dateIssued) {
        this.dateIssued = dateIssued;
        return this;
    }

    public void setDateIssued(ZonedDateTime dateIssued) {
        this.dateIssued = dateIssued;
    }

    public String getDescription() {
        return description;
    }

    public Quote description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<QuoteRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<QuoteRole> roles) {
        this.roles = roles;
    }

    public void setPartyRole(Party party, Integer role)  {
        for (QuoteRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) return;
        }

        QuoteRole current = new QuoteRole();
        current.setOwner(this);
        current.setIdRoleType(role);
		current.setParty(party);
        current.setDateFrom(LocalDateTime.now());
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        roles.add(current);
    }

    public Party getPartyRole(Party party, Integer role)  {
        for (QuoteRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) return current.getParty();
        }
        return null;
    }

    public void removePartyRole(Party party, Integer role)  {
        for (QuoteRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) {
				current.setDateThru(LocalDateTime.now());
				return;
			}
        }
    }

    public List<QuoteStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<QuoteStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now().plusNanos(1);
        
        for (QuoteStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now();

        for (QuoteStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        QuoteStatus current = new QuoteStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public Boolean getActiveData() {
        switch (getCurrentStatus()) {
            case BaseConstants.STATUS_DRAFT:
            case BaseConstants.STATUS_ACTIVE:
                return true;
        }
        return false;
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Quote quote = (Quote) o;
        if (quote.idQuote == null || this.idQuote == null) {
            return false;
        }
        return Objects.equals(this.idQuote, quote.idQuote);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idQuote);
    }

    @Override
    public String toString() {
        return "Quote{" +
            "idQuote=" + this.idQuote +
            ", dateIssued='" + getDateIssued() + "'" +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
