package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity FacilityType.
 */

@Entity
@Table(name = "facility_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "facilitytype")
public class FacilityType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idfacilitytype")
    private Integer idFacilityType;

    @Column(name = "description")
    private String description;

    public Integer getIdFacilityType() {
        return this.idFacilityType;
    }

    public void setIdFacilityType(Integer id) {
        this.idFacilityType = id;
    }

    public String getDescription() {
        return description;
    }

    public FacilityType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FacilityType facilityType = (FacilityType) o;
        if (facilityType.idFacilityType == null || this.idFacilityType == null) {
            return false;
        }
        return Objects.equals(this.idFacilityType, facilityType.idFacilityType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idFacilityType);
    }

    @Override
    public String toString() {
        return "FacilityType{" +
            "idFacilityType=" + this.idFacilityType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
