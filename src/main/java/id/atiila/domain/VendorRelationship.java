package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;import java.util.UUID;

import java.util.Objects;



/**
 * atiila consulting
 * Class definition for Entity VendorRelationship.
 */

@Entity
@Table(name = "vendor_relationship")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "vendorrelationship")
public class VendorRelationship extends PartyRelationship {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idvendor", referencedColumnName="idvendor")
    private Vendor vendor;

    public Internal getInternal() {
        return internal;
    }

    public VendorRelationship internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public VendorRelationship vendor(Vendor vendor) {
        this.vendor = vendor;
        return this;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VendorRelationship vendorRelationship = (VendorRelationship) o;
        if (vendorRelationship.getIdPartyRelationship() == null || this.getIdPartyRelationship() == null) {
            return false;
        }
        return Objects.equals(this.getIdPartyRelationship(), vendorRelationship.getIdPartyRelationship());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdPartyRelationship());
    }

    @Override
    public String toString() {
        return "VendorRelationship{" +
            "idPartyRelationship=" + this.getIdPartyRelationship() +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
