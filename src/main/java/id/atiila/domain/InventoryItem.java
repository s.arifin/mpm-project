package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.UUID;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity InventoryItem.
 */

@Entity
@Table(name = "inventory_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "inventoryitem")
public class InventoryItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idinvite", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idInventoryItem;

    @Column(name = "idowner")
    private UUID idOwner;

    @Column(name = "idfacility")
    private UUID idFacility;

    @Column(name = "idcontainer")
    private UUID idContainer;

    @Column(name = "idproduct")
    private String idProduct;

    @Column(name = "idfeature")
    private Integer idFeature;

    @Column(name = "dtcreated")
    private ZonedDateTime dateCreated;

    @Column(name = "qty")
    private Double qty;

    @Column(name = "qtybooking")
    private Double qtyBooking;

    @Column(name = "idframe")
    private String idFrame;

    @Column(name = "idmachine")
    private String idMachine;

    @Column(name = "yearassembly")
    private Integer yearAssembly;

    @ManyToOne
    @JoinColumn(name="idreceipt", referencedColumnName="idreceipt")
    private ShipmentReceipt shipmentReceipt;

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<InventoryItemStatus> statuses = new ArrayList<InventoryItemStatus>();

    public UUID getIdInventoryItem() {
        return this.idInventoryItem;
    }

    public void setIdInventoryItem(UUID id) {
        this.idInventoryItem = id;
    }

    public UUID getIdOwner() {
        return idOwner;
    }

    public InventoryItem idOwner(UUID idOwner) {
        this.idOwner = idOwner;
        return this;
    }

    public void setIdOwner(UUID idOwner) {
        this.idOwner = idOwner;
    }

    public UUID getIdFacility() {
        return idFacility;
    }

    public InventoryItem idFacility(UUID idFacility) {
        this.idFacility = idFacility;
        return this;
    }

    public void setIdFacility(UUID idFacility) {
        this.idFacility = idFacility;
    }

    public UUID getIdContainer() {
        return idContainer;
    }

    public InventoryItem idContainer(UUID idContainer) {
        this.idContainer = idContainer;
        return this;
    }

    public void setIdContainer(UUID idContainer) {
        this.idContainer = idContainer;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public InventoryItem idProduct(String idProduct) {
        this.idProduct = idProduct;
        return this;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdFeature() {
        return idFeature;
    }

    public InventoryItem idFeature(Integer idFeature) {
        this.idFeature = idFeature;
        return this;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public InventoryItem dateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Double getQty() {
        return qty;
    }

    public InventoryItem qty(Double qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public void setQty(Integer qty) {
        this.qty = Double.valueOf(qty);
    }

    public Double getQtyBooking() {
        return qtyBooking;
    }

    public InventoryItem qtyBooking(Double qtyBooking) {
        this.qtyBooking = qtyBooking;
        return this;
    }

    public void setQtyBooking(Double qtyBooking) {
        this.qtyBooking = qtyBooking;
    }

    public void setQtyBooking(Integer qtyBooking) {
        this.qtyBooking = Double.valueOf(qtyBooking);
    }

    public String getIdFrame() {
        return idFrame;
    }

    public InventoryItem idFrame(String idFrame) {
        this.idFrame = idFrame;
        return this;
    }

    public void setIdFrame(String idFrame) {
        this.idFrame = idFrame;
    }

    public String getIdMachine() {
        return idMachine;
    }

    public InventoryItem idMachine(String idMachine) {
        this.idMachine = idMachine;
        return this;
    }

    public void setIdMachine(String idMachine) {
        this.idMachine = idMachine;
    }

    public ShipmentReceipt getShipmentReceipt() {
        return shipmentReceipt;
    }

    public InventoryItem shipmentReceipt(ShipmentReceipt shipmentReceipt) {
        this.shipmentReceipt = shipmentReceipt;
        return this;
    }

    public void setShipmentReceipt(ShipmentReceipt shipmentReceipt) {
        this.shipmentReceipt = shipmentReceipt;
    }

    public List<InventoryItemStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<InventoryItemStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getYearAssembly() {
        return yearAssembly;
    }

    public void setYearAssembly(Integer yearAssembly) {
        this.yearAssembly = yearAssembly;
    }

    public InventoryItem assignFrom(ShipmentReceipt sr) {
        this.setIdProduct(sr.getIdProduct());
        this.setQty(sr.getQtyAccept() - sr.getQtyReject());
        this.setQtyBooking(0d);
        this.setIdFeature(sr.getIdFeature());
        this.setIdOwner(sr.getShipmentPackage().getInternal().getOrganization().getIdParty());
        return this;
    }

    public InventoryItem assignFrom(UnitShipmentReceipt sr) {
        this.setIdOwner(sr.getShipmentPackage().getInternal().getOrganization().getIdParty());
        this.setIdProduct(sr.getIdProduct());
        this.setQty(sr.getQtyAccept() - sr.getQtyReject());
        this.setQtyBooking(0d);
        this.setIdFeature(sr.getIdFeature());
        this.setIdFrame(sr.getIdFrame());
        this.setIdMachine(sr.getIdMachine());
        this.setYearAssembly(sr.getYearAssembly());
        return this;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now().plusNanos(1);

        for (InventoryItemStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now();

        for (InventoryItemStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        InventoryItemStatus current = new InventoryItemStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public Boolean getActiveData() {
        switch (getCurrentStatus()) {
            case BaseConstants.STATUS_DRAFT:
            case BaseConstants.STATUS_ACTIVE:
                return true;
        }
        return false;
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
        if (this.dateCreated == null) this.dateCreated = ZonedDateTime.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InventoryItem inventoryItem = (InventoryItem) o;
        if (inventoryItem.idInventoryItem == null || this.idInventoryItem == null) {
            return false;
        }
        return Objects.equals(this.idInventoryItem, inventoryItem.idInventoryItem);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idInventoryItem);
    }

    @Override
    public String toString() {
        return "InventoryItem{" +
            "idInventoryItem=" + this.idInventoryItem +
            ", idOwner='" + getIdOwner() + "'" +
            ", idFacility='" + getIdFacility() + "'" +
            ", idContainer='" + getIdContainer() + "'" +
            ", idProduct='" + getIdProduct() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            ", qty='" + getQty() + "'" +
            ", qtyBooking='" + getQtyBooking() + "'" +
            ", idFrame='" + getIdFrame() + "'" +
            ", idMachine='" + getIdMachine() + "'" +
            '}';
    }
}
