package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity StandardCalendar.
 */

@Entity
@Table(name = "standard_calendar")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "standardcalendar")
public class StandardCalendar extends BaseCalendar {

    private static final long serialVersionUID = 1L;

    @Column(name = "workday")
    private Boolean workDay;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    public Boolean isWorkDay() {
        return workDay;
    }

    public StandardCalendar workDay(Boolean workDay) {
        this.workDay = workDay;
        return this;
    }

    public void setWorkDay(Boolean workDay) {
        this.workDay = workDay;
    }

    public Internal getInternal() {
        return internal;
    }

    public StandardCalendar internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StandardCalendar standardCalendar = (StandardCalendar) o;
        if (standardCalendar.getIdCalendar() == null || this.getIdCalendar() == null) {
            return false;
        }
        return Objects.equals(this.getIdCalendar(), standardCalendar.getIdCalendar());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdCalendar());
    }

    @Override
    public String toString() {
        return "StandardCalendar{" +
            "idCalendar=" + this.getIdCalendar() +
            ", description='" + getDescription() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            ", dateKey='" + getDateKey() + "'" +
            ", workDay='" + isWorkDay() + "'" +
            '}';
    }
}
