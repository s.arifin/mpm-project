package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.*;

/**
 * BeSmart Team
 * Class definition for Entity SuspectOrganization.
 */

@Entity
@Table(name = "organization_suspect")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "suspectorganization")
public class SuspectOrganization extends Suspect {

    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name="idparty", referencedColumnName="idparty")
    private Organization organization;

    public Organization getOrganization() {
        return organization;
    }

    public SuspectOrganization organization(Organization organization) {
        this.organization = organization;
        return this;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Override
    public String toString() {
        return "SuspectOrganization{" +
            "idSuspect=" + this.getIdSuspect() +
            '}';
    }
}
