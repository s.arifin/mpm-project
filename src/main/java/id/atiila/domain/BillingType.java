package id.atiila.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;

/**
 * BeSmart Team
 * Class definition for Entity BillingType.
 */

@Entity
@Table(name = "billing_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "billingtype")
public class BillingType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idbiltyp")
    private Integer idBillingType;

    @Column(name = "description")
    private String description;

    public BillingType() {
    }

    public BillingType(Integer idBillingType) {
        this.idBillingType = idBillingType;
    }

    public Integer getIdBillingType() {
        return this.idBillingType;
    }

    public void setIdBillingType(Integer id) {
        this.idBillingType = id;
    }

    public String getDescription() {
        return description;
    }

    public BillingType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        BillingType that = (BillingType) o;

        return new EqualsBuilder()
            .append(idBillingType, that.idBillingType)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idBillingType)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "BillingType{" +
            "idBillingType=" + this.idBillingType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
