package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity Receipt.
 */

@Entity
@Table(name = "receipt")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "receipt")
public class Receipt extends Payment {

    @Column(name = "idreq")
    private UUID idRequirement;

    //getter and setter
    public UUID getIdRequirement() {
        return idRequirement;
    }

    public void setIdRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
    }

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Receipt receipt = (Receipt) o;
        if (receipt.getIdPayment() == null || this.getIdPayment() == null) {
            return false;
        }
        return Objects.equals(this.getIdPayment(), receipt.getIdPayment());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdPayment());
    }

    @Override
    public String toString() {
        return "Receipt{" +
            "idPayment=" + this.getIdPayment() +
            ", paymentNumber='" + getPaymentNumber() + "'" +
            ", refferenceNumber='" + getRefferenceNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", amount='" + getAmount() + "'" +
            '}';
    }
}
