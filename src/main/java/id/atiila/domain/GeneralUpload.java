package id.atiila.domain;

import org.hibernate.annotations.*;

import org.hibernate.annotations.Cache;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;import java.util.UUID;

import java.util.Objects;



/**
 * atiila consulting
 * Class definition for Entity GeneralUpload.
 */

@Entity
@Table(name = "general_upload")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "generalupload")
public class GeneralUpload implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idgenupl", columnDefinition = "BINARY(16)")
    private UUID idGeneralUpload;

    @Column(name = "dtcreate")
    @CreationTimestamp
    private ZonedDateTime dateCreate;

    @Column(name = "description")
    private String description;

    @Lob
    @Column(name = "value")
    private byte[] value;

    @Column(name = "valuecontenttype")
    private String valueContentType;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idpurposetype", referencedColumnName="idpurposetype")
    private PurposeType purpose;

    public UUID getIdGeneralUpload() {
        return this.idGeneralUpload;
    }

    public void setIdGeneralUpload(UUID id) {
        this.idGeneralUpload = id;
    }

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<GeneralUploadStatus> statuses = new ArrayList<GeneralUploadStatus>();

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public GeneralUpload dateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
        return this;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDescription() {
        return description;
    }

    public GeneralUpload description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getValue() {
        return value;
    }

    public GeneralUpload value(byte[] value) {
        this.value = value;
        return this;
    }

    public void setValue(byte[] value) {
        this.value = value;
    }

    public String getValueContentType() {
        return valueContentType;
    }

    public GeneralUpload valueContentType(String valueContentType) {
        this.valueContentType = valueContentType;
        return this;
    }

    public void setValueContentType(String valueContentType) {
        this.valueContentType = valueContentType;
    }

    public Internal getInternal() {
        return internal;
    }

    public GeneralUpload internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public PurposeType getPurpose() {
        return purpose;
    }

    public GeneralUpload purpose(PurposeType purposeType) {
        this.purpose = purposeType;
        return this;
    }

    public void setPurpose(PurposeType purposeType) {
        this.purpose = purposeType;
    }

    public List<GeneralUploadStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<GeneralUploadStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);

        for (GeneralUploadStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now();

        for (GeneralUploadStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        GeneralUploadStatus current = new GeneralUploadStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GeneralUpload generalUpload = (GeneralUpload) o;
        if (generalUpload.idGeneralUpload == null || this.idGeneralUpload == null) {
            return false;
        }
        return Objects.equals(this.idGeneralUpload, generalUpload.idGeneralUpload);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idGeneralUpload);
    }

    @Override
    public String toString() {
        return "GeneralUpload{" +
            "idGeneralUpload=" + this.idGeneralUpload +
            ", dateCreate='" + getDateCreate() + "'" +
            ", description='" + getDescription() + "'" +
            ", value='" + getValue() + "'" +
            ", valueContentType='" + getValueContentType() + "'" +
            '}';
    }
}
