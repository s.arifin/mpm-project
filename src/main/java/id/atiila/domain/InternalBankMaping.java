package id.atiila.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A InternalBankMaping.
 */
@Entity
@Table(name = "internal_bank_mapping")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "internalbankmaping")
public class InternalBankMaping implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id

    @Column(name = "idinternal")
    private String idinternal;

    @Column(name = "idbank")
    private String idbank;

    @Column(name = "bankname")
    private String bankname;

    @Column(name = "accountnumber")
    private String accountnumber;

    @Column(name = "glaccount")
    private String glaccount;

    @Column(name = "customermulia")
    private String customermulia;

    @Column(name = "deadstockmulia")
    private String deadstockmulia;

    @Column(name = "isppn")
    private Integer isppn;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove

    public String getIdinternal() {
        return idinternal;
    }

    public InternalBankMaping idinternal(String idinternal) {
        this.idinternal = idinternal;
        return this;
    }

    public void setIdinternal(String idinternal) {
        this.idinternal = idinternal;
    }

    public String getIdbank() {
        return idbank;
    }

    public InternalBankMaping idbank(String idbank) {
        this.idbank = idbank;
        return this;
    }

    public void setIdbank(String idbank) {
        this.idbank = idbank;
    }

    public String getBankname() {
        return bankname;
    }

    public InternalBankMaping bankname(String bankname) {
        this.bankname = bankname;
        return this;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getAccountnumber() {
        return accountnumber;
    }

    public InternalBankMaping accountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
        return this;
    }

    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }

    public String getGlaccount() {
        return glaccount;
    }

    public InternalBankMaping glaccount(String glaccount) {
        this.glaccount = glaccount;
        return this;
    }

    public void setGlaccount(String glaccount) {
        this.glaccount = glaccount;
    }

    public String getCustomermulia() {
        return customermulia;
    }

    public InternalBankMaping customermulia(String customermulia) {
        this.customermulia = customermulia;
        return this;
    }

    public void setCustomermulia(String customermulia) {
        this.customermulia = customermulia;
    }

    public String getDeadstockmulia() {
        return deadstockmulia;
    }

    public InternalBankMaping deadstockmulia(String deadstockmulia) {
        this.deadstockmulia = deadstockmulia;
        return this;
    }

    public void setDeadstockmulia(String deadstockmulia) {
        this.deadstockmulia = deadstockmulia;
    }

    public Integer getIsppn() {
        return isppn;
    }

    public InternalBankMaping isppn(Integer isppn) {
        this.isppn = isppn;
        return this;
    }

    public void setIsppn(Integer isppn) {
        this.isppn = isppn;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InternalBankMaping internalBankMaping = (InternalBankMaping) o;

        return new EqualsBuilder()
            .append(idinternal, internalBankMaping.idinternal)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idinternal)
            .toHashCode();
    }
    @Override
    public String toString() {
        return "InternalBankMaping{" +
            ", idinternal='" + getIdinternal() + "'" +
            ", idbank='" + getIdbank() + "'" +
            ", bankname='" + getBankname() + "'" +
            ", accountnumber='" + getAccountnumber() + "'" +
            ", glaccount='" + getGlaccount() + "'" +
            ", customermulia='" + getCustomermulia() + "'" +
            ", deadstockmulia='" + getDeadstockmulia() + "'" +
            ", isppn='" + getIsppn() + "'" +
            "}";
    }
}
