package id.atiila.domain;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.WhereJoinTable;
import org.springframework.data.elasticsearch.annotations.Document;

import id.atiila.base.BaseConstants;

import java.io.Serializable;
import java.util.Set;

/**
 * BeSmart Team
 * Class definition for Entity Vendor.
 */

@Entity
@Table(name = "vendor")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "vendor")
public class Vendor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idvendor")
    private String idVendor;

    @Column(name = "idroletype")
    private Integer idRoleType;

    @Column(name = "idvndtyp")
    private Integer idVendorType;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "idparty")
    private Organization organization;

    @Column(name = "ismaindealer")
    private Boolean isMainDealer;

    @Column(name = "isbirojasa")
    private Boolean isBiroJasa;

    public String getIdVendor() {
        return idVendor;
    }

    public Vendor idVendor(String idVendor) {
        this.idVendor = idVendor;
        return this;
    }

    public Boolean getMainDealer() {
        return isMainDealer;
    }

    public void setMainDealer(Boolean mainDealer) {
        isMainDealer = mainDealer;
    }

    public Boolean getBiroJasa() {
        return isBiroJasa;
    }

    public void setBiroJasa(Boolean biroJasa) {
        isBiroJasa = biroJasa;
    }

    public void setIdVendor(String idVendor) {
        this.idVendor = idVendor;
    }

    public Integer getIdRoleType() {
        return idRoleType;
    }

    public void setIdRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
    }

    public Integer getIdVendorType() { return idVendorType; }

    public void setIdVendorType(Integer idVendorType) { this.idVendorType = idVendorType; }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Vendor vendor = (Vendor) o;

        return new EqualsBuilder()
            .append(idVendor, vendor.idVendor)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idVendor)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "Vendor{" +
            "idVendor='" + getIdVendor() + "'" +
            '}';
    }
}
