package id.atiila.domain;

import id.atiila.base.BaseConstants;
import id.atiila.base.DomainEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.time.ZoneId;
import java.util.UUID;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * BeSmart Team
 * Class definition for Entity SalesUnitLeasing.
 */

@Entity
@Table(name = "sales_unit_leasing")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "salesunitleasing")
public class SalesUnitLeasing implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idsallea", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idSalesLeasing;

    @Column(name = "idstatustype")
    private Integer idStatusType;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idreq", referencedColumnName="idreq")
    private SalesUnitRequirement owner;

    @ManyToOne
    @JoinColumn(name="idleacom", referencedColumnName="idparrol")
    private LeasingCompany leasingCompany;

    public UUID getIdSalesLeasing() {
        return this.idSalesLeasing;
    }

    public void setIdSalesLeasing(UUID id) {
        this.idSalesLeasing = id;
    }

    public Integer getIdStatusType() {
        return idStatusType;
    }

    public SalesUnitLeasing idStatusType(Integer idStatusType) {
        this.idStatusType = idStatusType;
        return this;
    }

    public void setIdStatusType(Integer idStatusType) {
        this.idStatusType = idStatusType;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public SalesUnitLeasing dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public SalesUnitLeasing dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public SalesUnitRequirement getOwner() {
        return owner;
    }

    public SalesUnitLeasing owner(SalesUnitRequirement salesUnitRequirement) {
        this.owner = salesUnitRequirement;
        return this;
    }

    public void setOwner(SalesUnitRequirement salesUnitRequirement) {
        this.owner = salesUnitRequirement;
    }

    public LeasingCompany getLeasingCompany() {
        return leasingCompany;
    }

    public SalesUnitLeasing leasingCompany(LeasingCompany leasingCompany) {
        this.leasingCompany = leasingCompany;
        return this;
    }

    public void setLeasingCompany(LeasingCompany leasingCompany) {
        this.leasingCompany = leasingCompany;
    }

    @PreUpdate
    @PrePersist
    public void preUpdate() {
        if (this.idStatusType == null) this.idStatusType = BaseConstants.STATUS_DRAFT;
        if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
        if (this.dateThru == null) this.dateThru = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        SalesUnitLeasing that = (SalesUnitLeasing) o;

        return new EqualsBuilder()
            .append(idSalesLeasing, that.idSalesLeasing)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idSalesLeasing)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "SalesUnitLeasing{" +
            "idSalesLeasing=" + this.idSalesLeasing +
            ", idStatusType='" + getIdStatusType() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
