package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;

import java.time.ZonedDateTime;
import java.util.UUID;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;

/**
 * BeSmart Team
 * Class definition for Entity Prospect.
 */

@Entity
@Table(name = "prospect")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "prospect")
public class Prospect extends AuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idprospect", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idProspect;

    @Column(name = "prospectnumber")
    private String prospectNumber;

    @Column(name = "prospectcount")
    private Integer prospectCount;

    @Column(name = "eveloc")
    private String eventLocation;

    @Column(name="dtfollowup")
    private ZonedDateTime dateFollowUp;

    @Column(name="idsuspect")
    private UUID idSuspect;

    @Column(name="idsalescoordinator")
    private UUID idSalesCoordinator;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal dealer;

    @ManyToOne
    @JoinColumn(name="idbroker", referencedColumnName="idparrol")
    private SalesBroker broker;

    @ManyToOne
    @JoinColumn(name="idprosou", referencedColumnName="idprosou")
    private ProspectSource prospectSource;

    @ManyToOne
    @JoinColumn(name="idevetyp", referencedColumnName="idevetyp")
    private EventType eventType;

    @ManyToOne
    @JoinColumn(name="idfacility", referencedColumnName="idfacility")
    private Facility facility;

    @ManyToOne
    @JoinColumn(name="idsalesman", referencedColumnName="idparrol")
    private Salesman salesman;

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<ProspectStatus> statuses = new ArrayList<ProspectStatus>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<ProspectRole> roles = new ArrayList<ProspectRole>();

    @Column(name = "distribution")
    private Integer distribution;

    public Integer getDistribution() {
        return distribution;
    }

    public void setDistribution(Integer distribution) {
        this.distribution = distribution;
    }

    public UUID getIdProspect() {
        return this.idProspect;
    }

    public void setIdProspect(UUID id) {
        this.idProspect = id;
    }

    public String getProspectNumber() {
        return prospectNumber;
    }

    public void setProspectNumber(String prospectNumber) {
        this.prospectNumber = prospectNumber;
    }

    public Integer getProspectCount() { return prospectCount; }

    public Prospect prospectCount(Integer prospectCount) {
        this.prospectCount = prospectCount;
        return this;
    }

    public void setProspectCount(Integer prospectCount) { this.prospectCount = prospectCount; }

    public String getEventLocation() { return eventLocation; }

    public Prospect eventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
        return this;
    }

    public void setEventLocation(String eventLocation) { this.eventLocation = eventLocation; }

    public ZonedDateTime getDateFollowUp() { return dateFollowUp; }

    public Prospect dateFollowUp(ZonedDateTime dateFollowUp) {
        this.dateFollowUp = dateFollowUp;
        return this;
    }

    public void setDateFollowUp(ZonedDateTime dateFollowUp) { this.dateFollowUp = dateFollowUp; }

    public UUID getIdSuspect() {
        return idSuspect;
    }

    public void setIdSuspect(UUID idSuspect) {
        this.idSuspect = idSuspect;
    }

    public Internal getDealer() {
        return dealer;
    }

    public Prospect dealer(Internal internal) {
        this.dealer = internal;
        return this;
    }

    public void setDealer(Internal internal) {
        this.dealer = internal;
    }

    public SalesBroker getBroker() {
        return broker;
    }

    public Prospect broker(SalesBroker salesBroker) {
        this.broker = salesBroker;
        return this;
    }

    public void setBroker(SalesBroker salesBroker) {
        this.broker = salesBroker;
    }

    public ProspectSource getProspectSource() {
        return prospectSource;
    }

    public Prospect prospectSource(ProspectSource prospectSource) {
        this.prospectSource = prospectSource;
        return this;
    }

    public void setProspectSource(ProspectSource prospectSource) {
        this.prospectSource = prospectSource;
    }

    public EventType getEventType() {
        return eventType;
    }

    public Prospect eventType(EventType eventType) {
        this.eventType = eventType;
        return this;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public Facility getFacility() {
        return facility;
    }

    public Prospect facility(Facility facility) {
        this.facility = facility;
        return this;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    public UUID getIdSalesCoordinator() { return idSalesCoordinator; }

    public Prospect idSalesCoordinator(UUID idSalesCoordinator) {
        this.idSalesCoordinator = idSalesCoordinator;
        return this;
    }

    public void setIdSalesCoordinator(UUID idSalesCoordinator) { this.idSalesCoordinator = idSalesCoordinator; }

    public Salesman getSalesman() {
        return salesman;
    }

    public Prospect salesman(Salesman salesman) {
        this.salesman = salesman;
        return this;
    }

    public void setSalesman(Salesman salesman) {
        this.salesman = salesman;
    }

    public List<ProspectRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<ProspectRole> roles) {
        this.roles = roles;
    }

    public void setPartyRole(Party party, Integer role)  {
        for (ProspectRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) return;
        }

        ProspectRole current = new ProspectRole();
        current.setOwner(this);
        current.setIdRoleType(role);
		current.setParty(party);
        current.setDateFrom(LocalDateTime.now());
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        roles.add(current);
    }

    public Party getPartyRole(Party party, Integer role)  {
        for (ProspectRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) return current.getParty();
        }
        return null;
    }

    public void removePartyRole(Party party, Integer role)  {
        for (ProspectRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getParty().equals(party);
            if (found) {
				current.setDateThru(LocalDateTime.now());
				return;
			}
        }
    }

    public List<ProspectStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<ProspectStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now().plusNanos(1);

        for (ProspectStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        LocalDateTime now = LocalDateTime.now();

        for (ProspectStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        ProspectStatus current = new ProspectStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    public Boolean getActiveData() {
        switch (getCurrentStatus()) {
            case BaseConstants.STATUS_DRAFT:
            case BaseConstants.STATUS_ACTIVE:
                return true;
        }
        return false;
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
        if (this.getDistribution() == null) this.setDistribution(0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Prospect prospect = (Prospect) o;
        if (prospect.idProspect == null || this.idProspect == null) {
            return false;
        }
        return Objects.equals(this.idProspect, prospect.idProspect);
    }


    @Override
    public int hashCode() {
        return Objects.hashCode(this.idProspect);
    }

    @Override
    public String toString() {
        return "Prospect{" +
            "idProspect=" + this.idProspect +
            "prospectNumber=" + getProspectNumber() + "'" +
            '}';
    }
}
