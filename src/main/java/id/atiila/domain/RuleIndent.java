package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity RuleIndent.
 */

@Entity
@Table(name = "rule_indent")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "ruleindent")
public class RuleIndent extends Rules {

    private static final long serialVersionUID = 1L;

    @Column(name = "idproduct")
    private String idProduct;

    @Column(name = "minpayment")
    private BigDecimal minPayment;

    public BigDecimal getMinPayment() {
        return minPayment;
    }

    public void setMinPayment(BigDecimal minPayment) {
        this.minPayment = minPayment;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public RuleIndent idProduct(String idProduct) {
        this.idProduct = idProduct;
        return this;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    @Override
    public String toString() {
        return "RuleIndent{" +
            "idRule=" + this.getIdRule() +
            ", idProduct='" + getIdProduct() + "'" +
            ", idInternal='" + getIdInternal() + "'" +
            '}';
    }
}
