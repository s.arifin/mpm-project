package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity RequirementType.
 */

@Entity
@Table(name = "requirement_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "requirementtype")
public class RequirementType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @Column(name = "idreqtyp")
    private Integer idRequirementType;

    @Column(name = "description")
    private String description;

    public RequirementType() {
    }

    public RequirementType(Integer idRequirementType) {
        this.idRequirementType = idRequirementType;
    }

    public Integer getIdRequirementType() {
        return this.idRequirementType;
    }

    public void setIdRequirementType(Integer id) {
        this.idRequirementType = id;
    }

    public String getDescription() {
        return description;
    }

    public RequirementType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RequirementType requirementType = (RequirementType) o;
        if (requirementType.idRequirementType == null || this.idRequirementType == null) {
            return false;
        }
        return Objects.equals(this.idRequirementType, requirementType.idRequirementType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idRequirementType);
    }

    @Override
    public String toString() {
        return "RequirementType{" +
            "idRequirementType=" + this.idRequirementType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
