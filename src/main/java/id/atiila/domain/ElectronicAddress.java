package id.atiila.domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * BeSmart Team
 * Class definition for Entity ElectronicAddress.
 */

@Entity
@Table(name = "electronic_address")
@DiscriminatorValue("2")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "electronicaddress")
public class ElectronicAddress extends ContactMechanism {

    private static final long serialVersionUID = 1L;

    @Column(name = "address")
    private String address;

    public String getAddress() {
        return address;
    }

    public ElectronicAddress address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getDescription() {
        return this.address;
    }

    @Override
    public String toString() {
        return "ElectronicAddress{" +
            "idContact=" + this.getIdContact() +
            ", address='" + getAddress() + "'" +
            '}';
    }
}
