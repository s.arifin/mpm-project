package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity SalesOrder.
 */

@Entity
@Table(name = "sales_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "salesorder")
public class SalesOrder extends CustomerOrder {

    private static final long serialVersionUID = 1L;

    @Column(name="idsaletype", insertable = false, updatable = false)
    private Integer idSaleType;

    @ManyToOne
    @JoinColumn(name="idsaletype", referencedColumnName="idsaletype")
    private SaleType saleType;

    public SaleType getSaleType() {
        return saleType;
    }

    public SalesOrder saleType(SaleType saleType) {
        this.saleType = saleType;
        return this;
    }

    public void setSaleType(SaleType saleType) {
        this.saleType = saleType;
    }

    public Integer getIdSaleType() {
        return idSaleType;
    }

    public void setIdSaleType(Integer idSaleType) {
        this.idSaleType = idSaleType;
    }

    @Override
    public String toString() {
        return "SalesOrder{" +
            "idOrder=" + this.getIdOrder() +
            '}';
    }
}
