package id.atiila.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * BeSmart Team
 * Class definition for Entity WorkOrder.
 */

@Entity
@Table(name = "work_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "workorder")
public class WorkOrder extends VehicleWorkRequirement {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "WorkOrder{" +
            "idRequirement=" + this.getIdRequirement() +
            ", vehicleNumber='" + getVehicleNumber() + "'" +
            '}';
    }
}
