package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity ProspectSource.
 */

@Entity
@Table(name = "prospect_source")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "prospectsource")
public class ProspectSource implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idprosou")
    private Integer idProspectSource;

    @Column(name = "description")
    private String description;

    public Integer getIdProspectSource() {
        return this.idProspectSource;
    }

    public void setIdProspectSource(Integer id) {
        this.idProspectSource = id;
    }

    public String getDescription() {
        return description;
    }

    public ProspectSource description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProspectSource prospectSource = (ProspectSource) o;
        if (prospectSource.idProspectSource == null || this.idProspectSource == null) {
            return false;
        }
        return Objects.equals(this.idProspectSource, prospectSource.idProspectSource);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idProspectSource);
    }

    @Override
    public String toString() {
        return "ProspectSource{" +
            "idProspectSource=" + this.idProspectSource +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
