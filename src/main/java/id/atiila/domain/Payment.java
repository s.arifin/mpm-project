package id.atiila.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity Payment.
 */

@Entity
@Table(name = "payment")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "payment")
public class Payment extends AuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idpayment", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idPayment;

    @Column(name = "paymentnumber")
    private String paymentNumber;

    @Column(name = "reffnum")
    private String refferenceNumber;

    @Column(name = "dtcreate")
    private ZonedDateTime dateCreate;

    @Column(name = "amount", precision=10, scale=2)
    private BigDecimal amount;

    @OneToMany(mappedBy = "payment")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PaymentApplication> details = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="idpaytyp", referencedColumnName="idpaytyp")
    private PaymentType paymentType;

    @ManyToOne
    @JoinColumn(name="idpaymet", referencedColumnName="idpaymet")
    private PaymentMethod method;

    @ManyToOne
    @JoinColumn(name="idinternal", referencedColumnName="idinternal")
    private Internal internal;

    @ManyToOne
    @JoinColumn(name="idpaito", referencedColumnName="idbillto")
    private BillTo paidTo;

    @ManyToOne
    @JoinColumn(name="idpaifro", referencedColumnName="idbillto")
    private BillTo paidFrom;

    public UUID getIdPayment() {
        return this.idPayment;
    }

    public void setIdPayment(UUID id) {
        this.idPayment = id;
    }

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<PaymentStatus> statuses = new ArrayList<PaymentStatus>();

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<PaymentRole> roles = new ArrayList<PaymentRole>();

    public String getPaymentNumber() {
        return paymentNumber;
    }

    public Payment paymentNumber(String paymentNumber) {
        this.paymentNumber = paymentNumber;
        return this;
    }

    public void setPaymentNumber(String paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public String getRefferenceNumber() {
        return refferenceNumber;
    }

    public Payment refferenceNumber(String refferenceNumber) {
        this.refferenceNumber = refferenceNumber;
        return this;
    }

    public void setRefferenceNumber(String refferenceNumber) {
        this.refferenceNumber = refferenceNumber;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public Payment dateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
        return this;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Payment amount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Set<PaymentApplication> getDetails() {
        return details;
    }

    public Payment details(Set<PaymentApplication> paymentApplications) {
        this.details = paymentApplications;
        return this;
    }

    public Payment addDetail(PaymentApplication paymentApplication) {
        this.details.add(paymentApplication);
        paymentApplication.setPayment(this);
        return this;
    }

    public Payment removeDetail(PaymentApplication paymentApplication) {
        this.details.remove(paymentApplication);
        paymentApplication.setPayment(null);
        return this;
    }

    public void setDetails(Set<PaymentApplication> paymentApplications) {
        this.details = paymentApplications;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public Payment paymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public PaymentMethod getMethod() {
        return method;
    }

    public Payment method(PaymentMethod paymentMethod) {
        this.method = paymentMethod;
        return this;
    }

    public void setMethod(PaymentMethod paymentMethod) {
        this.method = paymentMethod;
    }

    public Internal getInternal() {
        return internal;
    }

    public Payment internal(Internal internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Internal internal) {
        this.internal = internal;
    }

    public BillTo getPaidTo() {
        return paidTo;
    }

    public Payment paidTo(BillTo billTo) {
        this.paidTo = billTo;
        return this;
    }

    public void setPaidTo(BillTo billTo) {
        this.paidTo = billTo;
    }

    public BillTo getPaidFrom() {
        return paidFrom;
    }

    public Payment paidFrom(BillTo billTo) {
        this.paidFrom = billTo;
        return this;
    }

    public void setPaidFrom(BillTo billTo) {
        this.paidFrom = billTo;
    }

    public List<PaymentRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<PaymentRole> roles) {
        this.roles = roles;
    }

    public void setPartyRole(String user, Integer role)  {
        for (PaymentRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) return;
        }

        PaymentRole current = new PaymentRole();
        current.setOwner(this);
        current.setIdRoleType(role);
		current.setUser(user);
        current.setDateFrom(ZonedDateTime.now());
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        roles.add(current);
    }

    public String getPartyRole(String user, Integer role)  {
        for (PaymentRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) return current.getUser();
        }
        return null;
    }

    public void removePartyRole(String user, Integer role)  {
        for (PaymentRole current: this.getRoles()) {
            Boolean found = current.getIdRoleType().equals(role) && current.getUser().equals(user);
            if (found) {
				current.setDateThru(ZonedDateTime.now());
				return;
			}
        }
    }

    public List<PaymentStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<PaymentStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);

        for (PaymentStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now();

        for (PaymentStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        PaymentStatus current = new PaymentStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Payment payment = (Payment) o;
        if (payment.idPayment == null || this.idPayment == null) {
            return false;
        }
        return Objects.equals(this.idPayment, payment.idPayment);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPayment);
    }

    @Override
    public String toString() {
        return "Payment{" +
            "idPayment=" + this.idPayment +
            ", paymentNumber='" + getPaymentNumber() + "'" +
            ", refferenceNumber='" + getRefferenceNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", amount='" + getAmount() + "'" +
            '}';
    }
}
