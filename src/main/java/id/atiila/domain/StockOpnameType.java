package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity StockOpnameType.
 */

@Entity
@Table(name = "stock_opname_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "stockopnametype")
public class StockOpnameType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idstkopntyp")
    private Integer idStockOpnameType;

    @Column(name = "description")
    private String description;

    public Integer getIdStockOpnameType() {
        return this.idStockOpnameType;
    }

    public void setIdStockOpnameType(Integer id) {
        this.idStockOpnameType = id;
    }

    public String getDescription() {
        return description;
    }

    public StockOpnameType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StockOpnameType stockOpnameType = (StockOpnameType) o;
        if (stockOpnameType.idStockOpnameType == null || this.idStockOpnameType == null) {
            return false;
        }
        return Objects.equals(this.idStockOpnameType, stockOpnameType.idStockOpnameType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idStockOpnameType);
    }

    @Override
    public String toString() {
        return "StockOpnameType{" +
            "idStockOpnameType=" + this.idStockOpnameType +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
