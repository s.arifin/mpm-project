package id.atiila.domain;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity PostalAddress.
 */

@Entity
@Table(name = "postal_address")
@DiscriminatorValue("1")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "postaladdress")
public class PostalAddress extends ContactMechanism {

    private static final long serialVersionUID = 1L;

    @Column(name = "address1")
    private String address1;

    @Column(name = "address2")
    private String address2;

    @ManyToOne
    @JoinColumn(name="iddistrict", referencedColumnName="idgeobou")
    private District district;

    @ManyToOne
    @JoinColumn(name="idvillage", referencedColumnName="idgeobou")
    private Village village;

    @ManyToOne
    @JoinColumn(name="idcity", referencedColumnName="idgeobou")
    private City city;

    @ManyToOne
    @JoinColumn(name="idprovince", referencedColumnName="idgeobou")
    private Province province;

    public String getAddress1() {
        return address1;
    }

    public PostalAddress address1(String address1) {
        this.address1 = address1;
        return this;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public PostalAddress address2(String address2) {
        this.address2 = address2;
        return this;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public District getDistrict() { return district; }

    public void setDistrict(District district) { this.district = district; }

    public Village getVillage() { return village; }

    public void setVillage(Village village) { this.village = village; }

    public City getCity() { return city; }

    public void setCity(City city) { this.city = city; }

    public Province getProvince() { return province; }

    public void setProvince(Province province) { this.province = province; }

    @Override
    public String getDescription() {
        StringBuffer s = new StringBuffer();
        if (this.address1 != null) s.append(this.address1);
        if (this.address2 != null) {
            if (s.toString() != null) s.append(", ");
            s.append(this.address2);
        }
        if (this.district != null) {
            if (s.toString() != null) s.append(", ");
            s.append(this.district.getDescription());
        }
        if (this.village != null)  {
            if (s.toString() != null) s.append(", ");
            s.append(this.village.getDescription());
        }
        if (this.city != null)  {
            if (s.toString() != null) s.append(", ");
            s.append(this.city.getDescription());
        }
        if (this.province != null)  {
            if (s.toString() != null) s.append(", ");
            s.append(this.province.getDescription());
        }
        return s.toString() ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PostalAddress that = (PostalAddress) o;

        return new EqualsBuilder()
            .append(getIdContact(), that.getIdContact())
            .isEquals();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdContact());
    }

    @Override
    public String toString() {
        return "PostalAddress{" +
            "idContact=" + this.getIdContact() +
            ", address1='" + getAddress1() + "'" +
            ", address2='" + getAddress2() + "'" +
//            ", district='" + getDistrict().getDescription() + "'" +
//            ", village='" + getVillage().getDescription() + "'" +
//            ", city='" + getCity().getDescription() + "'" +
//            ", province='" + getProvince().getDescription() + "'" +
            '}';
    }
}
