package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity GoodContainer.
 */

@Entity
@Table(name = "good_container")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "goodcontainer")
public class GoodContainer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idgoocon", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idGoodContainer;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idproduct", referencedColumnName="idproduct")
    private Good good;

    @ManyToOne
    @JoinColumn(name="idcontainer", referencedColumnName="idcontainer")
    private Container container;

    @ManyToOne
    @JoinColumn(name="idparty", referencedColumnName="idparty")
    private Organization organization;

    public UUID getIdGoodContainer() {
        return this.idGoodContainer;
    }

    public void setIdGoodContainer(UUID id) {
        this.idGoodContainer = id;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public GoodContainer dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public GoodContainer dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Good getGood() {
        return good;
    }

    public GoodContainer good(Good good) {
        this.good = good;
        return this;
    }

    public void setGood(Good good) {
        this.good = good;
    }

    public Container getContainer() {
        return container;
    }

    public GoodContainer container(Container container) {
        this.container = container;
        return this;
    }

    public void setContainer(Container container) {
        this.container = container;
    }

    public Organization getOrganization() {
        return organization;
    }

    public GoodContainer organization(Organization organization) {
        this.organization = organization;
        return this;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GoodContainer goodContainer = (GoodContainer) o;
        if (goodContainer.idGoodContainer == null || this.idGoodContainer == null) {
            return false;
        }
        return Objects.equals(this.idGoodContainer, goodContainer.idGoodContainer);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idGoodContainer);
    }

    @Override
    public String toString() {
        return "GoodContainer{" +
            "idGoodContainer=" + this.idGoodContainer +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
