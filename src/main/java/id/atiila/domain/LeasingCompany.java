package id.atiila.domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import id.atiila.base.BaseConstants;

/**
 * BeSmart Team
 * Class definition for Entity LeasingCompany.
 */

@Entity
@Table(name = "leasing_company")
@DiscriminatorValue(BaseConstants.ROLE_LEASING_COMPANY)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "leasingcompany")
public class LeasingCompany extends PartyRole {

    private static final long serialVersionUID = 1L;

    @Column(name = "idleacom")
    private String idLeasingCompany;

    @Column(name = "idleasingahm")
    private String idLeasingAhm;

    @Column(name = "idleasingfincoy")
    private String idLeasingFincoy;

    public String getIdLeasingAhm() {
        return idLeasingAhm;
    }

    public void setIdLeasingAhm(String idLeasingAhm) {
        this.idLeasingAhm = idLeasingAhm;
    }

    public String getIdLeasingCompany() {
        return idLeasingCompany;
    }

    public LeasingCompany idLeasingCompany(String idLeasingCompany) {
        this.idLeasingCompany = idLeasingCompany;
        return this;
    }

    public String getIdLeasingFincoy() {
        return idLeasingFincoy;
    }

    public void setIdLeasingFincoy(String idLeasingFincoy) {
        this.idLeasingFincoy = idLeasingFincoy;
    }

    public Organization getOrganization() {
        return (Organization) getParty();
    }

    public void setOrganization(Organization o) {
        setParty(o);
    }

    public void setIdLeasingCompany(String idLeasingCompany) {
        this.idLeasingCompany = idLeasingCompany;
    }

    @Override
    public String toString() {
        return "LeasingCompany{" +
            "idPartyRole=" + this.getIdLeasingCompany() +
            ", idLeasingCompany='" + getIdLeasingCompany() + "'" +
            '}';
    }
}
