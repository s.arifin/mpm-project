package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.*;
import java.util.Objects;

/**
 * BeSmart Team
 * Class definition for Entity PersonalCustomer.
 */

@Entity
@Table(name = "personal_customer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "personalcustomer")
public class PersonalCustomer extends Customer {

    private static final long serialVersionUID = 1L;

    public Person getPerson() {
        return (Person) getParty();
    }

    public void setPerson(Person person) {
        this.setParty(person);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PersonalCustomer customer = (PersonalCustomer) o;

        if (customer.getIdCustomer() == null || this.getIdCustomer() == null) {
            return false;
        }
        return Objects.equals(this.getIdCustomer(), customer.getIdCustomer());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getIdCustomer());
    }

    @Override
    public String toString() {
        return "PersonalCustomer{" +
            "idCustomer=" + this.getIdCustomer() +
            ", idRoleType='" + getIdRoleType() + "'" +
            '}';
    }
}
