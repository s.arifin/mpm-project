package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity PositionFullfillment.
 */

@Entity
@Table(name = "position_fullfillment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "positionfullfillment")
public class PositionFullfillment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idposfulfil")
    private Integer idPositionFullfillment;

    @Column(name = "dtfrom")
    private ZonedDateTime dateFrom;

    @Column(name = "dtthru")
    private ZonedDateTime dateThru;

    @ManyToOne
    @JoinColumn(name="idposition", referencedColumnName="idposition")
    private Position position;

    @ManyToOne
    @JoinColumn(name="idperson", referencedColumnName="idparty")
    private Person person;

    public Integer getIdPositionFullfillment() {
        return this.idPositionFullfillment;
    }

    public void setIdPositionFullfillment(Integer id) {
        this.idPositionFullfillment = id;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public PositionFullfillment dateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public PositionFullfillment dateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
        return this;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Position getPosition() {
        return position;
    }

    public PositionFullfillment position(Position position) {
        this.position = position;
        return this;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Person getPerson() {
        return person;
    }

    public PositionFullfillment person(Person person) {
        this.person = person;
        return this;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @PrePersist
    public void prePersist() {
        if (this.dateFrom == null) this.dateFrom = ZonedDateTime.now();
        if (this.dateThru == null)
            this.dateThru = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PositionFullfillment positionFullfillment = (PositionFullfillment) o;
        if (positionFullfillment.idPositionFullfillment == null || this.idPositionFullfillment == null) {
            return false;
        }
        return Objects.equals(this.idPositionFullfillment, positionFullfillment.idPositionFullfillment);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idPositionFullfillment);
    }

    @Override
    public String toString() {
        return "PositionFullfillment{" +
            "idPositionFullfillment=" + this.idPositionFullfillment +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            '}';
    }
}
