package id.atiila.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * BeSmart Team
 * Class definition for Entity CommunicationEventProspect.
 */

@Entity
@Table(name = "communication_event_prospect")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "communicationeventprospect")
public class CommunicationEventProspect extends CommunicationEvent {

    private static final long serialVersionUID = 1L;

    @Column(name = "idprospect")
    private UUID idProspect;

    @Column(name = "interest")
    private Boolean interest;

    @Column(name = "qty")
    private Integer qty;

    @Column(name = "purchaseplan")
    private String purchasePlan;

    @Column(name = "walkintype")
    private String walkInType;

    @Column(name = "note")
    private String note;

    @Column(name = "nextfollowup")
    private String nextFollowUp;

    public String getNextFollowUp() {
        return nextFollowUp;
    }

    public void setNextFollowUp(String nextFollowUp) {
        this.nextFollowUp = nextFollowUp;
    }

    @Override
    public String getNote() {

        return note;
    }

    @Override
    public void setNote(String note) {
        this.note = note;
    }


    @ManyToOne
    @JoinColumn(name="idsaletype", referencedColumnName="idsaletype")
    private SaleType saleType;

    @ManyToOne
    @JoinColumn(name="idcolor", referencedColumnName="idfeature")
    private Feature color;

    @ManyToOne
    @JoinColumn(name="idproduct", referencedColumnName="idproduct")
    private Motor product;

    public UUID getIdProspect() { return idProspect; }

    public CommunicationEventProspect idProspect(UUID idProspect) {
        this.idProspect = idProspect;
        return this;
    }

    public void setIdProspect(UUID idProspect) { this.idProspect = idProspect; }

    public Boolean getInterest() { return interest; }

    public CommunicationEventProspect interest(Boolean interest) {
        this.interest = interest;
        return this;
    }

    public void setInterest(Boolean interest) { this.interest = interest; }

    public Integer getQty() {
        return qty;
    }

    public CommunicationEventProspect qty(Integer qty) {
        this.qty = qty;
        return this;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getPurchasePlan() { return purchasePlan; }

    public CommunicationEventProspect purchasePlan(String purchasePlan) {
        this.purchasePlan = purchasePlan;
        return this;
    }

    public void setPurchasePlan(String purchasePlan) { this.purchasePlan = purchasePlan; }

    public SaleType getSaleType() {
        return saleType;
    }

    public CommunicationEventProspect saleType(SaleType saleType) {
        this.saleType = saleType;
        return this;
    }

    public void setSaleType(SaleType saleType) {
        this.saleType = saleType;
    }

    public Feature getColor() {
        return color;
    }

    public CommunicationEventProspect color(Feature feature) {
        this.color = feature;
        return this;
    }

    public void setColor(Feature feature) {
        this.color = feature;
    }

    public Motor getProduct() {
        return product;
    }

    public CommunicationEventProspect product(Motor motor) {
        this.product = motor;
        return this;
    }

    public void setProduct(Motor motor) {
        this.product = motor;
    }


    @Override
    public String toString() {
        return "CommunicationEventProspect{" +
            "idCommunicationEvent=" + this.getIdCommunicationEvent() +
            ", idprospect='" + getIdProspect() + "'" +
            ", interest='" + getInterest() + "'" +
            ", qty='" + getQty() + "'" +
            ", nextfollowup='" + getNextFollowUp() + "'" +
            ", note='" + getNote() + "'" +
            '}';
    }
}
