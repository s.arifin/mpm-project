package id.atiila.domain;

import id.atiila.base.DomainEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import org.hibernate.annotations.GenericGenerator;
import java.util.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import id.atiila.base.BaseConstants;

import javax.persistence.*;
import java.io.Serializable;import java.util.UUID;

import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity Facility.
 */

@Entity
@Table(name = "facility")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "facility")
public class Facility implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "idfacility", columnDefinition = DomainEntity.UUID_TYPE)
    private UUID idFacility;

    @Column(name = "code")
    private String facilityCode;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name="idfacilitytype", referencedColumnName="idfacilitytype")
    private FacilityType facilityType;

    @ManyToOne
    @JoinColumn(name="idpartof", referencedColumnName="idfacility")
    private Facility partOf;

    public UUID getIdFacility() {
        return this.idFacility;
    }

    public void setIdFacility(UUID id) {
        this.idFacility = id;
    }

    @JsonIgnore
    @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
    @Where(clause="current_timestamp between dtfrom and dtthru")
    private List<FacilityStatus> statuses = new ArrayList<FacilityStatus>();

    public String getFacilityCode() {
        return facilityCode;
    }

    public Facility facilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
        return this;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public String getDescription() {
        return description;
    }

    public Facility description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FacilityType getFacilityType() {
        return facilityType;
    }

    public Facility facilityType(FacilityType facilityType) {
        this.facilityType = facilityType;
        return this;
    }

    public void setFacilityType(FacilityType facilityType) {
        this.facilityType = facilityType;
    }

    public Facility getPartOf() {
        return partOf;
    }

    public Facility partOf(Facility facility) {
        this.partOf = facility;
        return this;
    }

    public void setPartOf(Facility facility) {
        this.partOf = facility;
    }

    public List<FacilityStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<FacilityStatus> statuses) {
        this.statuses = statuses;
    }

    public Integer getStatus() {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now().plusNanos(1);

        for (FacilityStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                return current.getIdStatusType();
            }
        }
        return result;
    }

    public void setStatus(Integer value)  {
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        ZonedDateTime now = ZonedDateTime.now();

        for (FacilityStatus current: getStatuses()) {
            if (now.isAfter(current.getDateFrom()) && now.isBefore(current.getDateThru())) {
                if (value.equals(current.getIdStatusType())) return;
                current.setDateThru(now.minusNanos(1));
            }
        }

        FacilityStatus current = new FacilityStatus();
        current.setOwner(this);
        current.setIdStatusType(value);
        current.setDateFrom(now);
        current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault()));
        statuses.add(current);
    }

    public Integer getCurrentStatus() {
        return getStatus();
    }

    @PrePersist
    @PreUpdate
    public void preUpdate() {
        if (getCurrentStatus().equals(BaseConstants.STATUS_NOT_DEFINED)) setStatus(BaseConstants.STATUS_DRAFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Facility facility = (Facility) o;
        if (facility.idFacility == null || this.idFacility == null) {
            return false;
        }
        return Objects.equals(this.idFacility, facility.idFacility);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idFacility);
    }

    @Override
    public String toString() {
        return "Facility{" +
            "idFacility=" + this.idFacility +
            ", facilityCode='" + getFacilityCode() + "'" +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
