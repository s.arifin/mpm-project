package id.atiila.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



/**
 * BeSmart Team
 * Class definition for Entity PartyCategory.
 */

@Entity
@Table(name = "party_category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "partycategory")
public class PartyCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcategory")
    private Integer idCategory;

    @Column(name = "refkey")
    private String refKey;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name="idcattyp", referencedColumnName="idcattyp")
    private PartyCategoryType categoryType;

    public Integer getIdCategory() {
        return this.idCategory;
    }

    public void setIdCategory(Integer id) {
        this.idCategory = id;
    }

    public String getRefKey() {
        return refKey;
    }

    public PartyCategory refKey(String refKey) {
        this.refKey = refKey;
        return this;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public String getDescription() {
        return description;
    }

    public PartyCategory description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PartyCategoryType getCategoryType() {
        return categoryType;
    }

    public PartyCategory categoryType(PartyCategoryType partyCategoryType) {
        this.categoryType = partyCategoryType;
        return this;
    }

    public void setCategoryType(PartyCategoryType partyCategoryType) {
        this.categoryType = partyCategoryType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PartyCategory partyCategory = (PartyCategory) o;
        if (partyCategory.idCategory == null || this.idCategory == null) {
            return false;
        }
        return Objects.equals(this.idCategory, partyCategory.idCategory);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.idCategory);
    }

    @Override
    public String toString() {
        return "PartyCategory{" +
            "idCategory=" + this.idCategory +
            ", refKey='" + getRefKey() + "'" +
            ", description='" + getDescription() + "'" +
            '}';
    }
}
