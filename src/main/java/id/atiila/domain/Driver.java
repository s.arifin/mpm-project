package id.atiila.domain;

import id.atiila.base.BaseConstants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;

/**
 * BeSmart Team
 * Class definition for Entity Driver.
 */

@Entity
@Table(name = "driver")
@DiscriminatorValue(BaseConstants.ROLE_DRIVER)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "driver")
public class Driver extends PartyRole {

    private static final long serialVersionUID = 1L;

    @Column(name = "iddriver")
    private String idDriver;

    @Column(name = "isexternal")
    private Boolean externalDriver;

    @Column(name = "idvendor ")
    private String idVendor;

    @Column(name = "username")
    private String username;

    public String getIdDriver() {
        return idDriver;
    }

    public Driver idDriver(String idDriver) {
        this.idDriver = idDriver;
        return this;
    }

    public void setIdDriver(String idDriver) {
        this.idDriver = idDriver;
    }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public Person getPerson() {
        return (Person) getParty();
    }

    public void setPerson(Person p) {
        setParty(p);
    }

    public Boolean getExternalDriver() {
        return externalDriver;
    }

    public void setExternalDriver(Boolean externalDriver) {
        this.externalDriver = externalDriver;
    }

    public String getIdVendor() {return idVendor;}

    public void setIdVendor(String idVendor) {this.idVendor = idVendor;}

    @Override
    public String toString() {
        return "Driver{" +
            "id=" + getIdPartyRole() +
            ", idDriver='" + getIdDriver() + "'" +
//            ", idVendor='" + getIdVendor() + "'" +
            "}";
    }
}
