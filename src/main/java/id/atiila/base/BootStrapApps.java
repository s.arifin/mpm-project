package id.atiila.base;

import id.atiila.config.tenant.TenantContext;
import id.atiila.domain.*;
import id.atiila.repository.MotorRepository;
import id.atiila.repository.UserMediatorRepository;
import id.atiila.repository.UserRepository;
import id.atiila.route.OrganizationRoute;
import id.atiila.service.*;
import id.atiila.service.dto.InternalDTO;
import id.atiila.service.dto.UserDTO;
import io.github.jhipster.config.JHipsterConstants;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by dev on 17/07/2017.
 */

@Component
public class BootStrapApps {

	@Autowired
	PositionUtils positionUtils;

	@Autowired
	private CustomerUtils customerUtils;

	@Autowired
	private VendorUtils vendorUtils;

    @Autowired
    private PriceUtils priceUtils;

    @Autowired
    private ProductUtils productUtils;

    @Autowired
    private PaymentMethodTypeService paymentMethodTypeService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private GeoBoundaryUtils geoBoundaryUtils;

    @Autowired
    private ProducerTemplate template;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private Environment env;

    @Autowired
    private UserMediatorRepository userMediatorRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MotorRepository motorRepository;

    protected void buildProduct(Internal intr, String idProduct, String name, String uom, String marketName) {
        if (productUtils.getProduct(idProduct) == null) {
            Long defaultPrice = 15500000l;
            productUtils.buildMotor(idProduct, name, uom, marketName);
            priceUtils.setInternalSalesPrice(intr, idProduct, BigDecimal.valueOf(defaultPrice + (RandomUtils.nextInt(50, 75) * 25000)));
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void run() {
        TenantContext.setCurrentTenant("default");
        try {

//            geoBoundaryUtils.initialize();
            positionUtils.buildDefaultPositionType();

            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("020", "MSO HO Malang"));
            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("021", "MSO HO Sby"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("022", "MSO Jember 1"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("023", "MSO Madiun"));

//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("024", "MSO Tulungagung"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("025", "MSO Ngagel"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("026", "MSO Sukun"));
            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("027", "MSO Blitar"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("028", "MSO Kediri"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("029", "MSO Pasuruan"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("030", "MSO Waru (GTW)"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("031", "MSO Sidoarjo (LRGN)"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("032", "MSO Mataram"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("033", "MSO Kupang"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("034", "MSO Ponorogo"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("035", "MSO Gresik"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("036", "MSO Kepanjen"));
            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("037", "MSO Pare"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("038", "MSO Mastrip"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("039", "MSO Bojonegoro"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("041", "MSO jember 2"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("042", "MSO Banyuwangi"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("043", "MSO Tuban"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("044", "MSO Nganjuk"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("045", "MSO Ngawi"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("046", "MSO Bondowoso"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("047", "MSO Batam"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("048", "MSO Pekanbaru"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("049", "MSO Medan"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("050", "MSO Sukorejo"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("051", "MSO Simpang Sby"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("052", "MSO Basra Mlg"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("053", "MSO Jombang"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("054", "MSO Palu"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("055", "MSO Lotim"));
            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("056", "MSO Mojokerto"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("057", "MSO Bogor"));
            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("058", "MSO Lamongan"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("059", "MSO Prabumulih"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("060", "MSO Tangerang"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("061", "MSO Makassar"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("062", "MSO Pontianak"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("133", "MSO Atambua"));
//            template.sendBody(OrganizationRoute.BUILD_MSO, new InternalDTO("233", "MSO Soe"));

            // Build init Motor
            if (productUtils.findOne("GFP") == null) {
                productUtils.buildInitMotor();
            }

            paymentMethodTypeService.initialize();
            vendorUtils.buildVendor("OTV", "One Time Vendor");
            customerUtils.buildOrganizationCustomer("OTC", "One Time Customer");

            if (env.acceptsProfiles(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT)) {
                String sidintr = "05801";
                String sidUser = "bogo";
                Optional<User> user = userRepository.findOneByLogin(sidUser);

                if (!user.isPresent()) {
                    Internal internal = internalUtils.findOne(sidintr);
                    Person person = partyUtils.buildPerson(sidUser, "", "Z1");
                    partyUtils.buildUser(internal, sidUser, sidUser + "@localhost", person, "ROLE_USER", "ROLE_ADMIN");
                }
            }

            // Move product feature to feature applicable
            List<Motor> motors = motorRepository.findAllWithEagerRelationships();
            for (Motor o: motors) {
                Set<Feature> features = o.getFeatures();
                for (Feature feature: features) {
                    productUtils.enableFeatured(o, feature);
                }
            }

        } finally {
            TenantContext.clear();
        }
    }
}
