package id.atiila.base;

public class DmsException extends RuntimeException {

    public DmsException() {
    }

    public DmsException(String message) {
        super(message);
    }

    public DmsException(String message, Throwable cause) {
        super(message, cause);
    }

    public DmsException(Throwable cause) {
        super(cause);
    }

    public DmsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
