package id.atiila.base;

import id.atiila.domain.Person;
import id.atiila.domain.User;
import id.atiila.repository.PersonRepository;
import id.atiila.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class LoginEventListener implements ApplicationListener<AuthenticationSuccessEvent> {

    @Autowired
    private PersonRepository personRepo;

    @Autowired
    private UserRepository userRepo;

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        Object principal = event.getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            UserDetails details = (UserDetails) principal;

            //Jika tidak Person untuk user, buat Person untuk user
            /*if (personRepo.findByUserUserLogin(details.getUsername()).isEmpty()) {
                Optional<User> user = userRepo.findOneByLogin(details.getUsername());
                Person p = new Person();
                p.setFirstName(user.get().getFirstName());
                p.setLastName(user.get().getLastName());
                p.setUser(user.get());
                personRepo.save(p);
            }*/
        };

    }
}
