package id.atiila.base.event;

import id.atiila.domain.GeneralUpload;
import org.springframework.context.ApplicationEvent;

public class UploadDataEvent extends ApplicationEvent {

    private Integer purposeType;

    private GeneralUpload generalUpload;

    public UploadDataEvent(Object source) {
        super(source);
    }

    public UploadDataEvent(Object source, GeneralUpload g, Integer purposeType) {
        super(source);
        this.generalUpload = g;
        this.purposeType = purposeType;
    }

    public GeneralUpload getGeneralUpload() {
        return generalUpload;
    }

    public void setGeneralUpload(GeneralUpload generalUpload) {
        this.generalUpload = generalUpload;
    }

    public Integer getPurposeType() {
        return purposeType;
    }

    public void setPurposeType(Integer purposeType) {
        this.purposeType = purposeType;
    }
}
