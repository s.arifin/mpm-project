package id.atiila.base.event;

import org.springframework.context.ApplicationEvent;

public class NotificationEvent extends ApplicationEvent {

    private String toUser;

    public NotificationEvent(Object source) {
        super(source);
    }

}
