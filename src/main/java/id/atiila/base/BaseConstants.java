package id.atiila.base;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Created by dev on 13/07/2017.
 */
public class BaseConstants {
    public static int GEO_TYPE_PROVINCE = 10;
    public static int GEO_TYPE_CITY = 11;
    public static int GEO_TYPE_DISTRICT = 12;
    public static int GEO_TYPE_VILLAGE = 13;

    public static int FACILITY_TYPE_BUILDING = 10;
    public static int FACILITY_TYPE_STORE = 11;
    public static int FACILITY_TYPE_WORKSHOP = 12;
    public static int FACILITY_TYPE_WAREHOUSE = 13;
    public static int FACILITY_TYPE_ROOM = 14;
    public static int FACILITY_TYPE_PIT = 41;

    public static int CALENDAR_TYPE_YEARLY = 1;
    public static int CALENDAR_TYPE_MONTHLY = 2;
    public static int CALENDAR_TYPE_DAILY = 3;
    public static int CALENDAR_TYPE_BOOKING_SLOT = 30;

    public static final int ROLE_PARENT_ORGANIZATION = 10;
    public static final int ROLE_INTERNAL = 11;
    public static final int ROLE_CUSTOMER = 12;
    public static final int ROLE_VENDOR = 13;
    public static final int ROLE_BRANCH = 14;
    public static final int ROLE_MAINDEALER = 15;

    public static final int ROLE_BILLTO = 20;
    public static final int ROLE_SHIPTO = 21;

    public static final String ROLE_MECHANIC = "35";
    public static final String ROLE_SALESMAN = "36";
    public static final String ROLE_SALES_BROKER = "37";
    public static final int ROLE_CUSTOMER_PERSONAL = 38;
    public static final int ROLE_CUSTOMER_ORGANIZATION = 39;
    public static final String ROLE_LEASING_COMPANY = "49";
    public static final String ROLE_DRIVER = "50";
    public static final int ROLE_CREATOR = 96;
    public static final int ROLE_VENDOR_BIRO_JASA_BBN = 80;

    public static final int ROLE_DEALER = 101;
    public static final int ROLE_WORKSHOP = 102;

    public static final String RELT_CUSTOMER = "10";

    public static final int STATUS_NOT_DEFINED = 0;
    public static final int STATUS_NOT_REQUIRED = 1;
    public static final int STATUS_DRAFT = 10;
    public static final int STATUS_OPEN = 11;
    public static final int STATUS_HOLD = 12;
    public static final int STATUS_CANCEL = 13;
    public static final int STATUS_COMPLETED = 17;
    public static final int STATUS_ACTIVE = 18;
    public static final int STATUS_LOW = 51;
    public static final int STATUS_MEDIUM = 52;
    public static final int STATUS_HOT = 53;
    public static final int STATUS_REQUEST_DROP_TL = 54;
    public static final int STATUS_REQUEST_DROP_KORSAL = 55;
    public static final int STATUS_COMPLETED_DROP = 56;
    public static final int STATUS_WAITING_FOR_APPROVAL = 60;
    public static final int STATUS_WAITING_FOR_APPROVAL_LEASING = 70;
    public static final int STATUS_REQUEST_ATTACHMENT = 75;
    public static final int STATUS_APPROVED = 61;
    public static final int STATUS_REJECTED = 62;
    public static final int STATUS_TERRITORIAL_VIOLATION = 63;
    public static final int STATUS_INDENT = 64;

    // shipment outgoing
    public static final int STATUS_KIRIM_GUDANG = 20;
    public static final int STATUS_CANCEL_SHIPMENT = 21;
    public static final int STATUS_ASSIGN_DRIVER = 22;
    public static final int STATUS_COMPLETE_SHIPMENT = 23;
    public static final int STATUS_PRINT_BAST = 24;
    public static final int STATUS_COMPLETE_GUDANG = 25;
    public static final int STATUS_EXT_DRIVER = 26;

    // VSO punyah
    // public static final int VSO_APPROVE = STATUS_APPROVED;
    public static final int STATUS_VSO_MATCHING = 74;
    public static final int STATUS_VSO_NOT_APPROVE = 62;
    public static final int STATUS_VSO_AFC_VERIFIKASI = 76;
    public static final int STATUS_VSO_ADMIN_SALES_VERIFIKASI = 77;
    public static final int STATUS_VSO_VERIFIKASI = 79;
    public static final int STATUS_VSO_MATCHING_STATUS = 81;
    public static final int STATUS_VSO_CANCEL_APPROVE_KACAB = 85;
    public static final int STATUS_VSO_WAITING_ADMIN_HANDLING = 88;
    public static final int STATUS_VSO_APPROVAL_ADMIN_HANDLING = 103;
    public static final int STATUS_VSO_ISSUBFINCO = 104;
    public static final int STATUS_VSO_GANTI = 105;
    public static final int STATUS_VSO_AFC_NOT_APPROVE = 78;
    public static final int STATUS_VSO_ADMINHANDLING_UPDATE = 18;
    public static final int VSO_SALES_EDIT = 106;
    public static final int CETAK_TYPE_BPKB = 192;

    // REGISTRATION
    public static final int STATUS_ISI_NAMA_CANCEL = 30;
    public static final int STATUS_REQUEST_FAKTUR = 31;
    public static final int STATUS_RECEIPT_FAKTUR = 32;
    public static final int STATUS_RECEIPT_REV_FAKTUR = 33;
    public static final int STATUS_REQUEST_BIRO_JASA = 34;
    public static final int STATUS_REQUEST_REV_BIRO_JASA = 35;
    public static final int STATUS_RECEIPT_BPKB_STNK = 36;
    public static final int STATUS_RECEIPT_REV_BPKB_STNK = 37;
    public static final int STATUS_REQUEST_LUAR = 38;
    public static final int STATUS_RECEIPT_LUAR = 39;
    public static final int STATUS_BAST = 40;
    public static final int STATUS_REQUEST_CANCEL_PENGAJUAN = 41;
    public static final int STATUS_RECEIPT_FAKTUR_UPLOAD_NON = 42;
    // public static final int GAGAL_MATCHING= 43;
    public static final int STATUS_RECEIPT_FAKTUR_UPLOAD = 43;
    public static final int STATUS_BAST_OFFTHEROAD = 44;

    public static final String BILLING_SALES_VEHICLE = "10";
    public static final String BILLING_SALES_PART = "11";

    public static final String PRODUCT_TYPE_GOOD = "10";
    public static final String PRODUCT_TYPE_SERVICE = "11";
    public static final String PRODUCT_TYPE_FINANCE = "20";
    public static final String PRODUCT_TYPE_MOTOR = "21";
    public static final String PRODUCT_TYPE_REM_PART = "22";

    public static final int FEATURE_COLOR_BLUE = 101;
    public static final int CATEGORY_TYPE_CUSTOMER = 50;

    public static final int CPM_POSTALADDRESS = 1;
    public static final int CPM_PHONE = 20;
    public static final int CPM_CELL_PHONE1 = 21;
    public static final int CPM_CELL_PHONE2 = 22;
    public static final int CPM_OFFICE_PHONE = 23;
    public static final int CPM_PRIVATE_MAIL = 50;
    public static final int CPM_OFFICE_MAIL = 51;

    //Price Component
    public static final int HET = 10;
    public static final int REGULAR_PRICE = 11;
    public static final int DISCOUNT = 12;
    public static final int DISCOUNT_CAMPAIGN = 13;
    public static final int BBN = 101;
    public static final int SUBSIDI_AHM = 102;
    public static final int SUBSIDI_MD = 103;
    public static final int SUBSIDI_DEALER = 104;
    public static final int SUBSIDI_FIN_COMP = 105;
    public static final int SURCHARGE = 999;

    //Billing Item Type
    public static final int ITEM_TYPE_PRODUCT = 10;
    public static final int ITEM_TYPE_SUBSIDI_DEALER = 501;
    public static final int ITEM_TYPE_SUBSIDI_FIN_CO = 502;
    public static final int ITEM_TYPE_SUBSIDI_MD = 503;
    public static final int ITEM_TYPE_SUBSIDI_AHM = 505;
    public static final int ITEM_TYPE_BBN_PRICE = 511;
    public static final int ITEM_TYPE_PPN = 5000;
    public static final int ITEM_TYPE_PPH = 5001;

    //approval
    public static final int DISCOUNT_SUR = 101;
    public static final int SALES_MESSAGE_UNIT = 102;
    public static final int PELANGGARAN_WILAYAH = 103;
    public static final int LEASING = 104;

    //suspect
    public static final int SUSPECT_UPLOAD = 10;
    public static final int SUSPECT_RO = 11;
    public static final int SUSPECT_SEGMENTASI = 12;
    public static final int SUSPECT_OTHER = 99;

    public static final int POSITION_KACAB = 10;
    public static final int POSITION_KABENG = 11;
    public static final int POSITION_KORSAL = 12;
    public static final int POSITION_SALESMAN = 13;
    public static final int POSITION_SERV_ANALYS = 14;
    public static final int POSITION_MEKANIK = 15;
    public static final int POSITION_AFC = 16;
    public static final int POSITION_ADMSALES = 17;
    public static final int POSITION_FRONTDESK = 18;
    public static final int POSITION_DRIVER = 19;
    public static final int POSITION_PDIMAN = 20;
    public static final int POSITION_KASIR = 21;

    public static final String AUTH_KACAB = "ROLE_KACAB";
    public static final String AUTH_KABENG = "ROLE_KABENG";
    public static final String AUTH_KORSAL = "ROLE_KORSAL";
    public static final String AUTH_SALESMAN = "ROLE_SALESMAN";
    public static final String AUTH_SERV_ANALYS = "ROLE_SERV_ANALYS";
    public static final String AUTH_MEKANIK = "ROLE_MEKANIK";
    public static final String AUTH_AFC = "ROLE_AFC";
    public static final String AUTH_ADMSALES = "ROLE_ADMSALES";
    public static final String AUTH_FRONTDESK = "ROLE_FRONTDESK";
    public static final String AUTH_DRIVER = "ROLE_DRIVER";
    public static final String AUTH_PDIMAN = "ROLE_PDIMAN";
    public static final String AUTH_KASIR = "ROLE_KASIR";

    public static final int REGISTRATION_TYPE_SUBMISSION = 32;
    public static final int REGISTRATION_TYPE_FAKTUR = 33;

    public static final int DELIVERABLE_TYPE_NOPOL = 101;
    public static final int DELIVERABLE_TYPE_STNK = 102;
    public static final int DELIVERABLE_TYPE_BPKB = 103;
    public static final int DELIVERABLE_TYPE_NOTICE = 104;
    public static final int DELIVERABLE_TYPE_FAKTUR_OFFTHEROAD = 105;

    public static final int REQ_TYPE_SUR = 10;
    public static final int REQ_TYPE_SUR_RETAIL = 40;
    public static final int REQ_TYPE_SUR_DS = 41;
    public static final int REQ_TYPE_SUR_GP = 42;
    public static final int REQ_TYPE_SUR_GC = 43;

    public static final int REQ_TYPE_VDR_RETAIL = 200;
    public static final int REQ_TYPE_VDR_EXTERNAL = 201;

    public static final int ORDER_TYPE_INIT = 9999;
    public static final int ORDER_TYPE_INTERNAL = 202;
    public static final int ORDER_TYPE_VSO = 110;
    public static final int ORDER_TYPE_DS = 113;
    public static final int ORDER_TYPE_GC = 111;
    public static final int ORDER_TYPE_GP = 112;
    public static final int ORDER_TYPE_REKANAN = 114;
    public static final int ORDER_TYPE_PO_UNIT = 20;

    //Prospect Source
    public static final int SOURCE_EVENT = 10;
    public static final int SOURCE_MAKELAR = 11;
    public static final int SOURCE_REFFERENCE = 12;
    public static final int SOURCE_REFFERAL = 13;
    public static final int SOURCE_WALKIN = 14;
    public static final int SOURCE_SUSPECT = 15;

    //
    public static final int PRICE_TYPE_HET = 10;
    public static final int PRICE_TYPE_REGULAR = 11;
    public static final int PRICE_TYPE_DISCOUNT = 12;
    public static final int PRICE_TYPE_BBN = 101;
    public static final int PRICE_TYPE_SUBS_AHM = 102;
    public static final int PRICE_TYPE_SUBS_MD = 103;
    public static final int PRICE_TYPE_DISC_DEALER = 104;
    public static final int PRICE_TYPE_SUBS_FINCOY = 105;
    public static final int PRICE_TYPE_BBNKB = 210;
    public static final int PRICE_TYPE_SWDKLLJ = 212;
    public static final int PRICE_TYPE_TNKB = 214;
    public static final int PRICE_TYPE_PKB = 211;
    public static final int PRICE_TYPE_STNK = 213;
    public static final int PRICE_TYPE_BIROJASA = 217;
    public static final int PRICE_TYPE_PARKIR = 215;
    public static final int PRICE_TYPE_BIAYA = 216;

    public static final int PURP_TYPE_MAIN_FACILITY = 1001;
    public static final int PURP_TYPE_STORE = 1002;
    public static final int PURP_TYPE_WORKSHOP = 1003;
    public static final int PURP_TYPE_WAREHOUSE = 1004;

    public static final int PURP_TYPE_INIT_STOCK = 2001;
    public static final int PURP_TYPE_INIT_CUSTOMER = 2002;
    public static final int PURP_TYPE_PRODUCT_FEATURE = 2003;
    public static final int PURP_TYPE_INIT_STNK_BPKB_ON_HAND = 2004;

    public static final int PROVINCE = 10;
    public static final int CITY = 11;
    public static final int DISTRICT = 12;
    public static final int VILLAGE = 13;

    public static final String TYPE_PROVINCE = "10";
    public static final String TYPE_CITY = "11";
    public static final String TYPE_DISTRICT = "12";
    public static final String TYPE_VILLAGE = "13";

    public static final int BILLING_TYPE_VEHICLE_SALES = 10;
    public static final int BILLING_TYPE_PART_SALES = 11;
    public static final int BILLING_TYPE_VEHICLE_PURCHASE = 12;
    public static final int BILLING_TYPE_VEHICLE_DIRECT = 110;
    public static final int BILLING_TYPE_VEHICLE_GC = 111;
    public static final int BILLING_TYPE_VEHICLE_GP = 112;
    public static final int BILLING_TYPE_VEHICLE_REKANAN = 113;
    public static final int BILLING_TYPE_VEHICLE_INTERNAL = 114;

    public static final int SHIPMENT_DO_UNIT = 10;

    public static final int CONTAINER_TYPE_RAK = 10;
    public static final int CONTAINER_TYPE_PALET = 11;
    public static final int CONTAINER_TYPE_BINBOX = 12;
    public static final int CONTAINER_TYPE_ROOM = 13;

    public static final int PACK_TYPE_INIT_STOCK_UNIT = 101;
    public static final int PACK_TYPE_INIT_STOCK_PRODUCT = 102;

    public static final int FEAT_TYPE_COLOR = 100;

    // CUSTOMER DATABASE SURVEY
    public static final String CDB_STATUS_RUMAH_SENDIRI = "11";
    public static final String CDB_STATUS_RUMAH_ORTU = "12";
    public static final String CDB_STATUS_RUMAH_SEWA = "13";
    public static final String CDB_GROUP_CUSTOMER = "N";

    public static final String CDB_PENGELUARAN_1 = "11";
    public static final String CDB_PENGELUARAN_2 = "12";
    public static final String CDB_PENGELUARAN_3 = "13";
    public static final String CDB_PENGELUARAN_4 = "14";
    public static final String CDB_PENGELUARAN_5 = "15";
    public static final String CDB_PENGELUARAN_6 = "16";
    public static final String CDB_PENGELUARAN_7 = "17";

    public static final String CDB_PEGAWAI_NEGERI = "11";
    public static final String CDB_PEGAWAI_SWASTA = "12";
    public static final String CDB_OJEK = "13";
    public static final String CDB_WIRASWASTA = "14";
    public static final String CDB_MAHASISWA = "15";
    public static final String CDB_GURU = "16";
    public static final String CDB_TNI = "17";
    public static final String CDB_IRT = "18";
    public static final String CDB_PETANI = "19";
    public static final String CDB_DOKTER = "20";
    public static final String CDB_PENGACARA = "21";
    public static final String CDB_WARTAWAN = "22";

    public static final String CDB_TIDAK_TAMAT_SD = "11";
    public static final String CDB_SD = "12";
    public static final String CDB_SLTP = "13";
    public static final String CDB_SLTA = "14";
    public static final String CDB_AKADEMI = "15";
    public static final String CDB_UNIVERSITAS = "16";
    public static final String CDB_PASCA_SARJANA = "17";

    public static final String CDB_PRABAYAR = "11";
    public static final String CDB_PASCABAYAR = "12";
    public static final String CDB_TIDAK_MEMILIKI_HP = "13";

    public static final String CDB_HONDA = "11";
    public static final String CDB_YAMAHA = "12";
    public static final String CDB_SUZUKI = "13";
    public static final String CDB_KAWASAKI = "14";
    public static final String CDB_MOTOR_LAIN = "15";
    public static final String CDB_BELUM_PERNAH_MEMILIKI_MERK = "16";


    public static final String CDB_SPORT = "11";
    public static final String CDB_CUB = "12";
    public static final String CDB_AT = "13";
    public static final String CDB_BELUM_PERNAH_MEMILIKI_TIPE = "14";

    public static final String CDB_BERDAGANG = "11";
    public static final String CDB_BEKERJA = "12";
    public static final String CDB_DISEWAKAN = "13";

    public static final String CDB_SAYA_SENDIRI = "11";
    public static final String CDB_ANAK = "12";
    public static final String CDB_KARYAWAN= "13";

    public static final int REL_TYPE_CUSTOMER = 10;
    public static final int REL_TYPE_VENDOR = 11;

    public static final int REQUEST_TYPE_VSO_GC = 101;
    public static final int REQUEST_TYPE_VSO_DIRECT = 102;
    public static final int REQUEST_TYPE_VSO_GP = 103;
    public static final int REQUEST_TYPE_VSO_REKANAN = 104;
    public static final int REQUEST_TYPE_VSO_INTERNAL = 105;

    public static final int REQUIREMENT_PERSON = 10;
    public static final int REQUIREMENT_GC = 101;
    public static final int REQUIREMENT_DIRECT = 102;
    public static final int REQUIREMENT_GP = 103;

    public static final int SALE_TYPE_COD = 50;

    public static final int SALE_TYPE_CASH = 20;
    public static final int SALE_TYPE_CASH_COD = 50;
    public static final int SALE_TYPE_CASH_CBD = 51;
    public static final int SALE_TYPE_CASH_OFFTHEROAD = 22;
    public static final int SALE_TYPE_CASH_OFFTHEROAD_COD = 54;
    public static final int SALE_TYPE_CASH_OFFTHEROAD_CBD = 55;
    public static final int SALE_TYPE_CREDIT = 21;
    public static final int SALE_TYPE_CREDIT_COD = 52;
    public static final int SALE_TYPE_CREDIT_CBD = 53;

    public static final int PAYMENT_TYPE_TITIPAN = 50;
    public static final int PAYMENT_TYPE_DP = 50;
    public static final int PAYMENT_TYPE_DP2 = 50;
    public static final int PAYMENT_TYPE_PELUNASAN = 53;

    public static final String PAYMENT_METHOD_CASH = "cash";

    public static LocalDateTime localDateEndTime() {
        return LocalDateTime.of(9999, 12, 31, 23, 59, 59);
    }

    public static ZonedDateTime zoneDateEndTime() {
        return ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 999, ZoneId.systemDefault());
    }
}
