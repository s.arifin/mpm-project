package id.atiila.base.executeprocess;

public class SuspectPersonConstants {

    //Process Execute
    public static final int ASSIGN_SUSPECT_BUILD_PROSPECT = 101;


    //Process Execute List
    public static final int UPLOAD_SUSPECT_BULK = 101;
    public static final int ASSIGN_SUSPECT_BULK = 102;
}
