package id.atiila.base.executeprocess;

public class UnitDocumentMessageConstants {
    public static final int SALES_NOTE_APPROVED = 101;
    public static final int SALES_NOTE_REJECTED = 102;
}
