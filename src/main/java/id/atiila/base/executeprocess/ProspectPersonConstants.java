package id.atiila.base.executeprocess;

public class ProspectPersonConstants {

    //Process Execute
    public static final int COMPLETION_PROSPECT = 101;
    public static final int CHANGE_PROSPECT_SALESMAN = 102;
    public static final int TL_REQUEST_DROP_PROSPECT = 103;
    public static final int DROP_PROSPECT_BUILD_SUSPECT = 104;
    public static final int SEARCH_EXISTING_PROSPECT = 105;
    public static final int KORSAL_REQUEST_DROP_PROSPECT = 106;
    public static final int REVERT_STATUS_PROSPECT = 107;

    //Process Execute List
    public static final int ASSIGN_SALESMAN_PROSPECT_BULK = 101;
    public static final int DROP_PROSPECT_BULK = 102;
    public static final int REQUEST_DROP_PROSPECT_BULK = 103;
    public static final int REJECT_DROP_PROSPECT_BULK = 104;
}
