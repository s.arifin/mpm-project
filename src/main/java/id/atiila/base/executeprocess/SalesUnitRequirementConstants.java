package id.atiila.base.executeprocess;

public class SalesUnitRequirementConstants {
    public static final int SUBMIT_VSO = 101;
//    public static final int APPROVE_SUBSIDI_OR_TERRITORIAL_VIOLATION = 102;
    public static final int BUILD_VSO = 103;
    public static final int RESET_VSO_DRAFT = 104;
    public static final int FIND_OR_CREATE_VSO_NUMBER = 105;
    public static final int APPROVE_SUBSIDI = 106;
    public static final int APPROVAL_KACAB = 107;
    public static final int APPROVAL_KORSAL = 108;
    public static final int CANCEL_VSO = 109;
    public static final int DUPLICATE_SUR = 110;
    public static final int GANTI_VSO = 111;
    public static final int APPROVAL_LEASING = 112;
    public static final int REJECTED_LEASING = 113;
    public static final int REQUEST_ATTACHMENT = 114;
    public static final int VSO_SALES_EDIT = 115;
    public static final int PROSES_INDENT = 116;
    public static final int PROSES_BACK_ORDER = 117;
    public static final int PROSES_GANTI_VSO_NOT_APPROVE = 118;
    public static final int PROSES_UPDATE_HARGA_PEMBAYARAN = 119;
    public static final int PROSES_BACK_ORDER_FROM_INDENT = 120;
    public static final int PROSES_UPDATE_FACILITY = 121;

}
