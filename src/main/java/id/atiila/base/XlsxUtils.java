package id.atiila.base;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.jxls.reader.DateConverter;
import org.jxls.reader.ReaderBuilder;
import org.jxls.reader.XLSReader;
import org.xml.sax.SAXException;

public class XlsxUtils {

	public static <T> List<T> parseExcelFileToBeans(final File xlsFile, final File jxlsConfigFile) throws IOException, SAXException, InvalidFormatException {
		final XLSReader xlsReader = ReaderBuilder.buildFromXML(jxlsConfigFile);
		final List<T> result = new ArrayList<>();
		final Map<String, Object> beans = new HashMap<>();

		beans.put("result", result);

		try (InputStream inputStream = new BufferedInputStream(new FileInputStream(xlsFile))) {
			xlsReader.read(inputStream, beans);
		}
		return result;
	}

	public static <T> List<T> parseExcelStreamToBeans(final File xlsFile, final InputStream is) throws IOException, SAXException, InvalidFormatException {
		final XLSReader xlsReader = ReaderBuilder.buildFromXML(is);
		final List<T> result = new ArrayList<>();
		final Map<String, Object> beans = new HashMap<>();

		beans.put("result", result);

		try (InputStream inputStream = new BufferedInputStream(new FileInputStream(xlsFile))) {
			xlsReader.read(inputStream, beans);
		}
		return result;
	}

    public static <T> List<T> parseExcelStreamToBeans(final InputStream xlsis, final InputStream is) throws IOException, SAXException, InvalidFormatException {
        final XLSReader xlsReader = ReaderBuilder.buildFromXML(is);
        final List<T> result = new ArrayList<>();
        final Map<String, Object> beans = new HashMap<>();
        beans.put("result", result);
        beans.put("df", new SimpleDateFormat("dd/MM/yyyy"));
        xlsReader.read(xlsis, beans);
        return result;
    }


}
