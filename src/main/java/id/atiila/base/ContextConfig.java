package id.atiila.base;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ContextConfig {

    @Bean
    public ContextHelper contextHelper(){
        return ContextHelper.getInstance();
    }

}
