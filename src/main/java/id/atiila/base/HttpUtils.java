package id.atiila.base;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class HttpUtils {

    public static String getValue(HttpServletRequest request, String id) {
        String result = null;
        String[] values = request.getParameterMap().get(id);
        if (values != null) {
            result = values[0];
        }
        return result;
    }

    public static List<Integer> getIntegers(HttpServletRequest request, String id) {
        List<Integer> result = null;
        String[] values = request.getParameterMap().get("command");
        if (values != null) {
            result = new ArrayList<>();
            for (String v: values) {
                result.add(Integer.valueOf(v));
            }
        }
        return result;
    }

    public static List<String> getStrings(HttpServletRequest request, String id) {
        List<String> result = null;
        String[] values = request.getParameterMap().get("command");
        if (values != null) {
            result = Arrays.asList(values);
        }
        return result;
    }

    public static <T> T getObject(HttpServletRequest request, Class<T> classT) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.convertValue(request.getParameterMap(), classT);
    }

    public static <T> T getObject(Map<String, Object> item, Class<T> classT) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.convertValue(item, classT);
    }
}
