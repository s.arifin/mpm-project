package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ProvinceService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ProvinceDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Province.
 */
@RestController
@RequestMapping("/api")
public class ProvinceResource {

    private final Logger log = LoggerFactory.getLogger(ProvinceResource.class);

    private static final String ENTITY_NAME = "province";

    private final ProvinceService provinceService;

    public ProvinceResource(ProvinceService provinceService) {
        this.provinceService = provinceService;
    }

    /**
     * POST  /provinces : Create a new province.
     *
     * @param provinceDTO the provinceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new provinceDTO, or with status 400 (Bad Request) if the province has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/provinces")
    @Timed
    public ResponseEntity<ProvinceDTO> createProvince(@RequestBody ProvinceDTO provinceDTO) throws URISyntaxException {
        log.debug("REST request to save Province : {}", provinceDTO);
        if (provinceDTO.getIdGeobou() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new province cannot already have an ID")).body(null);
        }
        ProvinceDTO result = provinceService.save(provinceDTO);
        return ResponseEntity.created(new URI("/api/provinces/" + result.getIdGeobou()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdGeobou().toString()))
            .body(result);
    }

    /**
     * POST  /provinces/execute{id}/{param} : Execute Bussiness Process province.
     *
     * @param provinceDTO the provinceDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  provinceDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/provinces/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ProvinceDTO> executedProvince(@PathVariable Integer id, @PathVariable String param, @RequestBody ProvinceDTO provinceDTO) throws URISyntaxException {
        log.debug("REST request to process Province : {}", provinceDTO);
        ProvinceDTO result = provinceService.processExecuteData(id, param, provinceDTO);
        return new ResponseEntity<ProvinceDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /provinces/execute-list{id}/{param} : Execute Bussiness Process province.
     *
     * @param provinceDTO the provinceDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  provinceDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/provinces/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ProvinceDTO>> executedListProvince(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ProvinceDTO> provinceDTO) throws URISyntaxException {
        log.debug("REST request to process List Province");
        Set<ProvinceDTO> result = provinceService.processExecuteListData(id, param, provinceDTO);
        return new ResponseEntity<Set<ProvinceDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /provinces : Updates an existing province.
     *
     * @param provinceDTO the provinceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated provinceDTO,
     * or with status 400 (Bad Request) if the provinceDTO is not valid,
     * or with status 500 (Internal Server Error) if the provinceDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/provinces")
    @Timed
    public ResponseEntity<ProvinceDTO> updateProvince(@RequestBody ProvinceDTO provinceDTO) throws URISyntaxException {
        log.debug("REST request to update Province : {}", provinceDTO);
        if (provinceDTO.getIdGeobou() == null) {
            return createProvince(provinceDTO);
        }
        ProvinceDTO result = provinceService.save(provinceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, provinceDTO.getIdGeobou().toString()))
            .body(result);
    }

    /**
     * GET  /provinces : get all the provinces.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of provinces in body
     */
    @GetMapping("/provinces")
    @Timed
    public ResponseEntity<List<ProvinceDTO>> getAllProvinces(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Provinces");
        Page<ProvinceDTO> page = provinceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/provinces");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * GET  /provinces/:id : get the "id" province.
     *
     * @param id the id of the provinceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the provinceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/provinces/{id}")
    @Timed
    public ResponseEntity<ProvinceDTO> getProvince(@PathVariable UUID id) {
        log.debug("REST request to get Province : {}", id);
        ProvinceDTO provinceDTO = provinceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(provinceDTO));
    }

    /**
     * DELETE  /provinces/:id : delete the "id" province.
     *
     * @param id the id of the provinceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/provinces/{id}")
    @Timed
    public ResponseEntity<Void> deleteProvince(@PathVariable UUID id) {
        log.debug("REST request to delete Province : {}", id);
        provinceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/provinces?query=:query : search for the province corresponding
     * to the query.
     *
     * @param query the query of the province search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/provinces")
    @Timed
    public ResponseEntity<List<ProvinceDTO>> searchProvinces(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Provinces for query {}", query);
        Page<ProvinceDTO> page = provinceService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/provinces");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
