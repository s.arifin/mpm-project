package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.GLAccountService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.GLAccountDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing GLAccount.
 */
@RestController
@RequestMapping("/api")
public class GLAccountResource {

    private final Logger log = LoggerFactory.getLogger(GLAccountResource.class);

    private static final String ENTITY_NAME = "gLAccount";

    private final GLAccountService gLAccountService;

    public GLAccountResource(GLAccountService gLAccountService) {
        this.gLAccountService = gLAccountService;
    }

    /**
     * POST  /gl-accounts : Create a new gLAccount.
     *
     * @param gLAccountDTO the gLAccountDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new gLAccountDTO, or with status 400 (Bad Request) if the gLAccount has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gl-accounts")
    @Timed
    public ResponseEntity<GLAccountDTO> createGLAccount(@RequestBody GLAccountDTO gLAccountDTO) throws URISyntaxException {
        log.debug("REST request to save GLAccount : {}", gLAccountDTO);
        if (gLAccountDTO.getIdGLAccount() != null) {
            throw new BadRequestAlertException("A new gLAccount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GLAccountDTO result = gLAccountService.save(gLAccountDTO);
        return ResponseEntity.created(new URI("/api/gl-accounts/" + result.getIdGLAccount()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdGLAccount().toString()))
            .body(result);
    }

    /**
     * POST  /gl-accounts/process : Execute Bussiness Process gLAccount.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gl-accounts/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processGLAccount(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process GLAccount ");
        Map<String, Object> result = gLAccountService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /gl-accounts/execute : Execute Bussiness Process gLAccount.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  gLAccountDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gl-accounts/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedGLAccount(HttpServletRequest request, @RequestBody GLAccountDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process GLAccount");
        Map<String, Object> result = gLAccountService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /gl-accounts/execute-list : Execute Bussiness Process gLAccount.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  gLAccountDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gl-accounts/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListGLAccount(HttpServletRequest request, @RequestBody Set<GLAccountDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List GLAccount");
        Map<String, Object> result = gLAccountService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /gl-accounts : Updates an existing gLAccount.
     *
     * @param gLAccountDTO the gLAccountDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated gLAccountDTO,
     * or with status 400 (Bad Request) if the gLAccountDTO is not valid,
     * or with status 500 (Internal Server Error) if the gLAccountDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/gl-accounts")
    @Timed
    public ResponseEntity<GLAccountDTO> updateGLAccount(@RequestBody GLAccountDTO gLAccountDTO) throws URISyntaxException {
        log.debug("REST request to update GLAccount : {}", gLAccountDTO);
        if (gLAccountDTO.getIdGLAccount() == null) {
            return createGLAccount(gLAccountDTO);
        }
        GLAccountDTO result = gLAccountService.save(gLAccountDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, gLAccountDTO.getIdGLAccount().toString()))
            .body(result);
    }

    /**
     * GET  /gl-accounts : get all the gLAccounts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of gLAccounts in body
     */
    @GetMapping("/gl-accounts")
    @Timed
    public ResponseEntity<List<GLAccountDTO>> getAllGLAccounts(Pageable pageable) {
        log.debug("REST request to get a page of GLAccounts");
        Page<GLAccountDTO> page = gLAccountService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/gl-accounts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/gl-accounts/filterBy")
    @Timed
    public ResponseEntity<List<GLAccountDTO>> getAllFilteredGLAccounts(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of GLAccounts");
        Page<GLAccountDTO> page = gLAccountService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/gl-accounts/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /gl-accounts/:id : get the "id" gLAccount.
     *
     * @param id the id of the gLAccountDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the gLAccountDTO, or with status 404 (Not Found)
     */
    @GetMapping("/gl-accounts/{id}")
    @Timed
    public ResponseEntity<GLAccountDTO> getGLAccount(@PathVariable UUID id) {
        log.debug("REST request to get GLAccount : {}", id);
        GLAccountDTO gLAccountDTO = gLAccountService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(gLAccountDTO));
    }

    /**
     * DELETE  /gl-accounts/:id : delete the "id" gLAccount.
     *
     * @param id the id of the gLAccountDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/gl-accounts/{id}")
    @Timed
    public ResponseEntity<Void> deleteGLAccount(@PathVariable UUID id) {
        log.debug("REST request to delete GLAccount : {}", id);
        gLAccountService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/gl-accounts?query=:query : search for the gLAccount corresponding
     * to the query.
     *
     * @param query the query of the gLAccount search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/gl-accounts")
    @Timed
    public ResponseEntity<List<GLAccountDTO>> searchGLAccounts(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of GLAccounts for query {}", query);
        Page<GLAccountDTO> page = gLAccountService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/gl-accounts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
