package id.atiila.web.rest;

import java.time.ZonedDateTime;
import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.DateUtils;
import id.atiila.service.PriceComponentService;
import id.atiila.service.ProductUtils;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PriceComponentDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PriceComponent.
 */
@RestController
@RequestMapping("/api")
public class PriceComponentResource {

    private final Logger log = LoggerFactory.getLogger(PriceComponentResource.class);

    private static final String ENTITY_NAME = "priceComponent";

    private final PriceComponentService priceComponentService;

    public PriceComponentResource(PriceComponentService priceComponentService) {
        this.priceComponentService = priceComponentService;
    }

    @Autowired
    private DateUtils dateUtils;

    /**
     * POST  /price-components : Create a new priceComponent.
     *
     * @param priceComponentDTO the priceComponentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new priceComponentDTO, or with status 400 (Bad Request) if the priceComponent has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/price-components")
    @Timed
    public ResponseEntity<PriceComponentDTO> createPriceComponent(@RequestBody PriceComponentDTO priceComponentDTO) throws URISyntaxException {
        log.debug("REST request to save PriceComponent : {}", priceComponentDTO);
        if (priceComponentDTO.getIdPriceComponent() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new priceComponent cannot already have an ID")).body(null);
        }
        PriceComponentDTO result = priceComponentService.save(priceComponentDTO);
        return ResponseEntity.created(new URI("/api/price-components/" + result.getIdPriceComponent()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPriceComponent().toString()))
            .body(result);
    }

    /**
     * POST  /price-components/execute{id}/{param} : Execute Bussiness Process priceComponent.
     *
     * @param priceComponentDTO the priceComponentDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  priceComponentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/price-components/execute/{id}/{param}")
    @Timed
    public ResponseEntity<PriceComponentDTO> executedPriceComponent(@PathVariable Integer id, @PathVariable String param, @RequestBody PriceComponentDTO priceComponentDTO) throws URISyntaxException {
        log.debug("REST request to process PriceComponent : {}", priceComponentDTO);
        PriceComponentDTO result = priceComponentService.processExecuteData(id, param, priceComponentDTO);
        return new ResponseEntity<PriceComponentDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /price-components/execute-list{id}/{param} : Execute Bussiness Process priceComponent.
     *
     * @param priceComponentDTO the priceComponentDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  priceComponentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/price-components/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<PriceComponentDTO>> executedListPriceComponent(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<PriceComponentDTO> priceComponentDTO) throws URISyntaxException {
        log.debug("REST request to process List PriceComponent");
        Set<PriceComponentDTO> result = priceComponentService.processExecuteListData(id, param, priceComponentDTO);
        return new ResponseEntity<Set<PriceComponentDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /price-components : Updates an existing priceComponent.
     *
     * @param priceComponentDTO the priceComponentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated priceComponentDTO,
     * or with status 400 (Bad Request) if the priceComponentDTO is not valid,
     * or with status 500 (Internal Server Error) if the priceComponentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/price-components")
    @Timed
    public ResponseEntity<PriceComponentDTO> updatePriceComponent(@RequestBody PriceComponentDTO priceComponentDTO) throws URISyntaxException {
        log.debug("REST request to update PriceComponent : {}", priceComponentDTO);
        if (priceComponentDTO.getIdPriceComponent() == null) {
            return createPriceComponent(priceComponentDTO);
        }
        PriceComponentDTO result = priceComponentService.save(priceComponentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, priceComponentDTO.getIdPriceComponent().toString()))
            .body(result);
    }

    /**
     * GET  /price-components : get all the priceComponents.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of priceComponents in body
     */
    @GetMapping("/price-components")
    @Timed
    public ResponseEntity<List<PriceComponentDTO>> getAllPriceComponents(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PriceComponents");
        Page<PriceComponentDTO> page = priceComponentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/price-components");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/price-components/by-product-and-internal")
    @Timed
    public ResponseEntity<List<PriceComponentDTO>> getPriceComponentsByProductAndInternal(@ApiParam String idproduct, @ApiParam  String idinternal,@ApiParam String datewhen){
        log.debug("REST request to get a page of PriceComponents By Product and Internal");
        ZonedDateTime zdt = dateUtils.parse(datewhen);
        List<PriceComponentDTO> l_dto = priceComponentService.findPriceByProductAndInternal(idproduct, idinternal, zdt);
        return new ResponseEntity<List<PriceComponentDTO>>(l_dto, null, HttpStatus.OK);
    }

    @GetMapping("/price-components/by-product-and-vendor")
    @Timed
    public ResponseEntity<List<PriceComponentDTO>> getPriceComponentByProductAndVendor(@ApiParam String idproduct, @ApiParam String idvendor, @ApiParam String datewhen){
        log.debug("REST request to get a page of PriceComponents By Product and Vendor");
        ZonedDateTime zdt = dateUtils.parse(datewhen);
        List<PriceComponentDTO> l_dto = priceComponentService.findPriceByProductAndVendor(idproduct, idvendor, zdt);
        return new ResponseEntity<List<PriceComponentDTO>>(l_dto,null,HttpStatus.OK);
    }

    @GetMapping("/price-components/all/by-product-and-internal")
    @Timed
    public ResponseEntity<List<PriceComponentDTO>> getAllPriceComponentByProductAndInternal(@ApiParam String idproduct, @ApiParam String idinternal){
        log.debug("REST request to get a page of all PriceComponents By Product and Internal");
        List<PriceComponentDTO> l_dto = priceComponentService.findAllPriceByProductAndInternal(idproduct, idinternal);
        return new ResponseEntity<List<PriceComponentDTO>>(l_dto,null,HttpStatus.OK);
    }

    @GetMapping("/price-components/all/by-product-and-vendor")
    @Timed
    public ResponseEntity<List<PriceComponentDTO>> getAllPriceComponentByProductAndVendor(@ApiParam String idproduct, @ApiParam String idvendor){
        log.debug("REST request to get a page of all PriceComponents By Product and Vendor");
        List<PriceComponentDTO> l_dto = priceComponentService.findAllPriceByProductAndVendor(idproduct, idvendor);
        return new ResponseEntity<List<PriceComponentDTO>>(l_dto,null,HttpStatus.OK);
    }

    /**
     * GET  /price-components/:id : get the "id" priceComponent.
     *
     * @param id the id of the priceComponentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the priceComponentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/price-components/{id}")
    @Timed
    public ResponseEntity<PriceComponentDTO> getPriceComponent(@PathVariable UUID id) {
        log.debug("REST request to get PriceComponent : {}", id);
        PriceComponentDTO priceComponentDTO = priceComponentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(priceComponentDTO));
    }

    /**
     * DELETE  /price-components/:id : delete the "id" priceComponent.
     *
     * @param id the id of the priceComponentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/price-components/{id}")
    @Timed
    public ResponseEntity<Void> deletePriceComponent(@PathVariable UUID id) {
        log.debug("REST request to delete PriceComponent : {}", id);
        priceComponentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/price-components?query=:query : search for the priceComponent corresponding
     * to the query.
     *
     * @param query the query of the priceComponent search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/price-components")
    @Timed
    public ResponseEntity<List<PriceComponentDTO>> searchPriceComponents(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of PriceComponents for query {}", query);
        Page<PriceComponentDTO> page = priceComponentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/price-components");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/price-components/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processPriceComponent(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process PriceComponent ");
        Map<String, Object> result = priceComponentService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    @GetMapping("/price-components/find-stnkb")
    @Timed
    public ResponseEntity<List<PriceComponentDTO>> getStnkbPrice(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Vendors");
        Page<PriceComponentDTO> page = priceComponentService.findPriceSTNKB(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/price-components/find-stnkb");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
