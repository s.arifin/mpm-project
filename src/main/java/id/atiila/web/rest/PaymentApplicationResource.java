package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PaymentApplicationService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PaymentApplicationDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PaymentApplication.
 */
@RestController
@RequestMapping("/api")
public class PaymentApplicationResource {

    private final Logger log = LoggerFactory.getLogger(PaymentApplicationResource.class);

    private static final String ENTITY_NAME = "paymentApplication";

    private final PaymentApplicationService paymentApplicationService;

    public PaymentApplicationResource(PaymentApplicationService paymentApplicationService) {
        this.paymentApplicationService = paymentApplicationService;
    }

    /**
     * POST  /payment-applications : Create a new paymentApplication.
     *
     * @param paymentApplicationDTO the paymentApplicationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new paymentApplicationDTO, or with status 400 (Bad Request) if the paymentApplication has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-applications")
    @Timed
    public ResponseEntity<PaymentApplicationDTO> createPaymentApplication(@RequestBody PaymentApplicationDTO paymentApplicationDTO) throws URISyntaxException {
        log.debug("REST request to save PaymentApplication : {}", paymentApplicationDTO);
        if (paymentApplicationDTO.getIdPaymentApplication() != null) {
            throw new BadRequestAlertException("A new paymentApplication cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PaymentApplicationDTO result = paymentApplicationService.save(paymentApplicationDTO);
        return ResponseEntity.created(new URI("/api/payment-applications/" + result.getIdPaymentApplication()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPaymentApplication().toString()))
            .body(result);
    }

    /**
     * POST  /payment-applications/process : Execute Bussiness Process paymentApplication.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-applications/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processPaymentApplication(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PaymentApplication ");
        Map<String, Object> result = paymentApplicationService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /payment-applications/execute : Execute Bussiness Process paymentApplication.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  paymentApplicationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-applications/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedPaymentApplication(HttpServletRequest request, @RequestBody PaymentApplicationDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PaymentApplication");
        Map<String, Object> result = paymentApplicationService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /payment-applications/execute-list : Execute Bussiness Process paymentApplication.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  paymentApplicationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-applications/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListPaymentApplication(HttpServletRequest request, @RequestBody Set<PaymentApplicationDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List PaymentApplication");
        Map<String, Object> result = paymentApplicationService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /payment-applications : Updates an existing paymentApplication.
     *
     * @param paymentApplicationDTO the paymentApplicationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated paymentApplicationDTO,
     * or with status 400 (Bad Request) if the paymentApplicationDTO is not valid,
     * or with status 500 (Internal Server Error) if the paymentApplicationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/payment-applications")
    @Timed
    public ResponseEntity<PaymentApplicationDTO> updatePaymentApplication(@RequestBody PaymentApplicationDTO paymentApplicationDTO) throws URISyntaxException {
        log.debug("REST request to update PaymentApplication : {}", paymentApplicationDTO);
        if (paymentApplicationDTO.getIdPaymentApplication() == null) {
            return createPaymentApplication(paymentApplicationDTO);
        }
        PaymentApplicationDTO result = paymentApplicationService.save(paymentApplicationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentApplicationDTO.getIdPaymentApplication().toString()))
            .body(result);
    }

    /**
     * GET  /payment-applications : get all the paymentApplications.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of paymentApplications in body
     */
    @GetMapping("/payment-applications")
    @Timed
    public ResponseEntity<List<PaymentApplicationDTO>> getAllPaymentApplications(Pageable pageable) {
        log.debug("REST request to get a page of PaymentApplications");
        Page<PaymentApplicationDTO> page = paymentApplicationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/payment-applications");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/payment-applications/filterBy")
    @Timed
    public ResponseEntity<List<PaymentApplicationDTO>> getAllFilteredPaymentApplications(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of PaymentApplications");
        Page<PaymentApplicationDTO> page = paymentApplicationService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/payment-applications/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /payment-applications/:id : get the "id" paymentApplication.
     *
     * @param id the id of the paymentApplicationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the paymentApplicationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/payment-applications/{id}")
    @Timed
    public ResponseEntity<PaymentApplicationDTO> getPaymentApplication(@PathVariable UUID id) {
        log.debug("REST request to get PaymentApplication : {}", id);
        PaymentApplicationDTO paymentApplicationDTO = paymentApplicationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentApplicationDTO));
    }

    /**
     * DELETE  /payment-applications/:id : delete the "id" paymentApplication.
     *
     * @param id the id of the paymentApplicationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/payment-applications/{id}")
    @Timed
    public ResponseEntity<Void> deletePaymentApplication(@PathVariable UUID id) {
        log.debug("REST request to delete PaymentApplication : {}", id);
        paymentApplicationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/payment-applications?query=:query : search for the paymentApplication corresponding
     * to the query.
     *
     * @param query the query of the paymentApplication search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/payment-applications")
    @Timed
    public ResponseEntity<List<PaymentApplicationDTO>> searchPaymentApplications(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of PaymentApplications for query {}", query);
        Page<PaymentApplicationDTO> page = paymentApplicationService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/payment-applications");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
