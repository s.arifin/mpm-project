package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.VehicleWorkRequirementService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VehicleWorkRequirementDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VehicleWorkRequirement.
 */
@RestController
@RequestMapping("/api")
public class VehicleWorkRequirementResource {

    private final Logger log = LoggerFactory.getLogger(VehicleWorkRequirementResource.class);

    private static final String ENTITY_NAME = "vehicleWorkRequirement";

    private final VehicleWorkRequirementService vehicleWorkRequirementService;

    public VehicleWorkRequirementResource(VehicleWorkRequirementService vehicleWorkRequirementService) {
        this.vehicleWorkRequirementService = vehicleWorkRequirementService;
    }

    /**
     * POST  /vehicle-work-requirements : Create a new vehicleWorkRequirement.
     *
     * @param vehicleWorkRequirementDTO the vehicleWorkRequirementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vehicleWorkRequirementDTO, or with status 400 (Bad Request) if the vehicleWorkRequirement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-work-requirements")
    @Timed
    public ResponseEntity<VehicleWorkRequirementDTO> createVehicleWorkRequirement(@RequestBody VehicleWorkRequirementDTO vehicleWorkRequirementDTO) throws URISyntaxException {
        log.debug("REST request to save VehicleWorkRequirement : {}", vehicleWorkRequirementDTO);
        if (vehicleWorkRequirementDTO.getIdRequirement() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new vehicleWorkRequirement cannot already have an ID")).body(null);
        }
        VehicleWorkRequirementDTO result = vehicleWorkRequirementService.save(vehicleWorkRequirementDTO);
        return ResponseEntity.created(new URI("/api/vehicle-work-requirements/" + result.getIdRequirement()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * POST  /vehicle-work-requirements/execute{id}/{param} : Execute Bussiness Process vehicleWorkRequirement.
     *
     * @param vehicleWorkRequirementDTO the vehicleWorkRequirementDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  vehicleWorkRequirementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-work-requirements/execute/{id}/{param}")
    @Timed
    public ResponseEntity<VehicleWorkRequirementDTO> executedVehicleWorkRequirement(@PathVariable Integer id, @PathVariable String param, @RequestBody VehicleWorkRequirementDTO vehicleWorkRequirementDTO) throws URISyntaxException {
        log.debug("REST request to process VehicleWorkRequirement : {}", vehicleWorkRequirementDTO);
        VehicleWorkRequirementDTO result = vehicleWorkRequirementService.processExecuteData(id, param, vehicleWorkRequirementDTO);
        return new ResponseEntity<VehicleWorkRequirementDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vehicle-work-requirements/execute-list{id}/{param} : Execute Bussiness Process vehicleWorkRequirement.
     *
     * @param vehicleWorkRequirementDTO the vehicleWorkRequirementDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  vehicleWorkRequirementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-work-requirements/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<VehicleWorkRequirementDTO>> executedListVehicleWorkRequirement(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<VehicleWorkRequirementDTO> vehicleWorkRequirementDTO) throws URISyntaxException {
        log.debug("REST request to process List VehicleWorkRequirement");
        Set<VehicleWorkRequirementDTO> result = vehicleWorkRequirementService.processExecuteListData(id, param, vehicleWorkRequirementDTO);
        return new ResponseEntity<Set<VehicleWorkRequirementDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /vehicle-work-requirements : Updates an existing vehicleWorkRequirement.
     *
     * @param vehicleWorkRequirementDTO the vehicleWorkRequirementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vehicleWorkRequirementDTO,
     * or with status 400 (Bad Request) if the vehicleWorkRequirementDTO is not valid,
     * or with status 500 (Internal Server Error) if the vehicleWorkRequirementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vehicle-work-requirements")
    @Timed
    public ResponseEntity<VehicleWorkRequirementDTO> updateVehicleWorkRequirement(@RequestBody VehicleWorkRequirementDTO vehicleWorkRequirementDTO) throws URISyntaxException {
        log.debug("REST request to update VehicleWorkRequirement : {}", vehicleWorkRequirementDTO);
        if (vehicleWorkRequirementDTO.getIdRequirement() == null) {
            return createVehicleWorkRequirement(vehicleWorkRequirementDTO);
        }
        VehicleWorkRequirementDTO result = vehicleWorkRequirementService.save(vehicleWorkRequirementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vehicleWorkRequirementDTO.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * GET  /vehicle-work-requirements : get all the vehicleWorkRequirements.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vehicleWorkRequirements in body
     */
    @GetMapping("/vehicle-work-requirements")
    @Timed
    public ResponseEntity<List<VehicleWorkRequirementDTO>> getAllVehicleWorkRequirements(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of VehicleWorkRequirements");
        Page<VehicleWorkRequirementDTO> page = vehicleWorkRequirementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-work-requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /vehicle-work-requirements/:id : get the "id" vehicleWorkRequirement.
     *
     * @param id the id of the vehicleWorkRequirementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vehicleWorkRequirementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vehicle-work-requirements/{id}")
    @Timed
    public ResponseEntity<VehicleWorkRequirementDTO> getVehicleWorkRequirement(@PathVariable UUID id) {
        log.debug("REST request to get VehicleWorkRequirement : {}", id);
        VehicleWorkRequirementDTO vehicleWorkRequirementDTO = vehicleWorkRequirementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vehicleWorkRequirementDTO));
    }

    /**
     * DELETE  /vehicle-work-requirements/:id : delete the "id" vehicleWorkRequirement.
     *
     * @param id the id of the vehicleWorkRequirementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vehicle-work-requirements/{id}")
    @Timed
    public ResponseEntity<Void> deleteVehicleWorkRequirement(@PathVariable UUID id) {
        log.debug("REST request to delete VehicleWorkRequirement : {}", id);
        vehicleWorkRequirementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/vehicle-work-requirements?query=:query : search for the vehicleWorkRequirement corresponding
     * to the query.
     *
     * @param query the query of the vehicleWorkRequirement search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vehicle-work-requirements")
    @Timed
    public ResponseEntity<List<VehicleWorkRequirementDTO>> searchVehicleWorkRequirements(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of VehicleWorkRequirements for query {}", query);
        Page<VehicleWorkRequirementDTO> page = vehicleWorkRequirementService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vehicle-work-requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
