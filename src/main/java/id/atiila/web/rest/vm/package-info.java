/**
 * View Models used by Spring MVC REST controllers.
 */
package id.atiila.web.rest.vm;
