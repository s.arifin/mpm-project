package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.VehicleSalesOrderService;
import id.atiila.service.dto.SalesUnitRequirementDTO;
import id.atiila.service.pto.OrderPTO;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VehicleSalesOrderDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.concurrent.TimeUnit;

/**
 * REST controller for managing VehicleSalesOrder.
 */
@RestController
@RequestMapping("/api")
public class VehicleSalesOrderResource {

    private final Logger log = LoggerFactory.getLogger(VehicleSalesOrderResource.class);

    private static final String ENTITY_NAME = "vehicleSalesOrder";

    private final VehicleSalesOrderService vehicleSalesOrderService;

    public VehicleSalesOrderResource(VehicleSalesOrderService vehicleSalesOrderService) {
        this.vehicleSalesOrderService = vehicleSalesOrderService;
    }

    /**
     * POST  /vehicle-sales-orders : Create a new vehicleSalesOrder.
     *
     * @param vehicleSalesOrderDTO the vehicleSalesOrderDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vehicleSalesOrderDTO, or with status 400 (Bad Request) if the vehicleSalesOrder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-sales-orders")
    @Timed
    public ResponseEntity<VehicleSalesOrderDTO> createVehicleSalesOrder(@RequestBody VehicleSalesOrderDTO vehicleSalesOrderDTO) throws URISyntaxException {
        log.debug("REST request to save VehicleSalesOrder : {}", vehicleSalesOrderDTO);
        if (vehicleSalesOrderDTO.getIdOrder() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new vehicleSalesOrder cannot already have an ID")).body(null);
        }
        VehicleSalesOrderDTO result = vehicleSalesOrderService.save(vehicleSalesOrderDTO);
        return ResponseEntity.created(new URI("/api/vehicle-sales-orders/" + result.getIdOrder()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdOrder().toString()))
            .body(result);
    }

    /**
     * POST  /vehicle-sales-orders/execute{id}/{param} : Execute Bussiness Process vehicleSalesOrder.
     *
     * @param vehicleSalesOrderDTO the vehicleSalesOrderDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  vehicleSalesOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-sales-orders/execute/{id}/{param}")
    @Timed
    public ResponseEntity<VehicleSalesOrderDTO> executedVehicleSalesOrder(@PathVariable Integer id, @PathVariable String param, @RequestBody VehicleSalesOrderDTO vehicleSalesOrderDTO) throws URISyntaxException {
        log.debug("REST request to process VehicleSalesOrder : {}  => masuk ga gan? ", vehicleSalesOrderDTO);
        VehicleSalesOrderDTO result = vehicleSalesOrderService.processExecuteData(id, param, vehicleSalesOrderDTO);
        return new ResponseEntity<VehicleSalesOrderDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vehicle-sales-orders/execute-list{id}/{param} : Execute Bussiness Process vehicleSalesOrder.
     *
     * @param vehicleSalesOrderDTO the vehicleSalesOrderDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  vehicleSalesOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-sales-orders/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<VehicleSalesOrderDTO>> executedListVehicleSalesOrder(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<VehicleSalesOrderDTO> vehicleSalesOrderDTO) throws URISyntaxException {
        log.debug("REST request to process List VehicleSalesOrder");
        Set<VehicleSalesOrderDTO> result = vehicleSalesOrderService.processExecuteListData(id, param, vehicleSalesOrderDTO);
        return new ResponseEntity<Set<VehicleSalesOrderDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /vehicle-sales-orders : Updates an existing vehicleSalesOrder.
     *
     * @param vehicleSalesOrderDTO the vehicleSalesOrderDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vehicleSalesOrderDTO,
     * or with status 400 (Bad Request) if the vehicleSalesOrderDTO is not valid,
     * or with status 500 (Internal Server Error) if the vehicleSalesOrderDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vehicle-sales-orders")
    @Timed
    public ResponseEntity<VehicleSalesOrderDTO> updateVehicleSalesOrder(@RequestBody VehicleSalesOrderDTO vehicleSalesOrderDTO) throws URISyntaxException {
        log.debug("REST request to update VehicleSalesOrder : {}", vehicleSalesOrderDTO);
        if (vehicleSalesOrderDTO.getIdOrder() == null) {
            return createVehicleSalesOrder(vehicleSalesOrderDTO);
        }
        VehicleSalesOrderDTO result = vehicleSalesOrderService.save(vehicleSalesOrderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vehicleSalesOrderDTO.getIdOrder().toString()))
            .body(result);
    }

    /**
     * GET  /vehicle-sales-orders : get all the vehicleSalesOrders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vehicleSalesOrders in body
     */
    @GetMapping("/vehicle-sales-orders")
    @Timed
    public ResponseEntity<List<VehicleSalesOrderDTO>> getAllVehicleSalesOrders(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of VehicleSalesOrders");
        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-sales-orders/manualmatching")
    @Timed
    public ResponseEntity<List<VehicleSalesOrderDTO>> getAllVsoForManualMatching(
        @RequestParam(required = false) String idInternal,
        @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of VehicleSalesOrders Manual Matching");
        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.findAllVsoForManualMatching(idInternal, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-orders/manualmatching");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-sales-orders-leasing/submit")
    @Timed
    public ResponseEntity<List<VehicleSalesOrderDTO>> getAllVehicleSalesOrdersLeasingSubmit(
        @RequestParam(required = false) String idInternal ,
        @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of BillFinanceCompany");
        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.findAllBillFinanceCompanySubmit(idInternal, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-orders-leasing/submit");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-sales-orders-leasing")
    @Timed
    public ResponseEntity<List<VehicleSalesOrderDTO>> getAllVehicleSalesOrdersLeasing(
        @RequestParam(required = false) String idInternal ,
        @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of BillFinanceCompany");
        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.findAllBillFinanceCompany(idInternal, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-orders-leasing");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

//    @GetMapping("/vehicle-sales-orders/findbyReq/{id}")
//    @Timed
//    public ResponseEntity<VehicleSalesOrderDTO> findbyReq(@PathVariable UUID id) {
//        log.debug("REST request to get PickingSlip : {}", id);
//        VehicleSalesOrderDTO vehicleSalesOrderDTO = vehicleSalesOrderService.findbyReq(id);
//        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vehicleSalesOrderDTO));
//    }

    @GetMapping("/vehicle-sales-orders/findSurByOrderId/{id}")
    @Timed
    public ResponseEntity<SalesUnitRequirementDTO> findSurByOrderId(
            @RequestParam(required = false) String idInternal ,
            @PathVariable UUID id) {
        log.debug("REST request to get VehicleSalesOrder : {}", id);
        SalesUnitRequirementDTO salesUnitRequirementDTO = vehicleSalesOrderService.findSurByOrderId(id, idInternal);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(salesUnitRequirementDTO));
    }

    @GetMapping("/vehicle-sales-orders/filterByy")
    @Timed
    public ResponseEntity<List<VehicleSalesOrderDTO>> getAllFilteredVSORequests(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of VehicleCustomerRequests");
        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.queryFilterByy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-orders/filterByy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-sales-orders/filterBy")
    @Timed
    public ResponseEntity<List<VehicleSalesOrderDTO>> getAllFilteredVSO(@ApiParam Pageable pageable,
                                                                        @ApiParam OrderPTO param) {
        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.findFilterBy(pageable, param);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

//    @GetMapping("/vehicle-sales-orders/filterAdminHandling")
//    @Timed
//    public ResponseEntity<List<VehicleSalesOrderDTO>> getAllFilteredAdminHandling(@ApiParam Pageable pageable,
//                                                                        @ApiParam OrderPTO param) {
//        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.findFilterAdminHandling(pageable, param);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-orders");
//        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
//    }



    /**
     * GET  /vehicle-sales-orders/:id : get the "id" vehicleSalesOrder.
     *
     * @param id the id of the vehicleSalesOrderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vehicleSalesOrderDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vehicle-sales-orders/{id}")
    @Timed
    public ResponseEntity<VehicleSalesOrderDTO> getVehicleSalesOrder(
            @PathVariable UUID id,
            @RequestParam(required = false) String idInternal) {
        log.debug("REST request to get VehicleSalesOrder : {}", id);
        VehicleSalesOrderDTO vehicleSalesOrderDTO = vehicleSalesOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vehicleSalesOrderDTO));
    }

    /**
     * DELETE  /vehicle-sales-orders/:id : delete the "id" vehicleSalesOrder.
     *
     * @param id the id of the vehicleSalesOrderDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vehicle-sales-orders/{id}")
    @Timed
    public ResponseEntity<Void> deleteVehicleSalesOrder(@PathVariable UUID id) {
        log.debug("REST request to delete VehicleSalesOrder : {}", id);
        vehicleSalesOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/vehicle-sales-orders?query=:query : search for the vehicleSalesOrder corresponding
     * to the query.
     *
     * @param query the query of the vehicleSalesOrder search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vehicle-sales-orders")
    @Timed
    public ResponseEntity<List<VehicleSalesOrderDTO>> searchVehicleSalesOrders(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
//        log.debug("REST request to search for a page of VehicleSalesOrders for query {}", query);
//        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.search(request, query, pageable);
//        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vehicle-sales-orders");
//        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
//    }public ResponseEntity<List<UnitOrderItemDTO>> searchUnitOrderItems(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of OrderItems for query {}", query);
        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vehicle-sales-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/_search/vehicle-sales-orders/adminHandling")
    @Timed
    public ResponseEntity<List<VehicleSalesOrderDTO>> searchVehicleSalesOrdersAdminHandling(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of OrderItems for query {}", query);
        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.searchAdminHandling(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vehicle-sales-orders/adminHandling");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @GetMapping("/vehicle-sales-orders/filter")
    @Timed
    public ResponseEntity<List<VehicleSalesOrderDTO>> getAllFilteredOrderItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of vehicle-sales-orders");
        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-orders/filter");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-sales-orders/tosearch")
    @Timed
    public ResponseEntity<List<VehicleSalesOrderDTO>> getToVsoSearch(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of vehicle-sales-orders");
        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.toSearch(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-orders/tosearch");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-sales-orders/tosearchAH")
    @Timed
    public ResponseEntity<List<VehicleSalesOrderDTO>> getToAdminHandlingSearch(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of vehicle-sales-orders");
        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.toSearchAH(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-orders/tosearchAH");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-sales-orders/tosearchAHSubmit")
    @Timed
    public ResponseEntity<List<VehicleSalesOrderDTO>> getToAdminHandlingSearchSubmit(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of vehicle-sales-orders");
        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.toSearchAH(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-orders/tosearchAHSubmit");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/vehicle-sales-orders/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processVSOIndex(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process OrderItem ");
        Map<String, Object> result = vehicleSalesOrderService.processIndex(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    @PostMapping("/vehicle-sales-orders/processAdminHandling")
    @Timed
    public ResponseEntity<Map<String, Object>> processVSOIndexAdminHandling(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process OrderItem ");
        Map<String, Object> result = vehicleSalesOrderService.processIndexAdminHandling(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    @GetMapping("/vehicle-sales-orders/approve-kacab")
    @Timed
    public ResponseEntity<List<VehicleSalesOrderDTO>> getAllVsoForMApproveKacab(
        @RequestParam(required = false) String idInternal,
        @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of VehicleSalesOrders Approve Kacab");
        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.findAllVsoForApproveKacab(idInternal, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-orders/approve-kacab");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-sales-orders/findByidOrderAndBillingNumber")
    @Timed
    public ResponseEntity<VehicleSalesOrderDTO> findByidOrderAndBillingNumber(@RequestParam(required = false) String billingNumber) {
        log.debug("REST request to get a page of VehicleSalesOrder Find By Id Requirement and Deliverble Type");
        VehicleSalesOrderDTO vso = vehicleSalesOrderService.findByIdReqandBill(billingNumber);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vso));
    }

    @GetMapping("/vehicle-sales-order-sales")
    @Timed
    public ResponseEntity<List<VehicleSalesOrderDTO>> getAllVehicleSalesOrderSales(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of VehicleSalesOrders");
        Page<VehicleSalesOrderDTO> page = vehicleSalesOrderService.findAllVSOSales(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-order-sales");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PutMapping ("/vehicle-sales-orders/update")
    @Timed
    public ResponseEntity<VehicleSalesOrderDTO> updateAdminHandling (@RequestBody VehicleSalesOrderDTO vehicleSalesOrderDTO) throws URISyntaxException {
        log.debug("REST request to update VehicleSalesOrder : {}", vehicleSalesOrderDTO);
        log.debug("ckeck id Order" + vehicleSalesOrderDTO.getIdOrder());
        if (vehicleSalesOrderDTO.getIdOrder() == null) {
            return createVehicleSalesOrder(vehicleSalesOrderDTO);
        }
        VehicleSalesOrderDTO result = vehicleSalesOrderService.update(vehicleSalesOrderDTO);
        return ResponseEntity.ok()
            .headers (HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vehicleSalesOrderDTO.getIdOrder().toString()))
            .body(result);
    }

}
