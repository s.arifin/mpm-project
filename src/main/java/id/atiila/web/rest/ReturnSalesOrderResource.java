package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ReturnSalesOrderService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ReturnSalesOrderDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ReturnSalesOrder.
 */
@RestController
@RequestMapping("/api")
public class ReturnSalesOrderResource {

    private final Logger log = LoggerFactory.getLogger(ReturnSalesOrderResource.class);

    private static final String ENTITY_NAME = "returnSalesOrder";

    private final ReturnSalesOrderService returnSalesOrderService;

    public ReturnSalesOrderResource(ReturnSalesOrderService returnSalesOrderService) {
        this.returnSalesOrderService = returnSalesOrderService;
    }

    /**
     * POST  /return-sales-orders : Create a new returnSalesOrder.
     *
     * @param returnSalesOrderDTO the returnSalesOrderDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new returnSalesOrderDTO, or with status 400 (Bad Request) if the returnSalesOrder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/return-sales-orders")
    @Timed
    public ResponseEntity<ReturnSalesOrderDTO> createReturnSalesOrder(@RequestBody ReturnSalesOrderDTO returnSalesOrderDTO) throws URISyntaxException {
        log.debug("REST request to save ReturnSalesOrder : {}", returnSalesOrderDTO);
        if (returnSalesOrderDTO.getIdOrder() != null) {
            throw new BadRequestAlertException("A new returnSalesOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ReturnSalesOrderDTO result = returnSalesOrderService.save(returnSalesOrderDTO);
        return ResponseEntity.created(new URI("/api/return-sales-orders/" + result.getIdOrder()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdOrder().toString()))
            .body(result);
    }

    /**
     * POST  /return-sales-orders/process : Execute Bussiness Process returnSalesOrder.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/return-sales-orders/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processReturnSalesOrder(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ReturnSalesOrder ");
        Map<String, Object> result = returnSalesOrderService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /return-sales-orders/execute : Execute Bussiness Process returnSalesOrder.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  returnSalesOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/return-sales-orders/execute")
    @Timed
    public ResponseEntity<ReturnSalesOrderDTO> executedReturnSalesOrder(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ReturnSalesOrder");
        ReturnSalesOrderDTO result = returnSalesOrderService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<ReturnSalesOrderDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /return-sales-orders/execute-list : Execute Bussiness Process returnSalesOrder.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  returnSalesOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/return-sales-orders/execute-list")
    @Timed
    public ResponseEntity<Set<ReturnSalesOrderDTO>> executedListReturnSalesOrder(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List ReturnSalesOrder");
        Set<ReturnSalesOrderDTO> result = returnSalesOrderService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<ReturnSalesOrderDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /return-sales-orders : Updates an existing returnSalesOrder.
     *
     * @param returnSalesOrderDTO the returnSalesOrderDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated returnSalesOrderDTO,
     * or with status 400 (Bad Request) if the returnSalesOrderDTO is not valid,
     * or with status 500 (Internal Server Error) if the returnSalesOrderDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/return-sales-orders")
    @Timed
    public ResponseEntity<ReturnSalesOrderDTO> updateReturnSalesOrder(@RequestBody ReturnSalesOrderDTO returnSalesOrderDTO) throws URISyntaxException {
        log.debug("REST request to update ReturnSalesOrder : {}", returnSalesOrderDTO);
        if (returnSalesOrderDTO.getIdOrder() == null) {
            return createReturnSalesOrder(returnSalesOrderDTO);
        }
        ReturnSalesOrderDTO result = returnSalesOrderService.save(returnSalesOrderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, returnSalesOrderDTO.getIdOrder().toString()))
            .body(result);
    }

    /**
     * GET  /return-sales-orders : get all the returnSalesOrders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of returnSalesOrders in body
     */
    @GetMapping("/return-sales-orders")
    @Timed
    public ResponseEntity<List<ReturnSalesOrderDTO>> getAllReturnSalesOrders(Pageable pageable) {
        log.debug("REST request to get a page of ReturnSalesOrders");
        Page<ReturnSalesOrderDTO> page = returnSalesOrderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/return-sales-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/return-sales-orders/filterBy")
    @Timed
    public ResponseEntity<List<ReturnSalesOrderDTO>> getAllFilteredReturnSalesOrders(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ReturnSalesOrders");
        Page<ReturnSalesOrderDTO> page = returnSalesOrderService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/return-sales-orders/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /return-sales-orders/:id : get the "id" returnSalesOrder.
     *
     * @param id the id of the returnSalesOrderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the returnSalesOrderDTO, or with status 404 (Not Found)
     */
    @GetMapping("/return-sales-orders/{id}")
    @Timed
    public ResponseEntity<ReturnSalesOrderDTO> getReturnSalesOrder(@PathVariable UUID id) {
        log.debug("REST request to get ReturnSalesOrder : {}", id);
        ReturnSalesOrderDTO returnSalesOrderDTO = returnSalesOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(returnSalesOrderDTO));
    }

    /**
     * DELETE  /return-sales-orders/:id : delete the "id" returnSalesOrder.
     *
     * @param id the id of the returnSalesOrderDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/return-sales-orders/{id}")
    @Timed
    public ResponseEntity<Void> deleteReturnSalesOrder(@PathVariable UUID id) {
        log.debug("REST request to delete ReturnSalesOrder : {}", id);
        returnSalesOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/return-sales-orders?query=:query : search for the returnSalesOrder corresponding
     * to the query.
     *
     * @param query the query of the returnSalesOrder search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/return-sales-orders")
    @Timed
    public ResponseEntity<List<ReturnSalesOrderDTO>> searchReturnSalesOrders(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ReturnSalesOrders for query {}", query);
        Page<ReturnSalesOrderDTO> page = returnSalesOrderService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/return-sales-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/return-sales-orders/set-status/{id}")
    @Timed
    public ResponseEntity< ReturnSalesOrderDTO> setStatusReturnSalesOrder(@PathVariable Integer id, @RequestBody ReturnSalesOrderDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status ReturnSalesOrder : {}", dto);
        ReturnSalesOrderDTO r = returnSalesOrderService.changeReturnSalesOrderStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
