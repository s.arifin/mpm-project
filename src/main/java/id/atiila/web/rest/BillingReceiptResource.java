package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.BillingReceiptService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.BillingReceiptDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BillingReceipt.
 */
@RestController
@RequestMapping("/api")
public class BillingReceiptResource {

    private final Logger log = LoggerFactory.getLogger(BillingReceiptResource.class);

    private static final String ENTITY_NAME = "billingReceipt";

    private final BillingReceiptService billingReceiptService;

    public BillingReceiptResource(BillingReceiptService billingReceiptService) {
        this.billingReceiptService = billingReceiptService;
    }

    /**
     * POST  /billing-receipts : Create a new billingReceipt.
     *
     * @param billingReceiptDTO the billingReceiptDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new billingReceiptDTO, or with status 400 (Bad Request) if the billingReceipt has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-receipts")
    @Timed
    public ResponseEntity<BillingReceiptDTO> createBillingReceipt(@RequestBody BillingReceiptDTO billingReceiptDTO) throws URISyntaxException {
        log.debug("REST request to save BillingReceipt : {}", billingReceiptDTO);
        if (billingReceiptDTO.getIdBilling() != null) {
            throw new BadRequestAlertException("A new billingReceipt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BillingReceiptDTO result = billingReceiptService.save(billingReceiptDTO);
        return ResponseEntity.created(new URI("/api/billing-receipts/" + result.getIdBilling()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdBilling().toString()))
            .body(result);
    }

    /**
     * POST  /billing-receipts/process : Execute Bussiness Process billingReceipt.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-receipts/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processBillingReceipt(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process BillingReceipt ");
        Map<String, Object> result = billingReceiptService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /billing-receipts/execute : Execute Bussiness Process billingReceipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  billingReceiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-receipts/execute")
    @Timed
    public ResponseEntity<BillingReceiptDTO> executedBillingReceipt(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process BillingReceipt");
        BillingReceiptDTO result = billingReceiptService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<BillingReceiptDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /billing-receipts/execute-list : Execute Bussiness Process billingReceipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  billingReceiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-receipts/execute-list")
    @Timed
    public ResponseEntity<Set<BillingReceiptDTO>> executedListBillingReceipt(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List BillingReceipt");
        Set<BillingReceiptDTO> result = billingReceiptService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<BillingReceiptDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /billing-receipts : Updates an existing billingReceipt.
     *
     * @param billingReceiptDTO the billingReceiptDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated billingReceiptDTO,
     * or with status 400 (Bad Request) if the billingReceiptDTO is not valid,
     * or with status 500 (Internal Server Error) if the billingReceiptDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/billing-receipts")
    @Timed
    public ResponseEntity<BillingReceiptDTO> updateBillingReceipt(@RequestBody BillingReceiptDTO billingReceiptDTO) throws URISyntaxException {
        log.debug("REST request to update BillingReceipt : {}", billingReceiptDTO);
        if (billingReceiptDTO.getIdBilling() == null) {
            return createBillingReceipt(billingReceiptDTO);
        }
        BillingReceiptDTO result = billingReceiptService.save(billingReceiptDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, billingReceiptDTO.getIdBilling().toString()))
            .body(result);
    }

    /**
     * GET  /billing-receipts : get all the billingReceipts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of billingReceipts in body
     */
    @GetMapping("/billing-receipts")
    @Timed
    public ResponseEntity<List<BillingReceiptDTO>> getAllBillingReceipts(Pageable pageable) {
        log.debug("REST request to get a page of BillingReceipts");
        Page<BillingReceiptDTO> page = billingReceiptService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/billing-receipts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/billing-receipts/filterBy")
    @Timed
    public ResponseEntity<List<BillingReceiptDTO>> getAllFilteredBillingReceipts(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of BillingReceipts");
        Page<BillingReceiptDTO> page = billingReceiptService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/billing-receipts/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /billing-receipts/:id : get the "id" billingReceipt.
     *
     * @param id the id of the billingReceiptDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the billingReceiptDTO, or with status 404 (Not Found)
     */
    @GetMapping("/billing-receipts/{id}")
    @Timed
    public ResponseEntity<BillingReceiptDTO> getBillingReceipt(@PathVariable UUID id) {
        log.debug("REST request to get BillingReceipt : {}", id);
        BillingReceiptDTO billingReceiptDTO = billingReceiptService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(billingReceiptDTO));
    }

    /**
     * DELETE  /billing-receipts/:id : delete the "id" billingReceipt.
     *
     * @param id the id of the billingReceiptDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/billing-receipts/{id}")
    @Timed
    public ResponseEntity<Void> deleteBillingReceipt(@PathVariable UUID id) {
        log.debug("REST request to delete BillingReceipt : {}", id);
        billingReceiptService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/billing-receipts?query=:query : search for the billingReceipt corresponding
     * to the query.
     *
     * @param query the query of the billingReceipt search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/billing-receipts")
    @Timed
    public ResponseEntity<List<BillingReceiptDTO>> searchBillingReceipts(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of BillingReceipts for query {}", query);
        Page<BillingReceiptDTO> page = billingReceiptService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/billing-receipts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/billing-receipts/set-status/{id}")
    @Timed
    public ResponseEntity< BillingReceiptDTO> setStatusBillingReceipt(@PathVariable Integer id, @RequestBody BillingReceiptDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status BillingReceipt : {}", dto);
        BillingReceiptDTO r = billingReceiptService.changeBillingReceiptStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
