package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.SalesSourceService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.SalesSourceDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SalesSource.
 */
@RestController
@RequestMapping("/api")
public class SalesSourceResource {

    private final Logger log = LoggerFactory.getLogger(SalesSourceResource.class);

    private static final String ENTITY_NAME = "salesSource";

    private final SalesSourceService salesSourceService;

    public SalesSourceResource(SalesSourceService salesSourceService) {
        this.salesSourceService = salesSourceService;
    }

    /**
     * POST  /sales-sources : Create a new salesSource.
     *
     * @param salesSourceDTO the salesSourceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new salesSourceDTO, or with status 400 (Bad Request) if the salesSource has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-sources")
    @Timed
    public ResponseEntity<SalesSourceDTO> createSalesSource(@RequestBody SalesSourceDTO salesSourceDTO) throws URISyntaxException {
        log.debug("REST request to save SalesSource : {}", salesSourceDTO);
        if (salesSourceDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new salesSource cannot already have an ID")).body(null);
        }
        SalesSourceDTO result = salesSourceService.save(salesSourceDTO);
        
        return ResponseEntity.created(new URI("/api/sales-sources/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * POST  /sales-sources/execute : Execute Bussiness Process salesSource.
     *
     * @param salesSourceDTO the salesSourceDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  salesSourceDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-sources/execute")
    @Timed
    public ResponseEntity<SalesSourceDTO> executedSalesSource(@RequestBody SalesSourceDTO salesSourceDTO) throws URISyntaxException {
        log.debug("REST request to process SalesSource : {}", salesSourceDTO);
        return new ResponseEntity<SalesSourceDTO>(salesSourceDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /sales-sources : Updates an existing salesSource.
     *
     * @param salesSourceDTO the salesSourceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated salesSourceDTO,
     * or with status 400 (Bad Request) if the salesSourceDTO is not valid,
     * or with status 500 (Internal Server Error) if the salesSourceDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sales-sources")
    @Timed
    public ResponseEntity<SalesSourceDTO> updateSalesSource(@RequestBody SalesSourceDTO salesSourceDTO) throws URISyntaxException {
        log.debug("REST request to update SalesSource : {}", salesSourceDTO);
        if (salesSourceDTO.getId() == null) {
            return createSalesSource(salesSourceDTO);
        }
        SalesSourceDTO result = salesSourceService.save(salesSourceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, salesSourceDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sales-sources : get all the salesSources.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of salesSources in body
     */
    @GetMapping("/sales-sources")
    @Timed
    public ResponseEntity<List<SalesSourceDTO>> getAllSalesSources(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SalesSources");
        Page<SalesSourceDTO> page = salesSourceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-sources");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /sales-sources/:id : get the "id" salesSource.
     *
     * @param id the id of the salesSourceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the salesSourceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sales-sources/{id}")
    @Timed
    public ResponseEntity<SalesSourceDTO> getSalesSource(@PathVariable Long id) {
        log.debug("REST request to get SalesSource : {}", id);
        SalesSourceDTO salesSourceDTO = salesSourceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(salesSourceDTO));
    }

    /**
     * DELETE  /sales-sources/:id : delete the "id" salesSource.
     *
     * @param id the id of the salesSourceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sales-sources/{id}")
    @Timed
    public ResponseEntity<Void> deleteSalesSource(@PathVariable Long id) {
        log.debug("REST request to delete SalesSource : {}", id);
        salesSourceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/sales-sources?query=:query : search for the salesSource corresponding
     * to the query.
     *
     * @param query the query of the salesSource search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/sales-sources")
    @Timed
    public ResponseEntity<List<SalesSourceDTO>> searchSalesSources(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SalesSources for query {}", query);
        Page<SalesSourceDTO> page = salesSourceService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/sales-sources");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
