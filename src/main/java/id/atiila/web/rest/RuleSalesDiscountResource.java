package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RuleSalesDiscountService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RuleSalesDiscountDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RuleSalesDiscount.
 */
@RestController
@RequestMapping("/api")
public class RuleSalesDiscountResource {

    private final Logger log = LoggerFactory.getLogger(RuleSalesDiscountResource.class);

    private static final String ENTITY_NAME = "ruleSalesDiscount";

    private final RuleSalesDiscountService ruleSalesDiscountService;

    public RuleSalesDiscountResource(RuleSalesDiscountService ruleSalesDiscountService) {
        this.ruleSalesDiscountService = ruleSalesDiscountService;
    }

    /**
     * POST  /rule-sales-discounts : Create a new ruleSalesDiscount.
     *
     * @param ruleSalesDiscountDTO the ruleSalesDiscountDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ruleSalesDiscountDTO, or with status 400 (Bad Request) if the ruleSalesDiscount has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rule-sales-discounts")
    @Timed
    public ResponseEntity<RuleSalesDiscountDTO> createRuleSalesDiscount(@RequestBody RuleSalesDiscountDTO ruleSalesDiscountDTO) throws URISyntaxException {
        log.debug("REST request to save RuleSalesDiscount : {}", ruleSalesDiscountDTO);
        if (ruleSalesDiscountDTO.getIdRule() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new ruleSalesDiscount cannot already have an ID")).body(null);
        }
        RuleSalesDiscountDTO result = ruleSalesDiscountService.save(ruleSalesDiscountDTO);
        return ResponseEntity.created(new URI("/api/rule-sales-discounts/" + result.getIdRule()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRule().toString()))
            .body(result);
    }

    /**
     * POST  /rule-sales-discounts/execute{id}/{param} : Execute Bussiness Process ruleSalesDiscount.
     *
     * @param ruleSalesDiscountDTO the ruleSalesDiscountDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  ruleSalesDiscountDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rule-sales-discounts/execute/{id}/{param}")
    @Timed
    public ResponseEntity<RuleSalesDiscountDTO> executedRuleSalesDiscount(@PathVariable Integer id, @PathVariable String param, @RequestBody RuleSalesDiscountDTO ruleSalesDiscountDTO) throws URISyntaxException {
        log.debug("REST request to process RuleSalesDiscount : {}", ruleSalesDiscountDTO);
        RuleSalesDiscountDTO result = ruleSalesDiscountService.processExecuteData(id, param, ruleSalesDiscountDTO);
        return new ResponseEntity<RuleSalesDiscountDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /rule-sales-discounts/execute-list{id}/{param} : Execute Bussiness Process ruleSalesDiscount.
     *
     * @param ruleSalesDiscountDTO the ruleSalesDiscountDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  ruleSalesDiscountDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rule-sales-discounts/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<RuleSalesDiscountDTO>> executedListRuleSalesDiscount(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<RuleSalesDiscountDTO> ruleSalesDiscountDTO) throws URISyntaxException {
        log.debug("REST request to process List RuleSalesDiscount");
        Set<RuleSalesDiscountDTO> result = ruleSalesDiscountService.processExecuteListData(id, param, ruleSalesDiscountDTO);
        return new ResponseEntity<Set<RuleSalesDiscountDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /rule-sales-discounts : Updates an existing ruleSalesDiscount.
     *
     * @param ruleSalesDiscountDTO the ruleSalesDiscountDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ruleSalesDiscountDTO,
     * or with status 400 (Bad Request) if the ruleSalesDiscountDTO is not valid,
     * or with status 500 (Internal Server Error) if the ruleSalesDiscountDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rule-sales-discounts")
    @Timed
    public ResponseEntity<RuleSalesDiscountDTO> updateRuleSalesDiscount(@RequestBody RuleSalesDiscountDTO ruleSalesDiscountDTO) throws URISyntaxException {
        log.debug("REST request to update RuleSalesDiscount : {}", ruleSalesDiscountDTO);
        if (ruleSalesDiscountDTO.getIdRule() == null) {
            return createRuleSalesDiscount(ruleSalesDiscountDTO);
        }
        RuleSalesDiscountDTO result = ruleSalesDiscountService.save(ruleSalesDiscountDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ruleSalesDiscountDTO.getIdRule().toString()))
            .body(result);
    }

    @GetMapping("/rule-sales-discounts/by-internal")
    @Timed
    public ResponseEntity<List<RuleSalesDiscountDTO>> getAllRuleSalesDiscountsByInternal(@RequestParam String idinternal ,@ApiParam Pageable pageable){
        log.debug("REST request to get RuleSalesDiscount By Internal");
        Page<RuleSalesDiscountDTO> page = ruleSalesDiscountService.findAllByInternal(idinternal ,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rule-sales-discounts/by-internal");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rule-sales-discounts : get all the ruleSalesDiscounts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of ruleSalesDiscounts in body
     */
    @GetMapping("/rule-sales-discounts")
    @Timed
    public ResponseEntity<List<RuleSalesDiscountDTO>> getAllRuleSalesDiscounts(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of RuleSalesDiscounts");
        Page<RuleSalesDiscountDTO> page = ruleSalesDiscountService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rule-sales-discounts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /rule-sales-discounts/:id : get the "id" ruleSalesDiscount.
     *
     * @param id the id of the ruleSalesDiscountDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ruleSalesDiscountDTO, or with status 404 (Not Found)
     */
    @GetMapping("/rule-sales-discounts/{id}")
    @Timed
    public ResponseEntity<RuleSalesDiscountDTO> getRuleSalesDiscount(@PathVariable Integer id) {
        log.debug("REST request to get RuleSalesDiscount : {}", id);
        RuleSalesDiscountDTO ruleSalesDiscountDTO = ruleSalesDiscountService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ruleSalesDiscountDTO));
    }

    /**
     * DELETE  /rule-sales-discounts/:id : delete the "id" ruleSalesDiscount.
     *
     * @param id the id of the ruleSalesDiscountDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rule-sales-discounts/{id}")
    @Timed
    public ResponseEntity<Void> deleteRuleSalesDiscount(@PathVariable Integer id) {
        log.debug("REST request to delete RuleSalesDiscount : {}", id);
        ruleSalesDiscountService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/rule-sales-discounts?query=:query : search for the ruleSalesDiscount corresponding
     * to the query.
     *
     * @param query the query of the ruleSalesDiscount search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/rule-sales-discounts")
    @Timed
    public ResponseEntity<List<RuleSalesDiscountDTO>> searchRuleSalesDiscounts(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of RuleSalesDiscounts for query {}", query);
        Page<RuleSalesDiscountDTO> page = ruleSalesDiscountService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/rule-sales-discounts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
