package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ContainerService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ContainerDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Container.
 */
@RestController
@RequestMapping("/api")
public class ContainerResource {

    private final Logger log = LoggerFactory.getLogger(ContainerResource.class);

    private static final String ENTITY_NAME = "container";

    private final ContainerService containerService;

    public ContainerResource(ContainerService containerService) {
        this.containerService = containerService;
    }

    /**
     * POST  /containers : Create a new container.
     *
     * @param containerDTO the containerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new containerDTO, or with status 400 (Bad Request) if the container has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/containers")
    @Timed
    public ResponseEntity<ContainerDTO> createContainer(@RequestBody ContainerDTO containerDTO) throws URISyntaxException {
        log.debug("REST request to save Container : {}", containerDTO);
        if (containerDTO.getIdContainer() != null) {
            throw new BadRequestAlertException("A new container cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContainerDTO result = containerService.save(containerDTO);
        return ResponseEntity.created(new URI("/api/containers/" + result.getIdContainer()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdContainer().toString()))
            .body(result);
    }

    /**
     * POST  /containers/process : Execute Bussiness Process container.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/containers/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processContainer(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Container ");
        Map<String, Object> result = containerService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /containers/execute : Execute Bussiness Process container.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  containerDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/containers/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedContainer(HttpServletRequest request, @RequestBody ContainerDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Container");
        Map<String, Object> result = containerService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /containers/execute-list : Execute Bussiness Process container.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  containerDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/containers/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListContainer(HttpServletRequest request, @RequestBody Set<ContainerDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List Container");
        Map<String, Object> result = containerService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /containers : Updates an existing container.
     *
     * @param containerDTO the containerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated containerDTO,
     * or with status 400 (Bad Request) if the containerDTO is not valid,
     * or with status 500 (Internal Server Error) if the containerDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/containers")
    @Timed
    public ResponseEntity<ContainerDTO> updateContainer(@RequestBody ContainerDTO containerDTO) throws URISyntaxException {
        log.debug("REST request to update Container : {}", containerDTO);
        if (containerDTO.getIdContainer() == null) {
            return createContainer(containerDTO);
        }
        ContainerDTO result = containerService.save(containerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, containerDTO.getIdContainer().toString()))
            .body(result);
    }

    /**
     * GET  /containers : get all the containers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of containers in body
     */
    @GetMapping("/containers")
    @Timed
    public ResponseEntity<List<ContainerDTO>> getAllContainers(Pageable pageable) {
        log.debug("REST request to get a page of Containers");
        Page<ContainerDTO> page = containerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/containers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/containers/filterBy")
    @Timed
    public ResponseEntity<List<ContainerDTO>> getAllFilteredContainers(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Containers");
        Page<ContainerDTO> page = containerService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/containers/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /containers/:id : get the "id" container.
     *
     * @param id the id of the containerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the containerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/containers/{id}")
    @Timed
    public ResponseEntity<ContainerDTO> getContainer(@PathVariable UUID id) {
        log.debug("REST request to get Container : {}", id);
        ContainerDTO containerDTO = containerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(containerDTO));
    }

    /**
     * DELETE  /containers/:id : delete the "id" container.
     *
     * @param id the id of the containerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/containers/{id}")
    @Timed
    public ResponseEntity<Void> deleteContainer(@PathVariable UUID id) {
        log.debug("REST request to delete Container : {}", id);
        containerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/containers?query=:query : search for the container corresponding
     * to the query.
     *
     * @param query the query of the container search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/containers")
    @Timed
    public ResponseEntity<List<ContainerDTO>> searchContainers(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Containers for query {}", query);
        Page<ContainerDTO> page = containerService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/containers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/containers/get-container-by-facility-id/{id}")
    @Timed
    public ResponseEntity<List<ContainerDTO>> findByFacilityId(@PathVariable String id){
        UUID facilityId = UUID.fromString(id);
        List<ContainerDTO> dto =  containerService.findByFacilityId(facilityId);
        return new ResponseEntity<List<ContainerDTO>>(dto, null, HttpStatus.OK);
    }
}
