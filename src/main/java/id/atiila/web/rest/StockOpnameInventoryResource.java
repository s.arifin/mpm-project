package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.StockOpnameInventoryService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.StockOpnameInventoryDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing StockOpnameInventory.
 */
@RestController
@RequestMapping("/api")
public class StockOpnameInventoryResource {

    private final Logger log = LoggerFactory.getLogger(StockOpnameInventoryResource.class);

    private static final String ENTITY_NAME = "stockOpnameInventory";

    private final StockOpnameInventoryService stockOpnameInventoryService;

    public StockOpnameInventoryResource(StockOpnameInventoryService stockOpnameInventoryService) {
        this.stockOpnameInventoryService = stockOpnameInventoryService;
    }

    /**
     * POST  /stock-opname-inventories : Create a new stockOpnameInventory.
     *
     * @param stockOpnameInventoryDTO the stockOpnameInventoryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new stockOpnameInventoryDTO, or with status 400 (Bad Request) if the stockOpnameInventory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opname-inventories")
    @Timed
    public ResponseEntity<StockOpnameInventoryDTO> createStockOpnameInventory(@RequestBody StockOpnameInventoryDTO stockOpnameInventoryDTO) throws URISyntaxException {
        log.debug("REST request to save StockOpnameInventory : {}", stockOpnameInventoryDTO);
        if (stockOpnameInventoryDTO.getIdStockOpnameInventory() != null) {
            throw new BadRequestAlertException("A new stockOpnameInventory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StockOpnameInventoryDTO result = stockOpnameInventoryService.save(stockOpnameInventoryDTO);
        return ResponseEntity.created(new URI("/api/stock-opname-inventories/" + result.getIdStockOpnameInventory()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdStockOpnameInventory().toString()))
            .body(result);
    }

    /**
     * POST  /stock-opname-inventories/process : Execute Bussiness Process stockOpnameInventory.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opname-inventories/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processStockOpnameInventory(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process StockOpnameInventory ");
        Map<String, Object> result = stockOpnameInventoryService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /stock-opname-inventories/execute : Execute Bussiness Process stockOpnameInventory.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  stockOpnameInventoryDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opname-inventories/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedStockOpnameInventory(HttpServletRequest request, @RequestBody StockOpnameInventoryDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process StockOpnameInventory");
        Map<String, Object> result = stockOpnameInventoryService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /stock-opname-inventories/execute-list : Execute Bussiness Process stockOpnameInventory.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  stockOpnameInventoryDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opname-inventories/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListStockOpnameInventory(HttpServletRequest request, @RequestBody Set<StockOpnameInventoryDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List StockOpnameInventory");
        Map<String, Object> result = stockOpnameInventoryService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /stock-opname-inventories : Updates an existing stockOpnameInventory.
     *
     * @param stockOpnameInventoryDTO the stockOpnameInventoryDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated stockOpnameInventoryDTO,
     * or with status 400 (Bad Request) if the stockOpnameInventoryDTO is not valid,
     * or with status 500 (Internal Server Error) if the stockOpnameInventoryDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/stock-opname-inventories")
    @Timed
    public ResponseEntity<StockOpnameInventoryDTO> updateStockOpnameInventory(@RequestBody StockOpnameInventoryDTO stockOpnameInventoryDTO) throws URISyntaxException {
        log.debug("REST request to update StockOpnameInventory : {}", stockOpnameInventoryDTO);
        if (stockOpnameInventoryDTO.getIdStockOpnameInventory() == null) {
            return createStockOpnameInventory(stockOpnameInventoryDTO);
        }
        StockOpnameInventoryDTO result = stockOpnameInventoryService.save(stockOpnameInventoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, stockOpnameInventoryDTO.getIdStockOpnameInventory().toString()))
            .body(result);
    }

    /**
     * GET  /stock-opname-inventories : get all the stockOpnameInventories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of stockOpnameInventories in body
     */
    @GetMapping("/stock-opname-inventories")
    @Timed
    public ResponseEntity<List<StockOpnameInventoryDTO>> getAllStockOpnameInventories(Pageable pageable) {
        log.debug("REST request to get a page of StockOpnameInventories");
        Page<StockOpnameInventoryDTO> page = stockOpnameInventoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/stock-opname-inventories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/stock-opname-inventories/filterBy")
    @Timed
    public ResponseEntity<List<StockOpnameInventoryDTO>> getAllFilteredStockOpnameInventories(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of StockOpnameInventories");
        Page<StockOpnameInventoryDTO> page = stockOpnameInventoryService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/stock-opname-inventories/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /stock-opname-inventories/:id : get the "id" stockOpnameInventory.
     *
     * @param id the id of the stockOpnameInventoryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the stockOpnameInventoryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/stock-opname-inventories/{id}")
    @Timed
    public ResponseEntity<StockOpnameInventoryDTO> getStockOpnameInventory(@PathVariable UUID id) {
        log.debug("REST request to get StockOpnameInventory : {}", id);
        StockOpnameInventoryDTO stockOpnameInventoryDTO = stockOpnameInventoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(stockOpnameInventoryDTO));
    }

    /**
     * DELETE  /stock-opname-inventories/:id : delete the "id" stockOpnameInventory.
     *
     * @param id the id of the stockOpnameInventoryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/stock-opname-inventories/{id}")
    @Timed
    public ResponseEntity<Void> deleteStockOpnameInventory(@PathVariable UUID id) {
        log.debug("REST request to delete StockOpnameInventory : {}", id);
        stockOpnameInventoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/stock-opname-inventories?query=:query : search for the stockOpnameInventory corresponding
     * to the query.
     *
     * @param query the query of the stockOpnameInventory search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/stock-opname-inventories")
    @Timed
    public ResponseEntity<List<StockOpnameInventoryDTO>> searchStockOpnameInventories(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of StockOpnameInventories for query {}", query);
        Page<StockOpnameInventoryDTO> page = stockOpnameInventoryService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/stock-opname-inventories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
