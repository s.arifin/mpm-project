package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ProspectOrganizationService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ProspectOrganizationDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProspectOrganization.
 */
@RestController
@RequestMapping("/api")
public class ProspectOrganizationResource {

    private final Logger log = LoggerFactory.getLogger(ProspectOrganizationResource.class);

    private static final String ENTITY_NAME = "prospectOrganization";

    private final ProspectOrganizationService prospectOrganizationService;

    public ProspectOrganizationResource(ProspectOrganizationService prospectOrganizationService) {
        this.prospectOrganizationService = prospectOrganizationService;
    }

    /**
     * POST  /prospect-organizations : Create a new prospectOrganization.
     *
     * @param prospectOrganizationDTO the prospectOrganizationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new prospectOrganizationDTO, or with status 400 (Bad Request) if the prospectOrganization has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prospect-organizations")
    @Timed
    public ResponseEntity<ProspectOrganizationDTO> createProspectOrganization(@RequestBody ProspectOrganizationDTO prospectOrganizationDTO) throws URISyntaxException {
        log.debug("REST request to save ProspectOrganization : {}", prospectOrganizationDTO);
        if (prospectOrganizationDTO.getIdProspect() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new prospectOrganization cannot already have an ID")).body(null);
        }
        ProspectOrganizationDTO result = prospectOrganizationService.save(prospectOrganizationDTO);
        return ResponseEntity.created(new URI("/api/prospect-organizations/" + result.getIdProspect()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdProspect().toString()))
            .body(result);
    }

    /**
     * POST  /prospect-organizations/execute{id}/{param} : Execute Bussiness Process prospectOrganization.
     *
     * @param prospectOrganizationDTO the prospectOrganizationDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  prospectOrganizationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prospect-organizations/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ProspectOrganizationDTO> executedProspectOrganization(@PathVariable Integer id, @PathVariable String param, @RequestBody ProspectOrganizationDTO prospectOrganizationDTO) throws URISyntaxException {
        log.debug("REST request to process ProspectOrganization : {}", prospectOrganizationDTO);
        ProspectOrganizationDTO result = prospectOrganizationService.processExecuteData(id, param, prospectOrganizationDTO);
        return new ResponseEntity<ProspectOrganizationDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /prospect-organizations/execute-list{id}/{param} : Execute Bussiness Process prospectOrganization.
     *
     * @param prospectOrganizationDTO the prospectOrganizationDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  prospectOrganizationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prospect-organizations/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ProspectOrganizationDTO>> executedListProspectOrganization(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ProspectOrganizationDTO> prospectOrganizationDTO) throws URISyntaxException {
        log.debug("REST request to process List ProspectOrganization");
        Set<ProspectOrganizationDTO> result = prospectOrganizationService.processExecuteListData(id, param, prospectOrganizationDTO);
        return new ResponseEntity<Set<ProspectOrganizationDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /prospect-organizations : Updates an existing prospectOrganization.
     *
     * @param prospectOrganizationDTO the prospectOrganizationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated prospectOrganizationDTO,
     * or with status 400 (Bad Request) if the prospectOrganizationDTO is not valid,
     * or with status 500 (Internal Server Error) if the prospectOrganizationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/prospect-organizations")
    @Timed
    public ResponseEntity<ProspectOrganizationDTO> updateProspectOrganization(@RequestBody ProspectOrganizationDTO prospectOrganizationDTO) throws URISyntaxException {
        log.debug("REST request to update ProspectOrganization : {}", prospectOrganizationDTO);
        if (prospectOrganizationDTO.getIdProspect() == null) {
            return createProspectOrganization(prospectOrganizationDTO);
        }
        ProspectOrganizationDTO result = prospectOrganizationService.save(prospectOrganizationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, prospectOrganizationDTO.getIdProspect().toString()))
            .body(result);
    }

    /**
     * GET  /prospect-organizations : get all the prospectOrganizations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of prospectOrganizations in body
     */
    @GetMapping("/prospect-organizations")
    @Timed
    public ResponseEntity<List<ProspectOrganizationDTO>> getAllProspectOrganizations(@RequestParam String idstatustype,@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ProspectOrganizations");
        Integer statusTypeId = Integer.parseInt(idstatustype);
        Page<ProspectOrganizationDTO> page = prospectOrganizationService.findAll(pageable,statusTypeId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/prospect-organizations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /prospect-organizations/:id : get the "id" prospectOrganization.
     *
     * @param id the id of the prospectOrganizationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the prospectOrganizationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/prospect-organizations/{id}")
    @Timed
    public ResponseEntity<ProspectOrganizationDTO> getProspectOrganization(@PathVariable UUID id) {
        log.debug("REST request to get ProspectOrganization : {}", id);
        ProspectOrganizationDTO prospectOrganizationDTO = prospectOrganizationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(prospectOrganizationDTO));
    }

    /**
     * DELETE  /prospect-organizations/:id : delete the "id" prospectOrganization.
     *
     * @param id the id of the prospectOrganizationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/prospect-organizations/{id}")
    @Timed
    public ResponseEntity<Void> deleteProspectOrganization(@PathVariable UUID id) {
        log.debug("REST request to delete ProspectOrganization : {}", id);
        prospectOrganizationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/prospect-organizations?query=:query : search for the prospectOrganization corresponding
     * to the query.
     *
     * @param query the query of the prospectOrganization search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/prospect-organizations")
    @Timed
    public ResponseEntity<List<ProspectOrganizationDTO>> searchProspectOrganizations(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ProspectOrganizations for query {}", query);
        Page<ProspectOrganizationDTO> page = prospectOrganizationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/prospect-organizations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/prospect-organizations/set-status/{id}")
    @Timed
    public ResponseEntity< ProspectOrganizationDTO> setStatusProspectOrganization(@PathVariable Integer id, @RequestBody ProspectOrganizationDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status ProspectOrganization : {}", dto);
        ProspectOrganizationDTO r = prospectOrganizationService.changeProspectOrganizationStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
