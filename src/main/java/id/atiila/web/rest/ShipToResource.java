package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ShipToService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ShipToDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ShipTo.
 */
@RestController
@RequestMapping("/api")
public class ShipToResource {

    private final Logger log = LoggerFactory.getLogger(ShipToResource.class);

    private static final String ENTITY_NAME = "shipTo";

    private final ShipToService shipToService;

    public ShipToResource(ShipToService shipToService) {
        this.shipToService = shipToService;
    }

    /**
     * POST  /ship-tos : Create a new shipTo.
     *
     * @param shipToDTO the shipToDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shipToDTO, or with status 400 (Bad Request) if the shipTo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ship-tos")
    @Timed
    public ResponseEntity<ShipToDTO> createShipTo(@RequestBody ShipToDTO shipToDTO) throws URISyntaxException {
        log.debug("REST request to save ShipTo : {}", shipToDTO);
        if (shipToDTO.getIdShipTo() != null) {
            throw new BadRequestAlertException("A new shipTo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShipToDTO result = shipToService.save(shipToDTO);
        return ResponseEntity.created(new URI("/api/ship-tos/" + result.getIdShipTo()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdShipTo().toString()))
            .body(result);
    }

    /**
     * POST  /ship-tos/process : Execute Bussiness Process shipTo.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ship-tos/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processShipTo(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ShipTo ");
        Map<String, Object> result = shipToService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /ship-tos/execute : Execute Bussiness Process shipTo.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  shipToDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ship-tos/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedShipTo(HttpServletRequest request, @RequestBody ShipToDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ShipTo");
        Map<String, Object> result = shipToService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /ship-tos/execute-list : Execute Bussiness Process shipTo.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  shipToDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ship-tos/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListShipTo(HttpServletRequest request, @RequestBody Set<ShipToDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List ShipTo");
        Map<String, Object> result = shipToService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /ship-tos : Updates an existing shipTo.
     *
     * @param shipToDTO the shipToDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shipToDTO,
     * or with status 400 (Bad Request) if the shipToDTO is not valid,
     * or with status 500 (Internal Server Error) if the shipToDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ship-tos")
    @Timed
    public ResponseEntity<ShipToDTO> updateShipTo(@RequestBody ShipToDTO shipToDTO) throws URISyntaxException {
        log.debug("REST request to update ShipTo : {}", shipToDTO);
        if (shipToDTO.getIdShipTo() == null) {
            return createShipTo(shipToDTO);
        }
        ShipToDTO result = shipToService.save(shipToDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shipToDTO.getIdShipTo().toString()))
            .body(result);
    }

    /**
     * GET  /ship-tos : get all the shipTos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shipTos in body
     */
    @GetMapping("/ship-tos")
    @Timed
    public ResponseEntity<List<ShipToDTO>> getAllShipTos(Pageable pageable) {
        log.debug("REST request to get a page of ShipTos");
        Page<ShipToDTO> page = shipToService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ship-tos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/ship-tos/filterBy")
    @Timed
    public ResponseEntity<List<ShipToDTO>> getAllFilteredShipTos(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ShipTos");
        Page<ShipToDTO> page = shipToService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ship-tos/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /ship-tos/:id : get the "id" shipTo.
     *
     * @param id the id of the shipToDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shipToDTO, or with status 404 (Not Found)
     */
    @GetMapping("/ship-tos/{id}")
    @Timed
    public ResponseEntity<ShipToDTO> getShipTo(@PathVariable String id) {
        log.debug("REST request to get ShipTo : {}", id);
        ShipToDTO shipToDTO = shipToService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(shipToDTO));
    }

    /**
     * DELETE  /ship-tos/:id : delete the "id" shipTo.
     *
     * @param id the id of the shipToDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ship-tos/{id}")
    @Timed
    public ResponseEntity<Void> deleteShipTo(@PathVariable String id) {
        log.debug("REST request to delete ShipTo : {}", id);
        shipToService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/ship-tos?query=:query : search for the shipTo corresponding
     * to the query.
     *
     * @param query the query of the shipTo search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/ship-tos")
    @Timed
    public ResponseEntity<List<ShipToDTO>> searchShipTos(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ShipTos for query {}", query);
        Page<ShipToDTO> page = shipToService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/ship-tos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
