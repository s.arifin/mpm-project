package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.CustomerRequestItemService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.CustomerRequestItemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CustomerRequestItem.
 */
@RestController
@RequestMapping("/api")
public class CustomerRequestItemResource {

    private final Logger log = LoggerFactory.getLogger(CustomerRequestItemResource.class);

    private static final String ENTITY_NAME = "customerRequestItem";

    private final CustomerRequestItemService customerRequestItemService;

    public CustomerRequestItemResource(CustomerRequestItemService customerRequestItemService) {
        this.customerRequestItemService = customerRequestItemService;
    }

    /**
     * POST  /customer-request-items : Create a new customerRequestItem.
     *
     * @param customerRequestItemDTO the customerRequestItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new customerRequestItemDTO, or with status 400 (Bad Request) if the customerRequestItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customer-request-items")
    @Timed
    public ResponseEntity<CustomerRequestItemDTO> createCustomerRequestItem(@RequestBody CustomerRequestItemDTO customerRequestItemDTO) throws URISyntaxException {
        log.debug("REST request to save CustomerRequestItem : {}", customerRequestItemDTO);
        if (customerRequestItemDTO.getIdRequestItem() != null) {
            throw new BadRequestAlertException("A new customerRequestItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CustomerRequestItemDTO result = customerRequestItemService.save(customerRequestItemDTO);
        return ResponseEntity.created(new URI("/api/customer-request-items/" + result.getIdRequestItem()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequestItem().toString()))
            .body(result);
    }

    /**
     * POST  /customer-request-items/process : Execute Bussiness Process customerRequestItem.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customer-request-items/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processCustomerRequestItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process CustomerRequestItem ");
        Map<String, Object> result = customerRequestItemService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /customer-request-items/execute : Execute Bussiness Process customerRequestItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  customerRequestItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customer-request-items/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedCustomerRequestItem(HttpServletRequest request, @RequestBody CustomerRequestItemDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process CustomerRequestItem");
        Map<String, Object> result = customerRequestItemService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /customer-request-items/execute-list : Execute Bussiness Process customerRequestItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  customerRequestItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customer-request-items/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListCustomerRequestItem(HttpServletRequest request, @RequestBody Set<CustomerRequestItemDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List CustomerRequestItem");
        Map<String, Object> result = customerRequestItemService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /customer-request-items : Updates an existing customerRequestItem.
     *
     * @param customerRequestItemDTO the customerRequestItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated customerRequestItemDTO,
     * or with status 400 (Bad Request) if the customerRequestItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the customerRequestItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/customer-request-items")
    @Timed
    public ResponseEntity<CustomerRequestItemDTO> updateCustomerRequestItem(@RequestBody CustomerRequestItemDTO customerRequestItemDTO) throws URISyntaxException {
        log.debug("REST request to update CustomerRequestItem : {}", customerRequestItemDTO);
        if (customerRequestItemDTO.getIdRequestItem() == null) {
            return createCustomerRequestItem(customerRequestItemDTO);
        }
        CustomerRequestItemDTO result = customerRequestItemService.save(customerRequestItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, customerRequestItemDTO.getIdRequestItem().toString()))
            .body(result);
    }

    /**
     * GET  /customer-request-items : get all the customerRequestItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of customerRequestItems in body
     */
    @GetMapping("/customer-request-items")
    @Timed
    public ResponseEntity<List<CustomerRequestItemDTO>> getAllCustomerRequestItems(Pageable pageable) {
        log.debug("REST request to get a page of CustomerRequestItems");
        Page<CustomerRequestItemDTO> page = customerRequestItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/customer-request-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/customer-request-items/filterBy")
    @Timed
    public ResponseEntity<List<CustomerRequestItemDTO>> getAllFilteredCustomerRequestItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of CustomerRequestItems");
        Page<CustomerRequestItemDTO> page = customerRequestItemService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/customer-request-items/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /customer-request-items/:id : get the "id" customerRequestItem.
     *
     * @param id the id of the customerRequestItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the customerRequestItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/customer-request-items/{id}")
    @Timed
    public ResponseEntity<CustomerRequestItemDTO> getCustomerRequestItem(@PathVariable UUID id) {
        log.debug("REST request to get CustomerRequestItem : {}", id);
        CustomerRequestItemDTO customerRequestItemDTO = customerRequestItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(customerRequestItemDTO));
    }

    /**
     * DELETE  /customer-request-items/:id : delete the "id" customerRequestItem.
     *
     * @param id the id of the customerRequestItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/customer-request-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteCustomerRequestItem(@PathVariable UUID id) {
        log.debug("REST request to delete CustomerRequestItem : {}", id);
        customerRequestItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/customer-request-items?query=:query : search for the customerRequestItem corresponding
     * to the query.
     *
     * @param query the query of the customerRequestItem search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/customer-request-items")
    @Timed
    public ResponseEntity<List<CustomerRequestItemDTO>> searchCustomerRequestItems(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of CustomerRequestItems for query {}", query);
        Page<CustomerRequestItemDTO> page = customerRequestItemService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/customer-request-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
