package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.BranchService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.BranchDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.dom4j.Branch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Branch.
 */
@RestController
@RequestMapping("/api")
public class BranchResource {

    private final Logger log = LoggerFactory.getLogger(BranchResource.class);

    private static final String ENTITY_NAME = "branch";

    private final BranchService branchService;

    public BranchResource(BranchService branchService) {
        this.branchService = branchService;
    }

    /**
     * POST  /branches : Create a new branch.
     *
     * @param branchDTO the branchDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new branchDTO, or with status 400 (Bad Request) if the branch has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/branches")
    @Timed
    public ResponseEntity<BranchDTO> createBranch(@RequestBody BranchDTO branchDTO) throws URISyntaxException {
        log.debug("REST request to save Branch : {}", branchDTO);
        if (branchDTO.getIdInternal() != null) {
            throw new BadRequestAlertException("A new branch cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BranchDTO result = branchService.save(branchDTO);
        return ResponseEntity.created(new URI("/api/branches/" + result.getIdInternal()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdInternal().toString()))
            .body(result);
    }

    /**
     * POST  /branches/execute{id}/{param} : Execute Bussiness Process branch.
     *
     * @param branchDTO the branchDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  branchDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/branches/execute/{id}/{param}")
    @Timed
    public ResponseEntity<BranchDTO> executedBranch(@PathVariable Integer id, @PathVariable String param, @RequestBody BranchDTO branchDTO) throws URISyntaxException {
        log.debug("REST request to process Branch : {}", branchDTO);
        BranchDTO result = branchService.processExecuteData(id, param, branchDTO);
        return new ResponseEntity<BranchDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /branches/execute-list{id}/{param} : Execute Bussiness Process branch.
     *
     * @param branchDTO the branchDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  branchDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/branches/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<BranchDTO>> executedListBranch(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<BranchDTO> branchDTO) throws URISyntaxException {
        log.debug("REST request to process List Branch");
        Set<BranchDTO> result = branchService.processExecuteListData(id, param, branchDTO);
        return new ResponseEntity<Set<BranchDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /branches : Updates an existing branch.
     *
     * @param branchDTO the branchDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated branchDTO,
     * or with status 400 (Bad Request) if the branchDTO is not valid,
     * or with status 500 (Internal Server Error) if the branchDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/branches")
    @Timed
    public ResponseEntity<BranchDTO> updateBranch(@RequestBody BranchDTO branchDTO) throws URISyntaxException {
        log.debug("REST request to update Branch : {}", branchDTO);
        if (branchDTO.getIdInternal() == null) {
            return createBranch(branchDTO);
        }
        BranchDTO result = branchService.save(branchDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, branchDTO.getIdInternal().toString()))
            .body(result);
    }

    /**
     * GET  /branches : get all the branches.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of branches in body
     */
    @GetMapping("/branches")
    @Timed
    public ResponseEntity<List<BranchDTO>> getAllBranches(Pageable pageable) {
        log.debug("REST request to get a page of Branches");
        Page<BranchDTO> page = branchService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/branches");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /branches/:id : get the "id" branch.
     *
     * @param id the id of the branchDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the branchDTO, or with status 404 (Not Found)
     */
    @GetMapping("/branches/{id}")
    @Timed
    public ResponseEntity<BranchDTO> getBranch(@PathVariable String id) {
        log.debug("REST request to get Branch : {}", id);
        BranchDTO branchDTO = branchService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(branchDTO));
    }

    /**
     * DELETE  /branches/:id : delete the "id" branch.
     *
     * @param id the id of the branchDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/branches/{id}")
    @Timed
    public ResponseEntity<Void> deleteBranch(@PathVariable String id) {
        log.debug("REST request to delete Branch : {}", id);
        branchService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/branches?query=:query : search for the branch corresponding
     * to the query.
     *
     * @param query the query of the branch search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/branches")
    @Timed
    public ResponseEntity<List<BranchDTO>> searchBranches(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Branches for query {}", query);
        Page<BranchDTO> page = branchService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/branches");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/branches/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrderItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process OrderItem ");
        Map<String, Object> result = branchService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    @GetMapping("/branches/filterBy")
    @Timed
    public ResponseEntity<List<BranchDTO>> getAllFilteredVendors(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Vendors");
        Page<BranchDTO> page = branchService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vendors/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
