package id.atiila.web.rest;

import java.util.Map;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.VendorProductService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VendorProductDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VendorProduct.
 */
@RestController
@RequestMapping("/api")
public class VendorProductResource {

    private final Logger log = LoggerFactory.getLogger(VendorProductResource.class);

    private static final String ENTITY_NAME = "vendorProduct";

    private final VendorProductService vendorProductService;

    public VendorProductResource(VendorProductService vendorProductService) {
        this.vendorProductService = vendorProductService;
    }

    /**
     * POST  /vendor-products : Create a new vendorProduct.
     *
     * @param vendorProductDTO the vendorProductDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vendorProductDTO, or with status 400 (Bad Request) if the vendorProduct has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendor-products")
    @Timed
    public ResponseEntity<VendorProductDTO> createVendorProduct(@RequestBody VendorProductDTO vendorProductDTO) throws URISyntaxException {
        log.debug("REST request to save VendorProduct : {}", vendorProductDTO);
        if (vendorProductDTO.getIdVendorProduct() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new vendorProduct cannot already have an ID")).body(null);
        }
        VendorProductDTO result = vendorProductService.save(vendorProductDTO);

        return ResponseEntity.created(new URI("/api/vendor-products/" + result.getIdVendorProduct()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdVendorProduct().toString()))
            .body(result);
    }

    /**
     * POST  /vendor-products/execute : Execute Bussiness Process vendorProduct.
     *
     * @param vendorProductDTO the vendorProductDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  vendorProductDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendor-products/execute")
    @Timed
    public ResponseEntity<VendorProductDTO> executedVendorProduct(@RequestBody VendorProductDTO vendorProductDTO) throws URISyntaxException {
        log.debug("REST request to process VendorProduct : {}", vendorProductDTO);
        return new ResponseEntity<VendorProductDTO>(vendorProductDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /vendor-products : Updates an existing vendorProduct.
     *
     * @param vendorProductDTO the vendorProductDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vendorProductDTO,
     * or with status 400 (Bad Request) if the vendorProductDTO is not valid,
     * or with status 500 (Internal Server Error) if the vendorProductDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vendor-products")
    @Timed
    public ResponseEntity<VendorProductDTO> updateVendorProduct(@RequestBody VendorProductDTO vendorProductDTO) throws URISyntaxException {
        log.debug("REST request to update VendorProduct : {}", vendorProductDTO);
        if (vendorProductDTO.getIdVendorProduct() == null) {
            return createVendorProduct(vendorProductDTO);
        }
        VendorProductDTO result = vendorProductService.save(vendorProductDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vendorProductDTO.getIdVendorProduct().toString()))
            .body(result);
    }

    /**
     * GET  /vendor-products : get all the vendorProducts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vendorProducts in body
     */
    @GetMapping("/vendor-products")
    @Timed
    public ResponseEntity<List<VendorProductDTO>> getAllVendorProducts(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of VendorProducts");
        Page<VendorProductDTO> page = vendorProductService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vendor-products");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /vendor-products/:id : get the "id" vendorProduct.
     *
     * @param id the id of the vendorProductDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vendorProductDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vendor-products/{id}")
    @Timed
    public ResponseEntity<VendorProductDTO> getVendorProduct(@PathVariable UUID id) {
        log.debug("REST request to get VendorProduct : {}", id);
        VendorProductDTO vendorProductDTO = vendorProductService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vendorProductDTO));
    }

    /**
     * DELETE  /vendor-products/:id : delete the "id" vendorProduct.
     *
     * @param id the id of the vendorProductDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vendor-products/{id}")
    @Timed
    public ResponseEntity<Void> deleteVendorProduct(@PathVariable UUID id) {
        log.debug("REST request to delete VendorProduct : {}", id);
        vendorProductService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/vendor-products?query=:query : search for the vendorProduct corresponding
     * to the query.
     *
     * @param query the query of the vendorProduct search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vendor-products")
    @Timed
    public ResponseEntity<List<VendorProductDTO>> searchVendorProducts(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of VendorProducts for query {}", query);
        Page<VendorProductDTO> page = vendorProductService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vendor-products");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/vendor-products/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processVendorProduct(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process VendorProduct ");
        Map<String, Object> result = vendorProductService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
