package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ServiceService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ServiceDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Service.
 */
@RestController
@RequestMapping("/api")
public class ServiceResource {

    private final Logger log = LoggerFactory.getLogger(ServiceResource.class);

    private static final String ENTITY_NAME = "service";

    private final ServiceService serviceService;

    public ServiceResource(ServiceService serviceService) {
        this.serviceService = serviceService;
    }

    /**
     * POST  /services : Create a new service.
     *
     * @param serviceDTO the serviceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new serviceDTO, or with status 400 (Bad Request) if the service has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/services")
    @Timed
    public ResponseEntity<ServiceDTO> createService(@RequestBody ServiceDTO serviceDTO) throws URISyntaxException {
        log.debug("REST request to save Service : {}", serviceDTO);
        if (serviceDTO.getIdProduct() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new service cannot already have an ID")).body(null);
        }
        ServiceDTO result = serviceService.save(serviceDTO);
        return ResponseEntity.created(new URI("/api/services/" + result.getIdProduct()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdProduct().toString()))
            .body(result);
    }

    /**
     * POST  /services/execute{id}/{param} : Execute Bussiness Process service.
     *
     * @param serviceDTO the serviceDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  serviceDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/services/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ServiceDTO> executedService(@PathVariable Integer id, @PathVariable String param, @RequestBody ServiceDTO serviceDTO) throws URISyntaxException {
        log.debug("REST request to process Service : {}", serviceDTO);
        ServiceDTO result = serviceService.processExecuteData(id, param, serviceDTO);
        return new ResponseEntity<ServiceDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /services/execute-list{id}/{param} : Execute Bussiness Process service.
     *
     * @param serviceDTO the serviceDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  serviceDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/services/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ServiceDTO>> executedListService(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ServiceDTO> serviceDTO) throws URISyntaxException {
        log.debug("REST request to process List Service");
        Set<ServiceDTO> result = serviceService.processExecuteListData(id, param, serviceDTO);
        return new ResponseEntity<Set<ServiceDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /services : Updates an existing service.
     *
     * @param serviceDTO the serviceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated serviceDTO,
     * or with status 400 (Bad Request) if the serviceDTO is not valid,
     * or with status 500 (Internal Server Error) if the serviceDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/services")
    @Timed
    public ResponseEntity<ServiceDTO> updateService(@RequestBody ServiceDTO serviceDTO) throws URISyntaxException {
        log.debug("REST request to update Service : {}", serviceDTO);
        if (serviceDTO.getIdProduct() == null) {
            return createService(serviceDTO);
        }
        ServiceDTO result = serviceService.save(serviceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, serviceDTO.getIdProduct().toString()))
            .body(result);
    }

    /**
     * GET  /services : get all the services.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of services in body
     */
    @GetMapping("/services")
    @Timed
    public ResponseEntity<List<ServiceDTO>> getAllServices(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Services");
        Page<ServiceDTO> page = serviceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/services");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /services/:id : get the "id" service.
     *
     * @param id the id of the serviceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the serviceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/services/{id}")
    @Timed
    public ResponseEntity<ServiceDTO> getService(@PathVariable String id) {
        log.debug("REST request to get Service : {}", id);
        ServiceDTO serviceDTO = serviceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(serviceDTO));
    }

    /**
     * DELETE  /services/:id : delete the "id" service.
     *
     * @param id the id of the serviceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/services/{id}")
    @Timed
    public ResponseEntity<Void> deleteService(@PathVariable String id) {
        log.debug("REST request to delete Service : {}", id);
        serviceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/services?query=:query : search for the service corresponding
     * to the query.
     *
     * @param query the query of the service search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/services")
    @Timed
    public ResponseEntity<List<ServiceDTO>> searchServices(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Services for query {}", query);
        Page<ServiceDTO> page = serviceService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/services");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/services/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processService(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process Service ");
        Map<String, Object> result = serviceService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
