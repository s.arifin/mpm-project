package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.StandardCalendarService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.StandardCalendarDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing StandardCalendar.
 */
@RestController
@RequestMapping("/api")
public class StandardCalendarResource {

    private final Logger log = LoggerFactory.getLogger(StandardCalendarResource.class);

    private static final String ENTITY_NAME = "standardCalendar";

    private final StandardCalendarService standardCalendarService;

    public StandardCalendarResource(StandardCalendarService standardCalendarService) {
        this.standardCalendarService = standardCalendarService;
    }

    /**
     * POST  /standard-calendars : Create a new standardCalendar.
     *
     * @param standardCalendarDTO the standardCalendarDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new standardCalendarDTO, or with status 400 (Bad Request) if the standardCalendar has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/standard-calendars")
    @Timed
    public ResponseEntity<StandardCalendarDTO> createStandardCalendar(@RequestBody StandardCalendarDTO standardCalendarDTO) throws URISyntaxException {
        log.debug("REST request to save StandardCalendar : {}", standardCalendarDTO);
        if (standardCalendarDTO.getIdCalendar() != null) {
            throw new BadRequestAlertException("A new standardCalendar cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StandardCalendarDTO result = standardCalendarService.save(standardCalendarDTO);
        return ResponseEntity.created(new URI("/api/standard-calendars/" + result.getIdCalendar()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdCalendar().toString()))
            .body(result);
    }

    /**
     * POST  /standard-calendars/process : Execute Bussiness Process standardCalendar.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/standard-calendars/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processStandardCalendar(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process StandardCalendar ");
        Map<String, Object> result = standardCalendarService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /standard-calendars/execute : Execute Bussiness Process standardCalendar.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  standardCalendarDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/standard-calendars/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedStandardCalendar(HttpServletRequest request, @RequestBody StandardCalendarDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process StandardCalendar");
        Map<String, Object> result = standardCalendarService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /standard-calendars/execute-list : Execute Bussiness Process standardCalendar.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  standardCalendarDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/standard-calendars/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListStandardCalendar(HttpServletRequest request, @RequestBody Set<StandardCalendarDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List StandardCalendar");
        Map<String, Object> result = standardCalendarService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /standard-calendars : Updates an existing standardCalendar.
     *
     * @param standardCalendarDTO the standardCalendarDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated standardCalendarDTO,
     * or with status 400 (Bad Request) if the standardCalendarDTO is not valid,
     * or with status 500 (Internal Server Error) if the standardCalendarDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/standard-calendars")
    @Timed
    public ResponseEntity<StandardCalendarDTO> updateStandardCalendar(@RequestBody StandardCalendarDTO standardCalendarDTO) throws URISyntaxException {
        log.debug("REST request to update StandardCalendar : {}", standardCalendarDTO);
        if (standardCalendarDTO.getIdCalendar() == null) {
            return createStandardCalendar(standardCalendarDTO);
        }
        StandardCalendarDTO result = standardCalendarService.save(standardCalendarDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, standardCalendarDTO.getIdCalendar().toString()))
            .body(result);
    }

    /**
     * GET  /standard-calendars : get all the standardCalendars.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of standardCalendars in body
     */
    @GetMapping("/standard-calendars")
    @Timed
    public ResponseEntity<List<StandardCalendarDTO>> getAllStandardCalendars(Pageable pageable) {
        log.debug("REST request to get a page of StandardCalendars");
        Page<StandardCalendarDTO> page = standardCalendarService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/standard-calendars");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/standard-calendars/filterBy")
    @Timed
    public ResponseEntity<List<StandardCalendarDTO>> getAllFilteredStandardCalendars(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of StandardCalendars");
        Page<StandardCalendarDTO> page = standardCalendarService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/standard-calendars/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /standard-calendars/:id : get the "id" standardCalendar.
     *
     * @param id the id of the standardCalendarDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the standardCalendarDTO, or with status 404 (Not Found)
     */
    @GetMapping("/standard-calendars/{id}")
    @Timed
    public ResponseEntity<StandardCalendarDTO> getStandardCalendar(@PathVariable Integer id) {
        log.debug("REST request to get StandardCalendar : {}", id);
        StandardCalendarDTO standardCalendarDTO = standardCalendarService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(standardCalendarDTO));
    }

    /**
     * DELETE  /standard-calendars/:id : delete the "id" standardCalendar.
     *
     * @param id the id of the standardCalendarDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/standard-calendars/{id}")
    @Timed
    public ResponseEntity<Void> deleteStandardCalendar(@PathVariable Integer id) {
        log.debug("REST request to delete StandardCalendar : {}", id);
        standardCalendarService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/standard-calendars?query=:query : search for the standardCalendar corresponding
     * to the query.
     *
     * @param query the query of the standardCalendar search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/standard-calendars")
    @Timed
    public ResponseEntity<List<StandardCalendarDTO>> searchStandardCalendars(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of StandardCalendars for query {}", query);
        Page<StandardCalendarDTO> page = standardCalendarService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/standard-calendars");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
