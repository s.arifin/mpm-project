package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.FacilityService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.FacilityDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Facility.
 */
@RestController
@RequestMapping("/api")
public class FacilityResource {

    private final Logger log = LoggerFactory.getLogger(FacilityResource.class);

    private static final String ENTITY_NAME = "facility";

    private final FacilityService facilityService;

    public FacilityResource(FacilityService facilityService) {
        this.facilityService = facilityService;
    }

    /**
     * POST  /facilities : Create a new facility.
     *
     * @param facilityDTO the facilityDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new facilityDTO, or with status 400 (Bad Request) if the facility has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/facilities")
    @Timed
    public ResponseEntity<FacilityDTO> createFacility(@RequestBody FacilityDTO facilityDTO) throws URISyntaxException {
        log.debug("REST request to save Facility : {}", facilityDTO);
        if (facilityDTO.getIdFacility() != null) {
            throw new BadRequestAlertException("A new facility cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FacilityDTO result = facilityService.save(facilityDTO);
        return ResponseEntity.created(new URI("/api/facilities/" + result.getIdFacility()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdFacility().toString()))
            .body(result);
    }

    /**
     * POST  /facilities/process : Execute Bussiness Process facility.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/facilities/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processFacility(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Facility ");
        Map<String, Object> result = facilityService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /facilities/execute : Execute Bussiness Process facility.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  facilityDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/facilities/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedFacility(HttpServletRequest request, @RequestBody FacilityDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Facility");
        Map<String, Object> result = facilityService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /facilities/execute-list : Execute Bussiness Process facility.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  facilityDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/facilities/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListFacility(HttpServletRequest request, @RequestBody Set<FacilityDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List Facility");
        Map<String, Object> result = facilityService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /facilities : Updates an existing facility.
     *
     * @param facilityDTO the facilityDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated facilityDTO,
     * or with status 400 (Bad Request) if the facilityDTO is not valid,
     * or with status 500 (Internal Server Error) if the facilityDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/facilities")
    @Timed
    public ResponseEntity<FacilityDTO> updateFacility(@RequestBody FacilityDTO facilityDTO) throws URISyntaxException {
        log.debug("REST request to update Facility : {}", facilityDTO);
        if (facilityDTO.getIdFacility() == null) {
            return createFacility(facilityDTO);
        }
        FacilityDTO result = facilityService.save(facilityDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, facilityDTO.getIdFacility().toString()))
            .body(result);
    }

    /**
     * GET  /facilities : get all the facilities.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of facilities in body
     */
    @GetMapping("/facilities")
    @Timed
    public ResponseEntity<List<FacilityDTO>> getAllFacilities(Pageable pageable) {
        log.debug("REST request to get a page of Facilities");
        Page<FacilityDTO> page = facilityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/facilities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/facilities/filterBy")
    @Timed
    public ResponseEntity<List<FacilityDTO>> getAllFilteredFacilities(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Facilities");
        Page<FacilityDTO> page = facilityService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/facilities/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /facilities/:id : get the "id" facility.
     *
     * @param id the id of the facilityDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the facilityDTO, or with status 404 (Not Found)
     */
    @GetMapping("/facilities/{id}")
    @Timed
    public ResponseEntity<FacilityDTO> getFacility(@PathVariable UUID id) {
        log.debug("REST request to get Facility : {}", id);
        FacilityDTO facilityDTO = facilityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(facilityDTO));
    }

    /**
     * DELETE  /facilities/:id : delete the "id" facility.
     *
     * @param id the id of the facilityDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/facilities/{id}")
    @Timed
    public ResponseEntity<Void> deleteFacility(@PathVariable UUID id) {
        log.debug("REST request to delete Facility : {}", id);
        facilityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/facilities?query=:query : search for the facility corresponding
     * to the query.
     *
     * @param query the query of the facility search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/facilities")
    @Timed
    public ResponseEntity<List<FacilityDTO>> searchFacilities(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Facilities for query {}", query);
        Page<FacilityDTO> page = facilityService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/facilities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/facilities/set-status/{id}")
    @Timed
    public ResponseEntity< FacilityDTO> setStatusFacility(@PathVariable Integer id, @RequestBody FacilityDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status Facility : {}", dto);
        FacilityDTO r = facilityService.changeFacilityStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    @GetMapping("/facilities/find-by-facility-type/{id}")
    @Timed
    public ResponseEntity<List<FacilityDTO>> getFacilityByFacilityId(@PathVariable Integer id) {
        log.debug("REST request to find facility by facility type " + id);
        List<FacilityDTO> list = facilityService.getFacilityByFacilityType(id);
        return new ResponseEntity<List<FacilityDTO>>(list, null, HttpStatus.OK);
    }


}
