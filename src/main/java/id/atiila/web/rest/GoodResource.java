package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.GoodService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.GoodDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Good.
 */
@RestController
@RequestMapping("/api")
public class GoodResource {

    private final Logger log = LoggerFactory.getLogger(GoodResource.class);

    private static final String ENTITY_NAME = "good";

    private final GoodService goodService;

    public GoodResource(GoodService goodService) {
        this.goodService = goodService;
    }

    /**
     * POST  /goods : Create a new good.
     *
     * @param goodDTO the goodDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new goodDTO, or with status 400 (Bad Request) if the good has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/goods")
    @Timed
    public ResponseEntity<GoodDTO> createGood(@RequestBody GoodDTO goodDTO) throws URISyntaxException {
        log.debug("REST request to save Good : {}", goodDTO);
        if (goodDTO.getIdProduct() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new good cannot already have an ID")).body(null);
        }
        GoodDTO result = goodService.save(goodDTO);

        return ResponseEntity.created(new URI("/api/goods/" + result.getIdProduct()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdProduct().toString()))
            .body(result);
    }

    /**
     * POST  /goods/execute : Execute Bussiness Process good.
     *
     * @param goodDTO the goodDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  goodDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/goods/execute")
    @Timed
    public ResponseEntity<GoodDTO> executedGood(@RequestBody GoodDTO goodDTO) throws URISyntaxException {
        log.debug("REST request to process Good : {}", goodDTO);
        return new ResponseEntity<GoodDTO>(goodDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /goods : Updates an existing good.
     *
     * @param goodDTO the goodDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated goodDTO,
     * or with status 400 (Bad Request) if the goodDTO is not valid,
     * or with status 500 (Internal Server Error) if the goodDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/goods")
    @Timed
    public ResponseEntity<GoodDTO> updateGood(@RequestBody GoodDTO goodDTO) throws URISyntaxException {
        log.debug("REST request to update Good : {}", goodDTO);
        if (goodDTO.getIdProduct() == null) {
            return createGood(goodDTO);
        }
        GoodDTO result = goodService.save(goodDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, goodDTO.getIdProduct().toString()))
            .body(result);
    }

    /**
     * GET  /goods : get all the goods.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of goods in body
     */
    @GetMapping("/goods")
    @Timed
    public ResponseEntity<List<GoodDTO>> getAllGoods(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Goods");
        Page<GoodDTO> page = goodService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/goods");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /goods/:id : get the "id" good.
     *
     * @param id the id of the goodDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the goodDTO, or with status 404 (Not Found)
     */
    @GetMapping("/goods/{id}")
    @Timed
    public ResponseEntity<GoodDTO> getGood(@PathVariable String id) {
        log.debug("REST request to get Good : {}", id);
        GoodDTO goodDTO = goodService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(goodDTO));
    }

    /**
     * DELETE  /goods/:id : delete the "id" good.
     *
     * @param id the id of the goodDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/goods/{id}")
    @Timed
    public ResponseEntity<Void> deleteGood(@PathVariable String id) {
        log.debug("REST request to delete Good : {}", id);
        goodService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/goods?query=:query : search for the good corresponding
     * to the query.
     *
     * @param query the query of the good search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/goods")
    @Timed
    public ResponseEntity<List<GoodDTO>> searchGoods(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Goods for query {}", query);
        Page<GoodDTO> page = goodService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/goods");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/goods/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrderItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process OrderItem ");
        Map<String, Object> result = goodService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
