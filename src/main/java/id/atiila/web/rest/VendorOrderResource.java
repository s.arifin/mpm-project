package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.VendorOrderService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VendorOrderDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VendorOrder.
 */
@RestController
@RequestMapping("/api")
public class VendorOrderResource {

    private final Logger log = LoggerFactory.getLogger(VendorOrderResource.class);

    private static final String ENTITY_NAME = "vendorOrder";

    private final VendorOrderService vendorOrderService;

    public VendorOrderResource(VendorOrderService vendorOrderService) {
        this.vendorOrderService = vendorOrderService;
    }

    /**
     * POST  /vendor-orders : Create a new vendorOrder.
     *
     * @param vendorOrderDTO the vendorOrderDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vendorOrderDTO, or with status 400 (Bad Request) if the vendorOrder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendor-orders")
    @Timed
    public ResponseEntity<VendorOrderDTO> createVendorOrder(@RequestBody VendorOrderDTO vendorOrderDTO) throws URISyntaxException {
        log.debug("REST request to save VendorOrder : {}", vendorOrderDTO);
        if (vendorOrderDTO.getIdOrder() != null) {
            throw new BadRequestAlertException("A new vendorOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VendorOrderDTO result = vendorOrderService.save(vendorOrderDTO);
        return ResponseEntity.created(new URI("/api/vendor-orders/" + result.getIdOrder()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdOrder().toString()))
            .body(result);
    }

    /**
     * POST  /vendor-orders/process : Execute Bussiness Process vendorOrder.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendor-orders/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processVendorOrder(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process VendorOrder ");
        Map<String, Object> result = vendorOrderService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vendor-orders/execute : Execute Bussiness Process vendorOrder.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  vendorOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendor-orders/execute")
    @Timed
    public ResponseEntity<VendorOrderDTO> executedVendorOrder(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process VendorOrder");
        VendorOrderDTO result = vendorOrderService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<VendorOrderDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vendor-orders/execute-list : Execute Bussiness Process vendorOrder.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  vendorOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendor-orders/execute-list")
    @Timed
    public ResponseEntity<Set<VendorOrderDTO>> executedListVendorOrder(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List VendorOrder");
        Set<VendorOrderDTO> result = vendorOrderService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<VendorOrderDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /vendor-orders : Updates an existing vendorOrder.
     *
     * @param vendorOrderDTO the vendorOrderDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vendorOrderDTO,
     * or with status 400 (Bad Request) if the vendorOrderDTO is not valid,
     * or with status 500 (Internal Server Error) if the vendorOrderDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vendor-orders")
    @Timed
    public ResponseEntity<VendorOrderDTO> updateVendorOrder(@RequestBody VendorOrderDTO vendorOrderDTO) throws URISyntaxException {
        log.debug("REST request to update VendorOrder : {}", vendorOrderDTO);
        if (vendorOrderDTO.getIdOrder() == null) {
            return createVendorOrder(vendorOrderDTO);
        }
        VendorOrderDTO result = vendorOrderService.save(vendorOrderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vendorOrderDTO.getIdOrder().toString()))
            .body(result);
    }

    /**
     * GET  /vendor-orders : get all the vendorOrders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vendorOrders in body
     */
    @GetMapping("/vendor-orders")
    @Timed
    public ResponseEntity<List<VendorOrderDTO>> getAllVendorOrders(Pageable pageable) {
        log.debug("REST request to get a page of VendorOrders");
        Page<VendorOrderDTO> page = vendorOrderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vendor-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vendor-orders/filterBy")
    @Timed
    public ResponseEntity<List<VendorOrderDTO>> getAllFilteredVendorOrders(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of VendorOrders");
        Page<VendorOrderDTO> page = vendorOrderService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vendor-orders/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /vendor-orders/:id : get the "id" vendorOrder.
     *
     * @param id the id of the vendorOrderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vendorOrderDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vendor-orders/{id}")
    @Timed
    public ResponseEntity<VendorOrderDTO> getVendorOrder(@PathVariable UUID id) {
        log.debug("REST request to get VendorOrder : {}", id);
        VendorOrderDTO vendorOrderDTO = vendorOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vendorOrderDTO));
    }

    /**
     * DELETE  /vendor-orders/:id : delete the "id" vendorOrder.
     *
     * @param id the id of the vendorOrderDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vendor-orders/{id}")
    @Timed
    public ResponseEntity<Void> deleteVendorOrder(@PathVariable UUID id) {
        log.debug("REST request to delete VendorOrder : {}", id);
        vendorOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/vendor-orders?query=:query : search for the vendorOrder corresponding
     * to the query.
     *
     * @param query the query of the vendorOrder search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vendor-orders")
    @Timed
    public ResponseEntity<List<VendorOrderDTO>> searchVendorOrders(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of VendorOrders for query {}", query);
        Page<VendorOrderDTO> page = vendorOrderService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vendor-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/vendor-orders/set-status/{id}")
    @Timed
    public ResponseEntity< VendorOrderDTO> setStatusVendorOrder(@PathVariable Integer id, @RequestBody VendorOrderDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status VendorOrder : {}", dto);
        VendorOrderDTO r = vendorOrderService.changeVendorOrderStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
