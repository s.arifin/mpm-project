package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RequestService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RequestDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Request.
 */
@RestController
@RequestMapping("/api")
public class RequestResource {

    private final Logger log = LoggerFactory.getLogger(RequestResource.class);

    private static final String ENTITY_NAME = "request";

    private final RequestService requestService;

    public RequestResource(RequestService requestService) {
        this.requestService = requestService;
    }

    /**
     * POST  /requests : Create a new request.
     *
     * @param requestDTO the requestDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new requestDTO, or with status 400 (Bad Request) if the request has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requests")
    @Timed
    public ResponseEntity<RequestDTO> createRequest(@RequestBody RequestDTO requestDTO) throws URISyntaxException {
        log.debug("REST request to save Request : {}", requestDTO);
        if (requestDTO.getIdRequest() != null) {
            throw new BadRequestAlertException("A new request cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RequestDTO result = requestService.save(requestDTO);
        return ResponseEntity.created(new URI("/api/requests/" + result.getIdRequest()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequest().toString()))
            .body(result);
    }

    /**
     * POST  /requests/process : Execute Bussiness Process request.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requests/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processRequest(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Request ");
        Map<String, Object> result = requestService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /requests/execute : Execute Bussiness Process request.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  requestDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requests/execute")
    @Timed
    public ResponseEntity<RequestDTO> executedRequest(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Request");
        RequestDTO result = requestService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<RequestDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /requests/execute-list : Execute Bussiness Process request.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  requestDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requests/execute-list")
    @Timed
    public ResponseEntity<Set<RequestDTO>> executedListRequest(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List Request");
        Set<RequestDTO> result = requestService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<RequestDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /requests : Updates an existing request.
     *
     * @param requestDTO the requestDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated requestDTO,
     * or with status 400 (Bad Request) if the requestDTO is not valid,
     * or with status 500 (Internal Server Error) if the requestDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/requests")
    @Timed
    public ResponseEntity<RequestDTO> updateRequest(@RequestBody RequestDTO requestDTO) throws URISyntaxException {
        log.debug("REST request to update Request : {}", requestDTO);
        if (requestDTO.getIdRequest() == null) {
            return createRequest(requestDTO);
        }
        RequestDTO result = requestService.save(requestDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, requestDTO.getIdRequest().toString()))
            .body(result);
    }

    /**
     * GET  /requests : get all the requests.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of requests in body
     */
    @GetMapping("/requests")
    @Timed
    public ResponseEntity<List<RequestDTO>> getAllRequests(Pageable pageable) {
        log.debug("REST request to get a page of Requests");
        Page<RequestDTO> page = requestService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/requests");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/requests/filterBy")
    @Timed
    public ResponseEntity<List<RequestDTO>> getAllFilteredRequests(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Requests");
        Page<RequestDTO> page = requestService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/requests/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /requests/:id : get the "id" request.
     *
     * @param id the id of the requestDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the requestDTO, or with status 404 (Not Found)
     */
    @GetMapping("/requests/{id}")
    @Timed
    public ResponseEntity<RequestDTO> getRequest(@PathVariable UUID id) {
        log.debug("REST request to get Request : {}", id);
        RequestDTO requestDTO = requestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(requestDTO));
    }

    /**
     * DELETE  /requests/:id : delete the "id" request.
     *
     * @param id the id of the requestDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/requests/{id}")
    @Timed
    public ResponseEntity<Void> deleteRequest(@PathVariable UUID id) {
        log.debug("REST request to delete Request : {}", id);
        requestService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/requests?query=:query : search for the request corresponding
     * to the query.
     *
     * @param query the query of the request search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/requests")
    @Timed
    public ResponseEntity<List<RequestDTO>> searchRequests(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Requests for query {}", query);
        Page<RequestDTO> page = requestService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/requests");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/requests/set-status/{id}")
    @Timed
    public ResponseEntity< RequestDTO> setStatusRequest(@PathVariable Integer id, @RequestBody RequestDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status Request : {}", dto);
        RequestDTO r = requestService.changeRequestStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
