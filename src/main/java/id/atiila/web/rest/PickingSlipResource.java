package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PickingSlipService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PickingSlipDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PickingSlip.
 */
@RestController
@RequestMapping("/api")
public class PickingSlipResource {

    private final Logger log = LoggerFactory.getLogger(PickingSlipResource.class);

    private static final String ENTITY_NAME = "pickingSlip";

    private final PickingSlipService pickingSlipService;

    public PickingSlipResource(PickingSlipService pickingSlipService) {
        this.pickingSlipService = pickingSlipService;
    }

    /**
     * POST  /picking-slips : Create a new pickingSlip.
     *
     * @param pickingSlipDTO the pickingSlipDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pickingSlipDTO, or with status 400 (Bad Request) if the pickingSlip has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/picking-slips")
    @Timed
    public ResponseEntity<PickingSlipDTO> createPickingSlip(@RequestBody PickingSlipDTO pickingSlipDTO) throws URISyntaxException {
        log.debug("REST request to save PickingSlip : {}", pickingSlipDTO);
        if (pickingSlipDTO.getIdSlip() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new pickingSlip cannot already have an ID")).body(null);
        }
        PickingSlipDTO result = pickingSlipService.save(pickingSlipDTO);
        return ResponseEntity.created(new URI("/api/picking-slips/" + result.getIdSlip()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdSlip().toString()))
            .body(result);
    }

    /**
     * POST  /picking-slips/execute{id}/{param} : Execute Bussiness Process pickingSlip.
     *
     * @param pickingSlipDTO the pickingSlipDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  pickingSlipDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/picking-slips/execute/{id}/{param}")
    @Timed
    public ResponseEntity<PickingSlipDTO> executedPickingSlip(@PathVariable Integer id, @PathVariable String param, @RequestBody PickingSlipDTO pickingSlipDTO) throws URISyntaxException {
        log.debug("REST request to process PickingSlip : {}", pickingSlipDTO);
        PickingSlipDTO result = pickingSlipService.processExecuteData(id, param, pickingSlipDTO);
        return new ResponseEntity<PickingSlipDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /picking-slips/execute-list{id}/{param} : Execute Bussiness Process pickingSlip.
     *
     * @param pickingSlipDTO the pickingSlipDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  pickingSlipDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/picking-slips/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<PickingSlipDTO>> executedListPickingSlip(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<PickingSlipDTO> pickingSlipDTO) throws URISyntaxException {
        log.debug("REST request to process List PickingSlip");
        Set<PickingSlipDTO> result = pickingSlipService.processExecuteListData(id, param, pickingSlipDTO);
        return new ResponseEntity<Set<PickingSlipDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /picking-slips : Updates an existing pickingSlip.
     *
     * @param pickingSlipDTO the pickingSlipDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pickingSlipDTO,
     * or with status 400 (Bad Request) if the pickingSlipDTO is not valid,
     * or with status 500 (Internal Server Error) if the pickingSlipDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/picking-slips")
    @Timed
    public ResponseEntity<PickingSlipDTO> updatePickingSlip(@RequestBody PickingSlipDTO pickingSlipDTO) throws URISyntaxException {
        log.debug("REST request to update PickingSlip : {}", pickingSlipDTO);
        if (pickingSlipDTO.getIdSlip() == null) {
            return createPickingSlip(pickingSlipDTO);
        }
        PickingSlipDTO result = pickingSlipService.save(pickingSlipDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pickingSlipDTO.getIdSlip().toString()))
            .body(result);
    }

    /**
     * GET  /picking-slips : get all the pickingSlips.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of pickingSlips in body
     */
    @GetMapping("/picking-slips")
    @Timed
    public ResponseEntity<List<PickingSlipDTO>> getAllPickingSlips(@RequestParam(required = false) String idstatustype,@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PickingSlips");
        Integer statusTypeId = null;
        if (idstatustype != null) { statusTypeId = Integer.parseInt(idstatustype); }
        Page<PickingSlipDTO> page = pickingSlipService.findAll(pageable, statusTypeId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/picking-slips");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /picking-slips/:id : get the "id" pickingSlip.
     *
     * @param id the id of the pickingSlipDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pickingSlipDTO, or with status 404 (Not Found)
     */
    @GetMapping("/picking-slips/{id}")
    @Timed
    public ResponseEntity<PickingSlipDTO> getPickingSlip(@PathVariable UUID id) {
        log.debug("REST request to get PickingSlip : {}", id);
        PickingSlipDTO pickingSlipDTO = pickingSlipService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pickingSlipDTO));
    }

    /**
     * DELETE  /picking-slips/:id : delete the "id" pickingSlip.
     *
     * @param id the id of the pickingSlipDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/picking-slips/{id}")
    @Timed
    public ResponseEntity<Void> deletePickingSlip(@PathVariable UUID id) {
        log.debug("REST request to delete PickingSlip : {}", id);
        pickingSlipService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/picking-slips?query=:query : search for the pickingSlip corresponding
     * to the query.
     *
     * @param query the query of the pickingSlip search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/picking-slips")
    @Timed
    public ResponseEntity<List<PickingSlipDTO>> searchPickingSlips(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of PickingSlips for query {}", query);
        Page<PickingSlipDTO> page = pickingSlipService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/picking-slips");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/picking-slips/set-status/{id}")
    @Timed
    public ResponseEntity< PickingSlipDTO> setStatusPickingSlip(@PathVariable Integer id, @RequestBody PickingSlipDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status PickingSlip : {}", dto);
        PickingSlipDTO r = pickingSlipService.changePickingSlipStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
