package id.atiila.web.rest;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import javax.sql.DataSource;

import id.atiila.service.ReportUtils;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.export.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;

@RestController
@RequestMapping("/api/report")
public class ReportSample {
    protected Logger logger = Logger.getLogger(ReportSample.class.getName());

    @Autowired
    JRFileVirtualizer fv;

    @Autowired
    private JRSwapFileVirtualizer sfv;

    @Autowired
    DataSource datasource;

    @Autowired
    ReportUtils reportUtils;

    @GetMapping("/test/pdf")
    public ResponseEntity<InputStreamResource> getReportPDF() {
        Map<String, Object> param = reportUtils.getReportParam();
        String name = "product.pdf";
        return reportUtils.generatePDF("product", name, param);
    }

    @GetMapping("/test/xlsx")
    public ResponseEntity<InputStreamResource> getReportXLSX() {
        Map<String, Object> param = reportUtils.getReportParam();
        String name = "product.xlsx";
        return reportUtils.generateXLSX("product", name, param);
    }

    @GetMapping("/test/csv")
    public ResponseEntity<InputStreamResource> getReportCSV() {
        Map<String, Object> param = reportUtils.getReportParam();
        String name = "product.csv";
        return reportUtils.generateCSV("product", name, param);
    }

    @GetMapping("/test/html")
    public ResponseEntity<InputStreamResource> getReportHTML() {
        Map<String, Object> param = reportUtils.getReportParam();
        String name = "product.html";
        return reportUtils.generateHTML("product", name, param);
    }

}
