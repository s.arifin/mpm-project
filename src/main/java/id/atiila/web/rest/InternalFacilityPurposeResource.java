package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.InternalFacilityPurposeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.InternalFacilityPurposeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing InternalFacilityPurpose.
 */
@RestController
@RequestMapping("/api")
public class InternalFacilityPurposeResource {

    private final Logger log = LoggerFactory.getLogger(InternalFacilityPurposeResource.class);

    private static final String ENTITY_NAME = "internalFacilityPurpose";

    private final InternalFacilityPurposeService internalFacilityPurposeService;

    public InternalFacilityPurposeResource(InternalFacilityPurposeService internalFacilityPurposeService) {
        this.internalFacilityPurposeService = internalFacilityPurposeService;
    }

    /**
     * POST  /internal-facility-purposes : Create a new internalFacilityPurpose.
     *
     * @param internalFacilityPurposeDTO the internalFacilityPurposeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new internalFacilityPurposeDTO, or with status 400 (Bad Request) if the internalFacilityPurpose has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/internal-facility-purposes")
    @Timed
    public ResponseEntity<InternalFacilityPurposeDTO> createInternalFacilityPurpose(@RequestBody InternalFacilityPurposeDTO internalFacilityPurposeDTO) throws URISyntaxException {
        log.debug("REST request to save InternalFacilityPurpose : {}", internalFacilityPurposeDTO);
        if (internalFacilityPurposeDTO.getIdPartyFacility() != null) {
            throw new BadRequestAlertException("A new internalFacilityPurpose cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InternalFacilityPurposeDTO result = internalFacilityPurposeService.save(internalFacilityPurposeDTO);
        return ResponseEntity.created(new URI("/api/internal-facility-purposes/" + result.getIdPartyFacility()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPartyFacility().toString()))
            .body(result);
    }

    /**
     * POST  /internal-facility-purposes/process : Execute Bussiness Process internalFacilityPurpose.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/internal-facility-purposes/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processInternalFacilityPurpose(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException  {
        log.debug("REST request to any process InternalFacilityPurpose ");
        Map<String, Object> result = internalFacilityPurposeService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /internal-facility-purposes/execute : Execute Bussiness Process internalFacilityPurpose.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  internalFacilityPurposeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/internal-facility-purposes/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedInternalFacilityPurpose(HttpServletRequest request, @RequestBody InternalFacilityPurposeDTO item) throws URISyntaxException  {
        log.debug("REST request to any process InternalFacilityPurpose");
        Map<String, Object> result = internalFacilityPurposeService.processExecuteData(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /internal-facility-purposes/execute-list : Execute Bussiness Process internalFacilityPurpose.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  internalFacilityPurposeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/internal-facility-purposes/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListInternalFacilityPurpose(HttpServletRequest request, @RequestBody Set<InternalFacilityPurposeDTO> items) throws URISyntaxException {
        log.debug("REST request to process List InternalFacilityPurpose");
        Map<String, Object> result = internalFacilityPurposeService.processExecuteListData(request, items);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /internal-facility-purposes : Updates an existing internalFacilityPurpose.
     *
     * @param internalFacilityPurposeDTO the internalFacilityPurposeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated internalFacilityPurposeDTO,
     * or with status 400 (Bad Request) if the internalFacilityPurposeDTO is not valid,
     * or with status 500 (Internal Server Error) if the internalFacilityPurposeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/internal-facility-purposes")
    @Timed
    public ResponseEntity<InternalFacilityPurposeDTO> updateInternalFacilityPurpose(@RequestBody InternalFacilityPurposeDTO internalFacilityPurposeDTO) throws URISyntaxException {
        log.debug("REST request to update InternalFacilityPurpose : {}", internalFacilityPurposeDTO);
        if (internalFacilityPurposeDTO.getIdPartyFacility() == null) {
            return createInternalFacilityPurpose(internalFacilityPurposeDTO);
        }
        InternalFacilityPurposeDTO result = internalFacilityPurposeService.save(internalFacilityPurposeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, internalFacilityPurposeDTO.getIdPartyFacility().toString()))
            .body(result);
    }

    /**
     * GET  /internal-facility-purposes : get all the internalFacilityPurposes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of internalFacilityPurposes in body
     */
    @GetMapping("/internal-facility-purposes")
    @Timed
    public ResponseEntity<List<InternalFacilityPurposeDTO>> getAllInternalFacilityPurposes(Pageable pageable) {
        log.debug("REST request to get a page of InternalFacilityPurposes");
        Page<InternalFacilityPurposeDTO> page = internalFacilityPurposeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/internal-facility-purposes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/internal-facility-purposes/filterBy")
    @Timed
    public ResponseEntity<List<InternalFacilityPurposeDTO>> getAllFilteredInternalFacilityPurposes(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of InternalFacilityPurposes");
        Page<InternalFacilityPurposeDTO> page = internalFacilityPurposeService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/internal-facility-purposes/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /internal-facility-purposes/:id : get the "id" internalFacilityPurpose.
     *
     * @param id the id of the internalFacilityPurposeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the internalFacilityPurposeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/internal-facility-purposes/{id}")
    @Timed
    public ResponseEntity<InternalFacilityPurposeDTO> getInternalFacilityPurpose(@PathVariable UUID id) {
        log.debug("REST request to get InternalFacilityPurpose : {}", id);
        InternalFacilityPurposeDTO internalFacilityPurposeDTO = internalFacilityPurposeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(internalFacilityPurposeDTO));
    }

    /**
     * DELETE  /internal-facility-purposes/:id : delete the "id" internalFacilityPurpose.
     *
     * @param id the id of the internalFacilityPurposeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/internal-facility-purposes/{id}")
    @Timed
    public ResponseEntity<Void> deleteInternalFacilityPurpose(@PathVariable UUID id) {
        log.debug("REST request to delete InternalFacilityPurpose : {}", id);
        internalFacilityPurposeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/internal-facility-purposes?query=:query : search for the internalFacilityPurpose corresponding
     * to the query.
     *
     * @param query the query of the internalFacilityPurpose search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/internal-facility-purposes")
    @Timed
    public ResponseEntity<List<InternalFacilityPurposeDTO>> searchInternalFacilityPurposes(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of InternalFacilityPurposes for query {}", query);
        Page<InternalFacilityPurposeDTO> page = internalFacilityPurposeService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/internal-facility-purposes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
