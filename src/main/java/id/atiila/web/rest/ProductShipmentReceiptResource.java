package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ProductShipmentReceiptService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ProductShipmentReceiptDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProductShipmentReceipt.
 */
@RestController
@RequestMapping("/api")
public class ProductShipmentReceiptResource {

    private final Logger log = LoggerFactory.getLogger(ProductShipmentReceiptResource.class);

    private static final String ENTITY_NAME = "productShipmentReceipt";

    private final ProductShipmentReceiptService productShipmentReceiptService;

    public ProductShipmentReceiptResource(ProductShipmentReceiptService productShipmentReceiptService) {
        this.productShipmentReceiptService = productShipmentReceiptService;
    }

    /**
     * POST  /product-shipment-receipts : Create a new productShipmentReceipt.
     *
     * @param productShipmentReceiptDTO the productShipmentReceiptDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productShipmentReceiptDTO, or with status 400 (Bad Request) if the productShipmentReceipt has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-shipment-receipts")
    @Timed
    public ResponseEntity<ProductShipmentReceiptDTO> createProductShipmentReceipt(@RequestBody ProductShipmentReceiptDTO productShipmentReceiptDTO) throws URISyntaxException {
        log.debug("REST request to save ProductShipmentReceipt : {}", productShipmentReceiptDTO);
        if (productShipmentReceiptDTO.getIdReceipt() != null) {
            throw new BadRequestAlertException("A new productShipmentReceipt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductShipmentReceiptDTO result = productShipmentReceiptService.save(productShipmentReceiptDTO);
        return ResponseEntity.created(new URI("/api/product-shipment-receipts/" + result.getIdReceipt()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdReceipt().toString()))
            .body(result);
    }

    /**
     * POST  /product-shipment-receipts/process : Execute Bussiness Process productShipmentReceipt.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-shipment-receipts/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processProductShipmentReceipt(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ProductShipmentReceipt ");
        Map<String, Object> result = productShipmentReceiptService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /product-shipment-receipts/execute : Execute Bussiness Process productShipmentReceipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  productShipmentReceiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-shipment-receipts/execute")
    @Timed
    public ResponseEntity<ProductShipmentReceiptDTO> executedProductShipmentReceipt(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ProductShipmentReceipt");
        ProductShipmentReceiptDTO result = productShipmentReceiptService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<ProductShipmentReceiptDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /product-shipment-receipts/execute-list : Execute Bussiness Process productShipmentReceipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  productShipmentReceiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-shipment-receipts/execute-list")
    @Timed
    public ResponseEntity<Set<ProductShipmentReceiptDTO>> executedListProductShipmentReceipt(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List ProductShipmentReceipt");
        Set<ProductShipmentReceiptDTO> result = productShipmentReceiptService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<ProductShipmentReceiptDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /product-shipment-receipts : Updates an existing productShipmentReceipt.
     *
     * @param productShipmentReceiptDTO the productShipmentReceiptDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productShipmentReceiptDTO,
     * or with status 400 (Bad Request) if the productShipmentReceiptDTO is not valid,
     * or with status 500 (Internal Server Error) if the productShipmentReceiptDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/product-shipment-receipts")
    @Timed
    public ResponseEntity<ProductShipmentReceiptDTO> updateProductShipmentReceipt(@RequestBody ProductShipmentReceiptDTO productShipmentReceiptDTO) throws URISyntaxException {
        log.debug("REST request to update ProductShipmentReceipt : {}", productShipmentReceiptDTO);
        if (productShipmentReceiptDTO.getIdReceipt() == null) {
            return createProductShipmentReceipt(productShipmentReceiptDTO);
        }
        ProductShipmentReceiptDTO result = productShipmentReceiptService.save(productShipmentReceiptDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, productShipmentReceiptDTO.getIdReceipt().toString()))
            .body(result);
    }

    /**
     * GET  /product-shipment-receipts : get all the productShipmentReceipts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of productShipmentReceipts in body
     */
    @GetMapping("/product-shipment-receipts")
    @Timed
    public ResponseEntity<List<ProductShipmentReceiptDTO>> getAllProductShipmentReceipts(Pageable pageable) {
        log.debug("REST request to get a page of ProductShipmentReceipts");
        Page<ProductShipmentReceiptDTO> page = productShipmentReceiptService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-shipment-receipts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/product-shipment-receipts/filterBy")
    @Timed
    public ResponseEntity<List<ProductShipmentReceiptDTO>> getAllFilteredProductShipmentReceipts(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ProductShipmentReceipts");
        Page<ProductShipmentReceiptDTO> page = productShipmentReceiptService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-shipment-receipts/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /product-shipment-receipts/:id : get the "id" productShipmentReceipt.
     *
     * @param id the id of the productShipmentReceiptDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productShipmentReceiptDTO, or with status 404 (Not Found)
     */
    @GetMapping("/product-shipment-receipts/{id}")
    @Timed
    public ResponseEntity<ProductShipmentReceiptDTO> getProductShipmentReceipt(@PathVariable UUID id) {
        log.debug("REST request to get ProductShipmentReceipt : {}", id);
        ProductShipmentReceiptDTO productShipmentReceiptDTO = productShipmentReceiptService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(productShipmentReceiptDTO));
    }

    /**
     * DELETE  /product-shipment-receipts/:id : delete the "id" productShipmentReceipt.
     *
     * @param id the id of the productShipmentReceiptDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/product-shipment-receipts/{id}")
    @Timed
    public ResponseEntity<Void> deleteProductShipmentReceipt(@PathVariable UUID id) {
        log.debug("REST request to delete ProductShipmentReceipt : {}", id);
        productShipmentReceiptService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/product-shipment-receipts?query=:query : search for the productShipmentReceipt corresponding
     * to the query.
     *
     * @param query the query of the productShipmentReceipt search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/product-shipment-receipts")
    @Timed
    public ResponseEntity<List<ProductShipmentReceiptDTO>> searchProductShipmentReceipts(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ProductShipmentReceipts for query {}", query);
        Page<ProductShipmentReceiptDTO> page = productShipmentReceiptService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/product-shipment-receipts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/product-shipment-receipts/set-status/{id}")
    @Timed
    public ResponseEntity< ProductShipmentReceiptDTO> setStatusProductShipmentReceipt(@PathVariable Integer id, @RequestBody ProductShipmentReceiptDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status ProductShipmentReceipt : {}", dto);
        ProductShipmentReceiptDTO r = productShipmentReceiptService.changeProductShipmentReceiptStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
