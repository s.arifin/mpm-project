package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.EmployeeCustomerRelationshipService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.EmployeeCustomerRelationshipDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing EmployeeCustomerRelationship.
 */
@RestController
@RequestMapping("/api")
public class EmployeeCustomerRelationshipResource {

    private final Logger log = LoggerFactory.getLogger(EmployeeCustomerRelationshipResource.class);

    private static final String ENTITY_NAME = "employeeCustomerRelationship";

    private final EmployeeCustomerRelationshipService employeeCustomerRelationshipService;

    public EmployeeCustomerRelationshipResource(EmployeeCustomerRelationshipService employeeCustomerRelationshipService) {
        this.employeeCustomerRelationshipService = employeeCustomerRelationshipService;
    }

    /**
     * POST  /employee-customer-relationships : Create a new employeeCustomerRelationship.
     *
     * @param employeeCustomerRelationshipDTO the employeeCustomerRelationshipDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new employeeCustomerRelationshipDTO, or with status 400 (Bad Request) if the employeeCustomerRelationship has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/employee-customer-relationships")
    @Timed
    public ResponseEntity<EmployeeCustomerRelationshipDTO> createEmployeeCustomerRelationship(@RequestBody EmployeeCustomerRelationshipDTO employeeCustomerRelationshipDTO) throws URISyntaxException {
        log.debug("REST request to save EmployeeCustomerRelationship : {}", employeeCustomerRelationshipDTO);
        if (employeeCustomerRelationshipDTO.getIdPartyRelationship() != null) {
            throw new BadRequestAlertException("A new employeeCustomerRelationship cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EmployeeCustomerRelationshipDTO result = employeeCustomerRelationshipService.save(employeeCustomerRelationshipDTO);
        return ResponseEntity.created(new URI("/api/employee-customer-relationships/" + result.getIdPartyRelationship()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPartyRelationship().toString()))
            .body(result);
    }

    /**
     * POST  /employee-customer-relationships/process : Execute Bussiness Process employeeCustomerRelationship.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/employee-customer-relationships/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processEmployeeCustomerRelationship(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process EmployeeCustomerRelationship ");
        Map<String, Object> result = employeeCustomerRelationshipService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /employee-customer-relationships/execute : Execute Bussiness Process employeeCustomerRelationship.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  employeeCustomerRelationshipDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/employee-customer-relationships/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedEmployeeCustomerRelationship(HttpServletRequest request, @RequestBody EmployeeCustomerRelationshipDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process EmployeeCustomerRelationship");
        Map<String, Object> result = employeeCustomerRelationshipService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /employee-customer-relationships/execute-list : Execute Bussiness Process employeeCustomerRelationship.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  employeeCustomerRelationshipDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/employee-customer-relationships/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListEmployeeCustomerRelationship(HttpServletRequest request, @RequestBody Set<EmployeeCustomerRelationshipDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List EmployeeCustomerRelationship");
        Map<String, Object> result = employeeCustomerRelationshipService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /employee-customer-relationships : Updates an existing employeeCustomerRelationship.
     *
     * @param employeeCustomerRelationshipDTO the employeeCustomerRelationshipDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated employeeCustomerRelationshipDTO,
     * or with status 400 (Bad Request) if the employeeCustomerRelationshipDTO is not valid,
     * or with status 500 (Internal Server Error) if the employeeCustomerRelationshipDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/employee-customer-relationships")
    @Timed
    public ResponseEntity<EmployeeCustomerRelationshipDTO> updateEmployeeCustomerRelationship(@RequestBody EmployeeCustomerRelationshipDTO employeeCustomerRelationshipDTO) throws URISyntaxException {
        log.debug("REST request to update EmployeeCustomerRelationship : {}", employeeCustomerRelationshipDTO);
        if (employeeCustomerRelationshipDTO.getIdPartyRelationship() == null) {
            return createEmployeeCustomerRelationship(employeeCustomerRelationshipDTO);
        }
        EmployeeCustomerRelationshipDTO result = employeeCustomerRelationshipService.save(employeeCustomerRelationshipDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, employeeCustomerRelationshipDTO.getIdPartyRelationship().toString()))
            .body(result);
    }

    /**
     * GET  /employee-customer-relationships : get all the employeeCustomerRelationships.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of employeeCustomerRelationships in body
     */
    @GetMapping("/employee-customer-relationships")
    @Timed
    public ResponseEntity<List<EmployeeCustomerRelationshipDTO>> getAllEmployeeCustomerRelationships(Pageable pageable) {
        log.debug("REST request to get a page of EmployeeCustomerRelationships");
        Page<EmployeeCustomerRelationshipDTO> page = employeeCustomerRelationshipService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/employee-customer-relationships");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/employee-customer-relationships/filterBy")
    @Timed
    public ResponseEntity<List<EmployeeCustomerRelationshipDTO>> getAllFilteredEmployeeCustomerRelationships(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of EmployeeCustomerRelationships");
        Page<EmployeeCustomerRelationshipDTO> page = employeeCustomerRelationshipService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/employee-customer-relationships/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /employee-customer-relationships/:id : get the "id" employeeCustomerRelationship.
     *
     * @param id the id of the employeeCustomerRelationshipDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the employeeCustomerRelationshipDTO, or with status 404 (Not Found)
     */
    @GetMapping("/employee-customer-relationships/{id}")
    @Timed
    public ResponseEntity<EmployeeCustomerRelationshipDTO> getEmployeeCustomerRelationship(@PathVariable UUID id) {
        log.debug("REST request to get EmployeeCustomerRelationship : {}", id);
        EmployeeCustomerRelationshipDTO employeeCustomerRelationshipDTO = employeeCustomerRelationshipService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(employeeCustomerRelationshipDTO));
    }

    /**
     * DELETE  /employee-customer-relationships/:id : delete the "id" employeeCustomerRelationship.
     *
     * @param id the id of the employeeCustomerRelationshipDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/employee-customer-relationships/{id}")
    @Timed
    public ResponseEntity<Void> deleteEmployeeCustomerRelationship(@PathVariable UUID id) {
        log.debug("REST request to delete EmployeeCustomerRelationship : {}", id);
        employeeCustomerRelationshipService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/employee-customer-relationships?query=:query : search for the employeeCustomerRelationship corresponding
     * to the query.
     *
     * @param query the query of the employeeCustomerRelationship search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/employee-customer-relationships")
    @Timed
    public ResponseEntity<List<EmployeeCustomerRelationshipDTO>> searchEmployeeCustomerRelationships(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of EmployeeCustomerRelationships for query {}", query);
        Page<EmployeeCustomerRelationshipDTO> page = employeeCustomerRelationshipService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/employee-customer-relationships");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
