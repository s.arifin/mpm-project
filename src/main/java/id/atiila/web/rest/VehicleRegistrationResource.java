package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.VehicleRegistrationService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VehicleRegistrationDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VehicleRegistration.
 */
@RestController
@RequestMapping("/api")
public class VehicleRegistrationResource {

    private final Logger log = LoggerFactory.getLogger(VehicleRegistrationResource.class);

    private static final String ENTITY_NAME = "vehicleRegistration";

    private final VehicleRegistrationService vehicleRegistrationService;

    public VehicleRegistrationResource(VehicleRegistrationService vehicleRegistrationService) {
        this.vehicleRegistrationService = vehicleRegistrationService;
    }

    /**
     * POST  /vehicle-registrations : Create a new vehicleRegistration.
     *
     * @param vehicleRegistrationDTO the vehicleRegistrationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vehicleRegistrationDTO, or with status 400 (Bad Request) if the vehicleRegistration has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-registrations")
    @Timed
    public ResponseEntity<VehicleRegistrationDTO> createVehicleRegistration(@RequestBody VehicleRegistrationDTO vehicleRegistrationDTO) throws URISyntaxException {
        log.debug("REST request to save VehicleRegistration : {}", vehicleRegistrationDTO);
        if (vehicleRegistrationDTO.getIdRequirement() != null) {
            throw new BadRequestAlertException("A new vehicleRegistration cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VehicleRegistrationDTO result = vehicleRegistrationService.save(vehicleRegistrationDTO);
        return ResponseEntity.created(new URI("/api/vehicle-registrations/" + result.getIdRequirement()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * POST  /vehicle-registrations/process : Execute Bussiness Process vehicleRegistration.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-registrations/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processVehicleRegistration(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process VehicleRegistration ");
        Map<String, Object> result = vehicleRegistrationService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vehicle-registrations/execute : Execute Bussiness Process vehicleRegistration.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  vehicleRegistrationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-registrations/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedVehicleRegistration(HttpServletRequest request, @RequestBody VehicleRegistrationDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process VehicleRegistration");
        Map<String, Object> result = vehicleRegistrationService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vehicle-registrations/execute-list : Execute Bussiness Process vehicleRegistration.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  vehicleRegistrationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-registrations/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListVehicleRegistration(HttpServletRequest request, @RequestBody Set<VehicleRegistrationDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List VehicleRegistration");
        Map<String, Object> result = vehicleRegistrationService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /vehicle-registrations : Updates an existing vehicleRegistration.
     *
     * @param vehicleRegistrationDTO the vehicleRegistrationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vehicleRegistrationDTO,
     * or with status 400 (Bad Request) if the vehicleRegistrationDTO is not valid,
     * or with status 500 (Internal Server Error) if the vehicleRegistrationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vehicle-registrations")
    @Timed
    public ResponseEntity<VehicleRegistrationDTO> updateVehicleRegistration(@RequestBody VehicleRegistrationDTO vehicleRegistrationDTO) throws URISyntaxException {
        log.debug("REST request to update VehicleRegistration : {}", vehicleRegistrationDTO);
        if (vehicleRegistrationDTO.getIdRequirement() == null) {
            return createVehicleRegistration(vehicleRegistrationDTO);
        }
        VehicleRegistrationDTO result = vehicleRegistrationService.save(vehicleRegistrationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vehicleRegistrationDTO.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * GET  /vehicle-registrations : get all the vehicleRegistrations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vehicleRegistrations in body
     */
    @GetMapping("/vehicle-registrations")
    @Timed
    public ResponseEntity<List<VehicleRegistrationDTO>> getAllVehicleRegistrations(Pageable pageable) {
        log.debug("REST request to get a page of VehicleRegistrations");
        Page<VehicleRegistrationDTO> page = vehicleRegistrationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-registrations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-registrations/filterBy")
    @Timed
    public ResponseEntity<List<VehicleRegistrationDTO>> getAllFilteredVehicleRegistrations(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of VehicleRegistrations");
        Page<VehicleRegistrationDTO> page = vehicleRegistrationService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-registrations/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /vehicle-registrations/:id : get the "id" vehicleRegistration.
     *
     * @param id the id of the vehicleRegistrationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vehicleRegistrationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vehicle-registrations/{id}")
    @Timed
    public ResponseEntity<VehicleRegistrationDTO> getVehicleRegistration(@PathVariable UUID id) {
        log.debug("REST request to get VehicleRegistration : {}", id);
        VehicleRegistrationDTO vehicleRegistrationDTO = vehicleRegistrationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vehicleRegistrationDTO));
    }

    /**
     * DELETE  /vehicle-registrations/:id : delete the "id" vehicleRegistration.
     *
     * @param id the id of the vehicleRegistrationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vehicle-registrations/{id}")
    @Timed
    public ResponseEntity<Void> deleteVehicleRegistration(@PathVariable UUID id) {
        log.debug("REST request to delete VehicleRegistration : {}", id);
        vehicleRegistrationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/vehicle-registrations?query=:query : search for the vehicleRegistration corresponding
     * to the query.
     *
     * @param query the query of the vehicleRegistration search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vehicle-registrations")
    @Timed
    public ResponseEntity<List<VehicleRegistrationDTO>> searchVehicleRegistrations(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of VehicleRegistrations for query {}", query);
        Page<VehicleRegistrationDTO> page = vehicleRegistrationService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vehicle-registrations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/vehicle-registrations/set-status/{id}")
    @Timed
    public ResponseEntity< VehicleRegistrationDTO> setStatusVehicleRegistration(@PathVariable Integer id, @RequestBody VehicleRegistrationDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status VehicleRegistration : {}", dto);
        VehicleRegistrationDTO r = vehicleRegistrationService.changeVehicleRegistrationStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(500);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
