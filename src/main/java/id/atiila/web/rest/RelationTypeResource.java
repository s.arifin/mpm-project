package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RelationTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RelationTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RelationType.
 */
@RestController
@RequestMapping("/api")
public class RelationTypeResource {

    private final Logger log = LoggerFactory.getLogger(RelationTypeResource.class);

    private static final String ENTITY_NAME = "relationType";

    private final RelationTypeService relationTypeService;

    public RelationTypeResource(RelationTypeService relationTypeService) {
        this.relationTypeService = relationTypeService;
    }

    /**
     * POST  /relation-types : Create a new relationType.
     *
     * @param relationTypeDTO the relationTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new relationTypeDTO, or with status 400 (Bad Request) if the relationType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/relation-types")
    @Timed
    public ResponseEntity<RelationTypeDTO> createRelationType(@RequestBody RelationTypeDTO relationTypeDTO) throws URISyntaxException {
        log.debug("REST request to save RelationType : {}", relationTypeDTO);
        if (relationTypeDTO.getIdRelationType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new relationType cannot already have an ID")).body(null);
        }
        RelationTypeDTO result = relationTypeService.save(relationTypeDTO);

        return ResponseEntity.created(new URI("/api/relation-types/" + result.getIdRelationType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRelationType().toString()))
            .body(result);
    }

    /**
     * POST  /relation-types/execute : Execute Bussiness Process relationType.
     *
     * @param relationTypeDTO the relationTypeDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  relationTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/relation-types/execute")
    @Timed
    public ResponseEntity<RelationTypeDTO> executedRelationType(@RequestBody RelationTypeDTO relationTypeDTO) throws URISyntaxException {
        log.debug("REST request to process RelationType : {}", relationTypeDTO);
        return new ResponseEntity<RelationTypeDTO>(relationTypeDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /relation-types : Updates an existing relationType.
     *
     * @param relationTypeDTO the relationTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated relationTypeDTO,
     * or with status 400 (Bad Request) if the relationTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the relationTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/relation-types")
    @Timed
    public ResponseEntity<RelationTypeDTO> updateRelationType(@RequestBody RelationTypeDTO relationTypeDTO) throws URISyntaxException {
        log.debug("REST request to update RelationType : {}", relationTypeDTO);
        if (relationTypeDTO.getIdRelationType() == null) {
            return createRelationType(relationTypeDTO);
        }
        RelationTypeDTO result = relationTypeService.save(relationTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, relationTypeDTO.getIdRelationType().toString()))
            .body(result);
    }

    /**
     * GET  /relation-types : get all the relationTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of relationTypes in body
     */
    @GetMapping("/relation-types")
    @Timed
    public ResponseEntity<List<RelationTypeDTO>> getAllRelationTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of RelationTypes");
        Page<RelationTypeDTO> page = relationTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/relation-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /relation-types/:id : get the "id" relationType.
     *
     * @param id the id of the relationTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the relationTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/relation-types/{id}")
    @Timed
    public ResponseEntity<RelationTypeDTO> getRelationType(@PathVariable Integer id) {
        log.debug("REST request to get RelationType : {}", id);
        RelationTypeDTO relationTypeDTO = relationTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(relationTypeDTO));
    }

    /**
     * DELETE  /relation-types/:id : delete the "id" relationType.
     *
     * @param id the id of the relationTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/relation-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteRelationType(@PathVariable Integer id) {
        log.debug("REST request to delete RelationType : {}", id);
        relationTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/relation-types?query=:query : search for the relationType corresponding
     * to the query.
     *
     * @param query the query of the relationType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/relation-types")
    @Timed
    public ResponseEntity<List<RelationTypeDTO>> searchRelationTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of RelationTypes for query {}", query);
        Page<RelationTypeDTO> page = relationTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/relation-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/relation-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processRelationType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process RelationType ");
        Map<String, Object> result = relationTypeService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }
}
