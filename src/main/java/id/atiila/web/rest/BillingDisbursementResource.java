package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.BillingDisbursementService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.BillingDisbursementDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BillingDisbursement.
 */
@RestController
@RequestMapping("/api")
public class BillingDisbursementResource {

    private final Logger log = LoggerFactory.getLogger(BillingDisbursementResource.class);

    private static final String ENTITY_NAME = "billingDisbursement";

    private final BillingDisbursementService billingDisbursementService;

    public BillingDisbursementResource(BillingDisbursementService billingDisbursementService) {
        this.billingDisbursementService = billingDisbursementService;
    }

    /**
     * POST  /billing-disbursements : Create a new billingDisbursement.
     *
     * @param billingDisbursementDTO the billingDisbursementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new billingDisbursementDTO, or with status 400 (Bad Request) if the billingDisbursement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-disbursements")
    @Timed
    public ResponseEntity<BillingDisbursementDTO> createBillingDisbursement(@RequestBody BillingDisbursementDTO billingDisbursementDTO) throws URISyntaxException {
        log.debug("REST request to save BillingDisbursement : {}", billingDisbursementDTO);
        if (billingDisbursementDTO.getIdBilling() != null) {
            throw new BadRequestAlertException("A new billingDisbursement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BillingDisbursementDTO result = billingDisbursementService.save(billingDisbursementDTO);
        return ResponseEntity.created(new URI("/api/billing-disbursements/" + result.getIdBilling()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdBilling().toString()))
            .body(result);
    }

    /**
     * POST  /billing-disbursements/process : Execute Bussiness Process billingDisbursement.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-disbursements/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processBillingDisbursement(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process BillingDisbursement ");
        Map<String, Object> result = billingDisbursementService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /billing-disbursements/execute : Execute Bussiness Process billingDisbursement.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  billingDisbursementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-disbursements/execute")
    @Timed
    public ResponseEntity<BillingDisbursementDTO> executedBillingDisbursement(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process BillingDisbursement");
        BillingDisbursementDTO result = billingDisbursementService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<BillingDisbursementDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /billing-disbursements/execute-list : Execute Bussiness Process billingDisbursement.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  billingDisbursementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-disbursements/execute-list")
    @Timed
    public ResponseEntity<Set<BillingDisbursementDTO>> executedListBillingDisbursement(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List BillingDisbursement");
        Set<BillingDisbursementDTO> result = billingDisbursementService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<BillingDisbursementDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /billing-disbursements : Updates an existing billingDisbursement.
     *
     * @param billingDisbursementDTO the billingDisbursementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated billingDisbursementDTO,
     * or with status 400 (Bad Request) if the billingDisbursementDTO is not valid,
     * or with status 500 (Internal Server Error) if the billingDisbursementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/billing-disbursements")
    @Timed
    public ResponseEntity<BillingDisbursementDTO> updateBillingDisbursement(@RequestBody BillingDisbursementDTO billingDisbursementDTO) throws URISyntaxException {
        log.debug("REST request to update BillingDisbursement : {}", billingDisbursementDTO);
        if (billingDisbursementDTO.getIdBilling() == null) {
            return createBillingDisbursement(billingDisbursementDTO);
        }
        BillingDisbursementDTO result = billingDisbursementService.save(billingDisbursementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, billingDisbursementDTO.getIdBilling().toString()))
            .body(result);
    }

    /**
     * GET  /billing-disbursements : get all the billingDisbursements.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of billingDisbursements in body
     */
    @GetMapping("/billing-disbursements")
    @Timed
    public ResponseEntity<List<BillingDisbursementDTO>> getAllBillingDisbursements(Pageable pageable) {
        log.debug("REST request to get a page of BillingDisbursements");
        Page<BillingDisbursementDTO> page = billingDisbursementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/billing-disbursements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/billing-disbursements/filterBy")
    @Timed
    public ResponseEntity<List<BillingDisbursementDTO>> getAllFilteredBillingDisbursements(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ganti dong cuyyy");
        log.debug("internal atas ==" + request);
        Page<BillingDisbursementDTO> page = billingDisbursementService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/billing-disbursements/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /billing-disbursements/:id : get the "id" billingDisbursement.
     *
     * @param id the id of the billingDisbursementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the billingDisbursementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/billing-disbursements/{id}")
    @Timed
    public ResponseEntity<BillingDisbursementDTO> getBillingDisbursement(@PathVariable UUID id) {
        log.debug("REST request to get BillingDisbursement : {}", id);
        BillingDisbursementDTO billingDisbursementDTO = billingDisbursementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(billingDisbursementDTO));
    }

    /**
     * DELETE  /billing-disbursements/:id : delete the "id" billingDisbursement.
     *
     * @param id the id of the billingDisbursementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/billing-disbursements/{id}")
    @Timed
    public ResponseEntity<Void> deleteBillingDisbursement(@PathVariable UUID id) {
        log.debug("REST request to delete BillingDisbursement : {}", id);
        billingDisbursementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/billing-disbursements?query=:query : search for the billingDisbursement corresponding
     * to the query.
     *
     * @param query the query of the billingDisbursement search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/billing-disbursements")
    @Timed
    public ResponseEntity<List<BillingDisbursementDTO>> searchBillingDisbursements(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of BillingDisbursements for query {}", query);
        Page<BillingDisbursementDTO> page = billingDisbursementService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/billing-disbursements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/billing-disbursements/set-status/{id}")
    @Timed
    public ResponseEntity< BillingDisbursementDTO> setStatusBillingDisbursement(@PathVariable Integer id, @RequestBody BillingDisbursementDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status BillingDisbursement : {}", dto);
        BillingDisbursementDTO r = billingDisbursementService.changeBillingDisbursementStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    @PostMapping("/billing-disbursements/calData/{id}/{param}")
    @Timed
    public ResponseEntity<BillingDisbursementDTO> proCalData(@PathVariable Integer id, @PathVariable String param, @RequestBody BillingDisbursementDTO billingDisbursementDTO) throws URISyntaxException {
        log.debug("REST request to any process BillingDisbursement ");
        BillingDisbursementDTO result = billingDisbursementService.processCalData(id, param, billingDisbursementDTO);
        return new ResponseEntity<BillingDisbursementDTO>(result,null, HttpStatus.OK);
    }

}
