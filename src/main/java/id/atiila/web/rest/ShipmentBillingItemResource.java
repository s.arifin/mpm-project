package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ShipmentBillingItemService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ShipmentBillingItemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ShipmentBillingItem.
 */
@RestController
@RequestMapping("/api")
public class ShipmentBillingItemResource {

    private final Logger log = LoggerFactory.getLogger(ShipmentBillingItemResource.class);

    private static final String ENTITY_NAME = "shipmentBillingItem";

    private final ShipmentBillingItemService shipmentBillingItemService;

    public ShipmentBillingItemResource(ShipmentBillingItemService shipmentBillingItemService) {
        this.shipmentBillingItemService = shipmentBillingItemService;
    }

    /**
     * POST  /shipment-billing-items : Create a new shipmentBillingItem.
     *
     * @param shipmentBillingItemDTO the shipmentBillingItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shipmentBillingItemDTO, or with status 400 (Bad Request) if the shipmentBillingItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-billing-items")
    @Timed
    public ResponseEntity<ShipmentBillingItemDTO> createShipmentBillingItem(@RequestBody ShipmentBillingItemDTO shipmentBillingItemDTO) throws URISyntaxException {
        log.debug("REST request to save ShipmentBillingItem : {}", shipmentBillingItemDTO);
        if (shipmentBillingItemDTO.getIdShipmentBillingItem() != null) {
            throw new BadRequestAlertException("A new shipmentBillingItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShipmentBillingItemDTO result = shipmentBillingItemService.save(shipmentBillingItemDTO);
        return ResponseEntity.created(new URI("/api/shipment-billing-items/" + result.getIdShipmentBillingItem()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdShipmentBillingItem().toString()))
            .body(result);
    }

    /**
     * POST  /shipment-billing-items/process : Execute Bussiness Process shipmentBillingItem.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-billing-items/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processShipmentBillingItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ShipmentBillingItem ");
        Map<String, Object> result = shipmentBillingItemService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /shipment-billing-items/execute : Execute Bussiness Process shipmentBillingItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  shipmentBillingItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-billing-items/execute")
    @Timed
    public ResponseEntity<ShipmentBillingItemDTO> executedShipmentBillingItem(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ShipmentBillingItem");
        ShipmentBillingItemDTO result = shipmentBillingItemService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<ShipmentBillingItemDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /shipment-billing-items/execute-list : Execute Bussiness Process shipmentBillingItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  shipmentBillingItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-billing-items/execute-list")
    @Timed
    public ResponseEntity<Set<ShipmentBillingItemDTO>> executedListShipmentBillingItem(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List ShipmentBillingItem");
        Set<ShipmentBillingItemDTO> result = shipmentBillingItemService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<ShipmentBillingItemDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /shipment-billing-items : Updates an existing shipmentBillingItem.
     *
     * @param shipmentBillingItemDTO the shipmentBillingItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shipmentBillingItemDTO,
     * or with status 400 (Bad Request) if the shipmentBillingItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the shipmentBillingItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/shipment-billing-items")
    @Timed
    public ResponseEntity<ShipmentBillingItemDTO> updateShipmentBillingItem(@RequestBody ShipmentBillingItemDTO shipmentBillingItemDTO) throws URISyntaxException {
        log.debug("REST request to update ShipmentBillingItem : {}", shipmentBillingItemDTO);
        if (shipmentBillingItemDTO.getIdShipmentBillingItem() == null) {
            return createShipmentBillingItem(shipmentBillingItemDTO);
        }
        ShipmentBillingItemDTO result = shipmentBillingItemService.save(shipmentBillingItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shipmentBillingItemDTO.getIdShipmentBillingItem().toString()))
            .body(result);
    }

    /**
     * GET  /shipment-billing-items : get all the shipmentBillingItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shipmentBillingItems in body
     */
    @GetMapping("/shipment-billing-items")
    @Timed
    public ResponseEntity<List<ShipmentBillingItemDTO>> getAllShipmentBillingItems(Pageable pageable) {
        log.debug("REST request to get a page of ShipmentBillingItems");
        Page<ShipmentBillingItemDTO> page = shipmentBillingItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-billing-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/shipment-billing-items/filterBy")
    @Timed
    public ResponseEntity<List<ShipmentBillingItemDTO>> getAllFilteredShipmentBillingItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ShipmentBillingItems");
        Page<ShipmentBillingItemDTO> page = shipmentBillingItemService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-billing-items/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /shipment-billing-items/:id : get the "id" shipmentBillingItem.
     *
     * @param id the id of the shipmentBillingItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shipmentBillingItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/shipment-billing-items/{id}")
    @Timed
    public ResponseEntity<ShipmentBillingItemDTO> getShipmentBillingItem(@PathVariable UUID id) {
        log.debug("REST request to get ShipmentBillingItem : {}", id);
        ShipmentBillingItemDTO shipmentBillingItemDTO = shipmentBillingItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(shipmentBillingItemDTO));
    }

    /**
     * DELETE  /shipment-billing-items/:id : delete the "id" shipmentBillingItem.
     *
     * @param id the id of the shipmentBillingItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shipment-billing-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteShipmentBillingItem(@PathVariable UUID id) {
        log.debug("REST request to delete ShipmentBillingItem : {}", id);
        shipmentBillingItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/shipment-billing-items?query=:query : search for the shipmentBillingItem corresponding
     * to the query.
     *
     * @param query the query of the shipmentBillingItem search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/shipment-billing-items")
    @Timed
    public ResponseEntity<List<ShipmentBillingItemDTO>> searchShipmentBillingItems(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ShipmentBillingItems for query {}", query);
        Page<ShipmentBillingItemDTO> page = shipmentBillingItemService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/shipment-billing-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
