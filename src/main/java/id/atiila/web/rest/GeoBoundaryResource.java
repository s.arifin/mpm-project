package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.GeoBoundaryService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.GeoBoundaryDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.GET;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing GeoBoundary.
 */
@RestController
@RequestMapping("/api")
public class GeoBoundaryResource {

    private final Logger log = LoggerFactory.getLogger(GeoBoundaryResource.class);

    private static final String ENTITY_NAME = "geoBoundary";

    private final GeoBoundaryService geoBoundaryService;

    public GeoBoundaryResource(GeoBoundaryService geoBoundaryService) {
        this.geoBoundaryService = geoBoundaryService;
    }

    /**
     * POST  /geo-boundaries : Create a new geoBoundary.
     *
     * @param geoBoundaryDTO the geoBoundaryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new geoBoundaryDTO, or with status 400 (Bad Request) if the geoBoundary has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/geo-boundaries")
    @Timed
    public ResponseEntity<GeoBoundaryDTO> createGeoBoundary(@RequestBody GeoBoundaryDTO geoBoundaryDTO) throws URISyntaxException {
        log.debug("REST request to save GeoBoundary : {}", geoBoundaryDTO);
        if (geoBoundaryDTO.getIdGeobou() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new geoBoundary cannot already have an ID")).body(null);
        }
        GeoBoundaryDTO result = geoBoundaryService.save(geoBoundaryDTO);
        return ResponseEntity.created(new URI("/api/geo-boundaries/" + result.getIdGeobou()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdGeobou().toString()))
            .body(result);
    }

    /**
     * POST  /geo-boundaries/execute{id}/{param} : Execute Bussiness Process geoBoundary.
     *
     * @param geoBoundaryDTO the geoBoundaryDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  geoBoundaryDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/geo-boundaries/execute/{id}/{param}")
    @Timed
    public ResponseEntity<GeoBoundaryDTO> executedGeoBoundary(@PathVariable Integer id, @PathVariable String param, @RequestBody GeoBoundaryDTO geoBoundaryDTO) throws URISyntaxException {
        log.debug("REST request to process GeoBoundary : {}", geoBoundaryDTO);
        GeoBoundaryDTO result = geoBoundaryService.processExecuteData(id, param, geoBoundaryDTO);
        return new ResponseEntity<GeoBoundaryDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /geo-boundaries/execute-list{id}/{param} : Execute Bussiness Process geoBoundary.
     *
     * @param geoBoundaryDTO the geoBoundaryDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  geoBoundaryDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/geo-boundaries/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<GeoBoundaryDTO>> executedListGeoBoundary(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<GeoBoundaryDTO> geoBoundaryDTO) throws URISyntaxException {
        log.debug("REST request to process List GeoBoundary");
        Set<GeoBoundaryDTO> result = geoBoundaryService.processExecuteListData(id, param, geoBoundaryDTO);
        return new ResponseEntity<Set<GeoBoundaryDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /geo-boundaries : Updates an existing geoBoundary.
     *
     * @param geoBoundaryDTO the geoBoundaryDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated geoBoundaryDTO,
     * or with status 400 (Bad Request) if the geoBoundaryDTO is not valid,
     * or with status 500 (Internal Server Error) if the geoBoundaryDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/geo-boundaries")
    @Timed
    public ResponseEntity<GeoBoundaryDTO> updateGeoBoundary(@RequestBody GeoBoundaryDTO geoBoundaryDTO) throws URISyntaxException {
        log.debug("REST request to update GeoBoundary : {}", geoBoundaryDTO);
        if (geoBoundaryDTO.getIdGeobou() == null) {
            return createGeoBoundary(geoBoundaryDTO);
        }
        GeoBoundaryDTO result = geoBoundaryService.save(geoBoundaryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, geoBoundaryDTO.getIdGeobou().toString()))
            .body(result);
    }

    /**
     * GET  /geo-boundaries : get all the geoBoundaries.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of geoBoundaries in body
     */
    @GetMapping("/geo-boundaries")
    @Timed
    public ResponseEntity<List<GeoBoundaryDTO>> getAllGeoBoundaries(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of GeoBoundaries");
        Page<GeoBoundaryDTO> page = geoBoundaryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/geo-boundaries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/geo-boundaries/by-parent/{id}")
    @Timed
    public ResponseEntity<List<GeoBoundaryDTO>> getGeoBoundaryByParent(@PathVariable UUID idparent){
        log.debug("REST request to get GeoBoundary By Parent : {}", idparent);
        List<GeoBoundaryDTO> list = geoBoundaryService.findAllByParent(idparent);
        return new ResponseEntity<>(list, null, HttpStatus.OK);
    }

    /**
     * GET  /geo-boundaries/:id : get the "id" geoBoundary.
     *
     * @param id the id of the geoBoundaryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the geoBoundaryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/geo-boundaries/{id}")
    @Timed
    public ResponseEntity<GeoBoundaryDTO> getGeoBoundary(@PathVariable UUID id) {
        log.debug("REST request to get GeoBoundary : {}", id);
        GeoBoundaryDTO geoBoundaryDTO = geoBoundaryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(geoBoundaryDTO));
    }

    /**
     * DELETE  /geo-boundaries/:id : delete the "id" geoBoundary.
     *
     * @param id the id of the geoBoundaryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/geo-boundaries/{id}")
    @Timed
    public ResponseEntity<Void> deleteGeoBoundary(@PathVariable UUID id) {
        log.debug("REST request to delete GeoBoundary : {}", id);
        geoBoundaryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/geo-boundaries?query=:query : search for the geoBoundary corresponding
     * to the query.
     *
     * @param query the query of the geoBoundary search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/geo-boundaries")
    @Timed
    public ResponseEntity<List<GeoBoundaryDTO>> searchGeoBoundaries(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of GeoBoundaries for query {}", query);
        Page<GeoBoundaryDTO> page = geoBoundaryService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/geo-boundaries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
