package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.DisbursementService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.DisbursementDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Disbursement.
 */
@RestController
@RequestMapping("/api")
public class DisbursementResource {

    private final Logger log = LoggerFactory.getLogger(DisbursementResource.class);

    private static final String ENTITY_NAME = "disbursement";

    private final DisbursementService disbursementService;

    public DisbursementResource(DisbursementService disbursementService) {
        this.disbursementService = disbursementService;
    }

    /**
     * POST  /disbursements : Create a new disbursement.
     *
     * @param disbursementDTO the disbursementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new disbursementDTO, or with status 400 (Bad Request) if the disbursement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/disbursements")
    @Timed
    public ResponseEntity<DisbursementDTO> createDisbursement(@RequestBody DisbursementDTO disbursementDTO) throws URISyntaxException {
        log.debug("REST request to save Disbursement : {}", disbursementDTO);
        if (disbursementDTO.getIdPayment() != null) {
            throw new BadRequestAlertException("A new disbursement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DisbursementDTO result = disbursementService.save(disbursementDTO);
        return ResponseEntity.created(new URI("/api/disbursements/" + result.getIdPayment()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPayment().toString()))
            .body(result);
    }

    /**
     * POST  /disbursements/process : Execute Bussiness Process disbursement.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/disbursements/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processDisbursement(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Disbursement ");
        Map<String, Object> result = disbursementService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /disbursements/execute : Execute Bussiness Process disbursement.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  disbursementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/disbursements/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedDisbursement(HttpServletRequest request, @RequestBody DisbursementDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Disbursement");
        Map<String, Object> result = disbursementService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /disbursements/execute-list : Execute Bussiness Process disbursement.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  disbursementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/disbursements/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListDisbursement(HttpServletRequest request, @RequestBody Set<DisbursementDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List Disbursement");
        Map<String, Object> result = disbursementService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /disbursements : Updates an existing disbursement.
     *
     * @param disbursementDTO the disbursementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated disbursementDTO,
     * or with status 400 (Bad Request) if the disbursementDTO is not valid,
     * or with status 500 (Internal Server Error) if the disbursementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/disbursements")
    @Timed
    public ResponseEntity<DisbursementDTO> updateDisbursement(@RequestBody DisbursementDTO disbursementDTO) throws URISyntaxException {
        log.debug("REST request to update Disbursement : {}", disbursementDTO);
        if (disbursementDTO.getIdPayment() == null) {
            return createDisbursement(disbursementDTO);
        }
        DisbursementDTO result = disbursementService.save(disbursementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, disbursementDTO.getIdPayment().toString()))
            .body(result);
    }

    /**
     * GET  /disbursements : get all the disbursements.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of disbursements in body
     */
    @GetMapping("/disbursements")
    @Timed
    public ResponseEntity<List<DisbursementDTO>> getAllDisbursements(Pageable pageable) {
        log.debug("REST request to get a page of Disbursements");
        Page<DisbursementDTO> page = disbursementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/disbursements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/disbursements/filterBy")
    @Timed
    public ResponseEntity<List<DisbursementDTO>> getAllFilteredDisbursements(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Disbursements");
        Page<DisbursementDTO> page = disbursementService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/disbursements/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /disbursements/:id : get the "id" disbursement.
     *
     * @param id the id of the disbursementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the disbursementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/disbursements/{id}")
    @Timed
    public ResponseEntity<DisbursementDTO> getDisbursement(@PathVariable UUID id) {
        log.debug("REST request to get Disbursement : {}", id);
        DisbursementDTO disbursementDTO = disbursementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(disbursementDTO));
    }

    /**
     * DELETE  /disbursements/:id : delete the "id" disbursement.
     *
     * @param id the id of the disbursementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/disbursements/{id}")
    @Timed
    public ResponseEntity<Void> deleteDisbursement(@PathVariable UUID id) {
        log.debug("REST request to delete Disbursement : {}", id);
        disbursementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/disbursements?query=:query : search for the disbursement corresponding
     * to the query.
     *
     * @param query the query of the disbursement search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/disbursements")
    @Timed
    public ResponseEntity<List<DisbursementDTO>> searchDisbursements(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Disbursements for query {}", query);
        Page<DisbursementDTO> page = disbursementService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/disbursements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/disbursements/set-status/{id}")
    @Timed
    public ResponseEntity< DisbursementDTO> setStatusDisbursement(@PathVariable Integer id, @RequestBody DisbursementDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status Disbursement : {}", dto);
        DisbursementDTO r = disbursementService.changeDisbursementStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
