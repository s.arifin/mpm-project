package id.atiila.web.rest;

import java.util.Map;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.WorkProductRequirementService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.WorkProductRequirementDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WorkProductRequirement.
 */
@RestController
@RequestMapping("/api")
public class WorkProductRequirementResource {

    private final Logger log = LoggerFactory.getLogger(WorkProductRequirementResource.class);

    private static final String ENTITY_NAME = "workProductRequirement";

    private final WorkProductRequirementService workProductRequirementService;

    public WorkProductRequirementResource(WorkProductRequirementService workProductRequirementService) {
        this.workProductRequirementService = workProductRequirementService;
    }

    /**
     * POST  /work-product-requirements : Create a new workProductRequirement.
     *
     * @param workProductRequirementDTO the workProductRequirementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new workProductRequirementDTO, or with status 400 (Bad Request) if the workProductRequirement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/work-product-requirements")
    @Timed
    public ResponseEntity<WorkProductRequirementDTO> createWorkProductRequirement(@RequestBody WorkProductRequirementDTO workProductRequirementDTO) throws URISyntaxException {
        log.debug("REST request to save WorkProductRequirement : {}", workProductRequirementDTO);
        if (workProductRequirementDTO.getIdRequirement() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new workProductRequirement cannot already have an ID")).body(null);
        }
        WorkProductRequirementDTO result = workProductRequirementService.save(workProductRequirementDTO);

        return ResponseEntity.created(new URI("/api/work-product-requirements/" + result.getIdRequirement()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * POST  /work-product-requirements/execute : Execute Bussiness Process workProductRequirement.
     *
     * @param workProductRequirementDTO the workProductRequirementDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  workProductRequirementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/work-product-requirements/execute/{id}")
    @Timed
    public ResponseEntity<WorkProductRequirementDTO> executedWorkProductRequirement(@PathVariable Integer id, @RequestBody WorkProductRequirementDTO workProductRequirementDTO) throws URISyntaxException {
        log.debug("REST request to process WorkProductRequirement : {}", workProductRequirementDTO);
        WorkProductRequirementDTO r = workProductRequirementService.processExecuteData(workProductRequirementDTO, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    /**
     * PUT  /work-product-requirements : Updates an existing workProductRequirement.
     *
     * @param workProductRequirementDTO the workProductRequirementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated workProductRequirementDTO,
     * or with status 400 (Bad Request) if the workProductRequirementDTO is not valid,
     * or with status 500 (Internal Server Error) if the workProductRequirementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/work-product-requirements")
    @Timed
    public ResponseEntity<WorkProductRequirementDTO> updateWorkProductRequirement(@RequestBody WorkProductRequirementDTO workProductRequirementDTO) throws URISyntaxException {
        log.debug("REST request to update WorkProductRequirement : {}", workProductRequirementDTO);
        if (workProductRequirementDTO.getIdRequirement() == null) {
            return createWorkProductRequirement(workProductRequirementDTO);
        }
        WorkProductRequirementDTO result = workProductRequirementService.save(workProductRequirementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, workProductRequirementDTO.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * GET  /work-product-requirements : get all the workProductRequirements.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of workProductRequirements in body
     */
    @GetMapping("/work-product-requirements")
    @Timed
    public ResponseEntity<List<WorkProductRequirementDTO>> getAllWorkProductRequirements(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of WorkProductRequirements");
        Page<WorkProductRequirementDTO> page = workProductRequirementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/work-product-requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @GetMapping("/work-product-requirements/filterBy")
    @Timed
    public ResponseEntity<List<WorkProductRequirementDTO>> getAllFilteredWorkProductRequirements(@RequestParam String filterBy, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of WorkProductRequirements");
        Page<WorkProductRequirementDTO> page = workProductRequirementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/work-product-requirements/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * GET  /work-product-requirements/:id : get the "id" workProductRequirement.
     *
     * @param id the id of the workProductRequirementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the workProductRequirementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/work-product-requirements/{id}")
    @Timed
    public ResponseEntity<WorkProductRequirementDTO> getWorkProductRequirement(@PathVariable UUID id) {
        log.debug("REST request to get WorkProductRequirement : {}", id);
        WorkProductRequirementDTO workProductRequirementDTO = workProductRequirementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(workProductRequirementDTO));
    }

    /**
     * DELETE  /work-product-requirements/:id : delete the "id" workProductRequirement.
     *
     * @param id the id of the workProductRequirementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/work-product-requirements/{id}")
    @Timed
    public ResponseEntity<Void> deleteWorkProductRequirement(@PathVariable UUID id) {
        log.debug("REST request to delete WorkProductRequirement : {}", id);
        workProductRequirementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/work-product-requirements?query=:query : search for the workProductRequirement corresponding
     * to the query.
     *
     * @param query the query of the workProductRequirement search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/work-product-requirements")
    @Timed
    public ResponseEntity<List<WorkProductRequirementDTO>> searchWorkProductRequirements(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of WorkProductRequirements for query {}", query);
        Page<WorkProductRequirementDTO> page = workProductRequirementService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/work-product-requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/work-product-requirements/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrderItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process OrderItem ");
        Map<String, Object> result = workProductRequirementService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
