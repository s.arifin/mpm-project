package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.QuoteItemService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.QuoteItemDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing QuoteItem.
 */
@RestController
@RequestMapping("/api")
public class QuoteItemResource {

    private final Logger log = LoggerFactory.getLogger(QuoteItemResource.class);

    private static final String ENTITY_NAME = "quoteItem";

    private final QuoteItemService quoteItemService;

    public QuoteItemResource(QuoteItemService quoteItemService) {
        this.quoteItemService = quoteItemService;
    }

    /**
     * POST  /quote-items : Create a new quoteItem.
     *
     * @param quoteItemDTO the quoteItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new quoteItemDTO, or with status 400 (Bad Request) if the quoteItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/quote-items")
    @Timed
    public ResponseEntity<QuoteItemDTO> createQuoteItem(@RequestBody QuoteItemDTO quoteItemDTO) throws URISyntaxException {
        log.debug("REST request to save QuoteItem : {}", quoteItemDTO);
        if (quoteItemDTO.getIdQuoteItem() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new quoteItem cannot already have an ID")).body(null);
        }
        QuoteItemDTO result = quoteItemService.save(quoteItemDTO);
        return ResponseEntity.created(new URI("/api/quote-items/" + result.getIdQuoteItem()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdQuoteItem().toString()))
            .body(result);
    }

    /**
     * POST  /quote-items/execute{id}/{param} : Execute Bussiness Process quoteItem.
     *
     * @param quoteItemDTO the quoteItemDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  quoteItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/quote-items/execute/{id}/{param}")
    @Timed
    public ResponseEntity<QuoteItemDTO> executedQuoteItem(@PathVariable Integer id, @PathVariable String param, @RequestBody QuoteItemDTO quoteItemDTO) throws URISyntaxException {
        log.debug("REST request to process QuoteItem : {}", quoteItemDTO);
        QuoteItemDTO result = quoteItemService.processExecuteData(id, param, quoteItemDTO);
        return new ResponseEntity<QuoteItemDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /quote-items/execute-list{id}/{param} : Execute Bussiness Process quoteItem.
     *
     * @param quoteItemDTO the quoteItemDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  quoteItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/quote-items/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<QuoteItemDTO>> executedListQuoteItem(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<QuoteItemDTO> quoteItemDTO) throws URISyntaxException {
        log.debug("REST request to process List QuoteItem");
        Set<QuoteItemDTO> result = quoteItemService.processExecuteListData(id, param, quoteItemDTO);
        return new ResponseEntity<Set<QuoteItemDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /quote-items : Updates an existing quoteItem.
     *
     * @param quoteItemDTO the quoteItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated quoteItemDTO,
     * or with status 400 (Bad Request) if the quoteItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the quoteItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/quote-items")
    @Timed
    public ResponseEntity<QuoteItemDTO> updateQuoteItem(@RequestBody QuoteItemDTO quoteItemDTO) throws URISyntaxException {
        log.debug("REST request to update QuoteItem : {}", quoteItemDTO);
        if (quoteItemDTO.getIdQuoteItem() == null) {
            return createQuoteItem(quoteItemDTO);
        }
        QuoteItemDTO result = quoteItemService.save(quoteItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, quoteItemDTO.getIdQuoteItem().toString()))
            .body(result);
    }

    /**
     * GET  /quote-items : get all the quoteItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of quoteItems in body
     */
    @GetMapping("/quote-items")
    @Timed
    public ResponseEntity<List<QuoteItemDTO>> getAllQuoteItems(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of QuoteItems");
        Page<QuoteItemDTO> page = quoteItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/quote-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /quote-items/:id : get the "id" quoteItem.
     *
     * @param id the id of the quoteItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the quoteItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/quote-items/{id}")
    @Timed
    public ResponseEntity<QuoteItemDTO> getQuoteItem(@PathVariable UUID id) {
        log.debug("REST request to get QuoteItem : {}", id);
        QuoteItemDTO quoteItemDTO = quoteItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(quoteItemDTO));
    }

    /**
     * DELETE  /quote-items/:id : delete the "id" quoteItem.
     *
     * @param id the id of the quoteItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/quote-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteQuoteItem(@PathVariable UUID id) {
        log.debug("REST request to delete QuoteItem : {}", id);
        quoteItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/quote-items?query=:query : search for the quoteItem corresponding
     * to the query.
     *
     * @param query the query of the quoteItem search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/quote-items")
    @Timed
    public ResponseEntity<List<QuoteItemDTO>> searchQuoteItems(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of QuoteItems for query {}", query);
        Page<QuoteItemDTO> page = quoteItemService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/quote-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
