package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.UnitDocumentMessageService;
import id.atiila.service.dto.CustomUnitDocumentMessageDTO;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.UnitDocumentMessageDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Response;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UnitDocumentMessage.
 */
@RestController
@RequestMapping("/api")
public class UnitDocumentMessageResource {

    private final Logger log = LoggerFactory.getLogger(UnitDocumentMessageResource.class);

    private static final String ENTITY_NAME = "unitDocumentMessage";

    private final UnitDocumentMessageService unitDocumentMessageService;

    public UnitDocumentMessageResource(UnitDocumentMessageService unitDocumentMessageService) {
        this.unitDocumentMessageService = unitDocumentMessageService;
    }

    @GetMapping("/unit-document-messages/by-requirement/{id}")
    public ResponseEntity<List<UnitDocumentMessageDTO>> getByRequirement(@PathVariable String id){
        log.debug("REST request to get unit document message : " + id);
        UUID idRequirement = UUID.fromString(id);
        List<UnitDocumentMessageDTO> list = unitDocumentMessageService.findByRequirement(idRequirement);
        return new ResponseEntity<>(list, null, HttpStatus.OK);
    }

    @GetMapping("/unit-document-messages/by-status-join-requirement")
    public ResponseEntity<List<CustomUnitDocumentMessageDTO>> getByStatusJoinRequirement(HttpServletRequest request, Pageable pageable){
        log.debug("REST request to get unit document message by status join requirement");
        Page<CustomUnitDocumentMessageDTO> list = unitDocumentMessageService.findByStatusJoinRequirement(request, pageable);
        return new ResponseEntity<>(list.getContent(), null, HttpStatus.OK);
    }

    @GetMapping("/unit-document-messages/by-status/{id}")
    public ResponseEntity<List<UnitDocumentMessageDTO>> getByApprovalStatus(@PathVariable Integer id){
        log.debug("REST request to get unit document message by status");
        List<UnitDocumentMessageDTO> list = unitDocumentMessageService.findByStatus(id);
        return new ResponseEntity<>(list, null, HttpStatus.OK);
    }

    @GetMapping("/unit-document-messages/by-requirement/{id}/status/{idstatus}")
    public ResponseEntity<List<UnitDocumentMessageDTO>> getByRequirementAndStatusApproval(@PathVariable String id, @PathVariable Integer idstatus){
        log.debug("REST request to get unit document message : " + id);
        UUID idRequirement = UUID.fromString(id);
        List<UnitDocumentMessageDTO> list = unitDocumentMessageService.findByRequirementAndApprovalStatus(idRequirement, idstatus);
        return new ResponseEntity<>(list, null, HttpStatus.OK);
    }

    /**
     * POST  /unit-document-messages : Create a new unitDocumentMessage.
     *
     * @param unitDocumentMessageDTO the unitDocumentMessageDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new unitDocumentMessageDTO, or with status 400 (Bad Request) if the unitDocumentMessage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-document-messages")
    @Timed
    public ResponseEntity<UnitDocumentMessageDTO> createUnitDocumentMessage(@RequestBody UnitDocumentMessageDTO unitDocumentMessageDTO) throws URISyntaxException {
        log.debug("REST request to save UnitDocumentMessage : {}", unitDocumentMessageDTO);
        if (unitDocumentMessageDTO.getIdMessage() != null) {
            throw new BadRequestAlertException("A new unitDocumentMessage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UnitDocumentMessageDTO result = unitDocumentMessageService.save(unitDocumentMessageDTO);
        return ResponseEntity.created(new URI("/api/unit-document-messages/" + result.getIdMessage()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdMessage().toString()))
            .body(result);
    }

    /**
     * POST  /unit-document-messages/process : Execute Bussiness Process unitDocumentMessage.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-document-messages/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processUnitDocumentMessage(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process UnitDocumentMessage ");
        Map<String, Object> result = unitDocumentMessageService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /unit-document-messages/execute : Execute Bussiness Process unitDocumentMessage.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  unitDocumentMessageDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-document-messages/execute")
    @Timed
    public ResponseEntity<Set<UnitDocumentMessageDTO>> executedUnitDocumentMessage(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        Set<UnitDocumentMessageDTO> result = unitDocumentMessageService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<UnitDocumentMessageDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /unit-document-messages/execute-list : Execute Bussiness Process unitDocumentMessage.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  unitDocumentMessageDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-document-messages/execute-list")
    @Timed
    public ResponseEntity<Set<UnitDocumentMessageDTO>> executedListUnitDocumentMessage(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List UnitDocumentMessage");
        Set<UnitDocumentMessageDTO> result = unitDocumentMessageService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<UnitDocumentMessageDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /unit-document-messages : Updates an existing unitDocumentMessage.
     *
     * @param unitDocumentMessageDTO the unitDocumentMessageDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated unitDocumentMessageDTO,
     * or with status 400 (Bad Request) if the unitDocumentMessageDTO is not valid,
     * or with status 500 (Internal Server Error) if the unitDocumentMessageDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/unit-document-messages")
    @Timed
    public ResponseEntity<UnitDocumentMessageDTO> updateUnitDocumentMessage(@RequestBody UnitDocumentMessageDTO unitDocumentMessageDTO) throws URISyntaxException {
        log.debug("REST request to update UnitDocumentMessage : {}", unitDocumentMessageDTO);
        if (unitDocumentMessageDTO.getIdMessage() == null) {
            return createUnitDocumentMessage(unitDocumentMessageDTO);
        }
        UnitDocumentMessageDTO result = unitDocumentMessageService.save(unitDocumentMessageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, unitDocumentMessageDTO.getIdMessage().toString()))
            .body(result);
    }

    /**
     * GET  /unit-document-messages : get all the unitDocumentMessages.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of unitDocumentMessages in body
     */
    @GetMapping("/unit-document-messages")
    @Timed
    public ResponseEntity<List<UnitDocumentMessageDTO>> getAllUnitDocumentMessages(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of UnitDocumentMessages");
        Page<UnitDocumentMessageDTO> page = unitDocumentMessageService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-document-messages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/unit-document-messages/filterBy")
    @Timed
    public ResponseEntity<List<UnitDocumentMessageDTO>> getAllFilteredUnitDocumentMessages(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of UnitDocumentMessages");
        Page<UnitDocumentMessageDTO> page = unitDocumentMessageService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-document-messages/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /unit-document-messages/:id : get the "id" unitDocumentMessage.
     *
     * @param id the id of the unitDocumentMessageDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the unitDocumentMessageDTO, or with status 404 (Not Found)
     */
    @GetMapping("/unit-document-messages/{id}")
    @Timed
    public ResponseEntity<UnitDocumentMessageDTO> getUnitDocumentMessage(@PathVariable Integer id) {
        log.debug("REST request to get UnitDocumentMessage : {}", id);
        UnitDocumentMessageDTO unitDocumentMessageDTO = unitDocumentMessageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(unitDocumentMessageDTO));
    }

    /**
     * DELETE  /unit-document-messages/:id : delete the "id" unitDocumentMessage.
     *
     * @param id the id of the unitDocumentMessageDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/unit-document-messages/{id}")
    @Timed
    public ResponseEntity<Void> deleteUnitDocumentMessage(@PathVariable Integer id) {
        log.debug("REST request to delete UnitDocumentMessage : {}", id);
        unitDocumentMessageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/unit-document-messages?query=:query : search for the unitDocumentMessage corresponding
     * to the query.
     *
     * @param query the query of the unitDocumentMessage search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/unit-document-messages")
    @Timed
    public ResponseEntity<List<UnitDocumentMessageDTO>> searchUnitDocumentMessages(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of UnitDocumentMessages for query {}", query);
        Page<UnitDocumentMessageDTO> page = unitDocumentMessageService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/unit-document-messages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/unit-document-messages/set-status-approval/{id}")
    @Timed
    public ResponseEntity<UnitDocumentMessageDTO> setStatusApprovalUnitDocumentMessage(@PathVariable Integer id, @RequestBody UnitDocumentMessageDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status Approval Unit Document Message : {}", dto);
        UnitDocumentMessageDTO r = unitDocumentMessageService.changeStatusApprovalUnitDocumentMessage(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }
}
