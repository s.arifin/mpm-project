package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ProductAssociationService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ProductAssociationDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProductAssociation.
 */
@RestController
@RequestMapping("/api")
public class ProductAssociationResource {

    private final Logger log = LoggerFactory.getLogger(ProductAssociationResource.class);

    private static final String ENTITY_NAME = "productAssociation";

    private final ProductAssociationService productAssociationService;

    public ProductAssociationResource(ProductAssociationService productAssociationService) {
        this.productAssociationService = productAssociationService;
    }

    /**
     * POST  /product-associations : Create a new productAssociation.
     *
     * @param productAssociationDTO the productAssociationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productAssociationDTO, or with status 400 (Bad Request) if the productAssociation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-associations")
    @Timed
    public ResponseEntity<ProductAssociationDTO> createProductAssociation(@RequestBody ProductAssociationDTO productAssociationDTO) throws URISyntaxException {
        log.debug("REST request to save ProductAssociation : {}", productAssociationDTO);
        if (productAssociationDTO.getIdProductAssociation() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new productAssociation cannot already have an ID")).body(null);
        }
        ProductAssociationDTO result = productAssociationService.save(productAssociationDTO);
        return ResponseEntity.created(new URI("/api/product-associations/" + result.getIdProductAssociation()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdProductAssociation().toString()))
            .body(result);
    }

    /**
     * POST  /product-associations/execute{id}/{param} : Execute Bussiness Process productAssociation.
     *
     * @param productAssociationDTO the productAssociationDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  productAssociationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-associations/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ProductAssociationDTO> executedProductAssociation(@PathVariable Integer id, @PathVariable String param, @RequestBody ProductAssociationDTO productAssociationDTO) throws URISyntaxException {
        log.debug("REST request to process ProductAssociation : {}", productAssociationDTO);
        ProductAssociationDTO result = productAssociationService.processExecuteData(id, param, productAssociationDTO);
        return new ResponseEntity<ProductAssociationDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /product-associations/execute-list{id}/{param} : Execute Bussiness Process productAssociation.
     *
     * @param productAssociationDTO the productAssociationDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  productAssociationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-associations/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ProductAssociationDTO>> executedListProductAssociation(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ProductAssociationDTO> productAssociationDTO) throws URISyntaxException {
        log.debug("REST request to process List ProductAssociation");
        Set<ProductAssociationDTO> result = productAssociationService.processExecuteListData(id, param, productAssociationDTO);
        return new ResponseEntity<Set<ProductAssociationDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /product-associations : Updates an existing productAssociation.
     *
     * @param productAssociationDTO the productAssociationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productAssociationDTO,
     * or with status 400 (Bad Request) if the productAssociationDTO is not valid,
     * or with status 500 (Internal Server Error) if the productAssociationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/product-associations")
    @Timed
    public ResponseEntity<ProductAssociationDTO> updateProductAssociation(@RequestBody ProductAssociationDTO productAssociationDTO) throws URISyntaxException {
        log.debug("REST request to update ProductAssociation : {}", productAssociationDTO);
        if (productAssociationDTO.getIdProductAssociation() == null) {
            return createProductAssociation(productAssociationDTO);
        }
        ProductAssociationDTO result = productAssociationService.save(productAssociationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, productAssociationDTO.getIdProductAssociation().toString()))
            .body(result);
    }

    /**
     * GET  /product-associations : get all the productAssociations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of productAssociations in body
     */
    @GetMapping("/product-associations")
    @Timed
    public ResponseEntity<List<ProductAssociationDTO>> getAllProductAssociations(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ProductAssociations");
        Page<ProductAssociationDTO> page = productAssociationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-associations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /product-associations/:id : get the "id" productAssociation.
     *
     * @param id the id of the productAssociationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productAssociationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/product-associations/{id}")
    @Timed
    public ResponseEntity<ProductAssociationDTO> getProductAssociation(@PathVariable UUID id) {
        log.debug("REST request to get ProductAssociation : {}", id);
        ProductAssociationDTO productAssociationDTO = productAssociationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(productAssociationDTO));
    }

    /**
     * DELETE  /product-associations/:id : delete the "id" productAssociation.
     *
     * @param id the id of the productAssociationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/product-associations/{id}")
    @Timed
    public ResponseEntity<Void> deleteProductAssociation(@PathVariable UUID id) {
        log.debug("REST request to delete ProductAssociation : {}", id);
        productAssociationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/product-associations?query=:query : search for the productAssociation corresponding
     * to the query.
     *
     * @param query the query of the productAssociation search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/product-associations")
    @Timed
    public ResponseEntity<List<ProductAssociationDTO>> searchProductAssociations(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ProductAssociations for query {}", query);
        Page<ProductAssociationDTO> page = productAssociationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/product-associations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/product-associations/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processProductAssociation(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process ProductAssociation ");
        Map<String, Object> result = productAssociationService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
