package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.DocumentTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.DocumentTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DocumentType.
 */
@RestController
@RequestMapping("/api")
public class DocumentTypeResource {

    private final Logger log = LoggerFactory.getLogger(DocumentTypeResource.class);

    private static final String ENTITY_NAME = "documentType";

    private final DocumentTypeService documentTypeService;

    public DocumentTypeResource(DocumentTypeService documentTypeService) {
        this.documentTypeService = documentTypeService;
    }

    /**
     * POST  /document-types : Create a new documentType.
     *
     * @param documentTypeDTO the documentTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new documentTypeDTO, or with status 400 (Bad Request) if the documentType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/document-types")
    @Timed
    public ResponseEntity<DocumentTypeDTO> createDocumentType(@RequestBody DocumentTypeDTO documentTypeDTO) throws URISyntaxException {
        log.debug("REST request to save DocumentType : {}", documentTypeDTO);
        if (documentTypeDTO.getIdDocumentType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new documentType cannot already have an ID")).body(null);
        }
        DocumentTypeDTO result = documentTypeService.save(documentTypeDTO);
        return ResponseEntity.created(new URI("/api/document-types/" + result.getIdDocumentType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdDocumentType().toString()))
            .body(result);
    }

    /**
     * POST  /document-types/execute{id}/{param} : Execute Bussiness Process documentType.
     *
     * @param documentTypeDTO the documentTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  documentTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/document-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<DocumentTypeDTO> executedDocumentType(@PathVariable Integer id, @PathVariable String param, @RequestBody DocumentTypeDTO documentTypeDTO) throws URISyntaxException {
        log.debug("REST request to process DocumentType : {}", documentTypeDTO);
        DocumentTypeDTO result = documentTypeService.processExecuteData(id, param, documentTypeDTO);
        return new ResponseEntity<DocumentTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /document-types/execute-list{id}/{param} : Execute Bussiness Process documentType.
     *
     * @param documentTypeDTO the documentTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  documentTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/document-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<DocumentTypeDTO>> executedListDocumentType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<DocumentTypeDTO> documentTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List DocumentType");
        Set<DocumentTypeDTO> result = documentTypeService.processExecuteListData(id, param, documentTypeDTO);
        return new ResponseEntity<Set<DocumentTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /document-types : Updates an existing documentType.
     *
     * @param documentTypeDTO the documentTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated documentTypeDTO,
     * or with status 400 (Bad Request) if the documentTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the documentTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/document-types")
    @Timed
    public ResponseEntity<DocumentTypeDTO> updateDocumentType(@RequestBody DocumentTypeDTO documentTypeDTO) throws URISyntaxException {
        log.debug("REST request to update DocumentType : {}", documentTypeDTO);
        if (documentTypeDTO.getIdDocumentType() == null) {
            return createDocumentType(documentTypeDTO);
        }
        DocumentTypeDTO result = documentTypeService.save(documentTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, documentTypeDTO.getIdDocumentType().toString()))
            .body(result);
    }

    @GetMapping("/document-types/by-id-parent/{idparent}")
    @Timed
    public ResponseEntity<List<DocumentTypeDTO>> getAllDocumentTypesByParentId(@PathVariable Integer idparent){
        log.debug("REST request to get Document Type By Id Parent");

        List<DocumentTypeDTO> list = documentTypeService.findAllByIdParent(idparent);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(list, headers, HttpStatus.OK);
    }

    /**
     * GET  /document-types : get all the documentTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of documentTypes in body
     */
    @GetMapping("/document-types")
    @Timed
    public ResponseEntity<List<DocumentTypeDTO>> getAllDocumentTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DocumentTypes");
        Page<DocumentTypeDTO> page = documentTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/document-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /document-types/:id : get the "id" documentType.
     *
     * @param id the id of the documentTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the documentTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/document-types/{id}")
    @Timed
    public ResponseEntity<DocumentTypeDTO> getDocumentType(@PathVariable Integer id) {
        log.debug("REST request to get DocumentType : {}", id);
        DocumentTypeDTO documentTypeDTO = documentTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(documentTypeDTO));
    }

    /**
     * DELETE  /document-types/:id : delete the "id" documentType.
     *
     * @param id the id of the documentTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/document-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteDocumentType(@PathVariable Integer id) {
        log.debug("REST request to delete DocumentType : {}", id);
        documentTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/document-types?query=:query : search for the documentType corresponding
     * to the query.
     *
     * @param query the query of the documentType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/document-types")
    @Timed
    public ResponseEntity<List<DocumentTypeDTO>> searchDocumentTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DocumentTypes for query {}", query);
        Page<DocumentTypeDTO> page = documentTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/document-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
