package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.StockOpnameItemService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.StockOpnameItemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing StockOpnameItem.
 */
@RestController
@RequestMapping("/api")
public class StockOpnameItemResource {

    private final Logger log = LoggerFactory.getLogger(StockOpnameItemResource.class);

    private static final String ENTITY_NAME = "stockOpnameItem";

    private final StockOpnameItemService stockOpnameItemService;

    public StockOpnameItemResource(StockOpnameItemService stockOpnameItemService) {
        this.stockOpnameItemService = stockOpnameItemService;
    }

    /**
     * POST  /stock-opname-items : Create a new stockOpnameItem.
     *
     * @param stockOpnameItemDTO the stockOpnameItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new stockOpnameItemDTO, or with status 400 (Bad Request) if the stockOpnameItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opname-items")
    @Timed
    public ResponseEntity<StockOpnameItemDTO> createStockOpnameItem(@RequestBody StockOpnameItemDTO stockOpnameItemDTO) throws URISyntaxException {
        log.debug("REST request to save StockOpnameItem : {}", stockOpnameItemDTO);
        if (stockOpnameItemDTO.getIdStockopnameItem() != null) {
            throw new BadRequestAlertException("A new stockOpnameItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StockOpnameItemDTO result = stockOpnameItemService.save(stockOpnameItemDTO);
        return ResponseEntity.created(new URI("/api/stock-opname-items/" + result.getIdStockopnameItem()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdStockopnameItem().toString()))
            .body(result);
    }

    /**
     * POST  /stock-opname-items/process : Execute Bussiness Process stockOpnameItem.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opname-items/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processStockOpnameItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process StockOpnameItem ");
        Map<String, Object> result = stockOpnameItemService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /stock-opname-items/execute : Execute Bussiness Process stockOpnameItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  stockOpnameItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opname-items/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedStockOpnameItem(HttpServletRequest request, @RequestBody StockOpnameItemDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process StockOpnameItem");
        Map<String, Object> result = stockOpnameItemService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /stock-opname-items/execute-list : Execute Bussiness Process stockOpnameItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  stockOpnameItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opname-items/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListStockOpnameItem(HttpServletRequest request, @RequestBody Set<StockOpnameItemDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List StockOpnameItem");
        Map<String, Object> result = stockOpnameItemService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /stock-opname-items : Updates an existing stockOpnameItem.
     *
     * @param stockOpnameItemDTO the stockOpnameItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated stockOpnameItemDTO,
     * or with status 400 (Bad Request) if the stockOpnameItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the stockOpnameItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/stock-opname-items")
    @Timed
    public ResponseEntity<StockOpnameItemDTO> updateStockOpnameItem(@RequestBody StockOpnameItemDTO stockOpnameItemDTO) throws URISyntaxException {
        log.debug("REST request to update StockOpnameItem : {}", stockOpnameItemDTO);
        if (stockOpnameItemDTO.getIdStockopnameItem() == null) {
            return createStockOpnameItem(stockOpnameItemDTO);
        }
        StockOpnameItemDTO result = stockOpnameItemService.save(stockOpnameItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, stockOpnameItemDTO.getIdStockopnameItem().toString()))
            .body(result);
    }

    /**
     * GET  /stock-opname-items : get all the stockOpnameItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of stockOpnameItems in body
     */
    @GetMapping("/stock-opname-items")
    @Timed
    public ResponseEntity<List<StockOpnameItemDTO>> getAllStockOpnameItems(Pageable pageable) {
        log.debug("REST request to get a page of StockOpnameItems");
        Page<StockOpnameItemDTO> page = stockOpnameItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/stock-opname-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/stock-opname-items/filterBy")
    @Timed
    public ResponseEntity<List<StockOpnameItemDTO>> getAllFilteredStockOpnameItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of StockOpnameItems");
        Page<StockOpnameItemDTO> page = stockOpnameItemService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/stock-opname-items/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /stock-opname-items/:id : get the "id" stockOpnameItem.
     *
     * @param id the id of the stockOpnameItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the stockOpnameItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/stock-opname-items/{id}")
    @Timed
    public ResponseEntity<StockOpnameItemDTO> getStockOpnameItem(@PathVariable UUID id) {
        log.debug("REST request to get StockOpnameItem : {}", id);
        StockOpnameItemDTO stockOpnameItemDTO = stockOpnameItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(stockOpnameItemDTO));
    }

    /**
     * DELETE  /stock-opname-items/:id : delete the "id" stockOpnameItem.
     *
     * @param id the id of the stockOpnameItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/stock-opname-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteStockOpnameItem(@PathVariable UUID id) {
        log.debug("REST request to delete StockOpnameItem : {}", id);
        stockOpnameItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/stock-opname-items?query=:query : search for the stockOpnameItem corresponding
     * to the query.
     *
     * @param query the query of the stockOpnameItem search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/stock-opname-items")
    @Timed
    public ResponseEntity<List<StockOpnameItemDTO>> searchStockOpnameItems(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of StockOpnameItems for query {}", query);
        Page<StockOpnameItemDTO> page = stockOpnameItemService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/stock-opname-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
