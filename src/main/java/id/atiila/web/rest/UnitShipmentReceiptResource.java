package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.UnitShipmentReceiptService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.UnitShipmentReceiptDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UnitShipmentReceipt.
 */
@RestController
@RequestMapping("/api")
public class UnitShipmentReceiptResource {

    private final Logger log = LoggerFactory.getLogger(UnitShipmentReceiptResource.class);

    private static final String ENTITY_NAME = "unitShipmentReceipt";

    private final UnitShipmentReceiptService unitShipmentReceiptService;

    public UnitShipmentReceiptResource(UnitShipmentReceiptService unitShipmentReceiptService) {
        this.unitShipmentReceiptService = unitShipmentReceiptService;
    }

    /**
     * POST  /unit-shipment-receipts : Create a new unitShipmentReceipt.
     *
     * @param unitShipmentReceiptDTO the unitShipmentReceiptDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new unitShipmentReceiptDTO, or with status 400 (Bad Request) if the unitShipmentReceipt has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-shipment-receipts")
    @Timed
    public ResponseEntity<UnitShipmentReceiptDTO> createUnitShipmentReceipt(@RequestBody UnitShipmentReceiptDTO unitShipmentReceiptDTO) throws URISyntaxException {
        log.debug("REST request to save UnitShipmentReceipt : {}", unitShipmentReceiptDTO);
        if (unitShipmentReceiptDTO.getIdReceipt() != null) {
            throw new BadRequestAlertException("A new unitShipmentReceipt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UnitShipmentReceiptDTO result = unitShipmentReceiptService.save(unitShipmentReceiptDTO);
        return ResponseEntity.created(new URI("/api/unit-shipment-receipts/" + result.getIdReceipt()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdReceipt().toString()))
            .body(result);
    }

    /**
     * POST  /unit-shipment-receipts/process : Execute Bussiness Process unitShipmentReceipt.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-shipment-receipts/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processUnitShipmentReceipt(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process UnitShipmentReceipt ");
        Map<String, Object> result = unitShipmentReceiptService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /unit-shipment-receipts/execute : Execute Bussiness Process unitShipmentReceipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  unitShipmentReceiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-shipment-receipts/execute")
    @Timed
    public ResponseEntity<UnitShipmentReceiptDTO> executedUnitShipmentReceipt(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process UnitShipmentReceipt");
        UnitShipmentReceiptDTO result = unitShipmentReceiptService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<UnitShipmentReceiptDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /unit-shipment-receipts/execute-list : Execute Bussiness Process unitShipmentReceipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  unitShipmentReceiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-shipment-receipts/execute-list")
    @Timed
    public ResponseEntity<Set<UnitShipmentReceiptDTO>> executedListUnitShipmentReceipt(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List UnitShipmentReceipt");
        Set<UnitShipmentReceiptDTO> result = unitShipmentReceiptService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<UnitShipmentReceiptDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /unit-shipment-receipts : Updates an existing unitShipmentReceipt.
     *
     * @param unitShipmentReceiptDTO the unitShipmentReceiptDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated unitShipmentReceiptDTO,
     * or with status 400 (Bad Request) if the unitShipmentReceiptDTO is not valid,
     * or with status 500 (Internal Server Error) if the unitShipmentReceiptDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/unit-shipment-receipts")
    @Timed
    public ResponseEntity<UnitShipmentReceiptDTO> updateUnitShipmentReceipt(@RequestBody UnitShipmentReceiptDTO unitShipmentReceiptDTO) throws URISyntaxException {
        log.debug("REST request to update UnitShipmentReceipt : {}", unitShipmentReceiptDTO);
        if (unitShipmentReceiptDTO.getIdReceipt() == null) {
            return createUnitShipmentReceipt(unitShipmentReceiptDTO);
        }
        UnitShipmentReceiptDTO result = unitShipmentReceiptService.save(unitShipmentReceiptDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, unitShipmentReceiptDTO.getIdReceipt().toString()))
            .body(result);
    }

    /**
     * GET  /unit-shipment-receipts : get all the unitShipmentReceipts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of unitShipmentReceipts in body
     */
    @GetMapping("/unit-shipment-receipts")
    @Timed
    public ResponseEntity<List<UnitShipmentReceiptDTO>> getAllUnitShipmentReceipts(Pageable pageable) {
        log.debug("REST request to get a page of UnitShipmentReceipts");
        Page<UnitShipmentReceiptDTO> page = unitShipmentReceiptService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-shipment-receipts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/unit-shipment-receipts/filterBy")
    @Timed
    public ResponseEntity<List<UnitShipmentReceiptDTO>> getAllFilteredUnitShipmentReceipts(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of UnitShipmentReceipts");
        Page<UnitShipmentReceiptDTO> page = unitShipmentReceiptService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-shipment-receipts/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /unit-shipment-receipts/:id : get the "id" unitShipmentReceipt.
     *
     * @param id the id of the unitShipmentReceiptDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the unitShipmentReceiptDTO, or with status 404 (Not Found)
     */
    @GetMapping("/unit-shipment-receipts/{id}")
    @Timed
    public ResponseEntity<UnitShipmentReceiptDTO> getUnitShipmentReceipt(@PathVariable UUID id) {
        log.debug("REST request to get UnitShipmentReceipt : {}", id);
        UnitShipmentReceiptDTO unitShipmentReceiptDTO = unitShipmentReceiptService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(unitShipmentReceiptDTO));
    }

    /**
     * DELETE  /unit-shipment-receipts/:id : delete the "id" unitShipmentReceipt.
     *
     * @param id the id of the unitShipmentReceiptDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/unit-shipment-receipts/{id}")
    @Timed
    public ResponseEntity<Void> deleteUnitShipmentReceipt(@PathVariable UUID id) {
        log.debug("REST request to delete UnitShipmentReceipt : {}", id);
        unitShipmentReceiptService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/unit-shipment-receipts?query=:query : search for the unitShipmentReceipt corresponding
     * to the query.
     *
     * @param query the query of the unitShipmentReceipt search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/unit-shipment-receipts")
    @Timed
    public ResponseEntity<List<UnitShipmentReceiptDTO>> searchUnitShipmentReceipts(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of UnitShipmentReceipts for query {}", query);
        Page<UnitShipmentReceiptDTO> page = unitShipmentReceiptService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/unit-shipment-receipts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/unit-shipment-receipts/set-status/{id}")
    @Timed
    public ResponseEntity< UnitShipmentReceiptDTO> setStatusUnitShipmentReceipt(@PathVariable Integer id, @RequestBody UnitShipmentReceiptDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status UnitShipmentReceipt : {}", dto);
        UnitShipmentReceiptDTO r = unitShipmentReceiptService.changeUnitShipmentReceiptStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
