package id.atiila.web.rest;

import java.time.ZonedDateTime;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.repository.OrderItemRepository;
import id.atiila.repository.ShipmentOutgoingRepository;
import id.atiila.service.ShipmentOutgoingService;
import id.atiila.service.dto.BillingDTO;
import id.atiila.service.pto.ShipmentOutgoingPTO;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ShipmentOutgoingDTO;
import id.atiila.service.dto.CustomShipmentDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.elasticsearch.search.aggregations.support.format.ValueFormatter;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ShipmentOutgoing.
 */
@RestController
@RequestMapping("/api")
public class ShipmentOutgoingResource {

    private final Logger log = LoggerFactory.getLogger(ShipmentOutgoingResource.class);

    private static final String ENTITY_NAME = "shipmentOutgoing";

    private final ShipmentOutgoingService shipmentOutgoingService;

    @Autowired
    private ShipmentOutgoingRepository repoShipment;

    @Autowired
    private OrderItemRepository orderItemRepository;

    public ShipmentOutgoingResource(ShipmentOutgoingService shipmentOutgoingService) {
        this.shipmentOutgoingService = shipmentOutgoingService;
    }

    /**
     * POST  /shipment-outgoings : Create a new shipmentOutgoing.
     *
     * @param shipmentOutgoingDTO the shipmentOutgoingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shipmentOutgoingDTO, or with status 400 (Bad Request) if the shipmentOutgoing has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-outgoings")
    @Timed
    public ResponseEntity<ShipmentOutgoingDTO> createShipmentOutgoing(@RequestBody ShipmentOutgoingDTO shipmentOutgoingDTO) throws URISyntaxException {
        log.debug("REST request to save ShipmentOutgoing : {}", shipmentOutgoingDTO);
        if (shipmentOutgoingDTO.getIdShipment() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new shipmentOutgoing cannot already have an ID")).body(null);
        }
        ShipmentOutgoingDTO result = shipmentOutgoingService.save(shipmentOutgoingDTO);
        return ResponseEntity.created(new URI("/api/shipment-outgoings/" + result.getIdShipment()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdShipment().toString()))
            .body(result);
    }

    /**
     * POST  /shipment-outgoings/execute{id}/{param} : Execute Bussiness Process shipmentOutgoing.
     *
     * @param shipmentOutgoingDTO the shipmentOutgoingDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  shipmentOutgoingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-outgoings/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ShipmentOutgoingDTO> executedShipmentOutgoing(@PathVariable Integer id, @PathVariable String param, @RequestBody ShipmentOutgoingDTO shipmentOutgoingDTO) throws URISyntaxException {
        log.debug("REST request to process ShipmentOutgoing : {}", shipmentOutgoingDTO);
        ShipmentOutgoingDTO result = shipmentOutgoingService.processExecuteData(id, param, shipmentOutgoingDTO);
        return new ResponseEntity<ShipmentOutgoingDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /shipment-outgoings/execute-list{id}/{param} : Execute Bussiness Process shipmentOutgoing.
     *
     * @param shipmentOutgoingDTO the shipmentOutgoingDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  shipmentOutgoingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-outgoings/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ShipmentOutgoingDTO>> executedListShipmentOutgoing(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ShipmentOutgoingDTO> shipmentOutgoingDTO) throws URISyntaxException {
        log.debug("REST request to process List ShipmentOutgoing");
        Set<ShipmentOutgoingDTO> result = shipmentOutgoingService.processExecuteListData(id, param, shipmentOutgoingDTO);
        return new ResponseEntity<Set<ShipmentOutgoingDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /shipment-outgoings : Updates an existing shipmentOutgoing.
     *
     * @param shipmentOutgoingDTO the shipmentOutgoingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shipmentOutgoingDTO,
     * or with status 400 (Bad Request) if the shipmentOutgoingDTO is not valid,
     * or with status 500 (Internal Server Error) if the shipmentOutgoingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/shipment-outgoings")
    @Timed
    public ResponseEntity<ShipmentOutgoingDTO> updateShipmentOutgoing(@RequestBody ShipmentOutgoingDTO shipmentOutgoingDTO) throws URISyntaxException {
        log.debug("REST request to update ShipmentOutgoing : {}", shipmentOutgoingDTO);
        if (shipmentOutgoingDTO.getIdShipment() == null) {
            return createShipmentOutgoing(shipmentOutgoingDTO);
        }
        ShipmentOutgoingDTO result = shipmentOutgoingService.save(shipmentOutgoingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shipmentOutgoingDTO.getIdShipment().toString()))
            .body(result);
    }

    /**
     * GET  /shipment-outgoings : get all the shipmentOutgoings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shipmentOutgoings in body
     */
//    @GetMapping("/shipment-outgoings")
//    @Timed
//    @Transactional(readOnly = true)
//    public ResponseEntity<List<ShipmentOutgoingDTO>> getAllShipmentOutgoings(@RequestParam(required = false) Set<Integer> idstatustype,@ApiParam Pageable pageable) {
//        log.debug("REST request to get a page of ShipmentOutgoings");
//        //        Integer statusTypeId = Integer.parseInt(idstatustype);
//        Page<ShipmentOutgoingDTO> page = shipmentOutgoingService.findAll(pageable,idstatustype);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-outgoings");
//        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
//    }
//    @GetMapping("/case/checkQuery")
    @GetMapping("/shipment-outgoings")
    @Timed
//    @Transactional(readOnly = true)
    public ResponseEntity<List<CustomShipmentDTO>> getAllShipmentOutgoings(@RequestParam(required = false) Set<Integer> idstatustype,@RequestParam(required = false) String idInternal, @ApiParam Pageable pageable) {
        Page<CustomShipmentDTO> r = orderItemRepository.findActiveShipmentbyStatus(pageable, idstatustype, idInternal);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(r, "/api/shipment-outgoings");
        log.debug("All driver : "+ r);
        return new ResponseEntity<>(r.getContent(), headers, HttpStatus.OK);
//        return new ResponseEntity<>(r.getContent(), null, HttpStatus.OK);
    }

    @GetMapping("/shipment-outgoings/getToday-assign-driver")
    @Timed
    //    @Transactional(readOnly = true)
    public ResponseEntity<List<CustomShipmentDTO>> getTodayAssignDriver(@RequestParam(required = false) Set<Integer> idstatustype, @RequestParam(required = false) String idInternal, @RequestParam(required = false) ZonedDateTime dateAwal, @RequestParam(required = false) ZonedDateTime dateAkhir, @ApiParam Pageable pageable) {
        Page<CustomShipmentDTO> r = orderItemRepository.findActiveShipmentbyStatusAssign(pageable, idstatustype, idInternal, dateAwal, dateAkhir);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(r, "/api/shipment-outgoings");
        log.debug("All today driver : "+ r);
        return new ResponseEntity<>(r.getContent(), headers, HttpStatus.OK);
//        return new ResponseEntity<>(r.getContent(), null, HttpStatus.OK);
    }

    /**
     * GET  /shipment-outgoings/:id : get the "id" shipmentOutgoing.
     *
     * @param id the id of the shipmentOutgoingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shipmentOutgoingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/shipment-outgoings/{id}")
    @Timed
    public ResponseEntity<ShipmentOutgoingDTO> getShipmentOutgoing(@PathVariable UUID id) {
        log.debug("REST request to get ShipmentOutgoing : {}", id);
        ShipmentOutgoingDTO shipmentOutgoingDTO = shipmentOutgoingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(shipmentOutgoingDTO));
    }

    /**
     * DELETE  /shipment-outgoings/:id : delete the "id" shipmentOutgoing.
     *
     * @param id the id of the shipmentOutgoingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shipment-outgoings/{id}")
    @Timed
    public ResponseEntity<Void> deleteShipmentOutgoing(@PathVariable UUID id) {
        log.debug("REST request to delete ShipmentOutgoing : {}", id);
        shipmentOutgoingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/shipment-outgoings?query=:query : search for the shipmentOutgoing corresponding
     * to the query.
     *
     * @param query the query of the shipmentOutgoing search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/shipment-outgoings")
    @Timed
    public ResponseEntity<List<ShipmentOutgoingDTO>> searchShipmentOutgoings(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ShipmentOutgoings for query {}", query);
        Page<ShipmentOutgoingDTO> page = shipmentOutgoingService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/shipment-outgoings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/_search/shipment-outgoings-query")
    @Timed
    public ResponseEntity<List<CustomShipmentDTO>> searchShipmentOutgoingsQuery(@ApiParam ShipmentOutgoingPTO param, @ApiParam Pageable pageable) {
        log.debug("REST request to search "+ param);
        Page<CustomShipmentDTO> page = shipmentOutgoingService.searchQuery(pageable, param);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/_search/shipment-outgoings-query");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/shipment-outgoings/set-status/{id}")
    @Timed
    public ResponseEntity< ShipmentOutgoingDTO> setStatusShipmentOutgoing(@PathVariable Integer id, @RequestBody ShipmentOutgoingDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status ShipmentOutgoing : {}", dto);
        ShipmentOutgoingDTO r = shipmentOutgoingService.changeShipmentOutgoingStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    @GetMapping("/shipment-outgoings/filterBy")
    @Timed
    public ResponseEntity<List<CustomShipmentDTO>> getAllFilteredOrderItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Custom Shipment Orders");
        Page<CustomShipmentDTO> page = shipmentOutgoingService.findCustomFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-outgoings/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/shipment-outgoings/findBilling")
    @Timed
    public ResponseEntity<List<BillingDTO>> getBillingFromOrderItem(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Custom Shipment Orders");
        Page<BillingDTO> page = shipmentOutgoingService.findBillingByOrderItem(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-outgoings/findBilling");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
