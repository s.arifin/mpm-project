package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.WeServiceTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.WeServiceTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WeServiceType.
 */
@RestController
@RequestMapping("/api")
public class WeServiceTypeResource {

    private final Logger log = LoggerFactory.getLogger(WeServiceTypeResource.class);

    private static final String ENTITY_NAME = "weServiceType";

    private final WeServiceTypeService weServiceTypeService;

    public WeServiceTypeResource(WeServiceTypeService weServiceTypeService) {
        this.weServiceTypeService = weServiceTypeService;
    }

    /**
     * POST  /we-service-types : Create a new weServiceType.
     *
     * @param weServiceTypeDTO the weServiceTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new weServiceTypeDTO, or with status 400 (Bad Request) if the weServiceType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/we-service-types")
    @Timed
    public ResponseEntity<WeServiceTypeDTO> createWeServiceType(@RequestBody WeServiceTypeDTO weServiceTypeDTO) throws URISyntaxException {
        log.debug("REST request to save WeServiceType : {}", weServiceTypeDTO);
        if (weServiceTypeDTO.getIdWeType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new weServiceType cannot already have an ID")).body(null);
        }
        WeServiceTypeDTO result = weServiceTypeService.save(weServiceTypeDTO);
        return ResponseEntity.created(new URI("/api/we-service-types/" + result.getIdWeType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdWeType().toString()))
            .body(result);
    }

    /**
     * POST  /we-service-types/execute{id}/{param} : Execute Bussiness Process weServiceType.
     *
     * @param weServiceTypeDTO the weServiceTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  weServiceTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/we-service-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<WeServiceTypeDTO> executedWeServiceType(@PathVariable Integer id, @PathVariable String param, @RequestBody WeServiceTypeDTO weServiceTypeDTO) throws URISyntaxException {
        log.debug("REST request to process WeServiceType : {}", weServiceTypeDTO);
        WeServiceTypeDTO result = weServiceTypeService.processExecuteData(id, param, weServiceTypeDTO);
        return new ResponseEntity<WeServiceTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /we-service-types/execute-list{id}/{param} : Execute Bussiness Process weServiceType.
     *
     * @param weServiceTypeDTO the weServiceTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  weServiceTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/we-service-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<WeServiceTypeDTO>> executedListWeServiceType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<WeServiceTypeDTO> weServiceTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List WeServiceType");
        Set<WeServiceTypeDTO> result = weServiceTypeService.processExecuteListData(id, param, weServiceTypeDTO);
        return new ResponseEntity<Set<WeServiceTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /we-service-types : Updates an existing weServiceType.
     *
     * @param weServiceTypeDTO the weServiceTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated weServiceTypeDTO,
     * or with status 400 (Bad Request) if the weServiceTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the weServiceTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/we-service-types")
    @Timed
    public ResponseEntity<WeServiceTypeDTO> updateWeServiceType(@RequestBody WeServiceTypeDTO weServiceTypeDTO) throws URISyntaxException {
        log.debug("REST request to update WeServiceType : {}", weServiceTypeDTO);
        if (weServiceTypeDTO.getIdWeType() == null) {
            return createWeServiceType(weServiceTypeDTO);
        }
        WeServiceTypeDTO result = weServiceTypeService.save(weServiceTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, weServiceTypeDTO.getIdWeType().toString()))
            .body(result);
    }

    /**
     * GET  /we-service-types : get all the weServiceTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of weServiceTypes in body
     */
    @GetMapping("/we-service-types")
    @Timed
    public ResponseEntity<List<WeServiceTypeDTO>> getAllWeServiceTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of WeServiceTypes");
        Page<WeServiceTypeDTO> page = weServiceTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/we-service-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /we-service-types/:id : get the "id" weServiceType.
     *
     * @param id the id of the weServiceTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the weServiceTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/we-service-types/{id}")
    @Timed
    public ResponseEntity<WeServiceTypeDTO> getWeServiceType(@PathVariable Integer id) {
        log.debug("REST request to get WeServiceType : {}", id);
        WeServiceTypeDTO weServiceTypeDTO = weServiceTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(weServiceTypeDTO));
    }

    /**
     * DELETE  /we-service-types/:id : delete the "id" weServiceType.
     *
     * @param id the id of the weServiceTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/we-service-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteWeServiceType(@PathVariable Integer id) {
        log.debug("REST request to delete WeServiceType : {}", id);
        weServiceTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/we-service-types?query=:query : search for the weServiceType corresponding
     * to the query.
     *
     * @param query the query of the weServiceType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/we-service-types")
    @Timed
    public ResponseEntity<List<WeServiceTypeDTO>> searchWeServiceTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of WeServiceTypes for query {}", query);
        Page<WeServiceTypeDTO> page = weServiceTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/we-service-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/we-service-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrderItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process OrderItem ");
        Map<String, Object> result = weServiceTypeService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
