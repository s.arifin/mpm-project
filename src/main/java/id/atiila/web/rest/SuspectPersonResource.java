package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.SuspectPersonService;
import id.atiila.service.pto.SuspectPTO;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.SuspectPersonDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;


/**
 * REST controller for managing SuspectPerson.
 */
@RestController
@RequestMapping("/api")
public class SuspectPersonResource {

    private final Logger log = LoggerFactory.getLogger(SuspectPersonResource.class);

    private static final String ENTITY_NAME = "suspectPerson";

    private final SuspectPersonService suspectPersonService;

    public SuspectPersonResource(SuspectPersonService suspectPersonService) {
        this.suspectPersonService = suspectPersonService;
    }

    /**
     * POST  /suspect-people : Create a new suspectPerson.
     *
     * @param suspectPersonDTO the suspectPersonDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new suspectPersonDTO, or with status 400 (Bad Request) if the suspectPerson has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/suspect-people")
    @Timed
    public ResponseEntity<SuspectPersonDTO> createSuspectPerson(@RequestBody SuspectPersonDTO suspectPersonDTO) throws URISyntaxException {
        log.debug("REST request to save SuspectPerson : {}", suspectPersonDTO);
        if (suspectPersonDTO.getIdSuspect() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new suspectPerson cannot already have an ID")).body(null);
        }
        SuspectPersonDTO result = suspectPersonService.save(suspectPersonDTO);
        return ResponseEntity.created(new URI("/api/suspect-people/" + result.getIdSuspect()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdSuspect().toString()))
            .body(result);
    }

    /**
     * POST  /suspect-people/execute{id}/{param} : Execute Bussiness Process suspectPerson.
     *
     * @param suspectPersonDTO the suspectPersonDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  suspectPersonDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/suspect-people/execute/{id}/{param}")
    @Timed
    public ResponseEntity<SuspectPersonDTO> executedSuspectPerson(@PathVariable Integer id, @PathVariable String param, @RequestBody SuspectPersonDTO suspectPersonDTO) throws URISyntaxException {
        log.debug("REST request to process SuspectPerson : {}", suspectPersonDTO);
        SuspectPersonDTO result = suspectPersonService.processExecuteData(id, param, suspectPersonDTO);
        return new ResponseEntity<>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /suspect-people/execute-list{id}/{param} : Execute Bussiness Process suspectPerson.
     *
     * @param suspectPersonDTO the suspectPersonDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  suspectPersonDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/suspect-people/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<SuspectPersonDTO>> executedListSuspectPerson(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<SuspectPersonDTO> suspectPersonDTO) throws URISyntaxException {
        log.debug("REST request to process List SuspectPerson");
        Integer suspectIn = suspectPersonDTO.size();
        Set<SuspectPersonDTO> result = suspectPersonService.processExecuteListData(id, param, suspectPersonDTO);
        Integer suspectOut = result.size();
        String summary = suspectOut+" Data berhasil disimpan dan "+(suspectIn - suspectOut)+ " Data gagal diupload";
        return new ResponseEntity<>(result, HeaderUtil.createAlert(summary,null), HttpStatus.OK);
    }

    /**
     * PUT  /suspect-people : Updates an existing suspectPerson.
     *
     * @param suspectPersonDTO the suspectPersonDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated suspectPersonDTO,
     * or with status 400 (Bad Request) if the suspectPersonDTO is not valid,
     * or with status 500 (Internal Server Error) if the suspectPersonDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/suspect-people")
    @Timed
    public ResponseEntity<SuspectPersonDTO> updateSuspectPerson(@RequestBody SuspectPersonDTO suspectPersonDTO) throws URISyntaxException {
        log.debug("REST request to update SuspectPerson : {}", suspectPersonDTO);
        if (suspectPersonDTO.getIdSuspect() == null) {
            return createSuspectPerson(suspectPersonDTO);
        }
        SuspectPersonDTO result = suspectPersonService.save(suspectPersonDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, suspectPersonDTO.getIdSuspect().toString()))
            .body(result);
    }

    /**
     * GET  /suspect-people : get all the suspectPeople.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of suspectPeople in body
     */
    @GetMapping("/suspect-people")
    @Timed
    public ResponseEntity<List<SuspectPersonDTO>> getAllSuspectPeople(@RequestParam String idsuspecttype,
                                                                      @RequestParam String idInternal,
                                                                      @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SuspectPeople");
        Integer suspectTypeId = Integer.parseInt(idsuspecttype);
        Page<SuspectPersonDTO> page = suspectPersonService.findAll(pageable,suspectTypeId, idInternal);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/suspect-people");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/suspect-people/filterBy")
    @Timed
    public ResponseEntity<List<SuspectPersonDTO>> getAllFilteredSuspectPeople(@ApiParam Pageable pageable,
                                                                              @ApiParam SuspectPTO suspectPTO) {
        Page<SuspectPersonDTO> r = suspectPersonService.findFilterBy(pageable, suspectPTO);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(r, "/api/suspect-people/filterBy");
        return new ResponseEntity<>(r.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /suspect-people/:id : get the "id" suspectPerson.
     *
     * @param id the id of the suspectPersonDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the suspectPersonDTO, or with status 404 (Not Found)
     */
    @GetMapping("/suspect-people/{id}")
    @Timed
    public ResponseEntity<SuspectPersonDTO> getSuspectPerson(@PathVariable UUID id) {
        log.debug("REST request to get SuspectPerson : {}", id);
        SuspectPersonDTO suspectPersonDTO = suspectPersonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(suspectPersonDTO));
    }

    /**
     * DELETE  /suspect-people/:id : delete the "id" suspectPerson.
     *
     * @param id the id of the suspectPersonDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/suspect-people/{id}")
    @Timed
    public ResponseEntity<Void> deleteSuspectPerson(@PathVariable UUID id) {
        log.debug("REST request to delete SuspectPerson : {}", id);
        suspectPersonService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/suspect-people?query=:query : search for the suspectPerson corresponding
     * to the query.
     *
     * @param query the query of the suspectPerson search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/suspect-people")
    @Timed
    public ResponseEntity<List<SuspectPersonDTO>> searchSuspectPeople(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SuspectPeople for query {}", query);
        Page<SuspectPersonDTO> page = suspectPersonService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/suspect-people");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/suspect-people/set-status/{id}")
    @Timed
    public ResponseEntity< SuspectPersonDTO> setStatusSuspectPerson(@PathVariable Integer id, @RequestBody SuspectPersonDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status SuspectPerson : {}", dto);
        SuspectPersonDTO r = suspectPersonService.changeSuspectPersonStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
