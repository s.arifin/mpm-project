package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.BookingTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.BookingTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BookingType.
 */
@RestController
@RequestMapping("/api")
public class BookingTypeResource {

    private final Logger log = LoggerFactory.getLogger(BookingTypeResource.class);

    private static final String ENTITY_NAME = "bookingType";

    private final BookingTypeService bookingTypeService;

    public BookingTypeResource(BookingTypeService bookingTypeService) {
        this.bookingTypeService = bookingTypeService;
    }

    /**
     * POST  /booking-types : Create a new bookingType.
     *
     * @param bookingTypeDTO the bookingTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bookingTypeDTO, or with status 400 (Bad Request) if the bookingType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/booking-types")
    @Timed
    public ResponseEntity<BookingTypeDTO> createBookingType(@RequestBody BookingTypeDTO bookingTypeDTO) throws URISyntaxException {
        log.debug("REST request to save BookingType : {}", bookingTypeDTO);
        if (bookingTypeDTO.getIdBookingType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new bookingType cannot already have an ID")).body(null);
        }
        BookingTypeDTO result = bookingTypeService.save(bookingTypeDTO);
        
        return ResponseEntity.created(new URI("/api/booking-types/" + result.getIdBookingType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdBookingType().toString()))
            .body(result);
    }

    /**
     * POST  /booking-types/execute : Execute Bussiness Process bookingType.
     *
     * @param bookingTypeDTO the bookingTypeDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  bookingTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/booking-types/execute/{id}")
    @Timed
    public ResponseEntity<BookingTypeDTO> executedBookingType(@PathVariable Integer id, @RequestBody BookingTypeDTO bookingTypeDTO) throws URISyntaxException {
        log.debug("REST request to process BookingType : {}", bookingTypeDTO);
        BookingTypeDTO r = bookingTypeService.processExecuteData(bookingTypeDTO, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    /**
     * PUT  /booking-types : Updates an existing bookingType.
     *
     * @param bookingTypeDTO the bookingTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bookingTypeDTO,
     * or with status 400 (Bad Request) if the bookingTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the bookingTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/booking-types")
    @Timed
    public ResponseEntity<BookingTypeDTO> updateBookingType(@RequestBody BookingTypeDTO bookingTypeDTO) throws URISyntaxException {
        log.debug("REST request to update BookingType : {}", bookingTypeDTO);
        if (bookingTypeDTO.getIdBookingType() == null) {
            return createBookingType(bookingTypeDTO);
        }
        BookingTypeDTO result = bookingTypeService.save(bookingTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bookingTypeDTO.getIdBookingType().toString()))
            .body(result);
    }

    /**
     * GET  /booking-types : get all the bookingTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of bookingTypes in body
     */
    @GetMapping("/booking-types")
    @Timed
    public ResponseEntity<List<BookingTypeDTO>> getAllBookingTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of BookingTypes");
        Page<BookingTypeDTO> page = bookingTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/booking-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /booking-types/:id : get the "id" bookingType.
     *
     * @param id the id of the bookingTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bookingTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/booking-types/{id}")
    @Timed
    public ResponseEntity<BookingTypeDTO> getBookingType(@PathVariable Integer id) {
        log.debug("REST request to get BookingType : {}", id);
        BookingTypeDTO bookingTypeDTO = bookingTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(bookingTypeDTO));
    }

    /**
     * DELETE  /booking-types/:id : delete the "id" bookingType.
     *
     * @param id the id of the bookingTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/booking-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteBookingType(@PathVariable Integer id) {
        log.debug("REST request to delete BookingType : {}", id);
        bookingTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/booking-types?query=:query : search for the bookingType corresponding
     * to the query.
     *
     * @param query the query of the bookingType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/booking-types")
    @Timed
    public ResponseEntity<List<BookingTypeDTO>> searchBookingTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of BookingTypes for query {}", query);
        Page<BookingTypeDTO> page = bookingTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/booking-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

	
}
