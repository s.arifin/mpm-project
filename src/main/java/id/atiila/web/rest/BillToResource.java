package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.BillToService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.BillToDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BillTo.
 */
@RestController
@RequestMapping("/api")
public class BillToResource {

    private final Logger log = LoggerFactory.getLogger(BillToResource.class);

    private static final String ENTITY_NAME = "billTo";

    private final BillToService billToService;

    public BillToResource(BillToService billToService) {
        this.billToService = billToService;
    }

    /**
     * POST  /bill-tos : Create a new billTo.
     *
     * @param billToDTO the billToDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new billToDTO, or with status 400 (Bad Request) if the billTo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bill-tos")
    @Timed
    public ResponseEntity<BillToDTO> createBillTo(@RequestBody BillToDTO billToDTO) throws URISyntaxException {
        log.debug("REST request to save BillTo : {}", billToDTO);
        if (billToDTO.getIdBillTo() != null) {
            throw new BadRequestAlertException("A new billTo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BillToDTO result = billToService.save(billToDTO);
        return ResponseEntity.created(new URI("/api/bill-tos/" + result.getIdBillTo()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdBillTo().toString()))
            .body(result);
    }

    /**
     * POST  /bill-tos/process : Execute Bussiness Process billTo.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bill-tos/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processBillTo(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process BillTo ");
        Map<String, Object> result = billToService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /bill-tos/execute : Execute Bussiness Process billTo.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  billToDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bill-tos/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedBillTo(HttpServletRequest request, @RequestBody BillToDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process BillTo");
        Map<String, Object> result = billToService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /bill-tos/execute-list : Execute Bussiness Process billTo.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  billToDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bill-tos/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListBillTo(HttpServletRequest request, @RequestBody Set<BillToDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List BillTo");
        Map<String, Object> result = billToService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /bill-tos : Updates an existing billTo.
     *
     * @param billToDTO the billToDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated billToDTO,
     * or with status 400 (Bad Request) if the billToDTO is not valid,
     * or with status 500 (Internal Server Error) if the billToDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/bill-tos")
    @Timed
    public ResponseEntity<BillToDTO> updateBillTo(@RequestBody BillToDTO billToDTO) throws URISyntaxException {
        log.debug("REST request to update BillTo : {}", billToDTO);
        if (billToDTO.getIdBillTo() == null) {
            return createBillTo(billToDTO);
        }
        BillToDTO result = billToService.save(billToDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, billToDTO.getIdBillTo().toString()))
            .body(result);
    }

    /**
     * GET  /bill-tos : get all the billTos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of billTos in body
     */
    @GetMapping("/bill-tos")
    @Timed
    public ResponseEntity<List<BillToDTO>> getAllBillTos(Pageable pageable) {
        log.debug("REST request to get a page of BillTos");
        Page<BillToDTO> page = billToService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bill-tos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/bill-tos/filterBy")
    @Timed
    public ResponseEntity<List<BillToDTO>> getAllFilteredBillTos(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of BillTos");
        Page<BillToDTO> page = billToService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bill-tos/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /bill-tos/:id : get the "id" billTo.
     *
     * @param id the id of the billToDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the billToDTO, or with status 404 (Not Found)
     */
    @GetMapping("/bill-tos/{id}")
    @Timed
    public ResponseEntity<BillToDTO> getBillTo(@PathVariable String id) {
        log.debug("REST request to get BillTo : {}", id);
        BillToDTO billToDTO = billToService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(billToDTO));
    }

    /**
     * DELETE  /bill-tos/:id : delete the "id" billTo.
     *
     * @param id the id of the billToDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/bill-tos/{id}")
    @Timed
    public ResponseEntity<Void> deleteBillTo(@PathVariable String id) {
        log.debug("REST request to delete BillTo : {}", id);
        billToService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/bill-tos?query=:query : search for the billTo corresponding
     * to the query.
     *
     * @param query the query of the billTo search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/bill-tos")
    @Timed
    public ResponseEntity<List<BillToDTO>> searchBillTos(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of BillTos for query {}", query);
        Page<BillToDTO> page = billToService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/bill-tos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
