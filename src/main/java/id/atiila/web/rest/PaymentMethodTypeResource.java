package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PaymentMethodTypeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PaymentMethodTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PaymentMethodType.
 */
@RestController
@RequestMapping("/api")
public class PaymentMethodTypeResource {

    private final Logger log = LoggerFactory.getLogger(PaymentMethodTypeResource.class);

    private static final String ENTITY_NAME = "paymentMethodType";

    private final PaymentMethodTypeService paymentMethodTypeService;

    public PaymentMethodTypeResource(PaymentMethodTypeService paymentMethodTypeService) {
        this.paymentMethodTypeService = paymentMethodTypeService;
    }

    /**
     * POST  /payment-method-types : Create a new paymentMethodType.
     *
     * @param paymentMethodTypeDTO the paymentMethodTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new paymentMethodTypeDTO, or with status 400 (Bad Request) if the paymentMethodType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-method-types")
    @Timed
    public ResponseEntity<PaymentMethodTypeDTO> createPaymentMethodType(@RequestBody PaymentMethodTypeDTO paymentMethodTypeDTO) throws URISyntaxException {
        log.debug("REST request to save PaymentMethodType : {}", paymentMethodTypeDTO);
        if (paymentMethodTypeDTO.getIdPaymentMethodType() != null) {
            throw new BadRequestAlertException("A new paymentMethodType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PaymentMethodTypeDTO result = paymentMethodTypeService.save(paymentMethodTypeDTO);
        return ResponseEntity.created(new URI("/api/payment-method-types/" + result.getIdPaymentMethodType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPaymentMethodType().toString()))
            .body(result);
    }

    /**
     * POST  /payment-method-types/process : Execute Bussiness Process paymentMethodType.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-method-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processPaymentMethodType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PaymentMethodType ");
        Map<String, Object> result = paymentMethodTypeService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /payment-method-types/execute : Execute Bussiness Process paymentMethodType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  paymentMethodTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-method-types/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedPaymentMethodType(HttpServletRequest request, @RequestBody PaymentMethodTypeDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PaymentMethodType");
        Map<String, Object> result = paymentMethodTypeService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /payment-method-types/execute-list : Execute Bussiness Process paymentMethodType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  paymentMethodTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-method-types/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListPaymentMethodType(HttpServletRequest request, @RequestBody Set<PaymentMethodTypeDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List PaymentMethodType");
        Map<String, Object> result = paymentMethodTypeService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /payment-method-types : Updates an existing paymentMethodType.
     *
     * @param paymentMethodTypeDTO the paymentMethodTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated paymentMethodTypeDTO,
     * or with status 400 (Bad Request) if the paymentMethodTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the paymentMethodTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/payment-method-types")
    @Timed
    public ResponseEntity<PaymentMethodTypeDTO> updatePaymentMethodType(@RequestBody PaymentMethodTypeDTO paymentMethodTypeDTO) throws URISyntaxException {
        log.debug("REST request to update PaymentMethodType : {}", paymentMethodTypeDTO);
        if (paymentMethodTypeDTO.getIdPaymentMethodType() == null) {
            return createPaymentMethodType(paymentMethodTypeDTO);
        }
        PaymentMethodTypeDTO result = paymentMethodTypeService.save(paymentMethodTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentMethodTypeDTO.getIdPaymentMethodType().toString()))
            .body(result);
    }

    /**
     * GET  /payment-method-types : get all the paymentMethodTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of paymentMethodTypes in body
     */
    @GetMapping("/payment-method-types")
    @Timed
    public ResponseEntity<List<PaymentMethodTypeDTO>> getAllPaymentMethodTypes(Pageable pageable) {
        log.debug("REST request to get a page of PaymentMethodTypes");
        Page<PaymentMethodTypeDTO> page = paymentMethodTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/payment-method-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/payment-method-types/filterBy")
    @Timed
    public ResponseEntity<List<PaymentMethodTypeDTO>> getAllFilteredPaymentMethodTypes(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of PaymentMethodTypes");
        Page<PaymentMethodTypeDTO> page = paymentMethodTypeService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/payment-method-types/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /payment-method-types/:id : get the "id" paymentMethodType.
     *
     * @param id the id of the paymentMethodTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the paymentMethodTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/payment-method-types/{id}")
    @Timed
    public ResponseEntity<PaymentMethodTypeDTO> getPaymentMethodType(@PathVariable Integer id) {
        log.debug("REST request to get PaymentMethodType : {}", id);
        PaymentMethodTypeDTO paymentMethodTypeDTO = paymentMethodTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentMethodTypeDTO));
    }

    /**
     * DELETE  /payment-method-types/:id : delete the "id" paymentMethodType.
     *
     * @param id the id of the paymentMethodTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/payment-method-types/{id}")
    @Timed
    public ResponseEntity<Void> deletePaymentMethodType(@PathVariable Integer id) {
        log.debug("REST request to delete PaymentMethodType : {}", id);
        paymentMethodTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/payment-method-types?query=:query : search for the paymentMethodType corresponding
     * to the query.
     *
     * @param query the query of the paymentMethodType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/payment-method-types")
    @Timed
    public ResponseEntity<List<PaymentMethodTypeDTO>> searchPaymentMethodTypes(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of PaymentMethodTypes for query {}", query);
        Page<PaymentMethodTypeDTO> page = paymentMethodTypeService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/payment-method-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
