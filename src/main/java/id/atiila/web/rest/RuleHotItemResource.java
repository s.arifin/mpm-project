package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RuleHotItemService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RuleHotItemDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RuleHotItem.
 */
@RestController
@RequestMapping("/api")
public class RuleHotItemResource {

    private final Logger log = LoggerFactory.getLogger(RuleHotItemResource.class);

    private static final String ENTITY_NAME = "ruleHotItem";

    private final RuleHotItemService ruleHotItemService;

    public RuleHotItemResource(RuleHotItemService ruleHotItemService) {
        this.ruleHotItemService = ruleHotItemService;
    }

    @GetMapping("/rule-hot-items/check-by-product-and-internal")
    @Timed
    public ResponseEntity<Boolean> checkRuleHotItemByProduct(@RequestParam String idproduct, @RequestParam String idinternal) {
        log.debug("REST to check rule hoy item by product and internal");
        Boolean r = ruleHotItemService.checkIsHotItemByProduct(idproduct, idinternal);
        return new ResponseEntity<>(r, null, HttpStatus.OK);
    }

    /**
     * POST  /rule-hot-items : Create a new ruleHotItem.
     *
     * @param ruleHotItemDTO the ruleHotItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ruleHotItemDTO, or with status 400 (Bad Request) if the ruleHotItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rule-hot-items")
    @Timed
    public ResponseEntity<RuleHotItemDTO> createRuleHotItem(@RequestBody RuleHotItemDTO ruleHotItemDTO) throws URISyntaxException {
        log.debug("REST request to save RuleHotItem : {}", ruleHotItemDTO);
        if (ruleHotItemDTO.getIdRule() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new ruleHotItem cannot already have an ID")).body(null);
        }
        RuleHotItemDTO result = ruleHotItemService.save(ruleHotItemDTO);
        return ResponseEntity.created(new URI("/api/rule-hot-items/" + result.getIdRule()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRule().toString()))
            .body(result);
    }

    /**
     * PUT  /rule-hot-items : Updates an existing ruleHotItem.
     *
     * @param ruleHotItemDTO the ruleHotItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ruleHotItemDTO,
     * or with status 400 (Bad Request) if the ruleHotItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the ruleHotItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rule-hot-items")
    @Timed
    public ResponseEntity<RuleHotItemDTO> updateRuleHotItem(@RequestBody RuleHotItemDTO ruleHotItemDTO) throws URISyntaxException {
        log.debug("REST request to update RuleHotItem : {}", ruleHotItemDTO);
        if (ruleHotItemDTO.getIdRule() == null) {
            return createRuleHotItem(ruleHotItemDTO);
        }
        RuleHotItemDTO result = ruleHotItemService.save(ruleHotItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ruleHotItemDTO.getIdRule().toString()))
            .body(result);
    }

    @GetMapping("/rule-hot-items/by-product-and-internal")
    @Timed
    public ResponseEntity<List<RuleHotItemDTO>> getAllRuleHotItemsByIdProduct(@RequestParam String idProduct, @RequestParam String idinternal, @ApiParam Pageable pageable){
        log.debug("REST request to get a page of RuleHotItems by product");
        Page<RuleHotItemDTO> page = ruleHotItemService.findAllByIdProduct(idProduct, idinternal, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rule-hot-items/by-product-and-internal");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/rule-hot-items/by-internal")
    @Timed
    public ResponseEntity<List<RuleHotItemDTO>> getAllRuleHotIteimsByInternal(@RequestParam String idinternal ,@ApiParam Pageable pageable){
        log.debug("REST request to get a page of RuleHotItems by internal");
        Page<RuleHotItemDTO> page = ruleHotItemService.findAllByIdInternal(idinternal, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rule-hot-items/by-internal");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rule-hot-items : get all the ruleHotItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of ruleHotItems in body
     */
    @GetMapping("/rule-hot-items")
    @Timed
    public ResponseEntity<List<RuleHotItemDTO>> getAllRuleHotItems(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of RuleHotItems");
        Page<RuleHotItemDTO> page = ruleHotItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rule-hot-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rule-hot-items/:id : get the "id" ruleHotItem.
     *
     * @param id the id of the ruleHotItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ruleHotItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/rule-hot-items/{id}")
    @Timed
    public ResponseEntity<RuleHotItemDTO> getRuleHotItem(@PathVariable Integer id) {
        log.debug("REST request to get RuleHotItem : {}", id);
        RuleHotItemDTO ruleHotItemDTO = ruleHotItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ruleHotItemDTO));
    }

    /**
     * DELETE  /rule-hot-items/:id : delete the "id" ruleHotItem.
     *
     * @param id the id of the ruleHotItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rule-hot-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteRuleHotItem(@PathVariable Integer id) {
        log.debug("REST request to delete RuleHotItem : {}", id);
        ruleHotItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/rule-hot-items?query=:query : search for the ruleHotItem corresponding
     * to the query.
     *
     * @param query the query of the ruleHotItem search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/rule-hot-items")
    @Timed
    public ResponseEntity<List<RuleHotItemDTO>> searchRuleHotItems(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of RuleHotItems for query {}", query);
        Page<RuleHotItemDTO> page = ruleHotItemService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/rule-hot-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
