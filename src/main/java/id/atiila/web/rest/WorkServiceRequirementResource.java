package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.WorkServiceRequirementService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.WorkServiceRequirementDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WorkServiceRequirement.
 */
@RestController
@RequestMapping("/api")
public class WorkServiceRequirementResource {

    private final Logger log = LoggerFactory.getLogger(WorkServiceRequirementResource.class);

    private static final String ENTITY_NAME = "workServiceRequirement";

    private final WorkServiceRequirementService workServiceRequirementService;

    public WorkServiceRequirementResource(WorkServiceRequirementService workServiceRequirementService) {
        this.workServiceRequirementService = workServiceRequirementService;
    }

    /**
     * POST  /work-service-requirements : Create a new workServiceRequirement.
     *
     * @param workServiceRequirementDTO the workServiceRequirementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new workServiceRequirementDTO, or with status 400 (Bad Request) if the workServiceRequirement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/work-service-requirements")
    @Timed
    public ResponseEntity<WorkServiceRequirementDTO> createWorkServiceRequirement(@RequestBody WorkServiceRequirementDTO workServiceRequirementDTO) throws URISyntaxException {
        log.debug("REST request to save WorkServiceRequirement : {}", workServiceRequirementDTO);
        if (workServiceRequirementDTO.getIdWe() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new workServiceRequirement cannot already have an ID")).body(null);
        }
        WorkServiceRequirementDTO result = workServiceRequirementService.save(workServiceRequirementDTO);
        return ResponseEntity.created(new URI("/api/work-service-requirements/" + result.getIdWe()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdWe().toString()))
            .body(result);
    }

    /**
     * POST  /work-service-requirements/execute{id}/{param} : Execute Bussiness Process workServiceRequirement.
     *
     * @param workServiceRequirementDTO the workServiceRequirementDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  workServiceRequirementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/work-service-requirements/execute/{id}/{param}")
    @Timed
    public ResponseEntity<WorkServiceRequirementDTO> executedWorkServiceRequirement(@PathVariable Integer id, @PathVariable String param, @RequestBody WorkServiceRequirementDTO workServiceRequirementDTO) throws URISyntaxException {
        log.debug("REST request to process WorkServiceRequirement : {}", workServiceRequirementDTO);
        WorkServiceRequirementDTO result = workServiceRequirementService.processExecuteData(id, param, workServiceRequirementDTO);
        return new ResponseEntity<WorkServiceRequirementDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /work-service-requirements/execute-list{id}/{param} : Execute Bussiness Process workServiceRequirement.
     *
     * @param workServiceRequirementDTO the workServiceRequirementDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  workServiceRequirementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/work-service-requirements/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<WorkServiceRequirementDTO>> executedListWorkServiceRequirement(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<WorkServiceRequirementDTO> workServiceRequirementDTO) throws URISyntaxException {
        log.debug("REST request to process List WorkServiceRequirement");
        Set<WorkServiceRequirementDTO> result = workServiceRequirementService.processExecuteListData(id, param, workServiceRequirementDTO);
        return new ResponseEntity<Set<WorkServiceRequirementDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /work-service-requirements : Updates an existing workServiceRequirement.
     *
     * @param workServiceRequirementDTO the workServiceRequirementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated workServiceRequirementDTO,
     * or with status 400 (Bad Request) if the workServiceRequirementDTO is not valid,
     * or with status 500 (Internal Server Error) if the workServiceRequirementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/work-service-requirements")
    @Timed
    public ResponseEntity<WorkServiceRequirementDTO> updateWorkServiceRequirement(@RequestBody WorkServiceRequirementDTO workServiceRequirementDTO) throws URISyntaxException {
        log.debug("REST request to update WorkServiceRequirement : {}", workServiceRequirementDTO);
        if (workServiceRequirementDTO.getIdWe() == null) {
            return createWorkServiceRequirement(workServiceRequirementDTO);
        }
        WorkServiceRequirementDTO result = workServiceRequirementService.save(workServiceRequirementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, workServiceRequirementDTO.getIdWe().toString()))
            .body(result);
    }

    /**
     * GET  /work-service-requirements : get all the workServiceRequirements.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of workServiceRequirements in body
     */
    @GetMapping("/work-service-requirements")
    @Timed
    public ResponseEntity<List<WorkServiceRequirementDTO>> getAllWorkServiceRequirements(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of WorkServiceRequirements");
        Page<WorkServiceRequirementDTO> page = workServiceRequirementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/work-service-requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /work-service-requirements/:id : get the "id" workServiceRequirement.
     *
     * @param id the id of the workServiceRequirementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the workServiceRequirementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/work-service-requirements/{id}")
    @Timed
    public ResponseEntity<WorkServiceRequirementDTO> getWorkServiceRequirement(@PathVariable UUID id) {
        log.debug("REST request to get WorkServiceRequirement : {}", id);
        WorkServiceRequirementDTO workServiceRequirementDTO = workServiceRequirementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(workServiceRequirementDTO));
    }

    /**
     * DELETE  /work-service-requirements/:id : delete the "id" workServiceRequirement.
     *
     * @param id the id of the workServiceRequirementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/work-service-requirements/{id}")
    @Timed
    public ResponseEntity<Void> deleteWorkServiceRequirement(@PathVariable UUID id) {
        log.debug("REST request to delete WorkServiceRequirement : {}", id);
        workServiceRequirementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/work-service-requirements?query=:query : search for the workServiceRequirement corresponding
     * to the query.
     *
     * @param query the query of the workServiceRequirement search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/work-service-requirements")
    @Timed
    public ResponseEntity<List<WorkServiceRequirementDTO>> searchWorkServiceRequirements(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of WorkServiceRequirements for query {}", query);
        Page<WorkServiceRequirementDTO> page = workServiceRequirementService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/work-service-requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/work-service-requirements/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrderItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process OrderItem ");
        Map<String, Object> result = workServiceRequirementService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
