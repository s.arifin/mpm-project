package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.DocumentsService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.DocumentsDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Documents.
 */
@RestController
@RequestMapping("/api")
public class DocumentsResource {

    private final Logger log = LoggerFactory.getLogger(DocumentsResource.class);

    private static final String ENTITY_NAME = "documents";

    private final DocumentsService documentsService;

    public DocumentsResource(DocumentsService documentsService) {
        this.documentsService = documentsService;
    }

    /**
     * POST  /documents : Create a new documents.
     *
     * @param documentsDTO the documentsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new documentsDTO, or with status 400 (Bad Request) if the documents has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/documents")
    @Timed
    public ResponseEntity<DocumentsDTO> createDocuments(@RequestBody DocumentsDTO documentsDTO) throws URISyntaxException {
        log.debug("REST request to save Documents : {}", documentsDTO);
        if (documentsDTO.getIdDocument() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new documents cannot already have an ID")).body(null);
        }
        DocumentsDTO result = documentsService.save(documentsDTO);
        return ResponseEntity.created(new URI("/api/documents/" + result.getIdDocument()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdDocument().toString()))
            .body(result);
    }

    /**
     * POST  /documents/execute{id}/{param} : Execute Bussiness Process documents.
     *
     * @param documentsDTO the documentsDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  documentsDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/documents/execute/{id}/{param}")
    @Timed
    public ResponseEntity<DocumentsDTO> executedDocuments(@PathVariable Integer id, @PathVariable String param, @RequestBody DocumentsDTO documentsDTO) throws URISyntaxException {
        log.debug("REST request to process Documents : {}", documentsDTO);
        DocumentsDTO result = documentsService.processExecuteData(id, param, documentsDTO);
        return new ResponseEntity<DocumentsDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /documents/execute-list{id}/{param} : Execute Bussiness Process documents.
     *
     * @param documentsDTO the documentsDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  documentsDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/documents/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<DocumentsDTO>> executedListDocuments(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<DocumentsDTO> documentsDTO) throws URISyntaxException {
        log.debug("REST request to process List Documents");
        Set<DocumentsDTO> result = documentsService.processExecuteListData(id, param, documentsDTO);
        return new ResponseEntity<Set<DocumentsDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /documents : Updates an existing documents.
     *
     * @param documentsDTO the documentsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated documentsDTO,
     * or with status 400 (Bad Request) if the documentsDTO is not valid,
     * or with status 500 (Internal Server Error) if the documentsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/documents")
    @Timed
    public ResponseEntity<DocumentsDTO> updateDocuments(@RequestBody DocumentsDTO documentsDTO) throws URISyntaxException {
        log.debug("REST request to update Documents : {}", documentsDTO);
        if (documentsDTO.getIdDocument() == null) {
            return createDocuments(documentsDTO);
        }
        DocumentsDTO result = documentsService.save(documentsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, documentsDTO.getIdDocument().toString()))
            .body(result);
    }

    /**
     * GET  /documents : get all the documents.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of documents in body
     */
    @GetMapping("/documents")
    @Timed
    public ResponseEntity<List<DocumentsDTO>> getAllDocuments(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Documents");
        Page<DocumentsDTO> page = documentsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/documents");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /documents/:id : get the "id" documents.
     *
     * @param id the id of the documentsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the documentsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/documents/{id}")
    @Timed
    public ResponseEntity<DocumentsDTO> getDocuments(@PathVariable UUID id) {
        log.debug("REST request to get Documents : {}", id);
        DocumentsDTO documentsDTO = documentsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(documentsDTO));
    }

    /**
     * DELETE  /documents/:id : delete the "id" documents.
     *
     * @param id the id of the documentsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/documents/{id}")
    @Timed
    public ResponseEntity<Void> deleteDocuments(@PathVariable UUID id) {
        log.debug("REST request to delete Documents : {}", id);
        documentsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/documents?query=:query : search for the documents corresponding
     * to the query.
     *
     * @param query the query of the documents search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/documents")
    @Timed
    public ResponseEntity<List<DocumentsDTO>> searchDocuments(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Documents for query {}", query);
        Page<DocumentsDTO> page = documentsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/documents");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
