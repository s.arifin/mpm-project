package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.InternalOrderService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.InternalOrderDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing InternalOrder.
 */
@RestController
@RequestMapping("/api")
public class InternalOrderResource {

    private final Logger log = LoggerFactory.getLogger(InternalOrderResource.class);

    private static final String ENTITY_NAME = "internalOrder";

    private final InternalOrderService internalOrderService;

    public InternalOrderResource(InternalOrderService internalOrderService) {
        this.internalOrderService = internalOrderService;
    }

    /**
     * POST  /internal-orders : Create a new internalOrder.
     *
     * @param internalOrderDTO the internalOrderDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new internalOrderDTO, or with status 400 (Bad Request) if the internalOrder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/internal-orders")
    @Timed
    public ResponseEntity<InternalOrderDTO> createInternalOrder(@RequestBody InternalOrderDTO internalOrderDTO) throws URISyntaxException {
        log.debug("REST request to save InternalOrder : {}", internalOrderDTO);
        if (internalOrderDTO.getIdOrder() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new internalOrder cannot already have an ID")).body(null);
        }
        InternalOrderDTO result = internalOrderService.save(internalOrderDTO);
        return ResponseEntity.created(new URI("/api/internal-orders/" + result.getIdOrder()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdOrder().toString()))
            .body(result);
    }

    /**
     * POST  /internal-orders/execute{id}/{param} : Execute Bussiness Process internalOrder.
     *
     * @param internalOrderDTO the internalOrderDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  internalOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/internal-orders/execute/{id}/{param}")
    @Timed
    public ResponseEntity<InternalOrderDTO> executedInternalOrder(@PathVariable Integer id, @PathVariable String param, @RequestBody InternalOrderDTO internalOrderDTO) throws URISyntaxException {
        log.debug("REST request to process InternalOrder : {}", internalOrderDTO);
        InternalOrderDTO result = internalOrderService.processExecuteData(id, param, internalOrderDTO);
        return new ResponseEntity<InternalOrderDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /internal-orders/execute-list{id}/{param} : Execute Bussiness Process internalOrder.
     *
     * @param internalOrderDTO the internalOrderDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  internalOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/internal-orders/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<InternalOrderDTO>> executedListInternalOrder(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<InternalOrderDTO> internalOrderDTO) throws URISyntaxException {
        log.debug("REST request to process List InternalOrder");
        Set<InternalOrderDTO> result = internalOrderService.processExecuteListData(id, param, internalOrderDTO);
        return new ResponseEntity<Set<InternalOrderDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /internal-orders : Updates an existing internalOrder.
     *
     * @param internalOrderDTO the internalOrderDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated internalOrderDTO,
     * or with status 400 (Bad Request) if the internalOrderDTO is not valid,
     * or with status 500 (Internal Server Error) if the internalOrderDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/internal-orders")
    @Timed
    public ResponseEntity<InternalOrderDTO> updateInternalOrder(@RequestBody InternalOrderDTO internalOrderDTO) throws URISyntaxException {
        log.debug("REST request to update InternalOrder : {}", internalOrderDTO);
        if (internalOrderDTO.getIdOrder() == null) {
            return createInternalOrder(internalOrderDTO);
        }
        InternalOrderDTO result = internalOrderService.save(internalOrderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, internalOrderDTO.getIdOrder().toString()))
            .body(result);
    }

    /**
     * GET  /internal-orders : get all the internalOrders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of internalOrders in body
     */
    @GetMapping("/internal-orders")
    @Timed
    public ResponseEntity<List<InternalOrderDTO>> getAllInternalOrders(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of InternalOrders");
        Page<InternalOrderDTO> page = internalOrderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/internal-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /internal-orders/:id : get the "id" internalOrder.
     *
     * @param id the id of the internalOrderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the internalOrderDTO, or with status 404 (Not Found)
     */
    @GetMapping("/internal-orders/{id}")
    @Timed
    public ResponseEntity<InternalOrderDTO> getInternalOrder(@PathVariable UUID id) {
        log.debug("REST request to get InternalOrder : {}", id);
        InternalOrderDTO internalOrderDTO = internalOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(internalOrderDTO));
    }

    /**
     * DELETE  /internal-orders/:id : delete the "id" internalOrder.
     *
     * @param id the id of the internalOrderDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/internal-orders/{id}")
    @Timed
    public ResponseEntity<Void> deleteInternalOrder(@PathVariable UUID id) {
        log.debug("REST request to delete InternalOrder : {}", id);
        internalOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/internal-orders?query=:query : search for the internalOrder corresponding
     * to the query.
     *
     * @param query the query of the internalOrder search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/internal-orders")
    @Timed
    public ResponseEntity<List<InternalOrderDTO>> searchInternalOrders(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of InternalOrders for query {}", query);
        Page<InternalOrderDTO> page = internalOrderService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/internal-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
