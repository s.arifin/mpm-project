package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PositionService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PositionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Position.
 */
@RestController
@RequestMapping("/api")
public class PositionResource {

    private final Logger log = LoggerFactory.getLogger(PositionResource.class);

    private static final String ENTITY_NAME = "position";

    private final PositionService positionService;

    public PositionResource(PositionService positionService) {
        this.positionService = positionService;
    }

    /**
     * POST  /positions : Create a new position.
     *
     * @param positionDTO the positionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new positionDTO, or with status 400 (Bad Request) if the position has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/positions")
    @Timed
    public ResponseEntity<PositionDTO> createPosition(@RequestBody PositionDTO positionDTO) throws URISyntaxException {
        log.debug("REST request to save Position : {}", positionDTO);
        if (positionDTO.getIdPosition() != null) {
            throw new BadRequestAlertException("A new position cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PositionDTO result = positionService.save(positionDTO);
        return ResponseEntity.created(new URI("/api/positions/" + result.getIdPosition()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPosition().toString()))
            .body(result);
    }

    /**
     * POST  /positions/execute{id}/{param} : Execute Bussiness Process position.
     *
     * @param positionDTO the positionDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  positionDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/positions/execute/{id}/{param}")
    @Timed
    public ResponseEntity<PositionDTO> executedPosition(@PathVariable Integer id, @PathVariable String param, @RequestBody PositionDTO positionDTO) throws URISyntaxException {
        log.debug("REST request to process Position : {}", positionDTO);
        PositionDTO result = positionService.processExecuteData(id, param, positionDTO);
        return new ResponseEntity<PositionDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /positions/execute-list{id}/{param} : Execute Bussiness Process position.
     *
     * @param positionDTO the positionDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  positionDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/positions/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<PositionDTO>> executedListPosition(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<PositionDTO> positionDTO) throws URISyntaxException {
        log.debug("REST request to process List Position");
        Set<PositionDTO> result = positionService.processExecuteListData(id, param, positionDTO);
        return new ResponseEntity<Set<PositionDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /positions : Updates an existing position.
     *
     * @param positionDTO the positionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated positionDTO,
     * or with status 400 (Bad Request) if the positionDTO is not valid,
     * or with status 500 (Internal Server Error) if the positionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/positions")
    @Timed
    public ResponseEntity<PositionDTO> updatePosition(@RequestBody PositionDTO positionDTO) throws URISyntaxException {
        log.debug("REST request to update Position : {}", positionDTO);
        if (positionDTO.getIdPosition() == null) {
            return createPosition(positionDTO);
        }
        PositionDTO result = positionService.save(positionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, positionDTO.getIdPosition().toString()))
            .body(result);
    }

    /**
     * GET  /positions : get all the positions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of positions in body
     */
    @GetMapping("/positions")
    @Timed
    public ResponseEntity<List<PositionDTO>> getAllPositions(Pageable pageable) {
        log.debug("REST request to get a page of Positions");
        Page<PositionDTO> page = positionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/positions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /positions/:id : get the "id" position.
     *
     * @param id the id of the positionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the positionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/positions/{id}")
    @Timed
    public ResponseEntity<PositionDTO> getPosition(@PathVariable UUID id) {
        log.debug("REST request to get Position : {}", id);
        PositionDTO positionDTO = positionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(positionDTO));
    }

    /**
     * DELETE  /positions/:id : delete the "id" position.
     *
     * @param id the id of the positionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/positions/{id}")
    @Timed
    public ResponseEntity<Void> deletePosition(@PathVariable UUID id) {
        log.debug("REST request to delete Position : {}", id);
        positionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/positions?query=:query : search for the position corresponding
     * to the query.
     *
     * @param query the query of the position search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/positions")
    @Timed
    public ResponseEntity<List<PositionDTO>> searchPositions(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Positions for query {}", query);
        Page<PositionDTO> page = positionService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/positions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/positions/set-status/{id}")
    @Timed
    public ResponseEntity< PositionDTO> setStatusPosition(@PathVariable Integer id, @RequestBody PositionDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status Position : {}", dto);
        PositionDTO r = positionService.changePositionStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
