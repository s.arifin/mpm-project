package id.atiila.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import id.atiila.service.AssociationTypeService;
import id.atiila.service.dto.AssociationTypeDTO;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

import javax.servlet.http.HttpServletRequest;

/**
 * REST controller for managing AssociationType.
 */
@RestController
@RequestMapping("/api")
public class AssociationTypeResource {

    private final Logger log = LoggerFactory.getLogger(AssociationTypeResource.class);

    private static final String ENTITY_NAME = "associationType";

    private final AssociationTypeService associationTypeService;

    public AssociationTypeResource(AssociationTypeService associationTypeService) {
        this.associationTypeService = associationTypeService;
    }

    /**
     * POST  /association-types : Create a new associationType.
     *
     * @param associationTypeDTO the associationTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new associationTypeDTO, or with status 400 (Bad Request) if the associationType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/association-types")
    @Timed
    public ResponseEntity<AssociationTypeDTO> createAssociationType(@RequestBody AssociationTypeDTO associationTypeDTO) throws URISyntaxException {
        log.debug("REST request to save AssociationType : {}", associationTypeDTO);
        if (associationTypeDTO.getIdAssociattionType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new associationType cannot already have an ID")).body(null);
        }
        AssociationTypeDTO result = associationTypeService.save(associationTypeDTO);
        return ResponseEntity.created(new URI("/api/association-types/" + result.getIdAssociattionType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdAssociattionType().toString()))
            .body(result);
    }

    /**
     * POST  /association-types/execute{id}/{param} : Execute Bussiness Process associationType.
     *
     * @param associationTypeDTO the associationTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  associationTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/association-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<AssociationTypeDTO> executedAssociationType(@PathVariable Integer id, @PathVariable String param, @RequestBody AssociationTypeDTO associationTypeDTO) throws URISyntaxException {
        log.debug("REST request to process AssociationType : {}", associationTypeDTO);
        AssociationTypeDTO result = associationTypeService.processExecuteData(id, param, associationTypeDTO);
        return new ResponseEntity<AssociationTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /association-types/execute-list{id}/{param} : Execute Bussiness Process associationType.
     *
     * @param associationTypeDTO the associationTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  associationTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/association-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<AssociationTypeDTO>> executedListAssociationType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<AssociationTypeDTO> associationTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List AssociationType");
        Set<AssociationTypeDTO> result = associationTypeService.processExecuteListData(id, param, associationTypeDTO);
        return new ResponseEntity<Set<AssociationTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /association-types : Updates an existing associationType.
     *
     * @param associationTypeDTO the associationTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated associationTypeDTO,
     * or with status 400 (Bad Request) if the associationTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the associationTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/association-types")
    @Timed
    public ResponseEntity<AssociationTypeDTO> updateAssociationType(@RequestBody AssociationTypeDTO associationTypeDTO) throws URISyntaxException {
        log.debug("REST request to update AssociationType : {}", associationTypeDTO);
        if (associationTypeDTO.getIdAssociattionType() == null) {
            return createAssociationType(associationTypeDTO);
        }
        AssociationTypeDTO result = associationTypeService.save(associationTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, associationTypeDTO.getIdAssociattionType().toString()))
            .body(result);
    }

    /**
     * GET  /association-types : get all the associationTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of associationTypes in body
     */
    @GetMapping("/association-types")
    @Timed
    public ResponseEntity<List<AssociationTypeDTO>> getAllAssociationTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of AssociationTypes");
        Page<AssociationTypeDTO> page = associationTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/association-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /association-types/:id : get the "id" associationType.
     *
     * @param id the id of the associationTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the associationTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/association-types/{id}")
    @Timed
    public ResponseEntity<AssociationTypeDTO> getAssociationType(@PathVariable Integer id) {
        log.debug("REST request to get AssociationType : {}", id);
        AssociationTypeDTO associationTypeDTO = associationTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(associationTypeDTO));
    }

    /**
     * DELETE  /association-types/:id : delete the "id" associationType.
     *
     * @param id the id of the associationTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/association-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteAssociationType(@PathVariable Integer id) {
        log.debug("REST request to delete AssociationType : {}", id);
        associationTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/association-types?query=:query : search for the associationType corresponding
     * to the query.
     *
     * @param query the query of the associationType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/association-types")
    @Timed
    public ResponseEntity<List<AssociationTypeDTO>> searchAssociationTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of AssociationTypes for query {}", query);
        Page<AssociationTypeDTO> page = associationTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/association-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/association-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processAssociationType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process AssociationType ");
        Map<String, Object> result = associationTypeService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
