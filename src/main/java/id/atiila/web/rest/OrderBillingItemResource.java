package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.OrderBillingItemService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.OrderBillingItemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing OrderBillingItem.
 */
@RestController
@RequestMapping("/api")
public class OrderBillingItemResource {

    private final Logger log = LoggerFactory.getLogger(OrderBillingItemResource.class);

    private static final String ENTITY_NAME = "orderBillingItem";

    private final OrderBillingItemService orderBillingItemService;

    public OrderBillingItemResource(OrderBillingItemService orderBillingItemService) {
        this.orderBillingItemService = orderBillingItemService;
    }

    /**
     * POST  /order-billing-items : Create a new orderBillingItem.
     *
     * @param orderBillingItemDTO the orderBillingItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new orderBillingItemDTO, or with status 400 (Bad Request) if the orderBillingItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-billing-items")
    @Timed
    public ResponseEntity<OrderBillingItemDTO> createOrderBillingItem(@RequestBody OrderBillingItemDTO orderBillingItemDTO) throws URISyntaxException {
        log.debug("REST request to save OrderBillingItem : {}", orderBillingItemDTO);
        if (orderBillingItemDTO.getIdOrderBillingItem() != null) {
            throw new BadRequestAlertException("A new orderBillingItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderBillingItemDTO result = orderBillingItemService.save(orderBillingItemDTO);
        return ResponseEntity.created(new URI("/api/order-billing-items/" + result.getIdOrderBillingItem()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdOrderBillingItem().toString()))
            .body(result);
    }

    /**
     * POST  /order-billing-items/process : Execute Bussiness Process orderBillingItem.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-billing-items/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrderBillingItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process OrderBillingItem ");
        Map<String, Object> result = orderBillingItemService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /order-billing-items/execute : Execute Bussiness Process orderBillingItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  orderBillingItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-billing-items/execute")
    @Timed
    public ResponseEntity<OrderBillingItemDTO> executedOrderBillingItem(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process OrderBillingItem");
        OrderBillingItemDTO result = orderBillingItemService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<OrderBillingItemDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /order-billing-items/execute-list : Execute Bussiness Process orderBillingItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  orderBillingItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-billing-items/execute-list")
    @Timed
    public ResponseEntity<Set<OrderBillingItemDTO>> executedListOrderBillingItem(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List OrderBillingItem");
        Set<OrderBillingItemDTO> result = orderBillingItemService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<OrderBillingItemDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /order-billing-items : Updates an existing orderBillingItem.
     *
     * @param orderBillingItemDTO the orderBillingItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated orderBillingItemDTO,
     * or with status 400 (Bad Request) if the orderBillingItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the orderBillingItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/order-billing-items")
    @Timed
    public ResponseEntity<OrderBillingItemDTO> updateOrderBillingItem(@RequestBody OrderBillingItemDTO orderBillingItemDTO) throws URISyntaxException {
        log.debug("REST request to update OrderBillingItem : {}", orderBillingItemDTO);
        if (orderBillingItemDTO.getIdOrderBillingItem() == null) {
            return createOrderBillingItem(orderBillingItemDTO);
        }
        OrderBillingItemDTO result = orderBillingItemService.save(orderBillingItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, orderBillingItemDTO.getIdOrderBillingItem().toString()))
            .body(result);
    }

    /**
     * GET  /order-billing-items : get all the orderBillingItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of orderBillingItems in body
     */
    @GetMapping("/order-billing-items")
    @Timed
    public ResponseEntity<List<OrderBillingItemDTO>> getAllOrderBillingItems(Pageable pageable) {
        log.debug("REST request to get a page of OrderBillingItems");
        Page<OrderBillingItemDTO> page = orderBillingItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/order-billing-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/order-billing-items/filterBy")
    @Timed
    public ResponseEntity<List<OrderBillingItemDTO>> getAllFilteredOrderBillingItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of OrderBillingItems");
        Page<OrderBillingItemDTO> page = orderBillingItemService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/order-billing-items/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /order-billing-items/:id : get the "id" orderBillingItem.
     *
     * @param id the id of the orderBillingItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the orderBillingItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/order-billing-items/{id}")
    @Timed
    public ResponseEntity<OrderBillingItemDTO> getOrderBillingItem(@PathVariable UUID id) {
        log.debug("REST request to get OrderBillingItem : {}", id);
        OrderBillingItemDTO orderBillingItemDTO = orderBillingItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(orderBillingItemDTO));
    }

    /**
     * DELETE  /order-billing-items/:id : delete the "id" orderBillingItem.
     *
     * @param id the id of the orderBillingItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/order-billing-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteOrderBillingItem(@PathVariable UUID id) {
        log.debug("REST request to delete OrderBillingItem : {}", id);
        orderBillingItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/order-billing-items?query=:query : search for the orderBillingItem corresponding
     * to the query.
     *
     * @param query the query of the orderBillingItem search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/order-billing-items")
    @Timed
    public ResponseEntity<List<OrderBillingItemDTO>> searchOrderBillingItems(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of OrderBillingItems for query {}", query);
        Page<OrderBillingItemDTO> page = orderBillingItemService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/order-billing-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
