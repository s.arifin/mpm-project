package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.domain.SalesUnitRequirement;
import id.atiila.service.SalesUnitRequirementService;
import id.atiila.service.dto.ApprovalDTO;
import id.atiila.service.dto.VehicleSalesOrderDTO;
import id.atiila.service.pto.OrderPTO;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.SalesUnitRequirementDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import id.atiila.service.pto.SalesUnitRequirementPTO;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SalesUnitRequirement.
 */
@RestController
@RequestMapping("/api")
public class SalesUnitRequirementResource {

    private final Logger log = LoggerFactory.getLogger(SalesUnitRequirementResource.class);

    private static final String ENTITY_NAME = "salesUnitRequirement";

    private final SalesUnitRequirementService salesUnitRequirementService;

    public SalesUnitRequirementResource(SalesUnitRequirementService salesUnitRequirementService) {
        this.salesUnitRequirementService = salesUnitRequirementService;
    }

    @GetMapping("/sales-unit-requirements/by-internal-and-request")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> findByInternalAndRequest(
        @RequestParam String idinternal,
        @RequestParam String idrequest,
        Pageable pageable
    ){
        log.debug("REST request to get SalesUnitRequirement by internal and request");

        UUID _idrequest = UUID.fromString(idrequest);
        Page<SalesUnitRequirementDTO> page = salesUnitRequirementService.findAllByIdRequest(idinternal, _idrequest, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,"/api/sales-unit-requirements/by-internal-and-request");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/sales-unit-requirements/by-status-and-approval")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getByStatusAndApproval(
        @RequestParam String idinternal,
        @RequestParam Integer idstatustype,
        @RequestParam Integer idstatusapproval,
        Pageable pageable
    ){
        log.debug("REST request to get SalesUnitRequirement by status and approval");
        Page<SalesUnitRequirementDTO> page = salesUnitRequirementService.findSalesUnitRequirementByStatusApprovalAndStatusType(idinternal, idstatustype, idstatusapproval, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-unit-requirements/by-status-and-approval");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * POST  /sales-unit-requirements : Create a new salesUnitRequirement.
     *
     * @param salesUnitRequirementDTO the salesUnitRequirementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new salesUnitRequirementDTO, or with status 400 (Bad Request) if the salesUnitRequirement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-unit-requirements")
    @Timed
    public ResponseEntity<SalesUnitRequirementDTO> createSalesUnitRequirement(@RequestBody SalesUnitRequirementDTO salesUnitRequirementDTO) throws URISyntaxException {
        log.debug("REST request to save SalesUnitRequirement : {}", salesUnitRequirementDTO);
        if (salesUnitRequirementDTO.getIdRequirement() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new salesUnitRequirement cannot already have an ID")).body(null);
        }
        SalesUnitRequirementDTO result = salesUnitRequirementService.save(salesUnitRequirementDTO);
        return ResponseEntity.created(new URI("/api/sales-unit-requirements/" + result.getIdRequirement()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * POST  /sales-unit-requirements/execute{id}/{param} : Execute Bussiness Process salesUnitRequirement.
     *
     * @param salesUnitRequirementDTO the salesUnitRequirementDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  salesUnitRequirementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-unit-requirements/execute/{id}/{param}")
    @Timed
    public ResponseEntity<SalesUnitRequirementDTO> executedSalesUnitRequirement(@PathVariable Integer id, @PathVariable String param, @RequestBody SalesUnitRequirementDTO salesUnitRequirementDTO) throws URISyntaxException {
        log.debug("REST request to process SalesUnitRequirement : {}", salesUnitRequirementDTO);
        SalesUnitRequirementDTO result = salesUnitRequirementService.processExecuteData(id, param, salesUnitRequirementDTO);
        return new ResponseEntity<SalesUnitRequirementDTO>(result, null, HttpStatus.OK);
    }


    @PostMapping(path = "/sales-unit-requirements/approve/email")
    @Timed
    public ResponseEntity<SalesUnitRequirementDTO> executedSalesUnitRequirementApprovedEmail(HttpServletRequest request, @RequestBody SalesUnitRequirementDTO salesUnitRequirementDTO) throws URISyntaxException {
        log.debug("REST request to process SalesUnitRequirement : {}", salesUnitRequirementDTO);
        SalesUnitRequirementDTO result = salesUnitRequirementService.processExecuteDataApproveEmail(request,salesUnitRequirementDTO);
        return new ResponseEntity<SalesUnitRequirementDTO>(result, null, HttpStatus.OK);
    }

    @PostMapping(path = "/sales-unit-requirements/not-approve/email")
    @Timed
    public ResponseEntity<SalesUnitRequirementDTO> executedSalesUnitRequirementNotApprovedEmail(@RequestBody SalesUnitRequirementDTO salesUnitRequirementDTO) throws URISyntaxException {
        log.debug("REST request to process SalesUnitRequirement : {}", salesUnitRequirementDTO);
        SalesUnitRequirementDTO result = salesUnitRequirementService.processExecuteDataNotApproveEmail(salesUnitRequirementDTO);
        return new ResponseEntity<SalesUnitRequirementDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /sales-unit-requirements/execute-list{id}/{param} :  Execute Bussiness Process salesUnitRequirement.
     *
     * @param salesUnitRequirementDTO the salesUnitRequirementDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  salesUnitRequirementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-unit-requirements/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<SalesUnitRequirementDTO>> executedListSalesUnitRequirement(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<SalesUnitRequirementDTO> salesUnitRequirementDTO) throws URISyntaxException {
        log.debug("REST request to process List SalesUnitRequirement");
        Set<SalesUnitRequirementDTO> result = salesUnitRequirementService.processExecuteListData(id, param, salesUnitRequirementDTO);
        return new ResponseEntity<Set<SalesUnitRequirementDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /sales-unit-requirements : Updates an existing salesUnitRequirement.
     *
     * @param salesUnitRequirementDTO the salesUnitRequirementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated salesUnitRequirementDTO,
     * or with status 400 (Bad Request) if the salesUnitRequirementDTO is not valid,
     * or with status 500 (Internal Server Error) if the salesUnitRequirementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sales-unit-requirements")
    @Timed
    public ResponseEntity<SalesUnitRequirementDTO> updateSalesUnitRequirement(@RequestBody SalesUnitRequirementDTO salesUnitRequirementDTO) throws URISyntaxException {
        log.debug("REST request to update SalesUnitRequirement : {}", salesUnitRequirementDTO);
        if (salesUnitRequirementDTO.getIdRequirement() == null) {
            return createSalesUnitRequirement(salesUnitRequirementDTO);
        }
        SalesUnitRequirementDTO result = salesUnitRequirementService.save(salesUnitRequirementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, salesUnitRequirementDTO.getIdRequirement().toString()))
            .body(result);
    }

    @GetMapping("/sales-unit-requirements/byname")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getAllSalesUnitRequirementFilterBy(@ApiParam SalesUnitRequirementPTO param,
                                                                                            @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SalesUnitRequirement");
        Page<SalesUnitRequirementDTO> page = salesUnitRequirementService.findAllByNameSalesUnitRequirementFilterBy(pageable, param);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-unit-requirements/byname");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/sales-unit-requirements/by-approval-diskon-and-pelanggaran-wilayah-by-prospect")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getSURByStatusApprovalBySales(@RequestParam String idinternal, Integer idstatustype, String idprospect, @ApiParam Pageable pageable){
        log.debug("REST request to get a page of SalesUnitRequirements by discount approval, pelanggaran wilayah by sales");

        UUID _idprospect = UUID.fromString(idprospect);
        Page<SalesUnitRequirementDTO> page = salesUnitRequirementService.findAllByApprovalDIscountAndPelanggaranWilayah(idinternal, idstatustype, _idprospect, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-unit-requirements/by-approval-diskon-and-pelanggaran-wilayah-by-prospect");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/sales-unit-requirements/by-approval-diskon-and-pelanggaran-wilayah-by-sales")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getAllSalesUnitRequirementsByStatusApprovalDiscount(@ApiParam Pageable pageable, @RequestParam String idstatustype, @RequestParam String idinternal){
        log.debug("REST request to get a page of SalesUnitRequirements by discount approval");

        int intIdStatus = Integer.parseInt(idstatustype);
        Page<SalesUnitRequirementDTO> page = salesUnitRequirementService.findAllByApprovalDiscountAndPelanggaranWilayahBySales(idinternal, intIdStatus, pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-unit-requirements/by-approval-diskon-and-pelanggaran-wilayah-by-sales");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/sales-unit-requirements/by-approval-active-for-leasing")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getAllSalesUnitRequirementByApprovalActiveForLeasing(@RequestParam String idleasing, @ApiParam Pageable pageable) {
        log.debug("REST request to get active approval for leasing");

        UUID _idleasing = UUID.fromString(idleasing);
        Page<SalesUnitRequirementDTO> page = salesUnitRequirementService.findByApprovalActiveForLeasing(_idleasing, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-unit-requirement/by-approval-active-for-leasing");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/sales-unit-requirements/waiting-for-credit-approval-by-internal")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getAllApprovalByInternal(@RequestParam String idinternal, @ApiParam Pageable pageable) {
        log.debug("REST request to get waiting for approval for credit leasing");

        Page<SalesUnitRequirementDTO> page = salesUnitRequirementService.findByWaitingApprovalCredit(idinternal, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-unit-requirement/waiting-for-credit-approval-by-internal");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /sales-unit-requirements : get all the salesUnitRequirements.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of salesUnitRequirements in body
     */
    @GetMapping("/sales-unit-requirements")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getAllSalesUnitRequirements(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SalesUnitRequirements");
        Page<SalesUnitRequirementDTO> page = salesUnitRequirementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-unit-requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @GetMapping("/leasing-approval")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getAllLeasingApproval(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SalesUnitRequirements");
        Page<SalesUnitRequirementDTO> page = salesUnitRequirementService.findAllApprovalLeasing(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/leasing-approval");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/leasing-approval/request-attachment")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getAllLeasingApprovalRequestAttachment(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SalesUnitRequirements");
        Page<SalesUnitRequirementDTO> page = salesUnitRequirementService.findAllApprovalLeasingRequestAttachment(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/leasing-approval/request-attachment");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @GetMapping("/sales-unit-requirements/filterBy")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getAllFilteredSalesUnitRequirements(HttpServletRequest request, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SalesUnitRequirements filteredd");
        Page<SalesUnitRequirementDTO> page = salesUnitRequirementService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-unit-requirements/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping(path = "/sales-unit-requirements/email")
    @Timed
    public ResponseEntity<SalesUnitRequirementDTO> getSalesUnitRequirementResetKey(HttpServletRequest request) {
        log.debug("REST request to get SalesUnitRequirement : {}", request);
        SalesUnitRequirementDTO salesUnitRequirementDTO = salesUnitRequirementService.findOneWithResetKey(request);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(salesUnitRequirementDTO));
    }


    /**
     * GET  /sales-unit-requirements/:id : get the "id" salesUnitRequirement.
     *
     * @param id the id of the salesUnitRequirementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the salesUnitRequirementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sales-unit-requirements/{id}")
    @Timed
    public ResponseEntity<SalesUnitRequirementDTO> getSalesUnitRequirement(@PathVariable UUID id) {
        log.debug("REST request to get SalesUnitRequirement : {}", id);
        SalesUnitRequirementDTO salesUnitRequirementDTO = salesUnitRequirementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(salesUnitRequirementDTO));
    }


    /**
     * DELETE  /sales-unit-requirements/:id : delete the "id" salesUnitRequirement.
     *
     * @param id the id of the salesUnitRequirementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sales-unit-requirements/{id}")
    @Timed
    public ResponseEntity<Void> deleteSalesUnitRequirement(@PathVariable UUID id) {
        log.debug("REST request to delete SalesUnitRequirement : {}", id);
        salesUnitRequirementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/sales-unit-requirements?query=:query : search for the salesUnitRequirement corresponding
     * to the query.
     *
     * @param query the query of the salesUnitRequirement search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/sales-unit-requirements")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> searchSalesUnitRequirements(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SalesUnitRequirements for query {}", query);
        Page<SalesUnitRequirementDTO> page = salesUnitRequirementService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/sales-unit-requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/sales-unit-requirements/set-status/{id}")
    @Timed
    public ResponseEntity< SalesUnitRequirementDTO> setStatusSalesUnitRequirement(@PathVariable Integer id, @RequestBody SalesUnitRequirementDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status SalesUnitRequirement : {}", dto);
        SalesUnitRequirementDTO r = salesUnitRequirementService.changeSalesUnitRequirementStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    @GetMapping("/sales-unit-requirements/by-approval-diskon-and-pelanggaran-wilayah-by-korsal")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getSURByStatusApprovalByKorsal(@RequestParam String idinternal, Integer idstatustype, String idprospect, @ApiParam Pageable pageable){
        log.debug("REST request to get a page of SalesUnitRequirements by discount approval, pelanggaran wilayah by sales");

//        UUID _idprospect = UUID.fromString(idprospect);
        Page<SalesUnitRequirementDTO> page = salesUnitRequirementService.findAllByApprovalDIscountAndPelanggaranWilayahByKorsal(idinternal, idstatustype, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-unit-requirements/by-approval-diskon-and-pelanggaran-wilayah-by-korsal");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/sales-unit-requirements/filter-approval-leasing")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getAllFilteredVSO(@ApiParam Pageable pageable,
                                                                        @ApiParam OrderPTO param) {
        Page<SalesUnitRequirementDTO> page = salesUnitRequirementService.findApprovalLeasingFilterBy(pageable, param);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-unit-requirements/filter-approval-leasing");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
