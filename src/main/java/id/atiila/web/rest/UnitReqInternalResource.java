package id.atiila.web.rest;

import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.UnitReqInternalService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.UnitReqInternalDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UnitReqInternal.
 */
@RestController
@RequestMapping("/api")
public class UnitReqInternalResource {

    private final Logger log = LoggerFactory.getLogger(UnitReqInternalResource.class);

    private static final String ENTITY_NAME = "unitReqInternal";

    private final UnitReqInternalService unitReqInternalService;

    public UnitReqInternalResource(UnitReqInternalService unitReqInternalService) {
        this.unitReqInternalService = unitReqInternalService;
    }

    /**
     * POST  /unit-req-internals : Create a new unitReqInternal.
     *
     * @param unitReqInternalDTO the unitReqInternalDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new unitReqInternalDTO, or with status 400 (Bad Request) if the unitReqInternal has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-req-internals")
    @Timed
    public ResponseEntity<UnitReqInternalDTO> createUnitReqInternal(@RequestBody UnitReqInternalDTO unitReqInternalDTO) throws URISyntaxException {
        log.debug("REST request to save UnitReqInternal : {}", unitReqInternalDTO);
        if (unitReqInternalDTO.getIdRequirement() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new unitReqInternal cannot already have an ID")).body(null);
        }
        UnitReqInternalDTO result = unitReqInternalService.save(unitReqInternalDTO);
        
        return ResponseEntity.created(new URI("/api/unit-req-internals/" + result.getIdRequirement()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * POST  /unit-req-internals/execute : Execute Bussiness Process unitReqInternal.
     *
     * @param unitReqInternalDTO the unitReqInternalDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  unitReqInternalDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-req-internals/execute")
    @Timed
    public ResponseEntity<UnitReqInternalDTO> executedUnitReqInternal(@RequestBody UnitReqInternalDTO unitReqInternalDTO) throws URISyntaxException {
        log.debug("REST request to process UnitReqInternal : {}", unitReqInternalDTO);
        return new ResponseEntity<UnitReqInternalDTO>(unitReqInternalDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /unit-req-internals : Updates an existing unitReqInternal.
     *
     * @param unitReqInternalDTO the unitReqInternalDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated unitReqInternalDTO,
     * or with status 400 (Bad Request) if the unitReqInternalDTO is not valid,
     * or with status 500 (Internal Server Error) if the unitReqInternalDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/unit-req-internals")
    @Timed
    public ResponseEntity<UnitReqInternalDTO> updateUnitReqInternal(@RequestBody UnitReqInternalDTO unitReqInternalDTO) throws URISyntaxException {
        log.debug("REST request to update UnitReqInternal : {}", unitReqInternalDTO);
        if (unitReqInternalDTO.getIdRequirement() == null) {
            return createUnitReqInternal(unitReqInternalDTO);
        }
        UnitReqInternalDTO result = unitReqInternalService.save(unitReqInternalDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, unitReqInternalDTO.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * GET  /unit-req-internals : get all the unitReqInternals.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of unitReqInternals in body
     */
    @GetMapping("/unit-req-internals")
    @Timed
    public ResponseEntity<List<UnitReqInternalDTO>> getAllUnitReqInternals(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UnitReqInternals");
        Page<UnitReqInternalDTO> page = unitReqInternalService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-req-internals");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /unit-req-internals/:id : get the "id" unitReqInternal.
     *
     * @param id the id of the unitReqInternalDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the unitReqInternalDTO, or with status 404 (Not Found)
     */
    @GetMapping("/unit-req-internals/{id}")
    @Timed
    public ResponseEntity<UnitReqInternalDTO> getUnitReqInternal(@PathVariable UUID id) {
        log.debug("REST request to get UnitReqInternal : {}", id);
        UnitReqInternalDTO unitReqInternalDTO = unitReqInternalService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(unitReqInternalDTO));
    }

    /**
     * DELETE  /unit-req-internals/:id : delete the "id" unitReqInternal.
     *
     * @param id the id of the unitReqInternalDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/unit-req-internals/{id}")
    @Timed
    public ResponseEntity<Void> deleteUnitReqInternal(@PathVariable UUID id) {
        log.debug("REST request to delete UnitReqInternal : {}", id);
        unitReqInternalService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/unit-req-internals?query=:query : search for the unitReqInternal corresponding
     * to the query.
     *
     * @param query the query of the unitReqInternal search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/unit-req-internals")
    @Timed
    public ResponseEntity<List<UnitReqInternalDTO>> searchUnitReqInternals(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of UnitReqInternals for query {}", query);
        Page<UnitReqInternalDTO> page = unitReqInternalService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/unit-req-internals");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
