package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RequestTypeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RequestTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RequestType.
 */
@RestController
@RequestMapping("/api")
public class RequestTypeResource {

    private final Logger log = LoggerFactory.getLogger(RequestTypeResource.class);

    private static final String ENTITY_NAME = "requestType";

    private final RequestTypeService requestTypeService;

    public RequestTypeResource(RequestTypeService requestTypeService) {
        this.requestTypeService = requestTypeService;
    }

    /**
     * POST  /request-types : Create a new requestType.
     *
     * @param requestTypeDTO the requestTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new requestTypeDTO, or with status 400 (Bad Request) if the requestType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-types")
    @Timed
    public ResponseEntity<RequestTypeDTO> createRequestType(@RequestBody RequestTypeDTO requestTypeDTO) throws URISyntaxException {
        log.debug("REST request to save RequestType : {}", requestTypeDTO);
        if (requestTypeDTO.getIdRequestType() != null) {
            throw new BadRequestAlertException("A new requestType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RequestTypeDTO result = requestTypeService.save(requestTypeDTO);
        return ResponseEntity.created(new URI("/api/request-types/" + result.getIdRequestType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequestType().toString()))
            .body(result);
    }

    /**
     * POST  /request-types/process : Execute Bussiness Process requestType.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processRequestType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RequestType ");
        Map<String, Object> result = requestTypeService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /request-types/execute : Execute Bussiness Process requestType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  requestTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-types/execute")
    @Timed
    public ResponseEntity<RequestTypeDTO> executedRequestType(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RequestType");
        RequestTypeDTO result = requestTypeService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<RequestTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /request-types/execute-list : Execute Bussiness Process requestType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  requestTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-types/execute-list")
    @Timed
    public ResponseEntity<Set<RequestTypeDTO>> executedListRequestType(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List RequestType");
        Set<RequestTypeDTO> result = requestTypeService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<RequestTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /request-types : Updates an existing requestType.
     *
     * @param requestTypeDTO the requestTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated requestTypeDTO,
     * or with status 400 (Bad Request) if the requestTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the requestTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/request-types")
    @Timed
    public ResponseEntity<RequestTypeDTO> updateRequestType(@RequestBody RequestTypeDTO requestTypeDTO) throws URISyntaxException {
        log.debug("REST request to update RequestType : {}", requestTypeDTO);
        if (requestTypeDTO.getIdRequestType() == null) {
            return createRequestType(requestTypeDTO);
        }
        RequestTypeDTO result = requestTypeService.save(requestTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, requestTypeDTO.getIdRequestType().toString()))
            .body(result);
    }

    /**
     * GET  /request-types : get all the requestTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of requestTypes in body
     */
    @GetMapping("/request-types")
    @Timed
    public ResponseEntity<List<RequestTypeDTO>> getAllRequestTypes(Pageable pageable) {
        log.debug("REST request to get a page of RequestTypes");
        Page<RequestTypeDTO> page = requestTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/request-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/request-types/filterBy")
    @Timed
    public ResponseEntity<List<RequestTypeDTO>> getAllFilteredRequestTypes(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of RequestTypes");
        Page<RequestTypeDTO> page = requestTypeService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/request-types/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /request-types/:id : get the "id" requestType.
     *
     * @param id the id of the requestTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the requestTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/request-types/{id}")
    @Timed
    public ResponseEntity<RequestTypeDTO> getRequestType(@PathVariable Integer id) {
        log.debug("REST request to get RequestType : {}", id);
        RequestTypeDTO requestTypeDTO = requestTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(requestTypeDTO));
    }

    /**
     * DELETE  /request-types/:id : delete the "id" requestType.
     *
     * @param id the id of the requestTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/request-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteRequestType(@PathVariable Integer id) {
        log.debug("REST request to delete RequestType : {}", id);
        requestTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/request-types?query=:query : search for the requestType corresponding
     * to the query.
     *
     * @param query the query of the requestType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/request-types")
    @Timed
    public ResponseEntity<List<RequestTypeDTO>> searchRequestTypes(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of RequestTypes for query {}", query);
        Page<RequestTypeDTO> page = requestTypeService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/request-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
