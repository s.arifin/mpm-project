package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.BillingService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.BillingDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Billing.
 */
@RestController
@RequestMapping("/api")
public class BillingResource {

    private final Logger log = LoggerFactory.getLogger(BillingResource.class);

    private static final String ENTITY_NAME = "billing";

    private final BillingService billingService;

    public BillingResource(BillingService billingService) {
        this.billingService = billingService;
    }

    /**
     * POST  /billings : Create a new billing.
     *
     * @param billingDTO the billingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new billingDTO, or with status 400 (Bad Request) if the billing has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billings")
    @Timed
    public ResponseEntity<BillingDTO> createBilling(@RequestBody BillingDTO billingDTO) throws URISyntaxException {
        log.debug("REST request to save Billing : {}", billingDTO);
        if (billingDTO.getIdBilling() != null) {
            throw new BadRequestAlertException("A new billing cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BillingDTO result = billingService.save(billingDTO);
        return ResponseEntity.created(new URI("/api/billings/" + result.getIdBilling()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdBilling().toString()))
            .body(result);
    }

    /**
     * POST  /billings/process : Execute Bussiness Process billing.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billings/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processBilling(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Billing ");
        Map<String, Object> result = billingService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /billings/execute : Execute Bussiness Process billing.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  billingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billings/execute")
    @Timed
    public ResponseEntity<BillingDTO> executedBilling(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Billing");
        BillingDTO result = billingService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<BillingDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /billings/execute-list : Execute Bussiness Process billing.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  billingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billings/execute-list")
    @Timed
    public ResponseEntity<Set<BillingDTO>> executedListBilling(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List Billing");
        Set<BillingDTO> result = billingService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<BillingDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /billings : Updates an existing billing.
     *
     * @param billingDTO the billingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated billingDTO,
     * or with status 400 (Bad Request) if the billingDTO is not valid,
     * or with status 500 (Internal Server Error) if the billingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/billings")
    @Timed
    public ResponseEntity<BillingDTO> updateBilling(@RequestBody BillingDTO billingDTO) throws URISyntaxException {
        log.debug("REST request to update Billing : {}", billingDTO);
        if (billingDTO.getIdBilling() == null) {
            return createBilling(billingDTO);
        }
        BillingDTO result = billingService.save(billingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, billingDTO.getIdBilling().toString()))
            .body(result);
    }

    /**
     * GET  /billings : get all the billings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of billings in body
     */
    @GetMapping("/billings")
    @Timed
    public ResponseEntity<List<BillingDTO>> getAllBillings(Pageable pageable) {
        log.debug("REST request to get a page of Billings");
        Page<BillingDTO> page = billingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/billings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/billings/filterBy")
    @Timed
    public ResponseEntity<List<BillingDTO>> getAllFilteredBillings(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Billings");
        Page<BillingDTO> page = billingService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/billings/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /billings/:id : get the "id" billing.
     *
     * @param id the id of the billingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the billingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/billings/{id}")
    @Timed
    public ResponseEntity<BillingDTO> getBilling(@PathVariable UUID id) {
        log.debug("REST request to get Billing : {}", id);
        BillingDTO billingDTO = billingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(billingDTO));
    }

    /**
     * DELETE  /billings/:id : delete the "id" billing.
     *
     * @param id the id of the billingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/billings/{id}")
    @Timed
    public ResponseEntity<Void> deleteBilling(@PathVariable UUID id) {
        log.debug("REST request to delete Billing : {}", id);
        billingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/billings?query=:query : search for the billing corresponding
     * to the query.
     *
     * @param query the query of the billing search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/billings")
    @Timed
    public ResponseEntity<List<BillingDTO>> searchBillings(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Billings for query {}", query);
        Page<BillingDTO> page = billingService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/billings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/billings/set-status/{id}")
    @Timed
    public ResponseEntity< BillingDTO> setStatusBilling(@PathVariable Integer id, @RequestBody BillingDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status Billing : {}", dto);
        BillingDTO r = billingService.changeBillingStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
