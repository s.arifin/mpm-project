package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PartyCategoryService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PartyCategoryDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PartyCategory.
 */
@RestController
@RequestMapping("/api")
public class PartyCategoryResource {

    private final Logger log = LoggerFactory.getLogger(PartyCategoryResource.class);

    private static final String ENTITY_NAME = "partyCategory";

    private final PartyCategoryService partyCategoryService;

    public PartyCategoryResource(PartyCategoryService partyCategoryService) {
        this.partyCategoryService = partyCategoryService;
    }

    /**
     * POST  /party-categories : Create a new partyCategory.
     *
     * @param partyCategoryDTO the partyCategoryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new partyCategoryDTO, or with status 400 (Bad Request) if the partyCategory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-categories")
    @Timed
    public ResponseEntity<PartyCategoryDTO> createPartyCategory(@RequestBody PartyCategoryDTO partyCategoryDTO) throws URISyntaxException {
        log.debug("REST request to save PartyCategory : {}", partyCategoryDTO);
        if (partyCategoryDTO.getIdCategory() != null) {
            throw new BadRequestAlertException("A new partyCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PartyCategoryDTO result = partyCategoryService.save(partyCategoryDTO);
        return ResponseEntity.created(new URI("/api/party-categories/" + result.getIdCategory()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdCategory().toString()))
            .body(result);
    }

    /**
     * POST  /party-categories/process : Execute Bussiness Process partyCategory.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-categories/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processPartyCategory(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PartyCategory ");
        Map<String, Object> result = partyCategoryService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /party-categories/execute : Execute Bussiness Process partyCategory.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  partyCategoryDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-categories/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedPartyCategory(HttpServletRequest request, @RequestBody PartyCategoryDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PartyCategory");
        Map<String, Object> result = partyCategoryService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /party-categories/execute-list : Execute Bussiness Process partyCategory.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  partyCategoryDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-categories/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListPartyCategory(HttpServletRequest request, @RequestBody Set<PartyCategoryDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List PartyCategory");
        Map<String, Object> result = partyCategoryService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /party-categories : Updates an existing partyCategory.
     *
     * @param partyCategoryDTO the partyCategoryDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated partyCategoryDTO,
     * or with status 400 (Bad Request) if the partyCategoryDTO is not valid,
     * or with status 500 (Internal Server Error) if the partyCategoryDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/party-categories")
    @Timed
    public ResponseEntity<PartyCategoryDTO> updatePartyCategory(@RequestBody PartyCategoryDTO partyCategoryDTO) throws URISyntaxException {
        log.debug("REST request to update PartyCategory : {}", partyCategoryDTO);
        if (partyCategoryDTO.getIdCategory() == null) {
            return createPartyCategory(partyCategoryDTO);
        }
        PartyCategoryDTO result = partyCategoryService.save(partyCategoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, partyCategoryDTO.getIdCategory().toString()))
            .body(result);
    }

    /**
     * GET  /party-categories : get all the partyCategories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of partyCategories in body
     */
    @GetMapping("/party-categories")
    @Timed
    public ResponseEntity<List<PartyCategoryDTO>> getAllPartyCategories(Pageable pageable) {
        log.debug("REST request to get a page of PartyCategories");
        Page<PartyCategoryDTO> page = partyCategoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/party-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/party-categories/filterBy")
    @Timed
    public ResponseEntity<List<PartyCategoryDTO>> getAllFilteredPartyCategories(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of PartyCategories");
        Page<PartyCategoryDTO> page = partyCategoryService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/party-categories/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /party-categories/:id : get the "id" partyCategory.
     *
     * @param id the id of the partyCategoryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the partyCategoryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/party-categories/{id}")
    @Timed
    public ResponseEntity<PartyCategoryDTO> getPartyCategory(@PathVariable Integer id) {
        log.debug("REST request to get PartyCategory : {}", id);
        PartyCategoryDTO partyCategoryDTO = partyCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(partyCategoryDTO));
    }

    /**
     * DELETE  /party-categories/:id : delete the "id" partyCategory.
     *
     * @param id the id of the partyCategoryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/party-categories/{id}")
    @Timed
    public ResponseEntity<Void> deletePartyCategory(@PathVariable Integer id) {
        log.debug("REST request to delete PartyCategory : {}", id);
        partyCategoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/party-categories?query=:query : search for the partyCategory corresponding
     * to the query.
     *
     * @param query the query of the partyCategory search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/party-categories")
    @Timed
    public ResponseEntity<List<PartyCategoryDTO>> searchPartyCategories(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of PartyCategories for query {}", query);
        Page<PartyCategoryDTO> page = partyCategoryService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/party-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
