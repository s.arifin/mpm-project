package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.StockOpnameTypeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.StockOpnameTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing StockOpnameType.
 */
@RestController
@RequestMapping("/api")
public class StockOpnameTypeResource {

    private final Logger log = LoggerFactory.getLogger(StockOpnameTypeResource.class);

    private static final String ENTITY_NAME = "stockOpnameType";

    private final StockOpnameTypeService stockOpnameTypeService;

    public StockOpnameTypeResource(StockOpnameTypeService stockOpnameTypeService) {
        this.stockOpnameTypeService = stockOpnameTypeService;
    }

    /**
     * POST  /stock-opname-types : Create a new stockOpnameType.
     *
     * @param stockOpnameTypeDTO the stockOpnameTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new stockOpnameTypeDTO, or with status 400 (Bad Request) if the stockOpnameType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opname-types")
    @Timed
    public ResponseEntity<StockOpnameTypeDTO> createStockOpnameType(@RequestBody StockOpnameTypeDTO stockOpnameTypeDTO) throws URISyntaxException {
        log.debug("REST request to save StockOpnameType : {}", stockOpnameTypeDTO);
        if (stockOpnameTypeDTO.getIdStockOpnameType() != null) {
            throw new BadRequestAlertException("A new stockOpnameType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StockOpnameTypeDTO result = stockOpnameTypeService.save(stockOpnameTypeDTO);
        return ResponseEntity.created(new URI("/api/stock-opname-types/" + result.getIdStockOpnameType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdStockOpnameType().toString()))
            .body(result);
    }

    /**
     * POST  /stock-opname-types/process : Execute Bussiness Process stockOpnameType.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opname-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processStockOpnameType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process StockOpnameType ");
        Map<String, Object> result = stockOpnameTypeService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /stock-opname-types/execute : Execute Bussiness Process stockOpnameType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  stockOpnameTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opname-types/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedStockOpnameType(HttpServletRequest request, @RequestBody StockOpnameTypeDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process StockOpnameType");
        Map<String, Object> result = stockOpnameTypeService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /stock-opname-types/execute-list : Execute Bussiness Process stockOpnameType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  stockOpnameTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opname-types/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListStockOpnameType(HttpServletRequest request, @RequestBody Set<StockOpnameTypeDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List StockOpnameType");
        Map<String, Object> result = stockOpnameTypeService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /stock-opname-types : Updates an existing stockOpnameType.
     *
     * @param stockOpnameTypeDTO the stockOpnameTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated stockOpnameTypeDTO,
     * or with status 400 (Bad Request) if the stockOpnameTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the stockOpnameTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/stock-opname-types")
    @Timed
    public ResponseEntity<StockOpnameTypeDTO> updateStockOpnameType(@RequestBody StockOpnameTypeDTO stockOpnameTypeDTO) throws URISyntaxException {
        log.debug("REST request to update StockOpnameType : {}", stockOpnameTypeDTO);
        if (stockOpnameTypeDTO.getIdStockOpnameType() == null) {
            return createStockOpnameType(stockOpnameTypeDTO);
        }
        StockOpnameTypeDTO result = stockOpnameTypeService.save(stockOpnameTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, stockOpnameTypeDTO.getIdStockOpnameType().toString()))
            .body(result);
    }

    /**
     * GET  /stock-opname-types : get all the stockOpnameTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of stockOpnameTypes in body
     */
    @GetMapping("/stock-opname-types")
    @Timed
    public ResponseEntity<List<StockOpnameTypeDTO>> getAllStockOpnameTypes(Pageable pageable) {
        log.debug("REST request to get a page of StockOpnameTypes");
        Page<StockOpnameTypeDTO> page = stockOpnameTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/stock-opname-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/stock-opname-types/filterBy")
    @Timed
    public ResponseEntity<List<StockOpnameTypeDTO>> getAllFilteredStockOpnameTypes(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of StockOpnameTypes");
        Page<StockOpnameTypeDTO> page = stockOpnameTypeService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/stock-opname-types/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /stock-opname-types/:id : get the "id" stockOpnameType.
     *
     * @param id the id of the stockOpnameTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the stockOpnameTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/stock-opname-types/{id}")
    @Timed
    public ResponseEntity<StockOpnameTypeDTO> getStockOpnameType(@PathVariable Integer id) {
        log.debug("REST request to get StockOpnameType : {}", id);
        StockOpnameTypeDTO stockOpnameTypeDTO = stockOpnameTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(stockOpnameTypeDTO));
    }

    /**
     * DELETE  /stock-opname-types/:id : delete the "id" stockOpnameType.
     *
     * @param id the id of the stockOpnameTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/stock-opname-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteStockOpnameType(@PathVariable Integer id) {
        log.debug("REST request to delete StockOpnameType : {}", id);
        stockOpnameTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/stock-opname-types?query=:query : search for the stockOpnameType corresponding
     * to the query.
     *
     * @param query the query of the stockOpnameType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/stock-opname-types")
    @Timed
    public ResponseEntity<List<StockOpnameTypeDTO>> searchStockOpnameTypes(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of StockOpnameTypes for query {}", query);
        Page<StockOpnameTypeDTO> page = stockOpnameTypeService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/stock-opname-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
