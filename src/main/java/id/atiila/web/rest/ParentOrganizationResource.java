package id.atiila.web.rest;

import java.util.Map;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ParentOrganizationService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ParentOrganizationDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ParentOrganization.
 */
@RestController
@RequestMapping("/api")
public class ParentOrganizationResource {

    private final Logger log = LoggerFactory.getLogger(ParentOrganizationResource.class);

    private static final String ENTITY_NAME = "parentOrganization";

    private final ParentOrganizationService parentOrganizationService;

    public ParentOrganizationResource(ParentOrganizationService parentOrganizationService) {
        this.parentOrganizationService = parentOrganizationService;
    }

    /**
     * POST  /parent-organizations : Create a new parentOrganization.
     *
     * @param parentOrganizationDTO the parentOrganizationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new parentOrganizationDTO, or with status 400 (Bad Request) if the parentOrganization has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/parent-organizations")
    @Timed
    public ResponseEntity<ParentOrganizationDTO> createParentOrganization(@RequestBody ParentOrganizationDTO parentOrganizationDTO) throws URISyntaxException {
        log.debug("REST request to save ParentOrganization : {}", parentOrganizationDTO);
        if (parentOrganizationDTO.getIdInternal() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new parentOrganization cannot already have an ID")).body(null);
        }
        ParentOrganizationDTO result = parentOrganizationService.save(parentOrganizationDTO);

        return ResponseEntity.created(new URI("/api/parent-organizations/" + result.getIdInternal()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdInternal()))
            .body(result);
    }

    /**
     * POST  /parent-organizations/execute : Execute Bussiness Process parentOrganization.
     *
     * @param parentOrganizationDTO the parentOrganizationDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  parentOrganizationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/parent-organizations/execute/{id}")
    @Timed
    public ResponseEntity<ParentOrganizationDTO> executedParentOrganization(@PathVariable Integer id, @RequestBody ParentOrganizationDTO parentOrganizationDTO) throws URISyntaxException {
        log.debug("REST request to process ParentOrganization : {}", parentOrganizationDTO);
        ParentOrganizationDTO r = parentOrganizationService.processExecuteData(parentOrganizationDTO, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    /**
     * PUT  /parent-organizations : Updates an existing parentOrganization.
     *
     * @param parentOrganizationDTO the parentOrganizationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated parentOrganizationDTO,
     * or with status 400 (Bad Request) if the parentOrganizationDTO is not valid,
     * or with status 500 (Internal Server Error) if the parentOrganizationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/parent-organizations")
    @Timed
    public ResponseEntity<ParentOrganizationDTO> updateParentOrganization(@RequestBody ParentOrganizationDTO parentOrganizationDTO) throws URISyntaxException {
        log.debug("REST request to update ParentOrganization : {}", parentOrganizationDTO);
        if (parentOrganizationDTO.getIdInternal() == null) {
            return createParentOrganization(parentOrganizationDTO);
        }
        ParentOrganizationDTO result = parentOrganizationService.save(parentOrganizationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, parentOrganizationDTO.getIdInternal()))
            .body(result);
    }

    /**
     * GET  /parent-organizations : get all the parentOrganizations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of parentOrganizations in body
     */
    @GetMapping("/parent-organizations")
    @Timed
    public ResponseEntity<List<ParentOrganizationDTO>> getAllParentOrganizations(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ParentOrganizations");
        Page<ParentOrganizationDTO> page = parentOrganizationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/parent-organizations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /parent-organizations/:id : get the "id" parentOrganization.
     *
     * @param id the id of the parentOrganizationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the parentOrganizationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/parent-organizations/{id}")
    @Timed
    public ResponseEntity<ParentOrganizationDTO> getParentOrganization(@PathVariable String id) {
        log.debug("REST request to get ParentOrganization : {}", id);
        ParentOrganizationDTO parentOrganizationDTO = parentOrganizationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(parentOrganizationDTO));
    }

    /**
     * DELETE  /parent-organizations/:id : delete the "id" parentOrganization.
     *
     * @param id the id of the parentOrganizationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/parent-organizations/{id}")
    @Timed
    public ResponseEntity<Void> deleteParentOrganization(@PathVariable String id) {
        log.debug("REST request to delete ParentOrganization : {}", id);
        parentOrganizationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/parent-organizations?query=:query : search for the parentOrganization corresponding
     * to the query.
     *
     * @param query the query of the parentOrganization search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/parent-organizations")
    @Timed
    public ResponseEntity<List<ParentOrganizationDTO>> searchParentOrganizations(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ParentOrganizations for query {}", query);
        Page<ParentOrganizationDTO> page = parentOrganizationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/parent-organizations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/parent-organizations/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processParentOrganization(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process ParentOrganization ");
        Map<String, Object> result = parentOrganizationService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
