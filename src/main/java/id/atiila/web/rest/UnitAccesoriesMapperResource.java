package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.UnitAccesoriesMapperService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.UnitAccesoriesMapperDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UnitAccesoriesMapper.
 */
@RestController
@RequestMapping("/api")
public class UnitAccesoriesMapperResource {

    private final Logger log = LoggerFactory.getLogger(UnitAccesoriesMapperResource.class);

    private static final String ENTITY_NAME = "unitAccesoriesMapper";

    private final UnitAccesoriesMapperService unitAccesoriesMapperService;

    public UnitAccesoriesMapperResource(UnitAccesoriesMapperService unitAccesoriesMapperService) {
        this.unitAccesoriesMapperService = unitAccesoriesMapperService;
    }

    /**
     * POST  /unit-accesories-mappers : Create a new unitAccesoriesMapper.
     *
     * @param unitAccesoriesMapperDTO the unitAccesoriesMapperDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new unitAccesoriesMapperDTO, or with status 400 (Bad Request) if the unitAccesoriesMapper has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-accesories-mappers")
    @Timed
    public ResponseEntity<UnitAccesoriesMapperDTO> createUnitAccesoriesMapper(@RequestBody UnitAccesoriesMapperDTO unitAccesoriesMapperDTO) throws URISyntaxException {
        log.debug("REST request to save UnitAccesoriesMapper : {}", unitAccesoriesMapperDTO);
        if (unitAccesoriesMapperDTO.getIdUnitAccMapper() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new unitAccesoriesMapper cannot already have an ID")).body(null);
        }
        UnitAccesoriesMapperDTO result = unitAccesoriesMapperService.save(unitAccesoriesMapperDTO);
        return ResponseEntity.created(new URI("/api/unit-accesories-mappers/" + result.getIdUnitAccMapper()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdUnitAccMapper().toString()))
            .body(result);
    }

    /**
     * POST  /unit-accesories-mappers/execute{id}/{param} : Execute Bussiness Process unitAccesoriesMapper.
     *
     * @param unitAccesoriesMapperDTO the unitAccesoriesMapperDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  unitAccesoriesMapperDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-accesories-mappers/execute/{id}/{param}")
    @Timed
    public ResponseEntity<UnitAccesoriesMapperDTO> executedUnitAccesoriesMapper(@PathVariable Integer id, @PathVariable String param, @RequestBody UnitAccesoriesMapperDTO unitAccesoriesMapperDTO) throws URISyntaxException {
        log.debug("REST request to process UnitAccesoriesMapper : {}", unitAccesoriesMapperDTO);
        UnitAccesoriesMapperDTO result = unitAccesoriesMapperService.processExecuteData(id, param, unitAccesoriesMapperDTO);
        return new ResponseEntity<UnitAccesoriesMapperDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /unit-accesories-mappers/execute-list{id}/{param} : Execute Bussiness Process unitAccesoriesMapper.
     *
     * @param unitAccesoriesMapperDTO the unitAccesoriesMapperDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  unitAccesoriesMapperDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-accesories-mappers/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<UnitAccesoriesMapperDTO>> executedListUnitAccesoriesMapper(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<UnitAccesoriesMapperDTO> unitAccesoriesMapperDTO) throws URISyntaxException {
        log.debug("REST request to process List UnitAccesoriesMapper");
        Set<UnitAccesoriesMapperDTO> result = unitAccesoriesMapperService.processExecuteListData(id, param, unitAccesoriesMapperDTO);
        return new ResponseEntity<Set<UnitAccesoriesMapperDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /unit-accesories-mappers : Updates an existing unitAccesoriesMapper.
     *
     * @param unitAccesoriesMapperDTO the unitAccesoriesMapperDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated unitAccesoriesMapperDTO,
     * or with status 400 (Bad Request) if the unitAccesoriesMapperDTO is not valid,
     * or with status 500 (Internal Server Error) if the unitAccesoriesMapperDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/unit-accesories-mappers")
    @Timed
    public ResponseEntity<UnitAccesoriesMapperDTO> updateUnitAccesoriesMapper(@RequestBody UnitAccesoriesMapperDTO unitAccesoriesMapperDTO) throws URISyntaxException {
        log.debug("REST request to update UnitAccesoriesMapper : {}", unitAccesoriesMapperDTO);
        if (unitAccesoriesMapperDTO.getIdUnitAccMapper() == null) {
            return createUnitAccesoriesMapper(unitAccesoriesMapperDTO);
        }
        UnitAccesoriesMapperDTO result = unitAccesoriesMapperService.save(unitAccesoriesMapperDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, unitAccesoriesMapperDTO.getIdUnitAccMapper().toString()))
            .body(result);
    }

    /**
     * GET  /unit-accesories-mappers : get all the unitAccesoriesMappers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of unitAccesoriesMappers in body
     */
    @GetMapping("/unit-accesories-mappers")
    @Timed
    public ResponseEntity<List<UnitAccesoriesMapperDTO>> getAllUnitAccesoriesMappers(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UnitAccesoriesMappers");
        Page<UnitAccesoriesMapperDTO> page = unitAccesoriesMapperService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-accesories-mappers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /unit-accesories-mappers/:id : get the "id" unitAccesoriesMapper.
     *
     * @param id the id of the unitAccesoriesMapperDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the unitAccesoriesMapperDTO, or with status 404 (Not Found)
     */
    @GetMapping("/unit-accesories-mappers/{id}")
    @Timed
    public ResponseEntity<UnitAccesoriesMapperDTO> getUnitAccesoriesMapper(@PathVariable UUID id) {
        log.debug("REST request to get UnitAccesoriesMapper : {}", id);
        UnitAccesoriesMapperDTO unitAccesoriesMapperDTO = unitAccesoriesMapperService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(unitAccesoriesMapperDTO));
    }

    /**
     * DELETE  /unit-accesories-mappers/:id : delete the "id" unitAccesoriesMapper.
     *
     * @param id the id of the unitAccesoriesMapperDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/unit-accesories-mappers/{id}")
    @Timed
    public ResponseEntity<Void> deleteUnitAccesoriesMapper(@PathVariable UUID id) {
        log.debug("REST request to delete UnitAccesoriesMapper : {}", id);
        unitAccesoriesMapperService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/unit-accesories-mappers?query=:query : search for the unitAccesoriesMapper corresponding
     * to the query.
     *
     * @param query the query of the unitAccesoriesMapper search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/unit-accesories-mappers")
    @Timed
    public ResponseEntity<List<UnitAccesoriesMapperDTO>> searchUnitAccesoriesMappers(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of UnitAccesoriesMappers for query {}", query);
        Page<UnitAccesoriesMapperDTO> page = unitAccesoriesMapperService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/unit-accesories-mappers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
