package id.atiila.web.rest;

import id.atiila.config.ApplicationProperties;
import id.atiila.service.AuditEventService;
import id.atiila.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

/**
 * REST controller for getting the audit events.
 */
@RestController
@RequestMapping("/api")
public class ConfigInfoResources {

    @Autowired
    private ApplicationProperties properties;

    @GetMapping("/config-info")
    public ResponseEntity<ApplicationProperties> getAll(Pageable pageable) {
        return new ResponseEntity<>(properties, null, HttpStatus.OK);
    }

}
