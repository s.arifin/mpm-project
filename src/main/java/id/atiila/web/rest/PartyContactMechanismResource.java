package id.atiila.web.rest;

import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PartyContactMechanismService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PartyContactMechanismDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PartyContactMechanism.
 */
@RestController
@RequestMapping("/api")
public class PartyContactMechanismResource {

    private final Logger log = LoggerFactory.getLogger(PartyContactMechanismResource.class);

    private static final String ENTITY_NAME = "PartyContactMechanism";

    private final PartyContactMechanismService PartyContactMechanismService;

    public PartyContactMechanismResource(PartyContactMechanismService PartyContactMechanismService) {
        this.PartyContactMechanismService = PartyContactMechanismService;
    }

    /**
     * POST  /contact-mechanism-purposes : Create a new PartyContactMechanism.
     *
     * @param PartyContactMechanismDTO the PartyContactMechanismDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new PartyContactMechanismDTO, or with status 400 (Bad Request) if the PartyContactMechanism has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-contact-mechanism-purposes")
    @Timed
    public ResponseEntity<PartyContactMechanismDTO> createPartyContactMechanism(@RequestBody PartyContactMechanismDTO PartyContactMechanismDTO) throws URISyntaxException {
        log.debug("REST request to save PartyContactMechanism : {}", PartyContactMechanismDTO);
        if (PartyContactMechanismDTO.getIdPartyContact() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new PartyContactMechanism cannot already have an ID")).body(null);
        }
        PartyContactMechanismDTO result = PartyContactMechanismService.save(PartyContactMechanismDTO);

        return ResponseEntity.created(new URI("/api/contact-mechanism-purposes/" + result.getIdPartyContact()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPartyContact().toString()))
            .body(result);
    }

    /**
     * POST  /contact-mechanism-purposes/execute : Execute Bussiness Process PartyContactMechanism.
     *
     * @param PartyContactMechanismDTO the PartyContactMechanismDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  PartyContactMechanismDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-contact-mechanism-purposes/execute")
    @Timed
    public ResponseEntity<PartyContactMechanismDTO> executedPartyContactMechanism(@RequestBody PartyContactMechanismDTO PartyContactMechanismDTO) throws URISyntaxException {
        log.debug("REST request to process PartyContactMechanism : {}", PartyContactMechanismDTO);
        return new ResponseEntity<PartyContactMechanismDTO>(PartyContactMechanismDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /contact-mechanism-purposes : Updates an existing PartyContactMechanism.
     *
     * @param PartyContactMechanismDTO the PartyContactMechanismDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated PartyContactMechanismDTO,
     * or with status 400 (Bad Request) if the PartyContactMechanismDTO is not valid,
     * or with status 500 (Internal Server Error) if the PartyContactMechanismDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/party-contact-mechanism-purposes")
    @Timed
    public ResponseEntity<PartyContactMechanismDTO> updatePartyContactMechanism(@RequestBody PartyContactMechanismDTO PartyContactMechanismDTO) throws URISyntaxException {
        log.debug("REST request to update PartyContactMechanism : {}", PartyContactMechanismDTO);
        if (PartyContactMechanismDTO.getIdPartyContact() == null) {
            return createPartyContactMechanism(PartyContactMechanismDTO);
        }
        PartyContactMechanismDTO result = PartyContactMechanismService.save(PartyContactMechanismDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, PartyContactMechanismDTO.getIdPartyContact().toString()))
            .body(result);
    }

    /**
     * GET  /contact-mechanism-purposes : get all the PartyContactMechanisms.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of PartyContactMechanisms in body
     */
    @GetMapping("/party-contact-mechanism-purposes")
    @Timed
    public ResponseEntity<List<PartyContactMechanismDTO>> getAllPartyContactMechanisms(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PartyContactMechanisms");
        Page<PartyContactMechanismDTO> page = PartyContactMechanismService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/contact-mechanism-purposes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @GetMapping("/party-contact-mechanism-purposes/filter-by")
    @Timed
    public ResponseEntity<List<PartyContactMechanismDTO>> getAllFilteredPartyContactMechanisms(@RequestParam String filterBy, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PartyContactMechanisms");
        Page<PartyContactMechanismDTO> page = PartyContactMechanismService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/contact-mechanism-purposes/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * GET  /contact-mechanism-purposes/:id : get the "id" PartyContactMechanism.
     *
     * @param id the id of the PartyContactMechanismDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the PartyContactMechanismDTO, or with status 404 (Not Found)
     */
    @GetMapping("/party-contact-mechanism-purposes/{id}")
    @Timed
    public ResponseEntity<PartyContactMechanismDTO> getPartyContactMechanism(@PathVariable Long id) {
        log.debug("REST request to get PartyContactMechanism : {}", id);
        PartyContactMechanismDTO PartyContactMechanismDTO = PartyContactMechanismService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(PartyContactMechanismDTO));
    }

    /**
     * DELETE  /contact-mechanism-purposes/:id : delete the "id" PartyContactMechanism.
     *
     * @param id the id of the PartyContactMechanismDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/party-contact-mechanism-purposes/{id}")
    @Timed
    public ResponseEntity<Void> deletePartyContactMechanism(@PathVariable Long id) {
        log.debug("REST request to delete PartyContactMechanism : {}", id);
        PartyContactMechanismService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/contact-mechanism-purposes?query=:query : search for the PartyContactMechanism corresponding
     * to the query.
     *
     * @param query the query of the PartyContactMechanism search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/party-contact-mechanism-purposes")
    @Timed
    public ResponseEntity<List<PartyContactMechanismDTO>> searchPartyContactMechanisms(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of PartyContactMechanisms for query {}", query);
        Page<PartyContactMechanismDTO> page = PartyContactMechanismService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/contact-mechanism-purposes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
