package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.InventoryMovementService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.InventoryMovementDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing InventoryMovement.
 */
@RestController
@RequestMapping("/api")
public class InventoryMovementResource {

    private final Logger log = LoggerFactory.getLogger(InventoryMovementResource.class);

    private static final String ENTITY_NAME = "inventoryMovement";

    private final InventoryMovementService inventoryMovementService;

    public InventoryMovementResource(InventoryMovementService inventoryMovementService) {
        this.inventoryMovementService = inventoryMovementService;
    }

    /**
     * POST  /inventory-movements : Create a new inventoryMovement.
     *
     * @param inventoryMovementDTO the inventoryMovementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new inventoryMovementDTO, or with status 400 (Bad Request) if the inventoryMovement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/inventory-movements")
    @Timed
    public ResponseEntity<InventoryMovementDTO> createInventoryMovement(@RequestBody InventoryMovementDTO inventoryMovementDTO) throws URISyntaxException {
        log.debug("REST request to save InventoryMovement : {}", inventoryMovementDTO);
        if (inventoryMovementDTO.getIdSlip() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new inventoryMovement cannot already have an ID")).body(null);
        }
        InventoryMovementDTO result = inventoryMovementService.save(inventoryMovementDTO);
        return ResponseEntity.created(new URI("/api/inventory-movements/" + result.getIdSlip()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdSlip().toString()))
            .body(result);
    }

    /**
     * POST  /inventory-movements/execute{id}/{param} : Execute Bussiness Process inventoryMovement.
     *
     * @param inventoryMovementDTO the inventoryMovementDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  inventoryMovementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/inventory-movements/execute/{id}/{param}")
    @Timed
    public ResponseEntity<InventoryMovementDTO> executedInventoryMovement(@PathVariable Integer id, @PathVariable String param, @RequestBody InventoryMovementDTO inventoryMovementDTO) throws URISyntaxException {
        log.debug("REST request to process InventoryMovement : {}", inventoryMovementDTO);
        InventoryMovementDTO result = inventoryMovementService.processExecuteData(id, param, inventoryMovementDTO);
        return new ResponseEntity<InventoryMovementDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /inventory-movements/execute-list{id}/{param} : Execute Bussiness Process inventoryMovement.
     *
     * @param inventoryMovementDTO the inventoryMovementDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  inventoryMovementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/inventory-movements/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<InventoryMovementDTO>> executedListInventoryMovement(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<InventoryMovementDTO> inventoryMovementDTO) throws URISyntaxException {
        log.debug("REST request to process List InventoryMovement");
        Set<InventoryMovementDTO> result = inventoryMovementService.processExecuteListData(id, param, inventoryMovementDTO);
        return new ResponseEntity<Set<InventoryMovementDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /inventory-movements : Updates an existing inventoryMovement.
     *
     * @param inventoryMovementDTO the inventoryMovementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated inventoryMovementDTO,
     * or with status 400 (Bad Request) if the inventoryMovementDTO is not valid,
     * or with status 500 (Internal Server Error) if the inventoryMovementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/inventory-movements")
    @Timed
    public ResponseEntity<InventoryMovementDTO> updateInventoryMovement(@RequestBody InventoryMovementDTO inventoryMovementDTO) throws URISyntaxException {
        log.debug("REST request to update InventoryMovement : {}", inventoryMovementDTO);
        if (inventoryMovementDTO.getIdSlip() == null) {
            return createInventoryMovement(inventoryMovementDTO);
        }
        InventoryMovementDTO result = inventoryMovementService.save(inventoryMovementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, inventoryMovementDTO.getIdSlip().toString()))
            .body(result);
    }

    /**
     * GET  /inventory-movements : get all the inventoryMovements.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of inventoryMovements in body
     */
    @GetMapping("/inventory-movements")
    @Timed
    public ResponseEntity<List<InventoryMovementDTO>> getAllInventoryMovements(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of InventoryMovements");
        Page<InventoryMovementDTO> page = inventoryMovementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/inventory-movements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /inventory-movements/:id : get the "id" inventoryMovement.
     *
     * @param id the id of the inventoryMovementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the inventoryMovementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/inventory-movements/{id}")
    @Timed
    public ResponseEntity<InventoryMovementDTO> getInventoryMovement(@PathVariable UUID id) {
        log.debug("REST request to get InventoryMovement : {}", id);
        InventoryMovementDTO inventoryMovementDTO = inventoryMovementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(inventoryMovementDTO));
    }

    /**
     * DELETE  /inventory-movements/:id : delete the "id" inventoryMovement.
     *
     * @param id the id of the inventoryMovementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/inventory-movements/{id}")
    @Timed
    public ResponseEntity<Void> deleteInventoryMovement(@PathVariable UUID id) {
        log.debug("REST request to delete InventoryMovement : {}", id);
        inventoryMovementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/inventory-movements?query=:query : search for the inventoryMovement corresponding
     * to the query.
     *
     * @param query the query of the inventoryMovement search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/inventory-movements")
    @Timed
    public ResponseEntity<List<InventoryMovementDTO>> searchInventoryMovements(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of InventoryMovements for query {}", query);
        Page<InventoryMovementDTO> page = inventoryMovementService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/inventory-movements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/inventory-movements/set-status/{id}")
    @Timed
    public ResponseEntity< InventoryMovementDTO> setStatusInventoryMovement(@PathVariable Integer id, @RequestBody InventoryMovementDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status InventoryMovement : {}", dto);
        InventoryMovementDTO r = inventoryMovementService.changeInventoryMovementStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
