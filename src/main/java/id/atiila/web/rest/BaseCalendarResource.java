package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.BaseCalendarService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.BaseCalendarDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BaseCalendar.
 */
@RestController
@RequestMapping("/api")
public class BaseCalendarResource {

    private final Logger log = LoggerFactory.getLogger(BaseCalendarResource.class);

    private static final String ENTITY_NAME = "baseCalendar";

    private final BaseCalendarService baseCalendarService;

    public BaseCalendarResource(BaseCalendarService baseCalendarService) {
        this.baseCalendarService = baseCalendarService;
    }

    /**
     * POST  /base-calendars : Create a new baseCalendar.
     *
     * @param baseCalendarDTO the baseCalendarDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new baseCalendarDTO, or with status 400 (Bad Request) if the baseCalendar has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/base-calendars")
    @Timed
    public ResponseEntity<BaseCalendarDTO> createBaseCalendar(@RequestBody BaseCalendarDTO baseCalendarDTO) throws URISyntaxException {
        log.debug("REST request to save BaseCalendar : {}", baseCalendarDTO);
        if (baseCalendarDTO.getIdCalendar() != null) {
            throw new BadRequestAlertException("A new baseCalendar cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BaseCalendarDTO result = baseCalendarService.save(baseCalendarDTO);
        return ResponseEntity.created(new URI("/api/base-calendars/" + result.getIdCalendar()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdCalendar().toString()))
            .body(result);
    }

    /**
     * POST  /base-calendars/execute{id}/{param} : Execute Bussiness Process baseCalendar.
     *
     * @param baseCalendarDTO the baseCalendarDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  baseCalendarDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/base-calendars/execute/{id}/{param}")
    @Timed
    public ResponseEntity<BaseCalendarDTO> executedBaseCalendar(@PathVariable Integer id, @PathVariable String param, @RequestBody BaseCalendarDTO baseCalendarDTO) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to process BaseCalendar : {}", baseCalendarDTO);
        BaseCalendarDTO result = baseCalendarService.processExecuteData(id, param, baseCalendarDTO);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<BaseCalendarDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /base-calendars/execute-list{id}/{param} : Execute Bussiness Process baseCalendar.
     *
     * @param baseCalendarDTO the baseCalendarDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  baseCalendarDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/base-calendars/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<BaseCalendarDTO>> executedListBaseCalendar(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<BaseCalendarDTO> baseCalendarDTO) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List BaseCalendar");
        Set<BaseCalendarDTO> result = baseCalendarService.processExecuteListData(id, param, baseCalendarDTO);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<BaseCalendarDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /base-calendars : Updates an existing baseCalendar.
     *
     * @param baseCalendarDTO the baseCalendarDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated baseCalendarDTO,
     * or with status 400 (Bad Request) if the baseCalendarDTO is not valid,
     * or with status 500 (Internal Server Error) if the baseCalendarDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/base-calendars")
    @Timed
    public ResponseEntity<BaseCalendarDTO> updateBaseCalendar(@RequestBody BaseCalendarDTO baseCalendarDTO) throws URISyntaxException {
        log.debug("REST request to update BaseCalendar : {}", baseCalendarDTO);
        if (baseCalendarDTO.getIdCalendar() == null) {
            return createBaseCalendar(baseCalendarDTO);
        }
        BaseCalendarDTO result = baseCalendarService.save(baseCalendarDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, baseCalendarDTO.getIdCalendar().toString()))
            .body(result);
    }

    /**
     * GET  /base-calendars : get all the baseCalendars.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of baseCalendars in body
     */
    @GetMapping("/base-calendars")
    @Timed
    public ResponseEntity<List<BaseCalendarDTO>> getAllBaseCalendars(Pageable pageable) {
        log.debug("REST request to get a page of BaseCalendars");
        Page<BaseCalendarDTO> page = baseCalendarService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/base-calendars");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /base-calendars/:id : get the "id" baseCalendar.
     *
     * @param id the id of the baseCalendarDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the baseCalendarDTO, or with status 404 (Not Found)
     */
    @GetMapping("/base-calendars/{id}")
    @Timed
    public ResponseEntity<BaseCalendarDTO> getBaseCalendar(@PathVariable Integer id) {
        log.debug("REST request to get BaseCalendar : {}", id);
        BaseCalendarDTO baseCalendarDTO = baseCalendarService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(baseCalendarDTO));
    }

    /**
     * DELETE  /base-calendars/:id : delete the "id" baseCalendar.
     *
     * @param id the id of the baseCalendarDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/base-calendars/{id}")
    @Timed
    public ResponseEntity<Void> deleteBaseCalendar(@PathVariable Integer id) {
        log.debug("REST request to delete BaseCalendar : {}", id);
        baseCalendarService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/base-calendars?query=:query : search for the baseCalendar corresponding
     * to the query.
     *
     * @param query the query of the baseCalendar search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/base-calendars")
    @Timed
    public ResponseEntity<List<BaseCalendarDTO>> searchBaseCalendars(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of BaseCalendars for query {}", query);
        Page<BaseCalendarDTO> page = baseCalendarService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/base-calendars");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/base-calendars/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processBaseCalendar(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process BaseCalendar ");
        Map<String, Object> result = baseCalendarService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
