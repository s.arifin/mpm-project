package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.CommunicationEventDeliveryService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.CommunicationEventDeliveryDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CommunicationEventDelivery.
 */
@RestController
@RequestMapping("/api")
public class CommunicationEventDeliveryResource {

    private final Logger log = LoggerFactory.getLogger(CommunicationEventDeliveryResource.class);

    private static final String ENTITY_NAME = "communicationEventDelivery";

    private final CommunicationEventDeliveryService communicationEventDeliveryService;

    public CommunicationEventDeliveryResource(CommunicationEventDeliveryService communicationEventDeliveryService) {
        this.communicationEventDeliveryService = communicationEventDeliveryService;
    }

    /**
     * POST  /communication-event-deliveries : Create a new communicationEventDelivery.
     *
     * @param communicationEventDeliveryDTO the communicationEventDeliveryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new communicationEventDeliveryDTO, or with status 400 (Bad Request) if the communicationEventDelivery has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/communication-event-deliveries")
    @Timed
    public ResponseEntity<CommunicationEventDeliveryDTO> createCommunicationEventDelivery(@RequestBody CommunicationEventDeliveryDTO communicationEventDeliveryDTO) throws URISyntaxException {
        log.debug("REST request to save CommunicationEventDelivery : {}", communicationEventDeliveryDTO);
        if (communicationEventDeliveryDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new communicationEventDelivery cannot already have an ID")).body(null);
        }
        CommunicationEventDeliveryDTO result = communicationEventDeliveryService.save(communicationEventDeliveryDTO);
        return ResponseEntity.created(new URI("/api/communication-event-deliveries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /communication-event-deliveries : Updates an existing communicationEventDelivery.
     *
     * @param communicationEventDeliveryDTO the communicationEventDeliveryDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated communicationEventDeliveryDTO,
     * or with status 400 (Bad Request) if the communicationEventDeliveryDTO is not valid,
     * or with status 500 (Internal Server Error) if the communicationEventDeliveryDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/communication-event-deliveries")
    @Timed
    public ResponseEntity<CommunicationEventDeliveryDTO> updateCommunicationEventDelivery(@RequestBody CommunicationEventDeliveryDTO communicationEventDeliveryDTO) throws URISyntaxException {
        log.debug("REST request to update CommunicationEventDelivery : {}", communicationEventDeliveryDTO);
        if (communicationEventDeliveryDTO.getId() == null) {
            return createCommunicationEventDelivery(communicationEventDeliveryDTO);
        }
        CommunicationEventDeliveryDTO result = communicationEventDeliveryService.save(communicationEventDeliveryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, communicationEventDeliveryDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /communication-event-deliveries : get all the communicationEventDeliveries.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of communicationEventDeliveries in body
     */
    @GetMapping("/communication-event-deliveries")
    @Timed
    public ResponseEntity<List<CommunicationEventDeliveryDTO>> getAllCommunicationEventDeliveries(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of CommunicationEventDeliveries");
        Page<CommunicationEventDeliveryDTO> page = communicationEventDeliveryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/communication-event-deliveries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /communication-event-deliveries/:id : get the "id" communicationEventDelivery.
     *
     * @param id the id of the communicationEventDeliveryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the communicationEventDeliveryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/communication-event-deliveries/{id}")
    @Timed
    public ResponseEntity<CommunicationEventDeliveryDTO> getCommunicationEventDelivery(@PathVariable UUID id) {
        log.debug("REST request to get CommunicationEventDelivery : {}", id);
        CommunicationEventDeliveryDTO communicationEventDeliveryDTO = communicationEventDeliveryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(communicationEventDeliveryDTO));
    }

    /**
     * DELETE  /communication-event-deliveries/:id : delete the "id" communicationEventDelivery.
     *
     * @param id the id of the communicationEventDeliveryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/communication-event-deliveries/{id}")
    @Timed
    public ResponseEntity<Void> deleteCommunicationEventDelivery(@PathVariable UUID id) {
        log.debug("REST request to delete CommunicationEventDelivery : {}", id);
        communicationEventDeliveryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/communication-event-deliveries?query=:query : search for the communicationEventDelivery corresponding
     * to the query.
     *
     * @param query the query of the communicationEventDelivery search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/communication-event-deliveries")
    @Timed
    public ResponseEntity<List<CommunicationEventDeliveryDTO>> searchCommunicationEventDeliveries(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of CommunicationEventDeliveries for query {}", query);
        Page<CommunicationEventDeliveryDTO> page = communicationEventDeliveryService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/communication-event-deliveries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
