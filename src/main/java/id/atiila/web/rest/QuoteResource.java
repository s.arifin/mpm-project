package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.QuoteService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.QuoteDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Quote.
 */
@RestController
@RequestMapping("/api")
public class QuoteResource {

    private final Logger log = LoggerFactory.getLogger(QuoteResource.class);

    private static final String ENTITY_NAME = "quote";

    private final QuoteService quoteService;

    public QuoteResource(QuoteService quoteService) {
        this.quoteService = quoteService;
    }

    /**
     * POST  /quotes : Create a new quote.
     *
     * @param quoteDTO the quoteDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new quoteDTO, or with status 400 (Bad Request) if the quote has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/quotes")
    @Timed
    public ResponseEntity<QuoteDTO> createQuote(@RequestBody QuoteDTO quoteDTO) throws URISyntaxException {
        log.debug("REST request to save Quote : {}", quoteDTO);
        if (quoteDTO.getIdQuote() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new quote cannot already have an ID")).body(null);
        }
        QuoteDTO result = quoteService.save(quoteDTO);
        return ResponseEntity.created(new URI("/api/quotes/" + result.getIdQuote()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdQuote().toString()))
            .body(result);
    }

    /**
     * POST  /quotes/execute{id}/{param} : Execute Bussiness Process quote.
     *
     * @param quoteDTO the quoteDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  quoteDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/quotes/execute/{id}/{param}")
    @Timed
    public ResponseEntity<QuoteDTO> executedQuote(@PathVariable Integer id, @PathVariable String param, @RequestBody QuoteDTO quoteDTO) throws URISyntaxException {
        log.debug("REST request to process Quote : {}", quoteDTO);
        QuoteDTO result = quoteService.processExecuteData(id, param, quoteDTO);
        return new ResponseEntity<QuoteDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /quotes/execute-list{id}/{param} : Execute Bussiness Process quote.
     *
     * @param quoteDTO the quoteDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  quoteDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/quotes/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<QuoteDTO>> executedListQuote(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<QuoteDTO> quoteDTO) throws URISyntaxException {
        log.debug("REST request to process List Quote");
        Set<QuoteDTO> result = quoteService.processExecuteListData(id, param, quoteDTO);
        return new ResponseEntity<Set<QuoteDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /quotes : Updates an existing quote.
     *
     * @param quoteDTO the quoteDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated quoteDTO,
     * or with status 400 (Bad Request) if the quoteDTO is not valid,
     * or with status 500 (Internal Server Error) if the quoteDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/quotes")
    @Timed
    public ResponseEntity<QuoteDTO> updateQuote(@RequestBody QuoteDTO quoteDTO) throws URISyntaxException {
        log.debug("REST request to update Quote : {}", quoteDTO);
        if (quoteDTO.getIdQuote() == null) {
            return createQuote(quoteDTO);
        }
        QuoteDTO result = quoteService.save(quoteDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, quoteDTO.getIdQuote().toString()))
            .body(result);
    }

    /**
     * GET  /quotes : get all the quotes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of quotes in body
     */
    @GetMapping("/quotes")
    @Timed
    public ResponseEntity<List<QuoteDTO>> getAllQuotes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Quotes");
        Page<QuoteDTO> page = quoteService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/quotes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /quotes/:id : get the "id" quote.
     *
     * @param id the id of the quoteDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the quoteDTO, or with status 404 (Not Found)
     */
    @GetMapping("/quotes/{id}")
    @Timed
    public ResponseEntity<QuoteDTO> getQuote(@PathVariable UUID id) {
        log.debug("REST request to get Quote : {}", id);
        QuoteDTO quoteDTO = quoteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(quoteDTO));
    }

    /**
     * DELETE  /quotes/:id : delete the "id" quote.
     *
     * @param id the id of the quoteDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/quotes/{id}")
    @Timed
    public ResponseEntity<Void> deleteQuote(@PathVariable UUID id) {
        log.debug("REST request to delete Quote : {}", id);
        quoteService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/quotes?query=:query : search for the quote corresponding
     * to the query.
     *
     * @param query the query of the quote search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/quotes")
    @Timed
    public ResponseEntity<List<QuoteDTO>> searchQuotes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Quotes for query {}", query);
        Page<QuoteDTO> page = quoteService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/quotes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/quotes/set-status/{id}")
    @Timed
    public ResponseEntity< QuoteDTO> setStatusQuote(@PathVariable Integer id, @RequestBody QuoteDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status Quote : {}", dto);
        QuoteDTO r = quoteService.changeQuoteStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
