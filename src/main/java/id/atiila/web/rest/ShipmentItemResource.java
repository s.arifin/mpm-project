package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ShipmentItemService;
import id.atiila.service.dto.CustomShipmentItemDTO;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ShipmentItemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ShipmentItem.
 */
@RestController
@RequestMapping("/api")
public class ShipmentItemResource {

    private final Logger log = LoggerFactory.getLogger(ShipmentItemResource.class);

    private static final String ENTITY_NAME = "shipmentItem";

    private final ShipmentItemService shipmentItemService;

    public ShipmentItemResource(ShipmentItemService shipmentItemService) {
        this.shipmentItemService = shipmentItemService;
    }

    /**
     * POST  /shipment-items : Create a new shipmentItem.
     *
     * @param shipmentItemDTO the shipmentItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shipmentItemDTO, or with status 400 (Bad Request) if the shipmentItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-items")
    @Timed
    public ResponseEntity<ShipmentItemDTO> createShipmentItem(@RequestBody ShipmentItemDTO shipmentItemDTO) throws URISyntaxException {
        log.debug("REST request to save ShipmentItem : {}", shipmentItemDTO);
        if (shipmentItemDTO.getIdShipmentItem() != null) {
            throw new BadRequestAlertException("A new shipmentItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShipmentItemDTO result = shipmentItemService.save(shipmentItemDTO);
        return ResponseEntity.created(new URI("/api/shipment-items/" + result.getIdShipmentItem()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdShipmentItem().toString()))
            .body(result);
    }

    /**
     * POST  /shipment-items/process : Execute Bussiness Process shipmentItem.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-items/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processShipmentItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ShipmentItem ");
        Map<String, Object> result = shipmentItemService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /shipment-items/execute : Execute Bussiness Process shipmentItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  shipmentItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-items/execute")
    @Timed
    public ResponseEntity<ShipmentItemDTO> executedShipmentItem(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ShipmentItem");
        ShipmentItemDTO result = shipmentItemService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<ShipmentItemDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /shipment-items/execute-list : Execute Bussiness Process shipmentItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  shipmentItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-items/execute-list")
    @Timed
    public ResponseEntity<Set<ShipmentItemDTO>> executedListShipmentItem(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List ShipmentItem");
        Set<ShipmentItemDTO> result = shipmentItemService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<ShipmentItemDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /shipment-items : Updates an existing shipmentItem.
     *
     * @param shipmentItemDTO the shipmentItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shipmentItemDTO,
     * or with status 400 (Bad Request) if the shipmentItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the shipmentItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/shipment-items")
    @Timed
    public ResponseEntity<ShipmentItemDTO> updateShipmentItem(@RequestBody ShipmentItemDTO shipmentItemDTO) throws URISyntaxException {
        log.debug("REST request to update ShipmentItem : {}", shipmentItemDTO);
        if (shipmentItemDTO.getIdShipmentItem() == null) {
            return createShipmentItem(shipmentItemDTO);
        }
        ShipmentItemDTO result = shipmentItemService.save(shipmentItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shipmentItemDTO.getIdShipmentItem().toString()))
            .body(result);
    }

    /**
     * GET  /shipment-items : get all the shipmentItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shipmentItems in body
     */
    @GetMapping("/shipment-items")
    @Timed
    public ResponseEntity<List<ShipmentItemDTO>> getAllShipmentItems(Pageable pageable) {
        log.debug("REST request to get a page of ShipmentItems");
        Page<ShipmentItemDTO> page = shipmentItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/shipment-items/filterBy")
    @Timed
    public ResponseEntity<List<ShipmentItemDTO>> getAllFilteredShipmentItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ShipmentItems");
        Page<ShipmentItemDTO> page = shipmentItemService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-items/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/shipment-items-spg/filterBy")
    @Timed
    public ResponseEntity<List<CustomShipmentItemDTO>> getAllCustomFilteredShipmentItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ShipmentItems SPG");
        Page<CustomShipmentItemDTO> page = shipmentItemService.findAllCustomBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-items-spg/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /shipment-items/:id : get the "id" shipmentItem.
     *
     * @param id the id of the shipmentItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shipmentItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/shipment-items/{id}")
    @Timed
    public ResponseEntity<ShipmentItemDTO> getShipmentItem(@PathVariable UUID id) {
        log.debug("REST request to get ShipmentItem : {}", id);
        ShipmentItemDTO shipmentItemDTO = shipmentItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(shipmentItemDTO));
    }

    /**
     * DELETE  /shipment-items/:id : delete the "id" shipmentItem.
     *
     * @param id the id of the shipmentItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shipment-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteShipmentItem(@PathVariable UUID id) {
        log.debug("REST request to delete ShipmentItem : {}", id);
        shipmentItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/shipment-items?query=:query : search for the shipmentItem corresponding
     * to the query.
     *
     * @param query the query of the shipmentItem search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/shipment-items")
    @Timed
    public ResponseEntity<List<ShipmentItemDTO>> searchShipmentItems(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ShipmentItems for query {}", query);
        Page<ShipmentItemDTO> page = shipmentItemService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/shipment-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
