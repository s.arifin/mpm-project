package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ShipmentTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ShipmentTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ShipmentType.
 */
@RestController
@RequestMapping("/api")
public class ShipmentTypeResource {

    private final Logger log = LoggerFactory.getLogger(ShipmentTypeResource.class);

    private static final String ENTITY_NAME = "shipmentType";

    private final ShipmentTypeService shipmentTypeService;

    public ShipmentTypeResource(ShipmentTypeService shipmentTypeService) {
        this.shipmentTypeService = shipmentTypeService;
    }

    /**
     * POST  /shipment-types : Create a new shipmentType.
     *
     * @param shipmentTypeDTO the shipmentTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shipmentTypeDTO, or with status 400 (Bad Request) if the shipmentType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-types")
    @Timed
    public ResponseEntity<ShipmentTypeDTO> createShipmentType(@RequestBody ShipmentTypeDTO shipmentTypeDTO) throws URISyntaxException {
        log.debug("REST request to save ShipmentType : {}", shipmentTypeDTO);
        if (shipmentTypeDTO.getIdShipmentType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new shipmentType cannot already have an ID")).body(null);
        }
        ShipmentTypeDTO result = shipmentTypeService.save(shipmentTypeDTO);

        return ResponseEntity.created(new URI("/api/shipment-types/" + result.getIdShipmentType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdShipmentType().toString()))
            .body(result);
    }

    /**
     * POST  /shipment-types/execute : Execute Bussiness Process shipmentType.
     *
     * @param shipmentTypeDTO the shipmentTypeDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  shipmentTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-types/execute")
    @Timed
    public ResponseEntity<ShipmentTypeDTO> executedShipmentType(@RequestBody ShipmentTypeDTO shipmentTypeDTO) throws URISyntaxException {
        log.debug("REST request to process ShipmentType : {}", shipmentTypeDTO);
        return new ResponseEntity<ShipmentTypeDTO>(shipmentTypeDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /shipment-types : Updates an existing shipmentType.
     *
     * @param shipmentTypeDTO the shipmentTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shipmentTypeDTO,
     * or with status 400 (Bad Request) if the shipmentTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the shipmentTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/shipment-types")
    @Timed
    public ResponseEntity<ShipmentTypeDTO> updateShipmentType(@RequestBody ShipmentTypeDTO shipmentTypeDTO) throws URISyntaxException {
        log.debug("REST request to update ShipmentType : {}", shipmentTypeDTO);
        if (shipmentTypeDTO.getIdShipmentType() == null) {
            return createShipmentType(shipmentTypeDTO);
        }
        ShipmentTypeDTO result = shipmentTypeService.save(shipmentTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shipmentTypeDTO.getIdShipmentType().toString()))
            .body(result);
    }

    /**
     * GET  /shipment-types : get all the shipmentTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shipmentTypes in body
     */
    @GetMapping("/shipment-types")
    @Timed
    public ResponseEntity<List<ShipmentTypeDTO>> getAllShipmentTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ShipmentTypes");
        Page<ShipmentTypeDTO> page = shipmentTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /shipment-types/:id : get the "id" shipmentType.
     *
     * @param id the id of the shipmentTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shipmentTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/shipment-types/{id}")
    @Timed
    public ResponseEntity<ShipmentTypeDTO> getShipmentType(@PathVariable Integer id) {
        log.debug("REST request to get ShipmentType : {}", id);
        ShipmentTypeDTO shipmentTypeDTO = shipmentTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(shipmentTypeDTO));
    }

    /**
     * DELETE  /shipment-types/:id : delete the "id" shipmentType.
     *
     * @param id the id of the shipmentTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shipment-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteShipmentType(@PathVariable Integer id) {
        log.debug("REST request to delete ShipmentType : {}", id);
        shipmentTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/shipment-types?query=:query : search for the shipmentType corresponding
     * to the query.
     *
     * @param query the query of the shipmentType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/shipment-types")
    @Timed
    public ResponseEntity<List<ShipmentTypeDTO>> searchShipmentTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ShipmentTypes for query {}", query);
        Page<ShipmentTypeDTO> page = shipmentTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/shipment-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/shipment-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processShipmentType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process ShipmentType ");
        Map<String, Object> result = shipmentTypeService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
