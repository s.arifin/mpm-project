package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.FacilityTypeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.FacilityTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing FacilityType.
 */
@RestController
@RequestMapping("/api")
public class FacilityTypeResource {

    private final Logger log = LoggerFactory.getLogger(FacilityTypeResource.class);

    private static final String ENTITY_NAME = "facilityType";

    private final FacilityTypeService facilityTypeService;

    public FacilityTypeResource(FacilityTypeService facilityTypeService) {
        this.facilityTypeService = facilityTypeService;
    }

    /**
     * POST  /facility-types : Create a new facilityType.
     *
     * @param facilityTypeDTO the facilityTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new facilityTypeDTO, or with status 400 (Bad Request) if the facilityType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/facility-types")
    @Timed
    public ResponseEntity<FacilityTypeDTO> createFacilityType(@RequestBody FacilityTypeDTO facilityTypeDTO) throws URISyntaxException {
        log.debug("REST request to save FacilityType : {}", facilityTypeDTO);
        if (facilityTypeDTO.getIdFacilityType() != null) {
            throw new BadRequestAlertException("A new facilityType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FacilityTypeDTO result = facilityTypeService.save(facilityTypeDTO);
        return ResponseEntity.created(new URI("/api/facility-types/" + result.getIdFacilityType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdFacilityType().toString()))
            .body(result);
    }

    /**
     * POST  /facility-types/process : Execute Bussiness Process facilityType.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/facility-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processFacilityType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process FacilityType ");
        Map<String, Object> result = facilityTypeService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /facility-types/execute : Execute Bussiness Process facilityType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  facilityTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/facility-types/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedFacilityType(HttpServletRequest request, @RequestBody FacilityTypeDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process FacilityType");
        Map<String, Object> result = facilityTypeService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /facility-types/execute-list : Execute Bussiness Process facilityType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  facilityTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/facility-types/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListFacilityType(HttpServletRequest request, @RequestBody Set<FacilityTypeDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List FacilityType");
        Map<String, Object> result = facilityTypeService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /facility-types : Updates an existing facilityType.
     *
     * @param facilityTypeDTO the facilityTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated facilityTypeDTO,
     * or with status 400 (Bad Request) if the facilityTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the facilityTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/facility-types")
    @Timed
    public ResponseEntity<FacilityTypeDTO> updateFacilityType(@RequestBody FacilityTypeDTO facilityTypeDTO) throws URISyntaxException {
        log.debug("REST request to update FacilityType : {}", facilityTypeDTO);
        if (facilityTypeDTO.getIdFacilityType() == null) {
            return createFacilityType(facilityTypeDTO);
        }
        FacilityTypeDTO result = facilityTypeService.save(facilityTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, facilityTypeDTO.getIdFacilityType().toString()))
            .body(result);
    }

    /**
     * GET  /facility-types : get all the facilityTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of facilityTypes in body
     */
    @GetMapping("/facility-types")
    @Timed
    public ResponseEntity<List<FacilityTypeDTO>> getAllFacilityTypes(Pageable pageable) {
        log.debug("REST request to get a page of FacilityTypes");
        Page<FacilityTypeDTO> page = facilityTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/facility-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/facility-types/filterBy")
    @Timed
    public ResponseEntity<List<FacilityTypeDTO>> getAllFilteredFacilityTypes(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of FacilityTypes");
        Page<FacilityTypeDTO> page = facilityTypeService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/facility-types/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /facility-types/:id : get the "id" facilityType.
     *
     * @param id the id of the facilityTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the facilityTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/facility-types/{id}")
    @Timed
    public ResponseEntity<FacilityTypeDTO> getFacilityType(@PathVariable Integer id) {
        log.debug("REST request to get FacilityType : {}", id);
        FacilityTypeDTO facilityTypeDTO = facilityTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(facilityTypeDTO));
    }

    /**
     * DELETE  /facility-types/:id : delete the "id" facilityType.
     *
     * @param id the id of the facilityTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/facility-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteFacilityType(@PathVariable Integer id) {
        log.debug("REST request to delete FacilityType : {}", id);
        facilityTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/facility-types?query=:query : search for the facilityType corresponding
     * to the query.
     *
     * @param query the query of the facilityType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/facility-types")
    @Timed
    public ResponseEntity<List<FacilityTypeDTO>> searchFacilityTypes(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of FacilityTypes for query {}", query);
        Page<FacilityTypeDTO> page = facilityTypeService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/facility-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
