package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RequirementTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RequirementTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RequirementType.
 */
@RestController
@RequestMapping("/api")
public class RequirementTypeResource {

    private final Logger log = LoggerFactory.getLogger(RequirementTypeResource.class);

    private static final String ENTITY_NAME = "requirementType";

    private final RequirementTypeService requirementTypeService;

    public RequirementTypeResource(RequirementTypeService requirementTypeService) {
        this.requirementTypeService = requirementTypeService;
    }

    /**
     * POST  /requirement-types : Create a new requirementType.
     *
     * @param requirementTypeDTO the requirementTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new requirementTypeDTO, or with status 400 (Bad Request) if the requirementType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requirement-types")
    @Timed
    public ResponseEntity<RequirementTypeDTO> createRequirementType(@RequestBody RequirementTypeDTO requirementTypeDTO) throws URISyntaxException {
        log.debug("REST request to save RequirementType : {}", requirementTypeDTO);
        if (requirementTypeDTO.getIdRequirementType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new requirementType cannot already have an ID")).body(null);
        }
        RequirementTypeDTO result = requirementTypeService.save(requirementTypeDTO);

        return ResponseEntity.created(new URI("/api/requirement-types/" + result.getIdRequirementType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequirementType().toString()))
            .body(result);
    }

    /**
     * POST  /requirement-types/execute : Execute Bussiness Process requirementType.
     *
     * @param requirementTypeDTO the requirementTypeDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  requirementTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requirement-types/execute")
    @Timed
    public ResponseEntity<RequirementTypeDTO> executedRequirementType(@RequestBody RequirementTypeDTO requirementTypeDTO) throws URISyntaxException {
        log.debug("REST request to process RequirementType : {}", requirementTypeDTO);
        return new ResponseEntity<RequirementTypeDTO>(requirementTypeDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /requirement-types : Updates an existing requirementType.
     *
     * @param requirementTypeDTO the requirementTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated requirementTypeDTO,
     * or with status 400 (Bad Request) if the requirementTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the requirementTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/requirement-types")
    @Timed
    public ResponseEntity<RequirementTypeDTO> updateRequirementType(@RequestBody RequirementTypeDTO requirementTypeDTO) throws URISyntaxException {
        log.debug("REST request to update RequirementType : {}", requirementTypeDTO);
        if (requirementTypeDTO.getIdRequirementType() == null) {
            return createRequirementType(requirementTypeDTO);
        }
        RequirementTypeDTO result = requirementTypeService.save(requirementTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, requirementTypeDTO.getIdRequirementType().toString()))
            .body(result);
    }

    /**
     * GET  /requirement-types : get all the requirementTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of requirementTypes in body
     */
    @GetMapping("/requirement-types")
    @Timed
    public ResponseEntity<List<RequirementTypeDTO>> getAllRequirementTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of RequirementTypes");
        Page<RequirementTypeDTO> page = requirementTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/requirement-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /requirement-types/:id : get the "id" requirementType.
     *
     * @param id the id of the requirementTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the requirementTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/requirement-types/{id}")
    @Timed
    public ResponseEntity<RequirementTypeDTO> getRequirementType(@PathVariable Integer id) {
        log.debug("REST request to get RequirementType : {}", id);
        RequirementTypeDTO requirementTypeDTO = requirementTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(requirementTypeDTO));
    }

    /**
     * DELETE  /requirement-types/:id : delete the "id" requirementType.
     *
     * @param id the id of the requirementTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/requirement-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteRequirementType(@PathVariable Integer id) {
        log.debug("REST request to delete RequirementType : {}", id);
        requirementTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/requirement-types?query=:query : search for the requirementType corresponding
     * to the query.
     *
     * @param query the query of the requirementType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/requirement-types")
    @Timed
    public ResponseEntity<List<RequirementTypeDTO>> searchRequirementTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of RequirementTypes for query {}", query);
        Page<RequirementTypeDTO> page = requirementTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/requirement-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/requirement-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processRequirementType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process RequirementType ");
        Map<String, Object> result = requirementTypeService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
