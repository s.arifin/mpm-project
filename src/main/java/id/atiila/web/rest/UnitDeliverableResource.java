package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.domain.UnitDeliverable;
import id.atiila.service.UnitDeliverableService;
import id.atiila.service.dto.CustomBpkbDTO;
import id.atiila.service.dto.SummaryRefferalDTO;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.UnitDeliverableDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import id.atiila.service.pto.UnitDeliverablePTO;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.concurrent.TimeUnit;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UnitDeliverable.
 */
@RestController
@RequestMapping("/api")
public class UnitDeliverableResource {

    private final Logger log = LoggerFactory.getLogger(UnitDeliverableResource.class);

    private static final String ENTITY_NAME = "unitDeliverable";

    private final UnitDeliverableService unitDeliverableService;

    public UnitDeliverableResource(UnitDeliverableService unitDeliverableService) {
        this.unitDeliverableService = unitDeliverableService;
    }

    /**
     * POST  /unit-deliverables : Create a new unitDeliverable.
     *
     * @param unitDeliverableDTO the unitDeliverableDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new unitDeliverableDTO, or with status 400 (Bad Request) if the unitDeliverable has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-deliverables")
    @Timed
    public ResponseEntity<UnitDeliverableDTO> createUnitDeliverable(@RequestBody UnitDeliverableDTO unitDeliverableDTO) throws URISyntaxException {
        log.debug("REST request to save UnitDeliverable : {}", unitDeliverableDTO);
        if (unitDeliverableDTO.getIdDeliverable() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new unitDeliverable cannot already have an ID")).body(null);
        }
        UnitDeliverableDTO result = unitDeliverableService.save(unitDeliverableDTO);
        return ResponseEntity.created(new URI("/api/unit-deliverables/" + result.getIdDeliverable()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdDeliverable().toString()))
            .body(result);
    }

    /**
     * POST  /unit-deliverables/migration-data : Create a new unitDeliverable.
     *
     * @param customBpkbDTO the customBpkbDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new customBpkbDTO, or with status 400 (Bad Request) if the customBpkb has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
//    @PostMapping("/unit-deliverables/migration-data")
//    @Timed
//    public ResponseEntity<CustomBpkbDTO> migrationDataUnitDeliverable(@RequestBody CustomBpkbDTO customBpkbDTO) throws URISyntaxException {
//        log.debug("REST request to save UnitDeliverable : {}", customBpkbDTO);
//        if (customBpkbDTO.getBpkbNumber() != null) {
//            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new unitDeliverable cannot already have an ID")).body(null);
//        }
//        CustomBpkbDTO result = unitDeliverableService.migrationData(customBpkbDTO);
//        return ResponseEntity.created(new URI("/api/unit-deliverables/" + result.getBpkbNumber()))
//            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getBpkbNumber().toString()))
//            .body(result);
//    }

    /**
     * POST  /unit-deliverables/execute{id}/{param} : Execute Bussiness Process unitDeliverable.
     *
     * @param unitDeliverableDTO the unitDeliverableDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  unitDeliverableDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-deliverables/execute/{id}/{param}")
    @Timed
    public ResponseEntity<UnitDeliverableDTO> executedUnitDeliverable(@PathVariable Integer id, @PathVariable String param, @RequestBody UnitDeliverableDTO unitDeliverableDTO) throws URISyntaxException {
        log.debug("REST request to process UnitDeliverable : {}", unitDeliverableDTO);
        UnitDeliverableDTO result = unitDeliverableService.processExecuteData(id, param, unitDeliverableDTO);
        return new ResponseEntity<UnitDeliverableDTO>(result, null, HttpStatus.OK);
    }

    @GetMapping("/unit-deliverables/setprint")
    @Timed
    public ResponseEntity<UnitDeliverableDTO> executePrintUnitDeliverable(HttpServletRequest request, UnitDeliverableDTO dto) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to print UnitDeliverable");
        UnitDeliverableDTO result = unitDeliverableService.setPrint(request, dto);
        return new ResponseEntity<UnitDeliverableDTO>(result, null, HttpStatus.OK);

    }

    /**
     * POST  /unit-deliverables/execute-list{id}/{param} : Execute Bussiness Process unitDeliverable.
     *
     * @param unitDeliverableDTO the unitDeliverableDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  unitDeliverableDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-deliverables/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<UnitDeliverableDTO>> executedListUnitDeliverable(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<UnitDeliverableDTO> unitDeliverableDTO) throws URISyntaxException {
        log.debug("REST request to process List UnitDeliverable");
        Set<UnitDeliverableDTO> result = unitDeliverableService.processExecuteListData(id, param, unitDeliverableDTO);
        return new ResponseEntity<Set<UnitDeliverableDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /unit-deliverables : Updates an existing unitDeliverable.
     *
     * @param unitDeliverableDTO the unitDeliverableDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated unitDeliverableDTO,
     * or with status 400 (Bad Request) if the unitDeliverableDTO is not valid,
     * or with status 500 (Internal Server Error) if the unitDeliverableDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/unit-deliverables")
    @Timed
    public ResponseEntity<UnitDeliverableDTO> updateUnitDeliverable(@RequestBody UnitDeliverableDTO unitDeliverableDTO) throws URISyntaxException {
        log.debug("REST request to update UnitDeliverable : {}", unitDeliverableDTO);
        if (unitDeliverableDTO.getIdDeliverable() == null) {
            return createUnitDeliverable(unitDeliverableDTO);
        }
        UnitDeliverableDTO result = unitDeliverableService.save(unitDeliverableDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, unitDeliverableDTO.getIdDeliverable().toString()))
            .body(result);
    }

    /**
     * GET  /unit-deliverables : get all the unitDeliverables.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of unitDeliverables in body
     */
    @GetMapping("/unit-deliverables")
    @Timed
    public ResponseEntity<List<UnitDeliverableDTO>> getAllUnitDeliverables(@RequestParam(required = false) Integer idDeliverableType, @RequestParam(required = false) String idInternal, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UnitDeliverables");
        Page<UnitDeliverableDTO> page = unitDeliverableService.findAllByIdDeliverableType(pageable, idDeliverableType, idInternal);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-deliverables");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    @GetMapping("/unit-deliverables/filterBy")
    @Timed
    public ResponseEntity<List<UnitDeliverableDTO>> getAllUnitDeliverablesFilterBy(@ApiParam UnitDeliverablePTO param,
                                                                                   @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UnitDeliverables");
        Page<UnitDeliverableDTO> page = unitDeliverableService.findAllByIdDeliverableTypeFilterBy(pageable, param);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-deliverables/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /unit-deliverables/:id : get the "id" unitDeliverable.
     *
     * @param id the id of the unitDeliverableDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the unitDeliverableDTO, or with status 404 (Not Found)
     */
    @GetMapping("/unit-deliverables/{id}")
    @Timed
    public ResponseEntity<UnitDeliverableDTO> getUnitDeliverable(@PathVariable String id) {
        UUID idDeliverable = UUID.fromString(id);
        log.debug("REST request to get UnitDeliverable : {}", idDeliverable);
        UnitDeliverableDTO unitDeliverableDTO = unitDeliverableService.findOne(idDeliverable);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(unitDeliverableDTO));
    }

    /**
     * DELETE  /unit-deliverables/:id : delete the "id" unitDeliverable.
     *
     * @param id the id of the unitDeliverableDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/unit-deliverables/{id}")
    @Timed
    public ResponseEntity<Void> deleteUnitDeliverable(@PathVariable UUID id) {
        log.debug("REST request to delete UnitDeliverable : {}", id);
        unitDeliverableService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/unit-deliverables?query=:query : search for the unitDeliverable corresponding
     * to the query.
     *
     * @param query the query of the unitDeliverable search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/unit-deliverables")
    @Timed
    public ResponseEntity<List<UnitDeliverableDTO>> searchUnitDeliverables(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of UnitDeliverables for query {}", query);
        Page<UnitDeliverableDTO> page = unitDeliverableService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/unit-deliverables");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /unit-deliverables/list-references : get all the unitDeliverables references.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of unitDeliverables in body
     */
//    @GetMapping("/unit-deliverables/list-references")
//    @Timed
//    public ResponseEntity<List<UnitDeliverableDTO>> getAllReferences(@ApiParam Pageable pageable) {
//        log.debug("REST request to get a page of UnitDeliverables List References");
//        Page<UnitDeliverableDTO> page = unitDeliverableService.findAllReferences(pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-deliverables");
//        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
//    }

    @GetMapping("/unit-deliverables/list-references")
    @Timed
    public ResponseEntity<List<SummaryRefferalDTO>> getAllReferences(@RequestParam(required = false) String idInternal, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UnitDeliverables List References");
        Page<SummaryRefferalDTO> page = unitDeliverableService.findAllReferences(idInternal, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-deliverables");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/unit-deliverables/list-references-lov")
    @Timed
    public ResponseEntity<List<SummaryRefferalDTO>> getListReferences( @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UnitDeliverables List References as LOV");
        Page<SummaryRefferalDTO> page = unitDeliverableService.findListReferences( pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-deliverables");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/unit-deliverables/list-references-lov1")
    @Timed
    public ResponseEntity<List<SummaryRefferalDTO>> getListReferences1( @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UnitDeliverables List References as LOV");
        Page<SummaryRefferalDTO> page = unitDeliverableService.findListReferences1( pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-deliverables1");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


//    @GetMapping("/unit-deliverables/findRef")
//    @Timed
//    public ResponseEntity<SummaryRefferalDTO> findRefByRefNumber(@RequestParam(required = false) String idInternal, @RequestParam(required = false) String refNumber) {
//        log.debug("REST request to get a page of References by refNumber");
//        SummaryRefferalDTO summaryRefferalDTO = unitDeliverableService.findRefferenceByRefNumber(idInternal, refNumber);
//        return new ResponseEntity<>(summaryRefferalDTO, null, HttpStatus.OK);
//    }

    /**
     * POST  /unit-shipment-receipts/execute : Execute Bussiness Process unitShipmentReceipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  unitShipmentReceiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-deliverables/executefind")
    @Timed
    public ResponseEntity<UnitDeliverableDTO> executeFindUnitDeliverable(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process UnitDeliverable");
        UnitDeliverableDTO result = unitDeliverableService.processExecuteFindData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<UnitDeliverableDTO>(result, null, HttpStatus.OK);
    }


    @GetMapping("/unit-deliverables/offtheroad")
    @Timed
    public ResponseEntity<List<UnitDeliverableDTO>> findOfftheRoad(@RequestParam(required = false) Integer idStatusType, @RequestParam(required = false) String idInternal, @RequestParam(required = false) Integer idDeliverableType, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UnitDeliverables Find Off The Road");
        Page<UnitDeliverableDTO> page = unitDeliverableService.findAllByIdDeliverableTypeandStatusType(pageable, idDeliverableType, idInternal, idStatusType);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-deliverables");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/unit-deliverables/filterleasing")
    @Timed
    public ResponseEntity<List<UnitDeliverableDTO>> findByLeasing(@RequestParam(required = false) Integer idDeliverableType, @RequestParam(required = false) String idInternal, @RequestParam(required = false) String idLeasing, @ApiParam Pageable pageable) {
//        UUID leasingId = UUID.fromString(idLeasing);
        log.debug("REST request to get a page of UnitDeliverables Find By Leasing");
        Page<UnitDeliverableDTO> page = unitDeliverableService.findAllByIdDeliverableLeasing(pageable, idDeliverableType, idInternal, idLeasing);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-deliverables/filterleasing");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/unit-deliverables/findByIdReqandIdDeltype")
    @Timed
    public ResponseEntity<UnitDeliverableDTO> findByIdReqandIdDeltype(@RequestParam(required = false) Integer idDeliverableType, @RequestParam(required = false) String idReq) {
        UUID idRequirement = UUID.fromString(idReq);
        log.debug("REST request to get a page of UnitDeliverables Find By Id Requirement and Deliverble Type");
        UnitDeliverableDTO ud = unitDeliverableService.findByIdReqandIdDeliverableTye(idRequirement, idDeliverableType);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ud));
    }

    @GetMapping("/_search/unit-deliverables-stnk")
    @Timed
    public ResponseEntity<List<UnitDeliverableDTO>> getFindSearch(@ApiParam UnitDeliverablePTO param,
                                                                  @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UnitDeliverables");
        Page<UnitDeliverableDTO> page = unitDeliverableService.findAllByIdDeliverableTypeFilterBy(pageable, param);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-deliverables-stnk");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
