package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.UnitShipmentOutgoingService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.UnitShipmentOutgoingDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ShipmentOutgoing.
 */
@RestController
@RequestMapping("/api")
public class UnitShipmentOutgoingResource {

    private final Logger log = LoggerFactory.getLogger(UnitShipmentOutgoingResource.class);

    private static final String ENTITY_NAME = "ShipmentOutgoing";

    private final UnitShipmentOutgoingService unitShipmentOutgoingService;

    public UnitShipmentOutgoingResource(UnitShipmentOutgoingService unitShipmentOutgoingService) {
        this.unitShipmentOutgoingService = unitShipmentOutgoingService;
    }

    /**
     * POST  /unit-shipment-outgoings : Create a new ShipmentOutgoing.
     *
     * @param unitShipmentOutgoingDTO the unitShipmentOutgoingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new unitShipmentOutgoingDTO, or with status 400 (Bad Request) if the ShipmentOutgoing has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-shipment-outgoings")
    @Timed
    public ResponseEntity<UnitShipmentOutgoingDTO> createUnitShipmentOutgoing(@RequestBody UnitShipmentOutgoingDTO unitShipmentOutgoingDTO) throws URISyntaxException {
        log.debug("REST request to save ShipmentOutgoing : {}", unitShipmentOutgoingDTO);
        if (unitShipmentOutgoingDTO.getIdShipment() != null) {
            throw new BadRequestAlertException("A new ShipmentOutgoing cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UnitShipmentOutgoingDTO result = unitShipmentOutgoingService.save(unitShipmentOutgoingDTO);
        return ResponseEntity.created(new URI("/api/unit-shipment-outgoings/" + result.getIdShipment()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdShipment().toString()))
            .body(result);
    }

    /**
     * POST  /unit-shipment-outgoings/process : Execute Bussiness Process ShipmentOutgoing.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-shipment-outgoings/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processUnitShipmentOutgoing(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ShipmentOutgoing ");
        Map<String, Object> result = unitShipmentOutgoingService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /unit-shipment-outgoings/execute : Execute Bussiness Process ShipmentOutgoing.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  unitShipmentOutgoingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-shipment-outgoings/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedUnitShipmentOutgoing(HttpServletRequest request, @RequestBody UnitShipmentOutgoingDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ShipmentOutgoing");
        Map<String, Object> result = unitShipmentOutgoingService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /unit-shipment-outgoings/execute-list : Execute Bussiness Process ShipmentOutgoing.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  unitShipmentOutgoingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-shipment-outgoings/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListUnitShipmentOutgoing(HttpServletRequest request, @RequestBody Set<UnitShipmentOutgoingDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List ShipmentOutgoing");
        Map<String, Object> result = unitShipmentOutgoingService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /unit-shipment-outgoings : Updates an existing ShipmentOutgoing.
     *
     * @param unitShipmentOutgoingDTO the unitShipmentOutgoingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated unitShipmentOutgoingDTO,
     * or with status 400 (Bad Request) if the unitShipmentOutgoingDTO is not valid,
     * or with status 500 (Internal Server Error) if the unitShipmentOutgoingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/unit-shipment-outgoings")
    @Timed
    public ResponseEntity<UnitShipmentOutgoingDTO> updateUnitShipmentOutgoing(@RequestBody UnitShipmentOutgoingDTO unitShipmentOutgoingDTO) throws URISyntaxException {
        log.debug("REST request to update ShipmentOutgoing : {}", unitShipmentOutgoingDTO);
        if (unitShipmentOutgoingDTO.getIdShipment() == null) {
            return createUnitShipmentOutgoing(unitShipmentOutgoingDTO);
        }
        UnitShipmentOutgoingDTO result = unitShipmentOutgoingService.save(unitShipmentOutgoingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, unitShipmentOutgoingDTO.getIdShipment().toString()))
            .body(result);
    }

    /**
     * GET  /unit-shipment-outgoings : get all the unitShipmentOutgoings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of unitShipmentOutgoings in body
     */
    @GetMapping("/unit-shipment-outgoings")
    @Timed
    public ResponseEntity<List<UnitShipmentOutgoingDTO>> getAllUnitShipmentOutgoings(Pageable pageable) {
        log.debug("REST request to get a page of UnitShipmentOutgoings");
        Page<UnitShipmentOutgoingDTO> page = unitShipmentOutgoingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-shipment-outgoings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/unit-shipment-outgoings/filterBy")
    @Timed
    public ResponseEntity<List<UnitShipmentOutgoingDTO>> getAllFilteredUnitShipmentOutgoings(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of UnitShipmentOutgoings");
        Page<UnitShipmentOutgoingDTO> page = unitShipmentOutgoingService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-shipment-outgoings/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /unit-shipment-outgoings/:id : get the "id" ShipmentOutgoing.
     *
     * @param id the id of the unitShipmentOutgoingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the unitShipmentOutgoingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/unit-shipment-outgoings/{id}")
    @Timed
    public ResponseEntity<UnitShipmentOutgoingDTO> getUnitShipmentOutgoing(@PathVariable UUID id) {
        log.debug("REST request to get ShipmentOutgoing : {}", id);
        UnitShipmentOutgoingDTO unitShipmentOutgoingDTO = unitShipmentOutgoingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(unitShipmentOutgoingDTO));
    }

    /**
     * DELETE  /unit-shipment-outgoings/:id : delete the "id" ShipmentOutgoing.
     *
     * @param id the id of the unitShipmentOutgoingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/unit-shipment-outgoings/{id}")
    @Timed
    public ResponseEntity<Void> deleteUnitShipmentOutgoing(@PathVariable UUID id) {
        log.debug("REST request to delete ShipmentOutgoing : {}", id);
        unitShipmentOutgoingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/unit-shipment-outgoings?query=:query : search for the ShipmentOutgoing corresponding
     * to the query.
     *
     * @param query the query of the ShipmentOutgoing search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/unit-shipment-outgoings")
    @Timed
    public ResponseEntity<List<UnitShipmentOutgoingDTO>> searchUnitShipmentOutgoings(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of UnitShipmentOutgoings for query {}", query);
        Page<UnitShipmentOutgoingDTO> page = unitShipmentOutgoingService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/unit-shipment-outgoings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/unit-shipment-outgoings/set-status/{id}")
    @Timed
    public ResponseEntity< UnitShipmentOutgoingDTO> setStatusUnitShipmentOutgoing(@PathVariable Integer id, @RequestBody UnitShipmentOutgoingDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status ShipmentOutgoing : {}", dto);
        UnitShipmentOutgoingDTO r = unitShipmentOutgoingService.changeUnitShipmentOutgoingStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(500);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
