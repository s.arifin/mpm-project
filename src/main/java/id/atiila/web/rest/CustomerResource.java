package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.CustomerService;
import id.atiila.service.dto.BillToDTO;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.CustomerDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Customer.
 */
@RestController
@RequestMapping("/api")
public class CustomerResource {

    private final Logger log = LoggerFactory.getLogger(CustomerResource.class);

    private static final String ENTITY_NAME = "customer";

    private final CustomerService customerService;

    public CustomerResource(CustomerService customerService) {
        this.customerService = customerService;
    }

    /**
     * POST  /customers : Create a new customer.
     *
     * @param customerDTO the customerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new customerDTO, or with status 400 (Bad Request) if the customer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customers")
    @Timed
    public ResponseEntity<CustomerDTO> createCustomer(@RequestBody CustomerDTO customerDTO) throws URISyntaxException {
        log.debug("REST request to save Customer : {}", customerDTO);
        if (customerDTO.getIdCustomer() != null) {
            throw new BadRequestAlertException("A new customer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CustomerDTO result = customerService.save(customerDTO);
        return ResponseEntity.created(new URI("/api/customers/" + result.getIdCustomer()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdCustomer().toString()))
            .body(result);
    }

    /**
     * POST  /customers/execute{id}/{param} : Execute Bussiness Process customer.
     *
     * @param customerDTO the customerDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  customerDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customers/execute/{id}/{param}")
    @Timed
    public ResponseEntity<CustomerDTO> executedCustomer(@PathVariable Integer id, @PathVariable String param, @RequestBody CustomerDTO customerDTO) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to process Customer : {}", customerDTO);
        CustomerDTO result = customerService.processExecuteData(id, param, customerDTO);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<CustomerDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /customers/execute-list{id}/{param} : Execute Bussiness Process customer.
     *
     * @param customerDTO the customerDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  customerDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customers/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<CustomerDTO>> executedListCustomer(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<CustomerDTO> customerDTO) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List Customer");
        Set<CustomerDTO> result = customerService.processExecuteListData(id, param, customerDTO);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<CustomerDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /customers : Updates an existing customer.
     *
     * @param customerDTO the customerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated customerDTO,
     * or with status 400 (Bad Request) if the customerDTO is not valid,
     * or with status 500 (Internal Server Error) if the customerDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/customers")
    @Timed
    public ResponseEntity<CustomerDTO> updateCustomer(@RequestBody CustomerDTO customerDTO) throws URISyntaxException {
        log.debug("REST request to update Customer : {}", customerDTO);
        if (customerDTO.getIdCustomer() == null) {
            return createCustomer(customerDTO);
        }
        CustomerDTO result = customerService.save(customerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, customerDTO.getIdCustomer().toString()))
            .body(result);
    }

    /**
     * GET  /customers : get all the customers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of customers in body
     */
    @GetMapping("/customers")
    @Timed
    public ResponseEntity<List<CustomerDTO>> getAllCustomers(Pageable pageable) {
        log.debug("REST request to get a page of Customers");
        Page<CustomerDTO> page = customerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/customers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /customers/:id : get the "id" customer.
     *
     * @param id the id of the customerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the customerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/customers/{id}")
    @Timed
    public ResponseEntity<CustomerDTO> getCustomer(@PathVariable String id) {
        log.debug("REST request to get Customer : {}", id);
        CustomerDTO customerDTO = customerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(customerDTO));
    }

    @GetMapping("/customers/bill-to/{id}")
    @Timed
    public ResponseEntity<BillToDTO> getCustomerBillTo(@PathVariable String id) {
        log.debug("REST request to get Customer : {}", id);
        BillToDTO billToDTO = customerService.findBillTo(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(billToDTO));
    }

    /**
     * DELETE  /customers/:id : delete the "id" customer.
     *
     * @param id the id of the customerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/customers/{id}")
    @Timed
    public ResponseEntity<Void> deleteCustomer(@PathVariable String id) {
        log.debug("REST request to delete Customer : {}", id);
        customerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/customers?query=:query : search for the customer corresponding
     * to the query.
     *
     * @param query the query of the customer search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/customers")
    @Timed
    public ResponseEntity<List<CustomerDTO>> searchCustomers(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Customers for query {}", query);
        Page<CustomerDTO> page = customerService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/customers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
