package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.OrderPaymentService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.OrderPaymentDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing OrderPayment.
 */
@RestController
@RequestMapping("/api")
public class OrderPaymentResource {

    private final Logger log = LoggerFactory.getLogger(OrderPaymentResource.class);

    private static final String ENTITY_NAME = "orderPayment";

    private final OrderPaymentService orderPaymentService;

    public OrderPaymentResource(OrderPaymentService orderPaymentService) {
        this.orderPaymentService = orderPaymentService;
    }

    /**
     * POST  /order-payments : Create a new orderPayment.
     *
     * @param orderPaymentDTO the orderPaymentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new orderPaymentDTO, or with status 400 (Bad Request) if the orderPayment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-payments")
    @Timed
    public ResponseEntity<OrderPaymentDTO> createOrderPayment(@RequestBody OrderPaymentDTO orderPaymentDTO) throws URISyntaxException {
        log.debug("REST request to save OrderPayment : {}", orderPaymentDTO);
        if (orderPaymentDTO.getIdOrderPayment() != null) {
            throw new BadRequestAlertException("A new orderPayment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderPaymentDTO result = orderPaymentService.save(orderPaymentDTO);
        return ResponseEntity.created(new URI("/api/order-payments/" + result.getIdOrderPayment()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdOrderPayment().toString()))
            .body(result);
    }

    /**
     * POST  /order-payments/process : Execute Bussiness Process orderPayment.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-payments/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrderPayment(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process OrderPayment ");
        Map<String, Object> result = orderPaymentService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /order-payments/execute : Execute Bussiness Process orderPayment.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  orderPaymentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-payments/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedOrderPayment(HttpServletRequest request, @RequestBody OrderPaymentDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process OrderPayment");
        Map<String, Object> result = orderPaymentService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /order-payments/execute-list : Execute Bussiness Process orderPayment.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  orderPaymentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-payments/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListOrderPayment(HttpServletRequest request, @RequestBody Set<OrderPaymentDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List OrderPayment");
        Map<String, Object> result = orderPaymentService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /order-payments : Updates an existing orderPayment.
     *
     * @param orderPaymentDTO the orderPaymentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated orderPaymentDTO,
     * or with status 400 (Bad Request) if the orderPaymentDTO is not valid,
     * or with status 500 (Internal Server Error) if the orderPaymentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/order-payments")
    @Timed
    public ResponseEntity<OrderPaymentDTO> updateOrderPayment(@RequestBody OrderPaymentDTO orderPaymentDTO) throws URISyntaxException {
        log.debug("REST request to update OrderPayment : {}", orderPaymentDTO);
        if (orderPaymentDTO.getIdOrderPayment() == null) {
            return createOrderPayment(orderPaymentDTO);
        }
        OrderPaymentDTO result = orderPaymentService.save(orderPaymentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, orderPaymentDTO.getIdOrderPayment().toString()))
            .body(result);
    }

    /**
     * GET  /order-payments : get all the orderPayments.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of orderPayments in body
     */
    @GetMapping("/order-payments")
    @Timed
    public ResponseEntity<List<OrderPaymentDTO>> getAllOrderPayments(Pageable pageable) {
        log.debug("REST request to get a page of OrderPayments");
        Page<OrderPaymentDTO> page = orderPaymentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/order-payments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/order-payments/filterBy")
    @Timed
    public ResponseEntity<List<OrderPaymentDTO>> getAllFilteredOrderPayments(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of OrderPayments");
        Page<OrderPaymentDTO> page = orderPaymentService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/order-payments/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /order-payments/:id : get the "id" orderPayment.
     *
     * @param id the id of the orderPaymentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the orderPaymentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/order-payments/{id}")
    @Timed
    public ResponseEntity<OrderPaymentDTO> getOrderPayment(@PathVariable UUID id) {
        log.debug("REST request to get OrderPayment : {}", id);
        OrderPaymentDTO orderPaymentDTO = orderPaymentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(orderPaymentDTO));
    }

    /**
     * DELETE  /order-payments/:id : delete the "id" orderPayment.
     *
     * @param id the id of the orderPaymentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/order-payments/{id}")
    @Timed
    public ResponseEntity<Void> deleteOrderPayment(@PathVariable UUID id) {
        log.debug("REST request to delete OrderPayment : {}", id);
        orderPaymentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/order-payments?query=:query : search for the orderPayment corresponding
     * to the query.
     *
     * @param query the query of the orderPayment search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/order-payments")
    @Timed
    public ResponseEntity<List<OrderPaymentDTO>> searchOrderPayments(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of OrderPayments for query {}", query);
        Page<OrderPaymentDTO> page = orderPaymentService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/order-payments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
