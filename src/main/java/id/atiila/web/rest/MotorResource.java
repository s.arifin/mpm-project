package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.DateUtils;
import id.atiila.service.MotorService;
import id.atiila.service.dto.PriceComponentDTO;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.MotorDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * REST controller for managing Motor.
 */
@RestController
@RequestMapping("/api")
public class MotorResource {

    private final Logger log = LoggerFactory.getLogger(MotorResource.class);

    private static final String ENTITY_NAME = "motor";

    private final MotorService motorService;

    public MotorResource(MotorService motorService) {
        this.motorService = motorService;
    }

    @Autowired
    private DateUtils dateUtils;

    @GetMapping("/motors/price")
    @Timed
    public ResponseEntity<List<PriceComponentDTO>> getMotorPrice(@ApiParam String idproduct, @ApiParam String idinternal){
        List<PriceComponentDTO> l_dto = motorService.getMotorPrice(idproduct, idinternal);
        return new ResponseEntity<List<PriceComponentDTO>>(l_dto,null, HttpStatus.OK);
    }

    /**
     * POST  /motors : Create a new motor.
     *
     * @param motorDTO the motorDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new motorDTO, or with status 400 (Bad Request) if the motor has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/motors")
    @Timed
    public ResponseEntity<MotorDTO> createMotor(@RequestBody MotorDTO motorDTO) throws URISyntaxException {
        log.debug("REST request to save Motor : {}", motorDTO);
        if (motorDTO.getIdProduct() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new motor cannot already have an ID")).body(null);
        }
        MotorDTO result = motorService.save(motorDTO);

        return ResponseEntity.created(new URI("/api/motors/" + result.getIdProduct()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdProduct().toString()))
            .body(result);
    }

    /**
     * POST  /motors/execute : Execute Bussiness Process motor.
     *
     * @param motorDTO the motorDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  motorDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/motors/execute")
    @Timed
    public ResponseEntity<MotorDTO> executedMotor(@RequestBody MotorDTO motorDTO) throws URISyntaxException {
        log.debug("REST request to process Motor : {}", motorDTO);
        return new ResponseEntity<MotorDTO>(motorDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /motors : Updates an existing motor.
     *
     * @param motorDTO the motorDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated motorDTO,
     * or with status 400 (Bad Request) if the motorDTO is not valid,
     * or with status 500 (Internal Server Error) if the motorDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/motors")
    @Timed
    public ResponseEntity<MotorDTO> updateMotor(@RequestBody MotorDTO motorDTO) throws URISyntaxException {
        log.debug("REST request to update Motor : {}", motorDTO);
        if (motorDTO.getIdProduct() == null) {
            return createMotor(motorDTO);
        }
        MotorDTO result = motorService.save(motorDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, motorDTO.getIdProduct().toString()))
            .body(result);
    }

    @GetMapping("/motors/find-by-id-product-or-description")
    @Timed
    public ResponseEntity<List<MotorDTO>> findByIdProductOrDescription(@ApiParam String keyword){
        log.debug("REST request to get a page of Motors by keyword");
        List<MotorDTO> list = motorService.findMotorByKeyword(keyword);
        return new ResponseEntity<>(list, null, HttpStatus.OK);
    }

    /**
     * GET  /motors : get all the motors.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of motors in body
     */
    @GetMapping("/motors")
    @Timed
    public ResponseEntity<List<MotorDTO>> getAllMotors(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Motors");
        Page<MotorDTO> page = motorService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/motors");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /motors/:id : get the "id" motor.
     *
     * @param id the id of the motorDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the motorDTO, or with status 404 (Not Found)
     */
    @GetMapping("/motors/{id}")
    @Timed
    public ResponseEntity<MotorDTO> getMotor(@PathVariable String id) {
        log.debug("REST request to get Motor : {}", id);
        MotorDTO motorDTO = motorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(motorDTO));
    }

    /**
     * DELETE  /motors/:id : delete the "id" motor.
     *
     * @param id the id of the motorDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/motors/{id}")
    @Timed
    public ResponseEntity<Void> deleteMotor(@PathVariable String id) {
        log.debug("REST request to delete Motor : {}", id);
        motorService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/motors?query=:query : search for the motor corresponding
     * to the query.
     *
     * @param query the query of the motor search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/motors")
    @Timed
    public ResponseEntity<List<MotorDTO>> searchMotors(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Motors for query {}", query);
        Page<MotorDTO> page = motorService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/motors");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/motors/hot-item")
    @Timed
    public ResponseEntity<List<MotorDTO>> getMotorHotItem(){
        log.debug("REST request to get all hot item motor");
        List<MotorDTO> list = motorService.getHotItem();
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(list, headers, HttpStatus.OK);
    }

    @PostMapping("/motors/builddata")
    @Timed
    public ResponseEntity<String> executedMotor() throws URISyntaxException {
        log.debug("REST request to process Build Data Motor : {}");
        motorService.buildData();
        return new ResponseEntity<String>("ok", null, HttpStatus.ACCEPTED);
    }

    @PostMapping("/motors/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrderItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process Motor ");
        Map<String, Object> result = motorService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }
}
