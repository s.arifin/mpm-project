package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ReceiptService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ReceiptDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Receipt.
 */
@RestController
@RequestMapping("/api")
public class ReceiptResource {

    private final Logger log = LoggerFactory.getLogger(ReceiptResource.class);

    private static final String ENTITY_NAME = "receipt";

    private final ReceiptService receiptService;

    public ReceiptResource(ReceiptService receiptService) {
        this.receiptService = receiptService;
    }

    /**
     * POST  /receipts : Create a new receipt.
     *
     * @param receiptDTO the receiptDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new receiptDTO, or with status 400 (Bad Request) if the receipt has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/receipts")
    @Timed
    public ResponseEntity<ReceiptDTO> createReceipt(@RequestBody ReceiptDTO receiptDTO) throws URISyntaxException {
        log.debug("REST request to save Receipt : {}", receiptDTO);
        if (receiptDTO.getIdPayment() != null) {
            throw new BadRequestAlertException("A new receipt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ReceiptDTO result = receiptService.save(receiptDTO);
        return ResponseEntity.created(new URI("/api/receipts/" + result.getIdPayment()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPayment().toString()))
            .body(result);
    }

    /**
     * POST  /receipts/process : Execute Bussiness Process receipt.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/receipts/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processReceipt(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Receipt ");
        Map<String, Object> result = receiptService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /receipts/execute : Execute Bussiness Process receipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  receiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/receipts/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedReceipt(HttpServletRequest request, @RequestBody ReceiptDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Receipt");
        Map<String, Object> result = receiptService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /receipts/execute-list : Execute Bussiness Process receipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  receiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/receipts/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListReceipt(HttpServletRequest request, @RequestBody Set<ReceiptDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List Receipt");
        Map<String, Object> result = receiptService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /receipts : Updates an existing receipt.
     *
     * @param receiptDTO the receiptDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated receiptDTO,
     * or with status 400 (Bad Request) if the receiptDTO is not valid,
     * or with status 500 (Internal Server Error) if the receiptDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/receipts")
    @Timed
    public ResponseEntity<ReceiptDTO> updateReceipt(@RequestBody ReceiptDTO receiptDTO) throws URISyntaxException {
        log.debug("REST request to update Receipt : {}", receiptDTO);
        if (receiptDTO.getIdPayment() == null) {
            return createReceipt(receiptDTO);
        }
        ReceiptDTO result = receiptService.save(receiptDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, receiptDTO.getIdPayment().toString()))
            .body(result);
    }

    /**
     * GET  /receipts : get all the receipts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of receipts in body
     */
    @GetMapping("/receipts")
    @Timed
    public ResponseEntity<List<ReceiptDTO>> getAllReceipts(Pageable pageable) {
        log.debug("REST request to get a page of Receipts");
        Page<ReceiptDTO> page = receiptService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/receipts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/receipts/filterBy")
    @Timed
    public ResponseEntity<List<ReceiptDTO>> getAllFilteredReceipts(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Receipts");
        Page<ReceiptDTO> page = receiptService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/receipts/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /receipts/:id : get the "id" receipt.
     *
     * @param id the id of the receiptDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the receiptDTO, or with status 404 (Not Found)
     */
    @GetMapping("/receipts/{id}")
    @Timed
    public ResponseEntity<ReceiptDTO> getReceipt(@PathVariable UUID id) {
        log.debug("REST request to get Receipt : {}", id);
        ReceiptDTO receiptDTO = receiptService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(receiptDTO));
    }


    @GetMapping("/receipts/findByPaidFromId/{id}")
    @Timed
    public ResponseEntity<List<ReceiptDTO>> findByPaidFromId(@PathVariable String id) {
        log.debug("REST request to get Receipt : {}", id);
        List<ReceiptDTO> list = receiptService.findByPaidFromId(id);
        return new ResponseEntity<List<ReceiptDTO>>(list, null, HttpStatus.OK);
    }

    /**
     * DELETE  /receipts/:id : delete the "id" receipt.
     *
     * @param id the id of the receiptDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/receipts/{id}")
    @Timed
    public ResponseEntity<Void> deleteReceipt(@PathVariable UUID id) {
        log.debug("REST request to delete Receipt : {}", id);
        receiptService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/receipts?query=:query : search for the receipt corresponding
     * to the query.
     *
     * @param query the query of the receipt search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/receipts")
    @Timed
    public ResponseEntity<List<ReceiptDTO>> searchReceipts(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Receipts for query {}", query);
        Page<ReceiptDTO> page = receiptService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/receipts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/receipts/set-status/{id}")
    @Timed
    public ResponseEntity< ReceiptDTO> setStatusReceipt(@PathVariable Integer id, @RequestBody ReceiptDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status Receipt : {}", dto);
        ReceiptDTO r = receiptService.changeReceiptStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
