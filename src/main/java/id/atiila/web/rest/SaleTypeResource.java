package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.SaleTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.SaleTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SaleType.
 */
@RestController
@RequestMapping("/api")
public class SaleTypeResource {

    private final Logger log = LoggerFactory.getLogger(SaleTypeResource.class);

    private static final String ENTITY_NAME = "saleType";

    private final SaleTypeService saleTypeService;

    public SaleTypeResource(SaleTypeService saleTypeService) {
        this.saleTypeService = saleTypeService;
    }

    /**
     * POST  /sale-types : Create a new saleType.
     *
     * @param saleTypeDTO the saleTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new saleTypeDTO, or with status 400 (Bad Request) if the saleType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sale-types")
    @Timed
    public ResponseEntity<SaleTypeDTO> createSaleType(@RequestBody SaleTypeDTO saleTypeDTO) throws URISyntaxException {
        log.debug("REST request to save SaleType : {}", saleTypeDTO);
        if (saleTypeDTO.getIdSaleType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new saleType cannot already have an ID")).body(null);
        }
        SaleTypeDTO result = saleTypeService.save(saleTypeDTO);
        return ResponseEntity.created(new URI("/api/sale-types/" + result.getIdSaleType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdSaleType().toString()))
            .body(result);
    }

    /**
     * POST  /sale-types/execute{id}/{param} : Execute Bussiness Process saleType.
     *
     * @param saleTypeDTO the saleTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  saleTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sale-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<SaleTypeDTO> executedSaleType(@PathVariable Integer id, @PathVariable String param, @RequestBody SaleTypeDTO saleTypeDTO) throws URISyntaxException {
        log.debug("REST request to process SaleType : {}", saleTypeDTO);
        SaleTypeDTO result = saleTypeService.processExecuteData(id, param, saleTypeDTO);
        return new ResponseEntity<SaleTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /sale-types/execute-list{id}/{param} : Execute Bussiness Process saleType.
     *
     * @param saleTypeDTO the saleTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  saleTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sale-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<SaleTypeDTO>> executedListSaleType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<SaleTypeDTO> saleTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List SaleType");
        Set<SaleTypeDTO> result = saleTypeService.processExecuteListData(id, param, saleTypeDTO);
        return new ResponseEntity<Set<SaleTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /sale-types : Updates an existing saleType.
     *
     * @param saleTypeDTO the saleTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated saleTypeDTO,
     * or with status 400 (Bad Request) if the saleTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the saleTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sale-types")
    @Timed
    public ResponseEntity<SaleTypeDTO> updateSaleType(@RequestBody SaleTypeDTO saleTypeDTO) throws URISyntaxException {
        log.debug("REST request to update SaleType : {}", saleTypeDTO);
        if (saleTypeDTO.getIdSaleType() == null) {
            return createSaleType(saleTypeDTO);
        }
        SaleTypeDTO result = saleTypeService.save(saleTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, saleTypeDTO.getIdSaleType().toString()))
            .body(result);
    }

    /**
     * GET  /sale-types : get all the saleTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of saleTypes in body
     */
    @GetMapping("/sale-types")
    @Timed
    public ResponseEntity<List<SaleTypeDTO>> getAllSaleTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SaleTypes");
        Page<SaleTypeDTO> page = saleTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sale-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /sale-types/:id : get the "id" saleType.
     *
     * @param id the id of the saleTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the saleTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sale-types/{id}")
    @Timed
    public ResponseEntity<SaleTypeDTO> getSaleType(@PathVariable Integer id) {
        log.debug("REST request to get SaleType : {}", id);
        SaleTypeDTO saleTypeDTO = saleTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(saleTypeDTO));
    }

    /**
     * DELETE  /sale-types/:id : delete the "id" saleType.
     *
     * @param id the id of the saleTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sale-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteSaleType(@PathVariable Integer id) {
        log.debug("REST request to delete SaleType : {}", id);
        saleTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/sale-types?query=:query : search for the saleType corresponding
     * to the query.
     *
     * @param query the query of the saleType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/sale-types")
    @Timed
    public ResponseEntity<List<SaleTypeDTO>> searchSaleTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SaleTypes for query {}", query);
        Page<SaleTypeDTO> page = saleTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/sale-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
