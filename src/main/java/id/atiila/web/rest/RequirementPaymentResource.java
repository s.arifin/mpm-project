package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RequirementPaymentService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RequirementPaymentDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RequirementPayment.
 */
@RestController
@RequestMapping("/api")
public class RequirementPaymentResource {

    private final Logger log = LoggerFactory.getLogger(RequirementPaymentResource.class);

    private static final String ENTITY_NAME = "requirementPayment";

    private final RequirementPaymentService requirementPaymentService;

    public RequirementPaymentResource(RequirementPaymentService requirementPaymentService) {
        this.requirementPaymentService = requirementPaymentService;
    }

    /**
     * POST  /requirement-payments : Create a new requirementPayment.
     *
     * @param requirementPaymentDTO the requirementPaymentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new requirementPaymentDTO, or with status 400 (Bad Request) if the requirementPayment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requirement-payments")
    @Timed
    public ResponseEntity<RequirementPaymentDTO> createRequirementPayment(@RequestBody RequirementPaymentDTO requirementPaymentDTO) throws URISyntaxException {
        log.debug("REST request to save RequirementPayment : {}", requirementPaymentDTO);
        if (requirementPaymentDTO.getIdReqPayment() != null) {
            throw new BadRequestAlertException("A new requirementPayment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RequirementPaymentDTO result = requirementPaymentService.save(requirementPaymentDTO);
        return ResponseEntity.created(new URI("/api/requirement-payments/" + result.getIdReqPayment()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdReqPayment().toString()))
            .body(result);
    }

    /**
     * POST  /requirement-payments/process : Execute Bussiness Process requirementPayment.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requirement-payments/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processRequirementPayment(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RequirementPayment ");
        Map<String, Object> result = requirementPaymentService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /requirement-payments/execute : Execute Bussiness Process requirementPayment.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  requirementPaymentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requirement-payments/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedRequirementPayment(HttpServletRequest request, @RequestBody RequirementPaymentDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RequirementPayment");
        Map<String, Object> result = requirementPaymentService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /requirement-payments/execute-list : Execute Bussiness Process requirementPayment.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  requirementPaymentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requirement-payments/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListRequirementPayment(HttpServletRequest request, @RequestBody Set<RequirementPaymentDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List RequirementPayment");
        Map<String, Object> result = requirementPaymentService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /requirement-payments : Updates an existing requirementPayment.
     *
     * @param requirementPaymentDTO the requirementPaymentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated requirementPaymentDTO,
     * or with status 400 (Bad Request) if the requirementPaymentDTO is not valid,
     * or with status 500 (Internal Server Error) if the requirementPaymentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/requirement-payments")
    @Timed
    public ResponseEntity<RequirementPaymentDTO> updateRequirementPayment(@RequestBody RequirementPaymentDTO requirementPaymentDTO) throws URISyntaxException {
        log.debug("REST request to update RequirementPayment : {}", requirementPaymentDTO);
        if (requirementPaymentDTO.getIdReqPayment() == null) {
            return createRequirementPayment(requirementPaymentDTO);
        }
        RequirementPaymentDTO result = requirementPaymentService.save(requirementPaymentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, requirementPaymentDTO.getIdReqPayment().toString()))
            .body(result);
    }

    /**
     * GET  /requirement-payments : get all the requirementPayments.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of requirementPayments in body
     */
    @GetMapping("/requirement-payments")
    @Timed
    public ResponseEntity<List<RequirementPaymentDTO>> getAllRequirementPayments(Pageable pageable) {
        log.debug("REST request to get a page of RequirementPayments");
        Page<RequirementPaymentDTO> page = requirementPaymentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/requirement-payments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/requirement-payments/filterBy")
    @Timed
    public ResponseEntity<List<RequirementPaymentDTO>> getAllFilteredRequirementPayments(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of RequirementPayments");
        Page<RequirementPaymentDTO> page = requirementPaymentService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/requirement-payments/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /requirement-payments/:id : get the "id" requirementPayment.
     *
     * @param id the id of the requirementPaymentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the requirementPaymentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/requirement-payments/{id}")
    @Timed
    public ResponseEntity<RequirementPaymentDTO> getRequirementPayment(@PathVariable UUID id) {
        log.debug("REST request to get RequirementPayment : {}", id);
        RequirementPaymentDTO requirementPaymentDTO = requirementPaymentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(requirementPaymentDTO));
    }

    /**
     * DELETE  /requirement-payments/:id : delete the "id" requirementPayment.
     *
     * @param id the id of the requirementPaymentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/requirement-payments/{id}")
    @Timed
    public ResponseEntity<Void> deleteRequirementPayment(@PathVariable UUID id) {
        log.debug("REST request to delete RequirementPayment : {}", id);
        requirementPaymentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/requirement-payments?query=:query : search for the requirementPayment corresponding
     * to the query.
     *
     * @param query the query of the requirementPayment search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/requirement-payments")
    @Timed
    public ResponseEntity<List<RequirementPaymentDTO>> searchRequirementPayments(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of RequirementPayments for query {}", query);
        Page<RequirementPaymentDTO> page = requirementPaymentService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/requirement-payments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
