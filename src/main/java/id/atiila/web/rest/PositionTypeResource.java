package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PositionTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PositionTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PositionType.
 */
@RestController
@RequestMapping("/api")
public class PositionTypeResource {

    private final Logger log = LoggerFactory.getLogger(PositionTypeResource.class);

    private static final String ENTITY_NAME = "positionType";

    private final PositionTypeService positionTypeService;

    public PositionTypeResource(PositionTypeService positionTypeService) {
        this.positionTypeService = positionTypeService;
    }

    /**
     * POST  /position-types : Create a new positionType.
     *
     * @param positionTypeDTO the positionTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new positionTypeDTO, or with status 400 (Bad Request) if the positionType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/position-types")
    @Timed
    public ResponseEntity<PositionTypeDTO> createPositionType(@RequestBody PositionTypeDTO positionTypeDTO) throws URISyntaxException {
        log.debug("REST request to save PositionType : {}", positionTypeDTO);
        if (positionTypeDTO.getIdPositionType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new positionType cannot already have an ID")).body(null);
        }
        PositionTypeDTO result = positionTypeService.save(positionTypeDTO);
        return ResponseEntity.created(new URI("/api/position-types/" + result.getIdPositionType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPositionType().toString()))
            .body(result);
    }

    /**
     * POST  /position-types/execute{id}/{param} : Execute Bussiness Process positionType.
     *
     * @param positionTypeDTO the positionTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  positionTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/position-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<PositionTypeDTO> executedPositionType(@PathVariable Integer id, @PathVariable String param, @RequestBody PositionTypeDTO positionTypeDTO) throws URISyntaxException {
        log.debug("REST request to process PositionType : {}", positionTypeDTO);
        PositionTypeDTO result = positionTypeService.processExecuteData(id, param, positionTypeDTO);
        return new ResponseEntity<PositionTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /position-types/execute-list{id}/{param} : Execute Bussiness Process positionType.
     *
     * @param positionTypeDTO the positionTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  positionTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/position-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<PositionTypeDTO>> executedListPositionType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<PositionTypeDTO> positionTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List PositionType");
        Set<PositionTypeDTO> result = positionTypeService.processExecuteListData(id, param, positionTypeDTO);
        return new ResponseEntity<Set<PositionTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /position-types : Updates an existing positionType.
     *
     * @param positionTypeDTO the positionTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated positionTypeDTO,
     * or with status 400 (Bad Request) if the positionTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the positionTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/position-types")
    @Timed
    public ResponseEntity<PositionTypeDTO> updatePositionType(@RequestBody PositionTypeDTO positionTypeDTO) throws URISyntaxException {
        log.debug("REST request to update PositionType : {}", positionTypeDTO);
        if (positionTypeDTO.getIdPositionType() == null) {
            return createPositionType(positionTypeDTO);
        }
        PositionTypeDTO result = positionTypeService.save(positionTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, positionTypeDTO.getIdPositionType().toString()))
            .body(result);
    }

    /**
     * GET  /position-types : get all the positionTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of positionTypes in body
     */
    @GetMapping("/position-types")
    @Timed
    public ResponseEntity<List<PositionTypeDTO>> getAllPositionTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PositionTypes");
        Page<PositionTypeDTO> page = positionTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/position-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /position-types/:id : get the "id" positionType.
     *
     * @param id the id of the positionTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the positionTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/position-types/{id}")
    @Timed
    public ResponseEntity<PositionTypeDTO> getPositionType(@PathVariable Integer id) {
        log.debug("REST request to get PositionType : {}", id);
        PositionTypeDTO positionTypeDTO = positionTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(positionTypeDTO));
    }

    /**
     * DELETE  /position-types/:id : delete the "id" positionType.
     *
     * @param id the id of the positionTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/position-types/{id}")
    @Timed
    public ResponseEntity<Void> deletePositionType(@PathVariable Integer id) {
        log.debug("REST request to delete PositionType : {}", id);
        positionTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/position-types?query=:query : search for the positionType corresponding
     * to the query.
     *
     * @param query the query of the positionType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/position-types")
    @Timed
    public ResponseEntity<List<PositionTypeDTO>> searchPositionTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of PositionTypes for query {}", query);
        Page<PositionTypeDTO> page = positionTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/position-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/position-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processPositionType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process PositionType ");
        Map<String, Object> result = positionTypeService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
