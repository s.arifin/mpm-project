package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ContainerTypeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ContainerTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ContainerType.
 */
@RestController
@RequestMapping("/api")
public class ContainerTypeResource {

    private final Logger log = LoggerFactory.getLogger(ContainerTypeResource.class);

    private static final String ENTITY_NAME = "containerType";

    private final ContainerTypeService containerTypeService;

    public ContainerTypeResource(ContainerTypeService containerTypeService) {
        this.containerTypeService = containerTypeService;
    }

    /**
     * POST  /container-types : Create a new containerType.
     *
     * @param containerTypeDTO the containerTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new containerTypeDTO, or with status 400 (Bad Request) if the containerType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/container-types")
    @Timed
    public ResponseEntity<ContainerTypeDTO> createContainerType(@RequestBody ContainerTypeDTO containerTypeDTO) throws URISyntaxException {
        log.debug("REST request to save ContainerType : {}", containerTypeDTO);
        if (containerTypeDTO.getIdContainerType() != null) {
            throw new BadRequestAlertException("A new containerType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContainerTypeDTO result = containerTypeService.save(containerTypeDTO);
        return ResponseEntity.created(new URI("/api/container-types/" + result.getIdContainerType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdContainerType().toString()))
            .body(result);
    }

    /**
     * POST  /container-types/process : Execute Bussiness Process containerType.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/container-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processContainerType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ContainerType ");
        Map<String, Object> result = containerTypeService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /container-types/execute : Execute Bussiness Process containerType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  containerTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/container-types/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedContainerType(HttpServletRequest request, @RequestBody ContainerTypeDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ContainerType");
        Map<String, Object> result = containerTypeService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /container-types/execute-list : Execute Bussiness Process containerType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  containerTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/container-types/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListContainerType(HttpServletRequest request, @RequestBody Set<ContainerTypeDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List ContainerType");
        Map<String, Object> result = containerTypeService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /container-types : Updates an existing containerType.
     *
     * @param containerTypeDTO the containerTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated containerTypeDTO,
     * or with status 400 (Bad Request) if the containerTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the containerTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/container-types")
    @Timed
    public ResponseEntity<ContainerTypeDTO> updateContainerType(@RequestBody ContainerTypeDTO containerTypeDTO) throws URISyntaxException {
        log.debug("REST request to update ContainerType : {}", containerTypeDTO);
        if (containerTypeDTO.getIdContainerType() == null) {
            return createContainerType(containerTypeDTO);
        }
        ContainerTypeDTO result = containerTypeService.save(containerTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, containerTypeDTO.getIdContainerType().toString()))
            .body(result);
    }

    /**
     * GET  /container-types : get all the containerTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of containerTypes in body
     */
    @GetMapping("/container-types")
    @Timed
    public ResponseEntity<List<ContainerTypeDTO>> getAllContainerTypes(Pageable pageable) {
        log.debug("REST request to get a page of ContainerTypes");
        Page<ContainerTypeDTO> page = containerTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/container-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/container-types/filterBy")
    @Timed
    public ResponseEntity<List<ContainerTypeDTO>> getAllFilteredContainerTypes(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ContainerTypes");
        Page<ContainerTypeDTO> page = containerTypeService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/container-types/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /container-types/:id : get the "id" containerType.
     *
     * @param id the id of the containerTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the containerTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/container-types/{id}")
    @Timed
    public ResponseEntity<ContainerTypeDTO> getContainerType(@PathVariable Integer id) {
        log.debug("REST request to get ContainerType : {}", id);
        ContainerTypeDTO containerTypeDTO = containerTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(containerTypeDTO));
    }

    /**
     * DELETE  /container-types/:id : delete the "id" containerType.
     *
     * @param id the id of the containerTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/container-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteContainerType(@PathVariable Integer id) {
        log.debug("REST request to delete ContainerType : {}", id);
        containerTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/container-types?query=:query : search for the containerType corresponding
     * to the query.
     *
     * @param query the query of the containerType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/container-types")
    @Timed
    public ResponseEntity<List<ContainerTypeDTO>> searchContainerTypes(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ContainerTypes for query {}", query);
        Page<ContainerTypeDTO> page = containerTypeService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/container-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
