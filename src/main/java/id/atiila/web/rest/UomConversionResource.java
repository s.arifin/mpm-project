package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.UomConversionService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.UomConversionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UomConversion.
 */
@RestController
@RequestMapping("/api")
public class UomConversionResource {

    private final Logger log = LoggerFactory.getLogger(UomConversionResource.class);

    private static final String ENTITY_NAME = "uomConversion";

    private final UomConversionService uomConversionService;

    public UomConversionResource(UomConversionService uomConversionService) {
        this.uomConversionService = uomConversionService;
    }

    /**
     * POST  /uom-conversions : Create a new uomConversion.
     *
     * @param uomConversionDTO the uomConversionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new uomConversionDTO, or with status 400 (Bad Request) if the uomConversion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/uom-conversions")
    @Timed
    public ResponseEntity<UomConversionDTO> createUomConversion(@RequestBody UomConversionDTO uomConversionDTO) throws URISyntaxException {
        log.debug("REST request to save UomConversion : {}", uomConversionDTO);
        if (uomConversionDTO.getIdUomConversion() != null) {
            throw new BadRequestAlertException("A new uomConversion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UomConversionDTO result = uomConversionService.save(uomConversionDTO);
        return ResponseEntity.created(new URI("/api/uom-conversions/" + result.getIdUomConversion()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdUomConversion().toString()))
            .body(result);
    }

    /**
     * POST  /uom-conversions/process : Execute Bussiness Process uomConversion.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/uom-conversions/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processUomConversion(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process UomConversion ");
        Map<String, Object> result = uomConversionService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /uom-conversions/execute : Execute Bussiness Process uomConversion.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  uomConversionDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/uom-conversions/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedUomConversion(HttpServletRequest request, @RequestBody UomConversionDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process UomConversion");
        Map<String, Object> result = uomConversionService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /uom-conversions/execute-list : Execute Bussiness Process uomConversion.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  uomConversionDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/uom-conversions/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListUomConversion(HttpServletRequest request, @RequestBody Set<UomConversionDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List UomConversion");
        Map<String, Object> result = uomConversionService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /uom-conversions : Updates an existing uomConversion.
     *
     * @param uomConversionDTO the uomConversionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated uomConversionDTO,
     * or with status 400 (Bad Request) if the uomConversionDTO is not valid,
     * or with status 500 (Internal Server Error) if the uomConversionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/uom-conversions")
    @Timed
    public ResponseEntity<UomConversionDTO> updateUomConversion(@RequestBody UomConversionDTO uomConversionDTO) throws URISyntaxException {
        log.debug("REST request to update UomConversion : {}", uomConversionDTO);
        if (uomConversionDTO.getIdUomConversion() == null) {
            return createUomConversion(uomConversionDTO);
        }
        UomConversionDTO result = uomConversionService.save(uomConversionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, uomConversionDTO.getIdUomConversion().toString()))
            .body(result);
    }

    /**
     * GET  /uom-conversions : get all the uomConversions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of uomConversions in body
     */
    @GetMapping("/uom-conversions")
    @Timed
    public ResponseEntity<List<UomConversionDTO>> getAllUomConversions(Pageable pageable) {
        log.debug("REST request to get a page of UomConversions");
        Page<UomConversionDTO> page = uomConversionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/uom-conversions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/uom-conversions/filterBy")
    @Timed
    public ResponseEntity<List<UomConversionDTO>> getAllFilteredUomConversions(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of UomConversions");
        Page<UomConversionDTO> page = uomConversionService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/uom-conversions/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /uom-conversions/:id : get the "id" uomConversion.
     *
     * @param id the id of the uomConversionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the uomConversionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/uom-conversions/{id}")
    @Timed
    public ResponseEntity<UomConversionDTO> getUomConversion(@PathVariable Integer id) {
        log.debug("REST request to get UomConversion : {}", id);
        UomConversionDTO uomConversionDTO = uomConversionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(uomConversionDTO));
    }

    /**
     * DELETE  /uom-conversions/:id : delete the "id" uomConversion.
     *
     * @param id the id of the uomConversionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/uom-conversions/{id}")
    @Timed
    public ResponseEntity<Void> deleteUomConversion(@PathVariable Integer id) {
        log.debug("REST request to delete UomConversion : {}", id);
        uomConversionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/uom-conversions?query=:query : search for the uomConversion corresponding
     * to the query.
     *
     * @param query the query of the uomConversion search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/uom-conversions")
    @Timed
    public ResponseEntity<List<UomConversionDTO>> searchUomConversions(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of UomConversions for query {}", query);
        Page<UomConversionDTO> page = uomConversionService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/uom-conversions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
