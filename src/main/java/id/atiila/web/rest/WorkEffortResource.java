package id.atiila.web.rest;

import java.util.Map;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.WorkEffortService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.WorkEffortDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WorkEffort.
 */
@RestController
@RequestMapping("/api")
public class WorkEffortResource {

    private final Logger log = LoggerFactory.getLogger(WorkEffortResource.class);

    private static final String ENTITY_NAME = "workEffort";

    private final WorkEffortService workEffortService;

    public WorkEffortResource(WorkEffortService workEffortService) {
        this.workEffortService = workEffortService;
    }

    /**
     * POST  /work-efforts : Create a new workEffort.
     *
     * @param workEffortDTO the workEffortDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new workEffortDTO, or with status 400 (Bad Request) if the workEffort has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/work-efforts")
    @Timed
    public ResponseEntity<WorkEffortDTO> createWorkEffort(@RequestBody WorkEffortDTO workEffortDTO) throws URISyntaxException {
        log.debug("REST request to save WorkEffort : {}", workEffortDTO);
        if (workEffortDTO.getIdWe() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new workEffort cannot already have an ID")).body(null);
        }
        WorkEffortDTO result = workEffortService.save(workEffortDTO);

        return ResponseEntity.created(new URI("/api/work-efforts/" + result.getIdWe()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdWe().toString()))
            .body(result);
    }

    /**
     * POST  /work-efforts/execute : Execute Bussiness Process workEffort.
     *
     * @param workEffortDTO the workEffortDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  workEffortDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/work-efforts/execute")
    @Timed
    public ResponseEntity<WorkEffortDTO> executedWorkEffort(@RequestBody WorkEffortDTO workEffortDTO) throws URISyntaxException {
        log.debug("REST request to process WorkEffort : {}", workEffortDTO);
        return new ResponseEntity<WorkEffortDTO>(workEffortDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /work-efforts : Updates an existing workEffort.
     *
     * @param workEffortDTO the workEffortDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated workEffortDTO,
     * or with status 400 (Bad Request) if the workEffortDTO is not valid,
     * or with status 500 (Internal Server Error) if the workEffortDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/work-efforts")
    @Timed
    public ResponseEntity<WorkEffortDTO> updateWorkEffort(@RequestBody WorkEffortDTO workEffortDTO) throws URISyntaxException {
        log.debug("REST request to update WorkEffort : {}", workEffortDTO);
        if (workEffortDTO.getIdWe() == null) {
            return createWorkEffort(workEffortDTO);
        }
        WorkEffortDTO result = workEffortService.save(workEffortDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, workEffortDTO.getIdWe().toString()))
            .body(result);
    }

    /**
     * GET  /work-efforts : get all the workEfforts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of workEfforts in body
     */
    @GetMapping("/work-efforts")
    @Timed
    public ResponseEntity<List<WorkEffortDTO>> getAllWorkEfforts(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of WorkEfforts");
        Page<WorkEffortDTO> page = workEffortService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/work-efforts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /work-efforts/:id : get the "id" workEffort.
     *
     * @param id the id of the workEffortDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the workEffortDTO, or with status 404 (Not Found)
     */
    @GetMapping("/work-efforts/{id}")
    @Timed
    public ResponseEntity<WorkEffortDTO> getWorkEffort(@PathVariable UUID id) {
        log.debug("REST request to get WorkEffort : {}", id);
        WorkEffortDTO workEffortDTO = workEffortService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(workEffortDTO));
    }

    /**
     * DELETE  /work-efforts/:id : delete the "id" workEffort.
     *
     * @param id the id of the workEffortDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/work-efforts/{id}")
    @Timed
    public ResponseEntity<Void> deleteWorkEffort(@PathVariable UUID id) {
        log.debug("REST request to delete WorkEffort : {}", id);
        workEffortService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/work-efforts?query=:query : search for the workEffort corresponding
     * to the query.
     *
     * @param query the query of the workEffort search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/work-efforts")
    @Timed
    public ResponseEntity<List<WorkEffortDTO>> searchWorkEfforts(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of WorkEfforts for query {}", query);
        Page<WorkEffortDTO> page = workEffortService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/work-efforts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/work-efforts/set-status/{id}")
    @Timed
    public ResponseEntity< WorkEffortDTO> setStatusWorkEffort(@PathVariable Integer id, @RequestBody WorkEffortDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status WorkEffort : {}", dto);
        WorkEffortDTO r = workEffortService.changeWorkEffortStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    @PostMapping("/work-efforts/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processWorkEffort(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process WorkEffort ");
        Map<String, Object> result = workEffortService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
