package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.UserMediatorService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.UserMediatorDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UserMediator.
 */
@RestController
@RequestMapping("/api")
public class UserMediatorResource {

    private final Logger log = LoggerFactory.getLogger(UserMediatorResource.class);

    private static final String ENTITY_NAME = "userMediator";

    private final UserMediatorService userMediatorService;

    public UserMediatorResource(UserMediatorService userMediatorService) {
        this.userMediatorService = userMediatorService;
    }

    /**
     * POST  /user-mediators : Create a new userMediator.
     *
     * @param userMediatorDTO the userMediatorDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userMediatorDTO, or with status 400 (Bad Request) if the userMediator has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-mediators")
    @Timed
    public ResponseEntity<UserMediatorDTO> createUserMediator(@RequestBody UserMediatorDTO userMediatorDTO) throws URISyntaxException {
        log.debug("REST request to save UserMediator : {}", userMediatorDTO);
        if (userMediatorDTO.getIdUserMediator() != null) {
            throw new BadRequestAlertException("A new userMediator cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserMediatorDTO result = userMediatorService.save(userMediatorDTO);
        return ResponseEntity.created(new URI("/api/user-mediators/" + result.getIdUserMediator()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdUserMediator().toString()))
            .body(result);
    }

    /**
     * POST  /user-mediators/execute{id}/{param} : Execute Bussiness Process userMediator.
     *
     * @param userMediatorDTO the userMediatorDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  userMediatorDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-mediators/execute/{id}/{param}")
    @Timed
    public ResponseEntity<UserMediatorDTO> executedUserMediator(@PathVariable Integer id, @PathVariable String param, @RequestBody UserMediatorDTO userMediatorDTO) throws URISyntaxException {
        log.debug("REST request to process UserMediator : {}", userMediatorDTO);
        UserMediatorDTO result = userMediatorService.processExecuteData(id, param, userMediatorDTO);
        return new ResponseEntity<UserMediatorDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /user-mediators/execute-list{id}/{param} : Execute Bussiness Process userMediator.
     *
     * @param userMediatorDTO the userMediatorDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  userMediatorDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-mediators/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<UserMediatorDTO>> executedListUserMediator(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<UserMediatorDTO> userMediatorDTO) throws URISyntaxException {
        log.debug("REST request to process List UserMediator");
        Set<UserMediatorDTO> result = userMediatorService.processExecuteListData(id, param, userMediatorDTO);
        return new ResponseEntity<Set<UserMediatorDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /user-mediators : Updates an existing userMediator.
     *
     * @param userMediatorDTO the userMediatorDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userMediatorDTO,
     * or with status 400 (Bad Request) if the userMediatorDTO is not valid,
     * or with status 500 (Internal Server Error) if the userMediatorDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-mediators")
    @Timed
    public ResponseEntity<UserMediatorDTO> updateUserMediator(@RequestBody UserMediatorDTO userMediatorDTO) throws URISyntaxException {
        log.debug("REST request to update UserMediator : {}", userMediatorDTO);
        if (userMediatorDTO.getIdUserMediator() == null) {
            return createUserMediator(userMediatorDTO);
        }
        UserMediatorDTO result = userMediatorService.save(userMediatorDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userMediatorDTO.getIdUserMediator().toString()))
            .body(result);
    }

    /**
     * GET  /user-mediators : get all the userMediators.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of userMediators in body
     */
    @GetMapping("/user-mediators")
    @Timed
    public ResponseEntity<List<UserMediatorDTO>> getAllUserMediators(Pageable pageable) {
        log.debug("REST request to get a page of UserMediators");
        Page<UserMediatorDTO> page = userMediatorService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-mediators");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /user-mediators/:id : get the "id" userMediator.
     *
     * @param id the id of the userMediatorDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userMediatorDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-mediators/{id}")
    @Timed
    public ResponseEntity<UserMediatorDTO> getUserMediator(@PathVariable UUID id) {
        log.debug("REST request to get UserMediator : {}", id);
        UserMediatorDTO userMediatorDTO = userMediatorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userMediatorDTO));
    }

    /**
     * DELETE  /user-mediators/:id : delete the "id" userMediator.
     *
     * @param id the id of the userMediatorDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-mediators/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserMediator(@PathVariable UUID id) {
        log.debug("REST request to delete UserMediator : {}", id);
        userMediatorService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/user-mediators?query=:query : search for the userMediator corresponding
     * to the query.
     *
     * @param query the query of the userMediator search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/user-mediators")
    @Timed
    public ResponseEntity<List<UserMediatorDTO>> searchUserMediators(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of UserMediators for query {}", query);
        Page<UserMediatorDTO> page = userMediatorService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/user-mediators");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/user-mediators/set-status/{id}")
    @Timed
    public ResponseEntity< UserMediatorDTO> setStatusUserMediator(@PathVariable Integer id, @RequestBody UserMediatorDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status UserMediator : {}", dto);
        UserMediatorDTO r = userMediatorService.changeUserMediatorStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
