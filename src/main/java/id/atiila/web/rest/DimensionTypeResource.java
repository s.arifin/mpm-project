package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.DimensionTypeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.DimensionTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DimensionType.
 */
@RestController
@RequestMapping("/api")
public class DimensionTypeResource {

    private final Logger log = LoggerFactory.getLogger(DimensionTypeResource.class);

    private static final String ENTITY_NAME = "dimensionType";

    private final DimensionTypeService dimensionTypeService;

    public DimensionTypeResource(DimensionTypeService dimensionTypeService) {
        this.dimensionTypeService = dimensionTypeService;
    }

    /**
     * POST  /dimension-types : Create a new dimensionType.
     *
     * @param dimensionTypeDTO the dimensionTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dimensionTypeDTO, or with status 400 (Bad Request) if the dimensionType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dimension-types")
    @Timed
    public ResponseEntity<DimensionTypeDTO> createDimensionType(@RequestBody DimensionTypeDTO dimensionTypeDTO) throws URISyntaxException {
        log.debug("REST request to save DimensionType : {}", dimensionTypeDTO);
        if (dimensionTypeDTO.getIdDimensionType() != null) {
            throw new BadRequestAlertException("A new dimensionType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DimensionTypeDTO result = dimensionTypeService.save(dimensionTypeDTO);
        return ResponseEntity.created(new URI("/api/dimension-types/" + result.getIdDimensionType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdDimensionType().toString()))
            .body(result);
    }

    /**
     * POST  /dimension-types/process : Execute Bussiness Process dimensionType.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dimension-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processDimensionType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process DimensionType ");
        Map<String, Object> result = dimensionTypeService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /dimension-types/execute : Execute Bussiness Process dimensionType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  dimensionTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dimension-types/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedDimensionType(HttpServletRequest request, @RequestBody DimensionTypeDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process DimensionType");
        Map<String, Object> result = dimensionTypeService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /dimension-types/execute-list : Execute Bussiness Process dimensionType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  dimensionTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dimension-types/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListDimensionType(HttpServletRequest request, @RequestBody Set<DimensionTypeDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List DimensionType");
        Map<String, Object> result = dimensionTypeService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /dimension-types : Updates an existing dimensionType.
     *
     * @param dimensionTypeDTO the dimensionTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dimensionTypeDTO,
     * or with status 400 (Bad Request) if the dimensionTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the dimensionTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dimension-types")
    @Timed
    public ResponseEntity<DimensionTypeDTO> updateDimensionType(@RequestBody DimensionTypeDTO dimensionTypeDTO) throws URISyntaxException {
        log.debug("REST request to update DimensionType : {}", dimensionTypeDTO);
        if (dimensionTypeDTO.getIdDimensionType() == null) {
            return createDimensionType(dimensionTypeDTO);
        }
        DimensionTypeDTO result = dimensionTypeService.save(dimensionTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dimensionTypeDTO.getIdDimensionType().toString()))
            .body(result);
    }

    /**
     * GET  /dimension-types : get all the dimensionTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dimensionTypes in body
     */
    @GetMapping("/dimension-types")
    @Timed
    public ResponseEntity<List<DimensionTypeDTO>> getAllDimensionTypes(Pageable pageable) {
        log.debug("REST request to get a page of DimensionTypes");
        Page<DimensionTypeDTO> page = dimensionTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dimension-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/dimension-types/filterBy")
    @Timed
    public ResponseEntity<List<DimensionTypeDTO>> getAllFilteredDimensionTypes(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of DimensionTypes");
        Page<DimensionTypeDTO> page = dimensionTypeService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dimension-types/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /dimension-types/:id : get the "id" dimensionType.
     *
     * @param id the id of the dimensionTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dimensionTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/dimension-types/{id}")
    @Timed
    public ResponseEntity<DimensionTypeDTO> getDimensionType(@PathVariable Integer id) {
        log.debug("REST request to get DimensionType : {}", id);
        DimensionTypeDTO dimensionTypeDTO = dimensionTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dimensionTypeDTO));
    }

    /**
     * DELETE  /dimension-types/:id : delete the "id" dimensionType.
     *
     * @param id the id of the dimensionTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dimension-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteDimensionType(@PathVariable Integer id) {
        log.debug("REST request to delete DimensionType : {}", id);
        dimensionTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/dimension-types?query=:query : search for the dimensionType corresponding
     * to the query.
     *
     * @param query the query of the dimensionType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/dimension-types")
    @Timed
    public ResponseEntity<List<DimensionTypeDTO>> searchDimensionTypes(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of DimensionTypes for query {}", query);
        Page<DimensionTypeDTO> page = dimensionTypeService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/dimension-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
