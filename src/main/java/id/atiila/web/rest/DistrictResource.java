package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.DistrictService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.DistrictDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing District.
 */
@RestController
@RequestMapping("/api")
public class DistrictResource {

    private final Logger log = LoggerFactory.getLogger(DistrictResource.class);

    private static final String ENTITY_NAME = "district";

    private final DistrictService districtService;

    public DistrictResource(DistrictService districtService) {
        this.districtService = districtService;
    }

    /**
     * POST  /districts : Create a new district.
     *
     * @param districtDTO the districtDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new districtDTO, or with status 400 (Bad Request) if the district has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/districts")
    @Timed
    public ResponseEntity<DistrictDTO> createDistrict(@RequestBody DistrictDTO districtDTO) throws URISyntaxException {
        log.debug("REST request to save District : {}", districtDTO);
        if (districtDTO.getIdGeobou() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new district cannot already have an ID")).body(null);
        }
        DistrictDTO result = districtService.save(districtDTO);
        return ResponseEntity.created(new URI("/api/districts/" + result.getIdGeobou()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdGeobou().toString()))
            .body(result);
    }

    /**
     * POST  /districts/execute{id}/{param} : Execute Bussiness Process district.
     *
     * @param districtDTO the districtDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  districtDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/districts/execute/{id}/{param}")
    @Timed
    public ResponseEntity<DistrictDTO> executedDistrict(@PathVariable Integer id, @PathVariable String param, @RequestBody DistrictDTO districtDTO) throws URISyntaxException {
        log.debug("REST request to process District : {}", districtDTO);
        DistrictDTO result = districtService.processExecuteData(id, param, districtDTO);
        return new ResponseEntity<DistrictDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /districts/execute-list{id}/{param} : Execute Bussiness Process district.
     *
     * @param districtDTO the districtDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  districtDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/districts/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<DistrictDTO>> executedListDistrict(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<DistrictDTO> districtDTO) throws URISyntaxException {
        log.debug("REST request to process List District");
        Set<DistrictDTO> result = districtService.processExecuteListData(id, param, districtDTO);
        return new ResponseEntity<Set<DistrictDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /districts : Updates an existing district.
     *
     * @param districtDTO the districtDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated districtDTO,
     * or with status 400 (Bad Request) if the districtDTO is not valid,
     * or with status 500 (Internal Server Error) if the districtDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/districts")
    @Timed
    public ResponseEntity<DistrictDTO> updateDistrict(@RequestBody DistrictDTO districtDTO) throws URISyntaxException {
        log.debug("REST request to update District : {}", districtDTO);
        if (districtDTO.getIdGeobou() == null) {
            return createDistrict(districtDTO);
        }
        DistrictDTO result = districtService.save(districtDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, districtDTO.getIdGeobou().toString()))
            .body(result);
    }

    /**
     * GET  /districts : get all the districts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of districts in body
     */
    @GetMapping("/districts")
    @Timed
    public ResponseEntity<List<DistrictDTO>> getAllDistricts(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Districts");
        Page<DistrictDTO> page = districtService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/districts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/districts/by-city")
    @Timed
    public ResponseEntity<List<DistrictDTO>> getAllDistrictsByCity(@RequestParam String idcity,
                                                                   @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Districts");
        UUID idCity = idcity.equals("none") ? null : UUID.fromString(idcity);

        if (idCity == null) return getAllDistricts(pageable);

        List<DistrictDTO> list = districtService.findAllByCity(idCity);
        return new ResponseEntity<>(list, null, HttpStatus.OK);
    }

    /**
     * GET  /districts/:id : get the "id" district.
     *
     * @param id the id of the districtDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the districtDTO, or with status 404 (Not Found)
     */
    @GetMapping("/districts/{id}")
    @Timed
    public ResponseEntity<DistrictDTO> getDistrict(@PathVariable UUID id) {
        log.debug("REST request to get District : {}", id);
        DistrictDTO districtDTO = districtService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(districtDTO));
    }

    /**
     * DELETE  /districts/:id : delete the "id" district.
     *
     * @param id the id of the districtDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/districts/{id}")
    @Timed
    public ResponseEntity<Void> deleteDistrict(@PathVariable UUID id) {
        log.debug("REST request to delete District : {}", id);
        districtService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/districts?query=:query : search for the district corresponding
     * to the query.
     *
     * @param query the query of the district search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/districts")
    @Timed
    public ResponseEntity<List<DistrictDTO>> searchDistricts(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Districts for query {}", query);
        Page<DistrictDTO> page = districtService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/districts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
