package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RequestProductService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RequestProductDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Response;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RequestProduct.
 */
@RestController
@RequestMapping("/api")
public class RequestProductResource {

    private final Logger log = LoggerFactory.getLogger(RequestProductResource.class);

    private static final String ENTITY_NAME = "requestProduct";

    private final RequestProductService requestProductService;

    public RequestProductResource(RequestProductService requestProductService) {
        this.requestProductService = requestProductService;
    }

    /**
     * POST  /request-products : Create a new requestProduct.
     *
     * @param requestProductDTO the requestProductDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new requestProductDTO, or with status 400 (Bad Request) if the requestProduct has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-products")
    @Timed
    public ResponseEntity<RequestProductDTO> createRequestProduct(@RequestBody RequestProductDTO requestProductDTO) throws URISyntaxException {
        log.debug("REST request to save RequestProduct : {}", requestProductDTO);
        if (requestProductDTO.getIdRequest() != null) {
            throw new BadRequestAlertException("A new requestProduct cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RequestProductDTO result = requestProductService.save(requestProductDTO);
        return ResponseEntity.created(new URI("/api/request-products/" + result.getIdRequest()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequest().toString()))
            .body(result);
    }

    /**
     * POST  /request-products/process : Execute Bussiness Process requestProduct.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-products/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processRequestProduct(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RequestProduct ");
        Map<String, Object> result = requestProductService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /request-products/execute : Execute Bussiness Process requestProduct.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  requestProductDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-products/execute")
    @Timed
    public ResponseEntity<RequestProductDTO> executedRequestProduct(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RequestProduct");
        RequestProductDTO result = requestProductService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<RequestProductDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /request-products/execute-list : Execute Bussiness Process requestProduct.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  requestProductDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-products/execute-list")
    @Timed
    public ResponseEntity<Set<RequestProductDTO>> executedListRequestProduct(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List RequestProduct");
        Set<RequestProductDTO> result = requestProductService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<RequestProductDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /request-products : Updates an existing requestProduct.
     *
     * @param requestProductDTO the requestProductDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated requestProductDTO,
     * or with status 400 (Bad Request) if the requestProductDTO is not valid,
     * or with status 500 (Internal Server Error) if the requestProductDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/request-products")
    @Timed
    public ResponseEntity<RequestProductDTO> updateRequestProduct(@RequestBody RequestProductDTO requestProductDTO) throws URISyntaxException {
        log.debug("REST request to update RequestProduct : {}", requestProductDTO);
        if (requestProductDTO.getIdRequest() == null) {
            return createRequestProduct(requestProductDTO);
        }
        RequestProductDTO result = requestProductService.save(requestProductDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, requestProductDTO.getIdRequest().toString()))
            .body(result);
    }

    /**
     * GET  /request-products : get all the requestProducts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of requestProducts in body
     */
    @GetMapping("/request-products")
    @Timed
    public ResponseEntity<List<RequestProductDTO>> getAllRequestProducts(Pageable pageable) {
        log.debug("REST request to get a page of RequestProducts");
        Page<RequestProductDTO> page = requestProductService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/request-products");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/request-products/filterBy")
    @Timed
    public ResponseEntity<List<RequestProductDTO>> getAllFilteredRequestProducts(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of RequestProducts");
        Page<RequestProductDTO> page = requestProductService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/request-products/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /request-products/:id : get the "id" requestProduct.
     *
     * @param id the id of the requestProductDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the requestProductDTO, or with status 404 (Not Found)
     */
    @GetMapping("/request-products/{id}")
    @Timed
    public ResponseEntity<RequestProductDTO> getRequestProduct(@PathVariable UUID id) {
        log.debug("REST request to get RequestProduct : {}", id);
        RequestProductDTO requestProductDTO = requestProductService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(requestProductDTO));
    }

    /**
     * DELETE  /request-products/:id : delete the "id" requestProduct.
     *
     * @param id the id of the requestProductDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/request-products/{id}")
    @Timed
    public ResponseEntity<Void> deleteRequestProduct(@PathVariable UUID id) {
        log.debug("REST request to delete RequestProduct : {}", id);
        requestProductService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/request-products?query=:query : search for the requestProduct corresponding
     * to the query.
     *
     * @param query the query of the requestProduct search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/request-products")
    @Timed
    public ResponseEntity<List<RequestProductDTO>> searchRequestProducts(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of RequestProducts for query {}", query);
        Page<RequestProductDTO> page = requestProductService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/request-products");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/request-products/set-status/{id}")
    @Timed
    public ResponseEntity< RequestProductDTO> setStatusRequestProduct(@PathVariable Integer id, @RequestBody RequestProductDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status RequestProduct : {}", dto);
        RequestProductDTO r = requestProductService.changeRequestProductStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
