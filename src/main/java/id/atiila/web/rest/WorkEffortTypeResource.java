package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.WorkEffortTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.WorkEffortTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WorkEffortType.
 */
@RestController
@RequestMapping("/api")
public class WorkEffortTypeResource {

    private final Logger log = LoggerFactory.getLogger(WorkEffortTypeResource.class);

    private static final String ENTITY_NAME = "workEffortType";

    private final WorkEffortTypeService workEffortTypeService;

    public WorkEffortTypeResource(WorkEffortTypeService workEffortTypeService) {
        this.workEffortTypeService = workEffortTypeService;
    }

    /**
     * POST  /work-effort-types : Create a new workEffortType.
     *
     * @param workEffortTypeDTO the workEffortTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new workEffortTypeDTO, or with status 400 (Bad Request) if the workEffortType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/work-effort-types")
    @Timed
    public ResponseEntity<WorkEffortTypeDTO> createWorkEffortType(@RequestBody WorkEffortTypeDTO workEffortTypeDTO) throws URISyntaxException {
        log.debug("REST request to save WorkEffortType : {}", workEffortTypeDTO);
        if (workEffortTypeDTO.getIdWeType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new workEffortType cannot already have an ID")).body(null);
        }
        WorkEffortTypeDTO result = workEffortTypeService.save(workEffortTypeDTO);

        return ResponseEntity.created(new URI("/api/work-effort-types/" + result.getIdWeType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdWeType().toString()))
            .body(result);
    }

    /**
     * POST  /work-effort-types/execute : Execute Bussiness Process workEffortType.
     *
     * @param workEffortTypeDTO the workEffortTypeDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  workEffortTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/work-effort-types/execute")
    @Timed
    public ResponseEntity<WorkEffortTypeDTO> executedWorkEffortType(@RequestBody WorkEffortTypeDTO workEffortTypeDTO) throws URISyntaxException {
        log.debug("REST request to process WorkEffortType : {}", workEffortTypeDTO);
        return new ResponseEntity<WorkEffortTypeDTO>(workEffortTypeDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /work-effort-types : Updates an existing workEffortType.
     *
     * @param workEffortTypeDTO the workEffortTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated workEffortTypeDTO,
     * or with status 400 (Bad Request) if the workEffortTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the workEffortTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/work-effort-types")
    @Timed
    public ResponseEntity<WorkEffortTypeDTO> updateWorkEffortType(@RequestBody WorkEffortTypeDTO workEffortTypeDTO) throws URISyntaxException {
        log.debug("REST request to update WorkEffortType : {}", workEffortTypeDTO);
        if (workEffortTypeDTO.getIdWeType() == null) {
            return createWorkEffortType(workEffortTypeDTO);
        }
        WorkEffortTypeDTO result = workEffortTypeService.save(workEffortTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, workEffortTypeDTO.getIdWeType().toString()))
            .body(result);
    }

    /**
     * GET  /work-effort-types : get all the workEffortTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of workEffortTypes in body
     */
    @GetMapping("/work-effort-types")
    @Timed
    public ResponseEntity<List<WorkEffortTypeDTO>> getAllWorkEffortTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of WorkEffortTypes");
        Page<WorkEffortTypeDTO> page = workEffortTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/work-effort-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /work-effort-types/:id : get the "id" workEffortType.
     *
     * @param id the id of the workEffortTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the workEffortTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/work-effort-types/{id}")
    @Timed
    public ResponseEntity<WorkEffortTypeDTO> getWorkEffortType(@PathVariable Integer id) {
        log.debug("REST request to get WorkEffortType : {}", id);
        WorkEffortTypeDTO workEffortTypeDTO = workEffortTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(workEffortTypeDTO));
    }

    /**
     * DELETE  /work-effort-types/:id : delete the "id" workEffortType.
     *
     * @param id the id of the workEffortTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/work-effort-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteWorkEffortType(@PathVariable Integer id) {
        log.debug("REST request to delete WorkEffortType : {}", id);
        workEffortTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/work-effort-types?query=:query : search for the workEffortType corresponding
     * to the query.
     *
     * @param query the query of the workEffortType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/work-effort-types")
    @Timed
    public ResponseEntity<List<WorkEffortTypeDTO>> searchWorkEffortTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of WorkEffortTypes for query {}", query);
        Page<WorkEffortTypeDTO> page = workEffortTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/work-effort-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/work-effort-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processWorkEffortType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process WorkEffortType ");
        Map<String, Object> result = workEffortTypeService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
