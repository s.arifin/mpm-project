package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PaymentMethodService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PaymentMethodDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PaymentMethod.
 */
@RestController
@RequestMapping("/api")
public class PaymentMethodResource {

    private final Logger log = LoggerFactory.getLogger(PaymentMethodResource.class);

    private static final String ENTITY_NAME = "paymentMethod";

    private final PaymentMethodService paymentMethodService;

    public PaymentMethodResource(PaymentMethodService paymentMethodService) {
        this.paymentMethodService = paymentMethodService;
    }

    /**
     * POST  /payment-methods : Create a new paymentMethod.
     *
     * @param paymentMethodDTO the paymentMethodDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new paymentMethodDTO, or with status 400 (Bad Request) if the paymentMethod has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-methods")
    @Timed
    public ResponseEntity<PaymentMethodDTO> createPaymentMethod(@RequestBody PaymentMethodDTO paymentMethodDTO) throws URISyntaxException {
        log.debug("REST request to save PaymentMethod : {}", paymentMethodDTO);
        if (paymentMethodDTO.getIdPaymentMethod() != null) {
            throw new BadRequestAlertException("A new paymentMethod cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PaymentMethodDTO result = paymentMethodService.save(paymentMethodDTO);
        return ResponseEntity.created(new URI("/api/payment-methods/" + result.getIdPaymentMethod()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPaymentMethod().toString()))
            .body(result);
    }

    /**
     * POST  /payment-methods/process : Execute Bussiness Process paymentMethod.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-methods/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processPaymentMethod(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PaymentMethod ");
        Map<String, Object> result = paymentMethodService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /payment-methods/execute : Execute Bussiness Process paymentMethod.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  paymentMethodDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-methods/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedPaymentMethod(HttpServletRequest request, @RequestBody PaymentMethodDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PaymentMethod");
        Map<String, Object> result = paymentMethodService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /payment-methods/execute-list : Execute Bussiness Process paymentMethod.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  paymentMethodDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-methods/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListPaymentMethod(HttpServletRequest request, @RequestBody Set<PaymentMethodDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List PaymentMethod");
        Map<String, Object> result = paymentMethodService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /payment-methods : Updates an existing paymentMethod.
     *
     * @param paymentMethodDTO the paymentMethodDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated paymentMethodDTO,
     * or with status 400 (Bad Request) if the paymentMethodDTO is not valid,
     * or with status 500 (Internal Server Error) if the paymentMethodDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/payment-methods")
    @Timed
    public ResponseEntity<PaymentMethodDTO> updatePaymentMethod(@RequestBody PaymentMethodDTO paymentMethodDTO) throws URISyntaxException {
        log.debug("REST request to update PaymentMethod : {}", paymentMethodDTO);
        if (paymentMethodDTO.getIdPaymentMethod() == null) {
            return createPaymentMethod(paymentMethodDTO);
        }
        PaymentMethodDTO result = paymentMethodService.save(paymentMethodDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentMethodDTO.getIdPaymentMethod().toString()))
            .body(result);
    }

    /**
     * GET  /payment-methods : get all the paymentMethods.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of paymentMethods in body
     */
    @GetMapping("/payment-methods")
    @Timed
    public ResponseEntity<List<PaymentMethodDTO>> getAllPaymentMethods(Pageable pageable) {
        log.debug("REST request to get a page of PaymentMethods");
        Page<PaymentMethodDTO> page = paymentMethodService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/payment-methods");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/payment-methods/filterBy")
    @Timed
    public ResponseEntity<List<PaymentMethodDTO>> getAllFilteredPaymentMethods(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of PaymentMethods");
        Page<PaymentMethodDTO> page = paymentMethodService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/payment-methods/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /payment-methods/:id : get the "id" paymentMethod.
     *
     * @param id the id of the paymentMethodDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the paymentMethodDTO, or with status 404 (Not Found)
     */
    @GetMapping("/payment-methods/{id}")
    @Timed
    public ResponseEntity<PaymentMethodDTO> getPaymentMethod(@PathVariable Integer id) {
        log.debug("REST request to get PaymentMethod : {}", id);
        PaymentMethodDTO paymentMethodDTO = paymentMethodService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentMethodDTO));
    }

    /**
     * DELETE  /payment-methods/:id : delete the "id" paymentMethod.
     *
     * @param id the id of the paymentMethodDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/payment-methods/{id}")
    @Timed
    public ResponseEntity<Void> deletePaymentMethod(@PathVariable Integer id) {
        log.debug("REST request to delete PaymentMethod : {}", id);
        paymentMethodService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/payment-methods?query=:query : search for the paymentMethod corresponding
     * to the query.
     *
     * @param query the query of the paymentMethod search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/payment-methods")
    @Timed
    public ResponseEntity<List<PaymentMethodDTO>> searchPaymentMethods(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of PaymentMethods for query {}", query);
        Page<PaymentMethodDTO> page = paymentMethodService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/payment-methods");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
