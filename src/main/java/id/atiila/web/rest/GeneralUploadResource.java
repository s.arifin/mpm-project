package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.GeneralUploadService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.GeneralUploadDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing GeneralUpload.
 */
@RestController
@RequestMapping("/api")
public class GeneralUploadResource {

    private final Logger log = LoggerFactory.getLogger(GeneralUploadResource.class);

    private static final String ENTITY_NAME = "generalUpload";

    private final GeneralUploadService generalUploadService;

    public GeneralUploadResource(GeneralUploadService generalUploadService) {
        this.generalUploadService = generalUploadService;
    }

    /**
     * POST  /general-uploads : Create a new generalUpload.
     *
     * @param generalUploadDTO the generalUploadDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new generalUploadDTO, or with status 400 (Bad Request) if the generalUpload has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/general-uploads")
    @Timed
    public ResponseEntity<GeneralUploadDTO> createGeneralUpload(@RequestBody GeneralUploadDTO generalUploadDTO) throws URISyntaxException {
        log.debug("REST request to save GeneralUpload : {}", generalUploadDTO);
        if (generalUploadDTO.getIdGeneralUpload() != null) {
            throw new BadRequestAlertException("A new generalUpload cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GeneralUploadDTO result = generalUploadService.save(generalUploadDTO);
        return ResponseEntity.created(new URI("/api/general-uploads/" + result.getIdGeneralUpload()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdGeneralUpload().toString()))
            .body(result);
    }

    /**
     * POST  /general-uploads/process : Execute Bussiness Process generalUpload.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/general-uploads/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processGeneralUpload(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process GeneralUpload ");
        Map<String, Object> result = generalUploadService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /general-uploads/execute : Execute Bussiness Process generalUpload.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  generalUploadDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/general-uploads/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedGeneralUpload(HttpServletRequest request, @RequestBody GeneralUploadDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process GeneralUpload");
        Map<String, Object> result = generalUploadService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /general-uploads/execute-list : Execute Bussiness Process generalUpload.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  generalUploadDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/general-uploads/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListGeneralUpload(HttpServletRequest request, @RequestBody Set<GeneralUploadDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List GeneralUpload");
        Map<String, Object> result = generalUploadService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /general-uploads : Updates an existing generalUpload.
     *
     * @param generalUploadDTO the generalUploadDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated generalUploadDTO,
     * or with status 400 (Bad Request) if the generalUploadDTO is not valid,
     * or with status 500 (Internal Server Error) if the generalUploadDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/general-uploads")
    @Timed
    public ResponseEntity<GeneralUploadDTO> updateGeneralUpload(@RequestBody GeneralUploadDTO generalUploadDTO) throws URISyntaxException {
        log.debug("REST request to update GeneralUpload : {}", generalUploadDTO);
        if (generalUploadDTO.getIdGeneralUpload() == null) {
            return createGeneralUpload(generalUploadDTO);
        }
        GeneralUploadDTO result = generalUploadService.save(generalUploadDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, generalUploadDTO.getIdGeneralUpload().toString()))
            .body(result);
    }

    /**
     * GET  /general-uploads : get all the generalUploads.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of generalUploads in body
     */
    @GetMapping("/general-uploads")
    @Timed
    public ResponseEntity<List<GeneralUploadDTO>> getAllGeneralUploads(Pageable pageable) {
        log.debug("REST request to get a page of GeneralUploads");
        Page<GeneralUploadDTO> page = generalUploadService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/general-uploads");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/general-uploads/filterBy")
    @Timed
    public ResponseEntity<List<GeneralUploadDTO>> getAllFilteredGeneralUploads(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of GeneralUploads");
        Page<GeneralUploadDTO> page = generalUploadService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/general-uploads/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /general-uploads/:id : get the "id" generalUpload.
     *
     * @param id the id of the generalUploadDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the generalUploadDTO, or with status 404 (Not Found)
     */
    @GetMapping("/general-uploads/{id}")
    @Timed
    public ResponseEntity<GeneralUploadDTO> getGeneralUpload(@PathVariable UUID id) {
        log.debug("REST request to get GeneralUpload : {}", id);
        GeneralUploadDTO generalUploadDTO = generalUploadService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(generalUploadDTO));
    }

    /**
     * DELETE  /general-uploads/:id : delete the "id" generalUpload.
     *
     * @param id the id of the generalUploadDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/general-uploads/{id}")
    @Timed
    public ResponseEntity<Void> deleteGeneralUpload(@PathVariable UUID id) {
        log.debug("REST request to delete GeneralUpload : {}", id);
        generalUploadService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/general-uploads?query=:query : search for the generalUpload corresponding
     * to the query.
     *
     * @param query the query of the generalUpload search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/general-uploads")
    @Timed
    public ResponseEntity<List<GeneralUploadDTO>> searchGeneralUploads(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of GeneralUploads for query {}", query);
        Page<GeneralUploadDTO> page = generalUploadService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/general-uploads");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/general-uploads/set-status/{id}")
    @Timed
    public ResponseEntity< GeneralUploadDTO> setStatusGeneralUpload(@PathVariable Integer id, @RequestBody GeneralUploadDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status GeneralUpload : {}", dto);
        GeneralUploadDTO r = generalUploadService.changeGeneralUploadStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(500);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
