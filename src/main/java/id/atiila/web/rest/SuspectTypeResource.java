package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.SuspectTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.SuspectTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SuspectType.
 */
@RestController
@RequestMapping("/api")
public class SuspectTypeResource {

    private final Logger log = LoggerFactory.getLogger(SuspectTypeResource.class);

    private static final String ENTITY_NAME = "suspectType";

    private final SuspectTypeService suspectTypeService;

    public SuspectTypeResource(SuspectTypeService suspectTypeService) {
        this.suspectTypeService = suspectTypeService;
    }

    /**
     * POST  /suspect-types : Create a new suspectType.
     *
     * @param suspectTypeDTO the suspectTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new suspectTypeDTO, or with status 400 (Bad Request) if the suspectType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/suspect-types")
    @Timed
    public ResponseEntity<SuspectTypeDTO> createSuspectType(@RequestBody SuspectTypeDTO suspectTypeDTO) throws URISyntaxException {
        log.debug("REST request to save SuspectType : {}", suspectTypeDTO);
        if (suspectTypeDTO.getIdSuspectType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new suspectType cannot already have an ID")).body(null);
        }
        SuspectTypeDTO result = suspectTypeService.save(suspectTypeDTO);
        return ResponseEntity.created(new URI("/api/suspect-types/" + result.getIdSuspectType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdSuspectType().toString()))
            .body(result);
    }

    /**
     * POST  /suspect-types/execute{id}/{param} : Execute Bussiness Process suspectType.
     *
     * @param suspectTypeDTO the suspectTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  suspectTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/suspect-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<SuspectTypeDTO> executedSuspectType(@PathVariable Integer id, @PathVariable String param, @RequestBody SuspectTypeDTO suspectTypeDTO) throws URISyntaxException {
        log.debug("REST request to process SuspectType : {}", suspectTypeDTO);
        SuspectTypeDTO result = suspectTypeService.processExecuteData(id, param, suspectTypeDTO);
        return new ResponseEntity<SuspectTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /suspect-types/execute-list{id}/{param} : Execute Bussiness Process suspectType.
     *
     * @param suspectTypeDTO the suspectTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  suspectTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/suspect-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<SuspectTypeDTO>> executedListSuspectType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<SuspectTypeDTO> suspectTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List SuspectType");
        Set<SuspectTypeDTO> result = suspectTypeService.processExecuteListData(id, param, suspectTypeDTO);
        return new ResponseEntity<Set<SuspectTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /suspect-types : Updates an existing suspectType.
     *
     * @param suspectTypeDTO the suspectTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated suspectTypeDTO,
     * or with status 400 (Bad Request) if the suspectTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the suspectTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/suspect-types")
    @Timed
    public ResponseEntity<SuspectTypeDTO> updateSuspectType(@RequestBody SuspectTypeDTO suspectTypeDTO) throws URISyntaxException {
        log.debug("REST request to update SuspectType : {}", suspectTypeDTO);
        if (suspectTypeDTO.getIdSuspectType() == null) {
            return createSuspectType(suspectTypeDTO);
        }
        SuspectTypeDTO result = suspectTypeService.save(suspectTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, suspectTypeDTO.getIdSuspectType().toString()))
            .body(result);
    }

    /**
     * GET  /suspect-types : get all the suspectTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of suspectTypes in body
     */
    @GetMapping("/suspect-types")
    @Timed
    public ResponseEntity<List<SuspectTypeDTO>> getAllSuspectTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SuspectTypes");
        Page<SuspectTypeDTO> page = suspectTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/suspect-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /suspect-types/:id : get the "id" suspectType.
     *
     * @param id the id of the suspectTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the suspectTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/suspect-types/{id}")
    @Timed
    public ResponseEntity<SuspectTypeDTO> getSuspectType(@PathVariable Integer id) {
        log.debug("REST request to get SuspectType : {}", id);
        SuspectTypeDTO suspectTypeDTO = suspectTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(suspectTypeDTO));
    }

    /**
     * DELETE  /suspect-types/:id : delete the "id" suspectType.
     *
     * @param id the id of the suspectTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/suspect-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteSuspectType(@PathVariable Integer id) {
        log.debug("REST request to delete SuspectType : {}", id);
        suspectTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/suspect-types?query=:query : search for the suspectType corresponding
     * to the query.
     *
     * @param query the query of the suspectType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/suspect-types")
    @Timed
    public ResponseEntity<List<SuspectTypeDTO>> searchSuspectTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SuspectTypes for query {}", query);
        Page<SuspectTypeDTO> page = suspectTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/suspect-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
