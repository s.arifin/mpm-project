package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.SuspectOrganizationService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.SuspectOrganizationDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SuspectOrganization.
 */
@RestController
@RequestMapping("/api")
public class SuspectOrganizationResource {

    private final Logger log = LoggerFactory.getLogger(SuspectOrganizationResource.class);

    private static final String ENTITY_NAME = "suspectOrganization";

    private final SuspectOrganizationService suspectOrganizationService;

    public SuspectOrganizationResource(SuspectOrganizationService suspectOrganizationService) {
        this.suspectOrganizationService = suspectOrganizationService;
    }

    /**
     * POST  /suspect-organizations : Create a new suspectOrganization.
     *
     * @param suspectOrganizationDTO the suspectOrganizationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new suspectOrganizationDTO, or with status 400 (Bad Request) if the suspectOrganization has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/suspect-organizations")
    @Timed
    public ResponseEntity<SuspectOrganizationDTO> createSuspectOrganization(@RequestBody SuspectOrganizationDTO suspectOrganizationDTO) throws URISyntaxException {
        log.debug("REST request to save SuspectOrganization : {}", suspectOrganizationDTO);
        if (suspectOrganizationDTO.getIdSuspect() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new suspectOrganization cannot already have an ID")).body(null);
        }
        SuspectOrganizationDTO result = suspectOrganizationService.save(suspectOrganizationDTO);
        return ResponseEntity.created(new URI("/api/suspect-organizations/" + result.getIdSuspect()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdSuspect().toString()))
            .body(result);
    }

    /**
     * POST  /suspect-organizations/execute{id}/{param} : Execute Bussiness Process suspectOrganization.
     *
     * @param suspectOrganizationDTO the suspectOrganizationDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  suspectOrganizationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/suspect-organizations/execute/{id}/{param}")
    @Timed
    public ResponseEntity<SuspectOrganizationDTO> executedSuspectOrganization(@PathVariable Integer id, @PathVariable String param, @RequestBody SuspectOrganizationDTO suspectOrganizationDTO) throws URISyntaxException {
        log.debug("REST request to process SuspectOrganization : {}", suspectOrganizationDTO);
        SuspectOrganizationDTO result = suspectOrganizationService.processExecuteData(id, param, suspectOrganizationDTO);
        return new ResponseEntity<SuspectOrganizationDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /suspect-organizations/execute-list{id}/{param} : Execute Bussiness Process suspectOrganization.
     *
     * @param suspectOrganizationDTO the suspectOrganizationDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  suspectOrganizationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/suspect-organizations/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<SuspectOrganizationDTO>> executedListSuspectOrganization(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<SuspectOrganizationDTO> suspectOrganizationDTO) throws URISyntaxException {
        log.debug("REST request to process List SuspectOrganization");
        Set<SuspectOrganizationDTO> result = suspectOrganizationService.processExecuteListData(id, param, suspectOrganizationDTO);
        return new ResponseEntity<Set<SuspectOrganizationDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /suspect-organizations : Updates an existing suspectOrganization.
     *
     * @param suspectOrganizationDTO the suspectOrganizationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated suspectOrganizationDTO,
     * or with status 400 (Bad Request) if the suspectOrganizationDTO is not valid,
     * or with status 500 (Internal Server Error) if the suspectOrganizationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/suspect-organizations")
    @Timed
    public ResponseEntity<SuspectOrganizationDTO> updateSuspectOrganization(@RequestBody SuspectOrganizationDTO suspectOrganizationDTO) throws URISyntaxException {
        log.debug("REST request to update SuspectOrganization : {}", suspectOrganizationDTO);
        if (suspectOrganizationDTO.getIdSuspect() == null) {
            return createSuspectOrganization(suspectOrganizationDTO);
        }
        SuspectOrganizationDTO result = suspectOrganizationService.save(suspectOrganizationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, suspectOrganizationDTO.getIdSuspect().toString()))
            .body(result);
    }

    /**
     * GET  /suspect-organizations : get all the suspectOrganizations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of suspectOrganizations in body
     */
    @GetMapping("/suspect-organizations")
    @Timed
    public ResponseEntity<List<SuspectOrganizationDTO>> getAllSuspectOrganizations(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SuspectOrganizations");
        Page<SuspectOrganizationDTO> page = suspectOrganizationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/suspect-organizations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /suspect-organizations/:id : get the "id" suspectOrganization.
     *
     * @param id the id of the suspectOrganizationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the suspectOrganizationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/suspect-organizations/{id}")
    @Timed
    public ResponseEntity<SuspectOrganizationDTO> getSuspectOrganization(@PathVariable UUID id) {
        log.debug("REST request to get SuspectOrganization : {}", id);
        SuspectOrganizationDTO suspectOrganizationDTO = suspectOrganizationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(suspectOrganizationDTO));
    }

    /**
     * DELETE  /suspect-organizations/:id : delete the "id" suspectOrganization.
     *
     * @param id the id of the suspectOrganizationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/suspect-organizations/{id}")
    @Timed
    public ResponseEntity<Void> deleteSuspectOrganization(@PathVariable UUID id) {
        log.debug("REST request to delete SuspectOrganization : {}", id);
        suspectOrganizationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/suspect-organizations?query=:query : search for the suspectOrganization corresponding
     * to the query.
     *
     * @param query the query of the suspectOrganization search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/suspect-organizations")
    @Timed
    public ResponseEntity<List<SuspectOrganizationDTO>> searchSuspectOrganizations(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SuspectOrganizations for query {}", query);
        Page<SuspectOrganizationDTO> page = suspectOrganizationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/suspect-organizations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/suspect-organizations/set-status/{id}")
    @Timed
    public ResponseEntity< SuspectOrganizationDTO> setStatusSuspectOrganization(@PathVariable Integer id, @RequestBody SuspectOrganizationDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status SuspectOrganization : {}", dto);
        SuspectOrganizationDTO r = suspectOrganizationService.changeSuspectOrganizationStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
