package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ReturnPurchaseOrderService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ReturnPurchaseOrderDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ReturnPurchaseOrder.
 */
@RestController
@RequestMapping("/api")
public class ReturnPurchaseOrderResource {

    private final Logger log = LoggerFactory.getLogger(ReturnPurchaseOrderResource.class);

    private static final String ENTITY_NAME = "returnPurchaseOrder";

    private final ReturnPurchaseOrderService returnPurchaseOrderService;

    public ReturnPurchaseOrderResource(ReturnPurchaseOrderService returnPurchaseOrderService) {
        this.returnPurchaseOrderService = returnPurchaseOrderService;
    }

    /**
     * POST  /return-purchase-orders : Create a new returnPurchaseOrder.
     *
     * @param returnPurchaseOrderDTO the returnPurchaseOrderDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new returnPurchaseOrderDTO, or with status 400 (Bad Request) if the returnPurchaseOrder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/return-purchase-orders")
    @Timed
    public ResponseEntity<ReturnPurchaseOrderDTO> createReturnPurchaseOrder(@RequestBody ReturnPurchaseOrderDTO returnPurchaseOrderDTO) throws URISyntaxException {
        log.debug("REST request to save ReturnPurchaseOrder : {}", returnPurchaseOrderDTO);
        if (returnPurchaseOrderDTO.getIdOrder() != null) {
            throw new BadRequestAlertException("A new returnPurchaseOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ReturnPurchaseOrderDTO result = returnPurchaseOrderService.save(returnPurchaseOrderDTO);
        return ResponseEntity.created(new URI("/api/return-purchase-orders/" + result.getIdOrder()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdOrder().toString()))
            .body(result);
    }

    /**
     * POST  /return-purchase-orders/process : Execute Bussiness Process returnPurchaseOrder.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/return-purchase-orders/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processReturnPurchaseOrder(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ReturnPurchaseOrder ");
        Map<String, Object> result = returnPurchaseOrderService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /return-purchase-orders/execute : Execute Bussiness Process returnPurchaseOrder.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  returnPurchaseOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/return-purchase-orders/execute")
    @Timed
    public ResponseEntity<ReturnPurchaseOrderDTO> executedReturnPurchaseOrder(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ReturnPurchaseOrder");
        ReturnPurchaseOrderDTO result = returnPurchaseOrderService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<ReturnPurchaseOrderDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /return-purchase-orders/execute-list : Execute Bussiness Process returnPurchaseOrder.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  returnPurchaseOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/return-purchase-orders/execute-list")
    @Timed
    public ResponseEntity<Set<ReturnPurchaseOrderDTO>> executedListReturnPurchaseOrder(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List ReturnPurchaseOrder");
        Set<ReturnPurchaseOrderDTO> result = returnPurchaseOrderService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<ReturnPurchaseOrderDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /return-purchase-orders : Updates an existing returnPurchaseOrder.
     *
     * @param returnPurchaseOrderDTO the returnPurchaseOrderDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated returnPurchaseOrderDTO,
     * or with status 400 (Bad Request) if the returnPurchaseOrderDTO is not valid,
     * or with status 500 (Internal Server Error) if the returnPurchaseOrderDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/return-purchase-orders")
    @Timed
    public ResponseEntity<ReturnPurchaseOrderDTO> updateReturnPurchaseOrder(@RequestBody ReturnPurchaseOrderDTO returnPurchaseOrderDTO) throws URISyntaxException {
        log.debug("REST request to update ReturnPurchaseOrder : {}", returnPurchaseOrderDTO);
        if (returnPurchaseOrderDTO.getIdOrder() == null) {
            return createReturnPurchaseOrder(returnPurchaseOrderDTO);
        }
        ReturnPurchaseOrderDTO result = returnPurchaseOrderService.save(returnPurchaseOrderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, returnPurchaseOrderDTO.getIdOrder().toString()))
            .body(result);
    }

    /**
     * GET  /return-purchase-orders : get all the returnPurchaseOrders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of returnPurchaseOrders in body
     */
    @GetMapping("/return-purchase-orders")
    @Timed
    public ResponseEntity<List<ReturnPurchaseOrderDTO>> getAllReturnPurchaseOrders(Pageable pageable) {
        log.debug("REST request to get a page of ReturnPurchaseOrders");
        Page<ReturnPurchaseOrderDTO> page = returnPurchaseOrderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/return-purchase-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/return-purchase-orders/filterBy")
    @Timed
    public ResponseEntity<List<ReturnPurchaseOrderDTO>> getAllFilteredReturnPurchaseOrders(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ReturnPurchaseOrders");
        Page<ReturnPurchaseOrderDTO> page = returnPurchaseOrderService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/return-purchase-orders/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /return-purchase-orders/:id : get the "id" returnPurchaseOrder.
     *
     * @param id the id of the returnPurchaseOrderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the returnPurchaseOrderDTO, or with status 404 (Not Found)
     */
    @GetMapping("/return-purchase-orders/{id}")
    @Timed
    public ResponseEntity<ReturnPurchaseOrderDTO> getReturnPurchaseOrder(@PathVariable UUID id) {
        log.debug("REST request to get ReturnPurchaseOrder : {}", id);
        ReturnPurchaseOrderDTO returnPurchaseOrderDTO = returnPurchaseOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(returnPurchaseOrderDTO));
    }

    /**
     * DELETE  /return-purchase-orders/:id : delete the "id" returnPurchaseOrder.
     *
     * @param id the id of the returnPurchaseOrderDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/return-purchase-orders/{id}")
    @Timed
    public ResponseEntity<Void> deleteReturnPurchaseOrder(@PathVariable UUID id) {
        log.debug("REST request to delete ReturnPurchaseOrder : {}", id);
        returnPurchaseOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/return-purchase-orders?query=:query : search for the returnPurchaseOrder corresponding
     * to the query.
     *
     * @param query the query of the returnPurchaseOrder search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/return-purchase-orders")
    @Timed
    public ResponseEntity<List<ReturnPurchaseOrderDTO>> searchReturnPurchaseOrders(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ReturnPurchaseOrders for query {}", query);
        Page<ReturnPurchaseOrderDTO> page = returnPurchaseOrderService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/return-purchase-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/return-purchase-orders/set-status/{id}")
    @Timed
    public ResponseEntity< ReturnPurchaseOrderDTO> setStatusReturnPurchaseOrder(@PathVariable Integer id, @RequestBody ReturnPurchaseOrderDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status ReturnPurchaseOrder : {}", dto);
        ReturnPurchaseOrderDTO r = returnPurchaseOrderService.changeReturnPurchaseOrderStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
