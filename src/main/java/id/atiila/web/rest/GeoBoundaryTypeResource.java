package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.GeoBoundaryTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.GeoBoundaryTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing GeoBoundaryType.
 */
@RestController
@RequestMapping("/api")
public class GeoBoundaryTypeResource {

    private final Logger log = LoggerFactory.getLogger(GeoBoundaryTypeResource.class);

    private static final String ENTITY_NAME = "geoBoundaryType";

    private final GeoBoundaryTypeService geoBoundaryTypeService;

    public GeoBoundaryTypeResource(GeoBoundaryTypeService geoBoundaryTypeService) {
        this.geoBoundaryTypeService = geoBoundaryTypeService;
    }

    /**
     * POST  /geo-boundary-types : Create a new geoBoundaryType.
     *
     * @param geoBoundaryTypeDTO the geoBoundaryTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new geoBoundaryTypeDTO, or with status 400 (Bad Request) if the geoBoundaryType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/geo-boundary-types")
    @Timed
    public ResponseEntity<GeoBoundaryTypeDTO> createGeoBoundaryType(@RequestBody GeoBoundaryTypeDTO geoBoundaryTypeDTO) throws URISyntaxException {
        log.debug("REST request to save GeoBoundaryType : {}", geoBoundaryTypeDTO);
        if (geoBoundaryTypeDTO.getIdGeobouType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new geoBoundaryType cannot already have an ID")).body(null);
        }
        GeoBoundaryTypeDTO result = geoBoundaryTypeService.save(geoBoundaryTypeDTO);
        return ResponseEntity.created(new URI("/api/geo-boundary-types/" + result.getIdGeobouType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdGeobouType().toString()))
            .body(result);
    }

    /**
     * POST  /geo-boundary-types/execute{id}/{param} : Execute Bussiness Process geoBoundaryType.
     *
     * @param geoBoundaryTypeDTO the geoBoundaryTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  geoBoundaryTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/geo-boundary-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<GeoBoundaryTypeDTO> executedGeoBoundaryType(@PathVariable Integer id, @PathVariable String param, @RequestBody GeoBoundaryTypeDTO geoBoundaryTypeDTO) throws URISyntaxException {
        log.debug("REST request to process GeoBoundaryType : {}", geoBoundaryTypeDTO);
        GeoBoundaryTypeDTO result = geoBoundaryTypeService.processExecuteData(id, param, geoBoundaryTypeDTO);
        return new ResponseEntity<GeoBoundaryTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /geo-boundary-types/execute-list{id}/{param} : Execute Bussiness Process geoBoundaryType.
     *
     * @param geoBoundaryTypeDTO the geoBoundaryTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  geoBoundaryTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/geo-boundary-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<GeoBoundaryTypeDTO>> executedListGeoBoundaryType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<GeoBoundaryTypeDTO> geoBoundaryTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List GeoBoundaryType");
        Set<GeoBoundaryTypeDTO> result = geoBoundaryTypeService.processExecuteListData(id, param, geoBoundaryTypeDTO);
        return new ResponseEntity<Set<GeoBoundaryTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /geo-boundary-types : Updates an existing geoBoundaryType.
     *
     * @param geoBoundaryTypeDTO the geoBoundaryTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated geoBoundaryTypeDTO,
     * or with status 400 (Bad Request) if the geoBoundaryTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the geoBoundaryTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/geo-boundary-types")
    @Timed
    public ResponseEntity<GeoBoundaryTypeDTO> updateGeoBoundaryType(@RequestBody GeoBoundaryTypeDTO geoBoundaryTypeDTO) throws URISyntaxException {
        log.debug("REST request to update GeoBoundaryType : {}", geoBoundaryTypeDTO);
        if (geoBoundaryTypeDTO.getIdGeobouType() == null) {
            return createGeoBoundaryType(geoBoundaryTypeDTO);
        }
        GeoBoundaryTypeDTO result = geoBoundaryTypeService.save(geoBoundaryTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, geoBoundaryTypeDTO.getIdGeobouType().toString()))
            .body(result);
    }

    /**
     * GET  /geo-boundary-types : get all the geoBoundaryTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of geoBoundaryTypes in body
     */
    @GetMapping("/geo-boundary-types")
    @Timed
    public ResponseEntity<List<GeoBoundaryTypeDTO>> getAllGeoBoundaryTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of GeoBoundaryTypes");
        Page<GeoBoundaryTypeDTO> page = geoBoundaryTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/geo-boundary-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /geo-boundary-types/:id : get the "id" geoBoundaryType.
     *
     * @param id the id of the geoBoundaryTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the geoBoundaryTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/geo-boundary-types/{id}")
    @Timed
    public ResponseEntity<GeoBoundaryTypeDTO> getGeoBoundaryType(@PathVariable Integer id) {
        log.debug("REST request to get GeoBoundaryType : {}", id);
        GeoBoundaryTypeDTO geoBoundaryTypeDTO = geoBoundaryTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(geoBoundaryTypeDTO));
    }

    /**
     * DELETE  /geo-boundary-types/:id : delete the "id" geoBoundaryType.
     *
     * @param id the id of the geoBoundaryTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/geo-boundary-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteGeoBoundaryType(@PathVariable Integer id) {
        log.debug("REST request to delete GeoBoundaryType : {}", id);
        geoBoundaryTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/geo-boundary-types?query=:query : search for the geoBoundaryType corresponding
     * to the query.
     *
     * @param query the query of the geoBoundaryType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/geo-boundary-types")
    @Timed
    public ResponseEntity<List<GeoBoundaryTypeDTO>> searchGeoBoundaryTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of GeoBoundaryTypes for query {}", query);
        Page<GeoBoundaryTypeDTO> page = geoBoundaryTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/geo-boundary-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
