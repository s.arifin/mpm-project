package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.DealerClaimTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.DealerClaimTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DealerClaimType.
 */
@RestController
@RequestMapping("/api")
public class DealerClaimTypeResource {

    private final Logger log = LoggerFactory.getLogger(DealerClaimTypeResource.class);

    private static final String ENTITY_NAME = "dealerClaimType";

    private final DealerClaimTypeService dealerClaimTypeService;

    public DealerClaimTypeResource(DealerClaimTypeService dealerClaimTypeService) {
        this.dealerClaimTypeService = dealerClaimTypeService;
    }

    /**
     * POST  /dealer-claim-types : Create a new dealerClaimType.
     *
     * @param dealerClaimTypeDTO the dealerClaimTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dealerClaimTypeDTO, or with status 400 (Bad Request) if the dealerClaimType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dealer-claim-types")
    @Timed
    public ResponseEntity<DealerClaimTypeDTO> createDealerClaimType(@RequestBody DealerClaimTypeDTO dealerClaimTypeDTO) throws URISyntaxException {
        log.debug("REST request to save DealerClaimType : {}", dealerClaimTypeDTO);
        if (dealerClaimTypeDTO.getIdClaimType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dealerClaimType cannot already have an ID")).body(null);
        }
        DealerClaimTypeDTO result = dealerClaimTypeService.save(dealerClaimTypeDTO);
        return ResponseEntity.created(new URI("/api/dealer-claim-types/" + result.getIdClaimType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdClaimType().toString()))
            .body(result);
    }

    /**
     * POST  /dealer-claim-types/execute{id}/{param} : Execute Bussiness Process dealerClaimType.
     *
     * @param dealerClaimTypeDTO the dealerClaimTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  dealerClaimTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dealer-claim-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<DealerClaimTypeDTO> executedDealerClaimType(@PathVariable Integer id, @PathVariable String param, @RequestBody DealerClaimTypeDTO dealerClaimTypeDTO) throws URISyntaxException {
        log.debug("REST request to process DealerClaimType : {}", dealerClaimTypeDTO);
        DealerClaimTypeDTO result = dealerClaimTypeService.processExecuteData(id, param, dealerClaimTypeDTO);
        return new ResponseEntity<DealerClaimTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /dealer-claim-types/execute-list{id}/{param} : Execute Bussiness Process dealerClaimType.
     *
     * @param dealerClaimTypeDTO the dealerClaimTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  dealerClaimTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dealer-claim-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<DealerClaimTypeDTO>> executedListDealerClaimType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<DealerClaimTypeDTO> dealerClaimTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List DealerClaimType");
        Set<DealerClaimTypeDTO> result = dealerClaimTypeService.processExecuteListData(id, param, dealerClaimTypeDTO);
        return new ResponseEntity<Set<DealerClaimTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /dealer-claim-types : Updates an existing dealerClaimType.
     *
     * @param dealerClaimTypeDTO the dealerClaimTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dealerClaimTypeDTO,
     * or with status 400 (Bad Request) if the dealerClaimTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the dealerClaimTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dealer-claim-types")
    @Timed
    public ResponseEntity<DealerClaimTypeDTO> updateDealerClaimType(@RequestBody DealerClaimTypeDTO dealerClaimTypeDTO) throws URISyntaxException {
        log.debug("REST request to update DealerClaimType : {}", dealerClaimTypeDTO);
        if (dealerClaimTypeDTO.getIdClaimType() == null) {
            return createDealerClaimType(dealerClaimTypeDTO);
        }
        DealerClaimTypeDTO result = dealerClaimTypeService.save(dealerClaimTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dealerClaimTypeDTO.getIdClaimType().toString()))
            .body(result);
    }

    /**
     * GET  /dealer-claim-types : get all the dealerClaimTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dealerClaimTypes in body
     */
    @GetMapping("/dealer-claim-types")
    @Timed
    public ResponseEntity<List<DealerClaimTypeDTO>> getAllDealerClaimTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DealerClaimTypes");
        Page<DealerClaimTypeDTO> page = dealerClaimTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dealer-claim-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /dealer-claim-types/:id : get the "id" dealerClaimType.
     *
     * @param id the id of the dealerClaimTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dealerClaimTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/dealer-claim-types/{id}")
    @Timed
    public ResponseEntity<DealerClaimTypeDTO> getDealerClaimType(@PathVariable Integer id) {
        log.debug("REST request to get DealerClaimType : {}", id);
        DealerClaimTypeDTO dealerClaimTypeDTO = dealerClaimTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dealerClaimTypeDTO));
    }

    /**
     * DELETE  /dealer-claim-types/:id : delete the "id" dealerClaimType.
     *
     * @param id the id of the dealerClaimTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dealer-claim-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteDealerClaimType(@PathVariable Integer id) {
        log.debug("REST request to delete DealerClaimType : {}", id);
        dealerClaimTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/dealer-claim-types?query=:query : search for the dealerClaimType corresponding
     * to the query.
     *
     * @param query the query of the dealerClaimType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/dealer-claim-types")
    @Timed
    public ResponseEntity<List<DealerClaimTypeDTO>> searchDealerClaimTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DealerClaimTypes for query {}", query);
        Page<DealerClaimTypeDTO> page = dealerClaimTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/dealer-claim-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
