package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.LeasingCompanyService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.LeasingCompanyDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing LeasingCompany.
 */
@RestController
@RequestMapping("/api")
public class LeasingCompanyResource {

    private final Logger log = LoggerFactory.getLogger(LeasingCompanyResource.class);

    private static final String ENTITY_NAME = "leasingCompany";

    private final LeasingCompanyService leasingCompanyService;

    public LeasingCompanyResource(LeasingCompanyService leasingCompanyService) {
        this.leasingCompanyService = leasingCompanyService;
    }

    /**
     * POST  /leasing-companies : Create a new leasingCompany.
     *
     * @param leasingCompanyDTO the leasingCompanyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new leasingCompanyDTO, or with status 400 (Bad Request) if the leasingCompany has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/leasing-companies")
    @Timed
    public ResponseEntity<LeasingCompanyDTO> createLeasingCompany(@RequestBody LeasingCompanyDTO leasingCompanyDTO) throws URISyntaxException {
        log.debug("REST request to save LeasingCompany : {}", leasingCompanyDTO);
        if (leasingCompanyDTO.getIdPartyRole() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new leasingCompany cannot already have an ID")).body(null);
        }
        LeasingCompanyDTO result = leasingCompanyService.save(leasingCompanyDTO);
        return ResponseEntity.created(new URI("/api/leasing-companies/" + result.getIdPartyRole()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPartyRole().toString()))
            .body(result);
    }

    /**
     * POST  /leasing-companies/execute{id}/{param} : Execute Bussiness Process leasingCompany.
     *
     * @param leasingCompanyDTO the leasingCompanyDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  leasingCompanyDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/leasing-companies/execute/{id}/{param}")
    @Timed
    public ResponseEntity<LeasingCompanyDTO> executedLeasingCompany(@PathVariable Integer id, @PathVariable String param, @RequestBody LeasingCompanyDTO leasingCompanyDTO) throws URISyntaxException {
        log.debug("REST request to process LeasingCompany : {}", leasingCompanyDTO);
        LeasingCompanyDTO result = leasingCompanyService.processExecuteData(id, param, leasingCompanyDTO);
        return new ResponseEntity<LeasingCompanyDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /leasing-companies/execute-list{id}/{param} : Execute Bussiness Process leasingCompany.
     *
     * @param leasingCompanyDTO the leasingCompanyDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  leasingCompanyDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/leasing-companies/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<LeasingCompanyDTO>> executedListLeasingCompany(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<LeasingCompanyDTO> leasingCompanyDTO) throws URISyntaxException {
        log.debug("REST request to process List LeasingCompany");
        Set<LeasingCompanyDTO> result = leasingCompanyService.processExecuteListData(id, param, leasingCompanyDTO);
        return new ResponseEntity<Set<LeasingCompanyDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /leasing-companies : Updates an existing leasingCompany.
     *
     * @param leasingCompanyDTO the leasingCompanyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated leasingCompanyDTO,
     * or with status 400 (Bad Request) if the leasingCompanyDTO is not valid,
     * or with status 500 (Internal Server Error) if the leasingCompanyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/leasing-companies")
    @Timed
    public ResponseEntity<LeasingCompanyDTO> updateLeasingCompany(@RequestBody LeasingCompanyDTO leasingCompanyDTO) throws URISyntaxException {
        log.debug("REST request to update LeasingCompany : {}", leasingCompanyDTO);
        if (leasingCompanyDTO.getIdPartyRole() == null) {
            return createLeasingCompany(leasingCompanyDTO);
        }
        LeasingCompanyDTO result = leasingCompanyService.save(leasingCompanyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, leasingCompanyDTO.getIdPartyRole().toString()))
            .body(result);
    }

    /**
     * GET  /leasing-companies : get all the leasingCompanies.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of leasingCompanies in body
     */
    @GetMapping("/leasing-companies")
    @Timed
    public ResponseEntity<List<LeasingCompanyDTO>> getAllLeasingCompanies(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of LeasingCompanies");
        Page<LeasingCompanyDTO> page = leasingCompanyService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/leasing-companies");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /leasing-companies/:id : get the "id" leasingCompany.
     *
     * @param id the id of the leasingCompanyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the leasingCompanyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/leasing-companies/{id}")
    @Timed
    public ResponseEntity<LeasingCompanyDTO> getLeasingCompany(@PathVariable UUID id) {
        log.debug("REST request to get LeasingCompany : {}", id);
        LeasingCompanyDTO leasingCompanyDTO = leasingCompanyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(leasingCompanyDTO));
    }

    /**
     * DELETE  /leasing-companies/:id : delete the "id" leasingCompany.
     *
     * @param id the id of the leasingCompanyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/leasing-companies/{id}")
    @Timed
    public ResponseEntity<Void> deleteLeasingCompany(@PathVariable UUID id) {
        log.debug("REST request to delete LeasingCompany : {}", id);
        leasingCompanyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/leasing-companies?query=:query : search for the leasingCompany corresponding
     * to the query.
     *
     * @param query the query of the leasingCompany search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/leasing-companies")
    @Timed
    public ResponseEntity<List<LeasingCompanyDTO>> searchLeasingCompanies(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of LeasingCompanies for query {}", query);
        Page<LeasingCompanyDTO> page = leasingCompanyService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/leasing-companies");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/leasing-companies/set-status/{id}")
    @Timed
    public ResponseEntity< LeasingCompanyDTO> setStatusLeasingCompany(@PathVariable Integer id, @RequestBody LeasingCompanyDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status LeasingCompany : {}", dto);
        LeasingCompanyDTO r = leasingCompanyService.changeLeasingCompanyStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
