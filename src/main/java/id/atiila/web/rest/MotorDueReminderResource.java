package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.MotorDueReminderService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.MotorDueReminderDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MotorDueReminder.
 */
@RestController
@RequestMapping("/api")
public class MotorDueReminderResource {

    private final Logger log = LoggerFactory.getLogger(MotorDueReminderResource.class);

    private static final String ENTITY_NAME = "motorDueReminder";

    private final MotorDueReminderService motorDueReminderService;

    public MotorDueReminderResource(MotorDueReminderService motorDueReminderService) {
        this.motorDueReminderService = motorDueReminderService;
    }

    /**
     * POST  /motor-due-reminders : Create a new motorDueReminder.
     *
     * @param motorDueReminderDTO the motorDueReminderDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new motorDueReminderDTO, or with status 400 (Bad Request) if the motorDueReminder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/motor-due-reminders")
    @Timed
    public ResponseEntity<MotorDueReminderDTO> createMotorDueReminder(@RequestBody MotorDueReminderDTO motorDueReminderDTO) throws URISyntaxException {
        log.debug("REST request to save MotorDueReminder : {}", motorDueReminderDTO);
        if (motorDueReminderDTO.getIdReminder() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new motorDueReminder cannot already have an ID")).body(null);
        }
        MotorDueReminderDTO result = motorDueReminderService.save(motorDueReminderDTO);
        return ResponseEntity.created(new URI("/api/motor-due-reminders/" + result.getIdReminder()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdReminder().toString()))
            .body(result);
    }

    /**
     * POST  /motor-due-reminders/execute{id}/{param} : Execute Bussiness Process motorDueReminder.
     *
     * @param motorDueReminderDTO the motorDueReminderDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  motorDueReminderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/motor-due-reminders/execute/{id}/{param}")
    @Timed
    public ResponseEntity<MotorDueReminderDTO> executedMotorDueReminder(@PathVariable Integer id, @PathVariable String param, @RequestBody MotorDueReminderDTO motorDueReminderDTO) throws URISyntaxException {
        log.debug("REST request to process MotorDueReminder : {}", motorDueReminderDTO);
        MotorDueReminderDTO result = motorDueReminderService.processExecuteData(id, param, motorDueReminderDTO);
        return new ResponseEntity<MotorDueReminderDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /motor-due-reminders/execute-list{id}/{param} : Execute Bussiness Process motorDueReminder.
     *
     * @param motorDueReminderDTO the motorDueReminderDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  motorDueReminderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/motor-due-reminders/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<MotorDueReminderDTO>> executedListMotorDueReminder(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<MotorDueReminderDTO> motorDueReminderDTO) throws URISyntaxException {
        log.debug("REST request to process List MotorDueReminder");
        Set<MotorDueReminderDTO> result = motorDueReminderService.processExecuteListData(id, param, motorDueReminderDTO);
        return new ResponseEntity<Set<MotorDueReminderDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /motor-due-reminders : Updates an existing motorDueReminder.
     *
     * @param motorDueReminderDTO the motorDueReminderDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated motorDueReminderDTO,
     * or with status 400 (Bad Request) if the motorDueReminderDTO is not valid,
     * or with status 500 (Internal Server Error) if the motorDueReminderDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/motor-due-reminders")
    @Timed
    public ResponseEntity<MotorDueReminderDTO> updateMotorDueReminder(@RequestBody MotorDueReminderDTO motorDueReminderDTO) throws URISyntaxException {
        log.debug("REST request to update MotorDueReminder : {}", motorDueReminderDTO);
        if (motorDueReminderDTO.getIdReminder() == null) {
            return createMotorDueReminder(motorDueReminderDTO);
        }
        MotorDueReminderDTO result = motorDueReminderService.save(motorDueReminderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, motorDueReminderDTO.getIdReminder().toString()))
            .body(result);
    }

    /**
     * GET  /motor-due-reminders : get all the motorDueReminders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of motorDueReminders in body
     */
    @GetMapping("/motor-due-reminders")
    @Timed
    public ResponseEntity<List<MotorDueReminderDTO>> getAllMotorDueReminders(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of MotorDueReminders");
        Page<MotorDueReminderDTO> page = motorDueReminderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/motor-due-reminders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /motor-due-reminders/:id : get the "id" motorDueReminder.
     *
     * @param id the id of the motorDueReminderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the motorDueReminderDTO, or with status 404 (Not Found)
     */
    @GetMapping("/motor-due-reminders/{id}")
    @Timed
    public ResponseEntity<MotorDueReminderDTO> getMotorDueReminder(@PathVariable Integer id) {
        log.debug("REST request to get MotorDueReminder : {}", id);
        MotorDueReminderDTO motorDueReminderDTO = motorDueReminderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(motorDueReminderDTO));
    }

    /**
     * DELETE  /motor-due-reminders/:id : delete the "id" motorDueReminder.
     *
     * @param id the id of the motorDueReminderDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/motor-due-reminders/{id}")
    @Timed
    public ResponseEntity<Void> deleteMotorDueReminder(@PathVariable Integer id) {
        log.debug("REST request to delete MotorDueReminder : {}", id);
        motorDueReminderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/motor-due-reminders?query=:query : search for the motorDueReminder corresponding
     * to the query.
     *
     * @param query the query of the motorDueReminder search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/motor-due-reminders")
    @Timed
    public ResponseEntity<List<MotorDueReminderDTO>> searchMotorDueReminders(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of MotorDueReminders for query {}", query);
        Page<MotorDueReminderDTO> page = motorDueReminderService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/motor-due-reminders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
