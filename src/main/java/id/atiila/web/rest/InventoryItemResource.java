package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.InventoryItemService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.InventoryItemDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing InventoryItem.
 */
@RestController
@RequestMapping("/api")
public class InventoryItemResource {

    private final Logger log = LoggerFactory.getLogger(InventoryItemResource.class);

    private static final String ENTITY_NAME = "inventoryItem";

    private final InventoryItemService inventoryItemService;

    public InventoryItemResource(InventoryItemService inventoryItemService) {
        this.inventoryItemService = inventoryItemService;
    }

    /**
     * POST  /inventory-items : Create a new inventoryItem.
     *
     * @param inventoryItemDTO the inventoryItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new inventoryItemDTO, or with status 400 (Bad Request) if the inventoryItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/inventory-items")
    @Timed
    public ResponseEntity<InventoryItemDTO> createInventoryItem(@RequestBody InventoryItemDTO inventoryItemDTO) throws URISyntaxException {
        log.debug("REST request to save InventoryItem : {}", inventoryItemDTO);
        if (inventoryItemDTO.getIdInventoryItem() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new inventoryItem cannot already have an ID")).body(null);
        }
        InventoryItemDTO result = inventoryItemService.save(inventoryItemDTO);
        return ResponseEntity.created(new URI("/api/inventory-items/" + result.getIdInventoryItem()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdInventoryItem().toString()))
            .body(result);
    }

    /**
     * POST  /inventory-items/execute{id}/{param} : Execute Bussiness Process inventoryItem.
     *
     * @param inventoryItemDTO the inventoryItemDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  inventoryItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/inventory-items/execute/{id}/{param}")
    @Timed
    public ResponseEntity<InventoryItemDTO> executedInventoryItem(@PathVariable Integer id, @PathVariable String param, @RequestBody InventoryItemDTO inventoryItemDTO) throws URISyntaxException {
        log.debug("REST request to process InventoryItem : {}", inventoryItemDTO);
        InventoryItemDTO result = inventoryItemService.processExecuteData(id, param, inventoryItemDTO);
        return new ResponseEntity<InventoryItemDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /inventory-items/execute-list{id}/{param} : Execute Bussiness Process inventoryItem.
     *
     * @param inventoryItemDTO the inventoryItemDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  inventoryItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/inventory-items/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<InventoryItemDTO>> executedListInventoryItem(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<InventoryItemDTO> inventoryItemDTO) throws URISyntaxException {
        log.debug("REST request to process List InventoryItem");
        Set<InventoryItemDTO> result = inventoryItemService.processExecuteListData(id, param, inventoryItemDTO);
        return new ResponseEntity<Set<InventoryItemDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /inventory-items : Updates an existing inventoryItem.
     *
     * @param inventoryItemDTO the inventoryItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated inventoryItemDTO,
     * or with status 400 (Bad Request) if the inventoryItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the inventoryItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/inventory-items")
    @Timed
    public ResponseEntity<InventoryItemDTO> updateInventoryItem(@RequestBody InventoryItemDTO inventoryItemDTO) throws URISyntaxException {
        log.debug("REST request to update InventoryItem : {}", inventoryItemDTO);
        if (inventoryItemDTO.getIdInventoryItem() == null) {
            return createInventoryItem(inventoryItemDTO);
        }
        InventoryItemDTO result = inventoryItemService.save(inventoryItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, inventoryItemDTO.getIdInventoryItem().toString()))
            .body(result);
    }

    /**
     * GET  /inventory-items : get all the inventoryItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of inventoryItems in body
     */
    @GetMapping("/inventory-items")
    @Timed
    public ResponseEntity<List<InventoryItemDTO>> getAllInventoryItems(
            @RequestParam(required = false) String regNoSin,
            @RequestParam(required = false) String idProduct,
            @RequestParam(required = false) String idInternal,
            @RequestParam(required = false) Integer idFeature,
            @RequestParam(required = false)String regNoKa,
            @RequestParam(required = false) Integer regYear,
// todo remak karena ga pakai gudang dulu
//            @RequestParam(required = false) UUID regGudang,
            @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of InventoryItems REGNoSin="+regNoSin+"=idProduct="+idProduct+"==");
        Page<InventoryItemDTO> page ;
// todo remark karena ga pakai gudang dulu
//        page = inventoryItemService.findByReguest(regNoSin, idProduct, idInternal, idFeature, regNoKa, regYear,regGudang, pageable);
        page = inventoryItemService.findByReguest(regNoSin, idProduct, idInternal, idFeature, regNoKa, regYear, pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/inventory-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/inventory-items/memo")
    @Timed
    public ResponseEntity<List<InventoryItemDTO>> getAllInventoryItemsForMemo(
        @RequestParam(required = false) String regNoSin,
        @RequestParam(required = false) String idProduct,
        @RequestParam(required = false) String idInternal,
        @RequestParam(required = false) Integer idFeature,
        @RequestParam(required = false) String regNoKa,
        @RequestParam(required = false) Integer cogs,
//        @RequestParam(required = false) Integer regYear,
        @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of InventoryItems REGNoSin="+regNoSin+"=idProduct="+idProduct+"==");
//        log.debug("filter memo year rest == "+ regYear);
        Page<InventoryItemDTO> page ;

//        page = inventoryItemService.findByReguestForMemo(regNoSin, idProduct, idInternal, idFeature, regNoKa, cogs ,regYear, pageable);
        page = inventoryItemService.findByReguestForMemo(regNoSin, idProduct, idInternal, idFeature, regNoKa, cogs , pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/inventory-items/memo");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    @GetMapping("/inventory-items/filterBy")
    @Timed
    public ResponseEntity<List<InventoryItemDTO>> getAllFilteredInventoryItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of InventoryItems");
        Page<InventoryItemDTO> page = inventoryItemService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/inventory-items/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * GET  /inventory-items/:id : get the "id" inventoryItem.
     *
     * @param id the id of the inventoryItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the inventoryItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/inventory-items/{id}")
    @Timed
    public ResponseEntity<InventoryItemDTO> getInventoryItem(@PathVariable UUID id) {
        log.debug("REST request to get InventoryItem : {}", id);
        InventoryItemDTO inventoryItemDTO = inventoryItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(inventoryItemDTO));
    }

    /**
     * DELETE  /inventory-items/:id : delete the "id" inventoryItem.
     *
     * @param id the id of the inventoryItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/inventory-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteInventoryItem(@PathVariable UUID id) {
        log.debug("REST request to delete InventoryItem : {}", id);
        inventoryItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/inventory-items?query=:query : search for the inventoryItem corresponding
     * to the query.
     *
     * @param query the query of the inventoryItem search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/inventory-items")
    @Timed
    public ResponseEntity<List<InventoryItemDTO>> searchInventoryItems(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of InventoryItems for query {}", query);
        Page<InventoryItemDTO> page = inventoryItemService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/inventory-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/inventory-items/set-status/{id}")
    @Timed
    public ResponseEntity< InventoryItemDTO> setStatusInventoryItem(@PathVariable Integer id, @RequestBody InventoryItemDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status InventoryItem : {}", dto);
        InventoryItemDTO r = inventoryItemService.changeInventoryItemStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    @GetMapping("/inventory-items/available-stock-by-product")
    @Timed
    public ResponseEntity<List<InventoryItemDTO>> findAvailableStockByProduct(
        @RequestParam(required = false) String idproduct,
        @RequestParam String idfacility,
        @RequestParam String idcontainer,
        @ApiParam Pageable pageable
    ){
        log.debug("REST request to get available stock");
        Page<InventoryItemDTO> page = inventoryItemService.findAvailableStock(idproduct, idfacility, idcontainer, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/inventory-items/available-stock-by-product");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/inventory-items/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processInventoryItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process InventoryItem ");
        Map<String, Object> result = inventoryItemService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
