package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.WorkOrderBookingService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.WorkOrderBookingDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WorkOrderBooking.
 */
@RestController
@RequestMapping("/api")
public class WorkOrderBookingResource {

    private final Logger log = LoggerFactory.getLogger(WorkOrderBookingResource.class);

    private static final String ENTITY_NAME = "workOrderBooking";

    private final WorkOrderBookingService workOrderBookingService;

    public WorkOrderBookingResource(WorkOrderBookingService workOrderBookingService) {
        this.workOrderBookingService = workOrderBookingService;
    }

    /**
     * POST  /work-order-bookings : Create a new workOrderBooking.
     *
     * @param workOrderBookingDTO the workOrderBookingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new workOrderBookingDTO, or with status 400 (Bad Request) if the workOrderBooking has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/work-order-bookings")
    @Timed
    public ResponseEntity<WorkOrderBookingDTO> createWorkOrderBooking(@RequestBody WorkOrderBookingDTO workOrderBookingDTO) throws URISyntaxException {
        log.debug("REST request to save WorkOrderBooking : {}", workOrderBookingDTO);
        if (workOrderBookingDTO.getIdBooking() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new workOrderBooking cannot already have an ID")).body(null);
        }
        WorkOrderBookingDTO result = workOrderBookingService.save(workOrderBookingDTO);
        return ResponseEntity.created(new URI("/api/work-order-bookings/" + result.getIdBooking()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdBooking().toString()))
            .body(result);
    }

    /**
     * POST  /work-order-bookings/execute{id}/{param} : Execute Bussiness Process workOrderBooking.
     *
     * @param workOrderBookingDTO the workOrderBookingDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  workOrderBookingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/work-order-bookings/execute/{id}/{param}")
    @Timed
    public ResponseEntity<WorkOrderBookingDTO> executedWorkOrderBooking(@PathVariable Integer id, @PathVariable String param, @RequestBody WorkOrderBookingDTO workOrderBookingDTO) throws URISyntaxException {
        log.debug("REST request to process WorkOrderBooking : {}", workOrderBookingDTO);
        WorkOrderBookingDTO result = workOrderBookingService.processExecuteData(id, param, workOrderBookingDTO);
        return new ResponseEntity<WorkOrderBookingDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /work-order-bookings/execute-list{id}/{param} : Execute Bussiness Process workOrderBooking.
     *
     * @param workOrderBookingDTO the workOrderBookingDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  workOrderBookingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/work-order-bookings/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<WorkOrderBookingDTO>> executedListWorkOrderBooking(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<WorkOrderBookingDTO> workOrderBookingDTO) throws URISyntaxException {
        log.debug("REST request to process List WorkOrderBooking");
        Set<WorkOrderBookingDTO> result = workOrderBookingService.processExecuteListData(id, param, workOrderBookingDTO);
        return new ResponseEntity<Set<WorkOrderBookingDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /work-order-bookings : Updates an existing workOrderBooking.
     *
     * @param workOrderBookingDTO the workOrderBookingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated workOrderBookingDTO,
     * or with status 400 (Bad Request) if the workOrderBookingDTO is not valid,
     * or with status 500 (Internal Server Error) if the workOrderBookingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/work-order-bookings")
    @Timed
    public ResponseEntity<WorkOrderBookingDTO> updateWorkOrderBooking(@RequestBody WorkOrderBookingDTO workOrderBookingDTO) throws URISyntaxException {
        log.debug("REST request to update WorkOrderBooking : {}", workOrderBookingDTO);
        if (workOrderBookingDTO.getIdBooking() == null) {
            return createWorkOrderBooking(workOrderBookingDTO);
        }
        WorkOrderBookingDTO result = workOrderBookingService.save(workOrderBookingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, workOrderBookingDTO.getIdBooking().toString()))
            .body(result);
    }

    /**
     * GET  /work-order-bookings : get all the workOrderBookings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of workOrderBookings in body
     */
    @GetMapping("/work-order-bookings")
    @Timed
    public ResponseEntity<List<WorkOrderBookingDTO>> getAllWorkOrderBookings(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of WorkOrderBookings");
        Page<WorkOrderBookingDTO> page = workOrderBookingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/work-order-bookings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /work-order-bookings/:id : get the "id" workOrderBooking.
     *
     * @param id the id of the workOrderBookingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the workOrderBookingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/work-order-bookings/{id}")
    @Timed
    public ResponseEntity<WorkOrderBookingDTO> getWorkOrderBooking(@PathVariable UUID id) {
        log.debug("REST request to get WorkOrderBooking : {}", id);
        WorkOrderBookingDTO workOrderBookingDTO = workOrderBookingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(workOrderBookingDTO));
    }

    /**
     * DELETE  /work-order-bookings/:id : delete the "id" workOrderBooking.
     *
     * @param id the id of the workOrderBookingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/work-order-bookings/{id}")
    @Timed
    public ResponseEntity<Void> deleteWorkOrderBooking(@PathVariable UUID id) {
        log.debug("REST request to delete WorkOrderBooking : {}", id);
        workOrderBookingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/work-order-bookings?query=:query : search for the workOrderBooking corresponding
     * to the query.
     *
     * @param query the query of the workOrderBooking search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/work-order-bookings")
    @Timed
    public ResponseEntity<List<WorkOrderBookingDTO>> searchWorkOrderBookings(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of WorkOrderBookings for query {}", query);
        Page<WorkOrderBookingDTO> page = workOrderBookingService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/work-order-bookings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/work-order-bookings/set-status/{id}")
    @Timed
    public ResponseEntity< WorkOrderBookingDTO> setStatusWorkOrderBooking(@PathVariable Integer id, @RequestBody WorkOrderBookingDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status WorkOrderBooking : {}", dto);
        WorkOrderBookingDTO r = workOrderBookingService.changeWorkOrderBookingStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    @PostMapping("/work-order-bookings/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processWorkOrderBooking(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process WorkOrderBooking ");
        Map<String, Object> result = workOrderBookingService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
