package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.OrderShipmentItemService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.OrderShipmentItemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing OrderShipmentItem.
 */
@RestController
@RequestMapping("/api")
public class OrderShipmentItemResource {

    private final Logger log = LoggerFactory.getLogger(OrderShipmentItemResource.class);

    private static final String ENTITY_NAME = "orderShipmentItem";

    private final OrderShipmentItemService orderShipmentItemService;

    public OrderShipmentItemResource(OrderShipmentItemService orderShipmentItemService) {
        this.orderShipmentItemService = orderShipmentItemService;
    }

    /**
     * POST  /order-shipment-items : Create a new orderShipmentItem.
     *
     * @param orderShipmentItemDTO the orderShipmentItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new orderShipmentItemDTO, or with status 400 (Bad Request) if the orderShipmentItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-shipment-items")
    @Timed
    public ResponseEntity<OrderShipmentItemDTO> createOrderShipmentItem(@RequestBody OrderShipmentItemDTO orderShipmentItemDTO) throws URISyntaxException {
        log.debug("REST request to save OrderShipmentItem : {}", orderShipmentItemDTO);
        if (orderShipmentItemDTO.getIdOrderShipmentItem() != null) {
            throw new BadRequestAlertException("A new orderShipmentItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderShipmentItemDTO result = orderShipmentItemService.save(orderShipmentItemDTO);
        return ResponseEntity.created(new URI("/api/order-shipment-items/" + result.getIdOrderShipmentItem()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdOrderShipmentItem().toString()))
            .body(result);
    }

    /**
     * POST  /order-shipment-items/process : Execute Bussiness Process orderShipmentItem.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-shipment-items/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrderShipmentItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process OrderShipmentItem ");
        Map<String, Object> result = orderShipmentItemService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /order-shipment-items/execute : Execute Bussiness Process orderShipmentItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  orderShipmentItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-shipment-items/execute")
    @Timed
    public ResponseEntity<OrderShipmentItemDTO> executedOrderShipmentItem(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process OrderShipmentItem");
        OrderShipmentItemDTO result = orderShipmentItemService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<OrderShipmentItemDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /order-shipment-items/execute-list : Execute Bussiness Process orderShipmentItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  orderShipmentItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-shipment-items/execute-list")
    @Timed
    public ResponseEntity<Set<OrderShipmentItemDTO>> executedListOrderShipmentItem(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List OrderShipmentItem");
        Set<OrderShipmentItemDTO> result = orderShipmentItemService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<OrderShipmentItemDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /order-shipment-items : Updates an existing orderShipmentItem.
     *
     * @param orderShipmentItemDTO the orderShipmentItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated orderShipmentItemDTO,
     * or with status 400 (Bad Request) if the orderShipmentItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the orderShipmentItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/order-shipment-items")
    @Timed
    public ResponseEntity<OrderShipmentItemDTO> updateOrderShipmentItem(@RequestBody OrderShipmentItemDTO orderShipmentItemDTO) throws URISyntaxException {
        log.debug("REST request to update OrderShipmentItem : {}", orderShipmentItemDTO);
        if (orderShipmentItemDTO.getIdOrderShipmentItem() == null) {
            return createOrderShipmentItem(orderShipmentItemDTO);
        }
        OrderShipmentItemDTO result = orderShipmentItemService.save(orderShipmentItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, orderShipmentItemDTO.getIdOrderShipmentItem().toString()))
            .body(result);
    }

    /**
     * GET  /order-shipment-items : get all the orderShipmentItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of orderShipmentItems in body
     */
    @GetMapping("/order-shipment-items")
    @Timed
    public ResponseEntity<List<OrderShipmentItemDTO>> getAllOrderShipmentItems(Pageable pageable) {
        log.debug("REST request to get a page of OrderShipmentItems");
        Page<OrderShipmentItemDTO> page = orderShipmentItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/order-shipment-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/order-shipment-items/filterBy")
    @Timed
    public ResponseEntity<List<OrderShipmentItemDTO>> getAllFilteredOrderShipmentItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of OrderShipmentItems");
        Page<OrderShipmentItemDTO> page = orderShipmentItemService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/order-shipment-items/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /order-shipment-items/:id : get the "id" orderShipmentItem.
     *
     * @param id the id of the orderShipmentItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the orderShipmentItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/order-shipment-items/{id}")
    @Timed
    public ResponseEntity<OrderShipmentItemDTO> getOrderShipmentItem(@PathVariable UUID id) {
        log.debug("REST request to get OrderShipmentItem : {}", id);
        OrderShipmentItemDTO orderShipmentItemDTO = orderShipmentItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(orderShipmentItemDTO));
    }

    /**
     * DELETE  /order-shipment-items/:id : delete the "id" orderShipmentItem.
     *
     * @param id the id of the orderShipmentItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/order-shipment-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteOrderShipmentItem(@PathVariable UUID id) {
        log.debug("REST request to delete OrderShipmentItem : {}", id);
        orderShipmentItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/order-shipment-items?query=:query : search for the orderShipmentItem corresponding
     * to the query.
     *
     * @param query the query of the orderShipmentItem search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/order-shipment-items")
    @Timed
    public ResponseEntity<List<OrderShipmentItemDTO>> searchOrderShipmentItems(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of OrderShipmentItems for query {}", query);
        Page<OrderShipmentItemDTO> page = orderShipmentItemService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/order-shipment-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
