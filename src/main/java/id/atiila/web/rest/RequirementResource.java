package id.atiila.web.rest;

import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RequirementService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RequirementDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Requirement.
 */
@RestController
@RequestMapping("/api")
public class RequirementResource {

    private final Logger log = LoggerFactory.getLogger(RequirementResource.class);

    private static final String ENTITY_NAME = "requirement";

    private final RequirementService requirementService;

    public RequirementResource(RequirementService requirementService) {
        this.requirementService = requirementService;
    }

    /**
     * POST  /requirements : Create a new requirement.
     *
     * @param requirementDTO the requirementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new requirementDTO, or with status 400 (Bad Request) if the requirement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requirements")
    @Timed
    public ResponseEntity<RequirementDTO> createRequirement(@RequestBody RequirementDTO requirementDTO) throws URISyntaxException {
        log.debug("REST request to save Requirement : {}", requirementDTO);
        if (requirementDTO.getIdRequirement() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new requirement cannot already have an ID")).body(null);
        }
        RequirementDTO result = requirementService.save(requirementDTO);
        
        return ResponseEntity.created(new URI("/api/requirements/" + result.getIdRequirement()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * POST  /requirements/execute : Execute Bussiness Process requirement.
     *
     * @param requirementDTO the requirementDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  requirementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requirements/execute")
    @Timed
    public ResponseEntity<RequirementDTO> executedRequirement(@RequestBody RequirementDTO requirementDTO) throws URISyntaxException {
        log.debug("REST request to process Requirement : {}", requirementDTO);
        return new ResponseEntity<RequirementDTO>(requirementDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /requirements : Updates an existing requirement.
     *
     * @param requirementDTO the requirementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated requirementDTO,
     * or with status 400 (Bad Request) if the requirementDTO is not valid,
     * or with status 500 (Internal Server Error) if the requirementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/requirements")
    @Timed
    public ResponseEntity<RequirementDTO> updateRequirement(@RequestBody RequirementDTO requirementDTO) throws URISyntaxException {
        log.debug("REST request to update Requirement : {}", requirementDTO);
        if (requirementDTO.getIdRequirement() == null) {
            return createRequirement(requirementDTO);
        }
        RequirementDTO result = requirementService.save(requirementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, requirementDTO.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * GET  /requirements : get all the requirements.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of requirements in body
     */
    @GetMapping("/requirements")
    @Timed
    public ResponseEntity<List<RequirementDTO>> getAllRequirements(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Requirements");
        Page<RequirementDTO> page = requirementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /requirements/:id : get the "id" requirement.
     *
     * @param id the id of the requirementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the requirementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/requirements/{id}")
    @Timed
    public ResponseEntity<RequirementDTO> getRequirement(@PathVariable UUID id) {
        log.debug("REST request to get Requirement : {}", id);
        RequirementDTO requirementDTO = requirementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(requirementDTO));
    }

    /**
     * DELETE  /requirements/:id : delete the "id" requirement.
     *
     * @param id the id of the requirementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/requirements/{id}")
    @Timed
    public ResponseEntity<Void> deleteRequirement(@PathVariable UUID id) {
        log.debug("REST request to delete Requirement : {}", id);
        requirementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/requirements?query=:query : search for the requirement corresponding
     * to the query.
     *
     * @param query the query of the requirement search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/requirements")
    @Timed
    public ResponseEntity<List<RequirementDTO>> searchRequirements(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Requirements for query {}", query);
        Page<RequirementDTO> page = requirementService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

	
    @PostMapping("/requirements/set-status/{id}")
    @Timed
    public ResponseEntity< RequirementDTO> setStatusRequirement(@PathVariable Integer id, @RequestBody RequirementDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status Requirement : {}", dto);
        RequirementDTO r = requirementService.changeRequirementStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }
	
}
