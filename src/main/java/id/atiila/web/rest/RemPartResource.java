package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RemPartService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RemPartDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RemPart.
 */
@RestController
@RequestMapping("/api")
public class RemPartResource {

    private final Logger log = LoggerFactory.getLogger(RemPartResource.class);

    private static final String ENTITY_NAME = "remPart";

    private final RemPartService remPartService;

    public RemPartResource(RemPartService remPartService) {
        this.remPartService = remPartService;
    }

    /**
     * POST  /rem-parts : Create a new remPart.
     *
     * @param remPartDTO the remPartDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new remPartDTO, or with status 400 (Bad Request) if the remPart has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rem-parts")
    @Timed
    public ResponseEntity<RemPartDTO> createRemPart(@RequestBody RemPartDTO remPartDTO) throws URISyntaxException {
        log.debug("REST request to save RemPart : {}", remPartDTO);
        if (remPartDTO.getIdProduct() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new remPart cannot already have an ID")).body(null);
        }
        RemPartDTO result = remPartService.save(remPartDTO);

        return ResponseEntity.created(new URI("/api/rem-parts/" + result.getIdProduct()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdProduct().toString()))
            .body(result);
    }

    /**
     * POST  /rem-parts/execute : Execute Bussiness Process remPart.
     *
     * @param remPartDTO the remPartDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  remPartDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rem-parts/execute")
    @Timed
    public ResponseEntity<RemPartDTO> executedRemPart(@RequestBody RemPartDTO remPartDTO) throws URISyntaxException {
        log.debug("REST request to process RemPart : {}", remPartDTO);
        return new ResponseEntity<RemPartDTO>(remPartDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /rem-parts : Updates an existing remPart.
     *
     * @param remPartDTO the remPartDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated remPartDTO,
     * or with status 400 (Bad Request) if the remPartDTO is not valid,
     * or with status 500 (Internal Server Error) if the remPartDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rem-parts")
    @Timed
    public ResponseEntity<RemPartDTO> updateRemPart(@RequestBody RemPartDTO remPartDTO) throws URISyntaxException {
        log.debug("REST request to update RemPart : {}", remPartDTO);
        if (remPartDTO.getIdProduct() == null) {
            return createRemPart(remPartDTO);
        }
        RemPartDTO result = remPartService.save(remPartDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, remPartDTO.getIdProduct().toString()))
            .body(result);
    }

    /**
     * GET  /rem-parts : get all the remParts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of remParts in body
     */
    @GetMapping("/rem-parts")
    @Timed
    public ResponseEntity<List<RemPartDTO>> getAllRemParts(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of RemParts");
        Page<RemPartDTO> page = remPartService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rem-parts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /rem-parts/:id : get the "id" remPart.
     *
     * @param id the id of the remPartDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the remPartDTO, or with status 404 (Not Found)
     */
    @GetMapping("/rem-parts/{id}")
    @Timed
    public ResponseEntity<RemPartDTO> getRemPart(@PathVariable String id) {
        log.debug("REST request to get RemPart : {}", id);
        RemPartDTO remPartDTO = remPartService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(remPartDTO));
    }

    /**
     * DELETE  /rem-parts/:id : delete the "id" remPart.
     *
     * @param id the id of the remPartDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rem-parts/{id}")
    @Timed
    public ResponseEntity<Void> deleteRemPart(@PathVariable String id) {
        log.debug("REST request to delete RemPart : {}", id);
        remPartService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/rem-parts?query=:query : search for the remPart corresponding
     * to the query.
     *
     * @param query the query of the remPart search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/rem-parts")
    @Timed
    public ResponseEntity<List<RemPartDTO>> searchRemParts(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of RemParts for query {}", query);
        Page<RemPartDTO> page = remPartService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/rem-parts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/rem-parts/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processRemPart(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process RemPart ");
        Map<String, Object> result = remPartService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
