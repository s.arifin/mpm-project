package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ShipmentReceiptService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ShipmentReceiptDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ShipmentReceipt.
 */
@RestController
@RequestMapping("/api")
public class ShipmentReceiptResource {

    private final Logger log = LoggerFactory.getLogger(ShipmentReceiptResource.class);

    private static final String ENTITY_NAME = "shipmentReceipt";

    private final ShipmentReceiptService shipmentReceiptService;

    public ShipmentReceiptResource(ShipmentReceiptService shipmentReceiptService) {
        this.shipmentReceiptService = shipmentReceiptService;
    }

    /**
     * POST  /shipment-receipts : Create a new shipmentReceipt.
     *
     * @param shipmentReceiptDTO the shipmentReceiptDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shipmentReceiptDTO, or with status 400 (Bad Request) if the shipmentReceipt has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-receipts")
    @Timed
    public ResponseEntity<ShipmentReceiptDTO> createShipmentReceipt(@RequestBody ShipmentReceiptDTO shipmentReceiptDTO) throws URISyntaxException {
        log.debug("REST request to save ShipmentReceipt : {}", shipmentReceiptDTO);
        if (shipmentReceiptDTO.getIdReceipt() != null) {
            throw new BadRequestAlertException("A new shipmentReceipt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShipmentReceiptDTO result = shipmentReceiptService.save(shipmentReceiptDTO);
        return ResponseEntity.created(new URI("/api/shipment-receipts/" + result.getIdReceipt()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdReceipt().toString()))
            .body(result);
    }

    /**
     * POST  /shipment-receipts/process : Execute Bussiness Process shipmentReceipt.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-receipts/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processShipmentReceipt(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ShipmentReceipt ");
        Map<String, Object> result = shipmentReceiptService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /shipment-receipts/execute : Execute Bussiness Process shipmentReceipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  shipmentReceiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-receipts/execute")
    @Timed
    public ResponseEntity<ShipmentReceiptDTO> executedShipmentReceipt(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ShipmentReceipt");
        ShipmentReceiptDTO result = shipmentReceiptService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<ShipmentReceiptDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /shipment-receipts/execute-list : Execute Bussiness Process shipmentReceipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  shipmentReceiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-receipts/execute-list")
    @Timed
    public ResponseEntity<Set<ShipmentReceiptDTO>> executedListShipmentReceipt(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List ShipmentReceipt");
        Set<ShipmentReceiptDTO> result = shipmentReceiptService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<ShipmentReceiptDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /shipment-receipts : Updates an existing shipmentReceipt.
     *
     * @param shipmentReceiptDTO the shipmentReceiptDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shipmentReceiptDTO,
     * or with status 400 (Bad Request) if the shipmentReceiptDTO is not valid,
     * or with status 500 (Internal Server Error) if the shipmentReceiptDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/shipment-receipts")
    @Timed
    public ResponseEntity<ShipmentReceiptDTO> updateShipmentReceipt(@RequestBody ShipmentReceiptDTO shipmentReceiptDTO) throws URISyntaxException {
        log.debug("REST request to update ShipmentReceipt : {}", shipmentReceiptDTO);
        if (shipmentReceiptDTO.getIdReceipt() == null) {
            return createShipmentReceipt(shipmentReceiptDTO);
        }
        ShipmentReceiptDTO result = shipmentReceiptService.save(shipmentReceiptDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shipmentReceiptDTO.getIdReceipt().toString()))
            .body(result);
    }

    /**
     * GET  /shipment-receipts : get all the shipmentReceipts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shipmentReceipts in body
     */
    @GetMapping("/shipment-receipts")
    @Timed
    public ResponseEntity<List<ShipmentReceiptDTO>> getAllShipmentReceipts(Pageable pageable) {
        log.debug("REST request to get a page of ShipmentReceipts");
        Page<ShipmentReceiptDTO> page = shipmentReceiptService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-receipts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/shipment-receipts/filterBy")
    @Timed
    public ResponseEntity<List<ShipmentReceiptDTO>> getAllFilteredShipmentReceipts(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ShipmentReceipts");
        Page<ShipmentReceiptDTO> page = shipmentReceiptService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-receipts/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /shipment-receipts/:id : get the "id" shipmentReceipt.
     *
     * @param id the id of the shipmentReceiptDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shipmentReceiptDTO, or with status 404 (Not Found)
     */
    @GetMapping("/shipment-receipts/{id}")
    @Timed
    public ResponseEntity<ShipmentReceiptDTO> getShipmentReceipt(@PathVariable UUID id) {
        log.debug("REST request to get ShipmentReceipt : {}", id);
        ShipmentReceiptDTO shipmentReceiptDTO = shipmentReceiptService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(shipmentReceiptDTO));
    }

    /**
     * DELETE  /shipment-receipts/:id : delete the "id" shipmentReceipt.
     *
     * @param id the id of the shipmentReceiptDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shipment-receipts/{id}")
    @Timed
    public ResponseEntity<Void> deleteShipmentReceipt(@PathVariable UUID id) {
        log.debug("REST request to delete ShipmentReceipt : {}", id);
        shipmentReceiptService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/shipment-receipts?query=:query : search for the shipmentReceipt corresponding
     * to the query.
     *
     * @param query the query of the shipmentReceipt search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/shipment-receipts")
    @Timed
    public ResponseEntity<List<ShipmentReceiptDTO>> searchShipmentReceipts(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ShipmentReceipts for query {}", query);
        Page<ShipmentReceiptDTO> page = shipmentReceiptService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/shipment-receipts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/shipment-receipts/set-status/{id}")
    @Timed
    public ResponseEntity< ShipmentReceiptDTO> setStatusShipmentReceipt(@PathVariable Integer id, @RequestBody ShipmentReceiptDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status ShipmentReceipt : {}", dto);
        ShipmentReceiptDTO r = shipmentReceiptService.changeShipmentReceiptStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
