package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RoleTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RoleTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RoleType.
 */
@RestController
@RequestMapping("/api")
public class RoleTypeResource {

    private final Logger log = LoggerFactory.getLogger(RoleTypeResource.class);

    private static final String ENTITY_NAME = "roleType";

    private final RoleTypeService roleTypeService;

    public RoleTypeResource(RoleTypeService roleTypeService) {
        this.roleTypeService = roleTypeService;
    }

    /**
     * POST  /role-types : Create a new roleType.
     *
     * @param roleTypeDTO the roleTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new roleTypeDTO, or with status 400 (Bad Request) if the roleType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/role-types")
    @Timed
    public ResponseEntity<RoleTypeDTO> createRoleType(@RequestBody RoleTypeDTO roleTypeDTO) throws URISyntaxException {
        log.debug("REST request to save RoleType : {}", roleTypeDTO);
        if (roleTypeDTO.getIdRoleType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new roleType cannot already have an ID")).body(null);
        }
        RoleTypeDTO result = roleTypeService.save(roleTypeDTO);
        
        return ResponseEntity.created(new URI("/api/role-types/" + result.getIdRoleType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRoleType().toString()))
            .body(result);
    }

    /**
     * POST  /role-types/execute : Execute Bussiness Process roleType.
     *
     * @param roleTypeDTO the roleTypeDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  roleTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/role-types/execute")
    @Timed
    public ResponseEntity<RoleTypeDTO> executedRoleType(@RequestBody RoleTypeDTO roleTypeDTO) throws URISyntaxException {
        log.debug("REST request to process RoleType : {}", roleTypeDTO);
        return new ResponseEntity<RoleTypeDTO>(roleTypeDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /role-types : Updates an existing roleType.
     *
     * @param roleTypeDTO the roleTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated roleTypeDTO,
     * or with status 400 (Bad Request) if the roleTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the roleTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/role-types")
    @Timed
    public ResponseEntity<RoleTypeDTO> updateRoleType(@RequestBody RoleTypeDTO roleTypeDTO) throws URISyntaxException {
        log.debug("REST request to update RoleType : {}", roleTypeDTO);
        if (roleTypeDTO.getIdRoleType() == null) {
            return createRoleType(roleTypeDTO);
        }
        RoleTypeDTO result = roleTypeService.save(roleTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, roleTypeDTO.getIdRoleType().toString()))
            .body(result);
    }

    /**
     * GET  /role-types : get all the roleTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of roleTypes in body
     */
    @GetMapping("/role-types")
    @Timed
    public ResponseEntity<List<RoleTypeDTO>> getAllRoleTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of RoleTypes");
        Page<RoleTypeDTO> page = roleTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/role-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /role-types/:id : get the "id" roleType.
     *
     * @param id the id of the roleTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the roleTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/role-types/{id}")
    @Timed
    public ResponseEntity<RoleTypeDTO> getRoleType(@PathVariable Integer id) {
        log.debug("REST request to get RoleType : {}", id);
        RoleTypeDTO roleTypeDTO = roleTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(roleTypeDTO));
    }

    /**
     * DELETE  /role-types/:id : delete the "id" roleType.
     *
     * @param id the id of the roleTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/role-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteRoleType(@PathVariable Integer id) {
        log.debug("REST request to delete RoleType : {}", id);
        roleTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/role-types?query=:query : search for the roleType corresponding
     * to the query.
     *
     * @param query the query of the roleType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/role-types")
    @Timed
    public ResponseEntity<List<RoleTypeDTO>> searchRoleTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of RoleTypes for query {}", query);
        Page<RoleTypeDTO> page = roleTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/role-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
