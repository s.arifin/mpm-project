package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PriceTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PriceTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PriceType.
 */
@RestController
@RequestMapping("/api")
public class PriceTypeResource {

    private final Logger log = LoggerFactory.getLogger(PriceTypeResource.class);

    private static final String ENTITY_NAME = "priceType";

    private final PriceTypeService priceTypeService;

    public PriceTypeResource(PriceTypeService priceTypeService) {
        this.priceTypeService = priceTypeService;
    }

    /**
     * POST  /price-types : Create a new priceType.
     *
     * @param priceTypeDTO the priceTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new priceTypeDTO, or with status 400 (Bad Request) if the priceType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/price-types")
    @Timed
    public ResponseEntity<PriceTypeDTO> createPriceType(@RequestBody PriceTypeDTO priceTypeDTO) throws URISyntaxException {
        log.debug("REST request to save PriceType : {}", priceTypeDTO);
        if (priceTypeDTO.getIdPriceType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new priceType cannot already have an ID")).body(null);
        }
        PriceTypeDTO result = priceTypeService.save(priceTypeDTO);
        return ResponseEntity.created(new URI("/api/price-types/" + result.getIdPriceType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPriceType().toString()))
            .body(result);
    }

    /**
     * POST  /price-types/execute{id}/{param} : Execute Bussiness Process priceType.
     *
     * @param priceTypeDTO the priceTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  priceTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/price-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<PriceTypeDTO> executedPriceType(@PathVariable Integer id, @PathVariable String param, @RequestBody PriceTypeDTO priceTypeDTO) throws URISyntaxException {
        log.debug("REST request to process PriceType : {}", priceTypeDTO);
        PriceTypeDTO result = priceTypeService.processExecuteData(id, param, priceTypeDTO);
        return new ResponseEntity<PriceTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /price-types/execute-list{id}/{param} : Execute Bussiness Process priceType.
     *
     * @param priceTypeDTO the priceTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  priceTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/price-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<PriceTypeDTO>> executedListPriceType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<PriceTypeDTO> priceTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List PriceType");
        Set<PriceTypeDTO> result = priceTypeService.processExecuteListData(id, param, priceTypeDTO);
        return new ResponseEntity<Set<PriceTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /price-types : Updates an existing priceType.
     *
     * @param priceTypeDTO the priceTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated priceTypeDTO,
     * or with status 400 (Bad Request) if the priceTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the priceTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/price-types")
    @Timed
    public ResponseEntity<PriceTypeDTO> updatePriceType(@RequestBody PriceTypeDTO priceTypeDTO) throws URISyntaxException {
        log.debug("REST request to update PriceType : {}", priceTypeDTO);
        if (priceTypeDTO.getIdPriceType() == null) {
            return createPriceType(priceTypeDTO);
        }
        PriceTypeDTO result = priceTypeService.save(priceTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, priceTypeDTO.getIdPriceType().toString()))
            .body(result);
    }

    /**
     * GET  /price-types : get all the priceTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of priceTypes in body
     */
    @GetMapping("/price-types")
    @Timed
    public ResponseEntity<List<PriceTypeDTO>> getAllPriceTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PriceTypes");
        Page<PriceTypeDTO> page = priceTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/price-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /price-types/:id : get the "id" priceType.
     *
     * @param id the id of the priceTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the priceTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/price-types/{id}")
    @Timed
    public ResponseEntity<PriceTypeDTO> getPriceType(@PathVariable Integer id) {
        log.debug("REST request to get PriceType : {}", id);
        PriceTypeDTO priceTypeDTO = priceTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(priceTypeDTO));
    }

    /**
     * DELETE  /price-types/:id : delete the "id" priceType.
     *
     * @param id the id of the priceTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/price-types/{id}")
    @Timed
    public ResponseEntity<Void> deletePriceType(@PathVariable Integer id) {
        log.debug("REST request to delete PriceType : {}", id);
        priceTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/price-types?query=:query : search for the priceType corresponding
     * to the query.
     *
     * @param query the query of the priceType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/price-types")
    @Timed
    public ResponseEntity<List<PriceTypeDTO>> searchPriceTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of PriceTypes for query {}", query);
        Page<PriceTypeDTO> page = priceTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/price-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
