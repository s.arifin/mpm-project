package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ProductDocumentService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ProductDocumentDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProductDocument.
 */
@RestController
@RequestMapping("/api")
public class ProductDocumentResource {

    private final Logger log = LoggerFactory.getLogger(ProductDocumentResource.class);

    private static final String ENTITY_NAME = "productDocument";

    private final ProductDocumentService productDocumentService;

    public ProductDocumentResource(ProductDocumentService productDocumentService) {
        this.productDocumentService = productDocumentService;
    }

    /**
     * POST  /product-documents : Create a new productDocument.
     *
     * @param productDocumentDTO the productDocumentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productDocumentDTO, or with status 400 (Bad Request) if the productDocument has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-documents")
    @Timed
    public ResponseEntity<ProductDocumentDTO> createProductDocument(@RequestBody ProductDocumentDTO productDocumentDTO) throws URISyntaxException {
        log.debug("REST request to save ProductDocument : {}", productDocumentDTO);
        if (productDocumentDTO.getIdDocument() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new productDocument cannot already have an ID")).body(null);
        }
        ProductDocumentDTO result = productDocumentService.save(productDocumentDTO);
        return ResponseEntity.created(new URI("/api/product-documents/" + result.getIdDocument()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdDocument().toString()))
            .body(result);
    }

    /**
     * POST  /product-documents/execute{id}/{param} : Execute Bussiness Process productDocument.
     *
     * @param productDocumentDTO the productDocumentDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  productDocumentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-documents/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ProductDocumentDTO> executedProductDocument(@PathVariable Integer id, @PathVariable String param, @RequestBody ProductDocumentDTO productDocumentDTO) throws URISyntaxException {
        log.debug("REST request to process ProductDocument : {}", productDocumentDTO);
        ProductDocumentDTO result = productDocumentService.processExecuteData(id, param, productDocumentDTO);
        return new ResponseEntity<ProductDocumentDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /product-documents/execute-list{id}/{param} : Execute Bussiness Process productDocument.
     *
     * @param productDocumentDTO the productDocumentDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  productDocumentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-documents/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ProductDocumentDTO>> executedListProductDocument(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ProductDocumentDTO> productDocumentDTO) throws URISyntaxException {
        log.debug("REST request to process List ProductDocument");
        Set<ProductDocumentDTO> result = productDocumentService.processExecuteListData(id, param, productDocumentDTO);
        return new ResponseEntity<Set<ProductDocumentDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /product-documents : Updates an existing productDocument.
     *
     * @param productDocumentDTO the productDocumentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productDocumentDTO,
     * or with status 400 (Bad Request) if the productDocumentDTO is not valid,
     * or with status 500 (Internal Server Error) if the productDocumentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/product-documents")
    @Timed
    public ResponseEntity<ProductDocumentDTO> updateProductDocument(@RequestBody ProductDocumentDTO productDocumentDTO) throws URISyntaxException {
        log.debug("REST request to update ProductDocument : {}", productDocumentDTO);
        if (productDocumentDTO.getIdDocument() == null) {
            return createProductDocument(productDocumentDTO);
        }
        ProductDocumentDTO result = productDocumentService.save(productDocumentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, productDocumentDTO.getIdDocument().toString()))
            .body(result);
    }

    @GetMapping("/product-documents/by-product")
    @Timed
    public ResponseEntity<List<ProductDocumentDTO>> getAllProductDocumentsByIdProduct(@RequestParam String idProduct, @ApiParam Pageable pageable){
        log.debug("REST request to get a page of ProductDocuments by idproduct");
        List<ProductDocumentDTO> list = productDocumentService.findAllByIdProduct(idProduct);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(list,headers, HttpStatus.OK );
    }

    /**
     * GET  /product-documents : get all the productDocuments.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of productDocuments in body
     */
    @GetMapping("/product-documents")
    @Timed
    public ResponseEntity<List<ProductDocumentDTO>> getAllProductDocuments(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ProductDocuments");
        Page<ProductDocumentDTO> page = productDocumentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-documents");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @GetMapping("/product-documents/filterBy")
    @Timed
    public ResponseEntity<List<ProductDocumentDTO>> getAllFilteredProductDocuments(@RequestParam String filterBy, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ProductDocuments");
        Page<ProductDocumentDTO> page = productDocumentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-documents/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * GET  /product-documents/:id : get the "id" productDocument.
     *
     * @param id the id of the productDocumentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productDocumentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/product-documents/{id}")
    @Timed
    public ResponseEntity<ProductDocumentDTO> getProductDocument(@PathVariable UUID id) {
        log.debug("REST request to get ProductDocument : {}", id);
        ProductDocumentDTO productDocumentDTO = productDocumentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(productDocumentDTO));
    }

    /**
     * DELETE  /product-documents/:id : delete the "id" productDocument.
     *
     * @param id the id of the productDocumentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/product-documents/{id}")
    @Timed
    public ResponseEntity<Void> deleteProductDocument(@PathVariable UUID id) {
        log.debug("REST request to delete ProductDocument : {}", id);
        productDocumentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/product-documents?query=:query : search for the productDocument corresponding
     * to the query.
     *
     * @param query the query of the productDocument search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/product-documents")
    @Timed
    public ResponseEntity<List<ProductDocumentDTO>> searchProductDocuments(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ProductDocuments for query {}", query);
        Page<ProductDocumentDTO> page = productDocumentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/product-documents");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
