package id.atiila.web.rest;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.BillingItemService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.BillingItemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BillingItem.
 */
@RestController
@RequestMapping("/api")
public class BillingItemResource {

    private final Logger log = LoggerFactory.getLogger(BillingItemResource.class);

    private static final String ENTITY_NAME = "billingItem";

    private final BillingItemService billingItemService;

    public BillingItemResource(BillingItemService billingItemService) {
        this.billingItemService = billingItemService;
    }

    /**
     * POST  /billing-items : Create a new billingItem.
     *
     * @param billingItemDTO the billingItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new billingItemDTO, or with status 400 (Bad Request) if the billingItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-items")
    @Timed
    public ResponseEntity<BillingItemDTO> createBillingItem(@RequestBody BillingItemDTO billingItemDTO) throws URISyntaxException {
        log.debug("REST request to save BillingItem : {}", billingItemDTO);
        if (billingItemDTO.getIdBillingItem() != null) {
            throw new BadRequestAlertException("A new billingItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if(billingItemDTO.getUnitPrice() == null){
            billingItemDTO.setUnitPrice(BigDecimal.valueOf(0));
        }
        if(billingItemDTO.getDiscount() == null){
            billingItemDTO.setDiscount(BigDecimal.valueOf(0));
        }
        BillingItemDTO result = billingItemService.save(billingItemDTO);
        return ResponseEntity.created(new URI("/api/billing-items/" + result.getIdBillingItem()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdBillingItem().toString()))
            .body(result);
    }

    /**
     * POST  /billing-items/process : Execute Bussiness Process billingItem.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-items/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processBillingItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process BillingItem ");
        Map<String, Object> result = billingItemService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /billing-items/execute : Execute Bussiness Process billingItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  billingItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-items/execute")
    @Timed
    public ResponseEntity<BillingItemDTO> executedBillingItem(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process BillingItem");
        BillingItemDTO result = billingItemService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<BillingItemDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /billing-items/execute-list : Execute Bussiness Process billingItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  billingItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-items/execute-list")
    @Timed
    public ResponseEntity<Set<BillingItemDTO>> executedListBillingItem(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List BillingItem");
        Set<BillingItemDTO> result = billingItemService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<BillingItemDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /billing-items : Updates an existing billingItem.
     *
     * @param billingItemDTO the billingItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated billingItemDTO,
     * or with status 400 (Bad Request) if the billingItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the billingItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/billing-items")
    @Timed
    public ResponseEntity<BillingItemDTO> updateBillingItem(@RequestBody BillingItemDTO billingItemDTO) throws URISyntaxException {
        log.debug("REST request to update BillingItem : {}", billingItemDTO);
        if (billingItemDTO.getIdBillingItem() == null) {
            return createBillingItem(billingItemDTO);
        }
        BillingItemDTO result = billingItemService.save(billingItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, billingItemDTO.getIdBillingItem().toString()))
            .body(result);
    }

    /**
     * GET  /billing-items : get all the billingItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of billingItems in body
     */
    @GetMapping("/billing-items")
    @Timed
    public ResponseEntity<List<BillingItemDTO>> getAllBillingItems(Pageable pageable) {
        log.debug("REST request to get a page of BillingItems");
        Page<BillingItemDTO> page = billingItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/billing-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/billing-items/req-faktur-pajak")
    @Timed
    public ResponseEntity<List<BillingItemDTO>> getAllBillingItemsForRequestTaxInvoice(
        Pageable pageable) {
        log.debug("REST request to get a page of BillingItems for tax invoice  ");
        Page<BillingItemDTO> page = billingItemService.findItemForTaxInvoice(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/billing-items/req-faktur-pajak");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/billing-items/filterBy")
    @Timed
    public ResponseEntity<List<BillingItemDTO>> getAllFilteredBillingItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of BillingItems");
        Page<BillingItemDTO> page = billingItemService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/billing-items/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /billing-items/:id : get the "id" billingItem.
     *
     * @param id the id of the billingItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the billingItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/billing-items/{id}")
    @Timed
    public ResponseEntity<BillingItemDTO> getBillingItem(@PathVariable UUID id) {
        log.debug("REST request to get BillingItem : {}", id);
        BillingItemDTO billingItemDTO = billingItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(billingItemDTO));
    }

    /**
     * DELETE  /billing-items/:id : delete the "id" billingItem.
     *
     * @param id the id of the billingItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/billing-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteBillingItem(@PathVariable UUID id) {
        log.debug("REST request to delete BillingItem : {}", id);
        billingItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/billing-items?query=:query : search for the billingItem corresponding
     * to the query.
     *
     * @param query the query of the billingItem search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/billing-items")
    @Timed
    public ResponseEntity<List<BillingItemDTO>> searchBillingItems(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of BillingItems for query {}", query);
        Page<BillingItemDTO> page = billingItemService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/billing-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
