package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ProductCategoryTypeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ProductCategoryTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProductCategoryType.
 */
@RestController
@RequestMapping("/api")
public class ProductCategoryTypeResource {

    private final Logger log = LoggerFactory.getLogger(ProductCategoryTypeResource.class);

    private static final String ENTITY_NAME = "productCategoryType";

    private final ProductCategoryTypeService productCategoryTypeService;

    public ProductCategoryTypeResource(ProductCategoryTypeService productCategoryTypeService) {
        this.productCategoryTypeService = productCategoryTypeService;
    }

    /**
     * POST  /product-category-types : Create a new productCategoryType.
     *
     * @param productCategoryTypeDTO the productCategoryTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productCategoryTypeDTO, or with status 400 (Bad Request) if the productCategoryType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-category-types")
    @Timed
    public ResponseEntity<ProductCategoryTypeDTO> createProductCategoryType(@RequestBody ProductCategoryTypeDTO productCategoryTypeDTO) throws URISyntaxException {
        log.debug("REST request to save ProductCategoryType : {}", productCategoryTypeDTO);
        if (productCategoryTypeDTO.getIdCategoryType() != null) {
            throw new BadRequestAlertException("A new productCategoryType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductCategoryTypeDTO result = productCategoryTypeService.save(productCategoryTypeDTO);
        return ResponseEntity.created(new URI("/api/product-category-types/" + result.getIdCategoryType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdCategoryType().toString()))
            .body(result);
    }

    /**
     * POST  /product-category-types/process : Execute Bussiness Process productCategoryType.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-category-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processProductCategoryType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ProductCategoryType ");
        Map<String, Object> result = productCategoryTypeService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /product-category-types/execute : Execute Bussiness Process productCategoryType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  productCategoryTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-category-types/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedProductCategoryType(HttpServletRequest request, @RequestBody ProductCategoryTypeDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ProductCategoryType");
        Map<String, Object> result = productCategoryTypeService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /product-category-types/execute-list : Execute Bussiness Process productCategoryType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  productCategoryTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-category-types/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListProductCategoryType(HttpServletRequest request, @RequestBody Set<ProductCategoryTypeDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List ProductCategoryType");
        Map<String, Object> result = productCategoryTypeService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /product-category-types : Updates an existing productCategoryType.
     *
     * @param productCategoryTypeDTO the productCategoryTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productCategoryTypeDTO,
     * or with status 400 (Bad Request) if the productCategoryTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the productCategoryTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/product-category-types")
    @Timed
    public ResponseEntity<ProductCategoryTypeDTO> updateProductCategoryType(@RequestBody ProductCategoryTypeDTO productCategoryTypeDTO) throws URISyntaxException {
        log.debug("REST request to update ProductCategoryType : {}", productCategoryTypeDTO);
        if (productCategoryTypeDTO.getIdCategoryType() == null) {
            return createProductCategoryType(productCategoryTypeDTO);
        }
        ProductCategoryTypeDTO result = productCategoryTypeService.save(productCategoryTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, productCategoryTypeDTO.getIdCategoryType().toString()))
            .body(result);
    }

    /**
     * GET  /product-category-types : get all the productCategoryTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of productCategoryTypes in body
     */
    @GetMapping("/product-category-types")
    @Timed
    public ResponseEntity<List<ProductCategoryTypeDTO>> getAllProductCategoryTypes(Pageable pageable) {
        log.debug("REST request to get a page of ProductCategoryTypes");
        Page<ProductCategoryTypeDTO> page = productCategoryTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-category-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/product-category-types/filterBy")
    @Timed
    public ResponseEntity<List<ProductCategoryTypeDTO>> getAllFilteredProductCategoryTypes(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ProductCategoryTypes");
        Page<ProductCategoryTypeDTO> page = productCategoryTypeService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-category-types/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /product-category-types/:id : get the "id" productCategoryType.
     *
     * @param id the id of the productCategoryTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productCategoryTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/product-category-types/{id}")
    @Timed
    public ResponseEntity<ProductCategoryTypeDTO> getProductCategoryType(@PathVariable Integer id) {
        log.debug("REST request to get ProductCategoryType : {}", id);
        ProductCategoryTypeDTO productCategoryTypeDTO = productCategoryTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(productCategoryTypeDTO));
    }

    /**
     * DELETE  /product-category-types/:id : delete the "id" productCategoryType.
     *
     * @param id the id of the productCategoryTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/product-category-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteProductCategoryType(@PathVariable Integer id) {
        log.debug("REST request to delete ProductCategoryType : {}", id);
        productCategoryTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/product-category-types?query=:query : search for the productCategoryType corresponding
     * to the query.
     *
     * @param query the query of the productCategoryType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/product-category-types")
    @Timed
    public ResponseEntity<List<ProductCategoryTypeDTO>> searchProductCategoryTypes(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ProductCategoryTypes for query {}", query);
        Page<ProductCategoryTypeDTO> page = productCategoryTypeService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/product-category-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
