package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.domain.StockOpname;
import id.atiila.service.*;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * REST controller for managing Branch.
 */
@RestController
@RequestMapping("/api")
public class EpisodeResource {

    private final Logger log = LoggerFactory.getLogger(EpisodeResource.class);

    private final EpisodeTemplateService epService;

    @Autowired
    protected ProducerTemplate producerTemplate;

    @Autowired
    protected ConsumerTemplate consumerTemplate;

    @Autowired
    private StockOpnameUtils stockOpnameUtils;

    @Autowired
    private VendorService vendorService;

    @Autowired
    private GeoBoundaryUtils geoBoundaryUtils;

    @Autowired
    private ActivitiProcessor processor;

    @Autowired
    private VendorUtils vendorUtils;

    @Autowired
    private CustomerUtils customerUtils;

    public EpisodeResource(EpisodeTemplateService epService) {
        this.epService = epService;
    }

    @PostMapping("/case/suspect/{idInternal}")
    @Timed
    public ResponseEntity<String> executedSuspect(@PathVariable String idInternal,
                                                  @RequestParam(value = "idSuspectType", required = true) Integer idSuspectType) throws URISyntaxException {
        epService.buildSuspect(idInternal, idSuspectType);
        producerTemplate.sendBody("direct:firstRoute", "Calling via Rest Controller");
        return new ResponseEntity<String>("Ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/prospect/{idInternal}")
    @Timed
    public ResponseEntity<String> executedProspect(@PathVariable String idInternal) throws URISyntaxException {
        log.debug("REST request to process Case Person : {}");
        epService.buildProspect(idInternal);
        producerTemplate.sendBody("direct:firstRoute", "Calling via Rest Controller");
        return new ResponseEntity<String>("Ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/sur/{idInternal}")
    @Timed
    public ResponseEntity<String> executedSur(@PathVariable String idInternal,
                                              @RequestParam(value = "idMotor", required = true) String idMotor,
                                              @RequestParam(value = "idColor", required = true) String idColor) throws URISyntaxException {
        epService.buildSalesUnitRequirement(idInternal, idMotor, idColor);
        return new ResponseEntity<String>("Ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/addUnitStock")
    @Timed
    public ResponseEntity<String> addUnitStock( @RequestParam(value = "idInternal", required = true) String idInternal,
                                                @RequestParam(value = "idMotor", required = true) String idMotor,
                                                @RequestParam(value = "idColor", required = true) String idColor,
                                                @RequestParam(value = "qty", required = true) Integer qty) throws URISyntaxException {
        epService.buildStockUnit(idInternal, idMotor, idColor, qty);
        return new ResponseEntity<String>("Ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/addLeasingCompany")
    @Timed
    public ResponseEntity<String> addLeasingCompany(
        @RequestParam(value="name", required = true) String name) throws URISyntaxException{
        epService.buildLeasingCompany(name);
        return new ResponseEntity<String>("Ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/addLeasingTenorProvide")
    @Timed
    public ResponseEntity<String> addLeasingTenorProvide(
        @RequestParam(value = "motor", required = true) String productId,
        @RequestParam(value = "leasing" ,required = true) String leasingId,
        @RequestParam(value = "tenor", required = true) Integer tenor,
        @RequestParam(value = "downpayment", required = true) BigDecimal downpayment,
        @RequestParam(value = "installment", required = true) BigDecimal installment
    ) throws URISyntaxException{
        epService.buildLeasingTenorProvide(productId, leasingId, tenor ,downpayment, installment);
        return new ResponseEntity<String>("OK", null, HttpStatus.OK);
    }

    @PostMapping("/case/testPrice")
    @Timed
    public ResponseEntity<String> testPrice() throws URISyntaxException{
        epService.buildPrice();
        return new ResponseEntity<String>("OK", null, HttpStatus.OK);
    }

    @PostMapping("/case/buildRuleSalesDiscount")
    @Timed
    @Transactional()
    public ResponseEntity<String> buildRuleSalesDiscount(){
        epService.buildRuleSalesDiscount();
        return new ResponseEntity<String>("OK", null, HttpStatus.OK);
    }

    @PostMapping("/case/buildReceiving")
    @Timed
    public ResponseEntity<String> buildReceiving(){
        epService.buildSampleReceiveing();
        return new ResponseEntity<String>("OK", null, HttpStatus.OK);
    }

    @PostMapping("/case/buildStockProduct")
    @Timed
    public ResponseEntity<String> buildStockProductCase(@RequestParam(value = "idInternal") String idInternal){
        epService.buildStockProductCase(idInternal);
        return new ResponseEntity<String>("OK", null, HttpStatus.OK);
    }

    @PostMapping("/case/buildStockOpname")
    @Timed
    public ResponseEntity<StockOpname> buildStockOpname(@RequestParam(value = "idInternal") String idInternal){
        StockOpname r = stockOpnameUtils.buildStockOpname(idInternal);
        return new ResponseEntity<StockOpname>(r, null, HttpStatus.OK);
    }

    @PostMapping("/case/vendor-migration")
    @Timed
    public ResponseEntity<String> checkVendorMigration(){
        vendorService.initialize();
        return new ResponseEntity<String>("ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/geoboundary-migration")
    @Timed
    public ResponseEntity<String> checkGeoBoundaryMigration(){
        geoBoundaryUtils.initialize();
        return new ResponseEntity<String>("ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/clean-up-db")
    @Timed
    public ResponseEntity<String> cleanUpDB(){
        // personalCustomerService.removeWrongData();
        return new ResponseEntity<String>("ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/build-process")
    @Timed
    public ResponseEntity<String> buildProcess(@RequestParam(value = "bussinesKey") String bkey){
        String processName = "ivu-cancel";
        Map<String, Object> vars = processor.getVariables("billingNumber", bkey);
        processor.startProcess(processName, bkey, vars);
        return new ResponseEntity<String>("ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/build-vendor")
    @Timed
    public ResponseEntity<String> buildVendor(@RequestParam(value = "name") String name){
        vendorUtils.buildVendor(name);
        return new ResponseEntity<String>("ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/build-vso-gc/{idInternal}")
    @Timed
    public ResponseEntity<String> vsogc(@PathVariable String idInternal) throws URISyntaxException {
        epService.buildVSOgc(idInternal);
        return new ResponseEntity<String>("Ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/build-vso-gp/{idInternal}")
    @Timed
    public ResponseEntity<String> vsogp(@PathVariable String idInternal) throws URISyntaxException {
        epService.buildVSOGP(idInternal);
        return new ResponseEntity<String>("Ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/build-vso-direct/{idInternal}")
    @Timed
    public ResponseEntity<String> vsodirect(@PathVariable String idInternal) throws URISyntaxException {
        epService.buildVSOdirect(idInternal);
        return new ResponseEntity<String>("Ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/clean-up-customer")
    @Timed
    public ResponseEntity<String> cleanUpCustomer() throws URISyntaxException {
        customerUtils.cleanUpDoubleCustomer();
        return new ResponseEntity<String>("Ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/demand-supply/po")
    @Timed
    public ResponseEntity<String> demandSupplyPO() throws URISyntaxException {
        epService.demandSupplyPO();
        return new ResponseEntity<String>("Ok", null, HttpStatus.OK);
    }

    @PostMapping("/case/demand-supply/do")
    @Timed
    public ResponseEntity<String> demandSupplyDO() throws URISyntaxException {
        epService.demandSupplyDO();
        return new ResponseEntity<String>("Ok", null, HttpStatus.OK);
    }

}
