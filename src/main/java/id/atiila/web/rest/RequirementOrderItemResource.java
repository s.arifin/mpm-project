package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RequirementOrderItemService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RequirementOrderItemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RequirementOrderItem.
 */
@RestController
@RequestMapping("/api")
public class RequirementOrderItemResource {

    private final Logger log = LoggerFactory.getLogger(RequirementOrderItemResource.class);

    private static final String ENTITY_NAME = "requirementOrderItem";

    private final RequirementOrderItemService requirementOrderItemService;

    public RequirementOrderItemResource(RequirementOrderItemService requirementOrderItemService) {
        this.requirementOrderItemService = requirementOrderItemService;
    }

    /**
     * POST  /requirement-order-items : Create a new requirementOrderItem.
     *
     * @param requirementOrderItemDTO the requirementOrderItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new requirementOrderItemDTO, or with status 400 (Bad Request) if the requirementOrderItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requirement-order-items")
    @Timed
    public ResponseEntity<RequirementOrderItemDTO> createRequirementOrderItem(@RequestBody RequirementOrderItemDTO requirementOrderItemDTO) throws URISyntaxException {
        log.debug("REST request to save RequirementOrderItem : {}", requirementOrderItemDTO);
        if (requirementOrderItemDTO.getIdReqOrderItem() != null) {
            throw new BadRequestAlertException("A new requirementOrderItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RequirementOrderItemDTO result = requirementOrderItemService.save(requirementOrderItemDTO);
        return ResponseEntity.created(new URI("/api/requirement-order-items/" + result.getIdReqOrderItem()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdReqOrderItem().toString()))
            .body(result);
    }

    /**
     * POST  /requirement-order-items/process : Execute Bussiness Process requirementOrderItem.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requirement-order-items/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processRequirementOrderItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RequirementOrderItem ");
        Map<String, Object> result = requirementOrderItemService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /requirement-order-items/execute : Execute Bussiness Process requirementOrderItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  requirementOrderItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requirement-order-items/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedRequirementOrderItem(HttpServletRequest request, @RequestBody RequirementOrderItemDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RequirementOrderItem");
        Map<String, Object> result = requirementOrderItemService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /requirement-order-items/execute-list : Execute Bussiness Process requirementOrderItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  requirementOrderItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/requirement-order-items/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListRequirementOrderItem(HttpServletRequest request, @RequestBody Set<RequirementOrderItemDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List RequirementOrderItem");
        Map<String, Object> result = requirementOrderItemService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /requirement-order-items : Updates an existing requirementOrderItem.
     *
     * @param requirementOrderItemDTO the requirementOrderItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated requirementOrderItemDTO,
     * or with status 400 (Bad Request) if the requirementOrderItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the requirementOrderItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/requirement-order-items")
    @Timed
    public ResponseEntity<RequirementOrderItemDTO> updateRequirementOrderItem(@RequestBody RequirementOrderItemDTO requirementOrderItemDTO) throws URISyntaxException {
        log.debug("REST request to update RequirementOrderItem : {}", requirementOrderItemDTO);
        if (requirementOrderItemDTO.getIdReqOrderItem() == null) {
            return createRequirementOrderItem(requirementOrderItemDTO);
        }
        RequirementOrderItemDTO result = requirementOrderItemService.save(requirementOrderItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, requirementOrderItemDTO.getIdReqOrderItem().toString()))
            .body(result);
    }

    /**
     * GET  /requirement-order-items : get all the requirementOrderItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of requirementOrderItems in body
     */
    @GetMapping("/requirement-order-items")
    @Timed
    public ResponseEntity<List<RequirementOrderItemDTO>> getAllRequirementOrderItems(Pageable pageable) {
        log.debug("REST request to get a page of RequirementOrderItems");
        Page<RequirementOrderItemDTO> page = requirementOrderItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/requirement-order-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/requirement-order-items/filterBy")
    @Timed
    public ResponseEntity<List<RequirementOrderItemDTO>> getAllFilteredRequirementOrderItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of RequirementOrderItems");
        Page<RequirementOrderItemDTO> page = requirementOrderItemService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/requirement-order-items/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /requirement-order-items/:id : get the "id" requirementOrderItem.
     *
     * @param id the id of the requirementOrderItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the requirementOrderItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/requirement-order-items/{id}")
    @Timed
    public ResponseEntity<RequirementOrderItemDTO> getRequirementOrderItem(@PathVariable UUID id) {
        log.debug("REST request to get RequirementOrderItem : {}", id);
        RequirementOrderItemDTO requirementOrderItemDTO = requirementOrderItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(requirementOrderItemDTO));
    }

    /**
     * DELETE  /requirement-order-items/:id : delete the "id" requirementOrderItem.
     *
     * @param id the id of the requirementOrderItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/requirement-order-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteRequirementOrderItem(@PathVariable UUID id) {
        log.debug("REST request to delete RequirementOrderItem : {}", id);
        requirementOrderItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/requirement-order-items?query=:query : search for the requirementOrderItem corresponding
     * to the query.
     *
     * @param query the query of the requirementOrderItem search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/requirement-order-items")
    @Timed
    public ResponseEntity<List<RequirementOrderItemDTO>> searchRequirementOrderItems(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of RequirementOrderItems for query {}", query);
        Page<RequirementOrderItemDTO> page = requirementOrderItemService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/requirement-order-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
