package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ProductSalesOrderService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ProductSalesOrderDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProductSalesOrder.
 */
@RestController
@RequestMapping("/api")
public class ProductSalesOrderResource {

    private final Logger log = LoggerFactory.getLogger(ProductSalesOrderResource.class);

    private static final String ENTITY_NAME = "productSalesOrder";

    private final ProductSalesOrderService productSalesOrderService;

    public ProductSalesOrderResource(ProductSalesOrderService productSalesOrderService) {
        this.productSalesOrderService = productSalesOrderService;
    }

    /**
     * POST  /product-sales-orders : Create a new productSalesOrder.
     *
     * @param productSalesOrderDTO the productSalesOrderDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productSalesOrderDTO, or with status 400 (Bad Request) if the productSalesOrder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-sales-orders")
    @Timed
    public ResponseEntity<ProductSalesOrderDTO> createProductSalesOrder(@RequestBody ProductSalesOrderDTO productSalesOrderDTO) throws URISyntaxException {
        log.debug("REST request to save ProductSalesOrder : {}", productSalesOrderDTO);
        if (productSalesOrderDTO.getIdOrder() != null) {
            throw new BadRequestAlertException("A new productSalesOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductSalesOrderDTO result = productSalesOrderService.save(productSalesOrderDTO);
        return ResponseEntity.created(new URI("/api/product-sales-orders/" + result.getIdOrder()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdOrder().toString()))
            .body(result);
    }

    /**
     * POST  /product-sales-orders/process : Execute Bussiness Process productSalesOrder.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-sales-orders/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processProductSalesOrder(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ProductSalesOrder ");
        Map<String, Object> result = productSalesOrderService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /product-sales-orders/execute : Execute Bussiness Process productSalesOrder.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  productSalesOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-sales-orders/execute")
    @Timed
    public ResponseEntity<ProductSalesOrderDTO> executedProductSalesOrder(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ProductSalesOrder");
        ProductSalesOrderDTO result = productSalesOrderService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<ProductSalesOrderDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /product-sales-orders/execute-list : Execute Bussiness Process productSalesOrder.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  productSalesOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-sales-orders/execute-list")
    @Timed
    public ResponseEntity<Set<ProductSalesOrderDTO>> executedListProductSalesOrder(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List ProductSalesOrder");
        Set<ProductSalesOrderDTO> result = productSalesOrderService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<ProductSalesOrderDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /product-sales-orders : Updates an existing productSalesOrder.
     *
     * @param productSalesOrderDTO the productSalesOrderDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productSalesOrderDTO,
     * or with status 400 (Bad Request) if the productSalesOrderDTO is not valid,
     * or with status 500 (Internal Server Error) if the productSalesOrderDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/product-sales-orders")
    @Timed
    public ResponseEntity<ProductSalesOrderDTO> updateProductSalesOrder(@RequestBody ProductSalesOrderDTO productSalesOrderDTO) throws URISyntaxException {
        log.debug("REST request to update ProductSalesOrder : {}", productSalesOrderDTO);
        if (productSalesOrderDTO.getIdOrder() == null) {
            return createProductSalesOrder(productSalesOrderDTO);
        }
        ProductSalesOrderDTO result = productSalesOrderService.save(productSalesOrderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, productSalesOrderDTO.getIdOrder().toString()))
            .body(result);
    }

    /**
     * GET  /product-sales-orders : get all the productSalesOrders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of productSalesOrders in body
     */
    @GetMapping("/product-sales-orders")
    @Timed
    public ResponseEntity<List<ProductSalesOrderDTO>> getAllProductSalesOrders(Pageable pageable) {
        log.debug("REST request to get a page of ProductSalesOrders");
        Page<ProductSalesOrderDTO> page = productSalesOrderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-sales-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/product-sales-orders/filterBy")
    @Timed
    public ResponseEntity<List<ProductSalesOrderDTO>> getAllFilteredProductSalesOrders(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ProductSalesOrders");
        Page<ProductSalesOrderDTO> page = productSalesOrderService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-sales-orders/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /product-sales-orders/:id : get the "id" productSalesOrder.
     *
     * @param id the id of the productSalesOrderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productSalesOrderDTO, or with status 404 (Not Found)
     */
    @GetMapping("/product-sales-orders/{id}")
    @Timed
    public ResponseEntity<ProductSalesOrderDTO> getProductSalesOrder(@PathVariable UUID id) {
        log.debug("REST request to get ProductSalesOrder : {}", id);
        ProductSalesOrderDTO productSalesOrderDTO = productSalesOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(productSalesOrderDTO));
    }

    /**
     * DELETE  /product-sales-orders/:id : delete the "id" productSalesOrder.
     *
     * @param id the id of the productSalesOrderDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/product-sales-orders/{id}")
    @Timed
    public ResponseEntity<Void> deleteProductSalesOrder(@PathVariable UUID id) {
        log.debug("REST request to delete ProductSalesOrder : {}", id);
        productSalesOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/product-sales-orders?query=:query : search for the productSalesOrder corresponding
     * to the query.
     *
     * @param query the query of the productSalesOrder search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/product-sales-orders")
    @Timed
    public ResponseEntity<List<ProductSalesOrderDTO>> searchProductSalesOrders(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ProductSalesOrders for query {}", query);
        Page<ProductSalesOrderDTO> page = productSalesOrderService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/product-sales-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
