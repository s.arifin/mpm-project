package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RuleTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RuleTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RuleType.
 */
@RestController
@RequestMapping("/api")
public class RuleTypeResource {

    private final Logger log = LoggerFactory.getLogger(RuleTypeResource.class);

    private static final String ENTITY_NAME = "ruleType";

    private final RuleTypeService ruleTypeService;

    public RuleTypeResource(RuleTypeService ruleTypeService) {
        this.ruleTypeService = ruleTypeService;
    }

    /**
     * POST  /rule-types : Create a new ruleType.
     *
     * @param ruleTypeDTO the ruleTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ruleTypeDTO, or with status 400 (Bad Request) if the ruleType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rule-types")
    @Timed
    public ResponseEntity<RuleTypeDTO> createRuleType(@RequestBody RuleTypeDTO ruleTypeDTO) throws URISyntaxException {
        log.debug("REST request to save RuleType : {}", ruleTypeDTO);
        if (ruleTypeDTO.getIdRuleType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new ruleType cannot already have an ID")).body(null);
        }
        RuleTypeDTO result = ruleTypeService.save(ruleTypeDTO);
        return ResponseEntity.created(new URI("/api/rule-types/" + result.getIdRuleType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRuleType().toString()))
            .body(result);
    }

    /**
     * POST  /rule-types/execute{id}/{param} : Execute Bussiness Process ruleType.
     *
     * @param ruleTypeDTO the ruleTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  ruleTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rule-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<RuleTypeDTO> executedRuleType(@PathVariable Integer id, @PathVariable String param, @RequestBody RuleTypeDTO ruleTypeDTO) throws URISyntaxException {
        log.debug("REST request to process RuleType : {}", ruleTypeDTO);
        RuleTypeDTO result = ruleTypeService.processExecuteData(id, param, ruleTypeDTO);
        return new ResponseEntity<RuleTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /rule-types/execute-list{id}/{param} : Execute Bussiness Process ruleType.
     *
     * @param ruleTypeDTO the ruleTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  ruleTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rule-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<RuleTypeDTO>> executedListRuleType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<RuleTypeDTO> ruleTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List RuleType");
        Set<RuleTypeDTO> result = ruleTypeService.processExecuteListData(id, param, ruleTypeDTO);
        return new ResponseEntity<Set<RuleTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /rule-types : Updates an existing ruleType.
     *
     * @param ruleTypeDTO the ruleTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ruleTypeDTO,
     * or with status 400 (Bad Request) if the ruleTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the ruleTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rule-types")
    @Timed
    public ResponseEntity<RuleTypeDTO> updateRuleType(@RequestBody RuleTypeDTO ruleTypeDTO) throws URISyntaxException {
        log.debug("REST request to update RuleType : {}", ruleTypeDTO);
        if (ruleTypeDTO.getIdRuleType() == null) {
            return createRuleType(ruleTypeDTO);
        }
        RuleTypeDTO result = ruleTypeService.save(ruleTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ruleTypeDTO.getIdRuleType().toString()))
            .body(result);
    }

    /**
     * GET  /rule-types : get all the ruleTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of ruleTypes in body
     */
    @GetMapping("/rule-types")
    @Timed
    public ResponseEntity<List<RuleTypeDTO>> getAllRuleTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of RuleTypes");
        Page<RuleTypeDTO> page = ruleTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rule-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /rule-types/:id : get the "id" ruleType.
     *
     * @param id the id of the ruleTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ruleTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/rule-types/{id}")
    @Timed
    public ResponseEntity<RuleTypeDTO> getRuleType(@PathVariable Integer id) {
        log.debug("REST request to get RuleType : {}", id);
        RuleTypeDTO ruleTypeDTO = ruleTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ruleTypeDTO));
    }

    /**
     * DELETE  /rule-types/:id : delete the "id" ruleType.
     *
     * @param id the id of the ruleTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rule-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteRuleType(@PathVariable Integer id) {
        log.debug("REST request to delete RuleType : {}", id);
        ruleTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/rule-types?query=:query : search for the ruleType corresponding
     * to the query.
     *
     * @param query the query of the ruleType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/rule-types")
    @Timed
    public ResponseEntity<List<RuleTypeDTO>> searchRuleTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of RuleTypes for query {}", query);
        Page<RuleTypeDTO> page = ruleTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/rule-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
