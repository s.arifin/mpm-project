package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.BillingTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.BillingTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BillingType.
 */
@RestController
@RequestMapping("/api")
public class BillingTypeResource {

    private final Logger log = LoggerFactory.getLogger(BillingTypeResource.class);

    private static final String ENTITY_NAME = "billingType";

    private final BillingTypeService billingTypeService;

    public BillingTypeResource(BillingTypeService billingTypeService) {
        this.billingTypeService = billingTypeService;
    }

    /**
     * POST  /billing-types : Create a new billingType.
     *
     * @param billingTypeDTO the billingTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new billingTypeDTO, or with status 400 (Bad Request) if the billingType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-types")
    @Timed
    public ResponseEntity<BillingTypeDTO> createBillingType(@RequestBody BillingTypeDTO billingTypeDTO) throws URISyntaxException {
        log.debug("REST request to save BillingType : {}", billingTypeDTO);
        if (billingTypeDTO.getIdBillingType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new billingType cannot already have an ID")).body(null);
        }
        BillingTypeDTO result = billingTypeService.save(billingTypeDTO);

        return ResponseEntity.created(new URI("/api/billing-types/" + result.getIdBillingType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdBillingType().toString()))
            .body(result);
    }

    /**
     * POST  /billing-types/execute : Execute Bussiness Process billingType.
     *
     * @param billingTypeDTO the billingTypeDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  billingTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-types/execute")
    @Timed
    public ResponseEntity<BillingTypeDTO> executedBillingType(@RequestBody BillingTypeDTO billingTypeDTO) throws URISyntaxException {
        log.debug("REST request to process BillingType : {}", billingTypeDTO);
        return new ResponseEntity<BillingTypeDTO>(billingTypeDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /billing-types : Updates an existing billingType.
     *
     * @param billingTypeDTO the billingTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated billingTypeDTO,
     * or with status 400 (Bad Request) if the billingTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the billingTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/billing-types")
    @Timed
    public ResponseEntity<BillingTypeDTO> updateBillingType(@RequestBody BillingTypeDTO billingTypeDTO) throws URISyntaxException {
        log.debug("REST request to update BillingType : {}", billingTypeDTO);
        if (billingTypeDTO.getIdBillingType() == null) {
            return createBillingType(billingTypeDTO);
        }
        BillingTypeDTO result = billingTypeService.save(billingTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, billingTypeDTO.getIdBillingType().toString()))
            .body(result);
    }

    /**
     * GET  /billing-types : get all the billingTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of billingTypes in body
     */
    @GetMapping("/billing-types")
    @Timed
    public ResponseEntity<List<BillingTypeDTO>> getAllBillingTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of BillingTypes");
        Page<BillingTypeDTO> page = billingTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/billing-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /billing-types/:id : get the "id" billingType.
     *
     * @param id the id of the billingTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the billingTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/billing-types/{id}")
    @Timed
    public ResponseEntity<BillingTypeDTO> getBillingType(@PathVariable Integer id) {
        log.debug("REST request to get BillingType : {}", id);
        BillingTypeDTO billingTypeDTO = billingTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(billingTypeDTO));
    }

    /**
     * DELETE  /billing-types/:id : delete the "id" billingType.
     *
     * @param id the id of the billingTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/billing-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteBillingType(@PathVariable Integer id) {
        log.debug("REST request to delete BillingType : {}", id);
        billingTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/billing-types?query=:query : search for the billingType corresponding
     * to the query.
     *
     * @param query the query of the billingType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/billing-types")
    @Timed
    public ResponseEntity<List<BillingTypeDTO>> searchBillingTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of BillingTypes for query {}", query);
        Page<BillingTypeDTO> page = billingTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/billing-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/billing-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processBillingType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process Billing Type ");
        Map<String, Object> result = billingTypeService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
