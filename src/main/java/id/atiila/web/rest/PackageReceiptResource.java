package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PackageReceiptService;
import id.atiila.service.pto.ShipmentPTO;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PackageReceiptDTO;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.olap4j.impl.ArrayMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PackageReceipt.
 */
@RestController
@RequestMapping("/api")
public class PackageReceiptResource {

    private final Logger log = LoggerFactory.getLogger(PackageReceiptResource.class);

    private static final String ENTITY_NAME = "packageReceipt";

    private final PackageReceiptService packageReceiptService;

    public PackageReceiptResource(PackageReceiptService packageReceiptService) {
        this.packageReceiptService = packageReceiptService;
    }

    /**
     * POST  /package-receipts : Create a new packageReceipt.
     *
     * @param packageReceiptDTO the packageReceiptDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new packageReceiptDTO, or with status 400 (Bad Request) if the packageReceipt has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/package-receipts")
    @Timed
    public ResponseEntity<PackageReceiptDTO> createPackageReceipt(@RequestBody PackageReceiptDTO packageReceiptDTO) throws URISyntaxException {
        log.debug("REST request to save PackageReceipt : {}", packageReceiptDTO);
        if (packageReceiptDTO.getIdPackage() != null) {
            throw new BadRequestAlertException("A new packageReceipt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PackageReceiptDTO result = packageReceiptService.save(packageReceiptDTO);
        return ResponseEntity.created(new URI("/api/package-receipts/" + result.getIdPackage()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPackage().toString()))
            .body(result);
    }

    /**
     * POST  /package-receipts/process : Execute Bussiness Process packageReceipt.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/package-receipts/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processPackageReceipt(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PackageReceipt ");
        Map<String, Object> result = new ArrayMap<>();
        try {
            result = packageReceiptService.process(request, item);
            TimeUnit.MILLISECONDS.sleep(100);
        } catch (Exception e) {
            result.put("errorMessage", e.getMessage());
            return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.EXPECTATION_FAILED);
        }
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /package-receipts/execute : Execute Bussiness Process packageReceipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  packageReceiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/package-receipts/execute")
    @Timed
    public ResponseEntity<PackageReceiptDTO> executedPackageReceipt(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PackageReceipt");
        PackageReceiptDTO result = packageReceiptService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<PackageReceiptDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /package-receipts/execute-list : Execute Bussiness Process packageReceipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  packageReceiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/package-receipts/execute-list")
    @Timed
    public ResponseEntity<Set<PackageReceiptDTO>> executedListPackageReceipt(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List PackageReceipt");
        Set<PackageReceiptDTO> result = packageReceiptService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<PackageReceiptDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /package-receipts : Updates an existing packageReceipt.
     *
     * @param packageReceiptDTO the packageReceiptDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated packageReceiptDTO,
     * or with status 400 (Bad Request) if the packageReceiptDTO is not valid,
     * or with status 500 (Internal Server Error) if the packageReceiptDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/package-receipts")
    @Timed
    public ResponseEntity<PackageReceiptDTO> updatePackageReceipt(@RequestBody PackageReceiptDTO packageReceiptDTO) throws URISyntaxException {
        log.debug("REST request to update PackageReceipt : {}", packageReceiptDTO);
        if (packageReceiptDTO.getIdPackage() == null) {
            return createPackageReceipt(packageReceiptDTO);
        }
        PackageReceiptDTO result = packageReceiptService.save(packageReceiptDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, packageReceiptDTO.getIdPackage().toString()))
            .body(result);
    }

    /**
     * GET  /package-receipts : get all the packageReceipts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of packageReceipts in body
     */
    @GetMapping("/package-receipts")
    @Timed
    public ResponseEntity<List<PackageReceiptDTO>> getAllPackageReceipts(Pageable pageable) {
        log.debug("REST request to get a page of PackageReceipts");
        Page<PackageReceiptDTO> page = packageReceiptService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/package-receipts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/package-receipts/filterBy")
    @Timed
    public ResponseEntity<List<PackageReceiptDTO>> getAllFilteredPackageReceipts(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of PackageReceipts");
        Page<PackageReceiptDTO> page = packageReceiptService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/package-receipts/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/package-receipts/filterByLov")
    @Timed
    public ResponseEntity<List<PackageReceiptDTO>> getAllPackageReceipts(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of PackageReceipts");
        Page<PackageReceiptDTO> page = packageReceiptService.findFilterByLov(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/package-receipts/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/package-receipts/filterByPTO")
    @Timed
    public ResponseEntity<List<PackageReceiptDTO>> getAllFilteredPackageReceiptsByPTO( @ApiParam Pageable pageable,
                                                                                       @ApiParam ShipmentPTO param) {
        Page<PackageReceiptDTO> page = packageReceiptService.findFilterByPTO(pageable, param);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/package-receipts/filterByPTO");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /package-receipts/:id : get the "id" packageReceipt.
     *
     * @param id the id of the packageReceiptDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the packageReceiptDTO, or with status 404 (Not Found)
     */
    @GetMapping("/package-receipts/{id}")
    @Timed
    public ResponseEntity<PackageReceiptDTO> getPackageReceipt(@PathVariable UUID id) {
        log.debug("REST request to get PackageReceipt : {}", id);
        PackageReceiptDTO packageReceiptDTO = packageReceiptService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(packageReceiptDTO));
    }

    @GetMapping("/package-receipts/by-doc-number")
    @Timed
    public ResponseEntity<PackageReceiptDTO> getPackageReceiptByDocumentNumber(@RequestParam String docNumber) {
        log.debug("REST request to get PackageReceipt by Document Number: {}", docNumber);
        PackageReceiptDTO packageReceiptDTO = packageReceiptService.findOneByDocumentNumber(docNumber);
        return new ResponseEntity<>(packageReceiptDTO, null, HttpStatus.OK);
    }

    /**
     * DELETE  /package-receipts/:id : delete the "id" packageReceipt.
     *
     * @param id the id of the packageReceiptDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/package-receipts/{id}")
    @Timed
    public ResponseEntity<Void> deletePackageReceipt(@PathVariable UUID id) {
        log.debug("REST request to delete PackageReceipt : {}", id);
        packageReceiptService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/package-receipts?query=:query : search for the packageReceipt corresponding
     * to the query.
     *
     * @param query the query of the packageReceipt search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/package-receipts")
    @Timed
    public ResponseEntity<List<PackageReceiptDTO>> searchPackageReceipts(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of PackageReceipts for query {}", query);
        Page<PackageReceiptDTO> page = packageReceiptService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/package-receipts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/package-receipts/set-status/{id}")
    @Timed
    public ResponseEntity< PackageReceiptDTO> setStatusPackageReceipt(@PathVariable Integer id, @RequestBody PackageReceiptDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status PackageReceipt : {}", dto);
        PackageReceiptDTO r = packageReceiptService.changePackageReceiptStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
