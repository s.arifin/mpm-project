package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RequestItemService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RequestItemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RequestItem.
 */
@RestController
@RequestMapping("/api")
public class RequestItemResource {

    private final Logger log = LoggerFactory.getLogger(RequestItemResource.class);

    private static final String ENTITY_NAME = "requestItem";

    private final RequestItemService requestItemService;

    public RequestItemResource(RequestItemService requestItemService) {
        this.requestItemService = requestItemService;
    }

    /**
     * POST  /request-items : Create a new requestItem.
     *
     * @param requestItemDTO the requestItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new requestItemDTO, or with status 400 (Bad Request) if the requestItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-items")
    @Timed
    public ResponseEntity<RequestItemDTO> createRequestItem(@RequestBody RequestItemDTO requestItemDTO) throws URISyntaxException {
        log.debug("REST request to save RequestItem : {}", requestItemDTO);
        if (requestItemDTO.getIdRequestItem() != null) {
            throw new BadRequestAlertException("A new requestItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RequestItemDTO result = requestItemService.save(requestItemDTO);
        return ResponseEntity.created(new URI("/api/request-items/" + result.getIdRequestItem()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequestItem().toString()))
            .body(result);
    }

    /**
     * POST  /request-items/process : Execute Bussiness Process requestItem.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-items/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processRequestItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RequestItem ");
        Map<String, Object> result = requestItemService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /request-items/execute : Execute Bussiness Process requestItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  requestItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-items/execute")
    @Timed
    public ResponseEntity<RequestItemDTO> executedRequestItem(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RequestItem");
        RequestItemDTO result = requestItemService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<RequestItemDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /request-items/execute-list : Execute Bussiness Process requestItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  requestItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-items/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<RequestItemDTO>> executedListRequestItem(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<RequestItemDTO> requestItemDTO) throws URISyntaxException{
        log.debug("REST request to process List RequestItem");
        Set<RequestItemDTO> result = requestItemService.processExecuteListData(id, param, requestItemDTO);
        return new ResponseEntity<Set<RequestItemDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /request-items : Updates an existing requestItem.
     *
     * @param requestItemDTO the requestItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated requestItemDTO,
     * or with status 400 (Bad Request) if the requestItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the requestItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/request-items")
    @Timed
    public ResponseEntity<RequestItemDTO> updateRequestItem(@RequestBody RequestItemDTO requestItemDTO) throws URISyntaxException {
        log.debug("REST request to update RequestItem : {}", requestItemDTO);
        if (requestItemDTO.getIdRequestItem() == null) {
            return createRequestItem(requestItemDTO);
        }
        RequestItemDTO result = requestItemService.save(requestItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, requestItemDTO.getIdRequestItem().toString()))
            .body(result);
    }

    /**
     * GET  /request-items : get all the requestItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of requestItems in body
     */
    @GetMapping("/request-items")
    @Timed
    public ResponseEntity<List<RequestItemDTO>> getAllRequestItems(Pageable pageable) {
        log.debug("REST request to get a page of RequestItems");
        Page<RequestItemDTO> page = requestItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/request-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/request-items/filterBy")
    @Timed
    public ResponseEntity<List<RequestItemDTO>> getAllFilteredRequestItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of RequestItems");
        Page<RequestItemDTO> page = requestItemService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/request-items/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /request-items/:id : get the "id" requestItem.
     *
     * @param id the id of the requestItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the requestItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/request-items/{id}")
    @Timed
    public ResponseEntity<RequestItemDTO> getRequestItem(@PathVariable UUID id) {
        log.debug("REST request to get RequestItem : {}", id);
        RequestItemDTO requestItemDTO = requestItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(requestItemDTO));
    }

    /**
     * DELETE  /request-items/:id : delete the "id" requestItem.
     *
     * @param id the id of the requestItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/request-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteRequestItem(@PathVariable UUID id) {
        log.debug("REST request to delete RequestItem : {}", id);
        requestItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/request-items?query=:query : search for the requestItem corresponding
     * to the query.
     *
     * @param query the query of the requestItem search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/request-items")
    @Timed
    public ResponseEntity<List<RequestItemDTO>> searchRequestItems(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of RequestItems for query {}", query);
        Page<RequestItemDTO> page = requestItemService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/request-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
