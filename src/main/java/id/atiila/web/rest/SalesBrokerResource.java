package id.atiila.web.rest;

import java.util.Map;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.SalesBrokerService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.SalesBrokerDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SalesBroker.
 */
@RestController
@RequestMapping("/api")
public class SalesBrokerResource {

    private final Logger log = LoggerFactory.getLogger(SalesBrokerResource.class);

    private static final String ENTITY_NAME = "salesBroker";

    private final SalesBrokerService salesBrokerService;

    public SalesBrokerResource(SalesBrokerService salesBrokerService) {
        this.salesBrokerService = salesBrokerService;
    }

    /**
     * POST  /sales-brokers : Create a new salesBroker.
     *
     * @param salesBrokerDTO the salesBrokerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new salesBrokerDTO, or with status 400 (Bad Request) if the salesBroker has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-brokers")
    @Timed
    public ResponseEntity<SalesBrokerDTO> createSalesBroker(@RequestBody SalesBrokerDTO salesBrokerDTO) throws URISyntaxException {
        log.debug("REST request to save SalesBroker : {}", salesBrokerDTO);
        if (salesBrokerDTO.getIdPartyRole() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new salesBroker cannot already have an ID")).body(null);
        }
        SalesBrokerDTO result = salesBrokerService.save(salesBrokerDTO);

        return ResponseEntity.created(new URI("/api/sales-brokers/" + result.getIdPartyRole()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPartyRole().toString()))
            .body(result);
    }

    /**
     * POST  /sales-brokers/execute : Execute Bussiness Process salesBroker.
     *
     * @param salesBrokerDTO the salesBrokerDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  salesBrokerDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-brokers/execute")
    @Timed
    public ResponseEntity<SalesBrokerDTO> executedSalesBroker(@RequestBody SalesBrokerDTO salesBrokerDTO) throws URISyntaxException {
        log.debug("REST request to process SalesBroker : {}", salesBrokerDTO);
        return new ResponseEntity<SalesBrokerDTO>(salesBrokerDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /sales-brokers : Updates an existing salesBroker.
     *
     * @param salesBrokerDTO the salesBrokerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated salesBrokerDTO,
     * or with status 400 (Bad Request) if the salesBrokerDTO is not valid,
     * or with status 500 (Internal Server Error) if the salesBrokerDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sales-brokers")
    @Timed
    public ResponseEntity<SalesBrokerDTO> updateSalesBroker(@RequestBody SalesBrokerDTO salesBrokerDTO) throws URISyntaxException {
        log.debug("REST request to update SalesBroker : {}", salesBrokerDTO);
        if (salesBrokerDTO.getIdPartyRole() == null) {
            return createSalesBroker(salesBrokerDTO);
        }
        SalesBrokerDTO result = salesBrokerService.save(salesBrokerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, salesBrokerDTO.getIdPartyRole().toString()))
            .body(result);
    }

    /**
     * GET  /sales-brokers : get all the salesBrokers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of salesBrokers in body
     */
    @GetMapping("/sales-brokers")
    @Timed
    public ResponseEntity<List<SalesBrokerDTO>> getAllSalesBrokers(@RequestParam(required = false) String idbrokertype, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SalesBrokers");
        Integer brokerTypeId = null;
        if (idbrokertype != null) { brokerTypeId = Integer.parseInt(idbrokertype); }
        Page<SalesBrokerDTO> page = salesBrokerService.findAll(pageable, brokerTypeId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-brokers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /sales-brokers/:id : get the "id" salesBroker.
     *
     * @param id the id of the salesBrokerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the salesBrokerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sales-brokers/{id}")
    @Timed
    public ResponseEntity<SalesBrokerDTO> getSalesBroker(@PathVariable UUID id) {
        log.debug("REST request to get SalesBroker : {}", id);
        SalesBrokerDTO salesBrokerDTO = salesBrokerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(salesBrokerDTO));
    }

    /**
     * DELETE  /sales-brokers/:id : delete the "id" salesBroker.
     *
     * @param id the id of the salesBrokerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sales-brokers/{id}")
    @Timed
    public ResponseEntity<Void> deleteSalesBroker(@PathVariable UUID id) {
        log.debug("REST request to delete SalesBroker : {}", id);
        salesBrokerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/sales-brokers?query=:query : search for the salesBroker corresponding
     * to the query.
     *
     * @param query the query of the salesBroker search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/sales-brokers")
    @Timed
    public ResponseEntity<List<SalesBrokerDTO>> searchSalesBrokers(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SalesBrokers for query {}", query);
        Page<SalesBrokerDTO> page = salesBrokerService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/sales-brokers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/sales-brokers/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processSalesBroker(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process SalesBroker ");
        Map<String, Object> result = salesBrokerService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
