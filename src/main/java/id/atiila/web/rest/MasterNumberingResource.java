package id.atiila.web.rest;

import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.MasterNumberingService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.MasterNumberingDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MasterNumbering.
 */
@RestController
@RequestMapping("/api")
public class MasterNumberingResource {

    private final Logger log = LoggerFactory.getLogger(MasterNumberingResource.class);

    private static final String ENTITY_NAME = "masterNumbering";

    private final MasterNumberingService masterNumberingService;

    public MasterNumberingResource(MasterNumberingService masterNumberingService) {
        this.masterNumberingService = masterNumberingService;
    }

    /**
     * POST  /master-numberings : Create a new masterNumbering.
     *
     * @param masterNumberingDTO the masterNumberingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new masterNumberingDTO, or with status 400 (Bad Request) if the masterNumbering has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/master-numberings")
    @Timed
    public ResponseEntity<MasterNumberingDTO> createMasterNumbering(@RequestBody MasterNumberingDTO masterNumberingDTO) throws URISyntaxException {
        log.debug("REST request to save MasterNumbering : {}", masterNumberingDTO);
        if (masterNumberingDTO.getIdNumbering() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new masterNumbering cannot already have an ID")).body(null);
        }
        MasterNumberingDTO result = masterNumberingService.save(masterNumberingDTO);
        
        return ResponseEntity.created(new URI("/api/master-numberings/" + result.getIdNumbering()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdNumbering().toString()))
            .body(result);
    }

    /**
     * POST  /master-numberings/execute : Execute Bussiness Process masterNumbering.
     *
     * @param masterNumberingDTO the masterNumberingDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  masterNumberingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/master-numberings/execute")
    @Timed
    public ResponseEntity<MasterNumberingDTO> executedMasterNumbering(@RequestBody MasterNumberingDTO masterNumberingDTO) throws URISyntaxException {
        log.debug("REST request to process MasterNumbering : {}", masterNumberingDTO);
        return new ResponseEntity<MasterNumberingDTO>(masterNumberingDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /master-numberings : Updates an existing masterNumbering.
     *
     * @param masterNumberingDTO the masterNumberingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated masterNumberingDTO,
     * or with status 400 (Bad Request) if the masterNumberingDTO is not valid,
     * or with status 500 (Internal Server Error) if the masterNumberingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/master-numberings")
    @Timed
    public ResponseEntity<MasterNumberingDTO> updateMasterNumbering(@RequestBody MasterNumberingDTO masterNumberingDTO) throws URISyntaxException {
        log.debug("REST request to update MasterNumbering : {}", masterNumberingDTO);
        if (masterNumberingDTO.getIdNumbering() == null) {
            return createMasterNumbering(masterNumberingDTO);
        }
        MasterNumberingDTO result = masterNumberingService.save(masterNumberingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, masterNumberingDTO.getIdNumbering().toString()))
            .body(result);
    }

    /**
     * GET  /master-numberings : get all the masterNumberings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of masterNumberings in body
     */
    @GetMapping("/master-numberings")
    @Timed
    public ResponseEntity<List<MasterNumberingDTO>> getAllMasterNumberings(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of MasterNumberings");
        Page<MasterNumberingDTO> page = masterNumberingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/master-numberings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /master-numberings/:id : get the "id" masterNumbering.
     *
     * @param id the id of the masterNumberingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the masterNumberingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/master-numberings/{id}")
    @Timed
    public ResponseEntity<MasterNumberingDTO> getMasterNumbering(@PathVariable UUID id) {
        log.debug("REST request to get MasterNumbering : {}", id);
        MasterNumberingDTO masterNumberingDTO = masterNumberingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(masterNumberingDTO));
    }

    /**
     * DELETE  /master-numberings/:id : delete the "id" masterNumbering.
     *
     * @param id the id of the masterNumberingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/master-numberings/{id}")
    @Timed
    public ResponseEntity<Void> deleteMasterNumbering(@PathVariable UUID id) {
        log.debug("REST request to delete MasterNumbering : {}", id);
        masterNumberingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/master-numberings?query=:query : search for the masterNumbering corresponding
     * to the query.
     *
     * @param query the query of the masterNumbering search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/master-numberings")
    @Timed
    public ResponseEntity<List<MasterNumberingDTO>> searchMasterNumberings(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of MasterNumberings for query {}", query);
        Page<MasterNumberingDTO> page = masterNumberingService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/master-numberings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
