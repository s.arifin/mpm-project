package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ProspectService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ProspectDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Prospect.
 */
@RestController
@RequestMapping("/api")
public class ProspectResource {

    private final Logger log = LoggerFactory.getLogger(ProspectResource.class);

    private static final String ENTITY_NAME = "prospect";

    private final ProspectService prospectService;

    public ProspectResource(ProspectService prospectService) {
        this.prospectService = prospectService;
    }

    /**
     * POST  /prospects : Create a new prospect.
     *
     * @param prospectDTO the prospectDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new prospectDTO, or with status 400 (Bad Request) if the prospect has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prospects")
    @Timed
    public ResponseEntity<ProspectDTO> createProspect(@RequestBody ProspectDTO prospectDTO) throws URISyntaxException {
        log.debug("REST request to save Prospect : {}", prospectDTO);
        if (prospectDTO.getIdProspect() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new prospect cannot already have an ID")).body(null);
        }
        ProspectDTO result = prospectService.save(prospectDTO);
        return ResponseEntity.created(new URI("/api/prospects/" + result.getIdProspect()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdProspect().toString()))
            .body(result);
    }

    /**
     * POST  /prospects/execute{id}/{param} : Execute Bussiness Process prospect.
     *
     * @param prospectDTO the prospectDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  prospectDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prospects/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ProspectDTO> executedProspect(@PathVariable Integer id, @PathVariable String param, @RequestBody ProspectDTO prospectDTO) throws URISyntaxException {
        log.debug("REST request to process Prospect : {}", prospectDTO);
        ProspectDTO result = prospectService.processExecuteData(id, param, prospectDTO);
        return new ResponseEntity<ProspectDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /prospects/execute-list{id}/{param} : Execute Bussiness Process prospect.
     *
     * @param prospectDTO the prospectDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  prospectDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prospects/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ProspectDTO>> executedListProspect(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ProspectDTO> prospectDTO) throws URISyntaxException {
        log.debug("REST request to process List Prospect");
        Set<ProspectDTO> result = prospectService.processExecuteListData(id, param, prospectDTO);
        return new ResponseEntity<Set<ProspectDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /prospects : Updates an existing prospect.
     *
     * @param prospectDTO the prospectDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated prospectDTO,
     * or with status 400 (Bad Request) if the prospectDTO is not valid,
     * or with status 500 (Internal Server Error) if the prospectDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/prospects")
    @Timed
    public ResponseEntity<ProspectDTO> updateProspect(@RequestBody ProspectDTO prospectDTO) throws URISyntaxException {
        log.debug("REST request to update Prospect : {}", prospectDTO);
        if (prospectDTO.getIdProspect() == null) {
            return createProspect(prospectDTO);
        }
        ProspectDTO result = prospectService.save(prospectDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, prospectDTO.getIdProspect().toString()))
            .body(result);
    }

    /**
     * GET  /prospects : get all the prospects.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of prospects in body
     */
    @GetMapping("/prospects")
    @Timed
    public ResponseEntity<List<ProspectDTO>> getAllProspects(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Prospects");
        Page<ProspectDTO> page = prospectService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/prospects");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /prospects/:id : get the "id" prospect.
     *
     * @param id the id of the prospectDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the prospectDTO, or with status 404 (Not Found)
     */
    @GetMapping("/prospects/{id}")
    @Timed
    public ResponseEntity<ProspectDTO> getProspect(@PathVariable UUID id) {
        log.debug("REST request to get Prospect : {}", id);
        ProspectDTO prospectDTO = prospectService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(prospectDTO));
    }

    /**
     * DELETE  /prospects/:id : delete the "id" prospect.
     *
     * @param id the id of the prospectDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/prospects/{id}")
    @Timed
    public ResponseEntity<Void> deleteProspect(@PathVariable UUID id) {
        log.debug("REST request to delete Prospect : {}", id);
        prospectService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/prospects?query=:query : search for the prospect corresponding
     * to the query.
     *
     * @param query the query of the prospect search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/prospects")
    @Timed
    public ResponseEntity<List<ProspectDTO>> searchProspects(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Prospects for query {}", query);
        Page<ProspectDTO> page = prospectService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/prospects");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/prospects/set-status/{id}")
    @Timed
    public ResponseEntity< ProspectDTO> setStatusProspect(@PathVariable Integer id, @RequestBody ProspectDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status Prospect : {}", dto);
        ProspectDTO r = prospectService.changeProspectStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    @PostMapping("/prospects/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processProspect(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process Prospect ");
        Map<String, Object> result = prospectService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
