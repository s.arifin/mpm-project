package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PartSalesOrderService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PartSalesOrderDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PartSalesOrder.
 */
@RestController
@RequestMapping("/api")
public class PartSalesOrderResource {

    private final Logger log = LoggerFactory.getLogger(PartSalesOrderResource.class);

    private static final String ENTITY_NAME = "partSalesOrder";

    private final PartSalesOrderService partSalesOrderService;

    public PartSalesOrderResource(PartSalesOrderService partSalesOrderService) {
        this.partSalesOrderService = partSalesOrderService;
    }

    /**
     * POST  /part-sales-orders : Create a new partSalesOrder.
     *
     * @param partSalesOrderDTO the partSalesOrderDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new partSalesOrderDTO, or with status 400 (Bad Request) if the partSalesOrder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/part-sales-orders")
    @Timed
    public ResponseEntity<PartSalesOrderDTO> createPartSalesOrder(@RequestBody PartSalesOrderDTO partSalesOrderDTO) throws URISyntaxException {
        log.debug("REST request to save PartSalesOrder : {}", partSalesOrderDTO);
        if (partSalesOrderDTO.getIdOrder() != null) {
            throw new BadRequestAlertException("A new partSalesOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PartSalesOrderDTO result = partSalesOrderService.save(partSalesOrderDTO);
        return ResponseEntity.created(new URI("/api/part-sales-orders/" + result.getIdOrder()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdOrder().toString()))
            .body(result);
    }

    /**
     * POST  /part-sales-orders/process : Execute Bussiness Process partSalesOrder.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/part-sales-orders/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processPartSalesOrder(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PartSalesOrder ");
        Map<String, Object> result = partSalesOrderService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /part-sales-orders/execute : Execute Bussiness Process partSalesOrder.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  partSalesOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/part-sales-orders/execute")
    @Timed
    public ResponseEntity<PartSalesOrderDTO> executedPartSalesOrder(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PartSalesOrder");
        PartSalesOrderDTO result = partSalesOrderService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<PartSalesOrderDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /part-sales-orders/execute-list : Execute Bussiness Process partSalesOrder.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  partSalesOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/part-sales-orders/execute-list")
    @Timed
    public ResponseEntity<Set<PartSalesOrderDTO>> executedListPartSalesOrder(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List PartSalesOrder");
        Set<PartSalesOrderDTO> result = partSalesOrderService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<PartSalesOrderDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /part-sales-orders : Updates an existing partSalesOrder.
     *
     * @param partSalesOrderDTO the partSalesOrderDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated partSalesOrderDTO,
     * or with status 400 (Bad Request) if the partSalesOrderDTO is not valid,
     * or with status 500 (Internal Server Error) if the partSalesOrderDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/part-sales-orders")
    @Timed
    public ResponseEntity<PartSalesOrderDTO> updatePartSalesOrder(@RequestBody PartSalesOrderDTO partSalesOrderDTO) throws URISyntaxException {
        log.debug("REST request to update PartSalesOrder : {}", partSalesOrderDTO);
        if (partSalesOrderDTO.getIdOrder() == null) {
            return createPartSalesOrder(partSalesOrderDTO);
        }
        PartSalesOrderDTO result = partSalesOrderService.save(partSalesOrderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, partSalesOrderDTO.getIdOrder().toString()))
            .body(result);
    }

    /**
     * GET  /part-sales-orders : get all the partSalesOrders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of partSalesOrders in body
     */
    @GetMapping("/part-sales-orders")
    @Timed
    public ResponseEntity<List<PartSalesOrderDTO>> getAllPartSalesOrders(Pageable pageable) {
        log.debug("REST request to get a page of PartSalesOrders");
        Page<PartSalesOrderDTO> page = partSalesOrderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/part-sales-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/part-sales-orders/filterBy")
    @Timed
    public ResponseEntity<List<PartSalesOrderDTO>> getAllFilteredPartSalesOrders(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of PartSalesOrders");
        Page<PartSalesOrderDTO> page = partSalesOrderService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/part-sales-orders/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /part-sales-orders/:id : get the "id" partSalesOrder.
     *
     * @param id the id of the partSalesOrderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the partSalesOrderDTO, or with status 404 (Not Found)
     */
    @GetMapping("/part-sales-orders/{id}")
    @Timed
    public ResponseEntity<PartSalesOrderDTO> getPartSalesOrder(@PathVariable UUID id) {
        log.debug("REST request to get PartSalesOrder : {}", id);
        PartSalesOrderDTO partSalesOrderDTO = partSalesOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(partSalesOrderDTO));
    }

    /**
     * DELETE  /part-sales-orders/:id : delete the "id" partSalesOrder.
     *
     * @param id the id of the partSalesOrderDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/part-sales-orders/{id}")
    @Timed
    public ResponseEntity<Void> deletePartSalesOrder(@PathVariable UUID id) {
        log.debug("REST request to delete PartSalesOrder : {}", id);
        partSalesOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/part-sales-orders?query=:query : search for the partSalesOrder corresponding
     * to the query.
     *
     * @param query the query of the partSalesOrder search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/part-sales-orders")
    @Timed
    public ResponseEntity<List<PartSalesOrderDTO>> searchPartSalesOrders(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of PartSalesOrders for query {}", query);
        Page<PartSalesOrderDTO> page = partSalesOrderService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/part-sales-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/part-sales-orders/set-status/{id}")
    @Timed
    public ResponseEntity< PartSalesOrderDTO> setStatusPartSalesOrder(@PathVariable Integer id, @RequestBody PartSalesOrderDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status PartSalesOrder : {}", dto);
        PartSalesOrderDTO r = partSalesOrderService.changePartSalesOrderStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
