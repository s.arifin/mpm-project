package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RuleIndentService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RuleIndentDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;
import javax.xml.ws.Response;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RuleIndent.
 */
@RestController
@RequestMapping("/api")
public class RuleIndentResource {

    private final Logger log = LoggerFactory.getLogger(RuleIndentResource.class);

    private static final String ENTITY_NAME = "ruleIndent";

    private final RuleIndentService ruleIndentService;

    public RuleIndentResource(RuleIndentService ruleIndentService) {
        this.ruleIndentService = ruleIndentService;
    }

    @GetMapping("/rule-indents/check-by-product-and-internal")
    @Timed
    public ResponseEntity<Boolean> checkRuleIndentsByProduct(@RequestParam String idProduct, @RequestParam String idInternal){
        log.debug("REST to check rule indent by product and internal");
        Boolean r = ruleIndentService.checkIsIndentByProduct(idProduct, idInternal);
        return new ResponseEntity<>(r, null, HttpStatus.OK);
    }

    @GetMapping("/rule-indents/by-product")
    @Timed
    public ResponseEntity<List<RuleIndentDTO>> getRuleIndentsByProduct(@RequestParam String idProduct, @RequestParam String idInternal, Pageable pageable){
        log.debug("REST to get rule indent by product");
        Page<RuleIndentDTO> page = ruleIndentService.getRuleIndentByProduct(idProduct, idInternal, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rule-indents");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * POST  /rule-indents : Create a new ruleIndent.
     *
     * @param ruleIndentDTO the ruleIndentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ruleIndentDTO, or with status 400 (Bad Request) if the ruleIndent has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rule-indents")
    @Timed
    public ResponseEntity<RuleIndentDTO> createRuleIndent(@RequestBody RuleIndentDTO ruleIndentDTO) throws URISyntaxException {
        log.debug("REST request to save RuleIndent : {}", ruleIndentDTO);
        if (ruleIndentDTO.getIdRule() != null) {
            throw new BadRequestAlertException("A new ruleIndent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RuleIndentDTO result = ruleIndentService.save(ruleIndentDTO);
        return ResponseEntity.created(new URI("/api/rule-indents/" + result.getIdRule()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRule().toString()))
            .body(result);
    }

    /**
     * POST  /rule-indents/process : Execute Bussiness Process ruleIndent.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rule-indents/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processRuleIndent(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RuleIndent ");
        Map<String, Object> result = ruleIndentService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /rule-indents/execute : Execute Bussiness Process ruleIndent.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  ruleIndentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rule-indents/execute")
    @Timed
    public ResponseEntity<RuleIndentDTO> executedRuleIndent(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RuleIndent");
        RuleIndentDTO result = ruleIndentService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<RuleIndentDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /rule-indents/execute-list : Execute Bussiness Process ruleIndent.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  ruleIndentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rule-indents/execute-list")
    @Timed
    public ResponseEntity<Set<RuleIndentDTO>> executedListRuleIndent(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List RuleIndent");
        Set<RuleIndentDTO> result = ruleIndentService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<RuleIndentDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /rule-indents : Updates an existing ruleIndent.
     *
     * @param ruleIndentDTO the ruleIndentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ruleIndentDTO,
     * or with status 400 (Bad Request) if the ruleIndentDTO is not valid,
     * or with status 500 (Internal Server Error) if the ruleIndentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rule-indents")
    @Timed
    public ResponseEntity<RuleIndentDTO> updateRuleIndent(@RequestBody RuleIndentDTO ruleIndentDTO) throws URISyntaxException {
        log.debug("REST request to update RuleIndent : {}", ruleIndentDTO);
        if (ruleIndentDTO.getIdRule() == null) {
            return createRuleIndent(ruleIndentDTO);
        }
        RuleIndentDTO result = ruleIndentService.save(ruleIndentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ruleIndentDTO.getIdRule().toString()))
            .body(result);
    }

    /**
     * GET  /rule-indents : get all the ruleIndents.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of ruleIndents in body
     */
    @GetMapping("/rule-indents")
    @Timed
    public ResponseEntity<List<RuleIndentDTO>> getAllRuleIndents(Pageable pageable) {
        log.debug("REST request to get a page of RuleIndents");
        Page<RuleIndentDTO> page = ruleIndentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rule-indents");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/rule-indents/filterBy")
    @Timed
    public ResponseEntity<List<RuleIndentDTO>> getAllFilteredRuleIndents(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of RuleIndents");
        Page<RuleIndentDTO> page = ruleIndentService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rule-indents/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rule-indents/:id : get the "id" ruleIndent.
     *
     * @param id the id of the ruleIndentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ruleIndentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/rule-indents/{id}")
    @Timed
    public ResponseEntity<RuleIndentDTO> getRuleIndent(@PathVariable Integer id) {
        log.debug("REST request to get RuleIndent : {}", id);
        RuleIndentDTO ruleIndentDTO = ruleIndentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ruleIndentDTO));
    }

    /**
     * DELETE  /rule-indents/:id : delete the "id" ruleIndent.
     *
     * @param id the id of the ruleIndentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rule-indents/{id}")
    @Timed
    public ResponseEntity<Void> deleteRuleIndent(@PathVariable Integer id) {
        log.debug("REST request to delete RuleIndent : {}", id);
        ruleIndentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/rule-indents?query=:query : search for the ruleIndent corresponding
     * to the query.
     *
     * @param query the query of the ruleIndent search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/rule-indents")
    @Timed
    public ResponseEntity<List<RuleIndentDTO>> searchRuleIndents(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of RuleIndents for query {}", query);
        Page<RuleIndentDTO> page = ruleIndentService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/rule-indents");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
