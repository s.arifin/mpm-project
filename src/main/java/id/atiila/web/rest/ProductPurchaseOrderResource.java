package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ProductPurchaseOrderService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ProductPurchaseOrderDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProductPurchaseOrder.
 */
@RestController
@RequestMapping("/api")
public class ProductPurchaseOrderResource {

    private final Logger log = LoggerFactory.getLogger(ProductPurchaseOrderResource.class);

    private static final String ENTITY_NAME = "productPurchaseOrder";

    private final ProductPurchaseOrderService productPurchaseOrderService;

    public ProductPurchaseOrderResource(ProductPurchaseOrderService productPurchaseOrderService) {
        this.productPurchaseOrderService = productPurchaseOrderService;
    }

    /**
     * POST  /product-purchase-orders : Create a new productPurchaseOrder.
     *
     * @param productPurchaseOrderDTO the productPurchaseOrderDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productPurchaseOrderDTO, or with status 400 (Bad Request) if the productPurchaseOrder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-purchase-orders")
    @Timed
    public ResponseEntity<ProductPurchaseOrderDTO> createProductPurchaseOrder(@RequestBody ProductPurchaseOrderDTO productPurchaseOrderDTO) throws URISyntaxException {
        log.debug("REST request to save ProductPurchaseOrder : {}", productPurchaseOrderDTO);
        if (productPurchaseOrderDTO.getIdOrder() != null) {
            throw new BadRequestAlertException("A new productPurchaseOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductPurchaseOrderDTO result = productPurchaseOrderService.save(productPurchaseOrderDTO);
        return ResponseEntity.created(new URI("/api/product-purchase-orders/" + result.getIdOrder()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdOrder().toString()))
            .body(result);
    }

    /**
     * POST  /product-purchase-orders/process : Execute Bussiness Process productPurchaseOrder.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-purchase-orders/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processProductPurchaseOrder(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ProductPurchaseOrder ");
        Map<String, Object> result = productPurchaseOrderService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /product-purchase-orders/execute : Execute Bussiness Process productPurchaseOrder.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  productPurchaseOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-purchase-orders/execute")
    @Timed
    public ResponseEntity<ProductPurchaseOrderDTO> executedProductPurchaseOrder(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ProductPurchaseOrder");
        ProductPurchaseOrderDTO result = productPurchaseOrderService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<ProductPurchaseOrderDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /product-purchase-orders/execute-list : Execute Bussiness Process productPurchaseOrder.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  productPurchaseOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-purchase-orders/execute-list")
    @Timed
    public ResponseEntity<Set<ProductPurchaseOrderDTO>> executedListProductPurchaseOrder(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List ProductPurchaseOrder");
        Set<ProductPurchaseOrderDTO> result = productPurchaseOrderService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<ProductPurchaseOrderDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /product-purchase-orders : Updates an existing productPurchaseOrder.
     *
     * @param productPurchaseOrderDTO the productPurchaseOrderDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productPurchaseOrderDTO,
     * or with status 400 (Bad Request) if the productPurchaseOrderDTO is not valid,
     * or with status 500 (Internal Server Error) if the productPurchaseOrderDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/product-purchase-orders")
    @Timed
    public ResponseEntity<ProductPurchaseOrderDTO> updateProductPurchaseOrder(@RequestBody ProductPurchaseOrderDTO productPurchaseOrderDTO) throws URISyntaxException {
        log.debug("REST request to update ProductPurchaseOrder : {}", productPurchaseOrderDTO);
        if (productPurchaseOrderDTO.getIdOrder() == null) {
            return createProductPurchaseOrder(productPurchaseOrderDTO);
        }
        ProductPurchaseOrderDTO result = productPurchaseOrderService.save(productPurchaseOrderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, productPurchaseOrderDTO.getIdOrder().toString()))
            .body(result);
    }

    /**
     * GET  /product-purchase-orders : get all the productPurchaseOrders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of productPurchaseOrders in body
     */
    @GetMapping("/product-purchase-orders")
    @Timed
    public ResponseEntity<List<ProductPurchaseOrderDTO>> getAllProductPurchaseOrders(Pageable pageable) {
        log.debug("REST request to get a page of ProductPurchaseOrders");
        Page<ProductPurchaseOrderDTO> page = productPurchaseOrderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-purchase-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/product-purchase-orders/filterBy")
    @Timed
    public ResponseEntity<List<ProductPurchaseOrderDTO>> getAllFilteredProductPurchaseOrders(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ProductPurchaseOrders");
        Page<ProductPurchaseOrderDTO> page = productPurchaseOrderService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-purchase-orders/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /product-purchase-orders/:id : get the "id" productPurchaseOrder.
     *
     * @param id the id of the productPurchaseOrderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productPurchaseOrderDTO, or with status 404 (Not Found)
     */
    @GetMapping("/product-purchase-orders/{id}")
    @Timed
    public ResponseEntity<ProductPurchaseOrderDTO> getProductPurchaseOrder(@PathVariable UUID id) {
        log.debug("REST request to get ProductPurchaseOrder : {}", id);
        ProductPurchaseOrderDTO productPurchaseOrderDTO = productPurchaseOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(productPurchaseOrderDTO));
    }

    /**
     * DELETE  /product-purchase-orders/:id : delete the "id" productPurchaseOrder.
     *
     * @param id the id of the productPurchaseOrderDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/product-purchase-orders/{id}")
    @Timed
    public ResponseEntity<Void> deleteProductPurchaseOrder(@PathVariable UUID id) {
        log.debug("REST request to delete ProductPurchaseOrder : {}", id);
        productPurchaseOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/product-purchase-orders?query=:query : search for the productPurchaseOrder corresponding
     * to the query.
     *
     * @param query the query of the productPurchaseOrder search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/product-purchase-orders")
    @Timed
    public ResponseEntity<List<ProductPurchaseOrderDTO>> searchProductPurchaseOrders(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ProductPurchaseOrders for query {}", query);
        Page<ProductPurchaseOrderDTO> page = productPurchaseOrderService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/product-purchase-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/product-purchase-orders/set-status/{id}")
    @Timed
    public ResponseEntity< ProductPurchaseOrderDTO> setStatusProductPurchaseOrder(@PathVariable Integer id, @RequestBody ProductPurchaseOrderDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status ProductPurchaseOrder : {}", dto);
        ProductPurchaseOrderDTO r = productPurchaseOrderService.changeProductPurchaseOrderStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
