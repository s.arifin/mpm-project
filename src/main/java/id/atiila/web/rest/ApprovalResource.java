package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ApprovalService;
import id.atiila.service.dto.SalesUnitRequirementDTO;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ApprovalDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Approval.
 */
@RestController
@RequestMapping("/api")
public class ApprovalResource {

    private final Logger log = LoggerFactory.getLogger(ApprovalResource.class);

    private static final String ENTITY_NAME = "approval";

    private final ApprovalService approvalService;

    public ApprovalResource(ApprovalService approvalService) {
        this.approvalService = approvalService;
    }

    /**
     * POST  /approvals : Create a new approval.
     *
     * @param approvalDTO the approvalDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new approvalDTO, or with status 400 (Bad Request) if the approval has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/approvals")
    @Timed
    public ResponseEntity<ApprovalDTO> createApproval(@RequestBody ApprovalDTO approvalDTO) throws URISyntaxException {
        log.debug("REST request to save Approval : {}", approvalDTO);
        if (approvalDTO.getIdApproval() != null) {
            throw new BadRequestAlertException("A new approval cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ApprovalDTO result = approvalService.save(approvalDTO);
        return ResponseEntity.created(new URI("/api/approvals/" + result.getIdApproval()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdApproval().toString()))
            .body(result);
    }

    /**
     * POST  /approvals/execute{id}/{param} : Execute Bussiness Process approval.
     *
     * @param approvalDTO the approvalDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  approvalDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/approvals/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ApprovalDTO> executedApproval(@PathVariable Integer id, @PathVariable String param, @RequestBody ApprovalDTO approvalDTO) throws URISyntaxException {
        log.debug("REST request to process Approval : {}", approvalDTO);
        ApprovalDTO result = approvalService.processExecuteData(id, param, approvalDTO);
        return new ResponseEntity<ApprovalDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /approvals/execute-list{id}/{param} : Execute Bussiness Process approval.
     *
     * @param approvalDTO the approvalDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  approvalDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/approvals/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ApprovalDTO>> executedListApproval(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ApprovalDTO> approvalDTO) throws URISyntaxException {
        log.debug("REST request to process List Approval");
        Set<ApprovalDTO> result = approvalService.processExecuteListData(id, param, approvalDTO);
        return new ResponseEntity<Set<ApprovalDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /approvals : Updates an existing approval.
     *
     * @param approvalDTO the approvalDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated approvalDTO,
     * or with status 400 (Bad Request) if the approvalDTO is not valid,
     * or with status 500 (Internal Server Error) if the approvalDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/approvals")
    @Timed
    public ResponseEntity<ApprovalDTO> updateApproval(@RequestBody ApprovalDTO approvalDTO) throws URISyntaxException {
        log.debug("REST request to update Approval : {}", approvalDTO);
        if (approvalDTO.getIdApproval() == null) {
            return createApproval(approvalDTO);
        }
        ApprovalDTO result = approvalService.save(approvalDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, approvalDTO.getIdApproval().toString()))
            .body(result);
    }

    /**
     * GET  /approvals : get all the approvals.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of approvals in body
     */
    @GetMapping("/approvals")
    @Timed
    public ResponseEntity<List<ApprovalDTO>> getAllApprovals(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Approvals");
        Page<ApprovalDTO> page = approvalService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/approvals");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/approvals/leasing-approval")
    @Timed
    public ResponseEntity<List<ApprovalDTO>> getAllApprovalLeasing(HttpServletRequest request, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Approvals");
        Page<ApprovalDTO> page = approvalService.findApproval(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/approvals/leasing-approval");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping(path ="/approvals/leasing-approval/email")
    @Timed
    public ResponseEntity<List<ApprovalDTO>> getAllApprovalLeasingEmail(HttpServletRequest request, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Approvals");
        Page<ApprovalDTO> page = approvalService.findApproval(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/approvals/leasing-approval/email");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET /approvals/kacab/subsidi-and-territorial-violation : get data for kacab approval
     *
     * @return the ResponseEntity with status 200 and the list of SUR
     */

    @GetMapping("/approvals/kacab/sales-unit-requirement/subsidi")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getSurApprovalSubsidiAndForKacab(@ApiParam Pageable pageable, @RequestParam String idinternal){
        log.debug("REST request to get a page of SUR Approval by kacab");
        Page<SalesUnitRequirementDTO> page = approvalService.findWaitingForApprovalSURForKacab(idinternal, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "api/approvals/kacab/sales-unit-requirement/subsidi");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/approvals/kacab/sales-unit-requirement/pelanggaran-wilayah")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getSurApprovalPelanggaranWilayahForKacab(@ApiParam Pageable pageable, @RequestParam String idinternal){
        log.debug("REST request to get a page of SUR Approval by kacab");
        Page<SalesUnitRequirementDTO> page = approvalService.findWaitingTerritorialViolationForApprovalSURForKacab(idinternal, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "api/approvals/kacab/sales-unit-requirement/pelanggaran-wilayah");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/approvals/korsal/sales-unit-requirement/subsidi")
    @Timed
    public ResponseEntity<List<SalesUnitRequirementDTO>> getSurApprovalSubsidiForKorsal(@ApiParam Pageable pageable, @RequestParam String idinternal) {
        log.debug("REST request to get a page of SUR Approval for korsal");
        Page<SalesUnitRequirementDTO> page = approvalService.findWaitingForApprovalSURForKorsal(idinternal, "ROLE_KORSAL", pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "api/approvals/korsal/subsidi");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /approvals/:id : get the "id" approval.
     *
     * @param id the id of the approvalDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the approvalDTO, or with status 404 (Not Found)
     */
    @GetMapping("/approvals/{id}")
    @Timed
    public ResponseEntity<ApprovalDTO> getApproval(@PathVariable UUID id) {
        log.debug("REST request to get Approval : {}", id);
        ApprovalDTO approvalDTO = approvalService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(approvalDTO));
    }

    /**
     * DELETE  /approvals/:id : delete the "id" approval.
     *
     * @param id the id of the approvalDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/approvals/{id}")
    @Timed
    public ResponseEntity<Void> deleteApproval(@PathVariable UUID id) {
        log.debug("REST request to delete Approval : {}", id);
        approvalService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/approvals?query=:query : search for the approval corresponding
     * to the query.
     *
     * @param query the query of the approval search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/approvals")
    @Timed
    public ResponseEntity<List<ApprovalDTO>> searchApprovals(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Approvals for query {}", query);
        Page<ApprovalDTO> page = approvalService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/approvals");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
