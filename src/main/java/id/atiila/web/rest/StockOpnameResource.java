package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.StockOpnameService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.StockOpnameDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing StockOpname.
 */
@RestController
@RequestMapping("/api")
public class StockOpnameResource {

    private final Logger log = LoggerFactory.getLogger(StockOpnameResource.class);

    private static final String ENTITY_NAME = "stockOpname";

    private final StockOpnameService stockOpnameService;

    public StockOpnameResource(StockOpnameService stockOpnameService) {
        this.stockOpnameService = stockOpnameService;
    }

    /**
     * POST  /stock-opnames : Create a new stockOpname.
     *
     * @param stockOpnameDTO the stockOpnameDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new stockOpnameDTO, or with status 400 (Bad Request) if the stockOpname has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opnames")
    @Timed
    public ResponseEntity<StockOpnameDTO> createStockOpname(@RequestBody StockOpnameDTO stockOpnameDTO) throws URISyntaxException {
        log.debug("REST request to save StockOpname : {}", stockOpnameDTO);
        if (stockOpnameDTO.getIdStockOpname() != null) {
            throw new BadRequestAlertException("A new stockOpname cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StockOpnameDTO result = stockOpnameService.save(stockOpnameDTO);
        return ResponseEntity.created(new URI("/api/stock-opnames/" + result.getIdStockOpname()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdStockOpname().toString()))
            .body(result);
    }

    /**
     * POST  /stock-opnames/process : Execute Bussiness Process stockOpname.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opnames/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processStockOpname(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process StockOpname ");
        Map<String, Object> result = stockOpnameService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /stock-opnames/execute : Execute Bussiness Process stockOpname.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  stockOpnameDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opnames/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedStockOpname(HttpServletRequest request, @RequestBody StockOpnameDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process StockOpname");
        Map<String, Object> result = stockOpnameService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /stock-opnames/execute-list : Execute Bussiness Process stockOpname.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  stockOpnameDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-opnames/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListStockOpname(HttpServletRequest request, @RequestBody Set<StockOpnameDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List StockOpname");
        Map<String, Object> result = stockOpnameService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /stock-opnames : Updates an existing stockOpname.
     *
     * @param stockOpnameDTO the stockOpnameDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated stockOpnameDTO,
     * or with status 400 (Bad Request) if the stockOpnameDTO is not valid,
     * or with status 500 (Internal Server Error) if the stockOpnameDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/stock-opnames")
    @Timed
    public ResponseEntity<StockOpnameDTO> updateStockOpname(@RequestBody StockOpnameDTO stockOpnameDTO) throws URISyntaxException {
        log.debug("REST request to update StockOpname : {}", stockOpnameDTO);
        if (stockOpnameDTO.getIdStockOpname() == null) {
            return createStockOpname(stockOpnameDTO);
        }
        StockOpnameDTO result = stockOpnameService.save(stockOpnameDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, stockOpnameDTO.getIdStockOpname().toString()))
            .body(result);
    }

    /**
     * GET  /stock-opnames : get all the stockOpnames.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of stockOpnames in body
     */
    @GetMapping("/stock-opnames")
    @Timed
    public ResponseEntity<List<StockOpnameDTO>> getAllStockOpnames(Pageable pageable) {
        log.debug("REST request to get a page of StockOpnames");
        Page<StockOpnameDTO> page = stockOpnameService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/stock-opnames");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/stock-opnames/filterBy")
    @Timed
    public ResponseEntity<List<StockOpnameDTO>> getAllFilteredStockOpnames(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of StockOpnames");
        Page<StockOpnameDTO> page = stockOpnameService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/stock-opnames/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /stock-opnames/:id : get the "id" stockOpname.
     *
     * @param id the id of the stockOpnameDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the stockOpnameDTO, or with status 404 (Not Found)
     */
    @GetMapping("/stock-opnames/{id}")
    @Timed
    public ResponseEntity<StockOpnameDTO> getStockOpname(@PathVariable UUID id) {
        log.debug("REST request to get StockOpname : {}", id);
        StockOpnameDTO stockOpnameDTO = stockOpnameService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(stockOpnameDTO));
    }

    /**
     * DELETE  /stock-opnames/:id : delete the "id" stockOpname.
     *
     * @param id the id of the stockOpnameDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/stock-opnames/{id}")
    @Timed
    public ResponseEntity<Void> deleteStockOpname(@PathVariable UUID id) {
        log.debug("REST request to delete StockOpname : {}", id);
        stockOpnameService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/stock-opnames?query=:query : search for the stockOpname corresponding
     * to the query.
     *
     * @param query the query of the stockOpname search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/stock-opnames")
    @Timed
    public ResponseEntity<List<StockOpnameDTO>> searchStockOpnames(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of StockOpnames for query {}", query);
        Page<StockOpnameDTO> page = stockOpnameService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/stock-opnames");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/stock-opnames/set-status/{id}")
    @Timed
    public ResponseEntity< StockOpnameDTO> setStatusStockOpname(@PathVariable Integer id, @RequestBody StockOpnameDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status StockOpname : {}", dto);
        StockOpnameDTO r = stockOpnameService.changeStockOpnameStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
