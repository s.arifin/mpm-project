package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.VendorTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VendorTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VendorType.
 */
@RestController
@RequestMapping("/api")
public class VendorTypeResource {

    private final Logger log = LoggerFactory.getLogger(VendorTypeResource.class);

    private static final String ENTITY_NAME = "vendorType";

    private final VendorTypeService vendorTypeService;

    public VendorTypeResource(VendorTypeService vendorTypeService) {
        this.vendorTypeService = vendorTypeService;
    }

    /**
     * POST  /vendor-types : Create a new vendorType.
     *
     * @param vendorTypeDTO the vendorTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vendorTypeDTO, or with status 400 (Bad Request) if the vendorType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendor-types")
    @Timed
    public ResponseEntity<VendorTypeDTO> createVendorType(@RequestBody VendorTypeDTO vendorTypeDTO) throws URISyntaxException {
        log.debug("REST request to save VendorType : {}", vendorTypeDTO);
        if (vendorTypeDTO.getIdVendorType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new vendorType cannot already have an ID")).body(null);
        }
        VendorTypeDTO result = vendorTypeService.save(vendorTypeDTO);
        return ResponseEntity.created(new URI("/api/vendor-types/" + result.getIdVendorType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdVendorType().toString()))
            .body(result);
    }

    /**
     * POST  /vendor-types/execute{id}/{param} : Execute Bussiness Process vendorType.
     *
     * @param vendorTypeDTO the vendorTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  vendorTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendor-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<VendorTypeDTO> executedVendorType(@PathVariable Integer id, @PathVariable String param, @RequestBody VendorTypeDTO vendorTypeDTO) throws URISyntaxException {
        log.debug("REST request to process VendorType : {}", vendorTypeDTO);
        VendorTypeDTO result = vendorTypeService.processExecuteData(id, param, vendorTypeDTO);
        return new ResponseEntity<VendorTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vendor-types/execute-list{id}/{param} : Execute Bussiness Process vendorType.
     *
     * @param vendorTypeDTO the vendorTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  vendorTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendor-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<VendorTypeDTO>> executedListVendorType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<VendorTypeDTO> vendorTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List VendorType");
        Set<VendorTypeDTO> result = vendorTypeService.processExecuteListData(id, param, vendorTypeDTO);
        return new ResponseEntity<Set<VendorTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /vendor-types : Updates an existing vendorType.
     *
     * @param vendorTypeDTO the vendorTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vendorTypeDTO,
     * or with status 400 (Bad Request) if the vendorTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the vendorTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vendor-types")
    @Timed
    public ResponseEntity<VendorTypeDTO> updateVendorType(@RequestBody VendorTypeDTO vendorTypeDTO) throws URISyntaxException {
        log.debug("REST request to update VendorType : {}", vendorTypeDTO);
        if (vendorTypeDTO.getIdVendorType() == null) {
            return createVendorType(vendorTypeDTO);
        }
        VendorTypeDTO result = vendorTypeService.save(vendorTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vendorTypeDTO.getIdVendorType().toString()))
            .body(result);
    }

    /**
     * GET  /vendor-types : get all the vendorTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vendorTypes in body
     */
    @GetMapping("/vendor-types")
    @Timed
    public ResponseEntity<List<VendorTypeDTO>> getAllVendorTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of VendorTypes");
        Page<VendorTypeDTO> page = vendorTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vendor-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /vendor-types/:id : get the "id" vendorType.
     *
     * @param id the id of the vendorTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vendorTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vendor-types/{id}")
    @Timed
    public ResponseEntity<VendorTypeDTO> getVendorType(@PathVariable Integer id) {
        log.debug("REST request to get VendorType : {}", id);
        VendorTypeDTO vendorTypeDTO = vendorTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vendorTypeDTO));
    }

    /**
     * DELETE  /vendor-types/:id : delete the "id" vendorType.
     *
     * @param id the id of the vendorTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vendor-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteVendorType(@PathVariable Integer id) {
        log.debug("REST request to delete VendorType : {}", id);
        vendorTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/vendor-types?query=:query : search for the vendorType corresponding
     * to the query.
     *
     * @param query the query of the vendorType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vendor-types")
    @Timed
    public ResponseEntity<List<VendorTypeDTO>> searchVendorTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of VendorTypes for query {}", query);
        Page<VendorTypeDTO> page = vendorTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vendor-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
