package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.repository.VehicleDocumentRequirementRepository;
import id.atiila.service.VehicleDocumentRequirementService;
import id.atiila.service.dto.CustomRequirementDTO;
import id.atiila.service.dto.CustomVDRDTO;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VehicleDocumentRequirementDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.concurrent.TimeUnit;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VehicleDocumentRequirement.
 */
@RestController
@RequestMapping("/api")
public class VehicleDocumentRequirementResource {

    private final Logger log = LoggerFactory.getLogger(VehicleDocumentRequirementResource.class);

    private static final String ENTITY_NAME = "vehicleDocumentRequirement";

    private final VehicleDocumentRequirementService vehicleDocumentRequirementService;

    @Autowired
    VehicleDocumentRequirementRepository vehicleDocumentRequirementRepository;

    public VehicleDocumentRequirementResource(VehicleDocumentRequirementService vehicleDocumentRequirementService) {
        this.vehicleDocumentRequirementService = vehicleDocumentRequirementService;
    }

    /**
     * POST  /vehicle-document-requirements : Create a new vehicleDocumentRequirement.
     *
     * @param vehicleDocumentRequirementDTO the vehicleDocumentRequirementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vehicleDocumentRequirementDTO, or with status 400 (Bad Request) if the vehicleDocumentRequirement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-document-requirements")
    @Timed
    public ResponseEntity<VehicleDocumentRequirementDTO> createVehicleDocumentRequirement(@RequestBody VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO) throws URISyntaxException {
        log.debug("REST request to save VehicleDocumentRequirement : {}", vehicleDocumentRequirementDTO);
        if (vehicleDocumentRequirementDTO.getIdRequirement() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new vehicleDocumentRequirement cannot already have an ID")).body(null);
        }
        VehicleDocumentRequirementDTO result = vehicleDocumentRequirementService.save(vehicleDocumentRequirementDTO);
        return ResponseEntity.created(new URI("/api/vehicle-document-requirements/" + result.getIdRequirement()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * POST  /vehicle-document-requirements/execute{id}/{param} : Execute Bussiness Process vehicleDocumentRequirement.
     *
     * @param vehicleDocumentRequirementDTO the vehicleDocumentRequirementDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  vehicleDocumentRequirementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-document-requirements/execute/{id}/{param}")
    @Timed
    public ResponseEntity<VehicleDocumentRequirementDTO> executedVehicleDocumentRequirement(@PathVariable Integer id, @PathVariable String param, @RequestBody VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO) throws URISyntaxException {
        log.debug("REST request to process VehicleDocumentRequirement : {}", vehicleDocumentRequirementDTO);
        VehicleDocumentRequirementDTO result = vehicleDocumentRequirementService.processExecuteData(id, param, vehicleDocumentRequirementDTO);
        return new ResponseEntity<VehicleDocumentRequirementDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vehicle-document-requirements/execute-list{id}/{param} : Execute Bussiness Process vehicleDocumentRequirement.
     *
     * @param vehicleDocumentRequirementDTO the vehicleDocumentRequirementDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  vehicleDocumentRequirementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-document-requirements/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<VehicleDocumentRequirementDTO>> executedListVehicleDocumentRequirement(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<VehicleDocumentRequirementDTO> vehicleDocumentRequirementDTO) throws URISyntaxException {
        log.debug("REST request to process List VehicleDocumentRequirement");
        Set<VehicleDocumentRequirementDTO> result = vehicleDocumentRequirementService.processExecuteListData(id, param, vehicleDocumentRequirementDTO);
//        if (id == 32){
//
//        } else {
//
//        }
        return new ResponseEntity<Set<VehicleDocumentRequirementDTO>>(result, null, HttpStatus.OK);
    }

    @GetMapping("/vehicle-document-requirements/findCustom/{id}")
    @Timed
    public ResponseEntity<CustomRequirementDTO> findCustom(@RequestBody String  id) throws URISyntaxException {
        log.debug("REST request to process List VehicleDocumentRequirement");
        UUID idReg = UUID.fromString(id);
        CustomRequirementDTO result = vehicleDocumentRequirementService.findCustom(idReg);
        return new ResponseEntity<CustomRequirementDTO>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /vehicle-document-requirements : Updates an existing vehicleDocumentRequirement.
     *
     * @param vehicleDocumentRequirementDTO the vehicleDocumentRequirementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vehicleDocumentRequirementDTO,
     * or with status 400 (Bad Request) if the vehicleDocumentRequirementDTO is not valid,
     * or with status 500 (Internal Server Error) if the vehicleDocumentRequirementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vehicle-document-requirements")
    @Timed
    public ResponseEntity<VehicleDocumentRequirementDTO> updateVehicleDocumentRequirement(@RequestBody VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO) throws URISyntaxException {
        log.debug("REST request to update VehicleDocumentRequirement : {}", vehicleDocumentRequirementDTO);
        if (vehicleDocumentRequirementDTO.getIdRequirement() == null) {
            return createVehicleDocumentRequirement(vehicleDocumentRequirementDTO);
        }
        VehicleDocumentRequirementDTO result = vehicleDocumentRequirementService.save(vehicleDocumentRequirementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vehicleDocumentRequirementDTO.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * GET  /vehicle-document-requirements : get all the vehicleDocumentRequirements.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vehicleDocumentRequirements in body
     */
    @GetMapping("/vehicle-document-requirements")
    @Timed
    public ResponseEntity<List<VehicleDocumentRequirementDTO>> getAllVehicleDocumentRequirements(@RequestParam(required = false) Integer idStatusType, @RequestParam(required = false) Boolean statusKonfirmasi,  @RequestParam(required = false) Integer idSaleType, @RequestParam(required = false) String idInternal, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of VehicleDocumentRequirements");
        Page<VehicleDocumentRequirementDTO> page = vehicleDocumentRequirementService.findAll(pageable, idStatusType, idSaleType, statusKonfirmasi, idInternal);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-document-requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-document-requirements/custom")
    @Timed
    public ResponseEntity<List<CustomRequirementDTO>> getCustomVehicleDocumentRequirements(@RequestParam(required = false) Integer idStatusType, @RequestParam(required = false) Boolean statusKonfirmasi, @RequestParam(required = false) Boolean statusPenerimaan, @RequestParam(required = false) Integer idSaleType, @RequestParam(required = false) String idInternal, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of VehicleDocumentRequirements");

        Page<CustomRequirementDTO> r = vehicleDocumentRequirementService.findCustom(pageable, idStatusType, statusKonfirmasi, idInternal, statusPenerimaan);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(r, "/api/vehicle-document-requirements");
        return new ResponseEntity<>(r.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-document-requirements/filter-items")
    @Timed
    public ResponseEntity<List<VehicleDocumentRequirementDTO>> getAllFilteredItemVehicleDocumentRequirements(
                    @ApiParam Pageable pageable, HttpServletRequest request, HttpServletResponse response) {
        log.debug("REST request to get a page of Filter Item VehicleDocumentRequirements");
        Page<VehicleDocumentRequirementDTO> page = vehicleDocumentRequirementService.findQueryItems(pageable, request, response);
        HttpHeaders respHeaders = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-document-requirements/filter-items");
        return new ResponseEntity<>(page.getContent(), respHeaders, HttpStatus.OK);
    }

    /**
     * GET  /vehicle-document-requirements/getbyFrame/:idFrame : get the "idFrame" vehicle.
     *
     * @param id the idFrame of the VehicleDocumentRequirementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the VehicleDocumentRequirementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vehicle-document-requirements/getbyFrame/{id}")
    @Timed
    public ResponseEntity<VehicleDocumentRequirementDTO> getVehicleDocumentRequirementsbyFrame(@PathVariable String id) {
        log.debug("REST request to get Vehicle : {}", id);
        VehicleDocumentRequirementDTO vehicleDTO = vehicleDocumentRequirementService.findOnebyFrame(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vehicleDTO));
    }




    /**
     * GET  /vehicle-document-requirements/:id : get the "id" vehicleDocumentRequirement.
     *
     * @param id the id of the vehicleDocumentRequirementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vehicleDocumentRequirementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vehicle-document-requirements/{id}")
    @Timed
    public ResponseEntity<VehicleDocumentRequirementDTO> getVehicleDocumentRequirement(@PathVariable UUID id) {
        log.debug("REST request to get VehicleDocumentRequirement : {}", id);
        VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO = vehicleDocumentRequirementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vehicleDocumentRequirementDTO));
    }

    /**
     * DELETE  /vehicle-document-requirements/:id : delete the "id" vehicleDocumentRequirement.
     *
     * @param id the id of the vehicleDocumentRequirementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vehicle-document-requirements/{id}")
    @Timed
    public ResponseEntity<Void> deleteVehicleDocumentRequirement(@PathVariable UUID id) {
        log.debug("REST request to delete VehicleDocumentRequirement : {}", id);
        vehicleDocumentRequirementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/vehicle-document-requirements?query=:query : search for the vehicleDocumentRequirement corresponding
     * to the query.
     *
     * @param query the query of the vehicleDocumentRequirement search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vehicle-document-requirements")
    @Timed
    public ResponseEntity<List<VehicleDocumentRequirementDTO>> searchVehicleDocumentRequirements(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of VehicleDocumentRequirements for query {}", query);
        Page<VehicleDocumentRequirementDTO> page = vehicleDocumentRequirementService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vehicle-document-requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/vehicle-document-requirements/set-status/{id}")
    @Timed
    public ResponseEntity< VehicleDocumentRequirementDTO> setStatusVehicleDocumentRequirement(@PathVariable Integer id, @RequestBody VehicleDocumentRequirementDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status VehicleDocumentRequirement : {}", dto);
        VehicleDocumentRequirementDTO r = vehicleDocumentRequirementService.changeVehicleDocumentRequirementStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

//    @GetMapping("/vehicle-document-requirements/customPenerimaan")
//    @Timed
//    public ResponseEntity<List<CustomVehicleDocumentReqDTO>> getCustomPenerimaanVehicleDocumentRequirements(@RequestParam(required = false) Integer idStatusType, @RequestParam(required = false) String idInternal, @ApiParam Pageable pageable) {
//        log.debug("REST request to get a page of CustomVehicleDocumentRequirements");
//
//        Page<CustomVehicleDocumentReqDTO> r = vehicleDocumentRequirementService.findCustomPenerimaan(pageable, idStatusType, idInternal);
//
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(r, "/api/vehicle-document-requirements/");
//        return new ResponseEntity<>(r.getContent(), headers, HttpStatus.OK);
//    }

    /**
     * PUT  /vehicle-document-requirements : Updates an existing vehicleDocumentRequirement.
     *
     * @param vehicleDocumentRequirementDTO the vehicleDocumentRequirementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vehicleDocumentRequirementDTO,
     * or with status 400 (Bad Request) if the vehicleDocumentRequirementDTO is not valid,
     * or with status 500 (Internal Server Error) if the vehicleDocumentRequirementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vehicle-document-requirements/update")
    @Timed
    public ResponseEntity<VehicleDocumentRequirementDTO> queryUpdateVehicleDocumentRequirement(@RequestBody VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO) throws URISyntaxException {
        log.debug("REST request to update VehicleDocumentRequirrrement : {}", vehicleDocumentRequirementDTO);
        log.debug("cekk id requiremennnt" + vehicleDocumentRequirementDTO.getIdRequirement());
        if (vehicleDocumentRequirementDTO.getIdRequirement() == null) {
            return createVehicleDocumentRequirement(vehicleDocumentRequirementDTO);
        }
        VehicleDocumentRequirementDTO result = vehicleDocumentRequirementService.update(vehicleDocumentRequirementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vehicleDocumentRequirementDTO.getIdRequirement().toString()))
            .body(result);
    }

    @PostMapping("/vehicle-document-requirements/updateNote")
    @Timed
    public ResponseEntity<VehicleDocumentRequirementDTO> updateNote(@RequestBody VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO) throws URISyntaxException {
        log.debug("REST request to update notee VehicleDocumentRequirement : {}", vehicleDocumentRequirementDTO);
        VehicleDocumentRequirementDTO result = vehicleDocumentRequirementService.updateNote(vehicleDocumentRequirementDTO);
        return ResponseEntity.created(new URI("/api/vehicle-document-requirements/" + result.getIdRequirement()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequirement().toString()))
            .body(result);
    }


    @GetMapping("/vehicle-document-requirements/filterBy")
    @Timed
    public ResponseEntity<List<VehicleDocumentRequirementDTO>> getAllFilteredVehicleCustomerRequests(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of VehicleCustomerRequests");
        Page<VehicleDocumentRequirementDTO> page = vehicleDocumentRequirementService.queryFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-document-requirements/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/vehicle-document-requirements/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processVehicleRegistration(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process VehicleDocumentRequirement ");
        Map<String, Object> result = vehicleDocumentRequirementService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }
}
