package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ProductShipmentIncomingService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ProductShipmentIncomingDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProductShipmentIncoming.
 */
@RestController
@RequestMapping("/api")
public class ProductShipmentIncomingResource {

    private final Logger log = LoggerFactory.getLogger(ProductShipmentIncomingResource.class);

    private static final String ENTITY_NAME = "productShipmentIncoming";

    private final ProductShipmentIncomingService productShipmentIncomingService;

    public ProductShipmentIncomingResource(ProductShipmentIncomingService productShipmentIncomingService) {
        this.productShipmentIncomingService = productShipmentIncomingService;
    }

    /**
     * POST  /product-shipment-incomings : Create a new productShipmentIncoming.
     *
     * @param productShipmentIncomingDTO the productShipmentIncomingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productShipmentIncomingDTO, or with status 400 (Bad Request) if the productShipmentIncoming has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-shipment-incomings")
    @Timed
    public ResponseEntity<ProductShipmentIncomingDTO> createProductShipmentIncoming(@RequestBody ProductShipmentIncomingDTO productShipmentIncomingDTO) throws URISyntaxException {
        log.debug("REST request to save ProductShipmentIncoming : {}", productShipmentIncomingDTO);
        if (productShipmentIncomingDTO.getIdShipment() != null) {
            throw new BadRequestAlertException("A new productShipmentIncoming cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductShipmentIncomingDTO result = productShipmentIncomingService.save(productShipmentIncomingDTO);
        return ResponseEntity.created(new URI("/api/product-shipment-incomings/" + result.getIdShipment()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdShipment().toString()))
            .body(result);
    }

    /**
     * POST  /product-shipment-incomings/process : Execute Bussiness Process productShipmentIncoming.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-shipment-incomings/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processProductShipmentIncoming(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ProductShipmentIncoming ");
        Map<String, Object> result = productShipmentIncomingService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /product-shipment-incomings/execute : Execute Bussiness Process productShipmentIncoming.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  productShipmentIncomingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-shipment-incomings/execute")
    @Timed
    public ResponseEntity<ProductShipmentIncomingDTO> executedProductShipmentIncoming(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ProductShipmentIncoming");
        ProductShipmentIncomingDTO result = productShipmentIncomingService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<ProductShipmentIncomingDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /product-shipment-incomings/execute-list : Execute Bussiness Process productShipmentIncoming.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  productShipmentIncomingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-shipment-incomings/execute-list")
    @Timed
    public ResponseEntity<Set<ProductShipmentIncomingDTO>> executedListProductShipmentIncoming(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List ProductShipmentIncoming");
        Set<ProductShipmentIncomingDTO> result = productShipmentIncomingService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<ProductShipmentIncomingDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /product-shipment-incomings : Updates an existing productShipmentIncoming.
     *
     * @param productShipmentIncomingDTO the productShipmentIncomingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productShipmentIncomingDTO,
     * or with status 400 (Bad Request) if the productShipmentIncomingDTO is not valid,
     * or with status 500 (Internal Server Error) if the productShipmentIncomingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/product-shipment-incomings")
    @Timed
    public ResponseEntity<ProductShipmentIncomingDTO> updateProductShipmentIncoming(@RequestBody ProductShipmentIncomingDTO productShipmentIncomingDTO) throws URISyntaxException {
        log.debug("REST request to update ProductShipmentIncoming : {}", productShipmentIncomingDTO);
        if (productShipmentIncomingDTO.getIdShipment() == null) {
            return createProductShipmentIncoming(productShipmentIncomingDTO);
        }
        ProductShipmentIncomingDTO result = productShipmentIncomingService.save(productShipmentIncomingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, productShipmentIncomingDTO.getIdShipment().toString()))
            .body(result);
    }

    /**
     * GET  /product-shipment-incomings : get all the productShipmentIncomings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of productShipmentIncomings in body
     */
    @GetMapping("/product-shipment-incomings")
    @Timed
    public ResponseEntity<List<ProductShipmentIncomingDTO>> getAllProductShipmentIncomings(Pageable pageable) {
        log.debug("REST request to get a page of ProductShipmentIncomings");
        Page<ProductShipmentIncomingDTO> page = productShipmentIncomingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-shipment-incomings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/product-shipment-incomings/filterBy")
    @Timed
    public ResponseEntity<List<ProductShipmentIncomingDTO>> getAllFilteredProductShipmentIncomings(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ProductShipmentIncomings");
        Page<ProductShipmentIncomingDTO> page = productShipmentIncomingService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-shipment-incomings/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /product-shipment-incomings/:id : get the "id" productShipmentIncoming.
     *
     * @param id the id of the productShipmentIncomingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productShipmentIncomingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/product-shipment-incomings/{id}")
    @Timed
    public ResponseEntity<ProductShipmentIncomingDTO> getProductShipmentIncoming(@PathVariable UUID id) {
        log.debug("REST request to get ProductShipmentIncoming : {}", id);
        ProductShipmentIncomingDTO productShipmentIncomingDTO = productShipmentIncomingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(productShipmentIncomingDTO));
    }

    /**
     * DELETE  /product-shipment-incomings/:id : delete the "id" productShipmentIncoming.
     *
     * @param id the id of the productShipmentIncomingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/product-shipment-incomings/{id}")
    @Timed
    public ResponseEntity<Void> deleteProductShipmentIncoming(@PathVariable UUID id) {
        log.debug("REST request to delete ProductShipmentIncoming : {}", id);
        productShipmentIncomingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/product-shipment-incomings?query=:query : search for the productShipmentIncoming corresponding
     * to the query.
     *
     * @param query the query of the productShipmentIncoming search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/product-shipment-incomings")
    @Timed
    public ResponseEntity<List<ProductShipmentIncomingDTO>> searchProductShipmentIncomings(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ProductShipmentIncomings for query {}", query);
        Page<ProductShipmentIncomingDTO> page = productShipmentIncomingService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/product-shipment-incomings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/product-shipment-incomings/set-status/{id}")
    @Timed
    public ResponseEntity< ProductShipmentIncomingDTO> setStatusProductShipmentIncoming(@PathVariable Integer id, @RequestBody ProductShipmentIncomingDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status ProductShipmentIncoming : {}", dto);
        ProductShipmentIncomingDTO r = productShipmentIncomingService.changeProductShipmentIncomingStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
