package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ProductPackageReceiptService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ProductPackageReceiptDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProductPackageReceipt.
 */
@RestController
@RequestMapping("/api")
public class ProductPackageReceiptResource {

    private final Logger log = LoggerFactory.getLogger(ProductPackageReceiptResource.class);

    private static final String ENTITY_NAME = "productPackageReceipt";

    private final ProductPackageReceiptService productPackageReceiptService;

    public ProductPackageReceiptResource(ProductPackageReceiptService productPackageReceiptService) {
        this.productPackageReceiptService = productPackageReceiptService;
    }

    /**
     * POST  /product-package-receipts : Create a new productPackageReceipt.
     *
     * @param productPackageReceiptDTO the productPackageReceiptDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productPackageReceiptDTO, or with status 400 (Bad Request) if the productPackageReceipt has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-package-receipts")
    @Timed
    public ResponseEntity<ProductPackageReceiptDTO> createProductPackageReceipt(@RequestBody ProductPackageReceiptDTO productPackageReceiptDTO) throws URISyntaxException {
        log.debug("REST request to save ProductPackageReceipt : {}", productPackageReceiptDTO);
        if (productPackageReceiptDTO.getIdPackage() != null) {
            throw new BadRequestAlertException("A new productPackageReceipt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductPackageReceiptDTO result = productPackageReceiptService.save(productPackageReceiptDTO);
        return ResponseEntity.created(new URI("/api/product-package-receipts/" + result.getIdPackage()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPackage().toString()))
            .body(result);
    }

    /**
     * POST  /product-package-receipts/process : Execute Bussiness Process productPackageReceipt.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-package-receipts/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processProductPackageReceipt(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ProductPackageReceipt ");
        Map<String, Object> result = productPackageReceiptService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /product-package-receipts/execute : Execute Bussiness Process productPackageReceipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  productPackageReceiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-package-receipts/execute")
    @Timed
    public ResponseEntity<ProductPackageReceiptDTO> executedProductPackageReceipt(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process ProductPackageReceipt");
        ProductPackageReceiptDTO result = productPackageReceiptService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<ProductPackageReceiptDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /product-package-receipts/execute-list : Execute Bussiness Process productPackageReceipt.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  productPackageReceiptDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-package-receipts/execute-list")
    @Timed
    public ResponseEntity<Set<ProductPackageReceiptDTO>> executedListProductPackageReceipt(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List ProductPackageReceipt");
        Set<ProductPackageReceiptDTO> result = productPackageReceiptService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<ProductPackageReceiptDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /product-package-receipts : Updates an existing productPackageReceipt.
     *
     * @param productPackageReceiptDTO the productPackageReceiptDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productPackageReceiptDTO,
     * or with status 400 (Bad Request) if the productPackageReceiptDTO is not valid,
     * or with status 500 (Internal Server Error) if the productPackageReceiptDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/product-package-receipts")
    @Timed
    public ResponseEntity<ProductPackageReceiptDTO> updateProductPackageReceipt(@RequestBody ProductPackageReceiptDTO productPackageReceiptDTO) throws URISyntaxException {
        log.debug("REST request to update ProductPackageReceipt : {}", productPackageReceiptDTO);
        if (productPackageReceiptDTO.getIdPackage() == null) {
            return createProductPackageReceipt(productPackageReceiptDTO);
        }
        ProductPackageReceiptDTO result = productPackageReceiptService.save(productPackageReceiptDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, productPackageReceiptDTO.getIdPackage().toString()))
            .body(result);
    }

    /**
     * GET  /product-package-receipts : get all the productPackageReceipts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of productPackageReceipts in body
     */
    @GetMapping("/product-package-receipts")
    @Timed
    public ResponseEntity<List<ProductPackageReceiptDTO>> getAllProductPackageReceipts(Pageable pageable) {
        log.debug("REST request to get a page of ProductPackageReceipts");
        Page<ProductPackageReceiptDTO> page = productPackageReceiptService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-package-receipts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/product-package-receipts/filterBy")
    @Timed
    public ResponseEntity<List<ProductPackageReceiptDTO>> getAllFilteredProductPackageReceipts(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of ProductPackageReceipts");
        Page<ProductPackageReceiptDTO> page = productPackageReceiptService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-package-receipts/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /product-package-receipts/:id : get the "id" productPackageReceipt.
     *
     * @param id the id of the productPackageReceiptDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productPackageReceiptDTO, or with status 404 (Not Found)
     */
    @GetMapping("/product-package-receipts/{id}")
    @Timed
    public ResponseEntity<ProductPackageReceiptDTO> getProductPackageReceipt(@PathVariable UUID id) {
        log.debug("REST request to get ProductPackageReceipt : {}", id);
        ProductPackageReceiptDTO productPackageReceiptDTO = productPackageReceiptService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(productPackageReceiptDTO));
    }

    /**
     * DELETE  /product-package-receipts/:id : delete the "id" productPackageReceipt.
     *
     * @param id the id of the productPackageReceiptDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/product-package-receipts/{id}")
    @Timed
    public ResponseEntity<Void> deleteProductPackageReceipt(@PathVariable UUID id) {
        log.debug("REST request to delete ProductPackageReceipt : {}", id);
        productPackageReceiptService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/product-package-receipts?query=:query : search for the productPackageReceipt corresponding
     * to the query.
     *
     * @param query the query of the productPackageReceipt search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/product-package-receipts")
    @Timed
    public ResponseEntity<List<ProductPackageReceiptDTO>> searchProductPackageReceipts(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ProductPackageReceipts for query {}", query);
        Page<ProductPackageReceiptDTO> page = productPackageReceiptService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/product-package-receipts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/product-package-receipts/set-status/{id}")
    @Timed
    public ResponseEntity< ProductPackageReceiptDTO> setStatusProductPackageReceipt(@PathVariable Integer id, @RequestBody ProductPackageReceiptDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status ProductPackageReceipt : {}", dto);
        ProductPackageReceiptDTO r = productPackageReceiptService.changeProductPackageReceiptStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
