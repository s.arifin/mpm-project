package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.BookingSlotStandardService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.BookingSlotStandardDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BookingSlotStandard.
 */
@RestController
@RequestMapping("/api")
public class BookingSlotStandardResource {

    private final Logger log = LoggerFactory.getLogger(BookingSlotStandardResource.class);

    private static final String ENTITY_NAME = "bookingSlotStandard";

    private final BookingSlotStandardService bookingSlotStandardService;

    public BookingSlotStandardResource(BookingSlotStandardService bookingSlotStandardService) {
        this.bookingSlotStandardService = bookingSlotStandardService;
    }

    /**
     * POST  /booking-slot-standards : Create a new bookingSlotStandard.
     *
     * @param bookingSlotStandardDTO the bookingSlotStandardDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bookingSlotStandardDTO, or with status 400 (Bad Request) if the bookingSlotStandard has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/booking-slot-standards")
    @Timed
    public ResponseEntity<BookingSlotStandardDTO> createBookingSlotStandard(@RequestBody BookingSlotStandardDTO bookingSlotStandardDTO) throws URISyntaxException {
        log.debug("REST request to save BookingSlotStandard : {}", bookingSlotStandardDTO);
        if (bookingSlotStandardDTO.getIdBookSlotStd() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new bookingSlotStandard cannot already have an ID")).body(null);
        }
        BookingSlotStandardDTO result = bookingSlotStandardService.save(bookingSlotStandardDTO);
        return ResponseEntity.created(new URI("/api/booking-slot-standards/" + result.getIdBookSlotStd()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdBookSlotStd().toString()))
            .body(result);
    }

    /**
     * POST  /booking-slot-standards/execute{id}/{param} : Execute Bussiness Process bookingSlotStandard.
     *
     * @param bookingSlotStandardDTO the bookingSlotStandardDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  bookingSlotStandardDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/booking-slot-standards/execute/{id}/{param}")
    @Timed
    public ResponseEntity<BookingSlotStandardDTO> executedBookingSlotStandard(@PathVariable Integer id, @PathVariable String param, @RequestBody BookingSlotStandardDTO bookingSlotStandardDTO) throws URISyntaxException {
        log.debug("REST request to process BookingSlotStandard : {}", bookingSlotStandardDTO);
        BookingSlotStandardDTO result = bookingSlotStandardService.processExecuteData(id, param, bookingSlotStandardDTO);
        return new ResponseEntity<BookingSlotStandardDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /booking-slot-standards/execute-list{id}/{param} : Execute Bussiness Process bookingSlotStandard.
     *
     * @param bookingSlotStandardDTO the bookingSlotStandardDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  bookingSlotStandardDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/booking-slot-standards/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<BookingSlotStandardDTO>> executedListBookingSlotStandard(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<BookingSlotStandardDTO> bookingSlotStandardDTO) throws URISyntaxException {
        log.debug("REST request to process List BookingSlotStandard");
        Set<BookingSlotStandardDTO> result = bookingSlotStandardService.processExecuteListData(id, param, bookingSlotStandardDTO);
        return new ResponseEntity<Set<BookingSlotStandardDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /booking-slot-standards : Updates an existing bookingSlotStandard.
     *
     * @param bookingSlotStandardDTO the bookingSlotStandardDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bookingSlotStandardDTO,
     * or with status 400 (Bad Request) if the bookingSlotStandardDTO is not valid,
     * or with status 500 (Internal Server Error) if the bookingSlotStandardDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/booking-slot-standards")
    @Timed
    public ResponseEntity<BookingSlotStandardDTO> updateBookingSlotStandard(@RequestBody BookingSlotStandardDTO bookingSlotStandardDTO) throws URISyntaxException {
        log.debug("REST request to update BookingSlotStandard : {}", bookingSlotStandardDTO);
        if (bookingSlotStandardDTO.getIdBookSlotStd() == null) {
            return createBookingSlotStandard(bookingSlotStandardDTO);
        }
        BookingSlotStandardDTO result = bookingSlotStandardService.save(bookingSlotStandardDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bookingSlotStandardDTO.getIdBookSlotStd().toString()))
            .body(result);
    }

    /**
     * GET  /booking-slot-standards : get all the bookingSlotStandards.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of bookingSlotStandards in body
     */
    @GetMapping("/booking-slot-standards")
    @Timed
    public ResponseEntity<List<BookingSlotStandardDTO>> getAllBookingSlotStandards(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of BookingSlotStandards");
        Page<BookingSlotStandardDTO> page = bookingSlotStandardService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/booking-slot-standards");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /booking-slot-standards/:id : get the "id" bookingSlotStandard.
     *
     * @param id the id of the bookingSlotStandardDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bookingSlotStandardDTO, or with status 404 (Not Found)
     */
    @GetMapping("/booking-slot-standards/{id}")
    @Timed
    public ResponseEntity<BookingSlotStandardDTO> getBookingSlotStandard(@PathVariable UUID id) {
        log.debug("REST request to get BookingSlotStandard : {}", id);
        BookingSlotStandardDTO bookingSlotStandardDTO = bookingSlotStandardService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(bookingSlotStandardDTO));
    }

    /**
     * DELETE  /booking-slot-standards/:id : delete the "id" bookingSlotStandard.
     *
     * @param id the id of the bookingSlotStandardDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/booking-slot-standards/{id}")
    @Timed
    public ResponseEntity<Void> deleteBookingSlotStandard(@PathVariable UUID id) {
        log.debug("REST request to delete BookingSlotStandard : {}", id);
        bookingSlotStandardService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/booking-slot-standards?query=:query : search for the bookingSlotStandard corresponding
     * to the query.
     *
     * @param query the query of the bookingSlotStandard search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/booking-slot-standards")
    @Timed
    public ResponseEntity<List<BookingSlotStandardDTO>> searchBookingSlotStandards(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of BookingSlotStandards for query {}", query);
        Page<BookingSlotStandardDTO> page = bookingSlotStandardService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/booking-slot-standards");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
