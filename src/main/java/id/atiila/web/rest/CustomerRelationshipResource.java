package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.CustomerRelationshipService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.CustomerRelationshipDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CustomerRelationship.
 */
@RestController
@RequestMapping("/api")
public class CustomerRelationshipResource {

    private final Logger log = LoggerFactory.getLogger(CustomerRelationshipResource.class);

    private static final String ENTITY_NAME = "customerRelationship";

    private final CustomerRelationshipService customerRelationshipService;

    public CustomerRelationshipResource(CustomerRelationshipService customerRelationshipService) {
        this.customerRelationshipService = customerRelationshipService;
    }

    /**
     * POST  /customer-relationships : Create a new customerRelationship.
     *
     * @param customerRelationshipDTO the customerRelationshipDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new customerRelationshipDTO, or with status 400 (Bad Request) if the customerRelationship has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customer-relationships")
    @Timed
    public ResponseEntity<CustomerRelationshipDTO> createCustomerRelationship(@RequestBody CustomerRelationshipDTO customerRelationshipDTO) throws URISyntaxException {
        log.debug("REST request to save CustomerRelationship : {}", customerRelationshipDTO);
        if (customerRelationshipDTO.getIdPartyRelationship() != null) {
            throw new BadRequestAlertException("A new customerRelationship cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CustomerRelationshipDTO result = customerRelationshipService.save(customerRelationshipDTO);
        return ResponseEntity.created(new URI("/api/customer-relationships/" + result.getIdPartyRelationship()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPartyRelationship().toString()))
            .body(result);
    }

    /**
     * POST  /customer-relationships/process : Execute Bussiness Process customerRelationship.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customer-relationships/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processCustomerRelationship(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process CustomerRelationship ");
        Map<String, Object> result = customerRelationshipService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /customer-relationships/execute : Execute Bussiness Process customerRelationship.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  customerRelationshipDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customer-relationships/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedCustomerRelationship(HttpServletRequest request, @RequestBody CustomerRelationshipDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process CustomerRelationship");
        Map<String, Object> result = customerRelationshipService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /customer-relationships/execute-list : Execute Bussiness Process customerRelationship.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  customerRelationshipDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customer-relationships/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListCustomerRelationship(HttpServletRequest request, @RequestBody Set<CustomerRelationshipDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List CustomerRelationship");
        Map<String, Object> result = customerRelationshipService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /customer-relationships : Updates an existing customerRelationship.
     *
     * @param customerRelationshipDTO the customerRelationshipDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated customerRelationshipDTO,
     * or with status 400 (Bad Request) if the customerRelationshipDTO is not valid,
     * or with status 500 (Internal Server Error) if the customerRelationshipDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/customer-relationships")
    @Timed
    public ResponseEntity<CustomerRelationshipDTO> updateCustomerRelationship(@RequestBody CustomerRelationshipDTO customerRelationshipDTO) throws URISyntaxException {
        log.debug("REST request to update CustomerRelationship : {}", customerRelationshipDTO);
        if (customerRelationshipDTO.getIdPartyRelationship() == null) {
            return createCustomerRelationship(customerRelationshipDTO);
        }
        CustomerRelationshipDTO result = customerRelationshipService.save(customerRelationshipDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, customerRelationshipDTO.getIdPartyRelationship().toString()))
            .body(result);
    }

    /**
     * GET  /customer-relationships : get all the customerRelationships.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of customerRelationships in body
     */
    @GetMapping("/customer-relationships")
    @Timed
    public ResponseEntity<List<CustomerRelationshipDTO>> getAllCustomerRelationships(Pageable pageable) {
        log.debug("REST request to get a page of CustomerRelationships");
        Page<CustomerRelationshipDTO> page = customerRelationshipService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/customer-relationships");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/customer-relationships/filterBy")
    @Timed
    public ResponseEntity<List<CustomerRelationshipDTO>> getAllFilteredCustomerRelationships(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of CustomerRelationships");
        Page<CustomerRelationshipDTO> page = customerRelationshipService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/customer-relationships/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /customer-relationships/:id : get the "id" customerRelationship.
     *
     * @param id the id of the customerRelationshipDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the customerRelationshipDTO, or with status 404 (Not Found)
     */
    @GetMapping("/customer-relationships/{id}")
    @Timed
    public ResponseEntity<CustomerRelationshipDTO> getCustomerRelationship(@PathVariable UUID id) {
        log.debug("REST request to get CustomerRelationship : {}", id);
        CustomerRelationshipDTO customerRelationshipDTO = customerRelationshipService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(customerRelationshipDTO));
    }

    /**
     * DELETE  /customer-relationships/:id : delete the "id" customerRelationship.
     *
     * @param id the id of the customerRelationshipDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/customer-relationships/{id}")
    @Timed
    public ResponseEntity<Void> deleteCustomerRelationship(@PathVariable UUID id) {
        log.debug("REST request to delete CustomerRelationship : {}", id);
        customerRelationshipService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/customer-relationships?query=:query : search for the customerRelationship corresponding
     * to the query.
     *
     * @param query the query of the customerRelationship search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/customer-relationships")
    @Timed
    public ResponseEntity<List<CustomerRelationshipDTO>> searchCustomerRelationships(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of CustomerRelationships for query {}", query);
        Page<CustomerRelationshipDTO> page = customerRelationshipService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/customer-relationships");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
