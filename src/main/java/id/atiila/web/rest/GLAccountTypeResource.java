package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.GLAccountTypeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.GLAccountTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing GLAccountType.
 */
@RestController
@RequestMapping("/api")
public class GLAccountTypeResource {

    private final Logger log = LoggerFactory.getLogger(GLAccountTypeResource.class);

    private static final String ENTITY_NAME = "gLAccountType";

    private final GLAccountTypeService gLAccountTypeService;

    public GLAccountTypeResource(GLAccountTypeService gLAccountTypeService) {
        this.gLAccountTypeService = gLAccountTypeService;
    }

    /**
     * POST  /gl-account-types : Create a new gLAccountType.
     *
     * @param gLAccountTypeDTO the gLAccountTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new gLAccountTypeDTO, or with status 400 (Bad Request) if the gLAccountType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gl-account-types")
    @Timed
    public ResponseEntity<GLAccountTypeDTO> createGLAccountType(@RequestBody GLAccountTypeDTO gLAccountTypeDTO) throws URISyntaxException {
        log.debug("REST request to save GLAccountType : {}", gLAccountTypeDTO);
        if (gLAccountTypeDTO.getIdGLAccountType() != null) {
            throw new BadRequestAlertException("A new gLAccountType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GLAccountTypeDTO result = gLAccountTypeService.save(gLAccountTypeDTO);
        return ResponseEntity.created(new URI("/api/gl-account-types/" + result.getIdGLAccountType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdGLAccountType().toString()))
            .body(result);
    }

    /**
     * POST  /gl-account-types/process : Execute Bussiness Process gLAccountType.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gl-account-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processGLAccountType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process GLAccountType ");
        Map<String, Object> result = gLAccountTypeService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /gl-account-types/execute : Execute Bussiness Process gLAccountType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  gLAccountTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gl-account-types/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedGLAccountType(HttpServletRequest request, @RequestBody GLAccountTypeDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process GLAccountType");
        Map<String, Object> result = gLAccountTypeService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /gl-account-types/execute-list : Execute Bussiness Process gLAccountType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  gLAccountTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gl-account-types/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListGLAccountType(HttpServletRequest request, @RequestBody Set<GLAccountTypeDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List GLAccountType");
        Map<String, Object> result = gLAccountTypeService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /gl-account-types : Updates an existing gLAccountType.
     *
     * @param gLAccountTypeDTO the gLAccountTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated gLAccountTypeDTO,
     * or with status 400 (Bad Request) if the gLAccountTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the gLAccountTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/gl-account-types")
    @Timed
    public ResponseEntity<GLAccountTypeDTO> updateGLAccountType(@RequestBody GLAccountTypeDTO gLAccountTypeDTO) throws URISyntaxException {
        log.debug("REST request to update GLAccountType : {}", gLAccountTypeDTO);
        if (gLAccountTypeDTO.getIdGLAccountType() == null) {
            return createGLAccountType(gLAccountTypeDTO);
        }
        GLAccountTypeDTO result = gLAccountTypeService.save(gLAccountTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, gLAccountTypeDTO.getIdGLAccountType().toString()))
            .body(result);
    }

    /**
     * GET  /gl-account-types : get all the gLAccountTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of gLAccountTypes in body
     */
    @GetMapping("/gl-account-types")
    @Timed
    public ResponseEntity<List<GLAccountTypeDTO>> getAllGLAccountTypes(Pageable pageable) {
        log.debug("REST request to get a page of GLAccountTypes");
        Page<GLAccountTypeDTO> page = gLAccountTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/gl-account-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/gl-account-types/filterBy")
    @Timed
    public ResponseEntity<List<GLAccountTypeDTO>> getAllFilteredGLAccountTypes(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of GLAccountTypes");
        Page<GLAccountTypeDTO> page = gLAccountTypeService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/gl-account-types/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /gl-account-types/:id : get the "id" gLAccountType.
     *
     * @param id the id of the gLAccountTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the gLAccountTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/gl-account-types/{id}")
    @Timed
    public ResponseEntity<GLAccountTypeDTO> getGLAccountType(@PathVariable Integer id) {
        log.debug("REST request to get GLAccountType : {}", id);
        GLAccountTypeDTO gLAccountTypeDTO = gLAccountTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(gLAccountTypeDTO));
    }

    /**
     * DELETE  /gl-account-types/:id : delete the "id" gLAccountType.
     *
     * @param id the id of the gLAccountTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/gl-account-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteGLAccountType(@PathVariable Integer id) {
        log.debug("REST request to delete GLAccountType : {}", id);
        gLAccountTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/gl-account-types?query=:query : search for the gLAccountType corresponding
     * to the query.
     *
     * @param query the query of the gLAccountType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/gl-account-types")
    @Timed
    public ResponseEntity<List<GLAccountTypeDTO>> searchGLAccountTypes(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of GLAccountTypes for query {}", query);
        Page<GLAccountTypeDTO> page = gLAccountTypeService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/gl-account-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
