package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.CalendarTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.CalendarTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CalendarType.
 */
@RestController
@RequestMapping("/api")
public class CalendarTypeResource {

    private final Logger log = LoggerFactory.getLogger(CalendarTypeResource.class);

    private static final String ENTITY_NAME = "calendarType";

    private final CalendarTypeService calendarTypeService;

    public CalendarTypeResource(CalendarTypeService calendarTypeService) {
        this.calendarTypeService = calendarTypeService;
    }

    /**
     * POST  /calendar-types : Create a new calendarType.
     *
     * @param calendarTypeDTO the calendarTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new calendarTypeDTO, or with status 400 (Bad Request) if the calendarType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/calendar-types")
    @Timed
    public ResponseEntity<CalendarTypeDTO> createCalendarType(@RequestBody CalendarTypeDTO calendarTypeDTO) throws URISyntaxException {
        log.debug("REST request to save CalendarType : {}", calendarTypeDTO);
        if (calendarTypeDTO.getIdCalendarType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new calendarType cannot already have an ID")).body(null);
        }
        CalendarTypeDTO result = calendarTypeService.save(calendarTypeDTO);

        return ResponseEntity.created(new URI("/api/calendar-types/" + result.getIdCalendarType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdCalendarType().toString()))
            .body(result);
    }

    /**
     * POST  /calendar-types/execute : Execute Bussiness Process calendarType.
     *
     * @param calendarTypeDTO the calendarTypeDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  calendarTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/calendar-types/execute")
    @Timed
    public ResponseEntity<CalendarTypeDTO> executedCalendarType(@RequestBody CalendarTypeDTO calendarTypeDTO) throws URISyntaxException {
        log.debug("REST request to process CalendarType : {}", calendarTypeDTO);
        return new ResponseEntity<CalendarTypeDTO>(calendarTypeDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /calendar-types : Updates an existing calendarType.
     *
     * @param calendarTypeDTO the calendarTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated calendarTypeDTO,
     * or with status 400 (Bad Request) if the calendarTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the calendarTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/calendar-types")
    @Timed
    public ResponseEntity<CalendarTypeDTO> updateCalendarType(@RequestBody CalendarTypeDTO calendarTypeDTO) throws URISyntaxException {
        log.debug("REST request to update CalendarType : {}", calendarTypeDTO);
        if (calendarTypeDTO.getIdCalendarType() == null) {
            return createCalendarType(calendarTypeDTO);
        }
        CalendarTypeDTO result = calendarTypeService.save(calendarTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, calendarTypeDTO.getIdCalendarType().toString()))
            .body(result);
    }

    /**
     * GET  /calendar-types : get all the calendarTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of calendarTypes in body
     */
    @GetMapping("/calendar-types")
    @Timed
    public ResponseEntity<List<CalendarTypeDTO>> getAllCalendarTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of CalendarTypes");
        Page<CalendarTypeDTO> page = calendarTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/calendar-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /calendar-types/:id : get the "id" calendarType.
     *
     * @param id the id of the calendarTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the calendarTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/calendar-types/{id}")
    @Timed
    public ResponseEntity<CalendarTypeDTO> getCalendarType(@PathVariable Integer id) {
        log.debug("REST request to get CalendarType : {}", id);
        CalendarTypeDTO calendarTypeDTO = calendarTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(calendarTypeDTO));
    }

    /**
     * DELETE  /calendar-types/:id : delete the "id" calendarType.
     *
     * @param id the id of the calendarTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/calendar-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteCalendarType(@PathVariable Integer id) {
        log.debug("REST request to delete CalendarType : {}", id);
        calendarTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/calendar-types?query=:query : search for the calendarType corresponding
     * to the query.
     *
     * @param query the query of the calendarType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/calendar-types")
    @Timed
    public ResponseEntity<List<CalendarTypeDTO>> searchCalendarTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of CalendarTypes for query {}", query);
        Page<CalendarTypeDTO> page = calendarTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/calendar-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/calendar-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processCalendarType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process CalendarType ");
        Map<String, Object> result = calendarTypeService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
