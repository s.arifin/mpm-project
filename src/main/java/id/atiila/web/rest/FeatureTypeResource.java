package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.FeatureTypeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.FeatureTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing FeatureType.
 */
@RestController
@RequestMapping("/api")
public class FeatureTypeResource {

    private final Logger log = LoggerFactory.getLogger(FeatureTypeResource.class);

    private static final String ENTITY_NAME = "featureType";

    private final FeatureTypeService featureTypeService;

    public FeatureTypeResource(FeatureTypeService featureTypeService) {
        this.featureTypeService = featureTypeService;
    }

    /**
     * POST  /feature-types : Create a new featureType.
     *
     * @param featureTypeDTO the featureTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new featureTypeDTO, or with status 400 (Bad Request) if the featureType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/feature-types")
    @Timed
    public ResponseEntity<FeatureTypeDTO> createFeatureType(@RequestBody FeatureTypeDTO featureTypeDTO) throws URISyntaxException {
        log.debug("REST request to save FeatureType : {}", featureTypeDTO);
        if (featureTypeDTO.getIdFeatureType() != null) {
            throw new BadRequestAlertException("A new featureType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FeatureTypeDTO result = featureTypeService.save(featureTypeDTO);
        return ResponseEntity.created(new URI("/api/feature-types/" + result.getIdFeatureType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdFeatureType().toString()))
            .body(result);
    }

    /**
     * POST  /feature-types/process : Execute Bussiness Process featureType.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/feature-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processFeatureType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process FeatureType ");
        Map<String, Object> result = featureTypeService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /feature-types/execute : Execute Bussiness Process featureType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  featureTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/feature-types/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedFeatureType(HttpServletRequest request, @RequestBody FeatureTypeDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process FeatureType");
        Map<String, Object> result = featureTypeService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /feature-types/execute-list : Execute Bussiness Process featureType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  featureTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/feature-types/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListFeatureType(HttpServletRequest request, @RequestBody Set<FeatureTypeDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List FeatureType");
        Map<String, Object> result = featureTypeService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /feature-types : Updates an existing featureType.
     *
     * @param featureTypeDTO the featureTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated featureTypeDTO,
     * or with status 400 (Bad Request) if the featureTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the featureTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/feature-types")
    @Timed
    public ResponseEntity<FeatureTypeDTO> updateFeatureType(@RequestBody FeatureTypeDTO featureTypeDTO) throws URISyntaxException {
        log.debug("REST request to update FeatureType : {}", featureTypeDTO);
        if (featureTypeDTO.getIdFeatureType() == null) {
            return createFeatureType(featureTypeDTO);
        }
        FeatureTypeDTO result = featureTypeService.save(featureTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, featureTypeDTO.getIdFeatureType().toString()))
            .body(result);
    }

    /**
     * GET  /feature-types : get all the featureTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of featureTypes in body
     */
    @GetMapping("/feature-types")
    @Timed
    public ResponseEntity<List<FeatureTypeDTO>> getAllFeatureTypes(Pageable pageable) {
        log.debug("REST request to get a page of FeatureTypes");
        Page<FeatureTypeDTO> page = featureTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/feature-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/feature-types/filterBy")
    @Timed
    public ResponseEntity<List<FeatureTypeDTO>> getAllFilteredFeatureTypes(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of FeatureTypes");
        Page<FeatureTypeDTO> page = featureTypeService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/feature-types/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /feature-types/:id : get the "id" featureType.
     *
     * @param id the id of the featureTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the featureTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/feature-types/{id}")
    @Timed
    public ResponseEntity<FeatureTypeDTO> getFeatureType(@PathVariable Integer id) {
        log.debug("REST request to get FeatureType : {}", id);
        FeatureTypeDTO featureTypeDTO = featureTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(featureTypeDTO));
    }

    /**
     * DELETE  /feature-types/:id : delete the "id" featureType.
     *
     * @param id the id of the featureTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/feature-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteFeatureType(@PathVariable Integer id) {
        log.debug("REST request to delete FeatureType : {}", id);
        featureTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/feature-types?query=:query : search for the featureType corresponding
     * to the query.
     *
     * @param query the query of the featureType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/feature-types")
    @Timed
    public ResponseEntity<List<FeatureTypeDTO>> searchFeatureTypes(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of FeatureTypes for query {}", query);
        Page<FeatureTypeDTO> page = featureTypeService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/feature-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
