package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ShipmentIncomingService;
import id.atiila.service.dto.CustomShipmentIncomingDTO;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ShipmentIncomingDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ShipmentIncoming.
 */
@RestController
@RequestMapping("/api")
public class ShipmentIncomingResource {

    private final Logger log = LoggerFactory.getLogger(ShipmentIncomingResource.class);

    private static final String ENTITY_NAME = "shipmentIncoming";

    private final ShipmentIncomingService shipmentIncomingService;

    public ShipmentIncomingResource(ShipmentIncomingService shipmentIncomingService) {
        this.shipmentIncomingService = shipmentIncomingService;
    }

    /**
     * POST  /shipment-incomings : Create a new shipmentIncoming.
     *
     * @param shipmentIncomingDTO the shipmentIncomingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shipmentIncomingDTO, or with status 400 (Bad Request) if the shipmentIncoming has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-incomings")
    @Timed
    public ResponseEntity<ShipmentIncomingDTO> createShipmentIncoming(@RequestBody ShipmentIncomingDTO shipmentIncomingDTO) throws URISyntaxException {
        log.debug("REST request to save ShipmentIncoming : {}", shipmentIncomingDTO);
        if (shipmentIncomingDTO.getIdShipment() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new shipmentIncoming cannot already have an ID")).body(null);
        }
        ShipmentIncomingDTO result = shipmentIncomingService.save(shipmentIncomingDTO);
        return ResponseEntity.created(new URI("/api/shipment-incomings/" + result.getIdShipment()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdShipment().toString()))
            .body(result);
    }

    /**
     * POST  /shipment-incomings/execute{id}/{param} : Execute Bussiness Process shipmentIncoming.
     *
     * @param shipmentIncomingDTO the shipmentIncomingDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  shipmentIncomingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-incomings/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ShipmentIncomingDTO> executedShipmentIncoming(@PathVariable Integer id, @PathVariable String param, @RequestBody ShipmentIncomingDTO shipmentIncomingDTO) throws URISyntaxException {
        log.debug("REST request to process ShipmentIncoming : {}", shipmentIncomingDTO);
        ShipmentIncomingDTO result = shipmentIncomingService.processExecuteData(id, param, shipmentIncomingDTO);
        return new ResponseEntity<ShipmentIncomingDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /shipment-incomings/execute-list{id}/{param} : Execute Bussiness Process shipmentIncoming.
     *
     * @param shipmentIncomingDTO the shipmentIncomingDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  shipmentIncomingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-incomings/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ShipmentIncomingDTO>> executedListShipmentIncoming(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ShipmentIncomingDTO> shipmentIncomingDTO) throws URISyntaxException {
        log.debug("REST request to process List ShipmentIncoming");
        Set<ShipmentIncomingDTO> result = shipmentIncomingService.processExecuteListData(id, param, shipmentIncomingDTO);
        return new ResponseEntity<Set<ShipmentIncomingDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /shipment-incomings : Updates an existing shipmentIncoming.
     *
     * @param shipmentIncomingDTO the shipmentIncomingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shipmentIncomingDTO,
     * or with status 400 (Bad Request) if the shipmentIncomingDTO is not valid,
     * or with status 500 (Internal Server Error) if the shipmentIncomingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/shipment-incomings")
    @Timed
    public ResponseEntity<ShipmentIncomingDTO> updateShipmentIncoming(@RequestBody ShipmentIncomingDTO shipmentIncomingDTO) throws URISyntaxException {
        log.debug("REST request to update ShipmentIncoming : {}", shipmentIncomingDTO);
        if (shipmentIncomingDTO.getIdShipment() == null) {
            return createShipmentIncoming(shipmentIncomingDTO);
        }
        ShipmentIncomingDTO result = shipmentIncomingService.save(shipmentIncomingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shipmentIncomingDTO.getIdShipment().toString()))
            .body(result);
    }

    /**
     * GET  /shipment-incomings : get all the shipmentIncomings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shipmentIncomings in body
     */
    @GetMapping("/shipment-incomings")
    @Timed
    public ResponseEntity<List<ShipmentIncomingDTO>> getAllShipmentIncomings(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ShipmentIncomings");
        Page<ShipmentIncomingDTO> page = shipmentIncomingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-incomings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/shipment-incomings/filterBy")
    @Timed
    public ResponseEntity<List<ShipmentIncomingDTO>> getAllFilteredShipmentIncomings(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of PackageReceipts");
        Page<ShipmentIncomingDTO> page = shipmentIncomingService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-incomings/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * GET  /shipment-incomings/:id : get the "id" shipmentIncoming.
     *
     * @param id the id of the shipmentIncomingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shipmentIncomingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/shipment-incomings/{id}")
    @Timed
    public ResponseEntity<ShipmentIncomingDTO> getShipmentIncoming(@PathVariable UUID id) {
        log.debug("REST request to get ShipmentIncoming : {}", id);
        ShipmentIncomingDTO shipmentIncomingDTO = shipmentIncomingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(shipmentIncomingDTO));
    }

    @GetMapping("/shipment-incomings-spg/{id}")
    @Timed
    public ResponseEntity<CustomShipmentIncomingDTO> getShipmentIncomingSpg(@PathVariable UUID id) {
        log.debug("REST request to get ShipmentIncoming SPG: {}", id);
        CustomShipmentIncomingDTO customShipmentIncomingDTO = shipmentIncomingService.findOneCustomBy(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(customShipmentIncomingDTO));
    }

    /**
     * DELETE  /shipment-incomings/:id : delete the "id" shipmentIncoming.
     *
     * @param id the id of the shipmentIncomingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shipment-incomings/{id}")
    @Timed
    public ResponseEntity<Void> deleteShipmentIncoming(@PathVariable UUID id) {
        log.debug("REST request to delete ShipmentIncoming : {}", id);
        shipmentIncomingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/shipment-incomings?query=:query : search for the shipmentIncoming corresponding
     * to the query.
     *
     * @param query the query of the shipmentIncoming search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/shipment-incomings")
    @Timed
    public ResponseEntity<List<ShipmentIncomingDTO>> searchShipmentIncomings(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ShipmentIncomings for query {}", query);
        Page<ShipmentIncomingDTO> page = shipmentIncomingService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/shipment-incomings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/shipment-incomings/set-status/{id}")
    @Timed
    public ResponseEntity< ShipmentIncomingDTO> setStatusShipmentIncoming(@PathVariable Integer id, @RequestBody ShipmentIncomingDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status ShipmentIncoming : {}", dto);
        ShipmentIncomingDTO r = shipmentIncomingService.changeShipmentIncomingStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    @PostMapping("/shipment-incomings/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processShipmentIncoming(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process ShipmentIncoming ");
        Map<String, Object> result = shipmentIncomingService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
