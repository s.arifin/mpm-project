package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.repository.ProspectPersonRepository;
import id.atiila.service.ProspectPersonService;
import id.atiila.service.dto.*;
import id.atiila.service.pto.ProspectPTO;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ProspectPerson.
 */
@RestController
@RequestMapping("/api")
public class ProspectPersonResource {

    private final Logger log = LoggerFactory.getLogger(ProspectPersonResource.class);

    private static final String ENTITY_NAME = "prospectPerson";

    private final ProspectPersonRepository prospectPersonRepository;

    private final ProspectPersonService prospectPersonService;

    public ProspectPersonResource(ProspectPersonRepository prospectPersonRepository, ProspectPersonService prospectPersonService) {
        this.prospectPersonRepository = prospectPersonRepository;
        this.prospectPersonService = prospectPersonService;
    }

    /**
     * POST  /prospect-people : Create a new prospectPerson.
     *
     * @param prospectPersonDTO the prospectPersonDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new prospectPersonDTO, or with status 400 (Bad Request) if the prospectPerson has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prospect-people")
    @Timed
    public ResponseEntity<ProspectPersonDTO> createProspectPerson(@RequestBody ProspectPersonDTO prospectPersonDTO) throws URISyntaxException {
        log.debug("REST request to save ProspectPerson : {}", prospectPersonDTO);
        if (prospectPersonDTO.getIdProspect() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new prospectPerson cannot already have an ID")).body(null);
        }
        ProspectPersonDTO result = prospectPersonService.save(prospectPersonDTO);
        return ResponseEntity.created(new URI("/api/prospect-people/" + result.getIdProspect()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdProspect().toString()))
            .body(result);
    }

    /**
     * POST  /prospect-people/execute{id}/{param} : Execute Bussiness Process prospectPerson.
     *
     * @param prospectPersonDTO the prospectPersonDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  prospectPersonDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prospect-people/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ProspectPersonDTO> executedProspectPerson(@PathVariable Integer id, @PathVariable String param, @RequestBody ProspectPersonDTO prospectPersonDTO) throws URISyntaxException {
        log.debug("REST request to process ProspectPerson : {}", prospectPersonDTO);
        ProspectPersonDTO result = prospectPersonService.processExecuteData(id, param, prospectPersonDTO);
        return new ResponseEntity<ProspectPersonDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /prospect-people/execute-list{id}/{param} : Execute Bussiness Process prospectPerson.
     *
     * @param prospectPersonDTO the prospectPersonDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  prospectPersonDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prospect-people/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ProspectPersonDTO>> executedListProspectPerson(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ProspectPersonDTO> prospectPersonDTO) throws URISyntaxException {
        log.debug("REST request to process List ProspectPerson");
        Set<ProspectPersonDTO> result = prospectPersonService.processExecuteListData(id, param, prospectPersonDTO);
        return new ResponseEntity<Set<ProspectPersonDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /prospect-people : Updates an existing prospectPerson.
     *
     * @param prospectPersonDTO the prospectPersonDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated prospectPersonDTO,
     * or with status 400 (Bad Request) if the prospectPersonDTO is not valid,
     * or with status 500 (Internal Server Error) if the prospectPersonDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/prospect-people/followUp")
    @Timed
    public ResponseEntity<ProspectPersonDTO> updateProspectPersonFollowUp(@RequestBody ProspectPersonDTO prospectPersonDTO) throws URISyntaxException {
        log.debug("REST request to update ProspectPerson", prospectPersonDTO.getProspectCount());
        if (prospectPersonDTO.getIdProspect() == null) {
            return createProspectPerson(prospectPersonDTO);
        }
        ProspectPersonDTO result = prospectPersonService.saveFollowUp(prospectPersonDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, prospectPersonDTO.getIdProspect().toString()))
            .body(result);
    }

    @PutMapping("/prospect-people")
    @Timed
    public ResponseEntity<ProspectPersonDTO> updateProspectPerson(@RequestBody ProspectPersonDTO prospectPersonDTO) throws URISyntaxException {
        log.debug("REST request to update ProspectPerson", prospectPersonDTO.getProspectCount());
        if (prospectPersonDTO.getIdProspect() == null) {
            return createProspectPerson(prospectPersonDTO);
        }
        ProspectPersonDTO result = prospectPersonService.save(prospectPersonDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, prospectPersonDTO.getIdProspect().toString()))
            .body(result);
    }

    /**
     * GET  /prospect-people : get all the prospectPeople.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of prospectPeople in body
     */
    @GetMapping("/prospect-people")
    @Timed
    public ResponseEntity<List<ProspectPersonDTO>> getAllProspectPeople(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ProspectPeople");
        Page<ProspectPersonDTO> page = prospectPersonService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/prospect-people");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/prospect-people/filterBy")
    @Timed
    public ResponseEntity<List<ProspectPersonDTO>> getAllFilteredProspectPeople(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Filtered ProspectPeople");
        Page<ProspectPersonDTO> page = prospectPersonService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/prospect-people/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /prospect-people/:id : get the "id" prospectPerson.
     *
     * @param id the id of the prospectPersonDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the prospectPersonDTO, or with status 404 (Not Found)
     */
    @GetMapping("/prospect-people/{id}")
    @Timed
    public ResponseEntity<ProspectPersonDTO> getProspectPerson(@PathVariable UUID id) {
        log.debug("REST request to get ProspectPerson : {}", id);
        ProspectPersonDTO prospectPersonDTO = prospectPersonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(prospectPersonDTO));
    }

    /**
     * DELETE  /prospect-people/:id : delete the "id" prospectPerson.
     *
     * @param id the id of the prospectPersonDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/prospect-people/{id}")
    @Timed
    public ResponseEntity<Void> deleteProspectPerson(@PathVariable UUID id) {
        log.debug("REST request to delete ProspectPerson : {}", id);
        prospectPersonService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/prospect-people?query=:query : search for the prospectPerson corresponding
     * to the query.
     *
     * @param query the query of the prospectPerson search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/prospect-people")
    @Timed
    public ResponseEntity<List<ProspectPersonDTO>> searchProspectPeople(HttpServletRequest request, @RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ProspectPeople for query {}", query);
        Page<ProspectPersonDTO> page = prospectPersonService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/prospect-people");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

//    @GetMapping("/prospect-people/tosearch")
//    @Timed
//    public ResponseEntity<List<ProspectPersonDTO>> getToPropspectPeopleSearch(HttpServletRequest request, Pageable pageable) {
//        log.debug("REST request to get a page of vehicle-sales-orders");
//        Page<ProspectPersonDTO> page = prospectPersonService.toSearch(request, pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/prospect-people/tosearch");
//        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
//    }


    @PostMapping("/prospect-people/set-status/{id}")
    @Timed
    public ResponseEntity< ProspectPersonDTO> setStatusProspectPerson(@PathVariable Integer id, @RequestBody ProspectPersonDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status ProspectPerson : {}", dto);
        ProspectPersonDTO r = prospectPersonService.changeProspectPersonStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }


     /* =============================== K O R S A L - R E S O U R C E =============================== */


    /**
     * GET  /prospect-people : get all the prospectPeople.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of prospectPeople in body
     */
    @GetMapping("/prospect-people/by-korsal")
    @Timed
    public ResponseEntity<List<ProspectPersonDTO>> getAllProspectPeopleByKorsal(@RequestParam(required = false) String idstatustype,
                                                                                @RequestParam(required = false) String idInternal,
                                                                                @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ProspectPeople");
        Integer statusTypeId = null;
        if (idstatustype != null) statusTypeId = Integer.parseInt(idstatustype);
        Page<ProspectPersonDTO> page = prospectPersonService.findAllByKorsal(pageable, statusTypeId, idInternal);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/prospect-people/by-korsal");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/prospect-people/get-summary")
    @Timed
    public ResponseEntity<List<CustomProspectDTO>> getSummary(@RequestParam(required = false) String idInternal,
                                                              @ApiParam Pageable pageable) {
        Page<CustomProspectDTO> r = prospectPersonService.findSummarizeProspect(pageable, idInternal);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(r, "/api/prospect-people/get-summary");
        return new ResponseEntity<>(r.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/prospect-people/korsal-filter")
    @Timed
    public ResponseEntity<List<ProspectPersonDTO>> getProspectPeoplebyFilterKorsal(@ApiParam Pageable pageable,
                                                                                   @ApiParam ProspectPTO prospectPTO,
                                                                                   @RequestParam(required = false) String idInternal) {
        Page<ProspectPersonDTO> r = prospectPersonService.findAllByFilterKorsal(pageable, prospectPTO, idInternal);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(r, "/api/prospect-people/korsal-filter");
        return new ResponseEntity<>(r.getContent(), headers, HttpStatus.OK);
    }

    /* =============================== T L - R E S O U R C E =============================== */
    @GetMapping("/prospect-people/by-teamleader")
    @Timed
    public ResponseEntity<List<ProspectPersonDTO>> getAllProspectPeopleByTeamLeader(@RequestParam(required = false) String idstatustype,
                                                                                    @RequestParam(required = false) String idInternal,
                                                                                    @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ProspectPeople");
        Integer statusTypeId = null;
        if (idstatustype != null) statusTypeId = Integer.parseInt(idstatustype);
        Page<ProspectPersonDTO> page = prospectPersonService.findAllByTeamLeader(pageable, statusTypeId, idInternal);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/prospect-people/by-teamleader");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

//    @GetMapping("/prospect-people/filterByNIK")
//    @Timed
//    public ResponseEntity<List<ProspectPersonDTO>> getAllbynik(HttpServletRequest request, PersonDTO dto, String nik, Pageable pageable) {
//        log.debug("REST request to get a page of Filtered ProspectPeople");
//        Page<ProspectPersonDTO> page = (Page<ProspectPersonDTO>) prospectPersonService.pro(dto, nik);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/prospect-people/filterByNIK");
//        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
//    }

}




