package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PositionReportingStructureService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PositionReportingStructureDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PositionReportingStructure.
 */
@RestController
@RequestMapping("/api")
public class PositionReportingStructureResource {

    private final Logger log = LoggerFactory.getLogger(PositionReportingStructureResource.class);

    private static final String ENTITY_NAME = "positionReportingStructure";

    private final PositionReportingStructureService positionReportingStructureService;

    public PositionReportingStructureResource(PositionReportingStructureService positionReportingStructureService) {
        this.positionReportingStructureService = positionReportingStructureService;
    }

    /**
     * POST  /position-reporting-structures : Create a new positionReportingStructure.
     *
     * @param positionReportingStructureDTO the positionReportingStructureDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new positionReportingStructureDTO, or with status 400 (Bad Request) if the positionReportingStructure has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/position-reporting-structures")
    @Timed
    public ResponseEntity<PositionReportingStructureDTO> createPositionReportingStructure(@RequestBody PositionReportingStructureDTO positionReportingStructureDTO) throws URISyntaxException {
        log.debug("REST request to save PositionReportingStructure : {}", positionReportingStructureDTO);
        if (positionReportingStructureDTO.getIdPositionStructure() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new positionReportingStructure cannot already have an ID")).body(null);
        }
        PositionReportingStructureDTO result = positionReportingStructureService.save(positionReportingStructureDTO);
        return ResponseEntity.created(new URI("/api/position-reporting-structures/" + result.getIdPositionStructure()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPositionStructure().toString()))
            .body(result);
    }

    /**
     * POST  /position-reporting-structures/execute{id}/{param} : Execute Bussiness Process positionReportingStructure.
     *
     * @param positionReportingStructureDTO the positionReportingStructureDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  positionReportingStructureDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/position-reporting-structures/execute/{id}/{param}")
    @Timed
    public ResponseEntity<PositionReportingStructureDTO> executedPositionReportingStructure(@PathVariable Integer id, @PathVariable String param, @RequestBody PositionReportingStructureDTO positionReportingStructureDTO) throws URISyntaxException {
        log.debug("REST request to process PositionReportingStructure : {}", positionReportingStructureDTO);
        PositionReportingStructureDTO result = positionReportingStructureService.processExecuteData(id, param, positionReportingStructureDTO);
        return new ResponseEntity<PositionReportingStructureDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /position-reporting-structures/execute-list{id}/{param} : Execute Bussiness Process positionReportingStructure.
     *
     * @param positionReportingStructureDTO the positionReportingStructureDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  positionReportingStructureDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/position-reporting-structures/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<PositionReportingStructureDTO>> executedListPositionReportingStructure(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<PositionReportingStructureDTO> positionReportingStructureDTO) throws URISyntaxException {
        log.debug("REST request to process List PositionReportingStructure");
        Set<PositionReportingStructureDTO> result = positionReportingStructureService.processExecuteListData(id, param, positionReportingStructureDTO);
        return new ResponseEntity<Set<PositionReportingStructureDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /position-reporting-structures : Updates an existing positionReportingStructure.
     *
     * @param positionReportingStructureDTO the positionReportingStructureDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated positionReportingStructureDTO,
     * or with status 400 (Bad Request) if the positionReportingStructureDTO is not valid,
     * or with status 500 (Internal Server Error) if the positionReportingStructureDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/position-reporting-structures")
    @Timed
    public ResponseEntity<PositionReportingStructureDTO> updatePositionReportingStructure(@RequestBody PositionReportingStructureDTO positionReportingStructureDTO) throws URISyntaxException {
        log.debug("REST request to update PositionReportingStructure : {}", positionReportingStructureDTO);
        if (positionReportingStructureDTO.getIdPositionStructure() == null) {
            return createPositionReportingStructure(positionReportingStructureDTO);
        }
        PositionReportingStructureDTO result = positionReportingStructureService.save(positionReportingStructureDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, positionReportingStructureDTO.getIdPositionStructure().toString()))
            .body(result);
    }

    /**
     * GET  /position-reporting-structures : get all the positionReportingStructures.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of positionReportingStructures in body
     */
    @GetMapping("/position-reporting-structures")
    @Timed
    public ResponseEntity<List<PositionReportingStructureDTO>> getAllPositionReportingStructures(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PositionReportingStructures");
        Page<PositionReportingStructureDTO> page = positionReportingStructureService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/position-reporting-structures");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /position-reporting-structures/:id : get the "id" positionReportingStructure.
     *
     * @param id the id of the positionReportingStructureDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the positionReportingStructureDTO, or with status 404 (Not Found)
     */
    @GetMapping("/position-reporting-structures/{id}")
    @Timed
    public ResponseEntity<PositionReportingStructureDTO> getPositionReportingStructure(@PathVariable Integer id) {
        log.debug("REST request to get PositionReportingStructure : {}", id);
        PositionReportingStructureDTO positionReportingStructureDTO = positionReportingStructureService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(positionReportingStructureDTO));
    }

    /**
     * DELETE  /position-reporting-structures/:id : delete the "id" positionReportingStructure.
     *
     * @param id the id of the positionReportingStructureDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/position-reporting-structures/{id}")
    @Timed
    public ResponseEntity<Void> deletePositionReportingStructure(@PathVariable Integer id) {
        log.debug("REST request to delete PositionReportingStructure : {}", id);
        positionReportingStructureService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/position-reporting-structures?query=:query : search for the positionReportingStructure corresponding
     * to the query.
     *
     * @param query the query of the positionReportingStructure search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/position-reporting-structures")
    @Timed
    public ResponseEntity<List<PositionReportingStructureDTO>> searchPositionReportingStructures(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of PositionReportingStructures for query {}", query);
        Page<PositionReportingStructureDTO> page = positionReportingStructureService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/position-reporting-structures");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/position-reporting-structures/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrderItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process OrderItem ");
        Map<String, Object> result = positionReportingStructureService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
