package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PartyCategoryTypeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PartyCategoryTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PartyCategoryType.
 */
@RestController
@RequestMapping("/api")
public class PartyCategoryTypeResource {

    private final Logger log = LoggerFactory.getLogger(PartyCategoryTypeResource.class);

    private static final String ENTITY_NAME = "partyCategoryType";

    private final PartyCategoryTypeService partyCategoryTypeService;

    public PartyCategoryTypeResource(PartyCategoryTypeService partyCategoryTypeService) {
        this.partyCategoryTypeService = partyCategoryTypeService;
    }

    /**
     * POST  /party-category-types : Create a new partyCategoryType.
     *
     * @param partyCategoryTypeDTO the partyCategoryTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new partyCategoryTypeDTO, or with status 400 (Bad Request) if the partyCategoryType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-category-types")
    @Timed
    public ResponseEntity<PartyCategoryTypeDTO> createPartyCategoryType(@RequestBody PartyCategoryTypeDTO partyCategoryTypeDTO) throws URISyntaxException {
        log.debug("REST request to save PartyCategoryType : {}", partyCategoryTypeDTO);
        if (partyCategoryTypeDTO.getIdCategoryType() != null) {
            throw new BadRequestAlertException("A new partyCategoryType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PartyCategoryTypeDTO result = partyCategoryTypeService.save(partyCategoryTypeDTO);
        return ResponseEntity.created(new URI("/api/party-category-types/" + result.getIdCategoryType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdCategoryType().toString()))
            .body(result);
    }

    /**
     * POST  /party-category-types/process : Execute Bussiness Process partyCategoryType.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-category-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processPartyCategoryType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PartyCategoryType ");
        Map<String, Object> result = partyCategoryTypeService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /party-category-types/execute : Execute Bussiness Process partyCategoryType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  partyCategoryTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-category-types/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedPartyCategoryType(HttpServletRequest request, @RequestBody PartyCategoryTypeDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PartyCategoryType");
        Map<String, Object> result = partyCategoryTypeService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /party-category-types/execute-list : Execute Bussiness Process partyCategoryType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  partyCategoryTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-category-types/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListPartyCategoryType(HttpServletRequest request, @RequestBody Set<PartyCategoryTypeDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List PartyCategoryType");
        Map<String, Object> result = partyCategoryTypeService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /party-category-types : Updates an existing partyCategoryType.
     *
     * @param partyCategoryTypeDTO the partyCategoryTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated partyCategoryTypeDTO,
     * or with status 400 (Bad Request) if the partyCategoryTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the partyCategoryTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/party-category-types")
    @Timed
    public ResponseEntity<PartyCategoryTypeDTO> updatePartyCategoryType(@RequestBody PartyCategoryTypeDTO partyCategoryTypeDTO) throws URISyntaxException {
        log.debug("REST request to update PartyCategoryType : {}", partyCategoryTypeDTO);
        if (partyCategoryTypeDTO.getIdCategoryType() == null) {
            return createPartyCategoryType(partyCategoryTypeDTO);
        }
        PartyCategoryTypeDTO result = partyCategoryTypeService.save(partyCategoryTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, partyCategoryTypeDTO.getIdCategoryType().toString()))
            .body(result);
    }

    /**
     * GET  /party-category-types : get all the partyCategoryTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of partyCategoryTypes in body
     */
    @GetMapping("/party-category-types")
    @Timed
    public ResponseEntity<List<PartyCategoryTypeDTO>> getAllPartyCategoryTypes(Pageable pageable) {
        log.debug("REST request to get a page of PartyCategoryTypes");
        Page<PartyCategoryTypeDTO> page = partyCategoryTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/party-category-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/party-category-types/filterBy")
    @Timed
    public ResponseEntity<List<PartyCategoryTypeDTO>> getAllFilteredPartyCategoryTypes(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of PartyCategoryTypes");
        Page<PartyCategoryTypeDTO> page = partyCategoryTypeService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/party-category-types/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /party-category-types/:id : get the "id" partyCategoryType.
     *
     * @param id the id of the partyCategoryTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the partyCategoryTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/party-category-types/{id}")
    @Timed
    public ResponseEntity<PartyCategoryTypeDTO> getPartyCategoryType(@PathVariable Integer id) {
        log.debug("REST request to get PartyCategoryType : {}", id);
        PartyCategoryTypeDTO partyCategoryTypeDTO = partyCategoryTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(partyCategoryTypeDTO));
    }

    /**
     * DELETE  /party-category-types/:id : delete the "id" partyCategoryType.
     *
     * @param id the id of the partyCategoryTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/party-category-types/{id}")
    @Timed
    public ResponseEntity<Void> deletePartyCategoryType(@PathVariable Integer id) {
        log.debug("REST request to delete PartyCategoryType : {}", id);
        partyCategoryTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/party-category-types?query=:query : search for the partyCategoryType corresponding
     * to the query.
     *
     * @param query the query of the partyCategoryType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/party-category-types")
    @Timed
    public ResponseEntity<List<PartyCategoryTypeDTO>> searchPartyCategoryTypes(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of PartyCategoryTypes for query {}", query);
        Page<PartyCategoryTypeDTO> page = partyCategoryTypeService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/party-category-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
