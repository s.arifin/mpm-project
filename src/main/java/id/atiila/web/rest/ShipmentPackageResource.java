package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ShipmentPackageService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ShipmentPackageDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ShipmentPackage.
 */
@RestController
@RequestMapping("/api")
public class ShipmentPackageResource {

    private final Logger log = LoggerFactory.getLogger(ShipmentPackageResource.class);

    private static final String ENTITY_NAME = "shipmentPackage";

    private final ShipmentPackageService shipmentPackageService;

    public ShipmentPackageResource(ShipmentPackageService shipmentPackageService) {
        this.shipmentPackageService = shipmentPackageService;
    }

    /**
     * POST  /shipment-packages : Create a new shipmentPackage.
     *
     * @param shipmentPackageDTO the shipmentPackageDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shipmentPackageDTO, or with status 400 (Bad Request) if the shipmentPackage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-packages")
    @Timed
    public ResponseEntity<ShipmentPackageDTO> createShipmentPackage(@RequestBody ShipmentPackageDTO shipmentPackageDTO) throws URISyntaxException {
        log.debug("REST request to save ShipmentPackage : {}", shipmentPackageDTO);
        if (shipmentPackageDTO.getIdPackage() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new shipmentPackage cannot already have an ID")).body(null);
        }
        ShipmentPackageDTO result = shipmentPackageService.save(shipmentPackageDTO);
        return ResponseEntity.created(new URI("/api/shipment-packages/" + result.getIdPackage()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPackage().toString()))
            .body(result);
    }

    /**
     * POST  /shipment-packages/execute{id}/{param} : Execute Bussiness Process shipmentPackage.
     *
     * @param shipmentPackageDTO the shipmentPackageDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  shipmentPackageDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-packages/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ShipmentPackageDTO> executedShipmentPackage(@PathVariable Integer id, @PathVariable String param, @RequestBody ShipmentPackageDTO shipmentPackageDTO) throws URISyntaxException {
        log.debug("REST request to process ShipmentPackage : {}", shipmentPackageDTO);
        ShipmentPackageDTO result = shipmentPackageService.processExecuteData(id, param, shipmentPackageDTO);
        return new ResponseEntity<ShipmentPackageDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /shipment-packages/execute-list{id}/{param} : Execute Bussiness Process shipmentPackage.
     *
     * @param shipmentPackageDTO the shipmentPackageDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  shipmentPackageDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-packages/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ShipmentPackageDTO>> executedListShipmentPackage(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ShipmentPackageDTO> shipmentPackageDTO) throws URISyntaxException {
        log.debug("REST request to process List ShipmentPackage");
        Set<ShipmentPackageDTO> result = shipmentPackageService.processExecuteListData(id, param, shipmentPackageDTO);
        return new ResponseEntity<Set<ShipmentPackageDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /shipment-packages : Updates an existing shipmentPackage.
     *
     * @param shipmentPackageDTO the shipmentPackageDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shipmentPackageDTO,
     * or with status 400 (Bad Request) if the shipmentPackageDTO is not valid,
     * or with status 500 (Internal Server Error) if the shipmentPackageDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/shipment-packages")
    @Timed
    public ResponseEntity<ShipmentPackageDTO> updateShipmentPackage(@RequestBody ShipmentPackageDTO shipmentPackageDTO) throws URISyntaxException {
        log.debug("REST request to update ShipmentPackage : {}", shipmentPackageDTO);
        if (shipmentPackageDTO.getIdPackage() == null) {
            return createShipmentPackage(shipmentPackageDTO);
        }
        ShipmentPackageDTO result = shipmentPackageService.save(shipmentPackageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shipmentPackageDTO.getIdPackage().toString()))
            .body(result);
    }

    /**
     * GET  /shipment-packages : get all the shipmentPackages.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shipmentPackages in body
     */
    @GetMapping("/shipment-packages")
    @Timed
    public ResponseEntity<List<ShipmentPackageDTO>> getAllShipmentPackages(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ShipmentPackages");
        Page<ShipmentPackageDTO> page = shipmentPackageService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-packages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @GetMapping("/shipment-packages/filterBy")
    @Timed
    public ResponseEntity<List<ShipmentPackageDTO>> getAllFilteredShipmentPackages(@RequestParam String filterBy, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ShipmentPackages");
        Page<ShipmentPackageDTO> page = shipmentPackageService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-packages/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * GET  /shipment-packages/:id : get the "id" shipmentPackage.
     *
     * @param id the id of the shipmentPackageDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shipmentPackageDTO, or with status 404 (Not Found)
     */
    @GetMapping("/shipment-packages/{id}")
    @Timed
    public ResponseEntity<ShipmentPackageDTO> getShipmentPackage(@PathVariable UUID id) {
        log.debug("REST request to get ShipmentPackage : {}", id);
        ShipmentPackageDTO shipmentPackageDTO = shipmentPackageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(shipmentPackageDTO));
    }

    /**
     * DELETE  /shipment-packages/:id : delete the "id" shipmentPackage.
     *
     * @param id the id of the shipmentPackageDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shipment-packages/{id}")
    @Timed
    public ResponseEntity<Void> deleteShipmentPackage(@PathVariable UUID id) {
        log.debug("REST request to delete ShipmentPackage : {}", id);
        shipmentPackageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/shipment-packages?query=:query : search for the shipmentPackage corresponding
     * to the query.
     *
     * @param query the query of the shipmentPackage search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/shipment-packages")
    @Timed
    public ResponseEntity<List<ShipmentPackageDTO>> searchShipmentPackages(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ShipmentPackages for query {}", query);
        Page<ShipmentPackageDTO> page = shipmentPackageService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/shipment-packages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/shipment-packages/set-status/{id}")
    @Timed
    public ResponseEntity< ShipmentPackageDTO> setStatusShipmentPackage(@PathVariable Integer id, @RequestBody ShipmentPackageDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status ShipmentPackage : {}", dto);
        ShipmentPackageDTO r = shipmentPackageService.changeShipmentPackageStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    @PostMapping("/shipment-packages/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processShipmentPackage(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process ShipmentPackage ");
        Map<String, Object> result = shipmentPackageService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
