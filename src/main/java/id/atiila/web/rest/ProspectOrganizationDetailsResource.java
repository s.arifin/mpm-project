package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ProspectOrganizationDetailsService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ProspectOrganizationDetailsDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProspectOrganizationDetails.
 */
@RestController
@RequestMapping("/api")
public class ProspectOrganizationDetailsResource {

    private final Logger log = LoggerFactory.getLogger(ProspectOrganizationDetailsResource.class);

    private static final String ENTITY_NAME = "prospectOrganizationDetails";

    private final ProspectOrganizationDetailsService prospectOrganizationDetailsService;

    public ProspectOrganizationDetailsResource(ProspectOrganizationDetailsService prospectOrganizationDetailsService) {
        this.prospectOrganizationDetailsService = prospectOrganizationDetailsService;
    }

    /**
     * POST  /prospect-organization-details : Create a new prospectOrganizationDetails.
     *
     * @param prospectOrganizationDetailsDTO the prospectOrganizationDetailsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new prospectOrganizationDetailsDTO, or with status 400 (Bad Request) if the prospectOrganizationDetails has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prospect-organization-details")
    @Timed
    public ResponseEntity<ProspectOrganizationDetailsDTO> createProspectOrganizationDetails(@RequestBody ProspectOrganizationDetailsDTO prospectOrganizationDetailsDTO) throws URISyntaxException {
        log.debug("REST request to save ProspectOrganizationDetails : {}", prospectOrganizationDetailsDTO);
        if (prospectOrganizationDetailsDTO.getIdProspectOrganizationDetail() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new prospectOrganizationDetails cannot already have an ID")).body(null);
        }
        ProspectOrganizationDetailsDTO result = prospectOrganizationDetailsService.save(prospectOrganizationDetailsDTO);
        return ResponseEntity.created(new URI("/api/prospect-organization-details/" + result.getIdProspectOrganizationDetail()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdProspectOrganizationDetail().toString()))
            .body(result);
    }

    /**
     * POST  /prospect-organization-details/execute{id}/{param} : Execute Bussiness Process prospectOrganizationDetails.
     *
     * @param prospectOrganizationDetailsDTO the prospectOrganizationDetailsDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  prospectOrganizationDetailsDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prospect-organization-details/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ProspectOrganizationDetailsDTO> executedProspectOrganizationDetails(@PathVariable Integer id, @PathVariable String param, @RequestBody ProspectOrganizationDetailsDTO prospectOrganizationDetailsDTO) throws URISyntaxException {
        log.debug("REST request to process ProspectOrganizationDetails : {}", prospectOrganizationDetailsDTO);
        ProspectOrganizationDetailsDTO result = prospectOrganizationDetailsService.processExecuteData(id, param, prospectOrganizationDetailsDTO);
        return new ResponseEntity<ProspectOrganizationDetailsDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /prospect-organization-details/execute-list{id}/{param} : Execute Bussiness Process prospectOrganizationDetails.
     *
     * @param prospectOrganizationDetailsDTO the prospectOrganizationDetailsDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  prospectOrganizationDetailsDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prospect-organization-details/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ProspectOrganizationDetailsDTO>> executedListProspectOrganizationDetails(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ProspectOrganizationDetailsDTO> prospectOrganizationDetailsDTO) throws URISyntaxException {
        log.debug("REST request to process List ProspectOrganizationDetails");
        Set<ProspectOrganizationDetailsDTO> result = prospectOrganizationDetailsService.processExecuteListData(id, param, prospectOrganizationDetailsDTO);
        return new ResponseEntity<Set<ProspectOrganizationDetailsDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /prospect-organization-details : Updates an existing prospectOrganizationDetails.
     *
     * @param prospectOrganizationDetailsDTO the prospectOrganizationDetailsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated prospectOrganizationDetailsDTO,
     * or with status 400 (Bad Request) if the prospectOrganizationDetailsDTO is not valid,
     * or with status 500 (Internal Server Error) if the prospectOrganizationDetailsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/prospect-organization-details")
    @Timed
    public ResponseEntity<ProspectOrganizationDetailsDTO> updateProspectOrganizationDetails(@RequestBody ProspectOrganizationDetailsDTO prospectOrganizationDetailsDTO) throws URISyntaxException {
        log.debug("REST request to update ProspectOrganizationDetails : {}", prospectOrganizationDetailsDTO);
        if (prospectOrganizationDetailsDTO.getIdProspectOrganizationDetail() == null) {
            return createProspectOrganizationDetails(prospectOrganizationDetailsDTO);
        }
        ProspectOrganizationDetailsDTO result = prospectOrganizationDetailsService.save(prospectOrganizationDetailsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, prospectOrganizationDetailsDTO.getIdProspectOrganizationDetail().toString()))
            .body(result);
    }

    /**
     * GET  /prospect-organization-details : get all the prospectOrganizationDetails.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of prospectOrganizationDetails in body
     */
    @GetMapping("/prospect-organization-details")
    @Timed
    public ResponseEntity<List<ProspectOrganizationDetailsDTO>> getAllProspectOrganizationDetails(@RequestParam (required = false)  String idProspect, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ProspectOrganizationDetails");
        UUID param = null;
        if (idProspect != null) {
            param = UUID.fromString(idProspect);
        }
        Page<ProspectOrganizationDetailsDTO> page = prospectOrganizationDetailsService.findAll(pageable,param);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/prospect-organization-details");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /prospect-organization-details/:id : get the "id" prospectOrganizationDetails.
     *
     * @param id the id of the prospectOrganizationDetailsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the prospectOrganizationDetailsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/prospect-organization-details/{id}")
    @Timed
    public ResponseEntity<ProspectOrganizationDetailsDTO> getProspectOrganizationDetails(@PathVariable UUID id) {
        log.debug("REST request to get ProspectOrganizationDetails : {}", id);
        ProspectOrganizationDetailsDTO prospectOrganizationDetailsDTO = prospectOrganizationDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(prospectOrganizationDetailsDTO));
    }

    /**
     * DELETE  /prospect-organization-details/:id : delete the "id" prospectOrganizationDetails.
     *
     * @param id the id of the prospectOrganizationDetailsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/prospect-organization-details/{id}")
    @Timed
    public ResponseEntity<Void> deleteProspectOrganizationDetails(@PathVariable UUID id) {
        log.debug("REST request to delete ProspectOrganizationDetails : {}", id);
        prospectOrganizationDetailsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/prospect-organization-details?query=:query : search for the prospectOrganizationDetails corresponding
     * to the query.
     *
     * @param query the query of the prospectOrganizationDetails search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/prospect-organization-details")
    @Timed
    public ResponseEntity<List<ProspectOrganizationDetailsDTO>> searchProspectOrganizationDetails(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ProspectOrganizationDetails for query {}", query);
        Page<ProspectOrganizationDetailsDTO> page = prospectOrganizationDetailsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/prospect-organization-details");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
