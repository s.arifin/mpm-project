package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ShipmentService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ShipmentDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Shipment.
 */
@RestController
@RequestMapping("/api")
public class ShipmentResource {

    private final Logger log = LoggerFactory.getLogger(ShipmentResource.class);

    private static final String ENTITY_NAME = "shipment";

    private final ShipmentService shipmentService;

    public ShipmentResource(ShipmentService shipmentService) {
        this.shipmentService = shipmentService;
    }

    /**
     * POST  /shipments : Create a new shipment.
     *
     * @param shipmentDTO the shipmentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shipmentDTO, or with status 400 (Bad Request) if the shipment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipments")
    @Timed
    public ResponseEntity<ShipmentDTO> createShipment(@RequestBody ShipmentDTO shipmentDTO) throws URISyntaxException {
        log.debug("REST request to save Shipment : {}", shipmentDTO);
        if (shipmentDTO.getIdShipment() != null) {
            throw new BadRequestAlertException("A new shipment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShipmentDTO result = shipmentService.save(shipmentDTO);
        return ResponseEntity.created(new URI("/api/shipments/" + result.getIdShipment()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdShipment().toString()))
            .body(result);
    }

    /**
     * POST  /shipments/process : Execute Bussiness Process shipment.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipments/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processShipment(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Shipment ");
        Map<String, Object> result = shipmentService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /shipments/execute : Execute Bussiness Process shipment.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  shipmentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipments/execute")
    @Timed
    public ResponseEntity<ShipmentDTO> executedShipment(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Shipment");
        ShipmentDTO result = shipmentService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<ShipmentDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /shipments/execute-list : Execute Bussiness Process shipment.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  shipmentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipments/execute-list")
    @Timed
    public ResponseEntity<Set<ShipmentDTO>> executedListShipment(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List Shipment");
        Set<ShipmentDTO> result = shipmentService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<ShipmentDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /shipments : Updates an existing shipment.
     *
     * @param shipmentDTO the shipmentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shipmentDTO,
     * or with status 400 (Bad Request) if the shipmentDTO is not valid,
     * or with status 500 (Internal Server Error) if the shipmentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/shipments")
    @Timed
    public ResponseEntity<ShipmentDTO> updateShipment(@RequestBody ShipmentDTO shipmentDTO) throws URISyntaxException {
        log.debug("REST request to update Shipment : {}", shipmentDTO);
        if (shipmentDTO.getIdShipment() == null) {
            return createShipment(shipmentDTO);
        }
        ShipmentDTO result = shipmentService.save(shipmentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shipmentDTO.getIdShipment().toString()))
            .body(result);
    }

    /**
     * GET  /shipments : get all the shipments.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shipments in body
     */
    @GetMapping("/shipments")
    @Timed
    public ResponseEntity<List<ShipmentDTO>> getAllShipments(Pageable pageable) {
        log.debug("REST request to get a page of Shipments");
        Page<ShipmentDTO> page = shipmentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/shipments/filterBy")
    @Timed
    public ResponseEntity<List<ShipmentDTO>> getAllFilteredShipments(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Shipments");
        Page<ShipmentDTO> page = shipmentService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipments/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /shipments/:id : get the "id" shipment.
     *
     * @param id the id of the shipmentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shipmentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/shipments/{id}")
    @Timed
    public ResponseEntity<ShipmentDTO> getShipment(@PathVariable UUID id) {
        log.debug("REST request to get Shipment : {}", id);
        ShipmentDTO shipmentDTO = shipmentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(shipmentDTO));
    }

    /**
     * DELETE  /shipments/:id : delete the "id" shipment.
     *
     * @param id the id of the shipmentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shipments/{id}")
    @Timed
    public ResponseEntity<Void> deleteShipment(@PathVariable UUID id) {
        log.debug("REST request to delete Shipment : {}", id);
        shipmentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/shipments?query=:query : search for the shipment corresponding
     * to the query.
     *
     * @param query the query of the shipment search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/shipments")
    @Timed
    public ResponseEntity<List<ShipmentDTO>> searchShipments(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Shipments for query {}", query);
        Page<ShipmentDTO> page = shipmentService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/shipments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/shipments/set-status/{id}")
    @Timed
    public ResponseEntity< ShipmentDTO> setStatusShipment(@PathVariable Integer id, @RequestBody ShipmentDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status Shipment : {}", dto);
        ShipmentDTO r = shipmentService.changeShipmentStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
