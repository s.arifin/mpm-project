package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.base.executeprocess.MovingSlipConstants;
import id.atiila.service.MovingSlipService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.MovingSlipDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.camel.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Response;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.concurrent.TimeUnit;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MovingSlip.
 */
@RestController
@RequestMapping("/api")
public class MovingSlipResource {

    private final Logger log = LoggerFactory.getLogger(MovingSlipResource.class);

    private static final String ENTITY_NAME = "movingSlip";

    private final MovingSlipService movingSlipService;

    public MovingSlipResource(MovingSlipService movingSlipService) {
        this.movingSlipService = movingSlipService;
    }

    /**
     * POST  /moving-slips : Create a new movingSlip.
     *
     * @param movingSlipDTO the movingSlipDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new movingSlipDTO, or with status 400 (Bad Request) if the movingSlip has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/moving-slips")
    @Timed
    public ResponseEntity<MovingSlipDTO> createMovingSlip(@RequestBody MovingSlipDTO movingSlipDTO) throws URISyntaxException {
        log.debug("REST request to save MovingSlip : {}", movingSlipDTO);
        if (movingSlipDTO.getIdSlip() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new movingSlip cannot already have an ID")).body(null);
        }
        MovingSlipDTO result = movingSlipService.save(movingSlipDTO);
        return ResponseEntity.created(new URI("/api/moving-slips/" + result.getIdSlip()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdSlip().toString()))
            .body(result);
    }

    /**
     * POST  /moving-slips/execute{id}/{param} : Execute Bussiness Process movingSlip.
     *
     * @param movingSlipDTO the movingSlipDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  movingSlipDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/moving-slips/execute/{id}/{param}")
    @Timed
    public ResponseEntity<MovingSlipDTO> executedMovingSlip(@PathVariable Integer id, @PathVariable String param, @RequestBody MovingSlipDTO movingSlipDTO) throws URISyntaxException {
        log.debug("REST request to process MovingSlip : {}", movingSlipDTO);
        MovingSlipDTO result = movingSlipService.processExecuteData(id, param, movingSlipDTO);
        return new ResponseEntity<MovingSlipDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /moving-slips/execute-list{id}/{param} : Execute Bussiness Process movingSlip.
     *
     * @param movingSlipDTO the movingSlipDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  movingSlipDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/moving-slips/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<?> executedListMovingSlip(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<MovingSlipDTO> movingSlipDTO) throws URISyntaxException{
        log.debug("REST request to process List Moving Slip");
        Set<MovingSlipDTO> result = movingSlipService.processExecuteListData(id, param, movingSlipDTO);
        return new ResponseEntity<Set<MovingSlipDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /moving-slips : Updates an existing movingSlip.
     *
     * @param movingSlipDTO the movingSlipDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated movingSlipDTO,
     * or with status 400 (Bad Request) if the movingSlipDTO is not valid,
     * or with status 500 (Internal Server Error) if the movingSlipDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/moving-slips")
    @Timed
    public ResponseEntity<MovingSlipDTO> updateMovingSlip(@RequestBody MovingSlipDTO movingSlipDTO) throws URISyntaxException {
        log.debug("REST request to update MovingSlip : {}", movingSlipDTO);
        if (movingSlipDTO.getIdSlip() == null) {
            return createMovingSlip(movingSlipDTO);
        }
        MovingSlipDTO result = movingSlipService.save(movingSlipDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, movingSlipDTO.getIdSlip().toString()))
            .body(result);
    }

    /**
     * GET  /moving-slips : get all the movingSlips.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of movingSlips in body
     */
    @GetMapping("/moving-slips")
    @Timed
    public ResponseEntity<List<MovingSlipDTO>> getAllMovingSlips(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of MovingSlips");
        Page<MovingSlipDTO> page = movingSlipService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/moving-slips");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /moving-slips/:id : get the "id" movingSlip.
     *
     * @param id the id of the movingSlipDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the movingSlipDTO, or with status 404 (Not Found)
     */
    @GetMapping("/moving-slips/{id}")
    @Timed
    public ResponseEntity<MovingSlipDTO> getMovingSlip(@PathVariable UUID id) {
        log.debug("REST request to get MovingSlip : {}", id);
        MovingSlipDTO movingSlipDTO = movingSlipService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(movingSlipDTO));
    }

    /**
     * DELETE  /moving-slips/:id : delete the "id" movingSlip.
     *
     * @param id the id of the movingSlipDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/moving-slips/{id}")
    @Timed
    public ResponseEntity<Void> deleteMovingSlip(@PathVariable UUID id) {
        log.debug("REST request to delete MovingSlip : {}", id);
        movingSlipService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/moving-slips?query=:query : search for the movingSlip corresponding
     * to the query.
     *
     * @param query the query of the movingSlip search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/moving-slips")
    @Timed
    public ResponseEntity<List<MovingSlipDTO>> searchMovingSlips(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of MovingSlips for query {}", query);
        Page<MovingSlipDTO> page = movingSlipService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/moving-slips");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/moving-slips/set-status/{id}")
    @Timed
    public ResponseEntity< MovingSlipDTO> setStatusMovingSlip(@PathVariable Integer id, @RequestBody MovingSlipDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status MovingSlip : {}", dto);
        MovingSlipDTO r = movingSlipService.changeMovingSlipStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
