package id.atiila.web.rest;

import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PartyRoleService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PartyRoleDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PartyRole.
 */
@RestController
@RequestMapping("/api")
public class PartyRoleResource {

    private final Logger log = LoggerFactory.getLogger(PartyRoleResource.class);

    private static final String ENTITY_NAME = "partyRole";

    private final PartyRoleService partyRoleService;

    public PartyRoleResource(PartyRoleService partyRoleService) {
        this.partyRoleService = partyRoleService;
    }

    /**
     * POST  /party-roles : Create a new partyRole.
     *
     * @param partyRoleDTO the partyRoleDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new partyRoleDTO, or with status 400 (Bad Request) if the partyRole has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-roles")
    @Timed
    public ResponseEntity<PartyRoleDTO> createPartyRole(@RequestBody PartyRoleDTO partyRoleDTO) throws URISyntaxException {
        log.debug("REST request to save PartyRole : {}", partyRoleDTO);
        if (partyRoleDTO.getIdPartyRole() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new partyRole cannot already have an ID")).body(null);
        }
        PartyRoleDTO result = partyRoleService.save(partyRoleDTO);
        
        return ResponseEntity.created(new URI("/api/party-roles/" + result.getIdPartyRole()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPartyRole().toString()))
            .body(result);
    }

    /**
     * POST  /party-roles/execute : Execute Bussiness Process partyRole.
     *
     * @param partyRoleDTO the partyRoleDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  partyRoleDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-roles/execute")
    @Timed
    public ResponseEntity<PartyRoleDTO> executedPartyRole(@RequestBody PartyRoleDTO partyRoleDTO) throws URISyntaxException {
        log.debug("REST request to process PartyRole : {}", partyRoleDTO);
        return new ResponseEntity<PartyRoleDTO>(partyRoleDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /party-roles : Updates an existing partyRole.
     *
     * @param partyRoleDTO the partyRoleDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated partyRoleDTO,
     * or with status 400 (Bad Request) if the partyRoleDTO is not valid,
     * or with status 500 (Internal Server Error) if the partyRoleDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/party-roles")
    @Timed
    public ResponseEntity<PartyRoleDTO> updatePartyRole(@RequestBody PartyRoleDTO partyRoleDTO) throws URISyntaxException {
        log.debug("REST request to update PartyRole : {}", partyRoleDTO);
        if (partyRoleDTO.getIdPartyRole() == null) {
            return createPartyRole(partyRoleDTO);
        }
        PartyRoleDTO result = partyRoleService.save(partyRoleDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, partyRoleDTO.getIdPartyRole().toString()))
            .body(result);
    }

    /**
     * GET  /party-roles : get all the partyRoles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of partyRoles in body
     */
    @GetMapping("/party-roles")
    @Timed
    public ResponseEntity<List<PartyRoleDTO>> getAllPartyRoles(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PartyRoles");
        Page<PartyRoleDTO> page = partyRoleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/party-roles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /party-roles/:id : get the "id" partyRole.
     *
     * @param id the id of the partyRoleDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the partyRoleDTO, or with status 404 (Not Found)
     */
    @GetMapping("/party-roles/{id}")
    @Timed
    public ResponseEntity<PartyRoleDTO> getPartyRole(@PathVariable UUID id) {
        log.debug("REST request to get PartyRole : {}", id);
        PartyRoleDTO partyRoleDTO = partyRoleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(partyRoleDTO));
    }

    /**
     * DELETE  /party-roles/:id : delete the "id" partyRole.
     *
     * @param id the id of the partyRoleDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/party-roles/{id}")
    @Timed
    public ResponseEntity<Void> deletePartyRole(@PathVariable UUID id) {
        log.debug("REST request to delete PartyRole : {}", id);
        partyRoleService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/party-roles?query=:query : search for the partyRole corresponding
     * to the query.
     *
     * @param query the query of the partyRole search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/party-roles")
    @Timed
    public ResponseEntity<List<PartyRoleDTO>> searchPartyRoles(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of PartyRoles for query {}", query);
        Page<PartyRoleDTO> page = partyRoleService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/party-roles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
