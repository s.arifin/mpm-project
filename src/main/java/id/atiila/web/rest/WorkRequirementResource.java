package id.atiila.web.rest;

import java.util.Map;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.WorkRequirementService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.WorkRequirementDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WorkRequirement.
 */
@RestController
@RequestMapping("/api")
public class WorkRequirementResource {

    private final Logger log = LoggerFactory.getLogger(WorkRequirementResource.class);

    private static final String ENTITY_NAME = "workRequirement";

    private final WorkRequirementService workRequirementService;

    public WorkRequirementResource(WorkRequirementService workRequirementService) {
        this.workRequirementService = workRequirementService;
    }

    /**
     * POST  /work-requirements : Create a new workRequirement.
     *
     * @param workRequirementDTO the workRequirementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new workRequirementDTO, or with status 400 (Bad Request) if the workRequirement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/work-requirements")
    @Timed
    public ResponseEntity<WorkRequirementDTO> createWorkRequirement(@RequestBody WorkRequirementDTO workRequirementDTO) throws URISyntaxException {
        log.debug("REST request to save WorkRequirement : {}", workRequirementDTO);
        if (workRequirementDTO.getIdRequirement() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new workRequirement cannot already have an ID")).body(null);
        }
        WorkRequirementDTO result = workRequirementService.save(workRequirementDTO);

        return ResponseEntity.created(new URI("/api/work-requirements/" + result.getIdRequirement()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * POST  /work-requirements/execute : Execute Bussiness Process workRequirement.
     *
     * @param workRequirementDTO the workRequirementDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  workRequirementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/work-requirements/execute")
    @Timed
    public ResponseEntity<WorkRequirementDTO> executedWorkRequirement(@RequestBody WorkRequirementDTO workRequirementDTO) throws URISyntaxException {
        log.debug("REST request to process WorkRequirement : {}", workRequirementDTO);
        return new ResponseEntity<WorkRequirementDTO>(workRequirementDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /work-requirements : Updates an existing workRequirement.
     *
     * @param workRequirementDTO the workRequirementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated workRequirementDTO,
     * or with status 400 (Bad Request) if the workRequirementDTO is not valid,
     * or with status 500 (Internal Server Error) if the workRequirementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/work-requirements")
    @Timed
    public ResponseEntity<WorkRequirementDTO> updateWorkRequirement(@RequestBody WorkRequirementDTO workRequirementDTO) throws URISyntaxException {
        log.debug("REST request to update WorkRequirement : {}", workRequirementDTO);
        if (workRequirementDTO.getIdRequirement() == null) {
            return createWorkRequirement(workRequirementDTO);
        }
        WorkRequirementDTO result = workRequirementService.save(workRequirementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, workRequirementDTO.getIdRequirement().toString()))
            .body(result);
    }

    /**
     * GET  /work-requirements : get all the workRequirements.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of workRequirements in body
     */
    @GetMapping("/work-requirements")
    @Timed
    public ResponseEntity<List<WorkRequirementDTO>> getAllWorkRequirements(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of WorkRequirements");
        Page<WorkRequirementDTO> page = workRequirementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/work-requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /work-requirements/:id : get the "id" workRequirement.
     *
     * @param id the id of the workRequirementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the workRequirementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/work-requirements/{id}")
    @Timed
    public ResponseEntity<WorkRequirementDTO> getWorkRequirement(@PathVariable UUID id) {
        log.debug("REST request to get WorkRequirement : {}", id);
        WorkRequirementDTO workRequirementDTO = workRequirementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(workRequirementDTO));
    }

    /**
     * DELETE  /work-requirements/:id : delete the "id" workRequirement.
     *
     * @param id the id of the workRequirementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/work-requirements/{id}")
    @Timed
    public ResponseEntity<Void> deleteWorkRequirement(@PathVariable UUID id) {
        log.debug("REST request to delete WorkRequirement : {}", id);
        workRequirementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/work-requirements?query=:query : search for the workRequirement corresponding
     * to the query.
     *
     * @param query the query of the workRequirement search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/work-requirements")
    @Timed
    public ResponseEntity<List<WorkRequirementDTO>> searchWorkRequirements(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of WorkRequirements for query {}", query);
        Page<WorkRequirementDTO> page = workRequirementService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/work-requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/work-requirements/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processWorkRequirement(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process WorkRequirement ");
        Map<String, Object> result = workRequirementService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
