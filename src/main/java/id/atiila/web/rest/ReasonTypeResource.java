package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ReasonTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ReasonTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ReasonType.
 */
@RestController
@RequestMapping("/api")
public class ReasonTypeResource {

    private final Logger log = LoggerFactory.getLogger(ReasonTypeResource.class);

    private static final String ENTITY_NAME = "reasonType";

    private final ReasonTypeService reasonTypeService;

    public ReasonTypeResource(ReasonTypeService reasonTypeService) {
        this.reasonTypeService = reasonTypeService;
    }

    /**
     * POST  /reason-types : Create a new reasonType.
     *
     * @param reasonTypeDTO the reasonTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new reasonTypeDTO, or with status 400 (Bad Request) if the reasonType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/reason-types")
    @Timed
    public ResponseEntity<ReasonTypeDTO> createReasonType(@RequestBody ReasonTypeDTO reasonTypeDTO) throws URISyntaxException {
        log.debug("REST request to save ReasonType : {}", reasonTypeDTO);
        if (reasonTypeDTO.getIdReason() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new reasonType cannot already have an ID")).body(null);
        }
        ReasonTypeDTO result = reasonTypeService.save(reasonTypeDTO);
        return ResponseEntity.created(new URI("/api/reason-types/" + result.getIdReason()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdReason().toString()))
            .body(result);
    }

    /**
     * POST  /reason-types/execute{id}/{param} : Execute Bussiness Process reasonType.
     *
     * @param reasonTypeDTO the reasonTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  reasonTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/reason-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ReasonTypeDTO> executedReasonType(@PathVariable Integer id, @PathVariable String param, @RequestBody ReasonTypeDTO reasonTypeDTO) throws URISyntaxException {
        log.debug("REST request to process ReasonType : {}", reasonTypeDTO);
        ReasonTypeDTO result = reasonTypeService.processExecuteData(id, param, reasonTypeDTO);
        return new ResponseEntity<ReasonTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /reason-types/execute-list{id}/{param} : Execute Bussiness Process reasonType.
     *
     * @param reasonTypeDTO the reasonTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  reasonTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/reason-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ReasonTypeDTO>> executedListReasonType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ReasonTypeDTO> reasonTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List ReasonType");
        Set<ReasonTypeDTO> result = reasonTypeService.processExecuteListData(id, param, reasonTypeDTO);
        return new ResponseEntity<Set<ReasonTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /reason-types : Updates an existing reasonType.
     *
     * @param reasonTypeDTO the reasonTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated reasonTypeDTO,
     * or with status 400 (Bad Request) if the reasonTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the reasonTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/reason-types")
    @Timed
    public ResponseEntity<ReasonTypeDTO> updateReasonType(@RequestBody ReasonTypeDTO reasonTypeDTO) throws URISyntaxException {
        log.debug("REST request to update ReasonType : {}", reasonTypeDTO);
        if (reasonTypeDTO.getIdReason() == null) {
            return createReasonType(reasonTypeDTO);
        }
        ReasonTypeDTO result = reasonTypeService.save(reasonTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, reasonTypeDTO.getIdReason().toString()))
            .body(result);
    }

    /**
     * GET  /reason-types : get all the reasonTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of reasonTypes in body
     */
    @GetMapping("/reason-types")
    @Timed
    public ResponseEntity<List<ReasonTypeDTO>> getAllReasonTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ReasonTypes");
        Page<ReasonTypeDTO> page = reasonTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/reason-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /reason-types/:id : get the "id" reasonType.
     *
     * @param id the id of the reasonTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the reasonTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/reason-types/{id}")
    @Timed
    public ResponseEntity<ReasonTypeDTO> getReasonType(@PathVariable Integer id) {
        log.debug("REST request to get ReasonType : {}", id);
        ReasonTypeDTO reasonTypeDTO = reasonTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(reasonTypeDTO));
    }

    /**
     * DELETE  /reason-types/:id : delete the "id" reasonType.
     *
     * @param id the id of the reasonTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/reason-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteReasonType(@PathVariable Integer id) {
        log.debug("REST request to delete ReasonType : {}", id);
        reasonTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/reason-types?query=:query : search for the reasonType corresponding
     * to the query.
     *
     * @param query the query of the reasonType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/reason-types")
    @Timed
    public ResponseEntity<List<ReasonTypeDTO>> searchReasonTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ReasonTypes for query {}", query);
        Page<ReasonTypeDTO> page = reasonTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/reason-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/reason-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processReasonType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process ReasonType ");
        Map<String, Object> result = reasonTypeService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
