package id.atiila.web.rest;

import java.util.Map;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.SalesmanService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.SalesmanDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Salesman.
 */
@RestController
@RequestMapping("/api")
public class SalesmanResource {

    private final Logger log = LoggerFactory.getLogger(SalesmanResource.class);

    private static final String ENTITY_NAME = "salesman";

    private final SalesmanService salesmanService;

    public SalesmanResource(SalesmanService salesmanService) {
        this.salesmanService = salesmanService;
    }

    /**
     * POST  /salesmen : Create a new salesman.
     *
     * @param salesmanDTO the salesmanDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new salesmanDTO, or with status 400 (Bad Request) if the salesman has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/salesmen")
    @Timed
    public ResponseEntity<SalesmanDTO> createSalesman(@RequestBody SalesmanDTO salesmanDTO) throws URISyntaxException {
        log.debug("REST request to save Salesman : {}", salesmanDTO);
        if (salesmanDTO.getIdPartyRole() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new salesman cannot already have an ID")).body(null);
        }
        SalesmanDTO result = salesmanService.save(salesmanDTO);

        return ResponseEntity.created(new URI("/api/salesmen/" + result.getIdPartyRole()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPartyRole().toString()))
            .body(result);
    }

    /**
     * POST  /salesmen/execute : Execute Bussiness Process salesman.
     *
     * @param salesmanDTO the salesmanDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  salesmanDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/salesmen/execute")
    @Timed
    public ResponseEntity<SalesmanDTO> executedSalesman(@RequestBody SalesmanDTO salesmanDTO) throws URISyntaxException {
        log.debug("REST request to process Salesman : {}", salesmanDTO);
        return new ResponseEntity<SalesmanDTO>(salesmanDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /salesmen : Updates an existing salesman.
     *
     * @param salesmanDTO the salesmanDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated salesmanDTO,
     * or with status 400 (Bad Request) if the salesmanDTO is not valid,
     * or with status 500 (Internal Server Error) if the salesmanDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/salesmen")
    @Timed
    public ResponseEntity<SalesmanDTO> updateSalesman(@RequestBody SalesmanDTO salesmanDTO) throws URISyntaxException {
        log.debug("REST request to update Salesman : {}", salesmanDTO);
        if (salesmanDTO.getIdPartyRole() == null) {
            return createSalesman(salesmanDTO);
        }
        SalesmanDTO result = salesmanService.save(salesmanDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, salesmanDTO.getIdPartyRole().toString()))
            .body(result);
    }

    /**
     * GET  /salesmen : get all the salesmen.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of salesmen in body
     */
    @GetMapping("/salesmen")
    @Timed
    public ResponseEntity<List<SalesmanDTO>> getAllSalesmen(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Salesmen");
        Page<SalesmanDTO> page = salesmanService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/salesmen");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/salesmen/filterBy")
    @Timed
    public ResponseEntity<List<SalesmanDTO>> getAllSalesmenByInternal(HttpServletRequest request, Pageable pageable)
    {
        log.debug("REST request to get a page of Salesmen");
        Page<SalesmanDTO> page = salesmanService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/salesmen/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /salesmen/:id : get the "id" salesman.
     *
     * @param id the id of the salesmanDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the salesmanDTO, or with status 404 (Not Found)
     */
    @GetMapping("/salesmen/{id}")
    @Timed
    public ResponseEntity<SalesmanDTO> getSalesman(@PathVariable UUID id) {
        log.debug("REST request to get Salesman : {}", id);
        SalesmanDTO salesmanDTO = salesmanService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(salesmanDTO));
    }

    /**
     * DELETE  /salesmen/:id : delete the "id" salesman.
     *
     * @param id the id of the salesmanDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/salesmen/{id}")
    @Timed
    public ResponseEntity<Void> deleteSalesman(@PathVariable UUID id) {
        log.debug("REST request to delete Salesman : {}", id);
        salesmanService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/salesmen?query=:query : search for the salesman corresponding
     * to the query.
     *
     * @param query the query of the salesman search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/salesmen")
    @Timed
    public ResponseEntity<List<SalesmanDTO>> searchSalesmen(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Salesmen for query {}", query);
        Page<SalesmanDTO> page = salesmanService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/salesmen");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/salesmens/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processSalesmen(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process Salesmen ");
        Map<String, Object> result = salesmanService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
