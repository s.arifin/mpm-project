package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.SalesUnitLeasingService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.SalesUnitLeasingDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SalesUnitLeasing.
 */
@RestController
@RequestMapping("/api")
public class SalesUnitLeasingResource {

    private final Logger log = LoggerFactory.getLogger(SalesUnitLeasingResource.class);

    private static final String ENTITY_NAME = "salesUnitLeasing";

    private final SalesUnitLeasingService salesUnitLeasingService;

    public SalesUnitLeasingResource(SalesUnitLeasingService salesUnitLeasingService) {
        this.salesUnitLeasingService = salesUnitLeasingService;
    }

    /**
     * POST  /sales-unit-leasings : Create a new salesUnitLeasing.
     *
     * @param salesUnitLeasingDTO the salesUnitLeasingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new salesUnitLeasingDTO, or with status 400 (Bad Request) if the salesUnitLeasing has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-unit-leasings")
    @Timed
    public ResponseEntity<SalesUnitLeasingDTO> createSalesUnitLeasing(@RequestBody SalesUnitLeasingDTO salesUnitLeasingDTO) throws URISyntaxException {
        log.debug("REST request to save SalesUnitLeasing : {}", salesUnitLeasingDTO);
        if (salesUnitLeasingDTO.getIdSalesLeasing() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new salesUnitLeasing cannot already have an ID")).body(null);
        }
        SalesUnitLeasingDTO result = salesUnitLeasingService.save(salesUnitLeasingDTO);
        return ResponseEntity.created(new URI("/api/sales-unit-leasings/" + result.getIdSalesLeasing()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdSalesLeasing().toString()))
            .body(result);
    }

    /**
     * POST  /sales-unit-leasings/execute{id}/{param} : Execute Bussiness Process salesUnitLeasing.
     *
     * @param salesUnitLeasingDTO the salesUnitLeasingDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  salesUnitLeasingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-unit-leasings/execute/{id}/{param}")
    @Timed
    public ResponseEntity<SalesUnitLeasingDTO> executedSalesUnitLeasing(@PathVariable Integer id, @PathVariable String param, @RequestBody SalesUnitLeasingDTO salesUnitLeasingDTO) throws URISyntaxException {
        log.debug("REST request to process SalesUnitLeasing : {}", salesUnitLeasingDTO);
        SalesUnitLeasingDTO result = salesUnitLeasingService.processExecuteData(id, param, salesUnitLeasingDTO);
        return new ResponseEntity<SalesUnitLeasingDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /sales-unit-leasings/execute-list{id}/{param} : Execute Bussiness Process salesUnitLeasing.
     *
     * @param salesUnitLeasingDTO the salesUnitLeasingDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  salesUnitLeasingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-unit-leasings/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<SalesUnitLeasingDTO>> executedListSalesUnitLeasing(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<SalesUnitLeasingDTO> salesUnitLeasingDTO) throws URISyntaxException {
        log.debug("REST request to process List SalesUnitLeasing");
        Set<SalesUnitLeasingDTO> result = salesUnitLeasingService.processExecuteListData(id, param, salesUnitLeasingDTO);
        return new ResponseEntity<Set<SalesUnitLeasingDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /sales-unit-leasings : Updates an existing salesUnitLeasing.
     *
     * @param salesUnitLeasingDTO the salesUnitLeasingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated salesUnitLeasingDTO,
     * or with status 400 (Bad Request) if the salesUnitLeasingDTO is not valid,
     * or with status 500 (Internal Server Error) if the salesUnitLeasingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sales-unit-leasings")
    @Timed
    public ResponseEntity<SalesUnitLeasingDTO> updateSalesUnitLeasing(@RequestBody SalesUnitLeasingDTO salesUnitLeasingDTO) throws URISyntaxException {
        log.debug("REST request to update SalesUnitLeasing : {}", salesUnitLeasingDTO);
        if (salesUnitLeasingDTO.getIdSalesLeasing() == null) {
            return createSalesUnitLeasing(salesUnitLeasingDTO);
        }
        SalesUnitLeasingDTO result = salesUnitLeasingService.save(salesUnitLeasingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, salesUnitLeasingDTO.getIdSalesLeasing().toString()))
            .body(result);
    }

    /**
     * GET  /sales-unit-leasings : get all the salesUnitLeasings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of salesUnitLeasings in body
     */
    @GetMapping("/sales-unit-leasings")
    @Timed
    public ResponseEntity<List<SalesUnitLeasingDTO>> getAllSalesUnitLeasings(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SalesUnitLeasings");
        Page<SalesUnitLeasingDTO> page = salesUnitLeasingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-unit-leasings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    
    @GetMapping("/sales-unit-leasings/filterBy")
    @Timed
    public ResponseEntity<List<SalesUnitLeasingDTO>> getAllFilteredSalesUnitLeasings(@RequestParam String filterBy, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SalesUnitLeasings");
        Page<SalesUnitLeasingDTO> page = salesUnitLeasingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-unit-leasings/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    

    /**
     * GET  /sales-unit-leasings/:id : get the "id" salesUnitLeasing.
     *
     * @param id the id of the salesUnitLeasingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the salesUnitLeasingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sales-unit-leasings/{id}")
    @Timed
    public ResponseEntity<SalesUnitLeasingDTO> getSalesUnitLeasing(@PathVariable UUID id) {
        log.debug("REST request to get SalesUnitLeasing : {}", id);
        SalesUnitLeasingDTO salesUnitLeasingDTO = salesUnitLeasingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(salesUnitLeasingDTO));
    }

    /**
     * DELETE  /sales-unit-leasings/:id : delete the "id" salesUnitLeasing.
     *
     * @param id the id of the salesUnitLeasingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sales-unit-leasings/{id}")
    @Timed
    public ResponseEntity<Void> deleteSalesUnitLeasing(@PathVariable UUID id) {
        log.debug("REST request to delete SalesUnitLeasing : {}", id);
        salesUnitLeasingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/sales-unit-leasings?query=:query : search for the salesUnitLeasing corresponding
     * to the query.
     *
     * @param query the query of the salesUnitLeasing search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/sales-unit-leasings")
    @Timed
    public ResponseEntity<List<SalesUnitLeasingDTO>> searchSalesUnitLeasings(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SalesUnitLeasings for query {}", query);
        Page<SalesUnitLeasingDTO> page = salesUnitLeasingService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/sales-unit-leasings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
