package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.StatusTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.StatusTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing StatusType.
 */
@RestController
@RequestMapping("/api")
public class StatusTypeResource {

    private final Logger log = LoggerFactory.getLogger(StatusTypeResource.class);

    private static final String ENTITY_NAME = "statusType";

    private final StatusTypeService statusTypeService;

    public StatusTypeResource(StatusTypeService statusTypeService) {
        this.statusTypeService = statusTypeService;
    }

    /**
     * POST  /status-types : Create a new statusType.
     *
     * @param statusTypeDTO the statusTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new statusTypeDTO, or with status 400 (Bad Request) if the statusType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/status-types")
    @Timed
    public ResponseEntity<StatusTypeDTO> createStatusType(@RequestBody StatusTypeDTO statusTypeDTO) throws URISyntaxException {
        log.debug("REST request to save StatusType : {}", statusTypeDTO);
        if (statusTypeDTO.getIdStatusType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new statusType cannot already have an ID")).body(null);
        }
        StatusTypeDTO result = statusTypeService.save(statusTypeDTO);
        
        return ResponseEntity.created(new URI("/api/status-types/" + result.getIdStatusType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdStatusType().toString()))
            .body(result);
    }

    /**
     * POST  /status-types/execute : Execute Bussiness Process statusType.
     *
     * @param statusTypeDTO the statusTypeDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  statusTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/status-types/execute")
    @Timed
    public ResponseEntity<StatusTypeDTO> executedStatusType(@RequestBody StatusTypeDTO statusTypeDTO) throws URISyntaxException {
        log.debug("REST request to process StatusType : {}", statusTypeDTO);
        return new ResponseEntity<StatusTypeDTO>(statusTypeDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /status-types : Updates an existing statusType.
     *
     * @param statusTypeDTO the statusTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated statusTypeDTO,
     * or with status 400 (Bad Request) if the statusTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the statusTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/status-types")
    @Timed
    public ResponseEntity<StatusTypeDTO> updateStatusType(@RequestBody StatusTypeDTO statusTypeDTO) throws URISyntaxException {
        log.debug("REST request to update StatusType : {}", statusTypeDTO);
        if (statusTypeDTO.getIdStatusType() == null) {
            return createStatusType(statusTypeDTO);
        }
        StatusTypeDTO result = statusTypeService.save(statusTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, statusTypeDTO.getIdStatusType().toString()))
            .body(result);
    }

    /**
     * GET  /status-types : get all the statusTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of statusTypes in body
     */
    @GetMapping("/status-types")
    @Timed
    public ResponseEntity<List<StatusTypeDTO>> getAllStatusTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of StatusTypes");
        Page<StatusTypeDTO> page = statusTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/status-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /status-types/:id : get the "id" statusType.
     *
     * @param id the id of the statusTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the statusTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/status-types/{id}")
    @Timed
    public ResponseEntity<StatusTypeDTO> getStatusType(@PathVariable Integer id) {
        log.debug("REST request to get StatusType : {}", id);
        StatusTypeDTO statusTypeDTO = statusTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(statusTypeDTO));
    }

    /**
     * DELETE  /status-types/:id : delete the "id" statusType.
     *
     * @param id the id of the statusTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/status-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteStatusType(@PathVariable Integer id) {
        log.debug("REST request to delete StatusType : {}", id);
        statusTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/status-types?query=:query : search for the statusType corresponding
     * to the query.
     *
     * @param query the query of the statusType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/status-types")
    @Timed
    public ResponseEntity<List<StatusTypeDTO>> searchStatusTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of StatusTypes for query {}", query);
        Page<StatusTypeDTO> page = statusTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/status-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
