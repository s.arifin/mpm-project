package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.MemoTypeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.MemoTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MemoType.
 */
@RestController
@RequestMapping("/api")
public class MemoTypeResource {

    private final Logger log = LoggerFactory.getLogger(MemoTypeResource.class);

    private static final String ENTITY_NAME = "memoType";

    private final MemoTypeService memoTypeService;

    public MemoTypeResource(MemoTypeService memoTypeService) {
        this.memoTypeService = memoTypeService;
    }

    /**
     * POST  /memo-types : Create a new memoType.
     *
     * @param memoTypeDTO the memoTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new memoTypeDTO, or with status 400 (Bad Request) if the memoType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/memo-types")
    @Timed
    public ResponseEntity<MemoTypeDTO> createMemoType(@RequestBody MemoTypeDTO memoTypeDTO) throws URISyntaxException {
        log.debug("REST request to save MemoType : {}", memoTypeDTO);
        if (memoTypeDTO.getIdMemoType() != null) {
            throw new BadRequestAlertException("A new memoType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MemoTypeDTO result = memoTypeService.save(memoTypeDTO);
        return ResponseEntity.created(new URI("/api/memo-types/" + result.getIdMemoType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdMemoType().toString()))
            .body(result);
    }

    /**
     * POST  /memo-types/execute{id}/{param} : Execute Bussiness Process memoType.
     *
     * @param memoTypeDTO the memoTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  memoTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/memo-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<MemoTypeDTO> executedMemoType(@PathVariable Integer id, @PathVariable String param, @RequestBody MemoTypeDTO memoTypeDTO) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to process MemoType : {}", memoTypeDTO);
        MemoTypeDTO result = memoTypeService.processExecuteData(id, param, memoTypeDTO);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<MemoTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /memo-types/execute-list{id}/{param} : Execute Bussiness Process memoType.
     *
     * @param memoTypeDTO the memoTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  memoTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/memo-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<MemoTypeDTO>> executedListMemoType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<MemoTypeDTO> memoTypeDTO) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List MemoType");
        Set<MemoTypeDTO> result = memoTypeService.processExecuteListData(id, param, memoTypeDTO);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<MemoTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /memo-types : Updates an existing memoType.
     *
     * @param memoTypeDTO the memoTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated memoTypeDTO,
     * or with status 400 (Bad Request) if the memoTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the memoTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/memo-types")
    @Timed
    public ResponseEntity<MemoTypeDTO> updateMemoType(@RequestBody MemoTypeDTO memoTypeDTO) throws URISyntaxException {
        log.debug("REST request to update MemoType : {}", memoTypeDTO);
        if (memoTypeDTO.getIdMemoType() == null) {
            return createMemoType(memoTypeDTO);
        }
        MemoTypeDTO result = memoTypeService.save(memoTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, memoTypeDTO.getIdMemoType().toString()))
            .body(result);
    }

    /**
     * GET  /memo-types : get all the memoTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of memoTypes in body
     */
    @GetMapping("/memo-types")
    @Timed
    public ResponseEntity<List<MemoTypeDTO>> getAllMemoTypes(Pageable pageable) {
        log.debug("REST request to get a page of MemoTypes");
        Page<MemoTypeDTO> page = memoTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/memo-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /memo-types/:id : get the "id" memoType.
     *
     * @param id the id of the memoTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the memoTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/memo-types/{id}")
    @Timed
    public ResponseEntity<MemoTypeDTO> getMemoType(@PathVariable Integer id) {
        log.debug("REST request to get MemoType : {}", id);
        MemoTypeDTO memoTypeDTO = memoTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(memoTypeDTO));
    }

    /**
     * DELETE  /memo-types/:id : delete the "id" memoType.
     *
     * @param id the id of the memoTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/memo-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteMemoType(@PathVariable Integer id) {
        log.debug("REST request to delete MemoType : {}", id);
        memoTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/memo-types?query=:query : search for the memoType corresponding
     * to the query.
     *
     * @param query the query of the memoType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/memo-types")
    @Timed
    public ResponseEntity<List<MemoTypeDTO>> searchMemoTypes(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of MemoTypes for query {}", query);
        Page<MemoTypeDTO> page = memoTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/memo-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
