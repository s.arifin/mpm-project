package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.base.DmsException;
import id.atiila.service.RequestUnitInternalService;
import id.atiila.service.dto.CustomPreRequestUnitInternalDTO;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RequestUnitInternalDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RequestUnitInternal.
 */
@RestController
@RequestMapping("/api")
public class RequestUnitInternalResource {

    private final Logger log = LoggerFactory.getLogger(RequestUnitInternalResource.class);

    private static final String ENTITY_NAME = "requestUnitInternal";

    private final RequestUnitInternalService requestUnitInternalService;

    public RequestUnitInternalResource(RequestUnitInternalService requestUnitInternalService) {
        this.requestUnitInternalService = requestUnitInternalService;
    }

    @GetMapping("/request-unit-internals/pre")
    @Timed
    public ResponseEntity<List<CustomPreRequestUnitInternalDTO>> findPreRequestUnitInternal(
        @RequestParam String idinternal, Pageable pageable) {
        log.debug("REST request to find pre request unit internal" + idinternal);
        if (idinternal == null) {
            throw new DmsException("No idinternal");
        }
        Page<CustomPreRequestUnitInternalDTO> page = requestUnitInternalService.findAllPreRequestUnitInternal(idinternal, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/request-unit-internals/pre");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * POST  /request-unit-internals : Create a new requestUnitInternal.
     *
     * @param requestUnitInternalDTO the requestUnitInternalDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new requestUnitInternalDTO, or with status 400 (Bad Request) if the requestUnitInternal has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-unit-internals")
    @Timed
    public ResponseEntity<RequestUnitInternalDTO> createRequestUnitInternal(@RequestBody RequestUnitInternalDTO requestUnitInternalDTO) throws URISyntaxException {
        log.debug("REST request to save RequestUnitInternal : {}", requestUnitInternalDTO);
        if (requestUnitInternalDTO.getIdRequest() != null) {
            throw new BadRequestAlertException("A new requestUnitInternal cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RequestUnitInternalDTO result = requestUnitInternalService.save(requestUnitInternalDTO);
        return ResponseEntity.created(new URI("/api/request-unit-internals/" + result.getIdRequest()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequest().toString()))
            .body(result);
    }

    /**
     * POST  /request-unit-internals/process : Execute Bussiness Process requestUnitInternal.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-unit-internals/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processRequestUnitInternal(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RequestUnitInternal ");
        Map<String, Object> result = requestUnitInternalService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /request-unit-internals/execute : Execute Bussiness Process requestUnitInternal.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  requestUnitInternalDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-unit-internals/execute")
    @Timed
    public ResponseEntity<RequestUnitInternalDTO> executedRequestUnitInternal(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RequestUnitInternal");
        RequestUnitInternalDTO result = requestUnitInternalService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<RequestUnitInternalDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /request-unit-internals/execute-list : Execute Bussiness Process requestUnitInternal.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  requestUnitInternalDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-unit-internals/execute-list")
    @Timed
    public ResponseEntity<Set<RequestUnitInternalDTO>> executedListRequestUnitInternal(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List RequestUnitInternal");
        Set<RequestUnitInternalDTO> result = requestUnitInternalService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<RequestUnitInternalDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /request-unit-internals : Updates an existing requestUnitInternal.
     *
     * @param requestUnitInternalDTO the requestUnitInternalDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated requestUnitInternalDTO,
     * or with status 400 (Bad Request) if the requestUnitInternalDTO is not valid,
     * or with status 500 (Internal Server Error) if the requestUnitInternalDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/request-unit-internals")
    @Timed
    public ResponseEntity<RequestUnitInternalDTO> updateRequestUnitInternal(@RequestBody RequestUnitInternalDTO requestUnitInternalDTO) throws URISyntaxException {
        log.debug("REST request to update RequestUnitInternal : {}", requestUnitInternalDTO);
        if (requestUnitInternalDTO.getIdRequest() == null) {
            return createRequestUnitInternal(requestUnitInternalDTO);
        }
        RequestUnitInternalDTO result = requestUnitInternalService.save(requestUnitInternalDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, requestUnitInternalDTO.getIdRequest().toString()))
            .body(result);
    }

    /**
     * GET  /request-unit-internals : get all the requestUnitInternals.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of requestUnitInternals in body
     */
    @GetMapping("/request-unit-internals")
    @Timed
    public ResponseEntity<List<RequestUnitInternalDTO>> getAllRequestUnitInternals(Pageable pageable) {
        log.debug("REST request to get a page of RequestUnitInternals");
        Page<RequestUnitInternalDTO> page = requestUnitInternalService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/request-unit-internals");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/request-unit-internals/filterBy")
    @Timed
    public ResponseEntity<List<RequestUnitInternalDTO>> getAllFilteredRequestUnitInternals(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of RequestUnitInternals");
        Page<RequestUnitInternalDTO> page = requestUnitInternalService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/request-unit-internals/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /request-unit-internals/:id : get the "id" requestUnitInternal.
     *
     * @param id the id of the requestUnitInternalDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the requestUnitInternalDTO, or with status 404 (Not Found)
     */
    @GetMapping("/request-unit-internals/{id}")
    @Timed
    public ResponseEntity<RequestUnitInternalDTO> getRequestUnitInternal(@PathVariable UUID id) {
        log.debug("REST request to get RequestUnitInternal : {}", id);
        RequestUnitInternalDTO requestUnitInternalDTO = requestUnitInternalService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(requestUnitInternalDTO));
    }

    /**
     * DELETE  /request-unit-internals/:id : delete the "id" requestUnitInternal.
     *
     * @param id the id of the requestUnitInternalDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/request-unit-internals/{id}")
    @Timed
    public ResponseEntity<Void> deleteRequestUnitInternal(@PathVariable UUID id) {
        log.debug("REST request to delete RequestUnitInternal : {}", id);
        requestUnitInternalService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/request-unit-internals?query=:query : search for the requestUnitInternal corresponding
     * to the query.
     *
     * @param query the query of the requestUnitInternal search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/request-unit-internals")
    @Timed
    public ResponseEntity<List<RequestUnitInternalDTO>> searchRequestUnitInternals(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of RequestUnitInternals for query {}", query);
        Page<RequestUnitInternalDTO> page = requestUnitInternalService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/request-unit-internals");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/request-unit-internals/set-status/{id}")
    @Timed
    public ResponseEntity< RequestUnitInternalDTO> setStatusRequestUnitInternal(@PathVariable Integer id, @RequestBody RequestUnitInternalDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status RequestUnitInternal : {}", dto);
        RequestUnitInternalDTO r = requestUnitInternalService.changeRequestUnitInternalStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
