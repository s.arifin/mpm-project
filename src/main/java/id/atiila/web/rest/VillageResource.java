package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.VillageService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VillageDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Village.
 */
@RestController
@RequestMapping("/api")
public class VillageResource {

    private final Logger log = LoggerFactory.getLogger(VillageResource.class);

    private static final String ENTITY_NAME = "village";

    private final VillageService villageService;

    public VillageResource(VillageService villageService) {
        this.villageService = villageService;
    }

    /**
     * POST  /villages : Create a new village.
     *
     * @param villageDTO the villageDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new villageDTO, or with status 400 (Bad Request) if the village has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/villages")
    @Timed
    public ResponseEntity<VillageDTO> createVillage(@RequestBody VillageDTO villageDTO) throws URISyntaxException {
        log.debug("REST request to save Village : {}", villageDTO);
        if (villageDTO.getIdGeobou() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new village cannot already have an ID")).body(null);
        }
        VillageDTO result = villageService.save(villageDTO);
        return ResponseEntity.created(new URI("/api/villages/" + result.getIdGeobou()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdGeobou().toString()))
            .body(result);
    }

    /**
     * POST  /villages/execute{id}/{param} : Execute Bussiness Process village.
     *
     * @param villageDTO the villageDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  villageDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/villages/execute/{id}/{param}")
    @Timed
    public ResponseEntity<VillageDTO> executedVillage(@PathVariable Integer id, @PathVariable String param, @RequestBody VillageDTO villageDTO) throws URISyntaxException {
        log.debug("REST request to process Village : {}", villageDTO);
        VillageDTO result = villageService.processExecuteData(id, param, villageDTO);
        return new ResponseEntity<VillageDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /villages/execute-list{id}/{param} : Execute Bussiness Process village.
     *
     * @param villageDTO the villageDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  villageDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/villages/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<VillageDTO>> executedListVillage(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<VillageDTO> villageDTO) throws URISyntaxException {
        log.debug("REST request to process List Village");
        Set<VillageDTO> result = villageService.processExecuteListData(id, param, villageDTO);
        return new ResponseEntity<Set<VillageDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /villages : Updates an existing village.
     *
     * @param villageDTO the villageDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated villageDTO,
     * or with status 400 (Bad Request) if the villageDTO is not valid,
     * or with status 500 (Internal Server Error) if the villageDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/villages")
    @Timed
    public ResponseEntity<VillageDTO> updateVillage(@RequestBody VillageDTO villageDTO) throws URISyntaxException {
        log.debug("REST request to update Village : {}", villageDTO);
        if (villageDTO.getIdGeobou() == null) {
            return createVillage(villageDTO);
        }
        VillageDTO result = villageService.save(villageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, villageDTO.getIdGeobou().toString()))
            .body(result);
    }

    /**
     * GET  /villages : get all the villages.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of villages in body
     */
    @GetMapping("/villages")
    @Timed
    public ResponseEntity<List<VillageDTO>> getAllVillages(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Villages");
        Page<VillageDTO> page = villageService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/villages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/villages/by-district")
    @Timed
    public ResponseEntity<List<VillageDTO>> getAllVillagesByDistrict(@RequestParam String iddistrict,
                                                                     @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Villages");
        UUID idDistrict = iddistrict.equals("none") ? null : UUID.fromString(iddistrict);

        if (idDistrict == null) return getAllVillages(pageable);

        List<VillageDTO> list = villageService.findAllByDistrict(idDistrict);
        return new ResponseEntity<>(list,null, HttpStatus.OK);
    }


    /**
     * GET  /villages/:id : get the "id" village.
     *
     * @param id the id of the villageDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the villageDTO, or with status 404 (Not Found)
     */
    @GetMapping("/villages/{id}")
    @Timed
    public ResponseEntity<VillageDTO> getVillage(@PathVariable String id) {
        UUID idVillage = UUID.fromString(id);
        log.debug("REST request to get Village : {}", idVillage);
        VillageDTO villageDTO = villageService.findOne(idVillage);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(villageDTO));
    }

    /**
     * DELETE  /villages/:id : delete the "id" village.
     *
     * @param id the id of the villageDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/villages/{id}")
    @Timed
    public ResponseEntity<Void> deleteVillage(@PathVariable UUID id) {
        log.debug("REST request to delete Village : {}", id);
        villageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/villages?query=:query : search for the village corresponding
     * to the query.
     *
     * @param query the query of the village search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/villages")
    @Timed
    public ResponseEntity<List<VillageDTO>> searchVillages(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Villages for query {}", query);
        Page<VillageDTO> page = villageService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/villages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
