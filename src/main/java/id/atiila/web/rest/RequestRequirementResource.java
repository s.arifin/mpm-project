package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.RequestRequirementService;
import id.atiila.service.dto.CustomRequestRequirementDTO;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.RequestRequirementDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Response;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RequestRequirement.
 */
@RestController
@RequestMapping("/api")
public class RequestRequirementResource {

    private final Logger log = LoggerFactory.getLogger(RequestRequirementResource.class);

    private static final String ENTITY_NAME = "requestRequirement";

    private final RequestRequirementService requestRequirementService;

    public RequestRequirementResource(RequestRequirementService requestRequirementService) {
        this.requestRequirementService = requestRequirementService;
    }

    @GetMapping("/request-requirements/custom")
    @Timed
    public ResponseEntity<List<CustomRequestRequirementDTO>> getCustomRequestRequirement(){
        log.debug("REST request to get custom RequestRequirement");
        List<CustomRequestRequirementDTO> list = requestRequirementService.getAllCustomRequestRequirement();
        return new ResponseEntity<>(list, null, HttpStatus.OK);
    }

    /**
     * POST  /request-requirements : Create a new requestRequirement.
     *
     * @param requestRequirementDTO the requestRequirementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new requestRequirementDTO, or with status 400 (Bad Request) if the requestRequirement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-requirements")
    @Timed
    public ResponseEntity<RequestRequirementDTO> createRequestRequirement(@RequestBody RequestRequirementDTO requestRequirementDTO) throws URISyntaxException {
        log.debug("REST request to save RequestRequirement : {}", requestRequirementDTO);
        if (requestRequirementDTO.getIdRequest() != null) {
            throw new BadRequestAlertException("A new requestRequirement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RequestRequirementDTO result = requestRequirementService.save(requestRequirementDTO);
        return ResponseEntity.created(new URI("/api/request-requirements/" + result.getIdRequest()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequest().toString()))
            .body(result);
    }

    /**
     * POST  /request-requirements/process : Execute Bussiness Process requestRequirement.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-requirements/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processRequestRequirement(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RequestRequirement ");
        Map<String, Object> result = requestRequirementService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /request-requirements/execute : Execute Bussiness Process requestRequirement.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  requestRequirementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-requirements/execute")
    @Timed
    public ResponseEntity<RequestRequirementDTO> executedRequestRequirement(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process RequestRequirement");
        RequestRequirementDTO result = requestRequirementService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<RequestRequirementDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /request-requirements/execute-list : Execute Bussiness Process requestRequirement.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  requestRequirementDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/request-requirements/execute-list")
    @Timed
    public ResponseEntity<Set<RequestRequirementDTO>> executedListRequestRequirement(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List RequestRequirement");
        Set<RequestRequirementDTO> result = requestRequirementService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<RequestRequirementDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /request-requirements : Updates an existing requestRequirement.
     *
     * @param requestRequirementDTO the requestRequirementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated requestRequirementDTO,
     * or with status 400 (Bad Request) if the requestRequirementDTO is not valid,
     * or with status 500 (Internal Server Error) if the requestRequirementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/request-requirements")
    @Timed
    public ResponseEntity<RequestRequirementDTO> updateRequestRequirement(@RequestBody RequestRequirementDTO requestRequirementDTO) throws URISyntaxException {
        log.debug("REST request to update RequestRequirement : {}", requestRequirementDTO);
        if (requestRequirementDTO.getIdRequest() == null) {
            return createRequestRequirement(requestRequirementDTO);
        }
        RequestRequirementDTO result = requestRequirementService.save(requestRequirementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, requestRequirementDTO.getIdRequest().toString()))
            .body(result);
    }

    /**
     * GET  /request-requirements : get all the requestRequirements.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of requestRequirements in body
     */
    @GetMapping("/request-requirements")
    @Timed
    public ResponseEntity<List<RequestRequirementDTO>> getAllRequestRequirements(Pageable pageable) {
        log.debug("REST request to get a page of RequestRequirements");
        Page<RequestRequirementDTO> page = requestRequirementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/request-requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/request-requirements/filterBy")
    @Timed
    public ResponseEntity<List<RequestRequirementDTO>> getAllFilteredRequestRequirements(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of RequestRequirements");
        Page<RequestRequirementDTO> page = requestRequirementService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/request-requirements/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /request-requirements/:id : get the "id" requestRequirement.
     *
     * @param id the id of the requestRequirementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the requestRequirementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/request-requirements/{id}")
    @Timed
    public ResponseEntity<RequestRequirementDTO> getRequestRequirement(@PathVariable UUID id) {
        log.debug("REST request to get RequestRequirement : {}", id);
        RequestRequirementDTO requestRequirementDTO = requestRequirementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(requestRequirementDTO));
    }

    /**
     * DELETE  /request-requirements/:id : delete the "id" requestRequirement.
     *
     * @param id the id of the requestRequirementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/request-requirements/{id}")
    @Timed
    public ResponseEntity<Void> deleteRequestRequirement(@PathVariable UUID id) {
        log.debug("REST request to delete RequestRequirement : {}", id);
        requestRequirementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/request-requirements?query=:query : search for the requestRequirement corresponding
     * to the query.
     *
     * @param query the query of the requestRequirement search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/request-requirements")
    @Timed
    public ResponseEntity<List<RequestRequirementDTO>> searchRequestRequirements(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of RequestRequirements for query {}", query);
        Page<RequestRequirementDTO> page = requestRequirementService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/request-requirements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/request-requirements/set-status/{id}")
    @Timed
    public ResponseEntity< RequestRequirementDTO> setStatusRequestRequirement(@PathVariable Integer id, @RequestBody RequestRequirementDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status RequestRequirement : {}", dto);
        RequestRequirementDTO r = requestRequirementService.changeRequestRequirementStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(100);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
