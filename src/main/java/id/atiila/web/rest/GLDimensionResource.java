package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.GLDimensionService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.GLDimensionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing GLDimension.
 */
@RestController
@RequestMapping("/api")
public class GLDimensionResource {

    private final Logger log = LoggerFactory.getLogger(GLDimensionResource.class);

    private static final String ENTITY_NAME = "gLDimension";

    private final GLDimensionService gLDimensionService;

    public GLDimensionResource(GLDimensionService gLDimensionService) {
        this.gLDimensionService = gLDimensionService;
    }

    /**
     * POST  /gl-dimensions : Create a new gLDimension.
     *
     * @param gLDimensionDTO the gLDimensionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new gLDimensionDTO, or with status 400 (Bad Request) if the gLDimension has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gl-dimensions")
    @Timed
    public ResponseEntity<GLDimensionDTO> createGLDimension(@RequestBody GLDimensionDTO gLDimensionDTO) throws URISyntaxException {
        log.debug("REST request to save GLDimension : {}", gLDimensionDTO);
        if (gLDimensionDTO.getIdGLDimension() != null) {
            throw new BadRequestAlertException("A new gLDimension cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GLDimensionDTO result = gLDimensionService.save(gLDimensionDTO);
        return ResponseEntity.created(new URI("/api/gl-dimensions/" + result.getIdGLDimension()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdGLDimension().toString()))
            .body(result);
    }

    /**
     * POST  /gl-dimensions/process : Execute Bussiness Process gLDimension.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gl-dimensions/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processGLDimension(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process GLDimension ");
        Map<String, Object> result = gLDimensionService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /gl-dimensions/execute : Execute Bussiness Process gLDimension.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  gLDimensionDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gl-dimensions/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedGLDimension(HttpServletRequest request, @RequestBody GLDimensionDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process GLDimension");
        Map<String, Object> result = gLDimensionService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /gl-dimensions/execute-list : Execute Bussiness Process gLDimension.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  gLDimensionDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gl-dimensions/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListGLDimension(HttpServletRequest request, @RequestBody Set<GLDimensionDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List GLDimension");
        Map<String, Object> result = gLDimensionService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /gl-dimensions : Updates an existing gLDimension.
     *
     * @param gLDimensionDTO the gLDimensionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated gLDimensionDTO,
     * or with status 400 (Bad Request) if the gLDimensionDTO is not valid,
     * or with status 500 (Internal Server Error) if the gLDimensionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/gl-dimensions")
    @Timed
    public ResponseEntity<GLDimensionDTO> updateGLDimension(@RequestBody GLDimensionDTO gLDimensionDTO) throws URISyntaxException {
        log.debug("REST request to update GLDimension : {}", gLDimensionDTO);
        if (gLDimensionDTO.getIdGLDimension() == null) {
            return createGLDimension(gLDimensionDTO);
        }
        GLDimensionDTO result = gLDimensionService.save(gLDimensionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, gLDimensionDTO.getIdGLDimension().toString()))
            .body(result);
    }

    /**
     * GET  /gl-dimensions : get all the gLDimensions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of gLDimensions in body
     */
    @GetMapping("/gl-dimensions")
    @Timed
    public ResponseEntity<List<GLDimensionDTO>> getAllGLDimensions(Pageable pageable) {
        log.debug("REST request to get a page of GLDimensions");
        Page<GLDimensionDTO> page = gLDimensionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/gl-dimensions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/gl-dimensions/filterBy")
    @Timed
    public ResponseEntity<List<GLDimensionDTO>> getAllFilteredGLDimensions(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of GLDimensions");
        Page<GLDimensionDTO> page = gLDimensionService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/gl-dimensions/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /gl-dimensions/:id : get the "id" gLDimension.
     *
     * @param id the id of the gLDimensionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the gLDimensionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/gl-dimensions/{id}")
    @Timed
    public ResponseEntity<GLDimensionDTO> getGLDimension(@PathVariable Integer id) {
        log.debug("REST request to get GLDimension : {}", id);
        GLDimensionDTO gLDimensionDTO = gLDimensionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(gLDimensionDTO));
    }

    /**
     * DELETE  /gl-dimensions/:id : delete the "id" gLDimension.
     *
     * @param id the id of the gLDimensionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/gl-dimensions/{id}")
    @Timed
    public ResponseEntity<Void> deleteGLDimension(@PathVariable Integer id) {
        log.debug("REST request to delete GLDimension : {}", id);
        gLDimensionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/gl-dimensions?query=:query : search for the gLDimension corresponding
     * to the query.
     *
     * @param query the query of the gLDimension search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/gl-dimensions")
    @Timed
    public ResponseEntity<List<GLDimensionDTO>> searchGLDimensions(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of GLDimensions for query {}", query);
        Page<GLDimensionDTO> page = gLDimensionService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/gl-dimensions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
