package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PartyDocumentService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PartyDocumentDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PartyDocument.
 */
@RestController
@RequestMapping("/api")
public class PartyDocumentResource {

    private final Logger log = LoggerFactory.getLogger(PartyDocumentResource.class);

    private static final String ENTITY_NAME = "partyDocument";

    private final PartyDocumentService partyDocumentService;

    public PartyDocumentResource(PartyDocumentService partyDocumentService) {
        this.partyDocumentService = partyDocumentService;
    }

    /**
     * POST  /party-documents : Create a new partyDocument.
     *
     * @param partyDocumentDTO the partyDocumentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new partyDocumentDTO, or with status 400 (Bad Request) if the partyDocument has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-documents")
    @Timed
    public ResponseEntity<PartyDocumentDTO> createPartyDocument(@RequestBody PartyDocumentDTO partyDocumentDTO) throws URISyntaxException {
        log.debug("REST request to save PartyDocument : {}", partyDocumentDTO);
        if (partyDocumentDTO.getIdDocument() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new partyDocument cannot already have an ID")).body(null);
        }
        PartyDocumentDTO result = partyDocumentService.save(partyDocumentDTO);
        return ResponseEntity.created(new URI("/api/party-documents/" + result.getIdDocument()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdDocument().toString()))
            .body(result);
    }

    /**
     * POST  /party-documents/execute{id}/{param} : Execute Bussiness Process partyDocument.
     *
     * @param partyDocumentDTO the partyDocumentDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  partyDocumentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-documents/execute/{id}/{param}")
    @Timed
    public ResponseEntity<PartyDocumentDTO> executedPartyDocument(@PathVariable Integer id, @PathVariable String param, @RequestBody PartyDocumentDTO partyDocumentDTO) throws URISyntaxException {
        log.debug("REST request to process PartyDocument : {}", partyDocumentDTO);
        PartyDocumentDTO result = partyDocumentService.processExecuteData(id, param, partyDocumentDTO);
        return new ResponseEntity<PartyDocumentDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /party-documents/execute-list{id}/{param} : Execute Bussiness Process partyDocument.
     *
     * @param partyDocumentDTO the partyDocumentDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  partyDocumentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-documents/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<PartyDocumentDTO>> executedListPartyDocument(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<PartyDocumentDTO> partyDocumentDTO) throws URISyntaxException {
        log.debug("REST request to process List PartyDocument");
        Set<PartyDocumentDTO> result = partyDocumentService.processExecuteListData(id, param, partyDocumentDTO);
        return new ResponseEntity<Set<PartyDocumentDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /party-documents : Updates an existing partyDocument.
     *
     * @param partyDocumentDTO the partyDocumentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated partyDocumentDTO,
     * or with status 400 (Bad Request) if the partyDocumentDTO is not valid,
     * or with status 500 (Internal Server Error) if the partyDocumentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/party-documents")
    @Timed
    public ResponseEntity<PartyDocumentDTO> updatePartyDocument(@RequestBody PartyDocumentDTO partyDocumentDTO) throws URISyntaxException {
        log.debug("REST request to update PartyDocument : {}", partyDocumentDTO);
        if (partyDocumentDTO.getIdDocument() == null) {
            return createPartyDocument(partyDocumentDTO);
        }
        PartyDocumentDTO result = partyDocumentService.save(partyDocumentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, partyDocumentDTO.getIdDocument().toString()))
            .body(result);
    }

    /**
     * GET  /party-documents : get all the partyDocuments.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of partyDocuments in body
     */
    @GetMapping("/party-documents")
    @Timed
    public ResponseEntity<List<PartyDocumentDTO>> getAllPartyDocuments(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PartyDocuments");
        Page<PartyDocumentDTO> page = partyDocumentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/party-documents");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @GetMapping("/party-documents/filterBy")
    @Timed
    public ResponseEntity<List<PartyDocumentDTO>> getAllFilteredPartyDocuments(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PartyDocuments");
        Page<PartyDocumentDTO> page = partyDocumentService.findByIdParty(UUID.fromString(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/party-documents/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping(path = "/party-documents/filterBy/email")
    @Timed
    public ResponseEntity<List<PartyDocumentDTO>> getAllFilteredPartyDocumentsEmail(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PartyDocuments");
        Page<PartyDocumentDTO> page = partyDocumentService.findByIdParty(UUID.fromString(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/party-documents/filterBy/email");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/party-documents/filterByDocType")
    @Timed
    public ResponseEntity<List<PartyDocumentDTO>> getAllFilteredPartyDocumentsByDocType(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of PartyDocuments");
        Page<PartyDocumentDTO> page = partyDocumentService.findByIdPartyAndIdDoctype(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/party-documents/filterByDocType");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * GET  /party-documents/:id : get the "id" partyDocument.
     *
     * @param id the id of the partyDocumentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the partyDocumentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/party-documents/{id}")
    @Timed
    public ResponseEntity<PartyDocumentDTO> getPartyDocument(@PathVariable UUID id) {
        log.debug("REST request to get PartyDocument : {}", id);
        PartyDocumentDTO partyDocumentDTO = partyDocumentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(partyDocumentDTO));
    }

    /**
     * DELETE  /party-documents/:id : delete the "id" partyDocument.
     *
     * @param id the id of the partyDocumentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/party-documents/{id}")
    @Timed
    public ResponseEntity<Void> deletePartyDocument(@PathVariable UUID id) {
        log.debug("REST request to delete PartyDocument : {}", id);
        partyDocumentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/party-documents?query=:query : search for the partyDocument corresponding
     * to the query.
     *
     * @param query the query of the partyDocument search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/party-documents")
    @Timed
    public ResponseEntity<List<PartyDocumentDTO>> searchPartyDocuments(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of PartyDocuments for query {}", query);
        Page<PartyDocumentDTO> page = partyDocumentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/party-documents");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
