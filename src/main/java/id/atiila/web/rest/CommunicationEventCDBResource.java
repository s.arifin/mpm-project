package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.CommunicationEventCDBService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.CommunicationEventCDBDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CommunicationEventCDB.
 */
@RestController
@RequestMapping("/api")
public class CommunicationEventCDBResource {

    private final Logger log = LoggerFactory.getLogger(CommunicationEventCDBResource.class);

    private static final String ENTITY_NAME = "communicationEventCDB";

    private final CommunicationEventCDBService communicationEventCDBService;

    public CommunicationEventCDBResource(CommunicationEventCDBService communicationEventCDBService) {
        this.communicationEventCDBService = communicationEventCDBService;
    }

    /**
     * POST  /communication-event-cdbs : Create a new communicationEventCDB.
     *
     * @param communicationEventCDBDTO the communicationEventCDBDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new communicationEventCDBDTO, or with status 400 (Bad Request) if the communicationEventCDB has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/communication-event-cdbs")
    @Timed
    public ResponseEntity<CommunicationEventCDBDTO> createCommunicationEventCDB(@RequestBody CommunicationEventCDBDTO communicationEventCDBDTO) throws URISyntaxException {
        log.debug("REST request to save CommunicationEventCDB : {}", communicationEventCDBDTO);
        if (communicationEventCDBDTO.getIdCommunicationEvent() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new communicationEventCDB cannot already have an ID")).body(null);
        }
        CommunicationEventCDBDTO result = communicationEventCDBService.save(communicationEventCDBDTO);
        return ResponseEntity.created(new URI("/api/communication-event-cdbs/" + result.getIdCommunicationEvent()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdCommunicationEvent().toString()))
            .body(result);
    }

    /**
     * POST  /communication-event-cdbs/execute{id}/{param} : Execute Bussiness Process communicationEventCDB.
     *
     * @param communicationEventCDBDTO the communicationEventCDBDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  communicationEventCDBDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/communication-event-cdbs/execute/{id}/{param}")
    @Timed
    public ResponseEntity<CommunicationEventCDBDTO> executedCommunicationEventCDB(@PathVariable Integer id, @PathVariable String param, @RequestBody CommunicationEventCDBDTO communicationEventCDBDTO) throws URISyntaxException {
        log.debug("REST request to process CommunicationEventCDB : {}", communicationEventCDBDTO);
        CommunicationEventCDBDTO result = communicationEventCDBService.processExecuteData(id, param, communicationEventCDBDTO);
        return new ResponseEntity<CommunicationEventCDBDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /communication-event-cdbs/execute-list{id}/{param} : Execute Bussiness Process communicationEventCDB.
     *
     * @param communicationEventCDBDTO the communicationEventCDBDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  communicationEventCDBDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/communication-event-cdbs/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<CommunicationEventCDBDTO>> executedListCommunicationEventCDB(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<CommunicationEventCDBDTO> communicationEventCDBDTO) throws URISyntaxException {
        log.debug("REST request to process List CommunicationEventCDB");
        Set<CommunicationEventCDBDTO> result = communicationEventCDBService.processExecuteListData(id, param, communicationEventCDBDTO);
        return new ResponseEntity<Set<CommunicationEventCDBDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /communication-event-cdbs : Updates an existing communicationEventCDB.
     *
     * @param communicationEventCDBDTO the communicationEventCDBDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated communicationEventCDBDTO,
     * or with status 400 (Bad Request) if the communicationEventCDBDTO is not valid,
     * or with status 500 (Internal Server Error) if the communicationEventCDBDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/communication-event-cdbs")
    @Timed
    public ResponseEntity<CommunicationEventCDBDTO> updateCommunicationEventCDB(@RequestBody CommunicationEventCDBDTO communicationEventCDBDTO) throws URISyntaxException {
        log.debug("REST request to update CommunicationEventCDB : {}", communicationEventCDBDTO);
        if (communicationEventCDBDTO.getIdCommunicationEvent() == null) {
            return createCommunicationEventCDB(communicationEventCDBDTO);
        }
        CommunicationEventCDBDTO result = communicationEventCDBService.save(communicationEventCDBDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, communicationEventCDBDTO.getIdCommunicationEvent().toString()))
            .body(result);
    }

    /**
     * GET  /communication-event-cdbs : get all the communicationEventCDBS.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of communicationEventCDBS in body
     */
    @GetMapping("/communication-event-cdbs")
    @Timed
    public ResponseEntity<List<CommunicationEventCDBDTO>> getAllCommunicationEventCDBS(
            @RequestParam(required = false) String idOrder,
            @ApiParam Pageable pageable) {
        log.debug("isi id order " + idOrder);
        log.debug("REST request to get a page of CommunicationEventCDBS ");
        Page<CommunicationEventCDBDTO> page = communicationEventCDBService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/communication-event-cdbs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/communication-event-cdbs/by-idcustomer")
    @Timed
    public ResponseEntity<List<CommunicationEventCDBDTO>> getAllCommunicationEventCDBSByIdCustomer(
        @RequestParam String idCustomer,
        @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of CommunicationEventCDBS" + idCustomer);
        Page<CommunicationEventCDBDTO> page = communicationEventCDBService.findAllByIdCustomer(pageable, idCustomer);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/communication-event-cdbs/by-idcustomer");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /communication-event-cdbs/:id : get the "id" communicationEventCDB.
     *
     * @param id the id of the communicationEventCDBDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the communicationEventCDBDTO, or with status 404 (Not Found)
     */
    @GetMapping("/communication-event-cdbs/{id}")
    @Timed
    public ResponseEntity<CommunicationEventCDBDTO> getCommunicationEventCDB(@PathVariable UUID id) {
        log.debug("REST request to get CommunicationEventCDB : {}", id);
        CommunicationEventCDBDTO communicationEventCDBDTO = communicationEventCDBService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(communicationEventCDBDTO));
    }

    /**
     * DELETE  /communication-event-cdbs/:id : delete the "id" communicationEventCDB.
     *
     * @param id the id of the communicationEventCDBDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/communication-event-cdbs/{id}")
    @Timed
    public ResponseEntity<Void> deleteCommunicationEventCDB(@PathVariable UUID id) {
        log.debug("REST request to delete CommunicationEventCDB : {}", id);
        communicationEventCDBService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/communication-event-cdbs?query=:query : search for the communicationEventCDB corresponding
     * to the query.
     *
     * @param query the query of the communicationEventCDB search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/communication-event-cdbs")
    @Timed
    public ResponseEntity<List<CommunicationEventCDBDTO>> searchCommunicationEventCDBS(
            @RequestParam String query,
            @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of CommunicationEventCDBS for query {}", query);
        //idOrder
        Page<CommunicationEventCDBDTO> page = communicationEventCDBService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/communication-event-cdbs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/communication-event-cdbs/set-status/{id}")
    @Timed
    public ResponseEntity< CommunicationEventCDBDTO> setStatusCommunicationEventCDB(@PathVariable Integer id, @RequestBody CommunicationEventCDBDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status CommunicationEventCDB : {}", dto);
        CommunicationEventCDBDTO r = communicationEventCDBService.changeCommunicationEventCDBStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
