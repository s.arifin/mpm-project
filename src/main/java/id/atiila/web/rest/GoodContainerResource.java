package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.GoodContainerService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.GoodContainerDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing GoodContainer.
 */
@RestController
@RequestMapping("/api")
public class GoodContainerResource {

    private final Logger log = LoggerFactory.getLogger(GoodContainerResource.class);

    private static final String ENTITY_NAME = "goodContainer";

    private final GoodContainerService goodContainerService;

    public GoodContainerResource(GoodContainerService goodContainerService) {
        this.goodContainerService = goodContainerService;
    }

    /**
     * POST  /good-containers : Create a new goodContainer.
     *
     * @param goodContainerDTO the goodContainerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new goodContainerDTO, or with status 400 (Bad Request) if the goodContainer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/good-containers")
    @Timed
    public ResponseEntity<GoodContainerDTO> createGoodContainer(@RequestBody GoodContainerDTO goodContainerDTO) throws URISyntaxException {
        log.debug("REST request to save GoodContainer : {}", goodContainerDTO);
        if (goodContainerDTO.getIdGoodContainer() != null) {
            throw new BadRequestAlertException("A new goodContainer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GoodContainerDTO result = goodContainerService.save(goodContainerDTO);
        return ResponseEntity.created(new URI("/api/good-containers/" + result.getIdGoodContainer()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdGoodContainer().toString()))
            .body(result);
    }

    /**
     * POST  /good-containers/process : Execute Bussiness Process goodContainer.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/good-containers/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processGoodContainer(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process GoodContainer ");
        Map<String, Object> result = goodContainerService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /good-containers/execute : Execute Bussiness Process goodContainer.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  goodContainerDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/good-containers/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedGoodContainer(HttpServletRequest request, @RequestBody GoodContainerDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process GoodContainer");
        Map<String, Object> result = goodContainerService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /good-containers/execute-list : Execute Bussiness Process goodContainer.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  goodContainerDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/good-containers/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListGoodContainer(HttpServletRequest request, @RequestBody Set<GoodContainerDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List GoodContainer");
        Map<String, Object> result = goodContainerService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /good-containers : Updates an existing goodContainer.
     *
     * @param goodContainerDTO the goodContainerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated goodContainerDTO,
     * or with status 400 (Bad Request) if the goodContainerDTO is not valid,
     * or with status 500 (Internal Server Error) if the goodContainerDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/good-containers")
    @Timed
    public ResponseEntity<GoodContainerDTO> updateGoodContainer(@RequestBody GoodContainerDTO goodContainerDTO) throws URISyntaxException {
        log.debug("REST request to update GoodContainer : {}", goodContainerDTO);
        if (goodContainerDTO.getIdGoodContainer() == null) {
            return createGoodContainer(goodContainerDTO);
        }
        GoodContainerDTO result = goodContainerService.save(goodContainerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, goodContainerDTO.getIdGoodContainer().toString()))
            .body(result);
    }

    /**
     * GET  /good-containers : get all the goodContainers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of goodContainers in body
     */
    @GetMapping("/good-containers")
    @Timed
    public ResponseEntity<List<GoodContainerDTO>> getAllGoodContainers(Pageable pageable) {
        log.debug("REST request to get a page of GoodContainers");
        Page<GoodContainerDTO> page = goodContainerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/good-containers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/good-containers/filterBy")
    @Timed
    public ResponseEntity<List<GoodContainerDTO>> getAllFilteredGoodContainers(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of GoodContainers");
        Page<GoodContainerDTO> page = goodContainerService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/good-containers/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /good-containers/:id : get the "id" goodContainer.
     *
     * @param id the id of the goodContainerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the goodContainerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/good-containers/{id}")
    @Timed
    public ResponseEntity<GoodContainerDTO> getGoodContainer(@PathVariable UUID id) {
        log.debug("REST request to get GoodContainer : {}", id);
        GoodContainerDTO goodContainerDTO = goodContainerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(goodContainerDTO));
    }

    /**
     * DELETE  /good-containers/:id : delete the "id" goodContainer.
     *
     * @param id the id of the goodContainerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/good-containers/{id}")
    @Timed
    public ResponseEntity<Void> deleteGoodContainer(@PathVariable UUID id) {
        log.debug("REST request to delete GoodContainer : {}", id);
        goodContainerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/good-containers?query=:query : search for the goodContainer corresponding
     * to the query.
     *
     * @param query the query of the goodContainer search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/good-containers")
    @Timed
    public ResponseEntity<List<GoodContainerDTO>> searchGoodContainers(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of GoodContainers for query {}", query);
        Page<GoodContainerDTO> page = goodContainerService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/good-containers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
