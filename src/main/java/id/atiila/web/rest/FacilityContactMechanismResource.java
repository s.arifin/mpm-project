package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.FacilityContactMechanismService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.FacilityContactMechanismDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing FacilityContactMechanism.
 */
@RestController
@RequestMapping("/api")
public class FacilityContactMechanismResource {

    private final Logger log = LoggerFactory.getLogger(FacilityContactMechanismResource.class);

    private static final String ENTITY_NAME = "facilityContactMechanism";

    private final FacilityContactMechanismService facilityContactMechanismService;

    public FacilityContactMechanismResource(FacilityContactMechanismService facilityContactMechanismService) {
        this.facilityContactMechanismService = facilityContactMechanismService;
    }

    /**
     * POST  /facility-contact-mechanisms : Create a new facilityContactMechanism.
     *
     * @param facilityContactMechanismDTO the facilityContactMechanismDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new facilityContactMechanismDTO, or with status 400 (Bad Request) if the facilityContactMechanism has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/facility-contact-mechanisms")
    @Timed
    public ResponseEntity<FacilityContactMechanismDTO> createFacilityContactMechanism(@RequestBody FacilityContactMechanismDTO facilityContactMechanismDTO) throws URISyntaxException {
        log.debug("REST request to save FacilityContactMechanism : {}", facilityContactMechanismDTO);
        if (facilityContactMechanismDTO.getIdContactMechPurpose() != null) {
            throw new BadRequestAlertException("A new facilityContactMechanism cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FacilityContactMechanismDTO result = facilityContactMechanismService.save(facilityContactMechanismDTO);
        return ResponseEntity.created(new URI("/api/facility-contact-mechanisms/" + result.getIdContactMechPurpose()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdContactMechPurpose().toString()))
            .body(result);
    }

    /**
     * POST  /facility-contact-mechanisms/process : Execute Bussiness Process facilityContactMechanism.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/facility-contact-mechanisms/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processFacilityContactMechanism(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process FacilityContactMechanism ");
        Map<String, Object> result = facilityContactMechanismService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /facility-contact-mechanisms/execute : Execute Bussiness Process facilityContactMechanism.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  facilityContactMechanismDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/facility-contact-mechanisms/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedFacilityContactMechanism(HttpServletRequest request, @RequestBody FacilityContactMechanismDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process FacilityContactMechanism");
        Map<String, Object> result = facilityContactMechanismService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /facility-contact-mechanisms/execute-list : Execute Bussiness Process facilityContactMechanism.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  facilityContactMechanismDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/facility-contact-mechanisms/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListFacilityContactMechanism(HttpServletRequest request, @RequestBody Set<FacilityContactMechanismDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List FacilityContactMechanism");
        Map<String, Object> result = facilityContactMechanismService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /facility-contact-mechanisms : Updates an existing facilityContactMechanism.
     *
     * @param facilityContactMechanismDTO the facilityContactMechanismDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated facilityContactMechanismDTO,
     * or with status 400 (Bad Request) if the facilityContactMechanismDTO is not valid,
     * or with status 500 (Internal Server Error) if the facilityContactMechanismDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/facility-contact-mechanisms")
    @Timed
    public ResponseEntity<FacilityContactMechanismDTO> updateFacilityContactMechanism(@RequestBody FacilityContactMechanismDTO facilityContactMechanismDTO) throws URISyntaxException {
        log.debug("REST request to update FacilityContactMechanism : {}", facilityContactMechanismDTO);
        if (facilityContactMechanismDTO.getIdContactMechPurpose() == null) {
            return createFacilityContactMechanism(facilityContactMechanismDTO);
        }
        FacilityContactMechanismDTO result = facilityContactMechanismService.save(facilityContactMechanismDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, facilityContactMechanismDTO.getIdContactMechPurpose().toString()))
            .body(result);
    }

    /**
     * GET  /facility-contact-mechanisms : get all the facilityContactMechanisms.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of facilityContactMechanisms in body
     */
    @GetMapping("/facility-contact-mechanisms")
    @Timed
    public ResponseEntity<List<FacilityContactMechanismDTO>> getAllFacilityContactMechanisms(Pageable pageable) {
        log.debug("REST request to get a page of FacilityContactMechanisms");
        Page<FacilityContactMechanismDTO> page = facilityContactMechanismService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/facility-contact-mechanisms");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/facility-contact-mechanisms/filterBy")
    @Timed
    public ResponseEntity<List<FacilityContactMechanismDTO>> getAllFilteredFacilityContactMechanisms(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of FacilityContactMechanisms");
        Page<FacilityContactMechanismDTO> page = facilityContactMechanismService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/facility-contact-mechanisms/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /facility-contact-mechanisms/:id : get the "id" facilityContactMechanism.
     *
     * @param id the id of the facilityContactMechanismDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the facilityContactMechanismDTO, or with status 404 (Not Found)
     */
    @GetMapping("/facility-contact-mechanisms/{id}")
    @Timed
    public ResponseEntity<FacilityContactMechanismDTO> getFacilityContactMechanism(@PathVariable UUID id) {
        log.debug("REST request to get FacilityContactMechanism : {}", id);
        FacilityContactMechanismDTO facilityContactMechanismDTO = facilityContactMechanismService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(facilityContactMechanismDTO));
    }

    /**
     * DELETE  /facility-contact-mechanisms/:id : delete the "id" facilityContactMechanism.
     *
     * @param id the id of the facilityContactMechanismDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/facility-contact-mechanisms/{id}")
    @Timed
    public ResponseEntity<Void> deleteFacilityContactMechanism(@PathVariable UUID id) {
        log.debug("REST request to delete FacilityContactMechanism : {}", id);
        facilityContactMechanismService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/facility-contact-mechanisms?query=:query : search for the facilityContactMechanism corresponding
     * to the query.
     *
     * @param query the query of the facilityContactMechanism search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/facility-contact-mechanisms")
    @Timed
    public ResponseEntity<List<FacilityContactMechanismDTO>> searchFacilityContactMechanisms(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of FacilityContactMechanisms for query {}", query);
        Page<FacilityContactMechanismDTO> page = facilityContactMechanismService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/facility-contact-mechanisms");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
