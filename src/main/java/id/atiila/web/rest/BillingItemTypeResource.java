package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.BillingItemTypeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.BillingItemTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BillingItemType.
 */
@RestController
@RequestMapping("/api")
public class BillingItemTypeResource {

    private final Logger log = LoggerFactory.getLogger(BillingItemTypeResource.class);

    private static final String ENTITY_NAME = "billingItemType";

    private final BillingItemTypeService billingItemTypeService;

    public BillingItemTypeResource(BillingItemTypeService billingItemTypeService) {
        this.billingItemTypeService = billingItemTypeService;
    }

    /**
     * POST  /billing-item-types : Create a new billingItemType.
     *
     * @param billingItemTypeDTO the billingItemTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new billingItemTypeDTO, or with status 400 (Bad Request) if the billingItemType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-item-types")
    @Timed
    public ResponseEntity<BillingItemTypeDTO> createBillingItemType(@RequestBody BillingItemTypeDTO billingItemTypeDTO) throws URISyntaxException {
        log.debug("REST request to save BillingItemType : {}", billingItemTypeDTO);
        if (billingItemTypeDTO.getIdItemType() != null) {
            throw new BadRequestAlertException("A new billingItemType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BillingItemTypeDTO result = billingItemTypeService.save(billingItemTypeDTO);
        return ResponseEntity.created(new URI("/api/billing-item-types/" + result.getIdItemType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdItemType().toString()))
            .body(result);
    }

    /**
     * POST  /billing-item-types/process : Execute Bussiness Process billingItemType.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-item-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processBillingItemType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process BillingItemType ");
        Map<String, Object> result = billingItemTypeService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /billing-item-types/execute : Execute Bussiness Process billingItemType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  billingItemTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-item-types/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedBillingItemType(HttpServletRequest request, @RequestBody BillingItemTypeDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process BillingItemType");
        Map<String, Object> result = billingItemTypeService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /billing-item-types/execute-list : Execute Bussiness Process billingItemType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  billingItemTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-item-types/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListBillingItemType(HttpServletRequest request, @RequestBody Set<BillingItemTypeDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List BillingItemType");
        Map<String, Object> result = billingItemTypeService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /billing-item-types : Updates an existing billingItemType.
     *
     * @param billingItemTypeDTO the billingItemTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated billingItemTypeDTO,
     * or with status 400 (Bad Request) if the billingItemTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the billingItemTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/billing-item-types")
    @Timed
    public ResponseEntity<BillingItemTypeDTO> updateBillingItemType(@RequestBody BillingItemTypeDTO billingItemTypeDTO) throws URISyntaxException {
        log.debug("REST request to update BillingItemType : {}", billingItemTypeDTO);
        if (billingItemTypeDTO.getIdItemType() == null) {
            return createBillingItemType(billingItemTypeDTO);
        }
        BillingItemTypeDTO result = billingItemTypeService.save(billingItemTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, billingItemTypeDTO.getIdItemType().toString()))
            .body(result);
    }

    /**
     * GET  /billing-item-types : get all the billingItemTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of billingItemTypes in body
     */
    @GetMapping("/billing-item-types")
    @Timed
    public ResponseEntity<List<BillingItemTypeDTO>> getAllBillingItemTypes(Pageable pageable) {
        log.debug("REST request to get a page of BillingItemTypes");
        Page<BillingItemTypeDTO> page = billingItemTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/billing-item-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/billing-item-types/filterBy")
    @Timed
    public ResponseEntity<List<BillingItemTypeDTO>> getAllFilteredBillingItemTypes(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of BillingItemTypes");
        Page<BillingItemTypeDTO> page = billingItemTypeService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/billing-item-types/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /billing-item-types/:id : get the "id" billingItemType.
     *
     * @param id the id of the billingItemTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the billingItemTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/billing-item-types/{id}")
    @Timed
    public ResponseEntity<BillingItemTypeDTO> getBillingItemType(@PathVariable Integer id) {
        log.debug("REST request to get BillingItemType : {}", id);
        BillingItemTypeDTO billingItemTypeDTO = billingItemTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(billingItemTypeDTO));
    }

    /**
     * DELETE  /billing-item-types/:id : delete the "id" billingItemType.
     *
     * @param id the id of the billingItemTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/billing-item-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteBillingItemType(@PathVariable Integer id) {
        log.debug("REST request to delete BillingItemType : {}", id);
        billingItemTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/billing-item-types?query=:query : search for the billingItemType corresponding
     * to the query.
     *
     * @param query the query of the billingItemType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/billing-item-types")
    @Timed
    public ResponseEntity<List<BillingItemTypeDTO>> searchBillingItemTypes(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of BillingItemTypes for query {}", query);
        Page<BillingItemTypeDTO> page = billingItemTypeService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/billing-item-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
