package id.atiila.web.rest;

import java.util.Map;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.SuspectService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.SuspectDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Suspect.
 */
@RestController
@RequestMapping("/api")
public class SuspectResource {

    private final Logger log = LoggerFactory.getLogger(SuspectResource.class);

    private static final String ENTITY_NAME = "suspect";

    private final SuspectService suspectService;

    public SuspectResource(SuspectService suspectService) {
        this.suspectService = suspectService;
    }

    /**
     * POST  /suspects : Create a new suspect.
     *
     * @param suspectDTO the suspectDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new suspectDTO, or with status 400 (Bad Request) if the suspect has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/suspects")
    @Timed
    public ResponseEntity<SuspectDTO> createSuspect(@RequestBody SuspectDTO suspectDTO) throws URISyntaxException {
        log.debug("REST request to save Suspect : {}", suspectDTO);
        if (suspectDTO.getIdSuspect() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new suspect cannot already have an ID")).body(null);
        }
        SuspectDTO result = suspectService.save(suspectDTO);

        return ResponseEntity.created(new URI("/api/suspects/" + result.getIdSuspect()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdSuspect().toString()))
            .body(result);
    }

    /**
     * POST  /suspects/execute : Execute Bussiness Process suspect.
     *
     * @param suspectDTO the suspectDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  suspectDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/suspects/execute")
    @Timed
    public ResponseEntity<SuspectDTO> executedSuspect(@RequestBody SuspectDTO suspectDTO) throws URISyntaxException {
        log.debug("REST request to process Suspect : {}", suspectDTO);
        return new ResponseEntity<SuspectDTO>(suspectDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /suspects : Updates an existing suspect.
     *
     * @param suspectDTO the suspectDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated suspectDTO,
     * or with status 400 (Bad Request) if the suspectDTO is not valid,
     * or with status 500 (Internal Server Error) if the suspectDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/suspects")
    @Timed
    public ResponseEntity<SuspectDTO> updateSuspect(@RequestBody SuspectDTO suspectDTO) throws URISyntaxException {
        log.debug("REST request to update Suspect : {}", suspectDTO);
        if (suspectDTO.getIdSuspect() == null) {
            return createSuspect(suspectDTO);
        }
        SuspectDTO result = suspectService.save(suspectDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, suspectDTO.getIdSuspect().toString()))
            .body(result);
    }

    /**
     * GET  /suspects : get all the suspects.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of suspects in body
     */
    @GetMapping("/suspects")
    @Timed
    public ResponseEntity<List<SuspectDTO>> getAllSuspects(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Suspects");
        Page<SuspectDTO> page = suspectService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/suspects");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /suspects/:id : get the "id" suspect.
     *
     * @param id the id of the suspectDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the suspectDTO, or with status 404 (Not Found)
     */
    @GetMapping("/suspects/{id}")
    @Timed
    public ResponseEntity<SuspectDTO> getSuspect(@PathVariable UUID id) {
        log.debug("REST request to get Suspect : {}", id);
        SuspectDTO suspectDTO = suspectService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(suspectDTO));
    }

    /**
     * DELETE  /suspects/:id : delete the "id" suspect.
     *
     * @param id the id of the suspectDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/suspects/{id}")
    @Timed
    public ResponseEntity<Void> deleteSuspect(@PathVariable UUID id) {
        log.debug("REST request to delete Suspect : {}", id);
        suspectService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/suspects?query=:query : search for the suspect corresponding
     * to the query.
     *
     * @param query the query of the suspect search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/suspects")
    @Timed
    public ResponseEntity<List<SuspectDTO>> searchSuspects(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Suspects for query {}", query);
        Page<SuspectDTO> page = suspectService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/suspects");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/suspects/set-status/{id}")
    @Timed
    public ResponseEntity< SuspectDTO> setStatusSuspect(@PathVariable Integer id, @RequestBody SuspectDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status Suspect : {}", dto);
        SuspectDTO r = suspectService.changeSuspectStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    @PostMapping("/suspects/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processSuspect(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process Suspect ");
        Map<String, Object> result = suspectService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
