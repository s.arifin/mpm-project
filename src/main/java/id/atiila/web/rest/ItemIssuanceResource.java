package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ItemIssuanceService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ItemIssuanceDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ItemIssuance.
 */
@RestController
@RequestMapping("/api")
public class ItemIssuanceResource {

    private final Logger log = LoggerFactory.getLogger(ItemIssuanceResource.class);

    private static final String ENTITY_NAME = "itemIssuance";

    private final ItemIssuanceService itemIssuanceService;

    public ItemIssuanceResource(ItemIssuanceService itemIssuanceService) {
        this.itemIssuanceService = itemIssuanceService;
    }

    /**
     * POST  /item-issuances : Create a new itemIssuance.
     *
     * @param itemIssuanceDTO the itemIssuanceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new itemIssuanceDTO, or with status 400 (Bad Request) if the itemIssuance has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/item-issuances")
    @Timed
    public ResponseEntity<ItemIssuanceDTO> createItemIssuance(@RequestBody ItemIssuanceDTO itemIssuanceDTO) throws URISyntaxException {
        log.debug("REST request to save ItemIssuance : {}", itemIssuanceDTO);
        if (itemIssuanceDTO.getIdItemIssuance() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new itemIssuance cannot already have an ID")).body(null);
        }
        ItemIssuanceDTO result = itemIssuanceService.save(itemIssuanceDTO);
        return ResponseEntity.created(new URI("/api/item-issuances/" + result.getIdItemIssuance()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdItemIssuance().toString()))
            .body(result);
    }

    /**
     * POST  /item-issuances/execute{id}/{param} : Execute Bussiness Process itemIssuance.
     *
     * @param itemIssuanceDTO the itemIssuanceDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  itemIssuanceDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/item-issuances/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ItemIssuanceDTO> executedItemIssuance(@PathVariable Integer id, @PathVariable String param, @RequestBody ItemIssuanceDTO itemIssuanceDTO) throws URISyntaxException {
        log.debug("REST request to process ItemIssuance : {}", itemIssuanceDTO);
        ItemIssuanceDTO result = itemIssuanceService.processExecuteData(id, param, itemIssuanceDTO);
        return new ResponseEntity<ItemIssuanceDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /item-issuances/execute-list{id}/{param} : Execute Bussiness Process itemIssuance.
     *
     * @param itemIssuanceDTO the itemIssuanceDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  itemIssuanceDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/item-issuances/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ItemIssuanceDTO>> executedListItemIssuance(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ItemIssuanceDTO> itemIssuanceDTO) throws URISyntaxException {
        log.debug("REST request to process List ItemIssuance");
        Set<ItemIssuanceDTO> result = itemIssuanceService.processExecuteListData(id, param, itemIssuanceDTO);
        return new ResponseEntity<Set<ItemIssuanceDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /item-issuances : Updates an existing itemIssuance.
     *
     * @param itemIssuanceDTO the itemIssuanceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated itemIssuanceDTO,
     * or with status 400 (Bad Request) if the itemIssuanceDTO is not valid,
     * or with status 500 (Internal Server Error) if the itemIssuanceDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/item-issuances")
    @Timed
    public ResponseEntity<ItemIssuanceDTO> updateItemIssuance(@RequestBody ItemIssuanceDTO itemIssuanceDTO) throws URISyntaxException {
        log.debug("REST request to update ItemIssuance : {}", itemIssuanceDTO);
        if (itemIssuanceDTO.getIdItemIssuance() == null) {
            return createItemIssuance(itemIssuanceDTO);
        }
        ItemIssuanceDTO result = itemIssuanceService.save(itemIssuanceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, itemIssuanceDTO.getIdItemIssuance().toString()))
            .body(result);
    }

    /**
     * GET  /item-issuances : get all the itemIssuances.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of itemIssuances in body
     */
    @GetMapping("/item-issuances")
    @Timed
    public ResponseEntity<List<ItemIssuanceDTO>> getAllItemIssuances(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ItemIssuances");
        Page<ItemIssuanceDTO> page = itemIssuanceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/item-issuances");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /item-issuances/:id : get the "id" itemIssuance.
     *
     * @param id the id of the itemIssuanceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the itemIssuanceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/item-issuances/{id}")
    @Timed
    public ResponseEntity<ItemIssuanceDTO> getItemIssuance(@PathVariable UUID id) {
        log.debug("REST request to get ItemIssuance : {}", id);
        ItemIssuanceDTO itemIssuanceDTO = itemIssuanceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(itemIssuanceDTO));
    }

    /**
     * DELETE  /item-issuances/:id : delete the "id" itemIssuance.
     *
     * @param id the id of the itemIssuanceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/item-issuances/{id}")
    @Timed
    public ResponseEntity<Void> deleteItemIssuance(@PathVariable UUID id) {
        log.debug("REST request to delete ItemIssuance : {}", id);
        itemIssuanceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/item-issuances?query=:query : search for the itemIssuance corresponding
     * to the query.
     *
     * @param query the query of the itemIssuance search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/item-issuances")
    @Timed
    public ResponseEntity<List<ItemIssuanceDTO>> searchItemIssuances(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ItemIssuances for query {}", query);
        Page<ItemIssuanceDTO> page = itemIssuanceService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/item-issuances");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
