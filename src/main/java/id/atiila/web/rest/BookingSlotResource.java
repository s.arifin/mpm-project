package id.atiila.web.rest;

import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.BookingSlotService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.BookingSlotDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BookingSlot.
 */
@RestController
@RequestMapping("/api")
public class BookingSlotResource {

    private final Logger log = LoggerFactory.getLogger(BookingSlotResource.class);

    private static final String ENTITY_NAME = "bookingSlot";

    private final BookingSlotService bookingSlotService;

    public BookingSlotResource(BookingSlotService bookingSlotService) {
        this.bookingSlotService = bookingSlotService;
    }

    /**
     * POST  /booking-slots : Create a new bookingSlot.
     *
     * @param bookingSlotDTO the bookingSlotDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bookingSlotDTO, or with status 400 (Bad Request) if the bookingSlot has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/booking-slots")
    @Timed
    public ResponseEntity<BookingSlotDTO> createBookingSlot(@RequestBody BookingSlotDTO bookingSlotDTO) throws URISyntaxException {
        log.debug("REST request to save BookingSlot : {}", bookingSlotDTO);
        if (bookingSlotDTO.getIdBookSlot() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new bookingSlot cannot already have an ID")).body(null);
        }
        BookingSlotDTO result = bookingSlotService.save(bookingSlotDTO);
        
        return ResponseEntity.created(new URI("/api/booking-slots/" + result.getIdBookSlot()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdBookSlot().toString()))
            .body(result);
    }

    /**
     * POST  /booking-slots/execute : Execute Bussiness Process bookingSlot.
     *
     * @param bookingSlotDTO the bookingSlotDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  bookingSlotDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/booking-slots/execute/{id}")
    @Timed
    public ResponseEntity<BookingSlotDTO> executedBookingSlot(@PathVariable Integer id, @RequestBody BookingSlotDTO bookingSlotDTO) throws URISyntaxException {
        log.debug("REST request to process BookingSlot : {}", bookingSlotDTO);
        BookingSlotDTO r = bookingSlotService.processExecuteData(bookingSlotDTO, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    /**
     * PUT  /booking-slots : Updates an existing bookingSlot.
     *
     * @param bookingSlotDTO the bookingSlotDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bookingSlotDTO,
     * or with status 400 (Bad Request) if the bookingSlotDTO is not valid,
     * or with status 500 (Internal Server Error) if the bookingSlotDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/booking-slots")
    @Timed
    public ResponseEntity<BookingSlotDTO> updateBookingSlot(@RequestBody BookingSlotDTO bookingSlotDTO) throws URISyntaxException {
        log.debug("REST request to update BookingSlot : {}", bookingSlotDTO);
        if (bookingSlotDTO.getIdBookSlot() == null) {
            return createBookingSlot(bookingSlotDTO);
        }
        BookingSlotDTO result = bookingSlotService.save(bookingSlotDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bookingSlotDTO.getIdBookSlot().toString()))
            .body(result);
    }

    /**
     * GET  /booking-slots : get all the bookingSlots.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of bookingSlots in body
     */
    @GetMapping("/booking-slots")
    @Timed
    public ResponseEntity<List<BookingSlotDTO>> getAllBookingSlots(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of BookingSlots");
        Page<BookingSlotDTO> page = bookingSlotService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/booking-slots");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /booking-slots/:id : get the "id" bookingSlot.
     *
     * @param id the id of the bookingSlotDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bookingSlotDTO, or with status 404 (Not Found)
     */
    @GetMapping("/booking-slots/{id}")
    @Timed
    public ResponseEntity<BookingSlotDTO> getBookingSlot(@PathVariable UUID id) {
        log.debug("REST request to get BookingSlot : {}", id);
        BookingSlotDTO bookingSlotDTO = bookingSlotService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(bookingSlotDTO));
    }

    /**
     * DELETE  /booking-slots/:id : delete the "id" bookingSlot.
     *
     * @param id the id of the bookingSlotDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/booking-slots/{id}")
    @Timed
    public ResponseEntity<Void> deleteBookingSlot(@PathVariable UUID id) {
        log.debug("REST request to delete BookingSlot : {}", id);
        bookingSlotService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/booking-slots?query=:query : search for the bookingSlot corresponding
     * to the query.
     *
     * @param query the query of the bookingSlot search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/booking-slots")
    @Timed
    public ResponseEntity<List<BookingSlotDTO>> searchBookingSlots(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of BookingSlots for query {}", query);
        Page<BookingSlotDTO> page = bookingSlotService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/booking-slots");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

	
}
