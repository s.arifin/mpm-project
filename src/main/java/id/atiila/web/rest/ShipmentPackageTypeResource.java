package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ShipmentPackageTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ShipmentPackageTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ShipmentPackageType.
 */
@RestController
@RequestMapping("/api")
public class ShipmentPackageTypeResource {

    private final Logger log = LoggerFactory.getLogger(ShipmentPackageTypeResource.class);

    private static final String ENTITY_NAME = "shipmentPackageType";

    private final ShipmentPackageTypeService shipmentPackageTypeService;

    public ShipmentPackageTypeResource(ShipmentPackageTypeService shipmentPackageTypeService) {
        this.shipmentPackageTypeService = shipmentPackageTypeService;
    }

    /**
     * POST  /shipment-package-types : Create a new shipmentPackageType.
     *
     * @param shipmentPackageTypeDTO the shipmentPackageTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shipmentPackageTypeDTO, or with status 400 (Bad Request) if the shipmentPackageType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-package-types")
    @Timed
    public ResponseEntity<ShipmentPackageTypeDTO> createShipmentPackageType(@RequestBody ShipmentPackageTypeDTO shipmentPackageTypeDTO) throws URISyntaxException {
        log.debug("REST request to save ShipmentPackageType : {}", shipmentPackageTypeDTO);
        if (shipmentPackageTypeDTO.getIdPackageType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new shipmentPackageType cannot already have an ID")).body(null);
        }
        ShipmentPackageTypeDTO result = shipmentPackageTypeService.save(shipmentPackageTypeDTO);
        return ResponseEntity.created(new URI("/api/shipment-package-types/" + result.getIdPackageType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPackageType().toString()))
            .body(result);
    }

    /**
     * POST  /shipment-package-types/execute{id}/{param} : Execute Bussiness Process shipmentPackageType.
     *
     * @param shipmentPackageTypeDTO the shipmentPackageTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  shipmentPackageTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-package-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ShipmentPackageTypeDTO> executedShipmentPackageType(@PathVariable Integer id, @PathVariable String param, @RequestBody ShipmentPackageTypeDTO shipmentPackageTypeDTO) throws URISyntaxException {
        log.debug("REST request to process ShipmentPackageType : {}", shipmentPackageTypeDTO);
        ShipmentPackageTypeDTO result = shipmentPackageTypeService.processExecuteData(id, param, shipmentPackageTypeDTO);
        return new ResponseEntity<ShipmentPackageTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /shipment-package-types/execute-list{id}/{param} : Execute Bussiness Process shipmentPackageType.
     *
     * @param shipmentPackageTypeDTO the shipmentPackageTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  shipmentPackageTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shipment-package-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ShipmentPackageTypeDTO>> executedListShipmentPackageType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ShipmentPackageTypeDTO> shipmentPackageTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List ShipmentPackageType");
        Set<ShipmentPackageTypeDTO> result = shipmentPackageTypeService.processExecuteListData(id, param, shipmentPackageTypeDTO);
        return new ResponseEntity<Set<ShipmentPackageTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /shipment-package-types : Updates an existing shipmentPackageType.
     *
     * @param shipmentPackageTypeDTO the shipmentPackageTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shipmentPackageTypeDTO,
     * or with status 400 (Bad Request) if the shipmentPackageTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the shipmentPackageTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/shipment-package-types")
    @Timed
    public ResponseEntity<ShipmentPackageTypeDTO> updateShipmentPackageType(@RequestBody ShipmentPackageTypeDTO shipmentPackageTypeDTO) throws URISyntaxException {
        log.debug("REST request to update ShipmentPackageType : {}", shipmentPackageTypeDTO);
        if (shipmentPackageTypeDTO.getIdPackageType() == null) {
            return createShipmentPackageType(shipmentPackageTypeDTO);
        }
        ShipmentPackageTypeDTO result = shipmentPackageTypeService.save(shipmentPackageTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shipmentPackageTypeDTO.getIdPackageType().toString()))
            .body(result);
    }

    /**
     * GET  /shipment-package-types : get all the shipmentPackageTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shipmentPackageTypes in body
     */
    @GetMapping("/shipment-package-types")
    @Timed
    public ResponseEntity<List<ShipmentPackageTypeDTO>> getAllShipmentPackageTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ShipmentPackageTypes");
        Page<ShipmentPackageTypeDTO> page = shipmentPackageTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-package-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /shipment-package-types/:id : get the "id" shipmentPackageType.
     *
     * @param id the id of the shipmentPackageTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shipmentPackageTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/shipment-package-types/{id}")
    @Timed
    public ResponseEntity<ShipmentPackageTypeDTO> getShipmentPackageType(@PathVariable Integer id) {
        log.debug("REST request to get ShipmentPackageType : {}", id);
        ShipmentPackageTypeDTO shipmentPackageTypeDTO = shipmentPackageTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(shipmentPackageTypeDTO));
    }

    /**
     * DELETE  /shipment-package-types/:id : delete the "id" shipmentPackageType.
     *
     * @param id the id of the shipmentPackageTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shipment-package-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteShipmentPackageType(@PathVariable Integer id) {
        log.debug("REST request to delete ShipmentPackageType : {}", id);
        shipmentPackageTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/shipment-package-types?query=:query : search for the shipmentPackageType corresponding
     * to the query.
     *
     * @param query the query of the shipmentPackageType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/shipment-package-types")
    @Timed
    public ResponseEntity<List<ShipmentPackageTypeDTO>> searchShipmentPackageTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ShipmentPackageTypes for query {}", query);
        Page<ShipmentPackageTypeDTO> page = shipmentPackageTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/shipment-package-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
