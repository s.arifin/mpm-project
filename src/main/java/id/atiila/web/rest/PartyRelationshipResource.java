package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PartyRelationshipService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PartyRelationshipDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PartyRelationship.
 */
@RestController
@RequestMapping("/api")
public class PartyRelationshipResource {

    private final Logger log = LoggerFactory.getLogger(PartyRelationshipResource.class);

    private static final String ENTITY_NAME = "partyRelationship";

    private final PartyRelationshipService partyRelationshipService;

    public PartyRelationshipResource(PartyRelationshipService partyRelationshipService) {
        this.partyRelationshipService = partyRelationshipService;
    }

    /**
     * POST  /party-relationships : Create a new partyRelationship.
     *
     * @param partyRelationshipDTO the partyRelationshipDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new partyRelationshipDTO, or with status 400 (Bad Request) if the partyRelationship has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-relationships")
    @Timed
    public ResponseEntity<PartyRelationshipDTO> createPartyRelationship(@RequestBody PartyRelationshipDTO partyRelationshipDTO) throws URISyntaxException {
        log.debug("REST request to save PartyRelationship : {}", partyRelationshipDTO);
        if (partyRelationshipDTO.getIdPartyRelationship() != null) {
            throw new BadRequestAlertException("A new partyRelationship cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PartyRelationshipDTO result = partyRelationshipService.save(partyRelationshipDTO);
        return ResponseEntity.created(new URI("/api/party-relationships/" + result.getIdPartyRelationship()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPartyRelationship().toString()))
            .body(result);
    }

    /**
     * POST  /party-relationships/process : Execute Bussiness Process partyRelationship.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-relationships/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processPartyRelationship(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PartyRelationship ");
        Map<String, Object> result = partyRelationshipService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /party-relationships/execute : Execute Bussiness Process partyRelationship.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  partyRelationshipDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-relationships/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedPartyRelationship(HttpServletRequest request, @RequestBody PartyRelationshipDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PartyRelationship");
        Map<String, Object> result = partyRelationshipService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /party-relationships/execute-list : Execute Bussiness Process partyRelationship.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  partyRelationshipDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-relationships/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListPartyRelationship(HttpServletRequest request, @RequestBody Set<PartyRelationshipDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List PartyRelationship");
        Map<String, Object> result = partyRelationshipService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /party-relationships : Updates an existing partyRelationship.
     *
     * @param partyRelationshipDTO the partyRelationshipDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated partyRelationshipDTO,
     * or with status 400 (Bad Request) if the partyRelationshipDTO is not valid,
     * or with status 500 (Internal Server Error) if the partyRelationshipDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/party-relationships")
    @Timed
    public ResponseEntity<PartyRelationshipDTO> updatePartyRelationship(@RequestBody PartyRelationshipDTO partyRelationshipDTO) throws URISyntaxException {
        log.debug("REST request to update PartyRelationship : {}", partyRelationshipDTO);
        if (partyRelationshipDTO.getIdPartyRelationship() == null) {
            return createPartyRelationship(partyRelationshipDTO);
        }
        PartyRelationshipDTO result = partyRelationshipService.save(partyRelationshipDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, partyRelationshipDTO.getIdPartyRelationship().toString()))
            .body(result);
    }

    /**
     * GET  /party-relationships : get all the partyRelationships.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of partyRelationships in body
     */
    @GetMapping("/party-relationships")
    @Timed
    public ResponseEntity<List<PartyRelationshipDTO>> getAllPartyRelationships(Pageable pageable) {
        log.debug("REST request to get a page of PartyRelationships");
        Page<PartyRelationshipDTO> page = partyRelationshipService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/party-relationships");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/party-relationships/filterBy")
    @Timed
    public ResponseEntity<List<PartyRelationshipDTO>> getAllFilteredPartyRelationships(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of PartyRelationships");
        Page<PartyRelationshipDTO> page = partyRelationshipService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/party-relationships/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /party-relationships/:id : get the "id" partyRelationship.
     *
     * @param id the id of the partyRelationshipDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the partyRelationshipDTO, or with status 404 (Not Found)
     */
    @GetMapping("/party-relationships/{id}")
    @Timed
    public ResponseEntity<PartyRelationshipDTO> getPartyRelationship(@PathVariable UUID id) {
        log.debug("REST request to get PartyRelationship : {}", id);
        PartyRelationshipDTO partyRelationshipDTO = partyRelationshipService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(partyRelationshipDTO));
    }

    /**
     * DELETE  /party-relationships/:id : delete the "id" partyRelationship.
     *
     * @param id the id of the partyRelationshipDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/party-relationships/{id}")
    @Timed
    public ResponseEntity<Void> deletePartyRelationship(@PathVariable UUID id) {
        log.debug("REST request to delete PartyRelationship : {}", id);
        partyRelationshipService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/party-relationships?query=:query : search for the partyRelationship corresponding
     * to the query.
     *
     * @param query the query of the partyRelationship search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/party-relationships")
    @Timed
    public ResponseEntity<List<PartyRelationshipDTO>> searchPartyRelationships(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of PartyRelationships for query {}", query);
        Page<PartyRelationshipDTO> page = partyRelationshipService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/party-relationships");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
