package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PersonalCustomerService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PersonalCustomerDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PersonalCustomer.
 */
@RestController
@RequestMapping("/api")
public class PersonalCustomerResource {

    private final Logger log = LoggerFactory.getLogger(PersonalCustomerResource.class);

    private static final String ENTITY_NAME = "personalCustomer";

    private final PersonalCustomerService personalCustomerService;

    public PersonalCustomerResource(PersonalCustomerService personalCustomerService) {
        this.personalCustomerService = personalCustomerService;
    }

    /**
     * POST  /personal-customers : Create a new personalCustomer.
     *
     * @param personalCustomerDTO the personalCustomerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new personalCustomerDTO, or with status 400 (Bad Request) if the personalCustomer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/personal-customers")
    @Timed
    public ResponseEntity<PersonalCustomerDTO> createPersonalCustomer(@RequestBody PersonalCustomerDTO personalCustomerDTO) throws URISyntaxException {
        log.debug("REST request to save PersonalCustomer : {}", personalCustomerDTO);
        if (personalCustomerDTO.getIdCustomer() != null) {
            throw new BadRequestAlertException("A new personalCustomer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PersonalCustomerDTO result = personalCustomerService.save(personalCustomerDTO);
        return ResponseEntity.created(new URI("/api/personal-customers/" + result.getIdCustomer()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdCustomer().toString()))
            .body(result);
    }

    /**
     * POST  /personal-customers/execute{id}/{param} : Execute Bussiness Process personalCustomer.
     *
     * @param personalCustomerDTO the personalCustomerDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  personalCustomerDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/personal-customers/execute/{id}/{param}")
    @Timed
    public ResponseEntity<PersonalCustomerDTO> executedPersonalCustomer(@PathVariable Integer id, @PathVariable String param, @RequestBody PersonalCustomerDTO personalCustomerDTO) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to process PersonalCustomer : {}", personalCustomerDTO);
        PersonalCustomerDTO result = personalCustomerService.processExecuteData(id, param, personalCustomerDTO);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<PersonalCustomerDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /personal-customers/execute-list{id}/{param} : Execute Bussiness Process personalCustomer.
     *
     * @param personalCustomerDTO the personalCustomerDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  personalCustomerDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/personal-customers/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<PersonalCustomerDTO>> executedListPersonalCustomer(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<PersonalCustomerDTO> personalCustomerDTO) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List PersonalCustomer");
        Set<PersonalCustomerDTO> result = personalCustomerService.processExecuteListData(id, param, personalCustomerDTO);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<PersonalCustomerDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /personal-customers : Updates an existing personalCustomer.
     *
     * @param personalCustomerDTO the personalCustomerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated personalCustomerDTO,
     * or with status 400 (Bad Request) if the personalCustomerDTO is not valid,
     * or with status 500 (Internal Server Error) if the personalCustomerDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/personal-customers")
    @Timed
    public ResponseEntity<PersonalCustomerDTO> updatePersonalCustomer(@RequestBody PersonalCustomerDTO personalCustomerDTO) throws URISyntaxException {
        log.debug("REST request to update PersonalCustomer : {}", personalCustomerDTO);
        if (personalCustomerDTO.getIdCustomer() == null) {
            return createPersonalCustomer(personalCustomerDTO);
        }
        PersonalCustomerDTO result = personalCustomerService.save(personalCustomerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, personalCustomerDTO.getIdCustomer().toString()))
            .body(result);
    }

    /**
     * GET  /personal-customers : get all the personalCustomers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of personalCustomers in body
     */
    @GetMapping("/personal-customers")
    @Timed
    public ResponseEntity<List<PersonalCustomerDTO>> getAllPersonalCustomers(Pageable pageable) {
        log.debug("REST request to get a page of PersonalCustomers");
        Page<PersonalCustomerDTO> page = personalCustomerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/personal-customers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/personal-customers/byPartyId/{id}")
    @Timed
    public ResponseEntity<PersonalCustomerDTO> getPersonalCustomerbyPartyId(@PathVariable String id) {
        log.debug("REST request to get PersonalCustomer : {}", id);
        UUID idParty = UUID.fromString(id);
        PersonalCustomerDTO personalCustomerDTO = personalCustomerService.findOnebypartyid(idParty);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(personalCustomerDTO));
    }



    /**
     * GET  /personal-customers/:id : get the "id" personalCustomer.
     *
     * @param id the id of the personalCustomerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the personalCustomerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/personal-customers/{id}")
    @Timed
    public ResponseEntity<PersonalCustomerDTO> getPersonalCustomer(@PathVariable String id) {
        log.debug("REST request to get PersonalCustomer : {}", id);
        PersonalCustomerDTO personalCustomerDTO = personalCustomerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(personalCustomerDTO));
    }

    /**
     * DELETE  /personal-customers/:id : delete the "id" personalCustomer.
     *
     * @param id the id of the personalCustomerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/personal-customers/{id}")
    @Timed
    public ResponseEntity<Void> deletePersonalCustomer(@PathVariable String id) {
        log.debug("REST request to delete PersonalCustomer : {}", id);
        personalCustomerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/personal-customers?query=:query : search for the personalCustomer corresponding
     * to the query.
     *
     * @param query the query of the personalCustomer search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/personal-customers")
    @Timed
    public ResponseEntity<List<PersonalCustomerDTO>> searchPersonalCustomers(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of PersonalCustomers for query {}", query);
        Page<PersonalCustomerDTO> page = personalCustomerService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/personal-customers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/personal-customers/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processPersonalCustomer(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process PersonalCustomer ");
        Map<String, Object> result = personalCustomerService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    @GetMapping(path = "/personal-customers/email")
    @Timed
    public ResponseEntity<PersonalCustomerDTO> getPersonalCustomerEmail(HttpServletRequest request) {
        log.debug("REST request to get PersonalCustomer : {}", request);
        PersonalCustomerDTO personalCustomerDTO = personalCustomerService.findOneEmail(request);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(personalCustomerDTO));
    }

}
