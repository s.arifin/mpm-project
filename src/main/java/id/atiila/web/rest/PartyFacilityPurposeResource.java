package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PartyFacilityPurposeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PartyFacilityPurposeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PartyFacilityPurpose.
 */
@RestController
@RequestMapping("/api")
public class PartyFacilityPurposeResource {

    private final Logger log = LoggerFactory.getLogger(PartyFacilityPurposeResource.class);

    private static final String ENTITY_NAME = "partyFacilityPurpose";

    private final PartyFacilityPurposeService partyFacilityPurposeService;

    public PartyFacilityPurposeResource(PartyFacilityPurposeService partyFacilityPurposeService) {
        this.partyFacilityPurposeService = partyFacilityPurposeService;
    }

    /**
     * POST  /party-facility-purposes : Create a new partyFacilityPurpose.
     *
     * @param partyFacilityPurposeDTO the partyFacilityPurposeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new partyFacilityPurposeDTO, or with status 400 (Bad Request) if the partyFacilityPurpose has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-facility-purposes")
    @Timed
    public ResponseEntity<PartyFacilityPurposeDTO> createPartyFacilityPurpose(@RequestBody PartyFacilityPurposeDTO partyFacilityPurposeDTO) throws URISyntaxException {
        log.debug("REST request to save PartyFacilityPurpose : {}", partyFacilityPurposeDTO);
        if (partyFacilityPurposeDTO.getIdPartyFacility() != null) {
            throw new BadRequestAlertException("A new partyFacilityPurpose cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PartyFacilityPurposeDTO result = partyFacilityPurposeService.save(partyFacilityPurposeDTO);
        return ResponseEntity.created(new URI("/api/party-facility-purposes/" + result.getIdPartyFacility()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPartyFacility().toString()))
            .body(result);
    }

    /**
     * POST  /party-facility-purposes/process : Execute Bussiness Process partyFacilityPurpose.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-facility-purposes/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processPartyFacilityPurpose(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PartyFacilityPurpose ");
        Map<String, Object> result = partyFacilityPurposeService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /party-facility-purposes/execute : Execute Bussiness Process partyFacilityPurpose.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  partyFacilityPurposeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-facility-purposes/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedPartyFacilityPurpose(HttpServletRequest request, @RequestBody PartyFacilityPurposeDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process PartyFacilityPurpose");
        Map<String, Object> result = partyFacilityPurposeService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /party-facility-purposes/execute-list : Execute Bussiness Process partyFacilityPurpose.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  partyFacilityPurposeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/party-facility-purposes/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListPartyFacilityPurpose(HttpServletRequest request, @RequestBody Set<PartyFacilityPurposeDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List PartyFacilityPurpose");
        Map<String, Object> result = partyFacilityPurposeService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /party-facility-purposes : Updates an existing partyFacilityPurpose.
     *
     * @param partyFacilityPurposeDTO the partyFacilityPurposeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated partyFacilityPurposeDTO,
     * or with status 400 (Bad Request) if the partyFacilityPurposeDTO is not valid,
     * or with status 500 (Internal Server Error) if the partyFacilityPurposeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/party-facility-purposes")
    @Timed
    public ResponseEntity<PartyFacilityPurposeDTO> updatePartyFacilityPurpose(@RequestBody PartyFacilityPurposeDTO partyFacilityPurposeDTO) throws URISyntaxException {
        log.debug("REST request to update PartyFacilityPurpose : {}", partyFacilityPurposeDTO);
        if (partyFacilityPurposeDTO.getIdPartyFacility() == null) {
            return createPartyFacilityPurpose(partyFacilityPurposeDTO);
        }
        PartyFacilityPurposeDTO result = partyFacilityPurposeService.save(partyFacilityPurposeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, partyFacilityPurposeDTO.getIdPartyFacility().toString()))
            .body(result);
    }

    /**
     * GET  /party-facility-purposes : get all the partyFacilityPurposes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of partyFacilityPurposes in body
     */
    @GetMapping("/party-facility-purposes")
    @Timed
    public ResponseEntity<List<PartyFacilityPurposeDTO>> getAllPartyFacilityPurposes(Pageable pageable) {
        log.debug("REST request to get a page of PartyFacilityPurposes");
        Page<PartyFacilityPurposeDTO> page = partyFacilityPurposeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/party-facility-purposes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/party-facility-purposes/filterBy")
    @Timed
    public ResponseEntity<List<PartyFacilityPurposeDTO>> getAllFilteredPartyFacilityPurposes(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of PartyFacilityPurposes");
        Page<PartyFacilityPurposeDTO> page = partyFacilityPurposeService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/party-facility-purposes/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /party-facility-purposes/:id : get the "id" partyFacilityPurpose.
     *
     * @param id the id of the partyFacilityPurposeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the partyFacilityPurposeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/party-facility-purposes/{id}")
    @Timed
    public ResponseEntity<PartyFacilityPurposeDTO> getPartyFacilityPurpose(@PathVariable UUID id) {
        log.debug("REST request to get PartyFacilityPurpose : {}", id);
        PartyFacilityPurposeDTO partyFacilityPurposeDTO = partyFacilityPurposeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(partyFacilityPurposeDTO));
    }

    /**
     * DELETE  /party-facility-purposes/:id : delete the "id" partyFacilityPurpose.
     *
     * @param id the id of the partyFacilityPurposeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/party-facility-purposes/{id}")
    @Timed
    public ResponseEntity<Void> deletePartyFacilityPurpose(@PathVariable UUID id) {
        log.debug("REST request to delete PartyFacilityPurpose : {}", id);
        partyFacilityPurposeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/party-facility-purposes?query=:query : search for the partyFacilityPurpose corresponding
     * to the query.
     *
     * @param query the query of the partyFacilityPurpose search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/party-facility-purposes")
    @Timed
    public ResponseEntity<List<PartyFacilityPurposeDTO>> searchPartyFacilityPurposes(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of PartyFacilityPurposes for query {}", query);
        Page<PartyFacilityPurposeDTO> page = partyFacilityPurposeService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/party-facility-purposes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
