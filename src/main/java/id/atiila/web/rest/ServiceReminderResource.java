package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ServiceReminderService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ServiceReminderDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ServiceReminder.
 */
@RestController
@RequestMapping("/api")
public class ServiceReminderResource {

    private final Logger log = LoggerFactory.getLogger(ServiceReminderResource.class);

    private static final String ENTITY_NAME = "serviceReminder";

    private final ServiceReminderService serviceReminderService;

    public ServiceReminderResource(ServiceReminderService serviceReminderService) {
        this.serviceReminderService = serviceReminderService;
    }

    /**
     * POST  /service-reminders : Create a new serviceReminder.
     *
     * @param serviceReminderDTO the serviceReminderDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new serviceReminderDTO, or with status 400 (Bad Request) if the serviceReminder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/service-reminders")
    @Timed
    public ResponseEntity<ServiceReminderDTO> createServiceReminder(@RequestBody ServiceReminderDTO serviceReminderDTO) throws URISyntaxException {
        log.debug("REST request to save ServiceReminder : {}", serviceReminderDTO);
        if (serviceReminderDTO.getIdReminder() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new serviceReminder cannot already have an ID")).body(null);
        }
        ServiceReminderDTO result = serviceReminderService.save(serviceReminderDTO);
        return ResponseEntity.created(new URI("/api/service-reminders/" + result.getIdReminder()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdReminder().toString()))
            .body(result);
    }

    /**
     * POST  /service-reminders/execute{id}/{param} : Execute Bussiness Process serviceReminder.
     *
     * @param serviceReminderDTO the serviceReminderDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  serviceReminderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/service-reminders/execute/{id}/{param}")
    @Timed
    public ResponseEntity<ServiceReminderDTO> executedServiceReminder(@PathVariable Integer id, @PathVariable String param, @RequestBody ServiceReminderDTO serviceReminderDTO) throws URISyntaxException {
        log.debug("REST request to process ServiceReminder : {}", serviceReminderDTO);
        ServiceReminderDTO result = serviceReminderService.processExecuteData(id, param, serviceReminderDTO);
        return new ResponseEntity<ServiceReminderDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /service-reminders/execute-list{id}/{param} : Execute Bussiness Process serviceReminder.
     *
     * @param serviceReminderDTO the serviceReminderDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  serviceReminderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/service-reminders/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<ServiceReminderDTO>> executedListServiceReminder(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<ServiceReminderDTO> serviceReminderDTO) throws URISyntaxException {
        log.debug("REST request to process List ServiceReminder");
        Set<ServiceReminderDTO> result = serviceReminderService.processExecuteListData(id, param, serviceReminderDTO);
        return new ResponseEntity<Set<ServiceReminderDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /service-reminders : Updates an existing serviceReminder.
     *
     * @param serviceReminderDTO the serviceReminderDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated serviceReminderDTO,
     * or with status 400 (Bad Request) if the serviceReminderDTO is not valid,
     * or with status 500 (Internal Server Error) if the serviceReminderDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/service-reminders")
    @Timed
    public ResponseEntity<ServiceReminderDTO> updateServiceReminder(@RequestBody ServiceReminderDTO serviceReminderDTO) throws URISyntaxException {
        log.debug("REST request to update ServiceReminder : {}", serviceReminderDTO);
        if (serviceReminderDTO.getIdReminder() == null) {
            return createServiceReminder(serviceReminderDTO);
        }
        ServiceReminderDTO result = serviceReminderService.save(serviceReminderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, serviceReminderDTO.getIdReminder().toString()))
            .body(result);
    }

    /**
     * GET  /service-reminders : get all the serviceReminders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of serviceReminders in body
     */
    @GetMapping("/service-reminders")
    @Timed
    public ResponseEntity<List<ServiceReminderDTO>> getAllServiceReminders(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ServiceReminders");
        Page<ServiceReminderDTO> page = serviceReminderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/service-reminders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /service-reminders/:id : get the "id" serviceReminder.
     *
     * @param id the id of the serviceReminderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the serviceReminderDTO, or with status 404 (Not Found)
     */
    @GetMapping("/service-reminders/{id}")
    @Timed
    public ResponseEntity<ServiceReminderDTO> getServiceReminder(@PathVariable Integer id) {
        log.debug("REST request to get ServiceReminder : {}", id);
        ServiceReminderDTO serviceReminderDTO = serviceReminderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(serviceReminderDTO));
    }

    /**
     * DELETE  /service-reminders/:id : delete the "id" serviceReminder.
     *
     * @param id the id of the serviceReminderDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/service-reminders/{id}")
    @Timed
    public ResponseEntity<Void> deleteServiceReminder(@PathVariable Integer id) {
        log.debug("REST request to delete ServiceReminder : {}", id);
        serviceReminderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/service-reminders?query=:query : search for the serviceReminder corresponding
     * to the query.
     *
     * @param query the query of the serviceReminder search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/service-reminders")
    @Timed
    public ResponseEntity<List<ServiceReminderDTO>> searchServiceReminders(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ServiceReminders for query {}", query);
        Page<ServiceReminderDTO> page = serviceReminderService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/service-reminders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
