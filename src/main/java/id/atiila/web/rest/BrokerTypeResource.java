package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.BrokerTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.BrokerTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BrokerType.
 */
@RestController
@RequestMapping("/api")
public class BrokerTypeResource {

    private final Logger log = LoggerFactory.getLogger(BrokerTypeResource.class);

    private static final String ENTITY_NAME = "brokerType";

    private final BrokerTypeService brokerTypeService;

    public BrokerTypeResource(BrokerTypeService brokerTypeService) {
        this.brokerTypeService = brokerTypeService;
    }

    /**
     * POST  /broker-types : Create a new brokerType.
     *
     * @param brokerTypeDTO the brokerTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new brokerTypeDTO, or with status 400 (Bad Request) if the brokerType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/broker-types")
    @Timed
    public ResponseEntity<BrokerTypeDTO> createBrokerType(@RequestBody BrokerTypeDTO brokerTypeDTO) throws URISyntaxException {
        log.debug("REST request to save BrokerType : {}", brokerTypeDTO);
        if (brokerTypeDTO.getIdBrokerType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new brokerType cannot already have an ID")).body(null);
        }
        BrokerTypeDTO result = brokerTypeService.save(brokerTypeDTO);
        return ResponseEntity.created(new URI("/api/broker-types/" + result.getIdBrokerType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdBrokerType().toString()))
            .body(result);
    }

    /**
     * POST  /broker-types/execute{id}/{param} : Execute Bussiness Process brokerType.
     *
     * @param brokerTypeDTO the brokerTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  brokerTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/broker-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<BrokerTypeDTO> executedBrokerType(@PathVariable Integer id, @PathVariable String param, @RequestBody BrokerTypeDTO brokerTypeDTO) throws URISyntaxException {
        log.debug("REST request to process BrokerType : {}", brokerTypeDTO);
        BrokerTypeDTO result = brokerTypeService.processExecuteData(id, param, brokerTypeDTO);
        return new ResponseEntity<BrokerTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /broker-types/execute-list{id}/{param} : Execute Bussiness Process brokerType.
     *
     * @param brokerTypeDTO the brokerTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  brokerTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/broker-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<BrokerTypeDTO>> executedListBrokerType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<BrokerTypeDTO> brokerTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List BrokerType");
        Set<BrokerTypeDTO> result = brokerTypeService.processExecuteListData(id, param, brokerTypeDTO);
        return new ResponseEntity<Set<BrokerTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /broker-types : Updates an existing brokerType.
     *
     * @param brokerTypeDTO the brokerTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated brokerTypeDTO,
     * or with status 400 (Bad Request) if the brokerTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the brokerTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/broker-types")
    @Timed
    public ResponseEntity<BrokerTypeDTO> updateBrokerType(@RequestBody BrokerTypeDTO brokerTypeDTO) throws URISyntaxException {
        log.debug("REST request to update BrokerType : {}", brokerTypeDTO);
        if (brokerTypeDTO.getIdBrokerType() == null) {
            return createBrokerType(brokerTypeDTO);
        }
        BrokerTypeDTO result = brokerTypeService.save(brokerTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, brokerTypeDTO.getIdBrokerType().toString()))
            .body(result);
    }

    /**
     * GET  /broker-types : get all the brokerTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of brokerTypes in body
     */
    @GetMapping("/broker-types")
    @Timed
    public ResponseEntity<List<BrokerTypeDTO>> getAllBrokerTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of BrokerTypes");
        Page<BrokerTypeDTO> page = brokerTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/broker-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /broker-types/:id : get the "id" brokerType.
     *
     * @param id the id of the brokerTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the brokerTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/broker-types/{id}")
    @Timed
    public ResponseEntity<BrokerTypeDTO> getBrokerType(@PathVariable Integer id) {
        log.debug("REST request to get BrokerType : {}", id);
        BrokerTypeDTO brokerTypeDTO = brokerTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(brokerTypeDTO));
    }

    /**
     * DELETE  /broker-types/:id : delete the "id" brokerType.
     *
     * @param id the id of the brokerTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/broker-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteBrokerType(@PathVariable Integer id) {
        log.debug("REST request to delete BrokerType : {}", id);
        brokerTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/broker-types?query=:query : search for the brokerType corresponding
     * to the query.
     *
     * @param query the query of the brokerType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/broker-types")
    @Timed
    public ResponseEntity<List<BrokerTypeDTO>> searchBrokerTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of BrokerTypes for query {}", query);
        Page<BrokerTypeDTO> page = brokerTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/broker-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
