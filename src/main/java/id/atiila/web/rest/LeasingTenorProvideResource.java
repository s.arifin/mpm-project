package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.domain.LeasingTenorProvide;
import id.atiila.service.LeasingCompanyService;
import id.atiila.service.LeasingTenorProvideService;
import id.atiila.service.dto.LeasingCompanyDTO;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.LeasingTenorProvideDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.GET;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing LeasingTenorProvide.
 */
@RestController
@RequestMapping("/api")
public class LeasingTenorProvideResource {

    private final Logger log = LoggerFactory.getLogger(LeasingTenorProvideResource.class);

    private static final String ENTITY_NAME = "leasingTenorProvide";

    private final LeasingTenorProvideService leasingTenorProvideService;

    public LeasingTenorProvideResource(LeasingTenorProvideService leasingTenorProvideService) {
        this.leasingTenorProvideService = leasingTenorProvideService;
    }

    @Autowired
    private LeasingCompanyService leasingCompanyService;

    /**
     * POST  /leasing-tenor-provides : Create a new leasingTenorProvide.
     *
     * @param leasingTenorProvideDTO the leasingTenorProvideDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new leasingTenorProvideDTO, or with status 400 (Bad Request) if the leasingTenorProvide has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/leasing-tenor-provides")
    @Timed
    public ResponseEntity<LeasingTenorProvideDTO> createLeasingTenorProvide(@RequestBody LeasingTenorProvideDTO leasingTenorProvideDTO) throws URISyntaxException {
        log.debug("REST request to save LeasingTenorProvide : {}", leasingTenorProvideDTO);
        if (leasingTenorProvideDTO.getIdLeasingProvide() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new leasingTenorProvide cannot already have an ID")).body(null);
        }
        LeasingTenorProvideDTO result = leasingTenorProvideService.save(leasingTenorProvideDTO);
        return ResponseEntity.created(new URI("/api/leasing-tenor-provides/" + result.getIdLeasingProvide()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdLeasingProvide().toString()))
            .body(result);
    }

    /**
     * POST  /leasing-tenor-provides/execute{id}/{param} : Execute Bussiness Process leasingTenorProvide.
     *
     * @param leasingTenorProvideDTO the leasingTenorProvideDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  leasingTenorProvideDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/leasing-tenor-provides/execute/{id}/{param}")
    @Timed
    public ResponseEntity<LeasingTenorProvideDTO> executedLeasingTenorProvide(@PathVariable Integer id, @PathVariable String param, @RequestBody LeasingTenorProvideDTO leasingTenorProvideDTO) throws URISyntaxException {
        log.debug("REST request to process LeasingTenorProvide : {}", leasingTenorProvideDTO);
        LeasingTenorProvideDTO result = leasingTenorProvideService.processExecuteData(id, param, leasingTenorProvideDTO);
        return new ResponseEntity<LeasingTenorProvideDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /leasing-tenor-provides/execute-list{id}/{param} : Execute Bussiness Process leasingTenorProvide.
     *
     * @param leasingTenorProvideDTO the leasingTenorProvideDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  leasingTenorProvideDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/leasing-tenor-provides/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<LeasingTenorProvideDTO>> executedListLeasingTenorProvide(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<LeasingTenorProvideDTO> leasingTenorProvideDTO) throws URISyntaxException {
        log.debug("REST request to process List LeasingTenorProvide");
        Set<LeasingTenorProvideDTO> result = leasingTenorProvideService.processExecuteListData(id, param, leasingTenorProvideDTO);
        return new ResponseEntity<Set<LeasingTenorProvideDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /leasing-tenor-provides : Updates an existing leasingTenorProvide.
     *
     * @param leasingTenorProvideDTO the leasingTenorProvideDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated leasingTenorProvideDTO,
     * or with status 400 (Bad Request) if the leasingTenorProvideDTO is not valid,
     * or with status 500 (Internal Server Error) if the leasingTenorProvideDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/leasing-tenor-provides")
    @Timed
    public ResponseEntity<LeasingTenorProvideDTO> updateLeasingTenorProvide(@RequestBody LeasingTenorProvideDTO leasingTenorProvideDTO) throws URISyntaxException {
        log.debug("REST request to update LeasingTenorProvide : {}", leasingTenorProvideDTO);
        if (leasingTenorProvideDTO.getIdLeasingProvide() == null) {
            return createLeasingTenorProvide(leasingTenorProvideDTO);
        }
        LeasingTenorProvideDTO result = leasingTenorProvideService.save(leasingTenorProvideDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, leasingTenorProvideDTO.getIdLeasingProvide().toString()))
            .body(result);
    }

    /**
     * GET  /leasing-tenor-provides : get all the leasingTenorProvides by id product.
     *
     * @param idproduct the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of leasingTenorProvides in body
     */
    @GetMapping("/leasing-tenor-provides/by-leasing-and-product")
    @Timed
    public ResponseEntity<List<LeasingTenorProvideDTO>> getActiveTenorByIdProduct(@RequestParam String idproduct, @RequestParam String idleasing){
        log.debug("REST request to search for a page of LeasingTenorProvides by id product and leasing{}");
        List<LeasingTenorProvideDTO> rs = leasingTenorProvideService.getActiveTenorByIdProductAndLeasing(idproduct, idleasing);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(rs, headers, HttpStatus.OK);
    }

    /**
     * GET  /leasing-tenor-provides : get all the leasingTenorProvides.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of leasingTenorProvides in body
     */
    @GetMapping("/leasing-tenor-provides")
    @Timed
    public ResponseEntity<List<LeasingTenorProvideDTO>> getAllLeasingTenorProvides(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of LeasingTenorProvides");
        Page<LeasingTenorProvideDTO> page = leasingTenorProvideService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/leasing-tenor-provides");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /leasing-tenor-provides/:id : get the "id" leasingTenorProvide.
     *
     * @param id the id of the leasingTenorProvideDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the leasingTenorProvideDTO, or with status 404 (Not Found)
     */
    @GetMapping("/leasing-tenor-provides/{id}")
    @Timed
    public ResponseEntity<LeasingTenorProvideDTO> getLeasingTenorProvide(@PathVariable UUID id) {
        log.debug("REST request to get LeasingTenorProvide : {}", id);
        LeasingTenorProvideDTO leasingTenorProvideDTO = leasingTenorProvideService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(leasingTenorProvideDTO));
    }

    /**
     * DELETE  /leasing-tenor-provides/:id : delete the "id" leasingTenorProvide.
     *
     * @param id the id of the leasingTenorProvideDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/leasing-tenor-provides/{id}")
    @Timed
    public ResponseEntity<Void> deleteLeasingTenorProvide(@PathVariable UUID id) {
        log.debug("REST request to delete LeasingTenorProvide : {}", id);
        leasingTenorProvideService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/leasing-tenor-provides?query=:query : search for the leasingTenorProvide corresponding
     * to the query.
     *
     * @param query the query of the leasingTenorProvide search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/leasing-tenor-provides")
    @Timed
    public ResponseEntity<List<LeasingTenorProvideDTO>> searchLeasingTenorProvides(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of LeasingTenorProvides for query {}", query);
        Page<LeasingTenorProvideDTO> page = leasingTenorProvideService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/leasing-tenor-provides");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
