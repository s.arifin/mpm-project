package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.UomTypeService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.UomTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UomType.
 */
@RestController
@RequestMapping("/api")
public class UomTypeResource {

    private final Logger log = LoggerFactory.getLogger(UomTypeResource.class);

    private static final String ENTITY_NAME = "uomType";

    private final UomTypeService uomTypeService;

    public UomTypeResource(UomTypeService uomTypeService) {
        this.uomTypeService = uomTypeService;
    }

    /**
     * POST  /uom-types : Create a new uomType.
     *
     * @param uomTypeDTO the uomTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new uomTypeDTO, or with status 400 (Bad Request) if the uomType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/uom-types")
    @Timed
    public ResponseEntity<UomTypeDTO> createUomType(@RequestBody UomTypeDTO uomTypeDTO) throws URISyntaxException {
        log.debug("REST request to save UomType : {}", uomTypeDTO);
        if (uomTypeDTO.getIdUomType() != null) {
            throw new BadRequestAlertException("A new uomType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UomTypeDTO result = uomTypeService.save(uomTypeDTO);
        return ResponseEntity.created(new URI("/api/uom-types/" + result.getIdUomType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdUomType().toString()))
            .body(result);
    }

    /**
     * POST  /uom-types/process : Execute Bussiness Process uomType.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/uom-types/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processUomType(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process UomType ");
        Map<String, Object> result = uomTypeService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /uom-types/execute : Execute Bussiness Process uomType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  uomTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/uom-types/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedUomType(HttpServletRequest request, @RequestBody UomTypeDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process UomType");
        Map<String, Object> result = uomTypeService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /uom-types/execute-list : Execute Bussiness Process uomType.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  uomTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/uom-types/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListUomType(HttpServletRequest request, @RequestBody Set<UomTypeDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List UomType");
        Map<String, Object> result = uomTypeService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /uom-types : Updates an existing uomType.
     *
     * @param uomTypeDTO the uomTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated uomTypeDTO,
     * or with status 400 (Bad Request) if the uomTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the uomTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/uom-types")
    @Timed
    public ResponseEntity<UomTypeDTO> updateUomType(@RequestBody UomTypeDTO uomTypeDTO) throws URISyntaxException {
        log.debug("REST request to update UomType : {}", uomTypeDTO);
        if (uomTypeDTO.getIdUomType() == null) {
            return createUomType(uomTypeDTO);
        }
        UomTypeDTO result = uomTypeService.save(uomTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, uomTypeDTO.getIdUomType().toString()))
            .body(result);
    }

    /**
     * GET  /uom-types : get all the uomTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of uomTypes in body
     */
    @GetMapping("/uom-types")
    @Timed
    public ResponseEntity<List<UomTypeDTO>> getAllUomTypes(Pageable pageable) {
        log.debug("REST request to get a page of UomTypes");
        Page<UomTypeDTO> page = uomTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/uom-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/uom-types/filterBy")
    @Timed
    public ResponseEntity<List<UomTypeDTO>> getAllFilteredUomTypes(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of UomTypes");
        Page<UomTypeDTO> page = uomTypeService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/uom-types/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /uom-types/:id : get the "id" uomType.
     *
     * @param id the id of the uomTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the uomTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/uom-types/{id}")
    @Timed
    public ResponseEntity<UomTypeDTO> getUomType(@PathVariable Integer id) {
        log.debug("REST request to get UomType : {}", id);
        UomTypeDTO uomTypeDTO = uomTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(uomTypeDTO));
    }

    /**
     * DELETE  /uom-types/:id : delete the "id" uomType.
     *
     * @param id the id of the uomTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/uom-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteUomType(@PathVariable Integer id) {
        log.debug("REST request to delete UomType : {}", id);
        uomTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/uom-types?query=:query : search for the uomType corresponding
     * to the query.
     *
     * @param query the query of the uomType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/uom-types")
    @Timed
    public ResponseEntity<List<UomTypeDTO>> searchUomTypes(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of UomTypes for query {}", query);
        Page<UomTypeDTO> page = uomTypeService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/uom-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
