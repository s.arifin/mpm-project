package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;

import id.atiila.domain.Internal;
import id.atiila.domain.SalesUnitRequirement;
import id.atiila.service.CustomerService;
import id.atiila.service.MasterNumberingService;
import id.atiila.service.SalesUnitRequirementService;
import id.atiila.service.dto.SalesUnitRequirementDTO;
import id.atiila.service.mapper.SalesUnitRequirementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import id.atiila.repository.InternalRepository;

import java.util.UUID;

/**
* REST controller for managing the current user's account.
*/
@RestController
@RequestMapping("/api")
public class NumberingResources {

    private final Logger log = LoggerFactory.getLogger(NumberingResources.class);

    @Autowired
    private MasterNumberingService numberingService;

    @Autowired
    private InternalRepository<Internal> internalRepository;

    @Autowired
    private SalesUnitRequirementService salesUnitRequirementService;

    @Autowired
    private SalesUnitRequirementMapper salesUnitRequirementMapper;

    @Autowired
    private CustomerService customerService;

    @GetMapping("/getNumber/{id}/{tag}")
    @Timed
    public ResponseEntity<Long> getNumber(@PathVariable String id, @PathVariable String tag) {
        Long value = numberingService.nextValue(id, tag);
        return new ResponseEntity<Long>(value, null, HttpStatus.OK);
    }

    @GetMapping("/getNumber/IdMPM/")
    @Timed
    public ResponseEntity<String> getNumberIdMPM(@PathVariable Internal idInternal) {
        String value = customerService.nextIdMPM(idInternal);
        return new ResponseEntity<String>(value, null, HttpStatus.OK);
    }

    @GetMapping("/getNumber/IdCustomer/")
    @Timed
    public ResponseEntity<String> getIdCustomer() {
        String value = numberingService.nextCustomerValue();
        return new ResponseEntity<String>(value, null, HttpStatus.OK);
    }

    @GetMapping("/getNumber/ProspectNumber/{idInternal}/{idTag}")
    @Timed
    public ResponseEntity<String> getProspectNumber(@PathVariable String idInternal, @PathVariable String idTag) {
        String value = numberingService.nextProspectNumber(idInternal, idTag);
        return new ResponseEntity<String>(value, null, HttpStatus.OK);
    }

    @GetMapping("/getNumber/VSO/{idInternal}/{idTag}")
    @Timed
    public ResponseEntity<String> getVSONumber(@PathVariable String idInternal, @PathVariable String idTag) {
        String value = numberingService.findTransNumberBy(idInternal, idTag);
        return new ResponseEntity<String>(value, null, HttpStatus.OK);
    }

    @GetMapping("/getNumber/BASTSTNK/{idInternal}/{idTag}")
    @Timed
    public ResponseEntity<String> getNumberBASTSTNK(@PathVariable String idInternal, @PathVariable String idTag) {
        String value = numberingService.findTransBastSTNKNumberBy(idInternal, idTag);
        return new ResponseEntity<String>(value, null, HttpStatus.OK);
    }

    @GetMapping("/getNumber/BASTBPKBCust/{idInternal}/{idTag}")
    @Timed
    public ResponseEntity<String> getNumberBASTBPKBCust(@PathVariable String idInternal, @PathVariable String idTag) {
        String value = numberingService.findTransBastBPKBCustomerNumberBy(idInternal, idTag);
        return new ResponseEntity<String>(value, null, HttpStatus.OK);
    }

    @GetMapping("/getNumber/BASTBPKBLea/{idInternal}/{idTag}")
    @Timed
    public ResponseEntity<String> getNumberBASTBPKBLea(@PathVariable String idInternal, @PathVariable String idTag) {
        String value = numberingService.findTransBastBPKBLeasingNumberBy(idInternal, idTag);
        return new ResponseEntity<String>(value, null, HttpStatus.OK);
    }

    @GetMapping("/getNumber/IdMPM/{idInternal}")
    @Timed
    public ResponseEntity<String> getNumberIdMPM(@PathVariable String idInternal) {
        Internal internal = internalRepository.findByIdInternal(idInternal);
        String value = customerService.nextIdMPM(internal);
        return new ResponseEntity<String>(value, null, HttpStatus.OK);
    }
    @GetMapping("/getNumber/IdRef/{idTag}")
    @Timed
    public ResponseEntity<String> getNumberIdRef(@PathVariable String idTag) {
        String value = numberingService.nextReferenceValue();
        return new ResponseEntity<String>(value, null, HttpStatus.OK);
    }

    // @GetMapping("/getNumber/build-approval-leasing/{idreq}")
    // @Timed
    // public ResponseEntity<String> buildApprovalLeasing(@PathVariable UUID idreq) {
    //     SalesUnitRequirementDTO surDTO = salesUnitRequirementService.findOne(idreq);
    //     SalesUnitRequirement sur =  salesUnitRequirementMapper.toEntity(surDTO);
    //     String value = salesUnitRequirementService.buildApprovalLEasing(sur);
    //     return new ResponseEntity<String>(value, null, HttpStatus.OK);
    // }
}
