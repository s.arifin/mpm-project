package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.VendorService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VendorDTO;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Vendor.
 */
@RestController
@RequestMapping("/api")
public class VendorResource {

    private final Logger log = LoggerFactory.getLogger(VendorResource.class);

    private static final String ENTITY_NAME = "vendor";

    private final VendorService vendorService;

    public VendorResource(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    /**
     * POST  /vendors : Create a new vendor.
     *
     * @param vendorDTO the vendorDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vendorDTO, or with status 400 (Bad Request) if the vendor has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendors")
    @Timed
    public ResponseEntity<VendorDTO> createVendor(@RequestBody VendorDTO vendorDTO) throws URISyntaxException {
        log.debug("REST request to save Vendor : {}", vendorDTO);
        if (vendorDTO.getIdVendor() != null) {
            throw new BadRequestAlertException("A new vendor cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VendorDTO result = vendorService.save(vendorDTO);
        return ResponseEntity.created(new URI("/api/vendors/" + result.getIdVendor()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdVendor().toString()))
            .body(result);
    }

    /**
     * POST  /vendors/process : Execute Bussiness Process vendor.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendors/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processVendor(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Vendor ");
        Map<String, Object> result = vendorService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vendors/execute : Execute Bussiness Process vendor.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  vendorDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendors/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedVendor(HttpServletRequest request, @RequestBody VendorDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Vendor");
        Map<String, Object> result = vendorService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vendors/execute-list : Execute Bussiness Process vendor.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  vendorDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendors/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListVendor(HttpServletRequest request, @RequestBody Set<VendorDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List Vendor");
        Map<String, Object> result = vendorService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /vendors : Updates an existing vendor.
     *
     * @param vendorDTO the vendorDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vendorDTO,
     * or with status 400 (Bad Request) if the vendorDTO is not valid,
     * or with status 500 (Internal Server Error) if the vendorDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vendors")
    @Timed
    public ResponseEntity<VendorDTO> updateVendor(@RequestBody VendorDTO vendorDTO) throws URISyntaxException {
        log.debug("REST request to update Vendor : {}", vendorDTO);
        if (vendorDTO.getIdVendor() == null) {
            return createVendor(vendorDTO);
        }
        VendorDTO result = vendorService.save(vendorDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vendorDTO.getIdVendor().toString()))
            .body(result);
    }

    /**
     * GET  /vendors : get all the vendors.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vendors in body
     */
    @GetMapping("/vendors")
    @Timed
    public ResponseEntity<List<VendorDTO>> getAllVendors(Pageable pageable) {
        log.debug("REST request to get a page of Vendors");
        Page<VendorDTO> page = vendorService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vendors");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vendors/biroJasa")
    @Timed
    public ResponseEntity<List<VendorDTO>> getAllVendorsBiroJasa(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Vendors");
        Page<VendorDTO> page = vendorService.findAllBiroJasa(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vendors/biroJasa");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vendors/mainDealer")
    @Timed
    public ResponseEntity<List<VendorDTO>> getAllVendorsMainDealer(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Vendors");
        Page<VendorDTO> page = vendorService.findAllMainDealer(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vendors/mainDealer");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vendors/filterBy")
    @Timed
    public ResponseEntity<List<VendorDTO>> getAllFilteredVendors(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Vendors");
        Page<VendorDTO> page = vendorService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vendors/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /vendors/:id : get the "id" vendor.
     *
     * @param id the id of the vendorDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vendorDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vendors/{id}")
    @Timed
    public ResponseEntity<VendorDTO> getVendor(@PathVariable String id) {
        log.debug("REST request to get Vendor : {}", id);
        VendorDTO vendorDTO = vendorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vendorDTO));
    }

    /**
     * DELETE  /vendors/:id : delete the "id" vendor.
     *
     * @param id the id of the vendorDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vendors/{id}")
    @Timed
    public ResponseEntity<Void> deleteVendor(@PathVariable String id) {
        log.debug("REST request to delete Vendor : {}", id);
        vendorService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/vendors?query=:query : search for the vendor corresponding
     * to the query.
     *
     * @param query the query of the vendor search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vendors")
    @Timed
    public ResponseEntity<List<VendorDTO>> searchVendors(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Vendors for query {}", query);
        Page<VendorDTO> page = vendorService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vendors");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vendors/byRoleType")
    @Timed
    public ResponseEntity<List<VendorDTO>> getVendorsByRoleType(
                        @RequestParam Integer idRoleType,
                        @ApiParam Pageable pageable) {
        log.debug("REST request to get by role type a page of Vendors");
        Page<VendorDTO> page = vendorService.findByRoleTypeAll(idRoleType, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vendors/byRoleType");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    };

}
