package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.FeatureService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.FeatureDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Feature.
 */
@RestController
@RequestMapping("/api")
public class FeatureResource {

    private final Logger log = LoggerFactory.getLogger(FeatureResource.class);

    private static final String ENTITY_NAME = "feature";

    private final FeatureService featureService;

    public FeatureResource(FeatureService featureService) {
        this.featureService = featureService;
    }

    /**
     * POST  /features : Create a new feature.
     *
     * @param featureDTO the featureDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new featureDTO, or with status 400 (Bad Request) if the feature has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/features")
    @Timed
    public ResponseEntity<FeatureDTO> createFeature(@RequestBody FeatureDTO featureDTO) throws URISyntaxException {
        log.debug("REST request to save Feature : {}", featureDTO);
        if (featureDTO.getIdFeature() != null) {
            throw new BadRequestAlertException("A new feature cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FeatureDTO result = featureService.save(featureDTO);
        return ResponseEntity.created(new URI("/api/features/" + result.getIdFeature()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdFeature().toString()))
            .body(result);
    }

    /**
     * POST  /features/process : Execute Bussiness Process feature.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/features/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processFeature(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Feature ");
        Map<String, Object> result = featureService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /features/execute : Execute Bussiness Process feature.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  featureDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/features/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedFeature(HttpServletRequest request, @RequestBody FeatureDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process Feature");
        Map<String, Object> result = featureService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /features/execute-list : Execute Bussiness Process feature.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  featureDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/features/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListFeature(HttpServletRequest request, @RequestBody Set<FeatureDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List Feature");
        Map<String, Object> result = featureService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /features : Updates an existing feature.
     *
     * @param featureDTO the featureDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated featureDTO,
     * or with status 400 (Bad Request) if the featureDTO is not valid,
     * or with status 500 (Internal Server Error) if the featureDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/features")
    @Timed
    public ResponseEntity<FeatureDTO> updateFeature(@RequestBody FeatureDTO featureDTO) throws URISyntaxException {
        log.debug("REST request to update Feature : {}", featureDTO);
        if (featureDTO.getIdFeature() == null) {
            return createFeature(featureDTO);
        }
        FeatureDTO result = featureService.save(featureDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, featureDTO.getIdFeature().toString()))
            .body(result);
    }

    /**
     * GET  /features : get all the features.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of features in body
     */
    @GetMapping("/features")
    @Timed
    public ResponseEntity<List<FeatureDTO>> getAllFeatures(Pageable pageable) {
        log.debug("REST request to get a page of Features");
        Page<FeatureDTO> page = featureService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/features");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/features/filterBy")
    @Timed
    public ResponseEntity<List<FeatureDTO>> getAllFilteredFeatures(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Features");
        Page<FeatureDTO> page = featureService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/features/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /features/:id : get the "id" feature.
     *
     * @param id the id of the featureDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the featureDTO, or with status 404 (Not Found)
     */
    @GetMapping("/features/{id}")
    @Timed
    public ResponseEntity<FeatureDTO> getFeature(@PathVariable Integer id) {
        log.debug("REST request to get Feature : {}", id);
        FeatureDTO featureDTO = featureService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(featureDTO));
    }

    /**
     * DELETE  /features/:id : delete the "id" feature.
     *
     * @param id the id of the featureDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/features/{id}")
    @Timed
    public ResponseEntity<Void> deleteFeature(@PathVariable Integer id) {
        log.debug("REST request to delete Feature : {}", id);
        featureService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/features?query=:query : search for the feature corresponding
     * to the query.
     *
     * @param query the query of the feature search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/features")
    @Timed
    public ResponseEntity<List<FeatureDTO>> searchFeatures(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Features for query {}", query);
        Page<FeatureDTO> page = featureService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/features");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
