package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.EventTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.EventTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing EventType.
 */
@RestController
@RequestMapping("/api")
public class EventTypeResource {

    private final Logger log = LoggerFactory.getLogger(EventTypeResource.class);

    private static final String ENTITY_NAME = "eventType";

    private final EventTypeService eventTypeService;

    public EventTypeResource(EventTypeService eventTypeService) {
        this.eventTypeService = eventTypeService;
    }

    /**
     * POST  /event-types : Create a new eventType.
     *
     * @param eventTypeDTO the eventTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new eventTypeDTO, or with status 400 (Bad Request) if the eventType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/event-types")
    @Timed
    public ResponseEntity<EventTypeDTO> createEventType(@RequestBody EventTypeDTO eventTypeDTO) throws URISyntaxException {
        log.debug("REST request to save EventType : {}", eventTypeDTO);
        if (eventTypeDTO.getIdEventType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new eventType cannot already have an ID")).body(null);
        }
        EventTypeDTO result = eventTypeService.save(eventTypeDTO);

        return ResponseEntity.created(new URI("/api/event-types/" + result.getIdEventType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdEventType().toString()))
            .body(result);
    }

    /**
     * POST  /event-types/execute : Execute Bussiness Process eventType.
     *
     * @param eventTypeDTO the eventTypeDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  eventTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/event-types/execute/{id}")
    @Timed
    public ResponseEntity<EventTypeDTO> executedEventType(@PathVariable Integer id, @RequestBody EventTypeDTO eventTypeDTO) throws URISyntaxException {
        log.debug("REST request to process EventType : {}", eventTypeDTO);
        EventTypeDTO r = eventTypeService.processExecuteData(eventTypeDTO, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    /**
     * PUT  /event-types : Updates an existing eventType.
     *
     * @param eventTypeDTO the eventTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated eventTypeDTO,
     * or with status 400 (Bad Request) if the eventTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the eventTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/event-types")
    @Timed
    public ResponseEntity<EventTypeDTO> updateEventType(@RequestBody EventTypeDTO eventTypeDTO) throws URISyntaxException {
        log.debug("REST request to update EventType : {}", eventTypeDTO);
        if (eventTypeDTO.getIdEventType() == null) {
            return createEventType(eventTypeDTO);
        }
        EventTypeDTO result = eventTypeService.save(eventTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, eventTypeDTO.getIdEventType().toString()))
            .body(result);
    }

    /**
     * GET  /event-types : get all the eventTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of eventTypes in body
     */
    @GetMapping("/event-types")
    @Timed
    public ResponseEntity<List<EventTypeDTO>> getAllEventTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of EventTypes");
        Page<EventTypeDTO> page = eventTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/event-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /event-types : get all the eventTypes.
     *
     */
    @GetMapping("/event-types/by-parent")
    @Timed
    public ResponseEntity<List<EventTypeDTO>> getAllEventTypes(@ApiParam Integer idparent) {
        List<EventTypeDTO> list = eventTypeService.findAllByIdParent(idparent);
        return new ResponseEntity<List<EventTypeDTO>>(list, null, HttpStatus.OK);
    }



    /**
     * GET  /event-types/:id : get the "id" eventType.
     *
     * @param id the id of the eventTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the eventTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/event-types/{id}")
    @Timed
    public ResponseEntity<EventTypeDTO> getEventType(@PathVariable Integer id) {
        log.debug("REST request to get EventType : {}", id);
        EventTypeDTO eventTypeDTO = eventTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(eventTypeDTO));
    }

    /**
     * DELETE  /event-types/:id : delete the "id" eventType.
     *
     * @param id the id of the eventTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/event-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteEventType(@PathVariable Integer id) {
        log.debug("REST request to delete EventType : {}", id);
        eventTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/event-types?query=:query : search for the eventType corresponding
     * to the query.
     *
     * @param query the query of the eventType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/event-types")
    @Timed
    public ResponseEntity<List<EventTypeDTO>> searchEventTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of EventTypes for query {}", query);
        Page<EventTypeDTO> page = eventTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/event-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
