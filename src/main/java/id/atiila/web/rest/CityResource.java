package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.CityService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.CityDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing City.
 */
@RestController
@RequestMapping("/api")
public class CityResource {

    private final Logger log = LoggerFactory.getLogger(CityResource.class);

    private static final String ENTITY_NAME = "city";

    private final CityService cityService;

    public CityResource(CityService cityService) {
        this.cityService = cityService;
    }

    /**
     * POST  /cities : Create a new city.
     *
     * @param cityDTO the cityDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cityDTO, or with status 400 (Bad Request) if the city has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cities")
    @Timed
    public ResponseEntity<CityDTO> createCity(@RequestBody CityDTO cityDTO) throws URISyntaxException {
        log.debug("REST request to save City : {}", cityDTO);
        if (cityDTO.getIdGeobou() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new city cannot already have an ID")).body(null);
        }
        CityDTO result = cityService.save(cityDTO);
        return ResponseEntity.created(new URI("/api/cities/" + result.getIdGeobou()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdGeobou().toString()))
            .body(result);
    }

    /**
     * POST  /cities/execute{id}/{param} : Execute Bussiness Process city.
     *
     * @param cityDTO the cityDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  cityDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cities/execute/{id}/{param}")
    @Timed
    public ResponseEntity<CityDTO> executedCity(@PathVariable Integer id, @PathVariable String param, @RequestBody CityDTO cityDTO) throws URISyntaxException {
        log.debug("REST request to process City : {}", cityDTO);
        CityDTO result = cityService.processExecuteData(id, param, cityDTO);
        return new ResponseEntity<CityDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /cities/execute-list{id}/{param} : Execute Bussiness Process city.
     *
     * @param cityDTO the cityDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  cityDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cities/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<CityDTO>> executedListCity(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<CityDTO> cityDTO) throws URISyntaxException {
        log.debug("REST request to process List City");
        Set<CityDTO> result = cityService.processExecuteListData(id, param, cityDTO);
        return new ResponseEntity<Set<CityDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /cities : Updates an existing city.
     *
     * @param cityDTO the cityDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cityDTO,
     * or with status 400 (Bad Request) if the cityDTO is not valid,
     * or with status 500 (Internal Server Error) if the cityDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cities")
    @Timed
    public ResponseEntity<CityDTO> updateCity(@RequestBody CityDTO cityDTO) throws URISyntaxException {
        log.debug("REST request to update City : {}", cityDTO);
        if (cityDTO.getIdGeobou() == null) {
            return createCity(cityDTO);
        }
        CityDTO result = cityService.save(cityDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cityDTO.getIdGeobou().toString()))
            .body(result);
    }

    /**
     * GET  /cities : get all the cities.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of cities in body
     */
    @GetMapping("/cities")
    @Timed
    public ResponseEntity<List<CityDTO>> getAllCities(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Cities");
        Page<CityDTO> page = cityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/cities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/cities/by-province")
    @Timed
    public ResponseEntity<List<CityDTO>> getAllCitiesByProvince(@RequestParam String idprovince,
                                                                @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Cities");
        UUID idProvince = idprovince.equals("none") ? null : UUID.fromString(idprovince);

        if (idProvince == null) return getAllCities(pageable);

        List<CityDTO> list = cityService.findAllByProvince(idProvince);
        return new ResponseEntity<>(list, null, HttpStatus.OK);
    }


    /**
     * GET  /cities/:id : get the "id" city.
     *
     * @param id the id of the cityDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cityDTO, or with status 404 (Not Found)
     */
    @GetMapping("/cities/{id}")
    @Timed
    public ResponseEntity<CityDTO> getCity(@PathVariable UUID id) {
        log.debug("REST request to get City : {}", id);
        CityDTO cityDTO = cityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cityDTO));
    }

    /**
     * DELETE  /cities/:id : delete the "id" city.
     *
     * @param id the id of the cityDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cities/{id}")
    @Timed
    public ResponseEntity<Void> deleteCity(@PathVariable UUID id) {
        log.debug("REST request to delete City : {}", id);
        cityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/cities?query=:query : search for the city corresponding
     * to the query.
     *
     * @param query the query of the city search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/cities")
    @Timed
    public ResponseEntity<List<CityDTO>> searchCities(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Cities for query {}", query);
        Page<CityDTO> page = cityService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/cities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
