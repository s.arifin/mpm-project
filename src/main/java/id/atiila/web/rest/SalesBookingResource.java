package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.SalesBookingService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.SalesBookingDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SalesBooking.
 */
@RestController
@RequestMapping("/api")
public class SalesBookingResource {

    private final Logger log = LoggerFactory.getLogger(SalesBookingResource.class);

    private static final String ENTITY_NAME = "salesBooking";

    private final SalesBookingService salesBookingService;

    public SalesBookingResource(SalesBookingService salesBookingService) {
        this.salesBookingService = salesBookingService;
    }

    /**
     * POST  /sales-bookings : Create a new salesBooking.
     *
     * @param salesBookingDTO the salesBookingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new salesBookingDTO, or with status 400 (Bad Request) if the salesBooking has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-bookings")
    @Timed
    public ResponseEntity<SalesBookingDTO> createSalesBooking(@RequestBody SalesBookingDTO salesBookingDTO) throws URISyntaxException {
        log.debug("REST request to save SalesBooking : {}", salesBookingDTO);
        if (salesBookingDTO.getIdSalesBooking() != null) {
            throw new BadRequestAlertException("A new salesBooking cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SalesBookingDTO result = salesBookingService.save(salesBookingDTO);
        return ResponseEntity.created(new URI("/api/sales-bookings/" + result.getIdSalesBooking()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdSalesBooking().toString()))
            .body(result);
    }

    /**
     * POST  /sales-bookings/process : Execute Bussiness Process salesBooking.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-bookings/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processSalesBooking(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process SalesBooking ");
        Map<String, Object> result = salesBookingService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /sales-bookings/execute : Execute Bussiness Process salesBooking.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  salesBookingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-bookings/execute")
    @Timed
    public ResponseEntity<SalesBookingDTO> executedSalesBooking(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process SalesBooking");
        SalesBookingDTO result = salesBookingService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<SalesBookingDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /sales-bookings/execute-list : Execute Bussiness Process salesBooking.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  salesBookingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-bookings/execute-list")
    @Timed
    public ResponseEntity<Set<SalesBookingDTO>> executedListSalesBooking(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List SalesBooking");
        Set<SalesBookingDTO> result = salesBookingService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<SalesBookingDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /sales-bookings : Updates an existing salesBooking.
     *
     * @param salesBookingDTO the salesBookingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated salesBookingDTO,
     * or with status 400 (Bad Request) if the salesBookingDTO is not valid,
     * or with status 500 (Internal Server Error) if the salesBookingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sales-bookings")
    @Timed
    public ResponseEntity<SalesBookingDTO> updateSalesBooking(@RequestBody SalesBookingDTO salesBookingDTO) throws URISyntaxException {
        log.debug("REST request to update SalesBooking : {}", salesBookingDTO);
        if (salesBookingDTO.getIdSalesBooking() == null) {
            return createSalesBooking(salesBookingDTO);
        }
        SalesBookingDTO result = salesBookingService.save(salesBookingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, salesBookingDTO.getIdSalesBooking().toString()))
            .body(result);
    }

    /**
     * GET  /sales-bookings : get all the salesBookings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of salesBookings in body
     */
    @GetMapping("/sales-bookings")
    @Timed
    public ResponseEntity<List<SalesBookingDTO>> getAllSalesBookings(Pageable pageable) {
        log.debug("REST request to get a page of SalesBookings");
        Page<SalesBookingDTO> page = salesBookingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-bookings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/sales-bookings/filterBy")
    @Timed
    public ResponseEntity<List<SalesBookingDTO>> getAllFilteredSalesBookings(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of SalesBookings");
        Page<SalesBookingDTO> page = salesBookingService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-bookings/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/sales-bookings/check-availability/{idinternal}/{idproduct}/{idfeature}/{yearassembly}")
    @Timed
    public ResponseEntity<Integer> checkAvailability(
        @PathVariable String idinternal,
        @PathVariable String idproduct,
        @PathVariable Integer idfeature,
        @PathVariable Integer yearassembly
    ){
        Integer rs = salesBookingService.checkAvailability(idinternal, idproduct, idfeature, yearassembly);
        log.debug("REST request to check availability");
        return new ResponseEntity<>(rs, null, HttpStatus.OK);
    }

    /**
     * GET  /sales-bookings/:id : get the "id" salesBooking.
     *
     * @param id the id of the salesBookingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the salesBookingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sales-bookings/{id}")
    @Timed
    public ResponseEntity<SalesBookingDTO> getSalesBooking(@PathVariable UUID id) {
        log.debug("REST request to get SalesBooking : {}", id);
        SalesBookingDTO salesBookingDTO = salesBookingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(salesBookingDTO));
    }

    /**
     * DELETE  /sales-bookings/:id : delete the "id" salesBooking.
     *
     * @param id the id of the salesBookingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sales-bookings/{id}")
    @Timed
    public ResponseEntity<Void> deleteSalesBooking(@PathVariable UUID id) {
        log.debug("REST request to delete SalesBooking : {}", id);
        salesBookingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/sales-bookings?query=:query : search for the salesBooking corresponding
     * to the query.
     *
     * @param query the query of the salesBooking search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/sales-bookings")
    @Timed
    public ResponseEntity<List<SalesBookingDTO>> searchSalesBookings(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of SalesBookings for query {}", query);
        Page<SalesBookingDTO> page = salesBookingService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/sales-bookings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
