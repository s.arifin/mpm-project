package id.atiila.web.rest;

import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.DealerReminderTypeService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.DealerReminderTypeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DealerReminderType.
 */
@RestController
@RequestMapping("/api")
public class DealerReminderTypeResource {

    private final Logger log = LoggerFactory.getLogger(DealerReminderTypeResource.class);

    private static final String ENTITY_NAME = "dealerReminderType";

    private final DealerReminderTypeService dealerReminderTypeService;

    public DealerReminderTypeResource(DealerReminderTypeService dealerReminderTypeService) {
        this.dealerReminderTypeService = dealerReminderTypeService;
    }

    /**
     * POST  /dealer-reminder-types : Create a new dealerReminderType.
     *
     * @param dealerReminderTypeDTO the dealerReminderTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dealerReminderTypeDTO, or with status 400 (Bad Request) if the dealerReminderType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dealer-reminder-types")
    @Timed
    public ResponseEntity<DealerReminderTypeDTO> createDealerReminderType(@RequestBody DealerReminderTypeDTO dealerReminderTypeDTO) throws URISyntaxException {
        log.debug("REST request to save DealerReminderType : {}", dealerReminderTypeDTO);
        if (dealerReminderTypeDTO.getIdReminderType() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dealerReminderType cannot already have an ID")).body(null);
        }
        DealerReminderTypeDTO result = dealerReminderTypeService.save(dealerReminderTypeDTO);
        return ResponseEntity.created(new URI("/api/dealer-reminder-types/" + result.getIdReminderType()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdReminderType().toString()))
            .body(result);
    }

    /**
     * POST  /dealer-reminder-types/execute{id}/{param} : Execute Bussiness Process dealerReminderType.
     *
     * @param dealerReminderTypeDTO the dealerReminderTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  dealerReminderTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dealer-reminder-types/execute/{id}/{param}")
    @Timed
    public ResponseEntity<DealerReminderTypeDTO> executedDealerReminderType(@PathVariable Integer id, @PathVariable String param, @RequestBody DealerReminderTypeDTO dealerReminderTypeDTO) throws URISyntaxException {
        log.debug("REST request to process DealerReminderType : {}", dealerReminderTypeDTO);
        DealerReminderTypeDTO result = dealerReminderTypeService.processExecuteData(id, param, dealerReminderTypeDTO);
        return new ResponseEntity<DealerReminderTypeDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /dealer-reminder-types/execute-list{id}/{param} : Execute Bussiness Process dealerReminderType.
     *
     * @param dealerReminderTypeDTO the dealerReminderTypeDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  dealerReminderTypeDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dealer-reminder-types/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<DealerReminderTypeDTO>> executedListDealerReminderType(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<DealerReminderTypeDTO> dealerReminderTypeDTO) throws URISyntaxException {
        log.debug("REST request to process List DealerReminderType");
        Set<DealerReminderTypeDTO> result = dealerReminderTypeService.processExecuteListData(id, param, dealerReminderTypeDTO);
        return new ResponseEntity<Set<DealerReminderTypeDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /dealer-reminder-types : Updates an existing dealerReminderType.
     *
     * @param dealerReminderTypeDTO the dealerReminderTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dealerReminderTypeDTO,
     * or with status 400 (Bad Request) if the dealerReminderTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the dealerReminderTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dealer-reminder-types")
    @Timed
    public ResponseEntity<DealerReminderTypeDTO> updateDealerReminderType(@RequestBody DealerReminderTypeDTO dealerReminderTypeDTO) throws URISyntaxException {
        log.debug("REST request to update DealerReminderType : {}", dealerReminderTypeDTO);
        if (dealerReminderTypeDTO.getIdReminderType() == null) {
            return createDealerReminderType(dealerReminderTypeDTO);
        }
        DealerReminderTypeDTO result = dealerReminderTypeService.save(dealerReminderTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dealerReminderTypeDTO.getIdReminderType().toString()))
            .body(result);
    }

    /**
     * GET  /dealer-reminder-types : get all the dealerReminderTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dealerReminderTypes in body
     */
    @GetMapping("/dealer-reminder-types")
    @Timed
    public ResponseEntity<List<DealerReminderTypeDTO>> getAllDealerReminderTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DealerReminderTypes");
        Page<DealerReminderTypeDTO> page = dealerReminderTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dealer-reminder-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    

    /**
     * GET  /dealer-reminder-types/:id : get the "id" dealerReminderType.
     *
     * @param id the id of the dealerReminderTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dealerReminderTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/dealer-reminder-types/{id}")
    @Timed
    public ResponseEntity<DealerReminderTypeDTO> getDealerReminderType(@PathVariable Integer id) {
        log.debug("REST request to get DealerReminderType : {}", id);
        DealerReminderTypeDTO dealerReminderTypeDTO = dealerReminderTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dealerReminderTypeDTO));
    }

    /**
     * DELETE  /dealer-reminder-types/:id : delete the "id" dealerReminderType.
     *
     * @param id the id of the dealerReminderTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dealer-reminder-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteDealerReminderType(@PathVariable Integer id) {
        log.debug("REST request to delete DealerReminderType : {}", id);
        dealerReminderTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/dealer-reminder-types?query=:query : search for the dealerReminderType corresponding
     * to the query.
     *
     * @param query the query of the dealerReminderType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/dealer-reminder-types")
    @Timed
    public ResponseEntity<List<DealerReminderTypeDTO>> searchDealerReminderTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DealerReminderTypes for query {}", query);
        Page<DealerReminderTypeDTO> page = dealerReminderTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/dealer-reminder-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
