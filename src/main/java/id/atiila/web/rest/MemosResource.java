package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.MemosService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.MemosDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Memos.
 */
@RestController
@RequestMapping("/api")
public class MemosResource {

    private final Logger log = LoggerFactory.getLogger(MemosResource.class);

    private static final String ENTITY_NAME = "memos";

    private final MemosService memosService;

    public MemosResource(MemosService memosService) {
        this.memosService = memosService;
    }

    /**
     * POST  /memos : Create a new memos.
     *
     * @param memosDTO the memosDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new memosDTO, or with status 400 (Bad Request) if the memos has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/memos")
    @Timed
    public ResponseEntity<MemosDTO> createMemos(@RequestBody MemosDTO memosDTO) throws URISyntaxException {
        log.debug("Req save ==>" + memosDTO);
        log.debug("REST request to save Memos : {}", memosDTO);
        if (memosDTO.getIdMemo() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new memos cannot already have an ID")).body(null);
        }
        MemosDTO result = memosService.save(memosDTO);
        return ResponseEntity.created(new URI("/api/memos/" + result.getIdMemo()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdMemo().toString()))
            .body(result);
    }

    /**
     * POST  /memos/execute{id}/{param} : Execute Bussiness Process memos.
     *
     * @param memosDTO the memosDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  memosDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/memos/execute/{id}/{param}")
    @Timed
    public ResponseEntity<MemosDTO> executedMemos(@PathVariable Integer id, @PathVariable String param, @RequestBody MemosDTO memosDTO) throws URISyntaxException {
        log.debug("REST request to process Memos : {}", memosDTO);
        MemosDTO result = memosService.processExecuteData(id, param, memosDTO);
        return new ResponseEntity<MemosDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /memos/execute-list{id}/{param} : Execute Bussiness Process memos.
     *
     * @param memosDTO the memosDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  memosDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/memos/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<MemosDTO>> executedListMemos(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<MemosDTO> memosDTO) throws URISyntaxException {
        log.debug("REST request to process List Memos");
        Set<MemosDTO> result = memosService.processExecuteListData(id, param, memosDTO);
        return new ResponseEntity<Set<MemosDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /memos : Updates an existing memos.
     *
     * @param memosDTO the memosDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated memosDTO,
     * or with status 400 (Bad Request) if the memosDTO is not valid,
     * or with status 500 (Internal Server Error) if the memosDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/memos")
    @Timed
    public ResponseEntity<MemosDTO> updateMemos(@RequestBody MemosDTO memosDTO) throws URISyntaxException {
        log.debug("REST request to update Memos : {}", memosDTO);
        if (memosDTO.getIdMemo() == null) {
            return createMemos(memosDTO);
        }
        MemosDTO result = memosService.save(memosDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, memosDTO.getIdMemo().toString()))
            .body(result);
    }

    /**
     * GET  /memos : get all the memos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of memos in body
     */
    @GetMapping("/memos")
    @Timed
    public ResponseEntity<List<MemosDTO>> getAllMemos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Memos");
        Page<MemosDTO> page = memosService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/memos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /memos/:id : get the "id" memos.
     *
     * @param id the id of the memosDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the memosDTO, or with status 404 (Not Found)
     */
    @GetMapping("/memos/{id}")
    @Timed
    public ResponseEntity<MemosDTO> getMemos(@PathVariable UUID id) {
        log.debug("REST request to get Memos : {}", id);
        MemosDTO memosDTO = memosService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(memosDTO));
    }

    /**
     * DELETE  /memos/:id : delete the "id" memos.
     *
     * @param id the id of the memosDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/memos/{id}")
    @Timed
    public ResponseEntity<Void> deleteMemos(@PathVariable UUID id) {
        log.debug("REST request to delete Memos : {}", id);
        memosService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/memos?query=:query : search for the memos corresponding
     * to the query.
     *
     * @param query the query of the memos search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/memos")
    @Timed
    public ResponseEntity<List<MemosDTO>> searchMemos(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Memos for query {}", query);
        Page<MemosDTO> page = memosService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/memos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/memos/set-status/{id}")
    @Timed
    public ResponseEntity< MemosDTO> setStatusMemos(@PathVariable Integer id, @RequestBody MemosDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status Memos : {}", dto);
        MemosDTO r = memosService.changeMemosStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
