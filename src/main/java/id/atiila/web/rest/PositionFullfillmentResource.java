package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.PositionFullfillmentService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.PositionFullfillmentDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PositionFullfillment.
 */
@RestController
@RequestMapping("/api")
public class PositionFullfillmentResource {

    private final Logger log = LoggerFactory.getLogger(PositionFullfillmentResource.class);

    private static final String ENTITY_NAME = "positionFullfillment";

    private final PositionFullfillmentService positionFullfillmentService;

    public PositionFullfillmentResource(PositionFullfillmentService positionFullfillmentService) {
        this.positionFullfillmentService = positionFullfillmentService;
    }

    /**
     * POST  /position-fullfillments : Create a new positionFullfillment.
     *
     * @param positionFullfillmentDTO the positionFullfillmentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new positionFullfillmentDTO, or with status 400 (Bad Request) if the positionFullfillment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/position-fullfillments")
    @Timed
    public ResponseEntity<PositionFullfillmentDTO> createPositionFullfillment(@RequestBody PositionFullfillmentDTO positionFullfillmentDTO) throws URISyntaxException {
        log.debug("REST request to save PositionFullfillment : {}", positionFullfillmentDTO);
        if (positionFullfillmentDTO.getIdPositionFullfillment() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new positionFullfillment cannot already have an ID")).body(null);
        }
        PositionFullfillmentDTO result = positionFullfillmentService.save(positionFullfillmentDTO);
        return ResponseEntity.created(new URI("/api/position-fullfillments/" + result.getIdPositionFullfillment()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPositionFullfillment().toString()))
            .body(result);
    }

    /**
     * POST  /position-fullfillments/execute{id}/{param} : Execute Bussiness Process positionFullfillment.
     *
     * @param positionFullfillmentDTO the positionFullfillmentDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  positionFullfillmentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/position-fullfillments/execute/{id}/{param}")
    @Timed
    public ResponseEntity<PositionFullfillmentDTO> executedPositionFullfillment(@PathVariable Integer id, @PathVariable String param, @RequestBody PositionFullfillmentDTO positionFullfillmentDTO) throws URISyntaxException {
        log.debug("REST request to process PositionFullfillment : {}", positionFullfillmentDTO);
        PositionFullfillmentDTO result = positionFullfillmentService.processExecuteData(id, param, positionFullfillmentDTO);
        return new ResponseEntity<PositionFullfillmentDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /position-fullfillments/execute-list{id}/{param} : Execute Bussiness Process positionFullfillment.
     *
     * @param positionFullfillmentDTO the positionFullfillmentDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  positionFullfillmentDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/position-fullfillments/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<PositionFullfillmentDTO>> executedListPositionFullfillment(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<PositionFullfillmentDTO> positionFullfillmentDTO) throws URISyntaxException {
        log.debug("REST request to process List PositionFullfillment");
        Set<PositionFullfillmentDTO> result = positionFullfillmentService.processExecuteListData(id, param, positionFullfillmentDTO);
        return new ResponseEntity<Set<PositionFullfillmentDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /position-fullfillments : Updates an existing positionFullfillment.
     *
     * @param positionFullfillmentDTO the positionFullfillmentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated positionFullfillmentDTO,
     * or with status 400 (Bad Request) if the positionFullfillmentDTO is not valid,
     * or with status 500 (Internal Server Error) if the positionFullfillmentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/position-fullfillments")
    @Timed
    public ResponseEntity<PositionFullfillmentDTO> updatePositionFullfillment(@RequestBody PositionFullfillmentDTO positionFullfillmentDTO) throws URISyntaxException {
        log.debug("REST request to update PositionFullfillment : {}", positionFullfillmentDTO);
        if (positionFullfillmentDTO.getIdPositionFullfillment() == null) {
            return createPositionFullfillment(positionFullfillmentDTO);
        }
        PositionFullfillmentDTO result = positionFullfillmentService.save(positionFullfillmentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, positionFullfillmentDTO.getIdPositionFullfillment().toString()))
            .body(result);
    }

    /**
     * GET  /position-fullfillments : get all the positionFullfillments.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of positionFullfillments in body
     */
    @GetMapping("/position-fullfillments")
    @Timed
    public ResponseEntity<List<PositionFullfillmentDTO>> getAllPositionFullfillments(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PositionFullfillments");
        Page<PositionFullfillmentDTO> page = positionFullfillmentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/position-fullfillments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /position-fullfillments/:id : get the "id" positionFullfillment.
     *
     * @param id the id of the positionFullfillmentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the positionFullfillmentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/position-fullfillments/{id}")
    @Timed
    public ResponseEntity<PositionFullfillmentDTO> getPositionFullfillment(@PathVariable Integer id) {
        log.debug("REST request to get PositionFullfillment : {}", id);
        PositionFullfillmentDTO positionFullfillmentDTO = positionFullfillmentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(positionFullfillmentDTO));
    }

    /**
     * DELETE  /position-fullfillments/:id : delete the "id" positionFullfillment.
     *
     * @param id the id of the positionFullfillmentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/position-fullfillments/{id}")
    @Timed
    public ResponseEntity<Void> deletePositionFullfillment(@PathVariable Integer id) {
        log.debug("REST request to delete PositionFullfillment : {}", id);
        positionFullfillmentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/position-fullfillments?query=:query : search for the positionFullfillment corresponding
     * to the query.
     *
     * @param query the query of the positionFullfillment search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/position-fullfillments")
    @Timed
    public ResponseEntity<List<PositionFullfillmentDTO>> searchPositionFullfillments(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of PositionFullfillments for query {}", query);
        Page<PositionFullfillmentDTO> page = positionFullfillmentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/position-fullfillments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/position-fullfillments/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrderItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process OrderItem ");
        Map<String, Object> result = positionFullfillmentService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
