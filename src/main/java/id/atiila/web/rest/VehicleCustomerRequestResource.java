package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.VehicleCustomerRequestService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VehicleCustomerRequestDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VehicleCustomerRequest.
 */
@RestController
@RequestMapping("/api")
public class VehicleCustomerRequestResource {

    private final Logger log = LoggerFactory.getLogger(VehicleCustomerRequestResource.class);

    private static final String ENTITY_NAME = "vehicleCustomerRequest";

    private final VehicleCustomerRequestService vehicleCustomerRequestService;

    public VehicleCustomerRequestResource(VehicleCustomerRequestService vehicleCustomerRequestService) {
        this.vehicleCustomerRequestService = vehicleCustomerRequestService;
    }

    /**
     * POST  /vehicle-customer-requests : Create a new vehicleCustomerRequest.
     *
     * @param vehicleCustomerRequestDTO the vehicleCustomerRequestDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vehicleCustomerRequestDTO, or with status 400 (Bad Request) if the vehicleCustomerRequest has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-customer-requests")
    @Timed
    public ResponseEntity<VehicleCustomerRequestDTO> createVehicleCustomerRequest(@RequestBody VehicleCustomerRequestDTO vehicleCustomerRequestDTO) throws URISyntaxException {
        log.debug("REST request to save VehicleCustomerRequest : {}", vehicleCustomerRequestDTO);
        if (vehicleCustomerRequestDTO.getIdRequest() != null) {
            throw new BadRequestAlertException("A new vehicleCustomerRequest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VehicleCustomerRequestDTO result = vehicleCustomerRequestService.save(vehicleCustomerRequestDTO);
        return ResponseEntity.created(new URI("/api/vehicle-customer-requests/" + result.getIdRequest()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdRequest().toString()))
            .body(result);
    }

    /**
     * POST  /vehicle-customer-requests/process : Execute Bussiness Process vehicleCustomerRequest.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-customer-requests/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processVehicleCustomerRequest(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process VehicleCustomerRequest ");
        Map<String, Object> result = vehicleCustomerRequestService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vehicle-customer-requests/execute : Execute Bussiness Process vehicleCustomerRequest.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  vehicleCustomerRequestDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-customer-requests/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedVehicleCustomerRequest(HttpServletRequest request, @RequestBody VehicleCustomerRequestDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process VehicleCustomerRequest");
        Map<String, Object> result = vehicleCustomerRequestService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vehicle-customer-requests/execute-list : Execute Bussiness Process vehicleCustomerRequest.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  vehicleCustomerRequestDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-customer-requests/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListVehicleCustomerRequest(HttpServletRequest request, @RequestBody Set<VehicleCustomerRequestDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List VehicleCustomerRequest");
        Map<String, Object> result = vehicleCustomerRequestService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /vehicle-customer-requests : Updates an existing vehicleCustomerRequest.
     *
     * @param vehicleCustomerRequestDTO the vehicleCustomerRequestDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vehicleCustomerRequestDTO,
     * or with status 400 (Bad Request) if the vehicleCustomerRequestDTO is not valid,
     * or with status 500 (Internal Server Error) if the vehicleCustomerRequestDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vehicle-customer-requests")
    @Timed
    public ResponseEntity<VehicleCustomerRequestDTO> updateVehicleCustomerRequest(@RequestBody VehicleCustomerRequestDTO vehicleCustomerRequestDTO) throws URISyntaxException {
        log.debug("REST request to update VehicleCustomerRequest : {}", vehicleCustomerRequestDTO);
        if (vehicleCustomerRequestDTO.getIdRequest() == null) {
            return createVehicleCustomerRequest(vehicleCustomerRequestDTO);
        }
        VehicleCustomerRequestDTO result = vehicleCustomerRequestService.save(vehicleCustomerRequestDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vehicleCustomerRequestDTO.getIdRequest().toString()))
            .body(result);
    }

    /**
     * GET  /vehicle-customer-requests : get all the vehicleCustomerRequests.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vehicleCustomerRequests in body
     */
    @GetMapping("/vehicle-customer-requests")
    @Timed
    public ResponseEntity<List<VehicleCustomerRequestDTO>> getAllVehicleCustomerRequests(Pageable pageable) {
        log.debug("REST request to get a page of VehicleCustomerRequests");
        Page<VehicleCustomerRequestDTO> page = vehicleCustomerRequestService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-customer-requests");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-customer-requests/filterBy")
    @Timed
    public ResponseEntity<List<VehicleCustomerRequestDTO>> getAllFilteredVehicleCustomerRequests(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of VehicleCustomerRequests");
        Page<VehicleCustomerRequestDTO> page = vehicleCustomerRequestService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-customer-requests/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /vehicle-customer-requests/:id : get the "id" vehicleCustomerRequest.
     *
     * @param id the id of the vehicleCustomerRequestDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vehicleCustomerRequestDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vehicle-customer-requests/{id}")
    @Timed
    public ResponseEntity<VehicleCustomerRequestDTO> getVehicleCustomerRequest(@PathVariable UUID id) {
        log.debug("REST request to get VehicleCustomerRequest : {}", id);
        VehicleCustomerRequestDTO vehicleCustomerRequestDTO = vehicleCustomerRequestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vehicleCustomerRequestDTO));
    }

    /**
     * DELETE  /vehicle-customer-requests/:id : delete the "id" vehicleCustomerRequest.
     *
     * @param id the id of the vehicleCustomerRequestDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vehicle-customer-requests/{id}")
    @Timed
    public ResponseEntity<Void> deleteVehicleCustomerRequest(@PathVariable UUID id) {
        log.debug("REST request to delete VehicleCustomerRequest : {}", id);
        vehicleCustomerRequestService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/vehicle-customer-requests?query=:query : search for the vehicleCustomerRequest corresponding
     * to the query.
     *
     * @param query the query of the vehicleCustomerRequest search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vehicle-customer-requests")
    @Timed
    public ResponseEntity<List<VehicleCustomerRequestDTO>> searchVehicleCustomerRequests(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of VehicleCustomerRequests for query {}", query);
        Page<VehicleCustomerRequestDTO> page = vehicleCustomerRequestService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vehicle-customer-requests");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/vehicle-customer-requests/set-status/{id}")
    @Timed
    public ResponseEntity< VehicleCustomerRequestDTO> setStatusVehicleCustomerRequest(@PathVariable Integer id, @RequestBody VehicleCustomerRequestDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status VehicleCustomerRequest : {}", dto);
        VehicleCustomerRequestDTO r = vehicleCustomerRequestService.changeVehicleCustomerRequestStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(500);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
