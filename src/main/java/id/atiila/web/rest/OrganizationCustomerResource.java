package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.OrganizationCustomerService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.OrganizationCustomerDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

//import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing OrganizationCustomer.
 */
@RestController
@RequestMapping("/api")
public class OrganizationCustomerResource {

    private final Logger log = LoggerFactory.getLogger(OrganizationCustomerResource.class);

    private static final String ENTITY_NAME = "organizationCustomer";

    private final OrganizationCustomerService organizationCustomerService;

    public OrganizationCustomerResource(OrganizationCustomerService organizationCustomerService) {
        this.organizationCustomerService = organizationCustomerService;
    }

    /**
     * POST  /organization-customers : Create a new organizationCustomer.
     *
     * @param organizationCustomerDTO the organizationCustomerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new organizationCustomerDTO, or with status 400 (Bad Request) if the organizationCustomer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organization-customers")
    @Timed
    public ResponseEntity<OrganizationCustomerDTO> createOrganizationCustomer(@RequestBody OrganizationCustomerDTO organizationCustomerDTO) throws URISyntaxException {
        log.debug("REST request to save OrganizationCustomer : {}", organizationCustomerDTO);
        if (organizationCustomerDTO.getIdCustomer() != null) {
            throw new BadRequestAlertException("A new organizationCustomer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrganizationCustomerDTO result = organizationCustomerService.save(organizationCustomerDTO);
        return ResponseEntity.created(new URI("/api/organization-customers/" + result.getIdCustomer()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdCustomer().toString()))
            .body(result);
    }

    /**
     * POST  /organization-customers/execute{id}/{param} : Execute Bussiness Process organizationCustomer.
     *
     * @param organizationCustomerDTO the organizationCustomerDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  organizationCustomerDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organization-customers/execute/{id}/{param}")
    @Timed
    public ResponseEntity<OrganizationCustomerDTO> executedOrganizationCustomer(@PathVariable Integer id, @PathVariable String param, @RequestBody OrganizationCustomerDTO organizationCustomerDTO) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to process OrganizationCustomer : {}", organizationCustomerDTO);
        OrganizationCustomerDTO result = organizationCustomerService.processExecuteData(id, param, organizationCustomerDTO);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<OrganizationCustomerDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /organization-customers/execute-list{id}/{param} : Execute Bussiness Process organizationCustomer.
     *
     * @param organizationCustomerDTO the organizationCustomerDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  organizationCustomerDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organization-customers/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<OrganizationCustomerDTO>> executedListOrganizationCustomer(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<OrganizationCustomerDTO> organizationCustomerDTO) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List OrganizationCustomer");
        Set<OrganizationCustomerDTO> result = organizationCustomerService.processExecuteListData(id, param, organizationCustomerDTO);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<OrganizationCustomerDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /organization-customers : Updates an existing organizationCustomer.
     *
     * @param organizationCustomerDTO the organizationCustomerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated organizationCustomerDTO,
     * or with status 400 (Bad Request) if the organizationCustomerDTO is not valid,
     * or with status 500 (Internal Server Error) if the organizationCustomerDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/organization-customers")
    @Timed
    public ResponseEntity<OrganizationCustomerDTO> updateOrganizationCustomer(@RequestBody OrganizationCustomerDTO organizationCustomerDTO) throws URISyntaxException {
        log.debug("REST request to update OrganizationCustomer : {}", organizationCustomerDTO);
        if (organizationCustomerDTO.getIdCustomer() == null) {
            return createOrganizationCustomer(organizationCustomerDTO);
        }
        OrganizationCustomerDTO result = organizationCustomerService.save(organizationCustomerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, organizationCustomerDTO.getIdCustomer().toString()))
            .body(result);
    }

    /**
     * GET  /organization-customers : get all the organizationCustomers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of organizationCustomers in body
     */
    @GetMapping("/organization-customers")
    @Timed
    public ResponseEntity<List<OrganizationCustomerDTO>> getAllOrganizationCustomers(Pageable pageable) {
        log.debug("REST request to get a page of OrganizationCustomers");
        Page<OrganizationCustomerDTO> page = organizationCustomerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/organization-customers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /organization-customers/:id : get the "id" organizationCustomer.
     *
     * @param id the id of the organizationCustomerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the organizationCustomerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/organization-customers/{id}")
    @Timed
    public ResponseEntity<OrganizationCustomerDTO> getOrganizationCustomer(@PathVariable String id) {
        log.debug("REST request to get OrganizationCustomer : {}", id);
        OrganizationCustomerDTO organizationCustomerDTO = organizationCustomerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(organizationCustomerDTO));
    }

    /**
     * DELETE  /organization-customers/:id : delete the "id" organizationCustomer.
     *
     * @param id the id of the organizationCustomerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/organization-customers/{id}")
    @Timed
    public ResponseEntity<Void> deleteOrganizationCustomer(@PathVariable String id) {
        log.debug("REST request to delete OrganizationCustomer : {}", id);
        organizationCustomerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/organization-customers?query=:query : search for the organizationCustomer corresponding
     * to the query.
     *
     * @param query the query of the organizationCustomer search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/organization-customers")
    @Timed
    public ResponseEntity<List<OrganizationCustomerDTO>> searchOrganizationCustomers(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of OrganizationCustomers for query {}", query);
        Page<OrganizationCustomerDTO> page = organizationCustomerService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/organization-customers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/organization-customers/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrganizationCustomer(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process OrganizationCustomer ");
        Map<String, Object> result = organizationCustomerService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
