package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.FeatureApplicableService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.FeatureApplicableDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing FeatureApplicable.
 */
@RestController
@RequestMapping("/api")
public class FeatureApplicableResource {

    private final Logger log = LoggerFactory.getLogger(FeatureApplicableResource.class);

    private static final String ENTITY_NAME = "featureApplicable";

    private final FeatureApplicableService featureApplicableService;

    public FeatureApplicableResource(FeatureApplicableService featureApplicableService) {
        this.featureApplicableService = featureApplicableService;
    }

    /**
     * POST  /feature-applicables : Create a new featureApplicable.
     *
     * @param featureApplicableDTO the featureApplicableDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new featureApplicableDTO, or with status 400 (Bad Request) if the featureApplicable has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/feature-applicables")
    @Timed
    public ResponseEntity<FeatureApplicableDTO> createFeatureApplicable(@RequestBody FeatureApplicableDTO featureApplicableDTO) throws URISyntaxException {
        log.debug("REST request to save FeatureApplicable : {}", featureApplicableDTO);
        if (featureApplicableDTO.getIdApplicability() != null) {
            throw new BadRequestAlertException("A new featureApplicable cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FeatureApplicableDTO result = featureApplicableService.save(featureApplicableDTO);
        return ResponseEntity.created(new URI("/api/feature-applicables/" + result.getIdApplicability()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdApplicability().toString()))
            .body(result);
    }

    /**
     * POST  /feature-applicables/process : Execute Bussiness Process featureApplicable.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/feature-applicables/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processFeatureApplicable(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process FeatureApplicable ");
        Map<String, Object> result = featureApplicableService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /feature-applicables/execute : Execute Bussiness Process featureApplicable.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  featureApplicableDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/feature-applicables/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedFeatureApplicable(HttpServletRequest request, @RequestBody FeatureApplicableDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process FeatureApplicable");
        Map<String, Object> result = featureApplicableService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /feature-applicables/execute-list : Execute Bussiness Process featureApplicable.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  featureApplicableDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/feature-applicables/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListFeatureApplicable(HttpServletRequest request, @RequestBody Set<FeatureApplicableDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List FeatureApplicable");
        Map<String, Object> result = featureApplicableService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /feature-applicables : Updates an existing featureApplicable.
     *
     * @param featureApplicableDTO the featureApplicableDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated featureApplicableDTO,
     * or with status 400 (Bad Request) if the featureApplicableDTO is not valid,
     * or with status 500 (Internal Server Error) if the featureApplicableDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/feature-applicables")
    @Timed
    public ResponseEntity<FeatureApplicableDTO> updateFeatureApplicable(@RequestBody FeatureApplicableDTO featureApplicableDTO) throws URISyntaxException {
        log.debug("REST request to update FeatureApplicable : {}", featureApplicableDTO);
        if (featureApplicableDTO.getIdApplicability() == null) {
            return createFeatureApplicable(featureApplicableDTO);
        }
        FeatureApplicableDTO result = featureApplicableService.save(featureApplicableDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, featureApplicableDTO.getIdApplicability().toString()))
            .body(result);
    }

    /**
     * GET  /feature-applicables : get all the featureApplicables.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of featureApplicables in body
     */
    @GetMapping("/feature-applicables")
    @Timed
    public ResponseEntity<List<FeatureApplicableDTO>> getAllFeatureApplicables(Pageable pageable) {
        log.debug("REST request to get a page of FeatureApplicables");
        Page<FeatureApplicableDTO> page = featureApplicableService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/feature-applicables");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/feature-applicables/filterBy")
    @Timed
    public ResponseEntity<List<FeatureApplicableDTO>> getAllFilteredFeatureApplicables(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of FeatureApplicables");
        Page<FeatureApplicableDTO> page = featureApplicableService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/feature-applicables/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /feature-applicables/:id : get the "id" featureApplicable.
     *
     * @param id the id of the featureApplicableDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the featureApplicableDTO, or with status 404 (Not Found)
     */
    @GetMapping("/feature-applicables/{id}")
    @Timed
    public ResponseEntity<FeatureApplicableDTO> getFeatureApplicable(@PathVariable UUID id) {
        log.debug("REST request to get FeatureApplicable : {}", id);
        FeatureApplicableDTO featureApplicableDTO = featureApplicableService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(featureApplicableDTO));
    }

    /**
     * DELETE  /feature-applicables/:id : delete the "id" featureApplicable.
     *
     * @param id the id of the featureApplicableDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/feature-applicables/{id}")
    @Timed
    public ResponseEntity<Void> deleteFeatureApplicable(@PathVariable UUID id) {
        log.debug("REST request to delete FeatureApplicable : {}", id);
        featureApplicableService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/feature-applicables?query=:query : search for the featureApplicable corresponding
     * to the query.
     *
     * @param query the query of the featureApplicable search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/feature-applicables")
    @Timed
    public ResponseEntity<List<FeatureApplicableDTO>> searchFeatureApplicables(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of FeatureApplicables for query {}", query);
        Page<FeatureApplicableDTO> page = featureApplicableService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/feature-applicables");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
