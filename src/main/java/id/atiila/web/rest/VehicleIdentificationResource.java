package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.VehicleIdentificationService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VehicleIdentificationDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VehicleIdentification.
 */
@RestController
@RequestMapping("/api")
public class VehicleIdentificationResource {

    private final Logger log = LoggerFactory.getLogger(VehicleIdentificationResource.class);

    private static final String ENTITY_NAME = "vehicleIdentification";

    private final VehicleIdentificationService vehicleIdentificationService;

    public VehicleIdentificationResource(VehicleIdentificationService vehicleIdentificationService) {
        this.vehicleIdentificationService = vehicleIdentificationService;
    }

    /**
     * POST  /vehicle-identifications : Create a new vehicleIdentification.
     *
     * @param vehicleIdentificationDTO the vehicleIdentificationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vehicleIdentificationDTO, or with status 400 (Bad Request) if the vehicleIdentification has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-identifications")
    @Timed
    public ResponseEntity<VehicleIdentificationDTO> createVehicleIdentification(@RequestBody VehicleIdentificationDTO vehicleIdentificationDTO) throws URISyntaxException {
        log.debug("REST request to save VehicleIdentification : {}", vehicleIdentificationDTO);
        if (vehicleIdentificationDTO.getIdVehicleIdentification() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new vehicleIdentification cannot already have an ID")).body(null);
        }
        VehicleIdentificationDTO result = vehicleIdentificationService.save(vehicleIdentificationDTO);

        return ResponseEntity.created(new URI("/api/vehicle-identifications/" + result.getIdVehicleIdentification()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdVehicleIdentification().toString()))
            .body(result);
    }

    /**
     * POST  /vehicle-identifications/execute : Execute Bussiness Process vehicleIdentification.
     *
     * @param vehicleIdentificationDTO the vehicleIdentificationDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  vehicleIdentificationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-identifications/execute/{id}/{param}")
    @Timed
    public ResponseEntity<VehicleIdentificationDTO> executedVehicleIdentification(@PathVariable Integer id, @PathVariable String param, @RequestBody VehicleIdentificationDTO vehicleIdentificationDTO) throws URISyntaxException {
        log.debug("REST request to process VehicleIdentification : {}", vehicleIdentificationDTO);
        VehicleIdentificationDTO r = vehicleIdentificationService.processExecuteData(id, param, vehicleIdentificationDTO);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    @PostMapping("/vehicle-identifications/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<VehicleIdentificationDTO>> executedListVehicleIdentification(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<VehicleIdentificationDTO> vehicleIdentificationDTO) throws URISyntaxException {
        log.debug("REST request to process List VehicleIdentification");
        Set<VehicleIdentificationDTO> r = vehicleIdentificationService.processExecuteListData(id, param, vehicleIdentificationDTO);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    /**
     * PUT  /vehicle-identifications : Updates an existing vehicleIdentification.
     *
     * @param vehicleIdentificationDTO the vehicleIdentificationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vehicleIdentificationDTO,
     * or with status 400 (Bad Request) if the vehicleIdentificationDTO is not valid,
     * or with status 500 (Internal Server Error) if the vehicleIdentificationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vehicle-identifications")
    @Timed
    public ResponseEntity<VehicleIdentificationDTO> updateVehicleIdentification(@RequestBody VehicleIdentificationDTO vehicleIdentificationDTO) throws URISyntaxException {
        log.debug("REST request to update VehicleIdentification : {}", vehicleIdentificationDTO);
        if (vehicleIdentificationDTO.getIdVehicleIdentification() == null) {
            return createVehicleIdentification(vehicleIdentificationDTO);
        }
        VehicleIdentificationDTO result = vehicleIdentificationService.save(vehicleIdentificationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vehicleIdentificationDTO.getIdVehicleIdentification().toString()))
            .body(result);
    }

    /**
     * GET  /vehicle-identifications : get all the vehicleIdentifications.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vehicleIdentifications in body
     */
    @GetMapping("/vehicle-identifications")
    @Timed
    public ResponseEntity<List<VehicleIdentificationDTO>> getAllVehicleIdentifications(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of VehicleIdentifications");
        Page<VehicleIdentificationDTO> page = vehicleIdentificationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-identifications");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /vehicle-identifications/:id : get the "id" vehicleIdentification.
     *
     * @param id the id of the vehicleIdentificationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vehicleIdentificationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vehicle-identifications/{id}")
    @Timed
    public ResponseEntity<VehicleIdentificationDTO> getVehicleIdentification(@PathVariable UUID id) {
        log.debug("REST request to get VehicleIdentification : {}", id);
        VehicleIdentificationDTO vehicleIdentificationDTO = vehicleIdentificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vehicleIdentificationDTO));
    }

    /**
     * DELETE  /vehicle-identifications/:id : delete the "id" vehicleIdentification.
     *
     * @param id the id of the vehicleIdentificationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vehicle-identifications/{id}")
    @Timed
    public ResponseEntity<Void> deleteVehicleIdentification(@PathVariable UUID id) {
        log.debug("REST request to delete VehicleIdentification : {}", id);
        vehicleIdentificationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/vehicle-identifications?query=:query : search for the vehicleIdentification corresponding
     * to the query.
     *
     * @param query the query of the vehicleIdentification search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vehicle-identifications")
    @Timed
    public ResponseEntity<List<VehicleIdentificationDTO>> searchVehicleIdentifications(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of VehicleIdentifications for query {}", query);
        Page<VehicleIdentificationDTO> page = vehicleIdentificationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vehicle-identifications");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-identifications/number/{id}")
    @Timed
    public ResponseEntity<VehicleIdentificationDTO> getVehicleIdentificationByNumber(@PathVariable String id) {
        log.debug("REST request to get VehicleIdentification : {}", id);
        VehicleIdentificationDTO vehicleIdentificationDTO = vehicleIdentificationService.findOneByNumber(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vehicleIdentificationDTO));
    }

    @PostMapping("/vehicle-identifications/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processVehicleIdentification(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process VehicleIdentification ");
        Map<String, Object> result = vehicleIdentificationService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
