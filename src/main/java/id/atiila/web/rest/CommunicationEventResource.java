package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.CommunicationEventService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.CommunicationEventDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CommunicationEvent.
 */
@RestController
@RequestMapping("/api")
public class CommunicationEventResource {

    private final Logger log = LoggerFactory.getLogger(CommunicationEventResource.class);

    private static final String ENTITY_NAME = "communicationEvent";

    private final CommunicationEventService communicationEventService;

    public CommunicationEventResource(CommunicationEventService communicationEventService) {
        this.communicationEventService = communicationEventService;
    }

    /**
     * POST  /communication-events : Create a new communicationEvent.
     *
     * @param communicationEventDTO the communicationEventDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new communicationEventDTO, or with status 400 (Bad Request) if the communicationEvent has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/communication-events")
    @Timed
    public ResponseEntity<CommunicationEventDTO> createCommunicationEvent(@RequestBody CommunicationEventDTO communicationEventDTO) throws URISyntaxException {
        log.debug("REST request to save CommunicationEvent : {}", communicationEventDTO);
        if (communicationEventDTO.getIdCommunicationEvent() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new communicationEvent cannot already have an ID")).body(null);
        }
        CommunicationEventDTO result = communicationEventService.save(communicationEventDTO);
        return ResponseEntity.created(new URI("/api/communication-events/" + result.getIdCommunicationEvent()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdCommunicationEvent().toString()))
            .body(result);
    }

    /**
     * POST  /communication-events/execute{id}/{param} : Execute Bussiness Process communicationEvent.
     *
     * @param communicationEventDTO the communicationEventDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  communicationEventDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/communication-events/execute/{id}/{param}")
    @Timed
    public ResponseEntity<CommunicationEventDTO> executedCommunicationEvent(@PathVariable Integer id, @PathVariable String param, @RequestBody CommunicationEventDTO communicationEventDTO) throws URISyntaxException {
        log.debug("REST request to process CommunicationEvent : {}", communicationEventDTO);
        CommunicationEventDTO result = communicationEventService.processExecuteData(id, param, communicationEventDTO);
        return new ResponseEntity<CommunicationEventDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /communication-events/execute-list{id}/{param} : Execute Bussiness Process communicationEvent.
     *
     * @param communicationEventDTO the communicationEventDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  communicationEventDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/communication-events/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<CommunicationEventDTO>> executedListCommunicationEvent(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<CommunicationEventDTO> communicationEventDTO) throws URISyntaxException {
        log.debug("REST request to process List CommunicationEvent");
        Set<CommunicationEventDTO> result = communicationEventService.processExecuteListData(id, param, communicationEventDTO);
        return new ResponseEntity<Set<CommunicationEventDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /communication-events : Updates an existing communicationEvent.
     *
     * @param communicationEventDTO the communicationEventDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated communicationEventDTO,
     * or with status 400 (Bad Request) if the communicationEventDTO is not valid,
     * or with status 500 (Internal Server Error) if the communicationEventDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/communication-events")
    @Timed
    public ResponseEntity<CommunicationEventDTO> updateCommunicationEvent(@RequestBody CommunicationEventDTO communicationEventDTO) throws URISyntaxException {
        log.debug("REST request to update CommunicationEvent : {}", communicationEventDTO);
        if (communicationEventDTO.getIdCommunicationEvent() == null) {
            return createCommunicationEvent(communicationEventDTO);
        }
        CommunicationEventDTO result = communicationEventService.save(communicationEventDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, communicationEventDTO.getIdCommunicationEvent().toString()))
            .body(result);
    }

    /**
     * GET  /communication-events : get all the communicationEvents.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of communicationEvents in body
     */
    @GetMapping("/communication-events")
    @Timed
    public ResponseEntity<List<CommunicationEventDTO>> getAllCommunicationEvents(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of CommunicationEvents");
        Page<CommunicationEventDTO> page = communicationEventService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/communication-events");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /communication-events/:id : get the "id" communicationEvent.
     *
     * @param id the id of the communicationEventDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the communicationEventDTO, or with status 404 (Not Found)
     */
    @GetMapping("/communication-events/{id}")
    @Timed
    public ResponseEntity<CommunicationEventDTO> getCommunicationEvent(@PathVariable UUID id) {
        log.debug("REST request to get CommunicationEvent : {}", id);
        CommunicationEventDTO communicationEventDTO = communicationEventService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(communicationEventDTO));
    }

    /**
     * DELETE  /communication-events/:id : delete the "id" communicationEvent.
     *
     * @param id the id of the communicationEventDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/communication-events/{id}")
    @Timed
    public ResponseEntity<Void> deleteCommunicationEvent(@PathVariable UUID id) {
        log.debug("REST request to delete CommunicationEvent : {}", id);
        communicationEventService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/communication-events?query=:query : search for the communicationEvent corresponding
     * to the query.
     *
     * @param query the query of the communicationEvent search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/communication-events")
    @Timed
    public ResponseEntity<List<CommunicationEventDTO>> searchCommunicationEvents(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of CommunicationEvents for query {}", query);
        Page<CommunicationEventDTO> page = communicationEventService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/communication-events");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/communication-events/set-status/{id}")
    @Timed
    public ResponseEntity< CommunicationEventDTO> setStatusCommunicationEvent(@PathVariable Integer id, @RequestBody CommunicationEventDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status CommunicationEvent : {}", dto);
        CommunicationEventDTO r = communicationEventService.changeCommunicationEventStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
