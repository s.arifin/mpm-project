package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.VendorRelationshipService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VendorRelationshipDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VendorRelationship.
 */
@RestController
@RequestMapping("/api")
public class VendorRelationshipResource {

    private final Logger log = LoggerFactory.getLogger(VendorRelationshipResource.class);

    private static final String ENTITY_NAME = "vendorRelationship";

    private final VendorRelationshipService vendorRelationshipService;

    public VendorRelationshipResource(VendorRelationshipService vendorRelationshipService) {
        this.vendorRelationshipService = vendorRelationshipService;
    }

    /**
     * POST  /vendor-relationships : Create a new vendorRelationship.
     *
     * @param vendorRelationshipDTO the vendorRelationshipDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vendorRelationshipDTO, or with status 400 (Bad Request) if the vendorRelationship has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendor-relationships")
    @Timed
    public ResponseEntity<VendorRelationshipDTO> createVendorRelationship(@RequestBody VendorRelationshipDTO vendorRelationshipDTO) throws URISyntaxException {
        log.debug("REST request to save VendorRelationship : {}", vendorRelationshipDTO);
        if (vendorRelationshipDTO.getIdPartyRelationship() != null) {
            throw new BadRequestAlertException("A new vendorRelationship cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VendorRelationshipDTO result = vendorRelationshipService.save(vendorRelationshipDTO);
        return ResponseEntity.created(new URI("/api/vendor-relationships/" + result.getIdPartyRelationship()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdPartyRelationship().toString()))
            .body(result);
    }

    /**
     * POST  /vendor-relationships/process : Execute Bussiness Process vendorRelationship.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendor-relationships/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processVendorRelationship(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process VendorRelationship ");
        Map<String, Object> result = vendorRelationshipService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vendor-relationships/execute : Execute Bussiness Process vendorRelationship.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  vendorRelationshipDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendor-relationships/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedVendorRelationship(HttpServletRequest request, @RequestBody VendorRelationshipDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process VendorRelationship");
        Map<String, Object> result = vendorRelationshipService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vendor-relationships/execute-list : Execute Bussiness Process vendorRelationship.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  vendorRelationshipDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vendor-relationships/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListVendorRelationship(HttpServletRequest request, @RequestBody Set<VendorRelationshipDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List VendorRelationship");
        Map<String, Object> result = vendorRelationshipService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /vendor-relationships : Updates an existing vendorRelationship.
     *
     * @param vendorRelationshipDTO the vendorRelationshipDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vendorRelationshipDTO,
     * or with status 400 (Bad Request) if the vendorRelationshipDTO is not valid,
     * or with status 500 (Internal Server Error) if the vendorRelationshipDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vendor-relationships")
    @Timed
    public ResponseEntity<VendorRelationshipDTO> updateVendorRelationship(@RequestBody VendorRelationshipDTO vendorRelationshipDTO) throws URISyntaxException {
        log.debug("REST request to update VendorRelationship : {}", vendorRelationshipDTO);
        if (vendorRelationshipDTO.getIdPartyRelationship() == null) {
            return createVendorRelationship(vendorRelationshipDTO);
        }
        VendorRelationshipDTO result = vendorRelationshipService.save(vendorRelationshipDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vendorRelationshipDTO.getIdPartyRelationship().toString()))
            .body(result);
    }

    /**
     * GET  /vendor-relationships : get all the vendorRelationships.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vendorRelationships in body
     */
    @GetMapping("/vendor-relationships")
    @Timed
    public ResponseEntity<List<VendorRelationshipDTO>> getAllVendorRelationships(Pageable pageable) {
        log.debug("REST request to get a page of VendorRelationships");
        Page<VendorRelationshipDTO> page = vendorRelationshipService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vendor-relationships");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vendor-relationships/filterBy")
    @Timed
    public ResponseEntity<List<VendorRelationshipDTO>> getAllFilteredVendorRelationships(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of VendorRelationships");
        Page<VendorRelationshipDTO> page = vendorRelationshipService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vendor-relationships/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /vendor-relationships/:id : get the "id" vendorRelationship.
     *
     * @param id the id of the vendorRelationshipDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vendorRelationshipDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vendor-relationships/{id}")
    @Timed
    public ResponseEntity<VendorRelationshipDTO> getVendorRelationship(@PathVariable UUID id) {
        log.debug("REST request to get VendorRelationship : {}", id);
        VendorRelationshipDTO vendorRelationshipDTO = vendorRelationshipService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vendorRelationshipDTO));
    }

    /**
     * DELETE  /vendor-relationships/:id : delete the "id" vendorRelationship.
     *
     * @param id the id of the vendorRelationshipDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vendor-relationships/{id}")
    @Timed
    public ResponseEntity<Void> deleteVendorRelationship(@PathVariable UUID id) {
        log.debug("REST request to delete VendorRelationship : {}", id);
        vendorRelationshipService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/vendor-relationships?query=:query : search for the vendorRelationship corresponding
     * to the query.
     *
     * @param query the query of the vendorRelationship search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vendor-relationships")
    @Timed
    public ResponseEntity<List<VendorRelationshipDTO>> searchVendorRelationships(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of VendorRelationships for query {}", query);
        Page<VendorRelationshipDTO> page = vendorRelationshipService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vendor-relationships");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
