package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.domain.SalesTarget;
import id.atiila.service.SalesTargetService;
import id.atiila.service.dto.SalesmanDTO;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.SalesTargetDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SalesTarget.
 */
@RestController
@RequestMapping("/api")
public class SalesTargetResource {

    private final Logger log = LoggerFactory.getLogger(SalesTargetResource.class);

    private static final String ENTITY_NAME = "salesTarget";

    private final SalesTargetService salesTargetService;

    public SalesTargetResource(SalesTargetService salesTargetService) {
        this.salesTargetService = salesTargetService;
    }

    /**
     * POST  /sales-targets : Create a new salesTarget.
     *
     * @param salesTargetDTO the salesTargetDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new salesTargetDTO, or with status 400 (Bad Request) if the salesTarget has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sales-targets")
    @Timed
    public ResponseEntity<SalesTargetDTO> createSalesTarget(@Valid @RequestBody SalesTargetDTO salesTargetDTO) throws URISyntaxException {
        log.debug("REST request to save SalesTarget : {}", salesTargetDTO);
        if (salesTargetDTO.getIdslstrgt() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new salesTarget cannot already have an ID")).body(null);
        }
        SalesTargetDTO result = salesTargetService.save(salesTargetDTO);
        return ResponseEntity.created(new URI("/api/sales-targets/" + result.getIdslstrgt()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdslstrgt().toString()))
            .body(result);
    }

    /**
     * PUT  /sales-targets : Updates an existing salesTarget.
     *
     * @param salesTargetDTO the salesTargetDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated salesTargetDTO,
     * or with status 400 (Bad Request) if the salesTargetDTO is not valid,
     * or with status 500 (Internal Server Error) if the salesTargetDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sales-targets")
    @Timed
    public ResponseEntity<SalesTargetDTO> updateSalesTarget(@Valid @RequestBody SalesTargetDTO salesTargetDTO) throws URISyntaxException {
        log.debug("REST request to update SalesTarget : {}", salesTargetDTO);
        if (salesTargetDTO.getIdslstrgt() == null) {
            return createSalesTarget(salesTargetDTO);
        }
        SalesTargetDTO result = salesTargetService.save(salesTargetDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, salesTargetDTO.getIdslstrgt().toString()))
            .body(result);
    }

    /**
     * GET  /sales-targets : get all the salesTargets.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of salesTargets in body
     */
    @GetMapping("/sales-targets")
    @Timed
    public ResponseEntity<List<SalesTargetDTO>> getAllSalesTargets(Pageable pageable) {
        log.debug("REST request to get a page of SalesTargets");
        Page<SalesTargetDTO> page = salesTargetService.findAll( pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sales-targets");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /sales-targets/:id : get the "id" salesTarget.
     *
     * @param id the id of the salesTargetDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the salesTargetDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sales-targets/{id}")
    @Timed
    public ResponseEntity<SalesTargetDTO> getSalesTarget(@PathVariable UUID id) {
        log.debug("REST request to get SalesTarget : {}", id);
        SalesTargetDTO salesTargetDTO = salesTargetService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(salesTargetDTO));
    }

    /**
     * DELETE  /sales-targets/:id : delete the "id" salesTarget.
     *
     * @param id the id of the salesTargetDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sales-targets/{id}")
    @Timed
    public ResponseEntity<Void> deleteSalesTarget(@PathVariable UUID id) {
        log.debug("REST request to delete SalesTarget : {}", id);
        salesTargetService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/sales-targets?query=:query : search for the salesTarget corresponding
     * to the query.
     *
     * @param query the query of the salesTarget search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/sales-targets")
    @Timed
    public ResponseEntity<List<SalesTargetDTO>> searchSalesTargets(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SalesTargets for query {}", query);
        Page<SalesTargetDTO> page = salesTargetService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/sales-targets");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
