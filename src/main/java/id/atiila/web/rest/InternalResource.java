package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.InternalService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.InternalDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Internal.
 */
@RestController
@RequestMapping("/api")
public class InternalResource {

    private final Logger log = LoggerFactory.getLogger(InternalResource.class);

    private static final String ENTITY_NAME = "internal";

    private final InternalService internalService;

    public InternalResource(InternalService internalService) {
        this.internalService = internalService;
    }

    /**
     * POST  /internals : Create a new internal.
     *
     * @param internalDTO the internalDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new internalDTO, or with status 400 (Bad Request) if the internal has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/internals")
    @Timed
    public ResponseEntity<InternalDTO> createInternal(@RequestBody InternalDTO internalDTO) throws URISyntaxException {
        log.debug("REST request to save Internal : {}", internalDTO);
        if (internalDTO.getIdInternal() != null) {
            throw new BadRequestAlertException("A new internal cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InternalDTO result = internalService.save(internalDTO);
        return ResponseEntity.created(new URI("/api/internals/" + result.getIdInternal()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdInternal().toString()))
            .body(result);
    }

    /**
     * POST  /internals/execute{id}/{param} : Execute Bussiness Process internal.
     *
     * @param internalDTO the internalDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  internalDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/internals/execute/{id}/{param}")
    @Timed
    public ResponseEntity<InternalDTO> executedInternal(@PathVariable Integer id, @PathVariable String param, @RequestBody InternalDTO internalDTO) throws URISyntaxException {
        log.debug("REST request to process Internal : {}", internalDTO);
        InternalDTO result = internalService.processExecuteData(id, param, internalDTO);
        return new ResponseEntity<InternalDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /internals/execute-list{id}/{param} : Execute Bussiness Process internal.
     *
     * @param internalDTO the internalDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  internalDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/internals/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<InternalDTO>> executedListInternal(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<InternalDTO> internalDTO) throws URISyntaxException {
        log.debug("REST request to process List Internal");
        Set<InternalDTO> result = internalService.processExecuteListData(id, param, internalDTO);
        return new ResponseEntity<Set<InternalDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /internals : Updates an existing internal.
     *
     * @param internalDTO the internalDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated internalDTO,
     * or with status 400 (Bad Request) if the internalDTO is not valid,
     * or with status 500 (Internal Server Error) if the internalDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/internals")
    @Timed
    public ResponseEntity<InternalDTO> updateInternal(@RequestBody InternalDTO internalDTO) throws URISyntaxException {
        log.debug("REST request to update Internal : {}", internalDTO);
        if (internalDTO.getIdInternal() == null) {
            return createInternal(internalDTO);
        }
        InternalDTO result = internalService.save(internalDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, internalDTO.getIdInternal().toString()))
            .body(result);
    }

    /**
     * GET  /internals : get all the internals.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of internals in body
     */
    @GetMapping("/internals")
    @Timed
    public ResponseEntity<List<InternalDTO>> getAllInternals(Pageable pageable) {
        log.debug("REST request to get a page of Internals");
        Page<InternalDTO> page = internalService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/internals");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /internals/:id : get the "id" internal.
     *
     * @param id the id of the internalDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the internalDTO, or with status 404 (Not Found)
     */
    @GetMapping("/internals/{id}")
    @Timed
    public ResponseEntity<InternalDTO> getInternal(@PathVariable String id) {
        log.debug("REST request to get Internal : {}", id);
        InternalDTO internalDTO = internalService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(internalDTO));
    }

    /**
     * DELETE  /internals/:id : delete the "id" internal.
     *
     * @param id the id of the internalDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/internals/{id}")
    @Timed
    public ResponseEntity<Void> deleteInternal(@PathVariable String id) {
        log.debug("REST request to delete Internal : {}", id);
        internalService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/internals?query=:query : search for the internal corresponding
     * to the query.
     *
     * @param query the query of the internal search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/internals")
    @Timed
    public ResponseEntity<List<InternalDTO>> searchInternals(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Internals for query {}", query);
        Page<InternalDTO> page = internalService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/internals");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/internals/filterBy")
    @Timed
    public ResponseEntity<List<InternalDTO>> getAllFilteredInternal(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of Vendors");
        Page<InternalDTO> page = internalService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/Internals/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/internals/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrderItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process OrderItem ");
        Map<String, Object> result = internalService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }
}
