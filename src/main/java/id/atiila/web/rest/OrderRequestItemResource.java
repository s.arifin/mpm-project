package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.OrderRequestItemService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.OrderRequestItemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing OrderRequestItem.
 */
@RestController
@RequestMapping("/api")
public class OrderRequestItemResource {

    private final Logger log = LoggerFactory.getLogger(OrderRequestItemResource.class);

    private static final String ENTITY_NAME = "orderRequestItem";

    private final OrderRequestItemService orderRequestItemService;

    public OrderRequestItemResource(OrderRequestItemService orderRequestItemService) {
        this.orderRequestItemService = orderRequestItemService;
    }

    /**
     * POST  /order-request-items : Create a new orderRequestItem.
     *
     * @param orderRequestItemDTO the orderRequestItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new orderRequestItemDTO, or with status 400 (Bad Request) if the orderRequestItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-request-items")
    @Timed
    public ResponseEntity<OrderRequestItemDTO> createOrderRequestItem(@RequestBody OrderRequestItemDTO orderRequestItemDTO) throws URISyntaxException {
        log.debug("REST request to save OrderRequestItem : {}", orderRequestItemDTO);
        if (orderRequestItemDTO.getIdOrderRequestItem() != null) {
            throw new BadRequestAlertException("A new orderRequestItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderRequestItemDTO result = orderRequestItemService.save(orderRequestItemDTO);
        return ResponseEntity.created(new URI("/api/order-request-items/" + result.getIdOrderRequestItem()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdOrderRequestItem().toString()))
            .body(result);
    }

    /**
     * POST  /order-request-items/process : Execute Bussiness Process orderRequestItem.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-request-items/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processOrderRequestItem(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process OrderRequestItem ");
        Map<String, Object> result = orderRequestItemService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /order-request-items/execute : Execute Bussiness Process orderRequestItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  orderRequestItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-request-items/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedOrderRequestItem(HttpServletRequest request, @RequestBody OrderRequestItemDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process OrderRequestItem");
        Map<String, Object> result = orderRequestItemService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /order-request-items/execute-list : Execute Bussiness Process orderRequestItem.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  orderRequestItemDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-request-items/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListOrderRequestItem(HttpServletRequest request, @RequestBody Set<OrderRequestItemDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List OrderRequestItem");
        Map<String, Object> result = orderRequestItemService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /order-request-items : Updates an existing orderRequestItem.
     *
     * @param orderRequestItemDTO the orderRequestItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated orderRequestItemDTO,
     * or with status 400 (Bad Request) if the orderRequestItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the orderRequestItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/order-request-items")
    @Timed
    public ResponseEntity<OrderRequestItemDTO> updateOrderRequestItem(@RequestBody OrderRequestItemDTO orderRequestItemDTO) throws URISyntaxException {
        log.debug("REST request to update OrderRequestItem : {}", orderRequestItemDTO);
        if (orderRequestItemDTO.getIdOrderRequestItem() == null) {
            return createOrderRequestItem(orderRequestItemDTO);
        }
        OrderRequestItemDTO result = orderRequestItemService.save(orderRequestItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, orderRequestItemDTO.getIdOrderRequestItem().toString()))
            .body(result);
    }

    /**
     * GET  /order-request-items : get all the orderRequestItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of orderRequestItems in body
     */
    @GetMapping("/order-request-items")
    @Timed
    public ResponseEntity<List<OrderRequestItemDTO>> getAllOrderRequestItems(Pageable pageable) {
        log.debug("REST request to get a page of OrderRequestItems");
        Page<OrderRequestItemDTO> page = orderRequestItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/order-request-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/order-request-items/filterBy")
    @Timed
    public ResponseEntity<List<OrderRequestItemDTO>> getAllFilteredOrderRequestItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of OrderRequestItems");
        Page<OrderRequestItemDTO> page = orderRequestItemService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/order-request-items/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /order-request-items/:id : get the "id" orderRequestItem.
     *
     * @param id the id of the orderRequestItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the orderRequestItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/order-request-items/{id}")
    @Timed
    public ResponseEntity<OrderRequestItemDTO> getOrderRequestItem(@PathVariable UUID id) {
        log.debug("REST request to get OrderRequestItem : {}", id);
        OrderRequestItemDTO orderRequestItemDTO = orderRequestItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(orderRequestItemDTO));
    }

    /**
     * DELETE  /order-request-items/:id : delete the "id" orderRequestItem.
     *
     * @param id the id of the orderRequestItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/order-request-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteOrderRequestItem(@PathVariable UUID id) {
        log.debug("REST request to delete OrderRequestItem : {}", id);
        orderRequestItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/order-request-items?query=:query : search for the orderRequestItem corresponding
     * to the query.
     *
     * @param query the query of the orderRequestItem search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/order-request-items")
    @Timed
    public ResponseEntity<List<OrderRequestItemDTO>> searchOrderRequestItems(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of OrderRequestItems for query {}", query);
        Page<OrderRequestItemDTO> page = orderRequestItemService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/order-request-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
