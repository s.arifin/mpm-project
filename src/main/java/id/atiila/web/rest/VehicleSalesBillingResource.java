package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.domain.VehicleSalesBilling;
import id.atiila.service.VehicleSalesBillingService;
import id.atiila.service.dto.VehicleSalesOrderDTO;
import id.atiila.service.mapper.VehicleSalesBillingMapper;
import id.atiila.service.pto.OrderPTO;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VehicleSalesBillingDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VehicleSalesBilling.
 */
@RestController
@RequestMapping("/api")
public class VehicleSalesBillingResource {

    private final Logger log = LoggerFactory.getLogger(VehicleSalesBillingResource.class);

    private static final String ENTITY_NAME = "vehicleSalesBilling";

    @Autowired
    private final VehicleSalesBillingService vehicleSalesBillingService;

    public VehicleSalesBillingResource(VehicleSalesBillingService vehicleSalesBillingService) {
        this.vehicleSalesBillingService = vehicleSalesBillingService;
    }

    /**
     * POST  /vehicle-sales-billings : Create a new vehicleSalesBilling.
     *
     * @param vehicleSalesBillingDTO the vehicleSalesBillingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vehicleSalesBillingDTO, or with status 400 (Bad Request) if the vehicleSalesBilling has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-sales-billings")
    @Timed
    public ResponseEntity<VehicleSalesBillingDTO> createVehicleSalesBilling(@RequestBody VehicleSalesBillingDTO vehicleSalesBillingDTO) throws URISyntaxException {
        log.debug("REST request to save VehicleSalesBilling : {}", vehicleSalesBillingDTO);
        if (vehicleSalesBillingDTO.getIdBilling() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new vehicleSalesBilling cannot already have an ID")).body(null);
        }
        VehicleSalesBillingDTO result = vehicleSalesBillingService.save(vehicleSalesBillingDTO);
        return ResponseEntity.created(new URI("/api/vehicle-sales-billings/" + result.getIdBilling()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdBilling().toString()))
            .body(result);
    }

    /**
     * POST  /vehicle-sales-billings/execute{id}/{param} : Execute Bussiness Process vehicleSalesBilling.
     *
     * @param vehicleSalesBillingDTO the vehicleSalesBillingDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  vehicleSalesBillingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-sales-billings/execute/{id}/{param}")
    @Timed
    public ResponseEntity<VehicleSalesBillingDTO> executedVehicleSalesBilling(
            @PathVariable Integer id,
            @PathVariable String param,
            @RequestBody VehicleSalesBillingDTO vehicleSalesBillingDTO) throws URISyntaxException {
        log.debug("REST request to process VehicleSalesBilling : {}", vehicleSalesBillingDTO);

        VehicleSalesBillingDTO  billingDTO = vehicleSalesBillingService.findOne(vehicleSalesBillingDTO.getIdBilling());
        log.debug("REST request to process generate DTO", billingDTO);

        VehicleSalesBillingDTO result = vehicleSalesBillingService.processExecuteData(id, param, billingDTO);
        return new ResponseEntity<VehicleSalesBillingDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vehicle-sales-billings/execute-list{id}/{param} : Execute Bussiness Process vehicleSalesBilling.
     *
     * @param vehicleSalesBillingDTO the vehicleSalesBillingDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  vehicleSalesBillingDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-sales-billings/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<VehicleSalesBillingDTO>> executedListVehicleSalesBilling(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<VehicleSalesBillingDTO> vehicleSalesBillingDTO) throws URISyntaxException {
        log.debug("REST request to process List VehicleSalesBilling");
        Set<VehicleSalesBillingDTO> result = vehicleSalesBillingService.processExecuteListData(id, param, vehicleSalesBillingDTO);
        return new ResponseEntity<Set<VehicleSalesBillingDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /vehicle-sales-billings : Updates an existing vehicleSalesBilling.
     *
     * @param vehicleSalesBillingDTO the vehicleSalesBillingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vehicleSalesBillingDTO,
     * or with status 400 (Bad Request) if the vehicleSalesBillingDTO is not valid,
     * or with status 500 (Internal Server Error) if the vehicleSalesBillingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vehicle-sales-billings")
    @Timed
    public ResponseEntity<VehicleSalesBillingDTO> updateVehicleSalesBilling(@RequestBody VehicleSalesBillingDTO vehicleSalesBillingDTO) throws URISyntaxException {
        log.debug("REST request to update VehicleSalesBilling : {}", vehicleSalesBillingDTO);
        if (vehicleSalesBillingDTO.getIdBilling() == null) {
            return createVehicleSalesBilling(vehicleSalesBillingDTO);
        }
        VehicleSalesBillingDTO result = vehicleSalesBillingService.save(vehicleSalesBillingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vehicleSalesBillingDTO.getIdBilling().toString()))
            .body(result);
    }

    /**
     * GET  /vehicle-sales-billings : get all the vehicleSalesBillings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vehicleSalesBillings in body
     */
    @GetMapping("/vehicle-sales-billings")
    @Timed
    public ResponseEntity<List<VehicleSalesBillingDTO>> getAllVehicleSalesBillings(
        @RequestParam(required = false) String noIvu,
        @RequestParam(required = false) String lov,
        @RequestParam(required = false) String forMemo,
        @ApiParam Pageable pageable) {

        Page<VehicleSalesBillingDTO> page;

        if (noIvu != null){
            log.debug("REST request to get a page of VehicleSalesBillings--noIvu=>" + noIvu+"--");
            page= vehicleSalesBillingService.findByNoIvu(noIvu, pageable);
        }else if (lov != null ){
            log.debug("REST request to get a page of VehicleSalesBillingsLov" + lov+"--");
            page= vehicleSalesBillingService.findAllLov(pageable);
        } else if (forMemo != null){
            log.debug("REST request to get a page of VehicleSalesBillingsLovForMemo" + forMemo+"--");
            page = vehicleSalesBillingService.findAllLovForMemo(pageable);
        } else {
            log.debug("REST request to get a page of VehicleSalesBillings");
            page = vehicleSalesBillingService.findAll(pageable);
        }

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-billings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @GetMapping("/vehicle-sales-billings/tax-list")
    @Timed
    public ResponseEntity<List<VehicleSalesBillingDTO>> getAllVehicleSalesBillingsTaxList(
        @RequestParam(required = false) String noIvu,
        @ApiParam Pageable pageable) {

        Page<VehicleSalesBillingDTO> page;
            log.debug("REST request to get a page of VehicleSalesBillings");
            page = vehicleSalesBillingService.findAllTaxListInv(pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-billings/tax-list");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /vehicle-sales-billings/:id : get the "id" vehicleSalesBilling.
     *
     * @param id the id of the vehicleSalesBillingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vehicleSalesBillingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vehicle-sales-billings/{id}")
    @Timed
    public ResponseEntity<VehicleSalesBillingDTO> getVehicleSalesBilling(@PathVariable UUID id) {
        log.debug("REST request to get VehicleSalesBilling : {}", id);
        VehicleSalesBillingDTO vehicleSalesBillingDTO = vehicleSalesBillingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vehicleSalesBillingDTO));
    }

    /**
     * DELETE  /vehicle-sales-billings/:id : delete the "id" vehicleSalesBilling.
     *
     * @param id the id of the vehicleSalesBillingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vehicle-sales-billings/{id}")
    @Timed
    public ResponseEntity<Void> deleteVehicleSalesBilling(@PathVariable UUID id) {
        log.debug("REST request to delete VehicleSalesBilling : {}", id);
        vehicleSalesBillingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/vehicle-sales-billings?query=:query : search for the vehicleSalesBilling corresponding
     * to the query.
     *
     * @param query the query of the vehicleSalesBilling search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vehicle-sales-billings")
    @Timed
    public ResponseEntity<List<VehicleSalesBillingDTO>> searchVehicleSalesBillings(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of VehicleSalesBillings for query {}", query);
        Page<VehicleSalesBillingDTO> page = vehicleSalesBillingService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vehicle-sales-billings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/vehicle-sales-billings/set-status/{id}")
    @Timed
    public ResponseEntity< VehicleSalesBillingDTO> setStatusVehicleSalesBilling(@PathVariable Integer id, @RequestBody VehicleSalesBillingDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status VehicleSalesBilling : {}", dto);
        VehicleSalesBillingDTO r = vehicleSalesBillingService.changeVehicleSalesBillingStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

    @GetMapping("/vehicle-sales-billings/filterBy")
    @Timed
    public ResponseEntity<List<VehicleSalesBillingDTO>> getAllFilteredOrderItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of vehicle-sales-billings");
        Page<VehicleSalesBillingDTO> page = vehicleSalesBillingService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-sales-billings/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-sales-billings/filterByPTO")
    @Timed
    public ResponseEntity<List<VehicleSalesBillingDTO>> getAllFilteredVSB(  @ApiParam Pageable pageable,
                                                                            @ApiParam OrderPTO param) {
        Page<VehicleSalesBillingDTO> page = vehicleSalesBillingService.findFilterBy(pageable, param);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/vehicle-sales-billings/filterByPTO");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-sales-billings/filterFakturByPTO")
    @Timed
    public ResponseEntity<List<VehicleSalesBillingDTO>> getFilterFakturByPTO(  @ApiParam Pageable pageable,
                                                                            @ApiParam OrderPTO param) {
        Page<VehicleSalesBillingDTO> page = vehicleSalesBillingService.findFakturFilterBy(pageable, param);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/vehicle-sales-billings/filterFakturByPTO");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
