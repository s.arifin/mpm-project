package id.atiila.web.rest;

import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.CommunicationEventProspectService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.CommunicationEventProspectDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CommunicationEventProspect.
 */
@RestController
@RequestMapping("/api")
public class CommunicationEventProspectResource {

    private final Logger log = LoggerFactory.getLogger(CommunicationEventProspectResource.class);

    private static final String ENTITY_NAME = "communicationEventProspect";

    private final CommunicationEventProspectService communicationEventProspectService;

    public CommunicationEventProspectResource(CommunicationEventProspectService communicationEventProspectService) {
        this.communicationEventProspectService = communicationEventProspectService;
    }

    /**
     * POST  /communication-event-prospects : Create a new communicationEventProspect.
     *
     * @param communicationEventProspectDTO the communicationEventProspectDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new communicationEventProspectDTO, or with status 400 (Bad Request) if the communicationEventProspect has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/communication-event-prospects")
    @Timed
    public ResponseEntity<CommunicationEventProspectDTO> createCommunicationEventProspect(@RequestBody CommunicationEventProspectDTO communicationEventProspectDTO) throws URISyntaxException {
        log.debug("REST request to save CommunicationEventProspect : {}", communicationEventProspectDTO);
        if (communicationEventProspectDTO.getIdCommunicationEvent() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new communicationEventProspect cannot already have an ID")).body(null);
        }
        CommunicationEventProspectDTO result = communicationEventProspectService.save(communicationEventProspectDTO);
        return ResponseEntity.created(new URI("/api/communication-event-prospects/" + result.getIdCommunicationEvent()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdCommunicationEvent().toString()))
            .body(result);
    }

    /**
     * POST  /communication-event-prospects/execute{id}/{param} : Execute Bussiness Process communicationEventProspect.
     *
     * @param communicationEventProspectDTO the communicationEventProspectDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  communicationEventProspectDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/communication-event-prospects/execute/{id}/{param}")
    @Timed
    public ResponseEntity<CommunicationEventProspectDTO> executedCommunicationEventProspect(@PathVariable Integer id, @PathVariable String param, @RequestBody CommunicationEventProspectDTO communicationEventProspectDTO) throws URISyntaxException {
        log.debug("REST request to process CommunicationEventProspect : {}", communicationEventProspectDTO);
        CommunicationEventProspectDTO result = communicationEventProspectService.processExecuteData(id, param, communicationEventProspectDTO);
        return new ResponseEntity<CommunicationEventProspectDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /communication-event-prospects/execute-list{id}/{param} : Execute Bussiness Process communicationEventProspect.
     *
     * @param communicationEventProspectDTO the communicationEventProspectDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  communicationEventProspectDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/communication-event-prospects/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<CommunicationEventProspectDTO>> executedListCommunicationEventProspect(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<CommunicationEventProspectDTO> communicationEventProspectDTO) throws URISyntaxException {
        log.debug("REST request to process List CommunicationEventProspect");
        Set<CommunicationEventProspectDTO> result = communicationEventProspectService.processExecuteListData(id, param, communicationEventProspectDTO);
        return new ResponseEntity<Set<CommunicationEventProspectDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /communication-event-prospects : Updates an existing communicationEventProspect.
     *
     * @param communicationEventProspectDTO the communicationEventProspectDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated communicationEventProspectDTO,
     * or with status 400 (Bad Request) if the communicationEventProspectDTO is not valid,
     * or with status 500 (Internal Server Error) if the communicationEventProspectDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/communication-event-prospects")
    @Timed
    public ResponseEntity<CommunicationEventProspectDTO> updateCommunicationEventProspect(@RequestBody CommunicationEventProspectDTO communicationEventProspectDTO) throws URISyntaxException {
        log.debug("REST request to update CommunicationEventProspect : {}", communicationEventProspectDTO);
        if (communicationEventProspectDTO.getIdCommunicationEvent() == null) {
            return createCommunicationEventProspect(communicationEventProspectDTO);
        }
        CommunicationEventProspectDTO result = communicationEventProspectService.save(communicationEventProspectDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, communicationEventProspectDTO.getIdCommunicationEvent().toString()))
            .body(result);
    }

    /**
     * GET  /communication-event-prospects : get all the communicationEventProspects.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of communicationEventProspects in body
     */
    @GetMapping("/communication-event-prospects")
    @Timed
    public ResponseEntity<List<CommunicationEventProspectDTO>> getAllCommunicationEventProspects(@RequestParam(required = false) String idprospect, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of CommunicationEventProspects");
        UUID idProspect = null;
        ResponseEntity<List<CommunicationEventProspectDTO>> result;

        if (idprospect != null) {
            idProspect = UUID.fromString(idprospect);
            List<CommunicationEventProspectDTO> list = communicationEventProspectService.findAllByIdProspect(idProspect);
            result = new ResponseEntity<>(list, null, HttpStatus.OK);
        }

        else {
            Page<CommunicationEventProspectDTO> page = communicationEventProspectService.findAll(pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/communication-event-prospects");
            result = new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
        }

        return result;
    }

    @GetMapping("/communication-event-prospects/filterBy")
    @Timed
    public ResponseEntity<List<CommunicationEventProspectDTO>> getAllFilteredOrderItems(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of OrderItems");
        Page<CommunicationEventProspectDTO> page = communicationEventProspectService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/communication-event-prospects/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /communication-event-prospects/:id : get the "id" communicationEventProspect.
     *
     * @param id the id of the communicationEventProspectDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the communicationEventProspectDTO, or with status 404 (Not Found)
     */
    @GetMapping("/communication-event-prospects/{id}")
    @Timed
    public ResponseEntity<CommunicationEventProspectDTO> getCommunicationEventProspect(@PathVariable UUID id) {
        log.debug("REST request to get CommunicationEventProspect : {}", id);
        CommunicationEventProspectDTO communicationEventProspectDTO = communicationEventProspectService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(communicationEventProspectDTO));
    }

    /**
     * DELETE  /communication-event-prospects/:id : delete the "id" communicationEventProspect.
     *
     * @param id the id of the communicationEventProspectDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/communication-event-prospects/{id}")
    @Timed
    public ResponseEntity<Void> deleteCommunicationEventProspect(@PathVariable UUID id) {
        log.debug("REST request to delete CommunicationEventProspect : {}", id);
        communicationEventProspectService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/communication-event-prospects?query=:query : search for the communicationEventProspect corresponding
     * to the query.
     *
     * @param query the query of the communicationEventProspect search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/communication-event-prospects")
    @Timed
    public ResponseEntity<List<CommunicationEventProspectDTO>> searchCommunicationEventProspects(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of CommunicationEventProspects for query {}", query);
        Page<CommunicationEventProspectDTO> page = communicationEventProspectService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/communication-event-prospects");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/communication-event-prospects/set-status/{id}")
    @Timed
    public ResponseEntity< CommunicationEventProspectDTO> setStatusCommunicationEventProspect(@PathVariable Integer id, @RequestBody CommunicationEventProspectDTO dto) throws URISyntaxException {
        log.debug("REST request to set Status CommunicationEventProspect : {}", dto);
        CommunicationEventProspectDTO r = communicationEventProspectService.changeCommunicationEventProspectStatus(dto, id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
