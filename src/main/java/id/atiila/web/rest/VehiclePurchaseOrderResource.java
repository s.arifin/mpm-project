package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.VehiclePurchaseOrderService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VehiclePurchaseOrderDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VehiclePurchaseOrder.
 */
@RestController
@RequestMapping("/api")
public class VehiclePurchaseOrderResource {

    private final Logger log = LoggerFactory.getLogger(VehiclePurchaseOrderResource.class);

    private static final String ENTITY_NAME = "vehiclePurchaseOrder";

    private final VehiclePurchaseOrderService vehiclePurchaseOrderService;

    public VehiclePurchaseOrderResource(VehiclePurchaseOrderService vehiclePurchaseOrderService) {
        this.vehiclePurchaseOrderService = vehiclePurchaseOrderService;
    }

    /**
     * POST  /vehicle-purchase-orders : Create a new vehiclePurchaseOrder.
     *
     * @param vehiclePurchaseOrderDTO the vehiclePurchaseOrderDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vehiclePurchaseOrderDTO, or with status 400 (Bad Request) if the vehiclePurchaseOrder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-purchase-orders")
    @Timed
    public ResponseEntity<VehiclePurchaseOrderDTO> createVehiclePurchaseOrder(@RequestBody VehiclePurchaseOrderDTO vehiclePurchaseOrderDTO) throws URISyntaxException {
        log.debug("REST request to save VehiclePurchaseOrder : {}", vehiclePurchaseOrderDTO);
        if (vehiclePurchaseOrderDTO.getIdOrder() != null) {
            throw new BadRequestAlertException("A new vehiclePurchaseOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VehiclePurchaseOrderDTO result = vehiclePurchaseOrderService.save(vehiclePurchaseOrderDTO);
        return ResponseEntity.created(new URI("/api/vehicle-purchase-orders/" + result.getIdOrder()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdOrder().toString()))
            .body(result);
    }

    /**
     * POST  /vehicle-purchase-orders/process : Execute Bussiness Process vehiclePurchaseOrder.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-purchase-orders/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processVehiclePurchaseOrder(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process VehiclePurchaseOrder ");
        Map<String, Object> result = vehiclePurchaseOrderService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vehicle-purchase-orders/execute : Execute Bussiness Process vehiclePurchaseOrder.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  vehiclePurchaseOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-purchase-orders/execute")
    @Timed
    public ResponseEntity<VehiclePurchaseOrderDTO> executedVehiclePurchaseOrder(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process VehiclePurchaseOrder");
        VehiclePurchaseOrderDTO result = vehiclePurchaseOrderService.processExecuteData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<VehiclePurchaseOrderDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vehicle-purchase-orders/execute-list : Execute Bussiness Process vehiclePurchaseOrder.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  vehiclePurchaseOrderDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicle-purchase-orders/execute-list")
    @Timed
    public ResponseEntity<Set<VehiclePurchaseOrderDTO>> executedListVehiclePurchaseOrder(HttpServletRequest request, @RequestBody Map<String, Object> params) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List VehiclePurchaseOrder");
        Set<VehiclePurchaseOrderDTO> result = vehiclePurchaseOrderService.processExecuteListData(request, params);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Set<VehiclePurchaseOrderDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /vehicle-purchase-orders : Updates an existing vehiclePurchaseOrder.
     *
     * @param vehiclePurchaseOrderDTO the vehiclePurchaseOrderDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vehiclePurchaseOrderDTO,
     * or with status 400 (Bad Request) if the vehiclePurchaseOrderDTO is not valid,
     * or with status 500 (Internal Server Error) if the vehiclePurchaseOrderDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vehicle-purchase-orders")
    @Timed
    public ResponseEntity<VehiclePurchaseOrderDTO> updateVehiclePurchaseOrder(@RequestBody VehiclePurchaseOrderDTO vehiclePurchaseOrderDTO) throws URISyntaxException {
        log.debug("REST request to update VehiclePurchaseOrder : {}", vehiclePurchaseOrderDTO);
        if (vehiclePurchaseOrderDTO.getIdOrder() == null) {
            return createVehiclePurchaseOrder(vehiclePurchaseOrderDTO);
        }
        VehiclePurchaseOrderDTO result = vehiclePurchaseOrderService.save(vehiclePurchaseOrderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vehiclePurchaseOrderDTO.getIdOrder().toString()))
            .body(result);
    }

    /**
     * GET  /vehicle-purchase-orders : get all the vehiclePurchaseOrders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vehiclePurchaseOrders in body
     */
    @GetMapping("/vehicle-purchase-orders")
    @Timed
    public ResponseEntity<List<VehiclePurchaseOrderDTO>> getAllVehiclePurchaseOrders(Pageable pageable) {
        log.debug("REST request to get a page of VehiclePurchaseOrders");
        Page<VehiclePurchaseOrderDTO> page = vehiclePurchaseOrderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-purchase-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/vehicle-purchase-orders/filterBy")
    @Timed
    public ResponseEntity<List<VehiclePurchaseOrderDTO>> getAllFilteredVehiclePurchaseOrders(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of VehiclePurchaseOrders");
        Page<VehiclePurchaseOrderDTO> page = vehiclePurchaseOrderService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicle-purchase-orders/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /vehicle-purchase-orders/:id : get the "id" vehiclePurchaseOrder.
     *
     * @param id the id of the vehiclePurchaseOrderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vehiclePurchaseOrderDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vehicle-purchase-orders/{id}")
    @Timed
    public ResponseEntity<VehiclePurchaseOrderDTO> getVehiclePurchaseOrder(@PathVariable UUID id) {
        log.debug("REST request to get VehiclePurchaseOrder : {}", id);
        VehiclePurchaseOrderDTO vehiclePurchaseOrderDTO = vehiclePurchaseOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vehiclePurchaseOrderDTO));
    }

    /**
     * DELETE  /vehicle-purchase-orders/:id : delete the "id" vehiclePurchaseOrder.
     *
     * @param id the id of the vehiclePurchaseOrderDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vehicle-purchase-orders/{id}")
    @Timed
    public ResponseEntity<Void> deleteVehiclePurchaseOrder(@PathVariable UUID id) {
        log.debug("REST request to delete VehiclePurchaseOrder : {}", id);
        vehiclePurchaseOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/vehicle-purchase-orders?query=:query : search for the vehiclePurchaseOrder corresponding
     * to the query.
     *
     * @param query the query of the vehiclePurchaseOrder search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vehicle-purchase-orders")
    @Timed
    public ResponseEntity<List<VehiclePurchaseOrderDTO>> searchVehiclePurchaseOrders(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of VehiclePurchaseOrders for query {}", query);
        Page<VehiclePurchaseOrderDTO> page = vehiclePurchaseOrderService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vehicle-purchase-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
