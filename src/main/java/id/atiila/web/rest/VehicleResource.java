package id.atiila.web.rest;

import java.util.*;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.VehicleService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.VehicleDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Vehicle.
 */
@RestController
@RequestMapping("/api")
public class VehicleResource {

    private final Logger log = LoggerFactory.getLogger(VehicleResource.class);

    private static final String ENTITY_NAME = "vehicle";

    private final VehicleService vehicleService;

    public VehicleResource(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    /**
     * POST  /vehicles : Create a new vehicle.
     *
     * @param vehicleDTO the vehicleDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vehicleDTO, or with status 400 (Bad Request) if the vehicle has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicles")
    @Timed
    public ResponseEntity<VehicleDTO> createVehicle(@RequestBody VehicleDTO vehicleDTO) throws URISyntaxException {
        log.debug("REST request to save Vehicle : {}", vehicleDTO);
        if (vehicleDTO.getIdVehicle() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new vehicle cannot already have an ID")).body(null);
        }
        VehicleDTO result = vehicleService.save(vehicleDTO);
        return ResponseEntity.created(new URI("/api/vehicles/" + result.getIdVehicle()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdVehicle().toString()))
            .body(result);
    }

    /**
     * POST  /vehicles/execute{id}/{param} : Execute Bussiness Process vehicle.
     *
     * @param vehicleDTO the vehicleDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  vehicleDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicles/execute/{id}/{param}")
    @Timed
    public ResponseEntity<VehicleDTO> executedVehicle(@PathVariable Integer id, @PathVariable String param, @RequestBody VehicleDTO vehicleDTO) throws URISyntaxException {
        log.debug("REST request to process Vehicle : {}", vehicleDTO);
        VehicleDTO result = vehicleService.processExecuteData(id, param, vehicleDTO);
        return new ResponseEntity<VehicleDTO>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /vehicles/execute-list{id}/{param} : Execute Bussiness Process vehicle.
     *
     * @param vehicleDTO the vehicleDTO to Execute
     * @param id the id process to Execute
     * @param param the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  vehicleDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vehicles/execute-list/{id}/{param}")
    @Timed
    public ResponseEntity<Set<VehicleDTO>> executedListVehicle(@PathVariable Integer id, @PathVariable String param, @RequestBody Set<VehicleDTO> vehicleDTO) throws URISyntaxException {
        log.debug("REST request to process List Vehicle");
        Set<VehicleDTO> result = vehicleService.processExecuteListData(id, param, vehicleDTO);
        return new ResponseEntity<Set<VehicleDTO>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /vehicles : Updates an existing vehicle.
     *
     * @param vehicleDTO the vehicleDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vehicleDTO,
     * or with status 400 (Bad Request) if the vehicleDTO is not valid,
     * or with status 500 (Internal Server Error) if the vehicleDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vehicles")
    @Timed
    public ResponseEntity<VehicleDTO> updateVehicle(@RequestBody VehicleDTO vehicleDTO) throws URISyntaxException {
        log.debug("REST request to update Vehicle : {}", vehicleDTO);
        if (vehicleDTO.getIdVehicle() == null) {
            return createVehicle(vehicleDTO);
        }
        VehicleDTO result = vehicleService.save(vehicleDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vehicleDTO.getIdVehicle().toString()))
            .body(result);
    }

    /**
     * GET  /vehicles : get all the vehicles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vehicles in body
     */
    @GetMapping("/vehicles")
    @Timed
    public ResponseEntity<List<VehicleDTO>> getAllVehicles(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Vehicles");
        Page<VehicleDTO> page = vehicleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /vehicles/:id : get the "id" vehicle.
     *
     * @param id the id of the vehicleDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vehicleDTO, or with status 404 (Not Found)
     */
    @GetMapping("/vehicles/{id}")
    @Timed
    public ResponseEntity<VehicleDTO> getVehicle(@PathVariable UUID id) {
        log.debug("REST request to get Vehicle : {}", id);
        VehicleDTO vehicleDTO = vehicleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vehicleDTO));
    }

   /**
     * DELETE  /vehicles/:id : delete the "id" vehicle.
     *
     * @param id the id of the vehicleDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vehicles/{id}")
    @Timed
    public ResponseEntity<Void> deleteVehicle(@PathVariable UUID id) {
        log.debug("REST request to delete Vehicle : {}", id);
        vehicleService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/vehicles?query=:query : search for the vehicle corresponding
     * to the query.
     *
     * @param query the query of the vehicle search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vehicles")
    @Timed
    public ResponseEntity<List<VehicleDTO>> searchVehicles(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Vehicles for query {}", query);
        Page<VehicleDTO> page = vehicleService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vehicles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/vehicles/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processVehicles(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process Vehicles ");
        Map<String, Object> result = vehicleService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

}
