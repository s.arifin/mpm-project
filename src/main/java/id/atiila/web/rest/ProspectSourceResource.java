package id.atiila.web.rest;

import com.codahale.metrics.annotation.Timed;
import id.atiila.service.ProspectSourceService;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.ProspectSourceDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProspectSource.
 */
@RestController
@RequestMapping("/api")
public class ProspectSourceResource {

    private final Logger log = LoggerFactory.getLogger(ProspectSourceResource.class);

    private static final String ENTITY_NAME = "prospectSource";

    private final ProspectSourceService prospectSourceService;

    public ProspectSourceResource(ProspectSourceService prospectSourceService) {
        this.prospectSourceService = prospectSourceService;
    }

    /**
     * POST  /prospect-sources : Create a new prospectSource.
     *
     * @param prospectSourceDTO the prospectSourceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new prospectSourceDTO, or with status 400 (Bad Request) if the prospectSource has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prospect-sources")
    @Timed
    public ResponseEntity<ProspectSourceDTO> createProspectSource(@RequestBody ProspectSourceDTO prospectSourceDTO) throws URISyntaxException {
        log.debug("REST request to save ProspectSource : {}", prospectSourceDTO);
        if (prospectSourceDTO.getIdProspectSource() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new prospectSource cannot already have an ID")).body(null);
        }
        ProspectSourceDTO result = prospectSourceService.save(prospectSourceDTO);

        return ResponseEntity.created(new URI("/api/prospect-sources/" + result.getIdProspectSource()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdProspectSource().toString()))
            .body(result);
    }

    /**
     * POST  /prospect-sources/execute : Execute Bussiness Process prospectSource.
     *
     * @param prospectSourceDTO the prospectSourceDTO to Execute
     * @return the ResponseEntity with status Accepted and with body the  prospectSourceDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prospect-sources/execute")
    @Timed
    public ResponseEntity<ProspectSourceDTO> executedProspectSource(@RequestBody ProspectSourceDTO prospectSourceDTO) throws URISyntaxException {
        log.debug("REST request to process ProspectSource : {}", prospectSourceDTO);
        return new ResponseEntity<ProspectSourceDTO>(prospectSourceDTO, null, HttpStatus.ACCEPTED);
    }

    /**
     * PUT  /prospect-sources : Updates an existing prospectSource.
     *
     * @param prospectSourceDTO the prospectSourceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated prospectSourceDTO,
     * or with status 400 (Bad Request) if the prospectSourceDTO is not valid,
     * or with status 500 (Internal Server Error) if the prospectSourceDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/prospect-sources")
    @Timed
    public ResponseEntity<ProspectSourceDTO> updateProspectSource(@RequestBody ProspectSourceDTO prospectSourceDTO) throws URISyntaxException {
        log.debug("REST request to update ProspectSource : {}", prospectSourceDTO);
        if (prospectSourceDTO.getIdProspectSource() == null) {
            return createProspectSource(prospectSourceDTO);
        }
        ProspectSourceDTO result = prospectSourceService.save(prospectSourceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, prospectSourceDTO.getIdProspectSource().toString()))
            .body(result);
    }

    /**
     * GET  /prospect-sources : get all the prospectSources.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of prospectSources in body
     */
    @GetMapping("/prospect-sources")
    @Timed
    public ResponseEntity<List<ProspectSourceDTO>> getAllProspectSources(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ProspectSources");
        Page<ProspectSourceDTO> page = prospectSourceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/prospect-sources");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



    /**
     * GET  /prospect-sources/:id : get the "id" prospectSource.
     *
     * @param id the id of the prospectSourceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the prospectSourceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/prospect-sources/{id}")
    @Timed
    public ResponseEntity<ProspectSourceDTO> getProspectSource(@PathVariable Integer id) {
        log.debug("REST request to get ProspectSource : {}", id);
        ProspectSourceDTO prospectSourceDTO = prospectSourceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(prospectSourceDTO));
    }

    /**
     * DELETE  /prospect-sources/:id : delete the "id" prospectSource.
     *
     * @param id the id of the prospectSourceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/prospect-sources/{id}")
    @Timed
    public ResponseEntity<Void> deleteProspectSource(@PathVariable Integer id) {
        log.debug("REST request to delete ProspectSource : {}", id);
        prospectSourceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/prospect-sources?query=:query : search for the prospectSource corresponding
     * to the query.
     *
     * @param query the query of the prospectSource search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/prospect-sources")
    @Timed
    public ResponseEntity<List<ProspectSourceDTO>> searchProspectSources(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ProspectSources for query {}", query);
        Page<ProspectSourceDTO> page = prospectSourceService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/prospect-sources");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/prospect-sources/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processProspectSource(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException {
        log.debug("REST request to any process ProspectSource ");
        Map<String, Object> result = prospectSourceService.process(request, item);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }


}
