package id.atiila.web.rest;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.codahale.metrics.annotation.Timed;
import id.atiila.service.UnitPreparationService;
import id.atiila.web.rest.errors.BadRequestAlertException;
import id.atiila.web.rest.util.HeaderUtil;
import id.atiila.web.rest.util.PaginationUtil;
import id.atiila.service.dto.UnitPreparationDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UnitPreparation.
 */
@RestController
@RequestMapping("/api")
public class UnitPreparationResource {

    private final Logger log = LoggerFactory.getLogger(UnitPreparationResource.class);

    private static final String ENTITY_NAME = "unitPreparation";

    private final UnitPreparationService unitPreparationService;

    public UnitPreparationResource(UnitPreparationService unitPreparationService) {
        this.unitPreparationService = unitPreparationService;
    }

    /**
     * POST  /unit-preparations : Create a new unitPreparation.
     *
     * @param unitPreparationDTO the unitPreparationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new unitPreparationDTO, or with status 400 (Bad Request) if the unitPreparation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-preparations")
    @Timed
    public ResponseEntity<UnitPreparationDTO> createUnitPreparation(@RequestBody UnitPreparationDTO unitPreparationDTO) throws URISyntaxException {
        log.debug("REST request to save UnitPreparation : {}", unitPreparationDTO);
        if (unitPreparationDTO.getIdSlip() != null) {
            throw new BadRequestAlertException("A new unitPreparation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UnitPreparationDTO result = unitPreparationService.save(unitPreparationDTO);
        return ResponseEntity.created(new URI("/api/unit-preparations/" + result.getIdSlip()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdSlip().toString()))
            .body(result);
    }

    /**
     * POST  /unit-preparations/process : Execute Bussiness Process unitPreparation.
     *
     * @return the ResponseEntity with status Accepted and with body the  String
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-preparations/process")
    @Timed
    public ResponseEntity<Map<String, Object>> processUnitPreparation(HttpServletRequest request, @RequestBody Map<String, Object> item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process UnitPreparation ");
        Map<String, Object> result = unitPreparationService.process(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /unit-preparations/execute : Execute Bussiness Process unitPreparation.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntity with status Accepted and with body the  unitPreparationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-preparations/execute")
    @Timed
    public ResponseEntity<Map<String, Object>> executedUnitPreparation(HttpServletRequest request, @RequestBody UnitPreparationDTO item) throws URISyntaxException, InterruptedException  {
        log.debug("REST request to any process UnitPreparation");
        Map<String, Object> result = unitPreparationService.processExecuteData(request, item);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * POST  /unit-preparations/execute-list : Execute Bussiness Process unitPreparation.
     *
     * @params params the paramater process to Execute
     * @return the ResponseEntities with status Accepted and with body the  unitPreparationDTO
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unit-preparations/execute-list")
    @Timed
    public ResponseEntity<Map<String, Object>> executedListUnitPreparation(HttpServletRequest request, @RequestBody Set<UnitPreparationDTO> items) throws URISyntaxException, InterruptedException {
        log.debug("REST request to process List UnitPreparation");
        Map<String, Object> result = unitPreparationService.processExecuteListData(request, items);
        TimeUnit.MILLISECONDS.sleep(100);
        return new ResponseEntity<Map<String, Object>>(result, null, HttpStatus.OK);
    }

    /**
     * PUT  /unit-preparations : Updates an existing unitPreparation.
     *
     * @param unitPreparationDTO the unitPreparationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated unitPreparationDTO,
     * or with status 400 (Bad Request) if the unitPreparationDTO is not valid,
     * or with status 500 (Internal Server Error) if the unitPreparationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/unit-preparations")
    @Timed
    public ResponseEntity<UnitPreparationDTO> updateUnitPreparation(@RequestBody UnitPreparationDTO unitPreparationDTO) throws URISyntaxException {
        log.debug("REST request to update UnitPreparation : {}", unitPreparationDTO);
        if (unitPreparationDTO.getIdSlip() == null) {
            return createUnitPreparation(unitPreparationDTO);
        }
        UnitPreparationDTO result = unitPreparationService.save(unitPreparationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, unitPreparationDTO.getIdSlip().toString()))
            .body(result);
    }

    /**
     * GET  /unit-preparations : get all the unitPreparations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of unitPreparations in body
     */
    @GetMapping("/unit-preparations")
    @Timed
    public ResponseEntity<List<UnitPreparationDTO>> getAllUnitPreparations(Pageable pageable) {
        log.debug("REST request to get a page of UnitPreparations");
        Page<UnitPreparationDTO> page = unitPreparationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-preparations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/unit-preparations/filterBy")
    @Timed
    public ResponseEntity<List<UnitPreparationDTO>> getAllFilteredUnitPreparations(HttpServletRequest request, Pageable pageable) {
        log.debug("REST request to get a page of UnitPreparations");
        Page<UnitPreparationDTO> page = unitPreparationService.findFilterBy(request, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unit-preparations/filterBy");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /unit-preparations/:id : get the "id" unitPreparation.
     *
     * @param id the id of the unitPreparationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the unitPreparationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/unit-preparations/{id}")
    @Timed
    public ResponseEntity<UnitPreparationDTO> getUnitPreparation(@PathVariable UUID id) {
        log.debug("REST request to get UnitPreparation : {}", id);
        UnitPreparationDTO unitPreparationDTO = unitPreparationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(unitPreparationDTO));
    }

    /**
     * DELETE  /unit-preparations/:id : delete the "id" unitPreparation.
     *
     * @param id the id of the unitPreparationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/unit-preparations/{id}")
    @Timed
    public ResponseEntity<Void> deleteUnitPreparation(@PathVariable UUID id) {
        log.debug("REST request to delete UnitPreparation : {}", id);
        unitPreparationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/unit-preparations?query=:query : search for the unitPreparation corresponding
     * to the query.
     *
     * @param query the query of the unitPreparation search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/unit-preparations")
    @Timed
    public ResponseEntity<List<UnitPreparationDTO>> searchUnitPreparations(HttpServletRequest request, @RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of UnitPreparations for query {}", query);
        Page<UnitPreparationDTO> page = unitPreparationService.search(request, query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/unit-preparations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @PostMapping("/unit-preparations/set-status/{id}")
    @Timed
    public ResponseEntity< UnitPreparationDTO> setStatusUnitPreparation(@PathVariable Integer id, @RequestBody UnitPreparationDTO dto) throws URISyntaxException, InterruptedException {
        log.debug("REST request to set Status UnitPreparation : {}", dto);
        UnitPreparationDTO r = unitPreparationService.changeUnitPreparationStatus(dto, id);
        TimeUnit.MILLISECONDS.sleep(500);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(r));
    }

}
