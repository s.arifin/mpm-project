package id.atiila.service;

import id.atiila.domain.*;
import id.atiila.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ShipmentUtils {

    @Autowired
    private ShipmentItemRepository shipmentItemRepository;

    @Autowired
    private ItemIssuanceRepository itemIssuanceRepository;

    @Autowired
    private ShipmentBillingItemRepository shipmentBillingItemRepository;

    public ShipmentBillingItem getShipmentBillingItem(ShipmentItem s, BillingItem b, Double qty) {
        ShipmentBillingItem r = shipmentBillingItemRepository.getShipmentBillingItem(s, b);
        if (r == null) {
            r = new ShipmentBillingItem();
            r.setBillingItem(b);
            r.setShipmentItem(s);
            r.setQty(qty);
            r = shipmentBillingItemRepository.saveAndFlush(r);
        }
        return r;
    }

    public ShipmentItem addItem(Shipment shipment, OrderItem o, Double qty) {
        //Buat Shipment Item
        ShipmentItem shi = new ShipmentItem();
        shi.setQty(qty);
        shi.setIdProduct(o.getIdProduct());
        shi.setIdFeature(o.getIdFeature());
        shi.setContentDescription(o.getItemDescription());
        shi.setItemDescription(o.getItemDescription());
        shi.setShipment(shipment);
        shi = shipmentItemRepository.save(shi);
        return shi;
    }

    public ItemIssuance buildIssuance(ShipmentItem shi, PickingSlip pis) {
        ItemIssuance issuance = new ItemIssuance();
        issuance.setInventoryItem(pis.getInventoryItem());
        issuance.setPicking(pis);
        issuance.setQty(pis.getQty());
        issuance.setShipmentItem(shi);
        issuance = itemIssuanceRepository.saveAndFlush(issuance);
        return issuance;
    }

}
