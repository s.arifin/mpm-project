package id.atiila.service;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IAtomicLong;
import com.hazelcast.core.ILock;
import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.base.XlsxUtils;
import id.atiila.base.event.UploadDataEvent;
import id.atiila.config.tenant.TenantContext;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.CustomerSearchRepository;
import id.atiila.route.OrganizationRoute;
import id.atiila.service.dto.*;
import id.atiila.service.mapper.CustomerMapper;
import id.atiila.service.mapper.PersonalCustomerMapper;
import org.apache.camel.ProducerTemplate;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.elasticsearch.common.Strings;
import org.jxls.reader.ReaderConfig;
import org.olap4j.impl.ArrayMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.xml.sax.SAXException;

import javax.annotation.PostConstruct;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing Customer.
 * BeSmart Team
 */

@Service
@Transactional
public class CustomerService {

    private final Logger log = LoggerFactory.getLogger(CustomerService.class);

    private final CustomerRepository<Customer> customerRepository;

    private final CustomerMapper customerMapper;

    private final CustomerSearchRepository customerSearchRepository;

    @Autowired
    private BillToService billToService;

    @Autowired
    private GeneralUploadRepository generalUploadRepository;

    @Autowired
    private CustomerUtils customerUtils;

    @Autowired
    private ProducerTemplate template;

    @Autowired
    private MasterNumberingService masterNumberingService;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private MasterNumberingRepository masterNumberingRepository;

    @Autowired
    private HazelcastInstance hz;

    @Autowired
    private PersonalCustomerRepository personalCustomerRepository;

    public CustomerService(CustomerRepository customerRepository, CustomerMapper customerMapper, CustomerSearchRepository customerSearchRepository) {
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
        this.customerSearchRepository = customerSearchRepository;
    }

    /**
     * Save a customer.
     *
     * @param customerDTO the entity to save
     * @return the persisted entity
     */
    public CustomerDTO save(CustomerDTO customerDTO) {
        log.debug("Request to save Customer : {}", customerDTO);
        Customer customer = customerMapper.toEntity(customerDTO);
        customer = customerRepository.save(customer);
        CustomerDTO result = customerMapper.toDto(customer);
        customerSearchRepository.save(customer);
        return result;
    }

    /**
     * Get all the customers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CustomerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Customers");
        return customerRepository.findAll(pageable)
            .map(customerMapper::toDto);
    }

    /**
     * Get one customer by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CustomerDTO findOne(String id) {
        log.debug("Request to get Customer : {}", id);
        Customer customer = customerRepository.findOne(id);
        return customerMapper.toDto(customer);
    }

    @Transactional(readOnly = true)
    public BillToDTO findBillTo(String id) {
        log.debug("Request to get Customer : {}", id);
        Customer customer = customerRepository.findOne(id);
        return billToService.findByParty(customer.getParty());
    }

    /**
     * Delete the customer by id.
     *
     * @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Customer : {}", id);
        customerRepository.delete(id);
        customerSearchRepository.delete(id);
    }

    /**
     * Search for the customer corresponding to the query.
     *
     * @param query    the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CustomerDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Customers for query {}", query);
        Page<Customer> result = customerSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(customerMapper::toDto);
    }

    @Transactional(readOnly = true)
    public CustomerDTO processExecuteData(Integer id, String param, CustomerDTO dto) {
        CustomerDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
        }
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void findOrCreateCustomerCode(String idCustomer, Internal internal){
        log.debug("START find or create customer code");
        Customer c = customerRepository.findByIdCustomer(idCustomer);
        PersonalCustomer cp = personalCustomerRepository.findByIdCustomer(idCustomer);
        Person person = null;
        if(cp != null) {
            person = cp.getPerson() ;
            if (person.getPersonalIdNumber() == null || person.getPersonalIdNumber().length() <= 0) {
                throw new DmsException("NIK Customer Kosong. Mohon Untuk di Isi");
            }
            if (person.getPersonalIdNumber() != null) {
                if (c.getIdMPM() == null) {
                    String idMpm = nextIdMPM(internal);
                    List<Customer> cust = customerRepository.findCustByIdMPM(idMpm);
                    if (!cust.isEmpty()) {
                        findOrCreateCustomerCode(idCustomer, internal);
                    }
                    c.setIdMPM(idMpm);
                    customerRepository.save(c);
                }
            }
            }else {
                String idMpm = nextIdMPM(internal);
                List<Customer> cust = customerRepository.findCustByIdMPM(idMpm);
                if (!cust.isEmpty()){
                    findOrCreateCustomerCode(idCustomer, internal);
                }
                c.setIdMPM(idMpm);
                customerRepository.save(c);
        }
    }

    @Transactional(readOnly = true)
    public Set<CustomerDTO> processExecuteListData(Integer id, String param, Set<CustomerDTO> dto) {
        Set<CustomerDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
        }
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void processCustomer(Internal internal, PersonalCustomer customer) {
        customerUtils.buildPersonalCustomer(internal, customer);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void migrateData(Internal internal, InputStream initFile) throws IOException, InvalidFormatException, SAXException {
        DefaultResourceLoader loader = new DefaultResourceLoader();
        InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/customer.xml").getInputStream());

        if (templateXML != null && initFile != null) {
            log.debug("START extract data customer");

            // ReaderConfig.getInstance().setSkipErrors(true);
            ReaderConfig.getInstance().setUseDefaultValuesForPrimitiveTypes( true );

            List<CustomCustomerDTO> customers = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
            for (CustomCustomerDTO o : customers) {

                if (o.getPerson().getPersonalIdNumber() == null) {
                    log.debug("NIK kosong dengan nama " + o.getPerson().getFirstName() + " tidak di masukkan ke dalam database");
                } else {
                    // Assert.isTrue(o.getIdMPM() == null, "id MPM null " + o.getIdMPM());
                    if (o.getPerson().getReligionTypeId().equals(0)) o.getPerson().setReligionTypeId(98);
                    Map<String, Object> headers = new ArrayMap<>();
                    headers.putIfAbsent("tenant", TenantContext.getCurrentTenant());
                    headers.putIfAbsent("internal", internal.getIdInternal());

                    template.sendBodyAndHeaders(PersonalCustomerService.QUEU_APPEND, o, headers);
                    log.info("Add Customer: " + o.toString());
                }
            }
        }
    }

    @EventListener
    @Transactional
    public void doUpload(UploadDataEvent e) throws InvalidFormatException, SAXException, IOException {
        if (e.getPurposeType().equals(BaseConstants.PURP_TYPE_INIT_CUSTOMER) && e.getGeneralUpload() != null && e.getGeneralUpload().getInternal() != null) {
            GeneralUpload g = e.getGeneralUpload();
            migrateData(g.getInternal(), new ByteArrayInputStream(g.getValue()));
            e.getGeneralUpload().setStatus(BaseConstants.STATUS_COMPLETED);
            generalUploadRepository.save(g);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    protected MasterNumbering getNumbering(String idTag, String idValue, Long startFrom) {
        MasterNumbering r = masterNumberingRepository.findOneByIdTagAndIdValue(idTag, idValue);
        if (r == null) {
            r = new MasterNumbering();
            r.setIdTag(idTag);
            r.setIdValue(idValue);
            r.setNextValue(startFrom);
            r.setNextValue(0l);
            r = masterNumberingRepository.saveAndFlush(r);
        }
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Long nextValue(String idTag, String idValue) {
        return nextValue(idTag, idValue, 0l);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public String nextIdMPM(Internal internal){
//        Assert.isTrue(internal == null, "Internal kosong !");
        int year = ZonedDateTime.now().getYear();
        String r = Strings.padStart(nextValue("idcustomer", internal.getIdDealerCode() + "-" + year).toString(), 5, '0');
        return r + "-" + internal.getIdDealerCode() + "-" + year;
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public Long nextValue(String idTag, String idValue, Long startFrom) {
        long result = 0l;
        IAtomicLong number = null;
        ILock lock = hz.getLock(idTag + "-" + idValue);
        lock.lock();
        try {
            number = hz.getAtomicLong(TenantContext.getCurrentTenant() + "numbering" + idTag + "-" + idValue);
            if (number.get() == 0) {
                MasterNumbering numb = getNumbering(idTag, idValue, startFrom);
                number.set(numb.getNextValue());
            }
            result = number.get();
            MasterNumbering numb = getNumbering(idTag, idValue, startFrom);
            numb.setNextValue(result + 1);
            masterNumberingRepository.saveAndFlush(numb);
        } finally {
            lock.unlock();
            number.set(0l);
        }
        return result;
    }

}
