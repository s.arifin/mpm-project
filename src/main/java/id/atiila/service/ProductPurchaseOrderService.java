package id.atiila.service;

import id.atiila.domain.Internal;
import id.atiila.domain.ProductPurchaseOrder;
import id.atiila.repository.ProductPurchaseOrderRepository;
import id.atiila.repository.search.ProductPurchaseOrderSearchRepository;
import id.atiila.service.dto.ProductPurchaseOrderDTO;
import id.atiila.service.mapper.ProductPurchaseOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ProductPurchaseOrder.
 * BeSmart Team
 */

@Service
@Transactional
public class ProductPurchaseOrderService {

    private final Logger log = LoggerFactory.getLogger(ProductPurchaseOrderService.class);

    private final ProductPurchaseOrderRepository productPurchaseOrderRepository;

    private final ProductPurchaseOrderMapper productPurchaseOrderMapper;

    private final ProductPurchaseOrderSearchRepository productPurchaseOrderSearchRepository;

    @Autowired
    private MasterNumberingService numbering;

    @Autowired
    private PartyUtils partyUtils;

    public ProductPurchaseOrderService(ProductPurchaseOrderRepository productPurchaseOrderRepository, ProductPurchaseOrderMapper productPurchaseOrderMapper, ProductPurchaseOrderSearchRepository productPurchaseOrderSearchRepository) {
        this.productPurchaseOrderRepository = productPurchaseOrderRepository;
        this.productPurchaseOrderMapper = productPurchaseOrderMapper;
        this.productPurchaseOrderSearchRepository = productPurchaseOrderSearchRepository;
    }

    /**
     * Save a productPurchaseOrder.
     *
     * @param productPurchaseOrderDTO the entity to save
     * @return the persisted entity
     */
    public ProductPurchaseOrderDTO save(ProductPurchaseOrderDTO productPurchaseOrderDTO) {
        log.debug("Request to save ProductPurchaseOrder : {}", productPurchaseOrderDTO);
        ProductPurchaseOrder productPurchaseOrder = productPurchaseOrderMapper.toEntity(productPurchaseOrderDTO);
        if (productPurchaseOrder.getOrderNumber() == null && productPurchaseOrder.getInternal() != null) {
            productPurchaseOrder.setOrderNumber(numbering.buildPONumber(productPurchaseOrder.getInternal()));
        }
        productPurchaseOrder = productPurchaseOrderRepository.save(productPurchaseOrder);
        ProductPurchaseOrderDTO result = productPurchaseOrderMapper.toDto(productPurchaseOrder);
        productPurchaseOrderSearchRepository.save(productPurchaseOrder);
        return result;
    }

    /**
     * Get all the productPurchaseOrders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductPurchaseOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductPurchaseOrders");
        Internal intr = partyUtils.getCurrentInternal();
        return productPurchaseOrderRepository.findActiveProductPurchaseOrder(intr, pageable)
            .map(productPurchaseOrderMapper::toDto);
    }

    /**
     * Get one productPurchaseOrder by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ProductPurchaseOrderDTO findOne(UUID id) {
        log.debug("Request to get ProductPurchaseOrder : {}", id);
        ProductPurchaseOrder productPurchaseOrder = productPurchaseOrderRepository.findOne(id);
        return productPurchaseOrderMapper.toDto(productPurchaseOrder);
    }

    /**
     * Delete the productPurchaseOrder by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ProductPurchaseOrder : {}", id);
        productPurchaseOrderRepository.delete(id);
        productPurchaseOrderSearchRepository.delete(id);
    }

    /**
     * Search for the productPurchaseOrder corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductPurchaseOrderDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of ProductPurchaseOrders for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idVendor = request.getParameter("idVendor");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");

        if (filterName != null) {
        }
        Page<ProductPurchaseOrder> result = productPurchaseOrderSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(productPurchaseOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ProductPurchaseOrderDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ProductPurchaseOrderDTO");
        String idVendor = request.getParameter("idVendor");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");

        return productPurchaseOrderRepository.findByParams(idVendor, idInternal, idBillTo, pageable)
            .map(productPurchaseOrderMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public ProductPurchaseOrderDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        ProductPurchaseOrderDTO r = null;
        return r;
    }

    @Transactional
    public Set<ProductPurchaseOrderDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<ProductPurchaseOrderDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public ProductPurchaseOrderDTO changeProductPurchaseOrderStatus(ProductPurchaseOrderDTO dto, Integer id) {
        if (dto != null) {
			ProductPurchaseOrder e = productPurchaseOrderRepository.findOne(dto.getIdOrder());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        productPurchaseOrderSearchRepository.delete(dto.getIdOrder());
                        break;
                    default:
                        productPurchaseOrderSearchRepository.save(e);
                }
				productPurchaseOrderRepository.save(e);
			}
		}
        return dto;
    }
}
