package id.atiila.service;

import id.atiila.domain.Motor;
import id.atiila.domain.WorkProductRequirement;
import id.atiila.repository.WorkProductRequirementRepository;
import id.atiila.repository.search.WorkProductRequirementSearchRepository;
import id.atiila.service.dto.WorkProductRequirementDTO;
import id.atiila.service.mapper.WorkProductRequirementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Service Implementation for managing WorkProductRequirement.
 * BeSmart Team
 */

@Service
@Transactional
public class WorkProductRequirementService {

    private final Logger log = LoggerFactory.getLogger(WorkProductRequirementService.class);

    private final WorkProductRequirementRepository workProductRequirementRepository;

    private final WorkProductRequirementMapper workProductRequirementMapper;

    private final WorkProductRequirementSearchRepository workProductRequirementSearchRepository;

    public WorkProductRequirementService(WorkProductRequirementRepository workProductRequirementRepository, WorkProductRequirementMapper workProductRequirementMapper, WorkProductRequirementSearchRepository workProductRequirementSearchRepository) {
        this.workProductRequirementRepository = workProductRequirementRepository;
        this.workProductRequirementMapper = workProductRequirementMapper;
        this.workProductRequirementSearchRepository = workProductRequirementSearchRepository;
    }

    /**
     * Save a workProductRequirement.
     *
     * @param workProductRequirementDTO the entity to save
     * @return the persisted entity
     */
    public WorkProductRequirementDTO save(WorkProductRequirementDTO workProductRequirementDTO) {
        log.debug("Request to save WorkProductRequirement : {}", workProductRequirementDTO);
        WorkProductRequirement workProductRequirement = workProductRequirementMapper.toEntity(workProductRequirementDTO);
        workProductRequirement = workProductRequirementRepository.save(workProductRequirement);
        WorkProductRequirementDTO result = workProductRequirementMapper.toDto(workProductRequirement);
        workProductRequirementSearchRepository.save(workProductRequirement);
        return result;
    }

    /**
     *  Get all the workProductRequirements.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WorkProductRequirementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WorkProductRequirements");
        return workProductRequirementRepository.findAll(pageable)
            .map(workProductRequirementMapper::toDto);
    }

    /**
     *  Get one workProductRequirement by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public WorkProductRequirementDTO findOne(UUID id) {
        log.debug("Request to get WorkProductRequirement : {}", id);
        WorkProductRequirement workProductRequirement = workProductRequirementRepository.findOne(id);
        return workProductRequirementMapper.toDto(workProductRequirement);
    }

    /**
     *  Delete the  workProductRequirement by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete WorkProductRequirement : {}", id);
        workProductRequirementRepository.delete(id);
        workProductRequirementSearchRepository.delete(id);
    }

    /**
     * Search for the workProductRequirement corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WorkProductRequirementDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WorkProductRequirements for query {}", query);
        Page<WorkProductRequirement> result = workProductRequirementSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(workProductRequirementMapper::toDto);
    }

    public WorkProductRequirementDTO processExecuteData(WorkProductRequirementDTO dto, Integer id) {
        WorkProductRequirementDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        workProductRequirementSearchRepository.deleteAll();
        List<WorkProductRequirement> workProductRequirements = workProductRequirementRepository.findAll();
        for (WorkProductRequirement m: workProductRequirements) {
            workProductRequirementSearchRepository.save(m);
            log.debug("Data work product requirement save !...");
        }
    }

}
