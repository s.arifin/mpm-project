package id.atiila.service;

import id.atiila.domain.ShipTo;
import id.atiila.domain.ShipToBaseService;
import id.atiila.repository.ShipToRepository;
import id.atiila.repository.search.ShipToSearchRepository;
import id.atiila.service.dto.ShipToDTO;
import id.atiila.service.mapper.ShipToMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ShipTo.
 * BeSmart Team
 */

@Service
@Transactional
public class ShipToService extends ShipToBaseService {

    private final Logger log = LoggerFactory.getLogger(ShipToService.class);

    public static final String QUEU_REMOVE = "removeShipTo";

    private final ShipToRepository shipToRepository;

    private final ShipToMapper shipToMapper;

    private final ShipToSearchRepository shipToSearchRepository;

    public ShipToService(ShipToRepository shipToRepository, ShipToMapper shipToMapper, ShipToSearchRepository shipToSearchRepository) {
        this.shipToRepository = shipToRepository;
        this.shipToMapper = shipToMapper;
        this.shipToSearchRepository = shipToSearchRepository;
    }

    /**
     * Save a shipTo.
     *
     * @param shipToDTO the entity to save
     * @return the persisted entity
     */
    public ShipToDTO save(ShipToDTO shipToDTO) {
        log.debug("Request to save ShipTo : {}", shipToDTO);
        ShipTo shipTo = shipToMapper.toEntity(shipToDTO);
        shipTo = shipToRepository.save(shipTo);
        ShipToDTO result = shipToMapper.toDto(shipTo);
        shipToSearchRepository.save(shipTo);
        return result;
    }

    /**
     * Get all the shipTos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipToDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShipTos");
        return shipToRepository.findAll(pageable)
            .map(shipToMapper::toDto);
    }

    /**
     * Get one shipTo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ShipToDTO findOne(String id) {
        log.debug("Request to get ShipTo : {}", id);
        ShipTo shipTo = shipToRepository.findOne(id);
        return shipToMapper.toDto(shipTo);
    }

    /**
     * Delete the shipTo by id.
     *
     * @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete ShipTo : {}", id);
        shipToRepository.delete(id);
        shipToSearchRepository.delete(id);
    }

    /**
     * Search for the shipTo corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipToDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of ShipTos for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idParty = request.getParameter("idParty");

        if (idParty != null) {
            q.withQuery(matchQuery("party.idParty", idParty));
        }

        Page<ShipTo> result = shipToSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(shipToMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ShipToDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ShipToDTO");
        String idParty = request.getParameter("idParty");
        String idRoleType = request.getParameter("idRoleType");

        if (idParty != null) {
            return shipToRepository.queryByIdParty(UUID.fromString(idParty), pageable).map(shipToMapper::toDto);
        } else if (idRoleType != null) {
            return shipToRepository.queryByIdRoleType(Integer.valueOf(idRoleType), pageable).map(shipToMapper::toDto);
        }

        return shipToRepository.queryNothing(pageable).map(shipToMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, ShipToDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<ShipToDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @JmsListener(destination = QUEU_REMOVE)
    public void removeBillTo(String id) {
        shipToRepository.delete(id);
        log.info("Remove Personal Customer : " + id);
    }
}
