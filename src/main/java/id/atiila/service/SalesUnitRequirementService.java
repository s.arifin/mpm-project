package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import id.atiila.service.mapper.ApprovalMapper;
import id.atiila.service.pto.OrderPTO;
import id.atiila.service.pto.SalesUnitRequirementPTO;

import id.atiila.base.DmsException;
import id.atiila.base.executeprocess.SalesUnitRequirementConstants;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.service.dto.*;
import id.atiila.service.mapper.PersonMapper;
import id.atiila.service.util.RandomUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.atiila.base.BaseConstants;
import id.atiila.repository.search.SalesUnitRequirementSearchRepository;
import id.atiila.service.mapper.SalesUnitRequirementMapper;

import javax.servlet.http.HttpServletRequest;

/**
 * Service Implementation for managing SalesUnitRequirement.
 * BeSmart Team
 */

@Service
@Transactional
public class SalesUnitRequirementService extends RequirementBaseService {

    private final Logger log = LoggerFactory.getLogger(SalesUnitRequirementService.class);

    private final SalesUnitRequirementRepository salesUnitRequirementRepository;

    private final SalesUnitRequirementMapper salesUnitRequirementMapper;

    private final SalesUnitRequirementSearchRepository salesUnitRequirementSearchRepository;


    @Autowired
    private ProspectPersonRepository prospectPersonRepository;

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private ProspectOrganizationRepository prospectOrganizationRepository;

    @Autowired
    private ProspectOrganizationDetailsRepository prospectOrganizationDetailsRepository;

    @Autowired
    private VehicleSalesOrderService vsoService;

    @Autowired
    private BillToService csBillToSrv;

    @Autowired
    private ShipToService csShipToSrv;

    @Autowired
    private MasterNumberingService masterNumberingService;

    @Autowired
    private InternalRepository<Internal> internalRepository;

    @Autowired
    private ApprovalService approvalService;

    @Autowired
    private OrganizationCustomerRepository organizationCustomerRepository;

    @Autowired
    private SalesmanRepository salesmanRepository;

    @Autowired
    private BillToRepository billToRepository;

    @Autowired
    private PersonalCustomerRepository personalCustomerRepository;

    @Autowired
    private PartyRepository partyRepository;

    @Autowired
    private RequirementTypeRepository requirementTypeRepository;

    @Autowired
    private ProspectRepository prospectRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PersonService personService;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private ApprovalRepository approvalRepository;

    @Autowired
    private DateUtils dateUtils;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private VehicleCustomerRequestRepository vehicleCustomerRequestRepository;

    @Autowired
    private LeasingCompanyRepository leasingCompanyRepository;

    @Autowired
    private ApprovalMapper approvalMapper;

    @Autowired
    private RequirementRepository requirementRepository;

    @Autowired
    private MailService mailService;

    @Autowired
    private RuleHotItemRepository ruleHotItemRepository;

    @Autowired
    private ReceiptRepository receiptRepository;

    @Autowired
    private FeatureRepository featureRepository;

    @Autowired
    private ProspectService prospectService;

    @Autowired
    private SalesBookingRepository salesBookingRepository;

    @Autowired
    private RuleIndentRepository ruleIndentRepository;

    @Autowired
    private FacilityRepository facilityRepository;

    @Autowired
    private SalesBookingService salesBookingService;

    public SalesUnitRequirementService(
        SalesUnitRequirementRepository salesUnitRequirementRepository,
        SalesUnitRequirementMapper salesUnitRequirementMapper,
        SalesUnitRequirementSearchRepository salesUnitRequirementSearchRepository
    ) {

        this.salesUnitRequirementRepository = salesUnitRequirementRepository;
        this.salesUnitRequirementMapper = salesUnitRequirementMapper;
        this.salesUnitRequirementSearchRepository = salesUnitRequirementSearchRepository;
    }

    @Transactional(readOnly = true)
    public Page<SalesUnitRequirementDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered SalesUnitRequirementDTO");
        String filterBy = request.getParameter("filterBy");
        String idRequest = request.getParameter("idRequest");
        String idInternal = request.getParameter("idInternal");
        Internal intr = partyUtils.getCurrentInternal();
        String queryFor = request.getParameter("queryFor");
        String data = request.getParameter("data");
        String idProduct = request.getParameter("idProduct");
        String idbilling = request.getParameter("idbilling");
        String datalike = '%'+data+'%';
        log.debug("param like = " + datalike);

        if (idbilling != null && idProduct != null){
            log.debug("bill and product daad " + idbilling + "  " + idProduct);
            return salesUnitRequirementRepository.findOrderItemByIdBillAndIdProduct(UUID.fromString(idbilling), idProduct, pageable).map(salesUnitRequirementMapper::toDto);
        }

        if ("approvalLeasing".equalsIgnoreCase(queryFor) && data != null) {
            log.debug("param like search = " + datalike);
            return salesUnitRequirementRepository.searchActiveLeasingApproval(intr.getIdInternal(), datalike, pageable)
                .map(salesUnitRequirementMapper::toDto)
                .map(this::assignOtherData);
        }

        if (idRequest != null || true) {
            UUID idrequest = UUID.fromString(idRequest);
            return salesUnitRequirementRepository.queryByIdRequest(idrequest, pageable)
                .map(salesUnitRequirementMapper::toDto);
        }

        return salesUnitRequirementRepository.queryNothing(pageable)
            .map(salesUnitRequirementMapper::toDto);
    }

    public Page<SalesUnitRequirementDTO> findAllByIdRequest(String idinternal, UUID idrequest, Pageable pageable) {
        log.debug("START find all Requirement by idrequest and idinternal " + idinternal + idrequest);
        return salesUnitRequirementRepository.findAllByIdRequest(idrequest, idinternal, pageable)
            .map(salesUnitRequirementMapper::toDto);
    }

    public Page<SalesUnitRequirementDTO> findAllByApprovalDIscountAndPelanggaranWilayah(String idinternal, Integer idstatustype, UUID idprospect, Pageable pageable) {
        log.debug("Start find all by approval discount, pelanggaran wilayah by sales");

        if (idstatustype == BaseConstants.STATUS_APPROVED || idstatustype == BaseConstants.STATUS_NOT_REQUIRED) {
            log.debug("cari SUR yang status approval pelanggaran wilayah dan diskon di approve / not required");

            return salesUnitRequirementRepository.findSalesUnitRequirementByStatusApprovalDiscountAndPelanggaranWilayahByProspect(idstatustype, idinternal, idprospect, pageable)
                .map(salesUnitRequirementMapper::toDto);
        } else {
            log.debug("cari SUR yang status approval pelanggaran wilayah atau Diskon sudah di approve / tidak di approve by prospect ");

            return salesUnitRequirementRepository.findSalesUnitRequirementByStatusApprovalDiscountOrPelanggaranWilayahByProspect(idstatustype, idinternal, idprospect, pageable)
                .map(salesUnitRequirementMapper::toDto);
        }
    }

//    private SalesUnitRequirementDTO mappingSurWithCustomerName(SalesUnitRequirementDTO salesUnitRequirementDTO){
//        log.debug("START MAPPING SUR DTO");
//
//        SalesUnitRequirementDTO dto = salesUnitRequirementDTO;
//        if (checkSalesUnitRequirementPersonOrOrganization(dto) == "person"){
//            Customer customer = customerRepository.findByIdCustomer(dto.getCustomerId());
//            Person person = personRepository.findOne(customer.getParty().getIdParty());
//
//            dto.setPersonCustomer(personService.findOne(person.getIdParty()));
//        }
//        return dto;
//    }

    public Page<SalesUnitRequirementDTO> findByApprovalActiveForLeasing(UUID idleasingcompany, Pageable pageable) {
        log.debug("Start find all by active approval for leasing");
        return salesUnitRequirementRepository.findByApprovalActiveForLeasing(idleasingcompany, pageable)
            .map(salesUnitRequirementMapper::toDto);
    }

    public Page<SalesUnitRequirementDTO> findByWaitingApprovalCredit(String idinternal, Pageable pageable) {
        log.debug("Start find all by active approval");
        return salesUnitRequirementRepository.findByApprovalForCredit(idinternal, pageable)
            .map(salesUnitRequirementMapper::toDto);
    }

    public Page<SalesUnitRequirementDTO> findAllByApprovalDiscountAndPelanggaranWilayahBySales(String idInternal, Integer idstatus, Pageable pageable) {
        log.debug("Start find all by approval discount and pelanggaran wilayah by sales");

        Salesman salesman = partyUtils.getCurrentSalesman();
        if (salesman != null) {
            UUID idsalesman = salesman.getIdPartyRole();
            if (idstatus == BaseConstants.STATUS_APPROVED || idstatus == BaseConstants.STATUS_NOT_REQUIRED) {
                log.debug("cari yang discount dan pelanggaran wilayah di approve / not required");

                return salesUnitRequirementRepository.findByApprovalDiscountAndPelanggaranWilayahBySales(idstatus, idInternal, idsalesman, pageable)
                    .map(salesUnitRequirementMapper::toDto);
            } else {
                log.debug("cari yang discount atau pelanggaran wilayah di approve / tidak");

                return salesUnitRequirementRepository.findByApprovalDiscountOrPelanggaranWilayahBySales(idstatus, idInternal, idsalesman, pageable)
                    .map(salesUnitRequirementMapper::toDto);
            }
        } else {
            throw new DmsException("Anda bukan sales dan tidak di ijinkan untuk melihat data ini");
        }
    }

    /**
     * Save a salesUnitRequirement.
     *
     * @param salesUnitRequirementDTO the entity to save
     * @return the persisted entity
     */
    public SalesUnitRequirementDTO save(SalesUnitRequirementDTO salesUnitRequirementDTO) {
        log.debug("Request to save SalesUnitRequirement : {}", salesUnitRequirementDTO);
        SalesUnitRequirement salesUnitRequirement = salesUnitRequirementMapper.toEntity(salesUnitRequirementDTO);
        salesUnitRequirement.setLeasingnote(salesUnitRequirementDTO.getLeasingnote());

        if (salesUnitRequirement.getIdRequest() != null) {
            log.debug("Check For GC");
            String a = checkSTNKGC(salesUnitRequirement.getIdRequest());
            VehicleCustomerRequest vcr = vehicleCustomerRequestRepository.findOne(salesUnitRequirement.getIdRequest());
            Salesman sales = salesmanRepository.findOne(vcr.getIdSalesman());
            if (a == "organization") {
                salesUnitRequirement.setPersonOwner(null);
            } else if (a == "person") {
                salesUnitRequirement.setOrganizationOwner(null);
            }
            if (vcr.getRequestType().getIdRequestType() == BaseConstants.REQUEST_TYPE_VSO_DIRECT) {
                salesUnitRequirement.setSalesman(sales);
            }

            if (vcr.getRequestType().getIdRequestType() == BaseConstants.REQUEST_TYPE_VSO_GP) {
                salesUnitRequirement.setSalesman(sales);
            }

            if (vcr.getRequestType().getIdRequestType() == BaseConstants.REQUEST_TYPE_VSO_GC) {
                salesUnitRequirement.setSalesman(sales);
            }

            if (vcr.getRequestType().getIdRequestType() == BaseConstants.REQUEST_TYPE_VSO_INTERNAL) {
                salesUnitRequirement.setSalesman(sales);
            }

            if (vcr.getRequestType().getIdRequestType() == BaseConstants.REQUEST_TYPE_VSO_REKANAN) {
                salesUnitRequirement.setSalesman(sales);
            }
        }

        salesUnitRequirement = salesUnitRequirementRepository.save(salesUnitRequirement);
        SalesUnitRequirementDTO result = salesUnitRequirementMapper.toDto(salesUnitRequirement);
        salesUnitRequirementSearchRepository.save(salesUnitRequirement);
        return result;
    }

    private String checkSTNKGC(UUID idRequest) {
        String a = null;
        String b = null;

        VehicleCustomerRequest vcr = vehicleCustomerRequestRepository.findOne(idRequest);
        if (vcr != null) {
            if (vcr.getRequestType().getIdRequestType() == BaseConstants.REQUEST_TYPE_VSO_GC) {
                a = "organization";
            } else if (vcr.getRequestType().getIdRequestType() == BaseConstants.REQUEST_TYPE_VSO_DIRECT) {
                a = "person";
                b = "direct";
            } else if (vcr.getRequestType().getIdRequestType() == BaseConstants.REQUEST_TYPE_VSO_GP) {
                a = "person";
            }
        }

        return a;
    }

    /**
     * Get all the salesUnitRequirements.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesUnitRequirementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SalesUnitRequirements");

        log.debug("Request to get all VehicleSalesOrders");
        Internal internal = partyUtils.getCurrentInternal();
        return salesUnitRequirementRepository.findActiveSalesUnitRequirement(internal != null ? internal.getIdInternal() : null, pageable)
            .map(salesUnitRequirementMapper::toDto);
    }


    @Transactional(readOnly = true)
    public Page<SalesUnitRequirementDTO> findAllApprovalLeasing(Pageable pageable) {
        log.debug("Request to get all SalesUnitRequirements");

        log.debug("Request to get all VehicleSalesOrders");
        Internal internal = partyUtils.getCurrentInternal();
        return salesUnitRequirementRepository.findActiveLeasingApproval(internal != null ? internal.getIdInternal() : null, pageable)
            .map(salesUnitRequirementMapper::toDto)
            .map(this::assignOtherData);
    }

    @Transactional(readOnly = true)
    public Page<SalesUnitRequirementDTO> findAllApprovalLeasingRequestAttachment(Pageable pageable) {
        log.debug("Request to get all SalesUnitRequirements");
        Internal internal = partyUtils.getCurrentInternal();
        Salesman salesman = partyUtils.getCurrentSalesman();
        Salesman korsal = null;
        log.debug("ini salesman baru = " + salesman);
        if (salesman.isCoordinator().equals(true)) {
            korsal = salesmanRepository.findCoordinator(salesman.getIdPartyRole());
        } else {
            throw new DmsException("Anda Login Bukan Sebagai Korsal");
        }
        log.debug("ini korsal baru = " + korsal);
        return salesUnitRequirementRepository.findActiveLeasingApprovalRequestAttachment(internal != null ? internal.getIdInternal() : null,salesman.getIdPartyRole(), pageable)
            .map(salesUnitRequirementMapper::toDto)
            .map(this::assignOtherData);
    }

    /**
     * Get one salesUnitRequirement by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SalesUnitRequirementDTO findOne(UUID id) {
        log.debug("Request to get SalesUnitRequirement : {}", id);
        SalesUnitRequirement salesUnitRequirement = salesUnitRequirementRepository.findOne(id);
        return salesUnitRequirementMapper.toDto(salesUnitRequirement);
    }

    @Transactional(readOnly = true)
    public SalesUnitRequirementDTO findOneWithResetKey(HttpServletRequest request) {
        log.debug("Request to get SalesUnitRequirement : {}", request);
        String resetKey = request.getParameter("resetKey");
        SalesUnitRequirement salesUnitRequirement = salesUnitRequirementRepository.findResetKey(resetKey);
        return salesUnitRequirementMapper.toDto(salesUnitRequirement);
    }
    /**
     * Delete the  salesUnitRequirement by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete SalesUnitRequirement : {}", id);
        salesUnitRequirementRepository.delete(id);
        salesUnitRequirementSearchRepository.delete(id);
    }

    /**
     * Search for the salesUnitRequirement corresponding to the query.
     *
     * @param query    the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesUnitRequirementDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SalesUnitRequirements for query {}", query);
        Page<SalesUnitRequirement> result = salesUnitRequirementSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(salesUnitRequirementMapper::toDto);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public SalesUnitRequirementDTO processExecuteData(Integer id, String param, SalesUnitRequirementDTO dto) {
        SalesUnitRequirementDTO r = dto;
        SalesUnitRequirement surs = salesUnitRequirementRepository.findOne(dto.getIdRequirement());
        if (r != null) {
            switch (id) {
                case SalesUnitRequirementConstants.SUBMIT_VSO:
                    validationForApprovalWilayah(r);
                    validationForApprovalDiskon(r);
                    validationForApprovalLeasing(r);
                    validationForHotItem(r);
                    break;
                case SalesUnitRequirementConstants.APPROVAL_KACAB:
                    approvalKacab(r);
                    break;
                case SalesUnitRequirementConstants.APPROVAL_KORSAL:
                    approvalKorsal(r);
                    break;
                case SalesUnitRequirementConstants.BUILD_VSO:
                    log.debug("SERVICE: build vso");
//                    buildVSO(dto);
                    buildVSOorApprovalLEasing(dto);
                    break;
                case SalesUnitRequirementConstants.RESET_VSO_DRAFT:
                    SalesUnitRequirementDTO x = setResetData(r);
                    save(x);
                  //  changeSalesUnitRequirementStatus(x, BaseConstants.STATUS_DRAFT);
                    setNewApproval(r.getIdRequirement(), BaseConstants.DISCOUNT_SUR, 1);
                    setNewApproval(r.getIdRequirement(), BaseConstants.PELANGGARAN_WILAYAH, 1);
                    setNewApproval(r.getIdRequirement(), BaseConstants.LEASING, 1);
                    break;
                case SalesUnitRequirementConstants.FIND_OR_CREATE_VSO_NUMBER:
                    findOrCreateVSONumber(r);
                    break;
                case SalesUnitRequirementConstants.CANCEL_VSO: // Pak Ded pakai yang ini yah
                    SalesUnitRequirementDTO z = setResetDataForCancelVSO(r);
                    changeSalesUnitRequirementStatus(z, BaseConstants.STATUS_CANCEL);
                    save(z);
                    this.closeProspectSur(z);
                    this.closeSalesBooking(z);
                    break;
                case SalesUnitRequirementConstants.DUPLICATE_SUR:
                    duplicateSalesUnitRequirement(dto);
                    break;
                case SalesUnitRequirementConstants.GANTI_VSO:
                    SalesUnitRequirementDTO z1 = setResetDataForGantiVSO(r);
                    changeSalesUnitRequirementStatus(z1, BaseConstants.STATUS_DRAFT);
                    save(z1);
                    this.cekSurOnSalesNote(z1);
                    setNewApproval(z1.getIdRequirement(), BaseConstants.DISCOUNT_SUR, 1);
                    setNewApproval(z1.getIdRequirement(), BaseConstants.PELANGGARAN_WILAYAH, 1);
                    setNewApproval(z1.getIdRequirement(), BaseConstants.LEASING, 1);
                    this.closeSalesBooking(z1);
                    break;
                case SalesUnitRequirementConstants.APPROVAL_LEASING:
                    if (r.getUnitIndent().equals(true)) {
                        log.debug("SERVICE: build vso");
                        surs = salesUnitRequirementMapper.toEntity(r);
                        salesUnitRequirementRepository.save(surs);
                        setNewApproval(r.getIdRequirement(), BaseConstants.LEASING, BaseConstants.STATUS_APPROVED);
                        changeSalesUnitRequirementStatus(r, BaseConstants.STATUS_INDENT);
                    }else {
                        log.debug("SERVICE: build vso");
                        surs = salesUnitRequirementMapper.toEntity(r);
                        salesUnitRequirementRepository.save(surs);
                        setNewApproval(r.getIdRequirement(), BaseConstants.LEASING, BaseConstants.STATUS_APPROVED);
                        buildVSO(dto);
                    }
//                    this.createIndent(r);
                    break;
                case SalesUnitRequirementConstants.REJECTED_LEASING:
                    log.debug("SERVICE: build vso");
                    surs = salesUnitRequirementMapper.toEntity(r);
                    surs.setRequestIdFrame(null);
                    surs.setRequestIdMachine(null);
                    salesUnitRequirementRepository.save(surs);
                    setNewApproval(r.getIdRequirement(), BaseConstants.LEASING, BaseConstants.STATUS_REJECTED);
                    changeSalesUnitRequirementStatus(r, BaseConstants.STATUS_DRAFT);
                    break;
                case SalesUnitRequirementConstants.REQUEST_ATTACHMENT:
                    setNewApproval(r.getIdRequirement(), BaseConstants.LEASING, BaseConstants.STATUS_REQUEST_ATTACHMENT);
                    break;
                    case SalesUnitRequirementConstants.VSO_SALES_EDIT:
                    SurvsoSalesEdit(r);
                    break;
                case SalesUnitRequirementConstants.PROSES_INDENT:
                    createIndentOrVSO(r);
                    break;
                case SalesUnitRequirementConstants.PROSES_BACK_ORDER:
                    log.debug("SERVICE: build vso");
                    buildVSOorApprovalLEasing(r);
                    break;
                case SalesUnitRequirementConstants.PROSES_GANTI_VSO_NOT_APPROVE:
                    SalesUnitRequirementDTO salesUnitRequirementDTO = setResetDataForGantiVSO(r);
                    changeSalesUnitRequirementStatus(salesUnitRequirementDTO, BaseConstants.STATUS_VSO_NOT_APPROVE);
                    save(salesUnitRequirementDTO);
                    setNewApproval(r.getIdRequirement(), BaseConstants.DISCOUNT_SUR, 1);
                    setNewApproval(r.getIdRequirement(), BaseConstants.PELANGGARAN_WILAYAH, 1);
                    setNewApproval(r.getIdRequirement(), BaseConstants.LEASING, 1);
                    this.closeSalesBooking(salesUnitRequirementDTO);
                    break;
                case SalesUnitRequirementConstants.PROSES_UPDATE_HARGA_PEMBAYARAN:
                    surs.setPoNumber(r.getPoNumber());
                    surs.setPodate(r.getPodate());
                    surs.setKomisiSales(r.getKomisiSales());
                    surs = salesUnitRequirementRepository.save(surs);
                    break;
                case SalesUnitRequirementConstants.PROSES_BACK_ORDER_FROM_INDENT:
                    buildVSO(r);
                    break;
                case SalesUnitRequirementConstants.PROSES_UPDATE_FACILITY:
                    setFacility(r);
                    break;
                default:
                    break;
            }
        }
        return r;
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public SalesUnitRequirementDTO processExecuteDataApproveEmail(HttpServletRequest request ,SalesUnitRequirementDTO salesUnitRequirementDTO) {
        log.debug("SERVICE: build vso atas" + salesUnitRequirementDTO);
        SalesUnitRequirementDTO r = salesUnitRequirementDTO;
        SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(salesUnitRequirementDTO.getIdRequirement());
        log.debug("SERVICE: build vso atas");

        if (r != null) {
            if (sur.getUnitIndent().equals(true)) {
                sur.setLeasingnote(r.getLeasingnote());
                sur.setIpAddress(r.getIpAddress());
                salesUnitRequirementRepository.save(sur);
                setNewApproval(r.getIdRequirement(), BaseConstants.LEASING, BaseConstants.STATUS_APPROVED);
                changeSalesUnitRequirementStatus(r, BaseConstants.STATUS_INDENT);
            } else {
                log.debug("SERVICE: build vso");
                sur.setLeasingnote(salesUnitRequirementDTO.getLeasingnote());
                sur.setIpAddress(r.getIpAddress());
                salesUnitRequirementRepository.save(sur);
                setNewApproval(r.getIdRequirement(), BaseConstants.LEASING, BaseConstants.STATUS_APPROVED);
                buildVSO(r);
//                this.createIndent(r);
            }
        }
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public SalesUnitRequirementDTO processExecuteDataNotApproveEmail(SalesUnitRequirementDTO salesUnitRequirementDTO) {
        SalesUnitRequirementDTO r = salesUnitRequirementDTO;
        SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(r.getIdRequirement());
        if (r != null) {
            log.debug("SERVICE: build vso");
            sur.setRequestIdFrame(null);
            sur.setRequestIdMachine(null);
            setNewApproval(r.getIdRequirement(), BaseConstants.LEASING, BaseConstants.STATUS_REJECTED);
            sur.setLeasingnote(salesUnitRequirementDTO.getLeasingnote());
            sur.setIpAddress(r.getIpAddress());
            salesUnitRequirementRepository.save(sur);
            changeSalesUnitRequirementStatus(r, BaseConstants.STATUS_DRAFT);
        }
        return r;
    }

    private void validationForApprovalWilayah(SalesUnitRequirementDTO dto) {
        log.debug("VALIDATION Approval For Wilayah");
        SalesUnitRequirementDTO r = dto;
        String idInternal = partyUtils.getCurrentInternal().getIdInternal();
        String idGeobou = dto.getPersonOwner().getPostalAddress().getDistrictCode();
        log.debug("INI internal For Wilayah" + idInternal);
        log.debug("INI geobou For Wilayah" + idGeobou);

        if (r.getCurrentStatus() == BaseConstants.STATUS_DRAFT || r.getCurrentStatus() == BaseConstants.STATUS_VSO_NOT_APPROVE) {
            if (idGeobou != null) {
              //  List<String> sur = salesUnitRequirementRepository.findCheckRegion(idInternal, idGeobou);
               // log.debug("Data For Wilayah " + sur);
               // if (!sur.isEmpty()) {
                    changeSalesUnitRequirementApprovalStatus(r, BaseConstants.PELANGGARAN_WILAYAH, BaseConstants.STATUS_APPROVED);
              //  } else {
               //     changeSalesUnitRequirementApprovalStatus(r, BaseConstants.PELANGGARAN_WILAYAH, BaseConstants.STATUS_WAITING_FOR_APPROVAL);
               // }
                log.debug("hasil set for wilayah " + r.getApprovalTerritorial());
            }
        }
    }

    public SalesUnitRequirementDTO forApprovalWilayah(SalesUnitRequirementDTO dto) {
        log.debug("INI VALIDATION Approval For Wilayah");
        SalesUnitRequirementDTO r = dto;
        String idInternal = partyUtils.getCurrentInternal().getIdInternal();
        String idGeobou = dto.getPersonOwner().getPostalAddress().getDistrictCode();
        log.debug("INI internal For Wilayah" + idInternal);
        log.debug("INI geobou For Wilayah" + idGeobou);
        if (idGeobou != null) {
            List<String> sur = salesUnitRequirementRepository.findCheckRegion(idInternal, idGeobou);
            log.debug("Data For Wilayah " + sur );
            if (!sur.isEmpty()) {
                r.setApprovalTerritorial(61);
            } else {
                r.setApprovalTerritorial(60);
            }
            log.debug("set for wilayah " + r.getApprovalTerritorial() );
        }
        return r;
    }

    private void validationForApprovalLeasing(SalesUnitRequirementDTO dto) {
        log.debug("VALIDATION Approval For Wilayah");
        SalesUnitRequirementDTO r = dto;

        if (r.getCurrentStatus() == BaseConstants.STATUS_DRAFT || r.getCurrentStatus() == BaseConstants.STATUS_VSO_NOT_APPROVE) {
            if (r.getApprovalLeasing().equals(BaseConstants.STATUS_NOT_REQUIRED) || r.getApprovalLeasing().equals(BaseConstants.STATUS_REJECTED)) {
                changeSalesUnitRequirementApprovalStatus(r, BaseConstants.LEASING, BaseConstants.STATUS_NOT_REQUIRED);
            }
        }
    }

    private void validationForHotItem(SalesUnitRequirementDTO dto) {
        log.debug("VALIDATION Approval For HotItem");
        SalesUnitRequirementDTO r = dto;

            String idinternal = partyUtils.getCurrentInternal().getIdInternal();
            List <RuleHotItem> ruleHotItemList = ruleHotItemRepository.findRoleHotItem(dto.getProductId(), dto.getInternalId());
            List <Receipt> titipan = receiptRepository.queryByIdRequirement(dto.getIdRequirement());

            if ( ruleHotItemList.size() > 0 ) {
                for (RuleHotItem each : ruleHotItemList) {
                    BigDecimal minPayment = titipan.iterator().next().getAmount();
                    log.debug("Nilai Titipan" + minPayment);
                    Double Bayar =  ruleHotItemList.iterator().next().getMinDownPayment().doubleValue();
                    if (titipan.isEmpty()) {
                        throw new DmsException("Product ini wajib ada titipan minimal RP. " + Bayar);
                    }
                    if (titipan != null) {
                        if ( minPayment.doubleValue() < ruleHotItemList.iterator().next().getMinDownPayment().doubleValue() ) {
                            throw new DmsException("Product ini wajib ada titipan minimal RP. " + Bayar);
                        }

                    }
                }

            }
    }

    private void validationForApprovalDiskon(SalesUnitRequirementDTO dto) {
        log.debug("VALIDATION Approval For Diskon");
        SalesUnitRequirementDTO r = dto;

        if (r.getApprovalDiscount().equals(BaseConstants.STATUS_NOT_REQUIRED) || r.getApprovalDiscount().equals(BaseConstants.STATUS_REJECTED)) {
            log.debug("Validation for check subsidi");
            boolean isFind = false;

            String idinternal = partyUtils.getCurrentInternal().getIdInternal();
            List<RuleSalesDiscount> ruleSalesDiscounts = salesUnitRequirementRepository.findActiveRuleSalesDiscount(idinternal);

            if (ruleSalesDiscounts.size() > 0) {
                for (RuleSalesDiscount each : ruleSalesDiscounts) {
                    BigDecimal subsOwn = r.getSubsown();
                    if (subsOwn == null) {
                        subsOwn = BigDecimal.ZERO;
                    }

                    BigDecimal subsFinComp = r.getSubsfincomp();
                    if (subsFinComp == null) {
                        subsFinComp = BigDecimal.ZERO;
                    }

                    BigDecimal totalSubsidi = subsOwn.add(subsFinComp);

                    int rsCompareTo = totalSubsidi.compareTo(each.getMaxAmount());
                    if (rsCompareTo == -1 || rsCompareTo == 0) {
                        if ((each.getRole().toLowerCase().equals(BaseConstants.AUTH_SALESMAN.toLowerCase())) && (!isFind)) {
                            log.debug("step 1");
                            isFind = true;
                            changeSalesUnitRequirementApprovalStatus(r, BaseConstants.DISCOUNT_SUR, BaseConstants.STATUS_APPROVED);
                            r.setApprovalDiscount(BaseConstants.STATUS_APPROVED);
                        } else if (!isFind) {
                            log.debug("step 2");
                            isFind = true;
                            changeSalesUnitRequirementApprovalStatus(r, BaseConstants.DISCOUNT_SUR, BaseConstants.STATUS_WAITING_FOR_APPROVAL);
                        }
                    }
                }
            } else {
                log.debug("step 3");
                changeSalesUnitRequirementApprovalStatus(r, BaseConstants.DISCOUNT_SUR, BaseConstants.STATUS_APPROVED);
            }
        }
    }

    private void approvalKacab(SalesUnitRequirementDTO dto) {
        approveDiskon(dto);
        approveWilayah(dto);
    }

    private void approvalKorsal(SalesUnitRequirementDTO dto) {
        approveDiskon(dto);
    }

    public SalesUnitRequirementDTO setResetDataForCancelVSO(SalesUnitRequirementDTO dto) {
        SalesUnitRequirementDTO r = dto;
        Internal inter = internalRepository.findByIdInternal(r.getInternalId());
        //TODO: by EAP, perubahan no vso saat di reset tidak membutuhkan penambahan angka 1 di belakang, buat no baru saja.
//        String reqNumber = dto.getRequirementNumber();
//        String newReqNumber = masterNumberingService.findTransNumberBy(inter.getIdInternal(), "VSO");
//        if (reqNumber.indexOf("+") >= 0) {
//            String[] s = reqNumber.split(" +");
//            Integer newSeq = Integer.parseInt(s[1]) + 1;
//            newReqNumber = s[0] + " +" + newSeq;
//        } else {
//            newReqNumber = reqNumber + " +1";
//        }
//        r.setRequirementNumber(newReqNumber);
        SalesUnitRequirementDTO s = setResetData(r);
        s.setRequestIdFrame("");
        s.setRequestIdFrame("");
        return s;
    }

    public SalesUnitRequirementDTO setResetDataForGantiVSO(SalesUnitRequirementDTO dto) {
        SalesUnitRequirementDTO r = dto;
        Internal inter = internalRepository.findByIdInternal(r.getInternalId());
        //TODO: by EAP, perubahan no vso saat di reset tidak membutuhkan penambahan angka 1 di belakang, buat no baru saja.
        String newReqNumber = masterNumberingService.findTransNumberBy(inter.getIdInternal(), "VSO");
        if (dto.getLeasingCompanyId() != null) {
            r.setLeasingCompanyId(dto.getLeasingCompanyId());
        }
        r.setRequestIdMachine("");
        r.setRequestIdFrame("");
        r.setRequirementNumber(newReqNumber);
        log.debug("masuk ngga di ganti sur " + newReqNumber);
        return r;
    }

    private SalesUnitRequirementDTO setResetData(SalesUnitRequirementDTO r) {
        log.debug("Start reset data");

//        r.setBrokerFee(BigDecimal.ZERO);
//        r.setColorId(null);
//        r.setProductId(null);
//        r.setSubsmd(BigDecimal.ZERO);
//        r.setUnitPrice(BigDecimal.ZERO);
//        r.setSubsown(BigDecimal.ZERO);
//        r.setSubsahm(BigDecimal.ZERO);
//        r.setSaleTypeId(null);
//        r.setProductYear(null);
//        r.setLeasingCompanyId(null);
//        r.setDownPayment(BigDecimal.ZERO);
//        r.setHetprice(BigDecimal.ZERO);
//        r.setBbnprice(BigDecimal.ZERO);
        r.setIdframe(null);
        r.setIdmachine(null);
        r.setRequestIdFrame(null);
        r.setRequestIdMachine(null);

        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Set<SalesUnitRequirementDTO> processExecuteListData(Integer id, String param, Set<SalesUnitRequirementDTO> dto) {
        Set<SalesUnitRequirementDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
        }
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public SalesUnitRequirementDTO approveDiskon(SalesUnitRequirementDTO dto) {
        if (dto != null) {
            SalesUnitRequirement e = salesUnitRequirementRepository.findOne(dto.getIdRequirement());

            Boolean isChange = false;
            if (!e.getApprovalDiscount().equals(dto.getApprovalDiscount())) {
                setNewApproval(e.getIdRequirement(), BaseConstants.DISCOUNT_SUR, dto.getApprovalDiscount());

                if (dto.getApprovalDiscount().equals(BaseConstants.STATUS_REJECTED)) {
                    dto.setSubsown(BigDecimal.ZERO);
                }
                isChange = true;
            }

            if (!e.getCurrentStatus().equals(dto.getCurrentStatus())) {
                e.setStatus(dto.getCurrentStatus());
                isChange = true;
            }

            if (isChange) {
                salesUnitRequirementRepository.save(e);
            }
        }
        return dto;
    }

    ;

    public SalesUnitRequirementDTO approveWilayah(SalesUnitRequirementDTO dto) {
        if (dto != null) {
            SalesUnitRequirement e = salesUnitRequirementRepository.findOne(dto.getIdRequirement());

            Boolean isChange = false;
            if (!e.getApprovalTerritorial().equals(dto.getApprovalTerritorial())) {
                setNewApproval(e.getIdRequirement(), BaseConstants.PELANGGARAN_WILAYAH, dto.getApprovalTerritorial());
                log.debug("ini approval wilayah : " + dto.getApprovalTerritorial());
                if (dto.getApprovalTerritorial().equals(BaseConstants.STATUS_REJECTED)) {
                    dto.setApprovalTerritorial(BaseConstants.STATUS_REJECTED);
                }
                isChange = true;
            }

            if (!e.getCurrentStatus().equals(dto.getCurrentStatus())) {
                e.setStatus(dto.getCurrentStatus());
                isChange = true;
            }

            if (isChange) {
                salesUnitRequirementRepository.save(e);
            }
        }
        log.debug("ini approval wilayah : " + dto.getApprovalTerritorial());
        return dto;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public SalesUnitRequirementDTO changeSalesUnitRequirementApprovalStatus(SalesUnitRequirementDTO dto, Integer approvalTypeId, Integer id) {
        if (dto != null) {
            SalesUnitRequirement e = salesUnitRequirementRepository.findOne(dto.getIdRequirement());
            if (approvalTypeId.equals(BaseConstants.DISCOUNT_SUR)) {
                if (!e.getApprovalDiscount().equals(id)) {
                    setNewApproval(e.getIdRequirement(), approvalTypeId, id);
                }
            } else if (approvalTypeId.equals(BaseConstants.PELANGGARAN_WILAYAH)) {
                if (!e.getApprovalTerritorial().equals(id)) {
                    setNewApproval(e.getIdRequirement(), approvalTypeId, id);
                }
            } else if (approvalTypeId.equals(BaseConstants.LEASING)) {
                if (!e.getApprovalLeasing().equals(id)) {
                    setNewApproval(e.getIdRequirement(), approvalTypeId, id);
                }
            }
        }
        return dto;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public SalesUnitRequirementDTO changeSalesUnitRequirementStatus(SalesUnitRequirementDTO dto, Integer id) {
        if (dto != null) {
            SalesUnitRequirement e = salesUnitRequirementRepository.findOne(dto.getIdRequirement());
            System.out.println("coba lagi in status lagi dan lagi == "+e.getStatus());
            if (!e.getCurrentStatus().equals(id)) {
                e.setStatus(id);
                salesUnitRequirementRepository.save(e);
            }
        }
        return dto;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildOneFromProspectPerson(ProspectPerson pp, PersonalCustomer pc) {

        ProspectPerson prospectPerson = prospectPersonRepository.findOne(pp.getIdProspect());
        PersonalCustomer personalCustomer = personalCustomerRepository.findOne(pc.getIdCustomer());

        BillTo bt = billToRepository.findOne(personalCustomer.getIdCustomer());

        ShipToDTO st = csShipToSrv.findOne(personalCustomer.getIdCustomer());

        SalesUnitRequirement sur = new SalesUnitRequirement();

//        String vsonumber = masterNumberingService.findTransNumberBy(prospectPerson.getDealer().getIdInternal(), "VSO");
//        sur.setRequirementNumber(vsonumber);
        sur.setCustomer(personalCustomer);
        sur.setSalesman(prospectPerson.getSalesman());
        sur.setIdProspect(prospectPerson.getIdProspect());
        sur.setIdProspectSource(prospectPerson.getProspectSource().getIdProspectSource());
        sur.setInternal(prospectPerson.getDealer());

        if (prospectPerson.getBroker() != null) {
            sur.setBrokerId(prospectPerson.getBroker().getIdPartyRole());
        }

        if (bt != null) sur.setBillTo(bt);
        sur.setSubsfincomp(BigDecimal.ZERO);
        SalesUnitRequirement s = salesUnitRequirementRepository.save(sur);

        Approval approval = new Approval();
        approval.setIdApprovalType(BaseConstants.DISCOUNT_SUR);
        approval.setIdStatusType(BaseConstants.STATUS_NOT_REQUIRED);
        approval.setDateFrom(LocalDateTime.now());
        approval.setDateThru(dateUtils.getApocalypseDate());
        approval.setIdRequirement(s.getIdRequirement());

        approvalRepository.save(approval);

        approval = new Approval();
        approval.setIdApprovalType(BaseConstants.PELANGGARAN_WILAYAH);
        approval.setIdStatusType(BaseConstants.STATUS_NOT_REQUIRED);
        approval.setDateFrom(LocalDateTime.now());
        approval.setDateThru(dateUtils.getApocalypseDate());
        approval.setIdRequirement(s.getIdRequirement());

        approvalRepository.save(approval);

        approval = new Approval();
        approval.setIdApprovalType(BaseConstants.LEASING);
        approval.setIdStatusType(BaseConstants.STATUS_NOT_REQUIRED);
        approval.setDateFrom(LocalDateTime.now());
        approval.setDateThru(dateUtils.getApocalypseDate());
        approval.setIdRequirement(s.getIdRequirement());

        approvalRepository.save(approval);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildOneFromProspectOrganization(ProspectOrganizationDetailsDTO podto, PersonalCustomerDTO pc, OrganizationCustomerDTO ocDto) {

//        ProspectPerson prospectPerson = prospectPersonRepository.findOne(pp.getIdProspect());
        ProspectOrganizationDetails prospectOrganizationDetails = prospectOrganizationDetailsRepository.findOne(podto.getIdProspect());
        ProspectOrganization prospectOrganization = prospectOrganizationRepository.findOne(podto.getIdProspect());
        PersonalCustomer personalCustomer = personalCustomerRepository.findOne(pc.getIdCustomer());
        Person person = personalCustomer.getPerson();
        PersonDTO personDTO = personMapper.toDto(person);
        OrganizationCustomer organizationCustomer = organizationCustomerRepository.findOne(ocDto.getIdCustomer());

        BillToDTO bt = csBillToSrv.findOne(organizationCustomer.getIdCustomer());
        ShipToDTO st = csShipToSrv.findOne(organizationCustomer.getIdCustomer());

        SalesUnitRequirement sur = new SalesUnitRequirement();
//        String vsonumber = masterNumberingService.findTransNumberBy(prospectOrganization.getDealer().getIdInternal(), "VSO");
//
//        sur.setRequirementNumber(vsonumber);
        sur.setCustomer(organizationCustomerRepository.findOne(organizationCustomer.getIdCustomer()));
        sur.setSalesman(prospectOrganization.getSalesman());
        sur.setIdProspect(prospectOrganization.getIdProspect());
        sur.setIdProspectSource(prospectOrganization.getProspectSource().getIdProspectSource());

        sur.setInternal(prospectOrganization.getDealer());
        sur.setPersonOwner(personalCustomer.getPerson());
//        sur.setIdReqTyp(BaseConstants.REQUIREMENT_ORGANIZATION);

//        RequirementType requirementType = requirementTypeRepository.findOne(BaseConstants.REQUIREMENT_ORGANIZATION);
        sur.setRequirementType(requirementTypeRepository.findOne(BaseConstants.REQUIREMENT_GC));
//        sur.setRequirementType();
//        setIdRequirementType(BaseConstants.REQUIREMENT_ORGANIZATION);

        if (prospectOrganization.getBroker() != null) {
            sur.setBrokerId(prospectOrganization.getBroker().getIdPartyRole());
        }

        if (bt != null) sur.setBillTo(billToRepository.findOne(bt.getIdBillTo()));
        sur.setSubsfincomp(BigDecimal.ZERO);
        salesUnitRequirementRepository.save(sur);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public SalesUnitRequirement buildOneFromProspectPersonAndOwner(InternalDTO intr, PersonalCustomerDTO buyer, PersonalCustomerDTO ownerCust, String idMotor, Integer idColor) {

        PersonalCustomer pcOwner = personalCustomerRepository.findOne(ownerCust.getIdCustomer());
        PersonalCustomer pcBuyer = personalCustomerRepository.findOne(buyer.getIdCustomer());

        BillToDTO bt = csBillToSrv.findByIdBillTo(buyer.getIdCustomer());
        Internal internal = internalRepository.findByIdInternal(intr.getIdInternal());

        SalesUnitRequirementDTO sur = new SalesUnitRequirementDTO();
        String vsonumber = masterNumberingService.findTransNumberBy(internal.getRoot().getIdInternal(), "VSO");
        sur.setRequirementNumber(vsonumber);
        sur.setInternalId(intr.getIdInternal());

        sur.setColorId(idColor);
        sur.setProductId(idMotor);

        //Ambil Harga Jual
        Double het = 15750000d;
        Double bbn = 1435000d;

        sur.setDownPayment(new BigDecimal(1500000));
        sur.setHetprice(BigDecimal.valueOf(het));
        sur.setUnitPrice(BigDecimal.valueOf(het * 100 / 110));
        sur.setBbnprice(BigDecimal.valueOf(bbn));
        sur.setSubsahm(BigDecimal.valueOf(50000));
        sur.setSubsmd(BigDecimal.valueOf(50000));
        sur.setSubsfincomp(BigDecimal.valueOf(50000));
        sur.setSubsown(BigDecimal.valueOf(50000));
        sur.setSaleTypeId(50);

        if (bt != null) sur.setBillToId(bt.getIdBillTo());

        sur = save(sur);

        SalesUnitRequirement s = salesUnitRequirementRepository.findOne(sur.getIdRequirement());
        s.setPersonOwner(pcOwner.getPerson());
        s.setCustomer(pcBuyer);
        s = salesUnitRequirementRepository.save(s);

        return s;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public SalesUnitRequirementDTO buildOneFromProspectPersonAndOwnerLsg(InternalDTO intr, PersonalCustomerDTO buyer, PersonalCustomerDTO ownerCust, PersonDTO owner, String idMotor, Integer idColor) {

        BillToDTO bt = csBillToSrv.findByIdBillTo(buyer.getIdCustomer());

        SalesUnitRequirementDTO sur = new SalesUnitRequirementDTO();

        String vsonumber = masterNumberingService.findTransNumberBy(intr.getIdInternal(), "VSO");

        sur.setRequirementNumber(vsonumber);
        sur.setCustomerId(buyer.getIdCustomer());
        sur.setInternalId(intr.getIdInternal());
        sur.setPersonOwner(owner);

        sur.setColorId(idColor);
        sur.setProductId(idMotor);

        sur.setDownPayment(new BigDecimal(1500000));
        //Ambil Harga Jual
        sur.setUnitPrice(new BigDecimal(15750000));
        sur.setSaleTypeId(10);

        if (bt != null) sur.setBillToId(bt.getIdBillTo());

        sur = save(sur);

        // Change Status dari SUR
        // sur = changeSalesUnitRequirementStatus(sur, BaseConstants.STATUS_COMPLETED);

        return sur;
    }

//    private String checkSalesUnitRequirementPersonOrOrganization(SalesUnitRequirementDTO dto){
//        log.debug("CHECK SUR PERSON OR ORGANIZATION");
//        String rs = "";
//
//        //check if person
//        Prospect prospect = prospectRepository.findOne(dto.getIdProspect());
//        if (prospect instanceof ProspectPerson){
//            rs = "person";
//        } else if (prospect instanceof ProspectOrganization) {
//            rs = "organization";
//        }
//
//        return rs;
//    }

    public SalesUnitRequirementDTO findOrCreateVSONumber(SalesUnitRequirementDTO dto) {
        log.debug("SERVICE: bikin vso number");
        SalesUnitRequirement salesUnitRequirement = salesUnitRequirementRepository.findOne(dto.getIdRequirement());

        if (salesUnitRequirement != null) {
            customerService.findOrCreateCustomerCode(dto.getCustomerId(), salesUnitRequirement.getInternal());
            if (salesUnitRequirement.getRequirementNumber() == null) {
                String vsoNumber = "";

//                Prospect prospect = prospectRepository.findOne(dto.getIdProspect());
                vsoNumber = masterNumberingService.findTransNumberBy(dto.getInternalId(), "VSO");
                dto.setRequirementNumber(vsoNumber);
//                SalesUnitRequirement s = salesUnitRequirementRepository.findOne(dto.getIdRequirement());

                if (vsoNumber != "") {
                    save(dto);
                } else {
                    throw new DmsException("Error format");
                }
            }
        }

        return dto;
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public void buildVSO(SalesUnitRequirementDTO dto) {
        log.debug("SERVICE build SUR");
        if ((dto.getApprovalDiscount().equals(BaseConstants.STATUS_APPROVED) &&
            dto.getApprovalTerritorial().equals(BaseConstants.STATUS_APPROVED))) {
            SalesUnitRequirement s = salesUnitRequirementRepository.findOne(dto.getIdRequirement());
            findOrCreateVSONumber(dto);
            customerService.findOrCreateCustomerCode(dto.getCustomerId(), s.getInternal());
            vsoService.buildFromSUR(s);
            this.changeSalesUnitRequirementStatus(dto, BaseConstants.STATUS_COMPLETED);
        } else {
            throw new DmsException("Diskon / Wilayah belum di approve");
        }
    }

    @Transactional(readOnly = true)
    public Page<SalesUnitRequirementDTO> findSalesUnitRequirementByStatusApprovalAndStatusType(String idinternal, Integer idstatustype, Integer idstatusapproval, Pageable pageable) {
        return salesUnitRequirementRepository.findSalesUnitRequirementByStatusApprovalAndStatusType(idinternal, idstatustype, idstatusapproval, pageable).map(salesUnitRequirementMapper::toDto);
    }

    @Transactional
    public void setNewApproval(UUID idreq, Integer idApprovalType, Integer idStatusType) {
        log.debug("SERVICE change status approval");
        LocalDateTime now = LocalDateTime.now();

        Approval currentApproval = salesUnitRequirementRepository.findActiveApprovalByApprovalType(idreq, idApprovalType);
        currentApproval.setDateThru(now.minusNanos(1));
        approvalRepository.save(currentApproval);

        Approval newApproval = new Approval();
        newApproval.setIdRequirement(idreq);
        newApproval.setDateFrom(now);
        newApproval.setIdApprovalType(idApprovalType);
        newApproval.setIdStatusType(idStatusType);
        approvalRepository.save(newApproval);
    }

    @Transactional
    public SalesUnitRequirementDTO createSurForDirect(String idcustomer, Integer idreqtype, String idinternal) {
        Internal internal = partyUtils.getCurrentInternal();

        SalesUnitRequirementDTO surDTO = new SalesUnitRequirementDTO();
        surDTO.setIdReqTyp(idreqtype);
        surDTO.setCustomerId(idcustomer);
        surDTO.setInternalId(internal.getIdInternal());
        surDTO.setQty(1);
        surDTO.setInternalId(idinternal);

        SalesUnitRequirementDTO newSurDTO = save(surDTO);

        setNewApproval(newSurDTO.getIdRequirement(), BaseConstants.DISCOUNT_SUR, 1);
        setNewApproval(newSurDTO.getIdRequirement(), BaseConstants.PELANGGARAN_WILAYAH, 1);
        setNewApproval(newSurDTO.getIdRequirement(), BaseConstants.LEASING, 1);

        return newSurDTO;
    }

    @Transactional(readOnly = true)
    public Page<SalesUnitRequirementDTO> findAllByNameSalesUnitRequirementFilterBy(Pageable pageable, SalesUnitRequirementPTO param) {
        log.debug("Request to get all SalesUnitRequirement by Nama and idInternal");
        log.debug("PTO unit internal atas == " + param.getInternalId());
        log.debug("PTO unit  name atas == " + param.getNama());
        String paramLike = '%' + param.getNama() + '%';
        String internal = param.getInternalId();
        Salesman sales = partyUtils.getCurrentSalesman();
        if (param.getNama() == null) {
            return salesUnitRequirementRepository.findAll(pageable)
                .map(salesUnitRequirementMapper::toDto);
        }
        if (param.getNama() != null) {
            log.debug("PTO unit salesunitrequirement dataParam == " + param.getNama());
            Page<SalesUnitRequirement> udto = salesUnitRequirementRepository.findSearchNama(paramLike, internal, sales,  pageable);
            return udto
                .map(salesUnitRequirementMapper::toDto);
        }
//        else {
//            log.debug("PTO salesunitrequirement == "+ param);
//            return unitDeliverableRepository.findAllByIdDeliverableTypeFilterBy(pageable, param)
//                .map(unitDeliverableMapper::toDto)
//                .map(this::assignSur)
//                .map(this::assignUD);
//        }
        return null;
    }

    @Transactional
    public SalesUnitRequirementDTO duplicateSalesUnitRequirement(SalesUnitRequirementDTO dto) {
//        List<SalesUnitRequirement> ListSur = salesUnitRequirementRepository.quesryByIdRequest(dto.getIdRequest());
        SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(dto.getIdRequirement());
//        if (!ListSur.isEmpty()){
//            log.debug("list surr == " + ListSur);
//            sur = ListSur.get(0);
//            log.debug("darii list surr == " + ListSur);
//        }
        SalesUnitRequirement s = new SalesUnitRequirement();
//            s.setIdProspect(s.se);
        s.setSaleType(sur.getSaleType());
        s.setSalesman(sur.getSalesman());
        s.setProduct(sur.getProduct());
        s.setColor(sur.getColor());
        s.setBrokerId(sur.getBrokerId());
        s.setPersonOwner(sur.getPersonOwner());
        s.setInternal(sur.getInternal());
        s.setBillTo(sur.getBillTo());
        s.setCustomer(sur.getCustomer());
        s.setDownPayment(sur.getDownPayment());
        s.setIdframe(sur.getIdframe());
        s.setIdmachine(sur.getIdmachine());
        s.setSubsfincomp(sur.getSubsfincomp());
        s.setSubsmd(sur.getSubsmd());
        s.setSubsown(sur.getSubsown());
        s.setSubsahm(sur.getSubsahm());
        s.setBbnprice(sur.getBbnprice());
        s.setHetprice(sur.getHetprice());
        s.setNote(sur.getNote());
        s.setUnitPrice(sur.getUnitPrice());
        s.setNotePartner(sur.getNotePartner());
        s.setNoteShipment(sur.getNoteShipment());
        s.setRequestpoliceid(sur.getRequestpoliceid());
        s.setLeasingCompany(sur.getLeasingCompany());
        s.setPodate(sur.getPodate());
        s.setBrokerFee(sur.getBrokerFee());
        s.setIdProspectSource(sur.getIdProspectSource());
        s.setProductYear(sur.getProductYear());
        s.setRequestIdMachine(sur.getRequestIdMachine());
        s.setRequestIdFrame(sur.getRequestIdFrame());
        s.setRequestIdMachineAndFrame(sur.getRequestIdMachineAndFrame());
        s.setMinPayment(sur.getMinPayment());
        s.setUnitHotItem(sur.getUnitHotItem());
        s.setUnitIndent(sur.getUnitIndent());
        s.setWaitStnk(sur.getWaitStnk());
        s.setCreditInstallment(sur.getCreditInstallment());
        s.setCreditDownPayment(sur.getCreditDownPayment());
        s.setCreditTenor(sur.getCreditTenor());
        s.setPoNumber(sur.getPoNumber());
        s.setOrganizationOwner(sur.getOrganizationOwner());
        s.setIdRequest(sur.getIdRequest());
        s.setRequirementType(sur.getRequirementType());
        log.debug("surrrrrrrrrrrrr = " + s);
        s = salesUnitRequirementRepository.save(s);
//        SalesUnitRequirement salesUnitRequirement = salesUnitRequirementMapper.toEntity(dto);


//        sur = salesUnitRequirementRepository.save(sur);
//        SalesUnitRequirementDTO result = salesUnitRequirementMapper.toDto(sur);
//        salesUnitRequirementSearchRepository.save(sur);
        log.debug("new surr = " + s);
        log.debug("new surr = " + s);

        return dto;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildVSOorApprovalLEasing(SalesUnitRequirementDTO dto) {
        if (dto.getSaleTypeId() == BaseConstants.SALE_TYPE_CREDIT ||
            dto.getSaleTypeId() == BaseConstants.SALE_TYPE_CREDIT_COD ||
            dto.getSaleTypeId() == BaseConstants.SALE_TYPE_CREDIT_CBD) {
            SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(dto.getIdRequirement());
            PersonalCustomer cust = personalCustomerRepository.findByIdCustomer(dto.getCustomerId());
            PostalAddress postalAddressCust = cust.getPerson().getPostalAddress();
            PostalAddress postalAddressStnk = sur.getPersonOwner().getPostalAddress();
            sur.setIpAddress(null);
            sur.setResetKey(RandomUtil.generateResetKey());
            salesUnitRequirementRepository.save(sur);
            this.changeSalesUnitRequirementStatus(dto, BaseConstants.STATUS_COMPLETED);
            setNewApproval(dto.getIdRequirement(), BaseConstants.LEASING, BaseConstants.STATUS_WAITING_FOR_APPROVAL_LEASING);
            mailService.sendApprovalLeasingMail(sur,cust, postalAddressCust, postalAddressStnk);
        } else {
            buildVSO(dto);
        }

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public String buildApprovalLEasing(SalesUnitRequirement salesUnitRequirement) {
        SalesUnitRequirement sur = salesUnitRequirement;
        PersonalCustomer cust = personalCustomerRepository.findByIdCustomer(sur.getCustomer().getIdCustomer());
        PostalAddress postalAddressCust = cust.getPerson().getPostalAddress();
        PostalAddress postalAddressStnk = sur.getPersonOwner().getPostalAddress();
        mailService.sendApprovalLeasingMail(sur,cust, postalAddressCust, postalAddressStnk);
        return "Sukses!";
    }

    public SalesUnitRequirementDTO assignOtherData(SalesUnitRequirementDTO salesUnitRequirementDTO) {
        String fname = null;
        String lname = null;
        String korsal = null;
        if (salesUnitRequirementDTO.getSaleTypeId() == BaseConstants.SALE_TYPE_CREDIT ||
            salesUnitRequirementDTO.getSaleTypeId() == BaseConstants.SALE_TYPE_CREDIT_COD ||
            salesUnitRequirementDTO.getSaleTypeId() == BaseConstants.SALE_TYPE_CREDIT_CBD) {
            Salesman sales = salesmanRepository.findOne(salesUnitRequirementDTO.getSalesmanId());
            LeasingCompany leasing = leasingCompanyRepository.findOne(salesUnitRequirementDTO.getLeasingCompanyId());
            List<Approval> lapproval = approvalRepository.findApprovalLeasingByIdReq(salesUnitRequirementDTO.getIdRequirement());
            Requirement requirement = requirementRepository.findOne(salesUnitRequirementDTO.getIdRequirement());
            if (requirement != null) {
                salesUnitRequirementDTO.setLastModifiedBy(requirement.getLastModifiedBy());
            }
            if (sales != null) {
                if (sales.getCoordinatorSales() != null) {
                    Salesman coorsales = salesmanRepository.findOne(sales.getCoordinatorSales().getIdPartyRole());
                    if (coorsales != null) {
                        korsal = coorsales.getPerson().getName();
                        log.debug("korsales vso = " + korsal);
                        salesUnitRequirementDTO.setKorsalName(korsal);
                    }
                }
            }
            if (leasing != null) {
                salesUnitRequirementDTO.setLeasingName(leasing.getOrganization().getName());
            }

            if (!lapproval.isEmpty()) {
                for (Approval a : lapproval) {
                    salesUnitRequirementDTO.setDtApproval(a.getDateFrom());
                }
            }
        }
        return salesUnitRequirementDTO;

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void SurvsoSalesEdit(SalesUnitRequirementDTO salesUnitRequirementDTO) {
        ZonedDateTime now = ZonedDateTime.now();
        ZonedDateTime end = ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 999, ZoneId.systemDefault());
        SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(salesUnitRequirementDTO.getIdRequirement());
        Integer qtyUnit = salesBookingService.checkAvailability(salesUnitRequirementDTO.getInternalId(), salesUnitRequirementDTO.getProductId(), salesUnitRequirementDTO.getColorId(), salesUnitRequirementDTO.getProductYear());
        Feature feature = null;
        if(sur != null){
            feature = featureRepository.findOne(salesUnitRequirementDTO.getColorId());
            if (feature != null) {
                sur.setColor(feature);
                sur = salesUnitRequirementRepository.save(sur);
            }
        }


        List<SalesBooking> listSalesBooking = salesBookingRepository.listSalesBookingByIdReq(salesUnitRequirementDTO.getIdRequirement());
        if (!listSalesBooking.isEmpty() && qtyUnit > 0) {
            SalesBooking salesBooking = listSalesBooking.iterator().next();
            salesBooking.setIdFeature(feature.getIdFeature());
            salesBooking = salesBookingRepository.save(salesBooking);
        } else if (!listSalesBooking.isEmpty() && qtyUnit <= 0) {
            SalesBooking salesBooking = listSalesBooking.iterator().next();
            salesBooking.setDateThru(now);
            salesBooking = salesBookingRepository.save(salesBooking);
        } else if (listSalesBooking.isEmpty() && qtyUnit > 0) {

            SalesBooking sb = new SalesBooking();
            sb.setIdInternal(sur.getInternal().getIdInternal());
            sb.setRequirement(sur);
            sb.setIdProduct(sur.getProduct().getIdProduct());
            sb.setIdFeature(sur.getColor().getIdFeature());
            sb.setDateFrom(now);
            sb.setDateThru(now.plusHours(6));
            sb.setNote(null);
            sb.setOnHand(false);
            sb.setInTransit(false);
            sb.setYearAssembly(sur.getProductYear());
            sb = salesBookingRepository.save(sb);

        }

            vsoService.vsoSalesEdit(sur);

    }

    @Transactional
    public void closeProspectSur(SalesUnitRequirementDTO salesUnitRequirementDTO) {
        SalesUnitRequirement sur =  salesUnitRequirementRepository.findOne(salesUnitRequirementDTO.getIdRequirement());
//        Integer reqtyp = sur.getRequirementType() == null;
        if (sur.getRequirementType() == null) {
            log.debug("close prospect on sur atas == " + salesUnitRequirementDTO);
            Prospect prospect = prospectRepository.findOne(salesUnitRequirementDTO.getIdProspect());
            log.debug("close prospect on sur bawah == " + prospect);
            prospectService.closeProspect(prospect);
        }
    }


    @Transactional
    public void closeSalesBooking(SalesUnitRequirementDTO dto) {
        SalesUnitRequirement sur  = salesUnitRequirementRepository.findOne(dto.getIdRequirement());
        if (sur != null) {
            List<SalesBooking> sb  = salesBookingRepository.listSalesBookingByIdReq(sur.getIdRequirement());
            if (!sb.isEmpty()) {
                for (SalesBooking salesBooking : sb) {
                    salesBookingService.closeSalesBookingActive(salesBooking);
                }
            }
        }
    }

    @Transactional
    public void createIndentOrVSO(SalesUnitRequirementDTO salesUnitRequirementDTO) {
        SalesUnitRequirement sur  = salesUnitRequirementRepository.findOne(salesUnitRequirementDTO.getIdRequirement());
        if (sur != null ){
            if (sur.getSaleType().getIdSaleType() == BaseConstants.SALE_TYPE_CASH ||
                sur.getSaleType().getIdSaleType() == BaseConstants.SALE_TYPE_CASH_CBD ||
                sur.getSaleType().getIdSaleType() == BaseConstants.SALE_TYPE_CASH_COD) {
                changeSalesUnitRequirementStatus(salesUnitRequirementDTO, BaseConstants.STATUS_INDENT);
            } else {
                buildVSOorApprovalLEasing(salesUnitRequirementDTO);
            }
        }
    }

    public Page<SalesUnitRequirementDTO> findAllByApprovalDIscountAndPelanggaranWilayahByKorsal(String idinternal, Integer idstatustype, Pageable pageable) {
        log.debug("Start find all by approval discount, pelanggaran wilayah by sales");
        Salesman salesman = partyUtils.getCurrentSalesman();
        if (idstatustype == BaseConstants.STATUS_APPROVED || idstatustype == BaseConstants.STATUS_NOT_REQUIRED) {
            log.debug("cari SUR yang status approval pelanggaran wilayah dan diskon di approve / not required");

            return salesUnitRequirementRepository.findSalesUnitRequirementByStatusApprovalDiscountAndPelanggaranWilayahByProspectByKorsal(idstatustype, idinternal, salesman.getIdPartyRole(), pageable)
                .map(salesUnitRequirementMapper::toDto);
        } else {
            log.debug("cari SUR yang status approval pelanggaran wilayah atau Diskon sudah di approve / tidak di approve by prospect ");

            return salesUnitRequirementRepository.findSalesUnitRequirementByStatusApprovalDiscountAndPelanggaranWilayahByProspectByKorsal(idstatustype, idinternal, salesman.getIdPartyRole(), pageable)
                .map(salesUnitRequirementMapper::toDto);
        }
    }

    public static InetAddress getClientIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        try {
            return InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            return null;
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void setFacility(SalesUnitRequirementDTO salesUnitRequirementDTO) {
        if (salesUnitRequirementDTO != null) {
            Facility facility = facilityRepository.findOne(salesUnitRequirementDTO.getIdGudang());
            if (facility != null) {
                SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(salesUnitRequirementDTO.getIdRequirement());
                if (sur != null) {
                    sur.setIdGudang(facility.getIdFacility());
                    salesUnitRequirementRepository.save(sur);
                }
            }
        }
    }

    @Transactional(readOnly = true)
    public Page<SalesUnitRequirementDTO> findApprovalLeasingFilterBy(Pageable pageable, OrderPTO param){
        log.debug("tanggal ==" + param.getOrderDateThru());
        return salesUnitRequirementRepository.findApprovalLeasingByFilter(pageable, param).map(salesUnitRequirementMapper::toDto).map(this::assignOtherData);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void cekSurOnSalesNote(SalesUnitRequirementDTO dto){
        List<SalesUnitRequirement> listSur = salesUnitRequirementRepository.getSurOnRequestNote(dto.getIdRequirement());
        if(!listSur.isEmpty()){
            throw new DmsException("Nomor VSO ini Masih Ada Proses Transfer Unit");
        }
    }

}
