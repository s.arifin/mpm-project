package id.atiila.service;

import id.atiila.domain.RuleType;
import id.atiila.repository.RuleTypeRepository;
import id.atiila.repository.search.RuleTypeSearchRepository;
import id.atiila.service.dto.RuleTypeDTO;
import id.atiila.service.mapper.RuleTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing RuleType.
 * BeSmart Team
 */

@Service
@Transactional
public class RuleTypeService {

    private final Logger log = LoggerFactory.getLogger(RuleTypeService.class);

    private final RuleTypeRepository ruleTypeRepository;

    private final RuleTypeMapper ruleTypeMapper;

    private final RuleTypeSearchRepository ruleTypeSearchRepository;

    public RuleTypeService(RuleTypeRepository ruleTypeRepository, RuleTypeMapper ruleTypeMapper, RuleTypeSearchRepository ruleTypeSearchRepository) {
        this.ruleTypeRepository = ruleTypeRepository;
        this.ruleTypeMapper = ruleTypeMapper;
        this.ruleTypeSearchRepository = ruleTypeSearchRepository;
    }

    /**
     * Save a ruleType.
     *
     * @param ruleTypeDTO the entity to save
     * @return the persisted entity
     */
    public RuleTypeDTO save(RuleTypeDTO ruleTypeDTO) {
        log.debug("Request to save RuleType : {}", ruleTypeDTO);
        RuleType ruleType = ruleTypeMapper.toEntity(ruleTypeDTO);
        ruleType = ruleTypeRepository.save(ruleType);
        RuleTypeDTO result = ruleTypeMapper.toDto(ruleType);
        ruleTypeSearchRepository.save(ruleType);
        return result;
    }

    /**
     *  Get all the ruleTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RuleTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RuleTypes");
        return ruleTypeRepository.findAll(pageable)
            .map(ruleTypeMapper::toDto);
    }

    /**
     *  Get one ruleType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public RuleTypeDTO findOne(Integer id) {
        log.debug("Request to get RuleType : {}", id);
        RuleType ruleType = ruleTypeRepository.findOne(id);
        return ruleTypeMapper.toDto(ruleType);
    }

    /**
     *  Delete the  ruleType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete RuleType : {}", id);
        ruleTypeRepository.delete(id);
        ruleTypeSearchRepository.delete(id);
    }

    /**
     * Search for the ruleType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RuleTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of RuleTypes for query {}", query);
        Page<RuleType> result = ruleTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(ruleTypeMapper::toDto);
    }

    public RuleTypeDTO processExecuteData(Integer id, String param, RuleTypeDTO dto) {
        RuleTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<RuleTypeDTO> processExecuteListData(Integer id, String param, Set<RuleTypeDTO> dto) {
        Set<RuleTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
