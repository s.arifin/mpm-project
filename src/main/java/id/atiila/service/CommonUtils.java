package id.atiila.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class CommonUtils {
    @Autowired
    private Environment environment;

    public Boolean getCurrentProfile(String profileName){
        Boolean a = false;

        String[] profiles = environment.getActiveProfiles();
        for(String r: profiles){
            if (r == profileName){
                a = true;
            }
        }

        return a;
    }
}
