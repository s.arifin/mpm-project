package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.atiila.domain.Approval;
import id.atiila.domain.RuleSalesDiscount;
import id.atiila.domain.SalesUnitRequirement;
import id.atiila.repository.ApprovalRepository;
import id.atiila.repository.RuleSalesDiscountRepository;
import id.atiila.repository.search.ApprovalSearchRepository;
import id.atiila.service.dto.ApprovalDTO;
import id.atiila.service.dto.SalesUnitRequirementDTO;
import id.atiila.service.mapper.ApprovalMapper;
import id.atiila.service.mapper.SalesUnitRequirementMapper;

import javax.servlet.http.HttpServletRequest;

/**
 * Service Implementation for managing Approval.
 * BeSmart Team
 */

@Service
@Transactional
public class ApprovalService {

    private final Logger log = LoggerFactory.getLogger(ApprovalService.class);

    private final ApprovalRepository approvalRepository;

    private final ApprovalMapper approvalMapper;

    private final ApprovalSearchRepository approvalSearchRepository;

    @Autowired
    private SalesUnitRequirementMapper salesUnitRequirementMapper;

    @Autowired
    private RuleSalesDiscountRepository ruleSalesDiscountRepository;

    @Autowired
    private PartyUtils partyUtils;

    public ApprovalService(ApprovalRepository approvalRepository, ApprovalMapper approvalMapper, ApprovalSearchRepository approvalSearchRepository) {
        this.approvalRepository = approvalRepository;
        this.approvalMapper = approvalMapper;
        this.approvalSearchRepository = approvalSearchRepository;
    }

    /**
     * Save a approval.
     *
     * @param approvalDTO the entity to save
     * @return the persisted entity
     */
    public ApprovalDTO save(ApprovalDTO approvalDTO) {
        log.debug("Request to save Approval : {}", approvalDTO);
        Approval approval = approvalMapper.toEntity(approvalDTO);
        approval = approvalRepository.save(approval);
        ApprovalDTO result = approvalMapper.toDto(approval);
        approvalSearchRepository.save(approval);
        return result;
    }

    /**
     *  Get all the approvals.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ApprovalDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Approvals");
        return approvalRepository.findAll(pageable)
            .map(approvalMapper::toDto);
    }

    /**
     *  Get one approval by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ApprovalDTO findOne(UUID id) {
        log.debug("Request to get Approval : {}", id);
        Approval approval = approvalRepository.findOne(id);
        return approvalMapper.toDto(approval);
    }

    /**
     *  Delete the  approval by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Approval : {}", id);
        approvalRepository.delete(id);
        approvalSearchRepository.delete(id);
    }

    /**
     * Search for the approval corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ApprovalDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Approvals for query {}", query);
        Page<Approval> result = approvalSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(approvalMapper::toDto);
    }

    public Page<SalesUnitRequirementDTO> findWaitingForApprovalSURForKacab(String idinternal, Pageable pageable) {
        Page<SalesUnitRequirementDTO> dto = approvalRepository.findWaitingForApprovalSURForKacab(idinternal, pageable).map(salesUnitRequirementMapper::toDto);
        return dto;
    }

    public Page<SalesUnitRequirementDTO> findWaitingTerritorialViolationForApprovalSURForKacab(String idinternal, Pageable pageable) {
        Page<SalesUnitRequirementDTO> dto = approvalRepository.findWaitingTerritorialForApprovalSURForKacab(idinternal, pageable).map(salesUnitRequirementMapper::toDto);
        return dto;
    }

    public Page<SalesUnitRequirementDTO> findWaitingForApprovalSURForKorsal(String idinternal, String role, Pageable pageable) {
        log.debug("CARI SUR yang bisa di approve korsal");
        RuleSalesDiscount ruleSalesDiscount = ruleSalesDiscountRepository.findActiveByRoleAndIdInternal("ROLE_KORSAL", partyUtils.getCurrentInternal().getIdInternal()).get(0);
        log.debug("rulesalesdiscount ==> " + ruleSalesDiscount);
        Page<SalesUnitRequirement> s = approvalRepository.findWaitingForApprovalSurForKorsal(idinternal, ruleSalesDiscount.getMaxAmount(), pageable);
        log.debug("s" + s);
        return s.map(salesUnitRequirementMapper::toDto);
    }

    public ApprovalDTO processExecuteData(Integer id, String param, ApprovalDTO dto) {
        ApprovalDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<ApprovalDTO> processExecuteListData(Integer id, String param, Set<ApprovalDTO> dto) {
        Set<ApprovalDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Page<ApprovalDTO> findApproval(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered SalesUnitRequirementDTO");
        String idReq = request.getParameter("idReq");
        log.debug("Request APPROVAL LEASING ++"+idReq);

        if (idReq != null) {
            UUID idrequirement = UUID.fromString(idReq);
            return approvalRepository.findActiveLeasingApprovalByIdReq(idrequirement, pageable)
                .map(approvalMapper::toDto);
        }
        return null;
    }

}
