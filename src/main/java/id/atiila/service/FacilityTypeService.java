package id.atiila.service;

import id.atiila.domain.FacilityType;
import id.atiila.repository.FacilityTypeRepository;
import id.atiila.repository.search.FacilityTypeSearchRepository;
import id.atiila.service.dto.FacilityTypeDTO;
import id.atiila.service.mapper.FacilityTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing FacilityType.
 * BeSmart Team
 */

@Service
@Transactional
public class FacilityTypeService {

    private final Logger log = LoggerFactory.getLogger(FacilityTypeService.class);

    private final FacilityTypeRepository facilityTypeRepository;

    private final FacilityTypeMapper facilityTypeMapper;

    private final FacilityTypeSearchRepository facilityTypeSearchRepository;

    public FacilityTypeService(FacilityTypeRepository facilityTypeRepository, FacilityTypeMapper facilityTypeMapper, FacilityTypeSearchRepository facilityTypeSearchRepository) {
        this.facilityTypeRepository = facilityTypeRepository;
        this.facilityTypeMapper = facilityTypeMapper;
        this.facilityTypeSearchRepository = facilityTypeSearchRepository;
    }

    /**
     * Save a facilityType.
     *
     * @param facilityTypeDTO the entity to save
     * @return the persisted entity
     */
    public FacilityTypeDTO save(FacilityTypeDTO facilityTypeDTO) {
        log.debug("Request to save FacilityType : {}", facilityTypeDTO);
        FacilityType facilityType = facilityTypeMapper.toEntity(facilityTypeDTO);
        facilityType = facilityTypeRepository.save(facilityType);
        FacilityTypeDTO result = facilityTypeMapper.toDto(facilityType);
        facilityTypeSearchRepository.save(facilityType);
        return result;
    }

    /**
     * Get all the facilityTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FacilityTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FacilityTypes");
        return facilityTypeRepository.findAll(pageable)
            .map(facilityTypeMapper::toDto);
    }

    /**
     * Get one facilityType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public FacilityTypeDTO findOne(Integer id) {
        log.debug("Request to get FacilityType : {}", id);
        FacilityType facilityType = facilityTypeRepository.findOne(id);
        return facilityTypeMapper.toDto(facilityType);
    }

    /**
     * Delete the facilityType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete FacilityType : {}", id);
        facilityTypeRepository.delete(id);
        facilityTypeSearchRepository.delete(id);
    }

    /**
     * Search for the facilityType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FacilityTypeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of FacilityTypes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));


        Page<FacilityType> result = facilityTypeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(facilityTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<FacilityTypeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered FacilityTypeDTO");


        return facilityTypeRepository.queryNothing(pageable).map(facilityTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, FacilityTypeDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<FacilityTypeDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        facilityTypeSearchRepository.deleteAll();
        List<FacilityType> facilityTypes =  facilityTypeRepository.findAll();
        for (FacilityType m: facilityTypes) {
            facilityTypeSearchRepository.save(m);
            log.debug("Data Facility Type save !...");
        }
    }

}
