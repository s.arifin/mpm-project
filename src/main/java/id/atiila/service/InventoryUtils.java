package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.service.dto.InventoryItemDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
@Transactional
public class
InventoryUtils {

    private final Logger log = LoggerFactory.getLogger(InventoryUtils.class);

    @Autowired
    private InventoryItemRepository inventoryItemRepository;

    @Autowired
    private UnitPreparationRepository UnitPreparationRepository;

    @Autowired
    private PackageReceiptRepository packageReceiptRepository;

    @Autowired
    private StorageUtils storageUtils;

    @Autowired
    private SalesBookingRepository salesBookingRepository;

    @Autowired
    private InventoryItemService inventoryItemService;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private ShipmentOutgoingRepository shipmentOutgoingRepository;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private ItemIssuanceRepository itemIssuanceRepository;

    @Autowired
    private OrderShipmentItemRepository orderShipmentItemRepository;

    @Autowired
    private BillingReceiptRepository billingReceiptRepository;

    @Autowired
    private OrderBillingItemRepository orderBillingItemRepository;

    @Autowired
    private BillingItemRepository billingItemRepository;

    @Autowired
    private MasterNumberingService numberingService;

    @Autowired
    private BillingTypeRepository billingTypeRepository;

    @Autowired
    private ShipmentTypeRepository shipmentTypeRepository;

    @Autowired
    private FacilityRepository facilityRepository;

    @Autowired
    private ContainerRepository containerRepository;

    public Integer checkAvailabilityBookingUnit(
        String idinternal,
        String idproduct,
        Integer idfeature,
        Integer yearassembly
    ) {
        Integer a = 0;

        List<SalesBooking> salesBookings = salesBookingRepository.getTotalBooking(idinternal, idproduct, idfeature, yearassembly);
        List<InventoryItemDTO> inventoryItemDTOS = inventoryItemService.checkAvailableStock(idinternal, idproduct, idfeature, yearassembly);

        Integer totalSalesBookings = salesBookings.size();
        Integer totalInventoryItems = inventoryItemDTOS.size();

        if (totalSalesBookings < totalInventoryItems) a = totalInventoryItems - totalSalesBookings;
        log.debug("cek totalInventoryItems" + totalInventoryItems);
        log.debug("cek totalSalesBookings" + totalSalesBookings);
        log.debug("cek inventory" + a);
        return a;

    }

    @Transactional(readOnly = true)
    public List<InventoryItem> getItems(UUID idparty, String idProduct, Integer feature) {
        return inventoryItemRepository.findAll();
            //getInventoryItems(idparty, idProduct, feature);
    }

    @Transactional
    public void buildInventoryItem(UnitShipmentReceipt r) {
        if (r.getInventoryItems().size() == 0) return;

        PackageReceipt p = (PackageReceipt) r.getShipmentPackage();

        InventoryItem item = new InventoryItem();
        item.dateCreated(ZonedDateTime.now());
        item.setIdFeature(r.getIdFeature());
        item.setIdProduct(r.getIdProduct());
        item.setQty(r.getQtyAccept() - r.getQtyReject());
        item.setQtyBooking(0d);
        item.setIdOwner(p.getInternal().getOrganization().getIdParty());
        item.setIdFrame(r.getIdFrame());
        item.setIdMachine(r.getIdMachine());
        item.setYearAssembly(r.getYearAssembly());

        GoodContainer container = storageUtils.getGoodContainer(p.getInternal().getOrganization(), r.getIdProduct());
        item.setIdContainer(container != null ? container.getContainer().getIdContainer() : null);

        // Find Good Container / Facility
        if (item.getIdContainer() == null) {
            Facility facl = p.getInternal().getFacility(BaseConstants.PURP_TYPE_WAREHOUSE);
            if (facl != null) item.setIdFacility(facl.getIdFacility());
        }

        //
        r.addInventoryItem(item);
    }

    @Transactional
    protected InventoryItem createInventoryItem(ProductPackageReceipt p, ProductShipmentReceipt r) {
         if (r.getInventoryItems().size() > 0) return r.getInventoryItems().iterator().next();

        InventoryItem item = new InventoryItem();
        item.dateCreated(ZonedDateTime.now());
        item.setIdFeature(r.getIdFeature());
        item.setIdProduct(r.getIdProduct());
        item.setQty(r.getQtyAccept() - r.getQtyReject());
        item.setQtyBooking(0d);
        item.setIdOwner(p.getInternal().getOrganization().getIdParty());
        item.setShipmentReceipt(r);

        GoodContainer container = storageUtils.getGoodContainer(p.getInternal().getOrganization(), r.getIdProduct());
        item.setIdContainer(container != null ? container.getContainer().getIdContainer() : null);

        // Find Good Container / Facility
        if (item.getIdContainer() == null) {
            Facility facl = p.getInternal().getPrimaryFacility();
            if (facl != null) item.setIdFacility(facl.getIdFacility());
        }

        return item;
    }

    @Transactional
    protected InventoryItem createUnitInventoryItem(PackageReceipt p, UnitShipmentReceipt r) {
        if (r.getInventoryItems().size() > 0) return r.getInventoryItems().iterator().next();

        InventoryItem item = new InventoryItem();
        item.dateCreated(ZonedDateTime.now());
        item.setIdFeature(r.getIdFeature());
        item.setIdProduct(r.getIdProduct());
        item.setQty(r.getQtyAccept() - r.getQtyReject());
        item.setQtyBooking(0d);
        item.setIdOwner(p.getInternal().getOrganization().getIdParty());
        item.setShipmentReceipt(r);
        item.setIdFrame(r.getIdFrame());
        item.setIdMachine(r.getIdMachine());
        item.setYearAssembly(r.getYearAssembly());

        GoodContainer container = storageUtils.getGoodContainer(p.getInternal().getOrganization(), r.getIdProduct());
        item.setIdContainer(container != null ? container.getContainer().getIdContainer() : null);

        // Find Good Container / Facility
        if (item.getIdContainer() == null) {
            Facility facl = p.getInternal().getPrimaryFacility();
            if (facl != null) item.setIdFacility(facl.getIdFacility());
        }

        return item;
    }

    public ProductShipmentReceipt buildShipmentReceipt(ProductPackageReceipt p, OrderItem oi, Double qty) {
        ProductShipmentReceipt sr = new ProductShipmentReceipt();

        sr.setIdProduct(oi.getIdProduct());
        sr.setItemDescription(oi.getItemDescription());
        sr.setShipmentPackage(p);
        sr.setQtyAccept(qty);
        sr.setQtyReject(0d);
        sr.setOrderItem(oi);
        sr.dateReceipt(ZonedDateTime.now());
        InventoryItem inv = createInventoryItem(p, sr);
        sr.getInventoryItems().add(inv);
        return sr;
    }

    public UnitShipmentReceipt buildUnitShipmentReceipt(PackageReceipt p, OrderItem oi, String idFrame, String idMachine, Integer yearAss) {
        UnitShipmentReceipt sr = new UnitShipmentReceipt();
        sr.setIdProduct(oi.getIdProduct());
        sr.setItemDescription(oi.getItemDescription());
        sr.setShipmentPackage(p);
        sr.setIdFeature(oi.getIdFeature());
        sr.setQtyAccept(1);
        sr.setQtyReject(0);
        sr.setIdFrame(idFrame);
        sr.setIdMachine(idMachine);
        sr.setYearAssembly(yearAss);
        sr.setOrderItem(oi);
        sr.dateReceipt(ZonedDateTime.now());
        InventoryItem inv = createUnitInventoryItem(p, sr);
        sr.getInventoryItems().add(inv);
        return sr;
    }

    @Transactional
    public void buildInventoryItem(PackageReceipt p) {
        if (p.getCurrentStatus().equals(BaseConstants.STATUS_ACTIVE)) {
            for (ShipmentReceipt r : p.getShipmentReceipts()) {
                if (r.getInventoryItems().size() == 0) {
                    UnitShipmentReceipt us = (UnitShipmentReceipt) r;
                    buildInventoryItem(us);
                }
            }
            p.setStatus(BaseConstants.STATUS_APPROVED);
            packageReceiptRepository.save(p);
        }
    }


    public List<UnitPreparation> savePickings(Iterable<UnitPreparation> spis) {
        return UnitPreparationRepository.save(spis);
    }

    public UnitPreparation save(UnitPreparation spis) {
        return UnitPreparationRepository.save(spis);
    }

    public List<InventoryItem> saveInventoryItems(Iterable<InventoryItem> item) {
        return inventoryItemRepository.save(item);
    }

    public InventoryItem save(InventoryItem item) {
        return inventoryItemRepository.save(item);
    }

    @Transactional
    public List<UnitPreparation> getUnitPreparation(OrderItem oi, List<InventoryItem> items) {
        List<UnitPreparation> spis = new ArrayList<>();
        List<InventoryItem> invitemupds = new ArrayList<>();

        CustomerOrder co = (CustomerOrder) oi.getOrders();
        Double qtyRequest = oi.getQty() - oi.getQtyFilled();
        Double qtyFilled = 0d;

        for (InventoryItem it: items) {
            Double qtyOH = it.getQty() - it.getQtyBooking();
            Double qtyTake = Math.min(qtyOH, qtyRequest);

            Facility fac = null;
            if (it.getIdContainer() != null) {
                fac = containerRepository.findOne(it.getIdContainer()).getFacility();
            } else if (it.getIdFacility() != null) {
                facilityRepository.findOne(it.getIdFacility());
            }

            if (qtyTake > 0) {
                UnitPreparation pis = new UnitPreparation();
                pis.setInventoryItem(it);
                pis.setQty(qtyTake);
                pis.setShipTo(partyUtils.getShipTo(oi.getIdShipTo()));
                pis.orderItem(oi);
                pis.setInternal(co.getInternal());
                pis.setFacility(fac);
                spis.add(pis);

                qtyFilled = qtyFilled + qtyTake;

                // Book Inventory
                it.setQtyBooking(it.getQtyBooking() + qtyTake);
                invitemupds.add(it);

                if (qtyFilled >= qtyRequest) {
                    break;
                }
            }
        }

        // Update booking
        if (invitemupds.size() > 0) inventoryItemRepository.save(invitemupds);

        // Build Picking
        if (spis.size() > 0) spis = UnitPreparationRepository.save(spis);

        return spis;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<ShipmentOutgoing> buildDO(Internal internal, List<UnitPreparation> spis) {
        List<ShipmentOutgoing> rso = new ArrayList<>();
        List<BillingReceipt> rbill = new ArrayList<>();
        List<ItemIssuance> liss = new ArrayList<>();
        List<OrderShipmentItem> losi = new ArrayList<>();
        List<OrderBillingItem> lobi = new ArrayList<>();

        ShipmentOutgoing so = null;
        ShipTo shipFrom = internalUtils.getShipTo(internal);
        BillTo billFrom = internalUtils.getBillTo(internal);
        BillingReceipt bill = null;

        for (UnitPreparation p: spis) {

            // Build SO
            if (so == null || !so.getShipTo().equals(p.getShipTo())) {

                // Cari draft SO
                List<ShipmentOutgoing> lso = shipmentOutgoingRepository.getDraftSO(shipFrom, p.getShipTo());

                if (lso.isEmpty()) {
                    so = new ShipmentOutgoing();
                    so.setShipFrom(shipFrom);
                    so.setShipTo(p.getShipTo());
                    so.setAddressFrom(shipFrom.getParty().getPostalAddress());
                    so.setAddressTo(p.getShipTo().getParty().getPostalAddress());
                    so.setShipmentNumber(numberingService.findTransNumberVDOBy(internal.getIdInternal(), "VDO"));
                    so.setDateSchedulle(ZonedDateTime.now());
                    so.setShipmentType(shipmentTypeRepository.findOne(BaseConstants.SHIPMENT_DO_UNIT));
                    rso.add(so);
                } else {
                    so = lso.get(0);
                    if (rso.indexOf(so) == -1) rso.add(so);
                }
            }

            // Build Billing
            if (bill == null || !bill.getBillTo().getParty().equals(p.getShipTo().getParty())) {

                BillTo billTo = partyUtils.getBillTo(p.getShipTo().getParty());

                // Cari draft Billing
                List<BillingReceipt> lbill = billingReceiptRepository.getDraftBilling(billFrom, billTo);

                if (lbill.isEmpty()) {
                    bill = new BillingReceipt();
                    bill.setBillFrom(billFrom);
                    bill.setBillTo(billTo);
                    bill.setInternal(internal);
                    bill.setDateDue(ZonedDateTime.now().plusDays(1));
                    bill.setBillingNumber(numberingService.findBillingReceipt(internal.getIdInternal(), "IVU"));
                    bill.setBillingType(billingTypeRepository.findOne(BaseConstants.BILLING_TYPE_VEHICLE_SALES));
                    rbill.add(bill);
                    bill = billingReceiptRepository.save(bill);
                } else {
                    bill = lbill.get(0);
                    bill.getDetails().size();
                    if (rbill.indexOf(bill) == -1) rbill.add(bill);
                }
            }

            // Shipment Item
            ShipmentItem si = new ShipmentItem();
            si.assignFrom(p.getOrderItem(), p.getQty());
            si.shipment(so);
            so.addDetail(si);

            // Item Issuance
            ItemIssuance issuance = new ItemIssuance();
            issuance.setInventoryItem(p.getInventoryItem());
            issuance.setQty(p.getQty());
            issuance.setPicking(p);
            issuance.setShipmentItem(si);
            liss.add(issuance);

            // Order Shipment Item
            OrderShipmentItem osi = new OrderShipmentItem();
            osi.setOrderItem(p.getOrderItem());
            osi.setShipmentItem(si);
            osi.setQty(si.getQty());
            losi.add(osi);

            // Build billing Item
            BillingItem billingItem = bill.addMotorCycle(p.getOrderItem());

            // Set HPP
            billingItem.setInventoryItem(p.getInventoryItem());
            billingItem.setBasePrice(p.getInventoryItem().getShipmentReceipt().getOrderItem().getUnitPrice());

            // Set Others Billing Item
            bill.addItem(BaseConstants.ITEM_TYPE_BBN_PRICE, "BBN", p.getOrderItem().getBbn());
            bill.addItem(BaseConstants.ITEM_TYPE_SUBSIDI_DEALER, "Diskon Dealer", p.getOrderItem().getDiscount());
            bill.addItem(BaseConstants.ITEM_TYPE_PPN, "PPN", p.getOrderItem().getTaxAmount());
            billingItem = billingItemRepository.save(billingItem);

            // Build Order billing Item
            OrderBillingItem obi = new OrderBillingItem();
            obi.setBillingItem(billingItem);
            obi.setOrderItem(p.getOrderItem());
            obi.setQty(p.getQty());
            obi.setAmount(billingItem.getTotalAmount());
            lobi.add(obi);
        }

        // Save Shipment Outgoing
        shipmentOutgoingRepository.save(rso);

        // Save Billing Receipt
        billingReceiptRepository.save(rbill);

        // Save Item Issuance
        itemIssuanceRepository.save(liss);

        // Save Order Shipment Item
        orderShipmentItemRepository.save(losi);

        // Save Order Billing Item
        orderBillingItemRepository.save(lobi);

        return rso;
    }
}
