package id.atiila.service;

import id.atiila.domain.InventoryMovement;
import id.atiila.repository.InventoryMovementRepository;
import id.atiila.repository.search.InventoryMovementSearchRepository;
import id.atiila.service.dto.InventoryMovementDTO;
import id.atiila.service.mapper.InventoryMovementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing InventoryMovement.
 * BeSmart Team
 */

@Service
@Transactional
public class InventoryMovementService {

    private final Logger log = LoggerFactory.getLogger(InventoryMovementService.class);

    private final InventoryMovementRepository inventoryMovementRepository;

    private final InventoryMovementMapper inventoryMovementMapper;

    private final InventoryMovementSearchRepository inventoryMovementSearchRepository;

    public InventoryMovementService(InventoryMovementRepository inventoryMovementRepository, InventoryMovementMapper inventoryMovementMapper, InventoryMovementSearchRepository inventoryMovementSearchRepository) {
        this.inventoryMovementRepository = inventoryMovementRepository;
        this.inventoryMovementMapper = inventoryMovementMapper;
        this.inventoryMovementSearchRepository = inventoryMovementSearchRepository;
    }

    /**
     * Save a inventoryMovement.
     *
     * @param inventoryMovementDTO the entity to save
     * @return the persisted entity
     */
    public InventoryMovementDTO save(InventoryMovementDTO inventoryMovementDTO) {
        log.debug("Request to save InventoryMovement : {}", inventoryMovementDTO);
        InventoryMovement inventoryMovement = inventoryMovementMapper.toEntity(inventoryMovementDTO);
        inventoryMovement = inventoryMovementRepository.save(inventoryMovement);
        InventoryMovementDTO result = inventoryMovementMapper.toDto(inventoryMovement);
        inventoryMovementSearchRepository.save(inventoryMovement);
        return result;
    }

    /**
     *  Get all the inventoryMovements.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InventoryMovementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all InventoryMovements");
        return inventoryMovementRepository.findActiveInventoryMovement(pageable)
            .map(inventoryMovementMapper::toDto);
    }

    /**
     *  Get one inventoryMovement by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public InventoryMovementDTO findOne(UUID id) {
        log.debug("Request to get InventoryMovement : {}", id);
        InventoryMovement inventoryMovement = inventoryMovementRepository.findOne(id);
        return inventoryMovementMapper.toDto(inventoryMovement);
    }

    /**
     *  Delete the  inventoryMovement by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete InventoryMovement : {}", id);
        inventoryMovementRepository.delete(id);
        inventoryMovementSearchRepository.delete(id);
    }

    /**
     * Search for the inventoryMovement corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InventoryMovementDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of InventoryMovements for query {}", query);
        Page<InventoryMovement> result = inventoryMovementSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(inventoryMovementMapper::toDto);
    }

    public InventoryMovementDTO processExecuteData(Integer id, String param, InventoryMovementDTO dto) {
        InventoryMovementDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<InventoryMovementDTO> processExecuteListData(Integer id, String param, Set<InventoryMovementDTO> dto) {
        Set<InventoryMovementDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public InventoryMovementDTO changeInventoryMovementStatus(InventoryMovementDTO dto, Integer id) {
        if (dto != null) {
			InventoryMovement e = inventoryMovementRepository.findOne(dto.getIdSlip());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        inventoryMovementSearchRepository.delete(dto.getIdSlip());
                        break;
                    default:
                        inventoryMovementSearchRepository.save(e);
                }
				inventoryMovementRepository.save(e);
			}
		}
        return dto;
    }
}
