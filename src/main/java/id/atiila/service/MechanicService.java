package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.atiila.base.BaseConstants;
import id.atiila.domain.Mechanic;
import id.atiila.domain.Person;
import id.atiila.repository.MechanicRepository;
import id.atiila.repository.search.MechanicSearchRepository;
import id.atiila.service.dto.MechanicDTO;
import id.atiila.service.mapper.MechanicMapper;

import javax.servlet.http.HttpServletRequest;

/**
 * Service Implementation for managing Mechanic.
 * BeSmart Team
 */

@Service
@Transactional
public class MechanicService {

    private final Logger log = LoggerFactory.getLogger(MechanicService.class);

    private final MechanicRepository mechanicRepository;

    private final MechanicMapper mechanicMapper;

    private final MechanicSearchRepository mechanicSearchRepository;

    @Autowired
    private PersonService personSvc;

    @Autowired
    private MasterNumberingService numberSvc;

    public MechanicService(MechanicRepository mechanicRepository, MechanicMapper mechanicMapper, MechanicSearchRepository mechanicSearchRepository) {
        this.mechanicRepository = mechanicRepository;
        this.mechanicMapper = mechanicMapper;
        this.mechanicSearchRepository = mechanicSearchRepository;
    }

    /**
     * Save a mechanic.
     *
     * @param dto the entity to save
     * @return the persisted entity
     */
    public MechanicDTO save(MechanicDTO dto) {
        log.debug("Request to save Mechanic : {}", dto);
        Mechanic mechanic = mechanicMapper.toEntity(dto);

        Boolean isNewData = mechanic.getIdPartyRole() == null;
        if (isNewData) {
            Mechanic mchn = null;

            //Cek Jika Konsumen sudah mempunyai ID
            if (dto.getIdMechanic() != null) {
                mchn = mechanicRepository.findOneByIdMechanic(dto.getIdMechanic());
                if (mchn != null) return mechanicMapper.toDto(mchn);
            }

            //Check Person, apakah sudah ada di database
            Person p = personSvc.getWhenExists(dto.getPerson());

            if (p != null) {
                mchn = mechanicRepository.findOneByParty(p);
                if (mchn != null) return mechanicMapper.toDto(mchn);
                mechanic.setPerson(p);
            };

            //Tidak di temukan, buat customer baru
            // mechanic.setStatus(BaseConstants.STATUS_ACTIVE);

            if (dto.getIdMechanic() == null) {
                String idMchn = null;
                while (idMchn == null || mechanicRepository.findOneByIdMechanic(idMchn) != null) {
                    idMchn = "M" + String.format("%04d", numberSvc.nextValue("idmechanic"));
                }
                mechanic.setIdMechanic(idMchn);
            }
            else
                mechanic.setIdMechanic(dto.getIdMechanic());
        }

        mechanic = mechanicRepository.save(mechanic);
        MechanicDTO result = mechanicMapper.toDto(mechanic);
        mechanicSearchRepository.save(mechanic);
        return result;
    }

    /**
     *  Get all the mechanics.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MechanicDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Mechanics");
        return mechanicRepository.findAll(pageable)
            .map(mechanicMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<MechanicDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        String idInternal = request.getParameter("idInternal");
        Integer idPositionType = request.getParameter("idPositionType") == null ?
            null : Integer.parseInt(request.getParameter("idPositionType"));

        log.debug("Request to get all mechanic");
        return mechanicRepository.findAllByInternal(idInternal, idPositionType, pageable)
            .map(mechanicMapper::toDto);
    }


    /**
     *  Get one mechanic by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public MechanicDTO findOne(UUID id) {
        log.debug("Request to get Mechanic : {}", id);
        Mechanic mechanic = mechanicRepository.findOne(id);
        return mechanicMapper.toDto(mechanic);
    }

    /**
     *  Delete the  mechanic by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Mechanic : {}", id);
        mechanicRepository.delete(id);
        mechanicSearchRepository.delete(id);
    }

    /**
     * Search for the mechanic corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MechanicDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Mechanics for query {}", query);
        Page<Mechanic> result = mechanicSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(mechanicMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        mechanicSearchRepository.deleteAll();
        List<Mechanic> mechanics =  mechanicRepository.findAll();
        for (Mechanic m: mechanics) {
            mechanicSearchRepository.save(m);
            log.debug("Data mechanic save !...");
        }
    }
}
