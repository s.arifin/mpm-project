package id.atiila.service;

import id.atiila.domain.BillingReceipt;
import id.atiila.domain.Internal;
import id.atiila.domain.MasterNumbering;
import id.atiila.domain.UserMediator;
import id.atiila.repository.BillingReceiptRepository;
import id.atiila.repository.search.BillingReceiptSearchRepository;
import id.atiila.service.dto.BillingReceiptDTO;
import id.atiila.service.mapper.BillingReceiptMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing BillingReceipt.
 * BeSmart Team
 */

@Service
@Transactional
public class BillingReceiptService {

    private final Logger log = LoggerFactory.getLogger(BillingReceiptService.class);

    private final BillingReceiptRepository billingReceiptRepository;

    private final BillingReceiptMapper billingReceiptMapper;

    private final BillingReceiptSearchRepository billingReceiptSearchRepository;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private MasterNumberingService numberingService;

    public BillingReceiptService(BillingReceiptRepository billingReceiptRepository, BillingReceiptMapper billingReceiptMapper, BillingReceiptSearchRepository billingReceiptSearchRepository) {
        this.billingReceiptRepository = billingReceiptRepository;
        this.billingReceiptMapper = billingReceiptMapper;
        this.billingReceiptSearchRepository = billingReceiptSearchRepository;
    }

    /**
     * Save a billingReceipt.
     *
     * @param billingReceiptDTO the entity to save
     * @return the persisted entity
     */
    public BillingReceiptDTO save(BillingReceiptDTO billingReceiptDTO) {
        log.debug("Request to save BillingReceipt : {}", billingReceiptDTO);
        BillingReceipt billingReceipt = billingReceiptMapper.toEntity(billingReceiptDTO);
        if (billingReceipt.getBillingNumber() == null) {
            String idInternal = billingReceiptDTO.getInternalId();
            billingReceipt.setBillingNumber(numberingService.findTransNumberVSBBy(idInternal, "IVU"));
        }
        billingReceipt = billingReceiptRepository.save(billingReceipt);
        BillingReceiptDTO result = billingReceiptMapper.toDto(billingReceipt);
        billingReceiptSearchRepository.save(billingReceipt);
        return result;
    }

    /**
     * Get all the billingReceipts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BillingReceiptDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BillingReceipts");
        Internal intr = partyUtils.getCurrentInternal();
        return billingReceiptRepository.findActiveBillingReceipt(intr, pageable)
            .map(billingReceiptMapper::toDto);
    }

    /**
     * Get one billingReceipt by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BillingReceiptDTO findOne(UUID id) {
        log.debug("Request to get BillingReceipt : {}", id);
        BillingReceipt billingReceipt = billingReceiptRepository.findOne(id);
        return billingReceiptMapper.toDto(billingReceipt);
    }

    /**
     * Delete the billingReceipt by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete BillingReceipt : {}", id);
        billingReceiptRepository.delete(id);
        billingReceiptSearchRepository.delete(id);
    }

    /**
     * Search for the billingReceipt corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BillingReceiptDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of BillingReceipts for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idBillingType = request.getParameter("idBillingType");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");
        String idBillFrom = request.getParameter("idBillFrom");

        if (filterName != null) {
        }
        Page<BillingReceipt> result = billingReceiptSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(billingReceiptMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<BillingReceiptDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered BillingReceiptDTO");
        String idBillingType = request.getParameter("idBillingType");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");
        String idBillFrom = request.getParameter("idBillFrom");

        return billingReceiptRepository.findByParams(idBillingType, idInternal, idBillTo, idBillFrom, pageable)
            .map(billingReceiptMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public BillingReceiptDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        BillingReceiptDTO r = null;
        return r;
    }

    @Transactional
    public Set<BillingReceiptDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<BillingReceiptDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public BillingReceiptDTO changeBillingReceiptStatus(BillingReceiptDTO dto, Integer id) {
        if (dto != null) {
			BillingReceipt e = billingReceiptRepository.findOne(dto.getIdBilling());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        billingReceiptSearchRepository.delete(dto.getIdBilling());
                        break;
                    default:
                        billingReceiptSearchRepository.save(e);
                }
				billingReceiptRepository.save(e);
			}
		}
        return dto;
    }
}
