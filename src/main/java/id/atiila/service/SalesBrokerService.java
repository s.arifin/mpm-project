package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.Person;
import id.atiila.domain.SalesBroker;
import id.atiila.repository.SalesBrokerRepository;
import id.atiila.repository.search.SalesBrokerSearchRepository;
import id.atiila.service.dto.SalesBrokerDTO;
import id.atiila.service.mapper.SalesBrokerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Service Implementation for managing SalesBroker.
 * BeSmart Team
 */

@Service
@Transactional
public class SalesBrokerService {

    private final Logger log = LoggerFactory.getLogger(SalesBrokerService.class);

    private final SalesBrokerRepository salesBrokerRepository;

    private final SalesBrokerMapper salesBrokerMapper;

    private final SalesBrokerSearchRepository salesBrokerSearchRepository;

    @Autowired
    private PersonService personSvc;

    @Autowired
    private MasterNumberingService numberSvc;


    public SalesBrokerService(SalesBrokerRepository salesBrokerRepository, SalesBrokerMapper salesBrokerMapper, SalesBrokerSearchRepository salesBrokerSearchRepository) {
        this.salesBrokerRepository = salesBrokerRepository;
        this.salesBrokerMapper = salesBrokerMapper;
        this.salesBrokerSearchRepository = salesBrokerSearchRepository;
    }

    /**
     * Save a salesBroker.
     *
     * @param dto the entity to save
     * @return the persisted entity
     */
    public SalesBrokerDTO save(SalesBrokerDTO dto) {
        log.debug("Request to save SalesBroker : {}", dto);
        SalesBroker salesBroker = salesBrokerMapper.toEntity(dto);
        Boolean isNewData = salesBroker.getIdPartyRole() == null;

        if (isNewData) {
            SalesBroker brkr = null;

            //Cek Jika Broker sudah mempunyai ID
            if (dto.getIdBroker() != null) {
                brkr = salesBrokerRepository.findOneByIdBroker(dto.getIdBroker());
                if (brkr != null) return salesBrokerMapper.toDto(brkr);
            }

            //Check Person, apakah sudah ada di database
            Person p = personSvc.getWhenExists(dto.getPerson());

            if (p != null) {
                brkr = salesBrokerRepository.findOneByParty(p);
                if (brkr != null) return salesBrokerMapper.toDto(brkr);
                salesBroker.setPerson(p);
            };

            //Tetap menggunakan person default
            // salesBroker.setStatus(BaseConstants.STATUS_ACTIVE);

            if (dto.getIdBroker() == null) {
                String idBroker = null;
                while (idBroker == null || salesBrokerRepository.findOneByIdBroker(idBroker) != null) {
                    idBroker = "B1" + String.format("%03d", numberSvc.nextValue("idbroker"));
                }
                salesBroker.setIdBroker(idBroker);
            }
            else
                salesBroker.setIdBroker(dto.getIdBroker());
        }

        salesBroker = salesBrokerRepository.save(salesBroker);
        SalesBrokerDTO result = salesBrokerMapper.toDto(salesBroker);
        salesBrokerSearchRepository.save(salesBroker);
        return result;
    }

    /**
     *  Get all the salesBrokers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesBrokerDTO> findAll(Pageable pageable, Integer idbrokertype) {
        log.debug("Request to get all SalesBrokers");
        Page<SalesBrokerDTO> salesBrokerDTOS;

        if (idbrokertype != null){
            salesBrokerDTOS = salesBrokerRepository.findAllByIdBrokerType(idbrokertype, pageable).map(salesBrokerMapper::toDto);
        }
        else {
            salesBrokerDTOS = salesBrokerRepository.findAll(pageable).map(salesBrokerMapper::toDto);
        }
        return salesBrokerDTOS;
    }

    /**
     *  Get one salesBroker by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SalesBrokerDTO findOne(UUID id) {
        log.debug("Request to get SalesBroker : {}", id);
        SalesBroker salesBroker = salesBrokerRepository.findOne(id);
        return salesBrokerMapper.toDto(salesBroker);
    }

    /**
     *  Delete the  salesBroker by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete SalesBroker : {}", id);
        salesBrokerRepository.delete(id);
        salesBrokerSearchRepository.delete(id);
    }

    /**
     * Search for the salesBroker corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesBrokerDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SalesBrokers for query {}", query);
        Page<SalesBroker> result = salesBrokerSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(salesBrokerMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        salesBrokerSearchRepository.deleteAll();
        List<SalesBroker> salesbrokers =  salesBrokerRepository.findAll();
        for (SalesBroker m: salesbrokers) {
            salesBrokerSearchRepository.save(m);
            log.debug("Data sales broker save !...");
        }
    }
}
