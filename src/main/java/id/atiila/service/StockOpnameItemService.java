package id.atiila.service;

import id.atiila.domain.StockOpname;
import id.atiila.domain.StockOpnameItem;
import id.atiila.repository.StockOpnameItemRepository;
import id.atiila.repository.search.StockOpnameItemSearchRepository;
import id.atiila.service.dto.StockOpnameItemDTO;
import id.atiila.service.mapper.StockOpnameItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing StockOpnameItem.
 * BeSmart Team
 */

@Service
@Transactional
public class StockOpnameItemService {

    private final Logger log = LoggerFactory.getLogger(StockOpnameItemService.class);

    private final StockOpnameItemRepository stockOpnameItemRepository;

    private final StockOpnameItemMapper stockOpnameItemMapper;

    private final StockOpnameItemSearchRepository stockOpnameItemSearchRepository;

    public StockOpnameItemService(StockOpnameItemRepository stockOpnameItemRepository, StockOpnameItemMapper stockOpnameItemMapper, StockOpnameItemSearchRepository stockOpnameItemSearchRepository) {
        this.stockOpnameItemRepository = stockOpnameItemRepository;
        this.stockOpnameItemMapper = stockOpnameItemMapper;
        this.stockOpnameItemSearchRepository = stockOpnameItemSearchRepository;
    }

    /**
     * Save a stockOpnameItem.
     *
     * @param stockOpnameItemDTO the entity to save
     * @return the persisted entity
     */
    public StockOpnameItemDTO save(StockOpnameItemDTO stockOpnameItemDTO) {
        log.debug("Request to save StockOpnameItem : {}", stockOpnameItemDTO);
        StockOpnameItem stockOpnameItem = stockOpnameItemMapper.toEntity(stockOpnameItemDTO);
        stockOpnameItem = stockOpnameItemRepository.save(stockOpnameItem);
        StockOpnameItemDTO result = stockOpnameItemMapper.toDto(stockOpnameItem);
        stockOpnameItemSearchRepository.save(stockOpnameItem);
        return result;
    }

    /**
     * Get all the stockOpnameItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StockOpnameItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all StockOpnameItems");
        return stockOpnameItemRepository.findAll(pageable)
            .map(stockOpnameItemMapper::toDto);
    }

    /**
     * Get one stockOpnameItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public StockOpnameItemDTO findOne(UUID id) {
        log.debug("Request to get StockOpnameItem : {}", id);
        StockOpnameItem stockOpnameItem = stockOpnameItemRepository.findOne(id);
        return stockOpnameItemMapper.toDto(stockOpnameItem);
    }

    /**
     * Delete the stockOpnameItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete StockOpnameItem : {}", id);
        stockOpnameItemRepository.delete(id);
        stockOpnameItemSearchRepository.delete(id);
    }

    /**
     * Search for the stockOpnameItem corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StockOpnameItemDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of StockOpnameItems for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idStockOpname = request.getParameter("idStockOpname");
        String idProduct = request.getParameter("idProduct");
        String idContainer = request.getParameter("idContainer");

        if (idStockOpname != null) {
            q.withQuery(matchQuery("stockOpname.idStockOpname", idStockOpname));
        }
        else if (idProduct != null) {
            q.withQuery(matchQuery("product.idProduct", idProduct));
        }
        else if (idContainer != null) {
            q.withQuery(matchQuery("container.idContainer", idContainer));
        }

        Page<StockOpnameItem> result = stockOpnameItemSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(stockOpnameItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<StockOpnameItemDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered StockOpnameItemDTO");
        String idStockOpname = request.getParameter("idStockOpname");
        String idProduct = request.getParameter("idProduct");
        String idContainer = request.getParameter("idContainer");

        if (idStockOpname != null) {
            return stockOpnameItemRepository.queryByIdStockOpname(UUID.fromString(idStockOpname), pageable).map(stockOpnameItemMapper::toDto);
        }
        else if (idProduct != null) {
            return stockOpnameItemRepository.queryByIdProduct(idProduct, pageable).map(stockOpnameItemMapper::toDto);
        }
        else if (idContainer != null) {
            return stockOpnameItemRepository.queryByIdContainer(UUID.fromString(idContainer), pageable).map(stockOpnameItemMapper::toDto);
        }

        return stockOpnameItemRepository.queryNothing(pageable).map(stockOpnameItemMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, StockOpnameItemDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<StockOpnameItemDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
