package id.atiila.service;

import id.atiila.domain.GLAccount;
import id.atiila.repository.GLAccountRepository;
import id.atiila.repository.search.GLAccountSearchRepository;
import id.atiila.service.dto.GLAccountDTO;
import id.atiila.service.mapper.GLAccountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing GLAccount.
 * atiila consulting
 */

@Service
@Transactional
public class GLAccountService {

    private final Logger log = LoggerFactory.getLogger(GLAccountService.class);

    private final GLAccountRepository gLAccountRepository;

    private final GLAccountMapper gLAccountMapper;

    private final GLAccountSearchRepository gLAccountSearchRepository;

    public GLAccountService(GLAccountRepository gLAccountRepository, GLAccountMapper gLAccountMapper, GLAccountSearchRepository gLAccountSearchRepository) {
        this.gLAccountRepository = gLAccountRepository;
        this.gLAccountMapper = gLAccountMapper;
        this.gLAccountSearchRepository = gLAccountSearchRepository;
    }

    /**
     * Save a gLAccount.
     *
     * @param gLAccountDTO the entity to save
     * @return the persisted entity
     */
    public GLAccountDTO save(GLAccountDTO gLAccountDTO) {
        log.debug("Request to save GLAccount : {}", gLAccountDTO);
        GLAccount gLAccount = gLAccountMapper.toEntity(gLAccountDTO);
        gLAccount = gLAccountRepository.save(gLAccount);
        GLAccountDTO result = gLAccountMapper.toDto(gLAccount);
        gLAccountSearchRepository.save(gLAccount);
        return result;
    }

    /**
     * Get all the gLAccounts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GLAccountDTO> findAll(Pageable pageable) {
        log.debug("Request to get all GLAccounts");
        return gLAccountRepository.findAll(pageable)
            .map(gLAccountMapper::toDto);
    }

    /**
     * Get one gLAccount by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public GLAccountDTO findOne(UUID id) {
        log.debug("Request to get GLAccount : {}", id);
        GLAccount gLAccount = gLAccountRepository.findOne(id);
        return gLAccountMapper.toDto(gLAccount);
    }

    /**
     * Delete the gLAccount by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete GLAccount : {}", id);
        gLAccountRepository.delete(id);
        gLAccountSearchRepository.delete(id);
    }

    /**
     * Search for the gLAccount corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GLAccountDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of GLAccounts for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idGLAccountType = request.getParameter("idGLAccountType");

        if (idGLAccountType != null) {
            q.withQuery(matchQuery("accountType.idGLAccountType", idGLAccountType));
        }

        Page<GLAccount> result = gLAccountSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(gLAccountMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<GLAccountDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered GLAccountDTO");
        String idGLAccountType = request.getParameter("idGLAccountType");

        if (idGLAccountType != null) {
            return gLAccountRepository.queryByIdGLAccountType(Integer.valueOf(idGLAccountType), pageable).map(gLAccountMapper::toDto); 
        }

        return gLAccountRepository.queryNothing(pageable).map(gLAccountMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, GLAccountDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<GLAccountDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
