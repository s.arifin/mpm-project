package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.Organization;
import id.atiila.domain.ProspectOrganization;
import id.atiila.repository.ProspectOrganizationRepository;
import id.atiila.repository.search.ProspectOrganizationSearchRepository;
import id.atiila.service.dto.*;
//import id.atiila.service.dto.ProspectOrganizationDTO;
import id.atiila.service.mapper.OrganizationMapper;
import id.atiila.service.mapper.ProspectOrganizationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing ProspectOrganization.
 * BeSmart Team
 */

@Service
@Transactional
public class ProspectOrganizationService {

    private final Logger log = LoggerFactory.getLogger(ProspectOrganizationService.class);

    private final ProspectOrganizationRepository prospectOrganizationRepository;

    private final ProspectOrganizationMapper prospectOrganizationMapper;

    private final ProspectOrganizationSearchRepository prospectOrganizationSearchRepository;

    @Autowired
    private OrganizationService organizationSrv;

    @Autowired
    private OrganizationMapper organizationMapper;

    @Autowired
    private InternalService internalSvc;

    public ProspectOrganizationService(ProspectOrganizationRepository prospectOrganizationRepository, ProspectOrganizationMapper prospectOrganizationMapper, ProspectOrganizationSearchRepository prospectOrganizationSearchRepository) {
        this.prospectOrganizationRepository = prospectOrganizationRepository;
        this.prospectOrganizationMapper = prospectOrganizationMapper;
        this.prospectOrganizationSearchRepository = prospectOrganizationSearchRepository;
    }

    /**
     * Save a prospectOrganization.
     *
     * @param prospectOrganizationDTO the entity to save
     * @return the persisted entity
     */
    public ProspectOrganizationDTO save(ProspectOrganizationDTO prospectOrganizationDTO) {
        log.debug("Request to save ProspectOrganization : {}", prospectOrganizationDTO);
        Boolean noInternal = prospectOrganizationDTO.getDealerId() == null;
        Organization organization = organizationSrv.findOneByName(prospectOrganizationDTO.getOrganization().getName());

        if ( noInternal ){
            InternalDTO i = internalSvc.findOne("10001");
            prospectOrganizationDTO.setDealerId(i.getIdInternal());
        }

        ProspectOrganization prospectOrganization = prospectOrganizationMapper.toEntity(prospectOrganizationDTO);
        if (prospectOrganization.getIdProspect() == null  && organization != null){ prospectOrganization.setOrganization(organization); }
        prospectOrganization = prospectOrganizationRepository.save(prospectOrganization);

        /*

        tulis step sebelum save


         */

        //save detail
        ProspectOrganizationDTO result = prospectOrganizationMapper.toDto(prospectOrganization);
        prospectOrganizationSearchRepository.save(prospectOrganization);
        return result;
    }

    /**
     *  Get all the prospectOrganizations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProspectOrganizationDTO> findAll(Pageable pageable, Integer idstatustype) {
        log.debug("Request to get all ProspectOrganizations");
        Page<ProspectOrganizationDTO> prospectOrganizationDTO = null;
        if (idstatustype != null){
            prospectOrganizationDTO =  prospectOrganizationRepository.findProspectOrganizationByStatusType(idstatustype,pageable).map(prospectOrganizationMapper::toDto);
        }
        else {
            prospectOrganizationDTO = prospectOrganizationRepository.findActiveProspectOrganization(pageable).map(prospectOrganizationMapper::toDto);
        }
        return prospectOrganizationDTO;
    }

    /**
     *  Get one prospectOrganization by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ProspectOrganizationDTO findOne(UUID id) {
        log.debug("Request to get ProspectOrganization : {}", id);
        ProspectOrganization prospectOrganization = prospectOrganizationRepository.findOne(id);
        return prospectOrganizationMapper.toDto(prospectOrganization);
    }

    /**
     *  Delete the  prospectOrganization by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ProspectOrganization : {}", id);
        prospectOrganizationRepository.delete(id);
        prospectOrganizationSearchRepository.delete(id);
    }

    /**
     * Search for the prospectOrganization corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProspectOrganizationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ProspectOrganizations for query {}", query);
        Page<ProspectOrganization> result = prospectOrganizationSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(prospectOrganizationMapper::toDto);
    }

    public ProspectOrganizationDTO processExecuteData(Integer id, String param, ProspectOrganizationDTO dto) {
        ProspectOrganizationDTO r = dto;
        if (r != null) {
            switch (id) {
                case 122:

                    changeProspectOrganizationStatus(r,BaseConstants.STATUS_COMPLETED);
                    break;
                case 123:

                    changeProspectOrganizationStatus(r,BaseConstants.STATUS_LOW);
                    break;
                case 124:

                    changeProspectOrganizationStatus(r,BaseConstants.STATUS_MEDIUM);
                    break;
                case 125:

                    changeProspectOrganizationStatus(r,BaseConstants.STATUS_HOT);
                    break;

                default:
                    break;
            }
		}
        return r;
    }

    public Set<ProspectOrganizationDTO> processExecuteListData(Integer id, String param, Set<ProspectOrganizationDTO> dto) {
        Set<ProspectOrganizationDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public ProspectOrganizationDTO changeProspectOrganizationStatus(ProspectOrganizationDTO dto, Integer id) {
        if (dto != null) {
			ProspectOrganization e = prospectOrganizationRepository.findOne(dto.getIdProspect());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        prospectOrganizationSearchRepository.delete(dto.getIdProspect());
                        break;
                    default:
                        prospectOrganizationSearchRepository.save(e);
                }
				prospectOrganizationRepository.save(e);
			}
		}
        return dto;
    }
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateStatusProspectBasedOnPurchasePlan(CommunicationEventProspectDTO ce){
        log.debug("Request to search for a page of ProspectOrganizations for query {}" + ce);
        ProspectOrganization p = prospectOrganizationRepository.findOne(ce.getIdProspect());
        ProspectOrganizationDTO pDTO = prospectOrganizationMapper.toDto(p);


        if (ce.getPurchasePlan().contains("< 2 Minggu")){
            changeProspectOrganizationStatus(pDTO,BaseConstants.STATUS_HOT);
        }
        else if (ce.getPurchasePlan().contains("< 1 Bulan")){
            changeProspectOrganizationStatus(pDTO,BaseConstants.STATUS_MEDIUM);
        }
        else if (ce.getPurchasePlan().contains("> 1 Bulan")){
            changeProspectOrganizationStatus(pDTO,BaseConstants.STATUS_LOW);
        }
    }
}
