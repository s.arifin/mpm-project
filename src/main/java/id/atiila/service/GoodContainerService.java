package id.atiila.service;

import id.atiila.domain.GoodContainer;
import id.atiila.repository.GoodContainerRepository;
import id.atiila.repository.search.GoodContainerSearchRepository;
import id.atiila.service.dto.GoodContainerDTO;
import id.atiila.service.mapper.GoodContainerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing GoodContainer.
 * BeSmart Team
 */

@Service
@Transactional
public class GoodContainerService {

    private final Logger log = LoggerFactory.getLogger(GoodContainerService.class);

    private final GoodContainerRepository goodContainerRepository;

    private final GoodContainerMapper goodContainerMapper;

    private final GoodContainerSearchRepository goodContainerSearchRepository;

    public GoodContainerService(GoodContainerRepository goodContainerRepository, GoodContainerMapper goodContainerMapper, GoodContainerSearchRepository goodContainerSearchRepository) {
        this.goodContainerRepository = goodContainerRepository;
        this.goodContainerMapper = goodContainerMapper;
        this.goodContainerSearchRepository = goodContainerSearchRepository;
    }

    /**
     * Save a goodContainer.
     *
     * @param goodContainerDTO the entity to save
     * @return the persisted entity
     */
    public GoodContainerDTO save(GoodContainerDTO goodContainerDTO) {
        log.debug("Request to save GoodContainer : {}", goodContainerDTO);
        GoodContainer goodContainer = goodContainerMapper.toEntity(goodContainerDTO);
        goodContainer = goodContainerRepository.save(goodContainer);
        GoodContainerDTO result = goodContainerMapper.toDto(goodContainer);
        goodContainerSearchRepository.save(goodContainer);
        return result;
    }

    /**
     * Get all the goodContainers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GoodContainerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all GoodContainers");
        return goodContainerRepository.findAll(pageable)
            .map(goodContainerMapper::toDto);
    }

    /**
     * Get one goodContainer by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public GoodContainerDTO findOne(UUID id) {
        log.debug("Request to get GoodContainer : {}", id);
        GoodContainer goodContainer = goodContainerRepository.findOne(id);
        return goodContainerMapper.toDto(goodContainer);
    }

    /**
     * Delete the goodContainer by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete GoodContainer : {}", id);
        goodContainerRepository.delete(id);
        goodContainerSearchRepository.delete(id);
    }

    /**
     * Search for the goodContainer corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GoodContainerDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of GoodContainers for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idGood = request.getParameter("idGood");
        String idContainer = request.getParameter("idContainer");
        String idOrganization = request.getParameter("idOrganization");

        if (idGood != null) {
            q.withQuery(matchQuery("good.idProduct", idGood));
        }
        else if (idContainer != null) {
            q.withQuery(matchQuery("container.idContainer", idContainer));
        }
        else if (idOrganization != null) {
            q.withQuery(matchQuery("organization.idParty", idOrganization));
        }

        Page<GoodContainer> result = goodContainerSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(goodContainerMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<GoodContainerDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered GoodContainerDTO");
        String idGood = request.getParameter("idGood");
        String idContainer = request.getParameter("idContainer");
        String idOrganization = request.getParameter("idOrganization");

        if (idGood != null) {
            return goodContainerRepository.findByIdGood(idGood, pageable).map(goodContainerMapper::toDto);
        }
        else if (idContainer != null) {
            return goodContainerRepository.findByIdContainer(UUID.fromString(idContainer), pageable).map(goodContainerMapper::toDto);
        }
        else if (idOrganization != null) {
            return goodContainerRepository.findByIdOrganization(UUID.fromString(idOrganization), pageable).map(goodContainerMapper::toDto);
        }

        return goodContainerRepository.findAll(pageable).map(goodContainerMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, GoodContainerDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<GoodContainerDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        goodContainerSearchRepository.deleteAll();
        List<GoodContainer> goodContainers =  goodContainerRepository.findAll();
        for (GoodContainer g: goodContainers) {
            goodContainerSearchRepository.save(g);
            log.debug("Data Good Container save !...");
        }
    }


}
