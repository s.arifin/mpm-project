package id.atiila.service.util;

public class CustomTextUtils {

    public static String elasticSearchEscapeString (String input) {
        final String[] metaCharacters =
            {"+", "-", "=", "&&", "||", ">", "<", "!", "(", ")", "{", "}",
             "[", "]", "^", "\"", "~", "?", ":", "/"};

        input = input.replace("\\", "\\"+"\\");
        String output = input;
        for (int i = 0 ; i < metaCharacters.length ; i++){
            if (input.contains(metaCharacters[i])){
                output = input.replace(metaCharacters[i],"\\"+metaCharacters[i]);
                input = output;
            }
        }
        return output;
    }

    public static String convertUUIDToMSGUIDFormat(String uuid){
        uuid = uuid.replaceAll("-", "");
        uuid = uuid.replaceAll("(.{8})(.{4})(.{4})(.{4})(.{12})", "$1-$2-$3-$4-$5")
                   .replaceAll("(.{2})(.{2})(.{2})(.{2}).(.{2})(.{2}).(.{2})(.{2})(.{18})", "$4$3$2$1-$6$5-$8$7$9");
        return uuid;
    }
}
