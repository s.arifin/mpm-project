package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import id.atiila.base.XlsxUtils;
import id.atiila.domain.GeoBoundary;
import id.atiila.domain.Province;
import id.atiila.repository.GeoBoundaryRepository;
import id.atiila.repository.ProvinceRepository;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import id.atiila.domain.City;
import id.atiila.repository.CityRepository;
import id.atiila.repository.search.CitySearchRepository;
import id.atiila.service.dto.CityDTO;
import id.atiila.service.mapper.CityMapper;
import javax.annotation.PostConstruct;

/**
 * Service Implementation for managing City.
 * BeSmart Team
 */

@Service
@Transactional
@DependsOn({"provinceService"})
public class CityService {

    private final Logger log = LoggerFactory.getLogger(CityService.class);

    private final CityRepository cityRepository;

    private final CityMapper cityMapper;

    private final CitySearchRepository citySearchRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private GeoBoundaryRepository geoRepository;

    public CityService(CityRepository cityRepository, CityMapper cityMapper, CitySearchRepository citySearchRepository) {
        this.cityRepository = cityRepository;
        this.cityMapper = cityMapper;
        this.citySearchRepository = citySearchRepository;
    }

    /**
     * Save a city.
     *
     * @param cityDTO the entity to save
     * @return the persisted entity
     */
    public CityDTO save(CityDTO cityDTO) {
        log.debug("Request to save City : {}", cityDTO);
        City city = cityMapper.toEntity(cityDTO);
        city = cityRepository.save(city);
        CityDTO result = cityMapper.toDto(city);
        citySearchRepository.save(city);
        return result;
    }

    /**
     *  Get all the cities.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Cities");
        return cityRepository.findAll(pageable)
            .map(cityMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<CityDTO> findAllByProvince(UUID idProvince) {
        log.debug("Request to get all Cities");

        List<? extends GeoBoundary> cityGeos = geoRepository.findAllByParent(idProvince);
        List<City> cities = (List<City>) cityGeos;

        return cityMapper.toDto(cities);
    }
    /**
     *  Get one city by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CityDTO findOne(UUID id) {
        log.debug("Request to get City : {}", id);
        City city = cityRepository.findOne(id);
        return cityMapper.toDto(city);
    }

    /**
     *  Delete the  city by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete City : {}", id);
        cityRepository.delete(id);
        citySearchRepository.delete(id);
    }

    /**
     * Search for the city corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CityDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Cities for query {}", query);
        Page<City> result = citySearchRepository.search(queryStringQuery(query), pageable);
        return result.map(cityMapper::toDto);
    }

    public CityDTO processExecuteData(Integer id, String param, CityDTO dto) {
        CityDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<CityDTO> processExecuteListData(Integer id, String param, Set<CityDTO> dto) {
        Set<CityDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public void addCity(String prov, String id, String name) {
        City p = cityRepository.findByGeoCode(id);
        if (p == null) {
            Province province = provinceRepository.findByGeoCode(prov);
            p = new City();
            p.setGeoCode(id);
            p.setDescription(name);
            p.setParent(province);
            cityRepository.save(p);
        }
    }

    @Transactional
    public void initialize() {
        try {

            // Jika data sudah ada, abaikan
            if (!cityRepository.findAll().isEmpty()) return;

            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/city.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/city.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<CityDTO> cities = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (CityDTO o: cities) {
                    if (o.getParentGeoCode() != null && o.getGeoCode() != null) {
                        addCity(o.getParentGeoCode(), o.getGeoCode(), o.getDescription());
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
