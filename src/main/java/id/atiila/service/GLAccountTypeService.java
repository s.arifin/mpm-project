package id.atiila.service;

import id.atiila.domain.GLAccountType;
import id.atiila.repository.GLAccountTypeRepository;
import id.atiila.repository.search.GLAccountTypeSearchRepository;
import id.atiila.service.dto.GLAccountTypeDTO;
import id.atiila.service.mapper.GLAccountTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing GLAccountType.
 * atiila consulting
 */

@Service
@Transactional
public class GLAccountTypeService {

    private final Logger log = LoggerFactory.getLogger(GLAccountTypeService.class);

    private final GLAccountTypeRepository gLAccountTypeRepository;

    private final GLAccountTypeMapper gLAccountTypeMapper;

    private final GLAccountTypeSearchRepository gLAccountTypeSearchRepository;

    public GLAccountTypeService(GLAccountTypeRepository gLAccountTypeRepository, GLAccountTypeMapper gLAccountTypeMapper, GLAccountTypeSearchRepository gLAccountTypeSearchRepository) {
        this.gLAccountTypeRepository = gLAccountTypeRepository;
        this.gLAccountTypeMapper = gLAccountTypeMapper;
        this.gLAccountTypeSearchRepository = gLAccountTypeSearchRepository;
    }

    /**
     * Save a gLAccountType.
     *
     * @param gLAccountTypeDTO the entity to save
     * @return the persisted entity
     */
    public GLAccountTypeDTO save(GLAccountTypeDTO gLAccountTypeDTO) {
        log.debug("Request to save GLAccountType : {}", gLAccountTypeDTO);
        GLAccountType gLAccountType = gLAccountTypeMapper.toEntity(gLAccountTypeDTO);
        gLAccountType = gLAccountTypeRepository.save(gLAccountType);
        GLAccountTypeDTO result = gLAccountTypeMapper.toDto(gLAccountType);
        gLAccountTypeSearchRepository.save(gLAccountType);
        return result;
    }

    /**
     * Get all the gLAccountTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GLAccountTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all GLAccountTypes");
        return gLAccountTypeRepository.findAll(pageable)
            .map(gLAccountTypeMapper::toDto);
    }

    /**
     * Get one gLAccountType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public GLAccountTypeDTO findOne(Integer id) {
        log.debug("Request to get GLAccountType : {}", id);
        GLAccountType gLAccountType = gLAccountTypeRepository.findOne(id);
        return gLAccountTypeMapper.toDto(gLAccountType);
    }

    /**
     * Delete the gLAccountType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete GLAccountType : {}", id);
        gLAccountTypeRepository.delete(id);
        gLAccountTypeSearchRepository.delete(id);
    }

    /**
     * Search for the gLAccountType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GLAccountTypeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of GLAccountTypes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));


        Page<GLAccountType> result = gLAccountTypeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(gLAccountTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<GLAccountTypeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered GLAccountTypeDTO");
        return gLAccountTypeRepository.queryNothing(pageable).map(gLAccountTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, GLAccountTypeDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<GLAccountTypeDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
