package id.atiila.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.base.executeprocess.UnitDocumentMessageConstants;
import id.atiila.domain.*;
import id.atiila.repository.ApprovalRepository;
import id.atiila.repository.SalesUnitRequirementRepository;
import id.atiila.repository.UnitDocumentMessageRepository;
import id.atiila.repository.search.UnitDocumentMessageSearchRepository;
import id.atiila.service.dto.*;
import id.atiila.service.mapper.UnitDocumentMessageMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing UnitDocumentMessage.
 * BeSmart Team
 */

@Service
@Transactional
public class UnitDocumentMessageService {

    private final Logger log = LoggerFactory.getLogger(UnitDocumentMessageService.class);

    private final UnitDocumentMessageRepository unitDocumentMessageRepository;

    private final UnitDocumentMessageMapper unitDocumentMessageMapper;

    private final UnitDocumentMessageSearchRepository unitDocumentMessageSearchRepository;

    @Autowired
    private SalesUnitRequirementService salesUnitRequirementService;

    @Autowired
    private MasterNumberingService masterNumberingService;

    @Autowired
    private RequestRequirementService requestRequirementService;

    @Autowired
    private RequestItemService requestItemService;

    @Autowired
    private ApprovalUtils approvalUtils;
    @Autowired
    private ApprovalRepository approvalRepository;

    @Autowired
    private DateUtils dateUtils;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private SalesUnitRequirementRepository salesUnitRequirementRepository;



    public UnitDocumentMessageService(UnitDocumentMessageRepository unitDocumentMessageRepository, UnitDocumentMessageMapper unitDocumentMessageMapper, UnitDocumentMessageSearchRepository unitDocumentMessageSearchRepository) {
        this.unitDocumentMessageRepository = unitDocumentMessageRepository;
        this.unitDocumentMessageMapper = unitDocumentMessageMapper;
        this.unitDocumentMessageSearchRepository = unitDocumentMessageSearchRepository;
    }

    /**
     * Save a unitDocumentMessage.
     *
     * @param unitDocumentMessageDTO the entity to save
     * @return the persisted entity
     */
    public UnitDocumentMessageDTO save(UnitDocumentMessageDTO unitDocumentMessageDTO) {
        log.debug("Request to save UnitDocumentMessage : {}", unitDocumentMessageDTO);
        UnitDocumentMessage unitDocumentMessage = unitDocumentMessageMapper.toEntity(unitDocumentMessageDTO);
        unitDocumentMessage = unitDocumentMessageRepository.save(unitDocumentMessage);
        UnitDocumentMessageDTO result = unitDocumentMessageMapper.toDto(unitDocumentMessage);
        unitDocumentMessageSearchRepository.save(unitDocumentMessage);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<CustomUnitDocumentMessageDTO> findByStatusJoinRequirement(HttpServletRequest request, Pageable pageable){
        String queryFor = request.getParameter("queryFor");
        Integer idStatustype = Integer.valueOf(request.getParameter("idStatustype"));
        ZonedDateTime endDate  = ZonedDateTime.now();
        ZonedDateTime startDate  = ZonedDateTime.now();
        ZonedDateTime awal = ZonedDateTime.of(startDate.getYear(), startDate.getMonth().getValue(), startDate.getDayOfMonth(), 0, 0, 0, 0, startDate.getZone());
        Internal internal = partyUtils.getCurrentInternal();
        log.debug("query forr = " + queryFor);
        log.debug("startdate message == " + awal);
        log.debug("endDate message = " + endDate);
        if (awal != null && endDate != null && idStatustype == 10) {
            return unitDocumentMessageRepository.findByStatusJoinRequirement(internal != null ? internal.getIdInternal(): null, awal, endDate, pageable);
        } else if (awal != null && endDate != null && idStatustype == 12) {
            return unitDocumentMessageRepository.findByStatusJoinRequirementPending(internal != null ? internal.getIdInternal(): null, pageable);
        } else if (awal != null && endDate != null && idStatustype == 11) {
            return unitDocumentMessageRepository.findByStatusJoinRequirementApprove(internal != null ? internal.getIdInternal(): null, pageable);
        } else if (awal != null && endDate != null && idStatustype == 62) {
            return unitDocumentMessageRepository.findByStatusJoinRequirementNotApprove(internal != null ? internal.getIdInternal(): null, pageable);
        } else if (awal != null && endDate != null && idStatustype == 69) {
            return unitDocumentMessageRepository.findByStatusJoinRequirementNotAvailable(internal != null ? internal.getIdInternal(): null, pageable);
        }
        return null;
    }

    @Transactional(readOnly = true)
    public List<UnitDocumentMessageDTO> findByStatus(Integer idStatus){
        List<UnitDocumentMessage> list = unitDocumentMessageRepository.findByStatusApproval(idStatus);
        List<UnitDocumentMessageDTO> list_dto = unitDocumentMessageMapper.toDto(list);
        return list_dto;
    }

    @Transactional(readOnly = true)
    public List<UnitDocumentMessageDTO> findByRequirement(UUID idRequirement){
        List<UnitDocumentMessage> list = unitDocumentMessageRepository.findAllByIdRequirement(idRequirement);
        List<UnitDocumentMessageDTO> list_dto = unitDocumentMessageMapper.toDto(list);
        return list_dto;
    };

    @Transactional(readOnly = true)
    public List<UnitDocumentMessageDTO> findByRequirementAndApprovalStatus(UUID idRequirement, Integer idStatus){
        List<UnitDocumentMessage> list = unitDocumentMessageRepository.findByRequirementAndApprovalStatus(idRequirement, idStatus);
        List<UnitDocumentMessageDTO> list_dto = unitDocumentMessageMapper.toDto(list);
        return list_dto;
    }

    /**
     * Get all the unitDocumentMessages.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UnitDocumentMessageDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UnitDocumentMessages");
        return unitDocumentMessageRepository.findAll(pageable)
            .map(unitDocumentMessageMapper::toDto);
    }

    /**
     * Get one unitDocumentMessage by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public UnitDocumentMessageDTO findOne(Integer id) {
        log.debug("Request to get UnitDocumentMessage : {}", id);
        UnitDocumentMessage unitDocumentMessage = unitDocumentMessageRepository.findOne(id);
        return unitDocumentMessageMapper.toDto(unitDocumentMessage);
    }

    /**
     * Delete the unitDocumentMessage by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete UnitDocumentMessage : {}", id);
        unitDocumentMessageRepository.delete(id);
        unitDocumentMessageSearchRepository.delete(id);
    }

    /**
     * Search for the unitDocumentMessage corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UnitDocumentMessageDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of UnitDocumentMessages for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idMessage = request.getParameter("idMessage");

        if (filterName != null) {
        }
        Page<UnitDocumentMessage> result = unitDocumentMessageSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(unitDocumentMessageMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<UnitDocumentMessageDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered UnitDocumentMessageDTO");
        String idMessage = request.getParameter("idMessage");
        String idreq = request.getParameter("idreq");

        if(idMessage != null) {
            return unitDocumentMessageRepository.findByParams(idMessage, pageable)
                .map(unitDocumentMessageMapper::toDto);
        }
        if (idreq != null) {
            return unitDocumentMessageRepository.findByIdReq(UUID.fromString(idreq), pageable)
                .map(unitDocumentMessageMapper::toDto);
        }
        return null;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public Set<UnitDocumentMessageDTO> processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        Integer id = Integer.parseInt(request.getParameter("execute").toString());
        Set<UnitDocumentMessageDTO> r = new HashSet<>();
        ObjectMapper mapper = null;

        switch (id){
            case UnitDocumentMessageConstants.SALES_NOTE_APPROVED:
                log.debug("START APPROVE SALES NOTE");

                String idInternal = request.getParameter("idInternal");
                if (idInternal == null){
                    throw new DmsException("internal tidak ada");
                }

                UnitDocumentMessageDTO a = new UnitDocumentMessageDTO();
                mapper = new ObjectMapper();
                mapper.findAndRegisterModules();
                mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                a = mapper.convertValue(item.get("item"), new TypeReference<UnitDocumentMessageDTO>() {});

                SalesUnitRequirementDTO surDTO = salesUnitRequirementService.findOne(a.getIdRequirement());

//                changeStatusApprovalUnitDocumentMessage(a, BaseConstants.STATUS_APPROVED);
                approvalUtils.changeStatusApprovalUnitDocumentMessage(a, BaseConstants.STATUS_APPROVED);

                RequestRequirementDTO requestRequirementDTO = mappingRequestRequirementFromUnitDocumentMessage(a, idInternal);
                RequestRequirementDTO rs = requestRequirementService.save(requestRequirementDTO);

                RequestItemDTO requestItemDTO = mappingRequestItemDTO(rs, surDTO);
                requestItemService.save(requestItemDTO);

//                salesUnitRequirementService.buildVSO(surDTO);
                changeStatusUnitDocumentMessage(a, BaseConstants.STATUS_OPEN);
                break;
            case UnitDocumentMessageConstants.SALES_NOTE_REJECTED:
                log.debug("REJECT ==> " + item);

                mapper = new ObjectMapper();
                mapper.findAndRegisterModules();
                mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                UnitDocumentMessageDTO b = mapper.convertValue(item.get("items"), new TypeReference<UnitDocumentMessageDTO>() {});
                log.debug("approvalUtils " + approvalUtils);
                approvalUtils.changeStatusApprovalUnitDocumentMessage(b, BaseConstants.STATUS_REJECTED);
                changeStatusUnitDocumentMessage(b, BaseConstants.STATUS_REJECTED);
                break;
            default:
                break;
        }
        return r;
    }

    private RequestItemDTO mappingRequestItemDTO(RequestRequirementDTO dto, SalesUnitRequirementDTO sur_dto){
        RequestItemDTO r = new RequestItemDTO();

        r.setIdProduct(sur_dto.getProductId());
        r.setQtyReq(1.0);
        r.setRequestId(dto.getIdRequest());
        r.setIdFeature(sur_dto.getColorId());

        return r;
    }

    private RequestRequirementDTO mappingRequestRequirementFromUnitDocumentMessage(UnitDocumentMessageDTO dto, String idInternal) {
        log.debug("START Mapping UnitDocumentMessage to RequestRequirement");

        String requestNumber = masterNumberingService.findTransNumberRequestNumber("REQ");

        RequestRequirementDTO r = new RequestRequirementDTO();
        r.setQty(1);
        r.setIdRequirement(dto.getIdRequirement());
        r.setRequestTypeId(2);
        r.setRequestNumber(requestNumber);
        r.setIdInternal(idInternal);

        return r;
    }

    @Transactional
    public Set<UnitDocumentMessageDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<UnitDocumentMessageDTO> r = new HashSet<>();
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public UnitDocumentMessageDTO changeStatusApprovalUnitDocumentMessage(UnitDocumentMessageDTO dto, Integer id){
        log.debug("SERVICE to change status approval Unit Document Message");
        if (dto != null) {
            UnitDocumentMessage e = unitDocumentMessageRepository.findOne(dto.getId());
            if (!e.getCurrentApprovalStatus().equals(id)) {
                e.setStatus(id);
                switch (id) {
                    case BaseConstants.STATUS_APPROVED:
                        break;
                    case BaseConstants.STATUS_REJECTED:
                        break;
                }
                unitDocumentMessageRepository.save(e);
            }
        }

        return dto;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public UnitDocumentMessageDTO changeStatusUnitDocumentMessage(UnitDocumentMessageDTO dto, Integer id){
        log.debug("SERVICE to change status approval Unit Document Message");
        if (dto != null) {
            UnitDocumentMessage e = unitDocumentMessageRepository.findOne(dto.getId());
            if (!e.getCurrentStatus().equals(id)) {
                e.setStatusDocument(id);
                switch (id) {
                    case BaseConstants.STATUS_APPROVED:
                        break;
                    case BaseConstants.STATUS_REJECTED:
                        break;
                }
                unitDocumentMessageRepository.save(e);
            }
        }

        return dto;
    }


}
