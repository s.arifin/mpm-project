package id.atiila.service;

import id.atiila.base.XlsxUtils;
import id.atiila.domain.Province;
import id.atiila.repository.ProvinceRepository;
import id.atiila.repository.search.ProvinceSearchRepository;
import id.atiila.service.dto.ProvinceDTO;
import id.atiila.service.mapper.ProvinceMapper;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing Province.
 * BeSmart Team
 */

@Service
@Transactional
public class ProvinceService {

    private final Logger log = LoggerFactory.getLogger(ProvinceService.class);

    private final ProvinceRepository provinceRepository;

    private final ProvinceMapper provinceMapper;

    private final ProvinceSearchRepository provinceSearchRepository;

    public ProvinceService(ProvinceRepository provinceRepository, ProvinceMapper provinceMapper, ProvinceSearchRepository provinceSearchRepository) {
        this.provinceRepository = provinceRepository;
        this.provinceMapper = provinceMapper;
        this.provinceSearchRepository = provinceSearchRepository;
    }

    /**
     * Save a province.
     *
     * @param provinceDTO the entity to save
     * @return the persisted entity
     */
    public ProvinceDTO save(ProvinceDTO provinceDTO) {
        log.debug("Request to save Province : {}", provinceDTO);
        Province province = provinceMapper.toEntity(provinceDTO);
        province = provinceRepository.save(province);
        ProvinceDTO result = provinceMapper.toDto(province);
        provinceSearchRepository.save(province);
        return result;
    }

    /**
     *  Get all the provinces.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProvinceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Provinces");
        return provinceRepository.findAll(pageable)
            .map(provinceMapper::toDto);
    }

    /**
     *  Get one province by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ProvinceDTO findOne(UUID id) {
        log.debug("Request to get Province : {}", id);
        Province province = provinceRepository.findOne(id);
        return provinceMapper.toDto(province);
    }

    /**
     *  Delete the  province by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Province : {}", id);
        provinceRepository.delete(id);
        provinceSearchRepository.delete(id);
    }

    /**
     * Search for the province corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProvinceDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Provinces for query {}", query);
        Page<Province> result = provinceSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(provinceMapper::toDto);
    }

    public ProvinceDTO processExecuteData(Integer id, String param, ProvinceDTO dto) {
        ProvinceDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<ProvinceDTO> processExecuteListData(Integer id, String param, Set<ProvinceDTO> dto) {
        Set<ProvinceDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public void addProvince(String id, String name) {
        Province p = provinceRepository.findByGeoCode(id);
        if (p == null) {
            p = new Province();
            p.setGeoCode(id);
            p.setDescription(name);
            provinceRepository.save(p);
        }
    }

    @Transactional
    public void initialize() {
        try {
            // Jika data sudah ada, abaikan
            if (!provinceRepository.findAll().isEmpty()) return;

            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/province.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/provinsi.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<ProvinceDTO> provinces = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (ProvinceDTO o: provinces) {
                    if (o.getGeoCode() != null) {
                        addProvince(o.getGeoCode(), o.getDescription());
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
