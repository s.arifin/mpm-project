package id.atiila.service;

import id.atiila.domain.SalesOrder;
import id.atiila.repository.SalesOrderRepository;
import id.atiila.repository.search.SalesOrderSearchRepository;
import id.atiila.service.dto.SalesOrderDTO;
import id.atiila.service.mapper.SalesOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing SalesOrder.
 * BeSmart Team
 */

@Service
@Transactional
public class SalesOrderService {

    private final Logger log = LoggerFactory.getLogger(SalesOrderService.class);

    private final SalesOrderRepository salesOrderRepository;

    private final SalesOrderMapper salesOrderMapper;

    private final SalesOrderSearchRepository salesOrderSearchRepository;

    public SalesOrderService(SalesOrderRepository salesOrderRepository, SalesOrderMapper salesOrderMapper, SalesOrderSearchRepository salesOrderSearchRepository) {
        this.salesOrderRepository = salesOrderRepository;
        this.salesOrderMapper = salesOrderMapper;
        this.salesOrderSearchRepository = salesOrderSearchRepository;
    }

    /**
     * Save a salesOrder.
     *
     * @param salesOrderDTO the entity to save
     * @return the persisted entity
     */
    public SalesOrderDTO save(SalesOrderDTO salesOrderDTO) {
        log.debug("Request to save SalesOrder : {}", salesOrderDTO);
        SalesOrder salesOrder = salesOrderMapper.toEntity(salesOrderDTO);
        salesOrder = salesOrderRepository.save(salesOrder);
        SalesOrderDTO result = salesOrderMapper.toDto(salesOrder);
        salesOrderSearchRepository.save(salesOrder);
        return result;
    }

    /**
     * Get all the salesOrders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SalesOrders");
        return salesOrderRepository.findActiveSalesOrder(pageable)
            .map(salesOrderMapper::toDto);
    }

    /**
     * Get one salesOrder by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SalesOrderDTO findOne(UUID id) {
        log.debug("Request to get SalesOrder : {}", id);
        SalesOrder salesOrder = salesOrderRepository.findOne(id);
        return salesOrderMapper.toDto(salesOrder);
    }

    /**
     * Delete the salesOrder by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete SalesOrder : {}", id);
        salesOrderRepository.delete(id);
        salesOrderSearchRepository.delete(id);
    }

    /**
     * Search for the salesOrder corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesOrderDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of SalesOrders for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idInternal = request.getParameter("idInternal");
        String idCustomer = request.getParameter("idCustomer");
        String idBillTo = request.getParameter("idBillTo");
        String idSaleType = request.getParameter("idSaleType");

        if (filterName != null) {
        }
        Page<SalesOrder> result = salesOrderSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(salesOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<SalesOrderDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered SalesOrderDTO");
        String idInternal = request.getParameter("idInternal");
        String idCustomer = request.getParameter("idCustomer");
        String idBillTo = request.getParameter("idBillTo");
        String idSaleType = request.getParameter("idSaleType");

        return salesOrderRepository.findByParams(idInternal, idCustomer, idBillTo, idSaleType, pageable)
            .map(salesOrderMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public SalesOrderDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        SalesOrderDTO r = null;
        return r;
    }

    @Transactional
    public Set<SalesOrderDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<SalesOrderDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public SalesOrderDTO changeSalesOrderStatus(SalesOrderDTO dto, Integer id) {
        if (dto != null) {
			SalesOrder e = salesOrderRepository.findOne(dto.getIdOrder());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        salesOrderSearchRepository.delete(dto.getIdOrder());
                        break;
                    default:
                        salesOrderSearchRepository.save(e);
                }
				salesOrderRepository.save(e);
			}
		}
        return dto;
    }
}
