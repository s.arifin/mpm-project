package id.atiila.service;

import id.atiila.service.dto.InternalBankMapingDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Interface for managing InternalBankMaping.
 */
@Service
@Transactional
public interface InternalBankMapingService {

    /**
     * Save a internalBankMaping.
     *
     * @param internalBankMapingDTO the entity to save
     * @return the persisted entity
     */
    InternalBankMapingDTO save(InternalBankMapingDTO internalBankMapingDTO);

    /**
     *  Get all the internalBankMapings.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<InternalBankMapingDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" internalBankMaping.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    InternalBankMapingDTO findOne(Long id);

    /**
     *  Delete the "id" internalBankMaping.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the internalBankMaping corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<InternalBankMapingDTO> search(String query, Pageable pageable);
}
