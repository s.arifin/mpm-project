package id.atiila.service;

import id.atiila.domain.Internal;
import id.atiila.domain.InternalBankMaping;
import id.atiila.repository.InternalBankMapingRepository;
import id.atiila.repository.InternalRepository;
import id.atiila.repository.search.InternalSearchRepository;
import id.atiila.service.dto.InternalDTO;
import id.atiila.service.dto.VendorDTO;
import id.atiila.service.mapper.InternalMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Internal.
 * BeSmart Team
 */

@Service
@Transactional
public class InternalService {

    private final Logger log = LoggerFactory.getLogger(InternalService.class);

    public static final String QUEU_APPEND = "addMSO";

    private final InternalRepository<Internal> internalRepository;

    private final InternalMapper internalMapper;

    private final InternalSearchRepository internalSearchRepository;

    @Autowired
    private InternalUtils internalUtils;
    @Autowired
    private InternalBankMapingRepository internalBankMapingRepository;
    @Autowired
    private PartyUtils partyUtils;


    public InternalService(InternalRepository<Internal> internalRepository, InternalMapper internalMapper, InternalSearchRepository internalSearchRepository) {
        this.internalRepository = internalRepository;
        this.internalMapper = internalMapper;
        this.internalSearchRepository = internalSearchRepository;
    }

    /**
     * Save a internal.
     *
     * @param internalDTO the entity to save
     * @return the persisted entity
     */
    public InternalDTO save(InternalDTO internalDTO) {
        log.debug("Request to save Internal : {}", internalDTO);
        Internal internal = internalMapper.toEntity(internalDTO);
        internal = internalRepository.save(internal);
        InternalDTO result = internalMapper.toDto(internal);
        internalSearchRepository.save(internal);
        return result;
    }

    /**
     * Get all the internals.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InternalDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Internals");
        return internalRepository.findAll(pageable)
            .map(internalMapper::toDto);
    }

    /**
     * Get one internal by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public InternalDTO findOne(String id) {
        log.debug("Request to get Internal : {}", id);
        Internal internal = internalRepository.findOne(id);
        return internalMapper.toDto(internal);
    }

    /**
     * Delete the internal by id.
     *
     * @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Internal : {}", id);
        internalRepository.delete(id);
        internalSearchRepository.delete(id);
    }

    /**
     * Search for the internal corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InternalDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Internals for query {}", query);
        Page<Internal> result = internalSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(internalMapper::toDto);
    }

    @Transactional(readOnly = true)
    public InternalDTO processExecuteData(Integer id, String param, InternalDTO dto) {
        InternalDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional(readOnly = true)
    public Set<InternalDTO> processExecuteListData(Integer id, String param, Set<InternalDTO> dto) {
        Set<InternalDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional(readOnly = true)
    public Page<InternalDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered VendorDTO");
        String idVendor = request.getParameter("idVendor");
        String bankMapping = request.getParameter("bankMapping");
        if (idVendor != null) {
            return internalRepository.queryByVendor(idVendor, pageable).map(internalMapper::toDto);
        }
        if(bankMapping.equalsIgnoreCase("true")){
            Internal internal = partyUtils.getCurrentInternal();
            Internal intr = new Internal();
            List<InternalBankMaping> internalBankMaping = internalBankMapingRepository.FindByIdRoot(internal.getRoot().getIdInternal());
            if(internalBankMaping.iterator().next().getIsppn() == 0){
                return  internalRepository.FindByRoot(internalBankMaping.iterator().next().getIdinternal(), pageable).map(internalMapper::toDto);
            }
        }
        return internalRepository.queryNothing(pageable).map(internalMapper::toDto);
    }

    @JmsListener(destination = QUEU_APPEND)
    public void addMSO(InternalDTO dto) {
        internalUtils.buildMSO(dto.getIdInternal(), dto.getOrganization().getName());
        log.info("Add MSO : " + dto.getIdInternal() + " " + dto.getOrganization().getName());
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        internalSearchRepository.deleteAll();
        List<Internal> items =  internalRepository.findAll();
        for (Internal m: items) {
            internalSearchRepository.save(m);
            log.debug("Data motor save !...");
        }
    }

}
