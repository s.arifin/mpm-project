package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.atiila.base.XlsxUtils;
import id.atiila.domain.City;
import id.atiila.domain.GeoBoundary;
import id.atiila.repository.CityRepository;
import id.atiila.repository.GeoBoundaryRepository;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import id.atiila.domain.District;
import id.atiila.repository.DistrictRepository;
import id.atiila.repository.search.DistrictSearchRepository;
import id.atiila.service.dto.DistrictDTO;
import id.atiila.service.mapper.DistrictMapper;
import javax.annotation.PostConstruct;

/**
 * Service Implementation for managing District.
 * BeSmart Team
 */

@Service
@Transactional
@DependsOn({"cityService"})
public class DistrictService {

    private final Logger log = LoggerFactory.getLogger(DistrictService.class);

    private final DistrictRepository districtRepository;

    private final DistrictMapper districtMapper;

    private final DistrictSearchRepository districtSearchRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private GeoBoundaryRepository geoRepository;

    public DistrictService(DistrictRepository districtRepository, DistrictMapper districtMapper, DistrictSearchRepository districtSearchRepository) {
        this.districtRepository = districtRepository;
        this.districtMapper = districtMapper;
        this.districtSearchRepository = districtSearchRepository;
    }

    /**
     * Save a district.
     *
     * @param districtDTO the entity to save
     * @return the persisted entity
     */
    public DistrictDTO save(DistrictDTO districtDTO) {
        log.debug("Request to save District : {}", districtDTO);
        District district = districtMapper.toEntity(districtDTO);
        district = districtRepository.save(district);
        DistrictDTO result = districtMapper.toDto(district);
        districtSearchRepository.save(district);
        return result;
    }

    /**
     *  Get all the districts.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DistrictDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Districts");
        return districtRepository.findAll(pageable)
            .map(districtMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<DistrictDTO> findAllByCity (UUID idCity) {
        log.debug("Request to get all Districts");

        List<? extends GeoBoundary> districtGeos = geoRepository.findAllByParent(idCity);
        List<District> cities = (List<District>) districtGeos;

        return districtMapper.toDto(cities);
    }

    /**
     *  Get one district by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public DistrictDTO findOne(UUID id) {
        log.debug("Request to get District : {}", id);
        District district = districtRepository.findOne(id);
        return districtMapper.toDto(district);
    }

    /**
     *  Delete the  district by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete District : {}", id);
        districtRepository.delete(id);
        districtSearchRepository.delete(id);
    }

    /**
     * Search for the district corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DistrictDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Districts for query {}", query);
        Page<District> result = districtSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(districtMapper::toDto);
    }

    public DistrictDTO processExecuteData(Integer id, String param, DistrictDTO dto) {
        DistrictDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<DistrictDTO> processExecuteListData(Integer id, String param, Set<DistrictDTO> dto) {
        Set<DistrictDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public void addDistrict(String idCity, String id, String name) {
        District p = districtRepository.findByGeoCode(id);
        if (p == null) {
            City city = cityRepository.findByGeoCode(idCity);
            p = new District();
            p.setGeoCode(id);
            p.setDescription(name);
            p.setParent(city);
            districtRepository.save(p);
        }
    }

    @Transactional
    public void initialize() {
        try {

            // Jika data sudah ada, abaikan
            if (!districtRepository.findAll().isEmpty()) return;

            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/kecamatan.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/kecamatan.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<DistrictDTO> districts = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (DistrictDTO o: districts) {
                    if (o.getParentGeoCode() != null && o.getGeoCode() != null) {
                        addDistrict(o.getParentGeoCode(), o.getGeoCode(), o.getDescription());
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
