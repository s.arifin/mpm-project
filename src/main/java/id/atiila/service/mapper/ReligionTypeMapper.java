package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ReligionTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity ReligionType and its DTO ReligionTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ReligionTypeMapper extends EntityMapper <ReligionTypeDTO, ReligionType> {
    
    

    default ReligionType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        ReligionType religionType = new ReligionType();
        religionType.setIdReligionType(id);
        return religionType;
    }
}
