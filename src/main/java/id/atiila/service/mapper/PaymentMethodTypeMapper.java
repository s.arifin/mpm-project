package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PaymentMethodTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity PaymentMethodType and its DTO PaymentMethodTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PaymentMethodTypeMapper extends EntityMapper<PaymentMethodTypeDTO, PaymentMethodType> {



    default PaymentMethodType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        PaymentMethodType paymentMethodType = new PaymentMethodType();
        paymentMethodType.setIdPaymentMethodType(id);
        return paymentMethodType;
    }
}
