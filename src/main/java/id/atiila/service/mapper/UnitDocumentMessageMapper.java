package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.UnitDocumentMessageDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity UnitDocumentMessage and its DTO UnitDocumentMessageDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UnitDocumentMessageMapper extends EntityMapper<UnitDocumentMessageDTO, UnitDocumentMessage> {

    @Mapping(source = "unitDocumentMessage.idMessage", target = "parentId")
    UnitDocumentMessageDTO toDto(UnitDocumentMessage unitDocumentMessage);

    @Mapping(source = "parentId", target = "parent")
    UnitDocumentMessage toEntity(UnitDocumentMessageDTO unitDocumentMessageDTO);

    default UnitDocumentMessage fromId(Integer id) {
        if (id == null) {
            return null;
        }
        UnitDocumentMessage unitDocumentMessage = new UnitDocumentMessage();
        unitDocumentMessage.setIdMessage(id);
        return unitDocumentMessage;
    }
}
