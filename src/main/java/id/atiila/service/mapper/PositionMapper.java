package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PositionDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Position and its DTO PositionDTO.
 */
@Mapper(componentModel = "spring", uses = {PositionTypeMapper.class, OrganizationMapper.class, UserMediatorMapper.class, InternalMapper.class})
public interface PositionMapper extends EntityMapper <PositionDTO, Position> {

    @Mapping(source = "positionType.idPositionType", target = "positionTypeId")
    @Mapping(source = "positionType.description", target = "positionTypeDescription")

    @Mapping(source = "organization.idParty", target = "organizationId")
    @Mapping(source = "organization.name", target = "organizationName")

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")

    @Mapping(source = "owner.idUserMediator", target = "ownerId")
    @Mapping(source = "owner.userName", target = "ownerName")

    @Mapping(source = "status", target = "currentStatus")
    PositionDTO toDto(Position position);

    @Mapping(source = "positionTypeId", target = "positionType")
    @Mapping(source = "organizationId", target = "organization")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "ownerId", target = "owner")
    @Mapping(target = "statuses", ignore = true)
    @Mapping(target = "status", ignore = true)
    Position toEntity(PositionDTO positionDTO);

    default Position fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Position position = new Position();
        position.setIdPosition(id);
        return position;
    }
}
