package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ReturnPurchaseOrderDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ReturnPurchaseOrder and its DTO ReturnPurchaseOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {VendorMapper.class, InternalMapper.class, BillToMapper.class})
public interface ReturnPurchaseOrderMapper extends EntityMapper<ReturnPurchaseOrderDTO, ReturnPurchaseOrder> {

    @Mapping(source = "vendor.idVendor", target = "vendorId")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    ReturnPurchaseOrderDTO toDto(ReturnPurchaseOrder returnPurchaseOrder);

    @Mapping(source = "vendorId", target = "vendor")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "billToId", target = "billTo")
    ReturnPurchaseOrder toEntity(ReturnPurchaseOrderDTO returnPurchaseOrderDTO);

    default ReturnPurchaseOrder fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ReturnPurchaseOrder returnPurchaseOrder = new ReturnPurchaseOrder();
        returnPurchaseOrder.setIdOrder(id);
        return returnPurchaseOrder;
    }
}
