package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.SalesmanDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Salesman and its DTO SalesmanDTO.
 */
@Mapper(componentModel = "spring", uses = {RoleTypeMapper.class, PersonMapper.class, })
public interface SalesmanMapper extends EntityMapper <SalesmanDTO, Salesman> {

    @Mapping(source = "coordinatorSales.idPartyRole", target = "coordinatorSalesId")
    @Mapping(source = "coordinatorSales.person.name", target = "coordinatorSalesName")
    @Mapping(source = "teamLeaderSales.idPartyRole", target = "teamLeaderSalesId")
    @Mapping(source = "teamLeaderSales.person.name", target = "teamLeaderSalesName")
    @Mapping(source = "party.idParty", target = "partyId")
    @Mapping(source = "party.name", target = "partyName")
    SalesmanDTO toDto(Salesman salesman);

    @Mapping(source = "coordinatorSalesId", target = "coordinatorSales")
    @Mapping(source = "teamLeaderSalesId", target = "teamLeaderSales")
    @Mapping(source = "partyId", target = "party")
    Salesman toEntity(SalesmanDTO salesmanDTO);

    default Salesman fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Salesman salesman = new Salesman();
        salesman.setIdPartyRole(id);
        return salesman;
    }
}
