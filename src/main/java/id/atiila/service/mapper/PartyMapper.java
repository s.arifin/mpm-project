package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.repository.PartyRepository;
import id.atiila.service.dto.OrganizationDTO;
import id.atiila.service.dto.PartyDTO;
import id.atiila.service.dto.PersonDTO;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

/**
 * Mapper for the entity Party and its DTO PartyDTO.
 */
@Mapper(componentModel = "spring", uses = {ContactMechanismMapper.class, PartyCategoryMapper.class,
                    FacilityMapper.class, PostalAddressMapper.class })
public abstract class PartyMapper {

    @Autowired
    private PartyRepository repo;

    public abstract Organization toEntity(OrganizationDTO d);

    public abstract OrganizationDTO toDto(Organization e);

    public abstract Person toEntity(PersonDTO d);

    public abstract PersonDTO toDto(Person e);

    public Party toEntity(PartyDTO d) {
        if (d instanceof PersonDTO) return toEntity((PersonDTO) d);
        else if (d instanceof OrganizationDTO) return  toEntity((OrganizationDTO) d);
        else return null;
    };

    public PartyDTO toDto(Party e) {
        if (e instanceof Person) return toDto((Person) e);
        else if (e instanceof Organization) return toDto((Organization) e);
        else return null;
    };

    public Party fromId(UUID id) {
        if (id == null) {
            return null;
        }
        return repo.findOne(id);
    }
}
