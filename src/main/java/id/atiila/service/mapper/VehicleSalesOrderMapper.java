package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.*;
import org.mapstruct.*;
import java.util.UUID;
import id.atiila.service.dto.SummaryFincoDTO;

/**
 * Mapper for the entity VehicleSalesOrder and its DTO VehicleSalesOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, CustomerMapper.class, BillToMapper.class,
    PaymentApplicationMapper.class, SalesUnitRequirementMapper.class, LeasingCompany.class, PersonMapper.class})
public interface VehicleSalesOrderMapper extends EntityMapper <VehicleSalesOrderDTO, VehicleSalesOrder> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "customer.idCustomer", target = "customerId")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    @Mapping(source = "leasing.organization.name", target ="leasingName")
//    @Mapping(source = "salesman.idPartyRole", target = "salesmanId")
//    @Mapping(source = "salesman.party.name", target = "salesmanName")
    //@Mapping(source = "salesUnitRequirement.idRequirement", target = "surId")
    VehicleSalesOrderDTO toDto(VehicleSalesOrder vehicleSalesOrder);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "customerId", target = "customer")
    @Mapping(source = "billToId", target = "billTo")
//    @Mapping(source = "salesmanId", target = "salesman")
    //@Mapping(source = "surId", target = "salesUnitRequirement")
    VehicleSalesOrder toEntity(VehicleSalesOrderDTO vehicleSalesOrderDTO);

    @Mapping(source = "bastFinco", target = "bastFinco" )
    VehicleSalesOrderDTO toDto(SummaryFincoDTO Summary);

    default VehicleSalesOrder fromId(UUID id) {
        if (id == null) {
            return null;
        }
        VehicleSalesOrder vehicleSalesOrder = new VehicleSalesOrder();
        vehicleSalesOrder.setIdOrder(id);
        return vehicleSalesOrder;
    }
}
