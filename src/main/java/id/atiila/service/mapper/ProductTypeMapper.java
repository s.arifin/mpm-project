package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProductTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity ProductType and its DTO ProductTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProductTypeMapper extends EntityMapper <ProductTypeDTO, ProductType> {
    
    

    default ProductType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        ProductType productType = new ProductType();
        productType.setIdProductType(id);
        return productType;
    }
}
