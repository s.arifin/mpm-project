package id.atiila.service.mapper;

    import id.atiila.domain.*;
    import id.atiila.service.dto.SaleTypeDTO;
    import org.mapstruct.*;

/**
 * Mapper for the entity SaleType and its DTO SaleTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SaleTypeMapper extends EntityMapper <SaleTypeDTO, SaleType> {

    @Mapping(source = "idParent", target = "parentId")
    SaleTypeDTO toDto(SaleType saleType);

    @Mapping(source = "parentId", target = "idParent")
    SaleType toEntity(SaleTypeDTO saleTypeDTO);

    default SaleType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        SaleType saleType = new SaleType();
        saleType.setIdSaleType(id);
        return saleType;
    }
}
