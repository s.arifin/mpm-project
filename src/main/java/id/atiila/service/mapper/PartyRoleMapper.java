package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PartyRoleDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity PartyRole and its DTO PartyRoleDTO.
 */
@Mapper(componentModel = "spring", uses = {RoleTypeMapper.class, PartyMapper.class, })
public interface PartyRoleMapper extends EntityMapper <PartyRoleDTO, PartyRole> {

    @Mapping(source = "party.name", target = "partyName")
    PartyRoleDTO toDto(PartyRole partyRole);

    PartyRole toEntity(PartyRoleDTO partyRoleDTO);

    default PartyRole fromId(UUID id) {
        if (id == null) {
            return null;
        }
        PartyRole partyRole = new PartyRole();
        partyRole.setIdPartyRole(id);
        return partyRole;
    }
}
