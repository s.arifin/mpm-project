package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PaymentApplicationDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity PaymentApplication and its DTO PaymentApplicationDTO.
 */
@Mapper(componentModel = "spring", uses = {PaymentMapper.class, BillingMapper.class})
public interface PaymentApplicationMapper extends EntityMapper<PaymentApplicationDTO, PaymentApplication> {

    @Mapping(source = "payment.idPayment", target = "paymentId")
    @Mapping(source = "payment.paymentNumber", target = "paymentPaymentNumber")
    @Mapping(source = "billing.idBilling", target = "billingId")
    @Mapping(source = "billing.billingNumber", target = "billingBillingNumber")
    PaymentApplicationDTO toDto(PaymentApplication paymentApplication);

    @Mapping(source = "paymentId", target = "payment")
    @Mapping(source = "billingId", target = "billing")
    PaymentApplication toEntity(PaymentApplicationDTO paymentApplicationDTO);

    default PaymentApplication fromId(UUID id) {
        if (id == null) {
            return null;
        }
        PaymentApplication paymentApplication = new PaymentApplication();
        paymentApplication.setIdPaymentApplication(id);
        return paymentApplication;
    }
}
