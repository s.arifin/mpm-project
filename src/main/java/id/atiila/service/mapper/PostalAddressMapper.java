package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PostalAddressDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity PostalAddress and its DTO PostalAddressDTO.
 */
@Mapper(componentModel = "spring", uses = {GeoBoundaryMapper.class, DistrictMapper.class, VillageMapper.class, ProvinceMapper.class, CityMapper.class})
public interface PostalAddressMapper extends EntityMapper <PostalAddressDTO, PostalAddress> {


    @Mapping(source = "district.idGeobou", target = "districtId")
    @Mapping(source = "district.geoCode", target = "districtCode")
    @Mapping(source = "district.description", target = "districtName")

    @Mapping(source = "village.idGeobou", target = "villageId")
    @Mapping(source = "village.geoCode", target = "villageCode")
    @Mapping(source = "village.description", target = "villageName")

    @Mapping(source = "city.idGeobou", target = "cityId")
    @Mapping(source = "city.geoCode", target = "cityCode")
    @Mapping(source = "city.description", target = "cityName")

    @Mapping(source = "province.idGeobou", target = "provinceId")
    @Mapping(source = "province.geoCode", target = "provinceCode")
    @Mapping(source = "province.description", target = "provinceName")
    PostalAddressDTO toDto(PostalAddress entity);


    @Mapping(source = "districtId", target = "district")
    @Mapping(source = "villageId", target = "village")
    @Mapping(source = "cityId", target = "city")
    @Mapping(source = "provinceId", target = "province")
    PostalAddress toEntity(PostalAddressDTO dto);

    default PostalAddress fromId(UUID id) {
        if (id == null) {
            return null;
        }
        PostalAddress postalAddress = new PostalAddress();
        postalAddress.setIdContact(id);
        return postalAddress;
    }
}
