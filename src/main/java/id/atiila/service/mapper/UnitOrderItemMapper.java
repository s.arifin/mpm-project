package id.atiila.service.mapper;

import id.atiila.domain.OrderItem;
import id.atiila.domain.VehicleSalesOrder;
import id.atiila.repository.VehicleSalesOrderRepository;
import id.atiila.service.dto.OrderItemDTO;
import id.atiila.service.dto.UnitOrderItemDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

/**
 * Mapper for the entity OrderItem and its DTO OrderItemDTO.
 */
@Mapper(componentModel = "spring", uses = {VehicleSalesOrderMapper.class, SalesmanMapper.class})
public abstract class UnitOrderItemMapper {

    @Mapping(source = "orders.idOrder", target = "ordersId")
    @Mapping(source = "orders.orderNumber", target = "orderNumber")
    @Mapping(source = "orders.dateOrder", target = "dateOrder")
    @Mapping(source = "orders.currentStatus", target = "currentStatus")
    public abstract UnitOrderItemDTO toDto(OrderItem orderItem);

    @Mapping(source = "ordersId", target = "orders")
    @Mapping(target = "shipmentItems", ignore = true)
    @Mapping(target = "billingItems", ignore = true)
    public abstract OrderItem toEntity(UnitOrderItemDTO dto);

    @Mapping(source = "ordersId", target = "orders")
    @Mapping(target = "shipmentItems", ignore = true)
    @Mapping(target = "billingItems", ignore = true)
    public abstract OrderItem toEntity(OrderItemDTO dto);

    public  OrderItem fromId(UUID id) {
        if (id == null) {
            return null;
        }
        OrderItem orderItem = new OrderItem();
        orderItem.setIdOrderItem(id);
        return orderItem;
    }
}
