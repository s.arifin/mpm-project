package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.FeatureTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity FeatureType and its DTO FeatureTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FeatureTypeMapper extends EntityMapper<FeatureTypeDTO, FeatureType> {



    default FeatureType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        FeatureType featureType = new FeatureType();
        featureType.setIdFeatureType(id);
        return featureType;
    }
}
