package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.StandardCalendarDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity StandardCalendar and its DTO StandardCalendarDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class})
public interface StandardCalendarMapper extends EntityMapper<StandardCalendarDTO, StandardCalendar> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    StandardCalendarDTO toDto(StandardCalendar standardCalendar);

    @Mapping(source = "internalId", target = "internal")
    StandardCalendar toEntity(StandardCalendarDTO standardCalendarDTO);

    default StandardCalendar fromId(Integer id) {
        if (id == null) {
            return null;
        }
        StandardCalendar standardCalendar = new StandardCalendar();
        standardCalendar.setIdCalendar(id);
        return standardCalendar;
    }
}
