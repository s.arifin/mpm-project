package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.StockOpnameDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity StockOpname and its DTO StockOpnameDTO.
 */
@Mapper(componentModel = "spring", uses = {StandardCalendarMapper.class, InternalMapper.class, StockOpnameTypeMapper.class})
public interface StockOpnameMapper extends EntityMapper<StockOpnameDTO, StockOpname> {

    @Mapping(source = "calendar.idCalendar", target = "calendarId")
    @Mapping(source = "calendar.description", target = "calendarDescription")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "stockOpnameType.idStockOpnameType", target = "stockOpnameTypeId")
    @Mapping(source = "stockOpnameType.description", target = "stockOpnameTypeDescription")
    StockOpnameDTO toDto(StockOpname stockOpname);

    @Mapping(source = "calendarId", target = "calendar")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "stockOpnameTypeId", target = "stockOpnameType")
    @Mapping(target = "items", ignore = true)
    StockOpname toEntity(StockOpnameDTO stockOpnameDTO);

    default StockOpname fromId(UUID id) {
        if (id == null) {
            return null;
        }
        StockOpname stockOpname = new StockOpname();
        stockOpname.setIdStockOpname(id);
        return stockOpname;
    }
}
