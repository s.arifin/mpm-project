package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.OrderRequestItemDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity OrderRequestItem and its DTO OrderRequestItemDTO.
 */
@Mapper(componentModel = "spring", uses = {OrderItemMapper.class, RequestItemMapper.class})
public interface OrderRequestItemMapper extends EntityMapper<OrderRequestItemDTO, OrderRequestItem> {

    @Mapping(source = "orderItem.idOrderItem", target = "orderItemId")
    @Mapping(source = "requestItem.idRequestItem", target = "requestItemId")
    OrderRequestItemDTO toDto(OrderRequestItem orderRequestItem);

    @Mapping(source = "orderItemId", target = "orderItem")
    @Mapping(source = "requestItemId", target = "requestItem")
    OrderRequestItem toEntity(OrderRequestItemDTO orderRequestItemDTO);

    default OrderRequestItem fromId(UUID id) {
        if (id == null) {
            return null;
        }
        OrderRequestItem orderRequestItem = new OrderRequestItem();
        orderRequestItem.setIdOrderRequestItem(id);
        return orderRequestItem;
    }
}
