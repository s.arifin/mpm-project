package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ItemIssuanceDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ItemIssuance and its DTO ItemIssuanceDTO.
 */
@Mapper(componentModel = "spring", uses = {ShipmentItemMapper.class, InventoryItemMapper.class, PickingSlipMapper.class, })
public interface ItemIssuanceMapper extends EntityMapper <ItemIssuanceDTO, ItemIssuance> {

    @Mapping(source = "shipmentItem.idShipmentItem", target = "shipmentItemId")

    @Mapping(source = "inventoryItem.idInventoryItem", target = "inventoryItemId")

    @Mapping(source = "picking.idSlip", target = "pickingId")
    ItemIssuanceDTO toDto(ItemIssuance itemIssuance); 

    @Mapping(source = "shipmentItemId", target = "shipmentItem")

    @Mapping(source = "inventoryItemId", target = "inventoryItem")

    @Mapping(source = "pickingId", target = "picking")
    ItemIssuance toEntity(ItemIssuanceDTO itemIssuanceDTO); 

    default ItemIssuance fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ItemIssuance itemIssuance = new ItemIssuance();
        itemIssuance.setIdItemIssuance(id);
        return itemIssuance;
    }
}
