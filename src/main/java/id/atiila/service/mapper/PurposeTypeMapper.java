package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PurposeTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity PurposeType and its DTO PurposeTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PurposeTypeMapper extends EntityMapper <PurposeTypeDTO, PurposeType> {
    
    

    default PurposeType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        PurposeType purposeType = new PurposeType();
        purposeType.setIdPurposeType(id);
        return purposeType;
    }
}
