package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.StockOpnameItemDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity StockOpnameItem and its DTO StockOpnameItemDTO.
 */
@Mapper(componentModel = "spring", uses = {StockOpnameMapper.class, ProductMapper.class, ContainerMapper.class})
public interface StockOpnameItemMapper extends EntityMapper<StockOpnameItemDTO, StockOpnameItem> {

    @Mapping(source = "stockOpname.idStockOpname", target = "stockOpnameId")
    @Mapping(source = "product.idProduct", target = "productId")
    @Mapping(source = "product.name", target = "productName")
    @Mapping(source = "container.idContainer", target = "containerId")
    @Mapping(source = "container.containerCode", target = "containerCode")
    StockOpnameItemDTO toDto(StockOpnameItem stockOpnameItem);

    @Mapping(source = "stockOpnameId", target = "stockOpname")
    @Mapping(source = "productId", target = "product")
    @Mapping(source = "containerId", target = "container")
    StockOpnameItem toEntity(StockOpnameItemDTO stockOpnameItemDTO);

    default StockOpnameItem fromId(UUID id) {
        if (id == null) {
            return null;
        }
        StockOpnameItem stockOpnameItem = new StockOpnameItem();
        stockOpnameItem.setIdStockopnameItem(id);
        return stockOpnameItem;
    }
}
