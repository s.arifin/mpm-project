package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.VehiclePurchaseOrderDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity VehiclePurchaseOrder and its DTO VehiclePurchaseOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {VendorMapper.class, InternalMapper.class, BillToMapper.class})
public interface VehiclePurchaseOrderMapper extends EntityMapper<VehiclePurchaseOrderDTO, VehiclePurchaseOrder> {

    @Mapping(source = "vendor.idVendor", target = "vendorId")
    @Mapping(source = "vendor.idVendor", target = "vendorIdVendor")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.idInternal", target = "internalIdInternal")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    VehiclePurchaseOrderDTO toDto(VehiclePurchaseOrder vehiclePurchaseOrder);

    @Mapping(source = "vendorId", target = "vendor")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "billToId", target = "billTo")
    VehiclePurchaseOrder toEntity(VehiclePurchaseOrderDTO vehiclePurchaseOrderDTO);

    default VehiclePurchaseOrder fromId(UUID id) {
        if (id == null) {
            return null;
        }
        VehiclePurchaseOrder vehiclePurchaseOrder = new VehiclePurchaseOrder();
        vehiclePurchaseOrder.setIdOrder(id);
        return vehiclePurchaseOrder;
    }
}
