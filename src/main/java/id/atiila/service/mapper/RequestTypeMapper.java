package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RequestTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity RequestType and its DTO RequestTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RequestTypeMapper extends EntityMapper<RequestTypeDTO, RequestType> {



    default RequestType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        RequestType requestType = new RequestType();
        requestType.setIdRequestType(id);
        return requestType;
    }
}
