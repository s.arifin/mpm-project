package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.InventoryMovementDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity InventoryMovement and its DTO InventoryMovementDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InventoryMovementMapper extends EntityMapper <InventoryMovementDTO, InventoryMovement> {



    default InventoryMovement fromId(UUID id) {
        if (id == null) {
            return null;
        }
        InventoryMovement inventoryMovement = new InventoryMovement();
        inventoryMovement.setIdSlip(id);
        return inventoryMovement;
    }
}
