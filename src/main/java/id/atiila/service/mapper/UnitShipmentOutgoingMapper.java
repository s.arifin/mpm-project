package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.UnitShipmentOutgoingDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ShipmentOutgoing and its DTO UnitShipmentOutgoingDTO.
 */
@Mapper(componentModel = "spring", uses = {ShipmentTypeMapper.class, ShipToMapper.class, PostalAddressMapper.class, InternalMapper.class})
public interface UnitShipmentOutgoingMapper extends EntityMapper<UnitShipmentOutgoingDTO, ShipmentOutgoing> {

    @Mapping(source = "shipmentType.idShipmentType", target = "shipmentTypeId")
    @Mapping(source = "shipFrom.idShipTo", target = "shipFromId")
    @Mapping(source = "shipTo.idShipTo", target = "shipToId")
    @Mapping(source = "addressFrom.idContact", target = "addressFromId")
    @Mapping(source = "addressTo.idContact", target = "addressToId")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    UnitShipmentOutgoingDTO toDto(ShipmentOutgoing shipmentOutgoing);

    @Mapping(target = "details", ignore = true)
    @Mapping(source = "shipmentTypeId", target = "shipmentType")
    @Mapping(source = "shipFromId", target = "shipFrom")
    @Mapping(source = "shipToId", target = "shipTo")
    @Mapping(source = "addressFromId", target = "addressFrom")
    @Mapping(source = "addressToId", target = "addressTo")
    @Mapping(target = "statuses", ignore = true)
    @Mapping(source = "internalId", target = "internal")
    ShipmentOutgoing toEntity(UnitShipmentOutgoingDTO unitShipmentOutgoingDTO);

    default ShipmentOutgoing fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ShipmentOutgoing shipmentOutgoing = new ShipmentOutgoing();
        shipmentOutgoing.setIdShipment(id);
        return shipmentOutgoing;
    }
}
