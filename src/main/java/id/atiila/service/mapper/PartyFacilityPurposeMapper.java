package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PartyFacilityPurposeDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity PartyFacilityPurpose and its DTO PartyFacilityPurposeDTO.
 */
@Mapper(componentModel = "spring", uses = {OrganizationMapper.class, FacilityMapper.class, PurposeTypeMapper.class})
public interface PartyFacilityPurposeMapper extends EntityMapper<PartyFacilityPurposeDTO, PartyFacilityPurpose> {

    @Mapping(source = "organization.idParty", target = "organizationId")
    @Mapping(source = "organization.name", target = "organizationName")
    @Mapping(source = "facility.idFacility", target = "facilityId")
    @Mapping(source = "facility.description", target = "facilityDescription")
    @Mapping(source = "purposeType.idPurposeType", target = "purposeTypeId")
    @Mapping(source = "purposeType.description", target = "purposeTypeDescription")
    PartyFacilityPurposeDTO toDto(PartyFacilityPurpose partyFacilityPurpose);

    @Mapping(source = "organizationId", target = "organization")
    @Mapping(source = "facilityId", target = "facility")
    @Mapping(source = "purposeTypeId", target = "purposeType")
    PartyFacilityPurpose toEntity(PartyFacilityPurposeDTO partyFacilityPurposeDTO);

    default PartyFacilityPurpose fromId(UUID id) {
        if (id == null) {
            return null;
        }
        PartyFacilityPurpose partyFacilityPurpose = new PartyFacilityPurpose();
        partyFacilityPurpose.setIdPartyFacility(id);
        return partyFacilityPurpose;
    }
}
