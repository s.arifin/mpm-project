package id.atiila.service.mapper;

import id.atiila.base.BaseConstants;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.service.dto.CustomVDRDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

@Mapper(componentModel = "spring", uses = {VehicleDocumentRequirementMapper.class, SaleTypeMapper.class,
  PersonMapper.class, VehicleMapper.class, VehicleSalesOrderMapper.class})
public abstract class CustomVDRMapper {

    @Autowired
    private VehicleDocumentRequirementRepository vehicleDocumentRequirementRepository;

    @Autowired
    private UnitDeliverableRepository unitDeliverableRepository;

    @Autowired
    private RequirementOrderItemRepository requirementOrderItemRepository;

    @Autowired
    private OrderBillingItemRepository orderBillingItemRepository;

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    @Mapping(source = "vendor.idVendor", target = "idVendor")
    @Mapping(source = "orderItem.idOrderItem", target = "idOrderItem")
    @Mapping(source = "saleType.idSaleType", target = "saleType")
    public abstract CustomVDRDTO toDto(VehicleDocumentRequirement e);

    public CustomVDRDTO assignDetail(CustomVDRDTO dto) {
        RequirementOrderItem r = requirementOrderItemRepository.findRequirementOrderItemByOrder(dto.getIdOrderItem());
        SalesUnitRequirement sur = r != null ? r.getRequirement() : null;
        OrderItem oi = r != null ? r.getOrderItem() : null;

        List<OrderBillingItem> obis = orderBillingItemRepository.queryByOrderItem(dto.getIdOrderItem());
        VehicleSalesBilling  bi = obis.size() > 0 ? (VehicleSalesBilling) obis.get(0).getBillingItem().getBilling() : null;

        Page<UnitDeliverable> items = unitDeliverableRepository.queryByIdRequirement(dto.getIdRequirement(), new PageRequest(0, 10));
        if (items.hasContent()) {
            for (UnitDeliverable ud: items.getContent()) {
                if (ud.getIdDeliverableType().equals(BaseConstants.DELIVERABLE_TYPE_NOTICE)) {
                    dto.setDateBASTNotice(ud.getDateReceipt());
                } else if (ud.getIdDeliverableType().equals(BaseConstants.DELIVERABLE_TYPE_NOPOL)) {
                    dto.setDateBASTPlatNomor(ud.getDateReceipt());
                } else if (ud.getIdDeliverableType().equals(BaseConstants.DELIVERABLE_TYPE_STNK)) {
                    dto.setDateBASTSTNK(ud.getDateReceipt());
                    dto.setPengambilSTNK(ud.getName());
                } else if (ud.getIdDeliverableType().equals(BaseConstants.DELIVERABLE_TYPE_BPKB)) {
                    dto.setDateBASTBPKB(ud.getDateReceipt());
                };
            }
        }

        if (bi != null) {
            dto.setIvuNumber(bi.getBillingNumber());
        }

        return dto;
    }
}
