package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.WorkProductRequirementDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity WorkProductRequirement and its DTO WorkProductRequirementDTO.
 */
@Mapper(componentModel = "spring", uses = {WorkOrderBookingMapper.class, PaymentApplicationMapper.class })
public interface WorkProductRequirementMapper extends EntityMapper <WorkProductRequirementDTO, WorkProductRequirement> {

    WorkProductRequirementDTO toDto(WorkProductRequirement workProductRequirement);

    WorkProductRequirement toEntity(WorkProductRequirementDTO workProductRequirementDTO);

    default WorkProductRequirement fromId(UUID id) {
        if (id == null) {
            return null;
        }
        WorkProductRequirement workProductRequirement = new WorkProductRequirement();
        workProductRequirement.setIdRequirement(id);
        return workProductRequirement;
    }
}
