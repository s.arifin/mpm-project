package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.CityDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity City and its DTO CityDTO.
 */
@Mapper(componentModel = "spring", uses = {GeoBoundaryTypeMapper.class, GeoBoundaryMapper.class})
public interface CityMapper extends EntityMapper <CityDTO, City> {



    default City fromId(UUID id) {
        if (id == null) {
            return null;
        }
        City city = new City();
        city.setIdGeobou(id);
        return city;
    }
}
