package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.CommunicationEventDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity CommunicationEvent and its DTO CommunicationEventDTO.
 */
@Mapper(componentModel = "spring", uses = {EventTypeMapper.class, PurposeTypeMapper.class, })
public interface CommunicationEventMapper extends EntityMapper <CommunicationEventDTO, CommunicationEvent> {

    @Mapping(source = "eventType.idEventType", target = "eventTypeId")
    @Mapping(source = "eventType.description", target = "eventTypeDescription")

    @Mapping(source = "purpose.idPurposeType", target = "purposeId")
    @Mapping(source = "purpose.description", target = "purposeDescription")
    CommunicationEventDTO toDto(CommunicationEvent communicationEvent);

    @Mapping(source = "eventTypeId", target = "eventType")

    @Mapping(source = "purposeId", target = "purpose")
    CommunicationEvent toEntity(CommunicationEventDTO communicationEventDTO);

    default CommunicationEvent fromId(UUID id) {
        if (id == null) {
            return null;
        }
        CommunicationEvent communicationEvent = new CommunicationEvent();
        communicationEvent.setIdCommunicationEvent(id);
        return communicationEvent;
    }
}
