package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PriceTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity PriceType and its DTO PriceTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {RuleTypeMapper.class, })
public interface PriceTypeMapper extends EntityMapper <PriceTypeDTO, PriceType> {

    @Mapping(source = "ruleType.idRuleType", target = "ruleTypeId")
    PriceTypeDTO toDto(PriceType priceType); 

    @Mapping(source = "ruleTypeId", target = "ruleType")
    PriceType toEntity(PriceTypeDTO priceTypeDTO); 

    default PriceType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        PriceType priceType = new PriceType();
        priceType.setIdPriceType(id);
        return priceType;
    }
}
