package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RequestUnitInternalDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity RequestUnitInternal and its DTO RequestUnitInternalDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class})
public interface RequestUnitInternalMapper extends EntityMapper<RequestUnitInternalDTO, RequestUnitInternal> {

    @Mapping(source = "parent.idRequest", target = "parentId")
    @Mapping(source = "internalTo.idInternal", target = "internalToId")
    RequestUnitInternalDTO toDto(RequestUnitInternal requestUnitInternal);

    @Mapping(source = "parentId", target = "parent")
    @Mapping(source = "internalToId", target = "internalTo")
    RequestUnitInternal toEntity(RequestUnitInternalDTO requestUnitInternalDTO);

    default RequestUnitInternal fromId(UUID id) {
        if (id == null) {
            return null;
        }
        RequestUnitInternal requestUnitInternal = new RequestUnitInternal();
        requestUnitInternal.setIdRequest(id);
        return requestUnitInternal;
    }
}
