package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.GLAccountDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity GLAccount and its DTO GLAccountDTO.
 */
@Mapper(componentModel = "spring", uses = {GLAccountTypeMapper.class})
public interface GLAccountMapper extends EntityMapper<GLAccountDTO, GLAccount> {

    @Mapping(source = "accountType.idGLAccountType", target = "accountTypeId")
    @Mapping(source = "accountType.description", target = "accountTypeDescription")
    GLAccountDTO toDto(GLAccount gLAccount);

    @Mapping(source = "accountTypeId", target = "accountType")
    GLAccount toEntity(GLAccountDTO gLAccountDTO);

    default GLAccount fromId(UUID id) {
        if (id == null) {
            return null;
        }
        GLAccount gLAccount = new GLAccount();
        gLAccount.setIdGLAccount(id);
        return gLAccount;
    }
}
