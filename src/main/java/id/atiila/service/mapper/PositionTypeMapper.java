package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PositionTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity PositionType and its DTO PositionTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PositionTypeMapper extends EntityMapper <PositionTypeDTO, PositionType> {
    
    

    default PositionType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        PositionType positionType = new PositionType();
        positionType.setIdPositionType(id);
        return positionType;
    }
}
