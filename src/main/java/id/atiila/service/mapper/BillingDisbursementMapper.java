package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.BillingDisbursementDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity BillingDisbursement and its DTO BillingDisbursementDTO.
 */
@Mapper(componentModel = "spring", uses = {BillingMapper.class, BillingTypeMapper.class, InternalMapper.class, BillToMapper.class, VendorMapper.class})
public interface BillingDisbursementMapper extends EntityMapper<BillingDisbursementDTO, BillingDisbursement> {

    @Mapping(source = "billingType.idBillingType", target = "billingTypeId")
    @Mapping(source = "billingType.description", target = "billingTypeDescription")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    @Mapping(source = "billFrom.idBillTo", target = "billFromId")
    @Mapping(source = "vendor.idVendor", target = "vendorId")
    @Mapping(source = "vendor.organization.name", target = "vendorName")
    BillingDisbursementDTO toDto(BillingDisbursement billingDisbursement);

    @Mapping(target = "details", ignore = true)
    @Mapping(source = "billingTypeId", target = "billingType")
    @Mapping(target = "payments", ignore = true)
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "billToId", target = "billTo")
    @Mapping(source = "billFromId", target = "billFrom")
    @Mapping(source = "vendorId", target = "vendor")
    BillingDisbursement toEntity(BillingDisbursementDTO billingDisbursementDTO);

    default BillingDisbursement fromId(UUID id) {
        if (id == null) {
            return null;
        }
        BillingDisbursement billingDisbursement = new BillingDisbursement();
        billingDisbursement.setIdBilling(id);
        return billingDisbursement;
    }
}
