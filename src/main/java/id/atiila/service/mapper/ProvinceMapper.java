package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProvinceDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Province and its DTO ProvinceDTO.
 */
@Mapper(componentModel = "spring", uses = {GeoBoundaryTypeMapper.class, GeoBoundaryMapper.class})
public interface ProvinceMapper extends EntityMapper <ProvinceDTO, Province> {



    default Province fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Province province = new Province();
        province.setIdGeobou(id);
        return province;
    }
}
