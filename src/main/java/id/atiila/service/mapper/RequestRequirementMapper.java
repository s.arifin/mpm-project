package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RequestRequirementDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity RequestRequirement and its DTO RequestRequirementDTO.
 */
@Mapper(componentModel = "spring", uses = {RequestTypeMapper.class, RequirementMapper.class})
public interface RequestRequirementMapper extends EntityMapper<RequestRequirementDTO, RequestRequirement> {

    @Mapping(source = "requestType.idRequestType", target = "requestTypeId")
    @Mapping(source = "requestType.description", target = "requestTypeDescription")
    RequestRequirementDTO toDto(RequestRequirement requestRequirement);

    @Mapping(source = "requestTypeId", target = "requestType")
    RequestRequirement toEntity(RequestRequirementDTO requestRequirementDTO);

    default RequestRequirement fromId(UUID id) {
        if (id == null) {
            return null;
        }
        RequestRequirement requestRequirement = new RequestRequirement();
        requestRequirement.setIdRequest(id);
        return requestRequirement;
    }
}
