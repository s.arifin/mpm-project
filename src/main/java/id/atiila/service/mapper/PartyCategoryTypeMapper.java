package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PartyCategoryTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity PartyCategoryType and its DTO PartyCategoryTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PartyCategoryTypeMapper extends EntityMapper<PartyCategoryTypeDTO, PartyCategoryType> {

    @Mapping(source = "parent.idCategoryType", target = "parentId")
    @Mapping(source = "parent.description", target = "parentDescription")
    PartyCategoryTypeDTO toDto(PartyCategoryType partyCategoryType);

    @Mapping(source = "parentId", target = "parent")
    PartyCategoryType toEntity(PartyCategoryTypeDTO partyCategoryTypeDTO);

    default PartyCategoryType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        PartyCategoryType partyCategoryType = new PartyCategoryType();
        partyCategoryType.setIdCategoryType(id);
        return partyCategoryType;
    }
}
