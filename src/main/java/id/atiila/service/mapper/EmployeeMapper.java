package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.EmployeeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity Employee and its DTO EmployeeDTO.
 */
@Mapper(componentModel = "spring", uses = {PersonMapper.class})
public interface EmployeeMapper extends EntityMapper<EmployeeDTO, Employee> {

    @Mapping(source = "person.idParty", target = "personId")
    EmployeeDTO toDto(Employee employee);

    @Mapping(source = "personId", target = "person")
    Employee toEntity(EmployeeDTO employeeDTO);

    default Employee fromId(String id) {
        if (id == null) {
            return null;
        }
        Employee employee = new Employee();
        employee.setIdEmployee(id);
        return employee;
    }
}
