package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.BaseCalendarDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity BaseCalendar and its DTO BaseCalendarDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BaseCalendarMapper extends EntityMapper<BaseCalendarDTO, BaseCalendar> {

    

    

    default BaseCalendar fromId(Integer id) {
        if (id == null) {
            return null;
        }
        BaseCalendar baseCalendar = new BaseCalendar();
        baseCalendar.setIdCalendar(id);
        return baseCalendar;
    }
}
