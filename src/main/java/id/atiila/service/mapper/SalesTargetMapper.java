package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.SalesTargetDTO;

import org.mapstruct.*;

import java.util.UUID;

/**
 * Mapper for the entity SalesTarget and its DTO SalesTargetDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SalesTargetMapper extends EntityMapper <SalesTargetDTO, SalesTarget> {


    default SalesTarget fromId(UUID id) {
        if (id == null) {
            return null;
        }
        SalesTarget salesTarget = new SalesTarget();
        salesTarget.setIdslstrgt(id);
        return salesTarget;
    }
}
