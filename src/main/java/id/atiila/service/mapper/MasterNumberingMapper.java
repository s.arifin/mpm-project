package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.MasterNumberingDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity MasterNumbering and its DTO MasterNumberingDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MasterNumberingMapper extends EntityMapper <MasterNumberingDTO, MasterNumbering> {
    
    

    default MasterNumbering fromId(UUID id) {
        if (id == null) {
            return null;
        }
        MasterNumbering masterNumbering = new MasterNumbering();
        masterNumbering.setIdNumbering(id);
        return masterNumbering;
    }
}
