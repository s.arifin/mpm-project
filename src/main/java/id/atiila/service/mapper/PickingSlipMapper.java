package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PickingSlipDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity PickingSlip and its DTO PickingSlipDTO.
 */
@Mapper(componentModel = "spring", uses = {ShipToMapper.class, InventoryItemMapper.class, OrderItemMapper.class, })
public interface PickingSlipMapper extends EntityMapper <PickingSlipDTO, PickingSlip> {

    @Mapping(source = "shipTo.idShipTo", target = "shipToId")
    @Mapping(source = "shipTo.party.name", target = "shipToName")

    @Mapping(source = "inventoryItem.idInventoryItem", target = "inventoryItemId")

    @Mapping(source = "orderItem.idOrderItem", target = "orderItemId")
    PickingSlipDTO toDto(PickingSlip pickingSlip);

    @Mapping(source = "shipToId", target = "shipTo")

    @Mapping(source = "inventoryItemId", target = "inventoryItem")

    @Mapping(source = "orderItemId", target = "orderItem")
    PickingSlip toEntity(PickingSlipDTO pickingSlipDTO);

    default PickingSlip fromId(UUID id) {
        if (id == null) {
            return null;
        }
        PickingSlip pickingSlip = new PickingSlip();
        pickingSlip.setIdSlip(id);
        return pickingSlip;
    }
}
