package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.BillingReceiptDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity BillingReceipt and its DTO BillingReceiptDTO.
 */
@Mapper(componentModel = "spring", uses = {BillingTypeMapper.class, InternalMapper.class, BillToMapper.class})
public interface BillingReceiptMapper extends EntityMapper<BillingReceiptDTO, BillingReceipt> {

    @Mapping(source = "billingType.idBillingType", target = "billingTypeId")
    @Mapping(source = "billingType.description", target = "billingTypeDescription")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    @Mapping(source = "billTo.party.name", target = "billToName")
    @Mapping(source = "billFrom.idBillTo", target = "billFromId")
    @Mapping(source = "billFrom.party.name", target = "billFromName")
    BillingReceiptDTO toDto(BillingReceipt billingReceipt);

    @Mapping(target = "details", ignore = true)
    @Mapping(source = "billingTypeId", target = "billingType")
    @Mapping(target = "payments", ignore = true)
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "billToId", target = "billTo")
    @Mapping(source = "billFromId", target = "billFrom")
    BillingReceipt toEntity(BillingReceiptDTO billingReceiptDTO);

    default BillingReceipt fromId(UUID id) {
        if (id == null) {
            return null;
        }
        BillingReceipt billingReceipt = new BillingReceipt();
        billingReceipt.setIdBilling(id);
        return billingReceipt;
    }
}
