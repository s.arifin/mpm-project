package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.InternalDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Internal and its DTO InternalDTO.
 */
@Mapper(componentModel = "spring", uses = {OrganizationMapper.class})
public interface InternalMapper extends EntityMapper<InternalDTO, Internal> {


    InternalDTO toDto(Internal internal);

    Internal toEntity(InternalDTO internalDTO);

    default Internal fromId(String id) {
        if (id == null) {
            return null;
        }
        Internal internal = new Internal();
        internal.setIdInternal(id);
        return internal;
    }
}
