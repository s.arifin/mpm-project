package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.VehicleDocumentRequirementDTO;
import org.mapstruct.*;
import java.util.UUID;
import id.atiila.service.dto.SummaryBastDTO;

/**
 * Mapper for the entity VehicleDocumentRequirement and its DTO VehicleDocumentRequirementDTO.
 */
@Mapper(componentModel = "spring", uses = {SalesUnitRequirementMapper.class, PaymentApplicationMapper.class,
    FacilityMapper.class, SaleTypeMapper.class, PersonMapper.class, VehicleMapper.class, InternalMapper.class, OrderItemMapper.class, VendorMapper.class, OrganizationMapper.class })
public interface VehicleDocumentRequirementMapper extends EntityMapper <VehicleDocumentRequirementDTO, VehicleDocumentRequirement> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    @Mapping(source = "vendor.idVendor", target = "idVendor")
    @Mapping(source = "orderItem.idOrderItem", target = "idOrderItem")

    /*@Mapping(source = "billTo.idBillTo", target = "manageById")
    @Mapping(source = "billTo.party.name", target = "manageByName")*/
    VehicleDocumentRequirementDTO toDto(VehicleDocumentRequirement vehicleDocumentRequirement);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "idVendor", target = "vendor")
    @Mapping(source = "idOrderItem", target = "orderItem")
    /*@Mapping(source = "manageById", target = "manageBy")*/
    VehicleDocumentRequirement toEntity(VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO);

    @Mapping(source = "submissionNo", target = "submissionNo" )
    VehicleDocumentRequirementDTO toDto(SummaryBastDTO Summary);

    default VehicleDocumentRequirement fromId(UUID id) {
        if (id == null) {
            return null;
        }
        VehicleDocumentRequirement vehicleDocumentRequirement = new VehicleDocumentRequirement();
        vehicleDocumentRequirement.setIdRequirement(id);
        return vehicleDocumentRequirement;
    }
}
