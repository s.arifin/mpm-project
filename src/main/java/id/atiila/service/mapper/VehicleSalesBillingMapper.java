package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.VehicleSalesBillingDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity VehicleSalesBilling and its DTO VehicleSalesBillingDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, BillToMapper.class, CustomerMapper.class, PaymentApplicationMapper.class,  })
public interface VehicleSalesBillingMapper extends EntityMapper <VehicleSalesBillingDTO, VehicleSalesBilling> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    @Mapping(source = "customer.idCustomer", target = "customerId")
    @Mapping(source = "customer.party.name", target = "customerName")
    VehicleSalesBillingDTO toDto(VehicleSalesBilling vehicleSalesBilling);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "billToId", target = "billTo")
    @Mapping(source = "customerId", target = "customer")
    VehicleSalesBilling toEntity(VehicleSalesBillingDTO vehicleSalesBillingDTO);

    default VehicleSalesBilling fromId(UUID id) {
        if (id == null) {
            return null;
        }
        VehicleSalesBilling vehicleSalesBilling = new VehicleSalesBilling();
        vehicleSalesBilling.setIdBilling(id);
        return vehicleSalesBilling;
    }
}
