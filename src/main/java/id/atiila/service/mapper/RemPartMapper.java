package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RemPartDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity RemPart and its DTO RemPartDTO.
 */
@Mapper(componentModel = "spring", uses = {UomMapper.class, FeatureMapper.class, ProductCategoryMapper.class, })
public interface RemPartMapper extends EntityMapper <RemPartDTO, RemPart> {

    @Mapping(source = "uom.idUom", target = "uomId")
    @Mapping(source = "uom.description", target = "uomDescription")
    RemPartDTO toDto(RemPart remPart); 

    @Mapping(source = "uomId", target = "uom")
    RemPart toEntity(RemPartDTO remPartDTO); 

    default RemPart fromId(String id) {
        if (id == null) {
            return null;
        }
        RemPart remPart = new RemPart();
        remPart.setIdProduct(id);
        return remPart;
    }
}
