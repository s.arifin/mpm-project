package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.DocumentTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity DocumentType and its DTO DocumentTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DocumentTypeMapper extends EntityMapper <DocumentTypeDTO, DocumentType> {
    
    

    default DocumentType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        DocumentType documentType = new DocumentType();
        documentType.setIdDocumentType(id);
        return documentType;
    }
}
