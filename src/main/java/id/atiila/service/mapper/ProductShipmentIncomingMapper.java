package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProductShipmentIncomingDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ProductShipmentIncoming and its DTO ProductShipmentIncomingDTO.
 */
@Mapper(componentModel = "spring", uses = {ShipmentTypeMapper.class, ShipToMapper.class, PostalAddressMapper.class})
public interface ProductShipmentIncomingMapper extends EntityMapper<ProductShipmentIncomingDTO, ProductShipmentIncoming> {

    @Mapping(source = "shipmentType.idShipmentType", target = "shipmentTypeId")
    @Mapping(source = "shipFrom.idShipTo", target = "shipFromId")
    @Mapping(source = "shipTo.idShipTo", target = "shipToId")
    @Mapping(source = "addressFrom.idContact", target = "addressFromId")
    @Mapping(source = "addressTo.idContact", target = "addressToId")
    ProductShipmentIncomingDTO toDto(ProductShipmentIncoming productShipmentIncoming);

    @Mapping(target = "details", ignore = true)
    @Mapping(source = "shipmentTypeId", target = "shipmentType")
    @Mapping(source = "shipFromId", target = "shipFrom")
    @Mapping(source = "shipToId", target = "shipTo")
    @Mapping(source = "addressFromId", target = "addressFrom")
    @Mapping(source = "addressToId", target = "addressTo")
    ProductShipmentIncoming toEntity(ProductShipmentIncomingDTO productShipmentIncomingDTO);

    default ProductShipmentIncoming fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ProductShipmentIncoming productShipmentIncoming = new ProductShipmentIncoming();
        productShipmentIncoming.setIdShipment(id);
        return productShipmentIncoming;
    }
}
