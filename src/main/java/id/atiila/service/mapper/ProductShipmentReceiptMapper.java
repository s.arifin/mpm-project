package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProductShipmentReceiptDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ProductShipmentReceipt and its DTO ProductShipmentReceiptDTO.
 */
@Mapper(componentModel = "spring", uses = {ShipmentPackageMapper.class, ShipmentItemMapper.class, OrderItemMapper.class})
public interface ProductShipmentReceiptMapper extends EntityMapper<ProductShipmentReceiptDTO, ProductShipmentReceipt> {

    @Mapping(source = "shipmentPackage.idPackage", target = "shipmentPackageId")
    @Mapping(source = "shipmentItem.idShipmentItem", target = "shipmentItemId")
    @Mapping(source = "orderItem.idOrderItem", target = "orderItemId")
    ProductShipmentReceiptDTO toDto(ProductShipmentReceipt productShipmentReceipt);

    @Mapping(source = "shipmentPackageId", target = "shipmentPackage")
    @Mapping(source = "shipmentItemId", target = "shipmentItem")
    @Mapping(source = "orderItemId", target = "orderItem")
    @Mapping(target = "inventoryItems", ignore = true)
    ProductShipmentReceipt toEntity(ProductShipmentReceiptDTO productShipmentReceiptDTO);

    default ProductShipmentReceipt fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ProductShipmentReceipt productShipmentReceipt = new ProductShipmentReceipt();
        productShipmentReceipt.setIdReceipt(id);
        return productShipmentReceipt;
    }
}
