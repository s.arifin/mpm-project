package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.OrderItemDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity OrderItem and its DTO OrderItemDTO.
 */
@Mapper(componentModel = "spring", uses = {OrdersMapper.class})
public interface OrderItemMapper extends EntityMapper<OrderItemDTO, OrderItem> {

    @Mapping(source = "orders.idOrder", target = "ordersId")
    OrderItemDTO toDto(OrderItem orderItem);

    @Mapping(source = "ordersId", target = "orders")
    @Mapping(target = "shipmentItems", ignore = true)
    @Mapping(target = "billingItems", ignore = true)
    OrderItem toEntity(OrderItemDTO orderItemDTO);

    default OrderItem fromId(UUID id) {
        if (id == null) {
            return null;
        }
        OrderItem orderItem = new OrderItem();
        orderItem.setIdOrderItem(id);
        return orderItem;
    }
}
