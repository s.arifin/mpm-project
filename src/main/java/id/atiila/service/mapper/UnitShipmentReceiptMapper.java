package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.UnitShipmentReceiptDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity UnitShipmentReceipt and its DTO UnitShipmentReceiptDTO.
 */
@Mapper(componentModel = "spring", uses = {ShipmentPackageMapper.class, ShipmentItemMapper.class,
    OrderItemMapper.class, FeatureMapper.class})
public interface UnitShipmentReceiptMapper extends EntityMapper<UnitShipmentReceiptDTO, UnitShipmentReceipt> {

    @Mapping(source = "shipmentPackage.idPackage", target = "shipmentPackageId")
    @Mapping(source = "shipmentItem.idShipmentItem", target = "shipmentItemId")
    @Mapping(source = "orderItem.idOrderItem", target = "orderItemId")
    @Mapping(source = "feature.idFeature", target = "featureId")
    @Mapping(source = "feature.description", target = "featureDescription")
    @Mapping(source = "feature.refKey", target = "featureRefKey")
    UnitShipmentReceiptDTO toDto(UnitShipmentReceipt unitShipmentReceipt);

    @Mapping(source = "shipmentPackageId", target = "shipmentPackage")
    @Mapping(source = "shipmentItemId", target = "shipmentItem")
    @Mapping(source = "orderItemId", target = "orderItem")
    @Mapping(target = "inventoryItems", ignore = true)
    UnitShipmentReceipt toEntity(UnitShipmentReceiptDTO unitShipmentReceiptDTO);

    default UnitShipmentReceipt fromId(UUID id) {
        if (id == null) {
            return null;
        }
        UnitShipmentReceipt unitShipmentReceipt = new UnitShipmentReceipt();
        unitShipmentReceipt.setIdReceipt(id);
        return unitShipmentReceipt;
    }
}
