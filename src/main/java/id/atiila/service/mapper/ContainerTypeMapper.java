package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ContainerTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity ContainerType and its DTO ContainerTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ContainerTypeMapper extends EntityMapper<ContainerTypeDTO, ContainerType> {



    default ContainerType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        ContainerType containerType = new ContainerType();
        containerType.setIdContainerType(id);
        return containerType;
    }
}
