package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PaymentTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity PaymentType and its DTO PaymentTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PaymentTypeMapper extends EntityMapper<PaymentTypeDTO, PaymentType> {



    default PaymentType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        PaymentType paymentType = new PaymentType();
        paymentType.setIdPaymentType(id);
        return paymentType;
    }
}
