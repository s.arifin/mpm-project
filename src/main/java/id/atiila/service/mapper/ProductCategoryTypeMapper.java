package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProductCategoryTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity ProductCategoryType and its DTO ProductCategoryTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProductCategoryTypeMapper extends EntityMapper<ProductCategoryTypeDTO, ProductCategoryType> {



    default ProductCategoryType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        ProductCategoryType productCategoryType = new ProductCategoryType();
        productCategoryType.setIdCategoryType(id);
        return productCategoryType;
    }
}
