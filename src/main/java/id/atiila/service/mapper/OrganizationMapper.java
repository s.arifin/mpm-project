package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.repository.OrganizationRepository;
import id.atiila.service.dto.OrganizationDTO;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

/**
 * Mapper for the entity Organization and its DTO OrganizationDTO.
 */
@Mapper(componentModel = "spring", uses = {ContactMechanismMapper.class, PartyCategoryMapper.class,
        FacilityMapper.class,PostalAddressMapper.class })
public abstract class OrganizationMapper {

    @Autowired
    private OrganizationRepository repo;

    public abstract OrganizationDTO toDto(Organization e);

    public abstract Organization toEntity(OrganizationDTO e);

    public Organization fromId(UUID id) {
        if (id == null) {
            return null;
        }
        return repo.findOne(id);
    }
}
