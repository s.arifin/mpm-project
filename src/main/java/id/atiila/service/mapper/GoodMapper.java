package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.GoodDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity Good and its DTO GoodDTO.
 */
@Mapper(componentModel = "spring", uses = {UomMapper.class, FeatureMapper.class, ProductCategoryMapper.class, })
public interface GoodMapper extends EntityMapper <GoodDTO, Good> {

    @Mapping(source = "uom.idUom", target = "uomId")
    @Mapping(source = "uom.description", target = "uomDescription")
    GoodDTO toDto(Good good); 

    @Mapping(source = "uomId", target = "uom")
    Good toEntity(GoodDTO goodDTO); 

    default Good fromId(String id) {
        if (id == null) {
            return null;
        }
        Good good = new Good();
        good.setIdProduct(id);
        return good;
    }
}
