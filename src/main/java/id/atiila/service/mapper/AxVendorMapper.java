package id.atiila.service.mapper;

import id.atiila.domain.Vendor;
import id.atiila.service.ax.dto.AxVendorDTO;
import id.atiila.service.dto.VendorDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity Vendor and its DTO VendorDTO.
 */
@Mapper(componentModel = "spring", uses = {RoleTypeMapper.class, OrganizationMapper.class, VendorTypeMapper.class})
public interface AxVendorMapper extends EntityMapper <AxVendorDTO, Vendor> {

    AxVendorDTO toDto(Vendor vendor);

    Vendor toEntity(AxVendorDTO vendorDTO);

    default Vendor fromId(String id) {
        if (id == null) {
            return null;
        }
        Vendor vendor = new Vendor();
        vendor.setIdVendor(id);
        return vendor;
    }
}
