package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.UomConversionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity UomConversion and its DTO UomConversionDTO.
 */
@Mapper(componentModel = "spring", uses = {UomMapper.class})
public interface UomConversionMapper extends EntityMapper<UomConversionDTO, UomConversion> {

    @Mapping(source = "uomTo.idUom", target = "uomToId")
    @Mapping(source = "uomTo.description", target = "uomToDescription")
    @Mapping(source = "uomFrom.idUom", target = "uomFromId")
    @Mapping(source = "uomFrom.description", target = "uomFromDescription")
    UomConversionDTO toDto(UomConversion uomConversion);

    @Mapping(source = "uomToId", target = "uomTo")
    @Mapping(source = "uomFromId", target = "uomFrom")
    UomConversion toEntity(UomConversionDTO uomConversionDTO);

    default UomConversion fromId(Integer id) {
        if (id == null) {
            return null;
        }
        UomConversion uomConversion = new UomConversion();
        uomConversion.setIdUomConversion(id);
        return uomConversion;
    }
}
