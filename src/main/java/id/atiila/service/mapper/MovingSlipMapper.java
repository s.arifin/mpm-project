package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.MovingSlipDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity MovingSlip and its DTO MovingSlipDTO.
 */
@Mapper(componentModel = "spring", uses = {
    InventoryItemMapper.class,
    ContainerMapper.class,
    FacilityMapper.class,
    ProductMapper.class,
    InventoryItemStatus.class
})
public interface MovingSlipMapper extends EntityMapper <MovingSlipDTO, MovingSlip> {

    @Mapping(source = "inventoryItemTo.idInventoryItem", target = "inventoryItemToId")

    @Mapping(source = "containerFrom.idContainer", target = "containerFromId")
    @Mapping(source = "containerFrom.containerCode", target = "containerFromContainerCode")

    @Mapping(source = "containerTo.idContainer", target = "containerToId")
    @Mapping(source = "containerTo.containerCode", target = "containerToContainerCode")

    @Mapping(source = "facilityFrom.idFacility", target = "facilityFromId")
    @Mapping(source = "facilityFrom.description", target = "facilityFromDescription")

    @Mapping(source = "facilityTo.idFacility", target = "facilityToId")
    @Mapping(source = "facilityTo.description", target = "facilityToDescription")

    @Mapping(source = "product.idProduct", target = "productId")
    @Mapping(source = "product.name", target = "productName")

    @Mapping(source = "inventoryItem.idInventoryItem", target = "inventoryItemId")
    MovingSlipDTO toDto(MovingSlip movingSlip);

    @Mapping(source = "inventoryItemToId", target = "inventoryItemTo")

    @Mapping(source = "containerFromId", target = "containerFrom")

    @Mapping(source = "containerToId", target = "containerTo")

    @Mapping(source = "facilityFromId", target = "facilityFrom")

    @Mapping(source = "facilityToId", target = "facilityTo")

    @Mapping(source = "productId", target = "product")

    @Mapping(source = "inventoryItemId", target = "inventoryItem")
    MovingSlip toEntity(MovingSlipDTO movingSlipDTO);

    default MovingSlip fromId(UUID id) {
        if (id == null) {
            return null;
        }
        MovingSlip movingSlip = new MovingSlip();
        movingSlip.setIdSlip(id);
        return movingSlip;
    }
}
