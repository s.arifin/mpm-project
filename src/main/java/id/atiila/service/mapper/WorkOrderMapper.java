package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.WorkOrderDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity WorkOrder and its DTO WorkOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {CustomerMapper.class, MechanicMapper.class, VehicleIdentificationMapper.class, VehicleMapper.class,
       PaymentApplicationMapper.class, WorkServiceRequirementMapper.class, WorkProductRequirementMapper.class, RequirementMapper.class   })
public interface WorkOrderMapper extends EntityMapper <WorkOrderDTO, WorkOrder> {

    @Mapping(source = "customer.idCustomer", target = "customerId")
    @Mapping(source = "mechanic.idPartyRole", target = "mechanicId")
    @Mapping(source = "vehicle.idVehicleIdentification", target = "vehicleId")
    WorkOrderDTO toDto(WorkOrder workOrder);

    @Mapping(source = "customerId", target = "customer")
    @Mapping(source = "mechanicId", target = "mechanic")
    @Mapping(source = "vehicleId", target = "vehicle")
    WorkOrder toEntity(WorkOrderDTO workOrderDTO);

    default WorkOrder fromId(UUID id) {
        if (id == null) {
            return null;
        }
        WorkOrder workOrder = new WorkOrder();
        workOrder.setIdRequirement(id);
        return workOrder;
    }
}
