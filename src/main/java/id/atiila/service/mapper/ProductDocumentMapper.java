package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProductDocumentDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ProductDocument and its DTO ProductDocumentDTO.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class, DocumentTypeMapper.class, })
public interface ProductDocumentMapper extends EntityMapper <ProductDocumentDTO, ProductDocument> {

    @Mapping(source = "product.idProduct", target = "productId")

    @Mapping(source = "documentType.idDocumentType", target = "documentTypeId")
    @Mapping(source = "documentType.description", target = "documentTypeDescription")
    ProductDocumentDTO toDto(ProductDocument productDocument);

    @Mapping(source = "productId", target = "product")
    @Mapping(source = "documentTypeId", target = "documentType")
    ProductDocument toEntity(ProductDocumentDTO productDocumentDTO);

    default ProductDocument fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ProductDocument productDocument = new ProductDocument();
        productDocument.setIdDocument(id);
        return productDocument;
    }
}
