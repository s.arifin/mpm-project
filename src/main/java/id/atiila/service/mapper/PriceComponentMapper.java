package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PriceComponentDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity PriceComponent and its DTO PriceComponentDTO.
 */
@Mapper(componentModel = "spring", uses = {
    ProductMapper.class,
    PartyMapper.class,
    WorkEffortTypeMapper.class,
    PriceTypeMapper.class
})
public interface PriceComponentMapper extends EntityMapper <PriceComponentDTO, PriceComponent> {

    @Mapping(source = "product.idProduct", target = "productId")
    @Mapping(source = "product.name", target = "productName")
    @Mapping(source = "seller.idParty", target = "sellerId")
    @Mapping(source = "seller.name", target = "sellerName")
    @Mapping(source = "priceType.description", target="priceTypeDescription")
    @Mapping(source = "priceType.idPriceType", target = "idPriceType")
    PriceComponentDTO toDto(PriceComponent priceComponent);

    @Mapping(source = "productId", target = "product")
    @Mapping(source = "sellerId", target = "seller")
    @Mapping(source = "idPriceType", target="priceType")
    PriceComponent toEntity(PriceComponentDTO priceComponentDTO);

    default PriceComponent fromId(UUID id) {
        if (id == null) {
            return null;
        }
        PriceComponent priceComponent = new PriceComponent();
        priceComponent.setIdPriceComponent(id);
        return priceComponent;
    }
}
