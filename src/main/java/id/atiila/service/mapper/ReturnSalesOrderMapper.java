package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ReturnSalesOrderDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ReturnSalesOrder and its DTO ReturnSalesOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, CustomerMapper.class, BillToMapper.class})
public interface ReturnSalesOrderMapper extends EntityMapper<ReturnSalesOrderDTO, ReturnSalesOrder> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "customer.idCustomer", target = "customerId")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    ReturnSalesOrderDTO toDto(ReturnSalesOrder returnSalesOrder);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "customerId", target = "customer")
    @Mapping(source = "billToId", target = "billTo")
    ReturnSalesOrder toEntity(ReturnSalesOrderDTO returnSalesOrderDTO);

    default ReturnSalesOrder fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ReturnSalesOrder returnSalesOrder = new ReturnSalesOrder();
        returnSalesOrder.setIdOrder(id);
        return returnSalesOrder;
    }
}
