package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.EventTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity EventType and its DTO EventTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EventTypeMapper extends EntityMapper <EventTypeDTO, EventType> {
    
    

    default EventType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        EventType eventType = new EventType();
        eventType.setIdEventType(id);
        return eventType;
    }
}
