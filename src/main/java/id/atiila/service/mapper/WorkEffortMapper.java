package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.WorkEffortDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity WorkEffort and its DTO WorkEffortDTO.
 */
@Mapper(componentModel = "spring", uses = {PaymentApplicationMapper.class, FacilityMapper.class, WorkEffortTypeMapper.class, })
public interface WorkEffortMapper extends EntityMapper <WorkEffortDTO, WorkEffort> {

    @Mapping(source = "facility.idFacility", target = "facilityId")
    @Mapping(source = "facility.description", target = "facilityDescription")

    @Mapping(source = "workType.idWeType", target = "workTypeId")
    @Mapping(source = "workType.description", target = "workTypeDescription")
    WorkEffortDTO toDto(WorkEffort workEffort); 

    @Mapping(source = "facilityId", target = "facility")

    @Mapping(source = "workTypeId", target = "workType")
    WorkEffort toEntity(WorkEffortDTO workEffortDTO); 

    default WorkEffort fromId(UUID id) {
        if (id == null) {
            return null;
        }
        WorkEffort workEffort = new WorkEffort();
        workEffort.setIdWe(id);
        return workEffort;
    }
}
