package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.DimensionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity Dimension and its DTO DimensionDTO.
 */
@Mapper(componentModel = "spring", uses = {DimensionTypeMapper.class})
public interface DimensionMapper extends EntityMapper<DimensionDTO, Dimension> {

    @Mapping(source = "dimensionType.idDimensionType", target = "dimensionTypeId")
    @Mapping(source = "dimensionType.description", target = "dimensionTypeDescription")
    DimensionDTO toDto(Dimension dimension);

    @Mapping(source = "dimensionTypeId", target = "dimensionType")
    Dimension toEntity(DimensionDTO dimensionDTO);

    default Dimension fromId(Integer id) {
        if (id == null) {
            return null;
        }
        Dimension dimension = new Dimension();
        dimension.setIdDimension(id);
        return dimension;
    }
}
