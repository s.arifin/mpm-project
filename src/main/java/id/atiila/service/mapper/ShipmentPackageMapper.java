package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ShipmentPackageDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ShipmentPackage and its DTO ShipmentPackageDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, })
public interface ShipmentPackageMapper extends EntityMapper <ShipmentPackageDTO, ShipmentPackage> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    ShipmentPackageDTO toDto(ShipmentPackage shipmentPackage);

    @Mapping(source = "internalId", target = "internal")
    ShipmentPackage toEntity(ShipmentPackageDTO shipmentPackageDTO);

    default ShipmentPackage fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ShipmentPackage shipmentPackage = new ShipmentPackage();
        shipmentPackage.setIdPackage(id);
        return shipmentPackage;
    }
}
