package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.CommunicationEventDeliveryDTO;

import org.mapstruct.*;

import java.util.UUID;

/**
 * Mapper for the entity CommunicationEventDelivery and its DTO CommunicationEventDeliveryDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CommunicationEventDeliveryMapper extends EntityMapper <CommunicationEventDeliveryDTO, CommunicationEventDelivery> {


    default CommunicationEventDelivery fromId(UUID id) {
        if (id == null) {
            return null;
        }
        CommunicationEventDelivery communicationEventDelivery = new CommunicationEventDelivery();
        communicationEventDelivery.setIdCommunicationEvent(id);
        return communicationEventDelivery;
    }
}
