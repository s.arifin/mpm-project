package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.UomTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity UomType and its DTO UomTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UomTypeMapper extends EntityMapper<UomTypeDTO, UomType> {



    default UomType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        UomType uomType = new UomType();
        uomType.setIdUomType(id);
        return uomType;
    }
}
