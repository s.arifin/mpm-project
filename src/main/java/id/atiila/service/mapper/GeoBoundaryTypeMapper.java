package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.GeoBoundaryTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity GeoBoundaryType and its DTO GeoBoundaryTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GeoBoundaryTypeMapper extends EntityMapper <GeoBoundaryTypeDTO, GeoBoundaryType> {
    
    

    default GeoBoundaryType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        GeoBoundaryType geoBoundaryType = new GeoBoundaryType();
        geoBoundaryType.setIdGeobouType(id);
        return geoBoundaryType;
    }
}
