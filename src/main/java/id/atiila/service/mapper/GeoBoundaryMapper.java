package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.GeoBoundaryDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity GeoBoundary and its DTO GeoBoundaryDTO.
 */
@Mapper(componentModel = "spring", uses = {GeoBoundaryTypeMapper.class, })
public interface GeoBoundaryMapper extends EntityMapper <GeoBoundaryDTO, GeoBoundary> {

    @Mapping(source = "idGeobouType", target = "geoBoundaryTypeId")
    GeoBoundaryDTO toDto(GeoBoundary geoBoundary);

    GeoBoundary toEntity(GeoBoundaryDTO geoBoundaryDTO);

    default GeoBoundary fromId(UUID id) {
        if (id == null) {
            return null;
        }
        GeoBoundary geoBoundary = new GeoBoundary();
        geoBoundary.setIdGeobou(id);
        return geoBoundary;
    }
}
