package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PersonalCustomerDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity PersonalCustomer and its DTO PersonalCustomerDTO.
 */
@Mapper(componentModel = "spring", uses = {PersonMapper.class})
public interface PersonalCustomerMapper extends EntityMapper<PersonalCustomerDTO, PersonalCustomer> {

    @Mapping(source = "party.idParty", target = "partyId")
    PersonalCustomerDTO toDto(PersonalCustomer personalCustomer);

    @Mapping(source = "partyId", target = "party")
    PersonalCustomer toEntity(PersonalCustomerDTO personalCustomerDTO);

    default PersonalCustomer fromId(String id) {
        if (id == null) {
            return null;
        }
        PersonalCustomer personalCustomer = new PersonalCustomer();
        personalCustomer.setIdCustomer(id);
        return personalCustomer;
    }
}
