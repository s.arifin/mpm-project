package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.FacilityTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity FacilityType and its DTO FacilityTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FacilityTypeMapper extends EntityMapper<FacilityTypeDTO, FacilityType> {



    default FacilityType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        FacilityType facilityType = new FacilityType();
        facilityType.setIdFacilityType(id);
        return facilityType;
    }
}
