package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.BillingItemDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity BillingItem and its DTO BillingItemDTO.
 */
@Mapper(componentModel = "spring", uses = {BillingMapper.class, InventoryItemMapper.class, FeatureMapper.class,
    BillTo.class, Party.class, Internal.class})
public interface BillingItemMapper extends EntityMapper<BillingItemDTO, BillingItem> {

    @Mapping(source = "billing.idBilling", target = "billingId")
    @Mapping(source = "inventoryItem.idInventoryItem", target = "inventoryItemId")
    @Mapping(source = "feature.idFeature", target = "featureId")
    @Mapping(source = "feature.description", target = "featureDescription")
    @Mapping(source = "feature.refKey", target = "featureRefKey")
    @Mapping(source = "billing.billingNumber", target = "billingNumber")
    @Mapping(source = "billing.dateCreate", target = "dateCreate")
    @Mapping(source = "billing.billTo.party.name", target = "name")
    @Mapping(source = "billing.internal.idInternal", target = "idInternal")
    BillingItemDTO toDto(BillingItem billingItem);

    @Mapping(source = "billingId", target = "billing")
    @Mapping(source = "inventoryItemId", target = "inventoryItem")
    @Mapping(source = "featureId", target = "feature")
    BillingItem toEntity(BillingItemDTO billingItemDTO);

    default BillingItem fromId(UUID id) {
        if (id == null) {
            return null;
        }
        BillingItem billingItem = new BillingItem();
        billingItem.setIdBillingItem(id);
        return billingItem;
    }
}
