package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.SalesBrokerDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity SalesBroker and its DTO SalesBrokerDTO.
 */
@Mapper(componentModel = "spring", uses = {PersonMapper.class, BrokerTypeMapper.class})
public interface SalesBrokerMapper extends EntityMapper <SalesBrokerDTO, SalesBroker> {

    @Mapping(source = "brokerType.idBrokerType", target = "brokerTypeId")
    SalesBrokerDTO toDto(SalesBroker salesBroker);

    @Mapping(source = "brokerTypeId", target = "brokerType")
    SalesBroker toEntity(SalesBrokerDTO salesBrokerDTO);

    default SalesBroker fromId(UUID id) {
        if (id == null) {
            return null;
        }
        SalesBroker salesBroker = new SalesBroker();
        salesBroker.setIdPartyRole(id);
        return salesBroker;
    }
}
