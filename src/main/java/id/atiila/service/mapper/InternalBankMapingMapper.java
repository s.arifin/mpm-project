package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.InternalBankMapingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity InternalBankMaping and its DTO InternalBankMapingDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InternalBankMapingMapper extends EntityMapper <InternalBankMapingDTO, InternalBankMaping> {

    InternalBankMapingDTO toDto(InternalBankMaping internalBankMaping);

    InternalBankMaping toEntity(InternalBankMapingDTO internalBankMapingDTO);

    default InternalBankMaping fromId(String id) {
        if (id == null) {
            return null;
        }
        InternalBankMaping internalBankMaping = new InternalBankMaping();
        internalBankMaping.setIdinternal(id);
        return internalBankMaping;
    }
}
