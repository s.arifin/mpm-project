package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.VendorProductDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity VendorProduct and its DTO VendorProductDTO.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class, InternalMapper.class, VendorMapper.class, })
public interface VendorProductMapper extends EntityMapper <VendorProductDTO, VendorProduct> {

    @Mapping(source = "product.idProduct", target = "productId")
    @Mapping(source = "product.name", target = "productName")

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.idInternal", target = "internalIdInternal")

    @Mapping(source = "vendor.idVendor", target = "vendorId")
    @Mapping(source = "vendor.organization.name", target = "vendorName")
    VendorProductDTO toDto(VendorProduct vendorProduct);

    @Mapping(source = "productId", target = "product")

    @Mapping(source = "internalId", target = "internal")

    @Mapping(source = "vendorId", target = "vendor")
    VendorProduct toEntity(VendorProductDTO vendorProductDTO);

    default VendorProduct fromId(UUID id) {
        if (id == null) {
            return null;
        }
        VendorProduct vendorProduct = new VendorProduct();
        vendorProduct.setIdVendorProduct(id);
        return vendorProduct;
    }
}
