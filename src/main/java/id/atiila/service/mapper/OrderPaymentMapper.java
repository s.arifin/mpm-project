package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.OrderPaymentDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity OrderPayment and its DTO OrderPaymentDTO.
 */
@Mapper(componentModel = "spring", uses = {OrdersMapper.class, PaymentMapper.class})
public interface OrderPaymentMapper extends EntityMapper<OrderPaymentDTO, OrderPayment> {

    @Mapping(source = "orders.idOrder", target = "ordersId")
    @Mapping(source = "payment.idPayment", target = "paymentId")
    OrderPaymentDTO toDto(OrderPayment orderPayment);

    @Mapping(source = "ordersId", target = "orders")
    @Mapping(source = "paymentId", target = "payment")
    OrderPayment toEntity(OrderPaymentDTO orderPaymentDTO);

    default OrderPayment fromId(UUID id) {
        if (id == null) {
            return null;
        }
        OrderPayment orderPayment = new OrderPayment();
        orderPayment.setIdOrderPayment(id);
        return orderPayment;
    }
}
