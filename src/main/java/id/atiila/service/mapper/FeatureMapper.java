package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.FeatureDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity Feature and its DTO FeatureDTO.
 */
@Mapper(componentModel = "spring", uses = {FeatureTypeMapper.class})
public interface FeatureMapper extends EntityMapper<FeatureDTO, Feature> {

    @Mapping(source = "featureType.idFeatureType", target = "featureTypeId")
    @Mapping(source = "featureType.description", target = "featureTypeDescription")
    FeatureDTO toDto(Feature feature);

    @Mapping(source = "featureTypeId", target = "featureType")
    Feature toEntity(FeatureDTO featureDTO);

    default Feature fromId(Integer id) {
        if (id == null) {
            return null;
        }
        Feature feature = new Feature();
        feature.setIdFeature(id);
        return feature;
    }
}
