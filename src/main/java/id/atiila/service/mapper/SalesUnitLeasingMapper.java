package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.SalesUnitLeasingDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity SalesUnitLeasing and its DTO SalesUnitLeasingDTO.
 */
@Mapper(componentModel = "spring", uses = {SalesUnitRequirementMapper.class, LeasingCompanyMapper.class, })
public interface SalesUnitLeasingMapper extends EntityMapper <SalesUnitLeasingDTO, SalesUnitLeasing> {

    @Mapping(source = "owner.idRequirement", target = "ownerId")

    @Mapping(source = "leasingCompany.idPartyRole", target = "leasingCompanyId")
    SalesUnitLeasingDTO toDto(SalesUnitLeasing salesUnitLeasing); 

    @Mapping(source = "ownerId", target = "owner")

    @Mapping(source = "leasingCompanyId", target = "leasingCompany")
    SalesUnitLeasing toEntity(SalesUnitLeasingDTO salesUnitLeasingDTO); 

    default SalesUnitLeasing fromId(UUID id) {
        if (id == null) {
            return null;
        }
        SalesUnitLeasing salesUnitLeasing = new SalesUnitLeasing();
        salesUnitLeasing.setIdSalesLeasing(id);
        return salesUnitLeasing;
    }
}
