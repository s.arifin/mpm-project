package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.BillingItemTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity BillingItemType and its DTO BillingItemTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BillingItemTypeMapper extends EntityMapper<BillingItemTypeDTO, BillingItemType> {



    default BillingItemType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        BillingItemType billingItemType = new BillingItemType();
        billingItemType.setIdItemType(id);
        return billingItemType;
    }
}
