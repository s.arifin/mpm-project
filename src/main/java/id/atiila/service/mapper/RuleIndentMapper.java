package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RuleIndentDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity RuleIndent and its DTO RuleIndentDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RuleIndentMapper extends EntityMapper<RuleIndentDTO, RuleIndent> {



    default RuleIndent fromId(Integer id) {
        if (id == null) {
            return null;
        }
        RuleIndent ruleIndent = new RuleIndent();
        ruleIndent.setIdRule(id);
        return ruleIndent;
    }
}
