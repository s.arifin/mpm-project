package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PartyDocumentDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity PartyDocument and its DTO PartyDocumentDTO.
 */
@Mapper(componentModel = "spring", uses = {DocumentTypeMapper.class, PartyMapper.class, })
public interface PartyDocumentMapper extends EntityMapper <PartyDocumentDTO, PartyDocument> {

    @Mapping(source = "documentType.idDocumentType", target = "documentTypeId")
    @Mapping(source = "documentType.description", target = "documentTypeDescription")
    @Mapping(source = "party.idParty", target = "partyId")
    PartyDocumentDTO toDto(PartyDocument partyDocument);

    @Mapping(source = "documentTypeId", target = "documentType")
    @Mapping(source = "partyId", target = "party")
    PartyDocument toEntity(PartyDocumentDTO partyDocumentDTO);

    default PartyDocument fromId(UUID id) {
        if (id == null) {
            return null;
        }
        PartyDocument partyDocument = new PartyDocument();
        partyDocument.setIdDocument(id);
        return partyDocument;
    }
}
