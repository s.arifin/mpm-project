package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ReceiptDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Receipt and its DTO ReceiptDTO.
 */
@Mapper(componentModel = "spring", uses = {
    PaymentTypeMapper.class,
    PaymentMethodMapper.class,
    InternalMapper.class,
    BillToMapper.class})
public interface ReceiptMapper extends EntityMapper<ReceiptDTO, Receipt> {

    @Mapping(source = "paymentType.idPaymentType", target = "paymentTypeId")
    @Mapping(source = "paymentType.description", target = "paymentTypeDescription")
    @Mapping(source = "method.idPaymentMethod", target = "methodId")
    @Mapping(source = "method.description", target = "methodDescription")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "paidTo.idBillTo", target = "paidToId")
    @Mapping(source = "paidFrom.idBillTo", target = "paidFromId")
    ReceiptDTO toDto(Receipt receipt);

    @Mapping(target = "details", ignore = true)
    @Mapping(source = "paymentTypeId", target = "paymentType")
    @Mapping(source = "methodId", target = "method")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "paidToId", target = "paidTo")
    @Mapping(source = "paidFromId", target = "paidFrom")
    Receipt toEntity(ReceiptDTO receiptDTO);

    default Receipt fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Receipt receipt = new Receipt();
        receipt.setIdPayment(id);
        return receipt;
    }
}
