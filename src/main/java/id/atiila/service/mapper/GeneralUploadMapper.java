package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.GeneralUploadDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity GeneralUpload and its DTO GeneralUploadDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, PurposeTypeMapper.class})
public interface GeneralUploadMapper extends EntityMapper<GeneralUploadDTO, GeneralUpload> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "purpose.idPurposeType", target = "purposeId")
    @Mapping(source = "purpose.description", target = "purposeDescription")
    GeneralUploadDTO toDto(GeneralUpload generalUpload);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "purposeId", target = "purpose")
    @Mapping(target = "statuses", ignore = true)
    @Mapping(target = "status", ignore = true)
    GeneralUpload toEntity(GeneralUploadDTO generalUploadDTO);

    default GeneralUpload fromId(UUID id) {
        if (id == null) {
            return null;
        }
        GeneralUpload generalUpload = new GeneralUpload();
        generalUpload.setIdGeneralUpload(id);
        return generalUpload;
    }
}
