package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.OrderBillingItemDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity OrderBillingItem and its DTO OrderBillingItemDTO.
 */
@Mapper(componentModel = "spring", uses = {OrderItemMapper.class, BillingItemMapper.class})
public interface OrderBillingItemMapper extends EntityMapper<OrderBillingItemDTO, OrderBillingItem> {

    @Mapping(source = "orderItem.idOrderItem", target = "orderItemId")
    @Mapping(source = "billingItem.idBillingItem", target = "billingItemId")
    OrderBillingItemDTO toDto(OrderBillingItem orderBillingItem);

    @Mapping(source = "orderItemId", target = "orderItem")
    @Mapping(source = "billingItemId", target = "billingItem")
    OrderBillingItem toEntity(OrderBillingItemDTO orderBillingItemDTO);

    default OrderBillingItem fromId(UUID id) {
        if (id == null) {
            return null;
        }
        OrderBillingItem orderBillingItem = new OrderBillingItem();
        orderBillingItem.setIdOrderBillingItem(id);
        return orderBillingItem;
    }
}
