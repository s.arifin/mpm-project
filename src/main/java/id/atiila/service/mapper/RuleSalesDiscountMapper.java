package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RuleSalesDiscountDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity RuleSalesDiscount and its DTO RuleSalesDiscountDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RuleSalesDiscountMapper extends EntityMapper <RuleSalesDiscountDTO, RuleSalesDiscount> {
    
    

    default RuleSalesDiscount fromId(Integer id) {
        if (id == null) {
            return null;
        }
        RuleSalesDiscount ruleSalesDiscount = new RuleSalesDiscount();
        ruleSalesDiscount.setIdRule(id);
        return ruleSalesDiscount;
    }
}
