package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.QuoteDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Quote and its DTO QuoteDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface QuoteMapper extends EntityMapper <QuoteDTO, Quote> {



    default Quote fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Quote quote = new Quote();
        quote.setIdQuote(id);
        return quote;
    }
}
