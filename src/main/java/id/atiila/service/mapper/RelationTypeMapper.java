package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RelationTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity RelationType and its DTO RelationTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RelationTypeMapper extends EntityMapper <RelationTypeDTO, RelationType> {
    
    

    default RelationType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        RelationType relationType = new RelationType();
        relationType.setIdRelationType(id);
        return relationType;
    }
}
