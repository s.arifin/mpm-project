package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.BookingSlotDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity BookingSlot and its DTO BookingSlotDTO.
 */
@Mapper(componentModel = "spring", uses = {BaseCalendarMapper.class, BookingSlotStandardMapper.class, })
public interface BookingSlotMapper extends EntityMapper <BookingSlotDTO, BookingSlot> {

    @Mapping(source = "calendar.idCalendar", target = "calendarId")
    @Mapping(source = "calendar.description", target = "calendarDescription")

    @Mapping(source = "standard.idBookSlotStd", target = "standardId")
    @Mapping(source = "standard.description", target = "standardDescription")
    BookingSlotDTO toDto(BookingSlot bookingSlot); 

    @Mapping(source = "calendarId", target = "calendar")

    @Mapping(source = "standardId", target = "standard")
    BookingSlot toEntity(BookingSlotDTO bookingSlotDTO); 

    default BookingSlot fromId(UUID id) {
        if (id == null) {
            return null;
        }
        BookingSlot bookingSlot = new BookingSlot();
        bookingSlot.setIdBookSlot(id);
        return bookingSlot;
    }
}
