package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.repository.ProductRepository;
import id.atiila.service.dto.MotorDTO;
import id.atiila.service.dto.ProductDTO;
import id.atiila.service.dto.RemPartDTO;
import id.atiila.service.dto.ServiceDTO;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity Product and its DTO ProductDTO.
 */
@Mapper(componentModel = "spring", uses = {ProductTypeMapper.class, FeatureMapper.class, ProductCategoryMapper.class, WeServiceTypeMapper.class })
public abstract class ProductMapper {

    @Autowired
    private ProductRepository repo;

    public abstract MotorDTO toDto(Motor e);

    public abstract Motor toEntity(MotorDTO d);

    public abstract ServiceDTO toDto(Service e);

    public abstract Service toEntity(ServiceDTO d);

    public abstract RemPartDTO toDto(RemPart e);

    public abstract RemPart toEntity(RemPartDTO d);

    public ProductDTO toDto(Product e)  {
        if (e instanceof Service) return toDto((Service) e);
        else if (e instanceof RemPart) return toDto((RemPart) e);
        else if (e instanceof Motor) return toDto((Motor) e);
        else return null;

    };

    @Mapping(source = "productTypeId", target = "productType")
    public Product toEntity(ProductDTO d){
        if (d instanceof ServiceDTO) return toEntity((ServiceDTO) d);
        else if (d instanceof RemPartDTO) return toEntity((RemPartDTO) d);
        else if (d instanceof MotorDTO) return toEntity((MotorDTO) d);
        else return null;
    };

    public Product fromId(String id) {
        if (id == null) {
            return null;
        }
        return repo.findOne(id);
    }
}
