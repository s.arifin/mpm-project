package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.BookingTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity BookingType and its DTO BookingTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BookingTypeMapper extends EntityMapper <BookingTypeDTO, BookingType> {
    
    

    default BookingType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        BookingType bookingType = new BookingType();
        bookingType.setIdBookingType(id);
        return bookingType;
    }
}
