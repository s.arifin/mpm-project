package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.SuspectPersonDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity SuspectPerson and its DTO SuspectPersonDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, PostalAddressMapper.class, PersonMapper.class, SalesmanMapper.class, SuspectTypeMapper.class, SaleTypeMapper.class})
public interface SuspectPersonMapper extends EntityMapper <SuspectPersonDTO, SuspectPerson> {

    @Mapping(source = "suspectType.idSuspectType", target = "suspectTypeId")
    @Mapping(source = "dealer.idInternal", target = "dealerId")
    @Mapping(source = "dealer.organization.name", target = "dealerName")
    @Mapping(source = "salesCoordinator.idPartyRole", target = "salesCoordinatorId")
    @Mapping(source = "salesCoordinator.party.name", target = "salesCoordinatorName")
    @Mapping(source = "salesman.idPartyRole", target = "salesmanId")
    @Mapping(source = "salesman.party.name", target = "salesmanName")
    @Mapping(source = "saleType.idSaleType", target = "saleTypeId")
    @Mapping(source = "saleType.description", target = "saleTypeDescription")
    SuspectPersonDTO toDto(SuspectPerson suspectPerson);

    @Mapping(source = "suspectTypeId", target = "suspectType")
    @Mapping(source = "dealerId", target = "dealer")
    @Mapping(source = "salesCoordinatorId", target = "salesCoordinator")
    @Mapping(source = "salesmanId", target = "salesman")
    @Mapping(source = "saleTypeId", target = "saleType")
    SuspectPerson toEntity(SuspectPersonDTO suspectPersonDTO);

    default SuspectPerson fromId(UUID id) {
        if (id == null) {
            return null;
        }
        SuspectPerson suspectPerson = new SuspectPerson();
        suspectPerson.setIdSuspect(id);
        return suspectPerson;
    }
}
