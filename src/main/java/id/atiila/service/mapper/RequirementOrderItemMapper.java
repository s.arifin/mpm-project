package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RequirementOrderItemDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity RequirementOrderItem and its DTO RequirementOrderItemDTO.
 */
@Mapper(componentModel = "spring", uses = {OrderItemMapper.class, SalesUnitRequirementMapper.class})
public interface RequirementOrderItemMapper extends EntityMapper<RequirementOrderItemDTO, RequirementOrderItem> {

    @Mapping(source = "orderItem.idOrderItem", target = "orderItemId")
    @Mapping(source = "requirement.idRequirement", target = "requirementId")
    RequirementOrderItemDTO toDto(RequirementOrderItem requirementOrderItem);

    @Mapping(source = "orderItemId", target = "orderItem")
    @Mapping(source = "requirementId", target = "requirement")
    RequirementOrderItem toEntity(RequirementOrderItemDTO requirementOrderItemDTO);

    default RequirementOrderItem fromId(UUID id) {
        if (id == null) {
            return null;
        }
        RequirementOrderItem requirementOrderItem = new RequirementOrderItem();
        requirementOrderItem.setIdReqOrderItem(id);
        return requirementOrderItem;
    }
}
