package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.LeasingTenorProvideDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity LeasingTenorProvide and its DTO LeasingTenorProvideDTO.
 */
@Mapper(componentModel = "spring", uses = {
    LeasingCompanyMapper.class,
    ProductMapper.class,
    PartyMapper.class
})
public interface LeasingTenorProvideMapper extends EntityMapper <LeasingTenorProvideDTO, LeasingTenorProvide> {
    @Mapping(source = "product.idProduct", target = "productId")
    @Mapping(source = "product.name", target = "productName")
    @Mapping(source = "leasing.idPartyRole", target = "leasingId")
    @Mapping(source = "leasing.party.name", target = "leasingName")
    LeasingTenorProvideDTO toDto(LeasingTenorProvide leasingTenorProvide);

    @Mapping(source = "leasingId", target = "leasing")
    @Mapping(source = "productId", target = "product")
    LeasingTenorProvide toEntity(LeasingTenorProvideDTO leasingTenorProvideDTO);

    default LeasingTenorProvide fromId(UUID id) {
        if (id == null) {
            return null;
        }
        LeasingTenorProvide leasingTenorProvide = new LeasingTenorProvide();
        leasingTenorProvide.setIdLeasingProvide(id);
        return leasingTenorProvide;
    }
}
