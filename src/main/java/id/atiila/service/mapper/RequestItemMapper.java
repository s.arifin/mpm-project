package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RequestItemDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity RequestItem and its DTO RequestItemDTO.
 */
@Mapper(componentModel = "spring", uses = {
    RequestMapper.class,
    ProductMapper.class,
    FeatureMapper.class
})
public interface RequestItemMapper extends EntityMapper<RequestItemDTO, RequestItem> {

    @Mapping(source = "product.idProduct", target = "idProduct")
    @Mapping(source = "product.name", target = "productName")
    @Mapping(source = "feature.idFeature", target = "idFeature")
    @Mapping(source = "feature.description", target = "featureName")
    @Mapping(source = "request.idRequest", target = "requestId")
    RequestItemDTO toDto(RequestItem requestItem);

    @Mapping(source = "idFeature", target = "feature")
    @Mapping(source = "idProduct", target = "product")
    @Mapping(source = "requestId", target = "request")
    RequestItem toEntity(RequestItemDTO requestItemDTO);

    default RequestItem fromId(UUID id) {
        if (id == null) {
            return null;
        }
        RequestItem requestItem = new RequestItem();
        requestItem.setIdRequestItem(id);
        return requestItem;
    }
}
