package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ParentOrganizationDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ParentOrganization and its DTO ParentOrganizationDTO.
 */
@Mapper(componentModel = "spring", uses = {PartyMapper.class, })
public interface ParentOrganizationMapper extends EntityMapper <ParentOrganizationDTO, ParentOrganization> {

    ParentOrganizationDTO toDto(ParentOrganization parentOrganization);

    @Mapping(target = "facilities", ignore = true)
    @Mapping(target = "primaryFacility", ignore = true)
    ParentOrganization toEntity(ParentOrganizationDTO parentOrganizationDTO);

    default ParentOrganization fromId(String id) {
        if (id == null) {
            return null;
        }
        ParentOrganization parentOrganization = new ParentOrganization();
        parentOrganization.setIdInternal(id);
        return parentOrganization;
    }
}
