package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.service.dto.CustomShipmentDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

@Mapper(componentModel = "spring", uses = {OrderShipmentItemMapper.class, OrderBillingItemMapper.class, PickingSlipMapper.class,
                ShipmentOutgoingMapper.class})
public abstract class CustomShipmentMapper {

    @Autowired
    private  OrderItemRepository orderItemRepository;

    @Autowired
    private RequirementOrderItemRepository requirementOrderItemRepository;

    @Autowired
    private OrderShipmentItemRepository orderShipmentItemRepository;

    @Autowired
    private PickingSlipRepository pickingSlipRepository;

    @Autowired
    private ShipmentOutgoingRepository shipmentOutgoingRepository;

    @Autowired
    private ShipmentItemRepository shipmentItemRepository;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private VehicleSalesOrderRepository vehicleSalesOrderRepository;

    @Autowired
    private SalesUnitRequirementRepository salesUnitRequirementRepository;

    @Autowired
    private RequirementTypeRepository requirementTypeRepository;

    @Autowired
    private OrganizationCustomerRepository organizationCustomerRepository;

    @Autowired
    private PersonalCustomerRepository personalCustomerRepository;

    @Mapping(source = "deliveryAddress", target = "deliveryAddress")
    @Mapping(source = "idShipment", target = "idShipment")
    public abstract CustomShipmentDTO toDto(ShipmentOutgoing e);

    @Mapping(source = "orderItem.orders.orderNumber", target = "orderNumber")
    @Mapping(source = "orderItem.orders.idOrder", target = "idOrder")
    @Mapping(source = "shipmentItem.shipment.idShipment", target = "idShipment")
    @Mapping(source = "shipmentItem.shipment.shipmentNumber", target = "shipmentNumber")
    @Mapping(source = "shipmentItem.idShipmentItem", target = "idShipmentItem")
    @Mapping(source = "orderItem.idOrderItem", target = "idOrderItem")
    public abstract CustomShipmentDTO toDto(OrderShipmentItem e);

    @Mapping(source = "billingItem.billing.billingNumber", target = "IVUNumber")
    @Mapping(source = "orderItem.orders.orderNumber", target = "orderNumber")
    @Mapping(source = "orderItem.orders.idOrder", target = "idOrder")
    public abstract CustomShipmentDTO toDto(OrderBillingItem e);

    @Mapping(source = "inventoryItem.idFrame", target = "idframe")
    @Mapping(source = "inventoryItem.idMachine", target = "idMachine")
    @Mapping(source = "orderItem.orders.orderNumber", target = "orderNumber")
    @Mapping(source = "orderItem.orders.idOrder", target = "idOrder")
    @Mapping(source = "idSlip", target = "idSlip")
    // @Mapping(source = "currentStatus", target = "currentStatus")
    @Mapping(source = "orderItem.idOrderItem", target = "idOrderItem")
    public abstract CustomShipmentDTO toDto(PickingSlip e);

    public CustomShipmentDTO assignPis(CustomShipmentDTO dto) {
        OrderItem orderItem = null;
        VehicleSalesOrder vso = null;
        ItemIssuance issuance = null;
        ShipmentItem shipmentItem = null;
        ShipmentOutgoing shipmentOutgoing = null;
        Page<RequirementOrderItem> psur = null;

        if (dto.getIdSlip() != null) {
            PickingSlip pis = pickingSlipRepository.findOne(dto.getIdSlip());
            orderItem = pis.getOrderItem();
            vso = (VehicleSalesOrder) orderItem.getOrders();

            dto.setCurrentStatus(pis.getCurrentStatus());
            if (vso.getCustomer() != null)
            {
                dto.setIdCustomer(vso.getCustomer().getIdCustomer());
            }
            dto.setIdframe(pis.getInventoryItem().getIdFrame());
            dto.setRefferenceNumber(pis.getRefferenceNumber());
            dto.setIdInventoryMovementType(pis.getIdInventoryMovementType());

            dto.setAcc1(pis.getAcc1());
            dto.setAcc2(pis.getAcc2());
            dto.setAcc3(pis.getAcc3());
            dto.setAcc4(pis.getAcc4());
            dto.setAcc5(pis.getAcc5());
            dto.setAcc6(pis.getAcc6());
            dto.setAcc7(pis.getAcc7());
            dto.setAcc8(pis.getAcc8());

            dto.setAcctambah1(pis.getAcctambah1());
            dto.setAcctambah2(pis.getAcctambah2());

            dto.setPromat1(pis.getPromat1());
            dto.setPromat2(pis.getPromat2());
            dto.setQty(1);

            if (!pis.getIssuances().isEmpty()) issuance = pis.getIssuances().iterator().next();
            if (issuance != null) shipmentItem = issuance.getShipmentItem();
            if (shipmentItem != null) shipmentOutgoing = (ShipmentOutgoing) shipmentItem.getShipment();
            if (dto.getIdOrderItem() == null) dto.setIdOrderItem(pis.getOrderItem().getIdOrderItem());
        }

        if (dto.getIdOrderItem() != null) {
            // Ambil data SUR
            psur = requirementOrderItemRepository.queryByIdOrderItem(dto.getIdOrderItem(), new PageRequest(0,1));
            if (psur.hasContent()) {
                SalesUnitRequirement sur = (SalesUnitRequirement) psur.getContent().get(0).getRequirement();
                dto.setIdRequirement(sur.getIdRequirement());
                dto.setIdInternalsrc(sur.getInternal().getIdInternal());
                dto.setRequirementNumber(sur.getRequirementNumber());
                dto.setYearAssembly(sur.getProductYear());
                if (sur.getProduct() != null)
                {
                    dto.setCodeType(sur.getProduct().getIdProduct());
                }
                if (sur.getColor() !=null)
                {
                    dto.setIdfeature(sur.getColor().getIdFeature());
                    dto.setColorDescription(sur.getColor().getDescription());
                }

            }
        }

        // Ambil data Order
        if (orderItem == null && psur.hasContent()) {
            orderItem = psur.getContent().get(0).getOrderItem();
            vso = (VehicleSalesOrder) orderItem.getOrders();
        }

        if (vso != null && vso.getSaleType() != null) {
            dto.setIdSaleType(vso.getSaleType().getIdSaleType());
            dto.setSaleTypeDescription(vso.getSaleType().getDescription());
        }

        //
        if (shipmentOutgoing != null) {
            dto.setIdShipment(shipmentOutgoing.getIdShipment());
            dto.setDateSchedulle(shipmentOutgoing.getDateSchedulle());
            dto.setShipmentNumber(shipmentOutgoing.getShipmentNumber());
            dto.setDeliveryAddress(shipmentOutgoing.getDeliveryAddress());
            dto.setCurrentStatus(shipmentOutgoing.getCurrentStatus());
        }

//        this.salesmanName = null ;//v.getSalesUnitRequirement().getSalesman().getPerson().getName();
//        this.colorDescription = color != null ? color.getDescription() : null;

        if (vso != null && vso.getCustomer() != null) {
            dto.setIdCustomer(vso.getCustomer().getIdCustomer());
        }
//        if (v.getBillingNumber() != null)this.IVUNumber = v.getBillingNumber();
//
//        this.deliveryAddress = so.getDeliveryAddress();
//        this.billingNumber = v.getBillingNumber();
//        this.jumlahprint = so.getJumlahprint();

        return dto;
    }

    public CustomShipmentDTO assignUnitPrep(CustomShipmentDTO dto) {
        OrderItem orderItem = null;
        VehicleSalesOrder vso = null;
        ItemIssuance issuance = null;
        ShipmentItem shipmentItem = null;
        ShipmentOutgoing shipmentOutgoing = null;
        Page<RequirementOrderItem> psur = null;

        if (dto.getIdSlip() != null) {
            PickingSlip pis = pickingSlipRepository.findOne(dto.getIdSlip());
            orderItem = pis.getOrderItem();
            vso = (VehicleSalesOrder) orderItem.getOrders();

            dto.setCurrentStatus(pis.getCurrentStatus());
            dto.setIdframe(pis.getInventoryItem().getIdFrame());
            dto.setRefferenceNumber(pis.getRefferenceNumber());
            dto.setIdInventoryMovementType(pis.getIdInventoryMovementType());

            if (!pis.getIssuances().isEmpty()) issuance = pis.getIssuances().iterator().next();
            if (issuance != null) shipmentItem = issuance.getShipmentItem();
            if (shipmentItem != null) shipmentOutgoing = (ShipmentOutgoing) shipmentItem.getShipment();
            if (dto.getIdOrderItem() == null) dto.setIdOrderItem(pis.getOrderItem().getIdOrderItem());
        }

        if (dto.getIdOrderItem() != null) {
            // Ambil data SUR
            psur = requirementOrderItemRepository.queryByIdOrderItem(dto.getIdOrderItem(), new PageRequest(0,1));
            if (psur.hasContent()) {
                SalesUnitRequirement sur = (SalesUnitRequirement) psur.getContent().get(0).getRequirement();
                dto.setIdRequirement(sur.getIdRequirement());
                dto.setIdInternalsrc(sur.getInternal().getIdInternal());
                dto.setSrcInternalName(sur.getInternal().getOrganization().getName());
                if (sur.getSalesman() != null ) {
                    Salesman salesman = sur.getSalesman();
                    dto.setSalesmanName(salesman.getPerson().getName());
                    dto.setCoordinatorSales(salesman.getCoordinatorSales().getPerson().getName());
                    dto.setNameCustomer(sur.getCustomer().getParty().getName());
                }
            }
        }

        // Ambil data Order
        if (orderItem == null && psur.hasContent()) {
            orderItem = psur.getContent().get(0).getOrderItem();
            vso = (VehicleSalesOrder) orderItem.getOrders();
        }

        if (vso != null && vso.getSaleType() != null) {
            dto.setIdSaleType(vso.getSaleType().getIdSaleType());
            dto.setSaleTypeDescription(vso.getSaleType().getDescription());
        }
        if (shipmentOutgoing != null) {
            dto.setIdShipment(shipmentOutgoing.getIdShipment());
            dto.setDateSchedulle(shipmentOutgoing.getDateSchedulle());
            dto.setDeliveryAddress(shipmentOutgoing.getDeliveryAddress());
            dto.setCurrentStatus(shipmentOutgoing.getCurrentStatus());
        }
        return dto;
    }

    public CustomShipmentDTO assignDriver(CustomShipmentDTO dto) {
        if (dto.getIdShipment() != null) {
            ShipmentOutgoing so = shipmentOutgoingRepository.findOne(dto.getIdShipment());
            dto.setDateSchedulle(so.getDateSchedulle());
            dto.setAddress(so.getDeliveryAddress());
            dto.setShipmentOutgoingdDescription(so.getDescription());
        }
        if (dto.getIdOrder() != null) {
            VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(dto.getIdOrder());
            SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(vso.getIdSalesUnitRequirement());
            dto.setIdInternalsrc(sur.getInternal().getIdInternal());
            dto.setSrcInternalName(sur.getInternal().getOrganization().getName());
            dto.setSaleTypeDescription(sur.getSaleType().getDescription());
            if (sur.getProduct().getIdProduct() != null)
            {
                dto.setCodeType(sur.getProduct().getIdProduct());
            }
            if (sur.getColor().getIdFeature() != null) {
                dto.setColorDescription(sur.getColor().getRefKey());
            }

            if (dto.getIdOrderItem() != null) {
                List<PickingSlip> ps = pickingSlipRepository.findOneByidOrderItem(dto.getIdOrderItem());
                if (ps != null) {
                    int l= ps.size()-1;
                    dto.setIdSlip(ps.get(l).getIdSlip());
                    InventoryItem ii = ps.get(l).getInventoryItem();
                    if (ii != null ) {
                        dto.setIdframe(ii.getIdFrame());
                        dto.setIdMachine(ii.getIdMachine());
                    }
                    if ( sur.getRequirementType() == null) {
                        dto.setNameCustomer(sur.getPersonOwner().getName());
                        dto.setNoHp(sur.getPersonOwner().getCellPhone1());
                    } else {
                        if ( sur.getRequirementType().getIdRequirementType() == 102 ) {
                            PersonalCustomer pc = personalCustomerRepository.findByIdCustomer(sur.getCustomer().getIdCustomer());
                            dto.setNameCustomer(pc.getPerson().getName());
                            dto.setNoHp(pc.getPerson().getCellPhone1());
                        } else if ( sur.getRequirementType().getIdRequirementType() == 101 || sur.getRequirementType().getIdRequirementType() == 103 ) {
                            OrganizationCustomer oc = organizationCustomerRepository.findByIdCustomer(sur.getCustomer().getIdCustomer());
                            dto.setNameCustomer(oc.getOrganization().getName());
                            dto.setNoHp(oc.getOrganization().getOfficePhone());
                        }
                    }
                }
            }

        }
        return dto;
    }

    public  CustomShipmentDTO assignDetailDriver(CustomShipmentDTO dto) {

        PickingSlip ps= null;
        if (dto.getIdShipment() != null) {
            ShipmentOutgoing so = shipmentOutgoingRepository.findOne(dto.getIdShipment());
            dto.setDateSchedulle(so.getDateSchedulle());
            //dto.setAddress(so.getDeliveryAddress());
            dto.setDeliveryAddress(so.getDeliveryAddress());
            dto.setShipmentOutgoingdDescription(so.getDescription());
        }
        if (dto.getIdOrder() != null) {
            VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(dto.getIdOrder());
            SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(vso.getIdSalesUnitRequirement());
            dto.setSaleTypeDescription(sur.getSaleType().getDescription());
            dto.setNameCustomer(sur.getPersonOwner().getName());
            dto.setNoHp(sur.getPersonOwner().getCellPhone1());
            dto.setEmail(sur.getPersonOwner().getPrivateMail());
            if (sur.getIdframe() != null)
            {
                dto.setIdframe(sur.getIdframe());
            } else  if (sur.getRequestIdFrame() != null) {
                dto.setIdframe(sur.getRequestIdFrame());
            }
            if (sur.getProduct() != null){
                dto.setProductName(sur.getProduct().getName());
            }
            if (sur.getColor() != null) {
                dto.setColorDescription(sur.getColor().getDescription());
            }
            if (sur.getCustomer() != null) {
                dto.setIdCustomer(sur.getCustomer().getIdCustomer());
                dto.setIdParty(sur.getCustomer().getParty().getIdParty());
            }
        }

        return  dto;
    }

    public CustomShipmentDTO assignJadwal(CustomShipmentDTO dto) {
        if (dto.getIdShipment() != null) {
            ShipmentOutgoing so = shipmentOutgoingRepository.findOne(dto.getIdShipment());
            dto.setDateSchedulle(so.getDateSchedulle());
            dto.setAddress(so.getDeliveryAddress());
            dto.setShipmentNumber(so.getShipmentNumber());
            dto.setCurrentStatus(so.getCurrentStatus());
            dto.setQty(1);
            dto.setDeliveryAddress(so.getDeliveryAddress());
        }
        if (dto.getIdOrder() != null) {
            VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(dto.getIdOrder());
            SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(vso.getIdSalesUnitRequirement());
            dto.setIdInternalsrc(sur.getInternal().getIdInternal());
            dto.setSrcInternalName(sur.getInternal().getOrganization().getName());
            Salesman s= sur.getSalesman();
            if (s != null) {
                dto.setSalesmanName(s.getPerson().getName());
                dto.setCoordinatorSales(s.getCoordinatorSales().getPerson().getName());
            }
            dto.setSaleTypeDescription(sur.getSaleType().getDescription());
        }
        if (dto.getIdOrderItem() != null) {
            List<PickingSlip> ps = pickingSlipRepository.findPickingSlipByidOrderItem(dto.getIdOrderItem());
            if (ps != null) {
                int l= ps.size()-1;
                dto.setIdSlip(ps.get(l).getIdSlip());
                InventoryItem ii = ps.get(l).getInventoryItem();
                if (ii != null ) {
                    dto.setIdframe(ii.getIdFrame());
                    dto.setIdMachine(ii.getIdMachine());
                }
            }
        }
        return dto;
    }

}
