package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.DriverDTO;

import org.mapstruct.*;

import java.util.UUID;

/**
 * Mapper for the entity Driver and its DTO DriverDTO.
 */
@Mapper(componentModel = "spring", uses = {RoleTypeMapper.class, PersonMapper.class})
public interface DriverMapper extends EntityMapper <DriverDTO, Driver> {

    @Mapping(source = "party.idParty", target = "partyId")
    @Mapping(source = "party.name", target = "partyName")
    DriverDTO toDto(Driver driver);

    @Mapping(source = "partyId", target = "party")
    Driver toEntity(DriverDTO driverDTO);

    default Driver fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Driver driver = new Driver();
        driver.setIdPartyRole(id);
        return driver;
    }
}
