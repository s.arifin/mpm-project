package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.UserMediatorDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity UserMediator and its DTO UserMediatorDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, PersonMapper.class})
public interface UserMediatorMapper extends EntityMapper<UserMediatorDTO, UserMediator> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    UserMediatorDTO toDto(UserMediator userMediator);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(target = "positions", ignore = true)
    @Mapping(target = "roles", ignore = true)
    @Mapping(target = "statuses", ignore = true)
    @Mapping(target = "status", ignore = true)
    UserMediator toEntity(UserMediatorDTO userMediatorDTO);

    default UserMediator fromId(UUID id) {
        if (id == null) {
            return null;
        }
        UserMediator userMediator = new UserMediator();
        userMediator.setIdUserMediator(id);
        return userMediator;
    }
}
