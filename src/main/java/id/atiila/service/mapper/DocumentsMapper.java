package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.DocumentsDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Documents and its DTO DocumentsDTO.
 */
@Mapper(componentModel = "spring", uses = {DocumentTypeMapper.class, })
public interface DocumentsMapper extends EntityMapper <DocumentsDTO, Documents> {

    @Mapping(source = "documentType.idDocumentType", target = "documentTypeId")
    @Mapping(source = "documentType.description", target = "documentTypeDescription")
    DocumentsDTO toDto(Documents documents); 

    @Mapping(source = "documentTypeId", target = "documentType")
    Documents toEntity(DocumentsDTO documentsDTO); 

    default Documents fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Documents documents = new Documents();
        documents.setIdDocument(id);
        return documents;
    }
}
