package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.VehicleDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Vehicle and its DTO VehicleDTO.
 */
@Mapper(componentModel = "spring", uses = {GoodMapper.class, FeatureMapper.class, })
public interface VehicleMapper extends EntityMapper <VehicleDTO, Vehicle> {

    @Mapping(source = "product.idProduct", target = "productId")
    @Mapping(source = "product.name", target = "productName")

    @Mapping(source = "feature.idFeature", target = "featureId")
    @Mapping(source = "feature.description", target = "featureDescription")
    VehicleDTO toDto(Vehicle vehicle); 

    @Mapping(source = "productId", target = "product")

    @Mapping(source = "featureId", target = "feature")
    Vehicle toEntity(VehicleDTO vehicleDTO); 

    default Vehicle fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Vehicle vehicle = new Vehicle();
        vehicle.setIdVehicle(id);
        return vehicle;
    }
}
