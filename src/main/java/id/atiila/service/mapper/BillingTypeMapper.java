package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.BillingTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity BillingType and its DTO BillingTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BillingTypeMapper extends EntityMapper <BillingTypeDTO, BillingType> {
    
    

    default BillingType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        BillingType billingType = new BillingType();
        billingType.setIdBillingType(id);
        return billingType;
    }
}
