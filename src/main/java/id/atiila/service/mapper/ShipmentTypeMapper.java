package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ShipmentTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity ShipmentType and its DTO ShipmentTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ShipmentTypeMapper extends EntityMapper <ShipmentTypeDTO, ShipmentType> {
    
    

    default ShipmentType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        ShipmentType shipmentType = new ShipmentType();
        shipmentType.setIdShipmentType(id);
        return shipmentType;
    }
}
