package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.LeasingCompanyDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity LeasingCompany and its DTO LeasingCompanyDTO.
 */
@Mapper(componentModel = "spring", uses = {
    PartyMapper.class
})
public interface LeasingCompanyMapper extends EntityMapper <LeasingCompanyDTO, LeasingCompany> {

    @Mapping(source = "party.idParty", target = "partyId")
    @Mapping(source = "party.name", target = "partyName")
    LeasingCompanyDTO toDto(LeasingCompany leasingCompany);

    LeasingCompany toEntity(LeasingCompanyDTO leasingCompanyDTO);

    default LeasingCompany fromId(UUID id) {
        if (id == null) {
            return null;
        }
        LeasingCompany leasingCompany = new LeasingCompany();
        leasingCompany.setIdPartyRole(id);
        return leasingCompany;
    }
}
