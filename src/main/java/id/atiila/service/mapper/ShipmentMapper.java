package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ShipmentDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Shipment and its DTO ShipmentDTO.
 */
@Mapper(componentModel = "spring", uses = {ShipmentTypeMapper.class, ShipToMapper.class, PostalAddressMapper.class, InternalMapper.class})
public interface ShipmentMapper extends EntityMapper<ShipmentDTO, Shipment> {

    @Mapping(source = "shipmentType.idShipmentType", target = "shipmentTypeId")
    @Mapping(source = "shipFrom.idShipTo", target = "shipFromId")
    @Mapping(source = "shipTo.idShipTo", target = "shipToId")
    @Mapping(source = "addressFrom.idContact", target = "addressFromId")
    @Mapping(source = "addressTo.idContact", target = "addressToId")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    ShipmentDTO toDto(Shipment shipment);

    @Mapping(target = "details", ignore = true)
    @Mapping(source = "shipmentTypeId", target = "shipmentType")
    @Mapping(source = "shipFromId", target = "shipFrom")
    @Mapping(source = "shipToId", target = "shipTo")
    @Mapping(source = "addressFromId", target = "addressFrom")
    @Mapping(source = "addressToId", target = "addressTo")
    @Mapping(source = "internalId", target = "internal")
    Shipment toEntity(ShipmentDTO shipmentDTO);

    default Shipment fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Shipment shipment = new Shipment();
        shipment.setIdShipment(id);
        return shipment;
    }
}
