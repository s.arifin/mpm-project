package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ElectronicAddressDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ElectronicAddress and its DTO ElectronicAddressDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ElectronicAddressMapper extends EntityMapper <ElectronicAddressDTO, ElectronicAddress> {

    ElectronicAddress toEntity(ElectronicAddressDTO dto);

    default ElectronicAddress fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ElectronicAddress electronicAddress = new ElectronicAddress();
        electronicAddress.setIdContact(id);
        return electronicAddress;
    }
}
