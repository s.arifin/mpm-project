package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ShipmentOutgoingDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ShipmentOutgoing and its DTO ShipmentOutgoingDTO.
 */
@Mapper(componentModel = "spring", uses = {ShipmentMapper.class, DriverMapper.class, PartyMapper.class,
            ShipToMapper.class, InventoryItemMapper.class, OrderItemMapper.class, ShipmentTypeMapper.class,
            PostalAddressMapper.class, InternalMapper.class, })
public interface ShipmentOutgoingMapper extends EntityMapper <ShipmentOutgoingDTO, ShipmentOutgoing> {

    @Mapping(source = "shipmentType.idShipmentType", target = "shipmentTypeId")
    @Mapping(source = "shipFrom.idShipTo", target = "shipFromId")
    @Mapping(source = "shipTo.idShipTo", target = "shipToId")
    @Mapping(source = "addressFrom.idContact", target = "addressFromId")
    @Mapping(source = "addressTo.idContact", target = "addressToId")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    ShipmentOutgoingDTO toDto(ShipmentOutgoing e);

    @Mapping(target = "details", ignore = true)
    @Mapping(source = "shipmentTypeId", target = "shipmentType")
    @Mapping(source = "shipFromId", target = "shipFrom")
    @Mapping(source = "shipToId", target = "shipTo")
    @Mapping(source = "addressFromId", target = "addressFrom")
    @Mapping(source = "addressToId", target = "addressTo")
    @Mapping(source = "internalId", target = "internal")
    ShipmentOutgoing toEntity(ShipmentOutgoingDTO d);

    default ShipmentOutgoing fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ShipmentOutgoing shipmentOutgoing = new ShipmentOutgoing();
        shipmentOutgoing.setIdShipment(id);
        return shipmentOutgoing;
    }
}
