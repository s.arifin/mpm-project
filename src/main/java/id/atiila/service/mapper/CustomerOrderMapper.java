package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.CustomerOrderDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity CustomerOrder and its DTO CustomerOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, CustomerMapper.class, BillToMapper.class})
public interface CustomerOrderMapper extends EntityMapper<CustomerOrderDTO, CustomerOrder> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "customer.idCustomer", target = "customerId")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    CustomerOrderDTO toDto(CustomerOrder customerOrder);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "customerId", target = "customer")
    @Mapping(source = "billToId", target = "billTo")
    CustomerOrder toEntity(CustomerOrderDTO customerOrderDTO);

    default CustomerOrder fromId(UUID id) {
        if (id == null) {
            return null;
        }
        CustomerOrder customerOrder = new CustomerOrder();
        customerOrder.setIdOrder(id);
        return customerOrder;
    }
}
