package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.VehicleCustomerRequestDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity VehicleCustomerRequest and its DTO VehicleCustomerRequestDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, OrganizationCustomerMapper.class, RequestTypeMapper.class, SalesBrokerMapper.class})
public interface VehicleCustomerRequestMapper extends EntityMapper<VehicleCustomerRequestDTO, VehicleCustomerRequest> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    @Mapping(source = "customer.idCustomer", target = "customerId")
    @Mapping(source = "customer.party.name", target = "customerName")
    @Mapping(source = "requestType.idRequestType", target = "requestTypeId")
    @Mapping(source = "requestType.description", target = "requestTypeDescription")
    @Mapping(source = "salesBroker.idPartyRole", target = "salesBrokerId")
    @Mapping(source = "salesBroker.idBroker", target = "salesBrokerCode")
    @Mapping(source = "salesBroker.party.name", target = "salesBrokerName")
    VehicleCustomerRequestDTO toDto(VehicleCustomerRequest vehicleCustomerRequest);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "customerId", target = "customer")
    @Mapping(source = "requestTypeId", target = "requestType")
    @Mapping(source = "salesBrokerId", target = "salesBroker")
    @Mapping(target = "statuses", ignore = true)
    @Mapping(target = "roles", ignore = true)
    @Mapping(target = "details", ignore = true)
    @Mapping(target = "status", ignore = true)
    VehicleCustomerRequest toEntity(VehicleCustomerRequestDTO vehicleCustomerRequestDTO);

    default VehicleCustomerRequest fromId(UUID id) {
        if (id == null) {
            return null;
        }
        VehicleCustomerRequest vehicleCustomerRequest = new VehicleCustomerRequest();
        vehicleCustomerRequest.setIdRequest(id);
        return vehicleCustomerRequest;
    }
}
