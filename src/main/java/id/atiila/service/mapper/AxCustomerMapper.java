package id.atiila.service.mapper;

import id.atiila.domain.Customer;
import id.atiila.domain.PersonalCustomer;
import id.atiila.repository.PersonalCustomerRepository;
import id.atiila.service.ax.dto.AxCustomerDTO;
import id.atiila.service.dto.CustomerDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity Customer and its DTO CustomerDTO.
 */
@Mapper(componentModel = "spring", uses = {PartyMapper.class})
public abstract class AxCustomerMapper {

    @Autowired
    private PersonalCustomerRepository personalCustomerRepository;

    @Mapping(source = "party.name", target = "name")
    public AxCustomerDTO toDto(Customer customer) {
        AxCustomerDTO r = new AxCustomerDTO();
        return r;
    };

//    public Customer toEntity(AxCustomerDTO customerDTO) {
//    };

    public Customer fromId(String id) {
        if (id == null) {
            return null;
        }
        Customer customer = new Customer();
        customer.setIdCustomer(id);
        return customer;
    }
}
