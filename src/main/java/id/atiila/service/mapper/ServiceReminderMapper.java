package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ServiceReminderDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity ServiceReminder and its DTO ServiceReminderDTO.
 */
@Mapper(componentModel = "spring", uses = {DealerClaimTypeMapper.class, DealerReminderTypeMapper.class, })
public interface ServiceReminderMapper extends EntityMapper <ServiceReminderDTO, ServiceReminder> {

    @Mapping(source = "claimType.idClaimType", target = "claimTypeId")
    @Mapping(source = "claimType.description", target = "claimTypeDescription")

    @Mapping(source = "reminderType.idReminderType", target = "reminderTypeId")
    @Mapping(source = "reminderType.description", target = "reminderTypeDescription")
    ServiceReminderDTO toDto(ServiceReminder serviceReminder); 

    @Mapping(source = "claimTypeId", target = "claimType")

    @Mapping(source = "reminderTypeId", target = "reminderType")
    ServiceReminder toEntity(ServiceReminderDTO serviceReminderDTO); 

    default ServiceReminder fromId(Integer id) {
        if (id == null) {
            return null;
        }
        ServiceReminder serviceReminder = new ServiceReminder();
        serviceReminder.setIdReminder(id);
        return serviceReminder;
    }
}
