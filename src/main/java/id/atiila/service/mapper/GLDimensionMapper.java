package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.GLDimensionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity GLDimension and its DTO GLDimensionDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, BillToMapper.class,
    VendorMapper.class, PartyCategoryMapper.class, ProductCategoryMapper.class, DimensionMapper.class})
public interface GLDimensionMapper extends EntityMapper<GLDimensionDTO, GLDimension> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    @Mapping(source = "vendor.idVendor", target = "vendorId")
    @Mapping(source = "partyCategory.idCategory", target = "partyCategoryId")
    @Mapping(source = "productCategory.idCategory", target = "productCategoryId")
    @Mapping(source = "dimension1.idDimension", target = "dimension1Id")
    @Mapping(source = "dimension1.description", target = "dimension1Description")
    @Mapping(source = "dimension2.idDimension", target = "dimension2Id")
    @Mapping(source = "dimension2.description", target = "dimension2Description")
    @Mapping(source = "dimension3.idDimension", target = "dimension3Id")
    @Mapping(source = "dimension3.description", target = "dimension3Description")
    @Mapping(source = "dimension4.idDimension", target = "dimension4Id")
    @Mapping(source = "dimension4.description", target = "dimension4Description")
    @Mapping(source = "dimension5.idDimension", target = "dimension5Id")
    @Mapping(source = "dimension5.description", target = "dimension5Description")
    @Mapping(source = "dimension6.idDimension", target = "dimension6Id")
    @Mapping(source = "dimension6.description", target = "dimension6Description")
    GLDimensionDTO toDto(GLDimension gLDimension);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "billToId", target = "billTo")
    @Mapping(source = "vendorId", target = "vendor")
    @Mapping(source = "partyCategoryId", target = "partyCategory")
    @Mapping(source = "productCategoryId", target = "productCategory")
    @Mapping(source = "dimension1Id", target = "dimension1")
    @Mapping(source = "dimension2Id", target = "dimension2")
    @Mapping(source = "dimension3Id", target = "dimension3")
    @Mapping(source = "dimension4Id", target = "dimension4")
    @Mapping(source = "dimension5Id", target = "dimension5")
    @Mapping(source = "dimension6Id", target = "dimension6")
    GLDimension toEntity(GLDimensionDTO gLDimensionDTO);

    default GLDimension fromId(Integer id) {
        if (id == null) {
            return null;
        }
        GLDimension gLDimension = new GLDimension();
        gLDimension.setIdGLDimension(id);
        return gLDimension;
    }
}
