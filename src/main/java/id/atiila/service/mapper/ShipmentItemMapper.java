package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ShipmentItemDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ShipmentItem and its DTO ShipmentItemDTO.
 */
@Mapper(componentModel = "spring", uses = {ShipmentMapper.class, FeatureMapper.class})
public interface ShipmentItemMapper extends EntityMapper<ShipmentItemDTO, ShipmentItem> {

    @Mapping(source = "shipment.idShipment", target = "shipmentId")
    @Mapping(source = "feature.idFeature", target = "featureId")
    @Mapping(source = "feature.description", target = "featureDescription")
    @Mapping(source = "feature.refKey", target = "featureRefKey")
    ShipmentItemDTO toDto(ShipmentItem shipmentItem);

    @Mapping(source = "shipmentId", target = "shipment")
    @Mapping(target = "billingItems", ignore = true)
    ShipmentItem toEntity(ShipmentItemDTO shipmentItemDTO);

    default ShipmentItem fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ShipmentItem shipmentItem = new ShipmentItem();
        shipmentItem.setIdShipmentItem(id);
        return shipmentItem;
    }
}
