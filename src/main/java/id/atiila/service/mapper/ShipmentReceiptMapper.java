package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ShipmentReceiptDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ShipmentReceipt and its DTO ShipmentReceiptDTO.
 */
@Mapper(componentModel = "spring", uses = {ShipmentPackageMapper.class, ShipmentItemMapper.class,
    OrderItemMapper.class, FeatureMapper.class})
public interface ShipmentReceiptMapper extends EntityMapper<ShipmentReceiptDTO, ShipmentReceipt> {

    @Mapping(source = "shipmentPackage.idPackage", target = "shipmentPackageId")
    @Mapping(source = "shipmentItem.idShipmentItem", target = "shipmentItemId")
    @Mapping(source = "orderItem.idOrderItem", target = "orderItemId")
    @Mapping(source = "feature.idFeature", target = "featureId")
    @Mapping(source = "feature.description", target = "featureDescription")
    @Mapping(source = "feature.refKey", target = "featureRefKey")
    ShipmentReceiptDTO toDto(ShipmentReceipt shipmentReceipt);

    @Mapping(source = "shipmentPackageId", target = "shipmentPackage")
    @Mapping(source = "shipmentItemId", target = "shipmentItem")
    @Mapping(source = "orderItemId", target = "orderItem")
    @Mapping(target = "inventoryItems", ignore = true)
    ShipmentReceipt toEntity(ShipmentReceiptDTO shipmentReceiptDTO);

    default ShipmentReceipt fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ShipmentReceipt shipmentReceipt = new ShipmentReceipt();
        shipmentReceipt.setIdReceipt(id);
        return shipmentReceipt;
    }
}
