package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.UnitAccesoriesMapperDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity UnitAccesoriesMapper and its DTO UnitAccesoriesMapperDTO.
 */
@Mapper(componentModel = "spring", uses = {MotorMapper.class, })
public interface UnitAccesoriesMapperMapper extends EntityMapper <UnitAccesoriesMapperDTO, UnitAccesoriesMapper> {

    @Mapping(source = "motor.idProduct", target = "motorId")
    @Mapping(source = "motor.name", target = "motorName")
    UnitAccesoriesMapperDTO toDto(UnitAccesoriesMapper unitAccesoriesMapper); 

    @Mapping(source = "motorId", target = "motor")
    UnitAccesoriesMapper toEntity(UnitAccesoriesMapperDTO unitAccesoriesMapperDTO); 

    default UnitAccesoriesMapper fromId(UUID id) {
        if (id == null) {
            return null;
        }
        UnitAccesoriesMapper unitAccesoriesMapper = new UnitAccesoriesMapper();
        unitAccesoriesMapper.setIdUnitAccMapper(id);
        return unitAccesoriesMapper;
    }
}
