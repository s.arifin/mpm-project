package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.WorkTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity WorkType and its DTO WorkTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WorkTypeMapper extends EntityMapper <WorkTypeDTO, WorkType> {
    
    

    default WorkType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        WorkType workType = new WorkType();
        workType.setIdWorkType(id);
        return workType;
    }
}
