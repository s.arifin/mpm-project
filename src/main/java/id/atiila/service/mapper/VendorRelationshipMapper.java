package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.VendorRelationshipDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity VendorRelationship and its DTO VendorRelationshipDTO.
 */
@Mapper(componentModel = "spring", uses = {StatusTypeMapper.class, RelationTypeMapper.class, InternalMapper.class, VendorMapper.class})
public interface VendorRelationshipMapper extends EntityMapper<VendorRelationshipDTO, VendorRelationship> {

    @Mapping(source = "statusType.idStatusType", target = "statusTypeId")
    @Mapping(source = "statusType.description", target = "statusTypeDescription")
    @Mapping(source = "relationType.idRelationType", target = "relationTypeId")
    @Mapping(source = "relationType.description", target = "relationTypeDescription")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "vendor.idVendor", target = "vendorId")
    VendorRelationshipDTO toDto(VendorRelationship vendorRelationship);

    @Mapping(source = "statusTypeId", target = "statusType")
    @Mapping(source = "relationTypeId", target = "relationType")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "vendorId", target = "vendor")
    VendorRelationship toEntity(VendorRelationshipDTO vendorRelationshipDTO);

    default VendorRelationship fromId(UUID id) {
        if (id == null) {
            return null;
        }
        VendorRelationship vendorRelationship = new VendorRelationship();
        vendorRelationship.setIdPartyRelationship(id);
        return vendorRelationship;
    }
}
