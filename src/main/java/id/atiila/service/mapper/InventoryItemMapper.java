package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.InventoryItemDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity InventoryItem and its DTO InventoryItemDTO.
 */
@Mapper(componentModel = "spring", uses = {ShipmentReceiptMapper.class, })
public interface InventoryItemMapper extends EntityMapper <InventoryItemDTO, InventoryItem> {

    @Mapping(source = "shipmentReceipt.idReceipt", target = "shipmentReceiptId")
    @Mapping(source = "shipmentReceipt.itemDescription", target = "shipmentReceiptDescription")
    @Mapping(source = "shipmentReceipt.receiptCode" , target = "shipmentReceiptCode")
    @Mapping(source = "shipmentReceipt.dateReceipt", target = "shipmentReceiptDate")
    InventoryItemDTO toDto(InventoryItem inventoryItem);

    @Mapping(source = "shipmentReceiptId", target = "shipmentReceipt")
    InventoryItem toEntity(InventoryItemDTO inventoryItemDTO);

    default InventoryItem fromId(UUID id) {
        if (id == null) {
            return null;
        }
        InventoryItem inventoryItem = new InventoryItem();
        inventoryItem.setIdInventoryItem(id);
        return inventoryItem;
    }
}
