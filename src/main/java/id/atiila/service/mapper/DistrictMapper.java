package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.DistrictDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity District and its DTO DistrictDTO.
 */
@Mapper(componentModel = "spring", uses = {GeoBoundaryTypeMapper.class, GeoBoundaryMapper.class})
public interface DistrictMapper extends EntityMapper <DistrictDTO, District> {



    default District fromId(UUID id) {
        if (id == null) {
            return null;
        }
        District district = new District();
        district.setIdGeobou(id);
        return district;
    }
}
