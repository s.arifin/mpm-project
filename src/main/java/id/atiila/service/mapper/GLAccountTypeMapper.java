package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.GLAccountTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity GLAccountType and its DTO GLAccountTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GLAccountTypeMapper extends EntityMapper<GLAccountTypeDTO, GLAccountType> {



    default GLAccountType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        GLAccountType gLAccountType = new GLAccountType();
        gLAccountType.setIdGLAccountType(id);
        return gLAccountType;
    }
}
