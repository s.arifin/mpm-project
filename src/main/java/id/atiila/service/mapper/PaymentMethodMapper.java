package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PaymentMethodDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity PaymentMethod and its DTO PaymentMethodDTO.
 */
@Mapper(componentModel = "spring", uses = {PaymentMethodTypeMapper.class})
public interface PaymentMethodMapper extends EntityMapper<PaymentMethodDTO, PaymentMethod> {

    @Mapping(source = "methodType.idPaymentMethodType", target = "methodTypeId")
    @Mapping(source = "methodType.description", target = "methodTypeDescription")
    PaymentMethodDTO toDto(PaymentMethod paymentMethod);

    @Mapping(source = "methodTypeId", target = "methodType")
    PaymentMethod toEntity(PaymentMethodDTO paymentMethodDTO);

    default PaymentMethod fromId(Integer id) {
        if (id == null) {
            return null;
        }
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setIdPaymentMethod(id);
        return paymentMethod;
    }
}
