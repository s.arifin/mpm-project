package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.DealerReminderTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity DealerReminderType and its DTO DealerReminderTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DealerReminderTypeMapper extends EntityMapper <DealerReminderTypeDTO, DealerReminderType> {
    
    

    default DealerReminderType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        DealerReminderType dealerReminderType = new DealerReminderType();
        dealerReminderType.setIdReminderType(id);
        return dealerReminderType;
    }
}
