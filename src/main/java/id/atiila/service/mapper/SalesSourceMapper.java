package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.SalesSourceDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity SalesSource and its DTO SalesSourceDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SalesSourceMapper extends EntityMapper <SalesSourceDTO, SalesSource> {
    
    

    default SalesSource fromId(Long id) {
        if (id == null) {
            return null;
        }
        SalesSource salesSource = new SalesSource();
        salesSource.setId(id);
        return salesSource;
    }
}
