package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.BranchDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Branch and its DTO BranchDTO.
 */
@Mapper(componentModel = "spring", uses = {OrganizationMapper.class})
public interface BranchMapper extends EntityMapper<BranchDTO, Branch> {

    @Mapping(source = "organization.name", target = "internalName")
    BranchDTO toDto(Branch branch);

    Branch toEntity(BranchDTO branchDTO);

    default Branch fromId(String id) {
        if (id == null) {
            return null;
        }
        Branch branch = new Branch();
        branch.setIdInternal(id);
        return branch;
    }
}
