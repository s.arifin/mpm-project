package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PositionFullfillmentDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity PositionFullfillment and its DTO PositionFullfillmentDTO.
 */
@Mapper(componentModel = "spring", uses = {PositionMapper.class, PersonMapper.class, })
public interface PositionFullfillmentMapper extends EntityMapper <PositionFullfillmentDTO, PositionFullfillment> {

    @Mapping(source = "position.idPosition", target = "positionId")

    @Mapping(source = "person.idParty", target = "personId")
    @Mapping(source = "person.name", target = "personName")
    PositionFullfillmentDTO toDto(PositionFullfillment positionFullfillment); 

    @Mapping(source = "positionId", target = "position")

    @Mapping(source = "personId", target = "person")
    PositionFullfillment toEntity(PositionFullfillmentDTO positionFullfillmentDTO); 

    default PositionFullfillment fromId(Integer id) {
        if (id == null) {
            return null;
        }
        PositionFullfillment positionFullfillment = new PositionFullfillment();
        positionFullfillment.setIdPositionFullfillment(id);
        return positionFullfillment;
    }
}
