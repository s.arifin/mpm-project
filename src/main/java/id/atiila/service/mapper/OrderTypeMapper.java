package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.OrderTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity OrderType and its DTO OrderTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OrderTypeMapper extends EntityMapper <OrderTypeDTO, OrderType> {
    
    

    default OrderType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        OrderType orderType = new OrderType();
        orderType.setIdOrderType(id);
        return orderType;
    }
}
