package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.DealerClaimTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity DealerClaimType and its DTO DealerClaimTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DealerClaimTypeMapper extends EntityMapper <DealerClaimTypeDTO, DealerClaimType> {
    
    

    default DealerClaimType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        DealerClaimType dealerClaimType = new DealerClaimType();
        dealerClaimType.setIdClaimType(id);
        return dealerClaimType;
    }
}
