package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.MotorDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity Motor and its DTO MotorDTO.
 */
@Mapper(componentModel = "spring", uses = {UomMapper.class, FeatureMapper.class, ProductCategoryMapper.class, })
public interface MotorMapper extends EntityMapper <MotorDTO, Motor> {

    @Mapping(source = "uom.idUom", target = "uomId")
    @Mapping(source = "uom.description", target = "uomDescription")
    MotorDTO toDto(Motor motor); 

    @Mapping(source = "uomId", target = "uom")
    Motor toEntity(MotorDTO motorDTO); 

    default Motor fromId(String id) {
        if (id == null) {
            return null;
        }
        Motor motor = new Motor();
        motor.setIdProduct(id);
        return motor;
    }
}
