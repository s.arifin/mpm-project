package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProspectOrganizationDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ProspectOrganization and its DTO ProspectOrganizationDTO.
 */
@Mapper(componentModel = "spring", uses = {OrganizationMapper.class, InternalMapper.class,
        SalesBrokerMapper.class, ProspectSourceMapper.class,
        EventTypeMapper.class, FacilityMapper.class, SalesmanMapper.class, PersonMapper.class, PostalAddressMapper.class})
public interface ProspectOrganizationMapper extends EntityMapper <ProspectOrganizationDTO, ProspectOrganization> {

//    @Mapping(source = "organization.idParty", target = "organizationId")
//    @Mapping(source = "organization.name", target = "organizationName")
    @Mapping(source = "dealer.idInternal", target = "dealerId")
    @Mapping(source = "dealer.organization.name", target = "dealerName")
    @Mapping(source = "broker.idPartyRole", target = "brokerId")
    @Mapping(source = "prospectSource.idProspectSource", target = "prospectSourceId")
    @Mapping(source = "eventType.idEventType", target = "eventTypeId")
    @Mapping(source = "facility.idFacility", target = "facilityId")
    @Mapping(source = "salesman.idPartyRole", target = "salesmanId")
    @Mapping(source = "idSuspect", target = "suspectId")
    ProspectOrganizationDTO toDto(ProspectOrganization prospectOrganization);

//    @Mapping(source = "organizationId", target = "organization")
    @Mapping(source = "dealerId", target = "dealer")
    @Mapping(source = "brokerId", target = "broker")
    @Mapping(source = "prospectSourceId", target = "prospectSource")
    @Mapping(source = "eventTypeId", target = "eventType")
    @Mapping(source = "facilityId", target = "facility")
    @Mapping(source = "salesmanId", target = "salesman")
    @Mapping(source = "suspectId", target = "idSuspect")
    ProspectOrganization toEntity(ProspectOrganizationDTO prospectOrganizationDTO);

    default ProspectOrganization fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ProspectOrganization prospectOrganization = new ProspectOrganization();
        prospectOrganization.setIdProspect(id);
        return prospectOrganization;
    }
}
