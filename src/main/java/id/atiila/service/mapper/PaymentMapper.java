package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PaymentDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Payment and its DTO PaymentDTO.
 */
@Mapper(componentModel = "spring", uses = {PaymentTypeMapper.class, PaymentMethodMapper.class, InternalMapper.class, BillToMapper.class})
public interface PaymentMapper extends EntityMapper<PaymentDTO, Payment> {

    @Mapping(source = "paymentType.idPaymentType", target = "paymentTypeId")
    @Mapping(source = "method.idPaymentMethod", target = "methodId")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "paidTo.idBillTo", target = "paidToId")
    @Mapping(source = "paidFrom.idBillTo", target = "paidFromId")
    PaymentDTO toDto(Payment payment);

    @Mapping(target = "details", ignore = true)
    @Mapping(source = "paymentTypeId", target = "paymentType")
    @Mapping(source = "methodId", target = "method")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "paidToId", target = "paidTo")
    @Mapping(source = "paidFromId", target = "paidFrom")
    Payment toEntity(PaymentDTO paymentDTO);

    default Payment fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Payment payment = new Payment();
        payment.setIdPayment(id);
        return payment;
    }
}
