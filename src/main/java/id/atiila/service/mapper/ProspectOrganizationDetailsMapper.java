package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProspectOrganizationDetailsDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ProspectOrganizationDetails and its DTO ProspectOrganizationDetailsDTO.
 */
@Mapper(componentModel = "spring", uses = {PersonMapper.class, MotorMapper.class, FeatureMapper.class, ProspectOrganizationMapper.class})
public interface ProspectOrganizationDetailsMapper extends EntityMapper <ProspectOrganizationDetailsDTO, ProspectOrganizationDetails> {

//    @Mapping(source = "person.idParty", target = "idpersonowner")
//    @Mapping(source = "person.name", target = "personName")

    @Mapping(source = "product.idProduct", target = "productId")
    @Mapping(source = "product.name", target = "productName")

    @Mapping(source = "color.idFeature", target = "colorId")
    @Mapping(source = "color.description", target = "colorDescription")

    @Mapping(source = "prospectOrganization.idProspect", target = "idProspect")
    ProspectOrganizationDetailsDTO toDto(ProspectOrganizationDetails prospectOrganizationDetails);

//    @Mapping(source = "idpersonowner", target = "person")

    @Mapping(source = "productId", target = "product")

    @Mapping(source = "colorId", target = "color")

    @Mapping(source = "idProspect", target = "prospectOrganization")
    ProspectOrganizationDetails toEntity(ProspectOrganizationDetailsDTO prospectOrganizationDetailsDTO);

    default ProspectOrganizationDetails fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ProspectOrganizationDetails prospectOrganizationDetails = new ProspectOrganizationDetails();
        prospectOrganizationDetails.setIdProspectOrganizationDetail(id);
        return prospectOrganizationDetails;
    }
}
