package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RoleTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity RoleType and its DTO RoleTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RoleTypeMapper extends EntityMapper <RoleTypeDTO, RoleType> {
    
    

    default RoleType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        RoleType roleType = new RoleType();
        roleType.setIdRoleType(id);
        return roleType;
    }
}
