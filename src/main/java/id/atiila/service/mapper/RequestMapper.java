package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RequestDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Request and its DTO RequestDTO.
 */
@Mapper(componentModel = "spring", uses = {RequestTypeMapper.class, InternalMapper.class})
public interface RequestMapper extends EntityMapper<RequestDTO, Request> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    @Mapping(source = "requestType.idRequestType", target = "requestTypeId")
    @Mapping(source = "requestType.description", target = "requestTypeDescription")
    RequestDTO toDto(Request request);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "requestTypeId", target = "requestType")
    @Mapping(target = "details", ignore = true)
    @Mapping(target = "statuses", ignore = true)
    @Mapping(target = "roles", ignore = true)
    Request toEntity(RequestDTO requestDTO);

    default Request fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Request request = new Request();
        request.setIdRequest(id);
        return request;
    }
}
