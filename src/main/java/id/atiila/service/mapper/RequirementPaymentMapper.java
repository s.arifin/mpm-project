package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RequirementPaymentDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity RequirementPayment and its DTO RequirementPaymentDTO.
 */
@Mapper(componentModel = "spring", uses = {RequirementMapper.class, PaymentMapper.class})
public interface RequirementPaymentMapper extends EntityMapper<RequirementPaymentDTO, RequirementPayment> {

    @Mapping(source = "requirement.idRequirement", target = "requirementId")
    @Mapping(source = "payment.idPayment", target = "paymentId")
    RequirementPaymentDTO toDto(RequirementPayment requirementPayment);

    @Mapping(source = "requirementId", target = "requirement")
    @Mapping(source = "paymentId", target = "payment")
    RequirementPayment toEntity(RequirementPaymentDTO requirementPaymentDTO);

    default RequirementPayment fromId(UUID id) {
        if (id == null) {
            return null;
        }
        RequirementPayment requirementPayment = new RequirementPayment();
        requirementPayment.setIdReqPayment(id);
        return requirementPayment;
    }
}
