package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.InternalOrderDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity InternalOrder and its DTO InternalOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, PaymentApplicationMapper.class, })
public interface InternalOrderMapper extends EntityMapper <InternalOrderDTO, InternalOrder> {

    @Mapping(source = "internalFrom.idInternal", target = "internalFromId")
    @Mapping(source = "internalFrom.idInternal", target = "internalFromIdInternal")

    @Mapping(source = "internalTo.idInternal", target = "internalToId")
    @Mapping(source = "internalTo.idInternal", target = "internalToIdInternal")
    InternalOrderDTO toDto(InternalOrder internalOrder);

    @Mapping(source = "internalFromId", target = "internalFrom")

    @Mapping(source = "internalToId", target = "internalTo")
    InternalOrder toEntity(InternalOrderDTO internalOrderDTO);

    default InternalOrder fromId(UUID id) {
        if (id == null) {
            return null;
        }
        InternalOrder internalOrder = new InternalOrder();
        internalOrder.setIdOrder(id);
        return internalOrder;
    }
}
