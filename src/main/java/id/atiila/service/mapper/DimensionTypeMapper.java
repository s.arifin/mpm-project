package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.DimensionTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity DimensionType and its DTO DimensionTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DimensionTypeMapper extends EntityMapper<DimensionTypeDTO, DimensionType> {



    default DimensionType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        DimensionType dimensionType = new DimensionType();
        dimensionType.setIdDimensionType(id);
        return dimensionType;
    }
}
