package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.MemosDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Memos and its DTO MemosDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, BillingMapper.class, MemoTypeMapper.class, InventoryItemMapper.class })
public interface MemosMapper extends EntityMapper <MemosDTO, Memos> {

    @Mapping(source = "dealer.idInternal", target = "dealerId")
    @Mapping(source = "dealer.organization.name", target = "dealerName")
    @Mapping(source = "billing.idBilling", target = "billingId")
    @Mapping(source = "memoType.idMemoType", target = "memoTypeId")
    @Mapping(source = "memoType.description", target = "memoTypeDescription")
    @Mapping(source = "inventoryItem.idInventoryItem", target = "inventoryItemId")
    @Mapping(source = "oldInventoryItem.idInventoryItem", target = "oldInventoryItemId")
    @Mapping(source = "billing.billingNumber", target ="billingIdbilling")
    MemosDTO toDto(Memos memos);

    @Mapping(source = "dealerId", target = "dealer")
    @Mapping(source = "billingId", target = "billing")
    @Mapping(source = "memoTypeId", target = "memoType")
    @Mapping(source = "inventoryItemId", target = "inventoryItem")
    @Mapping(source = "oldInventoryItemId", target = "oldInventoryItem")
    Memos toEntity(MemosDTO memosDTO);

    default Memos fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Memos memos = new Memos();
        memos.setIdMemo(id);
        return memos;
    }
}
