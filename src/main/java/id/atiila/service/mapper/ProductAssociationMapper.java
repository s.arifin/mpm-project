package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProductAssociationDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ProductAssociation and its DTO ProductAssociationDTO.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class, })
public interface ProductAssociationMapper extends EntityMapper <ProductAssociationDTO, ProductAssociation> {

    @Mapping(source = "productTo.idProduct", target = "productToId")
    @Mapping(source = "productTo.name", target = "productToName")

    @Mapping(source = "productFrom.idProduct", target = "productFromId")
    @Mapping(source = "productFrom.name", target = "productFromName")
    ProductAssociationDTO toDto(ProductAssociation productAssociation); 

    @Mapping(source = "productToId", target = "productTo")

    @Mapping(source = "productFromId", target = "productFrom")
    ProductAssociation toEntity(ProductAssociationDTO productAssociationDTO); 

    default ProductAssociation fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ProductAssociation productAssociation = new ProductAssociation();
        productAssociation.setIdProductAssociation(id);
        return productAssociation;
    }
}
