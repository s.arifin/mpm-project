package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.repository.RequirementRepository;
import id.atiila.service.dto.RequirementDTO;
import id.atiila.service.dto.WorkProductRequirementDTO;
import id.atiila.service.dto.WorkRequirementDTO;
import id.atiila.service.dto.WorkServiceRequirementDTO;
import org.mapstruct.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

/**
 * Mapper for the entity Requirement and its DTO RequirementDTO.
 */
@Mapper(componentModel = "spring", uses = {PaymentApplicationMapper.class, FacilityMapper.class,
    WorkOrderMapper.class, WorkProductRequirementMapper.class, WorkServiceRequirementMapper.class, RequirementTypeMapper.class })
public abstract class RequirementMapper {

    private final Logger log = LoggerFactory.getLogger(RequirementMapper.class);

    @Autowired
    private RequirementRepository repo;

    public abstract WorkProductRequirementDTO toDto(WorkProductRequirement e);

    public abstract WorkProductRequirement toEntity(WorkProductRequirementDTO d);

    public abstract WorkRequirementDTO toDto(WorkRequirement e);

    public abstract WorkRequirement toEntity(WorkRequirementDTO e);

//    @Mapping(source = "facility.idFacility", target = "facilityId")
//    @Mapping(source = "facility.description", target = "facilityDescription")
//    @Mapping(source = "parent.idRequirement", target = "parentId")
//    @Mapping(source = "parent.description", target = "parentDescription")
//    @Mapping(source = "requirementType.idRequirementType", target = "idReqTyp")
    public RequirementDTO toDto(Requirement e) {
        if (e instanceof WorkProductRequirement) return toDto((WorkProductRequirement) e);
        else if (e instanceof WorkRequirement) return toDto((WorkRequirement) e);
        else if (e  == null) return null;
        else {
            RequirementDTO r = new RequirementDTO();
            r.setRequirementNumber(e.getRequirementNumber());
            if (e.getRequirementType() != null)r.setIdReqTyp(e.getRequirementType().getIdRequirementType());
            r.setIdRequirement(e.getIdRequirement());
            r.setBudget(e.getBudget());
            r.setCurrentStatus(e.getCurrentStatus());
            r.setDateCreate(e.getDateCreate());
            if (e.getFacility() != null) {
                r.setFacilityId(e.getFacility().getIdFacility());
                r.setFacilityDescription(e.getFacility().getDescription());
            }
            if (e.getParent() != null) {
                r.setParentId(e.getParent().getIdRequirement());
                r.setParentDescription(e.getParent().getDescription());
            }
            return r;
        }
    };

//    @Mapping(target = "orderItems", ignore = true)
//    @Mapping(source = "facilityId", target = "facility")
//    @Mapping(target = "items", ignore = true)
//    @Mapping(source = "parentId", target = "parent")
//    @Mapping(source = "idReqTyp", target ="requirementType")
    public Requirement toEntity(RequirementDTO d) {
        if (d instanceof WorkProductRequirementDTO) return toEntity((WorkProductRequirementDTO) d);
        else if (d instanceof WorkRequirementDTO) return toEntity((WorkRequirementDTO) d);
        else if (d  == null) return null;
        else {
            return null;
        }
    };

    public Requirement fromId(UUID id) {
        if (id == null) {
            return null;
        }
        return repo.findOne(id);
    }
}
