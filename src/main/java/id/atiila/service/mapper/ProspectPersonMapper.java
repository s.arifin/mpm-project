package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProspectPersonDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ProspectPerson and its DTO ProspectPersonDTO.
 */
@Mapper(componentModel = "spring", uses = {PersonMapper.class, InternalMapper.class, SalesBrokerMapper.class,
    ProspectSourceMapper.class, EventTypeMapper.class, FacilityMapper.class, SalesmanMapper.class})
public interface ProspectPersonMapper extends EntityMapper <ProspectPersonDTO, ProspectPerson> {
    @Mapping(source = "dealer.idInternal", target = "dealerId")
    @Mapping(source = "dealer.organization.name", target = "dealerName")
    @Mapping(source = "broker.idPartyRole", target = "brokerId")
    @Mapping(source = "broker.party.name", target = "brokerName")
    @Mapping(source = "prospectSource.idProspectSource", target = "prospectSourceId")
    @Mapping(source = "prospectSource.description", target = "prospectSourceDescription")
    @Mapping(source = "eventType.idEventType", target = "eventTypeId")
    @Mapping(source = "facility.idFacility", target = "facilityId")
    @Mapping(source = "salesman.idPartyRole", target = "salesmanId")
    @Mapping(source = "idSuspect", target = "suspectId")
    ProspectPersonDTO toDto(ProspectPerson prospectPerson);

    @Mapping(source = "dealerId", target = "dealer")
    @Mapping(source = "brokerId", target = "broker")
    @Mapping(source = "prospectSourceId", target = "prospectSource")
    @Mapping(source = "eventTypeId", target = "eventType")
    @Mapping(source = "facilityId", target = "facility")
    @Mapping(source = "salesmanId", target = "salesman")
    @Mapping(source = "suspectId", target = "idSuspect")
    ProspectPerson toEntity(ProspectPersonDTO prospectPersonDTO);

    default ProspectPerson fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ProspectPerson prospectPerson = new ProspectPerson();
        prospectPerson.setIdProspect(id);
        return prospectPerson;
    }
}
