package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.InternalFacilityPurposeDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity InternalFacilityPurpose and its DTO InternalFacilityPurposeDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, FacilityMapper.class, PurposeTypeMapper.class})
public interface InternalFacilityPurposeMapper extends EntityMapper<InternalFacilityPurposeDTO, InternalFacilityPurpose> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    @Mapping(source = "facility.idFacility", target = "facilityId")
    @Mapping(source = "facility.description", target = "facilityDescription")
    @Mapping(source = "purposeType.idPurposeType", target = "purposeTypeId")
    @Mapping(source = "purposeType.description", target = "purposeTypeDescription")
    InternalFacilityPurposeDTO toDto(InternalFacilityPurpose internalFacilityPurpose);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "facilityId", target = "facility")
    @Mapping(source = "purposeTypeId", target = "purposeType")
    InternalFacilityPurpose toEntity(InternalFacilityPurposeDTO internalFacilityPurposeDTO);

    default InternalFacilityPurpose fromId(UUID id) {
        if (id == null) {
            return null;
        }
        InternalFacilityPurpose internalFacilityPurpose = new InternalFacilityPurpose();
        internalFacilityPurpose.setIdPartyFacility(id);
        return internalFacilityPurpose;
    }
}
