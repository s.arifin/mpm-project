package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.GoodContainerDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity GoodContainer and its DTO GoodContainerDTO.
 */
@Mapper(componentModel = "spring", uses = {GoodMapper.class, ContainerMapper.class, OrganizationMapper.class})
public interface GoodContainerMapper extends EntityMapper<GoodContainerDTO, GoodContainer> {

    @Mapping(source = "good.idProduct", target = "goodId")
    @Mapping(source = "good.name", target = "goodName")
    @Mapping(source = "container.idContainer", target = "containerId")
    @Mapping(source = "container.containerCode", target = "containerContainerCode")
    @Mapping(source = "organization.idParty", target = "organizationId")
    GoodContainerDTO toDto(GoodContainer goodContainer);

    @Mapping(source = "goodId", target = "good")
    @Mapping(source = "containerId", target = "container")
    @Mapping(source = "organizationId", target = "organization")
    GoodContainer toEntity(GoodContainerDTO goodContainerDTO);

    default GoodContainer fromId(UUID id) {
        if (id == null) {
            return null;
        }
        GoodContainer goodContainer = new GoodContainer();
        goodContainer.setIdGoodContainer(id);
        return goodContainer;
    }
}
