package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PartyCategoryDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity PartyCategory and its DTO PartyCategoryDTO.
 */
@Mapper(componentModel = "spring", uses = {PartyCategoryTypeMapper.class})
public interface PartyCategoryMapper extends EntityMapper<PartyCategoryDTO, PartyCategory> {

    @Mapping(source = "categoryType.idCategoryType", target = "categoryTypeId")
    @Mapping(source = "categoryType.description", target = "categoryTypeDescription")
    PartyCategoryDTO toDto(PartyCategory partyCategory);

    @Mapping(source = "categoryTypeId", target = "categoryType")
    PartyCategory toEntity(PartyCategoryDTO partyCategoryDTO);

    default PartyCategory fromId(Integer id) {
        if (id == null) {
            return null;
        }
        PartyCategory partyCategory = new PartyCategory();
        partyCategory.setIdCategory(id);
        return partyCategory;
    }
}
