package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.CustomerRequestItemDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity CustomerRequestItem and its DTO CustomerRequestItemDTO.
 */
@Mapper(componentModel = "spring", uses = {RequestMapper.class, ProductMapper.class, FeatureMapper.class, CustomerMapper.class})
public interface CustomerRequestItemMapper extends EntityMapper<CustomerRequestItemDTO, CustomerRequestItem> {

    @Mapping(source = "request.idRequest", target = "requestId")
    @Mapping(source = "request.currentStatus", target = "currentStatus")
    @Mapping(source = "product.idProduct", target = "idProduct")
    @Mapping(source = "product.name", target = "productName")
    @Mapping(source = "feature.idFeature", target = "idFeature")
    @Mapping(source = "feature.refKey", target = "idColor")
    @Mapping(source = "feature.description", target = "featureName")
    @Mapping(source = "customer.idCustomer", target = "customerId")
    @Mapping(source = "customer.party.name", target = "customerName")
    CustomerRequestItemDTO toDto(CustomerRequestItem customerRequestItem);

    @Mapping(source = "requestId", target = "request")
    @Mapping(source = "idProduct", target = "product")
    @Mapping(source = "idFeature", target = "feature")
    @Mapping(source = "customerId", target = "customer")
    CustomerRequestItem toEntity(CustomerRequestItemDTO customerRequestItemDTO);

    default CustomerRequestItem fromId(UUID id) {
        if (id == null) {
            return null;
        }
        CustomerRequestItem customerRequestItem = new CustomerRequestItem();
        customerRequestItem.setIdRequestItem(id);
        return customerRequestItem;
    }

}
