package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.BillToDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity BillTo and its DTO BillToDTO.
 */
@Mapper(componentModel = "spring", uses = {PartyMapper.class})
public interface BillToMapper extends EntityMapper<BillToDTO, BillTo> {

    @Mapping(source = "party.idParty", target = "partyId")
    @Mapping(source = "party.name", target = "partyName")
    BillToDTO toDto(BillTo billTo);

    @Mapping(source = "partyId", target = "party")
    BillTo toEntity(BillToDTO billToDTO);

    default BillTo fromId(String id) {
        if (id == null) {
            return null;
        }
        BillTo billTo = new BillTo();
        billTo.setIdBillTo(id);
        return billTo;
    }
}
