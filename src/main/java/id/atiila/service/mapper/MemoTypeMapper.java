package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.MemoTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity MemoType and its DTO MemoTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MemoTypeMapper extends EntityMapper<MemoTypeDTO, MemoType> {

    

    

    default MemoType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        MemoType memoType = new MemoType();
        memoType.setIdMemoType(id);
        return memoType;
    }
}
