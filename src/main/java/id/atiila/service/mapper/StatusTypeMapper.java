package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.StatusTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity StatusType and its DTO StatusTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface StatusTypeMapper extends EntityMapper <StatusTypeDTO, StatusType> {
    
    

    default StatusType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        StatusType statusType = new StatusType();
        statusType.setIdStatusType(id);
        return statusType;
    }
}
