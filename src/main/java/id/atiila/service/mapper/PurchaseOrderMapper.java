package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PurchaseOrderDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity PurchaseOrder and its DTO PurchaseOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {VendorMapper.class, InternalMapper.class, BillToMapper.class})
public interface PurchaseOrderMapper extends EntityMapper<PurchaseOrderDTO, PurchaseOrder> {

    @Mapping(source = "vendor.idVendor", target = "vendorId")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    PurchaseOrderDTO toDto(PurchaseOrder purchaseOrder);

    @Mapping(source = "vendorId", target = "vendor")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "billToId", target = "billTo")
    PurchaseOrder toEntity(PurchaseOrderDTO purchaseOrderDTO);

    default PurchaseOrder fromId(UUID id) {
        if (id == null) {
            return null;
        }
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setIdOrder(id);
        return purchaseOrder;
    }
}
