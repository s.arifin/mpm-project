package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ShipmentBillingItemDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ShipmentBillingItem and its DTO ShipmentBillingItemDTO.
 */
@Mapper(componentModel = "spring", uses = {ShipmentItemMapper.class, BillingItemMapper.class})
public interface ShipmentBillingItemMapper extends EntityMapper<ShipmentBillingItemDTO, ShipmentBillingItem> {

    @Mapping(source = "shipmentItem.idShipmentItem", target = "shipmentItemId")
    @Mapping(source = "billingItem.idBillingItem", target = "billingItemId")
    ShipmentBillingItemDTO toDto(ShipmentBillingItem shipmentBillingItem);

    @Mapping(source = "shipmentItemId", target = "shipmentItem")
    @Mapping(source = "billingItemId", target = "billingItem")
    ShipmentBillingItem toEntity(ShipmentBillingItemDTO shipmentBillingItemDTO);

    default ShipmentBillingItem fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ShipmentBillingItem shipmentBillingItem = new ShipmentBillingItem();
        shipmentBillingItem.setIdShipmentBillingItem(id);
        return shipmentBillingItem;
    }
}
