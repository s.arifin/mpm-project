package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.BillingDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Billing and its DTO BillingDTO.
 */
@Mapper(componentModel = "spring", uses = {BillingTypeMapper.class, InternalMapper.class, BillToMapper.class})
public interface BillingMapper extends EntityMapper<BillingDTO, Billing> {

    @Mapping(source = "billingType.idBillingType", target = "billingTypeId")
    @Mapping(source = "billingType.description", target = "billingTypeDescription")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    @Mapping(source = "billFrom.idBillTo", target = "billFromId")
    BillingDTO toDto(Billing billing);

    @Mapping(target = "details", ignore = true)
    @Mapping(source = "billingTypeId", target = "billingType")
    @Mapping(target = "payments", ignore = true)
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "billToId", target = "billTo")
    @Mapping(source = "billFromId", target = "billFrom")
    Billing toEntity(BillingDTO billingDTO);

    default Billing fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Billing billing = new Billing();
        billing.setIdBilling(id);
        return billing;
    }
}
