package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RuleHotItemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RuleHotItem and its DTO RuleHotItemDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RuleHotItemMapper extends EntityMapper <RuleHotItemDTO, RuleHotItem> {


    default RuleHotItem fromId(Integer id) {
        if (id == null) {
            return null;
        }
        RuleHotItem ruleHotItem = new RuleHotItem();
        ruleHotItem.setIdRule(id);
        return ruleHotItem;
    }
}
