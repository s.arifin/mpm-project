package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProspectDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Prospect and its DTO ProspectDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class,
    SalesBrokerMapper.class, ProspectSourceMapper.class, EventTypeMapper.class,
    FacilityMapper.class, SalesmanMapper.class})
public interface ProspectMapper extends EntityMapper <ProspectDTO, Prospect> {

    @Mapping(source = "dealer.idInternal", target = "dealerId")
    @Mapping(source = "dealer.organization.name", target = "dealerName")
    @Mapping(source = "broker.idPartyRole", target = "brokerId")
    @Mapping(source = "prospectSource.idProspectSource", target = "prospectSourceId")
    @Mapping(source = "prospectSource.description", target = "prospectSourceDescription")
    @Mapping(source = "eventType.idEventType", target = "eventTypeId")
    @Mapping(source = "facility.idFacility", target = "facilityId")
    @Mapping(source = "salesman.idPartyRole", target = "salesmanId")
    @Mapping(source = "salesman.person.name", target = "salesmanName")
    @Mapping(source = "idSuspect", target = "suspectId")
    ProspectDTO toDto(Prospect prospect);

    @Mapping(source = "dealerId", target = "dealer")
    @Mapping(source = "brokerId", target = "broker")
    @Mapping(source = "prospectSourceId", target = "prospectSource")
    @Mapping(source = "eventTypeId", target = "eventType")
    @Mapping(source = "facilityId", target = "facility")
    @Mapping(source = "salesmanId", target = "salesman")
    @Mapping(source = "suspectId", target = "idSuspect")
    Prospect toEntity(ProspectDTO prospectDTO);

    default Prospect fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Prospect prospect = new Prospect();
        prospect.setIdProspect(id);
        return prospect;
    }
}
