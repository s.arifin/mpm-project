package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ReasonTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity ReasonType and its DTO ReasonTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ReasonTypeMapper extends EntityMapper <ReasonTypeDTO, ReasonType> {
    
    

    default ReasonType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        ReasonType reasonType = new ReasonType();
        reasonType.setIdReason(id);
        return reasonType;
    }
}
