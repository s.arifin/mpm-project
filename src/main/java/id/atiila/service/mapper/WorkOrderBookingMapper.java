package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.*;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity WorkOrderBooking and its DTO WorkOrderBookingDTO.
 */
@Mapper(componentModel = "spring", uses = {PersonalCustomerMapper.class,
    VehicleMapper.class, BookingSlotMapper.class,
    BookingTypeMapper.class, EventTypeMapper.class, InternalMapper.class,
    WorkServiceRequirementMapper.class, WorkProductRequirementMapper.class, })
public interface WorkOrderBookingMapper extends EntityMapper <WorkOrderBookingDTO, WorkOrderBooking> {

    @Mapping(source = "slot.idBookSlot", target = "slotId")
    @Mapping(source = "slot.slotNumber", target = "slotSlotNumber")
    @Mapping(source = "bookingType.idBookingType", target = "bookingTypeId")
    @Mapping(source = "bookingType.description", target = "bookingTypeDescription")
    @Mapping(source = "eventType.idEventType", target = "eventTypeId")
    @Mapping(source = "eventType.description", target = "eventTypeDescription")
    @Mapping(source = "internal.idInternal", target = "internalId")
    WorkOrderBookingDTO toDto(WorkOrderBooking workOrderBooking);

    @Mapping(source = "slotId", target = "slot")
    @Mapping(source = "bookingTypeId", target = "bookingType")
    @Mapping(source = "eventTypeId", target = "eventType")
    @Mapping(source = "internalId", target = "internal")
    WorkOrderBooking toEntity(WorkOrderBookingDTO workOrderBookingDTO);

    default WorkOrderBooking fromId(UUID id) {
        if (id == null) {
            return null;
        }
        WorkOrderBooking workOrderBooking = new WorkOrderBooking();
        workOrderBooking.setIdBooking(id);
        return workOrderBooking;
    }
}
