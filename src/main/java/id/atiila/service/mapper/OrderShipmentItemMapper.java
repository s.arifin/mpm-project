package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.OrderShipmentItemDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity OrderShipmentItem and its DTO OrderShipmentItemDTO.
 */
@Mapper(componentModel = "spring", uses = {OrderItemMapper.class, ShipmentItemMapper.class})
public interface OrderShipmentItemMapper extends EntityMapper<OrderShipmentItemDTO, OrderShipmentItem> {

    @Mapping(source = "orderItem.idOrderItem", target = "orderItemId")
    @Mapping(source = "shipmentItem.idShipmentItem", target = "shipmentItemId")
    OrderShipmentItemDTO toDto(OrderShipmentItem orderShipmentItem);

    @Mapping(source = "orderItemId", target = "orderItem")
    @Mapping(source = "shipmentItemId", target = "shipmentItem")
    OrderShipmentItem toEntity(OrderShipmentItemDTO orderShipmentItemDTO);

    default OrderShipmentItem fromId(UUID id) {
        if (id == null) {
            return null;
        }
        OrderShipmentItem orderShipmentItem = new OrderShipmentItem();
        orderShipmentItem.setIdOrderShipmentItem(id);
        return orderShipmentItem;
    }
}
