package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.AssociationTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity AssociationType and its DTO AssociationTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AssociationTypeMapper extends EntityMapper <AssociationTypeDTO, AssociationType> {
    
    

    default AssociationType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        AssociationType associationType = new AssociationType();
        associationType.setIdAssociattionType(id);
        return associationType;
    }
}
