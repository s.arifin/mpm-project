package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RuleTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity RuleType and its DTO RuleTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RuleTypeMapper extends EntityMapper <RuleTypeDTO, RuleType> {
    
    

    default RuleType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        RuleType ruleType = new RuleType();
        ruleType.setIdRuleType(id);
        return ruleType;
    }
}
