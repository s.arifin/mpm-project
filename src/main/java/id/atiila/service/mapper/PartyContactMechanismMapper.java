package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.repository.PartyContactMechanismRepository;
import id.atiila.service.dto.PartyContactMechanismDTO;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

/**
 * Mapper for the entity PartyContactMechanism and its DTO PartyContactMechanismDTO.
 */
@Mapper(componentModel = "spring", uses = {PartyMapper.class, ContactMechanismMapper.class, })
public abstract class PartyContactMechanismMapper {

    @Autowired
    private PartyContactMechanismRepository repo;

    @Mapping(source = "party.idParty", target = "partyId")
    @Mapping(source = "party.name", target = "partyName")
    @Mapping(source = "contact.idContact", target = "contactId")
    public abstract PartyContactMechanismDTO toDto(PartyContactMechanism PartyContactMechanism);

    @Mapping(source = "partyId", target = "party")
    @Mapping(source = "contactId", target = "contact")
    public abstract PartyContactMechanism toEntity(PartyContactMechanismDTO PartyContactMechanismDTO);

    public PartyContactMechanism fromId(Long id) {
        if (id == null) {
            return null;
        }
        return repo.findOne(id);
    }
}
