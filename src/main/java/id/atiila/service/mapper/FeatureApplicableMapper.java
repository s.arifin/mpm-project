package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.FeatureApplicableDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity FeatureApplicable and its DTO FeatureApplicableDTO.
 */
@Mapper(componentModel = "spring", uses = {FeatureTypeMapper.class, FeatureMapper.class, ProductMapper.class})
public interface FeatureApplicableMapper extends EntityMapper<FeatureApplicableDTO, FeatureApplicable> {

    @Mapping(source = "featureType.idFeatureType", target = "featureTypeId")
    @Mapping(source = "featureType.description", target = "featureTypeDescription")
    @Mapping(source = "feature.idFeature", target = "featureId")
    @Mapping(source = "feature.refKey", target = "featureRefKey")
    @Mapping(source = "feature.description", target = "featureDescription")
    @Mapping(source = "product.idProduct", target = "productId")
    @Mapping(source = "product.name", target = "productName")
    FeatureApplicableDTO toDto(FeatureApplicable featureApplicable);

    @Mapping(source = "featureTypeId", target = "featureType")
    @Mapping(source = "featureId", target = "feature")
    @Mapping(source = "productId", target = "product")
    FeatureApplicable toEntity(FeatureApplicableDTO featureApplicableDTO);

    default FeatureApplicable fromId(UUID id) {
        if (id == null) {
            return null;
        }
        FeatureApplicable featureApplicable = new FeatureApplicable();
        featureApplicable.setIdApplicability(id);
        return featureApplicable;
    }
}
