package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.WorkEffortTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity WorkEffortType and its DTO WorkEffortTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WorkEffortTypeMapper extends EntityMapper <WorkEffortTypeDTO, WorkEffortType> {
    
    

    default WorkEffortType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        WorkEffortType workEffortType = new WorkEffortType();
        workEffortType.setIdWeType(id);
        return workEffortType;
    }
}
