package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.WeServiceTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity WeServiceType and its DTO WeServiceTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {ServiceMapper.class, })
public interface WeServiceTypeMapper extends EntityMapper <WeServiceTypeDTO, WeServiceType> {

    @Mapping(source = "service.idProduct", target = "serviceId")
    @Mapping(source = "service.name", target = "serviceName")
    WeServiceTypeDTO toDto(WeServiceType weServiceType);

    @Mapping(source = "serviceId", target = "service")
    WeServiceType toEntity(WeServiceTypeDTO weServiceTypeDTO);

    default WeServiceType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        WeServiceType weServiceType = new WeServiceType();
        weServiceType.setIdWeType(id);
        return weServiceType;
    }
}
