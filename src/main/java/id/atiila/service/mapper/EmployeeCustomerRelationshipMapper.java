package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.EmployeeCustomerRelationshipDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity EmployeeCustomerRelationship and its DTO EmployeeCustomerRelationshipDTO.
 */
@Mapper(componentModel = "spring", uses = {StatusTypeMapper.class, RelationTypeMapper.class, EmployeeMapper.class, CustomerMapper.class})
public interface EmployeeCustomerRelationshipMapper extends EntityMapper<EmployeeCustomerRelationshipDTO, EmployeeCustomerRelationship> {

    @Mapping(source = "statusType.idStatusType", target = "statusTypeId")
    @Mapping(source = "statusType.description", target = "statusTypeDescription")
    @Mapping(source = "relationType.idRelationType", target = "relationTypeId")
    @Mapping(source = "relationType.description", target = "relationTypeDescription")
    @Mapping(source = "employee.idEmployee", target = "employeeId")
    @Mapping(source = "customer.idCustomer", target = "customerId")
    EmployeeCustomerRelationshipDTO toDto(EmployeeCustomerRelationship employeeCustomerRelationship);

    @Mapping(source = "statusTypeId", target = "statusType")
    @Mapping(source = "relationTypeId", target = "relationType")
    @Mapping(source = "employeeId", target = "employee")
    @Mapping(source = "customerId", target = "customer")
    EmployeeCustomerRelationship toEntity(EmployeeCustomerRelationshipDTO employeeCustomerRelationshipDTO);

    default EmployeeCustomerRelationship fromId(UUID id) {
        if (id == null) {
            return null;
        }
        EmployeeCustomerRelationship employeeCustomerRelationship = new EmployeeCustomerRelationship();
        employeeCustomerRelationship.setIdPartyRelationship(id);
        return employeeCustomerRelationship;
    }
}
