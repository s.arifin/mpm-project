package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.CommunicationEventProspectDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity CommunicationEventProspect and its DTO CommunicationEventProspectDTO.
 */
@Mapper(componentModel = "spring", uses = {EventTypeMapper.class, SaleTypeMapper.class, FeatureMapper.class, MotorMapper.class, CommunicationEventMapper.class, PurposeTypeMapper.class,})
public interface CommunicationEventProspectMapper extends EntityMapper <CommunicationEventProspectDTO, CommunicationEventProspect> {

    @Mapping(source = "saleType.idSaleType", target = "saleTypeId")
    @Mapping(source = "saleType.description", target = "saleTypeDescription")

    @Mapping(source = "color.idFeature", target = "colorId")
    @Mapping(source = "color.description", target = "colorDescription")

    @Mapping(source = "product.idProduct", target = "productId")
    @Mapping(source = "product.name", target = "productName")

    @Mapping(source = "eventType.idEventType", target = "eventTypeId")
    @Mapping(source = "eventType.description", target = "eventTypeDescription")

    @Mapping(source = "purpose.idPurposeType", target = "purposeId")
    @Mapping(source = "purpose.description", target = "purposeDescription")
    CommunicationEventProspectDTO toDto(CommunicationEventProspect communicationEventProspect);

    @Mapping(source = "saleTypeId", target = "saleType")

    @Mapping(source = "colorId", target = "color")

    @Mapping(source = "productId", target = "product")

    @Mapping(source = "eventTypeId", target = "eventType")

    @Mapping(source = "purposeId", target = "purpose")

    CommunicationEventProspect toEntity(CommunicationEventProspectDTO communicationEventProspectDTO);

    default CommunicationEventProspect fromId(UUID id) {
        if (id == null) {
            return null;
        }
        CommunicationEventProspect communicationEventProspect = new CommunicationEventProspect();
        communicationEventProspect.setIdCommunicationEvent(id);
        return communicationEventProspect;
    }
}
