package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.ax.dto.AxCustomerDTO;
import id.atiila.service.dto.CustomerDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity Customer and its DTO CustomerDTO.
 */
@Mapper(componentModel = "spring", uses = {PartyMapper.class})
public interface CustomerMapper extends EntityMapper<CustomerDTO, Customer> {

    @Mapping(source = "party.idParty", target = "partyId")
    @Mapping(source = "party.name", target = "partyName")
    CustomerDTO toDto(Customer customer);

    @Mapping(source = "partyId", target = "party")
    Customer toEntity(CustomerDTO customerDTO);

    default Customer fromId(String id) {
        if (id == null) {
            return null;
        }
        Customer customer = new Customer();
        customer.setIdCustomer(id);
        return customer;
    }
}
