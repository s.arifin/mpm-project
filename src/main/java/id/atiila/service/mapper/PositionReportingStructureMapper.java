package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PositionReportingStructureDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity PositionReportingStructure and its DTO PositionReportingStructureDTO.
 */
@Mapper(componentModel = "spring", uses = {PositionMapper.class, })
public interface PositionReportingStructureMapper extends EntityMapper <PositionReportingStructureDTO, PositionReportingStructure> {

    @Mapping(source = "positionFrom.idPosition", target = "positionFromId")

    @Mapping(source = "positionTo.idPosition", target = "positionToId")
    PositionReportingStructureDTO toDto(PositionReportingStructure positionReportingStructure); 

    @Mapping(source = "positionFromId", target = "positionFrom")

    @Mapping(source = "positionToId", target = "positionTo")
    PositionReportingStructure toEntity(PositionReportingStructureDTO positionReportingStructureDTO); 

    default PositionReportingStructure fromId(Integer id) {
        if (id == null) {
            return null;
        }
        PositionReportingStructure positionReportingStructure = new PositionReportingStructure();
        positionReportingStructure.setIdPositionStructure(id);
        return positionReportingStructure;
    }
}
