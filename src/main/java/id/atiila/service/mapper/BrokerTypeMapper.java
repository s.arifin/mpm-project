package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.BrokerTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity BrokerType and its DTO BrokerTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BrokerTypeMapper extends EntityMapper <BrokerTypeDTO, BrokerType> {
    
    

    default BrokerType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        BrokerType brokerType = new BrokerType();
        brokerType.setIdBrokerType(id);
        return brokerType;
    }
}
