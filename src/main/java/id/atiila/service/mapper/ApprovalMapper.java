package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ApprovalDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Approval and its DTO ApprovalDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ApprovalMapper extends EntityMapper<ApprovalDTO, Approval> {

    

    

    default Approval fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Approval approval = new Approval();
        approval.setIdApproval(id);
        return approval;
    }
}
