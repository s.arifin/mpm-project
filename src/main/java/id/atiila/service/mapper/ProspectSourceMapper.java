package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProspectSourceDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity ProspectSource and its DTO ProspectSourceDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProspectSourceMapper extends EntityMapper <ProspectSourceDTO, ProspectSource> {
    
    

    default ProspectSource fromId(Integer id) {
        if (id == null) {
            return null;
        }
        ProspectSource prospectSource = new ProspectSource();
        prospectSource.setIdProspectSource(id);
        return prospectSource;
    }
}
