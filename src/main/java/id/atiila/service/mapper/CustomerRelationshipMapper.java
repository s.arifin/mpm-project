package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.CustomerRelationshipDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity CustomerRelationship and its DTO CustomerRelationshipDTO.
 */
@Mapper(componentModel = "spring", uses = {StatusTypeMapper.class, RelationTypeMapper.class, InternalMapper.class, CustomerMapper.class})
public interface CustomerRelationshipMapper extends EntityMapper<CustomerRelationshipDTO, CustomerRelationship> {

    @Mapping(source = "statusType.idStatusType", target = "statusTypeId")
    @Mapping(source = "statusType.description", target = "statusTypeDescription")
    @Mapping(source = "relationType.idRelationType", target = "relationTypeId")
    @Mapping(source = "relationType.description", target = "relationTypeDescription")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "customer.idCustomer", target = "customerId")
    CustomerRelationshipDTO toDto(CustomerRelationship customerRelationship);

    @Mapping(source = "statusTypeId", target = "statusType")
    @Mapping(source = "relationTypeId", target = "relationType")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "customerId", target = "customer")
    CustomerRelationship toEntity(CustomerRelationshipDTO customerRelationshipDTO);

    default CustomerRelationship fromId(UUID id) {
        if (id == null) {
            return null;
        }
        CustomerRelationship customerRelationship = new CustomerRelationship();
        customerRelationship.setIdPartyRelationship(id);
        return customerRelationship;
    }
}
