package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.StockOpnameTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity StockOpnameType and its DTO StockOpnameTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface StockOpnameTypeMapper extends EntityMapper<StockOpnameTypeDTO, StockOpnameType> {



    default StockOpnameType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        StockOpnameType stockOpnameType = new StockOpnameType();
        stockOpnameType.setIdStockOpnameType(id);
        return stockOpnameType;
    }
}
