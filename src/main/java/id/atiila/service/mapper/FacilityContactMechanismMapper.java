package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.FacilityContactMechanismDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity FacilityContactMechanism and its DTO FacilityContactMechanismDTO.
 */
@Mapper(componentModel = "spring", uses = {FacilityMapper.class, ContactMechanismMapper.class})
public interface FacilityContactMechanismMapper extends EntityMapper<FacilityContactMechanismDTO, FacilityContactMechanism> {

    @Mapping(source = "facility.idFacility", target = "facilityId")
    @Mapping(source = "facility.description", target = "facilityDescription")
    @Mapping(source = "contact.idContact", target = "contactId")
    @Mapping(source = "contact.description", target = "contactDescription")
    FacilityContactMechanismDTO toDto(FacilityContactMechanism facilityContactMechanism);

    @Mapping(source = "facilityId", target = "facility")
    @Mapping(source = "contactId", target = "contact")
    FacilityContactMechanism toEntity(FacilityContactMechanismDTO facilityContactMechanismDTO);

    default FacilityContactMechanism fromId(UUID id) {
        if (id == null) {
            return null;
        }
        FacilityContactMechanism facilityContactMechanism = new FacilityContactMechanism();
        facilityContactMechanism.setIdContactMechPurpose(id);
        return facilityContactMechanism;
    }
}
