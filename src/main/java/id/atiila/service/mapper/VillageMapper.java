package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.VillageDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Village and its DTO VillageDTO.
 */
@Mapper(componentModel = "spring", uses = {GeoBoundaryTypeMapper.class, GeoBoundaryMapper.class})
public interface VillageMapper extends EntityMapper <VillageDTO, Village> {

    default Village fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Village village = new Village();
        village.setIdGeobou(id);
        return village;
    }
}
