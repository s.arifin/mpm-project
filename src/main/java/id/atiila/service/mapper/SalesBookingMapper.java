package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.SalesBookingDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity SalesBooking and its DTO SalesBookingDTO.
 */
@Mapper(componentModel = "spring", uses = {RequirementMapper.class})
public interface SalesBookingMapper extends EntityMapper<SalesBookingDTO, SalesBooking> {

    @Mapping(source = "requirement.idRequirement", target = "requirementId")
    SalesBookingDTO toDto(SalesBooking salesBooking);

    @Mapping(source = "requirementId", target = "requirement")
    SalesBooking toEntity(SalesBookingDTO salesBookingDTO);

    default SalesBooking fromId(UUID id) {
        if (id == null) {
            return null;
        }
        SalesBooking salesBooking = new SalesBooking();
        salesBooking.setIdSalesBooking(id);
        return salesBooking;
    }
}
