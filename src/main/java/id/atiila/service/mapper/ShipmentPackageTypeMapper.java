package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ShipmentPackageTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity ShipmentPackageType and its DTO ShipmentPackageTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ShipmentPackageTypeMapper extends EntityMapper <ShipmentPackageTypeDTO, ShipmentPackageType> {
    
    

    default ShipmentPackageType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        ShipmentPackageType shipmentPackageType = new ShipmentPackageType();
        shipmentPackageType.setIdPackageType(id);
        return shipmentPackageType;
    }
}
