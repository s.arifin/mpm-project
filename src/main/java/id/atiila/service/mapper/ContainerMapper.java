package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ContainerDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Container and its DTO ContainerDTO.
 */
@Mapper(componentModel = "spring", uses = {FacilityMapper.class, ContainerTypeMapper.class})
public interface ContainerMapper extends EntityMapper<ContainerDTO, Container> {

    @Mapping(source = "facility.idFacility", target = "facilityId")
    @Mapping(source = "facility.description", target = "facilityDescription")
    @Mapping(source = "containerType.idContainerType", target = "containerTypeId")
    @Mapping(source = "containerType.description", target = "containerTypeDescription")
    ContainerDTO toDto(Container container);

    @Mapping(source = "facilityId", target = "facility")
    @Mapping(source = "containerTypeId", target = "containerType")
    Container toEntity(ContainerDTO containerDTO);

    default Container fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Container container = new Container();
        container.setIdContainer(id);
        return container;
    }
}
