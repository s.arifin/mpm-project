package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RequestProductDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity RequestProduct and its DTO RequestProductDTO.
 */
@Mapper(componentModel = "spring", uses = {FacilityTypeMapper.class, FacilityMapper.class, RequestTypeMapper.class})
public interface RequestProductMapper extends EntityMapper<RequestProductDTO, RequestProduct> {

    @Mapping(source = "facilityFrom.idFacility", target = "facilityFromId")
    @Mapping(source = "facilityFrom.facilityType.idFacilityType", target = "facilityTypeFromId")
    @Mapping(source = "facilityFrom.description", target = "facilityFromDescription")
    @Mapping(source = "facilityTo.idFacility", target = "facilityToId")
    @Mapping(source = "facilityTo.facilityType.idFacilityType", target = "facilityTypeToId")
    @Mapping(source = "facilityTo.description", target = "facilityToDescription")
    @Mapping(source = "requestType.idRequestType", target = "requestTypeId")
    @Mapping(source = "requestType.description", target = "requestTypeDescription")
    RequestProductDTO toDto(RequestProduct requestProduct);

    @Mapping(source = "facilityFromId", target = "facilityFrom")
    @Mapping(source = "facilityToId", target = "facilityTo")
    @Mapping(source = "requestTypeId", target = "requestType")
    RequestProduct toEntity(RequestProductDTO requestProductDTO);

    default RequestProduct fromId(UUID id) {
        if (id == null) {
            return null;
        }
        RequestProduct requestProduct = new RequestProduct();
        requestProduct.setIdRequest(id);
        return requestProduct;
    }
}
