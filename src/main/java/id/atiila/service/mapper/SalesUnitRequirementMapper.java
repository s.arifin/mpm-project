package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.SalesUnitRequirementDTO;
import org.mapstruct.*;
import java.util.UUID;


/**
 * Mapper for the entity SalesUnitRequirement and its DTO SalesUnitRequirementDTO.
 */
@Mapper(componentModel = "spring", uses = {
    PersonMapper.class,
    CustomerMapper.class,
    SalesmanMapper.class,
    InternalMapper.class,
    SaleTypeMapper.class,
    BillToMapper.class,
    SalesBrokerMapper.class,
    FeatureMapper.class,
    MotorMapper.class,
    PaymentApplicationMapper.class,
    LeasingTenorProvideMapper.class,
    RequirementTypeMapper.class,
    LeasingCompanyMapper.class,
    RequirementTypeMapper.class,
    RequirementMapper.class,
    OrganizationMapper.class
})
public interface SalesUnitRequirementMapper extends EntityMapper <SalesUnitRequirementDTO, SalesUnitRequirement> {

    @Mapping(source = "customer.idCustomer", target = "customerId")
    @Mapping(source = "customer.party.name", target = "customerName")

    @Mapping(source = "salesman.idPartyRole", target = "salesmanId")
    @Mapping(source = "salesman.party.name", target = "salesmanName")

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")

    @Mapping(source = "saleType.idSaleType", target = "saleTypeId")
    @Mapping(source = "saleType.description", target = "saleTypeDescription")

    @Mapping(source = "billTo.idBillTo", target = "billToId")
    @Mapping(source = "billTo.party.name", target = "billToName")

    @Mapping(source = "color.idFeature", target = "colorId")
    @Mapping(source = "color.description", target = "colorDescription")

    @Mapping(source = "product.idProduct", target = "productId")
    @Mapping(source = "product.name", target = "productName")

    @Mapping(source = "idProspectSource", target = "prospectSourceId")
    @Mapping(source = "leasingCompany.idPartyRole", target = "leasingCompanyId")
    @Mapping(source = "requirementType.idRequirementType", target = "idReqTyp")
    SalesUnitRequirementDTO toDto(SalesUnitRequirement salesUnitRequirement);

    @Mapping(source = "customerId", target = "customer")
    @Mapping(source = "salesmanId", target = "salesman")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "saleTypeId", target = "saleType")
    @Mapping(source = "billToId", target = "billTo")
    @Mapping(source = "colorId", target = "color")
    @Mapping(source = "productId", target = "product")
    @Mapping(source = "prospectSourceId", target = "idProspectSource")
    @Mapping(source = "leasingCompanyId", target = "leasingCompany")
    @Mapping(source = "idReqTyp", target = "requirementType")
    SalesUnitRequirement toEntity(SalesUnitRequirementDTO salesUnitRequirementDTO);

    default SalesUnitRequirement fromId(UUID id) {
        if (id == null) {
            return null;
        }
        SalesUnitRequirement salesUnitRequirement = new SalesUnitRequirement();
        salesUnitRequirement.setIdRequirement(id);
        return salesUnitRequirement;
    }
}
