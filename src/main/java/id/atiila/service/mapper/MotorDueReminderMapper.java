package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.MotorDueReminderDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity MotorDueReminder and its DTO MotorDueReminderDTO.
 */
@Mapper(componentModel = "spring", uses = {MotorMapper.class, })
public interface MotorDueReminderMapper extends EntityMapper <MotorDueReminderDTO, MotorDueReminder> {

    @Mapping(source = "motor.idProduct", target = "motorId")
    @Mapping(source = "motor.name", target = "motorName")
    MotorDueReminderDTO toDto(MotorDueReminder motorDueReminder); 

    @Mapping(source = "motorId", target = "motor")
    MotorDueReminder toEntity(MotorDueReminderDTO motorDueReminderDTO); 

    default MotorDueReminder fromId(Integer id) {
        if (id == null) {
            return null;
        }
        MotorDueReminder motorDueReminder = new MotorDueReminder();
        motorDueReminder.setIdReminder(id);
        return motorDueReminder;
    }
}
