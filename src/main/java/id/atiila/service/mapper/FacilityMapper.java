package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.FacilityDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Facility and its DTO FacilityDTO.
 */
@Mapper(componentModel = "spring", uses = {FacilityTypeMapper.class})
public interface FacilityMapper extends EntityMapper<FacilityDTO, Facility> {

    @Mapping(source = "facilityType.idFacilityType", target = "facilityTypeId")
    @Mapping(source = "facilityType.description", target = "facilityTypeDescription")
    @Mapping(source = "partOf.idFacility", target = "partOfId")
    @Mapping(source = "partOf.description", target = "partOfDescription")
    FacilityDTO toDto(Facility facility);

    @Mapping(source = "facilityTypeId", target = "facilityType")
    @Mapping(source = "partOfId", target = "partOf")
    Facility toEntity(FacilityDTO facilityDTO);

    default Facility fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Facility facility = new Facility();
        facility.setIdFacility(id);
        return facility;
    }
}
