package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProductPurchaseOrderDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ProductPurchaseOrder and its DTO ProductPurchaseOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {VendorMapper.class, InternalMapper.class, BillToMapper.class})
public interface ProductPurchaseOrderMapper extends EntityMapper<ProductPurchaseOrderDTO, ProductPurchaseOrder> {

    @Mapping(source = "vendor.idVendor", target = "vendorId")
    @Mapping(source = "vendor.organization.name", target = "vendorName")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    @Mapping(source = "billTo.party.name", target = "billToName")
    ProductPurchaseOrderDTO toDto(ProductPurchaseOrder productPurchaseOrder);

    @Mapping(source = "vendorId", target = "vendor")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "billToId", target = "billTo")
    ProductPurchaseOrder toEntity(ProductPurchaseOrderDTO productPurchaseOrderDTO);

    default ProductPurchaseOrder fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ProductPurchaseOrder productPurchaseOrder = new ProductPurchaseOrder();
        productPurchaseOrder.setIdOrder(id);
        return productPurchaseOrder;
    }
}
