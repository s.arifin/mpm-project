package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.VendorOrderDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity VendorOrder and its DTO VendorOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {VendorMapper.class, InternalMapper.class, BillToMapper.class})
public interface VendorOrderMapper extends EntityMapper<VendorOrderDTO, VendorOrder> {

    @Mapping(source = "vendor.idVendor", target = "vendorId")
    @Mapping(source = "vendor.organization.name", target = "vendorName")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    @Mapping(source = "billTo.party.name", target = "billToName")
    VendorOrderDTO toDto(VendorOrder vendorOrder);

    @Mapping(source = "vendorId", target = "vendor")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "billToId", target = "billTo")
    VendorOrder toEntity(VendorOrderDTO vendorOrderDTO);

    default VendorOrder fromId(UUID id) {
        if (id == null) {
            return null;
        }
        VendorOrder vendorOrder = new VendorOrder();
        vendorOrder.setIdOrder(id);
        return vendorOrder;
    }
}
