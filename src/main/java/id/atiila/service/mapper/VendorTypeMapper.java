package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.VendorTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity VendorType and its DTO VendorTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface VendorTypeMapper extends EntityMapper <VendorTypeDTO, VendorType> {
    
    

    default VendorType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        VendorType vendorType = new VendorType();
        vendorType.setIdVendorType(id);
        return vendorType;
    }
}
