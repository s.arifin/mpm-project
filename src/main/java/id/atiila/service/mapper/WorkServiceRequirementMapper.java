package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.WorkServiceRequirementDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity WorkServiceRequirement and its DTO WorkServiceRequirementDTO.
 */
@Mapper(componentModel = "spring", uses = {WorkOrderBookingMapper.class, PaymentApplicationMapper.class, FacilityMapper.class, WorkEffortTypeMapper.class,})
public interface WorkServiceRequirementMapper extends EntityMapper <WorkServiceRequirementDTO, WorkServiceRequirement> {

    @Mapping(source = "booking.idBooking", target = "bookingId")
    WorkServiceRequirementDTO toDto(WorkServiceRequirement workServiceRequirement); 
    @Mapping(target = "requirements", ignore = true)

    @Mapping(source = "bookingId", target = "booking")
    WorkServiceRequirement toEntity(WorkServiceRequirementDTO workServiceRequirementDTO); 

    default WorkServiceRequirement fromId(UUID id) {
        if (id == null) {
            return null;
        }
        WorkServiceRequirement workServiceRequirement = new WorkServiceRequirement();
        workServiceRequirement.setIdWe(id);
        return workServiceRequirement;
    }
}
