package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProductSalesOrderDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ProductSalesOrder and its DTO ProductSalesOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, CustomerMapper.class, BillToMapper.class, SaleTypeMapper.class})
public interface ProductSalesOrderMapper extends EntityMapper<ProductSalesOrderDTO, ProductSalesOrder> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "customer.idCustomer", target = "customerId")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    @Mapping(source = "saleType.idSaleType", target = "saleTypeId")
    @Mapping(source = "saleType.description", target = "saleTypeDescription")
    ProductSalesOrderDTO toDto(ProductSalesOrder productSalesOrder);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "customerId", target = "customer")
    @Mapping(source = "billToId", target = "billTo")
    @Mapping(source = "saleTypeId", target = "saleType")
    ProductSalesOrder toEntity(ProductSalesOrderDTO productSalesOrderDTO);

    default ProductSalesOrder fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ProductSalesOrder productSalesOrder = new ProductSalesOrder();
        productSalesOrder.setIdOrder(id);
        return productSalesOrder;
    }
}
