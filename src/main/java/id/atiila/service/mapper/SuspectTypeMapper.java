package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.SuspectTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity SuspectType and its DTO SuspectTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SuspectTypeMapper extends EntityMapper <SuspectTypeDTO, SuspectType> {


    default SuspectType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        SuspectType suspectType = new SuspectType();
        suspectType.setIdSuspectType(id);
        return suspectType;
    }
}
