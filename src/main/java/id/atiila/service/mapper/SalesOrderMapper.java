package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.SalesOrderDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity SalesOrder and its DTO SalesOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, CustomerMapper.class, BillToMapper.class, SaleTypeMapper.class})
public interface SalesOrderMapper extends EntityMapper<SalesOrderDTO, SalesOrder> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "customer.idCustomer", target = "customerId")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    @Mapping(source = "saleType.idSaleType", target = "saleTypeId")
    @Mapping(source = "saleType.description", target = "saleTypeDescription")
    SalesOrderDTO toDto(SalesOrder salesOrder);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "customerId", target = "customer")
    @Mapping(source = "billToId", target = "billTo")
    @Mapping(source = "saleTypeId", target = "saleType")
    SalesOrder toEntity(SalesOrderDTO salesOrderDTO);

    default SalesOrder fromId(UUID id) {
        if (id == null) {
            return null;
        }
        SalesOrder salesOrder = new SalesOrder();
        salesOrder.setIdOrder(id);
        return salesOrder;
    }
}
