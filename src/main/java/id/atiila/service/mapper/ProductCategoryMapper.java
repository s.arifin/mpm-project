package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProductCategoryDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity ProductCategory and its DTO ProductCategoryDTO.
 */
@Mapper(componentModel = "spring", uses = {ProductCategoryTypeMapper.class})
public interface ProductCategoryMapper extends EntityMapper<ProductCategoryDTO, ProductCategory> {

    @Mapping(source = "categoryType.idCategoryType", target = "categoryTypeId")
    @Mapping(source = "categoryType.description", target = "categoryTypeDescription")
    ProductCategoryDTO toDto(ProductCategory productCategory);

    @Mapping(source = "categoryTypeId", target = "categoryType")
    ProductCategory toEntity(ProductCategoryDTO productCategoryDTO);

    default ProductCategory fromId(Integer id) {
        if (id == null) {
            return null;
        }
        ProductCategory productCategory = new ProductCategory();
        productCategory.setIdCategory(id);
        return productCategory;
    }
}
