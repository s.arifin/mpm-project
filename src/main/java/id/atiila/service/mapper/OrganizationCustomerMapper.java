package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.OrganizationCustomerDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity OrganizationCustomer and its DTO OrganizationCustomerDTO.
 */
@Mapper(componentModel = "spring", uses = {OrganizationMapper.class, })
public interface OrganizationCustomerMapper extends EntityMapper <OrganizationCustomerDTO, OrganizationCustomer> {

    @Mapping(source = "party.idParty", target = "partyId")
    @Mapping(source = "party.name", target = "partyName")
    OrganizationCustomerDTO toDto(OrganizationCustomer organizationCustomer);

    @Mapping(source = "partyId", target = "party")
    OrganizationCustomer toEntity(OrganizationCustomerDTO organizationCustomerDTO);

    default OrganizationCustomer fromId(String id) {
        if (id == null) {
            return null;
        }
        OrganizationCustomer organizationCustomer = new OrganizationCustomer();
        organizationCustomer.setIdCustomer(id);
        return organizationCustomer;
    }
}
