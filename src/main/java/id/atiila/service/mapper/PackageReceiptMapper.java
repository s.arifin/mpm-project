package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PackageReceiptDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity PackageReceipt and its DTO PackageReceiptDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, ShipToMapper.class})
public interface PackageReceiptMapper extends EntityMapper<PackageReceiptDTO, PackageReceipt> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    @Mapping(source = "shipFrom.idShipTo", target = "shipFromId")
    @Mapping(source = "shipFrom.party.name", target = "shipFromName")
    PackageReceiptDTO toDto(PackageReceipt packageReceipt);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "shipFromId", target = "shipFrom")
    PackageReceipt toEntity(PackageReceiptDTO packageReceiptDTO);

    default PackageReceipt fromId(UUID id) {
        if (id == null) {
            return null;
        }
        PackageReceipt packageReceipt = new PackageReceipt();
        packageReceipt.setIdPackage(id);
        return packageReceipt;
    }
}
