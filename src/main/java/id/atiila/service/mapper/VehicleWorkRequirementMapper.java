package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.VehicleWorkRequirementDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity VehicleWorkRequirement and its DTO VehicleWorkRequirementDTO.
 */
@Mapper(componentModel = "spring", uses = {CustomerMapper.class, MechanicMapper.class, VehicleIdentificationMapper.class,
    PaymentApplicationMapper.class, WorkServiceRequirementMapper.class, RequirementMapper.class })
public interface VehicleWorkRequirementMapper extends EntityMapper <VehicleWorkRequirementDTO, VehicleWorkRequirement> {

    @Mapping(source = "customer.idCustomer", target = "customerId")
    @Mapping(source = "mechanic.idPartyRole", target = "mechanicId")
    @Mapping(source = "vehicle.idVehicleIdentification", target = "vehicleId")
    VehicleWorkRequirementDTO toDto(VehicleWorkRequirement vehicleWorkRequirement);

    @Mapping(source = "customerId", target = "customer")
    @Mapping(source = "mechanicId", target = "mechanic")
    @Mapping(source = "vehicleId", target = "vehicle")
    VehicleWorkRequirement toEntity(VehicleWorkRequirementDTO vehicleWorkRequirementDTO);

    default VehicleWorkRequirement fromId(UUID id) {
        if (id == null) {
            return null;
        }
        VehicleWorkRequirement vehicleWorkRequirement = new VehicleWorkRequirement();
        vehicleWorkRequirement.setIdRequirement(id);
        return vehicleWorkRequirement;
    }
}
