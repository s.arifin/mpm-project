package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.QuoteItemDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity QuoteItem and its DTO QuoteItemDTO.
 */
@Mapper(componentModel = "spring", uses = {QuoteMapper.class, ProductMapper.class, })
public interface QuoteItemMapper extends EntityMapper <QuoteItemDTO, QuoteItem> {

    @Mapping(source = "quote.idQuote", target = "quoteId")

    @Mapping(source = "product.idProduct", target = "productId")
    @Mapping(source = "product.name", target = "productName")
    QuoteItemDTO toDto(QuoteItem quoteItem);

    @Mapping(source = "quoteId", target = "quote")

    @Mapping(source = "productId", target = "product")
    QuoteItem toEntity(QuoteItemDTO quoteItemDTO);

    default QuoteItem fromId(UUID id) {
        if (id == null) {
            return null;
        }
        QuoteItem quoteItem = new QuoteItem();
        quoteItem.setIdQuoteItem(id);
        return quoteItem;
    }
}
