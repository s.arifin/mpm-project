package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ShipToDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity ShipTo and its DTO ShipToDTO.
 */
@Mapper(componentModel = "spring", uses = {PartyMapper.class})
public interface ShipToMapper extends EntityMapper<ShipToDTO, ShipTo> {

    @Mapping(source = "party.idParty", target = "partyId")
    @Mapping(source = "party.name", target = "partyName")
    ShipToDTO toDto(ShipTo shipTo);

    @Mapping(source = "partyId", target = "party")
    ShipTo toEntity(ShipToDTO shipToDTO);

    default ShipTo fromId(String id) {
        if (id == null) {
            return null;
        }
        ShipTo shipTo = new ShipTo();
        shipTo.setIdShipTo(id);
        return shipTo;
    }
}
