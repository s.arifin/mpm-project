package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.RequirementTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity RequirementType and its DTO RequirementTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RequirementTypeMapper extends EntityMapper <RequirementTypeDTO, RequirementType> {
    
    

    default RequirementType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        RequirementType requirementType = new RequirementType();
        requirementType.setIdRequirementType(id);
        return requirementType;
    }
}
