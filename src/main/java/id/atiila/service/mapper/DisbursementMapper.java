package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.DisbursementDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Disbursement and its DTO DisbursementDTO.
 */
@Mapper(componentModel = "spring", uses = {PaymentTypeMapper.class, PaymentMethodMapper.class, InternalMapper.class, BillToMapper.class})
public interface DisbursementMapper extends EntityMapper<DisbursementDTO, Disbursement> {

    @Mapping(source = "paymentType.idPaymentType", target = "paymentTypeId")
    @Mapping(source = "method.idPaymentMethod", target = "methodId")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "paidTo.idBillTo", target = "paidToId")
    @Mapping(source = "paidFrom.idBillTo", target = "paidFromId")
    DisbursementDTO toDto(Disbursement disbursement);

    @Mapping(target = "details", ignore = true)
    @Mapping(source = "paymentTypeId", target = "paymentType")
    @Mapping(source = "methodId", target = "method")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "paidToId", target = "paidTo")
    @Mapping(source = "paidFromId", target = "paidFrom")
    Disbursement toEntity(DisbursementDTO disbursementDTO);

    default Disbursement fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Disbursement disbursement = new Disbursement();
        disbursement.setIdPayment(id);
        return disbursement;
    }
}
