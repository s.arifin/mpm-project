package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PartSalesOrderDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity PartSalesOrder and its DTO PartSalesOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, CustomerMapper.class, BillToMapper.class})
public interface PartSalesOrderMapper extends EntityMapper<PartSalesOrderDTO, PartSalesOrder> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "customer.idCustomer", target = "customerId")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    PartSalesOrderDTO toDto(PartSalesOrder partSalesOrder);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "customerId", target = "customer")
    @Mapping(source = "billToId", target = "billTo")
    PartSalesOrder toEntity(PartSalesOrderDTO partSalesOrderDTO);

    default PartSalesOrder fromId(UUID id) {
        if (id == null) {
            return null;
        }
        PartSalesOrder partSalesOrder = new PartSalesOrder();
        partSalesOrder.setIdOrder(id);
        return partSalesOrder;
    }
}
