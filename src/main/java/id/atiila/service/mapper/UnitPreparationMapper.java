package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.UnitPreparationDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity UnitPreparation and its DTO UnitPreparationDTO.
 */
@Mapper(componentModel = "spring", uses = {ShipToMapper.class, InternalMapper.class, FacilityMapper.class, InventoryItemMapper.class, OrderItemMapper.class})
public interface UnitPreparationMapper extends EntityMapper<UnitPreparationDTO, UnitPreparation> {

    @Mapping(source = "shipTo.idShipTo", target = "shipToId")
    @Mapping(source = "shipTo.party.name", target = "shipToName")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    @Mapping(source = "facility.idFacility", target = "facilityId")
    @Mapping(source = "facility.description", target = "facilityDescription")
    @Mapping(source = "inventoryItem.idInventoryItem", target = "inventoryItemId")
    @Mapping(source = "orderItem.idOrderItem", target = "orderItemId")
    @Mapping(source = "orderItem.orders.orderNumber", target = "orderNumber")
    UnitPreparationDTO toDto(UnitPreparation unitPreparation);

    @Mapping(source = "shipToId", target = "shipTo")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "facilityId", target = "facility")
    @Mapping(source = "inventoryItemId", target = "inventoryItem")
    @Mapping(source = "orderItemId", target = "orderItem")
    @Mapping(target = "statuses", ignore = true)
    UnitPreparation toEntity(UnitPreparationDTO unitPreparationDTO);

    default UnitPreparation fromId(UUID id) {
        if (id == null) {
            return null;
        }
        UnitPreparation unitPreparation = new UnitPreparation();
        unitPreparation.setIdSlip(id);
        return unitPreparation;
    }
}
