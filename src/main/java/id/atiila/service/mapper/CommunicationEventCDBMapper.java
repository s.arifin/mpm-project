package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.CommunicationEventCDBDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity CommunicationEventCDB and its DTO CommunicationEventCDBDTO.
 */
@Mapper(componentModel = "spring", uses = {CommunicationEventMapper.class,})
public interface CommunicationEventCDBMapper extends EntityMapper <CommunicationEventCDBDTO, CommunicationEventCDB> {

    default CommunicationEventCDB fromId(UUID id) {
        if (id == null) {
            return null;
        }
        CommunicationEventCDB communicationEventCDB = new CommunicationEventCDB();
        communicationEventCDB.setIdCommunicationEvent(id);
        return communicationEventCDB;
    }
}
