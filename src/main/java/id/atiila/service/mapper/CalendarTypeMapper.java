package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.CalendarTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity CalendarType and its DTO CalendarTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CalendarTypeMapper extends EntityMapper <CalendarTypeDTO, CalendarType> {
    
    

    default CalendarType fromId(Integer id) {
        if (id == null) {
            return null;
        }
        CalendarType calendarType = new CalendarType();
        calendarType.setIdCalendarType(id);
        return calendarType;
    }
}
