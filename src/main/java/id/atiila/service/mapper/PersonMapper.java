package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.repository.PersonRepository;
import id.atiila.service.dto.PersonDTO;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.UUID;

/**
 * Mapper for the entity Person and its DTO PersonDTO.
 */
@Mapper(componentModel = "spring", uses = {ContactMechanismMapper.class, PartyCategoryMapper.class, FacilityMapper.class,
        ReligionTypeMapper.class, WorkTypeMapper.class, UserMapper.class, PostalAddressMapper.class })
public abstract class PersonMapper {

    @Autowired
    private PersonRepository repo;

    @Mapping(source = "religionType.idReligionType", target = "religionTypeId")
    @Mapping(source = "religionType.description", target = "religionTypeDescription")
    @Mapping(source = "workType.idWorkType", target = "workTypeId")
    @Mapping(source = "workType.description", target = "workTypeDescription")
    public abstract PersonDTO toDto(Person e);

    @Mapping(source = "religionTypeId", target = "religionType")
    @Mapping(source = "workTypeId", target = "workType")
    public abstract Person toEntity(PersonDTO dto);

    public Person fromId(UUID id) {
        if (id == null) {
            return null;
        }
        return repo.findOne(id);
    }
}
