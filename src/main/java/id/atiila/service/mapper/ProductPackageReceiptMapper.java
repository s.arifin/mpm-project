package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ProductPackageReceiptDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ProductPackageReceipt and its DTO ProductPackageReceiptDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, ShipToMapper.class})
public interface ProductPackageReceiptMapper extends EntityMapper<ProductPackageReceiptDTO, ProductPackageReceipt> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    @Mapping(source = "shipFrom.idShipTo", target = "shipFromId")
    @Mapping(source = "shipFrom.party.name", target = "shipFromName")
    ProductPackageReceiptDTO toDto(ProductPackageReceipt productPackageReceipt);

    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "shipFromId", target = "shipFrom")
    ProductPackageReceipt toEntity(ProductPackageReceiptDTO productPackageReceiptDTO);

    default ProductPackageReceipt fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ProductPackageReceipt productPackageReceipt = new ProductPackageReceipt();
        productPackageReceipt.setIdPackage(id);
        return productPackageReceipt;
    }
}
