package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ServiceDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity Service and its DTO ServiceDTO.
 */
@Mapper(componentModel = "spring", uses = {UomMapper.class, ProductTypeMapper.class, FeatureMapper.class, ProductCategoryMapper.class, WeServiceTypeMapper.class, })
public interface ServiceMapper extends EntityMapper <ServiceDTO, Service> {

    @Mapping(source = "uom.idUom", target = "uomId")
    @Mapping(source = "uom.description", target = "uomDescription")
    ServiceDTO toDto(Service service); 

    @Mapping(source = "uomId", target = "uom")
    Service toEntity(ServiceDTO serviceDTO); 

    default Service fromId(String id) {
        if (id == null) {
            return null;
        }
        Service service = new Service();
        service.setIdProduct(id);
        return service;
    }
}
