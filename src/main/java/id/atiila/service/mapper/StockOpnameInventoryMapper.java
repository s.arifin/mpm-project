package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.StockOpnameInventoryDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity StockOpnameInventory and its DTO StockOpnameInventoryDTO.
 */
@Mapper(componentModel = "spring", uses = {InventoryItemMapper.class, StockOpnameItemMapper.class})
public interface StockOpnameInventoryMapper extends EntityMapper<StockOpnameInventoryDTO, StockOpnameInventory> {

    @Mapping(source = "inventoryItem.idInventoryItem", target = "inventoryItemId")
    @Mapping(source = "item.idStockopnameItem", target = "itemId")
    StockOpnameInventoryDTO toDto(StockOpnameInventory stockOpnameInventory);

    @Mapping(source = "inventoryItemId", target = "inventoryItem")
    @Mapping(source = "itemId", target = "item")
    StockOpnameInventory toEntity(StockOpnameInventoryDTO stockOpnameInventoryDTO);

    default StockOpnameInventory fromId(UUID id) {
        if (id == null) {
            return null;
        }
        StockOpnameInventory stockOpnameInventory = new StockOpnameInventory();
        stockOpnameInventory.setIdStockOpnameInventory(id);
        return stockOpnameInventory;
    }
}
