package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.UnitDeliverableDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity UnitDeliverable and its DTO UnitDeliverableDTO.
 */
@Mapper(componentModel = "spring", uses = {VehicleDocumentRequirementMapper.class, StatusTypeMapper.class, })
public interface UnitDeliverableMapper extends EntityMapper <UnitDeliverableDTO, UnitDeliverable> {

    @Mapping(source = "vehicleDocumentRequirement.idRequirement", target = "vehicleDocumentRequirementId")

    @Mapping(source = "vndStatusType.idStatusType", target = "vndStatusTypeId")
    @Mapping(source = "vndStatusType.description", target = "vndStatusTypeDescription")

    @Mapping(source = "conStatusType.idStatusType", target = "conStatusTypeId")
    @Mapping(source = "conStatusType.description", target = "conStatusTypeDescription")

    @Mapping(source = "vehicleDocumentRequirement.vehicle.idFrame", target = "idframe")
    @Mapping(source = "vehicleDocumentRequirement.vehicle.idMachine", target = "idmachine")
    // @Mapping(source = "vehicleDocumentRequirement.sur.requestpoliceid", target = "requestpoliceid")
    // @Mapping(source = "vehicleDocumentRequirement.personOwner.name", target = "stnkNama")
    // @Mapping(source = "vehicleDocumentRequirement.personOwner.postalAddress.address1", target = "stnkAlamat")
    UnitDeliverableDTO toDto(UnitDeliverable unitDeliverable);

    @Mapping(source = "vehicleDocumentRequirementId", target = "vehicleDocumentRequirement")

    @Mapping(source = "vndStatusTypeId", target = "vndStatusType")

    @Mapping(source = "conStatusTypeId", target = "conStatusType")
    UnitDeliverable toEntity(UnitDeliverableDTO unitDeliverableDTO);

    default UnitDeliverable fromId(UUID id) {
        if (id == null) {
            return null;
        }
        UnitDeliverable unitDeliverable = new UnitDeliverable();
        unitDeliverable.setIdDeliverable(id);
        return unitDeliverable;
    }
}
