package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.UnitReqInternalDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity UnitReqInternal and its DTO UnitReqInternalDTO.
 */
@Mapper(componentModel = "spring", uses = {BranchMapper.class, MotorMapper.class, PaymentApplicationMapper.class, })
public interface UnitReqInternalMapper extends EntityMapper <UnitReqInternalDTO, UnitReqInternal> {

    @Mapping(source = "branch.idInternal", target = "branchId")
    @Mapping(source = "branch.organization.name", target = "branchName")
    @Mapping(source = "motor.idProduct", target = "motorId")
    @Mapping(source = "motor.name", target = "motorName")
    UnitReqInternalDTO toDto(UnitReqInternal unitReqInternal);

    @Mapping(source = "branchId", target = "branch")
    @Mapping(source = "motorId", target = "motor")
    UnitReqInternal toEntity(UnitReqInternalDTO unitReqInternalDTO);

    default UnitReqInternal fromId(UUID id) {
        if (id == null) {
            return null;
        }
        UnitReqInternal unitReqInternal = new UnitReqInternal();
        unitReqInternal.setIdRequirement(id);
        return unitReqInternal;
    }
}
