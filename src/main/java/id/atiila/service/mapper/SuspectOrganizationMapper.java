package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.SuspectOrganizationDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity SuspectOrganization and its DTO SuspectOrganizationDTO.
 */
@Mapper(componentModel = "spring", uses = {OrganizationMapper.class, InternalMapper.class, PostalAddressMapper.class, SalesmanMapper.class,})
public interface SuspectOrganizationMapper extends EntityMapper <SuspectOrganizationDTO, SuspectOrganization> {

    @Mapping(source = "dealer.idInternal", target = "dealerId")
    @Mapping(source = "dealer.organization.name", target = "dealerName")
    @Mapping(source = "salesCoordinator.idPartyRole", target = "salesCoordinatorId")
    @Mapping(source = "salesCoordinator.party.name", target = "salesCoordinatorName")
    @Mapping(source = "salesman.idPartyRole", target = "salesmanId")
    @Mapping(source = "salesman.party.name", target = "salesmanName")
    SuspectOrganizationDTO toDto(SuspectOrganization suspectOrganization);

    @Mapping(source = "dealerId", target = "dealer")
    @Mapping(source = "salesCoordinatorId", target = "salesCoordinator")
    @Mapping(source = "salesmanId", target = "salesman")
    SuspectOrganization toEntity(SuspectOrganizationDTO suspectOrganizationDTO);

    default SuspectOrganization fromId(UUID id) {
        if (id == null) {
            return null;
        }
        SuspectOrganization suspectOrganization = new SuspectOrganization();
        suspectOrganization.setIdSuspect(id);
        return suspectOrganization;
    }
}
