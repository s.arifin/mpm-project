package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.OrdersDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Orders and its DTO OrdersDTO.
 */
@Mapper(componentModel = "spring", uses = {PaymentApplicationMapper.class, OrderTypeMapper.class, })
public interface OrdersMapper extends EntityMapper <OrdersDTO, Orders> {

    @Mapping(source = "orderType.idOrderType", target = "orderTypeId")
    @Mapping(source = "orderType.description", target = "orderTypeDescription")
    OrdersDTO toDto(Orders orders);

    @Mapping(source = "orderTypeId", target = "orderType")
    @Mapping(target = "details", ignore = true)
    Orders toEntity(OrdersDTO ordersDTO);

    default Orders fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Orders orders = new Orders();
        orders.setIdOrder(id);
        return orders;
    }
}
