package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.VehicleRegistrationDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity VehicleRegistration and its DTO VehicleRegistrationDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, BillToMapper.class, VendorMapper.class, RequirementMapper.class,
            PartyMapper.class, RequirementTypeMapper.class})
public interface VehicleRegistrationMapper extends EntityMapper<VehicleRegistrationDTO, VehicleDocumentRequirement> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.root.idInternal", target = "rootId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    @Mapping(source = "billTo.idBillTo", target = "billToId")
    @Mapping(source = "billTo.party.name", target = "billToName")
    @Mapping(source = "vendor.idVendor", target = "vendorId")
    @Mapping(source = "vendor.organization.name", target = "vendorName")
    @Mapping(source = "requirementType.idRequirementType", target = "requirementTypeId")
    VehicleRegistrationDTO toDto(VehicleDocumentRequirement vehicleRegistration);

    @Mapping(source = "requirementTypeId", target = "requirementType")
    @Mapping(source = "internalId", target = "internal")
    @Mapping(source = "billToId", target = "billTo")
    @Mapping(source = "vendorId", target = "vendor")
    @Mapping(target = "statuses", ignore = true)
    @Mapping(target = "roles", ignore = true)
    VehicleDocumentRequirement toEntity(VehicleRegistrationDTO vehicleRegistrationDTO);

    default VehicleDocumentRequirement fromId(UUID id) {
        if (id == null) {
            return null;
        }
        VehicleDocumentRequirement vehicleRegistration = new VehicleDocumentRequirement();
        vehicleRegistration.setIdRequirement(id);
        return vehicleRegistration;
    }
}
