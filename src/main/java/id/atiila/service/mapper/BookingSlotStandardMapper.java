package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.BookingSlotStandardDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity BookingSlotStandard and its DTO BookingSlotStandardDTO.
 */
@Mapper(componentModel = "spring", uses = {InternalMapper.class, BookingTypeMapper.class, })
public interface BookingSlotStandardMapper extends EntityMapper <BookingSlotStandardDTO, BookingSlotStandard> {

    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")

    @Mapping(source = "bookingType.idBookingType", target = "bookingTypeId")
    @Mapping(source = "bookingType.description", target = "bookingTypeDescription")
    BookingSlotStandardDTO toDto(BookingSlotStandard bookingSlotStandard);

    @Mapping(source = "internalId", target = "internal")

    @Mapping(source = "bookingTypeId", target = "bookingType")
    BookingSlotStandard toEntity(BookingSlotStandardDTO bookingSlotStandardDTO);

    default BookingSlotStandard fromId(UUID id) {
        if (id == null) {
            return null;
        }
        BookingSlotStandard bookingSlotStandard = new BookingSlotStandard();
        bookingSlotStandard.setIdBookSlotStd(id);
        return bookingSlotStandard;
    }
}
