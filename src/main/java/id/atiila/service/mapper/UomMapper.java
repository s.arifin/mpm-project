package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.UomDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity Uom and its DTO UomDTO.
 */
@Mapper(componentModel = "spring", uses = {UomTypeMapper.class})
public interface UomMapper extends EntityMapper<UomDTO, Uom> {

    @Mapping(source = "uomType.idUomType", target = "uomTypeId")
    @Mapping(source = "uomType.description", target = "uomTypeDescription")
    UomDTO toDto(Uom uom);

    @Mapping(source = "uomTypeId", target = "uomType")
    Uom toEntity(UomDTO uomDTO);

    default Uom fromId(String id) {
        if (id == null) {
            return null;
        }
        Uom uom = new Uom();
        uom.setIdUom(id);
        return uom;
    }
}
