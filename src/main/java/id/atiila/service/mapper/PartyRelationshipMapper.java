package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.PartyRelationshipDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity PartyRelationship and its DTO PartyRelationshipDTO.
 */
@Mapper(componentModel = "spring", uses = {StatusTypeMapper.class, RelationTypeMapper.class})
public interface PartyRelationshipMapper extends EntityMapper<PartyRelationshipDTO, PartyRelationship> {

    @Mapping(source = "statusType.idStatusType", target = "statusTypeId")
    @Mapping(source = "statusType.description", target = "statusTypeDescription")
    @Mapping(source = "relationType.idRelationType", target = "relationTypeId")
    @Mapping(source = "relationType.description", target = "relationTypeDescription")
    PartyRelationshipDTO toDto(PartyRelationship partyRelationship);

    @Mapping(source = "statusTypeId", target = "statusType")
    @Mapping(source = "relationTypeId", target = "relationType")
    PartyRelationship toEntity(PartyRelationshipDTO partyRelationshipDTO);

    default PartyRelationship fromId(UUID id) {
        if (id == null) {
            return null;
        }
        PartyRelationship partyRelationship = new PartyRelationship();
        partyRelationship.setIdPartyRelationship(id);
        return partyRelationship;
    }
}
