package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.WorkRequirementDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity WorkRequirement and its DTO WorkRequirementDTO.
 */
@Mapper(componentModel = "spring", uses = {PaymentApplicationMapper.class, FacilityMapper.class, WorkServiceRequirementMapper.class, })
public interface WorkRequirementMapper extends EntityMapper <WorkRequirementDTO, WorkRequirement> {
    
    

    default WorkRequirement fromId(UUID id) {
        if (id == null) {
            return null;
        }
        WorkRequirement workRequirement = new WorkRequirement();
        workRequirement.setIdRequirement(id);
        return workRequirement;
    }
}
