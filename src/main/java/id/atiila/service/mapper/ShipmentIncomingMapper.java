package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.ShipmentIncomingDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity ShipmentIncoming and its DTO ShipmentIncomingDTO.
 */
@Mapper(componentModel = "spring", uses = {ShipmentMapper.class, DriverMapper.class, PickingSlipMapper.class, PartyMapper.class,
            PartyRoleMapper.class, ShipToMapper.class, InventoryItemMapper.class, OrderItemMapper.class, ShipmentTypeMapper.class,
            PostalAddressMapper.class, InternalMapper.class, })
public interface ShipmentIncomingMapper extends EntityMapper <ShipmentIncomingDTO, ShipmentIncoming> {


    @Mapping(source = "shipmentType.idShipmentType", target = "shipmentTypeId")
    @Mapping(source = "shipFrom.idShipTo", target = "shipFromId")
    @Mapping(source = "shipTo.idShipTo", target = "shipToId")
    @Mapping(source = "addressFrom.idContact", target = "addressFromId")
    @Mapping(source = "addressTo.idContact", target = "addressToId")
    @Mapping(source = "internal.idInternal", target = "internalId")
    @Mapping(source = "internal.organization.name", target = "internalName")
    ShipmentIncomingDTO toDto(ShipmentIncoming e);

    @Mapping(target = "details", ignore = true)
    @Mapping(source = "shipmentTypeId", target = "shipmentType")
    @Mapping(source = "shipFromId", target = "shipFrom")
    @Mapping(source = "shipToId", target = "shipTo")
    @Mapping(source = "addressFromId", target = "addressFrom")
    @Mapping(source = "addressToId", target = "addressTo")
    @Mapping(source = "internalId", target = "internal")
    ShipmentIncoming toEntity(ShipmentIncomingDTO d);

    default ShipmentIncoming fromId(UUID id) {
        if (id == null) {
            return null;
        }
        ShipmentIncoming shipmentIncoming = new ShipmentIncoming();
        shipmentIncoming.setIdShipment(id);
        return shipmentIncoming;
    }
}
