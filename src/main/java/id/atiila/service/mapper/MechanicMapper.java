package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.repository.MechanicRepository;
import id.atiila.service.dto.MechanicDTO;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.UUID;

/**
 * Mapper for the entity Mechanic and its DTO MechanicDTO.
 */

@Mapper(componentModel = "spring", uses = {RoleTypeMapper.class, PersonMapper.class, })
public abstract class MechanicMapper {

    @Autowired
    private MechanicRepository repo;

    public abstract MechanicDTO toDto(Mechanic e);

    public abstract Mechanic toEntity(MechanicDTO d);

    public Mechanic fromId(UUID id) {
        if (id == null) {
            return null;
        }
        return repo.findOne(id);
    }
}
