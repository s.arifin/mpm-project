package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.VehicleIdentificationDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity VehicleIdentification and its DTO VehicleIdentificationDTO.
 */
@Mapper(componentModel = "spring", uses = {VehicleMapper.class, CustomerMapper.class, })
public interface VehicleIdentificationMapper extends EntityMapper <VehicleIdentificationDTO, VehicleIdentification> {

    @Mapping(source = "vehicle.idVehicle", target = "vehicleId")
    @Mapping(source = "customer.idCustomer", target = "customerId")
    @Mapping(source = "customer.party.name", target = "customerName")
    VehicleIdentificationDTO toDto(VehicleIdentification vehicleIdentification);

    @Mapping(source = "vehicleId", target = "vehicle")

    @Mapping(source = "customerId", target = "customer")
    VehicleIdentification toEntity(VehicleIdentificationDTO vehicleIdentificationDTO);

    default VehicleIdentification fromId(UUID id) {
        if (id == null) {
            return null;
        }
        VehicleIdentification vehicleIdentification = new VehicleIdentification();
        vehicleIdentification.setIdVehicleIdentification(id);
        return vehicleIdentification;
    }
}
