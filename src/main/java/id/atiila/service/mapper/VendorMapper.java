package id.atiila.service.mapper;

import id.atiila.domain.*;
import id.atiila.service.dto.VendorDTO;
import org.mapstruct.*;
import java.util.UUID;

/**
 * Mapper for the entity Vendor and its DTO VendorDTO.
 */
@Mapper(componentModel = "spring", uses = {RoleTypeMapper.class, OrganizationMapper.class, VendorTypeMapper.class})
public interface VendorMapper extends EntityMapper <VendorDTO, Vendor> {

    VendorDTO toDto(Vendor vendor);

    Vendor toEntity(VendorDTO vendorDTO);

    default Vendor fromId(String id) {
        if (id == null) {
            return null;
        }
        Vendor vendor = new Vendor();
        vendor.setIdVendor(id);
        return vendor;
    }
}
