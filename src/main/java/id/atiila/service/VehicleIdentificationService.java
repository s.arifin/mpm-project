package id.atiila.service;

import id.atiila.domain.VehicleIdentification;
import id.atiila.repository.VehicleIdentificationRepository;
import id.atiila.repository.search.VehicleIdentificationSearchRepository;
import id.atiila.service.dto.VehicleIdentificationDTO;
import id.atiila.service.mapper.VehicleIdentificationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing VehicleIdentification.
 * BeSmart Team
 */

@Service
@Transactional
public class VehicleIdentificationService {

    private final Logger log = LoggerFactory.getLogger(VehicleIdentificationService.class);

    private final VehicleIdentificationRepository vehicleIdentificationRepository;

    private final VehicleIdentificationMapper vehicleIdentificationMapper;

    private final VehicleIdentificationSearchRepository vehicleIdentificationSearchRepository;

    public VehicleIdentificationService(VehicleIdentificationRepository vehicleIdentificationRepository, VehicleIdentificationMapper vehicleIdentificationMapper, VehicleIdentificationSearchRepository vehicleIdentificationSearchRepository) {
        this.vehicleIdentificationRepository = vehicleIdentificationRepository;
        this.vehicleIdentificationMapper = vehicleIdentificationMapper;
        this.vehicleIdentificationSearchRepository = vehicleIdentificationSearchRepository;
    }

    /**
     * Save a vehicleIdentification.
     *
     * @param vehicleIdentificationDTO the entity to save
     * @return the persisted entity
     */
    public VehicleIdentificationDTO save(VehicleIdentificationDTO vehicleIdentificationDTO) {
        log.debug("Request to save VehicleIdentification : {}", vehicleIdentificationDTO);
        VehicleIdentification vehicleIdentification = vehicleIdentificationMapper.toEntity(vehicleIdentificationDTO);
        vehicleIdentification = vehicleIdentificationRepository.save(vehicleIdentification);
        VehicleIdentificationDTO result = vehicleIdentificationMapper.toDto(vehicleIdentification);
        vehicleIdentificationSearchRepository.save(vehicleIdentification);
        return result;
    }

    /**
     *  Get all the vehicleIdentifications.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleIdentificationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VehicleIdentifications");
        return vehicleIdentificationRepository.findAll(pageable)
            .map(vehicleIdentificationMapper::toDto);
    }

    /**
     *  Get one vehicleIdentification by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public VehicleIdentificationDTO findOne(UUID id) {
        log.debug("Request to get VehicleIdentification : {}", id);
        VehicleIdentification vehicleIdentification = vehicleIdentificationRepository.findOne(id);
        return vehicleIdentificationMapper.toDto(vehicleIdentification);
    }

    /**
     *  Delete the  vehicleIdentification by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete VehicleIdentification : {}", id);
        vehicleIdentificationRepository.delete(id);
        vehicleIdentificationSearchRepository.delete(id);
    }

    /**
     * Search for the vehicleIdentification corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleIdentificationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of VehicleIdentifications for query {}", query);
        Page<VehicleIdentification> result = vehicleIdentificationSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(vehicleIdentificationMapper::toDto);
    }

    public VehicleIdentificationDTO processExecuteData(Integer id, String param, VehicleIdentificationDTO dto) {
        VehicleIdentificationDTO r = dto;
        if (r != null) {
            switch (id) {
                case 101: {
                    VehicleIdentification vehicleIdentification = null;
                    List<VehicleIdentification> l = vehicleIdentificationRepository.findAllByVehicleNumber(param);
                    if (l.isEmpty()) r = null;
                    else
                        r = vehicleIdentificationMapper.toDto(l.get(0));
                    break;
                }
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional(readOnly = true)
    public VehicleIdentificationDTO findOneByNumber(String id) {
        log.debug("Request to get VehicleIdentification : {}", id);
        VehicleIdentification vehicleIdentification = null;
        List<VehicleIdentification> l = vehicleIdentificationRepository.findAllByVehicleNumber(id);
        if (l.isEmpty()) return null;
        return vehicleIdentificationMapper.toDto(l.get(0));
    }

    public Set<VehicleIdentificationDTO> processExecuteListData(Integer id, String param, Set<VehicleIdentificationDTO> dto) {
        Set<VehicleIdentificationDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String command = (String) item.get("command");
        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        vehicleIdentificationSearchRepository.deleteAll();
        List<VehicleIdentification> vehicleIdentifications =  vehicleIdentificationRepository.findAll();
        for (VehicleIdentification m: vehicleIdentifications) {
            vehicleIdentificationSearchRepository.save(m);
            log.debug("Data vehicle Identification save !...");
        }
    }

}
