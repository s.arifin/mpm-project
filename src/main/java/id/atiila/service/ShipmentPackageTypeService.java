package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.ShipmentPackageType;
import id.atiila.repository.ShipmentPackageTypeRepository;
import id.atiila.repository.search.ShipmentPackageTypeSearchRepository;
import id.atiila.service.dto.ShipmentPackageTypeDTO;
import id.atiila.service.mapper.ShipmentPackageTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ShipmentPackageType.
 * BeSmart Team
 */

@Service
@Transactional
public class ShipmentPackageTypeService {

    private final Logger log = LoggerFactory.getLogger(ShipmentPackageTypeService.class);

    private final ShipmentPackageTypeRepository shipmentPackageTypeRepository;

    private final ShipmentPackageTypeMapper shipmentPackageTypeMapper;

    private final ShipmentPackageTypeSearchRepository shipmentPackageTypeSearchRepository;

    public ShipmentPackageTypeService(ShipmentPackageTypeRepository shipmentPackageTypeRepository, ShipmentPackageTypeMapper shipmentPackageTypeMapper, ShipmentPackageTypeSearchRepository shipmentPackageTypeSearchRepository) {
        this.shipmentPackageTypeRepository = shipmentPackageTypeRepository;
        this.shipmentPackageTypeMapper = shipmentPackageTypeMapper;
        this.shipmentPackageTypeSearchRepository = shipmentPackageTypeSearchRepository;
    }

    /**
     * Save a shipmentPackageType.
     *
     * @param shipmentPackageTypeDTO the entity to save
     * @return the persisted entity
     */
    public ShipmentPackageTypeDTO save(ShipmentPackageTypeDTO shipmentPackageTypeDTO) {
        log.debug("Request to save ShipmentPackageType : {}", shipmentPackageTypeDTO);
        ShipmentPackageType shipmentPackageType = shipmentPackageTypeMapper.toEntity(shipmentPackageTypeDTO);
        shipmentPackageType = shipmentPackageTypeRepository.save(shipmentPackageType);
        ShipmentPackageTypeDTO result = shipmentPackageTypeMapper.toDto(shipmentPackageType);
        shipmentPackageTypeSearchRepository.save(shipmentPackageType);
        return result;
    }

    /**
     *  Get all the shipmentPackageTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentPackageTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShipmentPackageTypes");
        return shipmentPackageTypeRepository.findAll(pageable)
            .map(shipmentPackageTypeMapper::toDto);
    }

    /**
     *  Get one shipmentPackageType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ShipmentPackageTypeDTO findOne(Integer id) {
        log.debug("Request to get ShipmentPackageType : {}", id);
        ShipmentPackageType shipmentPackageType = shipmentPackageTypeRepository.findOne(id);
        return shipmentPackageTypeMapper.toDto(shipmentPackageType);
    }

    /**
     *  Delete the  shipmentPackageType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete ShipmentPackageType : {}", id);
        shipmentPackageTypeRepository.delete(id);
        shipmentPackageTypeSearchRepository.delete(id);
    }

    /**
     * Search for the shipmentPackageType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentPackageTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ShipmentPackageTypes for query {}", query);
        Page<ShipmentPackageType> result = shipmentPackageTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(shipmentPackageTypeMapper::toDto);
    }

    public ShipmentPackageTypeDTO processExecuteData(Integer id, String param, ShipmentPackageTypeDTO dto) {
        ShipmentPackageTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<ShipmentPackageTypeDTO> processExecuteListData(Integer id, String param, Set<ShipmentPackageTypeDTO> dto) {
        Set<ShipmentPackageTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @PostConstruct
    @Transactional
    public void initialize() {
        save(new ShipmentPackageTypeDTO(BaseConstants.PACK_TYPE_INIT_STOCK_UNIT, "Init Stock Unit"));
        save(new ShipmentPackageTypeDTO(BaseConstants.PACK_TYPE_INIT_STOCK_PRODUCT, "Init Stock Product"));
    }

}
