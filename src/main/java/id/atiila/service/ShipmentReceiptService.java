package id.atiila.service;

import id.atiila.domain.ShipmentReceipt;
import id.atiila.repository.ShipmentReceiptRepository;
import id.atiila.repository.search.ShipmentReceiptSearchRepository;
import id.atiila.service.dto.ShipmentReceiptDTO;
import id.atiila.service.mapper.ShipmentReceiptMapper;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ShipmentReceipt.
 * BeSmart Team
 */

@Service
@Transactional
public class ShipmentReceiptService {

    private final Logger log = LoggerFactory.getLogger(ShipmentReceiptService.class);

    private final ShipmentReceiptRepository shipmentReceiptRepository;

    private final ShipmentReceiptMapper shipmentReceiptMapper;

    private final ShipmentReceiptSearchRepository shipmentReceiptSearchRepository;

    public ShipmentReceiptService(ShipmentReceiptRepository shipmentReceiptRepository, ShipmentReceiptMapper shipmentReceiptMapper, ShipmentReceiptSearchRepository shipmentReceiptSearchRepository) {
        this.shipmentReceiptRepository = shipmentReceiptRepository;
        this.shipmentReceiptMapper = shipmentReceiptMapper;
        this.shipmentReceiptSearchRepository = shipmentReceiptSearchRepository;
    }

    /**
     * Save a shipmentReceipt.
     *
     * @param shipmentReceiptDTO the entity to save
     * @return the persisted entity
     */
    public ShipmentReceiptDTO save(ShipmentReceiptDTO shipmentReceiptDTO) {
        log.debug("Request to save ShipmentReceipt : {}", shipmentReceiptDTO);
        ShipmentReceipt shipmentReceipt = shipmentReceiptMapper.toEntity(shipmentReceiptDTO);
        shipmentReceipt = shipmentReceiptRepository.save(shipmentReceipt);
        ShipmentReceiptDTO result = shipmentReceiptMapper.toDto(shipmentReceipt);
        shipmentReceiptSearchRepository.save(shipmentReceipt);
        return result;
    }

    /**
     * Get all the shipmentReceipts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentReceiptDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShipmentReceipts");
        return shipmentReceiptRepository.findActiveShipmentReceipt(pageable)
            .map(shipmentReceiptMapper::toDto);
    }

    /**
     * Get one shipmentReceipt by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ShipmentReceiptDTO findOne(UUID id) {
        log.debug("Request to get ShipmentReceipt : {}", id);
        ShipmentReceipt shipmentReceipt = shipmentReceiptRepository.findOne(id);
        return shipmentReceiptMapper.toDto(shipmentReceipt);
    }

    /**
     * Delete the shipmentReceipt by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ShipmentReceipt : {}", id);
        shipmentReceiptRepository.delete(id);
        shipmentReceiptSearchRepository.delete(id);
    }

    /**
     * Search for the shipmentReceipt corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentReceiptDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of ShipmentReceipts for query {}", query);
        String idShipmentPackage = request.getParameter("idShipmentPackage");
        String idShipmentItem = request.getParameter("idShipmentItem");
        String idOrderItem = request.getParameter("idOrderItem");

        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery().must(queryStringQuery(query));
        //     .filter(QueryBuilders.matchQuery("xxId", xxParam));

        return shipmentReceiptSearchRepository.search(boolQuery, pageable).map(shipmentReceiptMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ShipmentReceiptDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ShipmentReceiptDTO");
        String idShipmentPackage = request.getParameter("idShipmentPackage");
        String idShipmentItem = request.getParameter("idShipmentItem");
        String idOrderItem = request.getParameter("idOrderItem");

        return shipmentReceiptRepository.findByParams(idShipmentPackage, idShipmentItem, idOrderItem, pageable)
            .map(shipmentReceiptMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public ShipmentReceiptDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        ShipmentReceiptDTO r = null;
        return r;
    }

    @Transactional
    public Set<ShipmentReceiptDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<ShipmentReceiptDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public ShipmentReceiptDTO changeShipmentReceiptStatus(ShipmentReceiptDTO dto, Integer id) {
        if (dto != null) {
			ShipmentReceipt e = shipmentReceiptRepository.findOne(dto.getIdReceipt());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        shipmentReceiptSearchRepository.delete(dto.getIdReceipt());
                        break;
                    default:
                        shipmentReceiptSearchRepository.save(e);
                }
				shipmentReceiptRepository.save(e);
			}
		}
        return dto;
    }

    @Async
    public void buildIndex() {
        shipmentReceiptSearchRepository.deleteAll();
        List<ShipmentReceipt> shipmentReceipts = shipmentReceiptRepository.findAll();
        for (ShipmentReceipt m: shipmentReceipts) {
            shipmentReceiptSearchRepository.save(m);
            log.debug("Data shipment receipt save!...");
        }
    }
}
