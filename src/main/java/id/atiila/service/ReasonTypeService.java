package id.atiila.service;

import id.atiila.domain.Motor;
import id.atiila.domain.ReasonType;
import id.atiila.repository.ReasonTypeRepository;
import id.atiila.repository.search.ReasonTypeSearchRepository;
import id.atiila.service.dto.ReasonTypeDTO;
import id.atiila.service.mapper.ReasonTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ReasonType.
 * BeSmart Team
 */

@Service
@Transactional
public class ReasonTypeService {

    private final Logger log = LoggerFactory.getLogger(ReasonTypeService.class);

    private final ReasonTypeRepository reasonTypeRepository;

    private final ReasonTypeMapper reasonTypeMapper;

    private final ReasonTypeSearchRepository reasonTypeSearchRepository;
    public ReasonTypeService(ReasonTypeRepository reasonTypeRepository, ReasonTypeMapper reasonTypeMapper, ReasonTypeSearchRepository reasonTypeSearchRepository) {
        this.reasonTypeRepository = reasonTypeRepository;
        this.reasonTypeMapper = reasonTypeMapper;
        this.reasonTypeSearchRepository = reasonTypeSearchRepository;
    }

    /**
     * Save a reasonType.
     *
     * @param reasonTypeDTO the entity to save
     * @return the persisted entity
     */
    public ReasonTypeDTO save(ReasonTypeDTO reasonTypeDTO) {
        log.debug("Request to save ReasonType : {}", reasonTypeDTO);
        ReasonType reasonType = reasonTypeMapper.toEntity(reasonTypeDTO);
        reasonType = reasonTypeRepository.save(reasonType);
        ReasonTypeDTO result = reasonTypeMapper.toDto(reasonType);
        reasonTypeSearchRepository.save(reasonType);
        return result;
    }

    /**
     *  Get all the reasonTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ReasonTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ReasonTypes");
        return reasonTypeRepository.findAll(pageable)
            .map(reasonTypeMapper::toDto);
    }

    /**
     *  Get one reasonType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ReasonTypeDTO findOne(Integer id) {
        log.debug("Request to get ReasonType : {}", id);
        ReasonType reasonType = reasonTypeRepository.findOne(id);
        return reasonTypeMapper.toDto(reasonType);
    }

    /**
     *  Delete the  reasonType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete ReasonType : {}", id);
        reasonTypeRepository.delete(id);
        reasonTypeSearchRepository.delete(id);
    }

    /**
     * Search for the reasonType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ReasonTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ReasonTypes for query {}", query);
        Page<ReasonType> result = reasonTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(reasonTypeMapper::toDto);
    }

    public ReasonTypeDTO processExecuteData(Integer id, String param, ReasonTypeDTO dto) {
        ReasonTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<ReasonTypeDTO> processExecuteListData(Integer id, String param, Set<ReasonTypeDTO> dto) {
        Set<ReasonTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        reasonTypeSearchRepository.deleteAll();
        List<ReasonType> reasonTypes =  reasonTypeRepository.findAll();
        for (ReasonType m: reasonTypes) {
            reasonTypeSearchRepository.save(m);
            log.debug("Data reason type save !...");
        }
    }

}
