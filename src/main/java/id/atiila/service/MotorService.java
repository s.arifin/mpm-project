package id.atiila.service;

import id.atiila.base.XlsxUtils;
import id.atiila.domain.*;
import id.atiila.repository.InternalRepository;
import id.atiila.repository.MotorRepository;
import id.atiila.repository.search.MotorSearchRepository;
import id.atiila.service.dto.MotorDTO;
import id.atiila.service.dto.PriceComponentDTO;
import id.atiila.service.dto.UnitPriceDTO;
import id.atiila.service.dto.VillageDTO;
import id.atiila.service.mapper.MotorMapper;
import id.atiila.service.mapper.PriceComponentMapper;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Motor.
 * BeSmart Team
 */

@Service
@Transactional
public class MotorService extends ProductBaseService {

    private final Logger log = LoggerFactory.getLogger(MotorService.class);

    private final MotorRepository motorRepository;

    private final MotorMapper motorMapper;

    private final MotorSearchRepository motorSearchRepository;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private PriceUtils priceUtils;

    @Autowired
    private ProductUtils productUtils;

    @Autowired
    private PriceComponentMapper priceComponentMapper;

    public MotorService(MotorRepository motorRepository, MotorMapper motorMapper, MotorSearchRepository motorSearchRepository) {
        this.motorRepository = motorRepository;
        this.motorMapper = motorMapper;
        this.motorSearchRepository = motorSearchRepository;
    }

    public List<PriceComponentDTO> getMotorPrice(String idProduct, String idinternal){
        log.debug("SERVICE Request to get Motor Price");
        List<PriceComponent> l_price = productUtils.getPriceByProductAndInternal(partyUtils.getCurrentInternal(), idProduct);
        List<PriceComponentDTO> l_dto = priceComponentMapper.toDto(l_price);
        return l_dto;
    }

    public List<MotorDTO> findMotorByKeyword(String keyword){
        log.debug("SERVICE find by keyword");
        List<Motor> l_motor = motorRepository.findByIdProductOrDescription(keyword);
        List<MotorDTO> l_dto = motorMapper.toDto(l_motor);

        return l_dto;
    }

    /**
     * Save a motor.
     *
     * @param motorDTO the entity to save
     * @return the persisted entity
     */
    public MotorDTO save(MotorDTO motorDTO) {
        log.debug("Request to save Motor : {}", motorDTO);
        Motor motor = motorMapper.toEntity(motorDTO);

        Boolean isNew = motor.getIdProduct() == null;
        if (isNew) {
            String newValue = null;
            while (newValue == null || motorRepository.findOne(newValue) != null) {
                newValue = numbering.nextValue("idmotor", 4000l).toString();
            }
            motor.setIdProduct(newValue);
        }

        motor = motorRepository.save(motor);
        MotorDTO result = motorMapper.toDto(motor);
        motorSearchRepository.save(motor);
        return result;
    }

    /**
     *  Get all the motors.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MotorDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Motors");
        return motorRepository.findAll(pageable)
            .map(motorMapper::toDto);
    }

    /**
     *  Get one motor by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public MotorDTO findOne(String id) {
        log.debug("Request to get Motor : {}", id);
        Motor motor = motorRepository.findOneWithEagerRelationships(id);
        return motorMapper.toDto(motor);
    }

    /**
     *  Delete the  motor by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Motor : {}", id);
        motorRepository.delete(id);
        motorSearchRepository.delete(id);
    }

    /**
     * Search for the motor corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MotorDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Motors for query {}", query);
        Page<Motor> result = motorSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(motorMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<MotorDTO> getHotItem() {
        log.debug("SERVICE to get hot item with price");
        List<Motor> result = motorRepository.findHotItem();
        List<MotorDTO> l_dto = motorMapper.toDto(result);
        return l_dto;
    }

    @Override
    public Product findById(String id) {
        return motorRepository.findOne(id);
    }

    @Transactional
    public void addUnitPrice(String idInternal, String idMotor, BigDecimal unitPrice, BigDecimal bbn) {
        Internal intr = internalUtils.findOne(idInternal);
        Motor motor = (Motor) productUtils.findOne(idMotor);
        if (intr != null && motor != null) {
            priceUtils.setInternalManufacturePrice(intr.getRoot(), idMotor, unitPrice);
            priceUtils.setBBN(intr.getRoot(), idMotor, bbn);
        }
    }

    @Transactional
    public void buildData() {
        try {

            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/unitprice.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/unitprice.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<UnitPriceDTO> prices = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (UnitPriceDTO o: prices) {
                    if (o.getIdProduct() != null && o.getIdInternal() != null) {
                        addUnitPrice(o.getIdInternal(), o.getIdProduct(), o.getUnitPrice(), o.getBbn());
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }


    @Async
    public void buildIndex() {
        motorSearchRepository.deleteAll();
        List<Motor> motors =  motorRepository.findAll();
        // motorSearchRepository.save(motors);
        for (Motor m: motors) {
            motorSearchRepository.save(m);
            log.debug("Data motor save !...");
        }
    }
}
