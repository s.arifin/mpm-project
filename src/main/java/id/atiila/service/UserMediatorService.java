package id.atiila.service;

import id.atiila.domain.Person;
import id.atiila.domain.UserMediator;
import id.atiila.repository.UserMediatorRepository;
import id.atiila.repository.search.UserMediatorSearchRepository;
import id.atiila.service.dto.UserMediatorDTO;
import id.atiila.service.mapper.UserMediatorMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.Set;

import java.util.UUID;
import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing UserMediator.
 * BeSmart Team
 */

@Service
@Transactional
public class UserMediatorService {

    private final Logger log = LoggerFactory.getLogger(UserMediatorService.class);

    private final UserMediatorRepository userMediatorRepository;

    private final UserMediatorMapper userMediatorMapper;

    private final UserMediatorSearchRepository userMediatorSearchRepository;

    @Autowired
    private PartyUtils partyUtils;

    public UserMediatorService(UserMediatorRepository userMediatorRepository, UserMediatorMapper userMediatorMapper, UserMediatorSearchRepository userMediatorSearchRepository) {
        this.userMediatorRepository = userMediatorRepository;
        this.userMediatorMapper = userMediatorMapper;
        this.userMediatorSearchRepository = userMediatorSearchRepository;
    }

    /**
     * Save a userMediator.
     *
     * @param userMediatorDTO the entity to save
     * @return the persisted entity
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public UserMediatorDTO save(UserMediatorDTO userMediatorDTO) {
        log.debug("Request to save UserMediator : {}", userMediatorDTO);
        UserMediator userMediator = userMediatorMapper.toEntity(userMediatorDTO);
        if (userMediator.getPerson().getIdParty() == null) {
            Person p = partyUtils.buildPerson(userMediator.getPerson());
            userMediator.setPerson(p);
        }
        userMediator = userMediatorRepository.save(userMediator);
        partyUtils.buildAppUser(userMediator);
        UserMediatorDTO result = userMediatorMapper.toDto(userMediator);
        userMediatorSearchRepository.save(userMediator);
        return result;
    }

    /**
     * Get all the userMediators.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserMediatorDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserMediators");
        return userMediatorRepository.findActiveUserMediator(pageable)
            .map(userMediatorMapper::toDto);
    }

    /**
     * Get one userMediator by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public UserMediatorDTO findOne(UUID id) {
        log.debug("Request to get UserMediator : {}", id);
        UserMediator userMediator = userMediatorRepository.findOne(id);
        return userMediatorMapper.toDto(userMediator);
    }

    /**
     * Delete the userMediator by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete UserMediator : {}", id);
        userMediatorRepository.delete(id);
        userMediatorSearchRepository.delete(id);
    }

    /**
     * Search for the userMediator corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserMediatorDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of UserMediators for query {}", query);
        Page<UserMediator> result = userMediatorSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(userMediatorMapper::toDto);
    }

    @Transactional(readOnly = true)
    public UserMediatorDTO processExecuteData(Integer id, String param, UserMediatorDTO dto) {
        UserMediatorDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional(readOnly = true)
    public Set<UserMediatorDTO> processExecuteListData(Integer id, String param, Set<UserMediatorDTO> dto) {
        Set<UserMediatorDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional(readOnly = true)
    public UserMediatorDTO changeUserMediatorStatus(UserMediatorDTO dto, Integer id) {
        if (dto != null) {
			UserMediator e = userMediatorRepository.findOne(dto.getIdUserMediator());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        userMediatorSearchRepository.delete(dto.getIdUserMediator());
                        break;
                    default:
                        userMediatorSearchRepository.save(e);
                }
				userMediatorRepository.save(e);
			}
		}
        return dto;
    }
}
