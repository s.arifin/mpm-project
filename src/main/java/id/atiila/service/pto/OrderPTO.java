package id.atiila.service.pto;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class OrderPTO implements Serializable {

    private Set<Integer> status = new HashSet<>();

    private Set<Integer> statusNotIn = new HashSet<>();

    private ZonedDateTime orderDateFrom;

    private ZonedDateTime orderDateThru;

    private LocalDateTime appDateFrom;

    private LocalDateTime appDateThru;

    private ZonedDateTime ivuDateFrom;

    private ZonedDateTime ivuDateThru;

    private UUID leasingCompanyId;

    private UUID coordinatorSalesId;

    private UUID salesmanId;

    private Integer unitStatus;

    private String internalId;

    private Integer statusFaktur;

    private Set<Integer> statusAL = new HashSet<>();

    public Integer getStatusFaktur() { return statusFaktur; }

    public void setStatusFaktur(Integer statusFaktur) { this.statusFaktur = statusFaktur; }

    public Set<Integer> getStatus() { return status; }

    public void setStatus(Set<Integer> status) { this.status = status; }

    public Set<Integer> getStatusNotIn() { return statusNotIn; }

    public void setStatusNotIn(Set<Integer> statusNotIn) { this.statusNotIn = statusNotIn; }

    public ZonedDateTime getOrderDateFrom() { return orderDateFrom; }

    public void setOrderDateFrom(ZonedDateTime orderDateFrom) { this.orderDateFrom = orderDateFrom; }

    public ZonedDateTime getOrderDateThru() { return orderDateThru; }

    public void setOrderDateThru(ZonedDateTime orderDateThru) { this.orderDateThru = orderDateThru; }

    public ZonedDateTime getIvuDateFrom() { return ivuDateFrom; }

    public void setIvuDateFrom(ZonedDateTime ivuDateFrom) { this.ivuDateFrom = ivuDateFrom; }

    public ZonedDateTime getIvuDateThru() { return ivuDateThru; }

    public void setIvuDateThru(ZonedDateTime ivuDateThru) { this.ivuDateThru = ivuDateThru; }

    public UUID getLeasingCompanyId() { return leasingCompanyId; }

    public void setLeasingCompanyId(UUID leasingCompanyId) { this.leasingCompanyId = leasingCompanyId; }

    public UUID getCoordinatorSalesId() { return coordinatorSalesId; }

    public void setCoordinatorSalesId(UUID coordinatorSalesId) { this.coordinatorSalesId = coordinatorSalesId; }

    public UUID getSalesmanId() { return salesmanId; }

    public void setSalesmanId(UUID salesmanId) { this.salesmanId = salesmanId; }

    public Integer getUnitStatus() { return unitStatus; }

    public void setUnitStatus(Integer unitStatus) { this.unitStatus = unitStatus; }

    public String getInternalId() { return internalId; }

    public void setInternalId(String internalId) { this.internalId = internalId; }

    public LocalDateTime getAppDateFrom() { return appDateFrom; }

    public void setAppDateFrom(LocalDateTime appDateFrom) { this.appDateFrom = appDateFrom; }

    public LocalDateTime getAppDateThru() { return appDateThru; }

    public void setAppDateThru(LocalDateTime appDateThru) { this.appDateThru = appDateThru; }

    public Set<Integer> getStatusAL() { return statusAL; }

    public void setStatusAL(Set<Integer> statusAL) { this.statusAL = statusAL; }
}
