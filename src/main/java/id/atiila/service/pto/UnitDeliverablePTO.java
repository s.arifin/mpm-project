package id.atiila.service.pto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class UnitDeliverablePTO implements Serializable {

    private Set<Integer> status = new HashSet<>();

    private Set<Integer> statusNotIn = new HashSet<>();

    private ZonedDateTime deliverableDateFrom;

    private ZonedDateTime deliverableDateThru;

    private UUID leasingCompanyId;

    private UUID coordinatorSalesId;

    private UUID salesmanId;

    private Integer idDeliverableType;

    private Integer unitStatus;

    private String internalId;

    private String dataParam;

    public String getDataParam() {
        return dataParam;
    }

    public void setDataParam(String dataParam) {
        this.dataParam = dataParam;
    }

    public Integer getIdDeliverableType() {
        return idDeliverableType;
    }

    public void setIdDeliverableType(Integer idDeliverableType) {
        this.idDeliverableType = idDeliverableType;
    }

    public ZonedDateTime getDeliverableDateFrom() {
        return deliverableDateFrom;
    }

    public void setDeliverableDateFrom(ZonedDateTime deliverableDateFrom) {
        this.deliverableDateFrom = deliverableDateFrom;
    }

    public ZonedDateTime getDeliverableDateThru() {
        return deliverableDateThru;
    }

    public void setDeliverableDateThru(ZonedDateTime deliverableDateThru) {
        this.deliverableDateThru = deliverableDateThru;
    }

    public Set<Integer> getStatus() { return status; }

    public void setStatus(Set<Integer> status) { this.status = status; }

    public Set<Integer> getStatusNotIn() { return statusNotIn; }

    public void setStatusNotIn(Set<Integer> statusNotIn) { this.statusNotIn = statusNotIn; }

    public UUID getLeasingCompanyId() { return leasingCompanyId; }

    public void setLeasingCompanyId(UUID leasingCompanyId) { this.leasingCompanyId = leasingCompanyId; }

    public UUID getCoordinatorSalesId() { return coordinatorSalesId; }

    public void setCoordinatorSalesId(UUID coordinatorSalesId) { this.coordinatorSalesId = coordinatorSalesId; }

    public UUID getSalesmanId() { return salesmanId; }

    public void setSalesmanId(UUID salesmanId) { this.salesmanId = salesmanId; }

    public Integer getUnitStatus() { return unitStatus; }

    public void setUnitStatus(Integer unitStatus) { this.unitStatus = unitStatus; }

    public String getInternalId() { return internalId; }

    public void setInternalId(String internalId) { this.internalId = internalId; }
}
