package id.atiila.service.pto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class SuspectPTO implements Serializable {

    private Set<Integer> status = new HashSet<>();

    private String marketName;

    private Integer workTypeId;

    private ZonedDateTime salesDateFrom;

    private ZonedDateTime salesDateThru;

    private UUID districtId;

    private UUID coordinatorSalesId;

    private UUID salesmanId;

    private Integer saleTypeId;

    private Integer suspectTypeId;

    private String internalId;

    public Set<Integer> getStatus() { return status; }

    public void setStatus(Set<Integer> status) { this.status = status; }

    public String getMarketName() { return marketName; }

    public void setMarketName(String marketName) { this.marketName = marketName; }

    public Integer getWorkTypeId() { return workTypeId; }

    public void setWorkTypeId(Integer workTypeId) { this.workTypeId = workTypeId; }

    public ZonedDateTime getSalesDateFrom() { return salesDateFrom; }

    public void setSalesDateFrom(ZonedDateTime salesDateFrom) { this.salesDateFrom = salesDateFrom; }

    public ZonedDateTime getSalesDateThru() { return salesDateThru; }

    public void setSalesDateThru(ZonedDateTime salesDateThru) { this.salesDateThru = salesDateThru; }

    public UUID getDistrictId() { return districtId; }

    public void setDistrictId(UUID districtId) { this.districtId = districtId; }

    public UUID getCoordinatorSalesId() { return coordinatorSalesId; }

    public void setCoordinatorSalesId(UUID coordinatorSalesId) { this.coordinatorSalesId = coordinatorSalesId; }

    public UUID getSalesmanId() { return salesmanId; }

    public void setSalesmanId(UUID salesmanId) { this.salesmanId = salesmanId; }

    public Integer getSaleTypeId() { return saleTypeId; }

    public void setSaleTypeId(Integer saleTypeId) { this.saleTypeId = saleTypeId; }

    public Integer getSuspectTypeId() { return suspectTypeId; }

    public void setSuspectTypeId(Integer suspectTypeId) { this.suspectTypeId = suspectTypeId; }

    public String getInternalId() { return internalId; }

    public void setInternalId(String internalId) { this.internalId = internalId; }
}
