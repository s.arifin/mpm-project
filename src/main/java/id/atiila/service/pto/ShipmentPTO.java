package id.atiila.service.pto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

public class ShipmentPTO implements Serializable {

    private Set<Integer> status = new HashSet<>();

    private Set<Integer> statusNotIn = new HashSet<>();

    private ZonedDateTime packageReceiptDateFrom;

    private ZonedDateTime packageReceiptDateThru;

    private ZonedDateTime shipmentIncomingDateFrom;

    private ZonedDateTime shipmentIncomingDateThru;

    private String internalId;

    public Set<Integer> getStatus() { return status; }

    public void setStatus(Set<Integer> status) { this.status = status; }

    public Set<Integer> getStatusNotIn() { return statusNotIn; }

    public void setStatusNotIn(Set<Integer> statusNotIn) { this.statusNotIn = statusNotIn; }

    public ZonedDateTime getPackageReceiptDateFrom() { return packageReceiptDateFrom; }

    public void setPackageReceiptDateFrom(ZonedDateTime packageReceiptDateFrom) { this.packageReceiptDateFrom = packageReceiptDateFrom; }

    public ZonedDateTime getPackageReceiptDateThru() { return packageReceiptDateThru; }

    public void setPackageReceiptDateThru(ZonedDateTime packageReceiptDateThru) { this.packageReceiptDateThru = packageReceiptDateThru; }

    public ZonedDateTime getShipmentIncomingDateFrom() { return shipmentIncomingDateFrom; }

    public void setShipmentIncomingDateFrom(ZonedDateTime shipmentIncomingDateFrom) { this.shipmentIncomingDateFrom = shipmentIncomingDateFrom; }

    public ZonedDateTime getShipmentIncomingDateThru() { return shipmentIncomingDateThru; }

    public void setShipmentIncomingDateThru(ZonedDateTime shipmentIncomingDateThru) { this.shipmentIncomingDateThru = shipmentIncomingDateThru; }

    public String getInternalId() { return internalId; }

    public void setInternalId(String internalId) { this.internalId = internalId; }
}
