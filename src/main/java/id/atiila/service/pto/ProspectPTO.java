package id.atiila.service.pto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class ProspectPTO implements Serializable{

    private Set<Integer> status = new HashSet<>();

    private UUID salesmanId;

    private Integer workTypeId;

    private UUID districtId;

    private Integer saleTypeId;

    public Set<Integer> getStatus() { return status; }

    public void setStatus(Set<Integer> status) { this.status = status; }

    public UUID getSalesmanId() { return salesmanId; }

    public void setSalesmanId(UUID salesmanId) { this.salesmanId = salesmanId; }

    public Integer getWorkTypeId() { return workTypeId; }

    public void setWorkTypeId(Integer workTypeId) { this.workTypeId = workTypeId; }

    public UUID getDistrictId() { return districtId; }

    public void setDistrictId(UUID districtId) { this.districtId = districtId; }

    public Integer getSaleTypeId() { return saleTypeId; }

    public void setSaleTypeId(Integer saleTypeId) { this.saleTypeId = saleTypeId; }
}
