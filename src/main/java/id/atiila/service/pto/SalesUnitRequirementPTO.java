package id.atiila.service.pto;

import java.io.Serializable;

public class SalesUnitRequirementPTO implements Serializable {

    private String internalId;

    private String nama;

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
