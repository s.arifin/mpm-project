package id.atiila.service.pto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class ShipmentOutgoingPTO implements Serializable {
    private String reffNumber;
    private ZonedDateTime orderDateFrom;
    private ZonedDateTime orderDateThru;
    private String idFrame;
    private String coordinatorSalesId;
    private UUID salesmanId;
    private String color;
    private String shipmentNumber;
    private String shipmentAddress;
    private Integer idStatusType;
    private String internalId;
    private String dataParam;
    private String queryFor;
    private Boolean filterType;
    private Integer type;

    public Boolean getFilterType() {
        return filterType;
    }

    public void setFilterType(Boolean filterType) {
        this.filterType = filterType;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getQueryFor() {
        return queryFor;
    }

    public void setQueryFor(String queryFor) {
        this.queryFor = queryFor;
    }

    public String getReffNumber() {
        return reffNumber;
    }

    public void setReffNumber(String reffNumber) {
        this.reffNumber = reffNumber;
    }

    public ZonedDateTime getOrderDateFrom() {
        return orderDateFrom;
    }

    public void setOrderDateFrom(ZonedDateTime orderDateFrom) {
        this.orderDateFrom = orderDateFrom;
    }

    public ZonedDateTime getOrderDateThru() {
        return orderDateThru;
    }

    public void setOrderDateThru(ZonedDateTime orderDateThru) {
        this.orderDateThru = orderDateThru;
    }

    public String getIdFrame() {
        return idFrame;
    }

    public void setIdFrame(String idFrame) {
        this.idFrame = idFrame;
    }

    public String getCoordinatorSalesId() {
        return coordinatorSalesId;
    }

    public void setCoordinatorSalesId(String coordinatorSalesId) {
        this.coordinatorSalesId = coordinatorSalesId;
    }

    public UUID getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(UUID salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getShipmentNumber() {
        return shipmentNumber;
    }

    public void setShipmentNumber(String shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
    }

    public String getShipmentAddress() {
        return shipmentAddress;
    }

    public void setShipmentAddress(String shipmentAddress) {
        this.shipmentAddress = shipmentAddress;
    }

    public Integer getIdStatusType() {
        return idStatusType;
    }

    public void setIdStatusType(Integer idStatusType) {
        this.idStatusType = idStatusType;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getDataParam() {
        return dataParam;
    }

    public void setDataParam(String dataParam) {
        this.dataParam = dataParam;
    }
}
