package id.atiila.service;

import id.atiila.domain.CommunicationEvent;
import id.atiila.repository.CommunicationEventRepository;
import id.atiila.repository.search.CommunicationEventSearchRepository;
import id.atiila.service.dto.CommunicationEventDTO;
import id.atiila.service.mapper.CommunicationEventMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing CommunicationEvent.
 * BeSmart Team
 */

@Service
@Transactional
public class CommunicationEventService {

    private final Logger log = LoggerFactory.getLogger(CommunicationEventService.class);

    private final CommunicationEventRepository communicationEventRepository;

    private final CommunicationEventMapper communicationEventMapper;

    private final CommunicationEventSearchRepository communicationEventSearchRepository;

    public CommunicationEventService(CommunicationEventRepository communicationEventRepository, CommunicationEventMapper communicationEventMapper, CommunicationEventSearchRepository communicationEventSearchRepository) {
        this.communicationEventRepository = communicationEventRepository;
        this.communicationEventMapper = communicationEventMapper;
        this.communicationEventSearchRepository = communicationEventSearchRepository;
    }

    /**
     * Save a communicationEvent.
     *
     * @param communicationEventDTO the entity to save
     * @return the persisted entity
     */
    public CommunicationEventDTO save(CommunicationEventDTO communicationEventDTO) {
        log.debug("Request to save CommunicationEvent : {}", communicationEventDTO);
        CommunicationEvent communicationEvent = communicationEventMapper.toEntity(communicationEventDTO);
        communicationEvent = communicationEventRepository.save(communicationEvent);
        CommunicationEventDTO result = communicationEventMapper.toDto(communicationEvent);
        communicationEventSearchRepository.save(communicationEvent);
        return result;
    }

    /**
     *  Get all the communicationEvents.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CommunicationEventDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CommunicationEvents");
        return communicationEventRepository.findActiveCommunicationEvent(pageable)
            .map(communicationEventMapper::toDto);
    }

    /**
     *  Get one communicationEvent by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CommunicationEventDTO findOne(UUID id) {
        log.debug("Request to get CommunicationEvent : {}", id);
        CommunicationEvent communicationEvent = communicationEventRepository.findOne(id);
        return communicationEventMapper.toDto(communicationEvent);
    }

    /**
     *  Delete the  communicationEvent by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete CommunicationEvent : {}", id);
        communicationEventRepository.delete(id);
        communicationEventSearchRepository.delete(id);
    }

    /**
     * Search for the communicationEvent corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CommunicationEventDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CommunicationEvents for query {}", query);
        Page<CommunicationEvent> result = communicationEventSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(communicationEventMapper::toDto);
    }

    public CommunicationEventDTO processExecuteData(Integer id, String param, CommunicationEventDTO dto) {
        CommunicationEventDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<CommunicationEventDTO> processExecuteListData(Integer id, String param, Set<CommunicationEventDTO> dto) {
        Set<CommunicationEventDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public CommunicationEventDTO changeCommunicationEventStatus(CommunicationEventDTO dto, Integer id) {
        if (dto != null) {
			CommunicationEvent e = communicationEventRepository.findOne(dto.getIdCommunicationEvent());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        communicationEventSearchRepository.delete(dto.getIdCommunicationEvent());
                        break;
                    default:
                        communicationEventSearchRepository.save(e);
                }
				communicationEventRepository.save(e);
			}
		}
        return dto;
    }
}
