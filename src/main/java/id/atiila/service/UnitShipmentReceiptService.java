package id.atiila.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.InventoryItem;
import id.atiila.domain.OrderItem;
import id.atiila.domain.PackageReceipt;
import id.atiila.domain.UnitShipmentReceipt;
import id.atiila.repository.InventoryItemRepository;
import id.atiila.repository.PackageReceiptRepository;
import id.atiila.repository.UnitShipmentReceiptRepository;
import id.atiila.repository.search.UnitShipmentReceiptSearchRepository;
import id.atiila.service.dto.UnitShipmentReceiptDTO;
import id.atiila.service.mapper.UnitShipmentReceiptMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing UnitShipmentReceipt.
 * BeSmart Team
 */

@Service
@Transactional
public class UnitShipmentReceiptService {

    private final Logger log = LoggerFactory.getLogger(UnitShipmentReceiptService.class);

    private final UnitShipmentReceiptRepository unitShipmentReceiptRepository;

    private final UnitShipmentReceiptMapper unitShipmentReceiptMapper;

    private final UnitShipmentReceiptSearchRepository unitShipmentReceiptSearchRepository;

    @Autowired
    private PackageReceiptRepository packageReceiptRepository;

    @Autowired
    private InventoryItemRepository inventoryItemRepository;

    public UnitShipmentReceiptService(UnitShipmentReceiptRepository unitShipmentReceiptRepository, UnitShipmentReceiptMapper unitShipmentReceiptMapper, UnitShipmentReceiptSearchRepository unitShipmentReceiptSearchRepository) {
        this.unitShipmentReceiptRepository = unitShipmentReceiptRepository;
        this.unitShipmentReceiptMapper = unitShipmentReceiptMapper;
        this.unitShipmentReceiptSearchRepository = unitShipmentReceiptSearchRepository;
    }

    /**
     * Save a unitShipmentReceipt.
     *
     * @param unitShipmentReceiptDTO the entity to save
     * @return the persisted entity
     */
    public UnitShipmentReceiptDTO save(UnitShipmentReceiptDTO unitShipmentReceiptDTO) {
        log.debug("Request to save UnitShipmentReceipt : {}", unitShipmentReceiptDTO);
        PackageReceipt pcr = unitShipmentReceiptRepository.findPackageReceipt(unitShipmentReceiptDTO.getShipmentPackageId());
//        log.debug("isi package receipt : {}", pcr);
//        OrderItem oi = unitShipmentReceiptRepository.findOrderItem(unitShipmentReceiptDTO.getIdFrame(), pcr.getVendorInvoice());
//        log.debug("isi order item : {}", oi);

//        if (oi != null){
//            unitShipmentReceiptDTO.setOrderItemId(oi.getIdOrderItem());
//        } else {
//            throw  new DmsException("Order Tidak Ditemukan");
//        }
        UnitShipmentReceipt unitShipmentReceipt = unitShipmentReceiptMapper.toEntity(unitShipmentReceiptDTO);
        if (unitShipmentReceiptDTO.getIdFrame() != null || unitShipmentReceiptDTO.getIdMachine() != null) {
            log.debug("isi params: " + unitShipmentReceiptDTO.getIdInternal());
            List<InventoryItem> r = inventoryItemRepository.queryForTransferUnit(unitShipmentReceiptDTO.getIdFrame(), unitShipmentReceiptDTO.getIdMachine(), unitShipmentReceiptDTO.getIdInternal());
            log.debug("isi find motor: " + r);

            if (r.size() > 0) {
                throw new DmsException("Data Shipping List dengan Noka " + unitShipmentReceiptDTO.getIdFrame() + " Nosin " + unitShipmentReceiptDTO.getIdMachine() + "sudah ada");
//                PackageReceipt pr = p.get(0);
//                if (pr != null){
//                    throw new DmsException("Data Shipping List Sudah Ada");
//                }
            }
        } else {
            throw  new DmsException("Data Belum Lengkap");
        }
        if (unitShipmentReceiptDTO.getYearAssembly() != null){
            unitShipmentReceipt = unitShipmentReceiptRepository.save(unitShipmentReceipt);
        } else {
            throw  new DmsException("Year Assembly Belum Diisi");
        }
        UnitShipmentReceiptDTO result = unitShipmentReceiptMapper.toDto(unitShipmentReceipt);
        unitShipmentReceiptSearchRepository.save(unitShipmentReceipt);
        return result;
    }

    /**
     * Get all the unitShipmentReceipts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UnitShipmentReceiptDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UnitShipmentReceipts");
        return unitShipmentReceiptRepository.findActiveUnitShipmentReceipt(pageable)
            .map(unitShipmentReceiptMapper::toDto);
    }

    /**
     * Get one unitShipmentReceipt by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public UnitShipmentReceiptDTO findOne(UUID id) {
        log.debug("Request to get UnitShipmentReceipt : {}", id);
        UnitShipmentReceipt unitShipmentReceipt = unitShipmentReceiptRepository.findOne(id);
        return unitShipmentReceiptMapper.toDto(unitShipmentReceipt);
    }

    /**
     * Delete the unitShipmentReceipt by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete UnitShipmentReceipt : {}", id);
        unitShipmentReceiptRepository.delete(id);
        unitShipmentReceiptSearchRepository.delete(id);
    }

    /**
     * Search for the unitShipmentReceipt corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UnitShipmentReceiptDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of UnitShipmentReceipts for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idShipmentPackage = request.getParameter("idShipmentPackage");
        String idShipmentItem = request.getParameter("idShipmentItem");
        String idOrderItem = request.getParameter("idOrderItem");

        if (filterName != null) {
        }
        Page<UnitShipmentReceipt> result = unitShipmentReceiptSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(unitShipmentReceiptMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<UnitShipmentReceiptDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered UnitShipmentReceiptDTO");
        String idShipmentPackage = request.getParameter("idShipmentPackage");
        Double qtyAccept = request.getParameter("qtyAccept") == null ? null : Double.valueOf(request.getParameter("qtyAccept"));

        if (idShipmentPackage != null && qtyAccept == 1) {
            log.debug("jumlah qty accept == " + qtyAccept);
            return unitShipmentReceiptRepository.findByParams(UUID.fromString(idShipmentPackage), 1d, pageable)
                .map(unitShipmentReceiptMapper::toDto);
        }
        if (idShipmentPackage != null && qtyAccept == 2) {
            log.debug("jumlah qty accept == " + qtyAccept);
            return unitShipmentReceiptRepository.findByParams(UUID.fromString(idShipmentPackage), 0d, pageable)
                .map(unitShipmentReceiptMapper::toDto);
        }
        return unitShipmentReceiptRepository.findAll(pageable).map(unitShipmentReceiptMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public UnitShipmentReceiptDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        String execute = request.getParameter("execute").toString();
        UnitShipmentReceiptDTO r = new UnitShipmentReceiptDTO();

        if (execute.equals("CheckUnitOnShippingList")){
            ObjectMapper mapper = new ObjectMapper();
            mapper.findAndRegisterModules();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

            r = mapper.convertValue(item.get("item"), new TypeReference<UnitShipmentReceiptDTO>() {});
            UnitShipmentReceipt p = unitShipmentReceiptRepository.findOneByIdFrameAndIdMachineAndShipmentPackage(
                r.getIdFrame(), r.getIdMachine(), r.getShipmentPackageId()
            );

            if (p != null){
                r = unitShipmentReceiptMapper.toDto(p);
            }
            else {
                throw new DmsException("Unit Tidak Ditemukan !");
            }
        }

        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Set<UnitShipmentReceiptDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        String execute = request.getParameter("execute").toString();
        Set<UnitShipmentReceiptDTO> r = new HashSet<>();
        PackageReceipt p = null;

        if (execute.equals("UnitCheckSubmit")){

            ObjectMapper mapper = new ObjectMapper();
            mapper.findAndRegisterModules();

            List<Object> objects = (List<Object>) item.get("items");
            r = mapper.convertValue(objects, new TypeReference<Set<UnitShipmentReceiptDTO>>() { });

            if (r.iterator().hasNext()){
                p = packageReceiptRepository.findOne(r.iterator().next().getShipmentPackageId());
                if (p != null) {
                    if (p.getCurrentStatus().equals(BaseConstants.STATUS_DRAFT)) {
                        p.setStatus(BaseConstants.STATUS_OPEN);
                        packageReceiptRepository.save(p);
                    }
                }
            }

            for(UnitShipmentReceiptDTO usrDTO : r){
                UnitShipmentReceipt usr = unitShipmentReceiptRepository
                    .findOneByIdFrameAndIdMachineAndShipmentPackage(usrDTO.getIdFrame(),usrDTO.getIdMachine(),usrDTO.getShipmentPackageId());

                // Check if  Have Value
                if (usr != null && usr.getReceipt() == false){
                    usr.setDateReceipt(ZonedDateTime.now());
                    usr.setReceipt(true);
                    unitShipmentReceiptRepository.save(usr);
                }
                else if (usr == null){
                    usr = new UnitShipmentReceipt();
                    usr.setIdFrame(usrDTO.getIdFrame());
                    usr.setIdMachine(usrDTO.getIdMachine());
                    usr.setDateReceipt(ZonedDateTime.now());
                    usr.setQtyAccept(0.0);
                    usr.setQtyReject(0.0);
                    usr.setReceipt(true);
                    usr.setShipmentPackage(packageReceiptRepository.findOne(usrDTO.getShipmentPackageId()));
                    unitShipmentReceiptRepository.save(usr);
                }
            }
        }
        return r;
    }

    @Transactional
    public UnitShipmentReceiptDTO changeUnitShipmentReceiptStatus(UnitShipmentReceiptDTO dto, Integer id) {
        if (dto != null) {
			UnitShipmentReceipt e = unitShipmentReceiptRepository.findOne(dto.getIdReceipt());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        unitShipmentReceiptSearchRepository.delete(dto.getIdReceipt());
                        break;
                    default:
                        unitShipmentReceiptSearchRepository.save(e);
                }
				unitShipmentReceiptRepository.save(e);
			}
		}
        return dto;
    }
}
