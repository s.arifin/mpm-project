package id.atiila.service;

import id.atiila.domain.Quote;
import id.atiila.repository.QuoteRepository;
import id.atiila.repository.search.QuoteSearchRepository;
import id.atiila.service.dto.QuoteDTO;
import id.atiila.service.mapper.QuoteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing Quote.
 * BeSmart Team
 */

@Service
@Transactional
public class QuoteService {

    private final Logger log = LoggerFactory.getLogger(QuoteService.class);

    private final QuoteRepository quoteRepository;

    private final QuoteMapper quoteMapper;

    private final QuoteSearchRepository quoteSearchRepository;

    public QuoteService(QuoteRepository quoteRepository, QuoteMapper quoteMapper, QuoteSearchRepository quoteSearchRepository) {
        this.quoteRepository = quoteRepository;
        this.quoteMapper = quoteMapper;
        this.quoteSearchRepository = quoteSearchRepository;
    }

    /**
     * Save a quote.
     *
     * @param quoteDTO the entity to save
     * @return the persisted entity
     */
    public QuoteDTO save(QuoteDTO quoteDTO) {
        log.debug("Request to save Quote : {}", quoteDTO);
        Quote quote = quoteMapper.toEntity(quoteDTO);
        quote = quoteRepository.save(quote);
        QuoteDTO result = quoteMapper.toDto(quote);
        quoteSearchRepository.save(quote);
        return result;
    }

    /**
     *  Get all the quotes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<QuoteDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Quotes");
        return quoteRepository.findActiveQuote(pageable)
            .map(quoteMapper::toDto);
    }

    /**
     *  Get one quote by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public QuoteDTO findOne(UUID id) {
        log.debug("Request to get Quote : {}", id);
        Quote quote = quoteRepository.findOne(id);
        return quoteMapper.toDto(quote);
    }

    /**
     *  Delete the  quote by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Quote : {}", id);
        quoteRepository.delete(id);
        quoteSearchRepository.delete(id);
    }

    /**
     * Search for the quote corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<QuoteDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Quotes for query {}", query);
        Page<Quote> result = quoteSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(quoteMapper::toDto);
    }

    public QuoteDTO processExecuteData(Integer id, String param, QuoteDTO dto) {
        QuoteDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<QuoteDTO> processExecuteListData(Integer id, String param, Set<QuoteDTO> dto) {
        Set<QuoteDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

    public QuoteDTO changeQuoteStatus(QuoteDTO dto, Integer id) {
        if (dto != null) {
			Quote e = quoteRepository.findOne(dto.getIdQuote());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        quoteSearchRepository.delete(dto.getIdQuote());
                        break;
                    default:
                        quoteSearchRepository.save(e);
                }
				quoteRepository.save(e);
			}
		}
        return dto;
    }
}
