package id.atiila.service;

import id.atiila.domain.Billing;
import id.atiila.repository.BillingRepository;
import id.atiila.repository.search.BillingSearchRepository;
import id.atiila.service.dto.BillingDTO;
import id.atiila.service.mapper.BillingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing Billing.
 * BeSmart Team
 */

@Service
@Transactional
public class BillingService {

    private final Logger log = LoggerFactory.getLogger(BillingService.class);

    private final BillingRepository billingRepository;

    private final BillingMapper billingMapper;

    private final BillingSearchRepository billingSearchRepository;

    public BillingService(BillingRepository billingRepository, BillingMapper billingMapper, BillingSearchRepository billingSearchRepository) {
        this.billingRepository = billingRepository;
        this.billingMapper = billingMapper;
        this.billingSearchRepository = billingSearchRepository;
    }

    /**
     * Save a billing.
     *
     * @param billingDTO the entity to save
     * @return the persisted entity
     */
    public BillingDTO save(BillingDTO billingDTO) {
        log.debug("Request to save Billing : {}", billingDTO);
        Billing billing = billingMapper.toEntity(billingDTO);
        billing = billingRepository.save(billing);
        BillingDTO result = billingMapper.toDto(billing);
        billingSearchRepository.save(billing);
        return result;
    }

    /**
     * Get all the billings.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BillingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Billings");
        return billingRepository.findActiveBilling(pageable)
            .map(billingMapper::toDto);
    }

    /**
     * Get one billing by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BillingDTO findOne(UUID id) {
        log.debug("Request to get Billing : {}", id);
        Billing billing = billingRepository.findOne(id);
        return billingMapper.toDto(billing);
    }

    /**
     * Delete the billing by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Billing : {}", id);
        billingRepository.delete(id);
        billingSearchRepository.delete(id);
    }

    /**
     * Search for the billing corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BillingDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of Billings for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idBillingType = request.getParameter("idBillingType");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");
        String idBillFrom = request.getParameter("idBillFrom");

        if (filterName != null) {
        }
        Page<Billing> result = billingSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(billingMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<BillingDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered BillingDTO");
        String idBillingType = request.getParameter("idBillingType");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");
        String idBillFrom = request.getParameter("idBillFrom");

        return billingRepository.findByParams(idBillingType, idInternal, idBillTo, idBillFrom, pageable)
            .map(billingMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public BillingDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        BillingDTO r = null;
        return r;
    }

    @Transactional
    public Set<BillingDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<BillingDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public BillingDTO changeBillingStatus(BillingDTO dto, Integer id) {
        if (dto != null) {
			Billing e = billingRepository.findOne(dto.getIdBilling());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        billingSearchRepository.delete(dto.getIdBilling());
                        break;
                    default:
                        billingSearchRepository.save(e);
                }
				billingRepository.save(e);
			}
		}
        return dto;
    }
}
