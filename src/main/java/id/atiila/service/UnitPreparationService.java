package id.atiila.service;

import id.atiila.domain.MasterNumbering;
import id.atiila.domain.UnitPreparation;
import id.atiila.repository.UnitPreparationRepository;
import id.atiila.repository.search.UnitPreparationSearchRepository;
import id.atiila.service.dto.UnitPreparationDTO;
import id.atiila.service.mapper.UnitPreparationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing UnitPreparation.
 * atiila consulting
 */

@Service
@Transactional
public class UnitPreparationService {

    private final Logger log = LoggerFactory.getLogger(UnitPreparationService.class);

    private final UnitPreparationRepository unitPreparationRepository;

    private final UnitPreparationMapper unitPreparationMapper;

    private final UnitPreparationSearchRepository unitPreparationSearchRepository;

    @Autowired
    private MasterNumberingService numbering;

    public UnitPreparationService(UnitPreparationRepository unitPreparationRepository, UnitPreparationMapper unitPreparationMapper, UnitPreparationSearchRepository unitPreparationSearchRepository) {
        this.unitPreparationRepository = unitPreparationRepository;
        this.unitPreparationMapper = unitPreparationMapper;
        this.unitPreparationSearchRepository = unitPreparationSearchRepository;
    }

    /**
     * Save a unitPreparation.
     *
     * @param unitPreparationDTO the entity to save
     * @return the persisted entity
     */
    public UnitPreparationDTO save(UnitPreparationDTO unitPreparationDTO) {
        log.debug("Request to save UnitPreparation : {}", unitPreparationDTO);
        UnitPreparation unitPreparation = unitPreparationMapper.toEntity(unitPreparationDTO);
        if (unitPreparation.getSlipNumber() == null) {
            unitPreparation.setSlipNumber(numbering.getInternalNumber("PIS"));
        }
        unitPreparation = unitPreparationRepository.save(unitPreparation);
        UnitPreparationDTO result = unitPreparationMapper.toDto(unitPreparation);
        unitPreparationSearchRepository.save(unitPreparation);
        return result;
    }

    /**
     * Get all the unitPreparations.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UnitPreparationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UnitPreparations");
        return unitPreparationRepository.findActiveUnitPreparation(pageable)
            .map(unitPreparationMapper::toDto);
    }

    /**
     * Get one unitPreparation by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public UnitPreparationDTO findOne(UUID id) {
        log.debug("Request to get UnitPreparation : {}", id);
        UnitPreparation unitPreparation = unitPreparationRepository.findOne(id);
        return unitPreparationMapper.toDto(unitPreparation);
    }

    /**
     * Delete the unitPreparation by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete UnitPreparation : {}", id);
        unitPreparationRepository.delete(id);
        unitPreparationSearchRepository.delete(id);
    }

    /**
     * Search for the unitPreparation corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UnitPreparationDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of UnitPreparations for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idShipTo = request.getParameter("idShipTo");
        String idInternal = request.getParameter("idInternal");
        String idFacility = request.getParameter("idFacility");
        String idInventoryItem = request.getParameter("idInventoryItem");
        String idOrderItem = request.getParameter("idOrderItem");

        if (idShipTo != null) {
            q.withQuery(matchQuery("shipTo.idShipTo", idShipTo));
        }
        else if (idInternal != null) {
            q.withQuery(matchQuery("internal.idInternal", idInternal));
        }
        else if (idFacility != null) {
            q.withQuery(matchQuery("facility.idFacility", idFacility));
        }
        else if (idInventoryItem != null) {
            q.withQuery(matchQuery("inventoryItem.idInventoryItem", idInventoryItem));
        }
        else if (idOrderItem != null) {
            q.withQuery(matchQuery("orderItem.idOrderItem", idOrderItem));
        }

        Page<UnitPreparation> result = unitPreparationSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(unitPreparationMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<UnitPreparationDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered UnitPreparationDTO");
        String idShipTo = request.getParameter("idShipTo");
        String idInternal = request.getParameter("idInternal");
        String idFacility = request.getParameter("idFacility");
        String idInventoryItem = request.getParameter("idInventoryItem");
        String idOrderItem = request.getParameter("idOrderItem");

        if (idShipTo != null) {
            return unitPreparationRepository.queryByIdShipTo(idShipTo, pageable).map(unitPreparationMapper::toDto);
        }
        else if (idInternal != null) {
            return unitPreparationRepository.queryByIdInternal(idInternal, pageable).map(unitPreparationMapper::toDto);
        }
        else if (idFacility != null) {
            return unitPreparationRepository.queryByIdFacility(UUID.fromString(idFacility), pageable).map(unitPreparationMapper::toDto);
        }
        else if (idInventoryItem != null) {
            return unitPreparationRepository.queryByIdInventoryItem(UUID.fromString(idInventoryItem), pageable).map(unitPreparationMapper::toDto);
        }
        else if (idOrderItem != null) {
            return unitPreparationRepository.queryByIdOrderItem(UUID.fromString(idOrderItem), pageable).map(unitPreparationMapper::toDto);
        }

        return unitPreparationRepository.queryNothing(pageable).map(unitPreparationMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, UnitPreparationDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<UnitPreparationDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public UnitPreparationDTO changeUnitPreparationStatus(UnitPreparationDTO dto, Integer id) {
        if (dto != null) {
			UnitPreparation e = unitPreparationRepository.findOne(dto.getIdSlip());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                    case 17:
                        unitPreparationSearchRepository.delete(dto.getIdSlip());
                        break;
                    default:
                        unitPreparationSearchRepository.save(e);
                }
				e = unitPreparationRepository.save(e);
                return unitPreparationMapper.toDto(e);
			}
		}
        return dto;
    }
}
