package id.atiila.service;

import id.atiila.domain.WorkRequirement;
import id.atiila.repository.WorkRequirementRepository;
import id.atiila.repository.search.WorkRequirementSearchRepository;
import id.atiila.service.dto.WorkRequirementDTO;
import id.atiila.service.mapper.WorkRequirementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Service Implementation for managing WorkRequirement.
 * BeSmart Team
 */

@Service
@Transactional
public class WorkRequirementService {

    private final Logger log = LoggerFactory.getLogger(WorkRequirementService.class);

    private final WorkRequirementRepository workRequirementRepository;

    private final WorkRequirementMapper workRequirementMapper;

    private final WorkRequirementSearchRepository workRequirementSearchRepository;

    public WorkRequirementService(WorkRequirementRepository workRequirementRepository, WorkRequirementMapper workRequirementMapper, WorkRequirementSearchRepository workRequirementSearchRepository) {
        this.workRequirementRepository = workRequirementRepository;
        this.workRequirementMapper = workRequirementMapper;
        this.workRequirementSearchRepository = workRequirementSearchRepository;
    }

    /**
     * Save a workRequirement.
     *
     * @param workRequirementDTO the entity to save
     * @return the persisted entity
     */
    public WorkRequirementDTO save(WorkRequirementDTO workRequirementDTO) {
        log.debug("Request to save WorkRequirement : {}", workRequirementDTO);
        WorkRequirement workRequirement = workRequirementMapper.toEntity(workRequirementDTO);
        workRequirement = workRequirementRepository.save(workRequirement);
        WorkRequirementDTO result = workRequirementMapper.toDto(workRequirement);
        workRequirementSearchRepository.save(workRequirement);
        return result;
    }

    /**
     *  Get all the workRequirements.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WorkRequirementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WorkRequirements");
        return workRequirementRepository.findAll(pageable)
            .map(workRequirementMapper::toDto);
    }

    /**
     *  Get one workRequirement by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public WorkRequirementDTO findOne(UUID id) {
        log.debug("Request to get WorkRequirement : {}", id);
        WorkRequirement workRequirement = workRequirementRepository.findOneWithEagerRelationships(id);
        return workRequirementMapper.toDto(workRequirement);
    }

    /**
     *  Delete the  workRequirement by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete WorkRequirement : {}", id);
        workRequirementRepository.delete(id);
        workRequirementSearchRepository.delete(id);
    }

    /**
     * Search for the workRequirement corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WorkRequirementDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WorkRequirements for query {}", query);
        Page<WorkRequirement> result = workRequirementSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(workRequirementMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        workRequirementSearchRepository.deleteAll();
        List<WorkRequirement> workRequirements =  workRequirementRepository.findAll();
        for (WorkRequirement m: workRequirements) {
            workRequirementSearchRepository.save(m);
            log.debug("Data work requirement save !...");
        }
    }

}
