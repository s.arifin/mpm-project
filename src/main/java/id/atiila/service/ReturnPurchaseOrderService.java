package id.atiila.service;

import id.atiila.domain.ReturnPurchaseOrder;
import id.atiila.repository.ReturnPurchaseOrderRepository;
import id.atiila.repository.search.ReturnPurchaseOrderSearchRepository;
import id.atiila.service.dto.ReturnPurchaseOrderDTO;
import id.atiila.service.mapper.ReturnPurchaseOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ReturnPurchaseOrder.
 * BeSmart Team
 */

@Service
@Transactional
public class ReturnPurchaseOrderService {

    private final Logger log = LoggerFactory.getLogger(ReturnPurchaseOrderService.class);

    private final ReturnPurchaseOrderRepository returnPurchaseOrderRepository;

    private final ReturnPurchaseOrderMapper returnPurchaseOrderMapper;

    private final ReturnPurchaseOrderSearchRepository returnPurchaseOrderSearchRepository;

    public ReturnPurchaseOrderService(ReturnPurchaseOrderRepository returnPurchaseOrderRepository, ReturnPurchaseOrderMapper returnPurchaseOrderMapper, ReturnPurchaseOrderSearchRepository returnPurchaseOrderSearchRepository) {
        this.returnPurchaseOrderRepository = returnPurchaseOrderRepository;
        this.returnPurchaseOrderMapper = returnPurchaseOrderMapper;
        this.returnPurchaseOrderSearchRepository = returnPurchaseOrderSearchRepository;
    }

    /**
     * Save a returnPurchaseOrder.
     *
     * @param returnPurchaseOrderDTO the entity to save
     * @return the persisted entity
     */
    public ReturnPurchaseOrderDTO save(ReturnPurchaseOrderDTO returnPurchaseOrderDTO) {
        log.debug("Request to save ReturnPurchaseOrder : {}", returnPurchaseOrderDTO);
        ReturnPurchaseOrder returnPurchaseOrder = returnPurchaseOrderMapper.toEntity(returnPurchaseOrderDTO);
        returnPurchaseOrder = returnPurchaseOrderRepository.save(returnPurchaseOrder);
        ReturnPurchaseOrderDTO result = returnPurchaseOrderMapper.toDto(returnPurchaseOrder);
        returnPurchaseOrderSearchRepository.save(returnPurchaseOrder);
        return result;
    }

    /**
     * Get all the returnPurchaseOrders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ReturnPurchaseOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ReturnPurchaseOrders");
        return returnPurchaseOrderRepository.findActiveReturnPurchaseOrder(pageable)
            .map(returnPurchaseOrderMapper::toDto);
    }

    /**
     * Get one returnPurchaseOrder by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ReturnPurchaseOrderDTO findOne(UUID id) {
        log.debug("Request to get ReturnPurchaseOrder : {}", id);
        ReturnPurchaseOrder returnPurchaseOrder = returnPurchaseOrderRepository.findOne(id);
        return returnPurchaseOrderMapper.toDto(returnPurchaseOrder);
    }

    /**
     * Delete the returnPurchaseOrder by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ReturnPurchaseOrder : {}", id);
        returnPurchaseOrderRepository.delete(id);
        returnPurchaseOrderSearchRepository.delete(id);
    }

    /**
     * Search for the returnPurchaseOrder corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ReturnPurchaseOrderDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of ReturnPurchaseOrders for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idVendor = request.getParameter("idVendor");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");

        if (filterName != null) {
        }
        Page<ReturnPurchaseOrder> result = returnPurchaseOrderSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(returnPurchaseOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ReturnPurchaseOrderDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ReturnPurchaseOrderDTO");
        String idVendor = request.getParameter("idVendor");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");

        return returnPurchaseOrderRepository.findByParams(idVendor, idInternal, idBillTo, pageable)
            .map(returnPurchaseOrderMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public ReturnPurchaseOrderDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        ReturnPurchaseOrderDTO r = null;
        return r;
    }

    @Transactional
    public Set<ReturnPurchaseOrderDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<ReturnPurchaseOrderDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public ReturnPurchaseOrderDTO changeReturnPurchaseOrderStatus(ReturnPurchaseOrderDTO dto, Integer id) {
        if (dto != null) {
			ReturnPurchaseOrder e = returnPurchaseOrderRepository.findOne(dto.getIdOrder());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        returnPurchaseOrderSearchRepository.delete(dto.getIdOrder());
                        break;
                    default:
                        returnPurchaseOrderSearchRepository.save(e);
                }
				returnPurchaseOrderRepository.save(e);
			}
		}
        return dto;
    }
}
