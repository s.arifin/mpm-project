package id.atiila.service;

import id.atiila.domain.OrderShipmentItem;
import id.atiila.repository.OrderShipmentItemRepository;
import id.atiila.repository.search.OrderShipmentItemSearchRepository;
import id.atiila.service.dto.OrderShipmentItemDTO;
import id.atiila.service.mapper.OrderShipmentItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing OrderShipmentItem.
 * BeSmart Team
 */

@Service
@Transactional
public class OrderShipmentItemService {

    private final Logger log = LoggerFactory.getLogger(OrderShipmentItemService.class);

    private final OrderShipmentItemRepository orderShipmentItemRepository;

    private final OrderShipmentItemMapper orderShipmentItemMapper;

    private final OrderShipmentItemSearchRepository orderShipmentItemSearchRepository;

    public OrderShipmentItemService(OrderShipmentItemRepository orderShipmentItemRepository, OrderShipmentItemMapper orderShipmentItemMapper, OrderShipmentItemSearchRepository orderShipmentItemSearchRepository) {
        this.orderShipmentItemRepository = orderShipmentItemRepository;
        this.orderShipmentItemMapper = orderShipmentItemMapper;
        this.orderShipmentItemSearchRepository = orderShipmentItemSearchRepository;
    }

    /**
     * Save a orderShipmentItem.
     *
     * @param orderShipmentItemDTO the entity to save
     * @return the persisted entity
     */
    public OrderShipmentItemDTO save(OrderShipmentItemDTO orderShipmentItemDTO) {
        log.debug("Request to save OrderShipmentItem : {}", orderShipmentItemDTO);
        OrderShipmentItem orderShipmentItem = orderShipmentItemMapper.toEntity(orderShipmentItemDTO);
        orderShipmentItem = orderShipmentItemRepository.save(orderShipmentItem);
        OrderShipmentItemDTO result = orderShipmentItemMapper.toDto(orderShipmentItem);
        orderShipmentItemSearchRepository.save(orderShipmentItem);
        return result;
    }

    /**
     * Get all the orderShipmentItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrderShipmentItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrderShipmentItems");
        return orderShipmentItemRepository.findAll(pageable)
            .map(orderShipmentItemMapper::toDto);
    }

    /**
     * Get one orderShipmentItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public OrderShipmentItemDTO findOne(UUID id) {
        log.debug("Request to get OrderShipmentItem : {}", id);
        OrderShipmentItem orderShipmentItem = orderShipmentItemRepository.findOne(id);
        return orderShipmentItemMapper.toDto(orderShipmentItem);
    }

    /**
     * Delete the orderShipmentItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete OrderShipmentItem : {}", id);
        orderShipmentItemRepository.delete(id);
        orderShipmentItemSearchRepository.delete(id);
    }

    /**
     * Search for the orderShipmentItem corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrderShipmentItemDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of OrderShipmentItems for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idOrderItem = request.getParameter("idOrderItem");
        String idShipmentItem = request.getParameter("idShipmentItem");

        if (filterName != null) {
        }
        Page<OrderShipmentItem> result = orderShipmentItemSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(orderShipmentItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<OrderShipmentItemDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered OrderShipmentItemDTO");
        String idOrderItem = request.getParameter("idOrderItem");
        String idShipmentItem = request.getParameter("idShipmentItem");

        return orderShipmentItemRepository.findByParams(idOrderItem, idShipmentItem, pageable)
            .map(orderShipmentItemMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public OrderShipmentItemDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        OrderShipmentItemDTO r = null;
        return r;
    }

    @Transactional
    public Set<OrderShipmentItemDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<OrderShipmentItemDTO> r = new HashSet<>();
        return r;
    }

}
