package id.atiila.service;

import id.atiila.domain.VendorRelationship;
import id.atiila.repository.VendorRelationshipRepository;
import id.atiila.repository.search.VendorRelationshipSearchRepository;
import id.atiila.service.dto.VendorRelationshipDTO;
import id.atiila.service.mapper.VendorRelationshipMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing VendorRelationship.
 * atiila consulting
 */

@Service
@Transactional
public class VendorRelationshipService {

    private final Logger log = LoggerFactory.getLogger(VendorRelationshipService.class);

    private final VendorRelationshipRepository vendorRelationshipRepository;

    private final VendorRelationshipMapper vendorRelationshipMapper;

    private final VendorRelationshipSearchRepository vendorRelationshipSearchRepository;

    public VendorRelationshipService(VendorRelationshipRepository vendorRelationshipRepository, VendorRelationshipMapper vendorRelationshipMapper, VendorRelationshipSearchRepository vendorRelationshipSearchRepository) {
        this.vendorRelationshipRepository = vendorRelationshipRepository;
        this.vendorRelationshipMapper = vendorRelationshipMapper;
        this.vendorRelationshipSearchRepository = vendorRelationshipSearchRepository;
    }

    /**
     * Save a vendorRelationship.
     *
     * @param vendorRelationshipDTO the entity to save
     * @return the persisted entity
     */
    public VendorRelationshipDTO save(VendorRelationshipDTO vendorRelationshipDTO) {
        log.debug("Request to save VendorRelationship : {}", vendorRelationshipDTO);
        VendorRelationship vendorRelationship = vendorRelationshipMapper.toEntity(vendorRelationshipDTO);
        vendorRelationship = vendorRelationshipRepository.save(vendorRelationship);
        VendorRelationshipDTO result = vendorRelationshipMapper.toDto(vendorRelationship);
        vendorRelationshipSearchRepository.save(vendorRelationship);
        return result;
    }

    /**
     * Get all the vendorRelationships.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VendorRelationshipDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VendorRelationships");
        return vendorRelationshipRepository.findAll(pageable)
            .map(vendorRelationshipMapper::toDto);
    }

    /**
     * Get one vendorRelationship by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public VendorRelationshipDTO findOne(UUID id) {
        log.debug("Request to get VendorRelationship : {}", id);
        VendorRelationship vendorRelationship = vendorRelationshipRepository.findOne(id);
        return vendorRelationshipMapper.toDto(vendorRelationship);
    }

    /**
     * Delete the vendorRelationship by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete VendorRelationship : {}", id);
        vendorRelationshipRepository.delete(id);
        vendorRelationshipSearchRepository.delete(id);
    }

    /**
     * Search for the vendorRelationship corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VendorRelationshipDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of VendorRelationships for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idStatusType = request.getParameter("idStatusType");
        String idRelationType = request.getParameter("idRelationType");
        String idInternal = request.getParameter("idInternal");
        String idVendor = request.getParameter("idVendor");

        if (idStatusType != null) {
            q.withQuery(matchQuery("statusType.idStatusType", idStatusType));
        }
        else if (idRelationType != null) {
            q.withQuery(matchQuery("relationType.idRelationType", idRelationType));
        }
        else if (idInternal != null) {
            q.withQuery(matchQuery("internal.idInternal", idInternal));
        }
        else if (idVendor != null) {
            q.withQuery(matchQuery("vendor.idVendor", idVendor));
        }

        Page<VendorRelationship> result = vendorRelationshipSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(vendorRelationshipMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<VendorRelationshipDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered VendorRelationshipDTO");
        String idStatusType = request.getParameter("idStatusType");
        String idRelationType = request.getParameter("idRelationType");
        String idInternal = request.getParameter("idInternal");
        String idVendor = request.getParameter("idVendor");

        // Kalau idInternal dan idVendor nya di isi, lakukan Query Khusus
        if (idVendor != null && idInternal != null) {
            return vendorRelationshipRepository.queryByInternalVendor(idInternal, idVendor, pageable).map(vendorRelationshipMapper::toDto);
        }
        else if (idStatusType != null) {
            return vendorRelationshipRepository.queryByIdStatusType(Integer.valueOf(idStatusType), pageable).map(vendorRelationshipMapper::toDto);
        }
        else if (idRelationType != null) {
            return vendorRelationshipRepository.queryByIdRelationType(Integer.valueOf(idRelationType), pageable).map(vendorRelationshipMapper::toDto);
        }
        else if (idInternal != null) {
            return vendorRelationshipRepository.queryByIdInternal(idInternal, pageable).map(vendorRelationshipMapper::toDto);
        }
        else if (idVendor != null) {
            return vendorRelationshipRepository.queryByIdVendor(idVendor, pageable).map(vendorRelationshipMapper::toDto);
        }

        return vendorRelationshipRepository.queryNothing(pageable).map(vendorRelationshipMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, VendorRelationshipDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<VendorRelationshipDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
