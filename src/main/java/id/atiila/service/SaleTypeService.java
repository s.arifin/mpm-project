package id.atiila.service;

import id.atiila.domain.SaleType;
import id.atiila.repository.SaleTypeRepository;
import id.atiila.repository.search.SaleTypeSearchRepository;
import id.atiila.service.dto.SaleTypeDTO;
import id.atiila.service.mapper.SaleTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SaleType.
 * BeSmart Team
 */

@Service
@Transactional
public class SaleTypeService {

    private final Logger log = LoggerFactory.getLogger(SaleTypeService.class);

    private final SaleTypeRepository saleTypeRepository;

    private final SaleTypeMapper saleTypeMapper;

    private final SaleTypeSearchRepository saleTypeSearchRepository;

    public SaleTypeService(SaleTypeRepository saleTypeRepository, SaleTypeMapper saleTypeMapper, SaleTypeSearchRepository saleTypeSearchRepository) {
        this.saleTypeRepository = saleTypeRepository;
        this.saleTypeMapper = saleTypeMapper;
        this.saleTypeSearchRepository = saleTypeSearchRepository;
    }

    /**
     * Save a saleType.
     *
     * @param saleTypeDTO the entity to save
     * @return the persisted entity
     */
    public SaleTypeDTO save(SaleTypeDTO saleTypeDTO) {
        log.debug("Request to save SaleType : {}", saleTypeDTO);
        SaleType saleType = saleTypeMapper.toEntity(saleTypeDTO);
        saleType = saleTypeRepository.save(saleType);
        SaleTypeDTO result = saleTypeMapper.toDto(saleType);
        saleTypeSearchRepository.save(saleType);
        return result;
    }

    /**
     *  Get all the saleTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SaleTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SaleTypes");
        return saleTypeRepository.findAll(pageable)
            .map(saleTypeMapper::toDto);
    }

    /**
     *  Get one saleType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SaleTypeDTO findOne(Integer id) {
        log.debug("Request to get SaleType : {}", id);
        SaleType saleType = saleTypeRepository.findOne(id);
        return saleTypeMapper.toDto(saleType);
    }

    /**
     *  Delete the  saleType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete SaleType : {}", id);
        saleTypeRepository.delete(id);
        saleTypeSearchRepository.delete(id);
    }

    /**
     * Search for the saleType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SaleTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SaleTypes for query {}", query);
        Page<SaleType> result = saleTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(saleTypeMapper::toDto);
    }

    public SaleTypeDTO processExecuteData(Integer id, String param, SaleTypeDTO dto) {
        SaleTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<SaleTypeDTO> processExecuteListData(Integer id, String param, Set<SaleTypeDTO> dto) {
        Set<SaleTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
