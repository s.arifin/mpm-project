package id.atiila.service;

import id.atiila.domain.RequestProduct;
import id.atiila.domain.StatusType;
import id.atiila.repository.RequestProductRepository;
import id.atiila.repository.StatusTypeRepository;
import id.atiila.repository.search.RequestProductSearchRepository;
import id.atiila.service.dto.RequestProductDTO;
import id.atiila.service.mapper.RequestProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing RequestProduct.
 * BeSmart Team
 */

@Service
@Transactional
public class RequestProductService {

    private final Logger log = LoggerFactory.getLogger(RequestProductService.class);

    private final RequestProductRepository requestProductRepository;

    private final RequestProductMapper requestProductMapper;

    private final RequestProductSearchRepository requestProductSearchRepository;

    @Autowired
    private StatusTypeRepository statusTypeRepository;

    public RequestProductService(RequestProductRepository requestProductRepository, RequestProductMapper requestProductMapper, RequestProductSearchRepository requestProductSearchRepository) {
        this.requestProductRepository = requestProductRepository;
        this.requestProductMapper = requestProductMapper;
        this.requestProductSearchRepository = requestProductSearchRepository;
    }

    /**
     * Save a requestProduct.
     *
     * @param requestProductDTO the entity to save
     * @return the persisted entity
     */
    public RequestProductDTO save(RequestProductDTO requestProductDTO) {
        log.debug("Request to save RequestProduct : {}", requestProductDTO);
        RequestProduct requestProduct = requestProductMapper.toEntity(requestProductDTO);
        requestProduct = requestProductRepository.save(requestProduct);
        RequestProductDTO result = requestProductMapper.toDto(requestProduct);
        requestProductSearchRepository.save(requestProduct);
        return result;
    }

    /**
     * Get all the requestProducts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequestProductDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RequestProducts");
        return requestProductRepository.findActiveRequestProduct(pageable)
            .map(requestProductMapper::toDto)
            .map(new Converter<RequestProductDTO, RequestProductDTO>() {
                @Override
                public RequestProductDTO convert(RequestProductDTO requestProductDTO) {
                    RequestProductDTO r = requestProductDTO;
                    StatusType statusType = statusTypeRepository.findByIdStatusType(r.getCurrentStatus());
                    r.setCurrentStatusDescription(statusType.getDescription());
                    return r;
                }
            });
    }

    /**
     * Get one requestProduct by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public RequestProductDTO findOne(UUID id) {
        log.debug("Request to get RequestProduct : {}", id);
        RequestProduct requestProduct = requestProductRepository.findOne(id);
        return requestProductMapper.toDto(requestProduct);
    }

    /**
     * Delete the requestProduct by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete RequestProduct : {}", id);
        requestProductRepository.delete(id);
        requestProductSearchRepository.delete(id);
    }

    /**
     * Search for the requestProduct corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequestProductDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of RequestProducts for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idFacilityFrom = request.getParameter("idFacilityFrom");
        String idFacilityTo = request.getParameter("idFacilityTo");

        if (filterName != null) {
        }
        Page<RequestProduct> result = requestProductSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(requestProductMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<RequestProductDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered RequestProductDTO");
        String idFacilityFrom = request.getParameter("idFacilityFrom");
        String idFacilityTo = request.getParameter("idFacilityTo");
        String idStatusType = request.getParameter("idStatusType");

        if (idStatusType != null) {
            return requestProductRepository.findByStatusType(Integer.parseInt(idStatusType), pageable)
                    .map(requestProductMapper::toDto);
        }

        return requestProductRepository.findByParams(idFacilityFrom, idFacilityTo, pageable)
            .map(requestProductMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public RequestProductDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        RequestProductDTO r = null;
        return r;
    }

    @Transactional
    public Set<RequestProductDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<RequestProductDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public RequestProductDTO changeRequestProductStatus(RequestProductDTO dto, Integer id) {
        if (dto != null) {
			RequestProduct e = requestProductRepository.findOne(dto.getIdRequest());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        requestProductSearchRepository.delete(dto.getIdRequest());
                        break;
                    default:
                        requestProductSearchRepository.save(e);
                }
				requestProductRepository.save(e);
			}
		}
        return dto;
    }
}
