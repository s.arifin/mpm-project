package id.atiila.service;

import id.atiila.domain.SuspectType;
import id.atiila.repository.SuspectTypeRepository;
import id.atiila.repository.search.SuspectTypeSearchRepository;
import id.atiila.service.dto.SuspectTypeDTO;
import id.atiila.service.mapper.SuspectTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SuspectType.
 * BeSmart Team
 */

@Service
@Transactional
public class SuspectTypeService {

    private final Logger log = LoggerFactory.getLogger(SuspectTypeService.class);

    private final SuspectTypeRepository suspectTypeRepository;

    private final SuspectTypeMapper suspectTypeMapper;

    private final SuspectTypeSearchRepository suspectTypeSearchRepository;

    public SuspectTypeService(SuspectTypeRepository suspectTypeRepository, SuspectTypeMapper suspectTypeMapper, SuspectTypeSearchRepository suspectTypeSearchRepository) {
        this.suspectTypeRepository = suspectTypeRepository;
        this.suspectTypeMapper = suspectTypeMapper;
        this.suspectTypeSearchRepository = suspectTypeSearchRepository;
    }

    /**
     * Save a suspectType.
     *
     * @param suspectTypeDTO the entity to save
     * @return the persisted entity
     */
    public SuspectTypeDTO save(SuspectTypeDTO suspectTypeDTO) {
        log.debug("Request to save SuspectType : {}", suspectTypeDTO);
        SuspectType suspectType = suspectTypeMapper.toEntity(suspectTypeDTO);
        suspectType = suspectTypeRepository.save(suspectType);
        SuspectTypeDTO result = suspectTypeMapper.toDto(suspectType);
        suspectTypeSearchRepository.save(suspectType);
        return result;
    }

    /**
     *  Get all the suspectTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SuspectTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SuspectTypes");
        return suspectTypeRepository.findAll(pageable)
            .map(suspectTypeMapper::toDto);
    }

    /**
     *  Get one suspectType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SuspectTypeDTO findOne(Integer id) {
        log.debug("Request to get SuspectType : {}", id);
        SuspectType suspectType = suspectTypeRepository.findOne(id);
        return suspectTypeMapper.toDto(suspectType);
    }

    /**
     *  Delete the  suspectType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete SuspectType : {}", id);
        suspectTypeRepository.delete(id);
        suspectTypeSearchRepository.delete(id);
    }

    /**
     * Search for the suspectType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SuspectTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SuspectTypes for query {}", query);
        Page<SuspectType> result = suspectTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(suspectTypeMapper::toDto);
    }

    public SuspectTypeDTO processExecuteData(Integer id, String param, SuspectTypeDTO dto) {
        SuspectTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<SuspectTypeDTO> processExecuteListData(Integer id, String param, Set<SuspectTypeDTO> dto) {
        Set<SuspectTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
