package id.atiila.service;

import id.atiila.domain.PaymentMethodType;
import id.atiila.domain.PaymentType;
import id.atiila.repository.PaymentMethodTypeRepository;
import id.atiila.repository.search.PaymentMethodTypeSearchRepository;
import id.atiila.service.dto.PaymentMethodTypeDTO;
import id.atiila.service.mapper.PaymentMethodTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing PaymentMethodType.
 * BeSmart Team
 */

@Service
@Transactional
public class PaymentMethodTypeService {

    private final Logger log = LoggerFactory.getLogger(PaymentMethodTypeService.class);

    private final PaymentMethodTypeRepository paymentMethodTypeRepository;

    private final PaymentMethodTypeMapper paymentMethodTypeMapper;

    private final PaymentMethodTypeSearchRepository paymentMethodTypeSearchRepository;

    public PaymentMethodTypeService(PaymentMethodTypeRepository paymentMethodTypeRepository, PaymentMethodTypeMapper paymentMethodTypeMapper, PaymentMethodTypeSearchRepository paymentMethodTypeSearchRepository) {
        this.paymentMethodTypeRepository = paymentMethodTypeRepository;
        this.paymentMethodTypeMapper = paymentMethodTypeMapper;
        this.paymentMethodTypeSearchRepository = paymentMethodTypeSearchRepository;
    }

    /**
     * Save a paymentMethodType.
     *
     * @param paymentMethodTypeDTO the entity to save
     * @return the persisted entity
     */
    public PaymentMethodTypeDTO save(PaymentMethodTypeDTO paymentMethodTypeDTO) {
        log.debug("Request to save PaymentMethodType : {}", paymentMethodTypeDTO);
        PaymentMethodType paymentMethodType = paymentMethodTypeMapper.toEntity(paymentMethodTypeDTO);
        paymentMethodType = paymentMethodTypeRepository.save(paymentMethodType);
        PaymentMethodTypeDTO result = paymentMethodTypeMapper.toDto(paymentMethodType);
        paymentMethodTypeSearchRepository.save(paymentMethodType);
        return result;
    }

    /**
     * Get all the paymentMethodTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PaymentMethodTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PaymentMethodTypes");
        return paymentMethodTypeRepository.findAll(pageable)
            .map(paymentMethodTypeMapper::toDto);
    }

    /**
     * Get one paymentMethodType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PaymentMethodTypeDTO findOne(Integer id) {
        log.debug("Request to get PaymentMethodType : {}", id);
        PaymentMethodType paymentMethodType = paymentMethodTypeRepository.findOne(id);
        return paymentMethodTypeMapper.toDto(paymentMethodType);
    }

    /**
     * Delete the paymentMethodType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete PaymentMethodType : {}", id);
        paymentMethodTypeRepository.delete(id);
        paymentMethodTypeSearchRepository.delete(id);
    }

    /**
     * Search for the paymentMethodType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PaymentMethodTypeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of PaymentMethodTypes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));


        Page<PaymentMethodType> result = paymentMethodTypeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(paymentMethodTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PaymentMethodTypeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered PaymentMethodTypeDTO");


        return paymentMethodTypeRepository.queryNothing(pageable).map(paymentMethodTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, PaymentMethodTypeDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<PaymentMethodTypeDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    public void initialize(){
        List<String> list = new ArrayList<>();
        list.add("cash");
        list.add("electronic");
        list.add("edc");
        list.add("voucher");

        int i = 9;
        for(String a: list){
            PaymentMethodType p = paymentMethodTypeRepository.findByRefKey(a);
            if (p == null){
                i++;
                PaymentMethodType pt = new PaymentMethodType();
                pt.setIdPaymentMethodType(i);
                pt.setDescription(a);
                pt.setRefKey(a);
                paymentMethodTypeRepository.save(pt);
            }
        }
    }
}
