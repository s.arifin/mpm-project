package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.atiila.domain.BillTo;
import id.atiila.domain.Party;
import id.atiila.repository.BillToRepository;
import id.atiila.repository.search.BillToSearchRepository;
import id.atiila.service.dto.BillToDTO;
import id.atiila.service.mapper.BillToMapper;

/**
 * Service Implementation for managing BillTo.
 * BeSmart Team
 */

@Service
@Transactional
public class BillToService {

    private final Logger log = LoggerFactory.getLogger(BillToService.class);

    public static final String QUEU_REMOVE = "removeBillTo";

    private final BillToRepository billToRepository;

    private final BillToMapper billToMapper;

    private final BillToSearchRepository billToSearchRepository;

    public BillToService(BillToRepository billToRepository, BillToMapper billToMapper, BillToSearchRepository billToSearchRepository) {
        this.billToRepository = billToRepository;
        this.billToMapper = billToMapper;
        this.billToSearchRepository = billToSearchRepository;
    }

    /**
     * Save a billTo.
     *
     * @param billToDTO the entity to save
     * @return the persisted entity
     */
    public BillToDTO save(BillToDTO billToDTO) {
        log.debug("Request to save BillTo : {}", billToDTO);
        BillTo billTo = billToMapper.toEntity(billToDTO);
        billTo = billToRepository.save(billTo);
        BillToDTO result = billToMapper.toDto(billTo);
        billToSearchRepository.save(billTo);
        return result;
    }

    /**
     * Get all the billTos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BillToDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BillTos");
        return billToRepository.findAll(pageable)
            .map(billToMapper::toDto);
    }

    /**
     * Get one billTo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BillToDTO findOne(String id) {
        log.debug("Request to get BillTo : {}", id);
        BillTo billTo = billToRepository.findOne(id);
        return billToMapper.toDto(billTo);
    }

    /**
     * Delete the billTo by id.
     *
     * @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete BillTo : {}", id);
        billToRepository.delete(id);
        billToSearchRepository.delete(id);
    }

    /**
     * Search for the billTo corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BillToDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of BillTos for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idParty = request.getParameter("idParty");

        if (idParty != null) {
            q.withQuery(matchQuery("party.idParty", idParty));
        }

        Page<BillTo> result = billToSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(billToMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<BillToDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered BillToDTO");
        String idParty = request.getParameter("idParty");
        String idRoleType = request.getParameter("idRoleType");

        if (idParty != null) {
            return billToRepository.queryByIdParty(UUID.fromString(idParty), pageable).map(billToMapper::toDto);
        } else if (idRoleType != null) {
            return billToRepository.queryByIdRoleType(Integer.valueOf(idRoleType), pageable).map(billToMapper::toDto);
        }

        return billToRepository.queryNothing(pageable).map(billToMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, BillToDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<BillToDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional(readOnly = true)
    public BillToDTO findByIdBillTo(String idBillTo) {
        BillTo b = billToRepository.findOne(idBillTo);
        if (b == null) return null;
        return billToMapper.toDto(b);
    }

    @Transactional(readOnly = true)
    public BillToDTO findByParty(Party p) {
        BillTo b = billToRepository.findByParty(p);
        if (b == null) return null;
        return billToMapper.toDto(b);
    }

    @JmsListener(destination = QUEU_REMOVE)
    public void removeBillTo(String id) {
        billToRepository.delete(id);
        log.info("Remove Personal Customer : " + id);
    }
}
