package id.atiila.service;

import id.atiila.domain.FeatureType;
import id.atiila.repository.FeatureTypeRepository;
import id.atiila.repository.search.FeatureTypeSearchRepository;
import id.atiila.service.dto.FeatureTypeDTO;
import id.atiila.service.mapper.FeatureTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing FeatureType.
 * BeSmart Team
 */

@Service
@Transactional
public class FeatureTypeService {

    private final Logger log = LoggerFactory.getLogger(FeatureTypeService.class);

    private final FeatureTypeRepository featureTypeRepository;

    private final FeatureTypeMapper featureTypeMapper;

    private final FeatureTypeSearchRepository featureTypeSearchRepository;

    public FeatureTypeService(FeatureTypeRepository featureTypeRepository, FeatureTypeMapper featureTypeMapper, FeatureTypeSearchRepository featureTypeSearchRepository) {
        this.featureTypeRepository = featureTypeRepository;
        this.featureTypeMapper = featureTypeMapper;
        this.featureTypeSearchRepository = featureTypeSearchRepository;
    }

    /**
     * Save a featureType.
     *
     * @param featureTypeDTO the entity to save
     * @return the persisted entity
     */
    public FeatureTypeDTO save(FeatureTypeDTO featureTypeDTO) {
        log.debug("Request to save FeatureType : {}", featureTypeDTO);
        FeatureType featureType = featureTypeMapper.toEntity(featureTypeDTO);
        featureType = featureTypeRepository.save(featureType);
        FeatureTypeDTO result = featureTypeMapper.toDto(featureType);
        featureTypeSearchRepository.save(featureType);
        return result;
    }

    /**
     * Get all the featureTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FeatureTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FeatureTypes");
        return featureTypeRepository.findAll(pageable)
            .map(featureTypeMapper::toDto);
    }

    /**
     * Get one featureType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public FeatureTypeDTO findOne(Integer id) {
        log.debug("Request to get FeatureType : {}", id);
        FeatureType featureType = featureTypeRepository.findOne(id);
        return featureTypeMapper.toDto(featureType);
    }

    /**
     * Delete the featureType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete FeatureType : {}", id);
        featureTypeRepository.delete(id);
        featureTypeSearchRepository.delete(id);
    }

    /**
     * Search for the featureType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FeatureTypeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of FeatureTypes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));


        Page<FeatureType> result = featureTypeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(featureTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<FeatureTypeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered FeatureTypeDTO");


        return featureTypeRepository.queryNothing(pageable).map(featureTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, FeatureTypeDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<FeatureTypeDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        featureTypeSearchRepository.deleteAll();
        List<FeatureType> featureTypes =  featureTypeRepository.findAll();
        for (FeatureType f: featureTypes) {
            featureTypeSearchRepository.save(f);
            log.debug("Data Feature Type save !...");
        }
    }

}
