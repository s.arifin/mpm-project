package id.atiila.service;

import id.atiila.domain.CalendarType;
import id.atiila.repository.CalendarTypeRepository;
import id.atiila.repository.search.CalendarTypeSearchRepository;
import id.atiila.service.dto.CalendarTypeDTO;
import id.atiila.service.mapper.CalendarTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing CalendarType.
 * BeSmart Team
 */

@Service
@Transactional
public class CalendarTypeService {

    private final Logger log = LoggerFactory.getLogger(CalendarTypeService.class);

    private final CalendarTypeRepository calendarTypeRepository;

    private final CalendarTypeMapper calendarTypeMapper;

    private final CalendarTypeSearchRepository calendarTypeSearchRepository;

    public CalendarTypeService(CalendarTypeRepository calendarTypeRepository, CalendarTypeMapper calendarTypeMapper, CalendarTypeSearchRepository calendarTypeSearchRepository) {
        this.calendarTypeRepository = calendarTypeRepository;
        this.calendarTypeMapper = calendarTypeMapper;
        this.calendarTypeSearchRepository = calendarTypeSearchRepository;
    }

    /**
     * Save a calendarType.
     *
     * @param calendarTypeDTO the entity to save
     * @return the persisted entity
     */
    public CalendarTypeDTO save(CalendarTypeDTO calendarTypeDTO) {
        log.debug("Request to save CalendarType : {}", calendarTypeDTO);
        CalendarType calendarType = calendarTypeMapper.toEntity(calendarTypeDTO);
        calendarType = calendarTypeRepository.save(calendarType);
        CalendarTypeDTO result = calendarTypeMapper.toDto(calendarType);
        calendarTypeSearchRepository.save(calendarType);
        return result;
    }

    /**
     *  Get all the calendarTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CalendarTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CalendarTypes");
        return calendarTypeRepository.findAll(pageable)
            .map(calendarTypeMapper::toDto);
    }

    /**
     *  Get one calendarType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CalendarTypeDTO findOne(Integer id) {
        log.debug("Request to get CalendarType : {}", id);
        CalendarType calendarType = calendarTypeRepository.findOne(id);
        return calendarTypeMapper.toDto(calendarType);
    }

    /**
     *  Delete the  calendarType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete CalendarType : {}", id);
        calendarTypeRepository.delete(id);
        calendarTypeSearchRepository.delete(id);
    }

    /**
     * Search for the calendarType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CalendarTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CalendarTypes for query {}", query);
        Page<CalendarType> result = calendarTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(calendarTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        calendarTypeSearchRepository.deleteAll();
        List<CalendarType> calendarTypes =  calendarTypeRepository.findAll();
        for (CalendarType c: calendarTypes) {
            calendarTypeSearchRepository.save(c);
            log.debug("Data Calendar Type save !...");
        }
    }

}
