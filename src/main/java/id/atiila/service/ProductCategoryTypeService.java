package id.atiila.service;

import id.atiila.domain.ProductCategoryType;
import id.atiila.repository.ProductCategoryTypeRepository;
import id.atiila.repository.search.ProductCategoryTypeSearchRepository;
import id.atiila.service.dto.ProductCategoryTypeDTO;
import id.atiila.service.mapper.ProductCategoryTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ProductCategoryType.
 * BeSmart Team
 */

@Service
@Transactional
public class ProductCategoryTypeService {

    private final Logger log = LoggerFactory.getLogger(ProductCategoryTypeService.class);

    private final ProductCategoryTypeRepository productCategoryTypeRepository;

    private final ProductCategoryTypeMapper productCategoryTypeMapper;

    private final ProductCategoryTypeSearchRepository productCategoryTypeSearchRepository;

    public ProductCategoryTypeService(ProductCategoryTypeRepository productCategoryTypeRepository, ProductCategoryTypeMapper productCategoryTypeMapper, ProductCategoryTypeSearchRepository productCategoryTypeSearchRepository) {
        this.productCategoryTypeRepository = productCategoryTypeRepository;
        this.productCategoryTypeMapper = productCategoryTypeMapper;
        this.productCategoryTypeSearchRepository = productCategoryTypeSearchRepository;
    }

    /**
     * Save a productCategoryType.
     *
     * @param productCategoryTypeDTO the entity to save
     * @return the persisted entity
     */
    public ProductCategoryTypeDTO save(ProductCategoryTypeDTO productCategoryTypeDTO) {
        log.debug("Request to save ProductCategoryType : {}", productCategoryTypeDTO);
        ProductCategoryType productCategoryType = productCategoryTypeMapper.toEntity(productCategoryTypeDTO);
        productCategoryType = productCategoryTypeRepository.save(productCategoryType);
        ProductCategoryTypeDTO result = productCategoryTypeMapper.toDto(productCategoryType);
        productCategoryTypeSearchRepository.save(productCategoryType);
        return result;
    }

    /**
     * Get all the productCategoryTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductCategoryTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductCategoryTypes");
        return productCategoryTypeRepository.findAll(pageable)
            .map(productCategoryTypeMapper::toDto);
    }

    /**
     * Get one productCategoryType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ProductCategoryTypeDTO findOne(Integer id) {
        log.debug("Request to get ProductCategoryType : {}", id);
        ProductCategoryType productCategoryType = productCategoryTypeRepository.findOne(id);
        return productCategoryTypeMapper.toDto(productCategoryType);
    }

    /**
     * Delete the productCategoryType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete ProductCategoryType : {}", id);
        productCategoryTypeRepository.delete(id);
        productCategoryTypeSearchRepository.delete(id);
    }

    /**
     * Search for the productCategoryType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductCategoryTypeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of ProductCategoryTypes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));


        Page<ProductCategoryType> result = productCategoryTypeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(productCategoryTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ProductCategoryTypeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ProductCategoryTypeDTO");


        return productCategoryTypeRepository.queryNothing(pageable).map(productCategoryTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, ProductCategoryTypeDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<ProductCategoryTypeDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
