package id.atiila.service;

import id.atiila.domain.ContainerType;
import id.atiila.repository.ContainerTypeRepository;
import id.atiila.repository.search.ContainerTypeSearchRepository;
import id.atiila.service.dto.ContainerDTO;
import id.atiila.service.dto.ContainerTypeDTO;
import id.atiila.service.mapper.ContainerTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.*;
import java.util.HashSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ContainerType.
 * BeSmart Team
 */

@Service
@Transactional
public class ContainerTypeService {

    private final Logger log = LoggerFactory.getLogger(ContainerTypeService.class);

    private final ContainerTypeRepository containerTypeRepository;

    private final ContainerTypeMapper containerTypeMapper;

    private final ContainerTypeSearchRepository containerTypeSearchRepository;

    public ContainerTypeService(ContainerTypeRepository containerTypeRepository, ContainerTypeMapper containerTypeMapper, ContainerTypeSearchRepository containerTypeSearchRepository) {
        this.containerTypeRepository = containerTypeRepository;
        this.containerTypeMapper = containerTypeMapper;
        this.containerTypeSearchRepository = containerTypeSearchRepository;
    }

    /**
     * Save a containerType.
     *
     * @param containerTypeDTO the entity to save
     * @return the persisted entity
     */
    public ContainerTypeDTO save(ContainerTypeDTO containerTypeDTO) {
        log.debug("Request to save ContainerType : {}", containerTypeDTO);
        ContainerType containerType = containerTypeMapper.toEntity(containerTypeDTO);
        containerType = containerTypeRepository.save(containerType);
        ContainerTypeDTO result = containerTypeMapper.toDto(containerType);
        containerTypeSearchRepository.save(containerType);
        return result;
    }

    /**
     * Get all the containerTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ContainerTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ContainerTypes");
        return containerTypeRepository.findAll(pageable)
            .map(containerTypeMapper::toDto);
    }

    /**
     * Get one containerType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ContainerTypeDTO findOne(Integer id) {
        log.debug("Request to get ContainerType : {}", id);
        ContainerType containerType = containerTypeRepository.findOne(id);
        return containerTypeMapper.toDto(containerType);
    }

    /**
     * Delete the containerType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete ContainerType : {}", id);
        containerTypeRepository.delete(id);
        containerTypeSearchRepository.delete(id);
    }

    /**
     * Search for the containerType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ContainerTypeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of ContainerTypes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));


        Page<ContainerType> result = containerTypeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(containerTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ContainerTypeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ContainerTypeDTO");


        return containerTypeRepository.findAll(pageable).map(containerTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, ContainerTypeDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<ContainerTypeDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        containerTypeSearchRepository.deleteAll();
        List<ContainerType> containerTypes =  containerTypeRepository.findAll();
        for (ContainerType c: containerTypes) {
            containerTypeSearchRepository.save(c);
            log.debug("Data motor save !...");
        }
    }

}
