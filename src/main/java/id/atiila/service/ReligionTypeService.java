package id.atiila.service;

import id.atiila.base.XlsxUtils;
import id.atiila.domain.ReligionType;
import id.atiila.repository.ReligionTypeRepository;
import id.atiila.repository.search.ReligionTypeSearchRepository;
import id.atiila.service.dto.CustomReligionDTO;
import id.atiila.service.dto.ReligionTypeDTO;
import id.atiila.service.mapper.ReligionTypeMapper;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ReligionType.
 * BeSmart Team
 */

@Service
@Transactional
public class ReligionTypeService {

    private final Logger log = LoggerFactory.getLogger(ReligionTypeService.class);

    private final ReligionTypeRepository religionTypeRepository;

    private final ReligionTypeMapper religionTypeMapper;

    private final ReligionTypeSearchRepository religionTypeSearchRepository;

    public ReligionTypeService(ReligionTypeRepository religionTypeRepository, ReligionTypeMapper religionTypeMapper, ReligionTypeSearchRepository religionTypeSearchRepository) {
        this.religionTypeRepository = religionTypeRepository;
        this.religionTypeMapper = religionTypeMapper;
        this.religionTypeSearchRepository = religionTypeSearchRepository;
    }

    /**
     * Save a religionType.
     *
     * @param religionTypeDTO the entity to save
     * @return the persisted entity
     */
    public ReligionTypeDTO save(ReligionTypeDTO religionTypeDTO) {
        log.debug("Request to save ReligionType : {}", religionTypeDTO);
        ReligionType religionType = religionTypeMapper.toEntity(religionTypeDTO);
        religionType = religionTypeRepository.save(religionType);
        ReligionTypeDTO result = religionTypeMapper.toDto(religionType);
        religionTypeSearchRepository.save(religionType);
        return result;
    }

    /**
     *  Get all the religionTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ReligionTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ReligionTypes");
        return religionTypeRepository.findAll(pageable)
            .map(religionTypeMapper::toDto);
    }

    /**
     *  Get one religionType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ReligionTypeDTO findOne(Integer id) {
        log.debug("Request to get ReligionType : {}", id);
        ReligionType religionType = religionTypeRepository.findOne(id);
        return religionTypeMapper.toDto(religionType);
    }

    /**
     *  Delete the  religionType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete ReligionType : {}", id);
        religionTypeRepository.delete(id);
        religionTypeSearchRepository.delete(id);
    }

    /**
     * Search for the religionType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ReligionTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ReligionTypes for query {}", query);
        Page<ReligionType> result = religionTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(religionTypeMapper::toDto);
    }

    @PostConstruct
    @Transactional
    public void initialize() {
        try {
            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/agama.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/agama.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<CustomReligionDTO> religions = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (CustomReligionDTO o: religions) {
                    log.debug("cek data migrasi untuk agama", religions);
                    // if (o.getParentGeoCode() != null && o.getGeocode() != null) {
                    //     addVillage(o.getParentGeoCode(), o.getGeocode(), o.getDescription());
                    // }
                }
            }
        }   catch (Exception e) {
                System.out.println(e.getMessage());
            }
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    public void buildIndex() {
        religionTypeSearchRepository.deleteAll();
        List<ReligionType> religiontypes = religionTypeRepository.findAll();
//        log.debug("Data religion type save sementara!...");
        for (ReligionType m: religiontypes) {
            religionTypeSearchRepository.save(m);
            log.debug("Data religion type save !...");
        }
    }
}
