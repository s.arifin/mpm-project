package id.atiila.service;

import id.atiila.domain.DocumentType;
import id.atiila.repository.DocumentTypeRepository;
import id.atiila.repository.search.DocumentTypeSearchRepository;
import id.atiila.service.dto.DocumentTypeDTO;
import id.atiila.service.mapper.DocumentTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.text.Document;
import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing DocumentType.
 * BeSmart Team
 */

@Service
@Transactional
public class DocumentTypeService {

    private final Logger log = LoggerFactory.getLogger(DocumentTypeService.class);

    private final DocumentTypeRepository documentTypeRepository;

    private final DocumentTypeMapper documentTypeMapper;

    private final DocumentTypeSearchRepository documentTypeSearchRepository;

    public DocumentTypeService(DocumentTypeRepository documentTypeRepository, DocumentTypeMapper documentTypeMapper, DocumentTypeSearchRepository documentTypeSearchRepository) {
        this.documentTypeRepository = documentTypeRepository;
        this.documentTypeMapper = documentTypeMapper;
        this.documentTypeSearchRepository = documentTypeSearchRepository;
    }

    /**
     * Save a documentType.
     *
     * @param documentTypeDTO the entity to save
     * @return the persisted entity
     */
    public DocumentTypeDTO save(DocumentTypeDTO documentTypeDTO) {
        log.debug("Request to save DocumentType : {}", documentTypeDTO);
        DocumentType documentType = documentTypeMapper.toEntity(documentTypeDTO);
        documentType = documentTypeRepository.save(documentType);
        DocumentTypeDTO result = documentTypeMapper.toDto(documentType);
        documentTypeSearchRepository.save(documentType);
        return result;
    }

    @Transactional(readOnly = true)
    public List<DocumentTypeDTO> findAllByIdParent(Integer idparent){
        log.debug("SERVICE to get all document type by id parent");
        List<DocumentType> list = documentTypeRepository.findAllByIdParent(idparent);
        List<DocumentTypeDTO> list_DTO = documentTypeMapper.toDto(list);
        return list_DTO;
    }

    /**
     *  Get all the documentTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DocumentTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DocumentTypes");
        return documentTypeRepository.findAll(pageable)
            .map(documentTypeMapper::toDto);
    }

    /**
     *  Get one documentType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public DocumentTypeDTO findOne(Integer id) {
        log.debug("Request to get DocumentType : {}", id);
        DocumentType documentType = documentTypeRepository.findOne(id);
        return documentTypeMapper.toDto(documentType);
    }

    /**
     *  Delete the  documentType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete DocumentType : {}", id);
        documentTypeRepository.delete(id);
        documentTypeSearchRepository.delete(id);
    }

    /**
     * Search for the documentType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DocumentTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DocumentTypes for query {}", query);
        Page<DocumentType> result = documentTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(documentTypeMapper::toDto);
    }

    public DocumentTypeDTO processExecuteData(Integer id, String param, DocumentTypeDTO dto) {
        DocumentTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<DocumentTypeDTO> processExecuteListData(Integer id, String param, Set<DocumentTypeDTO> dto) {
        Set<DocumentTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

}
