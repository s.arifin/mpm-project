package id.atiila.service;

import id.atiila.domain.Dimension;
import id.atiila.repository.DimensionRepository;
import id.atiila.repository.search.DimensionSearchRepository;
import id.atiila.service.dto.DimensionDTO;
import id.atiila.service.mapper.DimensionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing Dimension.
 * atiila consulting
 */

@Service
@Transactional
public class DimensionService {

    private final Logger log = LoggerFactory.getLogger(DimensionService.class);

    private final DimensionRepository dimensionRepository;

    private final DimensionMapper dimensionMapper;

    private final DimensionSearchRepository dimensionSearchRepository;

    public DimensionService(DimensionRepository dimensionRepository, DimensionMapper dimensionMapper, DimensionSearchRepository dimensionSearchRepository) {
        this.dimensionRepository = dimensionRepository;
        this.dimensionMapper = dimensionMapper;
        this.dimensionSearchRepository = dimensionSearchRepository;
    }

    /**
     * Save a dimension.
     *
     * @param dimensionDTO the entity to save
     * @return the persisted entity
     */
    public DimensionDTO save(DimensionDTO dimensionDTO) {
        log.debug("Request to save Dimension : {}", dimensionDTO);
        Dimension dimension = dimensionMapper.toEntity(dimensionDTO);
        dimension = dimensionRepository.save(dimension);
        DimensionDTO result = dimensionMapper.toDto(dimension);
        dimensionSearchRepository.save(dimension);
        return result;
    }

    /**
     * Get all the dimensions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DimensionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Dimensions");
        return dimensionRepository.findAll(pageable)
            .map(dimensionMapper::toDto);
    }

    /**
     * Get one dimension by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public DimensionDTO findOne(Integer id) {
        log.debug("Request to get Dimension : {}", id);
        Dimension dimension = dimensionRepository.findOne(id);
        return dimensionMapper.toDto(dimension);
    }

    /**
     * Delete the dimension by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete Dimension : {}", id);
        dimensionRepository.delete(id);
        dimensionSearchRepository.delete(id);
    }

    /**
     * Search for the dimension corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DimensionDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of Dimensions for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idDimensionType = request.getParameter("idDimensionType");

        if (idDimensionType != null) {
            q.withQuery(matchQuery("dimensionType.idDimensionType", idDimensionType));
        }

        Page<Dimension> result = dimensionSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(dimensionMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<DimensionDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered DimensionDTO");
        String idDimensionType = request.getParameter("idDimensionType");

        if (idDimensionType != null) {
            return dimensionRepository.queryByIdDimensionType(Integer.valueOf(idDimensionType), pageable).map(dimensionMapper::toDto); 
        }

        return dimensionRepository.queryNothing(pageable).map(dimensionMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, DimensionDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<DimensionDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
