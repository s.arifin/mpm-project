package id.atiila.service;

import id.atiila.domain.UomConversion;
import id.atiila.repository.UomConversionRepository;
import id.atiila.repository.search.UomConversionSearchRepository;
import id.atiila.service.dto.UomConversionDTO;
import id.atiila.service.mapper.UomConversionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing UomConversion.
 * BeSmart Team
 */

@Service
@Transactional
public class UomConversionService {

    private final Logger log = LoggerFactory.getLogger(UomConversionService.class);

    private final UomConversionRepository uomConversionRepository;

    private final UomConversionMapper uomConversionMapper;

    private final UomConversionSearchRepository uomConversionSearchRepository;

    public UomConversionService(UomConversionRepository uomConversionRepository, UomConversionMapper uomConversionMapper, UomConversionSearchRepository uomConversionSearchRepository) {
        this.uomConversionRepository = uomConversionRepository;
        this.uomConversionMapper = uomConversionMapper;
        this.uomConversionSearchRepository = uomConversionSearchRepository;
    }

    /**
     * Save a uomConversion.
     *
     * @param uomConversionDTO the entity to save
     * @return the persisted entity
     */
    public UomConversionDTO save(UomConversionDTO uomConversionDTO) {
        log.debug("Request to save UomConversion : {}", uomConversionDTO);
        UomConversion uomConversion = uomConversionMapper.toEntity(uomConversionDTO);
        uomConversion = uomConversionRepository.save(uomConversion);
        UomConversionDTO result = uomConversionMapper.toDto(uomConversion);
        uomConversionSearchRepository.save(uomConversion);
        return result;
    }

    /**
     * Get all the uomConversions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UomConversionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UomConversions");
        return uomConversionRepository.findAll(pageable)
            .map(uomConversionMapper::toDto);
    }

    /**
     * Get one uomConversion by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public UomConversionDTO findOne(Integer id) {
        log.debug("Request to get UomConversion : {}", id);
        UomConversion uomConversion = uomConversionRepository.findOne(id);
        return uomConversionMapper.toDto(uomConversion);
    }

    /**
     * Delete the uomConversion by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete UomConversion : {}", id);
        uomConversionRepository.delete(id);
        uomConversionSearchRepository.delete(id);
    }

    /**
     * Search for the uomConversion corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UomConversionDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of UomConversions for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idUomTo = request.getParameter("idUomTo");
        String idUomFrom = request.getParameter("idUomFrom");

        if (idUomTo != null) {
            q.withQuery(matchQuery("uomTo.idUom", idUomTo));
        }
        else if (idUomFrom != null) {
            q.withQuery(matchQuery("uomFrom.idUom", idUomFrom));
        }

        Page<UomConversion> result = uomConversionSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(uomConversionMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<UomConversionDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered UomConversionDTO");
        String idUomTo = request.getParameter("idUomTo");
        String idUomFrom = request.getParameter("idUomFrom");

        if (idUomTo != null) {
            return uomConversionRepository.queryByIdUomTo(idUomTo, pageable).map(uomConversionMapper::toDto); 
        }
        else if (idUomFrom != null) {
            return uomConversionRepository.queryByIdUomFrom(idUomFrom, pageable).map(uomConversionMapper::toDto); 
        }

        return uomConversionRepository.queryNothing(pageable).map(uomConversionMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, UomConversionDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<UomConversionDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
