package id.atiila.service;

import id.atiila.domain.StandardCalendar;
import id.atiila.repository.StandardCalendarRepository;
import id.atiila.repository.search.StandardCalendarSearchRepository;
import id.atiila.service.dto.StandardCalendarDTO;
import id.atiila.service.mapper.StandardCalendarMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.*;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing StandardCalendar.
 * BeSmart Team
 */

@Service
@Transactional
public class StandardCalendarService {

    private final Logger log = LoggerFactory.getLogger(StandardCalendarService.class);

    private final StandardCalendarRepository standardCalendarRepository;

    private final StandardCalendarMapper standardCalendarMapper;

    private final StandardCalendarSearchRepository standardCalendarSearchRepository;

    public StandardCalendarService(StandardCalendarRepository standardCalendarRepository, StandardCalendarMapper standardCalendarMapper, StandardCalendarSearchRepository standardCalendarSearchRepository) {
        this.standardCalendarRepository = standardCalendarRepository;
        this.standardCalendarMapper = standardCalendarMapper;
        this.standardCalendarSearchRepository = standardCalendarSearchRepository;
    }

    /**
     * Save a standardCalendar.
     *
     * @param standardCalendarDTO the entity to save
     * @return the persisted entity
     */
    public StandardCalendarDTO save(StandardCalendarDTO standardCalendarDTO) {
        log.debug("Request to save StandardCalendar : {}", standardCalendarDTO);
        StandardCalendar standardCalendar = standardCalendarMapper.toEntity(standardCalendarDTO);
        standardCalendar = standardCalendarRepository.save(standardCalendar);
        StandardCalendarDTO result = standardCalendarMapper.toDto(standardCalendar);
        standardCalendarSearchRepository.save(standardCalendar);
        return result;
    }

    /**
     * Get all the standardCalendars.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StandardCalendarDTO> findAll(Pageable pageable) {
        log.debug("Request to get all StandardCalendars");
        return standardCalendarRepository.findAll(pageable)
            .map(standardCalendarMapper::toDto);
    }

    /**
     * Get one standardCalendar by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public StandardCalendarDTO findOne(Integer id) {
        log.debug("Request to get StandardCalendar : {}", id);
        StandardCalendar standardCalendar = standardCalendarRepository.findOne(id);
        return standardCalendarMapper.toDto(standardCalendar);
    }

    /**
     * Delete the standardCalendar by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete StandardCalendar : {}", id);
        standardCalendarRepository.delete(id);
        standardCalendarSearchRepository.delete(id);
    }

    /**
     * Search for the standardCalendar corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StandardCalendarDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of StandardCalendars for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idInternal = request.getParameter("idInternal");

        if (idInternal != null) {
            q.withQuery(matchQuery("internal.idInternal", idInternal));
        }

        Page<StandardCalendar> result = standardCalendarSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(standardCalendarMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<StandardCalendarDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered StandardCalendarDTO");
        String idInternal = request.getParameter("idInternal");
        String idCalendarType = request.getParameter("idCalendarType");

        if (idInternal != null && idCalendarType != null) {
            return standardCalendarRepository.findByTypeByIdInternal(Integer.valueOf(idCalendarType), idInternal, ZonedDateTime.now().minusMonths(1), pageable)
                .map(standardCalendarMapper::toDto);
        } else if (idInternal != null) {
            return standardCalendarRepository.findByIdInternal(idInternal, pageable).map(standardCalendarMapper::toDto);
        }

        return standardCalendarRepository.queryNothing(pageable).map(standardCalendarMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, StandardCalendarDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<StandardCalendarDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        standardCalendarSearchRepository.deleteAll();
        List<StandardCalendar> standardCalendars =  standardCalendarRepository.findAll();
        for (StandardCalendar m: standardCalendars) {
            standardCalendarSearchRepository.save(m);
            log.debug("Data standard calendar save !...");
        }
    }

}
