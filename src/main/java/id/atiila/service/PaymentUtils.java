package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.*;
import id.atiila.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class PaymentUtils {

    @Autowired
    private PaymentTypeRepository paymentTypeRepository;

    @Autowired
    private PaymentMethodRepository paymentMethodRepository;

    @Autowired
    private PaymentApplicationRepository paymentApplicationRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    public PaymentType getPaymentType(Integer id, String description) {
        PaymentType paymentType = paymentTypeRepository.findOne(id);
        if (paymentType == null) {
            paymentType = new PaymentType();
            paymentType.setIdPaymentType(id);
            paymentType.setDescription(description);
            paymentType = paymentTypeRepository.saveAndFlush(paymentType);
        }
        return paymentType;
    }

    public PaymentType getPaymentType(Integer id) {
        return getPaymentType(id, "Reserved");
    }

    public PaymentMethod getPaymentMethod(String refKey, String description) {
        PaymentMethod paymentMethod = null;
        List<PaymentMethod> items = paymentMethodRepository.queryByRefKey(refKey);
        if (items.isEmpty()) {
            paymentMethod  = new PaymentMethod();
            paymentMethod.setRefKey(refKey);
            paymentMethod.setDescription(description);
            paymentMethod = paymentMethodRepository.saveAndFlush(paymentMethod);
        } else
            paymentMethod = items.get(0);
        return paymentMethod;
    }

    public PaymentMethod getPaymentMethod(String refKey) {
        return getPaymentMethod(refKey, "Reserved");
    }

    public Double paymentOutstanding(UUID idPayment) {
        Payment payment = paymentRepository.findOne(idPayment);
        List<PaymentApplication> paymentApplications = paymentApplicationRepository.queryByIdPayment(idPayment);
        Double assigned = 0d;
        Double paymentAmount = 0d;

        for (PaymentApplication p: paymentApplications) {
            assigned = assigned + p.getAmountApplied().doubleValue();
        }

        if (payment.getAmount() != null){
            paymentAmount = payment.getAmount().doubleValue();
        }
        return paymentAmount - assigned;
    }

    public void assignBillingToPayment(Billing billing, Payment payment, Double amount) {
        PaymentApplication paymentApplication = new PaymentApplication();
        paymentApplication.setBilling(billing);
        paymentApplication.setPayment(payment);
        paymentApplication.setAmountApplied(BigDecimal.valueOf(amount));
        paymentApplicationRepository.save(paymentApplication);
    }

    @PostConstruct
    public void initialize() {
        getPaymentType(BaseConstants.PAYMENT_TYPE_TITIPAN, "Titipan");
        getPaymentType(BaseConstants.PAYMENT_TYPE_DP, "Down Payment");
        getPaymentType(BaseConstants.PAYMENT_TYPE_DP2, "Down Payment");
        getPaymentType(BaseConstants.PAYMENT_TYPE_PELUNASAN, "Pelunasan");

        getPaymentMethod(BaseConstants.PAYMENT_METHOD_CASH, "Tunai");
    }
}
