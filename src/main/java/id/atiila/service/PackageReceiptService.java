package id.atiila.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.BillingDisbursementRepository;
import id.atiila.repository.InventoryItemRepository;
import id.atiila.repository.PackageReceiptRepository;
import id.atiila.repository.search.PackageReceiptSearchRepository;
import id.atiila.route.DemandSupplyRoute;
import id.atiila.service.dto.PackageReceiptDTO;
import id.atiila.service.dto.SLUploadDTO;
import id.atiila.service.impl.ShipmentBeanImpl;
import id.atiila.service.mapper.PackageReceiptMapper;
import id.atiila.service.pto.ShipmentPTO;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing PackageReceipt.
 * BeSmart Team
 */

@Service
@Transactional
public class PackageReceiptService {

    private final Logger log = LoggerFactory.getLogger(PackageReceiptService.class);

    private final PackageReceiptRepository packageReceiptRepository;

    private final PackageReceiptMapper packageReceiptMapper;

    private final PackageReceiptSearchRepository packageReceiptSearchRepository;

    @Autowired
    private ProducerTemplate camelTemplate;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private ShipmentBeanImpl shipmentBean;

    @Autowired
    private BillingDisbursementRepository billingDisbursementRepository;

    @Autowired
    private MasterNumberingService numberingService;

    @Autowired
    private ActivitiProcessor activitiProcessor;

    @Autowired
    private InventoryItemRepository inventoryItemRepository;

    public PackageReceiptService(PackageReceiptRepository packageReceiptRepository, PackageReceiptMapper packageReceiptMapper, PackageReceiptSearchRepository packageReceiptSearchRepository) {
        this.packageReceiptRepository = packageReceiptRepository;
        this.packageReceiptMapper = packageReceiptMapper;
        this.packageReceiptSearchRepository = packageReceiptSearchRepository;
    }

    /**
     * Save a packageReceipt.
     *
     * @param packageReceiptDTO the entity to save
     * @return the persisted entity
     */
    public PackageReceiptDTO save(PackageReceiptDTO packageReceiptDTO) {
        log.debug("Request to save PackageReceipt : {}", packageReceiptDTO);
        PackageReceipt packageReceipt = packageReceiptMapper.toEntity(packageReceiptDTO);

        if (packageReceipt.getDocumentNumber() == null) {
            throw new DmsException(" No Shippinglist Belum Diisi ");
        }

        List<PackageReceipt> p = packageReceiptRepository.findOneByDocumentNumber(packageReceiptDTO.getDocumentNumber());

        if(p.size() > 0){
            throw new DmsException("No Shippinglist "+ packageReceiptDTO.getDocumentNumber() + " Sudah Ada");
        }
        packageReceipt.setShipType(null);
        log.debug("package1: " + packageReceipt);
        packageReceipt = packageReceiptRepository.saveAndFlush(packageReceipt);
        PackageReceiptDTO result = packageReceiptMapper.toDto(packageReceipt);
        packageReceiptSearchRepository.save(packageReceipt);
        log.debug("package2: " + packageReceipt);
        return result;
    }

    /**
     * Get all the packageReceipts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PackageReceiptDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PackageReceipts");
        log.debug("maskk kaga nihhh");
        return packageReceiptRepository.findActivePackageReceipt(pageable)
            .map(packageReceiptMapper::toDto);
    }

    /**
     * Get one packageReceipt by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PackageReceiptDTO findOne(UUID id) {
        log.debug("Request to get PackageReceipt : {}", id);
        PackageReceipt packageReceipt = packageReceiptRepository.findOne(id);
        return packageReceiptMapper.toDto(packageReceipt);
    }

    @Transactional(readOnly = true)
    public PackageReceiptDTO findOneByDocumentNumber(String docNumber) {
        log.debug("Request to get PackageReceipt by Document Number: {}", docNumber);
        List<PackageReceipt> packageReceipt = packageReceiptRepository.findOneByDocumentNumber(docNumber);
        return (PackageReceiptDTO) packageReceiptMapper.toDto(packageReceipt);
    }

    /**
     * Delete the packageReceipt by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete PackageReceipt : {}", id);
        packageReceiptRepository.delete(id);
        packageReceiptSearchRepository.delete(id);
    }

    /**
     * Search for the packageReceipt corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PackageReceiptDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of PackageReceipts for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idInternal = request.getParameter("idInternal");
        String idShipFrom = request.getParameter("idShipFrom");

        if (filterName != null) {
        }
        Page<PackageReceipt> result = packageReceiptSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(packageReceiptMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PackageReceiptDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered PackageReceiptDTO");
        String filterName = request.getParameter("filterName");
        log.debug("filtername cuk==" + filterName);
        String idInternal = request.getParameter("idInternal");
        log.debug("idinternal cukk===  " + idInternal);
        String idShipFrom = request.getParameter("idShipFrom");
        log.debug("idshipfrom" + idInternal);

        if (filterName != null && "byStatusInternal".equalsIgnoreCase(filterName)) {
            return packageReceiptRepository.queryByInternalByStatus(idInternal, pageable)
                .map(packageReceiptMapper::toDto);
        }

        Set<Integer> statuses = request.getParameter("idStatusType") == null ?
            new HashSet<>(Arrays.asList(10,11)) : convertStringToSetInteger(request.getParameter("idStatusType"));

        return packageReceiptRepository.findByParams(idInternal, idShipFrom, statuses, pageable)
            .map(packageReceiptMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PackageReceiptDTO> findFilterByLov(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered PackageReceiptDTO");
        String filterName = request.getParameter("filterName");
        log.debug("filtername cuk==" + filterName);
        String idInternal = request.getParameter("idInternal");
        log.debug("idinternal cukk===  " + idInternal);
        String idShipFrom = request.getParameter("idShipFrom");
        log.debug("idshipfrom" + idInternal);

        if (filterName != null && "byStatusInternal".equalsIgnoreCase(filterName)) {
            return packageReceiptRepository.queryByInternalByStatus(idInternal, pageable)
                .map(packageReceiptMapper::toDto);
        }

        Set<Integer> statuses = request.getParameter("idStatusType") == null ?
            new HashSet<>(Arrays.asList(10,11)) : convertStringToSetInteger(request.getParameter("idStatusType"));

        return packageReceiptRepository.findByParamsLov(idInternal, idShipFrom, statuses, pageable)
            .map(packageReceiptMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PackageReceiptDTO> findFilterByPTO(Pageable pageable, ShipmentPTO param) {
        log.debug("Request to get all filtered Vehicle Sales Order");
        return packageReceiptRepository.queryByShipmentPTO(pageable, param).map(packageReceiptMapper::toDto);
    }


    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String idProcess = "shipment-unit-receipt";
        Map<String, Object> r = new HashMap<>();
        String command = (String) item.get("command");
        String idPackage = (String) item.get("idPackage");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            // buildIndex();
        } else if (command != null && "doActivate".equalsIgnoreCase(command)) {
            ProcessInstance p = activitiProcessor.getProcessInstance(idProcess, idPackage);
            if (p == null) {
                PackageReceipt pr = packageReceiptRepository.findOne(UUID.fromString(idPackage));
                if (pr == null) throw new DmsException("Id Package Tidak di kenal !");
                Map<String, Object> params = activitiProcessor.getVariables("packageReceipt", pr);
                p = activitiProcessor.startProcess(idProcess, idPackage, params);
            }
            activitiProcessor.completeTask(idProcess, idPackage, "activate");

            PackageReceipt pr = packageReceiptRepository.findOne(UUID.fromString(idPackage));
            if (pr.getCurrentStatus().equals(BaseConstants.STATUS_DRAFT)) {
                throw new DmsException("Data anda tidak valid !");
            }

        } else if (command != null && "doApproved".equalsIgnoreCase(command)) {
            activitiProcessor.completeTask("shipment-unit-receipt", idPackage, "approve");
        } else if (command != null && "doCancelData".equalsIgnoreCase(command)) {
            ProcessInstance p = activitiProcessor.getProcessInstance(idProcess, idPackage);
            if (p != null) {
                activitiProcessor.cancelProcessInstance(idProcess, idPackage);
            }
            PackageReceipt pr = packageReceiptRepository.findOne(UUID.fromString(idPackage));
            if (pr != null) {
                pr.setStatus(BaseConstants.STATUS_CANCEL);
                pr = packageReceiptRepository.saveAndFlush(pr);
            }
        }
        return r;
    }

    @Transactional
    public PackageReceiptDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        String execute = request.getParameter("execute").toString();
        String docnumber = request.getParameter("docnumber").toString();
        PackageReceiptDTO r = new PackageReceiptDTO();
        log.debug("execute data :" +r);

        if (execute.equals("ApproveAndBuildShipmentIncoming")){
            ObjectMapper mapper = new ObjectMapper();
            mapper.findAndRegisterModules();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

            r = mapper.convertValue(item.get("item"), new TypeReference<PackageReceiptDTO>() {});
            List<PackageReceipt> lp = packageReceiptRepository.findOneByDocumentNumber(docnumber);
            log.debug("list package receipt == "+lp);
            PackageReceipt p = lp.get(0);
                log.debug("status p" + p.getCurrentStatus());
            if (p.getCurrentStatus().equals(BaseConstants.STATUS_OPEN) || p.getCurrentStatus().equals(BaseConstants.STATUS_DRAFT)) {
                log.debug("cari p =="+p);
                p = shipmentBean.buildShipmentIncoming(p, null);
                log.debug("udah masuk open" +p);
                shipmentBean.completePackageReceipt(p, null);
            }

            // camelTemplate.sendBodyAndHeader(DemandSupplyRoute.ACTION_APPROVE_PACKAGE_RECEIPT, p, "currentStatus", p.getCurrentStatus());
        }

        return r;
    }

    @Transactional
    public Set<PackageReceiptDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        String execute = request.getParameter("execute").toString();
        PackageReceipt packageReceipt = null;
        Set<PackageReceiptDTO> r = new HashSet<>();
        Set<SLUploadDTO> dtos;

        if (execute.equals("UploadShippingList")) {

            ObjectMapper mapper = new ObjectMapper();
            mapper.findAndRegisterModules();


            List<Object> objects = (List<Object>) item.get("items");
            dtos = mapper.convertValue(objects, new TypeReference<Set<SLUploadDTO>>() {});

            validationPackageReceipt(dtos);

            //camelTemplate.sendBody(DemandSupplyRoute.ACTION_UPLOAD_SHIPPING_LIST, dtos);

            shipmentBean.convertUploadShippingListToPackageReceipt(dtos, null);
        }
        return r;
    }

    @Transactional
    public PackageReceiptDTO changePackageReceiptStatus(PackageReceiptDTO dto, Integer id) {
        if (dto != null) {
			PackageReceipt e = packageReceiptRepository.findOne(dto.getIdPackage());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        packageReceiptSearchRepository.delete(dto.getIdPackage());
                        break;
                    default:
                        packageReceiptSearchRepository.save(e);
                }
				packageReceiptRepository.save(e);
			}
		}
        return dto;
    }

    private void validationPackageReceipt(Set<SLUploadDTO> dtos) {
        log.debug("START VALIDATION PACKAGE RECEIPT");
//        //Get internal
//        Internal internal = internalUtils.findOne(dtos.iterator().next().getDealerCode01());
//        if (internal == null){
//            throw new DmsException("Internal tidak di kenal !");
//            // internal = internalUtils.buildBranch(r.iterator().next().getDealerCode01(), r.iterator().next().getDealerCode01());
//        }

        List<BillingDisbursement> items = billingDisbursementRepository.findOneByVendorInvoice(dtos.iterator().next().getInvoiceNumber());
        BillingDisbursement b = items.size() > 0 ? items.get(0) : null;

        if (b == null) {
            throw new DmsException("Invoice Reference Tidak Ditemukan");
        }

        if (dtos.iterator().next().getShippingListNumber() != null) {
            String shippingListNumber = dtos.iterator().next().getShippingListNumber();
            String idFrame = dtos.iterator().next().getIdFrame();
            String idMachine = dtos.iterator().next().getIdMachine();
            List<PackageReceipt> p = packageReceiptRepository.findOneByDocumentNumber(shippingListNumber);

            if (idFrame != null || idMachine != null) {
                List<InventoryItem> r = inventoryItemRepository.queryByFrameOrMachineForPackageReceipt(idFrame, idMachine);

                if (r.size() > 0) {
                    throw new DmsException("Data Shipping List dengan Noka " + idFrame + "Nosin " + idMachine + "sudah ada");
//                PackageReceipt pr = p.get(0);
//                if (pr != null){
//                    throw new DmsException("Data Shipping List Sudah Ada");
//                }
                }
            }

            if (p.size() > 0) {
                throw new DmsException("Data Shipping List dengan nomor " + shippingListNumber + "sudah ada");
//                PackageReceipt pr = p.get(0);
//                if (pr != null){
//                    throw new DmsException("Data Shipping List Sudah Ada");
//                }
            }
        }
    }

    private Set<Integer> convertStringToSetInteger(String text){
        String[] items = text.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\s", "").split(",");
        Set<Integer> results = new HashSet<>();

        for (int i = 0; i < items.length; i++) {
            try {
                results.add(Integer.parseInt(items[i]));
            } catch (NumberFormatException nfe) {
                //NOTE: write something here if you need to recover from formatting errors
            };
        }
        return results;
    }
}
