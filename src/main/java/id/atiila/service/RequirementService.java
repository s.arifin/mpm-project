package id.atiila.service;

import id.atiila.domain.Requirement;
import id.atiila.repository.RequirementRepository;
import id.atiila.repository.search.RequirementSearchRepository;
import id.atiila.service.dto.RequirementDTO;
import id.atiila.service.mapper.RequirementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

import java.time.ZonedDateTime;
import java.util.UUID;

/**
 * Service Implementation for managing Requirement.
 * BeSmart Team
 */

@Service
@Transactional
public class RequirementService {

    private final Logger log = LoggerFactory.getLogger(RequirementService.class);

    private final RequirementRepository requirementRepository;

    private final RequirementMapper requirementMapper;

    private final RequirementSearchRepository requirementSearchRepository;

    @Autowired
    private MasterNumberingService numberSvc;

    public RequirementService(RequirementRepository requirementRepository, RequirementMapper requirementMapper, RequirementSearchRepository requirementSearchRepository) {
        this.requirementRepository = requirementRepository;
        this.requirementMapper = requirementMapper;
        this.requirementSearchRepository = requirementSearchRepository;
    }

    /**
     * Save a requirement.
     *
     * @param requirementDTO the entity to save
     * @return the persisted entity
     */
    public RequirementDTO save(RequirementDTO requirementDTO) {
        log.debug("Request to save Requirement : {}", requirementDTO);
        Requirement requirement = requirementMapper.toEntity(requirementDTO);

        Boolean isNewData = requirement.getIdRequirement() == null;
        if (isNewData) {
            if (requirementDTO.getRequirementNumber() == null) {
                String reqNo = null;
                Integer year = ZonedDateTime.now().getYear();
                while (reqNo == null || requirementRepository.findOneByRequirementNumber(reqNo) != null) {
                    reqNo = "RU-" + year + "-" + String.format("%05d", numberSvc.nextValue("idrequirement", year.toString()));
                }
                requirement.setRequirementNumber(reqNo);
            }
        }
        requirement = requirementRepository.save(requirement);
        RequirementDTO result = requirementMapper.toDto(requirement);
        requirementSearchRepository.save(requirement);
        return result;
    }

    /**
     *  Get all the requirements.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequirementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Requirements");
        return requirementRepository.findActiveRequirement(pageable)
            .map(requirementMapper::toDto);
    }

    /**
     *  Get one requirement by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public RequirementDTO findOne(UUID id) {
        log.debug("Request to get Requirement : {}", id);
        Requirement requirement = requirementRepository.findOneWithEagerRelationships(id);
        return requirementMapper.toDto(requirement);
    }

    /**
     *  Delete the  requirement by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Requirement : {}", id);
        requirementRepository.delete(id);
        requirementSearchRepository.delete(id);
    }

    /**
     * Search for the requirement corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequirementDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Requirements for query {}", query);
        Page<Requirement> result = requirementSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(requirementMapper::toDto);
    }

    public RequirementDTO changeRequirementStatus(RequirementDTO dto, Integer id) {
        if (dto != null) {
			Requirement e = requirementRepository.findOne(dto.getIdRequirement());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        requirementSearchRepository.delete(dto.getIdRequirement());
                        break;
                    default:
                        requirementSearchRepository.save(e);
                }
				requirementRepository.save(e);
			}
		}
        return dto;
    }
}
