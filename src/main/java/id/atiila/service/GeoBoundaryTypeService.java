package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.GeoBoundaryType;
import id.atiila.repository.GeoBoundaryTypeRepository;
import id.atiila.repository.search.GeoBoundaryTypeSearchRepository;
import id.atiila.service.dto.GeoBoundaryTypeDTO;
import id.atiila.service.mapper.GeoBoundaryTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing GeoBoundaryType.
 * BeSmart Team
 */

@Service
@Transactional
public class GeoBoundaryTypeService {

    private final Logger log = LoggerFactory.getLogger(GeoBoundaryTypeService.class);

    private final GeoBoundaryTypeRepository geoBoundaryTypeRepository;

    private final GeoBoundaryTypeMapper geoBoundaryTypeMapper;

    private final GeoBoundaryTypeSearchRepository geoBoundaryTypeSearchRepository;
    public GeoBoundaryTypeService(GeoBoundaryTypeRepository geoBoundaryTypeRepository, GeoBoundaryTypeMapper geoBoundaryTypeMapper, GeoBoundaryTypeSearchRepository geoBoundaryTypeSearchRepository) {
        this.geoBoundaryTypeRepository = geoBoundaryTypeRepository;
        this.geoBoundaryTypeMapper = geoBoundaryTypeMapper;
        this.geoBoundaryTypeSearchRepository = geoBoundaryTypeSearchRepository;
    }

    /**
     * Save a geoBoundaryType.
     *
     * @param geoBoundaryTypeDTO the entity to save
     * @return the persisted entity
     */
    public GeoBoundaryTypeDTO save(GeoBoundaryTypeDTO geoBoundaryTypeDTO) {
        log.debug("Request to save GeoBoundaryType : {}", geoBoundaryTypeDTO);
        GeoBoundaryType geoBoundaryType = geoBoundaryTypeMapper.toEntity(geoBoundaryTypeDTO);
        geoBoundaryType = geoBoundaryTypeRepository.save(geoBoundaryType);
        GeoBoundaryTypeDTO result = geoBoundaryTypeMapper.toDto(geoBoundaryType);
        geoBoundaryTypeSearchRepository.save(geoBoundaryType);
        return result;
    }

    /**
     *  Get all the geoBoundaryTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GeoBoundaryTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all GeoBoundaryTypes");
        return geoBoundaryTypeRepository.findAll(pageable)
            .map(geoBoundaryTypeMapper::toDto);
    }

    /**
     *  Get one geoBoundaryType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public GeoBoundaryTypeDTO findOne(Integer id) {
        log.debug("Request to get GeoBoundaryType : {}", id);
        GeoBoundaryType geoBoundaryType = geoBoundaryTypeRepository.findOne(id);
        return geoBoundaryTypeMapper.toDto(geoBoundaryType);
    }

    /**
     *  Delete the  geoBoundaryType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete GeoBoundaryType : {}", id);
        geoBoundaryTypeRepository.delete(id);
        geoBoundaryTypeSearchRepository.delete(id);
    }

    /**
     * Search for the geoBoundaryType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GeoBoundaryTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of GeoBoundaryTypes for query {}", query);
        Page<GeoBoundaryType> result = geoBoundaryTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(geoBoundaryTypeMapper::toDto);
    }

    public GeoBoundaryTypeDTO processExecuteData(Integer id, String param, GeoBoundaryTypeDTO dto) {
        GeoBoundaryTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<GeoBoundaryTypeDTO> processExecuteListData(Integer id, String param, Set<GeoBoundaryTypeDTO> dto) {
        Set<GeoBoundaryTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @PostConstruct
    @Transactional
    public void initialize() {
        save(new GeoBoundaryTypeDTO(BaseConstants.GEO_TYPE_PROVINCE, "Propinsi"));
        save(new GeoBoundaryTypeDTO(BaseConstants.GEO_TYPE_CITY, "kota"));
        save(new GeoBoundaryTypeDTO(BaseConstants.GEO_TYPE_DISTRICT, "Kecamatan"));
        save(new GeoBoundaryTypeDTO(BaseConstants.GEO_TYPE_VILLAGE, "Kelurahan/Desa"));
    }
}
