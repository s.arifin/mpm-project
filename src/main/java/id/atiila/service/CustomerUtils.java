package id.atiila.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.atiila.base.BaseConstants;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.OrganizationCustomerSearchRepository;
import id.atiila.repository.search.PersonalCustomerSearchRepository;
import id.atiila.service.ax.dto.AxCustomerDTO;
import id.atiila.service.dto.*;
import id.atiila.service.mapper.OrganizationCustomerMapper;
import id.atiila.service.mapper.PersonalCustomerMapper;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class CustomerUtils {

    private final Logger log = LoggerFactory.getLogger(CustomerUtils.class);

    @Autowired
    protected MasterNumberingService numberingService;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private BillToRepository billToRepository;

    @Autowired
    private ShipToRepository shipToRepository;

    @Autowired
    private PersonalCustomerRepository personalCustomerRepository;

    @Autowired
    private PersonalCustomerSearchRepository personalCustomerSearchRepository;

    @Autowired
    private OrganizationCustomerRepository organizationCustomerRepository;

    @Autowired
    private OrganizationCustomerSearchRepository organizationCustomerSearchRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PersonalCustomerMapper personalCustomerMapper;

    @Autowired
    private OrganizationCustomerMapper organizationCustomerMapper;

    @Autowired
    private ActivitiProcessor processor;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CustomerRelationshipRepository customerRelationshipRepository;

    @Autowired
    private ShipToRepository repoShipTo;

    @Autowired
    private BillToRepository repoBillTo;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private CustomerService customerService;

    @Transactional(propagation = Propagation.REQUIRED)
    public String getNewIdCustomer() {
        String idCust = null;
        do {
            idCust = numberingService.nextCustomerValue();
        } while (customerRepository.findOne(idCust) != null);
        return idCust;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public String getNewIdMPM(Internal internal) {
        String idCust = null;
        do {
            idCust = customerService.nextIdMPM(internal);
        } while (customerRepository.findOne(idCust) != null);
        return idCust;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    protected PersonalCustomer buildPersonalCustomer(Internal internal, PersonalCustomer p) {
        Person person = p.getPerson();
        PersonalCustomer r = null;
        if (person.getIdParty() == null){ person = partyUtils.buildPerson(person);}
        List<Customer> customer = customerRepository.findListCustomerByParty(person);
        if (!customer.isEmpty()){
            r = personalCustomerRepository.findByIdCustomer(customer.iterator().next().getIdCustomer());
        }
        if (customer.isEmpty()) {
            r = _buildPersonalCustomer(internal, p.getIdCustomer(), person);
            return r;
        }
        return r;
    }

    public CustomerRelationship getRelationhip(RelationType relationType, Internal internal, Customer customer) {
        CustomerRelationship r = customerRelationshipRepository.findRelationship(relationType.getIdRelationType(), internal.getIdInternal(), customer.getIdCustomer());
        if (r == null) {
            r = new CustomerRelationship();
            r.setCustomer(customer);
            r.setInternal(internal);
            r.setRelationType(relationType);
            r.setStatusType(partyUtils.getStatusType(BaseConstants.STATUS_ACTIVE, "Actived"));
            r = customerRelationshipRepository.save(r);
        }
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    protected PersonalCustomer _buildPersonalCustomer(Internal internal, String id, Person person) {
        PersonalCustomer r = personalCustomerRepository.findByParty(person);
        if (r == null) {

            partyUtils.getPartyRoleType(person, BaseConstants.ROLE_CUSTOMER_PERSONAL);

            r = new PersonalCustomer();
            r.setIdCustomer(id);
            r.setPerson(person);
            r.setIdRoleType(BaseConstants.ROLE_CUSTOMER);
//            r.setIdMPM(getNewIdMPM(internal));

            if (id == null || customerRepository.findOne(id) != null) {
                r.setIdCustomer(getNewIdCustomer());
            }
            r = personalCustomerRepository.save(r);
            personalCustomerSearchRepository.save(r);

            Map<String, Object> vars = processor.getVariables("person", r.getPerson());
            vars.put("idRoleType", BaseConstants.ROLE_CUSTOMER_PERSONAL);
            vars.put("idGeneral", r.getIdCustomer());
            vars.put("customer", r);
            processor.startProcess("customer-verification", r.getIdCustomer(), vars);

            // Add Customer Relationship
            if (internal != null) {
                getRelationhip(partyUtils.getRelationType(BaseConstants.REL_TYPE_CUSTOMER, "Customer"), internal, r);
            }

        }
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    protected OrganizationCustomer buildOrganizationCustomer(OrganizationCustomer o) {
        Organization organization = o.getOrganization();
        if (organization.getIdParty() == null) organization = partyUtils.buildOrganization(organization.getName());
        OrganizationCustomer r = _buildOrganizationCustomer(o.getIdCustomer(), organization);
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    protected synchronized OrganizationCustomer _buildOrganizationCustomer(String id, Organization organization) {
        OrganizationCustomer r = organizationCustomerRepository.findByParty(organization);
        if (r == null) {
            partyUtils.getPartyRoleType(organization, BaseConstants.ROLE_CUSTOMER_ORGANIZATION);

            r = new OrganizationCustomer();
            r.setIdCustomer(id);
            r.setParty(organization);
            r.setIdRoleType(BaseConstants.ROLE_CUSTOMER);

            if (id == null || customerRepository.findOne(id) != null) {
                r.setIdCustomer(getNewIdCustomer());
            }

            r = organizationCustomerRepository.save(r);
            organizationCustomerSearchRepository.save(r);

            Map<String, Object> vars = processor.getVariables("organization", r.getOrganization());
            vars.put("idRoleType", BaseConstants.ROLE_CUSTOMER_ORGANIZATION);
            vars.put("idGeneral", r.getIdCustomer());
            vars.put("customer", r);
            processor.startProcess("customer-verification", r.getIdCustomer(), vars);

            // Add Customer Relationship
            Internal internal = partyUtils.getCurrentInternal();
            if (internal != null) {
                getRelationhip(partyUtils.getRelationType(BaseConstants.REL_TYPE_CUSTOMER, "Customer"), internal, r);
            }

        }
        return r;
    }

    public PersonalCustomer buildPersonalCustomer(Internal internal, String firstName, String lastName, String pob, ZonedDateTime dob) {
        Person person = partyUtils.buildPerson(firstName, lastName, pob, dob);
        return _buildPersonalCustomer(internal,null, person);
    }

    public PersonalCustomer buildPersonalCustomer(Internal internal, String firstName, String lastName, String cellPhone) {
        Person person = partyUtils.buildPerson(firstName, lastName, cellPhone);
        return _buildPersonalCustomer(internal,null, person);
    }

    public PersonalCustomer buildPersonalCustomer(Internal internal, Person person) {
        return _buildPersonalCustomer(internal, null, person);
    }

    public OrganizationCustomer buildOrganizationCustomer(String name) {
        Organization organization = partyUtils.buildOrganization(name);
        return _buildOrganizationCustomer(null, organization);
    }

    public OrganizationCustomer buildOrganizationCustomer(String id, String name) {
        Organization organization = partyUtils.buildOrganization(name);
        return _buildOrganizationCustomer(id, organization);
    }

    public OrganizationCustomer buildOrganizationCustomer(Organization organization) {
        return _buildOrganizationCustomer( null, organization);
    }

    public ShipTo getShipTo(Customer c) {
        ShipTo shipTo = shipToRepository.findByParty(c.getParty());
        if (shipTo == null) {
            Boolean idHasFound = repoShipTo.findOne(c.getIdCustomer()) != null;

            partyUtils.getPartyRoleType(c.getParty(), BaseConstants.ROLE_CUSTOMER);
            partyUtils.getPartyRoleType(c.getParty(), BaseConstants.ROLE_SHIPTO);

            shipTo = new ShipTo();
            shipTo.setIdRoleType(BaseConstants.ROLE_CUSTOMER);
            shipTo.setParty(c.getParty());
            shipTo.setIdShipTo(c.getIdCustomer());
            if (idHasFound) {
                String idShipTo = null;
                do {
                    idShipTo = numberingService.nextShipToValue();
                } while (repoShipTo.findOne(idShipTo) != null);
                shipTo.setIdShipTo(idShipTo);
            }
            shipTo = repoShipTo.save(shipTo);
        }
        return shipTo;
    }

    public BillTo getBillTo(Customer c) {
        BillTo billTo = billToRepository.findByParty(c.getParty());
        if (billTo == null) {
            Boolean idHasFound = repoShipTo.findOne(c.getIdCustomer()) != null;

            partyUtils.getPartyRoleType(c.getParty(), BaseConstants.ROLE_CUSTOMER);
            partyUtils.getPartyRoleType(c.getParty(), BaseConstants.ROLE_SHIPTO);

            billTo = new BillTo();
            billTo.setIdRoleType(BaseConstants.ROLE_CUSTOMER);
            billTo.setParty(c.getParty());
            billTo.setIdBillTo(c.getIdCustomer());
            if (idHasFound) {
                String idBillTo = null;
                do {
                    idBillTo = numberingService.nextShipToValue();
                } while (repoBillTo.findOne(idBillTo) != null);
                billTo.setIdBillTo(idBillTo);
            }
            billTo = repoBillTo.save(billTo);
        }
        return billTo;
    }


    public void prepare(DelegateExecution execution) {
        CustomerDTO dto = (CustomerDTO) execution.getVariable("customer");
        if (dto == null) {
            execution.setVariable("customerExists", true);
            return;
        }

        Integer partyType = 1;
        if (dto instanceof OrganizationCustomerDTO) partyType = 2;

        execution.setVariable("partyType", partyType);
        execution.setVariable("customerExists", false);

        if (partyType == 1) {
            PersonalCustomerDTO custDto = (PersonalCustomerDTO) dto;
            PersonalCustomer customer = personalCustomerMapper.toEntity(custDto);

            Person person = partyUtils.findPerson(customer.getPerson());
            if (person == null) person = customer.getPerson();

            if (person == null) {
                execution.setVariable("badData", true);
            } else if (person.getIdParty() != null) {
                PersonalCustomer pc = personalCustomerRepository.findByParty(person.getIdParty());
                if (pc != null) execution.setVariable("customerExists", true);
            }

            execution.setVariable("person", partyUtils.getPersonDTO(person));

        } else {
            OrganizationCustomerDTO custDto = (OrganizationCustomerDTO) dto;
            OrganizationCustomer customer = organizationCustomerMapper.toEntity(custDto);

            Organization organization = partyUtils.findOrganization(customer.getOrganization());

            OrganizationCustomer oc = organizationCustomerRepository.findByParty(organization);
            if (oc != null) {
                execution.setVariable("customerExists", true);
                return;
            }
            execution.setVariable("organization", partyUtils.getOrganizationDTO(organization));
        }

    }

    public void buildCustomerData(DelegateExecution execution) {
        Person person = execution.getVariable("person", Person.class);
        Organization organization = execution.getVariable("organization", Organization.class);
        if (person != null) execution.setVariable("party", person);
        else execution.setVariable("party", organization);
    }


    public void buildParty(DelegateExecution execution) {
        CustomerDTO dto = (CustomerDTO) execution.getVariable("customer");
        Integer partyType = (Integer) execution.getVariable("partyType");

        if (partyType.equals(1)) {
            Person person = partyUtils.getPerson((PersonDTO) execution.getVariable("person"));

            person = partyUtils.buildPerson(person);
            if (person.getIdParty() == null) person = partyUtils.buildPerson(person);

            partyUtils.getPartyRoleType(person, BaseConstants.ROLE_CUSTOMER_PERSONAL);
            partyUtils.getPartyRoleType(person, BaseConstants.ROLE_CUSTOMER);

            PersonalCustomer r = new PersonalCustomer();
            r.setIdCustomer(dto.getIdCustomer());
            r.setPerson(person);
            r.setIdMPM(dto.getIdMPM());
            r.setIdRoleType(BaseConstants.ROLE_CUSTOMER_PERSONAL);

            if (dto.getIdCustomer() == null || customerRepository.findOne(dto.getIdCustomer()) != null) {
                r.setIdCustomer(getNewIdCustomer());
            }
            r = personalCustomerRepository.save(r);
            personalCustomerSearchRepository.save(r);

            execution.setVariable("idParty", person.getIdParty());
            execution.setVariable("idGeneral", r.getIdCustomer());
            execution.setVariable("idRoleType", BaseConstants.ROLE_CUSTOMER_PERSONAL);

        } else {
            Organization organization = partyUtils.getOrganization((OrganizationDTO) execution.getVariable("organization"));
            if (organization.getIdParty() == null) organization = partyUtils.buildOrganization(organization);

            partyUtils.getPartyRoleType(organization, BaseConstants.ROLE_CUSTOMER_ORGANIZATION);
            partyUtils.getPartyRoleType(organization, BaseConstants.ROLE_CUSTOMER);

            OrganizationCustomer r = new OrganizationCustomer();
            r.setIdCustomer(dto.getIdCustomer());
            r.setOrganization(organization);
            r.setIdMPM(dto.getIdMPM());
            r.setIdRoleType(BaseConstants.ROLE_CUSTOMER_ORGANIZATION);

            if (dto.getIdCustomer() == null || customerRepository.findOne(dto.getIdCustomer()) != null) {
                r.setIdCustomer(getNewIdCustomer());
            }
            r = organizationCustomerRepository.save(r);
            organizationCustomerRepository.save(r);

            execution.setVariable("idParty", organization.getIdParty());
            execution.setVariable("idGeneral", r.getIdCustomer());
            execution.setVariable("idRoleType", BaseConstants.ROLE_CUSTOMER_ORGANIZATION);
        }
    }

    public void buildWelcomeLetter(DelegateExecution execution) {
    }


    public void buildUplinkData(DelegateExecution execution) {
        String key = execution.getProcessInstanceBusinessKey();
        PersonalCustomer customer = personalCustomerRepository.findOne(key);
        if (customer != null) {

            Internal internal = partyUtils.getCurrentInternal();
            if (internal != null) {

                AxCustomerDTO dto = new AxCustomerDTO();
                dto.setMainAccount(customer.getIdMPM());
                dto.setName(customer.getPerson().getName());
                // dto.setAddress(customer.getPerson().getPostalAddress().getAddress1());

                dto.setDataArea(internal.getRoot().getIdInternal());
                // dto = restTemplate.postForObject("/api/ax_customer", dto, AxCustomerDTO.class);

                ObjectMapper objectMapper = new ObjectMapper();
                try {
                    objectMapper.writeValue(new File("d:/tmp/customer.json"), dto);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
//            Map<String, String> params = new HashMap<String, String>();
//            params.put("id", customer.getIdMPM());
//            AxCustomerDTO dto = restTemplate.getForObject("/api/ax_customer/{id}", AxCustomerDTO.class, params);
//            // Customer tidak di kenal
//            if (dto == null) {
//
//                dto = new AxCustomerDTO();
//                dto.setIdcustomer(customer.getIdMPM());
//                dto.setIdparty(customer.getPerson().getIdParty());
//                dto.setIdroletype(customer.getIdRoleType());
//                dto.setIdsegmen(10);
//                dto.getIdCostumerType();
//
//                dto = restTemplate.postForObject("/api/ax_customer", dto, AxCustomerDTO.class);
//            }
        }
    }

    public void cleanUpDoubleCustomer() {
        List<String> doubles = personRepository.queryDoubleNik();
        System.out.println("Do Check Double");
        for (String id: doubles) {
            System.out.println("Check Id " + id);
            if ( id != null) {
                List<Person> persons = personRepository.findAllByPersonalIdNumber(id);
                if (persons.size() > 1) {
                    Integer size = persons.size();
                    // Tandai sebagai personal-id-beda
                    for (int i = 1; i < size; i++) {
                        Person p = persons.get(i);
                        p.setPersonalIdNumber(p.getPersonalIdNumber() + "-" + i);
                        personRepository.save(p);
                        System.out.println("Check Person " + p.toString());
                    }
                }
            }
        }
    }
}
