package id.atiila.service;

import id.atiila.domain.PaymentApplication;
import id.atiila.repository.PaymentApplicationRepository;
import id.atiila.repository.search.PaymentApplicationSearchRepository;
import id.atiila.service.dto.PaymentApplicationDTO;
import id.atiila.service.mapper.PaymentApplicationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing PaymentApplication.
 * BeSmart Team
 */

@Service
@Transactional
public class PaymentApplicationService {

    private final Logger log = LoggerFactory.getLogger(PaymentApplicationService.class);

    private final PaymentApplicationRepository paymentApplicationRepository;

    private final PaymentApplicationMapper paymentApplicationMapper;

    private final PaymentApplicationSearchRepository paymentApplicationSearchRepository;

    public PaymentApplicationService(PaymentApplicationRepository paymentApplicationRepository, PaymentApplicationMapper paymentApplicationMapper, PaymentApplicationSearchRepository paymentApplicationSearchRepository) {
        this.paymentApplicationRepository = paymentApplicationRepository;
        this.paymentApplicationMapper = paymentApplicationMapper;
        this.paymentApplicationSearchRepository = paymentApplicationSearchRepository;
    }

    /**
     * Save a paymentApplication.
     *
     * @param paymentApplicationDTO the entity to save
     * @return the persisted entity
     */
    public PaymentApplicationDTO save(PaymentApplicationDTO paymentApplicationDTO) {
        log.debug("Request to save PaymentApplication : {}", paymentApplicationDTO);
        PaymentApplication paymentApplication = paymentApplicationMapper.toEntity(paymentApplicationDTO);
        paymentApplication = paymentApplicationRepository.save(paymentApplication);
        PaymentApplicationDTO result = paymentApplicationMapper.toDto(paymentApplication);
        paymentApplicationSearchRepository.save(paymentApplication);
        return result;
    }

    /**
     * Get all the paymentApplications.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PaymentApplicationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PaymentApplications");
        return paymentApplicationRepository.findAll(pageable)
            .map(paymentApplicationMapper::toDto);
    }

    /**
     * Get one paymentApplication by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PaymentApplicationDTO findOne(UUID id) {
        log.debug("Request to get PaymentApplication : {}", id);
        PaymentApplication paymentApplication = paymentApplicationRepository.findOne(id);
        return paymentApplicationMapper.toDto(paymentApplication);
    }

    /**
     * Delete the paymentApplication by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete PaymentApplication : {}", id);
        paymentApplicationRepository.delete(id);
        paymentApplicationSearchRepository.delete(id);
    }

    /**
     * Search for the paymentApplication corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PaymentApplicationDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of PaymentApplications for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idPayment = request.getParameter("idPayment");
        String idBilling = request.getParameter("idBilling");

        if (idPayment != null) {
            q.withQuery(matchQuery("payment.idPayment", idPayment));
        }
        else if (idBilling != null) {
            q.withQuery(matchQuery("billing.idBilling", idBilling));
        }

        Page<PaymentApplication> result = paymentApplicationSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(paymentApplicationMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PaymentApplicationDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered PaymentApplicationDTO");
        String idPayment = request.getParameter("idPayment");
        String idBilling = request.getParameter("idBilling");

        if (idPayment != null) {
            return paymentApplicationRepository.findByIdPayment(UUID.fromString(idPayment), pageable).map(paymentApplicationMapper::toDto);
        }
        else if (idBilling != null) {
            return paymentApplicationRepository.findByIdBilling(UUID.fromString(idBilling), pageable).map(paymentApplicationMapper::toDto);
        }

        return paymentApplicationRepository.findAll(pageable).map(paymentApplicationMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, PaymentApplicationDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<PaymentApplicationDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        paymentApplicationSearchRepository.deleteAll();
        List<PaymentApplication> paymentApplication =  paymentApplicationRepository.findAll();
        for (PaymentApplication m: paymentApplication) {
            paymentApplicationSearchRepository.save(m);
            log.debug("Data paymentApplication save !...");
        }
    }

}
