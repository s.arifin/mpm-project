package id.atiila.service;

import id.atiila.domain.*;
import id.atiila.repository.InventoryItemRepository;
import id.atiila.repository.PickingSlipRepository;
import id.atiila.repository.search.PickingSlipSearchRepository;
import id.atiila.service.dto.PickingSlipDTO;
import id.atiila.service.dto.ProductDTO;
import id.atiila.service.dto.SalesUnitRequirementDTO;
import id.atiila.service.dto.VehicleSalesOrderDTO;
import id.atiila.service.mapper.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing PickingSlip.
 * BeSmart Team
 */

@Service
@Transactional
public class PickingSlipService {

    private final Logger log = LoggerFactory.getLogger(PickingSlipService.class);

    private final PickingSlipRepository pickingSlipRepository;

    private final PickingSlipMapper pickingSlipMapper;

    private final PickingSlipSearchRepository pickingSlipSearchRepository;

    @Autowired private InventoryItemRepository inventoryItemRepository;

    @Autowired private GoodService goodService;

    @Autowired private GoodMapper goodMapper;

//    @Autowired private ProductService productService;
//
//    @Autowired private ProductMapper productMapper;
//
//    @Autowired private VehicleSalesOrderService vehicleSalesOrderService;
//
//    @Autowired private VehicleSalesOrderMapper vehicleSalesOrderMapper;
//
//    @Autowired private SalesUnitRequirementService salesUnitRequirementService;
//
//    @Autowired private PartyService partyService;
//
//    @Autowired private PartyMapper partyMapper;
//
//    @Autowired private OrderItemService orderItemService;
//
//    @Autowired private OrderItemMapper orderItemMapper;
//

    public PickingSlipService(PickingSlipRepository pickingSlipRepository, PickingSlipMapper pickingSlipMapper, PickingSlipSearchRepository pickingSlipSearchRepository) {
        this.pickingSlipRepository = pickingSlipRepository;
        this.pickingSlipMapper = pickingSlipMapper;
        this.pickingSlipSearchRepository = pickingSlipSearchRepository;
    }

    /**
     * Save a pickingSlip.
     *
     * @param pickingSlipDTO the entity to save
     * @return the persisted entity
     */
    public PickingSlipDTO save(PickingSlipDTO pickingSlipDTO) {
        log.debug("Request to save PickingSlip : {}", pickingSlipDTO);
        PickingSlip pickingSlip = pickingSlipMapper.toEntity(pickingSlipDTO);
        pickingSlip = pickingSlipRepository.save(pickingSlip);
        PickingSlipDTO result = pickingSlipMapper.toDto(pickingSlip);
        pickingSlipSearchRepository.save(pickingSlip);
        return result;
    }

    /**
     *  Get all the pickingSlips.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PickingSlipDTO> findAll(Pageable pageable,Integer idstatustype) {
        log.debug("Request to get all PickingSlips");
        if (idstatustype != null){
            return pickingSlipRepository.findActivePickingSlipbyShipmentStatus(pageable)
               .map(pickingSlipMapper::toDto);
        } else {
            return pickingSlipRepository.findActivePickingSlip(pageable)
                .map(pickingSlipMapper::toDto);
        }
    }

//    @Transactional(readOnly = true)
//    public Page<PickingSlipDTO> findAllbyShipmentStatus(Pageable pageable) {
//        log.debug("Request to get all PickingSlips");
//        return pickingSlipRepository.findActivePickingSlipbyShipmentStatus(pageable)
//            .map(pickingSlipMapper::toDto);
//    }

    /**
     *  Get one pickingSlip by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PickingSlipDTO findOne(UUID id) {
        log.debug("Request to get PickingSlip : {}", id);
        PickingSlip pickingSlip = pickingSlipRepository.findOne(id);
        return pickingSlipMapper.toDto(pickingSlip);
    }

    /**
     *  Delete the  pickingSlip by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete PickingSlip : {}", id);
        pickingSlipRepository.delete(id);
        pickingSlipSearchRepository.delete(id);
    }

    /**
     * Search for the pickingSlip corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PickingSlipDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PickingSlips for query {}", query);
        Page<PickingSlip> result = pickingSlipSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(pickingSlipMapper::toDto);
    }

    public PickingSlipDTO processExecuteData(Integer id, String param, PickingSlipDTO dto) {
        PickingSlipDTO r = dto;
        if (r != null) {
            switch (id) {
                case 101:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<PickingSlipDTO> processExecuteListData(Integer id, String param, Set<PickingSlipDTO> dto) {
        Set<PickingSlipDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public PickingSlipDTO changePickingSlipStatus(PickingSlipDTO dto, Integer id) {
        if (dto != null) {
			PickingSlip e = pickingSlipRepository.findOne(dto.getIdSlip());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        pickingSlipSearchRepository.delete(dto.getIdSlip());
                        break;
                    default:
                        pickingSlipSearchRepository.save(e);
                }
				pickingSlipRepository.save(e);
			}
		}
        return dto;
    }


    private void xbuildPSOneFromVSO(VehicleSalesOrderDTO vsoDto) {
//
//        //save picking list
//
//        VehicleSalesOrder vso = vehicleSalesOrderMapper.toEntity(vehicleSalesOrderService.findOne(vsoDto.getId())) ;
//        Product product = productMapper.toEntity(productService.findOne(vso.getDetails().iterator().next().getProduct().getIdProduct())) ;
//        SalesUnitRequirementDTO salesUnitRequirementDto = salesUnitRequirementService.findOne(vsoDto.getSalesUnitRequirement().getId());
//
//        // party -> internal Id
//        Party party =  partyMapper.toEntity(partyService.findOne(salesUnitRequirementDto.getInternalId()) ) ;
//
//        // create inventory item
//        InventoryItem inventoryItem = new InventoryItem();
//        inventoryItem.setOwner( party );
//        inventoryItem.setProduct( (Good) product );
//        inventoryItem.setQtyBooking(1);
//
//        OrderItem orderItem = orderItemMapper.toEntity(orderItemService.findOne(vso.getDetails().iterator().next().getIdOrderItem()) ) ;
//
//        //create picking list
//        PickingSlipDTO ps = new PickingSlipDTO();
//        ps.setOrderItemId(orderItem.getIdOrderItem());
//        ps.setShipToId(vsoDto.getSalesUnitRequirement().getShipToId());
//        ps.setShipToName(vsoDto.getSalesUnitRequirement().getShipToName());
//        ps.setInventoryItemId(inventoryItem.getIdInventoryItem());
//
//        save(ps);

    }

    public void buildPicking(Orders o, Internal intr, String noka, String nosin) {


        for (OrderItem d : o.getDetails()) {
            Double qtyFilled = 0d;

            for (OrderShipmentItem s: d.getShipmentItems()) {
                qtyFilled = qtyFilled + s.getQty();
            }

            //Check apakah perlu dibuat picking
            if (qtyFilled < d.getQty()) {
                int qtyOh = 0;
                int qtyWh=0;
                log.info("masuk qtyfilled < getQty");
                //buat picking sejumlah qty unfill
                //Get Stock by Owner, product = order item product, feature = order item feature
                //qtyWh = repo.findSumQty(idproduct, idparty, idfeature );

                //Good good = goodMapper.toEntity(goodService.findOne(d.getIdProduct())) ;
                if (noka ==null){
                    log.info("find sum qty tanpa noka");
                    qtyWh = inventoryItemRepository.findSumQty(d.getIdProduct(), intr.getOrganization().getIdParty(), d.getIdFeature() );
                } else {
                    log.info("find sum qty dg noka");
                    qtyWh = inventoryItemRepository.findSumQty(d.getIdProduct(), intr.getIdInternal(), d.getIdFeature(), noka, nosin );
                }

                int qtyPick = Math.min(qtyOh, qtyWh);
                if (qtyPick > 0) {
                    log.info("qtypick  > 0 ");
                    List<InventoryItem> inventoryItems;
                    if (noka == null) {
                        log.info("list inv item tanpa noka");
                        inventoryItems= inventoryItemRepository.findByIdProductAndIdOwnerAndIdFeatureOrderByDateCreated(d.getIdProduct(),
                            intr.getOrganization().getIdParty(), d.getIdFeature());
                    } else {
                        log.info("list inv item dengan noka");
                        inventoryItems = inventoryItemRepository.getByIdProductAndIdOwnerAndIdFeatureAndRegNokaAndRegNosinOrderByDateCreated(d.getIdProduct(),
                            intr.getIdInternal(), d.getIdFeature(), noka, nosin);
                    }
                    for (InventoryItem inventoryItem :inventoryItems){
                        log.info("generate pricking slip");
                        PickingSlip n = new PickingSlip();
                        n.setOrderItem(d);
                        //n.setShipTo(d.get);
                        n.setInventoryItem(inventoryItem);
                        pickingSlipRepository.save(n);
                    }
                }
            }
        }
    }






    // EO Class
}
