package id.atiila.service;

import id.atiila.domain.WorkOrder;
import id.atiila.repository.WorkOrderRepository;
import id.atiila.repository.search.WorkOrderSearchRepository;
import id.atiila.service.dto.WorkOrderDTO;
import id.atiila.service.mapper.WorkOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing WorkOrder.
 * BeSmart Team
 */

@Service
@Transactional
public class WorkOrderService {

    private final Logger log = LoggerFactory.getLogger(WorkOrderService.class);

    private final WorkOrderRepository workOrderRepository;

    private final WorkOrderMapper workOrderMapper;

    private final WorkOrderSearchRepository workOrderSearchRepository;

    public WorkOrderService(WorkOrderRepository workOrderRepository, WorkOrderMapper workOrderMapper, WorkOrderSearchRepository workOrderSearchRepository) {
        this.workOrderRepository = workOrderRepository;
        this.workOrderMapper = workOrderMapper;
        this.workOrderSearchRepository = workOrderSearchRepository;
    }

    /**
     * Save a workOrder.
     *
     * @param workOrderDTO the entity to save
     * @return the persisted entity
     */
    public WorkOrderDTO save(WorkOrderDTO workOrderDTO) {
        log.debug("Request to save WorkOrder : {}", workOrderDTO);
        WorkOrder workOrder = workOrderMapper.toEntity(workOrderDTO);
        workOrder = workOrderRepository.save(workOrder);
        WorkOrderDTO result = workOrderMapper.toDto(workOrder);
        workOrderSearchRepository.save(workOrder);
        return result;
    }

    /**
     *  Get all the workOrders.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WorkOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WorkOrders");
        return workOrderRepository.findAll(pageable)
            .map(workOrderMapper::toDto);
    }

    /**
     *  Get one workOrder by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public WorkOrderDTO findOne(UUID id) {
        log.debug("Request to get WorkOrder : {}", id);
        WorkOrder workOrder = workOrderRepository.findOne(id);
        return workOrderMapper.toDto(workOrder);
    }

    /**
     *  Delete the  workOrder by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete WorkOrder : {}", id);
        workOrderRepository.delete(id);
        workOrderSearchRepository.delete(id);
    }

    /**
     * Search for the workOrder corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WorkOrderDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WorkOrders for query {}", query);
        Page<WorkOrder> result = workOrderSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(workOrderMapper::toDto);
    }

}
