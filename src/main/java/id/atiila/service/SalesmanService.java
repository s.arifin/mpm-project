package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.SalesmanSearchRepository;
import id.atiila.service.dto.PersonDTO;
import id.atiila.service.dto.SalesmanDTO;
import id.atiila.service.mapper.PersonMapper;
import id.atiila.service.mapper.SalesmanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Service Implementation for managing Salesman.
 * BeSmart Team
 */

@Service
@Transactional
public class SalesmanService {

    private final Logger log = LoggerFactory.getLogger(SalesmanService.class);

    private final SalesmanRepository salesmanRepository;

    private final SalesmanMapper salesmanMapper;

    private final SalesmanSearchRepository salesmanSearchRepository;

    @Autowired
    private PersonService personSvc;

    @Autowired
    private MasterNumberingService numberSvc;

    @Autowired
    private Faker faker;

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private PersonRepository personRepo;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private PositionUtils positionUtils;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private UserMediatorRepository userMediatorRepository;

    @Autowired
    private PositionTypeRepository positionTypeRepository;

    @Autowired
    private PositionRepository positionRepository;

    public SalesmanService(SalesmanRepository salesmanRepository, SalesmanMapper salesmanMapper, SalesmanSearchRepository salesmanSearchRepository) {
        this.salesmanRepository = salesmanRepository;
        this.salesmanMapper = salesmanMapper;
        this.salesmanSearchRepository = salesmanSearchRepository;
    }

    /**
     * Save a salesman.
     *
     * @param dto the entity to save
     * @return the persisted entity
     */
    @Transactional(propagation =  Propagation.REQUIRED)
    public SalesmanDTO save(SalesmanDTO dto) {
        log.debug("Request to save Salesman : {}", dto);
        Salesman salesman = salesmanMapper.toEntity(dto);
        Boolean isNewData = salesman.getIdPartyRole() == null;

        if (isNewData) {
            Salesman slsm = null;

            //Cek Jika Konsumen sudah mempunyai ID
            if (dto.getIdSalesman() != null) {
                slsm = salesmanRepository.findOneByIdSalesman(dto.getIdSalesman());
                if (slsm != null) return salesmanMapper.toDto(slsm);
            }

            //Check Person, apakah sudah ada di database
            Person p = personSvc.getWhenExists(dto.getPerson());

            if (p != null) {
                slsm = salesmanRepository.findOneByParty(p);
                if (slsm != null) return salesmanMapper.toDto(slsm);
                salesman.setPerson(p);
            };

            //Tetap menggunakan person default
            // salesman.setStatus(BaseConstants.STATUS_ACTIVE);

            if (dto.getIdSalesman() == null) {
                String idSalesman = null;
                while (idSalesman == null || salesmanRepository.findOneByIdSalesman(idSalesman) != null) {
                    idSalesman = numberSvc.nextValue("idsalesman", 8000l).toString();
                }
                salesman.setIdSalesman(idSalesman);
            }
            else
                salesman.setIdSalesman(dto.getIdSalesman());
        }

        salesman.setUserName(dto.getUserName());
        salesman = salesmanRepository.save(salesman);
        SalesmanDTO result = salesmanMapper.toDto(salesman);
        salesmanSearchRepository.save(salesman);

        this.buildUserMediator(salesman, dto.getUserName());
        return result;
    }

    /**
     *  Get all the salesmen.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesmanDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Salesmen");
        return salesmanRepository.findAll(pageable)
            .map(salesmanMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<SalesmanDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        String idInternal = request.getParameter("idInternal");
        String userName = request.getParameter("userName");
        UUID idKorsal = null;
        Integer idPositionType = request.getParameter("idPositionType") == null ?
            null : Integer.parseInt(request.getParameter("idPositionType"));
        UUID idCoordinator = request.getParameter("idCoordinator") == null ? null : UUID.fromString(request.getParameter("idCoordinator"));
        Page<Salesman> korsal = salesmanRepository.findAllSalesmanByUsername(userName, pageable);
        if (!korsal.getContent().isEmpty()) {
            idKorsal = korsal.iterator().next().getIdPartyRole();
            log.debug("ini korsal :" + idKorsal);
        }
        if(userName != null){
            return salesmanRepository.findAllSalesmanByIdkKorsal(idKorsal, pageable)
                .map(salesmanMapper::toDto);
        }
        if (idCoordinator == null && userName == null) {
            log.debug("Request to get all Salesmen");
            return salesmanRepository.findAllByInternal(idInternal, idPositionType, pageable)
                .map(salesmanMapper::toDto);
        }  else {
            log.debug("Request to get All Salesmen by Coordinators");
            return salesmanRepository.findAllSalesmanByIdCoordinator(idCoordinator, pageable)
                .map(salesmanMapper::toDto);
        }
    }

    /**
     *  Get one salesman by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SalesmanDTO findOne(UUID id) {
        log.debug("Request to get Salesman : {}", id);
        Salesman salesman = salesmanRepository.findOne(id);
        return salesmanMapper.toDto(salesman);
    }

    /**
     *  Delete the  salesman by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Salesman : {}", id);
        salesmanRepository.delete(id);
        salesmanSearchRepository.delete(id);
    }

    /**
     * Search for the salesman corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesmanDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Salesmen for query {}", query);
        Page<Salesman> result = salesmanSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(salesmanMapper::toDto);
    }

    public void preInitialize(String idinternal1, String idinternal2){
        Internal int1 = internalUtils.findOne(idinternal1);
        Internal int2 = internalUtils.findOne(idinternal2);

        initialize(int1, int2);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void initialize(Internal internal1, Internal internal2){
        SalesmanDTO korsal, tl, sales;
        PersonDTO p;
        Person person;

        List<Salesman> salesmen = salesmanRepository.findAll();

        if (salesmen.size() <= 1){

            // INTERNAL 01
            /* -------------------------------------------------------------------------------------------*/
            //Create Korsal
            korsal = buildKorsal(internal1, "korsal01", 1);

            //Create TL
            tl = buildTL(internal1, korsal, "teamleader01", 1);

            //Create Salesman
            buildSales(internal1,korsal,tl,"sales01", 2);
            buildSales(internal1, korsal, null, "sales02", 3);

            // INTERNAL 02
            /* -------------------------------------------------------------------------------------------*/

            //Create Korsal
            korsal = buildKorsal(internal2, "korsal02", 2);

            //Create TL
            tl = buildTL(internal2, korsal, "teamleader02", 4);

            //Create Salesman
            buildSales(internal2,korsal,tl,"sales03", 5);
            buildSales(internal2, korsal, null, "sales04", 6);
        }
    }

    private SalesmanDTO buildKorsal(Internal internal, String username, Integer sequence){
        PersonDTO p = faker.buildMale();
        Person person = personSvc.getWhenExists(p);

        if (person == null) {
            p = personSvc.save(p);
            person = personMapper.toEntity(p);
        }
        else {
            p = personMapper.toDto(person);
        }

        partyUtils.buildUser(internal, username,
            person.getFirstName().toLowerCase()+"_"+person.getLastName().toLowerCase()+"@localhost", person);

        Position position = positionUtils.buildPosition(internal, BaseConstants.POSITION_KORSAL, sequence);
        positionUtils.buildPositionFilled(position, person);

        Salesman korsal = new Salesman();
        korsal.setPerson(personRepo.findOne(person.getIdParty()));
        korsal.setCoordinator(true);
        korsal.setTeamLeader(false);
        korsal = salesmanRepository.save(korsal);
        return salesmanMapper.toDto(korsal);
    }

    private SalesmanDTO buildTL(Internal internal, SalesmanDTO korsal, String username, Integer sequence){
        PersonDTO p = faker.buildeFemale();
        Person person = personSvc.getWhenExists(p);

        if (person == null) {
            p = personSvc.save(p);
            person = personMapper.toEntity(p);
        }
        else {
            p = personMapper.toDto(person);
        }

        partyUtils.buildUser(internal, username,
            person.getFirstName().toLowerCase()+"_"+person.getLastName().toLowerCase()+"@localhost", person);

        Position position = positionUtils.buildPosition(internal, BaseConstants.POSITION_SALESMAN, sequence);
        positionUtils.buildPositionFilled(position, person);

        Salesman tl = new Salesman();
        tl.setCoordinator(false);
        tl.setTeamLeader(true);
        tl.setCoordinatorSales(salesmanRepository.findOne(korsal.getIdPartyRole()));
        tl.setPerson(personRepo.findOne(person.getIdParty()));
        tl = salesmanRepository.save(tl);
        return salesmanMapper.toDto(tl);
    }

    private void buildSales(Internal internal, SalesmanDTO korsal, SalesmanDTO tl, String username, Integer sequence)
    {
        PersonDTO p = faker.buildeFemale();
        Person person = personSvc.getWhenExists(p);

        if (person == null) {
            p = personSvc.save(p);
            person = personMapper.toEntity(p);
        }
        else {
            p = personMapper.toDto(person);
        }

        partyUtils.buildUser(internal, username,
            person.getFirstName().toLowerCase()+"_"+person.getLastName().toLowerCase()+"@localhost", person);

        Position position = positionUtils.buildPosition(internal, BaseConstants.POSITION_SALESMAN, sequence);
        positionUtils.buildPositionFilled(position, person);

        Salesman sales = new Salesman();
        sales.setCoordinator(false);
        sales.setTeamLeader(false);
        sales.setCoordinatorSales(salesmanRepository.findOne(korsal.getIdPartyRole()));

        if (tl != null) {sales.setTeamLeaderSales(salesmanRepository.findOne(tl.getIdPartyRole()));}

        sales.setPerson(personRepo.findOne(person.getIdParty()));
        salesmanRepository.save(sales);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        salesmanSearchRepository.deleteAll();
        List<Salesman> salesmans =  salesmanRepository.findAll();
        for (Salesman m: salesmans) {
            salesmanSearchRepository.save(m);
            log.debug("Data salesman save !...");
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildUserMediator(Salesman salesman, String userName) {
        PositionType positionTypeSales = positionTypeRepository.findOne(BaseConstants.POSITION_SALESMAN);
        PositionType positionTypeKorsal = positionTypeRepository.findOne(BaseConstants.POSITION_KORSAL);
        Internal internal = partyUtils.getCurrentInternal();
        List<UserMediator> listUM = userMediatorRepository.findByPersonAndInternal(salesman.getPerson(), internal);
        List<UserMediator> listUMEmail = userMediatorRepository.findByEmailAndInternal(salesman.getPerson().getPrivateMail(), internal);
        List<UserMediator> listUMUsername = userMediatorRepository.findByUsernameAndInternal(salesman.getUserName(), internal);

        List<Position> positions = positionRepository.listAllPosition();
        Integer seq = positions.get(0).getSequenceNumber();
        Integer nextSeq = seq + 1;
        Person person = salesman.getPerson();

        if(listUM.isEmpty()) {

            if (!listUMEmail.isEmpty()) {
                throw new DmsException("Email Sudah ada didalam DataBase");
            }
            if (!listUMUsername.isEmpty()) {
                throw new DmsException("Username Sudah ada didalam DataBase");
            }
            UserMediator userMediator = new UserMediator();
            userMediator.setPerson(person);
            userMediator.setEmail(person.getPrivateMail());
            userMediator.setInternal(internal);
            userMediator.setUserName(userName);
            userMediator = userMediatorRepository.save(userMediator);

            if( salesman.getCoordinator().equals(true)){
                Position position = new Position();
                position.setInternal(userMediator.getInternal());
                position.setUserName(userMediator.getUserName());
                position.setOwner(userMediator);
                position.setSequenceNumber(nextSeq);
                position.setPositionType(positionTypeKorsal);
                position.setDescription(userMediator.getUserName());
                position = positionRepository.save(position);
            } else if (salesman.getCoordinator().equals(false)) {
                Position position = new Position();
                position.setInternal(userMediator.getInternal());
                position.setUserName(userMediator.getUserName());
                position.setOwner(userMediator);
                position.setSequenceNumber(nextSeq);
                position.setPositionType(positionTypeSales);
                position.setDescription(userMediator.getUserName());
                position = positionRepository.save(position);
            }


            partyUtils.buildAppUser(userMediator);
        }



    }
}
