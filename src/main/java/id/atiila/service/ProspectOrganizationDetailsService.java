package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.Organization;
import id.atiila.domain.OrganizationCustomer;
import id.atiila.domain.ProspectOrganizationDetails;
import id.atiila.domain.ProspectOrganization;
import id.atiila.repository.OrganizationCustomerRepository;
import id.atiila.repository.ProspectOrganizationDetailsRepository;
import id.atiila.repository.search.ProspectOrganizationDetailsSearchRepository;
import id.atiila.service.dto.OrganizationCustomerDTO;
import id.atiila.service.dto.PersonalCustomerDTO;
import id.atiila.service.dto.ProspectOrganizationDetailsDTO;
import id.atiila.service.mapper.OrganizationCustomerMapper;
import id.atiila.service.mapper.ProspectOrganizationDetailsMapper;
import id.atiila.repository.ProspectOrganizationRepository;
import id.atiila.domain.ProspectOrganization;
import id.atiila.service.dto.ProspectOrganizationDTO;
import id.atiila.service.mapper.ProspectOrganizationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing ProspectOrganizationDetails.
 * BeSmart Team
 */

@Service
@Transactional
public class ProspectOrganizationDetailsService {

    private final Logger log = LoggerFactory.getLogger(ProspectOrganizationDetailsService.class);

    private final ProspectOrganizationDetailsRepository prospectOrganizationDetailsRepository;

    private final ProspectOrganizationDetailsMapper prospectOrganizationDetailsMapper;

    private final ProspectOrganizationDetailsSearchRepository prospectOrganizationDetailsSearchRepository;


    @Autowired
    private ProspectOrganizationService poSrv;

    @Autowired
    private ProspectOrganizationRepository poRepo;

    @Autowired
    private ProspectOrganizationMapper poMapper;

    @Autowired
    private OrganizationCustomerMapper ocMapper;

    @Autowired
    private OrganizationCustomerService ocSrv;

    @Autowired
    private PersonalCustomerService pcSrv;

    @Autowired
    private SalesUnitRequirementService surSrv;

    public ProspectOrganizationDetailsService(ProspectOrganizationDetailsRepository prospectOrganizationDetailsRepository, ProspectOrganizationDetailsMapper prospectOrganizationDetailsMapper, ProspectOrganizationDetailsSearchRepository prospectOrganizationDetailsSearchRepository) {
        this.prospectOrganizationDetailsRepository = prospectOrganizationDetailsRepository;
        this.prospectOrganizationDetailsMapper = prospectOrganizationDetailsMapper;
        this.prospectOrganizationDetailsSearchRepository = prospectOrganizationDetailsSearchRepository;
    }

    /**
     * Save a prospectOrganizationDetails.
     *
     * @param prospectOrganizationDetailsDTO the entity to save
     * @return the persisted entity
     */
    public ProspectOrganizationDetailsDTO save(ProspectOrganizationDetailsDTO prospectOrganizationDetailsDTO) {
        log.debug("Request to save ProspectOrganizationDetails : {}", prospectOrganizationDetailsDTO);
        ProspectOrganizationDetails prospectOrganizationDetails = prospectOrganizationDetailsMapper.toEntity(prospectOrganizationDetailsDTO);
        prospectOrganizationDetails = prospectOrganizationDetailsRepository.save(prospectOrganizationDetails);
        ProspectOrganizationDetailsDTO result = prospectOrganizationDetailsMapper.toDto(prospectOrganizationDetails);
        prospectOrganizationDetailsSearchRepository.save(prospectOrganizationDetails);
        return result;
    }

    /**
     *  Get all the prospectOrganizationDetails.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProspectOrganizationDetailsDTO> findAll(Pageable pageable, UUID idProspects) {
        log.debug("Request to get all ProspectOrganizationDetails");
        Page<ProspectOrganizationDetailsDTO> prospectOrganizationDetailsDTO = null;
        if (idProspects != null){
            prospectOrganizationDetailsDTO = prospectOrganizationDetailsRepository.findAllbyProspect(idProspects,pageable).map(prospectOrganizationDetailsMapper::toDto);
        } else {
            prospectOrganizationDetailsDTO = prospectOrganizationDetailsRepository.findAll(pageable)
                .map(prospectOrganizationDetailsMapper::toDto);
        }
        return prospectOrganizationDetailsDTO;
    }

    /**
     *  Get one prospectOrganizationDetails by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ProspectOrganizationDetailsDTO findOne(UUID id) {
        log.debug("Request to get ProspectOrganizationDetails : {}", id);
        ProspectOrganizationDetails prospectOrganizationDetails = prospectOrganizationDetailsRepository.findOne(id);
        return prospectOrganizationDetailsMapper.toDto(prospectOrganizationDetails);
    }

    /**
     *  Delete the  prospectOrganizationDetails by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ProspectOrganizationDetails : {}", id);
        prospectOrganizationDetailsRepository.delete(id);
        prospectOrganizationDetailsSearchRepository.delete(id);
    }

    /**
     * Search for the prospectOrganizationDetails corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProspectOrganizationDetailsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ProspectOrganizationDetails for query {}", query);
        Page<ProspectOrganizationDetails> result = prospectOrganizationDetailsSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(prospectOrganizationDetailsMapper::toDto);
    }

    public ProspectOrganizationDetailsDTO processExecuteData(Integer id, String param, ProspectOrganizationDetailsDTO dto) {
        ProspectOrganizationDetailsDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<ProspectOrganizationDetailsDTO> processExecuteListData(Integer id, String param, Set<ProspectOrganizationDetailsDTO> dto) {
        Set<ProspectOrganizationDetailsDTO> r = dto;
//        r.iterator().next().getIdProspect();

        if (r != null) {
            switch (id) {
                case 102:
                      ProspectOrganization po = poRepo.findOne(r.iterator().next().getIdProspect());
                      ProspectOrganizationDTO poDto = poMapper.toDto(po);
                     OrganizationCustomerDTO ocDto = buildOrganizationCustomerSUR(po);
//                     OrganizationCustomerDTO ocDto = ocMapper.toDto(oc);

                    for(ProspectOrganizationDetailsDTO prospectOrganizationDetailsDTO : r) {
                        buildPersonalCustomerSUR(prospectOrganizationDetailsDTO,ocDto);
                    }
                    poSrv.changeProspectOrganizationStatus(poDto, BaseConstants.STATUS_COMPLETED);
                    break;
                default:
                    break;
            }
		}
        return r;
    }
    @Transactional(propagation = Propagation.REQUIRED)
    public OrganizationCustomerDTO buildOrganizationCustomerSUR(ProspectOrganization po){
        ProspectOrganizationDTO podto = poMapper.toDto(po);

        OrganizationCustomerDTO oc = new OrganizationCustomerDTO();
        oc.setOrganization(podto.getOrganization());
//        oc.setIdRoleType(Integer.parseInt(BaseConstants.ROLE_CUSTOMER_ORGANIZATION));
        oc = ocSrv.save(oc);

        return oc;
    }
    @Transactional(propagation = Propagation.REQUIRED)
    public void buildPersonalCustomerSUR(ProspectOrganizationDetailsDTO podto, OrganizationCustomerDTO ocDto){
        PersonalCustomerDTO pc = new PersonalCustomerDTO();
        pc.setPerson(podto.getPerson());
        pc = pcSrv.save(pc);
        surSrv.buildOneFromProspectOrganization( podto, pc, ocDto);
    }

}
