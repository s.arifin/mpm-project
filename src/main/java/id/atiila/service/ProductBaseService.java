package id.atiila.service;

import id.atiila.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ProductBaseService {

    @Autowired
    protected MasterNumberingService numbering;

    public abstract Product findById(String id);

}
