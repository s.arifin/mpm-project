package id.atiila.service;

import id.atiila.domain.CustomerRelationship;
import id.atiila.repository.CustomerRelationshipRepository;
import id.atiila.repository.search.CustomerRelationshipSearchRepository;
import id.atiila.service.dto.CustomerRelationshipDTO;
import id.atiila.service.mapper.CustomerRelationshipMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing CustomerRelationship.
 * atiila consulting
 */

@Service
@Transactional
public class CustomerRelationshipService {

    private final Logger log = LoggerFactory.getLogger(CustomerRelationshipService.class);

    private final CustomerRelationshipRepository customerRelationshipRepository;

    private final CustomerRelationshipMapper customerRelationshipMapper;

    private final CustomerRelationshipSearchRepository customerRelationshipSearchRepository;

    public CustomerRelationshipService(CustomerRelationshipRepository customerRelationshipRepository, CustomerRelationshipMapper customerRelationshipMapper, CustomerRelationshipSearchRepository customerRelationshipSearchRepository) {
        this.customerRelationshipRepository = customerRelationshipRepository;
        this.customerRelationshipMapper = customerRelationshipMapper;
        this.customerRelationshipSearchRepository = customerRelationshipSearchRepository;
    }

    /**
     * Save a customerRelationship.
     *
     * @param customerRelationshipDTO the entity to save
     * @return the persisted entity
     */
    public CustomerRelationshipDTO save(CustomerRelationshipDTO customerRelationshipDTO) {
        log.debug("Request to save CustomerRelationship : {}", customerRelationshipDTO);
        CustomerRelationship customerRelationship = customerRelationshipMapper.toEntity(customerRelationshipDTO);
        customerRelationship = customerRelationshipRepository.save(customerRelationship);
        CustomerRelationshipDTO result = customerRelationshipMapper.toDto(customerRelationship);
        customerRelationshipSearchRepository.save(customerRelationship);
        return result;
    }

    /**
     * Get all the customerRelationships.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CustomerRelationshipDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CustomerRelationships");
        return customerRelationshipRepository.findAll(pageable)
            .map(customerRelationshipMapper::toDto);
    }

    /**
     * Get one customerRelationship by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CustomerRelationshipDTO findOne(UUID id) {
        log.debug("Request to get CustomerRelationship : {}", id);
        CustomerRelationship customerRelationship = customerRelationshipRepository.findOne(id);
        return customerRelationshipMapper.toDto(customerRelationship);
    }

    /**
     * Delete the customerRelationship by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete CustomerRelationship : {}", id);
        customerRelationshipRepository.delete(id);
        customerRelationshipSearchRepository.delete(id);
    }

    /**
     * Search for the customerRelationship corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CustomerRelationshipDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of CustomerRelationships for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idStatusType = request.getParameter("idStatusType");
        String idRelationType = request.getParameter("idRelationType");
        String idInternal = request.getParameter("idInternal");
        String idCustomer = request.getParameter("idCustomer");

        if (idStatusType != null) {
            q.withQuery(matchQuery("statusType.idStatusType", idStatusType));
        }
        else if (idRelationType != null) {
            q.withQuery(matchQuery("relationType.idRelationType", idRelationType));
        }
        else if (idInternal != null) {
            q.withQuery(matchQuery("internal.idInternal", idInternal));
        }
        else if (idCustomer != null) {
            q.withQuery(matchQuery("customer.idCustomer", idCustomer));
        }

        Page<CustomerRelationship> result = customerRelationshipSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(customerRelationshipMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<CustomerRelationshipDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered CustomerRelationshipDTO");
        String idStatusType = request.getParameter("idStatusType");
        String idRelationType = request.getParameter("idRelationType");
        String idInternal = request.getParameter("idInternal");
        String idCustomer = request.getParameter("idCustomer");

        if (idInternal != null && idCustomer != null ) {
            return customerRelationshipRepository.queryByInternalCustomer(idInternal, idCustomer, pageable)
                .map(customerRelationshipMapper::toDto);
        }
        else if (idStatusType != null) {
            return customerRelationshipRepository.queryByIdStatusType(Integer.valueOf(idStatusType), pageable)
                .map(customerRelationshipMapper::toDto);
        }
        else if (idRelationType != null) {
            return customerRelationshipRepository.queryByIdRelationType(Integer.valueOf(idRelationType), pageable)
                .map(customerRelationshipMapper::toDto);
        }
        else if (idInternal != null) {
            return customerRelationshipRepository.queryByIdInternal(idInternal, pageable)
                .map(customerRelationshipMapper::toDto);
        }
        else if (idCustomer != null) {
            return customerRelationshipRepository.queryByIdCustomer(idCustomer, pageable)
                .map(customerRelationshipMapper::toDto);
        }
        return customerRelationshipRepository.queryNothing(pageable).map(customerRelationshipMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, CustomerRelationshipDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<CustomerRelationshipDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
