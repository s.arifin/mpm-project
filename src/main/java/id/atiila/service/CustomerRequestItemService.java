package id.atiila.service;

import id.atiila.domain.CustomerRequestItem;
import id.atiila.repository.CustomerRequestItemRepository;
import id.atiila.repository.search.CustomerRequestItemSearchRepository;
import id.atiila.service.dto.CustomerRequestItemDTO;
import id.atiila.service.mapper.CustomerRequestItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing CustomerRequestItem.
 * atiila consulting
 */

@Service
@Transactional
public class CustomerRequestItemService {

    private final Logger log = LoggerFactory.getLogger(CustomerRequestItemService.class);

    private final CustomerRequestItemRepository customerRequestItemRepository;

    private final CustomerRequestItemMapper customerRequestItemMapper;

    private final CustomerRequestItemSearchRepository customerRequestItemSearchRepository;

    @Autowired
    private RequestUtil requestUtil;

    public CustomerRequestItemService(CustomerRequestItemRepository customerRequestItemRepository, CustomerRequestItemMapper customerRequestItemMapper, CustomerRequestItemSearchRepository customerRequestItemSearchRepository) {
        this.customerRequestItemRepository = customerRequestItemRepository;
        this.customerRequestItemMapper = customerRequestItemMapper;
        this.customerRequestItemSearchRepository = customerRequestItemSearchRepository;
    }

    /**
     * Save a customerRequestItem.
     *
     * @param customerRequestItemDTO the entity to save
     * @return the persisted entity
     */
    public CustomerRequestItemDTO save(CustomerRequestItemDTO customerRequestItemDTO) {
        log.debug("Request to save CustomerRequestItem : {}", customerRequestItemDTO);
//        CustomerRequestItem customerRequestItem = customerRequestItemMapper.toEntity(customerRequestItemDTO);
//        customerRequestItem = customerRequestItemRepository.save(customerRequestItem);
        CustomerRequestItem customerRequestItem = requestUtil.save(customerRequestItemDTO);
        CustomerRequestItemDTO result = customerRequestItemMapper.toDto(customerRequestItem);
        customerRequestItemSearchRepository.save(customerRequestItem);
        return result;
    }

    /**
     * Get all the customerRequestItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CustomerRequestItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CustomerRequestItems");
        return customerRequestItemRepository.findAll(pageable)
            .map(customerRequestItemMapper::toDto);
    }

    /**
     * Get one customerRequestItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CustomerRequestItemDTO findOne(UUID id) {
        log.debug("Request to get CustomerRequestItem : {}", id);
        CustomerRequestItem customerRequestItem = customerRequestItemRepository.findOne(id);
        return customerRequestItemMapper.toDto(customerRequestItem);
    }

    /**
     * Delete the customerRequestItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete CustomerRequestItem : {}", id);
        customerRequestItemRepository.delete(id);
        customerRequestItemSearchRepository.delete(id);
    }

    /**
     * Search for the customerRequestItem corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CustomerRequestItemDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of CustomerRequestItems for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idRequest = request.getParameter("idRequest");
        String idProduct = request.getParameter("idProduct");
        String idFeature = request.getParameter("idFeature");
        String idCustomer = request.getParameter("idCustomer");

        if (idRequest != null) {
            q.withQuery(matchQuery("request.idRequest", idRequest));
        }
        else if (idProduct != null) {
            q.withQuery(matchQuery("product.idProduct", idProduct));
        }
        else if (idFeature != null) {
            q.withQuery(matchQuery("feature.idFeature", idFeature));
        }
        else if (idCustomer != null) {
            q.withQuery(matchQuery("customer.idCustomer", idCustomer));
        }

        Page<CustomerRequestItem> result = customerRequestItemSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(customerRequestItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<CustomerRequestItemDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered CustomerRequestItemDTO");
        String idRequest = request.getParameter("idRequest");
        String idProduct = request.getParameter("idProduct");
        String idFeature = request.getParameter("idFeature");
        String idCustomer = request.getParameter("idCustomer");

        if (idRequest != null) {
            return customerRequestItemRepository.queryByIdRequest(UUID.fromString(idRequest), pageable).map(customerRequestItemMapper::toDto);
        }
        else if (idProduct != null) {
            return customerRequestItemRepository.queryByIdProduct(idProduct, pageable).map(customerRequestItemMapper::toDto);
        }
        else if (idFeature != null) {
            return customerRequestItemRepository.queryByIdFeature(Integer.valueOf(idFeature), pageable).map(customerRequestItemMapper::toDto);
        }
        else if (idCustomer != null) {
            return customerRequestItemRepository.queryByIdCustomer(idCustomer, pageable).map(customerRequestItemMapper::toDto);
        }

        return customerRequestItemRepository.queryNothing(pageable).map(customerRequestItemMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, CustomerRequestItemDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        String command = request.getParameter("command");
        if ("shipToCustomer".equals(command)) {
            requestUtil.buildDO(item);
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<CustomerRequestItemDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
