package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.Set;
import java.util.UUID;

import id.atiila.service.dto.BillToDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.atiila.base.BaseConstants;
import id.atiila.domain.LeasingCompany;
import id.atiila.domain.Organization;
import id.atiila.repository.LeasingCompanyRepository;
import id.atiila.repository.search.LeasingCompanySearchRepository;
import id.atiila.service.dto.LeasingCompanyDTO;
import id.atiila.service.dto.OrganizationCustomerDTO;
import id.atiila.service.mapper.LeasingCompanyMapper;

/**
 * Service Implementation for managing LeasingCompany.
 * BeSmart Team
 */

@Service
@Transactional
public class LeasingCompanyService {

    private final Logger log = LoggerFactory.getLogger(LeasingCompanyService.class);

    @Autowired
    private final LeasingCompanyRepository leasingCompanyRepository;

    private final LeasingCompanyMapper leasingCompanyMapper;

    private final LeasingCompanySearchRepository leasingCompanySearchRepository;

    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private MasterNumberingService numberSvc;

    @Autowired
    private OrganizationCustomerService customerService;

    @Autowired
    private BillToService billTo;

    public LeasingCompanyService(LeasingCompanyRepository leasingCompanyRepository, LeasingCompanyMapper leasingCompanyMapper, LeasingCompanySearchRepository leasingCompanySearchRepository) {
        this.leasingCompanyRepository = leasingCompanyRepository;
        this.leasingCompanyMapper = leasingCompanyMapper;
        this.leasingCompanySearchRepository = leasingCompanySearchRepository;
    }

    /**
     * Save a leasingCompany.
     *
     * @param dto the entity to save
     * @return the persisted entity
     */
    public LeasingCompanyDTO save(LeasingCompanyDTO dto) {
        log.debug("Request to save LeasingCompany : {}", dto);
        LeasingCompany leasingCompany = leasingCompanyMapper.toEntity(dto);

        Boolean isNewData = leasingCompany.getIdPartyRole() == null;

        if (isNewData) {
            LeasingCompany vend = null;

            //Cek Jika Vendor sudah mempunyai ID
            if (dto.getIdLeasingCompany() != null) {
                vend = leasingCompanyRepository.findOneByIdLeasingCompany(dto.getIdLeasingCompany());
                if (vend != null) return leasingCompanyMapper.toDto(vend);
            }

            Organization org = organizationService.findOneByName(dto.getPartyName());

            //Cek Jika sudah mempunyai Organization
            if (org != null) {
                vend = leasingCompanyRepository.findOneByParty(org);
                if (vend != null) return leasingCompanyMapper.toDto(vend);
                leasingCompany.setParty(org);
            }

            // leasingCompany.setStatus(BaseConstants.STATUS_ACTIVE);

            if (dto.getIdLeasingCompany() == null){
                String idVend = null;
                while (idVend == null || leasingCompanyRepository.findOneByIdLeasingCompany(idVend) != null) {
                    idVend = numberSvc.nextValue("idleasing", 99000l).toString();
                }
                leasingCompany.setIdLeasingCompany(idVend);
            }
            else
                leasingCompany.setIdLeasingCompany(dto.getIdLeasingCompany());
        }

        leasingCompany = leasingCompanyRepository.save(leasingCompany);
        LeasingCompanyDTO result = leasingCompanyMapper.toDto(leasingCompany);
        leasingCompanySearchRepository.save(leasingCompany);

        if (isNewData) {
            BillToDTO bdto = new BillToDTO();
            bdto.setIdBillTo(result.getIdLeasingCompany());
            bdto.setPartyId(result.getPartyId());
            bdto = billTo.save(bdto);
        }

        return result;
    }

    /**
     *  Get all the leasingCompanies.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LeasingCompanyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LeasingCompanies");
        return leasingCompanyRepository.findAll(pageable)
            .map(leasingCompanyMapper::toDto);
    }

    /**
     *  Get one leasingCompany by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public LeasingCompanyDTO findOne(UUID id) {
        log.debug("Request to get LeasingCompany : {}", id);
        LeasingCompany leasingCompany = leasingCompanyRepository.findOne(id);
        return leasingCompanyMapper.toDto(leasingCompany);
    }

    /**
     *  Delete the  leasingCompany by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete LeasingCompany : {}", id);
        leasingCompanyRepository.delete(id);
        leasingCompanySearchRepository.delete(id);
    }

    /**
     * Search for the leasingCompany corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LeasingCompanyDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of LeasingCompanies for query {}", query);
        Page<LeasingCompany> result = leasingCompanySearchRepository.search(queryStringQuery(query), pageable);
        return result.map(leasingCompanyMapper::toDto);
    }

    public LeasingCompanyDTO processExecuteData(Integer id, String param, LeasingCompanyDTO dto) {
        LeasingCompanyDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<LeasingCompanyDTO> processExecuteListData(Integer id, String param, Set<LeasingCompanyDTO> dto) {
        Set<LeasingCompanyDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public LeasingCompanyDTO changeLeasingCompanyStatus(LeasingCompanyDTO dto, Integer id) {
        if (dto != null) {
			LeasingCompany e = leasingCompanyRepository.findOne(dto.getIdPartyRole());
		}
        return dto;
    }
}
