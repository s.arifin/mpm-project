package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.Internal;
import id.atiila.domain.StandardCalendar;
import id.atiila.repository.StandardCalendarRepository;
import id.atiila.repository.search.StandardCalendarSearchRepository;
import id.atiila.service.mapper.StandardCalendarMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;

@Service
@Transactional
public class CalendarUtils {

    private final Logger log = LoggerFactory.getLogger(CalendarUtils.class);

    @Autowired
    private StandardCalendarRepository standardCalendarRepository;

    @Autowired
    private StandardCalendarMapper standardCalendarMapper;

    @Autowired
    private StandardCalendarSearchRepository standardCalendarSearchRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildReqularCalendar(Internal internal, Integer year) {
        ZonedDateTime key = ZonedDateTime.of(year, 1, 1, 0, 0, 0, 0, ZoneId.systemDefault());
        StandardCalendar s = standardCalendarRepository.findOneByDateKeyAndIdCalendarType(internal, key, BaseConstants.CALENDAR_TYPE_YEARLY);
        if (s == null) {
            s = new StandardCalendar();
            s.setDateKey(key);
            s.setDateFrom(key);
            s.setDateThru(key.with(TemporalAdjusters.lastDayOfYear()));
            s.setDescription(DateTimeFormatter.ofPattern("yyyy").format(s.getDateKey()));
            s.setIdCalendarType(BaseConstants.CALENDAR_TYPE_YEARLY);
            s.setInternal(internal);
            standardCalendarRepository.save(s);

            ZonedDateTime mkey = key.with(TemporalAdjusters.firstDayOfYear());
            for (int i = 1; i <=12; i++) {
                StandardCalendar m = new StandardCalendar();
                m.setDateKey(mkey.with(TemporalAdjusters.firstDayOfMonth()));
                m.setDateFrom(mkey.with(TemporalAdjusters.firstDayOfMonth()));
                m.setDateThru(mkey.with(TemporalAdjusters.lastDayOfMonth()));
                m.setDescription(DateTimeFormatter.ofPattern("yyyy-MM").format(m.getDateKey()));
                m.setIdCalendarType(BaseConstants.CALENDAR_TYPE_MONTHLY);
                m.setIdParent(s.getIdCalendar());
                m.setInternal(internal);
                m.setSequence(i);
                standardCalendarRepository.save(m);
                mkey = mkey.plusMonths(1l);
            }

            ZonedDateTime dkey = key.with(TemporalAdjusters.firstDayOfYear());
            while (key.getYear() == dkey.getYear()) {
                StandardCalendar d = new StandardCalendar();
                d.setDateKey(dkey.truncatedTo(ChronoUnit.DAYS));
                d.setDateFrom(dkey.truncatedTo(ChronoUnit.DAYS));
                d.setDateThru(dkey.truncatedTo(ChronoUnit.DAYS).plusDays(1).minusSeconds(1));
                d.setDescription(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(d.getDateKey()));
                d.setIdCalendarType(BaseConstants.CALENDAR_TYPE_DAILY);
                d.setIdParent(s.getIdCalendar());
                d.setInternal(internal);
                d.setSequence(dkey.getDayOfYear());
                switch (dkey.getDayOfWeek()) {
                    case SATURDAY:
                    case SUNDAY:
                        d.setWorkDay(false);
                        break;
                    default:
                        d.setWorkDay(true);
                }
                standardCalendarRepository.save(d);
                dkey = dkey.plusDays(1);

            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public StandardCalendar save(StandardCalendar c) {
        StandardCalendar cal = standardCalendarRepository.save(c);
        standardCalendarSearchRepository.save(c);
        return cal;
    }

    public StandardCalendar getCurrentMonth(Internal internal) {
        StandardCalendar cal = standardCalendarRepository.findMonthlyCalendar(internal, BaseConstants.CALENDAR_TYPE_MONTHLY, ZonedDateTime.now());
        return cal;
    }

    public StandardCalendar getMonthlyCalendar(Internal internal, ZonedDateTime d) {
        StandardCalendar cal = standardCalendarRepository.findMonthlyCalendar(internal, BaseConstants.CALENDAR_TYPE_MONTHLY, d);
        return cal;
    }

}
