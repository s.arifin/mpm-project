package id.atiila.service;

import id.atiila.domain.DealerReminderType;
import id.atiila.repository.DealerReminderTypeRepository;
import id.atiila.repository.search.DealerReminderTypeSearchRepository;
import id.atiila.service.dto.DealerReminderTypeDTO;
import id.atiila.service.mapper.DealerReminderTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing DealerReminderType.
 * BeSmart Team
 */

@Service
@Transactional
public class DealerReminderTypeService {

    private final Logger log = LoggerFactory.getLogger(DealerReminderTypeService.class);

    private final DealerReminderTypeRepository dealerReminderTypeRepository;

    private final DealerReminderTypeMapper dealerReminderTypeMapper;

    private final DealerReminderTypeSearchRepository dealerReminderTypeSearchRepository;
    public DealerReminderTypeService(DealerReminderTypeRepository dealerReminderTypeRepository, DealerReminderTypeMapper dealerReminderTypeMapper, DealerReminderTypeSearchRepository dealerReminderTypeSearchRepository) {
        this.dealerReminderTypeRepository = dealerReminderTypeRepository;
        this.dealerReminderTypeMapper = dealerReminderTypeMapper;
        this.dealerReminderTypeSearchRepository = dealerReminderTypeSearchRepository;
    }

    /**
     * Save a dealerReminderType.
     *
     * @param dealerReminderTypeDTO the entity to save
     * @return the persisted entity
     */
    public DealerReminderTypeDTO save(DealerReminderTypeDTO dealerReminderTypeDTO) {
        log.debug("Request to save DealerReminderType : {}", dealerReminderTypeDTO);
        DealerReminderType dealerReminderType = dealerReminderTypeMapper.toEntity(dealerReminderTypeDTO);
        dealerReminderType = dealerReminderTypeRepository.save(dealerReminderType);
        DealerReminderTypeDTO result = dealerReminderTypeMapper.toDto(dealerReminderType);
        dealerReminderTypeSearchRepository.save(dealerReminderType);
        return result;
    }

    /**
     *  Get all the dealerReminderTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DealerReminderTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DealerReminderTypes");
        return dealerReminderTypeRepository.findAll(pageable)
            .map(dealerReminderTypeMapper::toDto);
    }

    /**
     *  Get one dealerReminderType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public DealerReminderTypeDTO findOne(Integer id) {
        log.debug("Request to get DealerReminderType : {}", id);
        DealerReminderType dealerReminderType = dealerReminderTypeRepository.findOne(id);
        return dealerReminderTypeMapper.toDto(dealerReminderType);
    }

    /**
     *  Delete the  dealerReminderType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete DealerReminderType : {}", id);
        dealerReminderTypeRepository.delete(id);
        dealerReminderTypeSearchRepository.delete(id);
    }

    /**
     * Search for the dealerReminderType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DealerReminderTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DealerReminderTypes for query {}", query);
        Page<DealerReminderType> result = dealerReminderTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(dealerReminderTypeMapper::toDto);
    }

    public DealerReminderTypeDTO processExecuteData(Integer id, String param, DealerReminderTypeDTO dto) {
        DealerReminderTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<DealerReminderTypeDTO> processExecuteListData(Integer id, String param, Set<DealerReminderTypeDTO> dto) {
        Set<DealerReminderTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
