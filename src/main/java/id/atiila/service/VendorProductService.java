package id.atiila.service;

import id.atiila.domain.VendorProduct;
import id.atiila.repository.VendorProductRepository;
import id.atiila.repository.search.VendorProductSearchRepository;
import id.atiila.service.dto.VendorProductDTO;
import id.atiila.service.mapper.VendorProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Service Implementation for managing VendorProduct.
 * BeSmart Team
 */

@Service
@Transactional
public class VendorProductService {

    private final Logger log = LoggerFactory.getLogger(VendorProductService.class);

    private final VendorProductRepository vendorProductRepository;

    private final VendorProductMapper vendorProductMapper;

    private final VendorProductSearchRepository vendorProductSearchRepository;

    public VendorProductService(VendorProductRepository vendorProductRepository, VendorProductMapper vendorProductMapper, VendorProductSearchRepository vendorProductSearchRepository) {
        this.vendorProductRepository = vendorProductRepository;
        this.vendorProductMapper = vendorProductMapper;
        this.vendorProductSearchRepository = vendorProductSearchRepository;
    }

    /**
     * Save a vendorProduct.
     *
     * @param vendorProductDTO the entity to save
     * @return the persisted entity
     */
    public VendorProductDTO save(VendorProductDTO vendorProductDTO) {
        log.debug("Request to save VendorProduct : {}", vendorProductDTO);
        VendorProduct vendorProduct = vendorProductMapper.toEntity(vendorProductDTO);
        vendorProduct = vendorProductRepository.save(vendorProduct);
        VendorProductDTO result = vendorProductMapper.toDto(vendorProduct);
        vendorProductSearchRepository.save(vendorProduct);
        return result;
    }

    /**
     *  Get all the vendorProducts.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VendorProductDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VendorProducts");
        return vendorProductRepository.findAll(pageable)
            .map(vendorProductMapper::toDto);
    }

    /**
     *  Get one vendorProduct by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public VendorProductDTO findOne(UUID id) {
        log.debug("Request to get VendorProduct : {}", id);
        VendorProduct vendorProduct = vendorProductRepository.findOne(id);
        return vendorProductMapper.toDto(vendorProduct);
    }

    /**
     *  Delete the  vendorProduct by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete VendorProduct : {}", id);
        vendorProductRepository.delete(id);
        vendorProductSearchRepository.delete(id);
    }

    /**
     * Search for the vendorProduct corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VendorProductDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of VendorProducts for query {}", query);
        Page<VendorProduct> result = vendorProductSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(vendorProductMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        vendorProductSearchRepository.deleteAll();
        List<VendorProduct> vendorProducts =  vendorProductRepository.findAll();
        for (VendorProduct m: vendorProducts) {
            vendorProductSearchRepository.save(m);
            log.debug("Data vendor-product save !...");
        }
    }

}
