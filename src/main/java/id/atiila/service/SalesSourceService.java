package id.atiila.service;

import id.atiila.domain.SalesSource;
import id.atiila.repository.SalesSourceRepository;
import id.atiila.repository.search.SalesSourceSearchRepository;
import id.atiila.service.dto.SalesSourceDTO;
import id.atiila.service.mapper.SalesSourceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SalesSource.
 * BeSmart Team
 */

@Service
@Transactional
public class SalesSourceService {

    private final Logger log = LoggerFactory.getLogger(SalesSourceService.class);

    private final SalesSourceRepository salesSourceRepository;

    private final SalesSourceMapper salesSourceMapper;

    private final SalesSourceSearchRepository salesSourceSearchRepository;

    public SalesSourceService(SalesSourceRepository salesSourceRepository, SalesSourceMapper salesSourceMapper, SalesSourceSearchRepository salesSourceSearchRepository) {
        this.salesSourceRepository = salesSourceRepository;
        this.salesSourceMapper = salesSourceMapper;
        this.salesSourceSearchRepository = salesSourceSearchRepository;
    }

    /**
     * Save a salesSource.
     *
     * @param salesSourceDTO the entity to save
     * @return the persisted entity
     */
    public SalesSourceDTO save(SalesSourceDTO salesSourceDTO) {
        log.debug("Request to save SalesSource : {}", salesSourceDTO);
        SalesSource salesSource = salesSourceMapper.toEntity(salesSourceDTO);
        salesSource = salesSourceRepository.save(salesSource);
        SalesSourceDTO result = salesSourceMapper.toDto(salesSource);
        salesSourceSearchRepository.save(salesSource);
        return result;
    }

    /**
     *  Get all the salesSources.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesSourceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SalesSources");
        return salesSourceRepository.findAll(pageable)
            .map(salesSourceMapper::toDto);
    }

    /**
     *  Get one salesSource by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SalesSourceDTO findOne(Long id) {
        log.debug("Request to get SalesSource : {}", id);
        SalesSource salesSource = salesSourceRepository.findOne(id);
        return salesSourceMapper.toDto(salesSource);
    }

    /**
     *  Delete the  salesSource by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SalesSource : {}", id);
        salesSourceRepository.delete(id);
        salesSourceSearchRepository.delete(id);
    }

    /**
     * Search for the salesSource corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesSourceDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SalesSources for query {}", query);
        Page<SalesSource> result = salesSourceSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(salesSourceMapper::toDto);
    }
}
