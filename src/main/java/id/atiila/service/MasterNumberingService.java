package id.atiila.service;

import com.hazelcast.core.*;
import id.atiila.base.DmsException;
import id.atiila.config.tenant.TenantContext;
import id.atiila.domain.Facility;
import id.atiila.domain.Internal;
import id.atiila.domain.MasterNumbering;
import id.atiila.domain.UserMediator;
import id.atiila.repository.InternalRepository;
import id.atiila.repository.MasterNumberingRepository;
import id.atiila.repository.UserMediatorRepository;
import id.atiila.repository.search.MasterNumberingSearchRepository;
import id.atiila.security.SecurityUtils;
import id.atiila.service.dto.MasterNumberingDTO;
import id.atiila.service.mapper.MasterNumberingMapper;
import org.elasticsearch.common.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Calendar;


import static org.elasticsearch.index.query.QueryBuilders.*;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


/**
 * Service Implementation for managing MasterNumbering.
 * BeSmart Team
 */

@Service
@Transactional
public class MasterNumberingService {

    private final Logger log = LoggerFactory.getLogger(MasterNumberingService.class);

    private final MasterNumberingRepository masterNumberingRepository;

    private final MasterNumberingMapper masterNumberingMapper;

    private final MasterNumberingSearchRepository masterNumberingSearchRepository;

    @Autowired
    private InternalRepository<Internal> internalRepository;

    @Autowired
    private UserMediatorRepository userMediatorRepository;

    @Autowired
    private HazelcastInstance hz;

    public MasterNumberingService(MasterNumberingRepository masterNumberingRepository, MasterNumberingMapper masterNumberingMapper, MasterNumberingSearchRepository masterNumberingSearchRepository) {
        this.masterNumberingRepository = masterNumberingRepository;
        this.masterNumberingMapper = masterNumberingMapper;
        this.masterNumberingSearchRepository = masterNumberingSearchRepository;
    }

    /**
     * Save a masterNumbering.
     *
     * @param masterNumberingDTO the entity to save
     * @return the persisted entity
     */
    public MasterNumberingDTO save(MasterNumberingDTO masterNumberingDTO) {
        log.debug("Request to save MasterNumbering : {}", masterNumberingDTO);
        MasterNumbering masterNumbering = masterNumberingMapper.toEntity(masterNumberingDTO);
        masterNumbering = masterNumberingRepository.save(masterNumbering);
        MasterNumberingDTO result = masterNumberingMapper.toDto(masterNumbering);
        masterNumberingSearchRepository.save(masterNumbering);
        return result;
    }

    /**
     *  Get all the masterNumberings.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MasterNumberingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MasterNumberings");
        return masterNumberingRepository.findAll(pageable)
            .map(masterNumberingMapper::toDto);
    }

    /**
     *  Get one masterNumbering by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public MasterNumberingDTO findOne(UUID id) {
        log.debug("Request to get MasterNumbering : {}", id);
        MasterNumbering masterNumbering = masterNumberingRepository.findOne(id);
        return masterNumberingMapper.toDto(masterNumbering);
    }

    /**
     *  Delete the  masterNumbering by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete MasterNumbering : {}", id);
        masterNumberingRepository.delete(id);
        masterNumberingSearchRepository.delete(id);
    }

    /**
     * Search for the masterNumbering corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MasterNumberingDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MasterNumberings for query {}", query);
        Page<MasterNumbering> result = masterNumberingSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(masterNumberingMapper::toDto);
    }

    @Transactional
    protected MasterNumbering getNumbering(String idTag, String idValue, Long startFrom) {
        MasterNumbering r = masterNumberingRepository.findOneByIdTagAndIdValue(idTag, idValue);
        if (r == null) {
            r = new MasterNumbering();
            r.setIdTag(idTag);
            r.setIdValue(idValue);
            r.setNextValue(startFrom);
            r.setNextValue(0l);
            r = masterNumberingRepository.saveAndFlush(r);
        }
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Long nextValue(String idTag, String idValue, Long startFrom) {
        long result = 0l;
        ILock lock = hz.getLock(idTag + "-" + idValue);
        lock.lock();
        try {
            IAtomicLong number = hz.getAtomicLong(TenantContext.getCurrentTenant() + "numbering" + idTag + "-" + idValue);
            if (number.get() == 0) {
                MasterNumbering numb = getNumbering(idTag, idValue, startFrom);
                number.set(numb.getNextValue());
            }
            result = number.incrementAndGet();
            MasterNumbering numb = getNumbering(idTag, idValue, startFrom);
            numb.setNextValue(result);
            masterNumberingRepository.saveAndFlush(numb);
        } finally {
            lock.unlock();
        }
        return result;
    }

    public Long nextValue(String idTag) {
        return nextValue(idTag, "default", 0l);
    }

    public Long nextValue(String idTag, Long startFrom) {
        return nextValue(idTag, "default", startFrom);
    }

    public Long nextValue(String idTag, String idValue) {
        return nextValue(idTag, idValue, 0l);
    }

    public String nextCustomerValue() {
        Long v = 1000000l + nextValue("idcustomer", 0l);
        return v.toString();
    }

    public String nextProspectNumberValue() {
        Long v = 1000000l + nextValue("prospectnumber", 0l);
        return v.toString();
    }

    public String nextBrolerValue() {
        Long v = 3000l + nextValue("idbroker", 0l);
        return v.toString();
    }

    public String nextVendorValue() {
        return nextValue("idvendor", 20000l).toString();
    }

    public String nextInternalValue() {
        return Strings.padStart(nextValue("idinternal", 0l).toString(), 4, '0');
    }

    public String nextShipToValue() {
        return "2" + Strings.padStart(nextValue("idshipto", 0l).toString(), 6, '0');
    }

    public String nextBillToValue() {
        return "1" + Strings.padStart(nextValue("idbillto", 0l).toString(), 6, '0');
    }

    public String nextGoodValue() {
        return "S" + Strings.padStart(nextValue("idgood", 0l).toString(), 5, '0');
    }

    public String nextFinancialProductValue() {
        return "F-" + Strings.padStart(nextValue("idfinancial", 0l).toString(), 5, '0');
    }

    public String nextBASTValue() {
        return "B-" + Strings.padStart(nextValue("bast", 0l).toString(), 5, '0');
    }

    public String nextSubmissionValue() {
        return "A-" + Strings.padStart(nextValue("submission", 0l).toString(), 7, '0');
    }

    public String nextReferenceValue() {
        return "REF-" + Strings.padStart(nextValue("reference", 0l).toString(), 4, '0');
    }

    public String generateTransNumberCustomerCode(String idCustomer){
        log.debug("Master number system: generate cust code ==> " + idCustomer);
        String str = "MPM-" + ZonedDateTime.now().getMonthValue() + LocalDateTime.now().getYear() + "-";
        str = str + Strings.padStart(nextValue(idCustomer, str).toString(), 8,'0');
        log.debug("Result ==> " + str);
        return str;
    }

    public String findTransNumberRequestNumber(String idTrans) {
        log.debug("Master numbering system: findTransBumberRequestNumber ===> " + idTrans);
        String r = "REQ-" + ZonedDateTime.now().getMonthValue() + LocalDateTime.now().getYear() + "-";
        r = r + Strings.padStart(nextValue(idTrans, r) .toString(), 8, '0');
        log.debug("result ===> " + r);
        return r;
    }

    public String findTransNumberMovingSlipBy(String idtrans){
        log.debug("master numbering system: findTransNumberBy trans==> " + idtrans);
        String r = "MOV-" + ZonedDateTime.now().getMonthValue() + LocalDateTime.now().getYear() + "-";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 8, '0');
        log.debug("result ===> " + r);
        return r;
    }

    public String findTransNumberBy(String id, String idtrans){
        log.debug("master numbering system: findTransNumberBy" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);
        if (internal == null){
            return null;
        }
        String r = internal.getRoot().getIdInternal() + LocalDateTime.now().getYear() + "-";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 8, '0');
        return r;
    }

    public String findTransNumberVSBBy(String id, String idtrans){
        log.debug("master numbering system: findTransNumberVSBBy" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);
        if (internal == null){
            log.debug("Internal null " );
            return null;
        }
        // String r = internal.getIdInternal() + "-IVU-" + LocalDateTime.now().getYear() + "-";
        String r = internal.getRoot().getIdInternal() + "-" + idtrans + "-" + LocalDateTime.now().getYear() + "-" + "DMS";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 8, '0');
        log.debug("hasil ==>" + r );
        return r;
    }

    public String findTransNumberVDOBy(String id, String idtrans){
        log.debug("master numbering system: findTransNumberVSBBy" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);
        if (internal == null){
            log.debug("Internal null " );
            return null;
        }
        String r = internal.getRoot().getIdInternal() + "-VDO-" + LocalDateTime.now().getYear() + "-";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 10, '0');
        log.debug("hasil ==>" + r );
        return r;
    }
    public String findTransNumberRegistrationBy(String id, String idtrans){
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        int month = cal.get(Calendar.MONTH);
        month = month + 1;
        log.debug("master numbering system: findTransNumberRegistrationBy" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);

        if (internal == null){
            return null;
        }
        String r = internal.getRoot().getIdInternal() + "-AFS-" +LocalDateTime.now().getYear() + "-" + month + "-";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 5, '0');
        return r;
    }

    public String findTransSubmissionNumberBy(String id, String idtrans){
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        int month = cal.get(Calendar.MONTH);
        month = month + 1;
        log.debug("master numbering system: findTransSubmissionNumberByY" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);

        if (internal == null){
            return null;
        }
        String r = internal.getRoot().getIdInternal() + "-FDB-" +LocalDateTime.now().getYear() + "-" + month + "-";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 5, '0');
        return r;
    }

    public String findTransBastSTNKNumberBy(String id, String idtrans){
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        int month = cal.get(Calendar.MONTH);
        month = month + 1;
        log.debug("master numbering system: findTransSubmissionNumberBy" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);

        if (internal == null){
            return null;
        }
        String r = internal.getRoot().getIdInternal() + "-STD-" +LocalDateTime.now().getYear() + "-" + month + "-";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 5, '0');
        return r;
    }

    public String findTransBastBPKBLeasingNumberBy(String id, String idtrans){
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        int month = cal.get(Calendar.MONTH);
        month = month + 1;
        log.debug("master numbering system: findTransSubmissionNumberBy" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);

        if (internal == null){
            return null;
        }
        String r = internal.getRoot().getIdInternal() + "-BDL-" +LocalDateTime.now().getYear() + "-" + month + "-";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 5, '0');
        return r;
    }

    public String findTransBastFincoNumberBy(String id, String idtrans){
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        int month = cal.get(Calendar.MONTH);
        month = month + 1;
        log.debug("master numbering system: findTransyuiyuy" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);

        if (internal == null){
            return null;
        }
        String r = internal.getRoot().getIdInternal() + "-FIN-" +LocalDateTime.now().getYear() + "-" + month + "-";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 5, '0');
        return r;
    }

    public String findTransBastBPKBCustomerNumberBy(String id, String idtrans){
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        int month = cal.get(Calendar.MONTH);
        month = month + 1;
        log.debug("master numbering system: findTransSubmissionNumberBy" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);

        if (internal == null){
            return null;
        }
        String r = internal.getRoot().getIdInternal() + "-BDC-" +LocalDateTime.now().getYear() + "-" + month + "-";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 5, '0');
        return r;
    }

    public String findTransBastOffTheRoad(String id, String idtrans){
        log.debug("master numbering system: findTransSubmissionNumberBy" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);

        if (internal == null){
            return null;
        }
        String r = internal.getRoot().getIdInternal() + "-FDC-" +LocalDateTime.now().getYear() + "-";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 5, '0');
        return r;
    }

    public String buildPONumber(Internal intr){
        if (intr == null) return null;

        String r = intr.getRoot().getIdInternal() + "-PO-" + LocalDateTime.now().getYear() + "-";
        r = r + Strings.padStart(nextValue("PO", r) .toString(), 10, '0');
        return r;
    }

    public String buildSPGNumber(Internal intr){
        if (intr == null) return null;

        String r = intr.getRoot().getIdInternal() + "-SPG-" + LocalDateTime.now().getYear() + "-";
        r = r + Strings.padStart(nextValue("SPG", r) .toString(), 10, '0');
        return r;
    }

    public String findBillingReceipt(String id, String idtrans){
        Internal internal = internalRepository.findOne(id);
        if (internal == null){
            log.debug("Internal null " );
            return null;
        }
        String r = internal.getRoot().getIdInternal() + "-" + idtrans + "-" + LocalDateTime.now().getYear() + "-";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 8, '0');
        return r;
    }


    public String getInternalNumber(String idtrans){
        UserMediator um = userMediatorRepository.findByUserName(SecurityUtils.getCurrentUserLogin().get());
        String r = um.getInternal().getRoot().getIdInternal() + "-" + idtrans + "-" + LocalDateTime.now().getYear() + "-";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 8, '0');
        return r;
    }

    public String getInternalNumber(Internal internal, String idtrans){
        String r = internal.getRoot().getIdInternal() + "-" + idtrans + "-" + LocalDateTime.now().getYear() + "-";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 8, '0');
        return r;
    }

    public String getContainerNumber(Facility facility){
        String r = "A" + Strings.padStart(nextValue("idcontainer", facility.getFacilityCode()) .toString(), 5, '0');
        return r;
    }

    public String nextIdMPM(Internal internal){
//        Assert.isTrue(internal == null, "Internal kosong !");
        int year = ZonedDateTime.now().getYear();
        String r = Strings.padStart(nextValue("idcustomer", internal.getIdDealerCode() + "-" + year).toString(), 5, '0');
        return r + "-" + internal.getIdDealerCode() + "-" + year;
    }

//    public String nextProspectNumber(Internal internal){
////        Assert.isTrue(internal != null, "Internal kosong !");
//        int year = ZonedDateTime.now().getYear();
//        String r = Strings.padStart(nextValue("idcustomer", internal.getIdDealerCode() + "-" + year).toString(), 8, '0');
//        return internal.getIdDealerCode() + r + "-" +  "-" + year;
//    }

    public String nextProspectNumber(String id, String idtrans){
        log.debug("master numbering system: nextProspectNumber" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);
        if (internal == null){
            log.debug("Internal null " );
            return null;
        }
        // String r = internal.getIdInternal() + "-IVU-" + LocalDateTime.now().getYear() + "-";
        String r = internal.getRoot().getIdInternal() + "-" + idtrans + "-" + LocalDateTime.now().getYear() + "-" + "DMS";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 8, '0');
        log.debug("hasil ==>" + r );
        return r;
    }

    public String findTransNumberIRUBy(String id, String idtrans){
        log.debug("master numbering system: findTransNumberIRUBy" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);
        if (internal == null){
            log.debug("Internal null " );
            return null;
        }
        // String r = internal.getIdInternal() + "-IVU-" + LocalDateTime.now().getYear() + "-";
        String r = internal.getRoot().getIdInternal() + "-" + idtrans + "-" + LocalDateTime.now().getYear() + "-" + "DMS";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 8, '0');
        log.debug("hasil ==>" + r );
        return r;
    }

    public String findTransNumberMemoBy(String id, String idtrans){
        log.debug("master numbering system: findTransNumberMemoBy" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);
        if (internal == null){
            log.debug("Internal null " );
            return null;
        }
        // String r = internal.getIdInternal() + "-IVU-" + LocalDateTime.now().getYear() + "-";
        String r = internal.getRoot().getIdInternal() + "-" + idtrans + "-" + LocalDateTime.now().getYear() + "-" + "DMS";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 8, '0');
        log.debug("hasil ==>" + r );
        return r;
    }
}
