package id.atiila.service;

import id.atiila.domain.AssociationType;
import id.atiila.repository.AssociationTypeRepository;
import id.atiila.repository.search.AssociationTypeSearchRepository;
import id.atiila.service.dto.AssociationTypeDTO;
import id.atiila.service.mapper.AssociationTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing AssociationType.
 * BeSmart Team
 */

@Service
@Transactional
public class AssociationTypeService {

    private final Logger log = LoggerFactory.getLogger(AssociationTypeService.class);

    private final AssociationTypeRepository associationTypeRepository;

    private final AssociationTypeMapper associationTypeMapper;

    private final AssociationTypeSearchRepository associationTypeSearchRepository;
    public AssociationTypeService(AssociationTypeRepository associationTypeRepository, AssociationTypeMapper associationTypeMapper, AssociationTypeSearchRepository associationTypeSearchRepository) {
        this.associationTypeRepository = associationTypeRepository;
        this.associationTypeMapper = associationTypeMapper;
        this.associationTypeSearchRepository = associationTypeSearchRepository;
    }

    /**
     * Save a associationType.
     *
     * @param associationTypeDTO the entity to save
     * @return the persisted entity
     */
    public AssociationTypeDTO save(AssociationTypeDTO associationTypeDTO) {
        log.debug("Request to save AssociationType : {}", associationTypeDTO);
        AssociationType associationType = associationTypeMapper.toEntity(associationTypeDTO);
        associationType = associationTypeRepository.save(associationType);
        AssociationTypeDTO result = associationTypeMapper.toDto(associationType);
        associationTypeSearchRepository.save(associationType);
        return result;
    }

    /**
     *  Get all the associationTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AssociationTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AssociationTypes");
        return associationTypeRepository.findAll(pageable)
            .map(associationTypeMapper::toDto);
    }

    /**
     *  Get one associationType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AssociationTypeDTO findOne(Integer id) {
        log.debug("Request to get AssociationType : {}", id);
        AssociationType associationType = associationTypeRepository.findOne(id);
        return associationTypeMapper.toDto(associationType);
    }

    /**
     *  Delete the  associationType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete AssociationType : {}", id);
        associationTypeRepository.delete(id);
        associationTypeSearchRepository.delete(id);
    }

    /**
     * Search for the associationType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AssociationTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of AssociationTypes for query {}", query);
        Page<AssociationType> result = associationTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(associationTypeMapper::toDto);
    }

    public AssociationTypeDTO processExecuteData(Integer id, String param, AssociationTypeDTO dto) {
        AssociationTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<AssociationTypeDTO> processExecuteListData(Integer id, String param, Set<AssociationTypeDTO> dto) {
        Set<AssociationTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        associationTypeSearchRepository.deleteAll();
        List<AssociationType> associationTypes =  associationTypeRepository.findAll();
        for (AssociationType m: associationTypes) {
            associationTypeSearchRepository.save(m);
            log.debug("Data Association Type save !...");
        }
    }

}
