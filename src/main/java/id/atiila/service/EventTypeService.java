package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;

import id.atiila.base.XlsxUtils;
import id.atiila.service.dto.CustomEventDTO;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.atiila.domain.EventType;
import id.atiila.repository.EventTypeRepository;
import id.atiila.repository.search.EventTypeSearchRepository;
import id.atiila.service.dto.EventTypeDTO;
import id.atiila.service.mapper.EventTypeMapper;

import javax.annotation.PostConstruct;

/**
 * Service Implementation for managing EventType.
 * BeSmart Team
 */

@Service
@Transactional
public class EventTypeService {

    private final Logger log = LoggerFactory.getLogger(EventTypeService.class);

    private final EventTypeRepository eventTypeRepository;

    private final EventTypeMapper eventTypeMapper;

    private final EventTypeSearchRepository eventTypeSearchRepository;

    public EventTypeService(EventTypeRepository eventTypeRepository, EventTypeMapper eventTypeMapper, EventTypeSearchRepository eventTypeSearchRepository) {
        this.eventTypeRepository = eventTypeRepository;
        this.eventTypeMapper = eventTypeMapper;
        this.eventTypeSearchRepository = eventTypeSearchRepository;
    }

    /**
     * Save a eventType.
     *
     * @param eventTypeDTO the entity to save
     * @return the persisted entity
     */
    public EventTypeDTO save(EventTypeDTO eventTypeDTO) {
        log.debug("Request to save EventType : {}", eventTypeDTO);
        EventType eventType = eventTypeMapper.toEntity(eventTypeDTO);
        eventType = eventTypeRepository.save(eventType);
        EventTypeDTO result = eventTypeMapper.toDto(eventType);
        eventTypeSearchRepository.save(eventType);
        return result;
    }

    /**
     *  Get all the eventTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<EventTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EventTypes");
        return eventTypeRepository.findAll(pageable)
            .map(eventTypeMapper::toDto);
    }

    /**
        *  Get all the eventTypes by idParent.
     **/
    @Transactional(readOnly = true)
    public List<EventTypeDTO> findAllByIdParent(Integer idParent) {
        List<EventTypeDTO> result = eventTypeMapper.toDto(eventTypeRepository.findAllByIdParentEvent(idParent));
        return result;
    }

    /**
     *  Get one eventType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public EventTypeDTO findOne(Integer id) {
        log.debug("Request to get EventType : {}", id);
        EventType eventType = eventTypeRepository.findOne(id);
        return eventTypeMapper.toDto(eventType);
    }

    /**
     *  Delete the  eventType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete EventType : {}", id);
        eventTypeRepository.delete(id);
        eventTypeSearchRepository.delete(id);
    }

    /**
     * Search for the eventType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<EventTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of EventTypes for query {}", query);
        Page<EventType> result = eventTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(eventTypeMapper::toDto);
    }

    public EventTypeDTO processExecuteData(EventTypeDTO dto, Integer id) {
        EventTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    /*@PostConstruct
    @Transactional
    public void initialize() {
        if (eventTypeRepository.findOne(10) == null) {
            save(new EventTypeDTO(10, "Customer Walkin"));
            save(new EventTypeDTO(11, "Customer Email"));
            save(new EventTypeDTO(12, "Customer Call"));
            save(new EventTypeDTO(13, "Others"));
        }
    }*/

//    @PostConstruct
    @Transactional
    public void initialize() {
        try {
            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/event.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/event.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<CustomEventDTO> eventType = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (CustomEventDTO o: eventType) {
                    log.debug("Tampilan MIGRASI EVENT TYPE", eventType);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
