package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.FeatureApplicable;
import id.atiila.repository.FeatureApplicableRepository;
import id.atiila.repository.search.FeatureApplicableSearchRepository;
import id.atiila.service.dto.FeatureApplicableDTO;
import id.atiila.service.mapper.FeatureApplicableMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing FeatureApplicable.
 * atiila consulting
 */

@Service
@Transactional
public class FeatureApplicableService {

    private final Logger log = LoggerFactory.getLogger(FeatureApplicableService.class);

    private final FeatureApplicableRepository featureApplicableRepository;

    private final FeatureApplicableMapper featureApplicableMapper;

    private final FeatureApplicableSearchRepository featureApplicableSearchRepository;

    public FeatureApplicableService(FeatureApplicableRepository featureApplicableRepository, FeatureApplicableMapper featureApplicableMapper, FeatureApplicableSearchRepository featureApplicableSearchRepository) {
        this.featureApplicableRepository = featureApplicableRepository;
        this.featureApplicableMapper = featureApplicableMapper;
        this.featureApplicableSearchRepository = featureApplicableSearchRepository;
    }

    /**
     * Save a featureApplicable.
     *
     * @param featureApplicableDTO the entity to save
     * @return the persisted entity
     */
    public FeatureApplicableDTO save(FeatureApplicableDTO featureApplicableDTO) {
        log.debug("Request to save FeatureApplicable : {}", featureApplicableDTO);
        FeatureApplicable featureApplicable = featureApplicableMapper.toEntity(featureApplicableDTO);
        featureApplicable = featureApplicableRepository.save(featureApplicable);
        FeatureApplicableDTO result = featureApplicableMapper.toDto(featureApplicable);
        featureApplicableSearchRepository.save(featureApplicable);
        return result;
    }

    /**
     * Get all the featureApplicables.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FeatureApplicableDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FeatureApplicables");
        return featureApplicableRepository.findAll(pageable)
            .map(featureApplicableMapper::toDto);
    }

    /**
     * Get one featureApplicable by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public FeatureApplicableDTO findOne(UUID id) {
        log.debug("Request to get FeatureApplicable : {}", id);
        FeatureApplicable featureApplicable = featureApplicableRepository.findOne(id);
        return featureApplicableMapper.toDto(featureApplicable);
    }

    /**
     * Delete the featureApplicable by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete FeatureApplicable : {}", id);
        featureApplicableRepository.delete(id);
        featureApplicableSearchRepository.delete(id);
    }

    /**
     * Search for the featureApplicable corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FeatureApplicableDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of FeatureApplicables for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));

        String idFeatureType = request.getParameter("idFeatureType");
        String idFeature = request.getParameter("idFeature");
        String idProduct = request.getParameter("idProduct");
        String filterName = request.getParameter("filterName");

        if ("unitColor".equalsIgnoreCase(filterName)) {
            idFeature = null; idProduct = null; idFeature = null;
            q.withQuery(matchQuery("featureType.idFeatureType", Integer.valueOf(BaseConstants.FEAT_TYPE_COLOR)));
        }
        if (idFeatureType != null) {
            q.withQuery(matchQuery("featureType.idFeatureType", Integer.valueOf(idFeatureType)));
        }
        else if (idFeature != null) {
            q.withQuery(matchQuery("feature.idFeature", idFeature));
        }
        else if (idProduct != null) {
            q.withQuery(matchQuery("product.idProduct", idProduct));
        }

        Page<FeatureApplicable> result = featureApplicableSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(featureApplicableMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<FeatureApplicableDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered FeatureApplicableDTO");
        String filterName = request.getParameter("filterName");
        String idFeatureType = request.getParameter("idFeatureType");
        String idFeature = request.getParameter("idFeature");
        String idProduct = request.getParameter("idProduct");

        if (filterName != null) {
            if ("unitcolor".equalsIgnoreCase(filterName)) {
                return featureApplicableRepository.queryByFeatureType(BaseConstants.FEAT_TYPE_COLOR, pageable)
                    .map(featureApplicableMapper::toDto);
            }
            return featureApplicableRepository.queryNothing(pageable).map(featureApplicableMapper::toDto);
        }

        if (idFeatureType != null) {
            return featureApplicableRepository.queryByIdFeatureType(Integer.valueOf(idFeatureType), pageable).map(featureApplicableMapper::toDto);
        }
        else if (idFeature != null) {
            return featureApplicableRepository.queryByIdFeature(Integer.valueOf(idFeature), pageable).map(featureApplicableMapper::toDto);
        }
        else if (idProduct != null) {
            return featureApplicableRepository.queryByIdProduct(idProduct, pageable).map(featureApplicableMapper::toDto);
        }
        return featureApplicableRepository.queryNothing(pageable).map(featureApplicableMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, FeatureApplicableDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<FeatureApplicableDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        featureApplicableSearchRepository.deleteAll();
        List<FeatureApplicable> items =  featureApplicableRepository.findAll();
        for (FeatureApplicable m: items) {
            featureApplicableSearchRepository.save(m);
            log.debug("Data Feature Applicable save !...");
        }
    }


}
