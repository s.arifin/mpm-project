package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.service.dto.CustomerRequestItemDTO;
import id.atiila.service.mapper.CustomerRequestItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class RequestUtil {

    @Autowired
    private CustomerRequestItemRepository customerRequestItemRepository;

    @Autowired
    private VehicleCustomerRequestRepository vehicleCustomerRequestRepository;

    @Autowired
    private CustomerRequestItemMapper customerRequestItemMapper;

    @Autowired
    private PriceUtils priceUtils;

    @Autowired
    private ProductUtils productUtils;

    @Autowired
    private CustomerUtils customerUtils;

    @Autowired
    private SaleTypeRepository saleTypeRepository;

    @Autowired
    private OrderTypeRepository orderTypeRepository;

    @Autowired
    private VehicleSalesOrderRepository vehicleSalesOrderRepository;

    @Autowired
    private OrderRequestItemRepository orderRequestItemRepository;

    @Autowired
    private OrderUtils orderUtils;

    @Autowired
    private MasterNumberingService numbering;

    public CustomerRequestItem save(CustomerRequestItemDTO dto) {
        CustomerRequestItem r = customerRequestItemMapper.toEntity(dto);
        VehicleCustomerRequest vcr = vehicleCustomerRequestRepository.findOne(r.getRequest().getIdRequest());

        boolean isNew = dto.getIdRequestItem() == null;
        Internal internal = vcr.getInternal();

        if (r.getDescription() == null && r.getProduct() != null) {
            Product product = productUtils.getProduct(r.getProduct().getIdProduct());
            r.setDescription(product.getName());
        }

        if (r.getUnitPrice() == null || r.getUnitPrice().equals(0d)) {

            if (internal != null && r.getProduct() != null) {
                BigDecimal price = priceUtils.getSalesPrice(internal, r.getProduct().getIdProduct());

                if (price.doubleValue() == 0d || price == null) {
                    priceUtils.setInternalSalesPrice(internal, r.getProduct().getIdProduct(), BigDecimal.valueOf(14500000));
                    price = priceUtils.getSalesPrice(internal, r.getProduct().getIdProduct());
                }

                r.setUnitPrice(price);
            }
        }

        if (r.getBbn().doubleValue() == 0d) {

            if (internal != null && r.getProduct() != null) {
                BigDecimal bbn = priceUtils.getBBN(internal, r.getProduct().getIdProduct());

                if (bbn.doubleValue() == 0d) {
                    priceUtils.setBBN(internal, r.getProduct().getIdProduct(), 1500000);
                    bbn = priceUtils.getBBN(internal, r.getProduct().getIdProduct());
                }

                r.setBbn(bbn);
            }
        }

        r = customerRequestItemRepository.save(r);
        return r;
    }

    public void buildVSO(VehicleCustomerRequest r) {

        //Skip jika status bukan draft
        if (!r.getCurrentStatus().equals(BaseConstants.STATUS_OPEN))
            throw new DmsException("VSO Group/Direct has been processed !");

        // Jika Item kosong abaikan
        if (r.getDetails().isEmpty())
            throw new DmsException("VSO Group/Direct empty items");

        VehicleSalesOrder vso = new VehicleSalesOrder();
        vso.setInternal(r.getInternal());
        vso.setBillTo(customerUtils.getBillTo(r.getCustomer()));
        vso.setCustomer(r.getCustomer());
        vso.setSaleType(saleTypeRepository.findOne(SaleType.CASH));
//        if (vso.getOrderNumber() == null) {
//            vso.setOrderNumber(numbering.getInternalNumber("VSO"));
//        }
        int vsoType = r.getRequestType().getIdRequestType();
        switch (vsoType) {
            case RequestType.VSO_GROUP:
                vso.setOrderType(orderTypeRepository.findOne(OrderType.VSO_GROUP));
                break;
            case RequestType.VSO_INTERNAL:
                vso.setOrderType(orderTypeRepository.findOne(OrderType.VSO_DIRECT));
                break;
            default:
                vso.setOrderType(orderTypeRepository.findOne(OrderType.VSO_DIRECT));
                break;
        }

        Set<OrderRequestItem> slist = new HashSet<>();

        for (RequestItem ritem: r.getDetails()) {
            CustomerRequestItem cri = (CustomerRequestItem) ritem;
            OrderItem oi = new OrderItem();
            oi.assignFrom(cri);
            ShipTo shipTo = customerUtils.getShipTo(cri.getCustomer());
            oi.setIdShipTo(shipTo != null ? shipTo.getIdShipTo() : null);
            oi.setOrders(vso);
            vso.addDetail(oi);

            OrderRequestItem ori = new OrderRequestItem();
            ori.setOrderItem(oi);
            ori.setRequestItem(cri);
            ori.setQty(oi.getQty());
            ori.setQtyDelivered(0d);
            slist.add(ori);
        }

        vso.setStatus(BaseConstants.STATUS_APPROVED);
        vehicleSalesOrderRepository.save(vso);

        if (!slist.isEmpty()) {
            orderRequestItemRepository.save(slist);
        }
    }

    public void buildDO(CustomerRequestItemDTO cri) {
        List<OrderRequestItem> r = orderRequestItemRepository.queryByRequestItem(cri.getIdRequestItem());
        for (OrderRequestItem o: r) {
            if (o.getQty() > o.getQtyDelivered()) {
                List<UnitPreparation> lpis = orderUtils.buildDO(o.getOrderItem());
                Double qtyFilled = 0d;
                for (PickingSlip p: lpis) {
                    qtyFilled += p.getQty();
                }
                o.setQtyDelivered(o.getQtyDelivered() + qtyFilled);
                orderRequestItemRepository.save(o);
            }
        }
    }

}
