package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.security.SecurityUtils;
import id.atiila.service.dto.OrganizationDTO;
import id.atiila.service.dto.PersonDTO;
import id.atiila.service.mapper.OrganizationMapper;
import id.atiila.service.mapper.PersonMapper;
import id.atiila.web.rest.vm.ManagedUserVM;
import io.jsonwebtoken.lang.Assert;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;

@Service
@Transactional
public class PartyUtils {

    private final Logger log = LoggerFactory.getLogger(PartyUtils.class);

    @Autowired
    private ShipToRepository repoShipTo;

    @Autowired
    private BillToRepository repoBillTo;

    @Autowired
    private InternalRepository<Internal> internalRepo;

    @Autowired
    private OrganizationRepository repoOrganization;

    @Autowired
    private PersonRepository repoPerson;

    @Autowired
    private SalesmanRepository salesRepo;

    @Autowired
    private PartyContactMechanismRepository PartyContactMechanismRepository;

    @Autowired
    private PartyRoleTypeRepository partyRoleTypeRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMediatorRepository userMediatorRepository;

    @Autowired
    private PositionUtils positionUtils;

    @Autowired
    private InternalRepository internalRepository;

    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private RoleTypeRepository roleTypeRepository;

    @Autowired
    protected MasterNumberingService numberingService;

    @Autowired
    protected PersonMapper personMapper;

    @Autowired
    protected OrganizationMapper organizationMapper;

    @Autowired
    private PartyRepository partyRepository;

    @Autowired
    private RelationTypeRepository relationTypeRepository;

    @Autowired
    private StatusTypeRepository statusTypeRepository;

    @Autowired
    private PartyContactMechanismRepository partyContactMechanismRepository;

    public Organization findOrganizationByName(String name) {
        List<Organization> orgs = repoOrganization.findAllByName(name);
        if (orgs.isEmpty()) return null;
        return orgs.get(0);
    }

    protected PartyContactMechanism getContactMechanism(Party party, Integer purpose) {
        PartyContactMechanism cmp = partyContactMechanismRepository.getContactMechanism(party, purpose);
        if (cmp == null) {
            cmp = new PartyContactMechanism();
            cmp.setContact(new PostalAddress());
            cmp.setParty(party);
            cmp.setIdPurposeType(purpose);
            cmp = partyContactMechanismRepository.save(cmp);
        }
        return cmp;
    }

    public Organization buildOrganization(String name) {
        Organization organization = findOrganizationByName(name);
        if (organization == null) {
            organization = new Organization();
            organization.setName(name);
            organization = repoOrganization.save(organization);
            getContactMechanism(organization, BaseConstants.CPM_POSTALADDRESS);
        }
        return organization;
    }

    public Organization buildOrganization(Organization organization) {
        Organization org = null;
        Boolean found = false;

        if (organization.getIdParty() == null) {
            org = findOrganizationByName(organization.getName());
            if (org != null) found = true;
        }

        if (!found) {
            org = repoOrganization.save(organization);
            getContactMechanism(organization, BaseConstants.CPM_POSTALADDRESS);
        }
        return org;
    }

    private Person _buildPerson(Person person) {
        if (person.getIdParty() == null) {
            person = repoPerson.save(person);
            getContactMechanism(person, BaseConstants.CPM_POSTALADDRESS);
        }
        return person;
    }

    public Person buildPerson(String firstName, String lastName, String pob, ZonedDateTime dob) {
        Person person = repoPerson.findOneByFirstNameAndLastNameAndPobAndDob(firstName, lastName, pob, dob);
        if (person == null) {
            person = new Person();
            person.setFirstName(firstName);
            person.setLastName(lastName);
            person.setPob(pob);
            person.setDob(dob);
            person = _buildPerson(person);
        }
        return person;
    }

    public Person buildPerson(String firstName, String lastName, String cellPhone) {
        Person person = null;

        Set<Person> personSet = repoPerson.findByFirstNameAndLastNameAndCellPhone1(firstName, lastName, cellPhone);

        if (personSet.iterator().hasNext()) {
            if (personSet.size() > 1) {
                throw new DmsException("Data Customer Dengan Nomor Handphone " + cellPhone + " Lebih Dari 1, Mohon Hubungi Administrator");
            }
            else {
                person = personSet.iterator().next();
            }
        }

        if (person == null) {
            person = new Person();
            person.setFirstName(firstName);
            person.setLastName(lastName);
            person.setCellPhone1(cellPhone);
            person = _buildPerson(person);
        }
        return person;
    }

    public Organization findOrganization(Organization o) {
        if (o.getIdParty() != null) return o;
        Organization organization = null;
        organization = findOrganizationByName(o.getName());
        return organization;
    }

    public Person findPerson(Person p) {
        if (p.getIdParty() != null) return p;

        Person person = null;
        if (p.getPersonalIdNumber() != null) {
            List<Person> persons = repoPerson.findAllByPersonalIdNumber(p.getPersonalIdNumber());
            person = persons.isEmpty() ? null : persons.get(0);
        }
        if (person == null) person = repoPerson.findOneByFirstNameAndLastNameAndPobAndDob(p.getFirstName(), p.getLastName(), p.getPob(), p.getDob());
        if (person == null) {
            Set<Person> personSet = repoPerson.findByFirstNameAndLastNameAndCellPhone1(p.getFirstName(), p.getLastName(), p.getCellPhone1());
            if (personSet.iterator().hasNext()) {
                if (personSet.size() > 1) {
                    throw new DmsException("Data Customer Dengan Nomor Handphone " + p.getCellPhone1() + " Lebih Dari 1, Mohon Hubungi Administrator");
                }
                else {
                    person = personSet.iterator().next();
                }
            }
        }
        return person;
    }

    public Person findPersonByPhone(Person p){
        Set<Person> personSet;

        if (p.getIdParty() != null) return p;

        Person person = null;
        if (person == null) {
            personSet = repoPerson.findByFirstNameAndLastNameAndCellPhone1(p.getFirstName(), p.getLastName(), p.getCellPhone1());
            if (!personSet.isEmpty()) {
                if (personSet.size() > 1) {
                    throw new DmsException("Data Customer Dengan Nomor Handphone " + p.getCellPhone1() + " Lebih Dari 1, Mohon Hubungi Administrator");
                }
                else {
                    person = personSet.iterator().next();
                }
            }
        }
        if (person == null) {
            personSet = repoPerson.findByCellPhone1(p.getCellPhone1());
            if (personSet.iterator().hasNext()) {
                if (personSet.size() > 1) {
                    throw new DmsException("Data Customer Dengan Nomor Handphone " + p.getCellPhone1() + " Lebih Dari 1, Mohon Hubungi Administrator");
                }
                else {
                    person = personSet.iterator().next();
                }
            }
        }
        return person;
    }

    public Person buildPerson(Person p) {
        if (p.getIdParty() != null) return p;

        Person person = null;
        if (p.getPersonalIdNumber() != null) {
            List<Person> persons = repoPerson.findAllByPersonalIdNumber(p.getPersonalIdNumber());
            person = persons.isEmpty() ? null : persons.get(0);
        }
        if (person == null) person = repoPerson.findOneByFirstNameAndLastNameAndPobAndDob(p.getFirstName(), p.getLastName(), p.getPob(), p.getDob());
        if (person == null) {

            Set<Person> personSet = repoPerson.findByFirstNameAndLastNameAndCellPhone1(p.getFirstName(), p.getLastName(), p.getCellPhone1());

            if (personSet.iterator().hasNext()) {
                if (personSet.size() > 1) {
                    throw new DmsException("Data Customer Dengan Nomor Handphone " + p.getCellPhone1() + " Lebih Dari 1, Mohon Hubungi Administrator");
                }
                else {
                    person = personSet.iterator().next();
                }
            }
        }
        if (person == null) person = _buildPerson(p);
        return person;
    }

    private RoleType getRoleType(Integer role) {
        RoleType r = roleTypeRepository.findOne(role);
        if (r == null) {
            r = new RoleType();
            r.setIdRoleType(role);
            r.setDescription("Unknown");
            r = roleTypeRepository.save(r);
        }
        return r;
    }

    public PartyRoleType getPartyRoleType(Party p, RoleType role) {
        PartyRoleTypeId pr = new PartyRoleTypeId(role.getIdRoleType(), p.getIdParty());
        PartyRoleType r = partyRoleTypeRepository.findOne(pr);
        if (r == null) {
            r = new PartyRoleType(p, role);
            partyRoleTypeRepository.save(r);
        }
        return r;
    }

    public PartyRoleType getPartyRoleType(Party p, Integer role) {
        RoleType roletype = getRoleType(role);
        return getPartyRoleType(p, roletype);
    }

    public ShipTo getShipTo(String id) {
        return repoShipTo.findOne(id);
    }

    public BillTo getBillTo(String id) {
        return repoBillTo.findOne(id);
    }

    public BillTo getBillTo(Party p) {
        return repoBillTo.findByParty(p);
    }

    public ShipTo getShipTo(Party p) {
        return repoShipTo.findByParty(p);
    }

    @Transactional
    public Person save(Person p) {
        p = repoPerson.save(p);
        return p;
    }

    @Transactional
    public Organization save(Organization p) {
        p = repoOrganization.save(p);
        return p;
    }

    @Transactional
    public void buildAppUser(UserMediator um, String ...roles) {
        if (!userRepository.findOneByLogin(um.getUserName()).isPresent()) {
            Set<String> items = new HashSet<>();
            for (String item: roles) {
                items.add(item);
            }
            if (items.isEmpty()) items.add("ROLE_USER");

            ManagedUserVM userDTO = new ManagedUserVM();
            userDTO.setLogin(um.getUserName());
            userDTO.setEmail(um.getEmail());
            userDTO.setFirstName(um.getPerson().getFirstName());
            userDTO.setLastName(um.getPerson().getLastName());
            userDTO.setLangKey("id");
            userDTO.setActivated(true);
            userDTO.setAuthorities(Collections.singleton("ROLE_USER"));
            User user =  userService.registerUser(userDTO, "nop4ssword");
        }
    }

    @Transactional
    public void buildUser(Internal internal, String userLogin, String userEmail, String firstName, String lastName) {
        UserMediator um = userMediatorRepository.findByUserName(userLogin);
        if (um == null) {
            Person person = new Person();
            person.setFirstName(firstName);
            person.setLastName(lastName);
            person.setPrivateMail(userEmail);
            person = repoPerson.save(person);

            um = new UserMediator();
            um.setUserName(userLogin);
            um.setEmail(userEmail);
            um.setPerson(person);
            um.setInternal(internal);
            userMediatorRepository.save(um);

            buildAppUser(um);

        }
    }

    @Transactional
    public void buildUser(Internal internal, Position position, String userLogin, String userEmail, String firstName, String lastName) {
        UserMediator um = userMediatorRepository.findByUserName(userLogin);
        if (um == null) {
            Person person = new Person();
            person.setFirstName(firstName);
            person.setLastName(lastName);
            person.setPrivateMail(userEmail);
            person = repoPerson.save(person);

            um = new UserMediator();
            um.setUserName(userLogin);
            um.setEmail(userEmail);
            um.setPerson(person);
            um.setInternal(internal);
            um = userMediatorRepository.save(um);

            positionUtils.assignPosition(position, um);

            buildAppUser(um);

        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildUser(Internal internal, String userLogin, String userEmail, Person person, String ...roles) {
        UserMediator um = userMediatorRepository.findByUserName(userLogin);
        if (um == null) {
            um = new UserMediator();
            um.setUserName(userLogin);
            um.setEmail(userEmail);
            um.setPerson(person);
            um.setInternal(internal);
            um = userMediatorRepository.save(um);
            buildAppUser(um, roles);
        }
    }

    public Salesman getCurrentSalesman(){
        UserMediator um = userMediatorRepository.findByUserName(SecurityUtils.getCurrentUserLogin().get());
        return salesRepo.findOneByParty(um.getPerson());
    }

    public Organization getCurrentOrganizationByInternal(String idinternal) {
        Internal i = internalRepository.findByIdInternal(idinternal);
        if (i != null) {
            Organization organization = repoOrganization.findOne(i.getOrganization().getIdParty());
            if (organization != null) return organization;
        }
        return null;
    }

    public Vendor getCurrentVendorByIdVendor(String idvendor) {
        Vendor v = vendorRepository.findOne(idvendor);
        return v;
    }

    public Internal getCurrentInternal() {
        if (!SecurityUtils.getCurrentUserLogin().isPresent()) return null;
        UserMediator um = userMediatorRepository.findByUserName(SecurityUtils.getCurrentUserLogin().get());
        return um != null ? um.getInternal() : null;
    }

    public Person getPerson(UUID id) {
        return repoPerson.findOne(id);
    }

    public Organization getOrganization(UUID id) {
        return repoOrganization.findOne(id);
    }

    public PersonDTO getPersonDTO(Person person) {
        return personMapper.toDto(person);
    }

    public OrganizationDTO getOrganizationDTO(Organization organization) {
        return organizationMapper.toDto(organization);
    }

    public Person getPerson(PersonDTO dto) {
        return personMapper.toEntity(dto);
    }

    public Organization getOrganization(OrganizationDTO dto) {
        return organizationMapper.toEntity(dto);
    }

    public void buildShipTo(DelegateExecution execution) {
        Party party = execution.getVariable("party", Party.class);
        ShipTo shipTo = repoShipTo.findByParty(party);
        if (shipTo == null) {

            String idGeneral = execution.getVariable("idGeneral", String.class);
            Integer idRoleType = execution.getVariable("idRoleType", Integer.class);

            Boolean idHasFound = true;
            if (idGeneral != null) {
                idHasFound = repoShipTo.findOne(idGeneral) != null;
            }

            getPartyRoleType(party, idRoleType);
            getPartyRoleType(party, BaseConstants.ROLE_SHIPTO);

            shipTo = new ShipTo();
            shipTo.setIdRoleType(idRoleType);
            shipTo.setParty(party);
            shipTo.setIdShipTo(idGeneral);
            if (idHasFound) {
                String idShipTo = null;
                do {
                    idShipTo = numberingService.nextShipToValue();
                } while (repoShipTo.findOne(idShipTo) != null);
                shipTo.setIdShipTo(idShipTo);
            }

            repoShipTo.save(shipTo);
        }
    }

    public void buildBillTo(DelegateExecution execution) {
        Party party = execution.getVariable("party", Party.class);
        BillTo billTo = repoBillTo.findByParty(party);
        if (billTo == null) {
            String idGeneral = execution.getVariable("idGeneral", String.class);
            Integer idRoleType = execution.getVariable("idRoleType", Integer.class);

            Boolean idHasFound = true;
            if (idGeneral != null) {
                idHasFound = repoBillTo.findOne(idGeneral) != null;
            }

            getPartyRoleType(party, idRoleType);
            getPartyRoleType(party, BaseConstants.ROLE_BILLTO);

            billTo = new BillTo();
            billTo.setIdRoleType(idRoleType);
            billTo.setParty(party);
            billTo.setIdBillTo(idGeneral);
            if (idHasFound) {
                String idBillTo = null;
                do {
                    idBillTo = numberingService.nextBillToValue();
                } while (repoBillTo.findOne(idBillTo) != null);
                billTo.setIdBillTo(idBillTo);
            }
            repoBillTo.save(billTo);
        }
    }

    public RelationType getRelationType(Integer id, String description) {
        RelationType r = relationTypeRepository.findOne(id);
        if (r == null) {
            r = new RelationType();
            r.setIdRelationType(id);
            r.setDescription(description);
            r = relationTypeRepository.save(r);
        }
        return r;
    }

    public StatusType getStatusType(Integer id, String description) {
        StatusType r = statusTypeRepository.findOne(id);
        if (r == null) {
            r = new StatusType();
            r.setIdStatusType(id);
            r.setDescription(description);
            r = statusTypeRepository.save(r);
        }
        return r;
    }

}
