package id.atiila.service;

import id.atiila.domain.BillingItemType;
import id.atiila.repository.BillingItemTypeRepository;
import id.atiila.repository.search.BillingItemTypeSearchRepository;
import id.atiila.service.dto.BillingItemTypeDTO;
import id.atiila.service.mapper.BillingItemTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing BillingItemType.
 * atiila consulting
 */

@Service
@Transactional
public class BillingItemTypeService {

    private final Logger log = LoggerFactory.getLogger(BillingItemTypeService.class);

    private final BillingItemTypeRepository billingItemTypeRepository;

    private final BillingItemTypeMapper billingItemTypeMapper;

    private final BillingItemTypeSearchRepository billingItemTypeSearchRepository;

    public BillingItemTypeService(BillingItemTypeRepository billingItemTypeRepository, BillingItemTypeMapper billingItemTypeMapper, BillingItemTypeSearchRepository billingItemTypeSearchRepository) {
        this.billingItemTypeRepository = billingItemTypeRepository;
        this.billingItemTypeMapper = billingItemTypeMapper;
        this.billingItemTypeSearchRepository = billingItemTypeSearchRepository;
    }

    /**
     * Save a billingItemType.
     *
     * @param billingItemTypeDTO the entity to save
     * @return the persisted entity
     */
    public BillingItemTypeDTO save(BillingItemTypeDTO billingItemTypeDTO) {
        log.debug("Request to save BillingItemType : {}", billingItemTypeDTO);
        BillingItemType billingItemType = billingItemTypeMapper.toEntity(billingItemTypeDTO);
        billingItemType = billingItemTypeRepository.save(billingItemType);
        BillingItemTypeDTO result = billingItemTypeMapper.toDto(billingItemType);
        billingItemTypeSearchRepository.save(billingItemType);
        return result;
    }

    /**
     * Get all the billingItemTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BillingItemTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BillingItemTypes");
        return billingItemTypeRepository.findAll(pageable)
            .map(billingItemTypeMapper::toDto);
    }

    /**
     * Get one billingItemType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BillingItemTypeDTO findOne(Integer id) {
        log.debug("Request to get BillingItemType : {}", id);
        BillingItemType billingItemType = billingItemTypeRepository.findOne(id);
        return billingItemTypeMapper.toDto(billingItemType);
    }

    /**
     * Delete the billingItemType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete BillingItemType : {}", id);
        billingItemTypeRepository.delete(id);
        billingItemTypeSearchRepository.delete(id);
    }

    /**
     * Search for the billingItemType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BillingItemTypeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of BillingItemTypes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));


        Page<BillingItemType> result = billingItemTypeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(billingItemTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<BillingItemTypeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered BillingItemTypeDTO");


        return billingItemTypeRepository.queryNothing(pageable).map(billingItemTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, BillingItemTypeDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<BillingItemTypeDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
