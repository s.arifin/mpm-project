package id.atiila.service;

import id.atiila.domain.PositionReportingStructure;
import id.atiila.repository.PositionReportingStructureRepository;
import id.atiila.repository.search.PositionReportingStructureSearchRepository;
import id.atiila.service.dto.PositionReportingStructureDTO;
import id.atiila.service.mapper.PositionReportingStructureMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PositionReportingStructure.
 * BeSmart Team
 */

@Service
@Transactional
public class PositionReportingStructureService {

    private final Logger log = LoggerFactory.getLogger(PositionReportingStructureService.class);

    private final PositionReportingStructureRepository positionReportingStructureRepository;

    private final PositionReportingStructureMapper positionReportingStructureMapper;

    private final PositionReportingStructureSearchRepository positionReportingStructureSearchRepository;

    public PositionReportingStructureService(PositionReportingStructureRepository positionReportingStructureRepository, PositionReportingStructureMapper positionReportingStructureMapper, PositionReportingStructureSearchRepository positionReportingStructureSearchRepository) {
        this.positionReportingStructureRepository = positionReportingStructureRepository;
        this.positionReportingStructureMapper = positionReportingStructureMapper;
        this.positionReportingStructureSearchRepository = positionReportingStructureSearchRepository;
    }

    /**
     * Save a positionReportingStructure.
     *
     * @param positionReportingStructureDTO the entity to save
     * @return the persisted entity
     */
    public PositionReportingStructureDTO save(PositionReportingStructureDTO positionReportingStructureDTO) {
        log.debug("Request to save PositionReportingStructure : {}", positionReportingStructureDTO);
        PositionReportingStructure positionReportingStructure = positionReportingStructureMapper.toEntity(positionReportingStructureDTO);
        positionReportingStructure = positionReportingStructureRepository.save(positionReportingStructure);
        PositionReportingStructureDTO result = positionReportingStructureMapper.toDto(positionReportingStructure);
        positionReportingStructureSearchRepository.save(positionReportingStructure);
        return result;
    }

    /**
     *  Get all the positionReportingStructures.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PositionReportingStructureDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PositionReportingStructures");
        return positionReportingStructureRepository.findAll(pageable)
            .map(positionReportingStructureMapper::toDto);
    }

    /**
     *  Get one positionReportingStructure by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PositionReportingStructureDTO findOne(Integer id) {
        log.debug("Request to get PositionReportingStructure : {}", id);
        PositionReportingStructure positionReportingStructure = positionReportingStructureRepository.findOne(id);
        return positionReportingStructureMapper.toDto(positionReportingStructure);
    }

    /**
     *  Delete the  positionReportingStructure by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete PositionReportingStructure : {}", id);
        positionReportingStructureRepository.delete(id);
        positionReportingStructureSearchRepository.delete(id);
    }

    /**
     * Search for the positionReportingStructure corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PositionReportingStructureDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PositionReportingStructures for query {}", query);
        Page<PositionReportingStructure> result = positionReportingStructureSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(positionReportingStructureMapper::toDto);
    }

    public PositionReportingStructureDTO processExecuteData(Integer id, String param, PositionReportingStructureDTO dto) {
        PositionReportingStructureDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<PositionReportingStructureDTO> processExecuteListData(Integer id, String param, Set<PositionReportingStructureDTO> dto) {
        Set<PositionReportingStructureDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        positionReportingStructureSearchRepository.deleteAll();
        List<PositionReportingStructure> positionReportingStructures =  positionReportingStructureRepository.findAll();
        for (PositionReportingStructure m: positionReportingStructures) {
            positionReportingStructureSearchRepository.save(m);
            log.debug("Data position reporting structure save !...");
        }
    }

}
