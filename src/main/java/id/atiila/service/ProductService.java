package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.base.XlsxUtils;
import id.atiila.base.event.UploadDataEvent;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.ProductSearchRepository;
import id.atiila.service.dto.CustomProductDTO;
import id.atiila.service.dto.CustomProductFeatureDTO;
import id.atiila.service.dto.ProductDTO;
import id.atiila.service.dto.ReligionTypeDTO;
import id.atiila.service.mapper.ProductMapper;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.xml.sax.SAXException;

import javax.annotation.PostConstruct;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Product.
 * BeSmart Team
 */

@Service
@Transactional
public class ProductService extends ProductBaseService {

    private final Logger log = LoggerFactory.getLogger(ProductService.class);

    public static final String QUEU_APPEND_FEATRUE = "addProductFeature";

    private final ProductRepository productRepository;

    private final ProductMapper productMapper;

    private final ProductSearchRepository productSearchRepository;

    @Autowired
    private FeatureRepository featureRepository;

    @Autowired
    private JmsTemplate template;

    @Autowired
    private GeneralUploadRepository generalUploadRepository;

    @Autowired
    private MotorRepository motorRepository;

    @Autowired
    private FeatureApplicableRepository featureApplicableRepository;

    @Autowired
    private ProductUtils productUtils;

    public ProductService(ProductRepository productRepository, ProductMapper productMapper, ProductSearchRepository productSearchRepository) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.productSearchRepository = productSearchRepository;
    }

    /**
     * Save a product.
     *
     * @param productDTO the entity to save
     * @return the persisted entity
     */
    public ProductDTO save(ProductDTO productDTO) {
        log.debug("Request to save Product : {}", productDTO);
        Product product = productMapper.toEntity(productDTO);

        Boolean isNew = product.getIdProduct() == null;
        if (isNew) {
            String newValue = null;
            while (newValue == null || productRepository.findOne(newValue) != null) {
                newValue = numbering.nextValue("idproduct", 1000000l).toString();
            }
            product.setIdProduct(newValue);
        }

        product = productRepository.save(product);
        ProductDTO result = productMapper.toDto(product);
        productSearchRepository.save(product);
        return result;
    }

    /**
     *  Get all the products.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Products");
        return productRepository.findAll(pageable)
            .map(productMapper::toDto);
    }

    /**
     *  Get one product by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ProductDTO findOne(String id) {
        log.debug("Request to get Product : {}", id);
        Product product = productRepository.findOneWithEagerRelationships(id);
        return productMapper.toDto(product);
    }

    /**
     *  Delete the  product by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Product : {}", id);
        productRepository.delete(id);
        productSearchRepository.delete(id);
    }

    /**
     * Search for the product corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Products for query {}", query);
        Page<Product> result = productSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(productMapper::toDto);
    }

    public ProductDTO processExecuteData(Integer id, String param, ProductDTO dto) {
        ProductDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<ProductDTO> processExecuteListData(Integer id, String param, Set<ProductDTO> dto) {
        Set<ProductDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Override
    public Product findById(String id) {
        return productRepository.findOne(id);
    }


    @Transactional
    public void initialize() {
        try {
            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/product_unit.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/product_unit.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<CustomProductDTO> products = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (CustomProductDTO o: products) {
                    log.debug("Tampilan DTO migrasi product id", products);
                    // if (o.getParentGeoCode() != null && o.getGeocode() != null) {
                    //     addVillage(o.getParentGeoCode(), o.getGeocode(), o.getDescription());
                    // }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void migrateFeature(Internal internal, InputStream initFile) throws IOException, InvalidFormatException, SAXException {
        DefaultResourceLoader loader = new DefaultResourceLoader();
        InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/product-color.xml").getInputStream());

        if (templateXML != null && initFile != null) {
            ReaderConfig.getInstance().setSkipErrors(true);
            ReaderConfig.getInstance().setUseDefaultValuesForPrimitiveTypes( true );

            List<CustomProductFeatureDTO> productFeatures = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
            for (CustomProductFeatureDTO o : productFeatures) {
                if (o.getIdFeatureType() == null) o.setIdFeatureType(BaseConstants.FEAT_TYPE_COLOR);
                template.convertAndSend(ProductService.QUEU_APPEND_FEATRUE, o);
                log.info("Add Product Feature: " + o.toString());
            }
        }
    }

    @EventListener
    @Transactional
    public void doUpload(UploadDataEvent e) throws InvalidFormatException, SAXException, IOException {
        if (e.getGeneralUpload() != null && e.getPurposeType().equals(BaseConstants.PURP_TYPE_PRODUCT_FEATURE)) {
            GeneralUpload g = e.getGeneralUpload();
            migrateFeature(g.getInternal(), new ByteArrayInputStream(g.getValue()));
            e.getGeneralUpload().setStatus(BaseConstants.STATUS_COMPLETED);
            generalUploadRepository.save(g);
        }
    }

    @JmsListener(destination = QUEU_APPEND_FEATRUE)
    public void addProductFeature(CustomProductFeatureDTO dto) {
        Product product = motorRepository.findOneWithEagerRelationships(dto.getIdProduct());
        Feature feature = featureRepository.findByKey(dto.getIdFeatureType(), dto.getIdFeature());
        productUtils.enableFeatured(product, BaseConstants.FEAT_TYPE_COLOR, feature.getRefKey());

        if (product != null && feature != null && !product.getFeatures().contains(feature)) {
            product.addFeature(feature);
            productRepository.save(product);
            log.info("Append Product Feature : " + product.getIdProduct() + " " + feature.getIdFeature());
        }
    }

}
