package id.atiila.service;

import id.atiila.domain.ShipmentOutgoing;
import id.atiila.repository.UnitShipmentOutgoingRepository;
import id.atiila.repository.search.UnitShipmentOutgoingSearchRepository;
import id.atiila.service.dto.UnitShipmentOutgoingDTO;
import id.atiila.service.mapper.UnitShipmentOutgoingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ShipmentOutgoing.
 * atiila consulting
 */

@Service
@Transactional
public class UnitShipmentOutgoingService {

    private final Logger log = LoggerFactory.getLogger(UnitShipmentOutgoingService.class);

    private final UnitShipmentOutgoingRepository unitShipmentOutgoingRepository;

    private final UnitShipmentOutgoingMapper unitShipmentOutgoingMapper;

    private final UnitShipmentOutgoingSearchRepository unitShipmentOutgoingSearchRepository;

    @Autowired
    private MasterNumberingService numbering;

    public UnitShipmentOutgoingService(UnitShipmentOutgoingRepository unitShipmentOutgoingRepository, UnitShipmentOutgoingMapper unitShipmentOutgoingMapper, UnitShipmentOutgoingSearchRepository unitShipmentOutgoingSearchRepository) {
        this.unitShipmentOutgoingRepository = unitShipmentOutgoingRepository;
        this.unitShipmentOutgoingMapper = unitShipmentOutgoingMapper;
        this.unitShipmentOutgoingSearchRepository = unitShipmentOutgoingSearchRepository;
    }

    /**
     * Save a ShipmentOutgoing.
     *
     * @param unitShipmentOutgoingDTO the entity to save
     * @return the persisted entity
     */
    public UnitShipmentOutgoingDTO save(UnitShipmentOutgoingDTO unitShipmentOutgoingDTO) {
        log.debug("Request to save ShipmentOutgoing : {}", unitShipmentOutgoingDTO);
        ShipmentOutgoing shipmentOutgoing = unitShipmentOutgoingMapper.toEntity(unitShipmentOutgoingDTO);
        if (shipmentOutgoing.getShipmentNumber() == null) {
            shipmentOutgoing.setShipmentNumber(numbering.getInternalNumber("DO"));
        }
        shipmentOutgoing = unitShipmentOutgoingRepository.save(shipmentOutgoing);
        UnitShipmentOutgoingDTO result = unitShipmentOutgoingMapper.toDto(shipmentOutgoing);
        unitShipmentOutgoingSearchRepository.save(shipmentOutgoing);
        return result;
    }

    /**
     * Get all the unitShipmentOutgoings.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UnitShipmentOutgoingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UnitShipmentOutgoings");
        return unitShipmentOutgoingRepository.findActiveUnitShipmentOutgoing(pageable)
            .map(unitShipmentOutgoingMapper::toDto);
    }

    /**
     * Get one ShipmentOutgoing by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public UnitShipmentOutgoingDTO findOne(UUID id) {
        log.debug("Request to get ShipmentOutgoing : {}", id);
        ShipmentOutgoing shipmentOutgoing = unitShipmentOutgoingRepository.findOne(id);
        return unitShipmentOutgoingMapper.toDto(shipmentOutgoing);
    }

    /**
     * Delete the ShipmentOutgoing by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ShipmentOutgoing : {}", id);
        unitShipmentOutgoingRepository.delete(id);
        unitShipmentOutgoingSearchRepository.delete(id);
    }

    /**
     * Search for the ShipmentOutgoing corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UnitShipmentOutgoingDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of UnitShipmentOutgoings for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idShipmentType = request.getParameter("idShipmentType");
        String idShipFrom = request.getParameter("idShipFrom");
        String idShipTo = request.getParameter("idShipTo");
        String idAddressFrom = request.getParameter("idAddressFrom");
        String idAddressTo = request.getParameter("idAddressTo");

        if (idShipmentType != null) {
            q.withQuery(matchQuery("shipmentType.idShipmentType", idShipmentType));
        }
        else if (idShipFrom != null) {
            q.withQuery(matchQuery("shipFrom.idShipTo", idShipFrom));
        }
        else if (idShipTo != null) {
            q.withQuery(matchQuery("shipTo.idShipTo", idShipTo));
        }
        else if (idAddressFrom != null) {
            q.withQuery(matchQuery("addressFrom.idContact", idAddressFrom));
        }
        else if (idAddressTo != null) {
            q.withQuery(matchQuery("addressTo.idContact", idAddressTo));
        }

        Page<ShipmentOutgoing> result = unitShipmentOutgoingSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(unitShipmentOutgoingMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<UnitShipmentOutgoingDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered UnitShipmentOutgoingDTO");
        String idShipmentType = request.getParameter("idShipmentType");
        String idShipFrom = request.getParameter("idShipFrom");
        String idShipTo = request.getParameter("idShipTo");
        String idAddressFrom = request.getParameter("idAddressFrom");
        String idAddressTo = request.getParameter("idAddressTo");

        if (idShipmentType != null) {
            return unitShipmentOutgoingRepository.queryByIdShipmentType(Integer.valueOf(idShipmentType), pageable).map(unitShipmentOutgoingMapper::toDto);
        }
        else if (idShipFrom != null) {
            return unitShipmentOutgoingRepository.queryByIdShipFrom(idShipFrom, pageable).map(unitShipmentOutgoingMapper::toDto);
        }
        else if (idShipTo != null) {
            return unitShipmentOutgoingRepository.queryByIdShipTo(idShipTo, pageable).map(unitShipmentOutgoingMapper::toDto);
        }
        else if (idAddressFrom != null) {
            return unitShipmentOutgoingRepository.queryByIdAddressFrom(UUID.fromString(idAddressFrom), pageable).map(unitShipmentOutgoingMapper::toDto);
        }
        else if (idAddressTo != null) {
            return unitShipmentOutgoingRepository.queryByIdAddressTo(UUID.fromString(idAddressTo), pageable).map(unitShipmentOutgoingMapper::toDto);
        }

        return unitShipmentOutgoingRepository.queryNothing(pageable).map(unitShipmentOutgoingMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, UnitShipmentOutgoingDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<UnitShipmentOutgoingDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public UnitShipmentOutgoingDTO changeUnitShipmentOutgoingStatus(UnitShipmentOutgoingDTO dto, Integer id) {
        if (dto != null) {
			ShipmentOutgoing e = unitShipmentOutgoingRepository.findOne(dto.getIdShipment());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                    case 17:
                        unitShipmentOutgoingSearchRepository.delete(dto.getIdShipment());
                        break;
                    default:
                        unitShipmentOutgoingSearchRepository.save(e);
                }
				e = unitShipmentOutgoingRepository.save(e);
                return unitShipmentOutgoingMapper.toDto(e);
			}
		}
        return dto;
    }
}
