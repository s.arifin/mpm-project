package id.atiila.service;

import id.atiila.domain.Product;
import id.atiila.domain.RemPart;
import id.atiila.repository.RemPartRepository;
import id.atiila.repository.search.RemPartSearchRepository;
import id.atiila.service.dto.RemPartDTO;
import id.atiila.service.mapper.RemPartMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing RemPart.
 * BeSmart Team
 */

@Service
@Transactional
public class RemPartService extends ProductBaseService {

    private final Logger log = LoggerFactory.getLogger(RemPartService.class);

    private final RemPartRepository remPartRepository;

    private final RemPartMapper remPartMapper;

    private final RemPartSearchRepository remPartSearchRepository;

    public RemPartService(RemPartRepository remPartRepository, RemPartMapper remPartMapper, RemPartSearchRepository remPartSearchRepository) {
        this.remPartRepository = remPartRepository;
        this.remPartMapper = remPartMapper;
        this.remPartSearchRepository = remPartSearchRepository;
    }

    /**
     * Save a remPart.
     *
     * @param remPartDTO the entity to save
     * @return the persisted entity
     */
    public RemPartDTO save(RemPartDTO remPartDTO) {
        log.debug("Request to save RemPart : {}", remPartDTO);
        RemPart remPart = remPartMapper.toEntity(remPartDTO);

        Boolean isNew = remPart.getIdProduct() == null;
        if (isNew) {
            String newValue = null;
            while (newValue == null || remPartRepository.findOne(newValue) != null) {
                newValue = numbering.nextValue("idrempart", 40000l).toString();
            }
            remPart.setIdProduct(newValue);
        }

        remPart = remPartRepository.save(remPart);
        RemPartDTO result = remPartMapper.toDto(remPart);
        remPartSearchRepository.save(remPart);
        return result;
    }

    /**
     *  Get all the remParts.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RemPartDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RemParts");
        return remPartRepository.findAll(pageable)
            .map(remPartMapper::toDto);
    }

    /**
     *  Get one remPart by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public RemPartDTO findOne(String id) {
        log.debug("Request to get RemPart : {}", id);
        RemPart remPart = remPartRepository.findOneWithEagerRelationships(id);
        return remPartMapper.toDto(remPart);
    }

    /**
     *  Delete the  remPart by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete RemPart : {}", id);
        remPartRepository.delete(id);
        remPartSearchRepository.delete(id);
    }

    /**
     * Search for the remPart corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RemPartDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of RemParts for query {}", query);
        Page<RemPart> result = remPartSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(remPartMapper::toDto);
    }

    @Override
    public Product findById(String id) {
        return remPartRepository.findOne(id);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        remPartSearchRepository.deleteAll();
        List<RemPart> remparts =  remPartRepository.findAll();
        for (RemPart m: remparts) {
            remPartSearchRepository.save(m);
            log.debug("Data Rem save !...");
        }
    }
}
