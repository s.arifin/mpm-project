package id.atiila.service;

import id.atiila.domain.Shipment;
import id.atiila.repository.ShipmentRepository;
import id.atiila.repository.search.ShipmentSearchRepository;
import id.atiila.service.dto.ShipmentDTO;
import id.atiila.service.mapper.ShipmentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing Shipment.
 * BeSmart Team
 */

@Service
@Transactional
public class ShipmentService {

    private final Logger log = LoggerFactory.getLogger(ShipmentService.class);

    private final ShipmentRepository shipmentRepository;

    private final ShipmentMapper shipmentMapper;

    private final ShipmentSearchRepository shipmentSearchRepository;

    public ShipmentService(ShipmentRepository shipmentRepository, ShipmentMapper shipmentMapper, ShipmentSearchRepository shipmentSearchRepository) {
        this.shipmentRepository = shipmentRepository;
        this.shipmentMapper = shipmentMapper;
        this.shipmentSearchRepository = shipmentSearchRepository;
    }

    /**
     * Save a shipment.
     *
     * @param shipmentDTO the entity to save
     * @return the persisted entity
     */
    public ShipmentDTO save(ShipmentDTO shipmentDTO) {
        log.debug("Request to save Shipment : {}", shipmentDTO);
        Shipment shipment = shipmentMapper.toEntity(shipmentDTO);
        shipment = shipmentRepository.save(shipment);
        ShipmentDTO result = shipmentMapper.toDto(shipment);
        shipmentSearchRepository.save(shipment);
        return result;
    }

    /**
     * Get all the shipments.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Shipments");
        return shipmentRepository.findActiveShipment(pageable)
            .map(shipmentMapper::toDto);
    }

    /**
     * Get one shipment by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ShipmentDTO findOne(UUID id) {
        log.debug("Request to get Shipment : {}", id);
        Shipment shipment = shipmentRepository.findOne(id);
        return shipmentMapper.toDto(shipment);
    }

    /**
     * Delete the shipment by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Shipment : {}", id);
        shipmentRepository.delete(id);
        shipmentSearchRepository.delete(id);
    }

    /**
     * Search for the shipment corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of Shipments for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idShipmentType = request.getParameter("idShipmentType");
        String idShipFrom = request.getParameter("idShipFrom");
        String idShipTo = request.getParameter("idShipTo");
        String idAddressFrom = request.getParameter("idAddressFrom");
        String idAddressTo = request.getParameter("idAddressTo");

        if (filterName != null) {
        }
        Page<Shipment> result = shipmentSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(shipmentMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ShipmentDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ShipmentDTO");
        String idShipmentType = request.getParameter("idShipmentType");
        String idShipFrom = request.getParameter("idShipFrom");
        String idShipTo = request.getParameter("idShipTo");
        String idAddressFrom = request.getParameter("idAddressFrom");
        String idAddressTo = request.getParameter("idAddressTo");

        return shipmentRepository.findByParams(idShipmentType, idShipFrom, idShipTo, idAddressFrom, idAddressTo, pageable)
            .map(shipmentMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public ShipmentDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        ShipmentDTO r = null;
        return r;
    }

    @Transactional
    public Set<ShipmentDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<ShipmentDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public ShipmentDTO changeShipmentStatus(ShipmentDTO dto, Integer id) {
        if (dto != null) {
			Shipment e = shipmentRepository.findOne(dto.getIdShipment());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        shipmentSearchRepository.delete(dto.getIdShipment());
                        break;
                    default:
                        shipmentSearchRepository.save(e);
                }
				shipmentRepository.save(e);
			}
		}
        return dto;
    }
}
