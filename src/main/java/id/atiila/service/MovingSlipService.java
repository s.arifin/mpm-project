package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.base.executeprocess.MovingSlipConstants;
import id.atiila.domain.*;
import id.atiila.repository.InventoryItemRepository;
import id.atiila.repository.MovingSlipRepository;
import id.atiila.repository.search.MovingSlipSearchRepository;
import id.atiila.route.DemandSupplyRoute;
import id.atiila.service.dto.InventoryItemDTO;
import id.atiila.service.dto.MovingSlipDTO;
import id.atiila.service.dto.ShipmentReceiptDTO;
import id.atiila.service.mapper.InventoryItemMapper;
import id.atiila.service.mapper.MovingSlipMapper;
import id.atiila.service.mapper.ShipmentReceiptMapper;
import id.atiila.web.rest.errors.InternalServerErrorException;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing MovingSlip.
 * BeSmart Team
 */

@Service
@Transactional
public class MovingSlipService {

    private final Logger log = LoggerFactory.getLogger(MovingSlipService.class);

    private final MovingSlipRepository movingSlipRepository;

    private final MovingSlipMapper movingSlipMapper;

    private final MovingSlipSearchRepository movingSlipSearchRepository;

    @Autowired
    private InventoryItemService inventoryItemService;

    @Autowired
    private ShipmentReceiptService shipmentReceiptService;

    @Autowired
    private ShipmentReceiptMapper shipmentReceiptMapper;

    @Autowired
    private InventoryItemMapper inventoryItemMapper;

    @Autowired
    private InventoryItemRepository inventoryItemRepository;

    @Autowired
    private MasterNumberingService masterNumberingService;

    @Autowired
    private ProducerTemplate camelTemplate;

    public MovingSlipService(MovingSlipRepository movingSlipRepository, MovingSlipMapper movingSlipMapper, MovingSlipSearchRepository movingSlipSearchRepository) {
        this.movingSlipRepository = movingSlipRepository;
        this.movingSlipMapper = movingSlipMapper;
        this.movingSlipSearchRepository = movingSlipSearchRepository;
    }

    public void checkQtySize(Set<MovingSlipDTO> list){
        if (!list.isEmpty()){
            for(MovingSlipDTO a : list){
                if (a.getQty() > a.getQtyAwal() ){
                    throw new DmsException("Qty yang dipindahkan lebih besar daripada qty awal");
                }
            }
        }
    }

    /**
     * Save a movingSlip.
     *
     * @param movingSlipDTO the entity to save
     * @return the persisted entity
     */
    public MovingSlipDTO save(MovingSlipDTO movingSlipDTO) {
        log.debug("Request to save MovingSlip : {}", movingSlipDTO);
        MovingSlip movingSlip = movingSlipMapper.toEntity(movingSlipDTO);
        movingSlip = movingSlipRepository.save(movingSlip);
        MovingSlipDTO result = movingSlipMapper.toDto(movingSlip);
        movingSlipSearchRepository.save(movingSlip);
        return result;
    }

    /**
     *  Get all the movingSlips.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MovingSlipDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MovingSlips");
        return movingSlipRepository.findActiveMovingSlip(pageable)
            .map(movingSlipMapper::toDto);
    }

    /**
     *  Get one movingSlip by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public MovingSlipDTO findOne(UUID id) {
        log.debug("Request to get MovingSlip : {}", id);
        MovingSlip movingSlip = movingSlipRepository.findOne(id);
        return movingSlipMapper.toDto(movingSlip);
    }

    /**
     *  Delete the  movingSlip by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete MovingSlip : {}", id);
        movingSlipRepository.delete(id);
        movingSlipSearchRepository.delete(id);
    }

    /**
     * Search for the movingSlip corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MovingSlipDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MovingSlips for query {}", query);
        Page<MovingSlip> result = movingSlipSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(movingSlipMapper::toDto);
    }

    public MovingSlipDTO processExecuteData(Integer id, String param, MovingSlipDTO dto) {
        MovingSlipDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void saveBulk(Set<MovingSlipDTO> dtos){
        Map<String, Object> headers = new HashMap<>();

        camelTemplate.sendBodyAndHeader(DemandSupplyRoute.ACTION_BUILD_MOVING_SLIP, dtos, "", headers);
    }

    public Set<MovingSlipDTO> processExecuteListData(Integer id, String param, Set<MovingSlipDTO> dto) {
        Set<MovingSlipDTO> r = dto;
        if (r != null) {
            switch (id) {
                case MovingSlipConstants.SAVE_BULK:
                    this.checkQtySize(r);
                    this.saveBulk(r);
                    break;
                default:
                    break;
            }
        }
        return r;
    }

    public MovingSlipDTO changeMovingSlipStatus(MovingSlipDTO dto, Integer id) {
        if (dto != null) {
			MovingSlip e = movingSlipRepository.findOne(dto.getIdSlip());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        movingSlipSearchRepository.delete(dto.getIdSlip());
                        break;
                    default:
                        movingSlipSearchRepository.save(e);
                }
				movingSlipRepository.save(e);
			}
		}
        return dto;
    }
}
