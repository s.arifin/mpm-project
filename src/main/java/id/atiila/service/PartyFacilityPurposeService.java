package id.atiila.service;

import id.atiila.domain.PartyFacilityPurpose;
import id.atiila.repository.PartyFacilityPurposeRepository;
import id.atiila.repository.search.PartyFacilityPurposeSearchRepository;
import id.atiila.service.dto.PartyFacilityPurposeDTO;
import id.atiila.service.mapper.PartyFacilityPurposeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing PartyFacilityPurpose.
 * BeSmart Team
 */

@Service
@Transactional
public class PartyFacilityPurposeService {

    private final Logger log = LoggerFactory.getLogger(PartyFacilityPurposeService.class);

    private final PartyFacilityPurposeRepository partyFacilityPurposeRepository;

    private final PartyFacilityPurposeMapper partyFacilityPurposeMapper;

    private final PartyFacilityPurposeSearchRepository partyFacilityPurposeSearchRepository;

    public PartyFacilityPurposeService(PartyFacilityPurposeRepository partyFacilityPurposeRepository, PartyFacilityPurposeMapper partyFacilityPurposeMapper, PartyFacilityPurposeSearchRepository partyFacilityPurposeSearchRepository) {
        this.partyFacilityPurposeRepository = partyFacilityPurposeRepository;
        this.partyFacilityPurposeMapper = partyFacilityPurposeMapper;
        this.partyFacilityPurposeSearchRepository = partyFacilityPurposeSearchRepository;
    }

    /**
     * Save a partyFacilityPurpose.
     *
     * @param partyFacilityPurposeDTO the entity to save
     * @return the persisted entity
     */
    public PartyFacilityPurposeDTO save(PartyFacilityPurposeDTO partyFacilityPurposeDTO) {
        log.debug("Request to save PartyFacilityPurpose : {}", partyFacilityPurposeDTO);
        PartyFacilityPurpose partyFacilityPurpose = partyFacilityPurposeMapper.toEntity(partyFacilityPurposeDTO);
        partyFacilityPurpose = partyFacilityPurposeRepository.save(partyFacilityPurpose);
        PartyFacilityPurposeDTO result = partyFacilityPurposeMapper.toDto(partyFacilityPurpose);
        partyFacilityPurposeSearchRepository.save(partyFacilityPurpose);
        return result;
    }

    /**
     * Get all the partyFacilityPurposes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartyFacilityPurposeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PartyFacilityPurposes");
        return partyFacilityPurposeRepository.findAll(pageable)
            .map(partyFacilityPurposeMapper::toDto);
    }

    /**
     * Get one partyFacilityPurpose by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PartyFacilityPurposeDTO findOne(UUID id) {
        log.debug("Request to get PartyFacilityPurpose : {}", id);
        PartyFacilityPurpose partyFacilityPurpose = partyFacilityPurposeRepository.findOne(id);
        return partyFacilityPurposeMapper.toDto(partyFacilityPurpose);
    }

    /**
     * Delete the partyFacilityPurpose by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete PartyFacilityPurpose : {}", id);
        partyFacilityPurposeRepository.delete(id);
        partyFacilityPurposeSearchRepository.delete(id);
    }

    /**
     * Search for the partyFacilityPurpose corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartyFacilityPurposeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of PartyFacilityPurposes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idOrganization = request.getParameter("idOrganization");
        String idFacility = request.getParameter("idFacility");
        String idPurposeType = request.getParameter("idPurposeType");

        if (idOrganization != null) {
            q.withQuery(matchQuery("organization.idParty", idOrganization));
        }
        else if (idFacility != null) {
            q.withQuery(matchQuery("facility.idFacility", idFacility));
        }
        else if (idPurposeType != null) {
            q.withQuery(matchQuery("purposeType.idPurposeType", idPurposeType));
        }

        Page<PartyFacilityPurpose> result = partyFacilityPurposeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(partyFacilityPurposeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PartyFacilityPurposeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered PartyFacilityPurposeDTO");
        String idOrganization = request.getParameter("idOrganization");
        String idFacility = request.getParameter("idFacility");
        String idPurposeType = request.getParameter("idPurposeType");

        if (idOrganization != null) {
            return partyFacilityPurposeRepository.findByIdOrganization(UUID.fromString(idOrganization), pageable).map(partyFacilityPurposeMapper::toDto); 
        }
        else if (idFacility != null) {
            return partyFacilityPurposeRepository.findByIdFacility(UUID.fromString(idFacility), pageable).map(partyFacilityPurposeMapper::toDto); 
        }
        else if (idPurposeType != null) {
            return partyFacilityPurposeRepository.findByIdPurposeType(Integer.valueOf(idPurposeType), pageable).map(partyFacilityPurposeMapper::toDto); 
        }

        return partyFacilityPurposeRepository.findAll(pageable).map(partyFacilityPurposeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, PartyFacilityPurposeDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<PartyFacilityPurposeDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
