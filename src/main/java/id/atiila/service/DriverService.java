package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.Driver;
import id.atiila.domain.Person;
import id.atiila.repository.DriverRepository;
import id.atiila.repository.search.DriverSearchRepository;
import id.atiila.service.dto.DriverDTO;
import id.atiila.service.dto.SalesmanDTO;
import id.atiila.service.mapper.DriverMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Driver.
 */
@Service
@Transactional
public class DriverService {

    private final Logger log = LoggerFactory.getLogger(DriverService.class);

    private final DriverRepository driverRepository;

    private final DriverMapper driverMapper;

    private final DriverSearchRepository driverSearchRepository;

    @Autowired
    private PersonService personSvc;

    @Autowired
    private MasterNumberingService numberSvc;

    public DriverService(DriverRepository driverRepository, DriverMapper driverMapper, DriverSearchRepository driverSearchRepository) {
        this.driverRepository = driverRepository;
        this.driverMapper = driverMapper;
        this.driverSearchRepository = driverSearchRepository;
    }

    /**
     * Save a driver.
     *
     * @param driverDTO the entity to save
     * @return the persisted entity
     */
    public DriverDTO save(DriverDTO driverDTO) {
        log.debug("Request to save Driver : {}", driverDTO);
        Driver driver = driverMapper.toEntity(driverDTO);
        Boolean isNewData = driver.getIdPartyRole() == null;

        if (isNewData) {
            Driver drv = null;

            //Cek Jika Konsumen sudah mempunyai ID
            if (driverDTO.getIdDriver() != null) {
                drv = driverRepository.findOneByIdDriver(driverDTO.getIdDriver());
                if (drv != null) return driverMapper.toDto(drv);
            }

            //Check Person, apakah sudah ada di database
            Person p = personSvc.getWhenExists(driverDTO.getPerson());

            if (p != null) {
                drv = driverRepository.findOneByParty(p);
                if (drv != null) return driverMapper.toDto(drv);
                driver.setPerson(p);
            };

            //Tetap menggunakan person default
            // driver.setStatus(BaseConstants.STATUS_ACTIVE);

            if (driverDTO.getIdDriver() == null) {
                String idDriver = null;
                while (idDriver == null || driverRepository.findOneByIdDriver(idDriver) != null) {
                    idDriver = numberSvc.nextValue("iddriver", 4000l).toString();
                }
                driver.idDriver(idDriver);
            }
            else
                driver.setIdDriver(driverDTO.getIdDriver());
        }

        driver = driverRepository.save(driver);
        DriverDTO result = driverMapper.toDto(driver);
        driverSearchRepository.save(driver);
        return result;
    }

    /**
     *  Get all the drivers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DriverDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Drivers");
        return driverRepository.findAll(pageable)
            .map(driverMapper::toDto);
    }

    /**
     *  Get one driver by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public DriverDTO findOne(UUID id) {
        log.debug("Request to get Driver : {}", id);
        Driver driver = driverRepository.findOne(id);
        return driverMapper.toDto(driver);
    }

    /**
     *  Delete the  driver by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Driver : {}", id);
        driverRepository.delete(id);
        driverSearchRepository.delete(id);
    }

    /**
     * Search for the driver corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DriverDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Drivers for query {}", query);
        Page<Driver> result = driverSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(driverMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<DriverDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        String idInternal = request.getParameter("idInternal");
        Integer idPositionType = request.getParameter("idPositionType") == null ?
            null : Integer.parseInt(request.getParameter("idPositionType"));

        log.debug("Request to get all Driver");
        return driverRepository.findAllByInternal(idInternal, idPositionType, pageable)
            .map(driverMapper::toDto);
    }
}
