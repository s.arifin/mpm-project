package id.atiila.service;

import id.atiila.domain.PaymentType;
import id.atiila.repository.PaymentTypeRepository;
import id.atiila.repository.search.PaymentTypeSearchRepository;
import id.atiila.service.dto.PaymentTypeDTO;
import id.atiila.service.mapper.PaymentTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing PaymentType.
 * BeSmart Team
 */

@Service
@Transactional
public class PaymentTypeService {

    private final Logger log = LoggerFactory.getLogger(PaymentTypeService.class);

    private final PaymentTypeRepository paymentTypeRepository;

    private final PaymentTypeMapper paymentTypeMapper;

    private final PaymentTypeSearchRepository paymentTypeSearchRepository;

    public PaymentTypeService(PaymentTypeRepository paymentTypeRepository, PaymentTypeMapper paymentTypeMapper, PaymentTypeSearchRepository paymentTypeSearchRepository) {
        this.paymentTypeRepository = paymentTypeRepository;
        this.paymentTypeMapper = paymentTypeMapper;
        this.paymentTypeSearchRepository = paymentTypeSearchRepository;
    }

    /**
     * Save a paymentType.
     *
     * @param paymentTypeDTO the entity to save
     * @return the persisted entity
     */
    public PaymentTypeDTO save(PaymentTypeDTO paymentTypeDTO) {
        log.debug("Request to save PaymentType : {}", paymentTypeDTO);
        PaymentType paymentType = paymentTypeMapper.toEntity(paymentTypeDTO);
        paymentType = paymentTypeRepository.save(paymentType);
        PaymentTypeDTO result = paymentTypeMapper.toDto(paymentType);
        paymentTypeSearchRepository.save(paymentType);
        return result;
    }

    /**
     * Get all the paymentTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PaymentTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PaymentTypes");
        return paymentTypeRepository.findAll(pageable)
            .map(paymentTypeMapper::toDto);
    }

    /**
     * Get one paymentType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PaymentTypeDTO findOne(Integer id) {
        log.debug("Request to get PaymentType : {}", id);
        PaymentType paymentType = paymentTypeRepository.findOne(id);
        return paymentTypeMapper.toDto(paymentType);
    }

    /**
     * Delete the paymentType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete PaymentType : {}", id);
        paymentTypeRepository.delete(id);
        paymentTypeSearchRepository.delete(id);
    }

    /**
     * Search for the paymentType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PaymentTypeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of PaymentTypes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));


        Page<PaymentType> result = paymentTypeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(paymentTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PaymentTypeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered PaymentTypeDTO");
        String idPaytype = request.getParameter("idPaytype");

        if( idPaytype != null) {
            return  paymentTypeRepository.findByIdPaymentType(Integer.valueOf(idPaytype), pageable).map(paymentTypeMapper::toDto);
        }



        return paymentTypeRepository.findAll(pageable).map(paymentTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, PaymentTypeDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<PaymentTypeDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        paymentTypeSearchRepository.deleteAll();
        List<PaymentType> paymentTypes =  paymentTypeRepository.findAll();
        for (PaymentType m: paymentTypes) {
            paymentTypeSearchRepository.save(m);
            log.debug("Data paymentType save !...");
        }
    }
}
