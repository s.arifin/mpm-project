package id.atiila.service;

import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class VehicleDocumentServices {

    private final Logger log = LoggerFactory.getLogger(VehicleDocumentServices.class);

    @Autowired
    private PersonalCustomerRepository repoCustomer;

    @Autowired
    private VehicleDocumentRequirementRepository repoDocument;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private VehicleRepository repoVehicle;

    @Autowired
    private BillToRepository billToRepository;

    @Autowired
    private SaleTypeRepository saleTypeRepository;

    @Autowired
    private VehicleSalesOrderRepository vsoRepo;

    @Autowired
    private SalesUnitRequirementRepository salesUnitRequirementRepository;

    public VehicleDocumentRequirement buildDocument(String idInternal, String idRequestor, String reffDoc, UUID idPerson) {
        return null;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public VehicleDocumentRequirement buildVehicleDocument(Internal intr, OrderItem o) {
        VehicleDocumentRequirement vd = repoDocument.findByOrderItem(o);
        VehicleSalesOrder vso = null;

        if (vd == null) {
            if (o.getOrders() == null) {
                return null;
            }
            if (o.getOrders() instanceof VehicleSalesOrder) {
                vso = (VehicleSalesOrder) o.getOrders();
            }
            else return null;

            log.debug("vds:find vso " + vso);
            vso = vsoRepo.findOne(vso.getIdOrder());
            log.debug("vds:find vso repo " + vso);
            SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(vso.getIdSalesUnitRequirement());
            ShipTo shipTo = partyUtils.getShipTo(o.getIdShipTo());
            BillTo billTo = partyUtils.getBillTo(intr.getOrganization());
            Vehicle vh = getVehicle(o);

            log.debug("vds:find sale type {}" , vso.getIdSaleType());
            // SaleType saleType = saleTypeRepository.findOne(vso.getIdSaleType());
            // log.debug("vds:find sale type {} finish " , saleType);
            SaleType saleType = saleTypeRepository.findOne(sur.getSaleType().getIdSaleType());

            vd = new VehicleDocumentRequirement();
            log.debug("vds:order iteme {}" , o);
            vd.setOrderItem(o);
            // Assign internal (Owner Data)

            //assign waitstnk
            vd.setWaitStnk(sur.getWaitStnk());

            log.debug("vds: internal {}" , vso.getInternal());
            vd.setInternal(vso.getInternal());
            // Assign Bill To

            log.debug("vds: billTo {}" , billTo);
            vd.setBillTo(billTo);
            // Assign Ship To

            log.debug("vds: shipTo {}" , shipTo);
            vd.setShipTo(shipTo);
            // Assign Person Owner


            log.debug("vds: person {}" , shipTo.getParty());
            vd.setPersonOwner(sur.getPersonOwner());
            // Assign Vehicle

            log.debug("vds: vh {}" , vh);
            vd.setVehicle(vh);
            //
            // log.debug("vds: saleType {}" , saleType);
            vd.setSaleType(saleType);
            //
            log.debug("vds: getRequestpoliceid {}" , sur.getRequestpoliceid());
            vd.setRequestPoliceNumber(sur.getRequestpoliceid());

            log.debug("vds: getNote {}" , sur.getNote());
            vd.setNote(sur.getNote());

            //vd.setBbn(sur.get);
            log.debug("vds: save {}" , vd);
            repoDocument.save(vd);
        }
        return vd;
    }

    public Vehicle findByIdMachine(String id) {
        List<Vehicle> vehicles = repoVehicle.findByIdMachine(id);
        return vehicles.size() > 0 ? vehicles.get(0) : null;
    }

    public Vehicle findByIdFrame(String id) {
        List<Vehicle> vehicles = repoVehicle.findOnebyidFrame(id);
        return vehicles.size() > 0 ? vehicles.get(0) : null;
    }

    @Transactional
    protected Vehicle getVehicle(String noSin, String noKa, String idProduct, Integer color, Integer yearAss) {
        log.debug("nosin {} noka {} id prod{} color {} year ass {}", noSin, noKa, idProduct, color, yearAss);
        Vehicle r = findByIdMachine(noSin);
        if (r == null) {
            r = new Vehicle();
            r.setIdFrame(noKa);
            r.setIdMachine(noSin);
            r.setYearOfAssembly(yearAss);
            r.setIdColor(color);
            r.setIdProduct(idProduct);
            // r = repoVehicle.save(r);
        }
        return r;
    }

    @Transactional
    protected Vehicle getVehicle(OrderItem o) {
        BillingItem p = o.getBillingItems().iterator().next().getBillingItem();
        // BillingItem p = this.findBillingItemByOrder(o);
        log.debug("vds:get vehicle , biling item = " + p);
        if (p == null)
            throw new DmsException("Belum di buatkan Faktur, proses di batalkan !");
        InventoryItem invitem = p.getInventoryItem();
        log.debug("vds:get vehicle , inv item = " + invitem);
        Vehicle r = getVehicle(invitem.getIdMachine(), invitem.getIdFrame(), invitem.getIdProduct(), invitem.getIdFeature(), invitem.getYearAssembly());
        log.debug("vds:get vehicle finish {} ",r );
        return r;
    }

    BillingItem findBillingItemByOrder(OrderItem oi){
        return repoDocument.findBillingContainItem(oi.getIdOrderItem()).iterator().next();
    }

}
