package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the OrderRequestItem entity.
 * atiila consulting
 */

public class OrderRequestItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idOrderRequestItem;

    private Double qty;

    private Double qtyDelivered;

    private UUID orderItemId;

    private UUID requestItemId;

    public UUID getIdOrderRequestItem() {
        return this.idOrderRequestItem;
    }

    public void setIdOrderRequestItem(UUID id) {
        this.idOrderRequestItem = id;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Double getQtyDelivered() {
        return qtyDelivered;
    }

    public void setQtyDelivered(Double qtyDelivered) {
        this.qtyDelivered = qtyDelivered;
    }

    public UUID getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(UUID orderItemId) {
        this.orderItemId = orderItemId;
    }

    public UUID getRequestItemId() {
        return requestItemId;
    }

    public void setRequestItemId(UUID requestItemId) {
        this.requestItemId = requestItemId;
    }

    public UUID getId() {
        return this.idOrderRequestItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderRequestItemDTO orderRequestItemDTO = (OrderRequestItemDTO) o;
        if (orderRequestItemDTO.getIdOrderRequestItem() == null || getIdOrderRequestItem() == null) {
            return false;
        }
        return Objects.equals(getIdOrderRequestItem(), orderRequestItemDTO.getIdOrderRequestItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdOrderRequestItem());
    }

    @Override
    public String toString() {
        return "OrderRequestItemDTO{" +
            "id=" + getIdOrderRequestItem() +
            ", qty='" + getQty() + "'" +
            ", qtyDelivered='" + getQtyDelivered() + "'" +
            "}";
    }
}
