package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the ParentOrganization entity.
 * BeSmart Team
 */

public class ParentOrganizationDTO extends InternalDTO {

    private static final long serialVersionUID = 1L;

    public ParentOrganizationDTO() {
    }

    public ParentOrganizationDTO(String idInternal, String name) {
        super(idInternal, name);
    }

    @Override
    public String toString() {
        return "ParentOrganizationDTO{" +
            "idInternal='" + getIdInternal() + "'" +
            "}";
    }
}
