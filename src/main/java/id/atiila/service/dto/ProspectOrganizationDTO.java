package id.atiila.service.dto;

import java.io.Serializable;
import java.text.DateFormat;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the ProspectOrganization entity.
 * BeSmart Team
 */

public class ProspectOrganizationDTO extends ProspectDTO {

    private static final long serialVersionUID = 1L;

    private OrganizationDTO organization = new OrganizationDTO();

    private PersonDTO pic = new PersonDTO();

    private PostalAddressDTO addressTDP = new PostalAddressDTO();

    public OrganizationDTO getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationDTO organization) {
        this.organization = organization;
    }

    public PersonDTO getPic() {
        return pic;
    }

    public void setPic(PersonDTO pic) {
        this.pic = pic;
    }

    public PostalAddressDTO getAddressTDP() {
        return addressTDP;
    }

    public void setAddressTDP(PostalAddressDTO addressTDP) {
        this.addressTDP = addressTDP;
    }

    @Override
    public String toString() {
        return "ProspectOrganizationDTO{" +
            "id=" + getIdProspect() + "'" +
            "}";
    }
}
