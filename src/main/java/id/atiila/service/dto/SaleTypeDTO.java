package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the SaleType entity.
 * BeSmart Team
 */

public class SaleTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idSaleType;

    private String description;

    private Integer parentId;

    private String parentDescription;

    public Integer getIdSaleType() {
        return this.idSaleType;
    }

    public void setIdSaleType(Integer id) {
        this.idSaleType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer saleTypeId) {
        this.parentId = saleTypeId;
    }

    public String getParentDescription() {
        return parentDescription;
    }

    public void setParentDescription(String saleTypeDescription) {
        this.parentDescription = saleTypeDescription;
    }
    
    public Integer getId() {
        return this.idSaleType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SaleTypeDTO saleTypeDTO = (SaleTypeDTO) o;
        if (saleTypeDTO.getIdSaleType() == null || getIdSaleType() == null) {
            return false;
        }
        return Objects.equals(getIdSaleType(), saleTypeDTO.getIdSaleType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdSaleType());
    }

    @Override
    public String toString() {
        return "SaleTypeDTO{" +
            "id=" + getIdSaleType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
