package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the DimensionType entity.
 * atiila consulting
 */

public class DimensionTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idDimensionType;

    private String description;

    public Integer getIdDimensionType() {
        return this.idDimensionType;
    }

    public void setIdDimensionType(Integer id) {
        this.idDimensionType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.idDimensionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DimensionTypeDTO dimensionTypeDTO = (DimensionTypeDTO) o;
        if (dimensionTypeDTO.getIdDimensionType() == null || getIdDimensionType() == null) {
            return false;
        }
        return Objects.equals(getIdDimensionType(), dimensionTypeDTO.getIdDimensionType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdDimensionType());
    }

    @Override
    public String toString() {
        return "DimensionTypeDTO{" +
            "id=" + getIdDimensionType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
