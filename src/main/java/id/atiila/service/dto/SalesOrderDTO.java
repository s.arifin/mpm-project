package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the SalesOrder entity.
 * BeSmart Team
 */

public class SalesOrderDTO extends CustomerOrderDTO {

    private static final long serialVersionUID = 1L;

    private Integer idSaleType;

    private Integer saleTypeId;

    private String saleTypeDescription;

    public Integer getIdSaleType() {
        return idSaleType;
    }

    public void setIdSaleType(Integer idSaleType) {
        this.idSaleType = idSaleType;
    }

    public Integer getSaleTypeId() {
        return saleTypeId;
    }

    public void setSaleTypeId(Integer saleTypeId) {
        this.saleTypeId = saleTypeId;
    }

    public String getSaleTypeDescription() {
        return saleTypeDescription;
    }

    public void setSaleTypeDescription(String saleTypeDescription) {
        this.saleTypeDescription = saleTypeDescription;
    }

    @Override
    public String toString() {
        return "SalesOrderDTO{" +
            "id=" + getIdOrder() +
            "}";
    }
}
