package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the BrokerType entity.
 * BeSmart Team
 */

public class BrokerTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idBrokerType;

    private String description;

    public Integer getIdBrokerType() {
        return this.idBrokerType;
    }

    public void setIdBrokerType(Integer id) {
        this.idBrokerType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idBrokerType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BrokerTypeDTO brokerTypeDTO = (BrokerTypeDTO) o;
        if (brokerTypeDTO.getIdBrokerType() == null || getIdBrokerType() == null) {
            return false;
        }
        return Objects.equals(getIdBrokerType(), brokerTypeDTO.getIdBrokerType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdBrokerType());
    }

    @Override
    public String toString() {
        return "BrokerTypeDTO{" +
            "id=" + getIdBrokerType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
