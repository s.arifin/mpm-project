package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the Disbursement entity.
 * BeSmart Team
 */

public class DisbursementDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idPayment;

    private String paymentNumber;

    private String refferenceNumber;

    private ZonedDateTime dateCreate;

    private BigDecimal amount;

    private Integer paymentTypeId;

    private Integer methodId;

    private String internalId;

    private String paidToId;

    private String paidFromId;

    public UUID getIdPayment() {
        return this.idPayment;
    }

    public void setIdPayment(UUID id) {
        this.idPayment = id;
    }

    public String getPaymentNumber() {
        return paymentNumber;
    }

    public void setPaymentNumber(String paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public String getRefferenceNumber() {
        return refferenceNumber;
    }

    public void setRefferenceNumber(String refferenceNumber) {
        this.refferenceNumber = refferenceNumber;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(Integer paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public Integer getMethodId() {
        return methodId;
    }

    public void setMethodId(Integer paymentMethodId) {
        this.methodId = paymentMethodId;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getPaidToId() {
        return paidToId;
    }

    public void setPaidToId(String billToId) {
        this.paidToId = billToId;
    }

    public String getPaidFromId() {
        return paidFromId;
    }

    public void setPaidFromId(String billToId) {
        this.paidFromId = billToId;
    }

    public UUID getId() {
        return this.idPayment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DisbursementDTO disbursementDTO = (DisbursementDTO) o;
        if (disbursementDTO.getIdPayment() == null || getIdPayment() == null) {
            return false;
        }
        return Objects.equals(getIdPayment(), disbursementDTO.getIdPayment());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPayment());
    }

    @Override
    public String toString() {
        return "DisbursementDTO{" +
            "id=" + getIdPayment() +
            ", paymentNumber='" + getPaymentNumber() + "'" +
            ", refferenceNumber='" + getRefferenceNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", amount='" + getAmount() + "'" +
            "}";
    }
}
