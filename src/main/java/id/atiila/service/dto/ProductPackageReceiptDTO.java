package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the ProductPackageReceipt entity.
 * BeSmart Team
 */

public class ProductPackageReceiptDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idPackage;

    private ZonedDateTime dateCreated;

    private String documentNumber;

    private String internalId;

    private String internalName;

    private String shipFromId;

    private String shipFromName;

    public UUID getIdPackage() {
        return this.idPackage;
    }

    public void setIdPackage(UUID id) {
        this.idPackage = id;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getShipFromId() {
        return shipFromId;
    }

    public void setShipFromId(String shipToId) {
        this.shipFromId = shipToId;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public String getShipFromName() {
        return shipFromName;
    }

    public void setShipFromName(String shipFromName) {
        this.shipFromName = shipFromName;
    }

    public UUID getId() {
        return this.idPackage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductPackageReceiptDTO productPackageReceiptDTO = (ProductPackageReceiptDTO) o;
        if (productPackageReceiptDTO.getIdPackage() == null || getIdPackage() == null) {
            return false;
        }
        return Objects.equals(getIdPackage(), productPackageReceiptDTO.getIdPackage());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPackage());
    }

    @Override
    public String toString() {
        return "ProductPackageReceiptDTO{" +
            "id=" + getIdPackage() +
            ", dateCreated='" + getDateCreated() + "'" +
            ", documentNumber='" + getDocumentNumber() + "'" +
            "}";
    }
}
