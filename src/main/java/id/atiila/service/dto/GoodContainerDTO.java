package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the GoodContainer entity.
 * BeSmart Team
 */

public class GoodContainerDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idGoodContainer;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private String goodId;

    private String goodName;

    private UUID containerId;

    private String containerContainerCode;

    private UUID organizationId;

    public UUID getIdGoodContainer() {
        return this.idGoodContainer;
    }

    public void setIdGoodContainer(UUID id) {
        this.idGoodContainer = id;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public UUID getContainerId() {
        return containerId;
    }

    public void setContainerId(UUID containerId) {
        this.containerId = containerId;
    }

    public String getContainerContainerCode() {
        return containerContainerCode;
    }

    public void setContainerContainerCode(String containerContainerCode) {
        this.containerContainerCode = containerContainerCode;
    }

    public UUID getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(UUID organizationId) {
        this.organizationId = organizationId;
    }

    public UUID getId() {
        return this.idGoodContainer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GoodContainerDTO goodContainerDTO = (GoodContainerDTO) o;
        if (goodContainerDTO.getIdGoodContainer() == null || getIdGoodContainer() == null) {
            return false;
        }
        return Objects.equals(getIdGoodContainer(), goodContainerDTO.getIdGoodContainer());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdGoodContainer());
    }

    @Override
    public String toString() {
        return "GoodContainerDTO{" +
            "id=" + getIdGoodContainer() +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
