package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the ShipmentBillingItem entity.
 * BeSmart Team
 */

public class ShipmentBillingItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idShipmentBillingItem;

    private Integer qty;

    private UUID shipmentItemId;

    private UUID billingItemId;

    public UUID getIdShipmentBillingItem() {
        return this.idShipmentBillingItem;
    }

    public void setIdShipmentBillingItem(UUID id) {
        this.idShipmentBillingItem = id;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public UUID getShipmentItemId() {
        return shipmentItemId;
    }

    public void setShipmentItemId(UUID shipmentItemId) {
        this.shipmentItemId = shipmentItemId;
    }

    public UUID getBillingItemId() {
        return billingItemId;
    }

    public void setBillingItemId(UUID billingItemId) {
        this.billingItemId = billingItemId;
    }

    public UUID getId() {
        return this.idShipmentBillingItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShipmentBillingItemDTO shipmentBillingItemDTO = (ShipmentBillingItemDTO) o;
        if (shipmentBillingItemDTO.getIdShipmentBillingItem() == null || getIdShipmentBillingItem() == null) {
            return false;
        }
        return Objects.equals(getIdShipmentBillingItem(), shipmentBillingItemDTO.getIdShipmentBillingItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdShipmentBillingItem());
    }

    @Override
    public String toString() {
        return "ShipmentBillingItemDTO{" +
            "id=" + getIdShipmentBillingItem() +
            ", qty='" + getQty() + "'" +
            "}";
    }
}
