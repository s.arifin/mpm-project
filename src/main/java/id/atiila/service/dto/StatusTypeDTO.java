package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the StatusType entity.
 * BeSmart Team
 */

public class StatusTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idStatusType;

    private String description;

    public StatusTypeDTO() {
    }

    public StatusTypeDTO(Integer idStatusType, String description) {
        this.idStatusType = idStatusType;
        this.description = description;
    }

    public Integer getIdStatusType() {
        return this.idStatusType;
    }

    public void setIdStatusType(Integer id) {
        this.idStatusType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idStatusType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StatusTypeDTO statusTypeDTO = (StatusTypeDTO) o;
        if (statusTypeDTO.getIdStatusType() == null || getIdStatusType() == null) {
            return false;
        }
        return Objects.equals(getIdStatusType(), statusTypeDTO.getIdStatusType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdStatusType());
    }

    @Override
    public String toString() {
        return "StatusTypeDTO{" +
            "id=" + getIdStatusType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
