package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the Facility entity.
 * BeSmart Team
 */

public class FacilityDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idFacility;

    private String facilityCode;

    private String description;

    private Integer currentStatus;

    private Integer facilityTypeId;

    private String facilityTypeDescription;

    private UUID partOfId;

    private String partOfDescription;

    public UUID getIdFacility() {
        return this.idFacility;
    }

    public void setIdFacility(UUID id) {
        this.idFacility = id;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getFacilityTypeId() {
        return facilityTypeId;
    }

    public void setFacilityTypeId(Integer facilityTypeId) {
        this.facilityTypeId = facilityTypeId;
    }

    public String getFacilityTypeDescription() {
        return facilityTypeDescription;
    }

    public void setFacilityTypeDescription(String facilityTypeDescription) {
        this.facilityTypeDescription = facilityTypeDescription;
    }

    public UUID getPartOfId() {
        return partOfId;
    }

    public void setPartOfId(UUID facilityId) {
        this.partOfId = facilityId;
    }

    public String getPartOfDescription() {
        return partOfDescription;
    }

    public void setPartOfDescription(String facilityDescription) {
        this.partOfDescription = facilityDescription;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public UUID getId() {
        return this.idFacility;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FacilityDTO facilityDTO = (FacilityDTO) o;
        if (facilityDTO.getIdFacility() == null || getIdFacility() == null) {
            return false;
        }
        return Objects.equals(getIdFacility(), facilityDTO.getIdFacility());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdFacility());
    }

    @Override
    public String toString() {
        return "FacilityDTO{" +
            "id=" + getIdFacility() +
            ", facilityCode='" + getFacilityCode() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
