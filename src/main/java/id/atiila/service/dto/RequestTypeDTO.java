package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the RequestType entity.
 * BeSmart Team
 */

public class RequestTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idRequestType;

    private String description;

    public RequestTypeDTO() {
    }

    public RequestTypeDTO(Integer idRequestType, String description) {
        this.idRequestType = idRequestType;
        this.description = description;
    }

    public Integer getIdRequestType() {
        return this.idRequestType;
    }

    public void setIdRequestType(Integer id) {
        this.idRequestType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.idRequestType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequestTypeDTO requestTypeDTO = (RequestTypeDTO) o;
        if (requestTypeDTO.getIdRequestType() == null || getIdRequestType() == null) {
            return false;
        }
        return Objects.equals(getIdRequestType(), requestTypeDTO.getIdRequestType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRequestType());
    }

    @Override
    public String toString() {
        return "RequestTypeDTO{" +
            "id=" + getIdRequestType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
