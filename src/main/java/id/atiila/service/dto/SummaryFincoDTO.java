package id.atiila.service.dto;

import java.io.Serializable;
import java.util.UUID;

public class SummaryFincoDTO implements  Serializable {

    private String bastFinco;

    public SummaryFincoDTO(String bastfinco) { this.bastFinco = bastfinco; }

    public String getBastFinco() {return bastFinco; }

    public void setBastFinco (String bastFinco) {this.bastFinco = bastFinco; }
}
