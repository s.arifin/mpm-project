package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the PositionType entity.
 * BeSmart Team
 */

public class PositionTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idPositionType;

    private String description;

    private String title;

    public Integer getIdPositionType() {
        return this.idPositionType;
    }

    public void setIdPositionType(Integer id) {
        this.idPositionType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public Integer getId() {
        return this.idPositionType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PositionTypeDTO positionTypeDTO = (PositionTypeDTO) o;
        if (positionTypeDTO.getIdPositionType() == null || getIdPositionType() == null) {
            return false;
        }
        return Objects.equals(getIdPositionType(), positionTypeDTO.getIdPositionType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPositionType());
    }

    @Override
    public String toString() {
        return "PositionTypeDTO{" +
            "id=" + getIdPositionType() +
            ", description='" + getDescription() + "'" +
            ", title='" + getTitle() + "'" +
            "}";
    }
}
