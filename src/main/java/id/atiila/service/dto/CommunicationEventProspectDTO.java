package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the CommunicationEventProspect entity.
 * BeSmart Team
 */

public class CommunicationEventProspectDTO extends CommunicationEventDTO {

    private static final long serialVersionUID = 1L;

    private UUID idProspect;

    private Boolean interest;

    private Integer qty;

    private String purchasePlan;

    private Integer saleTypeId;

    private String saleTypeDescription;

    private Integer colorId;

    private String colorDescription;

    private String productId;

    private String connect;

    private String productName;

    private String nextFollowUp;

    private String note;

    public String getConnect() {
        return connect;
    }

    public void setConnect(String connect) {
        this.connect = connect;
    }

    public String getNextFollowUp() {
        return nextFollowUp;
    }

    public void setNextFollowUp(String nextFollowUp) {
        this.nextFollowUp = nextFollowUp;
    }

    @Override
    public String getNote() {
        return note;
    }

    @Override
    public void setNote(String note) {
        this.note = note;
    }

    private String walkInType;

    public String getWalkInType() {
        return walkInType;
    }

    public void setWalkInType(String walkInType) {
        this.walkInType = walkInType;
    }

    public UUID getIdProspect() { return idProspect; }

    public void setIdProspect(UUID idProspect) { this.idProspect = idProspect; }

    public Boolean getInterest() { return interest; }

    public void setInterest(Boolean interest) { this.interest = interest; }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) { this.qty = qty; }

    public String getPurchasePlan() { return purchasePlan; }

    public void setPurchasePlan(String purchasePlan) { this.purchasePlan = purchasePlan; }

    public Integer getSaleTypeId() {
        return saleTypeId;
    }

    public void setSaleTypeId(Integer saleTypeId) {
        this.saleTypeId = saleTypeId;
    }

    public String getSaleTypeDescription() {
        return saleTypeDescription;
    }

    public void setSaleTypeDescription(String saleTypeDescription) {
        this.saleTypeDescription = saleTypeDescription;
    }

    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer featureId) {
        this.colorId = featureId;
    }

    public String getColorDescription() {
        return colorDescription;
    }

    public void setColorDescription(String featureDescription) {
        this.colorDescription = featureDescription;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String motorId) {
        this.productId = motorId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String motorName) {
        this.productName = motorName;
    }

    @Override
    public String toString() {
        return "CommunicationEventProspectDTO{" +
            "id=" + getIdCommunicationEvent() +
            ", idprospect='" + getIdProspect() + "'" +
            ", interest='" + getInterest() + "'" +
            ", qty='" + getQty() + "'" +
            ", qty='" + getNextFollowUp() + "'" +
            ", qty='" + getNote() + "'" +
            "}";
    }
}
