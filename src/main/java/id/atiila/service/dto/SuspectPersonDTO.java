package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the SuspectPerson entity.
 * BeSmart Team
 */

public class SuspectPersonDTO extends SuspectDTO {

    private static final long serialVersionUID = 1L;

    private PersonDTO person = new PersonDTO();

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "SuspectPersonDTO{" +
            "id=" + getIdSuspect() +
            "}";
    }
}
