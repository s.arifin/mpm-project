package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AssociationType entity.
 * BeSmart Team
 */

public class AssociationTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idAssociattionType;

    private String description;

    public Integer getIdAssociattionType() {
        return this.idAssociattionType;
    }

    public void setIdAssociattionType(Integer id) {
        this.idAssociattionType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idAssociattionType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AssociationTypeDTO associationTypeDTO = (AssociationTypeDTO) o;
        if (associationTypeDTO.getIdAssociattionType() == null || getIdAssociattionType() == null) {
            return false;
        }
        return Objects.equals(getIdAssociattionType(), associationTypeDTO.getIdAssociattionType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdAssociattionType());
    }

    @Override
    public String toString() {
        return "AssociationTypeDTO{" +
            "id=" + getIdAssociattionType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
