package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the SalesSource entity.
 * BeSmart Team
 */

public class SalesSourceDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String description;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SalesSourceDTO salesSourceDTO = (SalesSourceDTO) o;
        if (salesSourceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), salesSourceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SalesSourceDTO{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
