package id.atiila.service.dto;

import id.atiila.domain.RequirementOrderItem;
import id.atiila.domain.SalesUnitRequirement;
import id.atiila.domain.ShipTo;
import id.atiila.domain.VehicleSalesOrder;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the OrderItem entity.
 * BeSmart Team
 */

@Document(indexName = "unitorderitem")
public class UnitOrderItemDTO extends OrderItemDTO {

    private static final long serialVersionUID = 1L;

    private String customerId;

    private String customerName;

    private String personOwnerName;

    private ZonedDateTime dateOrder;

    private String orderNumber;

    private String salesmanName;

    private String salesmanId;

    private Integer currentStatus;

    private Integer adminSalesVerifyValue;

    private Integer afcVerifyValue;

    private String idInternal;

    public String getIdInternal() {
        return idInternal;
    }

    public void setIdInternal(String idInternal) {
        this.idInternal = idInternal;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public ZonedDateTime getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(ZonedDateTime dateOrder) {
        this.dateOrder = dateOrder;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Integer getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public Integer getAdminSalesVerifyValue() {
        return adminSalesVerifyValue;
    }

    public void setAdminSalesVerifyValue(Integer adminSalesVerifyValue) {
        this.adminSalesVerifyValue = adminSalesVerifyValue;
    }

    public Integer getAfcVerifyValue() {
        return afcVerifyValue;
    }

    public void setAfcVerifyValue(Integer afcVerifyValue) {
        this.afcVerifyValue = afcVerifyValue;
    }

    public String getPersonOwnerName() {
        return personOwnerName;
    }

    public void setPersonOwnerName(String personOwnerName) {
        this.personOwnerName = personOwnerName;
    }

    public UnitOrderItemDTO setRequirementOrderItem(RequirementOrderItem roi, ShipTo shipTo) {
        SalesUnitRequirement sur = roi.getRequirement();
        VehicleSalesOrder vso = (VehicleSalesOrder) roi.getOrderItem().getOrders();

        this.adminSalesVerifyValue = vso.getAdminSalesVerifyValue();
        this.afcVerifyValue = vso.getAfcVerifyValue();

        if (shipTo != null) this.setShipToName(shipTo.getParty().getName());

        if (sur.getCustomer() != null) {
            this.customerId = sur.getCustomer().getIdCustomer();
            this.customerName = sur.getCustomer().getParty().getName();
        }

        if (sur.getPersonOwner() != null) {
            this.personOwnerName = sur.getPersonOwner().getName();
        }

        if (sur.getSalesman() != null) {
            this.salesmanId = sur.getSalesman().getIdSalesman();
            this.salesmanName = sur.getSalesman().getPerson().getName();
        }
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UnitOrderItemDTO orderItemDTO = (UnitOrderItemDTO) o;
        if (orderItemDTO.getIdOrderItem() == null || getIdOrderItem() == null) {
            return false;
        }
        return Objects.equals(getIdOrderItem(), orderItemDTO.getIdOrderItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdOrderItem());
    }

    @Override
    public String toString() {
        return "OrderItemDTO{" +
            "id=" + getIdOrderItem() +
            ", idProduct='" + getIdProduct() + "'" +
            ", itemDescription='" + getItemDescription() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", idShipTo='" + getIdShipTo() + "'" +
            ", qty='" + getQty() + "'" +
            ", unitPrice='" + getUnitPrice() + "'" +
            ", discount='" + getDiscount() + "'" +
            ", taxAmount='" + getTaxAmount() + "'" +
            ", totalAmount='" + getTotalAmount() + "'" +
            "}";
    }
}
