package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the RequestUnitInternal entity.
 * BeSmart Team
 */

public class RequestUnitInternalDTO extends RequestDTO {

    private static final long serialVersionUID = 1L;

    private UUID parentId;

    private String internalToId;

    public UUID getParentId() { return parentId; }

    public void setParentId(UUID parentId) { this.parentId = parentId; }

    public String getInternalToId() {
        return internalToId;
    }

    public void setInternalToId(String internalId) {
        this.internalToId = internalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequestUnitInternalDTO requestUnitInternalDTO = (RequestUnitInternalDTO) o;
        if (requestUnitInternalDTO.getIdRequest() == null || getIdRequest() == null) {
            return false;
        }
        return Objects.equals(getIdRequest(), requestUnitInternalDTO.getIdRequest());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRequest());
    }

    @Override
    public String toString() {
        return "RequestUnitInternalDTO{" +
            "id=" + getIdRequest() +
            "}";
    }
}
