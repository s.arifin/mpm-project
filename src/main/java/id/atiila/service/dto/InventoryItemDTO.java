package id.atiila.service.dto;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the InventoryItem entity.
 * BeSmart Team
 */

@Document(indexName = "inventoryitemdto")
public class InventoryItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private UUID idInventoryItem;

    private UUID idOwner;

    private UUID idFacility;

    private String facilityCode;

    private UUID idContainer;

    private String containerCode;

    private String idProduct;

    private String itemDescription;

    private Integer idFeature;

    private String featureCode;

    private String featureDescription;

    private ZonedDateTime dateCreated;

    private Double qty;

    private Double qtyBooking;

    private String idFrame;

    private String idMachine;

    private Integer currentStatus;

    private UUID shipmentReceiptId;

    private String shipmentReceiptDescription;

    private String shipmentReceiptCode;

    private ZonedDateTime shipmentReceiptDate;

    private Integer yearAssembly;

    //getter setter


    public Integer getYearAssembly() {
        return yearAssembly;
    }

    public void setYearAssembly(Integer yearAssembly) {
        this.yearAssembly = yearAssembly;
    }

    public ZonedDateTime getShipmentReceiptDate() {
        return shipmentReceiptDate;
    }

    public void setShipmentReceiptDate(ZonedDateTime shipmentReceiptDate) {
        this.shipmentReceiptDate = shipmentReceiptDate;
    }

    public String getShipmentReceiptCode() {
        return shipmentReceiptCode;
    }

    public void setShipmentReceiptCode(String shipmentReceiptCode) {
        this.shipmentReceiptCode = shipmentReceiptCode;
    }

    public String getShipmentReceiptDescription() {
        return shipmentReceiptDescription;
    }

    public void setShipmentReceiptDescription(String shipmentReceiptDescription) {
        this.shipmentReceiptDescription = shipmentReceiptDescription;
    }

    public UUID getIdInventoryItem() {
        return this.idInventoryItem;
    }

    public void setIdInventoryItem(UUID id) {
        this.idInventoryItem = id;
    }

    public UUID getIdOwner() {
        return idOwner;
    }

    public void setIdOwner(UUID idOwner) {
        this.idOwner = idOwner;
    }

    public UUID getIdFacility() {
        return idFacility;
    }

    public void setIdFacility(UUID idFacility) {
        this.idFacility = idFacility;
    }

    public UUID getIdContainer() {
        return idContainer;
    }

    public void setIdContainer(UUID idContainer) {
        this.idContainer = idContainer;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Double getQtyBooking() {
        return qtyBooking;
    }

    public void setQtyBooking(Double qtyBooking) {
        this.qtyBooking = qtyBooking;
    }

    public String getIdFrame() {
        return idFrame;
    }

    public void setIdFrame(String idFrame) {
        this.idFrame = idFrame;
    }

    public String getIdMachine() {
        return idMachine;
    }

    public void setIdMachine(String idMachine) {
        this.idMachine = idMachine;
    }

    public UUID getShipmentReceiptId() {
        return shipmentReceiptId;
    }

    public void setShipmentReceiptId(UUID shipmentReceiptId) {
        this.shipmentReceiptId = shipmentReceiptId;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public String getContainerCode() {
        return containerCode;
    }

    public void setContainerCode(String containerCode) {
        this.containerCode = containerCode;
    }

    public UUID getId() {
        return this.idInventoryItem;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getFeatureCode() {
        return featureCode;
    }

    public void setFeatureCode(String featureCode) {
        this.featureCode = featureCode;
    }

    public String getFeatureDescription() {
        return featureDescription;
    }

    public void setFeatureDescription(String featureDescription) {
        this.featureDescription = featureDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InventoryItemDTO inventoryItemDTO = (InventoryItemDTO) o;
        if (inventoryItemDTO.getIdInventoryItem() == null || getIdInventoryItem() == null) {
            return false;
        }
        return Objects.equals(getIdInventoryItem(), inventoryItemDTO.getIdInventoryItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdInventoryItem());
    }

    @Override
    public String toString() {
        return "InventoryItemDTO{" +
            "id=" + getIdInventoryItem() +
            ", idOwner='" + getIdOwner() + "'" +
            ", idFacility='" + getIdFacility() + "'" +
            ", idContainer='" + getIdContainer() + "'" +
            ", idProduct='" + getIdProduct() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            ", qty='" + getQty() + "'" +
            ", qtyBooking='" + getQtyBooking() + "'" +
            ", idFrame='" + getIdFrame() + "'" +
            ", idMachine='" + getIdMachine() + "'" +
            "}";
    }
}
