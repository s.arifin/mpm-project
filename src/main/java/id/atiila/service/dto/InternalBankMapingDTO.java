package id.atiila.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the InternalBankMaping entity.
 */
public class InternalBankMapingDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idinternal;

    private String idbank;

    private String bankname;

    private String accountnumber;

    private String glaccount;

    private String customermulia;

    private String deadstockmulia;

    private Integer isppn;

    public String getIdinternal() {
        return idinternal;
    }

    public void setIdinternal(String idinternal) {
        this.idinternal = idinternal;
    }

    public String getIdbank() {
        return idbank;
    }

    public void setIdbank(String idbank) {
        this.idbank = idbank;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }

    public String getGlaccount() {
        return glaccount;
    }

    public void setGlaccount(String glaccount) {
        this.glaccount = glaccount;
    }

    public String getCustomermulia() {
        return customermulia;
    }

    public void setCustomermulia(String customermulia) {
        this.customermulia = customermulia;
    }

    public String getDeadstockmulia() {
        return deadstockmulia;
    }

    public void setDeadstockmulia(String deadstockmulia) {
        this.deadstockmulia = deadstockmulia;
    }

    public Integer getIsppn() {
        return isppn;
    }

    public void setIsppn(Integer isppn) {
        this.isppn = isppn;
    }

    @Override
    public String toString() {
        return "InternalBankMapingDTO{" +
            ", idinternal='" + getIdinternal() + "'" +
            ", idbank='" + getIdbank() + "'" +
            ", bankname='" + getBankname() + "'" +
            ", accountnumber='" + getAccountnumber() + "'" +
            ", glaccount='" + getGlaccount() + "'" +
            ", customermulia='" + getCustomermulia() + "'" +
            ", deadstockmulia='" + getDeadstockmulia() + "'" +
            ", isppn='" + getIsppn() + "'" +
            "}";
    }
}
