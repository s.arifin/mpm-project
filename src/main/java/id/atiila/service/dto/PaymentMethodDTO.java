package id.atiila.service.dto;

import java.io.Serializable;
import java.util.*;

/**
 * A DTO for the PaymentMethod entity.
 * BeSmart Team
 */

public class PaymentMethodDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idPaymentMethod;

    private String refKey;

    private String description;

    private String accountNumber;

    private Integer methodTypeId;

    private String methodTypeDescription;

    public PaymentMethodDTO(){}

    public PaymentMethodDTO(Integer idmethodtype, String description){
        this.description = description;
        this.methodTypeId = idmethodtype;
        this.accountNumber = "xxx-xxx-xxx-xxx-xxx";
    }

    public Integer getIdPaymentMethod() {
        return this.idPaymentMethod;
    }

    public void setIdPaymentMethod(Integer id) {
        this.idPaymentMethod = id;
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Integer getMethodTypeId() {
        return methodTypeId;
    }

    public void setMethodTypeId(Integer paymentMethodTypeId) {
        this.methodTypeId = paymentMethodTypeId;
    }

    public String getMethodTypeDescription() {
        return methodTypeDescription;
    }

    public void setMethodTypeDescription(String paymentMethodTypeDescription) {
        this.methodTypeDescription = paymentMethodTypeDescription;
    }

    public Integer getId() {
        return this.idPaymentMethod;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PaymentMethodDTO paymentMethodDTO = (PaymentMethodDTO) o;
        if (paymentMethodDTO.getIdPaymentMethod() == null || getIdPaymentMethod() == null) {
            return false;
        }
        return Objects.equals(getIdPaymentMethod(), paymentMethodDTO.getIdPaymentMethod());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPaymentMethod());
    }

    @Override
    public String toString() {
        return "PaymentMethodDTO{" +
            "id=" + getIdPaymentMethod() +
            ", refKey='" + getRefKey() + "'" +
            ", description='" + getDescription() + "'" +
            ", accountNumber='" + getAccountNumber() + "'" +
            "}";
    }
}
