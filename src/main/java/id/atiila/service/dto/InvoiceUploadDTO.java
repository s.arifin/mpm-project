package id.atiila.service.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class InvoiceUploadDTO implements Serializable {

    private String invoiceNumber;

    private ZonedDateTime dateCreate;

    private String idProduct;

    private String idColor;

    private String customerCode;

    private String mainDealerCode;

    private Integer qty;

    private BigDecimal totalAmount;

    private BigDecimal ppn;

    private BigDecimal unitPrice;

    private BigDecimal unitDiscount;

    private String poNumber;

    private ZonedDateTime dueDate;

    private ZonedDateTime dateIssued;

    public ZonedDateTime getDateIssued() { return dateIssued; }

    public void setDateIssued(ZonedDateTime dateIssued) { this.dateIssued = dateIssued; }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getIdColor() {
        return idColor;
    }

    public void setIdColor(String idColor) {
        this.idColor = idColor;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getMainDealerCode() {
        return mainDealerCode;
    }

    public void setMainDealerCode(String mainDealerCode) {
        this.mainDealerCode = mainDealerCode;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getPpn() {
        return ppn;
    }

    public void setPpn(BigDecimal ppn) {
        this.ppn = ppn;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getUnitDiscount() {
        return unitDiscount;
    }

    public void setUnitDiscount(BigDecimal unitDiscount) {
        this.unitDiscount = unitDiscount;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public ZonedDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(ZonedDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public InvoiceUploadDTO() {
        super();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        InvoiceUploadDTO that = (InvoiceUploadDTO) o;

        return new EqualsBuilder()
            .append(invoiceNumber, that.invoiceNumber)
            .append(idProduct, that.idProduct)
            .append(idColor, that.idColor)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(invoiceNumber)
            .append(idProduct)
            .append(idColor)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "InvoiceUploadDTO{" +
            "invoiceNumber='" + invoiceNumber + '\'' +
            ", dateCreate=" + dateCreate +
            ", idProduct='" + idProduct + '\'' +
            ", idColor='" + idColor + '\'' +
            ", customerCode='" + customerCode + '\'' +
            ", mainDealerCode='" + mainDealerCode + '\'' +
            ", qty=" + qty +
            ", totalAmount=" + totalAmount +
            ", ppn=" + ppn +
            ", unitPrice=" + unitPrice +
            ", unitDiscount=" + unitDiscount +
            ", poNumber='" + poNumber + '\'' +
            ", dueDate=" + dueDate +
            '}';
    }
}
