package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the UomConversion entity.
 * BeSmart Team
 */

public class UomConversionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idUomConversion;

    private Double factor;

    private String uomToId;

    private String uomToDescription;

    private String uomFromId;

    private String uomFromDescription;

    public Integer getIdUomConversion() {
        return this.idUomConversion;
    }

    public void setIdUomConversion(Integer id) {
        this.idUomConversion = id;
    }

    public Double getFactor() {
        return factor;
    }

    public void setFactor(Double factor) {
        this.factor = factor;
    }

    public String getUomToId() {
        return uomToId;
    }

    public void setUomToId(String uomId) {
        this.uomToId = uomId;
    }

    public String getUomToDescription() {
        return uomToDescription;
    }

    public void setUomToDescription(String uomDescription) {
        this.uomToDescription = uomDescription;
    }

    public String getUomFromId() {
        return uomFromId;
    }

    public void setUomFromId(String uomId) {
        this.uomFromId = uomId;
    }

    public String getUomFromDescription() {
        return uomFromDescription;
    }

    public void setUomFromDescription(String uomDescription) {
        this.uomFromDescription = uomDescription;
    }

    public Integer getId() {
        return this.idUomConversion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UomConversionDTO uomConversionDTO = (UomConversionDTO) o;
        if (uomConversionDTO.getIdUomConversion() == null || getIdUomConversion() == null) {
            return false;
        }
        return Objects.equals(getIdUomConversion(), uomConversionDTO.getIdUomConversion());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdUomConversion());
    }

    @Override
    public String toString() {
        return "UomConversionDTO{" +
            "id=" + getIdUomConversion() +
            ", factor='" + getFactor() + "'" +
            "}";
    }
}
