package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Product entity.
 * BeSmart Team
 */

public class ProductDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idProduct;

    private String name;

    private ZonedDateTime dateIntroduction;

    private Integer productTypeId;

    private String productTypeDescription;

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProductDTO() {
    }

    public ProductDTO(String idProduct, String name) {
        this.idProduct = idProduct;
        this.name = name;
    }

    private Set<FeatureDTO> features = new HashSet<>();

    private Set<ProductCategoryDTO> categories = new HashSet<>();

    public String getIdProduct() {
        return this.idProduct;
    }

    public void setIdProduct(String id) {
        this.idProduct = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getDateIntroduction() {
        return dateIntroduction;
    }

    public void setDateIntroduction(ZonedDateTime dateIntroduction) {
        this.dateIntroduction = dateIntroduction;
    }

    public Integer getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Integer productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getProductTypeDescription() {
        return productTypeDescription;
    }

    public void setProductTypeDescription(String productTypeDescription) {
        this.productTypeDescription = productTypeDescription;
    }

    public Set<FeatureDTO> getFeatures() {
        return features;
    }

    public void setFeatures(Set<FeatureDTO> features) {
        this.features = features;
    }

    public Set<ProductCategoryDTO> getCategories() {
        return categories;
    }

    public void setCategories(Set<ProductCategoryDTO> productCategories) {
        this.categories = productCategories;
    }

    public String getId() {
        return this.idProduct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductDTO productDTO = (ProductDTO) o;
        if (productDTO.getIdProduct() == null || getIdProduct() == null) {
            return false;
        }
        return Objects.equals(getIdProduct(), productDTO.getIdProduct());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdProduct());
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
            "id=" + getIdProduct() +
            ", name='" + getName() + "'" +
            ", dateIntroduction='" + getDateIntroduction() + "'" +
            "}";
    }
}
