package id.atiila.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Driver entity.
 */
public class DriverDTO extends PartyRoleDTO {

    private String idDriver;

    private Boolean externalDriver;

    private String idVendor;

    private PersonDTO person = new PersonDTO();

    public String getIdDriver() {
        return idDriver;
    }

    public void setIdDriver(String idDriver) {
        this.idDriver = idDriver;
    }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    public Boolean getExternalDriver() {
        return externalDriver;
    }

    public void setExternalDriver(Boolean externalDriver) {
        this.externalDriver = externalDriver;
    }

    public String getIdVendor() { return idVendor; }

    public void setIdVendor(String idVendor) { this.idVendor = idVendor; }

    @Override
    public String toString() {
        return "DriverDTO{" +
            "id=" + getIdPartyRole() +
            ", idDriver='" + getIdDriver() + "'" +
            "}";
    }
}
