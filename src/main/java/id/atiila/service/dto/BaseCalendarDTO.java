package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the BaseCalendar entity.
 * BeSmart Team
 */

public class BaseCalendarDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idCalendar;

    private Integer idCalendarType;

    private Integer idParent;

    private ZonedDateTime dateKey;

    private Boolean workDay;

    private Integer sequence;

    private String description;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    public Integer getIdCalendar() {
        return this.idCalendar;
    }

    public void setIdCalendar(Integer id) {
        this.idCalendar = id;
    }

    public Integer getIdCalendarType() {
        return idCalendarType;
    }

    public void setIdCalendarType(Integer idCalendarType) {
        this.idCalendarType = idCalendarType;
    }

    public Integer getIdParent() {
        return idParent;
    }

    public void setIdParent(Integer idParent) {
        this.idParent = idParent;
    }

    public ZonedDateTime getDateKey() {
        return dateKey;
    }

    public void setDateKey(ZonedDateTime dateKey) {
        this.dateKey = dateKey;
    }

    public Boolean isWorkDay() {
        return workDay;
    }

    public void setWorkDay(Boolean workDay) {
        this.workDay = workDay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Integer getId() {
        return this.idCalendar;
    }

    public Boolean getWorkDay() {
        return workDay;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BaseCalendarDTO baseCalendarDTO = (BaseCalendarDTO) o;
        if (baseCalendarDTO.getIdCalendar() == null || getIdCalendar() == null) {
            return false;
        }
        return Objects.equals(getIdCalendar(), baseCalendarDTO.getIdCalendar());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdCalendar());
    }

    @Override
    public String toString() {
        return "BaseCalendarDTO{" +
            "id=" + getIdCalendar() +
            ", idCalendarType='" + getIdCalendarType() + "'" +
            ", idParent='" + getIdParent() + "'" +
            ", dateKey='" + getDateKey() + "'" +
            ", workDay='" + isWorkDay() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
