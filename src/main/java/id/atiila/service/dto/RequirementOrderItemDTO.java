package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the RequirementOrderItem entity.
 * atiila consulting
 */

public class RequirementOrderItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idReqOrderItem;

    private Double qty;

    private UUID orderItemId;

    private UUID requirementId;

    public UUID getIdReqOrderItem() {
        return this.idReqOrderItem;
    }

    public void setIdReqOrderItem(UUID id) {
        this.idReqOrderItem = id;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public UUID getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(UUID orderItemId) {
        this.orderItemId = orderItemId;
    }

    public UUID getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(UUID requirementId) {
        this.requirementId = requirementId;
    }

    public UUID getId() {
        return this.idReqOrderItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequirementOrderItemDTO requirementOrderItemDTO = (RequirementOrderItemDTO) o;
        if (requirementOrderItemDTO.getIdReqOrderItem() == null || getIdReqOrderItem() == null) {
            return false;
        }
        return Objects.equals(getIdReqOrderItem(), requirementOrderItemDTO.getIdReqOrderItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdReqOrderItem());
    }

    @Override
    public String toString() {
        return "RequirementOrderItemDTO{" +
            "id=" + getIdReqOrderItem() +
            ", qty='" + getQty() + "'" +
            "}";
    }
}
