package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the PartyFacilityPurpose entity.
 * BeSmart Team
 */

public class PartyFacilityPurposeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idPartyFacility;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private UUID organizationId;

    private String organizationName;

    private UUID facilityId;

    private String facilityDescription;

    private Integer purposeTypeId;

    private String purposeTypeDescription;

    public UUID getIdPartyFacility() {
        return this.idPartyFacility;
    }

    public void setIdPartyFacility(UUID id) {
        this.idPartyFacility = id;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public UUID getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(UUID organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public UUID getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(UUID facilityId) {
        this.facilityId = facilityId;
    }

    public String getFacilityDescription() {
        return facilityDescription;
    }

    public void setFacilityDescription(String facilityDescription) {
        this.facilityDescription = facilityDescription;
    }

    public Integer getPurposeTypeId() {
        return purposeTypeId;
    }

    public void setPurposeTypeId(Integer purposeTypeId) {
        this.purposeTypeId = purposeTypeId;
    }

    public String getPurposeTypeDescription() {
        return purposeTypeDescription;
    }

    public void setPurposeTypeDescription(String purposeTypeDescription) {
        this.purposeTypeDescription = purposeTypeDescription;
    }

    public UUID getId() {
        return this.idPartyFacility;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PartyFacilityPurposeDTO partyFacilityPurposeDTO = (PartyFacilityPurposeDTO) o;
        if (partyFacilityPurposeDTO.getIdPartyFacility() == null || getIdPartyFacility() == null) {
            return false;
        }
        return Objects.equals(getIdPartyFacility(), partyFacilityPurposeDTO.getIdPartyFacility());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPartyFacility());
    }

    @Override
    public String toString() {
        return "PartyFacilityPurposeDTO{" +
            "id=" + getIdPartyFacility() +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
