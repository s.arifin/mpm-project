package id.atiila.service.dto;

import java.io.Serializable;

public class CustomEventDTO implements Serializable {

    private String idEventType;
    private String description;

    public String getIdEventType() {
        return idEventType;
    }

    public void setIdEventType(String idEventType) {
        this.idEventType = idEventType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
