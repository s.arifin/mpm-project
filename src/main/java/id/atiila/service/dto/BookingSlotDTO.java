package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the BookingSlot entity.
 * BeSmart Team
 */

public class BookingSlotDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idBookSlot;

    private String slotNumber;

    private Integer calendarId;

    private String calendarDescription;

    private UUID standardId;

    private String standardDescription;

    public BookingSlotDTO() {
    }

    public BookingSlotDTO(String slotNumber, Integer calendarId, UUID standardId) {
        this.slotNumber = slotNumber;
        this.calendarId = calendarId;
        this.standardId = standardId;
    }

    public UUID getIdBookSlot() {
        return this.idBookSlot;
    }

    public void setIdBookSlot(UUID id) {
        this.idBookSlot = id;
    }

    public String getSlotNumber() {
        return slotNumber;
    }

    public void setSlotNumber(String slotNumber) {
        this.slotNumber = slotNumber;
    }

    public Integer getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(Integer baseCalendarId) {
        this.calendarId = baseCalendarId;
    }

    public String getCalendarDescription() {
        return calendarDescription;
    }

    public void setCalendarDescription(String baseCalendarDescription) {
        this.calendarDescription = baseCalendarDescription;
    }

    public UUID getStandardId() {
        return standardId;
    }

    public void setStandardId(UUID bookingSlotStandardId) {
        this.standardId = bookingSlotStandardId;
    }

    public String getStandardDescription() {
        return standardDescription;
    }

    public void setStandardDescription(String bookingSlotStandardDescription) {
        this.standardDescription = bookingSlotStandardDescription;
    }

    public UUID getId() {
        return this.idBookSlot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BookingSlotDTO bookingSlotDTO = (BookingSlotDTO) o;
        if (bookingSlotDTO.getIdBookSlot() == null || getIdBookSlot() == null) {
            return false;
        }
        return Objects.equals(getIdBookSlot(), bookingSlotDTO.getIdBookSlot());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdBookSlot());
    }

    @Override
    public String toString() {
        return "BookingSlotDTO{" +
            "id=" + getIdBookSlot() +
            ", slotNumber='" + getSlotNumber() + "'" +
            "}";
    }
}
