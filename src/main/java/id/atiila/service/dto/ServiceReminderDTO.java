package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the ServiceReminder entity.
 * BeSmart Team
 */

public class ServiceReminderDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idReminder;

    private Integer days;

    private Integer claimTypeId;

    private String claimTypeDescription;

    private Integer reminderTypeId;

    private String reminderTypeDescription;

    public Integer getIdReminder() {
        return this.idReminder;
    }

    public void setIdReminder(Integer id) {
        this.idReminder = id;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Integer getClaimTypeId() {
        return claimTypeId;
    }

    public void setClaimTypeId(Integer dealerClaimTypeId) {
        this.claimTypeId = dealerClaimTypeId;
    }

    public String getClaimTypeDescription() {
        return claimTypeDescription;
    }

    public void setClaimTypeDescription(String dealerClaimTypeDescription) {
        this.claimTypeDescription = dealerClaimTypeDescription;
    }

    public Integer getReminderTypeId() {
        return reminderTypeId;
    }

    public void setReminderTypeId(Integer dealerReminderTypeId) {
        this.reminderTypeId = dealerReminderTypeId;
    }

    public String getReminderTypeDescription() {
        return reminderTypeDescription;
    }

    public void setReminderTypeDescription(String dealerReminderTypeDescription) {
        this.reminderTypeDescription = dealerReminderTypeDescription;
    }
    
    public Integer getId() {
        return this.idReminder;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ServiceReminderDTO serviceReminderDTO = (ServiceReminderDTO) o;
        if (serviceReminderDTO.getIdReminder() == null || getIdReminder() == null) {
            return false;
        }
        return Objects.equals(getIdReminder(), serviceReminderDTO.getIdReminder());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdReminder());
    }

    @Override
    public String toString() {
        return "ServiceReminderDTO{" +
            "id=" + getIdReminder() +
            ", days='" + getDays() + "'" +
            "}";
    }
}
