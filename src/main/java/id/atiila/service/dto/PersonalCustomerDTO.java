package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the PersonalCustomer entity.
 * BeSmart Team
 */

public class PersonalCustomerDTO extends CustomerDTO {

    private static final long serialVersionUID = 1L;

    private PersonDTO person;

    public PersonDTO getPerson() {
        return this.person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    public PersonalCustomerDTO() {
        super();
        this.person = new PersonDTO();
    }

    @Override
    public String toString() {
        return "PersonalCustomerDTO{" +
            "id=" + getIdCustomer() +
            ", idRoleType='" + getIdRoleType() + "'" +
            "}";
    }
}
