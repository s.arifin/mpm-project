package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the PaymentType entity.
 * BeSmart Team
 */

public class PaymentTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idPaymentType;

    private String description;

    public Integer getIdPaymentType() {
        return this.idPaymentType;
    }

    public void setIdPaymentType(Integer id) {
        this.idPaymentType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.idPaymentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PaymentTypeDTO paymentTypeDTO = (PaymentTypeDTO) o;
        if (paymentTypeDTO.getIdPaymentType() == null || getIdPaymentType() == null) {
            return false;
        }
        return Objects.equals(getIdPaymentType(), paymentTypeDTO.getIdPaymentType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPaymentType());
    }

    @Override
    public String toString() {
        return "PaymentTypeDTO{" +
            "id=" + getIdPaymentType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
