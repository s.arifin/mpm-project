package id.atiila.service.dto;

    import id.atiila.domain.*;
    import io.swagger.models.auth.In;

    import java.io.Serializable;
    import java.math.BigDecimal;
    import java.time.ZonedDateTime;
    import java.util.UUID;


public class CustomRequirementDTO implements Serializable {

    private String internalId;

    private String internalName;

    private String fakturATPM;

    private ZonedDateTime fakturATPMDate;

    private String ivuNumber;

    private String vsoNumber;

    private String name;
    private String idFrame;
    private String idMachine;
    private String salesname;
    private String note;
    private BigDecimal selisih;
    private String statusFaktur;
    private UUID idRequirement;
    private String registrationNumber;
    private String requestPoliceNumber;
    private String idVendor;
    private ZonedDateTime dateFillName;
    private UUID idOrderItem;
    private UUID idOrder;
    private UUID idPersonOwner;
    private UUID idOrganizationOwner;
    private String familyIdNumber;

    private Integer saleType;

    private UUID idVehicle;
    private String idInternal;
    private String idBillTo;
    private String idShipTo;
    private BigDecimal creditInstallment;
    private BigDecimal creditDownPayment;
    private Integer creditTenor;
    private String idCustomer;

    private String saleTypeDesc;

    private String fincoy;
    private String productId;
    private String personalIdNumber;
    private String gender;
    private ZonedDateTime dob;
    private String address1;
//    private String address2;
    private String village;
    private String district;
    private String city;
    private String province;
    private Integer religionTypeId;
    private Integer workTypeId;
    private String cellPhone1;
    private String cellPhone2;
    private String expenditureOneMonth;
    private String lastEducation;
    private String infoAboutHonda;
    private String vehicleBrandOwner;
    private String vehicleTypeOwner;
    private String buyFor;
    private String vehicleUser;
    private String email;
    private String homeStatus;
    private String mobilePhoneOwnershipStatus;
    private String facebook;
    private String instagram;
    private String twitter;
    private String youtube;
    private String hobby;
    private String keterangan;
    private String groupCustomer;
    private String jenisPembayaran;
    private UUID idProvinceSurat;
    private String codeProvinceSurat;
    private UUID idCitySurat;
    private String codeCitySurat;
    private UUID idDistrictSurat;
    private String codeDistrictSurat;
    private UUID idVillageSurat;
    private String codeVillageSurat;
    private String addressSurat;
    private String salesCode;
    private String picName;
    private ZonedDateTime shippingListDate; // tanggal spg
    private String refNumber;
    private ZonedDateTime refDate;
    private ZonedDateTime platNomor;
    private ZonedDateTime notice;
    private ZonedDateTime stnk;
    private ZonedDateTime bpkb;
    private String bpkbNumber;
    private BigDecimal costHandling;
    private ZonedDateTime dateSubmission;
    private BigDecimal bbn;
    private BigDecimal otherCost;
    private String numberTDP;
    private String officeMail;
    private String officePhone;
    private String citizenship;
    private String postalCode;
    private String postalCodeSurat;
    // private String officeFax;

    public CustomRequirementDTO() {
    }

    public CustomRequirementDTO(VehicleSalesOrder v, VehicleDocumentRequirement r) {
        this.fakturATPM = r.getFakturATPM();
        this.fakturATPMDate = r.getFakturDate();
//        if (v.getBillingNumber() != null)this.ivuNumber = v.getBillingNumber();
        this.vsoNumber = v.getOrderNumber();
        this.idFrame = r.getVehicle().getIdFrame();
        this.idMachine = r.getVehicle().getIdMachine();

//        this.salesname = vso
        this.note = r.getNote();
        //konfirmasi
        this.selisih = null;
        this.statusFaktur = r.getStatusFaktur();
        this.idRequirement = r.getIdRequirement();
        this.registrationNumber = r.getRegistrationNumber();
        this.requestPoliceNumber = r.getRequestPoliceNumber();
        // konfirmasi
        this.idVendor =  null;
        this.dateFillName = r.getDateFillName();
        this.idOrderItem = r.getOrderItem().getIdOrderItem();
        this.idOrder = v.getIdOrder();

        this.dateSubmission = r.getDateSubmission();
        this.bbn = r.getBbn();
        if (r.getOtherCost() != null) {
            this.otherCost = r.getOtherCost();
        }
        this.costHandling = r.getCostHandling();

        this.idVehicle = r.getVehicle().getIdVehicle();
        this.idInternal = r.getInternal().getIdInternal();
        if (r.getBillTo() != null){
            if (r.getBillTo().getIdBillTo() != null){
                this.idBillTo = r.getBillTo().getIdBillTo();
            }
        }

        //TODO: UNTUK KEPERLUAN GC
//        this.numberTDP = r.getOrganizationOwner().getNumberTDP();
//        this.officeMail = r.getOrganizationOwner().getOfficeMail();
//        this.officePhone = r.getOrganizationOwner().getOfficePhone();
        if (r.getPersonOwner() != null) {
            this.idPersonOwner = r.getPersonOwner().getIdParty();
        } else {
            this.idOrganizationOwner = r.getOrganizationOwner().getIdParty();
        }

        if (this.idPersonOwner != null) {
            this.name = r.getPersonOwner().getName();
        } else {
            this.name = r.getOrganizationOwner().getName();
        }

        if (v.getCustomer() != null) {
            this.idCustomer = v.getCustomer().getIdCustomer();
        }

        this.productId = r.getVehicle().getIdProduct();

        if (r.getPersonOwner() != null){
            this.cellPhone1 = r.getPersonOwner().getCellPhone1();
            this.cellPhone2 = r.getPersonOwner().getPhone();
            if (r.getPersonOwner().getGender() != null)this.gender = r.getPersonOwner().getGender();
            if (r.getPersonOwner().getDob() != null)this.dob = r.getPersonOwner().getDob();

            if (r.getPersonOwner().getFamilyIdNumber() != null) this.familyIdNumber = r.getPersonOwner().getFamilyIdNumber();
            if (r.getPersonOwner().getPersonalIdNumber() != null) this.personalIdNumber = r.getPersonOwner().getPersonalIdNumber();

            if (r.getPersonOwner().getPostalAddress() != null){
                if (r.getPersonOwner().getPostalAddress().getVillage() != null)this.village = r.getPersonOwner().getPostalAddress().getVillage().getGeoCode();
                if (r.getPersonOwner().getPostalAddress().getDistrict() != null)this.district = r.getPersonOwner().getPostalAddress().getDistrict().getGeoCode();
                if (r.getPersonOwner().getPostalAddress().getCity() != null)this.city = r.getPersonOwner().getPostalAddress().getCity().getGeoCode();
                if (r.getPersonOwner().getPostalAddress().getProvince() != null)this.province = r.getPersonOwner().getPostalAddress().getProvince().getGeoCode();
                if (r.getPersonOwner().getPostalAddress().getAddress2() != null) {
                    this.address1 = r.getPersonOwner().getPostalAddress().getAddress1().concat("~").concat(r.getPersonOwner().getPostalAddress().getAddress2());
                } else {
                    this.address1 = r.getPersonOwner().getPostalAddress().getAddress1();
                }
           } else {
                System.out.println("Homeless");
            }
            this.religionTypeId = r.getPersonOwner().getReligionType().getIdReligionType();
        } else {
            this.cellPhone2 = r.getOrganizationOwner().getOfficePhone();
            this.cellPhone1 = r.getOrganizationOwner().getPicPhone();
            this.picName = r.getOrganizationOwner().getPicName();
            if (r.getOrganizationOwner().getNpwp() != null) this.personalIdNumber = r.getOrganizationOwner().getNpwp();
            if (r.getOrganizationOwner().getDateOrganizationEstablish() != null)this.dob = r.getOrganizationOwner().getDateOrganizationEstablish();
//            if (r.getOrganizationOwner().getPic().getFamilyIdNumber() != null) this.familyIdNumber = r.getOrganizationOwner().getPic().getFamilyIdNumber();

            if (r.getOrganizationOwner().getPostalAddress() != null) {
                if (r.getOrganizationOwner().getPostalAddress().getVillage() != null)this.village = r.getOrganizationOwner().getPostalAddress().getVillage().getGeoCode();
                if (r.getOrganizationOwner().getPostalAddress().getDistrict() != null)this.district = r.getOrganizationOwner().getPostalAddress().getDistrict().getGeoCode();
                if (r.getOrganizationOwner().getPostalAddress().getCity() != null)this.city = r.getOrganizationOwner().getPostalAddress().getCity().getGeoCode();
                if (r.getOrganizationOwner().getPostalAddress().getProvince() != null)this.province = r.getOrganizationOwner().getPostalAddress().getProvince().getGeoCode();
                if (r.getOrganizationOwner().getPostalAddress().getAddress2() != null) {
                    this.address1 = r.getOrganizationOwner().getPostalAddress().getAddress1().concat("~").concat(r.getOrganizationOwner().getPostalAddress().getAddress2());
                } else {
                    this.address1 = r.getOrganizationOwner().getPostalAddress().getAddress1();
                }
            } else {
                System.out.println("Homeless");
            }
        }
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCodeSurat() {
        return postalCodeSurat;
    }

    public void setPostalCodeSurat(String postalCodeSurat) {
        this.postalCodeSurat = postalCodeSurat;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getNumberTDP() {
        return numberTDP;
    }

    public void setNumberTDP(String numberTDP) {
        this.numberTDP = numberTDP;
    }

    public String getOfficeMail() {
        return officeMail;
    }

    public void setOfficeMail(String officeMail) {
        this.officeMail = officeMail;
    }

    public String getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    public UUID getIdOrganizationOwner() {
        return idOrganizationOwner;
    }

    public void setIdOrganizationOwner(UUID idOrganizationOwner) {
        this.idOrganizationOwner = idOrganizationOwner;
    }

    public String getFakturATPM() {
        return fakturATPM;
    }

    public void setFakturATPM(String fakturATPM) {
        this.fakturATPM = fakturATPM;
    }

    public ZonedDateTime getFakturATPMDate() {
        return fakturATPMDate;
    }

    public void setFakturATPMDate(ZonedDateTime fakturATPMDate) {
        this.fakturATPMDate = fakturATPMDate;
    }

    public String getIvuNumber() {
        return ivuNumber;
    }

    public void setIvuNumber(String ivuNumber) {
        this.ivuNumber = ivuNumber;
    }

    public String getVsoNumber() {
        return vsoNumber;
    }

    public void setVsoNumber(String vsoNumber) {
        this.vsoNumber = vsoNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdFrame() {
        return idFrame;
    }

    public void setIdFrame(String idFrame) {
        this.idFrame = idFrame;
    }

    public String getIdMachine() {
        return idMachine;
    }

    public void setIdMachine(String idMachine) {
        this.idMachine = idMachine;
    }

    public String getSalesname() {
        return salesname;
    }

    public void setSalesname(String salesname) {
        this.salesname = salesname;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public BigDecimal getSelisih() {
        return selisih;
    }

    public void setSelisih(BigDecimal selisih) {
        this.selisih = selisih;
    }

    public String getStatusFaktur() {
        return statusFaktur;
    }

    public void setStatusFaktur(String statusFaktur) {
        this.statusFaktur = statusFaktur;
    }

    public UUID getIdRequirement() {
        return idRequirement;
    }

    public void setIdRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getRequestPoliceNumber() {
        return requestPoliceNumber;
    }

    public void setRequestPoliceNumber(String requestPoliceNumber) {
        this.requestPoliceNumber = requestPoliceNumber;
    }

    public String getIdVendor() {
        return idVendor;
    }

    public void setIdVendor(String idVendor) {
        this.idVendor = idVendor;
    }

    public ZonedDateTime getDateFillName() {
        return dateFillName;
    }

    public void setDateFillName(ZonedDateTime dateFillName) {
        this.dateFillName = dateFillName;
    }

    public UUID getIdOrderItem() {
        return idOrderItem;
    }

    public void setIdOrderItem(UUID idOrderItem) {
        this.idOrderItem = idOrderItem;
    }

    public UUID getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(UUID idOrder) {
        this.idOrder = idOrder;
    }

    public UUID getIdPersonOwner() {
        return idPersonOwner;
    }

    public void setIdPersonOwner(UUID idPersonOwner) {
        this.idPersonOwner = idPersonOwner;
    }

    public UUID getIdVehicle() {
        return idVehicle;
    }

    public void setIdVehicle(UUID idVehicle) {
        this.idVehicle = idVehicle;
    }

    public String getIdInternal() {
        return idInternal;
    }

    public void setIdInternal(String idInternal) {
        this.idInternal = idInternal;
    }

    public String getIdBillTo() {
        return idBillTo;
    }

    public void setIdBillTo(String idBillTo) {
        this.idBillTo = idBillTo;
    }

    public String getIdShipTo() {
        return idShipTo;
    }

    public void setIdShipTo(String idShipTo) {
        this.idShipTo = idShipTo;
    }

    public BigDecimal getCreditInstallment() {
        return creditInstallment;
    }

    public void setCreditInstallment(BigDecimal creditInstallment) {
        this.creditInstallment = creditInstallment;
    }

    public BigDecimal getCreditDownPayment() {
        return creditDownPayment;
    }

    public void setCreditDownPayment(BigDecimal creditDownPayment) {
        this.creditDownPayment = creditDownPayment;
    }

    public Integer getCreditTenor() {
        return creditTenor;
    }

    public void setCreditTenor(Integer creditTenor) {
        this.creditTenor = creditTenor;
    }

    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getSaleTypeDesc() {
        return saleTypeDesc;
    }

    public void setSaleTypeDesc(String saleTypeDesc) {
        this.saleTypeDesc = saleTypeDesc;
    }

    public String getFincoy() {
        return fincoy;
    }

    public void setFincoy(String fincoy) {
        this.fincoy = fincoy;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPersonalIdNumber() {
        return personalIdNumber;
    }

    public void setPersonalIdNumber(String personalIdNumber) {
        this.personalIdNumber = personalIdNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ZonedDateTime getDob() {
        return dob;
    }

    public void setDob(ZonedDateTime dob) {
        this.dob = dob;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getFamilyIdNumber() {
        return familyIdNumber;
    }

    public void setFamilyIdNumber(String familyIdNumber) {
        this.familyIdNumber = familyIdNumber;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Integer getReligionTypeId() {
        return religionTypeId;
    }

    public void setReligionTypeId(Integer religionTypeId) {
        this.religionTypeId = religionTypeId;
    }

    public Integer getWorkTypeId() {
        return workTypeId;
    }

    public void setWorkTypeId(Integer workTypeId) {
        this.workTypeId = workTypeId;
    }

    public String getCellPhone1() {
        return cellPhone1;
    }

    public void setCellPhone1(String cellPhone1) {
        this.cellPhone1 = cellPhone1;
    }

    public String getCellPhone2() {
        return cellPhone2;
    }

    public void setCellPhone2(String cellPhone2) {
        this.cellPhone2 = cellPhone2;
    }

    public String getExpenditureOneMonth() {
        return expenditureOneMonth;
    }

    public void setExpenditureOneMonth(String expenditureOneMonth) {
        this.expenditureOneMonth = expenditureOneMonth;
    }

    public String getLastEducation() {
        return lastEducation;
    }

    public void setLastEducation(String lastEducation) {
        this.lastEducation = lastEducation;
    }

    public String getInfoAboutHonda() {
        return infoAboutHonda;
    }

    public void setInfoAboutHonda(String infoAboutHonda) {
        this.infoAboutHonda = infoAboutHonda;
    }

    public String getVehicleBrandOwner() {
        return vehicleBrandOwner;
    }

    public void setVehicleBrandOwner(String vehicleBrandOwner) {
        this.vehicleBrandOwner = vehicleBrandOwner;
    }

    public String getVehicleTypeOwner() {
        return vehicleTypeOwner;
    }

    public void setVehicleTypeOwner(String vehicleTypeOwner) {
        this.vehicleTypeOwner = vehicleTypeOwner;
    }

    public String getBuyFor() {
        return buyFor;
    }

    public void setBuyFor(String buyFor) {
        this.buyFor = buyFor;
    }

    public String getVehicleUser() {
        return vehicleUser;
    }

    public void setVehicleUser(String vehicleUser) {
        this.vehicleUser = vehicleUser;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHomeStatus() {
        return homeStatus;
    }

    public void setHomeStatus(String homeStatus) {
        this.homeStatus = homeStatus;
    }

    public String getMobilePhoneOwnershipStatus() {
        return mobilePhoneOwnershipStatus;
    }

    public void setMobilePhoneOwnershipStatus(String mobilePhoneOwnershipStatus) {
        this.mobilePhoneOwnershipStatus = mobilePhoneOwnershipStatus;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getGroupCustomer() {
        return groupCustomer;
    }

    public void setGroupCustomer(String groupCustomer) {
        this.groupCustomer = groupCustomer;
    }

    public String getJenisPembayaran() {
        return jenisPembayaran;
    }

    public void setJenisPembayaran(String jenisPembayaran) {
        this.jenisPembayaran = jenisPembayaran;
    }

    public UUID getIdProvinceSurat() {
        return idProvinceSurat;
    }

    public void setIdProvinceSurat(UUID idProvinceSurat) {
        this.idProvinceSurat = idProvinceSurat;
    }

    public UUID getIdCitySurat() {
        return idCitySurat;
    }

    public void setIdCitySurat(UUID idCitySurat) {
        this.idCitySurat = idCitySurat;
    }

    public UUID getIdDistrictSurat() {
        return idDistrictSurat;
    }

    public void setIdDistrictSurat(UUID idDistrictSurat) {
        this.idDistrictSurat = idDistrictSurat;
    }

    public String getAddressSurat() {
        return addressSurat;
    }

    public void setAddressSurat(String addressSurat) {
        this.addressSurat = addressSurat;
    }

    public String getCodeProvinceSurat() {
        return codeProvinceSurat;
    }

    public void setCodeProvinceSurat(String codeProvinceSurat) {
        this.codeProvinceSurat = codeProvinceSurat;
    }

    public String getCodeCitySurat() {
        return codeCitySurat;
    }

    public void setCodeCitySurat(String codeCitySurat) {
        this.codeCitySurat = codeCitySurat;
    }

    public String getCodeDistrictSurat() {
        return codeDistrictSurat;
    }

    public void setCodeDistrictSurat(String codeDistrictSurat) {
        this.codeDistrictSurat = codeDistrictSurat;
    }

    public UUID getIdVillageSurat() {
        return idVillageSurat;
    }

    public void setIdVillageSurat(UUID idVillageSurat) {
        this.idVillageSurat = idVillageSurat;
    }

    public String getCodeVillageSurat() {
        return codeVillageSurat;
    }

    public void setCodeVillageSurat(String codeVillageSurat) {
        this.codeVillageSurat = codeVillageSurat;
    }

    public String getSalesCode() {
        return salesCode;
    }

    public void setSalesCode(String salesCode) {
        this.salesCode = salesCode;
    }

    public String getPicName() {
        return picName;
    }

    public void setPicName(String picName) {
        this.picName = picName;
    }

    public ZonedDateTime getShippingListDate() {
        return shippingListDate;
    }

    public void setShippingListDate(ZonedDateTime shippingListDate) {
        this.shippingListDate = shippingListDate;
    }

    public Integer getSaleType() {
        return saleType;
    }

    public void setSaleType(Integer saleType) {
        this.saleType = saleType;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public ZonedDateTime getRefDate() {
        return refDate;
    }

    public void setRefDate(ZonedDateTime refDate) {
        this.refDate = refDate;
    }

    public ZonedDateTime getPlatNomor() {
        return platNomor;
    }

    public void setPlatNomor(ZonedDateTime platNomor) {
        this.platNomor = platNomor;
    }

    public ZonedDateTime getNotice() {
        return notice;
    }

    public void setNotice(ZonedDateTime notice) {
        this.notice = notice;
    }

    public ZonedDateTime getStnk() {
        return stnk;
    }

    public void setStnk(ZonedDateTime stnk) {
        this.stnk = stnk;
    }

    public ZonedDateTime getBpkb() {
        return bpkb;
    }

    public void setBpkb(ZonedDateTime bpkb) {
        this.bpkb = bpkb;
    }

    public String getBpkbNumber() {
        return bpkbNumber;
    }

    public void setBpkbNumber(String bpkbNumber) {
        this.bpkbNumber = bpkbNumber;
    }

    public BigDecimal getCostHandling() {
        return costHandling;
    }

    public void setCostHandling(BigDecimal costHandling) {
        this.costHandling = costHandling;
    }

    public ZonedDateTime getDateSubmission() {
        return dateSubmission;
    }

    public void setDateSubmission(ZonedDateTime dateSubmission) {
        this.dateSubmission = dateSubmission;
    }

    public BigDecimal getBbn() {
        return bbn;
    }

    public void setBbn(BigDecimal bbn) {
        this.bbn = bbn;
    }

    public BigDecimal getOtherCost() {
        return otherCost;
    }

    public void setOtherCost(BigDecimal otherCost) {
        this.otherCost = otherCost;
    }
}
