package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Product entity.
 * BeSmart Team
 */

public class CustomProductDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idProduct;

    private String name;

    private String description;

    private String nameType;

    private String stockItem;

    private String stockItemName;

    private String discontinuedId;

    private String discontinuedName;

    private String productClassId;

    private String productClassName;

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public String getStockItem() {
        return stockItem;
    }

    public void setStockItem(String stockItem) {
        this.stockItem = stockItem;
    }

    public String getStockItemName() {
        return stockItemName;
    }

    public void setStockItemName(String stockItemName) {
        this.stockItemName = stockItemName;
    }

    public String getDiscontinuedId() {
        return discontinuedId;
    }

    public void setDiscontinuedId(String discontinuedId) {
        this.discontinuedId = discontinuedId;
    }

    public String getDiscontinuedName() {
        return discontinuedName;
    }

    public void setDiscontinuedName(String discontinuedName) {
        this.discontinuedName = discontinuedName;
    }

    public String getProductClassId() {
        return productClassId;
    }

    public void setProductClassId(String productClassId) {
        this.productClassId = productClassId;
    }

    public String getProductClassName() {
        return productClassName;
    }

    public void setProductClassName(String productClassName) {
        this.productClassName = productClassName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductDTO productDTO = (ProductDTO) o;
        if (productDTO.getIdProduct() == null || getIdProduct() == null) {
            return false;
        }
        return Objects.equals(getIdProduct(), productDTO.getIdProduct());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdProduct());
    }

    @Override
    public String toString() {
        return "CustomProductDTO{" +
            "idProduct='" + getIdProduct() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", nameType='" + getNameType() + "'" +
            ", stockItem='" + getStockItem() + "'" +
            ", stockItemName='" + getStockItemName() + "'" +
            ", discontinuedId='" + getDiscontinuedId() + "'" +
            ", discontinuedName='" + getDiscontinuedName() + "'" +
            ", productClassId='" + getProductClassId() + "'" +
            ", productClassName='" + getProductClassName() + "'" +
            '}';
    }
}
