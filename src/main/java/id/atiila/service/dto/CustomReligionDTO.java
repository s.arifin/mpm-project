package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ReligionType entity.
 * BeSmart Team
 */

public class CustomReligionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idReligionType;

    private String description;

    public String getIdReligionType() {
        return this.idReligionType;
    }

    public void setIdReligionType(String id) {
        this.idReligionType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return this.idReligionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReligionTypeDTO religionTypeDTO = (ReligionTypeDTO) o;
        if (religionTypeDTO.getIdReligionType() == null || getIdReligionType() == null) {
            return false;
        }
        return Objects.equals(getIdReligionType(), religionTypeDTO.getIdReligionType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdReligionType());
    }

    @Override
    public String toString() {
        return "CustomReligionDTO{" +
            "id=" + getIdReligionType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
