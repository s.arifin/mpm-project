package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the OrderType entity.
 * BeSmart Team
 */

public class OrderTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idOrderType;

    private String description;

    public OrderTypeDTO() {
    }

    public OrderTypeDTO(Integer idOrderType, String description) {
        this.idOrderType = idOrderType;
        this.description = description;
    }

    public Integer getIdOrderType() {
        return this.idOrderType;
    }

    public void setIdOrderType(Integer id) {
        this.idOrderType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idOrderType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderTypeDTO orderTypeDTO = (OrderTypeDTO) o;
        if (orderTypeDTO.getIdOrderType() == null || getIdOrderType() == null) {
            return false;
        }
        return Objects.equals(getIdOrderType(), orderTypeDTO.getIdOrderType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdOrderType());
    }

    @Override
    public String toString() {
        return "OrderTypeDTO{" +
            "id=" + getIdOrderType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
