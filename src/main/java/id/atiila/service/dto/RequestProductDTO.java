package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the RequestProduct entity.
 * BeSmart Team
 */

public class RequestProductDTO extends RequestDTO {

    private static final long serialVersionUID = 1L;

    private UUID facilityFromId;

    private Integer facilityTypeFromId;

    private String facilityFromDescription;

    private UUID facilityToId;

    private Integer facilityTypeToId;

    private String facilityToDescription;

    public UUID getFacilityFromId() {
        return facilityFromId;
    }

    public void setFacilityFromId(UUID facilityId) {
        this.facilityFromId = facilityId;
    }

    public Integer getFacilityTypeFromId() { return facilityTypeFromId; }

    public void setFacilityTypeFromId(Integer facilityTypeFromId) { this.facilityTypeFromId = facilityTypeFromId; }

    public String getFacilityFromDescription() {
        return facilityFromDescription;
    }

    public void setFacilityFromDescription(String facilityDescription) { this.facilityFromDescription = facilityDescription; }

    public UUID getFacilityToId() {
        return facilityToId;
    }

    public void setFacilityToId(UUID facilityId) {
        this.facilityToId = facilityId;
    }

    public Integer getFacilityTypeToId() { return facilityTypeToId; }

    public void setFacilityTypeToId(Integer facilityTypeToId) { this.facilityTypeToId = facilityTypeToId; }

    public String getFacilityToDescription() {
        return facilityToDescription;
    }

    public void setFacilityToDescription(String facilityDescription) {
        this.facilityToDescription = facilityDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequestProductDTO requestProductDTO = (RequestProductDTO) o;
        if (requestProductDTO.getIdRequest() == null || getIdRequest() == null) {
            return false;
        }
        return Objects.equals(getIdRequest(), requestProductDTO.getIdRequest());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRequest());
    }

    @Override
    public String toString() {
        return "RequestProductDTO{" +
            "id=" + getIdRequest() +
            "}";
    }
}
