package id.atiila.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the PaymentApplication entity.
 * BeSmart Team
 */

public class PaymentApplicationDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idPaymentApplication;

    private BigDecimal amountApplied;

    private UUID paymentId;

    private String paymentPaymentNumber;

    private UUID billingId;

    private String billingBillingNumber;

    public UUID getIdPaymentApplication() {
        return this.idPaymentApplication;
    }

    public void setIdPaymentApplication(UUID id) {
        this.idPaymentApplication = id;
    }

    public BigDecimal getAmountApplied() {
        return amountApplied;
    }

    public void setAmountApplied(BigDecimal amountApplied) {
        this.amountApplied = amountApplied;
    }

    public UUID getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(UUID paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentPaymentNumber() {
        return paymentPaymentNumber;
    }

    public void setPaymentPaymentNumber(String paymentPaymentNumber) {
        this.paymentPaymentNumber = paymentPaymentNumber;
    }

    public UUID getBillingId() {
        return billingId;
    }

    public void setBillingId(UUID billingId) {
        this.billingId = billingId;
    }

    public String getBillingBillingNumber() {
        return billingBillingNumber;
    }

    public void setBillingBillingNumber(String billingBillingNumber) {
        this.billingBillingNumber = billingBillingNumber;
    }

    public UUID getId() {
        return this.idPaymentApplication;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PaymentApplicationDTO paymentApplicationDTO = (PaymentApplicationDTO) o;
        if (paymentApplicationDTO.getIdPaymentApplication() == null || getIdPaymentApplication() == null) {
            return false;
        }
        return Objects.equals(getIdPaymentApplication(), paymentApplicationDTO.getIdPaymentApplication());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPaymentApplication());
    }

    @Override
    public String toString() {
        return "PaymentApplicationDTO{" +
            "id=" + getIdPaymentApplication() +
            ", amountApplied='" + getAmountApplied() + "'" +
            "}";
    }
}
