package id.atiila.service.dto;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the BillingDisbursement entity.
 * BeSmart Team
 */

public class BillingDisbursementDTO extends BillingDTO {

    private static final long serialVersionUID = 1L;

    private String vendorInvoice;

    private String vendorId;

    private String vendorName;

    private ZonedDateTime dateIssued;

    private BigDecimal discountLine;

    private  BigDecimal freightCost;

    private Double totalFeightCost;

    public Double getTotalFeightCost() {
        return totalFeightCost;
    }

    public void setTotalFeightCost(Double totalFeightCost) {
        this.totalFeightCost = totalFeightCost;
    }

    public BigDecimal getDiscountLine() {
        return discountLine;
    }

    public void setDiscountLine(BigDecimal discountLine) {
        this.discountLine = discountLine;
    }

    public BigDecimal getFreightCost() {
        return freightCost;
    }

    public void setFreightCost(BigDecimal freightCost) {
        this.freightCost = freightCost;
    }

    public ZonedDateTime getDateIssued() { return dateIssued; }

    public void setDateIssued(ZonedDateTime dateIssued) { this.dateIssued = dateIssued; }

    public String getVendorInvoice() {
        return vendorInvoice;
    }

    public void setVendorInvoice(String vendorInvoice) {
        this.vendorInvoice = vendorInvoice;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() { return vendorName; }

    public void setVendorName(String vendorName) { this.vendorName = vendorName; }

    public UUID getId() {
        return this.getIdBilling();
    }


    @Override
    public String toString() {
        return "BillingDisbursementDTO{" +
            "id=" + getIdBilling() +
            ", billingNumber='" + getBillingNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", dateDue='" + getDateDue() + "'" +
            ", vendorInvoice='" + getVendorInvoice() + "'" +
            "}";
    }
}
