package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the BillingType entity.
 * BeSmart Team
 */

public class BillingTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idBillingType;

    private String description;

    public BillingTypeDTO() {
    }

    public BillingTypeDTO(Integer idBillingType, String description) {
        this.idBillingType = idBillingType;
        this.description = description;
    }

    public Integer getIdBillingType() {
        return this.idBillingType;
    }

    public void setIdBillingType(Integer id) {
        this.idBillingType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idBillingType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BillingTypeDTO billingTypeDTO = (BillingTypeDTO) o;
        if (billingTypeDTO.getIdBillingType() == null || getIdBillingType() == null) {
            return false;
        }
        return Objects.equals(getIdBillingType(), billingTypeDTO.getIdBillingType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdBillingType());
    }

    @Override
    public String toString() {
        return "BillingTypeDTO{" +
            "id=" + getIdBillingType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
