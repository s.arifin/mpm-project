package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.util.UUID;

public class CustomUnitDocumentMessageDTO {
    private Integer idMessage;

    private String content;

    private ZonedDateTime dateCreated;

    private UUID idRequirement;

    private String requirementNumber;

    private String productName;

    private String idProduct;

    private String description;

    private String idFeature;
    public CustomUnitDocumentMessageDTO(Integer idMessage, String content, ZonedDateTime dateCreated,
                                        UUID idRequirement, String requirementNumber, String productName,
                                        String idProduct, String description, String idFeature) {
        this.idMessage = idMessage;
        this.content = content;
        this.dateCreated = dateCreated;
        this.idRequirement = idRequirement;
        this.requirementNumber = requirementNumber;
        this.productName = productName;
        this.idProduct = idProduct;
        this.description = description;
        this.idFeature = idFeature;
    }

    public String getIdFeature() { return idFeature; }

    public void setIdFeature(String idFeature) { this.idFeature = idFeature; }

    public String getIdProduct() { return idProduct; }

    public void setIdProduct(String idProduct) { this.idProduct = idProduct; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public Integer getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(Integer idMessage) {
        this.idMessage = idMessage;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public UUID getIdRequirement() {
        return idRequirement;
    }

    public void setIdRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
    }

    public String getRequirementNumber() {
        return requirementNumber;
    }

    public void setRequirementNumber(String requirementNumber) {
        this.requirementNumber = requirementNumber;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
