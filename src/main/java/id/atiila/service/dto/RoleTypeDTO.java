package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RoleType entity.
 * BeSmart Team
 */

public class RoleTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idRoleType;

    private String description;

    public RoleTypeDTO() {
    }

    public RoleTypeDTO(Integer idRoleType, String description) {
        this.idRoleType = idRoleType;
        this.description = description;
    }

    public Integer getIdRoleType() {
        return this.idRoleType;
    }

    public void setIdRoleType(Integer id) {
        this.idRoleType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idRoleType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RoleTypeDTO roleTypeDTO = (RoleTypeDTO) o;
        if (roleTypeDTO.getIdRoleType() == null || getIdRoleType() == null) {
            return false;
        }
        return Objects.equals(getIdRoleType(), roleTypeDTO.getIdRoleType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRoleType());
    }

    @Override
    public String toString() {
        return "RoleTypeDTO{" +
            "id=" + getIdRoleType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
