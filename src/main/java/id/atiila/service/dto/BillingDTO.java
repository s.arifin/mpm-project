package id.atiila.service.dto;

import org.springframework.data.annotation.Id;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the Billing entity.
 * BeSmart Team
 */

public class BillingDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private UUID idBilling;

    private String billingNumber;

    private String description;

    private ZonedDateTime dateCreate;

    private ZonedDateTime dateDue;

    private Integer currentStatus;

    private Integer billingTypeId;

    private String billingTypeDescription;

    private String internalId;

    private String internalName;

    private String billToId;

    private String billToName;

    private String billFromId;

    private String billFromName;

    private Integer printCounter;

    public Integer getPrintCounter() {
        return printCounter;
    }

    public Integer setPrintCounter(Integer printCounter) {
        this.printCounter = printCounter;
        return printCounter;
    }

    public UUID getIdBilling() {
        return this.idBilling;
    }

    public void setIdBilling(UUID id) {
        this.idBilling = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBillingNumber() {
        return billingNumber;
    }

    public void setBillingNumber(String billingNumber) {
        this.billingNumber = billingNumber;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public ZonedDateTime getDateDue() {
        return dateDue;
    }

    public void setDateDue(ZonedDateTime dateDue) {
        this.dateDue = dateDue;
    }

    public Integer getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(Integer billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getBillingTypeDescription() {
        return billingTypeDescription;
    }

    public void setBillingTypeDescription(String billingTypeDescription) {
        this.billingTypeDescription = billingTypeDescription;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getInternalName() { return internalName; }

    public void setInternalName(String internalName) { this.internalName = internalName; }

    public String getBillToId() {
        return billToId;
    }

    public void setBillToId(String billToId) {
        this.billToId = billToId;
    }

    public String getBillFromId() {
        return billFromId;
    }

    public void setBillFromId(String billToId) {
        this.billFromId = billToId;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getBillToName() {
        return billToName;
    }

    public void setBillToName(String billToName) {
        this.billToName = billToName;
    }

    public String getBillFromName() {
        return billFromName;
    }

    public void setBillFromName(String billFromName) {
        this.billFromName = billFromName;
    }

    public UUID getId() {
        return this.idBilling;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BillingDTO billingDTO = (BillingDTO) o;
        if (billingDTO.getIdBilling() == null || getIdBilling() == null) {
            return false;
        }
        return Objects.equals(getIdBilling(), billingDTO.getIdBilling());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdBilling());
    }

    @Override
    public String toString() {
        return "BillingDTO{" +
            "id=" + getIdBilling() +
            ", billingNumber='" + getBillingNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", dateDue='" + getDateDue() + "'" +
            "}";
    }
}
