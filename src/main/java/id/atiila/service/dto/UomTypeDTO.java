package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the UomType entity.
 * BeSmart Team
 */

public class UomTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idUomType;

    private String description;

    public Integer getIdUomType() {
        return this.idUomType;
    }

    public void setIdUomType(Integer id) {
        this.idUomType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.idUomType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UomTypeDTO uomTypeDTO = (UomTypeDTO) o;
        if (uomTypeDTO.getIdUomType() == null || getIdUomType() == null) {
            return false;
        }
        return Objects.equals(getIdUomType(), uomTypeDTO.getIdUomType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdUomType());
    }

    @Override
    public String toString() {
        return "UomTypeDTO{" +
            "id=" + getIdUomType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
