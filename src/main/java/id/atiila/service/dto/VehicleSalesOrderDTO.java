package id.atiila.service.dto;

import id.atiila.domain.Billing;
import id.atiila.domain.Customer;
import id.atiila.domain.SalesUnitRequirement;
import id.atiila.domain.VehicleSalesOrder;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the VehicleSalesOrder entity.
 * BeSmart Team
 */
@Document(indexName = "vehiclesalesorder")
public class VehicleSalesOrderDTO extends SalesOrderDTO {

    private static final long serialVersionUID = 1L;

    private UUID idSalesUnitRequirement;

    private Integer idSaleType;

    private LocalDate dateBASTUnit;

    private LocalDate dateCustomerReceipt;

    private LocalDate dateSwipeMachine;

    private LocalDate dateCoverNote;

    private LocalDate dateTakePicture;

    private Integer idLeasingStatusPayment;

    private LocalDate afcVeriDate;

    private LocalDate adminSalesVeriDate;

    private String leasingName;

    private String billingNumber;

    private BigDecimal billingAmount;

    private ZonedDateTime dateBilling;

    private CustomerDTO customer = new CustomerDTO();

    private SalesUnitRequirementDTO salesUnitRequirement = new SalesUnitRequirementDTO();

    private LocalDate dateOrderStatusApprove;

    private Integer adminSalesVerifyValue;

    private Integer afcVerifyValue;

    private String adminSalesNotApprvNote;

    private String afcNotApprvNote;

    private String vrnama1;

    private String vrnama2;

    private String vrnamamarket;

    private String vrwarna;

    private String vrtahunproduksi;

    private Double vrangsuran;

    private Double vrtenor;

    private String vrleasing;

    private ZonedDateTime vrtglpengiriman;

    private String vrjampengiriman;

    private String vrtipepenjualan;

    private Double vrdpmurni;

    private Double vrtandajadi;

    private ZonedDateTime vrtglpembayaran;

    private Double vrsisa;

    private Integer vriscod;

    private String vrcatatantambahan;

    private Integer vrisverified;

    private String vrcatatan;

    private ZonedDateTime DateSchedulle;

    private String kacabnote;

    private String idBiroJasa;

    private UUID idSalesman;

    public String getBastFinco() {
        return BastFinco;
    }

    public void setBastFinco(String bastFinco) {
        BastFinco = bastFinco;
    }

    private String BastFinco;

    public ZonedDateTime getFincoy() {
        return fincoy;
    }

    public void setFincoy(ZonedDateTime fincoy) {
        this.fincoy = fincoy;
    }

    private ZonedDateTime fincoy;

    public String statusDesc;

    public String requestNumber;

    private Boolean subFincoyAsDp;

    private BigDecimal refundFinco;

    private Boolean canCloseDP;

    private String salesNumber;

    private String korsalName;

    private Integer downpayment;

    private Number ArLeasing;

    private String name;

    private String identityNumber;

    public Number getArLeasing() {
        return ArLeasing;
    }

    public void setArLeasing(Number arLeasing) {
        ArLeasing = arLeasing;
    }

    public Integer getDownpayment() {
        return downpayment;
    }

    public void setDownpayment(Integer downpayment) {
        this.downpayment = downpayment;
    }

    public String getSalesNumber() {
        return salesNumber;
    }

    public void setSalesNumber(String salesNumber) {
        this.salesNumber = salesNumber;
    }

    public BigDecimal getRefundFinco() {
        return refundFinco;
    }

    public void setRefundFinco(BigDecimal refundFinco) {
        this.refundFinco = refundFinco;
    }

    public Boolean getSubFincoyAsDp() {
        return subFincoyAsDp;
    }

    public void setSubFincoyAsDp(Boolean subFincoyAsDp) {
        this.subFincoyAsDp = subFincoyAsDp;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public UUID getIdSalesman() {
        return idSalesman;
    }

    public void setIdSalesman(UUID idSalesman) {
        this.idSalesman = idSalesman;
    }
    //    private UUID idSales;
//
//    public UUID getIdSales() {
//        return idSales;
//    }
//
//    public void setIdSales(UUID idSales) {
//        this.idSales = idSales;
//    }

    public String getIdBiroJasa() {
        return idBiroJasa;
    }

    public void setIdBiroJasa(String idBiroJasa) {
        this.idBiroJasa = idBiroJasa;
    }

    //    private UUID salesmanId;
//
//    private String salesmanName;

//    public UUID getSalesmanId() {
//        return salesmanId;
//    }
//
//    public void setSalesmanId(UUID salesmanId) {
//        this.salesmanId = salesmanId;
//    }
//
//    public String getSalesmanName() {
//        return salesmanName;
//    }
//
//    public void setSalesmanName(String salesmanName) {
//        this.salesmanName = salesmanName;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public UUID getIdSalesUnitRequirement() {
        return idSalesUnitRequirement;
    }

    public void setIdSalesUnitRequirement(UUID idSalesUnitRequirement) {
        this.idSalesUnitRequirement = idSalesUnitRequirement;
    }

    public String getKacabnote() {
        return kacabnote;
    }

    public void setKacabnote(String kacabnote) {
        this.kacabnote = kacabnote;
    }

    public ZonedDateTime getDateSchedulle() {
        return this.DateSchedulle;
    }

    public String getAfcNotApprvNote() {
        return afcNotApprvNote;
    }

    public void setAfcNotApprvNote(String afcNotApprvNote) {
        this.afcNotApprvNote = afcNotApprvNote;
    }

    public String getAdminSalesNotApprvNote() {
        return adminSalesNotApprvNote;
    }

    public void setAdminSalesNotApprvNote(String adminSalesNotApprvNote) {
        this.adminSalesNotApprvNote = adminSalesNotApprvNote;
    }

    public Integer getAdminSalesVerifyValue() {
        return adminSalesVerifyValue;
    }

    public void setAdminSalesVerifyValue(Integer adminSalesVerifyValue) {
        this.adminSalesVerifyValue = adminSalesVerifyValue;
    }

    public Integer getAfcVerifyValue() {
        return afcVerifyValue;
    }

    public void setAfcVerifyValue(Integer afcVerifyValue) {
        this.afcVerifyValue = afcVerifyValue;
    }

    public LocalDate getDateOrderStatusApprove() {
        return dateOrderStatusApprove;
    }

    public void setDateOrderStatusApprove(LocalDate dateOrderStatusApprove) {
        this.dateOrderStatusApprove = dateOrderStatusApprove;
    }

    public String getLeasingName() {
        return leasingName;
    }

    public void setLeasingName(String leasingName) {
        this.leasingName = leasingName;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public SalesUnitRequirementDTO getSalesUnitRequirement() {
        return salesUnitRequirement;
    }

    public void setSalesUnitRequirement(SalesUnitRequirementDTO salesUnitRequirement) {
        this.salesUnitRequirement = salesUnitRequirement;
    }

    public LocalDate getDateBASTUnit() {
        return dateBASTUnit;
    }

    public void setDateBASTUnit(LocalDate dateBASTUnit) {
        this.dateBASTUnit = dateBASTUnit;
    }

    public LocalDate getDateCustomerReceipt() {
        return dateCustomerReceipt;
    }

    public void setDateCustomerReceipt(LocalDate dateCustomerReceipt) {
        this.dateCustomerReceipt = dateCustomerReceipt;
    }

    public LocalDate getDateSwipeMachine() {
        return dateSwipeMachine;
    }

    public void setDateSwipeMachine(LocalDate dateSwipeMachine) {
        this.dateSwipeMachine = dateSwipeMachine;
    }

    public LocalDate getDateCoverNote() {
        return dateCoverNote;
    }

    public void setDateCoverNote(LocalDate dateCoverNote) {
        this.dateCoverNote = dateCoverNote;
    }

    public LocalDate getDateTakePicture() {
        return dateTakePicture;
    }

    public void setDateTakePicture(LocalDate dateTakePicture) {
        this.dateTakePicture = dateTakePicture;
    }

    public Integer getIdLeasingStatusPayment() {
        return idLeasingStatusPayment;
    }

    public Integer getIdSaleType() {
        return idSaleType;
    }

    public void setIdSaleType(Integer idSaleType) {
        this.idSaleType = idSaleType;
    }

    public void setIdLeasingStatusPayment(Integer idLeasingStatusPayment) {
        this.idLeasingStatusPayment = idLeasingStatusPayment;
    }

    public String getBillingNumber() {
        return billingNumber;
    }

    public BigDecimal getBillingAmount() {
        return billingAmount;
    }

    public ZonedDateTime getDateBilling() {
        return dateBilling;
    }

    public LocalDate getAfcVeriDate() {
        return afcVeriDate;
    }

    public void setAfcVeriDate(LocalDate afcVeriDate) {
        this.afcVeriDate = afcVeriDate;
    }

    public LocalDate getAdminSalesVeriDate() {
        return adminSalesVeriDate;
    }

    public void setAdminSalesVeriDate(LocalDate adminSalesVeriDate) {
        this.adminSalesVeriDate = adminSalesVeriDate;
    }

    public String getVrnama1() {
        return vrnama1;
    }

    public void setVrnama1(String vrnama1) {
        this.vrnama1 = vrnama1;
    }

    public String getVrnama2() {
        return vrnama2;
    }

    public void setVrnama2(String vrnama2) {
        this.vrnama2 = vrnama2;
    }

    public String getVrnamamarket() {
        return vrnamamarket;
    }

    public void setVrnamamarket(String vrnamamarket) {
        this.vrnamamarket = vrnamamarket;
    }

    public String getVrwarna() {
        return vrwarna;
    }

    public void setVrwarna(String vrwarna) {
        this.vrwarna = vrwarna;
    }

    public String getVrtahunproduksi() {
        return vrtahunproduksi;
    }

    public void setVrtahunproduksi(String vrtahunproduksi) {
        this.vrtahunproduksi = vrtahunproduksi;
    }

    public Double getVrangsuran() {
        return vrangsuran;
    }

    public void setVrangsuran(Double vrangsuran) {
        this.vrangsuran = vrangsuran;
    }

    public Double getVrtenor() {
        return vrtenor;
    }

    public void setVrtenor(Double vrtenor) {
        this.vrtenor = vrtenor;
    }

    public String getVrleasing() {
        return vrleasing;
    }

    public void setVrleasing(String vrleasing) {
        this.vrleasing = vrleasing;
    }

    public ZonedDateTime getVrtglpengiriman() {
        return vrtglpengiriman;
    }

    public void setVrtglpengiriman(ZonedDateTime vrtglpengiriman) {
        this.vrtglpengiriman = vrtglpengiriman;
    }

    public String getVrjampengiriman() {
        return vrjampengiriman;
    }

    public void setVrjampengiriman(String vrjampengiriman) {
        this.vrjampengiriman = vrjampengiriman;
    }

    public String getVrtipepenjualan() {
        return vrtipepenjualan;
    }

    public void setVrtipepenjualan(String vrtipepenjualan) {
        this.vrtipepenjualan = vrtipepenjualan;
    }

    public Double getVrdpmurni() {
        return vrdpmurni;
    }

    public void setVrdpmurni(Double vrdpmurni) {
        this.vrdpmurni = vrdpmurni;
    }

    public Double getVrtandajadi() {
        return vrtandajadi;
    }

    public void setVrtandajadi(Double vrtandajadi) {
        this.vrtandajadi = vrtandajadi;
    }

    public ZonedDateTime getVrtglpembayaran() {
        return vrtglpembayaran;
    }

    public void setVrtglpembayaran(ZonedDateTime vrtglpembayaran) {
        this.vrtglpembayaran = vrtglpembayaran;
    }

    public Double getVrsisa() {
        return vrsisa;
    }

    public void setVrsisa(Double vrsisa) {
        this.vrsisa = vrsisa;
    }

    public Integer getVriscod() {
        return vriscod;
    }

    public void setVriscod(Integer vriscod) {
        this.vriscod = vriscod;
    }

    public String getVrcatatantambahan() {
        return vrcatatantambahan;
    }

    public void setVrcatatantambahan(String vrcatatantambahan) {
        this.vrcatatantambahan = vrcatatantambahan;
    }

    public Integer getVrisverified() {
        return vrisverified;
    }

    public void setVrisverified(Integer vrisverified) {
        this.vrisverified = vrisverified;
    }

    public String getVrcatatan() {
        return vrcatatan;
    }

    public void setVrcatatan(String vrcatatan) {
        this.vrcatatan = vrcatatan;
    }

    public void setBillingNumber(String billingNumber) {
        this.billingNumber = billingNumber;
    }

    public void setBillingAmount(BigDecimal billingAmount) {
        this.billingAmount = billingAmount;
    }

    public void setDateBilling(ZonedDateTime dateBilling) {
        this.dateBilling = dateBilling;
    }

    public void setDateSchedulle(ZonedDateTime dateSchedulle) {
        DateSchedulle = dateSchedulle;
    }

    public Boolean getCanCloseDP() {
        return canCloseDP;
    }

    public void setCanCloseDP(Boolean canCloseDP) {
        this.canCloseDP = canCloseDP;
    }

    public String getKorsalName() {
        return korsalName;
    }

    public void setKorsalName(String korsalName) {
        this.korsalName = korsalName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VehicleSalesOrderDTO dto = (VehicleSalesOrderDTO) o;
        if (dto.getIdOrder() == null || getIdOrder() == null) {
            return false;
        }
        if (dto.getCurrentStatus() == 10 || dto.getCurrentStatus() == 11) {
            dto.setStatusDesc("OPEN");
        }
        if (dto.getCurrentStatus() == 13) {
            dto.setStatusDesc("CANCELED");
        }
        if (dto.getCurrentStatus() == 17) {
            dto.setStatusDesc("COMPLETED");
        }
        if (dto.getCurrentStatus() == 74) {
            dto.setStatusDesc("MATCHING");
        }
        if (dto.getCurrentStatus() == 61) {
            dto.setStatusDesc("APPROVE");
        }
        if (dto.getCurrentStatus() == 88) {
            dto.setStatusDesc("WAITING ADMNIN HANDLING");
        }
        return Objects.equals(getIdOrder(), dto.getIdOrder());
    }

    @Override
    public String toString() {
        return "VehicleSalesOrderDTO {" +
            "id=" + getIdOrder() +
            "}";
    }

}
