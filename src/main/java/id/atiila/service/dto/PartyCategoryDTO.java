package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the PartyCategory entity.
 * BeSmart Team
 */

public class PartyCategoryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idCategory;

    private String refKey;

    private String description;

    private Integer categoryTypeId;

    private String categoryTypeDescription;

    public Integer getIdCategory() {
        return this.idCategory;
    }

    public void setIdCategory(Integer id) {
        this.idCategory = id;
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCategoryTypeId() {
        return categoryTypeId;
    }

    public void setCategoryTypeId(Integer partyCategoryTypeId) {
        this.categoryTypeId = partyCategoryTypeId;
    }

    public String getCategoryTypeDescription() {
        return categoryTypeDescription;
    }

    public void setCategoryTypeDescription(String partyCategoryTypeDescription) {
        this.categoryTypeDescription = partyCategoryTypeDescription;
    }

    public Integer getId() {
        return this.idCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PartyCategoryDTO partyCategoryDTO = (PartyCategoryDTO) o;
        if (partyCategoryDTO.getIdCategory() == null || getIdCategory() == null) {
            return false;
        }
        return Objects.equals(getIdCategory(), partyCategoryDTO.getIdCategory());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdCategory());
    }

    @Override
    public String toString() {
        return "PartyCategoryDTO{" +
            "id=" + getIdCategory() +
            ", refKey='" + getRefKey() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
