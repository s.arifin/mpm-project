package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the ShipmentPackage entity.
 * BeSmart Team
 */

public class ShipmentPackageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idPackage;

    private Integer idPackageType;

    private ZonedDateTime dateCreated;

    private Integer currentStatus;

    private String internalId;

    private String internalName;

    public UUID getIdPackage() {
        return this.idPackage;
    }

    public void setIdPackage(UUID id) {
        this.idPackage = id;
    }

    public Integer getIdPackageType() {
        return idPackageType;
    }

    public void setIdPackageType(Integer idPackageType) {
        this.idPackageType = idPackageType;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public UUID getId() {
        return this.idPackage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShipmentPackageDTO shipmentPackageDTO = (ShipmentPackageDTO) o;
        if (shipmentPackageDTO.getIdPackage() == null || getIdPackage() == null) {
            return false;
        }
        return Objects.equals(getIdPackage(), shipmentPackageDTO.getIdPackage());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPackage());
    }

    @Override
    public String toString() {
        return "ShipmentPackageDTO{" +
            "id=" + getIdPackage() +
            ", idPackageType='" + getIdPackageType() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            "}";
    }
}
