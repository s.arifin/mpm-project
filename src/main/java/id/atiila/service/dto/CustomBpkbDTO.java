package id.atiila.service.dto;

import java.io.Serializable;

public class CustomBpkbDTO implements Serializable {

    private String idframe;

    private String stnkNama;

    private String dtStnkReceive;

    private String bpkbNumber;

    private String dtBpkbReceive;

    private String idCustomer;

    private String idCustomerBuyer;

    private String bastStnkNumber;

    private String dtBastStnk;

    private String stnkNumber;

    private String bastBpkbNumber;

    private String dtBastBpkb;

    private String kodeArea;

    private String bbn;

    private String dtFakturReceive;

    private String dtCancel;

    private String dtPlatNomorReceive;

    private String kodeDealer;

    private String transactionCode;

    private String vendorCode;

    public String getIdframe() {
        return idframe;
    }

    public void setIdframe(String idframe) {
        this.idframe = idframe;
    }

    public String getStnkNama() {
        return stnkNama;
    }

    public void setStnkNama(String stnkNama) {
        this.stnkNama = stnkNama;
    }

    public String getDtStnkReceive() {
        return dtStnkReceive;
    }

    public void setDtStnkReceive(String dtStnkReceive) {
        this.dtStnkReceive = dtStnkReceive;
    }

    public String getBpkbNumber() {
        return bpkbNumber;
    }

    public void setBpkbNumber(String bpkbNumber) {
        this.bpkbNumber = bpkbNumber;
    }

    public String getDtBpkbReceive() {
        return dtBpkbReceive;
    }

    public void setDtBpkbReceive(String dtBpkbReceive) {
        this.dtBpkbReceive = dtBpkbReceive;
    }

    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getIdCustomerBuyer() {
        return idCustomerBuyer;
    }

    public void setIdCustomerBuyer(String idCustomerBuyer) {
        this.idCustomerBuyer = idCustomerBuyer;
    }

    public String getBastStnkNumber() {
        return bastStnkNumber;
    }

    public void setBastStnkNumber(String bastStnkNumber) {
        this.bastStnkNumber = bastStnkNumber;
    }

    public String getDtBastStnk() {
        return dtBastStnk;
    }

    public void setDtBastStnk(String dtBastStnk) {
        this.dtBastStnk = dtBastStnk;
    }

    public String getStnkNumber() {
        return stnkNumber;
    }

    public void setStnkNumber(String stnkNumber) {
        this.stnkNumber = stnkNumber;
    }

    public String getBastBpkbNumber() {
        return bastBpkbNumber;
    }

    public void setBastBpkbNumber(String bastBpkbNumber) {
        this.bastBpkbNumber = bastBpkbNumber;
    }

    public String getDtBastBpkb() {
        return dtBastBpkb;
    }

    public void setDtBastBpkb(String dtBastBpkb) {
        this.dtBastBpkb = dtBastBpkb;
    }

    public String getKodeArea() {
        return kodeArea;
    }

    public void setKodeArea(String kodeArea) {
        this.kodeArea = kodeArea;
    }

    public String getBbn() {
        return bbn;
    }

    public void setBbn(String bbn) {
        this.bbn = bbn;
    }

    public String getDtFakturReceive() {
        return dtFakturReceive;
    }

    public void setDtFakturReceive(String dtFakturReceive) {
        this.dtFakturReceive = dtFakturReceive;
    }

    public String getDtCancel() {
        return dtCancel;
    }

    public void setDtCancel(String dtCancel) {
        this.dtCancel = dtCancel;
    }

    public String getDtPlatNomorReceive() {
        return dtPlatNomorReceive;
    }

    public void setDtPlatNomorReceive(String dtPlatNomorReceive) {
        this.dtPlatNomorReceive = dtPlatNomorReceive;
    }

    public String getKodeDealer() {
        return kodeDealer;
    }

    public void setKodeDealer(String kodeDealer) {
        this.kodeDealer = kodeDealer;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    @Override
    public String toString() {
        return "CustomBpkbDTO{" +
            "idframe='" + getIdframe() + "'" +
            ", stnkNama='" + getStnkNama() + "'" +
            ", dtStnkReceive='" + getDtStnkReceive() + "'" +
            ", bpkbNumber='" + getBpkbNumber() + "'" +
            ", dtBpkbReceive='" + getDtBpkbReceive() + "'" +
            ", idCustomer='" + getIdCustomer() + "'" +
            ", idCustomerBuyer='" + getIdCustomerBuyer() + "'" +
            ", bastStnkNumber='" + getBastStnkNumber() + "'" +
            ", dtBastStnk='" + getDtBastStnk() + "'" +
            ", stnkNumber='" + getStnkNumber() + "'" +
            ", bastBpkbNumber='" + getBastBpkbNumber() + "'" +
            ", dtBastBpkb='" + getDtBastBpkb() + "'" +
            ", kodeArea='" + getKodeArea() + "'" +
            ", bbn='" + getBbn() + "'" +
            ", dtFakturReceive='" + getDtFakturReceive() + "'" +
            ", dtCancel='" + getDtCancel() + "'" +
            ", dtPlatNomorReceive='" + getDtPlatNomorReceive() + "'" +
            ", kodeDealer='" + getKodeDealer() + "'" +
            ", transactionCode='" + getTransactionCode() + "'" +
            ", vendorCode='" + getVendorCode() + "'" +
            '}';
    }
}
