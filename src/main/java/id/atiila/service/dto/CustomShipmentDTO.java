package id.atiila.service.dto;
import id.atiila.base.BeanUtil;
import id.atiila.domain.*;
import id.atiila.service.ProductUtils;

import javax.persistence.Entity;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.UUID;

public class CustomShipmentDTO implements Serializable {
    private String billingNumber; //billing
    private String requirementNumber; //sur
    private String idframe; //sur
    private ZonedDateTime DateSchedulle; //shipment
    private String saleTypeDescription; // sur
    private String salesmanName; //sur
    private String coordinatorSales; //sur
    private String driverName;
    private String IVUNumber;
    private ZonedDateTime shipmentCreated;
    private Integer jumlahprint;
    private String orderNumber;
    private Integer yearAssembly;

    private String codeType;
    private String colorDescription;  //sur
    private Boolean acc1; // picking slip
    private Boolean acc2; // picking slip
    private Boolean acc3; // picking slip
    private Boolean acc4; // picking slip
    private Boolean acc5; // picking slip
    private Boolean acc6; // picking slip
    private Boolean acc7; // picking slip
    private Boolean acc8;
    private Boolean acctambah1;
    private Boolean acctambah2;
    private Boolean promat1; // picking slip
    private Boolean promat2; // picking slip

    private String shipmentNumber; // nomor do shipment
    private Integer qty ; //sur
    private Integer currentStatus; // shipment status
    private String deliveryAddress; // shipment outgoing address

    private UUID idShipment; // id shipment
    private UUID idRequirement; // id sur
    private UUID idOrder; // id order,vso
    private String idCustomer; //id customer, vso

    private Integer idInventoryMovementType;
    private String refferenceNumber;

    private UUID idSlip; // id picking slip
    private UUID idOrderItem;
    private String idMachine;
    private Integer idSaleType;

    private UUID idShipmentItem;
    private String productName;
    private UUID idParty;

    private String email;

    private String idInternalsrc;
    private String srcInternalName;
    private String shipmentOutgoingdDescription;
    //Data untuk DO Driver
    private String nameCustomer;
    private String noHp;
    private String address;
    private String city;
    private Integer idfeature;

    public CustomShipmentDTO() {
    }

    public CustomShipmentDTO(VehicleSalesOrder v, Shipment s, PickingSlip pis, ShipmentOutgoing so, InventoryItem ivi) {
        ProductUtils productUtils = (ProductUtils) BeanUtil.getBean("productUtils");
        Feature color = productUtils.getFeatureById(ivi.getIdFeature());

        this.requirementNumber = v.getOrderNumber();
        this.idframe =  ivi.getIdFrame();

        this.DateSchedulle = s.getDateSchedulle();
        this.salesmanName = null ;//v.getSalesUnitRequirement().getSalesman().getPerson().getName();
        this.colorDescription = color != null ? color.getDescription() : null;
        this.acc1 = pis.getAcc1();
        this.acc2 = pis.getAcc2();
        this.acc3 = pis.getAcc3();
        this.acc4 = pis.getAcc4();
        this.acc5 = pis.getAcc5();
        this.acc6 = pis.getAcc6();
        this.acc7 = pis.getAcc7();
        this.acc8 = pis.getAcc8();
        this.acctambah1 = pis.getAcctambah1();
        this.acctambah2 = pis.getAcctambah2();
        this.promat1 = pis.getPromat1();
        this.promat2 = pis.getPromat2();
        this.qty = 1;
        if (v.getBillingNumber() != null)this.IVUNumber = v.getBillingNumber();
        this.currentStatus = s.getCurrentStatus();
        this.idShipment = s.getIdShipment();
        this.idRequirement = v.getIdSalesUnitRequirement();
        this.idOrder = v.getIdOrder();
        this.idSlip = pis.getIdSlip();
        this.idCustomer = v.getCustomer().getIdCustomer();
        this.deliveryAddress = so.getDeliveryAddress();
        this.shipmentNumber = s.getShipmentNumber();
        this.billingNumber = v.getBillingNumber();
        this.shipmentCreated = null ;// v.getSalesUnitRequirement().getDateCreate();
        this.jumlahprint = so.getJumlahprint();
//        this.driverName = so.getDriver().getPerson().getName();
    }

    public Integer getYearAssembly() {
        return yearAssembly;
    }

    public void setYearAssembly(Integer yearAssembly) {
        this.yearAssembly = yearAssembly;
    }

    public Integer getIdfeature() {
        return idfeature;
    }

    public void setIdfeature(Integer idfeature) {
        this.idfeature = idfeature;
    }

    public String getSrcInternalName() {
        return srcInternalName;
    }

    public void setSrcInternalName(String srcInternalName) {
        this.srcInternalName = srcInternalName;
    }
    public Integer getIdInventoryMovementType() {
        return idInventoryMovementType;
    }

    public void setIdInventoryMovementType(Integer idInventoryMovementType) {
        this.idInventoryMovementType = idInventoryMovementType;
    }

    public String getIdInternalsrc() {
        return idInternalsrc;
    }

    public void setIdInternalsrc(String idInternalsrc) {
        this.idInternalsrc = idInternalsrc;
    }
    public String getRefferenceNumber() {
        return refferenceNumber;
    }

    public void setRefferenceNumber(String refferenceNumber) {
        this.refferenceNumber = refferenceNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public UUID getIdParty() {
        return idParty;
    }

    public void setIdParty(UUID idParty) {
        this.idParty = idParty;
    }
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public UUID getIdShipmentItem() {
        return idShipmentItem;
    }

    public void setIdShipmentItem(UUID idShipmentItem) {
        this.idShipmentItem = idShipmentItem;
    }
    public String getShipmentOutgoingdDescription() {
        return shipmentOutgoingdDescription;
    }

    public void setShipmentOutgoingdDescription(String shipmentOutgoingdDescription) {
        this.shipmentOutgoingdDescription = shipmentOutgoingdDescription;
    }
    public String getNameCustomer() {
        return nameCustomer;
    }

    public void setNameCustomer(String nameCustomer) {
        this.nameCustomer = nameCustomer;
    }
    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }
    public ZonedDateTime getShipmentCreated() {
        return shipmentCreated;
    }

    public void setShipmentCreated(ZonedDateTime shipmentCreated) {
        this.shipmentCreated = shipmentCreated;
    }

    public String getIVUNumber() {
        return IVUNumber;
    }

    public void setIVUNumber(String IVUNumber) {
        this.IVUNumber = IVUNumber;
    }

    public String getIdframe() {
        return idframe;
    }

    public void setIdframe(String idframe) {
        this.idframe = idframe;
    }

    public String getCoordinatorSales() {
        return coordinatorSales;
    }

    public void setCoordinatorSales(String coordinatorSales) {
        this.coordinatorSales = coordinatorSales;
    }

    public String getRequirementNumber() {
        return requirementNumber;
    }

    public void setRequirementNumber(String requirementNumber) {
        this.requirementNumber = requirementNumber;
    }

    public ZonedDateTime getDateSchedulle() {
        return DateSchedulle;
    }

    public void setDateSchedulle(ZonedDateTime DateSchedulle) {
        this.DateSchedulle = DateSchedulle;
    }

    public String getSaleTypeDescription() {
        return saleTypeDescription;
    }

    public void setSaleTypeDescription(String saleTypeDescription) {
        this.saleTypeDescription = saleTypeDescription;
    }

    public String getBillingNumber() {
        return billingNumber;
    }

    public void setBillingNumber(String billingNumber) {
        this.billingNumber = billingNumber;
    }

//    public String getSalesmanName() {
//        return salesmanName;
//    }
//
//    public void setSalesmanName(String salesmanName) {
//        this.salesmanName = salesmanName;
//    }

    public String getColorDescription() {
        return colorDescription;
    }

    public void setColorDescription(String colorDescription) {
        this.colorDescription = colorDescription;
    }

    public Boolean getAcc1() {
        return acc1;
    }

    public void setAcc1(Boolean acc1) {
        this.acc1 = acc1;
    }

    public Boolean getAcc2() {
        return acc2;
    }

    public void setAcc2(Boolean acc2) {
        this.acc2 = acc2;
    }

    public Boolean getAcc3() {
        return acc3;
    }

    public void setAcc3(Boolean acc3) {
        this.acc3 = acc3;
    }

    public Boolean getAcc4() {
        return acc4;
    }

    public void setAcc4(Boolean acc4) {
        this.acc4 = acc4;
    }

    public Boolean getAcc5() {
        return acc5;
    }

    public void setAcc5(Boolean acc5) {
        this.acc5 = acc5;
    }

    public Boolean getAcc6() {
        return acc6;
    }

    public void setAcc6(Boolean acc6) {
        this.acc6 = acc6;
    }

    public Boolean getAcc7() {
        return acc7;
    }

    public void setAcc7(Boolean acc7) {
        this.acc7 = acc7;
    }

    public Boolean getAcc8() {
        return acc8;
    }

    public void setAcc8(Boolean acc8) {
        this.acc8 = acc8;
    }

    public Boolean getAcctambah1() {
        return acctambah1;
    }

    public void setAcctambah1(Boolean acctambah1) {
        this.acctambah1 = acctambah1;
    }

    public Boolean getAcctambah2() {
        return acctambah2;
    }

    public void setAcctambah2(Boolean acctambah2) {
        this.acctambah2 = acctambah2;
    }

    public Boolean getPromat1() {
        return promat1;
    }

    public void setPromat1(Boolean promat1) {
        this.promat1 = promat1;
    }

    public Boolean getPromat2() {
        return promat2;
    }

    public void setPromat2(Boolean promat2) {
        this.promat2 = promat2;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public UUID getIdShipment() {
        return idShipment;
    }

    public void setIdShipment(UUID idShipment) {
        this.idShipment = idShipment;
    }

    public UUID getIdRequirement() {
        return idRequirement;
    }

    public void setIdRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
    }

    public UUID getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(UUID idOrder) {
        this.idOrder = idOrder;
    }

    public UUID getIdSlip() {
        return idSlip;
    }

    public void setIdSlip(UUID idSlip) {
        this.idSlip = idSlip;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getShipmentNumber() {
        return shipmentNumber;
    }

    public void setShipmentNumber(String shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
    }

    public Integer getJumlahprint() {
        return jumlahprint;
    }

    public void setJumlahprint(Integer jumlahprint) {
        this.jumlahprint = jumlahprint;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public UUID getIdOrderItem() {
        return idOrderItem;
    }

    public void setIdOrderItem(UUID idOrderItem) {
        this.idOrderItem = idOrderItem;
    }

    public String getIdMachine() {
        return idMachine;
    }

    public void setIdMachine(String idMachine) {
        this.idMachine = idMachine;
    }

    public Integer getIdSaleType() {
        return idSaleType;
    }

    public void setIdSaleType(Integer idSaleType) {
        this.idSaleType = idSaleType;
    }

    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }
}
