package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the PositionFullfillment entity.
 * BeSmart Team
 */

public class PositionFullfillmentDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idPositionFullfillment;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private UUID positionId;

    private UUID personId;

    private String personName;

    public Integer getIdPositionFullfillment() {
        return this.idPositionFullfillment;
    }

    public void setIdPositionFullfillment(Integer id) {
        this.idPositionFullfillment = id;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public UUID getPositionId() {
        return positionId;
    }

    public void setPositionId(UUID positionId) {
        this.positionId = positionId;
    }

    public UUID getPersonId() {
        return personId;
    }

    public void setPersonId(UUID personId) {
        this.personId = personId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public Integer getId() {
        return this.idPositionFullfillment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PositionFullfillmentDTO positionFullfillmentDTO = (PositionFullfillmentDTO) o;
        if (positionFullfillmentDTO.getIdPositionFullfillment() == null || getIdPositionFullfillment() == null) {
            return false;
        }
        return Objects.equals(getIdPositionFullfillment(), positionFullfillmentDTO.getIdPositionFullfillment());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPositionFullfillment());
    }

    @Override
    public String toString() {
        return "PositionFullfillmentDTO{" +
            "id=" + getIdPositionFullfillment() +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
