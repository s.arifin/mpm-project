package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the WorkRequirement entity.
 * BeSmart Team
 */

public class WorkRequirementDTO extends RequirementDTO {

    private static final long serialVersionUID = 1L;

    private Set<WorkServiceRequirementDTO> services = new HashSet<>();
    
    public Set<WorkServiceRequirementDTO> getServices() {
        return services;
    }

    public void setServices(Set<WorkServiceRequirementDTO> workServiceRequirements) {
        this.services = workServiceRequirements;
    }
    
    @Override
    public String toString() {
        return "WorkRequirementDTO{" +
            "id=" + getIdRequirement() +
            "}";
    }
}
