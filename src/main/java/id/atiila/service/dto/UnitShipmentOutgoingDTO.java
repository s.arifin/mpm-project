package id.atiila.service.dto;

import id.atiila.domain.Shipment;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the ShipmentOutgoing entity.
 * atiila consulting
 */

public class UnitShipmentOutgoingDTO extends ShipmentDTO {

    private static final long serialVersionUID = 1L;

    private ZonedDateTime dateBAST;

    private String note;

    private String otherName;

    private String deliveryOption;

    private String deliveryAddress;

    public ZonedDateTime getDateBAST() {
        return dateBAST;
    }

    public void setDateBAST(ZonedDateTime dateBAST) {
        this.dateBAST = dateBAST;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public String getDeliveryOption() {
        return deliveryOption;
    }

    public void setDeliveryOption(String deliveryOption) {
        this.deliveryOption = deliveryOption;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UnitShipmentOutgoingDTO unitShipmentOutgoingDTO = (UnitShipmentOutgoingDTO) o;
        if (unitShipmentOutgoingDTO.getIdShipment() == null || getIdShipment() == null) {
            return false;
        }
        return Objects.equals(getIdShipment(), unitShipmentOutgoingDTO.getIdShipment());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdShipment());
    }

    @Override
    public String toString() {
        return "UnitShipmentOutgoingDTO{" +
            "id=" + getIdShipment() +
            ", shipmentNumber='" + getShipmentNumber() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateSchedulle='" + getDateSchedulle() + "'" +
            ", reason='" + getReason() + "'" +
            ", dateBAST='" + getDateBAST() + "'" +
            ", note='" + getNote() + "'" +
            ", otherName='" + getOtherName() + "'" +
            ", deliveryOption='" + getDeliveryOption() + "'" +
            ", deliveryAddress='" + getDeliveryAddress() + "'" +
            "}";
    }
}
