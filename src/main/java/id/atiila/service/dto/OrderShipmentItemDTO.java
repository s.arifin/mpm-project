package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the OrderShipmentItem entity.
 * BeSmart Team
 */

public class OrderShipmentItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idOrderShipmentItem;

    private Integer qty;

    private UUID orderItemId;

    private UUID shipmentItemId;

    public UUID getIdOrderShipmentItem() {
        return this.idOrderShipmentItem;
    }

    public void setIdOrderShipmentItem(UUID id) {
        this.idOrderShipmentItem = id;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public UUID getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(UUID orderItemId) {
        this.orderItemId = orderItemId;
    }

    public UUID getShipmentItemId() {
        return shipmentItemId;
    }

    public void setShipmentItemId(UUID shipmentItemId) {
        this.shipmentItemId = shipmentItemId;
    }

    public UUID getId() {
        return this.idOrderShipmentItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderShipmentItemDTO orderShipmentItemDTO = (OrderShipmentItemDTO) o;
        if (orderShipmentItemDTO.getIdOrderShipmentItem() == null || getIdOrderShipmentItem() == null) {
            return false;
        }
        return Objects.equals(getIdOrderShipmentItem(), orderShipmentItemDTO.getIdOrderShipmentItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdOrderShipmentItem());
    }

    @Override
    public String toString() {
        return "OrderShipmentItemDTO{" +
            "id=" + getIdOrderShipmentItem() +
            ", qty='" + getQty() + "'" +
            "}";
    }
}
