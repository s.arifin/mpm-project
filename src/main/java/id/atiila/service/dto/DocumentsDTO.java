package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Documents entity.
 * BeSmart Team
 */

public class DocumentsDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idDocument;

    private String note;

    private ZonedDateTime dateCreate;

    private Integer documentTypeId;

    private String documentTypeDescription;

    public UUID getIdDocument() {
        return this.idDocument;
    }

    public void setIdDocument(UUID id) {
        this.idDocument = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Integer getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(Integer documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    public String getDocumentTypeDescription() {
        return documentTypeDescription;
    }

    public void setDocumentTypeDescription(String documentTypeDescription) {
        this.documentTypeDescription = documentTypeDescription;
    }
    
    public UUID getId() {
        return this.idDocument;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DocumentsDTO documentsDTO = (DocumentsDTO) o;
        if (documentsDTO.getIdDocument() == null || getIdDocument() == null) {
            return false;
        }
        return Objects.equals(getIdDocument(), documentsDTO.getIdDocument());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdDocument());
    }

    @Override
    public String toString() {
        return "DocumentsDTO{" +
            "id=" + getIdDocument() +
            ", note='" + getNote() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            "}";
    }
}
