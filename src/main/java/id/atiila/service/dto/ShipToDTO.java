package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the ShipTo entity.
 * BeSmart Team
 */

public class ShipToDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idShipTo;

    private Integer idRoleType;

    private UUID partyId;

    private String partyName;

    public String getIdShipTo() {
        return this.idShipTo;
    }

    public void setIdShipTo(String id) {
        this.idShipTo = id;
    }

    public Integer getIdRoleType() {
        return idRoleType;
    }

    public void setIdRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
    }

    public UUID getPartyId() {
        return partyId;
    }

    public void setPartyId(UUID partyId) {
        this.partyId = partyId;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getId() {
        return this.idShipTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShipToDTO shipToDTO = (ShipToDTO) o;
        if (shipToDTO.getIdShipTo() == null || getIdShipTo() == null) {
            return false;
        }
        return Objects.equals(getIdShipTo(), shipToDTO.getIdShipTo());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdShipTo());
    }

    @Override
    public String toString() {
        return "ShipToDTO{" +
            "id=" + getIdShipTo() +
            ", idRoleType='" + getIdRoleType() + "'" +
            "}";
    }
}
