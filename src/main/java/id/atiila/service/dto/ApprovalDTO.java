package id.atiila.service.dto;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Approval entity.
 * BeSmart Team
 */

public class ApprovalDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idApproval;

    private Integer idApprovalType;

    private Integer idStatusType;

    private Integer idMessage;

    private LocalDateTime dateFrom;

    private LocalDateTime dateThru;

    private UUID idRequirement;

    private UUID idLeasingCompany;

    //getter and setter
    public UUID getIdLeasingCompany() { return idLeasingCompany; }

    public void setIdLeasingCompany(UUID idLeasingCompany) { this.idLeasingCompany = idLeasingCompany; }

    public Integer getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(Integer idMessage) {
        this.idMessage = idMessage;
    }

    public UUID getIdRequirement() {
        return idRequirement;
    }

    public void setIdRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
    }

    public UUID getIdApproval() {
        return this.idApproval;
    }

    public void setIdApproval(UUID id) {
        this.idApproval = id;
    }

    public Integer getIdApprovalType() {
        return idApprovalType;
    }

    public void setIdApprovalType(Integer idApprovalType) {
        this.idApprovalType = idApprovalType;
    }

    public Integer getIdStatusType() {
        return idStatusType;
    }

    public void setIdStatusType(Integer idStatusType) {
        this.idStatusType = idStatusType;
    }

    public LocalDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(LocalDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public UUID getId() {
        return this.idApproval;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ApprovalDTO approvalDTO = (ApprovalDTO) o;
        if (approvalDTO.getIdApproval() == null || getIdApproval() == null) {
            return false;
        }
        return Objects.equals(getIdApproval(), approvalDTO.getIdApproval());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdApproval());
    }

    @Override
    public String toString() {
        return "ApprovalDTO{" +
            "id=" + getIdApproval() +
            ", idApprovalType='" + getIdApprovalType() + "'" +
            ", idStatusType='" + getIdStatusType() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
