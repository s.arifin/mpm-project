package id.atiila.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the RequestRequirement entity.
 * BeSmart Team
 */

public class RequestRequirementDTO extends RequestDTO {

    private static final long serialVersionUID = 1L;

    private Integer qty;

    private UUID idRequirement;

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public UUID getIdRequirement() {
        return idRequirement;
    }

    public void setIdRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
    }

    @Override
    public String toString() {
        return "RequestRequirementDTO{" +
            "id=" + getIdRequest() +
            ", qty='" + getQty() + "'" +
            "}";
    }
}
