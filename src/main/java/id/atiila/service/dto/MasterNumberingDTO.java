package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the MasterNumbering entity.
 * BeSmart Team
 */

public class MasterNumberingDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idNumbering;

    private String idTag;

    private String idValue;

    private Long nextValue;

    public UUID getIdNumbering() {
        return this.idNumbering;
    }

    public void setIdNumbering(UUID id) {
        this.idNumbering = id;
    }

    public String getIdTag() {
        return idTag;
    }

    public void setIdTag(String idTag) {
        this.idTag = idTag;
    }

    public String getIdValue() {
        return idValue;
    }

    public void setIdValue(String idValue) {
        this.idValue = idValue;
    }

    public Long getNextValue() {
        return nextValue;
    }

    public void setNextValue(Long nextValue) {
        this.nextValue = nextValue;
    }
    
    public UUID getId() {
        return this.idNumbering;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MasterNumberingDTO masterNumberingDTO = (MasterNumberingDTO) o;
        if (masterNumberingDTO.getIdNumbering() == null || getIdNumbering() == null) {
            return false;
        }
        return Objects.equals(getIdNumbering(), masterNumberingDTO.getIdNumbering());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdNumbering());
    }

    @Override
    public String toString() {
        return "MasterNumberingDTO{" +
            "id=" + getIdNumbering() +
            ", idTag='" + getIdTag() + "'" +
            ", idValue='" + getIdValue() + "'" +
            ", nextValue='" + getNextValue() + "'" +
            "}";
    }
}
