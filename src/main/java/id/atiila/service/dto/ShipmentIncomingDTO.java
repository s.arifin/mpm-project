package id.atiila.service.dto;

import id.atiila.domain.Shipment;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the ShipmentIncoming entity.
 * BeSmart Team
 */

public class ShipmentIncomingDTO extends ShipmentDTO {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "ShipmentIncomingDTO{" +
            "id=" + getIdShipment() +
            "}";
    }
}
