package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Vehicle entity.
 * BeSmart Team
 */

public class VehicleDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idVehicle;

    private String idMachine;

    private String idFrame;

    private Integer yearOfAssembly;

    private String productId;

    private String productName;

    private Integer featureId;

    private String featureDescription;

    public UUID getIdVehicle() {
        return this.idVehicle;
    }

    public void setIdVehicle(UUID id) {
        this.idVehicle = id;
    }

    public String getIdMachine() {
        return idMachine;
    }

    public void setIdMachine(String idMachine) {
        this.idMachine = idMachine;
    }

    public String getIdFrame() {
        return idFrame;
    }

    public void setIdFrame(String idFrame) {
        this.idFrame = idFrame;
    }

    public Integer getYearOfAssembly() {
        return yearOfAssembly;
    }

    public void setYearOfAssembly(Integer yearOfAssembly) {
        this.yearOfAssembly = yearOfAssembly;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String goodId) {
        this.productId = goodId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String goodName) {
        this.productName = goodName;
    }

    public Integer getFeatureId() {
        return featureId;
    }

    public void setFeatureId(Integer featureId) {
        this.featureId = featureId;
    }

    public String getFeatureDescription() {
        return featureDescription;
    }

    public void setFeatureDescription(String featureDescription) {
        this.featureDescription = featureDescription;
    }

    public UUID getId() {
        return this.idVehicle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VehicleDTO vehicleDTO = (VehicleDTO) o;
        if (vehicleDTO.getIdVehicle() == null || getIdVehicle() == null) {
            return false;
        }
        return Objects.equals(getIdVehicle(), vehicleDTO.getIdVehicle());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdVehicle());
    }

    @Override
    public String toString() {
        return "VehicleDTO{" +
            "id=" + getIdVehicle() +
            ", idMachine='" + getIdMachine() + "'" +
            ", idFrame='" + getIdFrame() + "'" +
            ", yearOfAssembly='" + getYearOfAssembly() + "'" +
            "}";
    }
}
