package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the RequestItem entity.
 * BeSmart Team
 */

public class RequestItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idRequestItem;

    private Integer sequence;

    private String idProduct;

    private Integer idFeature;

    private String featureName;

    private String productName;

    private String description;

    private Double qtyReq;

    private Double qtyTransfer;

    private UUID requestId;

    public Integer getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public UUID getIdRequestItem() {
        return this.idRequestItem;
    }

    public void setIdRequestItem(UUID id) {
        this.idRequestItem = id;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getQtyReq() {
        return qtyReq;
    }

    public void setQtyReq(Double qtyReq) {
        this.qtyReq = qtyReq;
    }

    public Double getQtyTransfer() {
        return qtyTransfer;
    }

    public void setQtyTransfer(Double qtyTransfer) {
        this.qtyTransfer = qtyTransfer;
    }

    public UUID getRequestId() {
        return requestId;
    }

    public void setRequestId(UUID requestId) {
        this.requestId = requestId;
    }

    public UUID getId() {
        return this.idRequestItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequestItemDTO requestItemDTO = (RequestItemDTO) o;
        if (requestItemDTO.getIdRequestItem() == null || getIdRequestItem() == null) {
            return false;
        }
        return Objects.equals(getIdRequestItem(), requestItemDTO.getIdRequestItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRequestItem());
    }

    @Override
    public String toString() {
        return "RequestItemDTO{" +
            "id=" + getIdRequestItem() +
            ", sequence='" + getSequence() + "'" +
            ", idProduct='" + getIdProduct() + "'" +
            ", description='" + getDescription() + "'" +
            ", qtyReq='" + getQtyReq() + "'" +
            ", qtyTransfer='" + getQtyTransfer() + "'" +
            "}";
    }
}
