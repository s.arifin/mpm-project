package id.atiila.service.dto;

/**
 * A DTO for the RemPart entity.
 * BeSmart Team
 */

public class RemPartDTO extends GoodDTO {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "RemPartDTO{" +
            "id=" + getIdProduct() +
            ", name='" + getName() + "'" +
            ", dateIntroduction='" + getDateIntroduction() + "'" +
            "}";
    }
}
