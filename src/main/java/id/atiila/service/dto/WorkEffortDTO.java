package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the WorkEffort entity.
 * BeSmart Team
 */

public class WorkEffortDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idWe;

    private String name;

    private String description;
	
    private Integer currentStatus;

    private Set<PaymentApplicationDTO> payments = new HashSet<>();

    private UUID facilityId;

    private String facilityDescription;

    private Integer workTypeId;

    private String workTypeDescription;

    public UUID getIdWe() {
        return this.idWe;
    }

    public void setIdWe(UUID id) {
        this.idWe = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<PaymentApplicationDTO> getPayments() {
        return payments;
    }

    public void setPayments(Set<PaymentApplicationDTO> paymentApplications) {
        this.payments = paymentApplications;
    }

    public UUID getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(UUID facilityId) {
        this.facilityId = facilityId;
    }

    public String getFacilityDescription() {
        return facilityDescription;
    }

    public void setFacilityDescription(String facilityDescription) {
        this.facilityDescription = facilityDescription;
    }

    public Integer getWorkTypeId() {
        return workTypeId;
    }

    public void setWorkTypeId(Integer workEffortTypeId) {
        this.workTypeId = workEffortTypeId;
    }

    public String getWorkTypeDescription() {
        return workTypeDescription;
    }

    public void setWorkTypeDescription(String workEffortTypeDescription) {
        this.workTypeDescription = workEffortTypeDescription;
    }
    
    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }
    
    public UUID getId() {
        return this.idWe;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WorkEffortDTO workEffortDTO = (WorkEffortDTO) o;
        if (workEffortDTO.getIdWe() == null || getIdWe() == null) {
            return false;
        }
        return Objects.equals(getIdWe(), workEffortDTO.getIdWe());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdWe());
    }

    @Override
    public String toString() {
        return "WorkEffortDTO{" +
            "id=" + getIdWe() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
