package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the PositionReportingStructure entity.
 * BeSmart Team
 */

public class PositionReportingStructureDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idPositionStructure;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private UUID positionFromId;

    private UUID positionToId;

    public Integer getIdPositionStructure() {
        return this.idPositionStructure;
    }

    public void setIdPositionStructure(Integer id) {
        this.idPositionStructure = id;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public UUID getPositionFromId() {
        return positionFromId;
    }

    public void setPositionFromId(UUID positionId) {
        this.positionFromId = positionId;
    }

    public UUID getPositionToId() {
        return positionToId;
    }

    public void setPositionToId(UUID positionId) {
        this.positionToId = positionId;
    }
    
    public Integer getId() {
        return this.idPositionStructure;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PositionReportingStructureDTO positionReportingStructureDTO = (PositionReportingStructureDTO) o;
        if (positionReportingStructureDTO.getIdPositionStructure() == null || getIdPositionStructure() == null) {
            return false;
        }
        return Objects.equals(getIdPositionStructure(), positionReportingStructureDTO.getIdPositionStructure());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPositionStructure());
    }

    @Override
    public String toString() {
        return "PositionReportingStructureDTO{" +
            "id=" + getIdPositionStructure() +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
