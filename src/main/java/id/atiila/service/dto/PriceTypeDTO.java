package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the PriceType entity.
 * BeSmart Team
 */

public class PriceTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idPriceType;

    private String description;

    private Integer ruleTypeId;

    public Integer getIdPriceType() {
        return this.idPriceType;
    }

    public void setIdPriceType(Integer id) {
        this.idPriceType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRuleTypeId() {
        return ruleTypeId;
    }

    public void setRuleTypeId(Integer ruleTypeId) {
        this.ruleTypeId = ruleTypeId;
    }
    
    public Integer getId() {
        return this.idPriceType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PriceTypeDTO priceTypeDTO = (PriceTypeDTO) o;
        if (priceTypeDTO.getIdPriceType() == null || getIdPriceType() == null) {
            return false;
        }
        return Objects.equals(getIdPriceType(), priceTypeDTO.getIdPriceType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPriceType());
    }

    @Override
    public String toString() {
        return "PriceTypeDTO{" +
            "id=" + getIdPriceType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
