package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the GLDimension entity.
 * atiila consulting
 */

public class GLDimensionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idGLDimension;

    private String internalId;

    private String billToId;

    private String vendorId;

    private Integer partyCategoryId;

    private Long productCategoryId;

    private Integer dimension1Id;

    private String dimension1Description;

    private Integer dimension2Id;

    private String dimension2Description;

    private Integer dimension3Id;

    private String dimension3Description;

    private Integer dimension4Id;

    private String dimension4Description;

    private Integer dimension5Id;

    private String dimension5Description;

    private Integer dimension6Id;

    private String dimension6Description;

    public Integer getIdGLDimension() {
        return this.idGLDimension;
    }

    public void setIdGLDimension(Integer id) {
        this.idGLDimension = id;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getBillToId() {
        return billToId;
    }

    public void setBillToId(String billToId) {
        this.billToId = billToId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public Integer getPartyCategoryId() {
        return partyCategoryId;
    }

    public void setPartyCategoryId(Integer partyCategoryId) {
        this.partyCategoryId = partyCategoryId;
    }

    public Long getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(Long productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public Integer getDimension1Id() {
        return dimension1Id;
    }

    public void setDimension1Id(Integer dimensionId) {
        this.dimension1Id = dimensionId;
    }

    public Integer getDimension2Id() {
        return dimension2Id;
    }

    public void setDimension2Id(Integer dimensionId) {
        this.dimension2Id = dimensionId;
    }

    public Integer getDimension3Id() {
        return dimension3Id;
    }

    public void setDimension3Id(Integer dimensionId) {
        this.dimension3Id = dimensionId;
    }

    public Integer getDimension4Id() {
        return dimension4Id;
    }

    public void setDimension4Id(Integer dimensionId) {
        this.dimension4Id = dimensionId;
    }

    public Integer getDimension5Id() {
        return dimension5Id;
    }

    public void setDimension5Id(Integer dimensionId) {
        this.dimension5Id = dimensionId;
    }

    public Integer getDimension6Id() {
        return dimension6Id;
    }

    public void setDimension6Id(Integer dimensionId) {
        this.dimension6Id = dimensionId;
    }

    public String getDimension1Description() {
        return dimension1Description;
    }

    public void setDimension1Description(String dimension1Description) {
        this.dimension1Description = dimension1Description;
    }

    public String getDimension2Description() {
        return dimension2Description;
    }

    public void setDimension2Description(String dimension2Description) {
        this.dimension2Description = dimension2Description;
    }

    public String getDimension3Description() {
        return dimension3Description;
    }

    public void setDimension3Description(String dimension3Description) {
        this.dimension3Description = dimension3Description;
    }

    public String getDimension4Description() {
        return dimension4Description;
    }

    public void setDimension4Description(String dimension4Description) {
        this.dimension4Description = dimension4Description;
    }

    public String getDimension5Description() {
        return dimension5Description;
    }

    public void setDimension5Description(String dimension5Description) {
        this.dimension5Description = dimension5Description;
    }

    public String getDimension6Description() {
        return dimension6Description;
    }

    public void setDimension6Description(String dimension6Description) {
        this.dimension6Description = dimension6Description;
    }

    public Integer getId() {
        return this.idGLDimension;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GLDimensionDTO gLDimensionDTO = (GLDimensionDTO) o;
        if (gLDimensionDTO.getIdGLDimension() == null || getIdGLDimension() == null) {
            return false;
        }
        return Objects.equals(getIdGLDimension(), gLDimensionDTO.getIdGLDimension());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdGLDimension());
    }

    @Override
    public String toString() {
        return "GLDimensionDTO{" +
            "id=" + getIdGLDimension() +
            "}";
    }
}
