package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the CommunicationEvent entity.
 * BeSmart Team
 */

public class CommunicationEventDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idCommunicationEvent;

    private Integer eventTypeId;

    private String eventTypeDescription;

    private String note;

    private Integer currentStatus;

    private Integer purposeId;

    private String purposeDescription;

    public UUID getIdCommunicationEvent() { return this.idCommunicationEvent; }

    public void setIdCommunicationEvent(UUID id) { this.idCommunicationEvent = id; }

    public Integer getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(Integer eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public String getEventTypeDescription() { return eventTypeDescription; }

    public void setEventTypeDescription(String eventTypeDescription) { this.eventTypeDescription = eventTypeDescription; }

    public String getNote() { return note; }

    public void setNote(String note) { this.note = note; }

    public Integer getPurposeId() { return purposeId; }

    public void setPurposeId(Integer purposeTypeId) { this.purposeId = purposeTypeId; }

    public String getPurposeDescription() { return purposeDescription; }

    public void setPurposeDescription(String purposeTypeDescription) {
        this.purposeDescription = purposeTypeDescription;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public UUID getId() {
        return this.idCommunicationEvent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CommunicationEventDTO communicationEventDTO = (CommunicationEventDTO) o;
        if (communicationEventDTO.getIdCommunicationEvent() == null || getIdCommunicationEvent() == null) {
            return false;
        }
        return Objects.equals(getIdCommunicationEvent(), communicationEventDTO.getIdCommunicationEvent());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdCommunicationEvent());
    }

    @Override
    public String toString() {
        return "CommunicationEventDTO{" +
            "id=" + getIdCommunicationEvent() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
