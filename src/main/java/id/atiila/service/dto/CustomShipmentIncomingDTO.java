package id.atiila.service.dto;

import id.atiila.domain.Organization;
import id.atiila.domain.PackageReceipt;
import id.atiila.domain.ShipmentIncoming;

import java.time.ZonedDateTime;
import java.util.UUID;

public class CustomShipmentIncomingDTO {

    private UUID idPackage;

    private String shippingListNumber;

    private ZonedDateTime shippingListDate;

    private UUID idShipment;

    private String spgNumber;

    private ZonedDateTime spgDate;

    private String mainDealerName;

    private String spgLocation;

    private Long qty;

    public CustomShipmentIncomingDTO() { }

    public CustomShipmentIncomingDTO(ShipmentIncoming si, PackageReceipt pr, Long qty) {
        Organization vendor = (Organization) si.getShipFrom().getParty();

        this.idPackage = pr.getIdPackage();
        this.shippingListNumber = pr.getDocumentNumber();
        this.shippingListDate = pr.getDateCreated();
        this.idShipment = si.getIdShipment();
        this.spgNumber = si.getShipmentNumber();
        this.spgDate = si.getDateSchedulle();
        this.mainDealerName = vendor.getName();
        this.spgLocation = si.getShipTo().getParty().getPostalAddress().getDescription();
        this.qty = qty;
    }

    public UUID getIdPackage() { return idPackage; }

    public void setIdPackage(UUID idPackage) { this.idPackage = idPackage; }

    public String getShippingListNumber() { return shippingListNumber; }

    public void setShippingListNumber(String shippingListNumber) { this.shippingListNumber = shippingListNumber; }

    public ZonedDateTime getShippingListDate() { return shippingListDate; }

    public void setShippingListDate(ZonedDateTime shippingListDate) { this.shippingListDate = shippingListDate; }

    public UUID getIdShipment() { return idShipment; }

    public void setIdShipment(UUID idShipment) { this.idShipment = idShipment; }

    public String getSpgNumber() { return spgNumber; }

    public void setSpgNumber(String spgNumber) { this.spgNumber = spgNumber; }

    public ZonedDateTime getSpgDate() { return spgDate; }

    public void setSpgDate(ZonedDateTime spgDate) { this.spgDate = spgDate; }

    public String getMainDealerName() { return mainDealerName; }

    public void setMainDealerName(String mainDealerName) { this.mainDealerName = mainDealerName; }

    public String getSpgLocation() { return spgLocation; }

    public void setSpgLocation(String spgLocation) { this.spgLocation = spgLocation; }

    public Long getQty() { return qty; }

    public void setQty(Long qty) { this.qty = qty; }
}
