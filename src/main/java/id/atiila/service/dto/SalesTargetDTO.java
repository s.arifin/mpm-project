package id.atiila.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the SalesTarget entity.
 */
public class SalesTargetDTO implements Serializable {

    private UUID idslstrgt;

    private UUID idsalesman;

    private String internal;

    private Integer targetsales;

    private ZonedDateTime dtfrom;

    private ZonedDateTime dtthru;

    public String getNamaSales() {
        return namaSales;
    }

    public void setNamaSales(String namaSales) {
        this.namaSales = namaSales;
    }

    private String namaSales;


    public UUID getIdslstrgt() {
        return idslstrgt;
    }

    public void setIdslstrgt(UUID idslstrgt) {
        this.idslstrgt = idslstrgt;
    }

    public UUID getIdsalesman() {
        return idsalesman;
    }

    public void setIdsalesman(UUID idsalesman) {
        this.idsalesman = idsalesman;
    }

    public String getInternal() {
        return internal;
    }

    public void setInternal(String internal) {
        this.internal = internal;
    }

    public Integer getTargetsales() {
        return targetsales;
    }

    public void setTargetsales(Integer targetsales) {
        this.targetsales = targetsales;
    }

    public ZonedDateTime getDtfrom() {
        return dtfrom;
    }

    public void setDtfrom(ZonedDateTime dtfrom) {
        this.dtfrom = dtfrom;
    }

    public ZonedDateTime getDtthru() {
        return dtthru;
    }

    public void setDtthru(ZonedDateTime dtthru) {
        this.dtthru = dtthru;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SalesTargetDTO salesTargetDTO = (SalesTargetDTO) o;
        if(salesTargetDTO.getIdslstrgt() == null || getIdslstrgt() == null) {
            return false;
        }
        return Objects.equals(getIdslstrgt(), salesTargetDTO.getIdslstrgt());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdslstrgt());
    }

    @Override
    public String toString() {
        return "SalesTargetDTO{" +
            ", idslstrgt='" + getIdslstrgt() + "'" +
            ", idsalesman='" + getIdsalesman() + "'" +
            ", internal='" + getInternal() + "'" +
            ", targetsales='" + getTargetsales() + "'" +
            ", dtfrom='" + getDtfrom() + "'" +
            ", dtthru='" + getDtthru() + "'" +
            "}";
    }
}
