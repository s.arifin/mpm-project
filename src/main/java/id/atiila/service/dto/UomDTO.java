package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the Uom entity.
 * BeSmart Team
 */

public class UomDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idUom;

    private String description;

    private String abbreviation;

    private Integer uomTypeId;

    private String uomTypeDescription;

    public String getIdUom() {
        return this.idUom;
    }

    public void setIdUom(String id) {
        this.idUom = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public Integer getUomTypeId() {
        return uomTypeId;
    }

    public void setUomTypeId(Integer uomTypeId) {
        this.uomTypeId = uomTypeId;
    }

    public String getUomTypeDescription() {
        return uomTypeDescription;
    }

    public void setUomTypeDescription(String uomTypeDescription) {
        this.uomTypeDescription = uomTypeDescription;
    }

    public String getId() {
        return this.idUom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UomDTO uomDTO = (UomDTO) o;
        if (uomDTO.getIdUom() == null || getIdUom() == null) {
            return false;
        }
        return Objects.equals(getIdUom(), uomDTO.getIdUom());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdUom());
    }

    @Override
    public String toString() {
        return "UomDTO{" +
            "id=" + getIdUom() +
            ", description='" + getDescription() + "'" +
            ", abbreviation='" + getAbbreviation() + "'" +
            "}";
    }
}
