package id.atiila.service.dto;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the BillingReceipt entity.
 * BeSmart Team
 */

@Document(indexName = "billingreceipt")
public class BillingReceiptDTO extends BillingDTO {

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BillingReceiptDTO billingReceiptDTO = (BillingReceiptDTO) o;
        if (billingReceiptDTO.getIdBilling() == null || getIdBilling() == null) {
            return false;
        }
        return Objects.equals(getIdBilling(), billingReceiptDTO.getIdBilling());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdBilling());
    }

    @Override
    public String toString() {
        return "BillingReceiptDTO{" +
            "id=" + getIdBilling() +
            ", billingNumber='" + getBillingNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", dateDue='" + getDateDue() + "'" +
            "}";
    }
}
