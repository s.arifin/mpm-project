package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the MotorDueReminder entity.
 * BeSmart Team
 */

public class MotorDueReminderDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idReminder;

    private Integer service1;

    private Integer service2;

    private Integer service3;

    private Integer service4;

    private Integer service5;

    private String motorId;

    private String motorName;

    private Integer km;

    private Integer nDays;

    public Integer getIdReminder() {
        return this.idReminder;
    }

    public void setIdReminder(Integer id) {
        this.idReminder = id;
    }

    public Integer getService1() {
        return service1;
    }

    public void setService1(Integer service1) {
        this.service1 = service1;
    }

    public Integer getService2() {
        return service2;
    }

    public void setService2(Integer service2) {
        this.service2 = service2;
    }

    public Integer getService3() {
        return service3;
    }

    public void setService3(Integer service3) {
        this.service3 = service3;
    }

    public Integer getService4() {
        return service4;
    }

    public void setService4(Integer service4) {
        this.service4 = service4;
    }

    public Integer getService5() {
        return service5;
    }

    public void setService5(Integer service5) {
        this.service5 = service5;
    }

    public String getMotorId() {
        return motorId;
    }

    public void setMotorId(String motorId) {
        this.motorId = motorId;
    }

    public String getMotorName() {
        return motorName;
    }

    public void setMotorName(String motorName) {
        this.motorName = motorName;
    }

    public Integer getId() {
        return this.idReminder;
    }

    public Integer getKm() {
        return km;
    }

    public void setKm(Integer km) {
        this.km = km;
    }

    public Integer getnDays() {
        return nDays;
    }

    public void setnDays(Integer nDays) {
        this.nDays = nDays;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MotorDueReminderDTO motorDueReminderDTO = (MotorDueReminderDTO) o;
        if (motorDueReminderDTO.getIdReminder() == null || getIdReminder() == null) {
            return false;
        }
        return Objects.equals(getIdReminder(), motorDueReminderDTO.getIdReminder());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdReminder());
    }

    @Override
    public String toString() {
        return "MotorDueReminderDTO{" +
            "id=" + getIdReminder() +
            ", service1='" + getService1() + "'" +
            ", service2='" + getService2() + "'" +
            ", service3='" + getService3() + "'" +
            ", service4='" + getService4() + "'" +
            ", service5='" + getService5() + "'" +
            "}";
    }
}
