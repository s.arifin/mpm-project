package id.atiila.service.dto;

import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the Request entity.
 * BeSmart Team
 */

public class RequestDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idRequest;

    private String idInternal;

    private String requestNumber;

    private ZonedDateTime dateCreate;

    private ZonedDateTime dateRequest;

    private String description;

    private Integer currentStatus;

    private String currentStatusDescription;

    private Integer requestTypeId;

    private String requestTypeDescription;

    private String internalId;

    private String internalName;

    //getter and setter
    public String getIdInternal() {
        return idInternal;
    }

    public void setIdInternal(String idInternal) {
        this.idInternal = idInternal;
    }

    public String getCurrentStatusDescription() {
        return currentStatusDescription;
    }

    public void setCurrentStatusDescription(String currentStatusDescription) {
        this.currentStatusDescription = currentStatusDescription;
    }

    public UUID getIdRequest() {
        return this.idRequest;
    }

    public void setIdRequest(UUID id) {
        this.idRequest = id;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRequestTypeId() {
        return requestTypeId;
    }

    public void setRequestTypeId(Integer requestTypeId) {
        this.requestTypeId = requestTypeId;
    }

    public String getRequestTypeDescription() {
        return requestTypeDescription;
    }

    public void setRequestTypeDescription(String requestTypeDescription) {
        this.requestTypeDescription = requestTypeDescription;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }


    public ZonedDateTime getDateRequest() {
        return dateRequest;
    }

    public void setDateRequest(ZonedDateTime dateRequest) {
        this.dateRequest = dateRequest;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public UUID getId() {
        return this.idRequest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequestDTO requestDTO = (RequestDTO) o;
        if (requestDTO.getIdRequest() == null || getIdRequest() == null) {
            return false;
        }
        return Objects.equals(getIdRequest(), requestDTO.getIdRequest());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRequest());
    }

    @Override
    public String toString() {
        return "RequestDTO{" +
            "id=" + getIdRequest() +
            ", requestNumber='" + getRequestNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
