package id.atiila.service.dto;

import id.atiila.domain.Uom;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the Motor entity.
 * BeSmart Team
 */

public class MotorDTO extends GoodDTO {

    private static final long serialVersionUID = 1L;

    public MotorDTO() {
    }

    public MotorDTO(String idProduct, String name, String uomId) {
        super(idProduct, name, uomId);
    }

    @Override
    public String toString() {
        return "MotorDTO{" +
            "id=" + getIdProduct() +
            ", name='" + getName() + "'" +
            ", dateIntroduction='" + getDateIntroduction() + "'" +
            "}";
    }
}
