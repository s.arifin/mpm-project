package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the Receipt entity.
 * BeSmart Team
 */

public class ReceiptDTO extends PaymentDTO {

    private static final long serialVersionUID = 1L;

    private UUID idRequirement;

    //getter and setter
    public UUID getIdRequirement() {
        return idRequirement;
    }

    public void setIdRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReceiptDTO receiptDTO = (ReceiptDTO) o;
        if (receiptDTO.getIdPayment() == null || getIdPayment() == null) {
            return false;
        }
        return Objects.equals(getIdPayment(), receiptDTO.getIdPayment());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPayment());
    }

    @Override
    public String toString() {
        return "ReceiptDTO{" +
            "id=" + getIdPayment() +
            ", paymentNumber='" + getPaymentNumber() + "'" +
            ", refferenceNumber='" + getRefferenceNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", amount='" + getAmount() + "'" +
            "}";
    }
}
