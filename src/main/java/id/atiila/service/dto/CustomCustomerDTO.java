package id.atiila.service.dto;

import java.util.Date;
import java.util.Objects;

/**
 * A DTO for the Customer entity.
 * BeSmart Team
 */

public class CustomCustomerDTO extends PersonalCustomerDTO {

    private static final long serialVersionUID = 1L;

    private static final int LEN_ADDRESS = 60;

    private static final int LEN_NAME = 30;

    private String oldCustNo;

    private String jobTitle;

    private String hobby;

    private String lastEducation;

    private String salutation;

    private String accountId;

    private String customerClassId;

    private String accountNumber;

    private String termId;

    private String customerType;

    private String referenceName;

    private String leadSource;

    private String eventSource;

    private String salesmanId;

    private String gender;

    private String name;

    private String fullAddress;

    private Date toDob;

    public CustomCustomerDTO() {
        super();
    }

    public String getOldCustNo() {
        return oldCustNo;
    }

    public void setOldCustNo(String oldCustNo) {
        this.oldCustNo = oldCustNo;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCustomerClassId() {
        return customerClassId;
    }

    public void setCustomerClassId(String customerClassId) {
        this.customerClassId = customerClassId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getReferenceName() {
        return referenceName;
    }

    public void setReferenceName(String referenceName) {
        this.referenceName = referenceName;
    }

    public String getLeadSource() {
        return leadSource;
    }

    public void setLeadSource(String leadSource) {
        this.leadSource = leadSource;
    }

    public String getEventSource() {
        return eventSource;
    }

    public void setEventSource(String eventSource) {
        this.eventSource = eventSource;
    }

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getLastEducation() {
        return lastEducation;
    }

    public void setLastEducation(String lastEducation) {
        this.lastEducation = lastEducation;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        if ("Male".equalsIgnoreCase(gender)) getPerson().setGender("P");
        else getPerson().setGender("W");
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String[] values = name != null ? name.split(" ") : new String[]{};
        if (values.length > 0) {
            getPerson().setFirstName(values[0]);
            StringBuilder sb = new StringBuilder();
            for (int i=1; i < values.length; i++) {
                sb.append(values[i]);
                sb.append(" ");
                if (sb.toString().length() + values[i].length() < LEN_NAME - 1) {
                    getPerson().setLastName(sb.toString().trim());
                }
            }
        }
        this.name = name;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
        if (this.fullAddress != null && this.fullAddress.length() > 0) {
            PostalAddressDTO addr = getPerson().getPostalAddress();
            if (this.fullAddress.length() > LEN_ADDRESS) {
                addr.setAddress1(this.fullAddress.substring(0, LEN_ADDRESS - 1));
                addr.setAddress2(this.fullAddress.substring(LEN_ADDRESS, this.fullAddress.length() - 1));
            } else {
                addr.setAddress1(this.fullAddress.substring(0, this.fullAddress.length() - 1));
            }
        }
    }

    public Date getToDob() {
        return toDob;
    }

    public void setToDob(Date toDob) {
        if (toDob != null) {
            // this.getPerson().setDob(toDob.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        }
        this.toDob = toDob;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomCustomerDTO CustomCustomerDTO = (CustomCustomerDTO) o;
        if (CustomCustomerDTO.getIdCustomer() == null || getIdCustomer() == null) {
            return false;
        }
        return Objects.equals(getIdCustomer(), CustomCustomerDTO.getIdCustomer());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdCustomer());
    }

    @Override
    public String toString() {
        return "CustomCustomerDTO{" +
            "id=" + getIdCustomer() +
            "}";
    }
}
