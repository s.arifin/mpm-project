package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the PartyCategoryType entity.
 * BeSmart Team
 */

public class PartyCategoryTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idCategoryType;

    private String description;

    private String refKey;

    private Integer parentId;

    private String parentDescription;

    public Integer getIdCategoryType() {
        return this.idCategoryType;
    }

    public void setIdCategoryType(Integer id) {
        this.idCategoryType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer partyCategoryTypeId) {
        this.parentId = partyCategoryTypeId;
    }

    public String getParentDescription() {
        return parentDescription;
    }

    public void setParentDescription(String partyCategoryTypeDescription) {
        this.parentDescription = partyCategoryTypeDescription;
    }

    public Integer getId() {
        return this.idCategoryType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PartyCategoryTypeDTO partyCategoryTypeDTO = (PartyCategoryTypeDTO) o;
        if (partyCategoryTypeDTO.getIdCategoryType() == null || getIdCategoryType() == null) {
            return false;
        }
        return Objects.equals(getIdCategoryType(), partyCategoryTypeDTO.getIdCategoryType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdCategoryType());
    }

    @Override
    public String toString() {
        return "PartyCategoryTypeDTO{" +
            "id=" + getIdCategoryType() +
            ", description='" + getDescription() + "'" +
            ", refKey='" + getRefKey() + "'" +
            "}";
    }
}
