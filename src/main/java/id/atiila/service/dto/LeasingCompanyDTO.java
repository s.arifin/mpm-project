package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the LeasingCompany entity.
 * BeSmart Team
 */

public class LeasingCompanyDTO extends PartyRoleDTO {

    private static final long serialVersionUID = 1L;

    private String idLeasingCompany;

    private String idLeasingFincoy;

    private String idLeasingAhm;

    private OrganizationDTO organization = new OrganizationDTO();

    public String getIdLeasingCompany() {
        return idLeasingCompany;
    }

    public OrganizationDTO getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationDTO organization) {
        this.organization = organization;
    }

    public void setIdLeasingCompany(String idLeasingCompany) {
        this.idLeasingCompany = idLeasingCompany;
    }

    public String getIdLeasingFincoy() {
        return idLeasingFincoy;
    }

    public void setIdLeasingFincoy(String idLeasingFincoy) {
        this.idLeasingFincoy = idLeasingFincoy;
    }

    public String getIdLeasingAhm() {
        return idLeasingAhm;
    }

    public void setIdLeasingAhm(String idLeasingAhm) {
        this.idLeasingAhm = idLeasingAhm;
    }

    @Override
    public String toString() {
        return "LeasingCompanyDTO{" +
            "id=" + getIdPartyRole() +
            ", idLeasingCompany='" + getIdLeasingCompany() + "'" +
            "}";
    }
}
