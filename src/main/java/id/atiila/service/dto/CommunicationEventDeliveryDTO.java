package id.atiila.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the CommunicationEventDelivery entity.
 */
public class CommunicationEventDeliveryDTO extends CommunicationEventDTO {

    private String idCustomer;

    private String idDealer;

    private String dealerName;

    private String bussinessCode;

    private String dealerAddress;

    private String b1;

    private String b2;

    private String b3;

    private String b4;

    private String c11Rate;

    private String c11Reason;

    private String c11Hope;

    private String c12Rate;

    private String c12Reason;

    private String c12Hope;

    private String c13Rate;

    private String c13Reason;

    private String c13Hope;

    private String c14Rate;

    private String c14Reason;

    private String c14Hope;

    private String c15Rate;

    private String c15Reason;

    private String c15Hope;

    private String c16Rate;

    private String c16Reason;

    private String c16Hope;

    private String c17Rate;

    private String c17Reason;

    private String c17Hope;

    private String c18Rate;

    private String c18Reason;

    private String c18Hope;

    private String c19Rate;

    private String c19Reason;

    private String c19Hope;

    private String c110Rate;

    private String c110Reason;

    private String c110Hope;

    private String c2Rate;

    private String c3Option;

    private String d1Name;

    private String d2Gender;

    private Integer d3Age;

    private String d4Phone;

    private String d5Vehicle;

    private String d6Education;

    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getIdDealer() {
        return idDealer;
    }

    public void setIdDealer(String idDealer) {
        this.idDealer = idDealer;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getBussinessCode() {
        return bussinessCode;
    }

    public void setBussinessCode(String bussinessCode) {
        this.bussinessCode = bussinessCode;
    }

    public String getDealerAddress() {
        return dealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        this.dealerAddress = dealerAddress;
    }

    public String getb1() {
        return b1;
    }

    public void setb1(String b1) {
        this.b1 = b1;
    }

    public String getb2() {
        return b2;
    }

    public void setb2(String b2) {
        this.b2 = b2;
    }

    public String getb3() {
        return b3;
    }

    public void setb3(String b3) {
        this.b3 = b3;
    }

    public String getb4() {
        return b4;
    }

    public void setb4(String b4) {
        this.b4 = b4;
    }

    public String getc11Rate() {
        return c11Rate;
    }

    public void setc11Rate(String c11Rate) {
        this.c11Rate = c11Rate;
    }

    public String getc11Reason() {
        return c11Reason;
    }

    public void setc11Reason(String c11Reason) {
        this.c11Reason = c11Reason;
    }

    public String getc11Hope() {
        return c11Hope;
    }

    public void setc11Hope(String c11Hope) {
        this.c11Hope = c11Hope;
    }

    public String getc12Rate() {
        return c12Rate;
    }

    public void setc12Rate(String c12Rate) {
        this.c12Rate = c12Rate;
    }

    public String getc12Reason() {
        return c12Reason;
    }

    public void setc12Reason(String c12Reason) {
        this.c12Reason = c12Reason;
    }

    public String getc12Hope() {
        return c12Hope;
    }

    public void setc12Hope(String c12Hope) {
        this.c12Hope = c12Hope;
    }

    public String getc13Rate() {
        return c13Rate;
    }

    public void setc13Rate(String c13Rate) {
        this.c13Rate = c13Rate;
    }

    public String getc13Reason() {
        return c13Reason;
    }

    public void setc13Reason(String c13Reason) {
        this.c13Reason = c13Reason;
    }

    public String getc13Hope() {
        return c13Hope;
    }

    public void setc13Hope(String c13Hope) {
        this.c13Hope = c13Hope;
    }

    public String getc14Rate() {
        return c14Rate;
    }

    public void setc14Rate(String c14Rate) {
        this.c14Rate = c14Rate;
    }

    public String getc14Reason() {
        return c14Reason;
    }

    public void setc14Reason(String c14Reason) {
        this.c14Reason = c14Reason;
    }

    public String getc14Hope() {
        return c14Hope;
    }

    public void setc14Hope(String c14Hope) {
        this.c14Hope = c14Hope;
    }

    public String getc15Rate() {
        return c15Rate;
    }

    public void setc15Rate(String c15Rate) {
        this.c15Rate = c15Rate;
    }

    public String getc15Reason() {
        return c15Reason;
    }

    public void setc15Reason(String c15Reason) {
        this.c15Reason = c15Reason;
    }

    public String getc15Hope() {
        return c15Hope;
    }

    public void setc15Hope(String c15Hope) {
        this.c15Hope = c15Hope;
    }

    public String getc16Rate() {
        return c16Rate;
    }

    public void setc16Rate(String c16Rate) {
        this.c16Rate = c16Rate;
    }

    public String getc16Reason() {
        return c16Reason;
    }

    public void setc16Reason(String c16Reason) {
        this.c16Reason = c16Reason;
    }

    public String getc16Hope() {
        return c16Hope;
    }

    public void setc16Hope(String c16Hope) {
        this.c16Hope = c16Hope;
    }

    public String getc17Rate() {
        return c17Rate;
    }

    public void setc17Rate(String c17Rate) {
        this.c17Rate = c17Rate;
    }

    public String getc17Reason() {
        return c17Reason;
    }

    public void setc17Reason(String c17Reason) {
        this.c17Reason = c17Reason;
    }

    public String getc17Hope() {
        return c17Hope;
    }

    public void setc17Hope(String c17Hope) {
        this.c17Hope = c17Hope;
    }

    public String getc18Rate() {
        return c18Rate;
    }

    public void setc18Rate(String c18Rate) {
        this.c18Rate = c18Rate;
    }

    public String getc18Reason() {
        return c18Reason;
    }

    public void setc18Reason(String c18Reason) {
        this.c18Reason = c18Reason;
    }

    public String getc18Hope() {
        return c18Hope;
    }

    public void setc18Hope(String c18Hope) {
        this.c18Hope = c18Hope;
    }

    public String getc19Rate() {
        return c19Rate;
    }

    public void setc19Rate(String c19Rate) {
        this.c19Rate = c19Rate;
    }

    public String getc19Reason() {
        return c19Reason;
    }

    public void setc19Reason(String c19Reason) {
        this.c19Reason = c19Reason;
    }

    public String getc19Hope() {
        return c19Hope;
    }

    public void setc19Hope(String c19Hope) {
        this.c19Hope = c19Hope;
    }

    public String getc110Rate() {
        return c110Rate;
    }

    public void setc110Rate(String c110Rate) {
        this.c110Rate = c110Rate;
    }

    public String getc110Reason() {
        return c110Reason;
    }

    public void setc110Reason(String c110Reason) {
        this.c110Reason = c110Reason;
    }

    public String getc110Hope() {
        return c110Hope;
    }

    public void setc110Hope(String c110Hope) {
        this.c110Hope = c110Hope;
    }

    public String getc2Rate() {
        return c2Rate;
    }

    public void setc2Rate(String c2Rate) {
        this.c2Rate = c2Rate;
    }

    public String getc3Option() {
        return c3Option;
    }

    public void setc3Option(String c3Option) {
        this.c3Option = c3Option;
    }

    public String getd1Name() {
        return d1Name;
    }

    public void setd1Name(String d1Name) {
        this.d1Name = d1Name;
    }

    public String getd2Gender() {
        return d2Gender;
    }

    public void setd2Gender(String d2Gender) {
        this.d2Gender = d2Gender;
    }

    public Integer getd3Age() {
        return d3Age;
    }

    public void setd3Age(Integer d3Age) {
        this.d3Age = d3Age;
    }

    public String getd4Phone() {
        return d4Phone;
    }

    public void setd4Phone(String d4Phone) {
        this.d4Phone = d4Phone;
    }

    public String getd5Vehicle() {
        return d5Vehicle;
    }

    public void setd5Vehicle(String d5Vehicle) {
        this.d5Vehicle = d5Vehicle;
    }

    public String getd6Education() {
        return d6Education;
    }

    public void setd6Education(String d6Education) {
        this.d6Education = d6Education;
    }

    @Override
    public String toString() {
        return "CommunicationEventDeliveryDTO{" +
            "id=" + getId() +
            ", idCustomer='" + getIdCustomer() + "'" +
            ", idDealer='" + getIdDealer() + "'" +
            ", bussinessCode='" + getBussinessCode() + "'" +
            ", b1='" + getb1() + "'" +
            ", b2='" + getb2() + "'" +
            ", b3='" + getb3() + "'" +
            ", b4='" + getb4() + "'" +
            ", c11Rate='" + getc11Rate() + "'" +
            ", c11Reason='" + getc11Reason() + "'" +
            ", c11Hope='" + getc11Hope() + "'" +
            ", c12Rate='" + getc12Rate() + "'" +
            ", c12Reason='" + getc12Reason() + "'" +
            ", c12Hope='" + getc12Hope() + "'" +
            ", c13Rate='" + getc13Rate() + "'" +
            ", c13Reason='" + getc13Reason() + "'" +
            ", c13Hope='" + getc13Hope() + "'" +
            ", c14Rate='" + getc14Rate() + "'" +
            ", c14Reason='" + getc14Reason() + "'" +
            ", c14Hope='" + getc14Hope() + "'" +
            ", c15Rate='" + getc15Rate() + "'" +
            ", c15Reason='" + getc15Reason() + "'" +
            ", c15Hope='" + getc15Hope() + "'" +
            ", c16Rate='" + getc16Rate() + "'" +
            ", c16Reason='" + getc16Reason() + "'" +
            ", c16Hope='" + getc16Hope() + "'" +
            ", c17Rate='" + getc17Rate() + "'" +
            ", c17Reason='" + getc17Reason() + "'" +
            ", c17Hope='" + getc17Hope() + "'" +
            ", c18Rate='" + getc18Rate() + "'" +
            ", c18Reason='" + getc18Reason() + "'" +
            ", c18Hope='" + getc18Hope() + "'" +
            ", c19Rate='" + getc19Rate() + "'" +
            ", c19Reason='" + getc19Reason() + "'" +
            ", c19Hope='" + getc19Hope() + "'" +
            ", c110Rate='" + getc110Rate() + "'" +
            ", c110Reason='" + getc110Reason() + "'" +
            ", c110Hope='" + getc110Hope() + "'" +
            ", c2Rate='" + getc2Rate() + "'" +
            ", c3Option='" + getc3Option() + "'" +
            ", d1Name='" + getd1Name() + "'" +
            ", d2Gender='" + getd2Gender() + "'" +
            ", d3Age='" + getd3Age() + "'" +
            ", d4Phone='" + getd4Phone() + "'" +
            ", d5Vehicle='" + getd5Vehicle() + "'" +
            ", d6Education='" + getd6Education() + "'" +
            "}";
    }
}
