package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the CustomerRelationship entity.
 * atiila consulting
 */

public class CustomerRelationshipDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idPartyRelationship;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private Integer statusTypeId;

    private String statusTypeDescription;

    private Integer relationTypeId;

    private String relationTypeDescription;

    private String internalId;

    private String customerId;

    public UUID getIdPartyRelationship() {
        return this.idPartyRelationship;
    }

    public void setIdPartyRelationship(UUID id) {
        this.idPartyRelationship = id;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Integer getStatusTypeId() {
        return statusTypeId;
    }

    public void setStatusTypeId(Integer statusTypeId) {
        this.statusTypeId = statusTypeId;
    }

    public String getStatusTypeDescription() {
        return statusTypeDescription;
    }

    public void setStatusTypeDescription(String statusTypeDescription) {
        this.statusTypeDescription = statusTypeDescription;
    }

    public Integer getRelationTypeId() {
        return relationTypeId;
    }

    public void setRelationTypeId(Integer relationTypeId) {
        this.relationTypeId = relationTypeId;
    }

    public String getRelationTypeDescription() {
        return relationTypeDescription;
    }

    public void setRelationTypeDescription(String relationTypeDescription) {
        this.relationTypeDescription = relationTypeDescription;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public UUID getId() {
        return this.idPartyRelationship;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomerRelationshipDTO customerRelationshipDTO = (CustomerRelationshipDTO) o;
        if (customerRelationshipDTO.getIdPartyRelationship() == null || getIdPartyRelationship() == null) {
            return false;
        }
        return Objects.equals(getIdPartyRelationship(), customerRelationshipDTO.getIdPartyRelationship());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPartyRelationship());
    }

    @Override
    public String toString() {
        return "CustomerRelationshipDTO{" +
            "id=" + getIdPartyRelationship() +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
