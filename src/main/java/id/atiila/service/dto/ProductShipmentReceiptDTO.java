package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the ProductShipmentReceipt entity.
 * BeSmart Team
 */

public class ProductShipmentReceiptDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idReceipt;

    private String idProduct;

    private Integer idFeature;

    private String receiptCode;

    private Integer qtyAccept;

    private Integer qtyReject;

    private String itemDescription;

    private ZonedDateTime dateReceipt;

    private UUID shipmentPackageId;

    private UUID shipmentItemId;

    private UUID orderItemId;

    public UUID getIdReceipt() {
        return this.idReceipt;
    }

    public void setIdReceipt(UUID id) {
        this.idReceipt = id;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public String getReceiptCode() {
        return receiptCode;
    }

    public void setReceiptCode(String receiptCode) {
        this.receiptCode = receiptCode;
    }

    public Integer getQtyAccept() {
        return qtyAccept;
    }

    public void setQtyAccept(Integer qtyAccept) {
        this.qtyAccept = qtyAccept;
    }

    public Integer getQtyReject() {
        return qtyReject;
    }

    public void setQtyReject(Integer qtyReject) {
        this.qtyReject = qtyReject;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public ZonedDateTime getDateReceipt() {
        return dateReceipt;
    }

    public void setDateReceipt(ZonedDateTime dateReceipt) {
        this.dateReceipt = dateReceipt;
    }

    public UUID getShipmentPackageId() {
        return shipmentPackageId;
    }

    public void setShipmentPackageId(UUID shipmentPackageId) {
        this.shipmentPackageId = shipmentPackageId;
    }

    public UUID getShipmentItemId() {
        return shipmentItemId;
    }

    public void setShipmentItemId(UUID shipmentItemId) {
        this.shipmentItemId = shipmentItemId;
    }

    public UUID getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(UUID orderItemId) {
        this.orderItemId = orderItemId;
    }

    public UUID getId() {
        return this.idReceipt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductShipmentReceiptDTO productShipmentReceiptDTO = (ProductShipmentReceiptDTO) o;
        if (productShipmentReceiptDTO.getIdReceipt() == null || getIdReceipt() == null) {
            return false;
        }
        return Objects.equals(getIdReceipt(), productShipmentReceiptDTO.getIdReceipt());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdReceipt());
    }

    @Override
    public String toString() {
        return "ProductShipmentReceiptDTO{" +
            "id=" + getIdReceipt() +
            ", idProduct='" + getIdProduct() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", receiptCode='" + getReceiptCode() + "'" +
            ", qtyAccept='" + getQtyAccept() + "'" +
            ", qtyReject='" + getQtyReject() + "'" +
            ", itemDescription='" + getItemDescription() + "'" +
            ", dateReceipt='" + getDateReceipt() + "'" +
            "}";
    }
}
