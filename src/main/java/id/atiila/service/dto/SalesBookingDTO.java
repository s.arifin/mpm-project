package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the SalesBooking entity.
 * BeSmart Team
 */

public class SalesBookingDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idSalesBooking;

    private String idInternal;

    private String idProduct;

    private Integer idFeature;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private UUID requirementId;

    private String note;

    private Integer yearAssembly;

    private Boolean onHand = false;

    private Boolean inTransit = false;

    //getter and setter
    public Boolean getOnHand() {
        return onHand;
    }

    public void setOnHand(Boolean onHand) {
        this.onHand = onHand;
    }

    public Boolean getInTransit() {
        return inTransit;
    }

    public void setInTransit(Boolean inTransit) {
        this.inTransit = inTransit;
    }

    public Integer getYearAssembly() {
        return yearAssembly;
    }

    public void setYearAssembly(Integer yearAssembly) {
        this.yearAssembly = yearAssembly;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public UUID getIdSalesBooking() {
        return this.idSalesBooking;
    }

    public void setIdSalesBooking(UUID id) {
        this.idSalesBooking = id;
    }

    public String getIdInternal() {
        return idInternal;
    }

    public void setIdInternal(String idInternal) {
        this.idInternal = idInternal;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public UUID getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(UUID requirementId) {
        this.requirementId = requirementId;
    }

    public UUID getId() {
        return this.idSalesBooking;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SalesBookingDTO salesBookingDTO = (SalesBookingDTO) o;
        if (salesBookingDTO.getIdSalesBooking() == null || getIdSalesBooking() == null) {
            return false;
        }
        return Objects.equals(getIdSalesBooking(), salesBookingDTO.getIdSalesBooking());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdSalesBooking());
    }

    @Override
    public String toString() {
        return "SalesBookingDTO{" +
            "id=" + getIdSalesBooking() +
            ", idInternal='" + getIdInternal() + "'" +
            ", idProduct='" + getIdProduct() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
