package id.atiila.service.dto;

import org.springframework.data.elasticsearch.annotations.Document;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Vendor entity.
 * BeSmart Team
 */

@Document(indexName = "vendordto")
public class VendorDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idVendor;

    private Integer idRoleType;

    private Integer idVendorType;

    private Boolean isMainDealer;

    private Boolean isBiroJasa;

    private OrganizationDTO organization = new OrganizationDTO();

    public VendorDTO() {
    }

    public VendorDTO(String idVendor, String partyName) {
        this.idVendor = idVendor;
        this.getOrganization().setName(partyName);
    }

    public Integer getIdVendorType() {
        return idVendorType;
    }

    public void setIdVendorType(Integer idVendorType) {
        this.idVendorType = idVendorType;
    }

    public Boolean getMainDealer() {
        return isMainDealer;
    }

    public void setMainDealer(Boolean mainDealer) {
        isMainDealer = mainDealer;
    }

    public Boolean getBiroJasa() {
        return isBiroJasa;
    }

    public void setBiroJasa(Boolean biroJasa) {
        isBiroJasa = biroJasa;
    }

    public String getIdVendor() {
        return idVendor;
    }

    public void setIdVendor(String idVendor) {
        this.idVendor = idVendor;
    }

    public OrganizationDTO getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationDTO organization) {
        this.organization = organization;
    }

    public Integer getIdRoleType() {
        return idRoleType;
    }

    public void setIdRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
    }

    @Override
    public String toString() {
        return "VendorDTO{" +
            "idVendor='" + getIdVendor() + "'" +
            "}";
    }
}
