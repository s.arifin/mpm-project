package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the BookingType entity.
 * BeSmart Team
 */

public class BookingTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idBookingType;

    private String description;
    
    public BookingTypeDTO() {
    }
    
    public BookingTypeDTO(Integer idBookingType, String description) {
        this.idBookingType = idBookingType;
        this.description = description;
    }
    
    public Integer getIdBookingType() {
        return this.idBookingType;
    }

    public void setIdBookingType(Integer id) {
        this.idBookingType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idBookingType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BookingTypeDTO bookingTypeDTO = (BookingTypeDTO) o;
        if (bookingTypeDTO.getIdBookingType() == null || getIdBookingType() == null) {
            return false;
        }
        return Objects.equals(getIdBookingType(), bookingTypeDTO.getIdBookingType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdBookingType());
    }

    @Override
    public String toString() {
        return "BookingTypeDTO{" +
            "id=" + getIdBookingType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
