package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the WorkProductRequirement entity.
 * BeSmart Team
 */

public class WorkProductRequirementDTO extends RequirementDTO {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "WorkProductRequirementDTO{" +
            "id=" + getIdRequirement() +
            "}";
    }
}
