package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the StandardCalendar entity.
 * BeSmart Team
 */

public class StandardCalendarDTO extends BaseCalendarDTO {

    private static final long serialVersionUID = 1L;

    private Boolean workDay;

    private String internalId;

    public Boolean isWorkDay() {
        return workDay;
    }

    public void setWorkDay(Boolean workDay) {
        this.workDay = workDay;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StandardCalendarDTO standardCalendarDTO = (StandardCalendarDTO) o;
        if (standardCalendarDTO.getIdCalendar() == null || getIdCalendar() == null) {
            return false;
        }
        return Objects.equals(getIdCalendar(), standardCalendarDTO.getIdCalendar());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdCalendar());
    }

    @Override
    public String toString() {
        return "StandardCalendarDTO{" +
            "id=" + getIdCalendar() +
            ", workDay='" + isWorkDay() + "'" +
            "}";
    }
}
