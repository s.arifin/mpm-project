package id.atiila.service.dto;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.UUID;

/**
 * A DTO for the Organization entity.
 * BeSmart Team
 */

public class OrganizationDTO extends PartyDTO {

    private static final long serialVersionUID = 1L;

    private String numberTDP;

    private ZonedDateTime dateOrganizationEstablish;

    private PostalAddressDTO postalAddress = new PostalAddressDTO();

    private String officeMail;

    private String officePhone;

    private String officeFax;

    private UUID idPic;

    private String npwp;

    private String picName;

    private String picPhone;

    private String picMail;

    public String getPicName() {
        return picName;
    }

    public void setPicName(String picName) {
        this.picName = picName;
    }

    public String getPicPhone() {
        return picPhone;
    }

    public void setPicPhone(String picPhone) {
        this.picPhone = picPhone;
    }

    public String getPicMail() {
        return picMail;
    }

    public void setPicMail(String picMail) {
        this.picMail = picMail;
    }

    public String getNumberTDP(){return numberTDP;}
    public void setNumberTDP(String numberTDP){ this.numberTDP = numberTDP; }

    public ZonedDateTime getDateOrganizationEstablish(){return dateOrganizationEstablish;}
    public void setDateOrganizationEstablish(ZonedDateTime dateOrganizationEstablish) {this.dateOrganizationEstablish = dateOrganizationEstablish;}

    public OrganizationDTO() {
    }

    public String getOfficeMail() {
        return officeMail;
    }

    public void setOfficeMail(String officeMail) {
        this.officeMail = officeMail;
    }

    public String getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    public String getOfficeFax() {
        return officeFax;
    }

    public void setOfficeFax(String officeFax) {
        this.officeFax = officeFax;
    }

    public OrganizationDTO(String name) {
        super();
        setName(name);
    }

    public PostalAddressDTO getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(PostalAddressDTO postalAddress) {
        this.postalAddress = postalAddress;
    }

    public UUID getIdPic() {
        return idPic;
    }

    public void setIdPic(UUID idPic) {
        this.idPic = idPic;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    @Override
    public String toString() {
        return "OrganizationDTO{" +
            "id=" + getIdParty() +
            ", name='" + getName() + "'" +
            ", numberTDP='" +getNumberTDP() + "'" +
            ", dateOrganizationEstablish='" +getDateOrganizationEstablish() + "'" +
            "}";
    }
}
