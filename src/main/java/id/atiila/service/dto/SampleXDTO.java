package id.atiila.service.dto;

import id.atiila.domain.PickingSlip;
import id.atiila.domain.Shipment;
import id.atiila.domain.VehicleSalesOrder;

import javax.persistence.Entity;
import java.io.Serializable;

public class SampleXDTO implements Serializable {

    private String idShipment;

    private Boolean acc1;

    private Boolean acc2;

    private Boolean acc3;

    public SampleXDTO() {
    }

    public SampleXDTO(VehicleSalesOrder v, Shipment s, PickingSlip pis) {
        this.idShipment = s.getShipmentNumber();
        this.acc1 = pis.getAcc1();
    }

    public Boolean getAcc1() {
        return acc1;
    }

    public void setAcc1(Boolean acc1) {
        this.acc1 = acc1;
    }

    public Boolean getAcc2() {
        return acc2;
    }

    public void setAcc2(Boolean acc2) {
        this.acc2 = acc2;
    }

    public Boolean getAcc3() {
        return acc3;
    }

    public void setAcc3(Boolean acc3) {
        this.acc3 = acc3;
    }

    public String getIdShipment() {
        return idShipment;
    }

    public void setIdShipment(String idShipment) {
        this.idShipment = idShipment;
    }

}
