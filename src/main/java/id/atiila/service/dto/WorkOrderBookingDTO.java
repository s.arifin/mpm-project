package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the WorkOrderBooking entity.
 * BeSmart Team
 */

public class WorkOrderBookingDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idBooking;

    private String bookingNumber;

    private String vehicleNumber;

    private ZonedDateTime dateCreate;

    private Integer currentStatus;

    private UUID slotId;

    private String slotSlotNumber;

    private Integer bookingTypeId;

    private String bookingTypeDescription;

    private Integer eventTypeId;

    private String eventTypeDescription;

    private VehicleDTO vehicle = new VehicleDTO();

    private PersonalCustomerDTO customer = new PersonalCustomerDTO();

    private String internalId;

    public UUID getIdBooking() {
        return this.idBooking;
    }

    public void setIdBooking(UUID id) {
        this.idBooking = id;
    }

    public String getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public UUID getSlotId() {
        return slotId;
    }

    public void setSlotId(UUID bookingSlotId) {
        this.slotId = bookingSlotId;
    }

    public String getSlotSlotNumber() {
        return slotSlotNumber;
    }

    public void setSlotSlotNumber(String bookingSlotSlotNumber) {
        this.slotSlotNumber = bookingSlotSlotNumber;
    }

    public Integer getBookingTypeId() {
        return bookingTypeId;
    }

    public void setBookingTypeId(Integer bookingTypeId) {
        this.bookingTypeId = bookingTypeId;
    }

    public String getBookingTypeDescription() {
        return bookingTypeDescription;
    }

    public void setBookingTypeDescription(String bookingTypeDescription) {
        this.bookingTypeDescription = bookingTypeDescription;
    }

    public Integer getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(Integer eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public String getEventTypeDescription() {
        return eventTypeDescription;
    }

    public void setEventTypeDescription(String eventTypeDescription) {
        this.eventTypeDescription = eventTypeDescription;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public VehicleDTO getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleDTO vehicle) {
        this.vehicle = vehicle;
    }

    public PersonalCustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(PersonalCustomerDTO customer) {
        this.customer = customer;
    }

    public UUID getId() {
        return this.idBooking;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WorkOrderBookingDTO workOrderBookingDTO = (WorkOrderBookingDTO) o;
        if (workOrderBookingDTO.getIdBooking() == null || getIdBooking() == null) {
            return false;
        }
        return Objects.equals(getIdBooking(), workOrderBookingDTO.getIdBooking());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdBooking());
    }

    @Override
    public String toString() {
        return "WorkOrderBookingDTO{" +
            "id=" + getIdBooking() +
            ", bookingNumber='" + getBookingNumber() + "'" +
            ", vehicleNumber='" + getVehicleNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            "}";
    }
}
