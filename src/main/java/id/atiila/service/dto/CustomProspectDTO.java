package id.atiila.service.dto;


import java.io.Serializable;
import java.util.UUID;

public class CustomProspectDTO implements Serializable {

    private UUID salesmanId;

    private String salesmanName;

    private Long hotCount;

    private Long mediumCount;

    private Long lowCount;

    public UUID getSalesmanId() { return salesmanId; }

    public void setSalesmanId(UUID salesmanId) { this.salesmanId = salesmanId; }

    public String getSalesmanName() { return salesmanName; }

    public void setSalesmanName(String salesmanName) { this.salesmanName = salesmanName; }

    public Long getHotCount() { return hotCount; }

    public void setHotCount(Long hotCount) { this.hotCount = hotCount; }

    public Long getMediumCount() { return mediumCount; }

    public void setMediumCount(Long mediumCount) { this.mediumCount = mediumCount; }

    public Long getLowCount() { return lowCount; }

    public void setLowCount(Long lowCount) { this.lowCount = lowCount; }
}
