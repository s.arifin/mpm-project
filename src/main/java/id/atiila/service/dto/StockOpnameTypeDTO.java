package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the StockOpnameType entity.
 * BeSmart Team
 */

public class StockOpnameTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idStockOpnameType;

    private String description;

    public Integer getIdStockOpnameType() {
        return this.idStockOpnameType;
    }

    public void setIdStockOpnameType(Integer id) {
        this.idStockOpnameType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.idStockOpnameType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StockOpnameTypeDTO stockOpnameTypeDTO = (StockOpnameTypeDTO) o;
        if (stockOpnameTypeDTO.getIdStockOpnameType() == null || getIdStockOpnameType() == null) {
            return false;
        }
        return Objects.equals(getIdStockOpnameType(), stockOpnameTypeDTO.getIdStockOpnameType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdStockOpnameType());
    }

    @Override
    public String toString() {
        return "StockOpnameTypeDTO{" +
            "id=" + getIdStockOpnameType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
