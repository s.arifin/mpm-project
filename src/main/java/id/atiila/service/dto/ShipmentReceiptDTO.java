package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the ShipmentReceipt entity.
 * BeSmart Team
 */

public class ShipmentReceiptDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idReceipt;

    private String idProduct;

    private Integer idFeature;

    private String receiptCode;

    private Double qtyAccept;

    private Double qtyReject;

    private String itemDescription;

    private ZonedDateTime dateReceipt;

    private Integer currentStatus;

    private UUID shipmentPackageId;

    private UUID shipmentItemId;

    private UUID orderItemId;

    private Integer featureId;

    private String featureDescription;

    private String featureRefKey;

    public UUID getIdReceipt() {
        return this.idReceipt;
    }

    public void setIdReceipt(UUID id) {
        this.idReceipt = id;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public String getReceiptCode() {
        return receiptCode;
    }

    public void setReceiptCode(String receiptCode) {
        this.receiptCode = receiptCode;
    }

    public Double getQtyAccept() {
        return qtyAccept;
    }

    public void setQtyAccept(Double qtyAccept) {
        this.qtyAccept = qtyAccept;
    }

    public Double getQtyReject() {
        return qtyReject;
    }

    public void setQtyReject(Double qtyReject) {
        this.qtyReject = qtyReject;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public ZonedDateTime getDateReceipt() {
        return dateReceipt;
    }

    public void setDateReceipt(ZonedDateTime dateReceipt) {
        this.dateReceipt = dateReceipt;
    }

    public UUID getShipmentPackageId() {
        return shipmentPackageId;
    }

    public void setShipmentPackageId(UUID shipmentPackageId) {
        this.shipmentPackageId = shipmentPackageId;
    }

    public UUID getShipmentItemId() {
        return shipmentItemId;
    }

    public void setShipmentItemId(UUID shipmentItemId) {
        this.shipmentItemId = shipmentItemId;
    }

    public UUID getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(UUID orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Integer getFeatureId() { return featureId; }

    public void setFeatureId(Integer featureId) { this.featureId = featureId; }

    public String getFeatureDescription() { return featureDescription; }

    public void setFeatureDescription(String featureDescription) { this.featureDescription = featureDescription; }

    public String getFeatureRefKey() { return featureRefKey; }

    public void setFeatureRefKey(String featureRefKey) { this.featureRefKey = featureRefKey; }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public UUID getId() {
        return this.idReceipt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShipmentReceiptDTO shipmentReceiptDTO = (ShipmentReceiptDTO) o;
        if (shipmentReceiptDTO.getIdReceipt() == null || getIdReceipt() == null) {
            return false;
        }
        return Objects.equals(getIdReceipt(), shipmentReceiptDTO.getIdReceipt());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdReceipt());
    }

    @Override
    public String toString() {
        return "ShipmentReceiptDTO{" +
            "id=" + getIdReceipt() +
            ", idProduct='" + getIdProduct() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", receiptCode='" + getReceiptCode() + "'" +
            ", qtyAccept='" + getQtyAccept() + "'" +
            ", qtyReject='" + getQtyReject() + "'" +
            ", itemDescription='" + getItemDescription() + "'" +
            ", dateReceipt='" + getDateReceipt() + "'" +
            "}";
    }
}
