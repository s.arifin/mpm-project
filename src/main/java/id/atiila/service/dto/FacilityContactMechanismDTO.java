package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the FacilityContactMechanism entity.
 * BeSmart Team
 */

public class FacilityContactMechanismDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idContactMechPurpose;

    private Integer idPurposeType;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private UUID facilityId;

    private String facilityDescription;

    private UUID contactId;

    private String contactDescription;

    public UUID getIdContactMechPurpose() {
        return this.idContactMechPurpose;
    }

    public void setIdContactMechPurpose(UUID id) {
        this.idContactMechPurpose = id;
    }

    public Integer getIdPurposeType() {
        return idPurposeType;
    }

    public void setIdPurposeType(Integer idPurposeType) {
        this.idPurposeType = idPurposeType;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public UUID getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(UUID facilityId) {
        this.facilityId = facilityId;
    }

    public String getFacilityDescription() {
        return facilityDescription;
    }

    public void setFacilityDescription(String facilityDescription) {
        this.facilityDescription = facilityDescription;
    }

    public UUID getContactId() {
        return contactId;
    }

    public void setContactId(UUID contactMechanismId) {
        this.contactId = contactMechanismId;
    }

    public String getContactDescription() {
        return contactDescription;
    }

    public void setContactDescription(String contactMechanismDescription) {
        this.contactDescription = contactMechanismDescription;
    }

    public UUID getId() {
        return this.idContactMechPurpose;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FacilityContactMechanismDTO facilityContactMechanismDTO = (FacilityContactMechanismDTO) o;
        if (facilityContactMechanismDTO.getIdContactMechPurpose() == null || getIdContactMechPurpose() == null) {
            return false;
        }
        return Objects.equals(getIdContactMechPurpose(), facilityContactMechanismDTO.getIdContactMechPurpose());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdContactMechPurpose());
    }

    @Override
    public String toString() {
        return "FacilityContactMechanismDTO{" +
            "id=" + getIdContactMechPurpose() +
            ", idPurposeType='" + getIdPurposeType() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
