package id.atiila.service.dto;

import java.io.Serializable;

public class CustomFeatureDTO implements Serializable {

    private String idFeature;

    private String description;

    private String refKey;

    public String getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(String idFeature) {
        this.idFeature = idFeature;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    @Override
    public String toString() {
        return "CustomFeatureDTO{" +
            "id=" + getIdFeature() +
            ", description='" + getDescription() + "'" +
            ", refKey='" + getRefKey() + "'" +
            "}";
    }
}
