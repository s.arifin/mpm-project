package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the ContactMechanism entity.
 * BeSmart Team
 */

public class ContactMechanismDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idContact;

    private Integer idContactType;

    private Integer idPurposeType;

    private String description;

    public UUID getIdContact() {
        return this.idContact;
    }

    public void setIdContact(UUID id) {
        this.idContact = id;
    }

    public Integer getIdContactType() {
        return idContactType;
    }

    public void setIdContactType(Integer idContactType) {
        this.idContactType = idContactType;
    }

    public Integer getIdPurposeType() {
        return idPurposeType;
    }

    public void setIdPurposeType(Integer idPurposeType) {
        this.idPurposeType = idPurposeType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getId() {
        return this.idContact;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContactMechanismDTO contactMechanismDTO = (ContactMechanismDTO) o;
        if (contactMechanismDTO.getIdContact() == null || getIdContact() == null) {
            return false;
        }
        return Objects.equals(getIdContact(), contactMechanismDTO.getIdContact());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdContact());
    }

    @Override
    public String toString() {
        return "ContactMechanismDTO{" +
            "id=" + getIdContact() +
            ", idContactType='" + getIdContactType() + "'" +
            ", idPurposeType='" + getIdPurposeType() + "'" +
            "}";
    }
}
