package id.atiila.service.dto;

import id.atiila.domain.VehicleDocumentRequirement;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Date;

public class ImportBpkbOnHandDTO extends VehicleDocumentRequirementDTO {

    private String idFrame;
    private String stnkName;
    private Date stnkReceipt;
    private String receiptBpkbNumber;
    private Date bpkbReceipt;
    private String namaCustomer;
    private String personalIdNumber;
    private String cellphone;
    private String cellphone2;
    private String custBuyer;
    private String personalIdNumberCustBuyer;
    private String cellphoneBuyer;
    private String bastNumber;
    private Date dateBastStnk;
    private String policeNumber;
    private String bastNoBpkb;
    private String bastBpkb;
    private String bpkbNumber;
    private String area;
    private BigDecimal bbn;
    private Date receiptFakturStnk;
    private String cancel;
    private String receiptPlat;
    private Date dateReceiptPlat;
    private String compCode;
    private String transCode;
    private String partnerCode;
    private String partnerName;
    private String idMachine;
    private String idProduct;
    private String color;
    private String productColorCode;
    private String leasingCode;
    private String leasingName;
    private String invoiceNumber;
    private String faktur;


    public String getIdFrame() {
        return idFrame;
    }

    public void setIdFrame(String idFrame) {
        this.idFrame = idFrame;
    }

    public String getStnkName() {
        return stnkName;
    }

    public void setStnkName(String stnkName) {
        this.stnkName = stnkName;
    }

    public Date getStnkReceipt() {
        return stnkReceipt;
    }

    public void setStnkReceipt(Date stnkReceipt) {
        this.stnkReceipt = stnkReceipt;
    }

    public String getReceiptBpkbNumber() {
        return receiptBpkbNumber;
    }

    public void setReceiptBpkbNumber(String receiptBpkbNumber) {
        this.receiptBpkbNumber = receiptBpkbNumber;
    }

    public Date getBpkbReceipt() {
        return bpkbReceipt;
    }

    public void setBpkbReceipt(Date bpkbReceipt) {
        this.bpkbReceipt = bpkbReceipt;
    }

    public String getNamaCustomer() {
        return namaCustomer;
    }

    public void setNamaCustomer(String namaCustomer) {
        this.namaCustomer = namaCustomer;
    }

    public String getPersonalIdNumber() {
        return personalIdNumber;
    }

    public void setPersonalIdNumber(String personalIdNumber) {
        this.personalIdNumber = personalIdNumber;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getCellphone2() {
        return cellphone2;
    }

    public void setCellphone2(String cellphone2) {
        this.cellphone2 = cellphone2;
    }

    public String getCustBuyer() {
        return custBuyer;
    }

    public void setCustBuyer(String custBuyer) {
        this.custBuyer = custBuyer;
    }

    public String getPersonalIdNumberCustBuyer() {
        return personalIdNumberCustBuyer;
    }

    public void setPersonalIdNumberCustBuyer(String personalIdNumberCustBuyer) {
        this.personalIdNumberCustBuyer = personalIdNumberCustBuyer;
    }

    public String getCellphoneBuyer() {
        return cellphoneBuyer;
    }

    public void setCellphoneBuyer(String cellphoneBuyer) {
        this.cellphoneBuyer = cellphoneBuyer;
    }

    public String getBastNumber() {
        return bastNumber;
    }

    public void setBastNumber(String bastNumber) {
        this.bastNumber = bastNumber;
    }

    public Date getDateBastStnk() {
        return dateBastStnk;
    }

    public void setDateBastStnk(Date dateBastStnk) {
        this.dateBastStnk = dateBastStnk;
    }

    public String getPoliceNumber() {
        return policeNumber;
    }

    public void setPoliceNumber(String policeNumber) {
        this.policeNumber = policeNumber;
    }

    public String getBastNoBpkb() {
        return bastNoBpkb;
    }

    public void setBastNoBpkb(String bastNoBpkb) {
        this.bastNoBpkb = bastNoBpkb;
    }

    public String getBastBpkb() {
        return bastBpkb;
    }

    public void setBastBpkb(String bastBpkb) {
        this.bastBpkb = bastBpkb;
    }

    public String getBpkbNumber() {
        return bpkbNumber;
    }

    public void setBpkbNumber(String bpkbNumber) {
        this.bpkbNumber = bpkbNumber;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Override
    public BigDecimal getBbn() {
        return bbn;
    }

    @Override
    public void setBbn(BigDecimal bbn) {
        this.bbn = bbn;
    }

    public Date getReceiptFakturStnk() {
        return receiptFakturStnk;
    }

    public void setReceiptFakturStnk(Date receiptFakturStnk) {
        this.receiptFakturStnk = receiptFakturStnk;
    }

    public String getCancel() {
        return cancel;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    public String getReceiptPlat() {
        return receiptPlat;
    }

    public void setReceiptPlat(String receiptPlat) {
        this.receiptPlat = receiptPlat;
    }

    public Date getDateReceiptPlat() {
        return dateReceiptPlat;
    }

    public void setDateReceiptPlat(Date dateReceiptPlat) {
        this.dateReceiptPlat = dateReceiptPlat;
    }

    public String getCompCode() {
        return compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getTransCode() {
        return transCode;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getIdMachine() {
        return idMachine;
    }

    public void setIdMachine(String idMachine) {
        this.idMachine = idMachine;
    }
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getProductColorCode() {
        return productColorCode;
    }

    public void setProductColorCode(String productColorCode) {
        this.productColorCode = productColorCode;
    }

    public String getLeasingCode() {
        return leasingCode;
    }

    public void setLeasingCode(String leasingCode) {
        this.leasingCode = leasingCode;
    }

    public String getLeasingName() {
        return leasingName;
    }

    public void setLeasingName(String leasingName) {
        this.leasingName = leasingName;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getFaktur() {
        return faktur;
    }

    public void setFaktur(String faktur) {
        this.faktur = faktur;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }
}
