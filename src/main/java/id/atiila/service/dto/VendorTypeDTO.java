package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the VendorType entity.
 * BeSmart Team
 */

public class VendorTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idVendorType;

    private String description;

    public Integer getIdVendorType() {
        return this.idVendorType;
    }

    public void setIdVendorType(Integer id) {
        this.idVendorType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idVendorType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VendorTypeDTO vendorTypeDTO = (VendorTypeDTO) o;
        if (vendorTypeDTO.getIdVendorType() == null || getIdVendorType() == null) {
            return false;
        }
        return Objects.equals(getIdVendorType(), vendorTypeDTO.getIdVendorType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdVendorType());
    }

    @Override
    public String toString() {
        return "VendorTypeDTO{" +
            "id=" + getIdVendorType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
