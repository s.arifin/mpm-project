package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the FacilityType entity.
 * BeSmart Team
 */

public class FacilityTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idFacilityType;

    private String description;

    public Integer getIdFacilityType() {
        return this.idFacilityType;
    }

    public void setIdFacilityType(Integer id) {
        this.idFacilityType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.idFacilityType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FacilityTypeDTO facilityTypeDTO = (FacilityTypeDTO) o;
        if (facilityTypeDTO.getIdFacilityType() == null || getIdFacilityType() == null) {
            return false;
        }
        return Objects.equals(getIdFacilityType(), facilityTypeDTO.getIdFacilityType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdFacilityType());
    }

    @Override
    public String toString() {
        return "FacilityTypeDTO{" +
            "id=" + getIdFacilityType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
