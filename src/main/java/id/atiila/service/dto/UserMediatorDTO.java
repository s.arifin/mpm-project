package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the UserMediator entity.
 * BeSmart Team
 */

public class UserMediatorDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idUserMediator;

    private String userName;

    private String email;

    private Integer currentStatus;

    private String internalId;

    private String internalName;

    private PersonDTO person = new PersonDTO();

    public UUID getIdUserMediator() {
        return this.idUserMediator;
    }

    public void setIdUserMediator(UUID id) {
        this.idUserMediator = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public UUID getId() {
        return this.idUserMediator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserMediatorDTO userMediatorDTO = (UserMediatorDTO) o;
        if (userMediatorDTO.getIdUserMediator() == null || getIdUserMediator() == null) {
            return false;
        }
        return Objects.equals(getIdUserMediator(), userMediatorDTO.getIdUserMediator());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdUserMediator());
    }

    @Override
    public String toString() {
        return "UserMediatorDTO{" +
            "id=" + getIdUserMediator() +
            ", userName='" + getUserName() + "'" +
            ", email='" + getEmail() + "'" +
            "}";
    }
}
