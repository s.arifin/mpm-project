package id.atiila.service.dto;

import org.springframework.data.elasticsearch.annotations.Document;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Vendor entity.
 * BeSmart Team
 */

//@Document(indexName = "vendordto")
public class CustomVendorDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idVendor;

    private String vendorName;

    private String vendorCode;

    private String vendorClassId;

    private String vendorClassName;

    private String address;

    private String vendorMail;

    private String phoneNumber;

    private String businessUnitId;

    private String businessUnitName;

    private String termId;

    private String termName;

    private String taxId;

    private String taxable;

    private String taxableName;


    public String getIdVendor() {
        return idVendor;
    }

    public void setIdVendor(String idVendor) {
        this.idVendor = idVendor;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorClassId() {
        return vendorClassId;
    }

    public void setVendorClassId(String vendorClassId) {
        this.vendorClassId = vendorClassId;
    }

    public String getVendorClassName() {
        return vendorClassName;
    }

    public void setVendorClassName(String vendorClassName) {
        this.vendorClassName = vendorClassName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVendorMail() {
        return vendorMail;
    }

    public void setVendorMail(String vendorMail) {
        this.vendorMail = vendorMail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBusinessUnitId() {
        return businessUnitId;
    }

    public void setBusinessUnitId(String businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

    public String getBusinessUnitName() {
        return businessUnitName;
    }

    public void setBusinessUnitName(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getTaxable() {
        return taxable;
    }

    public void setTaxable(String taxable) {
        this.taxable = taxable;
    }

    public String getTaxableName() {
        return taxableName;
    }

    public void setTaxableName(String taxableName) {
        this.taxableName = taxableName;
    }

    @Override
    public String toString() {
        return "CustomVendorDTO{" +
            "idVendor='" + getIdVendor() + "'" +
            "vendorName'" + getVendorName() + "'" +
            "vendorCode'" + getVendorCode() + "'" +
            "vendorClassId'" + getVendorClassId() + "'" +
            "vendorClassName='" + getVendorClassName() + "'" +
            "address'" + getAddress() + "'" +
            "mail'" + getVendorMail() + "'" +
            "phoneNumber'" + getPhoneNumber() + "'" +
            "}";
    }
}
