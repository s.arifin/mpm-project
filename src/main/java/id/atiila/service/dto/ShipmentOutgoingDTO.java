package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the ShipmentOutgoing entity.
 * BeSmart Team
 */

public class ShipmentOutgoingDTO extends ShipmentDTO {

    private static final long serialVersionUID = 1L;

    private UUID idDriver;

    private String otherName;

    private String deliveryOpt;

    private String deliveryAddress;

    private Integer jumlahprint;

    public UUID getIdDriver() {
        return idDriver;
    }

    public void setIdDriver(UUID idDriver) {
        this.idDriver = idDriver;
    }

    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public String getDeliveryOpt() {
        return deliveryOpt;
    }

    public void setDeliveryOpt(String deliveryOpt) {
        this.deliveryOpt = deliveryOpt;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public Integer getJumlahprint() {
        return jumlahprint;
    }

    public void setJumlahprint(Integer jumlahprint) {
        this.jumlahprint = jumlahprint;
    }

    @Override
    public String toString() {
        return "ShipmentOutgoingDTO{" +
            "id=" + getIdShipment() + "'" +
            "otherName=" + getOtherName() +
            "}";
    }
}
