package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Internal entity.
 * BeSmart Team
 */

public class InternalDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idInternal;

    private String idDealerCode;

    private String idMPM;

    private String idAHM;

    private Integer idRoleType;

    private String internalName;

    private OrganizationDTO organization = new OrganizationDTO();

    public InternalDTO() {
    }

    public InternalDTO(String idInternal, String partyName) {
        this.idInternal = idInternal;
        organization.setName(partyName);
    }

    public String getIdInternal() {
        return idInternal;
    }

    public void setIdInternal(String idInternal) {
        this.idInternal = idInternal;
    }

    public String getIdDealerCode() { return idDealerCode; }

    public void setIdDealerCode(String idDealerCode) { this.idDealerCode = idDealerCode; }

    public String getIdMPM() { return idMPM; }

    public void setIdMPM(String idMPM) { this.idMPM = idMPM; }

    public String getIdAHM() { return idAHM; }

    public void setIdAHM(String idAHM) {
        this.idAHM = idAHM;
    }

    public OrganizationDTO getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationDTO organization) {
        this.organization = organization;
    }

    public Integer getIdRoleType() {
        return idRoleType;
    }

    public void setIdRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    @Override
    public String toString() {
        return "InternalDTO{" +
            "idInternal='" + getIdInternal() + "'" +
            "}";
    }
}
