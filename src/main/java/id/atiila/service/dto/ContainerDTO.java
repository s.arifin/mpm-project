package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the Container entity.
 * BeSmart Team
 */

public class ContainerDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idContainer;

    private String containerCode;

    private String description;

    private UUID facilityId;

    private String facilityDescription;

    private Integer containerTypeId;

    private String containerTypeDescription;

    public UUID getIdContainer() {
        return this.idContainer;
    }

    public void setIdContainer(UUID id) {
        this.idContainer = id;
    }

    public String getContainerCode() {
        return containerCode;
    }

    public void setContainerCode(String containerCode) {
        this.containerCode = containerCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(UUID facilityId) {
        this.facilityId = facilityId;
    }

    public String getFacilityDescription() {
        return facilityDescription;
    }

    public void setFacilityDescription(String facilityDescription) {
        this.facilityDescription = facilityDescription;
    }

    public Integer getContainerTypeId() {
        return containerTypeId;
    }

    public void setContainerTypeId(Integer containerTypeId) {
        this.containerTypeId = containerTypeId;
    }

    public String getContainerTypeDescription() {
        return containerTypeDescription;
    }

    public void setContainerTypeDescription(String containerTypeDescription) {
        this.containerTypeDescription = containerTypeDescription;
    }

    public UUID getId() {
        return this.idContainer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContainerDTO containerDTO = (ContainerDTO) o;
        if (containerDTO.getIdContainer() == null || getIdContainer() == null) {
            return false;
        }
        return Objects.equals(getIdContainer(), containerDTO.getIdContainer());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdContainer());
    }

    @Override
    public String toString() {
        return "ContainerDTO{" +
            "id=" + getIdContainer() +
            ", containerCode='" + getContainerCode() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
