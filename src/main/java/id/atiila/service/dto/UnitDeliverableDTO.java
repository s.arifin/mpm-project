package id.atiila.service.dto;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the UnitDeliverable entity.
 * BeSmart Team
 */
@Document(indexName = "unitdeliverable")
public class UnitDeliverableDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    @org.springframework.data.annotation.Id
	private UUID idDeliverable;

	private Integer idDeliverableType;

	private String description;

	private ZonedDateTime dateReceipt;

	private ZonedDateTime dateDelivery;

	private String bastNumber;

	private String name;

	private String identityNumber;

	private String cellPhone;

	private UUID vehicleDocumentRequirementId;

	private String vehicleDocumentRequirementIdreq;

	private Integer vndStatusTypeId;

	private String vndStatusTypeDescription;

	private Integer conStatusTypeId;

	private String conStatusTypeDescription;

	private String idmachine;

	private String idframe;

	private String requestpoliceid;

	private String stnkNama;

	private String stnkAlamat;

	private VehicleDocumentRequirementDTO vehicleDocumentRequirement = new VehicleDocumentRequirementDTO();

	private String refNumber;

	private ZonedDateTime refDate;

	private Integer receiptQty;

	private BigDecimal receiptNominal;

	private BigDecimal costHandling;

	private BigDecimal costOther;

	private ZonedDateTime platNomor;

	private ZonedDateTime notice;

	private ZonedDateTime stnk;

	private ZonedDateTime bpkb;

	private String bpkbNumber;

    private String customerName;

    private ZonedDateTime delivStnk;

    private ZonedDateTime delivNotice;

    private ZonedDateTime delivPlatNomor;

    private String salesName;

    private String fincoyName;

    private String vendorName;

    private String ivuNumber;

    private ZonedDateTime dateBastSales;

    public ZonedDateTime getDateBastSales() {
        return dateBastSales;
    }

    public void setDateBastSales(ZonedDateTime dateBastSales) {
        this.dateBastSales = dateBastSales;
    }

    public Boolean getPrinted() {
        return isPrinted;
    }

    public void setPrinted(Boolean printed) {
        isPrinted = printed;
    }

    private Boolean isPrinted;

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getIvuNumber() {
        return ivuNumber;
    }

    public void setIvuNumber(String ivuNumber) {
        this.ivuNumber = ivuNumber;
    }

    public String getFincoyName() {
        return fincoyName;
    }

    public void setFincoyName(String fincoyName) {
        this.fincoyName = fincoyName;
    }

    public ZonedDateTime getDelivStnk() {
        return delivStnk;
    }

    public void setDelivStnk(ZonedDateTime delivStnk) {
        this.delivStnk = delivStnk;
    }

    public ZonedDateTime getDelivNotice() {
        return delivNotice;
    }

    public void setDelivNotice(ZonedDateTime delivNotice) {
        this.delivNotice = delivNotice;
    }

    public ZonedDateTime getDelivPlatNomor() {
        return delivPlatNomor;
    }

    public void setDelivPlatNomor(ZonedDateTime delivPlatNomor) {
        this.delivPlatNomor = delivPlatNomor;
    }

    public String getSalesName() {
        return salesName;
    }

    public void setSalesName(String salesName) {
        this.salesName = salesName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

	public UUID getIdDeliverable() {
		return this.idDeliverable;
	}

	public void setIdDeliverable(UUID id) {
		this.idDeliverable = id;
	}

	public Integer getIdDeliverableType() {
		return idDeliverableType;
	}

	public void setIdDeliverableType(Integer idDeliverableType) {
		this.idDeliverableType = idDeliverableType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ZonedDateTime getDateReceipt() {
		return dateReceipt;
	}

	public void setDateReceipt(ZonedDateTime dateReceipt) {
		this.dateReceipt = dateReceipt;
	}

	public ZonedDateTime getDateDelivery() {
		return dateDelivery;
	}

	public void setDateDelivery(ZonedDateTime dateDelivery) {
		this.dateDelivery = dateDelivery;
	}

	public String getBastNumber() {
		return bastNumber;
	}

	public void setBastNumber(String bastNumber) {
		this.bastNumber = bastNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentityNumber() {
		return identityNumber;
	}

	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public UUID getVehicleDocumentRequirementId() {
		return vehicleDocumentRequirementId;
	}

	public void setVehicleDocumentRequirementId(UUID vehicleDocumentRequirementId) {
		this.vehicleDocumentRequirementId = vehicleDocumentRequirementId;
	}

	public String getVehicleDocumentRequirementIdreq() {
		return vehicleDocumentRequirementIdreq;
	}

	public void setVehicleDocumentRequirementIdreq(String vehicleDocumentRequirementIdreq) {
		this.vehicleDocumentRequirementIdreq = vehicleDocumentRequirementIdreq;
	}

	public Integer getVndStatusTypeId() {
		return vndStatusTypeId;
	}

	public void setVndStatusTypeId(Integer statusTypeId) {
		this.vndStatusTypeId = statusTypeId;
	}

	public String getVndStatusTypeDescription() {
		return vndStatusTypeDescription;
	}

	public void setVndStatusTypeDescription(String statusTypeDescription) {
		this.vndStatusTypeDescription = statusTypeDescription;
	}

	public Integer getConStatusTypeId() {
		return conStatusTypeId;
	}

	public void setConStatusTypeId(Integer statusTypeId) {
		this.conStatusTypeId = statusTypeId;
	}

	public String getConStatusTypeDescription() {
		return conStatusTypeDescription;
	}

	public void setConStatusTypeDescription(String statusTypeDescription) {
		this.conStatusTypeDescription = statusTypeDescription;
	}

	public String getIdmachine() {
		return idmachine;
	}

	public void setIdmachine(String idmachine) {
		this.idmachine = idmachine;
	}

	public String getIdframe() {
		return idframe;
	}

	public void setIdframe(String idframe) {
		this.idframe = idframe;
	}

	public String getRequestpoliceid() {
		return requestpoliceid;
	}

	public void setRequestpoliceid(String requestpoliceid) {
		this.requestpoliceid = requestpoliceid;
	}

	public String getStnkNama() {
		return stnkNama;
	}

	public void setStnkNama(String stnkNama) {
		this.stnkNama = stnkNama;
	}

	public String getStnkAlamat() {
		return stnkAlamat;
	}

	public void setStnkAlamat(String stnkAlamat) {
		this.stnkAlamat = stnkAlamat;
	}

	public UUID getId() {
		return this.idDeliverable;
	}

	public VehicleDocumentRequirementDTO getVehicleDocumentRequirement() {
		return vehicleDocumentRequirement;
	}

	public void setVehicleDocumentRequirement(VehicleDocumentRequirementDTO vehicleDocumentRequirement) {
		this.vehicleDocumentRequirement = vehicleDocumentRequirement;
	}

	public String getRefNumber() {
		return refNumber;
	}

	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}

	public ZonedDateTime getRefDate() {
		return refDate;
	}

	public void setRefDate(ZonedDateTime refDate) {
		this.refDate = refDate;
	}

	public Integer getReceiptQty() {
		return receiptQty;
	}

	public void setReceiptQty(Integer receiptQty) {
		this.receiptQty = receiptQty;
	}

	public BigDecimal getReceiptNominal() {
		return receiptNominal;
	}

	public void setReceiptNominal(BigDecimal receiptNominal) {
		this.receiptNominal = receiptNominal;
	}

	public BigDecimal getCostHandling() {
		return costHandling;
	}

	public void setCostHandling(BigDecimal costHandling) {
		this.costHandling = costHandling;
	}

	public BigDecimal getCostOther() {
		return costOther;
	}

	public void setCostOther(BigDecimal costOther) {
		this.costOther = costOther;
	}

	public ZonedDateTime getPlatNomor() {
		return platNomor;
	}

	public void setPlatNomor(ZonedDateTime platNomor) {
		this.platNomor = platNomor;
	}

	public ZonedDateTime getNotice() {
		return notice;
	}

	public void setNotice(ZonedDateTime notice) {
		this.notice = notice;
	}

	public ZonedDateTime getStnk() {
		return stnk;
	}

	public void setStnk(ZonedDateTime stnk) {
		this.stnk = stnk;
	}

	public ZonedDateTime getBpkb() {
		return bpkb;
	}

	public void setBpkb(ZonedDateTime bpkb) {
		this.bpkb = bpkb;
	}

	public String getBpkbNumber() {
		return bpkbNumber;
	}

	public void setBpkbNumber(String bpkbNumber) {
		this.bpkbNumber = bpkbNumber;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		UnitDeliverableDTO unitDeliverableDTO = (UnitDeliverableDTO) o;
		if (unitDeliverableDTO.getIdDeliverable() == null || getIdDeliverable() == null) {
			return false;
		}
		return Objects.equals(getIdDeliverable(), unitDeliverableDTO.getIdDeliverable());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getIdDeliverable());
	}

	@Override
	public String toString() {
		return "UnitDeliverableDTO{" + "id='" + getIdDeliverable() + ", idDeliverableType='" + getIdDeliverableType()
				+ "'" + ", description='" + getDescription() + "'" + ", dateReceipt='" + getDateReceipt() + "'"
				+ ", dateDelivery='" + getDateDelivery() + "'" + ", bastNumber='" + getBastNumber() + "'" + ", name='"
				+ getName() + "'" + ", identityNumber='" + getIdentityNumber() + "'" + ", cellPhone='" + getCellPhone()
				+ "'" + "}";
	}
}
