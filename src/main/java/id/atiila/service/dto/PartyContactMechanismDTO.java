package id.atiila.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the PartyContactMechanism entity.
 * BeSmart Team
 */

public class PartyContactMechanismDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPartyContact;

    private Integer idPurposeType;

    private UUID partyId;

    private String partyName;

    private UUID contactId;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private String description;

    public Long getIdPartyContact() {
        return this.idPartyContact;
    }

    public void setIdPartyContact(Long id) {
        this.idPartyContact = id;
    }

    public Integer getIdPurposeType() {
        return idPurposeType;
    }

    public void setIdPurposeType(Integer idPurposeType) {
        this.idPurposeType = idPurposeType;
    }

    public UUID getPartyId() {
        return partyId;
    }

    public void setPartyId(UUID partyId) {
        this.partyId = partyId;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getContactId() {
        return contactId;
    }

    public void setContactId(UUID contactId) {
        this.contactId = contactId;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PartyContactMechanismDTO PartyContactMechanismDTO = (PartyContactMechanismDTO) o;
        if (PartyContactMechanismDTO.getIdPartyContact() == null || getIdPartyContact() == null) {
            return false;
        }
        return Objects.equals(getIdPartyContact(), PartyContactMechanismDTO.getIdPartyContact());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPartyContact());
    }

    @Override
    public String toString() {
        return "PartyContactMechanismDTO{" +
            "id=" + getIdPartyContact() +
            ", idPurposeType='" + getIdPurposeType() + "'" +
            "}";
    }
}
