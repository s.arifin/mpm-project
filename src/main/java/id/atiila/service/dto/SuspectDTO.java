package id.atiila.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Suspect entity.
 * BeSmart Team
 */

public class SuspectDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idSuspect;

    private String suspectNumber;

    private ZonedDateTime salesDate;

    private String marketName;

    private Float repurchaseProbability;

    private Float priority;

    private Integer currentStatus;

    private Integer suspectTypeId;

    private String dealerId;

    private String dealerName;

    private UUID salesCoordinatorId;

    private String salesCoordinatorName;

    private UUID salesmanId;

    private String salesmanName;

    private Integer saleTypeId;

    private String saleTypeDescription;

    public UUID getIdSuspect() {
        return this.idSuspect;
    }

    public void setIdSuspect(UUID id) {
        this.idSuspect = id;
    }

    public String getSuspectNumber() {
        return suspectNumber;
    }

    public void setSuspectNumber(String suspectNumber) {
        this.suspectNumber = suspectNumber;
    }

    public ZonedDateTime getSalesDate() { return salesDate; }

    public void setSalesDate(ZonedDateTime salesDate) { this.salesDate = salesDate; }

    public String getMarketName() { return marketName; }

    public void setMarketName(String marketName) { this.marketName = marketName; }

    public Float getRepurchaseProbability() { return repurchaseProbability; }

    public void setRepurchaseProbability(Float repurchaseProbability) { this.repurchaseProbability = repurchaseProbability; }

    public Float getPriority() { return priority; }

    public void setPriority(Float priority) { this.priority = priority; }

    public Integer getSuspectTypeId() { return suspectTypeId; }

    public void setSuspectTypeId(Integer suspectTypeId) { this.suspectTypeId = suspectTypeId; }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String internalId) {
        this.dealerId = internalId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String internalName) {
        this.dealerName = internalName;
    }

    public UUID getSalesCoordinatorId() {
        return salesCoordinatorId;
    }

    public void setSalesCoordinatorId(UUID salesmanId) {
        this.salesCoordinatorId = salesmanId;
    }

    public String getSalesCoordinatorName() {
        return salesCoordinatorName;
    }

    public void setSalesCoordinatorName(String salesmanName) {
        this.salesCoordinatorName = salesmanName;
    }

    public UUID getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(UUID salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public Integer getSaleTypeId() { return saleTypeId; }

    public void setSaleTypeId(Integer saleTypeId) { this.saleTypeId = saleTypeId; }

    public String getSaleTypeDescription() { return saleTypeDescription; }

    public void setSaleTypeDescription(String saleTypeDescription) { this.saleTypeDescription = saleTypeDescription; }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public UUID getId() {
        return this.idSuspect;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SuspectDTO suspectDTO = (SuspectDTO) o;
        if (suspectDTO.getIdSuspect() == null || getIdSuspect() == null) {
            return false;
        }
        return Objects.equals(getIdSuspect(), suspectDTO.getIdSuspect());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdSuspect());
    }

    @Override
    public String toString() {
        return "SuspectDTO{" +
            "id=" + getIdSuspect() +
            ", suspectNumber='" + getSuspectNumber() + "'" +
            "}";
    }
}
