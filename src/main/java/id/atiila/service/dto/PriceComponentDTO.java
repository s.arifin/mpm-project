package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the PriceComponent entity.
 * BeSmart Team
 */

public class PriceComponentDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idPriceComponent;

    private BigDecimal price;

    private BigDecimal manfucterPrice;

    private Float percent;

    private Integer idPriceType;

    private String priceTypeDescription;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private String productId;

    private String productName;

    private UUID sellerId;

    private String sellerName;

    public String getPriceTypeDescription() {
        return priceTypeDescription;
    }

    public void setPriceTypeDescription(String priceTypeDescription) {
        this.priceTypeDescription = priceTypeDescription;
    }

    public UUID getIdPriceComponent() {
        return this.idPriceComponent;
    }

    public void setIdPriceComponent(UUID id) {
        this.idPriceComponent = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getManfucterPrice() {
        return manfucterPrice;
    }

    public void setManfucterPrice(BigDecimal manfucterPrice) {
        this.manfucterPrice = manfucterPrice;
    }

    public Float getPercent() {
        return percent;
    }

    public void setPercent(Float percent) {
        this.percent = percent;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getIdPriceType() {
        return idPriceType;
    }

    public UUID getSellerId() {
        return sellerId;
    }

    public void setSellerId(UUID sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public void setIdPriceType(Integer idPriceType) {
        this.idPriceType = idPriceType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public UUID getId() {
        return this.idPriceComponent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PriceComponentDTO priceComponentDTO = (PriceComponentDTO) o;
        if (priceComponentDTO.getIdPriceComponent() == null || getIdPriceComponent() == null) {
            return false;
        }
        return Objects.equals(getIdPriceComponent(), priceComponentDTO.getIdPriceComponent());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPriceComponent());
    }

    @Override
    public String toString() {
        return "PriceComponentDTO{" +
            "id=" + getIdPriceComponent() +
            ", price='" + getPrice() + "'" +
            ", manfucterPrice='" + getManfucterPrice() + "'" +
            ", discount='" + getPercent() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
