package id.atiila.service.dto;


import id.atiila.domain.Organization;

/**
 * A DTO for the OrganizationCustomer entity.
 * BeSmart Team
 */

public class OrganizationCustomerDTO extends CustomerDTO {

    private static final long serialVersionUID = 1L;

    private OrganizationDTO organization = new OrganizationDTO();

    public OrganizationDTO getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationDTO organization) {
        this.organization = organization;
    }

    @Override
    public String toString() {
        return "OrganizationCustomerDTO{" +
            "idCustomer='" + getIdCustomer() + "'" +
            "}";
    }
}
