package id.atiila.service.dto;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * A DTO for the VehicleWorkRequirement entity.
 * BeSmart Team
 */

public class VehicleWorkRequirementDTO extends WorkRequirementDTO {

    private static final long serialVersionUID = 1L;

    private String vehicleNumber;

    private String customerId;

    private String customerName;

    private UUID mechanicId;

    private String mechanicName;

    private UUID vehicleId;

    private String idMotor;

    private String idColor;

    private String colorDescription;

    // private Set<WorkProductRequirementDTO> items = new HashSet<>();

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public UUID getMechanicId() {
        return mechanicId;
    }

    public void setMechanicId(UUID mechanicId) {
        this.mechanicId = mechanicId;
    }

    public UUID getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(UUID vehicleIdentificationId) {
        this.vehicleId = vehicleIdentificationId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMechanicName() {
        return mechanicName;
    }

    public void setMechanicName(String mechanicName) {
        this.mechanicName = mechanicName;
    }

    public String getIdMotor() {
        return idMotor;
    }

    public void setIdMotor(String idMotor) {
        this.idMotor = idMotor;
    }

    public String getIdColor() {
        return idColor;
    }

    public void setIdColor(String idColor) {
        this.idColor = idColor;
    }

    public String getColorDescription() {
        return colorDescription;
    }

    public void setColorDescription(String colorDescription) {
        this.colorDescription = colorDescription;
    }

/*    public Set<WorkProductRequirementDTO> getItems() {
        return items;
    }

    public void setItems(Set<WorkProductRequirementDTO> items) {
        this.items = items;
    }*/

    @Override
    public String toString() {
        return "VehicleWorkRequirementDTO{" +
            "id=" + getIdRequirement() +
            ", vehicleNumber='" + getVehicleNumber() + "'" +
            "}";
    }
}
