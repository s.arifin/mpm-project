package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the CalendarType entity.
 * BeSmart Team
 */

public class CalendarTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idCalendarType;

    private String description;

    public CalendarTypeDTO() {
    }

    public CalendarTypeDTO(Integer idCalendarType, String description) {
        this.idCalendarType = idCalendarType;
        this.description = description;
    }

    public Integer getIdCalendarType() {
        return this.idCalendarType;
    }

    public void setIdCalendarType(Integer id) {
        this.idCalendarType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idCalendarType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CalendarTypeDTO calendarTypeDTO = (CalendarTypeDTO) o;
        if (calendarTypeDTO.getIdCalendarType() == null || getIdCalendarType() == null) {
            return false;
        }
        return Objects.equals(getIdCalendarType(), calendarTypeDTO.getIdCalendarType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdCalendarType());
    }

    @Override
    public String toString() {
        return "CalendarTypeDTO{" +
            "id=" + getIdCalendarType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
