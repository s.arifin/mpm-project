package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the PurposeType entity.
 * BeSmart Team
 */

public class PurposeTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idPurposeType;

    private String description;

    public PurposeTypeDTO() {
    }

    public PurposeTypeDTO(Integer idPurposeType, String description) {
        this.idPurposeType = idPurposeType;
        this.description = description;
    }

    public Integer getIdPurposeType() {
        return this.idPurposeType;
    }

    public void setIdPurposeType(Integer id) {
        this.idPurposeType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idPurposeType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PurposeTypeDTO purposeTypeDTO = (PurposeTypeDTO) o;
        if (purposeTypeDTO.getIdPurposeType() == null || getIdPurposeType() == null) {
            return false;
        }
        return Objects.equals(getIdPurposeType(), purposeTypeDTO.getIdPurposeType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPurposeType());
    }

    @Override
    public String toString() {
        return "PurposeTypeDTO{" +
            "id=" + getIdPurposeType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
