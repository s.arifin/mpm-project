package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the SuspectType entity.
 * BeSmart Team
 */

public class SuspectTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idSuspectType;

    private String description;

    public Integer getIdSuspectType() {
        return this.idSuspectType;
    }

    public void setIdSuspectType(Integer id) {
        this.idSuspectType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idSuspectType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SuspectTypeDTO suspectTypeDTO = (SuspectTypeDTO) o;
        if (suspectTypeDTO.getIdSuspectType() == null || getIdSuspectType() == null) {
            return false;
        }
        return Objects.equals(getIdSuspectType(), suspectTypeDTO.getIdSuspectType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdSuspectType());
    }

    @Override
    public String toString() {
        return "SuspectTypeDTO{" +
            "id=" + getIdSuspectType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
