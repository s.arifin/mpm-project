package id.atiila.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the RuleIndent entity.
 * BeSmart Team
 */

public class RuleIndentDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idRule;

    private Integer idRuleType;

    private Integer sequenceNumber;

    private String description;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private String idProduct;

    private String idInternal;

    private BigDecimal minPayment;

    //getter and setter

    public BigDecimal getMinPayment() {
        return minPayment;
    }

    public void setMinPayment(BigDecimal minPayment) {
        this.minPayment = minPayment;
    }

    public Integer getIdRule() {
        return this.idRule;
    }

    public void setIdRule(Integer id) {
        this.idRule = id;
    }

    public Integer getIdRuleType() { return idRuleType; }

    public void setIdRuleType(Integer idRuleType) { this.idRuleType = idRuleType; }

    public Integer getSequenceNumber() { return sequenceNumber; }

    public void setSequenceNumber(Integer sequenceNumber) { this.sequenceNumber = sequenceNumber; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public ZonedDateTime getDateFrom() { return dateFrom; }

    public void setDateFrom(ZonedDateTime dateFrom) { this.dateFrom = dateFrom; }

    public ZonedDateTime getDateThru() { return dateThru; }

    public void setDateThru(ZonedDateTime dateThru) { this.dateThru = dateThru; }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getIdInternal() {
        return idInternal;
    }

    public void setIdInternal(String idInternal) {
        this.idInternal = idInternal;
    }

    public Integer getId() {
        return this.idRule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RuleIndentDTO ruleIndentDTO = (RuleIndentDTO) o;
        if (ruleIndentDTO.getIdRule() == null || getIdRule() == null) {
            return false;
        }
        return Objects.equals(getIdRule(), ruleIndentDTO.getIdRule());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRule());
    }

    @Override
    public String toString() {
        return "RuleIndentDTO{" +
            "id=" + getIdRule() +
            ", idProduct='" + getIdProduct() + "'" +
            ", idInternal='" + getIdInternal() + "'" +
            "}";
    }
}
