package id.atiila.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Service entity.
 * BeSmart Team
 */

public class ServiceDTO extends ProductDTO {

    private static final long serialVersionUID = 1L;

    private String uomId;

    private String uomDescription;

    private WeServiceTypeDTO weType = new WeServiceTypeDTO();

    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }

    public String getUomDescription() {
        return uomDescription;
    }

    public void setUomDescription(String uomDescription) {
        this.uomDescription = uomDescription;
    }

    public WeServiceTypeDTO getWeType() {
        return weType;
    }

    public void setWeType(WeServiceTypeDTO weType) {
        this.weType = weType;
    }

    @Override
    public String toString() {
        return "ServiceDTO{" +
            "id=" + getIdProduct() +
            ", name='" + getName() + "'" +
            ", dateIntroduction='" + getDateIntroduction() + "'" +
            "}";
    }
}
