package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RuleType entity.
 * BeSmart Team
 */

public class RuleTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idRuleType;

    private String description;

    public Integer getIdRuleType() {
        return this.idRuleType;
    }

    public void setIdRuleType(Integer id) {
        this.idRuleType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idRuleType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RuleTypeDTO ruleTypeDTO = (RuleTypeDTO) o;
        if (ruleTypeDTO.getIdRuleType() == null || getIdRuleType() == null) {
            return false;
        }
        return Objects.equals(getIdRuleType(), ruleTypeDTO.getIdRuleType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRuleType());
    }

    @Override
    public String toString() {
        return "RuleTypeDTO{" +
            "id=" + getIdRuleType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
