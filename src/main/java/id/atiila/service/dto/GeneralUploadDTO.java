package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import javax.persistence.Lob;
import java.util.UUID;

/**
 * A DTO for the GeneralUpload entity.
 * atiila consulting
 */

public class GeneralUploadDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idGeneralUpload;

    private ZonedDateTime dateCreate;

    private String description;

    @Lob
    private byte[] value;
    private String valueContentType;

    private Integer currentStatus;

    private String internalId;

    private Integer purposeId;

    private String purposeDescription;

    public UUID getIdGeneralUpload() {
        return this.idGeneralUpload;
    }

    public void setIdGeneralUpload(UUID id) {
        this.idGeneralUpload = id;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getValue() {
        return value;
    }

    public void setValue(byte[] value) {
        this.value = value;
    }

    public String getValueContentType() {
        return valueContentType;
    }

    public void setValueContentType(String valueContentType) {
        this.valueContentType = valueContentType;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public Integer getPurposeId() {
        return purposeId;
    }

    public void setPurposeId(Integer purposeTypeId) {
        this.purposeId = purposeTypeId;
    }

    public String getPurposeDescription() {
        return purposeDescription;
    }

    public void setPurposeDescription(String purposeTypeDescription) {
        this.purposeDescription = purposeTypeDescription;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public UUID getId() {
        return this.idGeneralUpload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GeneralUploadDTO generalUploadDTO = (GeneralUploadDTO) o;
        if (generalUploadDTO.getIdGeneralUpload() == null || getIdGeneralUpload() == null) {
            return false;
        }
        return Objects.equals(getIdGeneralUpload(), generalUploadDTO.getIdGeneralUpload());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdGeneralUpload());
    }

    @Override
    public String toString() {
        return "GeneralUploadDTO{" +
            "id=" + getIdGeneralUpload() +
            ", dateCreate='" + getDateCreate() + "'" +
            ", description='" + getDescription() + "'" +
            ", value='" + getValue() + "'" +
            "}";
    }
}
