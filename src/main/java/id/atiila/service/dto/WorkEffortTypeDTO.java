package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WorkEffortType entity.
 * BeSmart Team
 */

public class WorkEffortTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idWeType;

    private String description;

    public WorkEffortTypeDTO() {
    }

    public WorkEffortTypeDTO(Integer idWeType, String description) {
        this.idWeType = idWeType;
        this.description = description;
    }

    public Integer getIdWeType() {
        return this.idWeType;
    }

    public void setIdWeType(Integer id) {
        this.idWeType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idWeType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WorkEffortTypeDTO workEffortTypeDTO = (WorkEffortTypeDTO) o;
        if (workEffortTypeDTO.getIdWeType() == null || getIdWeType() == null) {
            return false;
        }
        return Objects.equals(getIdWeType(), workEffortTypeDTO.getIdWeType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdWeType());
    }

    @Override
    public String toString() {
        return "WorkEffortTypeDTO{" +
            "id=" + getIdWeType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
