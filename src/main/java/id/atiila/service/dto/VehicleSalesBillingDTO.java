package id.atiila.service.dto;

import org.springframework.data.elasticsearch.annotations.Document;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * A DTO for the VehicleSalesBilling entity.
 * BeSmart Team
 */

@Document(indexName = "vehiclesalesbilling")
public class VehicleSalesBillingDTO extends BillingReceiptDTO {

    private static final long serialVersionUID = 1L;

    private String customerId;

    private Integer idSaleType;

    private String customerName;

    private String billingNumber;

    private String internalId;

    private ZonedDateTime dateOrder;

    private Integer typeId;

    private String iruNumber;

    private BigDecimal offTheRoad;

    private Integer RequestTaxInvoice;

    private String taxInvoiceNote;

    public Integer getRequestTaxInvoice() { return RequestTaxInvoice; }

    public void setRequestTaxInvoice(Integer requestTaxInvoice) { RequestTaxInvoice = requestTaxInvoice; }

    public String getTaxInvoiceNote() { return taxInvoiceNote; }

    public void setTaxInvoiceNote(String taxInvoiceNote) { this.taxInvoiceNote = taxInvoiceNote; }

    public BigDecimal getOffTheRoad() { return offTheRoad; }

    public void setOffTheRoad(BigDecimal offTheRoad) { this.offTheRoad = offTheRoad; }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public ZonedDateTime getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(ZonedDateTime dateOrder) {
        this.dateOrder = dateOrder;
    }

    @Override
    public String getBillingNumber() {
        return billingNumber;
    }

    @Override
    public void setBillingNumber(String billingNumber) {
        this.billingNumber = billingNumber;
    }

    @Override
    public String getInternalId() { return internalId; }

    @Override
    public void setInternalId(String internalId) { this.internalId = internalId; }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Integer getIdSaleType() {
        return idSaleType;
    }

    public void setIdSaleType(Integer idSaleType) {
        this.idSaleType = idSaleType;
    }

    public String getIruNumber() {
        return iruNumber;
    }

    public void setIruNumber(String iruNumber) {
        this.iruNumber = iruNumber;
    }

    @Override
    public String toString() {
        return "VehicleSalesBillingDTO{" +
            "id=" + getIdBilling() +
            "}";
    }
}
