package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the UnitAccesoriesMapper entity.
 * BeSmart Team
 */

public class UnitAccesoriesMapperDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idUnitAccMapper;

    private String acc1;

    private Integer qtyAcc1;

    private String acc2;

    private Integer qtyAcc2;

    private String acc3;

    private Integer qtyAcc3;

    private String acc4;

    private Integer qtyAcc4;

    private String acc5;

    private Integer qtyAcc5;

    private String acc6;

    private Integer qtyAcc6;

    private String acc7;

    private Integer qtyAcc7;

    private String promat1;

    private Integer qtyPromat1;

    private String promat2;

    private Integer qtyPromat2;

    private String motorId;

    private String motorName;

    public UUID getIdUnitAccMapper() {
        return this.idUnitAccMapper;
    }

    public void setIdUnitAccMapper(UUID id) {
        this.idUnitAccMapper = id;
    }

    public String getAcc1() {
        return acc1;
    }

    public void setAcc1(String acc1) {
        this.acc1 = acc1;
    }

    public Integer getQtyAcc1() {
        return qtyAcc1;
    }

    public void setQtyAcc1(Integer qtyAcc1) {
        this.qtyAcc1 = qtyAcc1;
    }

    public String getAcc2() {
        return acc2;
    }

    public void setAcc2(String acc2) {
        this.acc2 = acc2;
    }

    public Integer getQtyAcc2() {
        return qtyAcc2;
    }

    public void setQtyAcc2(Integer qtyAcc2) {
        this.qtyAcc2 = qtyAcc2;
    }

    public String getAcc3() {
        return acc3;
    }

    public void setAcc3(String acc3) {
        this.acc3 = acc3;
    }

    public Integer getQtyAcc3() {
        return qtyAcc3;
    }

    public void setQtyAcc3(Integer qtyAcc3) {
        this.qtyAcc3 = qtyAcc3;
    }

    public String getAcc4() {
        return acc4;
    }

    public void setAcc4(String acc4) {
        this.acc4 = acc4;
    }

    public Integer getQtyAcc4() {
        return qtyAcc4;
    }

    public void setQtyAcc4(Integer qtyAcc4) {
        this.qtyAcc4 = qtyAcc4;
    }

    public String getAcc5() {
        return acc5;
    }

    public void setAcc5(String acc5) {
        this.acc5 = acc5;
    }

    public Integer getQtyAcc5() {
        return qtyAcc5;
    }

    public void setQtyAcc5(Integer qtyAcc5) {
        this.qtyAcc5 = qtyAcc5;
    }

    public String getAcc6() {
        return acc6;
    }

    public void setAcc6(String acc6) {
        this.acc6 = acc6;
    }

    public Integer getQtyAcc6() {
        return qtyAcc6;
    }

    public void setQtyAcc6(Integer qtyAcc6) {
        this.qtyAcc6 = qtyAcc6;
    }

    public String getAcc7() {
        return acc7;
    }

    public void setAcc7(String acc7) {
        this.acc7 = acc7;
    }

    public Integer getQtyAcc7() {
        return qtyAcc7;
    }

    public void setQtyAcc7(Integer qtyAcc7) {
        this.qtyAcc7 = qtyAcc7;
    }

    public String getPromat1() {
        return promat1;
    }

    public void setPromat1(String promat1) {
        this.promat1 = promat1;
    }

    public Integer getQtyPromat1() {
        return qtyPromat1;
    }

    public void setQtyPromat1(Integer qtyPromat1) {
        this.qtyPromat1 = qtyPromat1;
    }

    public String getPromat2() {
        return promat2;
    }

    public void setPromat2(String promat2) {
        this.promat2 = promat2;
    }

    public Integer getQtyPromat2() {
        return qtyPromat2;
    }

    public void setQtyPromat2(Integer qtyPromat2) {
        this.qtyPromat2 = qtyPromat2;
    }

    public String getMotorId() {
        return motorId;
    }

    public void setMotorId(String motorId) {
        this.motorId = motorId;
    }

    public String getMotorName() {
        return motorName;
    }

    public void setMotorName(String motorName) {
        this.motorName = motorName;
    }
    
    public UUID getId() {
        return this.idUnitAccMapper;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UnitAccesoriesMapperDTO unitAccesoriesMapperDTO = (UnitAccesoriesMapperDTO) o;
        if (unitAccesoriesMapperDTO.getIdUnitAccMapper() == null || getIdUnitAccMapper() == null) {
            return false;
        }
        return Objects.equals(getIdUnitAccMapper(), unitAccesoriesMapperDTO.getIdUnitAccMapper());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdUnitAccMapper());
    }

    @Override
    public String toString() {
        return "UnitAccesoriesMapperDTO{" +
            "id=" + getIdUnitAccMapper() +
            ", acc1='" + getAcc1() + "'" +
            ", qtyAcc1='" + getQtyAcc1() + "'" +
            ", acc2='" + getAcc2() + "'" +
            ", qtyAcc2='" + getQtyAcc2() + "'" +
            ", acc3='" + getAcc3() + "'" +
            ", qtyAcc3='" + getQtyAcc3() + "'" +
            ", acc4='" + getAcc4() + "'" +
            ", qtyAcc4='" + getQtyAcc4() + "'" +
            ", acc5='" + getAcc5() + "'" +
            ", qtyAcc5='" + getQtyAcc5() + "'" +
            ", acc6='" + getAcc6() + "'" +
            ", qtyAcc6='" + getQtyAcc6() + "'" +
            ", acc7='" + getAcc7() + "'" +
            ", qtyAcc7='" + getQtyAcc7() + "'" +
            ", promat1='" + getPromat1() + "'" +
            ", qtyPromat1='" + getQtyPromat1() + "'" +
            ", promat2='" + getPromat2() + "'" +
            ", qtyPromat2='" + getQtyPromat2() + "'" +
            "}";
    }
}
