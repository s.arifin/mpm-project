package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the Feature entity.
 * BeSmart Team
 */

public class FeatureDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idFeature;

    private String description;

    private String refKey;

    private Integer featureTypeId;

    private String featureTypeDescription;

    public Integer getIdFeature() {
        return this.idFeature;
    }

    public void setIdFeature(Integer id) {
        this.idFeature = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public Integer getFeatureTypeId() {
        return featureTypeId;
    }

    public void setFeatureTypeId(Integer featureTypeId) {
        this.featureTypeId = featureTypeId;
    }

    public String getFeatureTypeDescription() {
        return featureTypeDescription;
    }

    public void setFeatureTypeDescription(String featureTypeDescription) {
        this.featureTypeDescription = featureTypeDescription;
    }

    public Integer getId() {
        return this.idFeature;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FeatureDTO featureDTO = (FeatureDTO) o;
        if (featureDTO.getIdFeature() == null || getIdFeature() == null) {
            return false;
        }
        return Objects.equals(getIdFeature(), featureDTO.getIdFeature());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdFeature());
    }

    @Override
    public String toString() {
        return "FeatureDTO{" +
            "id=" + getIdFeature() +
            ", description='" + getDescription() + "'" +
            ", refKey='" + getRefKey() + "'" +
            "}";
    }
}
