package id.atiila.service.dto;

import id.atiila.domain.RequestItem;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the CustomerRequestItem entity.
 * atiila consulting
 */

public class CustomerRequestItemDTO extends RequestItemDTO {

    private static final long serialVersionUID = 1L;

    private BigDecimal unitPrice;

    private BigDecimal bbn;

    private String idColor;

    private Double qty;

    private Double qtyDelivered;

    private String idFrame;

    private String idMachine;

    private String customerId;

    private String customerName;

    private Integer currentStatus;

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getBbn() {
        return bbn;
    }

    public void setBbn(BigDecimal bbn) {
        this.bbn = bbn;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public String getIdFrame() {
        return idFrame;
    }

    public void setIdFrame(String idFrame) {
        this.idFrame = idFrame;
    }

    public String getIdMachine() {
        return idMachine;
    }

    public void setIdMachine(String idMachine) {
        this.idMachine = idMachine;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Double getQtyDelivered() {
        return qtyDelivered;
    }

    public void setQtyDelivered(Double qtyDelivered) {
        this.qtyDelivered = qtyDelivered;
    }

    public String getIdColor() {
        return idColor;
    }

    public void setIdColor(String idColor) {
        this.idColor = idColor;
    }

    public Integer getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomerRequestItemDTO customerRequestItemDTO = (CustomerRequestItemDTO) o;
        if (customerRequestItemDTO.getIdRequestItem() == null || getIdRequestItem() == null) {
            return false;
        }
        return Objects.equals(getIdRequestItem(), customerRequestItemDTO.getIdRequestItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRequestItem());
    }

    @Override
    public String toString() {
        return "CustomerRequestItemDTO{" +
            "id=" + getIdRequestItem() +
            ", itemDescription='" + getDescription() + "'" +
            ", qty='" + getQty() + "'" +
            ", unitPrice='" + getUnitPrice() + "'" +
            ", idFrame='" + getIdFrame() + "'" +
            ", idMachine='" + getIdMachine() + "'" +
            ", sequence='" + getSequence() + "'" +
            "}";
    }
}
