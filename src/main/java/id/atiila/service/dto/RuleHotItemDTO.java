package id.atiila.service.dto;


import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the RuleHotItem entity.
 * BeSmart Team
 */
public class RuleHotItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idRule;

    private Integer idRuleType;

    private Integer sequenceNumber;

    private String description;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private String idProduct;

    private BigDecimal minDownPayment;

    private String idInternal;

    //gettter and setter

    public String getIdInternal() {
        return idInternal;
    }

    public void setIdInternal(String idInternal) {
        this.idInternal = idInternal;
    }

    public Integer getIdRule() { return idRule; }

    public void setIdRule(Integer idRule) { this.idRule = idRule; }

    public Integer getIdRuleType() { return idRuleType; }

    public void setIdRuleType(Integer idRuleType) { this.idRuleType = idRuleType; }

    public Integer getSequenceNumber() { return sequenceNumber; }

    public void setSequenceNumber(Integer sequenceNumber) { this.sequenceNumber = sequenceNumber; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public ZonedDateTime getDateFrom() { return dateFrom; }

    public void setDateFrom(ZonedDateTime dateFrom) { this.dateFrom = dateFrom; }

    public ZonedDateTime getDateThru() { return dateThru; }

    public void setDateThru(ZonedDateTime dateThru) { this.dateThru = dateThru; }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public BigDecimal getMinDownPayment() {
        return minDownPayment;
    }

    public void setMinDownPayment(BigDecimal minDownPayment) {
        this.minDownPayment = minDownPayment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RuleHotItemDTO ruleHotItemDTO = (RuleHotItemDTO) o;
        if (ruleHotItemDTO.getIdRule() == null || getIdRule() == null) {
            return false;
        }
        return Objects.equals(getIdRule(), ruleHotItemDTO.getIdRule());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRule());
    }

    @Override
    public String toString() {
        return "RuleHotItemDTO{" +
            "id=" + getIdRule() +
            ", idProduct='" + getIdProduct() + "'" +
            ", minDownPayment='" + getMinDownPayment() + "'" +
            "}";
    }
}
