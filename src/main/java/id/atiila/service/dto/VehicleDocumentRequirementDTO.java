package id.atiila.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the VehicleDocumentRequirement entity. BeSmart Team
 */

public class VehicleDocumentRequirementDTO extends RequirementDTO {

	private static final long serialVersionUID = 1L;

	private String note;

	private String statusFaktur;

	private BigDecimal bbn;

	private BigDecimal otherCost;

	private SaleTypeDTO saleType = new SaleTypeDTO();

	private String fakturATPM;

	private ZonedDateTime fakturDate;

    private ZonedDateTime invoicePrint;

	private String requestPoliceNumber;

	private String internalId;

	private String internalName;

	private UUID manageById;

	private String manageByName;

	private ZonedDateTime dateFillName;

	private String registrationNumber;

	private PersonDTO personOwner = new PersonDTO();

	private OrganizationDTO organizationOwner = new OrganizationDTO();

	private VehicleDTO vehicle = new VehicleDTO();

	private String idVendor;

	private String submissionNo;

	private ZonedDateTime dateSubmission;

	private UUID idOrderItem;

	private UUID idOrder;

    private Boolean waitStnk;

    private String ivuNumber;

    private String salesName;

    private BigDecimal costHandling;

    public OrganizationDTO getOrganizationOwner() {
        return organizationOwner;
    }

    public void setOrganizationOwner(OrganizationDTO organizationOwner) {
        this.organizationOwner = organizationOwner;
    }

    public BigDecimal getCostHandling() {
        return costHandling;
    }

    public void setCostHandling(BigDecimal costHandling) {
        this.costHandling = costHandling;
    }

    public String getSalesName() {
        return salesName;
    }

    public void setSalesName(String salesName) {
        this.salesName = salesName;
    }

    public String getIvuNumber() {
        return ivuNumber;
    }

    public void setIvuNumber(String ivuNumber) {
        this.ivuNumber = ivuNumber;
    }

    public ZonedDateTime getInvoicePrint() { return invoicePrint; }

    public void setInvoicePrint(ZonedDateTime invoicePrint) { this.invoicePrint = invoicePrint; }

    //    private ZonedDateTime dateFaktur;
//
//    private ZonedDateTime dateBiroJasa;
//
//    private ZonedDateTime dateStnkBpkb;

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

    public String getStatusFaktur() {
        return statusFaktur;
    }

    public void setStatusFaktur(String statusFaktur) {
        this.statusFaktur = statusFaktur;
    }

    public BigDecimal getBbn() {
		return bbn;
	}

	public void setBbn(BigDecimal bbn) {
		this.bbn = bbn;
	}

	public BigDecimal getOtherCost() {
		return otherCost;
	}

	public void setOtherCost(BigDecimal otherCost) {
		this.otherCost = otherCost;
	}

	public SaleTypeDTO getSaleType() {
		return saleType;
	}

	public void setSaleType(SaleTypeDTO saleType) {
		this.saleType = saleType;
	}

	public String getFakturATPM() {
		return fakturATPM;
	}

	public void setFakturATPM(String fakturATPM) {
		this.fakturATPM = fakturATPM;
	}

	public ZonedDateTime getFakturDate() {
		return fakturDate;
	}

	public void setFakturDate(ZonedDateTime fakturDate) {
		this.fakturDate = fakturDate;
	}

	public String getRequestPoliceNumber() {
		return requestPoliceNumber;
	}

	public void setRequestPoliceNumber(String requestPoliceNumber) {
		this.requestPoliceNumber = requestPoliceNumber;
	}

	public PersonDTO getPersonOwner() {
		return personOwner;
	}

	public void setPersonOwner(PersonDTO personOwner) {
		this.personOwner = personOwner;
	}

	public VehicleDTO getVehicle() {
		return vehicle;
	}

	public void setVehicle(VehicleDTO vehicle) {
		this.vehicle = vehicle;
	}

	public String getInternalId() {
		return internalId;
	}

	public void setInternalId(String internalId) {
		this.internalId = internalId;
	}

	public String getInternalName() {
		return internalName;
	}

	public void setInternalName(String internalName) {
		this.internalName = internalName;
	}

	public UUID getManageById() {
		return manageById;
	}

	public void setManageById(UUID manageById) {
		this.manageById = manageById;
	}

	public String getManageByName() {
		return manageByName;
	}

	public void setManageByName(String manageByName) {
		this.manageByName = manageByName;
	}

	public String getIdVendor() {
		return idVendor;
	}

	public void setIdVendor(String idVendor) {
		this.idVendor = idVendor;
	}

	public String getSubmissionNo() {
		return submissionNo;
	}

	public void setSubmissionNo(String submissionNo) {
		this.submissionNo = submissionNo;
	}

	public ZonedDateTime getDateSubmission() {
		return dateSubmission;
	}

	public void setDateSubmission(ZonedDateTime dateSubmission) {
		this.dateSubmission = dateSubmission;
	}

    public ZonedDateTime getDateFillName() {
        return dateFillName;
    }

    public void setDateFillName(ZonedDateTime dateFillName) {
        this.dateFillName = dateFillName;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Boolean getWaitStnk() {
        return waitStnk;
    }

    public void setWaitStnk(Boolean waitStnk) {
        this.waitStnk = waitStnk;
    }

    public UUID getIdOrderItem() {
        return idOrderItem;
    }

    public void setIdOrderItem(UUID idOrderItem) {
        this.idOrderItem = idOrderItem;
    }

    public UUID getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(UUID idOrder) {
        this.idOrder = idOrder;
    }

    //    public ZonedDateTime getDateFaktur() {
//        return dateFaktur;
//    }
//
//    public void setDateFaktur(ZonedDateTime dateFaktur) {
//        this.dateFaktur = dateFaktur;
//    }
//
//    public ZonedDateTime getDateBiroJasa() {
//        return dateBiroJasa;
//    }
//
//    public void setDateBiroJasa(ZonedDateTime dateBiroJasa) {
//        this.dateBiroJasa = dateBiroJasa;
//    }
//
//    public ZonedDateTime getDateStnkBpkb() {
//        return dateStnkBpkb;
//    }
//
//    public void setDateStnkBpkb(ZonedDateTime dateStnkBpkb) {
//        this.dateStnkBpkb = dateStnkBpkb;
//    }

    @Override
	public String toString() {
		return "VehicleDocumentRequirementDTO{" + ", Id='" + getIdRequirement() + '\'' + ", note='" + note + '\''
				+ ", bbn=" + bbn + ", idSaleType=" + saleType.getIdSaleType() + ", fakturATPM='"
				+ fakturATPM + '\'' + ", requestPoliceNumber='" + requestPoliceNumber + '\'' + ", personOwner="
				+ personOwner + ", vehicle=" + vehicle + ", organizationOwner=" + organizationOwner +'}';
	}
}
