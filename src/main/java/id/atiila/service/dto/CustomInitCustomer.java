package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.UUID;
import java.util.Objects;

/**
 * A DTO for the Customer entity.
 * BeSmart Team
 */

public class CustomInitCustomer implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idCustomer;

    private String partyName;

    private String address;

    private String phoneNumber;

    private String cellPhone1;

    private String pob;

    private String dob;

    private String gender;

    private String religionId;

    private String religionName;

    private String personalIdNumber;

    private String lastEducation;

    private String lastEducationName;

    private String privateMail;

    private String bussinesUnitId;

    private  String bussinesUnitName;

    private  String salesmanid;

    private String salesmanName;


    public String getIdCustomer() {
        return this.idCustomer;
    }

    public void setIdCustomer(String id) {
        this.idCustomer = id;
    }



    public String getDob() {
        return this.dob;
    }

    public void setIdDob(String idDob) {
        this.dob = idDob;
    }

    public String getPob() {
        return this.pob;
    }

    public void setIdPob(String idPob) {
        this.pob = idPob;
    }


    public String getGender() {
        return this.gender;
    }

    public void setGender(String idGender) {
        this.gender = idGender;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getPersonalIdNumber() {
        return personalIdNumber;
    }

    public void setPersonalIdNumber(String personalIdNumber) {
        this.personalIdNumber = personalIdNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCellPhone1() {
        return cellPhone1;
    }

    public void setCellPhone1(String cellPhone1) {
        this.cellPhone1 = cellPhone1;
    }

    public String getPrivateMail() {
        return privateMail;
    }

    public void setPrivateMail(String privateMail) {
        this.privateMail = privateMail;
    }

    public String getReligionId() { return religionId; }

    public  void setReligionId (String religionId) {this.religionId = religionId;}

    public String getReligionName() { return religionName;}

    public  void setReligionName (String religionName) {this.religionName = religionName;}

    public String getLastEducation() {
        return lastEducation;
    }

    public void setLastEducation(String lastEducation) {
        this.lastEducation = lastEducation;
    }

    public String getLastEducationName() {return lastEducationName;}

    public  void  setLastEducationName (String lastEducationName) {this.lastEducationName = lastEducationName;}

    public String getSalesmanid () {return salesmanid;}

    public void setSalesmanid (String salesmanid) {this.salesmanid= salesmanid;}

    public String getSalesmanName () { return salesmanName;}

    public  void setSalesmanName (String salesmanName) {this.salesmanName = salesmanName;}

    public String getBussinesUnitId () { return bussinesUnitId;}

    public void setBussinesUnitId (String bussinesUnitId) { this.bussinesUnitId = bussinesUnitId;}

    public String getBussinesUnitName () {return bussinesUnitName;}

    public void setBussinesUnitName (String bussinesUnitName) {this.bussinesUnitName = bussinesUnitName;}





    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomCustomerDTO CustomCustomerDTO = (CustomCustomerDTO) o;
        if (CustomCustomerDTO.getIdCustomer() == null || getIdCustomer() == null) {
            return false;
        }
        return Objects.equals(getIdCustomer(), CustomCustomerDTO.getIdCustomer());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdCustomer());
    }

    @Override
    public String toString() {
        return "CustomCustomerDTO{" +
            "id=" + getIdCustomer() +
            "idDob=" + getDob() +
            "idPob=" + getPob() +
            "idGender=" + getGender() +
            "}";
    }
}
