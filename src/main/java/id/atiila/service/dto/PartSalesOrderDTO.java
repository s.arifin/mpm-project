package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the PartSalesOrder entity.
 * BeSmart Team
 */

public class PartSalesOrderDTO extends SalesOrderDTO {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "PartSalesOrderDTO{" +
            "id=" + getIdOrder() +
            "}";
    }
}
