package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.util.UUID;

public class CustomRequestRequirementDTO {
    private UUID idRequest;

    private String requirementNumber;

    private UUID idRequirement;

    private String featureName;

    private String productName;

    private ZonedDateTime dateCreated;

    private String requestNumber;

    private Integer currentStatus;

    public CustomRequestRequirementDTO(UUID idRequest, String requirementNumber, UUID idRequirement, String featureName, String productName, ZonedDateTime dateCreated, String requestNumber, Integer currentStatus){
        this.idRequest = idRequest;
        this.requirementNumber = requirementNumber;
        this.featureName = featureName;
        this.productName = productName;
        this.dateCreated = dateCreated;
        this.requestNumber = requestNumber;
        this.idRequirement = idRequirement;
        this.currentStatus = currentStatus;
    }

    public Integer getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public UUID getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(UUID idRequest) {
        this.idRequest = idRequest;
    }

    public String getRequirementNumber() {
        return requirementNumber;
    }

    public void setRequirementNumber(String requirementNumber) {
        this.requirementNumber = requirementNumber;
    }

    public UUID getIdRequirement() {
        return idRequirement;
    }

    public void setIdRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
