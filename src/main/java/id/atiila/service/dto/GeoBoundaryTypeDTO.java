package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the GeoBoundaryType entity.
 * BeSmart Team
 */

public class GeoBoundaryTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idGeobouType;

    private String description;

    public GeoBoundaryTypeDTO() {
    }

    public GeoBoundaryTypeDTO(Integer idGeobouType, String description) {
        this.idGeobouType = idGeobouType;
        this.description = description;
    }

    public Integer getIdGeobouType() {
        return this.idGeobouType;
    }

    public void setIdGeobouType(Integer id) {
        this.idGeobouType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.idGeobouType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GeoBoundaryTypeDTO geoBoundaryTypeDTO = (GeoBoundaryTypeDTO) o;
        if (geoBoundaryTypeDTO.getIdGeobouType() == null || getIdGeobouType() == null) {
            return false;
        }
        return Objects.equals(getIdGeobouType(), geoBoundaryTypeDTO.getIdGeobouType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdGeobouType());
    }

    @Override
    public String toString() {
        return "GeoBoundaryTypeDTO{" +
            "id=" + getIdGeobouType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
