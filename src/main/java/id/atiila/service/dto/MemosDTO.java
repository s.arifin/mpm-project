package id.atiila.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Memos entity.
 * BeSmart Team
 */

public class MemosDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idMemo;

    private String memoNumber;

    private BigDecimal bbn;

    private Float discount;

    private BigDecimal unitPrice;

    private Integer currentStatus;

    private String dealerId;

    private String dealerName;

    private UUID billingId;

    private String billingIdbilling;

    private Integer memoTypeId;

    private String memoTypeDescription;

    private UUID inventoryItemId;

    private UUID oldInventoryItemId;

    private InventoryItemDTO inventoryItem = new InventoryItemDTO() ;
//
    private InventoryItemDTO oldInventoryItem = new InventoryItemDTO();

    private String currbillnumb;

    private BigDecimal ppn;

    private BigDecimal arAmount;

    private BigDecimal dpp;

    private BigDecimal saleSamount;

    private ZonedDateTime dateCreated;

    public BigDecimal getDpp() {
        return dpp;
    }

    public void setDpp(BigDecimal dpp) {
        this.dpp = dpp;
    }

    public BigDecimal getSaleSamount() {
        return saleSamount;
    }

    public void setSaleSamount(BigDecimal saleSamount) {
        this.saleSamount = saleSamount;
    }

    public BigDecimal getPpn() {
        return ppn;
    }

    public void setPpn(BigDecimal ppn) {
        this.ppn = ppn;
    }

    public BigDecimal getArAmount() {
        return arAmount;
    }

    public void setArAmount(BigDecimal arAmount) {
        this.arAmount = arAmount;
    }

    public String getCurrbillnumb() {
        return currbillnumb;
    }

    public void setCurrbillnumb(String currbillnumb) {
        this.currbillnumb = currbillnumb;
    }

    public UUID getIdMemo() {
        return this.idMemo;
    }

    public void setIdMemo(UUID id) {
        this.idMemo = id;
    }

    public String getMemoNumber() {
        return memoNumber;
    }

    public void setMemoNumber(String memoNumber) {
        this.memoNumber = memoNumber;
    }

    public BigDecimal getBbn() {
        return bbn;
    }

    public void setBbn(BigDecimal bbn) {
        this.bbn = bbn;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String internalId) {
        this.dealerId = internalId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String internalName) {
        this.dealerName = internalName;
    }

    public UUID getBillingId() {
        return billingId;
    }

    public void setBillingId(UUID billingId) {
        this.billingId = billingId;
    }

    public String getBillingIdbilling() {
        return billingIdbilling;
    }

    public void setBillingIdbilling(String billingIdbilling) {
        this.billingIdbilling = billingIdbilling;
    }

    public Integer getMemoTypeId() {
        return memoTypeId;
    }

    public void setMemoTypeId(Integer memoTypeId) {
        this.memoTypeId = memoTypeId;
    }

    public String getMemoTypeDescription() {
        return memoTypeDescription;
    }

    public void setMemoTypeDescription(String memoTypeDescription) {
        this.memoTypeDescription = memoTypeDescription;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public UUID getId() {
        return this.idMemo;
    }

    public UUID getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(UUID inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    public UUID getOldInventoryItemId() {
        return oldInventoryItemId;
    }

    public void setOldInventoryItemId(UUID oldInventoryItemId) {
        this.oldInventoryItemId = oldInventoryItemId;
    }

    public InventoryItemDTO getInventoryItem() {
        return inventoryItem;
    }

    public void setInventoryItem(InventoryItemDTO inventoryItem) {
        this.inventoryItem = inventoryItem;
    }

    public InventoryItemDTO getOldInventoryItem() {
        return oldInventoryItem;
    }

    public void setOldInventoryItem(InventoryItemDTO oldInventoryItem) {
        this.oldInventoryItem = oldInventoryItem;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MemosDTO memosDTO = (MemosDTO) o;
        if (memosDTO.getIdMemo() == null || getIdMemo() == null) {
            return false;
        }
        return Objects.equals(getIdMemo(), memosDTO.getIdMemo());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdMemo());
    }

    @Override
    public String toString() {
        return "MemosDTO{" +
            "id=" + getIdMemo() +
            ", memoNumber='" + getMemoNumber() + "'" +
            ", bbn='" + getBbn() + "'" +
            ", discount='" + getDiscount() + "'" +
            ", unitPrice='" + getUnitPrice() + "'" +
            "}";
    }
}
