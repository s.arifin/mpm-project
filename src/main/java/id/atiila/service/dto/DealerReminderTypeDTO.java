package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the DealerReminderType entity.
 * BeSmart Team
 */

public class DealerReminderTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idReminderType;

    private String description;

    private String messages;

    public Integer getIdReminderType() {
        return this.idReminderType;
    }

    public void setIdReminderType(Integer id) {
        this.idReminderType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }
    
    public Integer getId() {
        return this.idReminderType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DealerReminderTypeDTO dealerReminderTypeDTO = (DealerReminderTypeDTO) o;
        if (dealerReminderTypeDTO.getIdReminderType() == null || getIdReminderType() == null) {
            return false;
        }
        return Objects.equals(getIdReminderType(), dealerReminderTypeDTO.getIdReminderType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdReminderType());
    }

    @Override
    public String toString() {
        return "DealerReminderTypeDTO{" +
            "id=" + getIdReminderType() +
            ", description='" + getDescription() + "'" +
            ", messages='" + getMessages() + "'" +
            "}";
    }
}
