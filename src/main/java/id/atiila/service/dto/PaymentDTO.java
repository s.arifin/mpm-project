package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the Payment entity.
 * BeSmart Team
 */

public class PaymentDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idPayment;

    private String paymentNumber;

    private String refferenceNumber;

    private ZonedDateTime dateCreate;

    private BigDecimal amount;

    private Integer currentStatus;

    private Integer paymentTypeId;

    private String paymentTypeDescription;

    private Integer methodId;

    private String methodDescription;

    private String internalId;

    private String paidToId;

    private String paidFromId;

    //getter and setter
    public String getPaymentTypeDescription() {
        return paymentTypeDescription;
    }

    public void setPaymentTypeDescription(String paymentTypeDescription) {
        this.paymentTypeDescription = paymentTypeDescription;
    }

    public String getMethodDescription() {
        return methodDescription;
    }

    public void setMethodDescription(String methodDescription) {
        this.methodDescription = methodDescription;
    }

    public UUID getIdPayment() {
        return this.idPayment;
    }

    public void setIdPayment(UUID id) {
        this.idPayment = id;
    }

    public String getPaymentNumber() {
        return paymentNumber;
    }

    public void setPaymentNumber(String paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public String getRefferenceNumber() {
        return refferenceNumber;
    }

    public void setRefferenceNumber(String refferenceNumber) {
        this.refferenceNumber = refferenceNumber;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(Integer paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public Integer getMethodId() {
        return methodId;
    }

    public void setMethodId(Integer paymentMethodId) {
        this.methodId = paymentMethodId;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getPaidToId() {
        return paidToId;
    }

    public void setPaidToId(String billToId) {
        this.paidToId = billToId;
    }

    public String getPaidFromId() {
        return paidFromId;
    }

    public void setPaidFromId(String billToId) {
        this.paidFromId = billToId;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public UUID getId() {
        return this.idPayment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PaymentDTO paymentDTO = (PaymentDTO) o;
        if (paymentDTO.getIdPayment() == null || getIdPayment() == null) {
            return false;
        }
        return Objects.equals(getIdPayment(), paymentDTO.getIdPayment());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPayment());
    }

    @Override
    public String toString() {
        return "PaymentDTO{" +
            "id=" + getIdPayment() +
            ", paymentNumber='" + getPaymentNumber() + "'" +
            ", refferenceNumber='" + getRefferenceNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", amount='" + getAmount() + "'" +
            "}";
    }
}
