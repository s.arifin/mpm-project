package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the UnitPreparation entity.
 * atiila consulting
 */

public class UnitPreparationDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idSlip;

    private ZonedDateTime dateCreate;

    private String slipNumber;

    private String userMekanik;

    private String shipToId;

    private String shipToName;

    private String internalId;

    private String internalName;

    private UUID facilityId;

    private String facilityDescription;

    private UUID inventoryItemId;

    private UUID orderItemId;

    private String orderNumber;

    private Integer currentStatus;

    public UUID getIdSlip() {
        return this.idSlip;
    }

    public void setIdSlip(UUID id) {
        this.idSlip = id;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getSlipNumber() {
        return slipNumber;
    }

    public void setSlipNumber(String slipNumber) {
        this.slipNumber = slipNumber;
    }

    public String getUserMekanik() {
        return userMekanik;
    }

    public void setUserMekanik(String userMekanik) {
        this.userMekanik = userMekanik;
    }

    public String getShipToId() {
        return shipToId;
    }

    public void setShipToId(String shipToId) {
        this.shipToId = shipToId;
    }

    public String getShipToName() {
        return shipToName;
    }

    public void setShipToName(String shipToName) {
        this.shipToName = shipToName;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public UUID getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(UUID facilityId) {
        this.facilityId = facilityId;
    }

    public String getFacilityDescription() {
        return facilityDescription;
    }

    public void setFacilityDescription(String facilityDescription) {
        this.facilityDescription = facilityDescription;
    }

    public UUID getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(UUID inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    public UUID getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(UUID orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Integer getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public UUID getId() {
        return this.idSlip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UnitPreparationDTO unitPreparationDTO = (UnitPreparationDTO) o;
        if (unitPreparationDTO.getIdSlip() == null || getIdSlip() == null) {
            return false;
        }
        return Objects.equals(getIdSlip(), unitPreparationDTO.getIdSlip());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdSlip());
    }

    @Override
    public String toString() {
        return "UnitPreparationDTO{" +
            "id=" + getIdSlip() +
            ", dateCreate='" + getDateCreate() + "'" +
            ", slipNumber='" + getSlipNumber() + "'" +
            ", userMekanik='" + getUserMekanik() + "'" +
            "}";
    }
}
