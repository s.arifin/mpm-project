package id.atiila.service.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class CustomProductFeatureDTO implements Serializable {

    private String idFeature;

    private String idProduct;

    private Integer idFeatureType;

    private Double qty;

    public String getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(String idFeature) {
        this.idFeature = idFeature;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Integer getIdFeatureType() {
        return idFeatureType;
    }

    public void setIdFeatureType(Integer idFeatureType) {
        this.idFeatureType = idFeatureType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CustomProductFeatureDTO that = (CustomProductFeatureDTO) o;

        return new EqualsBuilder()
            .append(idFeature, that.idFeature)
            .append(idProduct, that.idProduct)
            .append(idFeatureType, that.idFeatureType)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idFeature)
            .append(idProduct)
            .append(idFeatureType)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "CustomProductFeatureDTO{" +
            "idFeature='" + idFeature + '\'' +
            ", idProduct='" + idProduct + '\'' +
            ", qty=" + qty +
            '}';
    }
}
