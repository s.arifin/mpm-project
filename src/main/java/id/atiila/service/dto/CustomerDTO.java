package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.UUID;
import java.util.Objects;

/**
 * A DTO for the Customer entity.
 * BeSmart Team
 */

public class CustomerDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idCustomer;

    private Integer idRoleType;

    private UUID partyId;

    private String partyName;

    private String idMPM;

    public String getIdCustomer() {
        return this.idCustomer;
    }

    public void setIdCustomer(String id) {
        this.idCustomer = id;
    }

    public Integer getIdRoleType() {
        return idRoleType;
    }

    public void setIdRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
    }

    public UUID getPartyId() {
        return partyId;
    }

    public void setPartyId(UUID partyId) {
        this.partyId = partyId;
    }

    public String getId() {
        return this.idCustomer;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getIdMPM() {
        return idMPM;
    }

    public void setIdMPM(String idMPM) {
        this.idMPM = idMPM;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomerDTO customerDTO = (CustomerDTO) o;
        if (customerDTO.getIdCustomer() == null || getIdCustomer() == null) {
            return false;
        }
        return Objects.equals(getIdCustomer(), customerDTO.getIdCustomer());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdCustomer());
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
            "idCustomer='" + idCustomer + '\'' +
            ", idRoleType=" + idRoleType +
            ", partyId=" + partyId +
            ", partyName='" + partyName + '\'' +
            ", idMPM='" + idMPM + '\'' +
            '}';
    }
}
