package id.atiila.service.dto;

import id.atiila.base.BaseConstants;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the UnitDocumentMessage entity.
 * BeSmart Team
 */

public class UnitDocumentMessageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idMessage;

    private UUID idRequirement;

    private String idProspect;

    private String content;

    private ZonedDateTime dateCreated;

    private String usernameFrom;

    private String usernameTo;

    private Integer parentId;

    private Integer currentApprovalStatus;

    //getter and setter
    public Integer getCurrentApprovalStatus() {
        return currentApprovalStatus;
    }

    public void setCurrentApprovalStatus(Integer currentApprovalStatus) {
        this.currentApprovalStatus = currentApprovalStatus;
    }

    public Integer getIdMessage() {
        return this.idMessage;
    }

    public void setIdMessage(Integer id) {
        this.idMessage = id;
    }

    public UUID getIdRequirement() {
        return idRequirement;
    }

    public void setIdRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
    }

    public String getIdProspect() {
        return idProspect;
    }

    public void setIdProspect(String idProspect) {
        this.idProspect = idProspect;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getUsernameFrom() {
        return usernameFrom;
    }

    public void setUsernameFrom(String usernameFrom) {
        this.usernameFrom = usernameFrom;
    }

    public String getUsernameTo() {
        return usernameTo;
    }

    public void setUsernameTo(String usernameTo) {
        this.usernameTo = usernameTo;
    }

    public Integer getParentId() { return parentId; }

    public void setParentId(Integer parentId) { this.parentId = parentId; }

    public Integer getId() {
        return this.idMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UnitDocumentMessageDTO unitDocumentMessageDTO = (UnitDocumentMessageDTO) o;
        if (unitDocumentMessageDTO.getIdMessage() == null || getIdMessage() == null) {
            return false;
        }
        return Objects.equals(getIdMessage(), unitDocumentMessageDTO.getIdMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdMessage());
    }

    @Override
    public String toString() {
        return "UnitDocumentMessageDTO{" +
            "id=" + getIdMessage() +
            ", idRequirement='" + getIdRequirement() + "'" +
            ", idProspect='" + getIdProspect() + "'" +
            ", content='" + getContent() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            ", usernameFrom='" + getUsernameFrom() + "'" +
            ", usernameTo='" + getUsernameTo() + "'" +
            "}";
    }
}
