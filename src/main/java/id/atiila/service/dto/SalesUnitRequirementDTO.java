package id.atiila.service.dto;

import id.atiila.base.BaseConstants;
import id.atiila.domain.OrganizationCustomer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the SalesUnitRequirement entity.
 * BeSmart Team
 */

public class SalesUnitRequirementDTO extends RequirementDTO {

    private static final long serialVersionUID = 1L;

    private String idmachine;

    private String idframe;

    private String requestIdFrame;

    private String requestIdMachine;

    private String note;

    private String requestpoliceid;

    private BigDecimal brokerFee;

    private BigDecimal subsfincomp;

    private BigDecimal subsmd;

    private BigDecimal subsown;

    private BigDecimal subsahm;

    private BigDecimal bbnprice;

    private BigDecimal hetprice;

    private BigDecimal downPayment;

    private BigDecimal unitprice;

    private String noteshipment;

    private String notepartner;

    private PersonDTO personOwner = new PersonDTO();

    private OrganizationDTO organizationOwner = new OrganizationDTO();

    private String customerId;

    private String customerName;

    private UUID salesmanId;

    private String salesmanName;

    private String internalId;

    private String internalName;

    private Integer saleTypeId;

    private String saleTypeDescription;

    private String billToId;

    private String billToName;

    private UUID brokerId;

//    private String brokerName;

    private Integer colorId;

    private String colorDescription;

    private String productId;

    private String productName;

    private UUID idProspect;

    private UUID idRequest;

    private Integer prospectSourceId;

    private UUID leasingCompanyId;

    private Integer productYear;

    private Integer approvalDiscount;

    private Integer approvalTerritorial;

    private Integer approvalLeasing;

    private String requestIdMachineAndFrame;

    private BigDecimal minPayment;

    private Boolean unitHotItem;

    private Boolean unitIndent;

    private Boolean waitStnk;

    private BigDecimal creditInstallment;

    private Integer creditTenor;

    private BigDecimal creditDownPayment;

    private String poNumber;

    private ZonedDateTime podate;

    private BigDecimal komisiSales;

    private String leasingName;

    private String korsalName;

    private LocalDateTime dtApproval;

    private String leasingnote;

    private String lastModifiedBy;

    private String ipAddress;

    private String resetKey;

    private UUID idGudang;

    public UUID getIdGudang() { return idGudang; }

    public void setIdGudang(UUID idGudang) { this.idGudang = idGudang; }

    public String getResetKey() { return resetKey; }

    public void setResetKey(String resetKey) { this.resetKey = resetKey; }

    public String getIpAddress() { return ipAddress; }

    public void setIpAddress(String ipAddress) { this.ipAddress = ipAddress; }

    public String getLastModifiedBy() { return lastModifiedBy; }

    public void setLastModifiedBy(String lastModifiedBy) { this.lastModifiedBy = lastModifiedBy; }

    public String getLeasingnote() { return leasingnote; }

    public void setLeasingnote(String leasingnote) { this.leasingnote = leasingnote; }

    public LocalDateTime getDtApproval() { return dtApproval; }

    public void setDtApproval(LocalDateTime dtApproval) { this.dtApproval = dtApproval; }

    public String getKorsalName() { return korsalName; }

    public void setKorsalName(String korsalName) { this.korsalName = korsalName; }

    public String getLeasingName() { return leasingName; }

    public void setLeasingName(String leasingName) { this.leasingName = leasingName; }

    public BigDecimal getKomisiSales() {
        return komisiSales;
    }

    public void setKomisiSales(BigDecimal komisiSales) {
        this.komisiSales = komisiSales;
    }

    public UUID getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(UUID idRequest) {
        this.idRequest = idRequest;
    }

    public Integer getApprovalLeasing() { return approvalLeasing; }

    public void setApprovalLeasing(Integer approvalLeasing) { this.approvalLeasing = approvalLeasing; }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public ZonedDateTime getPodate() {
        return podate;
    }

    public void setPodate(ZonedDateTime podate) {
        this.podate = podate;
    }

    public Integer getApprovalTerritorial() {
        return approvalTerritorial;
    }

    public void setApprovalTerritorial(Integer approvalTerritorial) {
        this.approvalTerritorial = approvalTerritorial;
    }

    public BigDecimal getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(BigDecimal unitprice) {
        this.unitprice = unitprice;
    }

    public BigDecimal getCreditInstallment() {
        return creditInstallment;
    }

    public void setCreditInstallment(BigDecimal creditInstallment) {
        this.creditInstallment = creditInstallment;
    }

    public Integer getCreditTenor() {
        return creditTenor;
    }

    public void setCreditTenor(Integer creditTenor) {
        this.creditTenor = creditTenor;
    }

    public BigDecimal getCreditDownPayment() {
        return creditDownPayment;
    }

    public void setCreditDownPayment(BigDecimal creditDownPayment) {
        this.creditDownPayment = creditDownPayment;
    }

    public Boolean getWaitStnk() {
        return waitStnk;
    }

    public void setWaitStnk(Boolean waitStnk) {
        this.waitStnk = waitStnk;
    }

    public Boolean getUnitHotItem() {
        return unitHotItem;
    }

    public void setUnitHotItem(Boolean unitHotItem) {
        this.unitHotItem = unitHotItem;
    }

    public Boolean getUnitIndent() {
        return unitIndent;
    }

    public void setUnitIndent(Boolean unitIndent) {
        this.unitIndent = unitIndent;
    }

    public BigDecimal getMinPayment() {
        return minPayment;
    }

    public void setMinPayment(BigDecimal minPayment) {
        this.minPayment = minPayment;
    }

    public String getRequestIdMachineAndFrame() { return requestIdMachineAndFrame; }

    public void setRequestIdMachineAndFrame(String requestIdMachineAndFrame) { this.requestIdMachineAndFrame = requestIdMachineAndFrame; }

    public String getRequestIdFrame() {
        return requestIdFrame;
    }

    public void setRequestIdFrame(String requestIdFrame) {
        this.requestIdFrame = requestIdFrame;
    }

    public String getRequestIdMachine() {
        return requestIdMachine;
    }

    public void setRequestIdMachine(String requestIdMachine) {
        this.requestIdMachine = requestIdMachine;
    }

    public Integer getProspectSourceId() {
        return prospectSourceId;
    }

    public void setProspectSourceId(Integer prospectSourceId) {
        this.prospectSourceId = prospectSourceId;
    }

    public BigDecimal getBbnprice() {
        return bbnprice;
    }

    public void setBbnprice(BigDecimal bbnprice) {
        this.bbnprice = bbnprice;
    }

    public BigDecimal getHetprice() {
        return hetprice;
    }

    public void setHetprice(BigDecimal hetprice) {
        this.hetprice = hetprice;
    }

    public Integer getApprovalDiscount() {
        return approvalDiscount;
    }

    public void setApprovalDiscount(Integer approvalDiscount) {
        this.approvalDiscount = approvalDiscount;
    }

    public BigDecimal getBrokerFee() { return brokerFee; }

    public void setBrokerFee(BigDecimal brokerFee) { this.brokerFee = brokerFee; }

    public UUID getLeasingCompanyId() {
        return leasingCompanyId;
    }

    public void setLeasingCompanyId(UUID leasingCompanyId) {
        this.leasingCompanyId = leasingCompanyId;
    }

//    public UUID getLeasingTenorProvideId() {
//        return leasingTenorProvideId;
//    }
//
//    public void setLeasingTenorProvideId(UUID leasingTenorProvideId) {
//        this.leasingTenorProvideId = leasingTenorProvideId;
//    }

    public Integer getProductYear() { return productYear; }

    public void setProductYear(Integer productYear) { this.productYear = productYear; }

    //requestpoliceid

    public String getRequestpoliceid() {
        return requestpoliceid;
    }

    public void setRequestpoliceid(String requestpoliceid) {
        this.requestpoliceid = requestpoliceid;
    }

    //price

    public BigDecimal getUnitPrice() { return unitprice; }

    public void setUnitPrice(BigDecimal surprice) { this.unitprice = surprice; }

    //note rekanan

    public String getNotePartner() {
        return notepartner;
    }

    public void setNotePartner(String note) {
        this.notepartner = note;
    }

    //shipping note

    public String getNoteshipment() {
        return noteshipment;
    }

    public void setNoteshipment(String noteshipment) {
        this.noteshipment = noteshipment;
    }


    //note nomor machine

    public String getIdmachine() {
        return idmachine;
    }

    public void setIdmachine(String idmachine) {
        this.idmachine = idmachine;
    }

    public String getIdframe() {
        return idframe;
    }

    public void setIdframe(String idframe) {
        this.idframe = idframe;
    }

    public BigDecimal getSubsfincomp() {
        return subsfincomp;
    }

    public void setSubsfincomp(BigDecimal subsfincomp) {
        this.subsfincomp = subsfincomp;
    }

    public BigDecimal getSubsmd() {
        return subsmd;
    }

    public void setSubsmd(BigDecimal subsmd) {
        this.subsmd = subsmd;
    }

    public BigDecimal getSubsown() {
        return subsown;
    }

    public void setSubsown(BigDecimal subsown) {
        this.subsown = subsown;
    }

    public BigDecimal getSubsahm() {
        return subsahm;
    }

    public void setSubsahm(BigDecimal subsahm) {
        this.subsahm = subsahm;
    }

    //note

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    //person owner

    public PersonDTO getPersonOwner() {
        return personOwner;
    }

    public void setPersonOwner(PersonDTO personOwner) {
        this.personOwner = personOwner;
    }

    //down payment

    public BigDecimal getDownPayment() {
        return downPayment;
    }

    public void setDownPayment(BigDecimal downPayment) {
        this.downPayment = downPayment;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public UUID getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(UUID salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public Integer getSaleTypeId() {
        return saleTypeId;
    }

    public void setSaleTypeId(Integer saleTypeId) {
        this.saleTypeId = saleTypeId;
    }

    public String getSaleTypeDescription() {
        return saleTypeDescription;
    }

    public void setSaleTypeDescription(String saleTypeDescription) {
        this.saleTypeDescription = saleTypeDescription;
    }

    public String getBillToId() {
        return billToId;
    }

    public void setBillToId(String billToId) {
        this.billToId = billToId;
    }

    public String getBillToName() {
        return billToName;
    }

    public void setBillToName(String billToName) {
        this.billToName = billToName;
    }

    public UUID getBrokerId() {
        return brokerId;
    }

    public void setBrokerId(UUID salesBrokerId) {
        this.brokerId = salesBrokerId;
    }

//    public String getBrokerName() {
//        return brokerName;
//    }
//
//    public void setBrokerName(String salesBrokerName) {
//        this.brokerName = salesBrokerName;
//    }

    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer featureId) {
        this.colorId = featureId;
    }

    public String getColorDescription() {
        return colorDescription;
    }

    public void setColorDescription(String featureDescription) {
        this.colorDescription = featureDescription;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String motorId) {
        this.productId = motorId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String motorName) {
        this.productName = motorName;
    }

    public String getNotepartner() {
        return notepartner;
    }

    public void setNotepartner(String notepartner) {
        this.notepartner = notepartner;
    }

    public UUID getIdProspect() {
        return idProspect;
    }

    public void setIdProspect(UUID idProspect) {
        this.idProspect = idProspect;
    }

    public OrganizationDTO getOrganizationOwner() {
        return organizationOwner;
    }

    public void setOrganizationOwner(OrganizationDTO organizationOwner) {
        this.organizationOwner = organizationOwner;
    }

    @Override
    public String toString() {
        return "SalesUnitRequirementDTO{" +
            "id=" + getIdRequirement() +
            ", downPayment='" + getDownPayment() + "'" +
            "}";
    }
}
