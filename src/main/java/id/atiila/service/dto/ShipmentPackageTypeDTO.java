package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ShipmentPackageType entity.
 * BeSmart Team
 */

public class ShipmentPackageTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idPackageType;

    private String description;

    public ShipmentPackageTypeDTO() {
    }

    public ShipmentPackageTypeDTO(Integer idPackageType, String description) {
        this.idPackageType = idPackageType;
        this.description = description;
    }

    public Integer getIdPackageType() {
        return this.idPackageType;
    }

    public void setIdPackageType(Integer id) {
        this.idPackageType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.idPackageType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShipmentPackageTypeDTO shipmentPackageTypeDTO = (ShipmentPackageTypeDTO) o;
        if (shipmentPackageTypeDTO.getIdPackageType() == null || getIdPackageType() == null) {
            return false;
        }
        return Objects.equals(getIdPackageType(), shipmentPackageTypeDTO.getIdPackageType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPackageType());
    }

    @Override
    public String toString() {
        return "ShipmentPackageTypeDTO{" +
            "id=" + getIdPackageType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
