package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the Employee entity.
 * atiila consulting
 */

public class EmployeeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idEmployee;

    private Integer idRoleType;

    private UUID personId;

    public String getIdEmployee() {
        return this.idEmployee;
    }

    public void setIdEmployee(String id) {
        this.idEmployee = id;
    }

    public Integer getIdRoleType() {
        return idRoleType;
    }

    public void setIdRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
    }

    public UUID getPersonId() {
        return personId;
    }

    public void setPersonId(UUID personId) {
        this.personId = personId;
    }

    public String getId() {
        return this.idEmployee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EmployeeDTO employeeDTO = (EmployeeDTO) o;
        if (employeeDTO.getIdEmployee() == null || getIdEmployee() == null) {
            return false;
        }
        return Objects.equals(getIdEmployee(), employeeDTO.getIdEmployee());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdEmployee());
    }

    @Override
    public String toString() {
        return "EmployeeDTO{" +
            "id=" + getIdEmployee() +
            ", idRoleType='" + getIdRoleType() + "'" +
            "}";
    }
}
