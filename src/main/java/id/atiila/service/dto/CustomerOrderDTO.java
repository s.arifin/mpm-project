package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the CustomerOrder entity.
 * BeSmart Team
 */

public class CustomerOrderDTO extends OrdersDTO {

    private static final long serialVersionUID = 1L;

    private String internalId;

    private String customerId;

    private String billToId;

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getBillToId() {
        return billToId;
    }

    public void setBillToId(String billToId) {
        this.billToId = billToId;
    }

    @Override
    public String toString() {
        return "CustomerOrderDTO{" +
            "id=" + getIdOrder() +
            "}";
    }
}
