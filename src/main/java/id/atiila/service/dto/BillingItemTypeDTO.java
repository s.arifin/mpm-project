package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the BillingItemType entity.
 * atiila consulting
 */

public class BillingItemTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idItemType;

    private String description;

    public Integer getIdItemType() {
        return this.idItemType;
    }

    public void setIdItemType(Integer id) {
        this.idItemType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.idItemType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BillingItemTypeDTO billingItemTypeDTO = (BillingItemTypeDTO) o;
        if (billingItemTypeDTO.getIdItemType() == null || getIdItemType() == null) {
            return false;
        }
        return Objects.equals(getIdItemType(), billingItemTypeDTO.getIdItemType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdItemType());
    }

    @Override
    public String toString() {
        return "BillingItemTypeDTO{" +
            "id=" + getIdItemType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
