package id.atiila.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the StockOpnameItem entity.
 * BeSmart Team
 */

public class StockOpnameItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idStockopnameItem;

    private String itemDescription;

    private Integer qty;

    private Integer qtyCount;

    private String tagNumber;

    private Boolean hasChecked;

    private BigDecimal het;

    private UUID stockOpnameId;

    private String productId;

    private String productName;

    private UUID containerId;

    private String containerCode;

    public UUID getIdStockopnameItem() {
        return this.idStockopnameItem;
    }

    public void setIdStockopnameItem(UUID id) {
        this.idStockopnameItem = id;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getQtyCount() {
        return qtyCount;
    }

    public void setQtyCount(Integer qtyCount) {
        this.qtyCount = qtyCount;
    }

    public String getTagNumber() {
        return tagNumber;
    }

    public void setTagNumber(String tagNumber) {
        this.tagNumber = tagNumber;
    }

    public Boolean isHasChecked() {
        return hasChecked;
    }

    public void setHasChecked(Boolean hasChecked) {
        this.hasChecked = hasChecked;
    }

    public BigDecimal getHet() {
        return het;
    }

    public void setHet(BigDecimal het) {
        this.het = het;
    }

    public UUID getStockOpnameId() {
        return stockOpnameId;
    }

    public void setStockOpnameId(UUID stockOpnameId) {
        this.stockOpnameId = stockOpnameId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public UUID getContainerId() {
        return containerId;
    }

    public void setContainerId(UUID containerId) {
        this.containerId = containerId;
    }

    public Boolean getHasChecked() {
        return hasChecked;
    }

    public String getContainerCode() {
        return containerCode;
    }

    public void setContainerCode(String containerCode) {
        this.containerCode = containerCode;
    }

    public UUID getId() {
        return this.idStockopnameItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StockOpnameItemDTO stockOpnameItemDTO = (StockOpnameItemDTO) o;
        if (stockOpnameItemDTO.getIdStockopnameItem() == null || getIdStockopnameItem() == null) {
            return false;
        }
        return Objects.equals(getIdStockopnameItem(), stockOpnameItemDTO.getIdStockopnameItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdStockopnameItem());
    }

    @Override
    public String toString() {
        return "StockOpnameItemDTO{" +
            "id=" + getIdStockopnameItem() +
            ", itemDescription='" + getItemDescription() + "'" +
            ", qty='" + getQty() + "'" +
            ", qtyCount='" + getQtyCount() + "'" +
            ", tagNumber='" + getTagNumber() + "'" +
            ", hasChecked='" + isHasChecked() + "'" +
            ", het='" + getHet() + "'" +
            "}";
    }
}
