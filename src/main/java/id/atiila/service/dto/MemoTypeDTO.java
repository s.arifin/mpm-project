package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the MemoType entity.
 * BeSmart Team
 */

public class MemoTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idMemoType;

    private String description;

    public Integer getIdMemoType() {
        return this.idMemoType;
    }

    public void setIdMemoType(Integer id) {
        this.idMemoType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.idMemoType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MemoTypeDTO memoTypeDTO = (MemoTypeDTO) o;
        if (memoTypeDTO.getIdMemoType() == null || getIdMemoType() == null) {
            return false;
        }
        return Objects.equals(getIdMemoType(), memoTypeDTO.getIdMemoType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdMemoType());
    }

    @Override
    public String toString() {
        return "MemoTypeDTO{" +
            "id=" + getIdMemoType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
