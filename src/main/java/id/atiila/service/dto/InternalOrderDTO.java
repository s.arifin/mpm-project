package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the InternalOrder entity.
 * BeSmart Team
 */

public class InternalOrderDTO extends OrdersDTO {

    private static final long serialVersionUID = 1L;

    private String internalFromId;

    private String internalFromIdInternal;

    private String internalToId;

    private String internalToIdInternal;

    public String getInternalFromId() {
        return internalFromId;
    }

    public void setInternalFromId(String internalId) {
        this.internalFromId = internalId;
    }

    public String getInternalFromIdInternal() {
        return internalFromIdInternal;
    }

    public void setInternalFromIdInternal(String internalIdInternal) {
        this.internalFromIdInternal = internalIdInternal;
    }

    public String getInternalToId() {
        return internalToId;
    }

    public void setInternalToId(String internalId) {
        this.internalToId = internalId;
    }

    public String getInternalToIdInternal() {
        return internalToIdInternal;
    }

    public void setInternalToIdInternal(String internalIdInternal) {
        this.internalToIdInternal = internalIdInternal;
    }

    @Override
    public String toString() {
        return "InternalOrderDTO{" +
            "id=" + getIdOrder() +
            "}";
    }
}
