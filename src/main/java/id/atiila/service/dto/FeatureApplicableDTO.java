package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the FeatureApplicable entity.
 * atiila consulting
 */

public class FeatureApplicableDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idApplicability;

    private Boolean boolValue;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private Integer featureTypeId;

    private String featureTypeDescription;

    private Integer featureId;

    private String featureRefKey;

    private String featureDescription;

    private String productId;

    private String productName;

    public UUID getIdApplicability() {
        return this.idApplicability;
    }

    public void setIdApplicability(UUID id) {
        this.idApplicability = id;
    }

    public Boolean isBoolValue() {
        return boolValue;
    }

    public void setBoolValue(Boolean boolValue) {
        this.boolValue = boolValue;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Integer getFeatureTypeId() {
        return featureTypeId;
    }

    public void setFeatureTypeId(Integer featureTypeId) {
        this.featureTypeId = featureTypeId;
    }

    public String getFeatureTypeDescription() {
        return featureTypeDescription;
    }

    public void setFeatureTypeDescription(String featureTypeDescription) {
        this.featureTypeDescription = featureTypeDescription;
    }

    public Integer getFeatureId() {
        return featureId;
    }

    public void setFeatureId(Integer featureId) {
        this.featureId = featureId;
    }

    public String getFeatureDescription() {
        return featureDescription;
    }

    public void setFeatureDescription(String featureDescription) {
        this.featureDescription = featureDescription;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public UUID getId() {
        return this.idApplicability;
    }

    public String getFeatureRefKey() {
        return featureRefKey;
    }

    public void setFeatureRefKey(String featureRefKey) {
        this.featureRefKey = featureRefKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FeatureApplicableDTO featureApplicableDTO = (FeatureApplicableDTO) o;
        if (featureApplicableDTO.getIdApplicability() == null || getIdApplicability() == null) {
            return false;
        }
        return Objects.equals(getIdApplicability(), featureApplicableDTO.getIdApplicability());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdApplicability());
    }

    @Override
    public String toString() {
        return "FeatureApplicableDTO{" +
            "id=" + getIdApplicability() +
            ", boolValue='" + isBoolValue() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
