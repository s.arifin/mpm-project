package id.atiila.service.dto;

import org.springframework.data.annotation.Id;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

/**
 * A DTO for the Requirement entity.
 * BeSmart Team
 */

public class RequirementDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private UUID idRequirement;

    private String requirementNumber;

    private String description;

    private ZonedDateTime dateCreate;

    private ZonedDateTime dateRequired;

    private BigDecimal budget;

    private Integer qty;

    private Integer idReqTyp;

    private Integer qtyFilled;

    private Integer currentStatus;

    private Set<PaymentApplicationDTO> payments = new HashSet<>();

    private UUID facilityId;

    private String facilityDescription;

    private UUID parentId;

    private String parentDescription;

    private Integer requirementTypeId;

    // Getter Setter

    public UUID getIdRequirement() {
        return this.idRequirement;
    }

    public void setIdRequirement(UUID id) {
        this.idRequirement = id;
    }

    public String getRequirementNumber() {
        return requirementNumber;
    }

    public void setRequirementNumber(String requirementNumber) {
        this.requirementNumber = requirementNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public ZonedDateTime getDateRequired() {
        return dateRequired;
    }

    public void setDateRequired(ZonedDateTime dateRequired) {
        this.dateRequired = dateRequired;
    }

    public BigDecimal getBudget() {
        return budget;
    }

    public void setBudget(BigDecimal budget) {
        this.budget = budget;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

//    public Integer getIdReqTyp() {
//        return idReqTyp;
//    }
//
//    public void setIdReqTyp(Integer idReqTyp) {
//        this.idReqTyp = idReqTyp;
//    }


    public Integer getIdReqTyp() {
        return idReqTyp;
    }

    public void setIdReqTyp(Integer idReqTyp) {
        this.idReqTyp = idReqTyp;
    }

    public Set<PaymentApplicationDTO> getPayments() {
        return payments;
    }

    public void setPayments(Set<PaymentApplicationDTO> paymentApplications) {
        this.payments = paymentApplications;
    }

    public UUID getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(UUID facilityId) {
        this.facilityId = facilityId;
    }

    public String getFacilityDescription() {
        return facilityDescription;
    }

    public void setFacilityDescription(String facilityDescription) {
        this.facilityDescription = facilityDescription;
    }

    public UUID getParentId() {
        return parentId;
    }

    public void setParentId(UUID requirementId) {
        this.parentId = requirementId;
    }

    public String getParentDescription() {
        return parentDescription;
    }

    public void setParentDescription(String requirementDescription) {
        this.parentDescription = requirementDescription;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public Integer getQtyFilled() {
        return qtyFilled;
    }

    public void setQtyFilled(Integer qtyFilled) {
        this.qtyFilled = qtyFilled;
    }

    public Integer getRequirementTypeId() {
        return requirementTypeId;
    }

    public void setRequirementTypeId(Integer requirementTypeId) {
        this.requirementTypeId = requirementTypeId;
    }

    public UUID getId() {
        return this.idRequirement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequirementDTO requirementDTO = (RequirementDTO) o;
        if (requirementDTO.getIdRequirement() == null || getIdRequirement() == null) {
            return false;
        }
        return Objects.equals(getIdRequirement(), requirementDTO.getIdRequirement());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRequirement());
    }

    @Override
    public String toString() {
        return "RequirementDTO{" +
            "id=" + getIdRequirement() +
            ", requirementNumber='" + getRequirementNumber() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", dateRequired='" + getDateRequired() + "'" +
            ", budget='" + getBudget() + "'" +
            ", qty='" + getQty() + "'" +
            "}";
    }
}
