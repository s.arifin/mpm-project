package id.atiila.service.dto;

import java.io.Serializable;
import java.util.UUID;

public class SummaryProspectDTO implements Serializable {

    private UUID salesmanId;

    private Integer statustypeId;

    private Long statusTotal;

    public SummaryProspectDTO(UUID idPartyRole, Integer idStatusType, Long totalStatus){
        this.salesmanId = idPartyRole;
        this.statustypeId = idStatusType;
        this.statusTotal = totalStatus;
    }

    public UUID getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(UUID salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Integer getStatustypeId() {
        return statustypeId;
    }

    public void setStatustypeId(Integer statustypeId) {
        this.statustypeId = statustypeId;
    }

    public Long getStatusTotal() { return statusTotal; }

    public void setStatusTotal(Long statusTotal) { this.statusTotal = statusTotal; }
}
