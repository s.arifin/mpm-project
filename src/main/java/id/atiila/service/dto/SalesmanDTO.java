package id.atiila.service.dto;

import java.util.UUID;

/**
 * A DTO for the Salesman entity.
 * BeSmart Team
 */

public class SalesmanDTO extends PartyRoleDTO {

    private static final long serialVersionUID = 1L;

    private Boolean coordinator;

    private Boolean teamLeader;

    private UUID coordinatorSalesId;

    private String coordinatorSalesName;

    private UUID teamLeaderSalesId;

    private String teamLeaderSalesName;

    private String idSalesman;

    private String ahmCode;

    private PersonDTO person = new PersonDTO();

    private String userName;

    public String getUserName() { return userName; }

    public void setUserName(String userName) { this.userName = userName; }

    public String getIdSalesman() {
        return idSalesman;
    }

    public void setIdSalesman(String idSalesman) {
        this.idSalesman = idSalesman;
    }

    public Boolean isCoordinator() {
        return coordinator;
    }

    public void setCoordinator(Boolean coordinator) {
        this.coordinator = coordinator;
    }

    public Boolean isTeamLeader() { return teamLeader; }

    public void setTeamLeader(Boolean teamLeader) { this.teamLeader = teamLeader; }

    public UUID getCoordinatorSalesId() {
        return coordinatorSalesId;
    }

    public void setCoordinatorSalesId(UUID salesmanId) {
        this.coordinatorSalesId = salesmanId;
    }

    public String getCoordinatorSalesName() {
        return coordinatorSalesName;
    }

    public void setCoordinatorSalesName(String salesmanName) {
        this.coordinatorSalesName = salesmanName;
    }

    public UUID getTeamLeaderSalesId() { return teamLeaderSalesId; }

    public void setTeamLeaderSalesId(UUID teamLeaderSalesId) { this.teamLeaderSalesId = teamLeaderSalesId; }

    public String getTeamLeaderSalesName() { return teamLeaderSalesName; }

    public void setTeamLeaderSalesName(String teamLeaderSalesName) { this.teamLeaderSalesName = teamLeaderSalesName; }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    public String getAhmCode() {
        return ahmCode;
    }

    public void setAhmCode(String ahmCode) {
        this.ahmCode = ahmCode;
    }

    @Override
    public String toString() {
        return "SalesmanDTO{" +
            "id=" + getIdPartyRole() +
            ", idSalesman='" + getIdSalesman() + "'" +
            ", coordinator='" + isCoordinator() + "'" +
            ", currentStatus='" + getCurrentStatus() + "'" +
            ", dateRegister='" + getDateRegister() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
