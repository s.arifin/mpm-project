package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the TelecomunicationNumber entity.
 * BeSmart Team
 */

public class TelecomunicationNumberDTO extends ContactMechanismDTO {

    private static final long serialVersionUID = 1L;

    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "TelecomunicationNumberDTO{" +
            "id=" + getIdContact() +
            ", number='" + getNumber() + "'" +
            "}";
    }
}
