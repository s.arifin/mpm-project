package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the GeoBoundary entity.
 * BeSmart Team
 */

public class GeoBoundaryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idGeobou;

    private String geoCode;

    private String description;

    private Integer geoBoundaryTypeId;

    private UUID parentId;

    private String parentGeoCode;

    public UUID getIdGeobou() {
        return this.idGeobou;
    }

    public void setIdGeobou(UUID id) {
        this.idGeobou = id;
    }

    public String getGeoCode() { return geoCode; }

    public void setGeoCode(String geoCode) { this.geoCode = geoCode; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getGeoBoundaryTypeId() {
        return geoBoundaryTypeId;
    }

    public void setGeoBoundaryTypeId(Integer geoBoundaryTypeId) {
        this.geoBoundaryTypeId = geoBoundaryTypeId;
    }

    public UUID getParentId() {
        return parentId;
    }

    public void setParentId(UUID parentId) {
        this.parentId = parentId;
    }

    public String getParentGeoCode() {
        return parentGeoCode;
    }

    public void setParentGeoCode(String parentGeoCode) {
        this.parentGeoCode = parentGeoCode;
    }

    public UUID getId() {
        return this.idGeobou;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GeoBoundaryDTO e = (GeoBoundaryDTO) o;
        if (e.getIdGeobou() == null || getIdGeobou() == null) {
            return false;
        }
        return Objects.equals(getIdGeobou(), e.getIdGeobou());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdGeobou());
    }

    @Override
    public String toString() {
        return "GeoBoundaryDTO{" +
            "id=" + getIdGeobou() +
            ", geocode='" + getGeoCode() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
