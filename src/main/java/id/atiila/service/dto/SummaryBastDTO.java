package id.atiila.service.dto;

import java.io.Serializable;
import java.util.UUID;

public class SummaryBastDTO implements Serializable {

    private String submissionNo;

    public SummaryBastDTO(String submissionno){
        this.submissionNo = submissionno;
    }

    public String getSubmissionNo() {
        return submissionNo;
    }

    public void setSubmissionNo(String submissionNo) {
        this.submissionNo = submissionNo;
    }


}
