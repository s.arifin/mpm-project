package id.atiila.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the QuoteItem entity.
 * BeSmart Team
 */

public class QuoteItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idQuoteItem;

    private BigDecimal unitPrice;

    private Integer qty;

    private UUID quoteId;

    private String quoteIdquote;

    private String productId;

    private String productName;

    public UUID getIdQuoteItem() {
        return this.idQuoteItem;
    }

    public void setIdQuoteItem(UUID id) {
        this.idQuoteItem = id;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public UUID getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(UUID quoteId) {
        this.quoteId = quoteId;
    }

    public String getQuoteIdquote() {
        return quoteIdquote;
    }

    public void setQuoteIdquote(String quoteIdquote) {
        this.quoteIdquote = quoteIdquote;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
    
    public UUID getId() {
        return this.idQuoteItem;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        QuoteItemDTO quoteItemDTO = (QuoteItemDTO) o;
        if (quoteItemDTO.getIdQuoteItem() == null || getIdQuoteItem() == null) {
            return false;
        }
        return Objects.equals(getIdQuoteItem(), quoteItemDTO.getIdQuoteItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdQuoteItem());
    }

    @Override
    public String toString() {
        return "QuoteItemDTO{" +
            "id=" + getIdQuoteItem() +
            ", unitPrice='" + getUnitPrice() + "'" +
            ", qty='" + getQty() + "'" +
            "}";
    }
}
