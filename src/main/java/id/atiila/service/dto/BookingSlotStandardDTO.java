package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the BookingSlotStandard entity.
 * BeSmart Team
 */

public class BookingSlotStandardDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idBookSlotStd;

    private String description;

    private Integer capacity;

    private Integer startHour;

    private Integer startMinute;

    private Integer endHour;

    private Integer endMinute;

    private String internalId;

    private String internalName;

    private Integer bookingTypeId;

    private String bookingTypeDescription;

    public UUID getIdBookSlotStd() {
        return this.idBookSlotStd;
    }

    public void setIdBookSlotStd(UUID id) {
        this.idBookSlotStd = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Integer getStartHour() {
        return startHour;
    }

    public void setStartHour(Integer startHour) {
        this.startHour = startHour;
    }

    public Integer getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(Integer startMinute) {
        this.startMinute = startMinute;
    }

    public Integer getEndHour() {
        return endHour;
    }

    public void setEndHour(Integer endHour) {
        this.endHour = endHour;
    }

    public Integer getEndMinute() {
        return endMinute;
    }

    public void setEndMinute(Integer endMinute) {
        this.endMinute = endMinute;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public Integer getBookingTypeId() {
        return bookingTypeId;
    }

    public void setBookingTypeId(Integer bookingTypeId) {
        this.bookingTypeId = bookingTypeId;
    }

    public String getBookingTypeDescription() {
        return bookingTypeDescription;
    }

    public void setBookingTypeDescription(String bookingTypeDescription) {
        this.bookingTypeDescription = bookingTypeDescription;
    }
    
    public UUID getId() {
        return this.idBookSlotStd;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BookingSlotStandardDTO bookingSlotStandardDTO = (BookingSlotStandardDTO) o;
        if (bookingSlotStandardDTO.getIdBookSlotStd() == null || getIdBookSlotStd() == null) {
            return false;
        }
        return Objects.equals(getIdBookSlotStd(), bookingSlotStandardDTO.getIdBookSlotStd());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdBookSlotStd());
    }

    @Override
    public String toString() {
        return "BookingSlotStandardDTO{" +
            "id=" + getIdBookSlotStd() +
            ", description='" + getDescription() + "'" +
            ", capacity='" + getCapacity() + "'" +
            ", startHour='" + getStartHour() + "'" +
            ", startMinute='" + getStartMinute() + "'" +
            ", endHour='" + getEndHour() + "'" +
            ", endMinute='" + getEndMinute() + "'" +
            "}";
    }
}
