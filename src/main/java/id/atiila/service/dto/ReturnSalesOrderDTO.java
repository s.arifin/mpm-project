package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the ReturnSalesOrder entity.
 * BeSmart Team
 */

public class ReturnSalesOrderDTO extends CustomerOrderDTO {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "ReturnSalesOrderDTO{" +
            "id=" + getIdOrder() +
            "}";
    }
}
