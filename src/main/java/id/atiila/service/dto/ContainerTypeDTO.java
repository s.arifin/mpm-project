package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the ContainerType entity.
 * BeSmart Team
 */

public class ContainerTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idContainerType;

    private String description;

    public Integer getIdContainerType() {
        return this.idContainerType;
    }

    public void setIdContainerType(Integer id) {
        this.idContainerType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.idContainerType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContainerTypeDTO containerTypeDTO = (ContainerTypeDTO) o;
        if (containerTypeDTO.getIdContainerType() == null || getIdContainerType() == null) {
            return false;
        }
        return Objects.equals(getIdContainerType(), containerTypeDTO.getIdContainerType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdContainerType());
    }

    @Override
    public String toString() {
        return "ContainerTypeDTO{" +
            "id=" + getIdContainerType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
