package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the VendorProduct entity.
 * BeSmart Team
 */

public class VendorProductDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idVendorProduct;

    private Integer orderRatio;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private String productId;

    private String productName;

    private String internalId;

    private String internalIdInternal;

    private String vendorId;

    private String vendorName;

    public UUID getIdVendorProduct() {
        return this.idVendorProduct;
    }

    public void setIdVendorProduct(UUID id) {
        this.idVendorProduct = id;
    }

    public Integer getOrderRatio() {
        return orderRatio;
    }

    public void setOrderRatio(Integer orderRatio) {
        this.orderRatio = orderRatio;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getInternalIdInternal() {
        return internalIdInternal;
    }

    public void setInternalIdInternal(String internalIdInternal) {
        this.internalIdInternal = internalIdInternal;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public UUID getId() {
        return this.idVendorProduct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VendorProductDTO vendorProductDTO = (VendorProductDTO) o;
        if (vendorProductDTO.getIdVendorProduct() == null || getIdVendorProduct() == null) {
            return false;
        }
        return Objects.equals(getIdVendorProduct(), vendorProductDTO.getIdVendorProduct());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdVendorProduct());
    }

    @Override
    public String toString() {
        return "VendorProductDTO{" +
            "id=" + getIdVendorProduct() +
            ", orderRatio='" + getOrderRatio() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
