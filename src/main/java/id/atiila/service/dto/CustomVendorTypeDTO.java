package id.atiila.service.dto;

import java.io.Serializable;

public class CustomVendorTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idVendorType;

    private String description;

    public String getIdVendorType() {
        return idVendorType;
    }

    public void setIdVendorType(String idVendorType) {
        this.idVendorType = idVendorType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
