package id.atiila.service.dto;

/**
 * A DTO for the Mechanic entity.
 * BeSmart Team
 */

public class MechanicDTO extends PartyRoleDTO {

    private static final long serialVersionUID = 1L;

    private String idMechanic;

    private String idVendor;

    private String external;

    private PersonDTO person = new PersonDTO();

    public String getIdMechanic() {
        return idMechanic;
    }

    public void setIdMechanic(String idMechanic) {
        this.idMechanic = idMechanic;
    }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    public String getIdVendor() { return idVendor; }

    public void setIdVendor(String idVendor) { this.idVendor = idVendor; }

    public String getExternal() {
        return external;
    }

    public void setExternal(String external) {
        this.external = external;
    }

    //    public String getExternal() {
//        return external;
//    }
//
//    public void setExternal(String external) {
//        this.external = external;
//    }

    @Override
    public String toString() {
        return "MechanicDTO{" +
            "id=" + getIdPartyRole() +
            ", idMechanic='" + getIdMechanic() + "'" +
            ", external='" + getExternal() + "'" +
            ", idVendor='" + getIdVendor() + "'" +
            "}";
    }
}
