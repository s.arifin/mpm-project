package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the CommunicationEventCDB entity.
 * BeSmart Team
 */

public class CommunicationEventCDBDTO extends CommunicationEventDTO {

    private static final long serialVersionUID = 1L;

    private String idCustomer;

    private String hobby;

    private String homeStatus;

    private String expenditureOneMonth;

    private String job;

    private String lastEducation;

    private String mobilePhoneOwnershipStatus;

    private String infoAboutHonda;

    private String vehicleBrandOwner;

    private String vehicleTypeOwner;

    private String buyFor;

    private String vehicleUser;

    private String facebook;

    private String instagram;

    private String twitter;

    private String youtube;

    private String email;

    private Integer idReligion;

    private Integer isCityzenIdCardAddress;

    private Integer isMailAddress;

    private UUID idProvince;

    private UUID idCity;

    private UUID idDistrict;

    private UUID idVillage;

    private UUID idVillageSurat;

    private String address;

    private UUID idProvinceSurat;

    private UUID idCitySurat;

    private UUID idDistrictSurat;

    private String addressSurat;

    private String jenisPembayaran;

    private String groupCustomer;

    private String keterangan;

    private String citizenship;

    private String postalCode;

    private String postalCodeSurat;

    private UUID idSalesUnitRequirement;

    public UUID getIdSalesUnitRequirement() {
        return idSalesUnitRequirement;
    }

    public void setIdSalesUnitRequirement(UUID idSalesUnitRequirement) {
        this.idSalesUnitRequirement = idSalesUnitRequirement;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCodeSurat() {
        return postalCodeSurat;
    }

    public void setPostalCodeSurat(String postalCodeSurat) {
        this.postalCodeSurat = postalCodeSurat;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getHomeStatus() {
        return homeStatus;
    }

    public void setHomeStatus(String homeStatus) {
        this.homeStatus = homeStatus;
    }

    public String getExpenditureOneMonth() {
        return expenditureOneMonth;
    }

    public void setExpenditureOneMonth(String expenditureOneMonth) {
        this.expenditureOneMonth = expenditureOneMonth;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getLastEducation() {
        return lastEducation;
    }

    public void setLastEducation(String lastEducation) {
        this.lastEducation = lastEducation;
    }

    public String getMobilePhoneOwnershipStatus() {
        return mobilePhoneOwnershipStatus;
    }

    public void setMobilePhoneOwnershipStatus(String mobilePhoneOwnershipStatus) {
        this.mobilePhoneOwnershipStatus = mobilePhoneOwnershipStatus;
    }

    public String getInfoAboutHonda() {
        return infoAboutHonda;
    }

    public void setInfoAboutHonda(String infoAboutHonda) {
        this.infoAboutHonda = infoAboutHonda;
    }

    public String getVehicleBrandOwner() {
        return vehicleBrandOwner;
    }

    public void setVehicleBrandOwner(String vehicleBrandOwner) {
        this.vehicleBrandOwner = vehicleBrandOwner;
    }

    public String getVehicleTypeOwner() {
        return vehicleTypeOwner;
    }

    public void setVehicleTypeOwner(String vehicleTypeOwner) {
        this.vehicleTypeOwner = vehicleTypeOwner;
    }

    public String getBuyFor() {
        return buyFor;
    }

    public void setBuyFor(String buyFor) {
        this.buyFor = buyFor;
    }

    public String getVehicleUser () {
        return vehicleUser ;
    }

    public void setVehicleUser (String vehicleUser ) {
        this.vehicleUser  = vehicleUser ;
    }

    public String getFacebook() { return facebook; }

    public void setFacebook(String facebook) { this.facebook = facebook; }

    public String getInstagram() { return instagram; }

    public void setInstagram(String instagram) { this.instagram = instagram; }

    public String getTwitter() { return twitter; }

    public void setTwitter(String twitter) { this.twitter = twitter; }

    public String getYoutube() { return youtube; }

    public void setYoutube(String youtube) { this.youtube = youtube; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public Integer getIdReligion() { return idReligion; }

    public void setIdReligion(Integer idReligion) { this.idReligion = idReligion; }

    public Integer getIsCityzenIdCardAddress() { return isCityzenIdCardAddress; }

    public void setIsCityzenIdCardAddress(Integer isCityzenIdCardAddress) { this.isCityzenIdCardAddress = isCityzenIdCardAddress; }

    public Integer getIsMailAddress() { return isMailAddress; }

    public void setIsMailAddress(Integer isMailAddress) { this.isMailAddress = isMailAddress; }

    public UUID getIdProvince() { return idProvince; }

    public void setIdProvince(UUID idProvince) { this.idProvince = idProvince; }

    public UUID getIdCity() { return idCity; }

    public void setIdCity(UUID idCity) { this.idCity = idCity; }

    public UUID getIdDistrict() { return idDistrict; }

    public void setIdDistrict(UUID idDistrict) { this.idDistrict = idDistrict; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public UUID getIdProvinceSurat() {
        return idProvinceSurat;
    }

    public void setIdProvinceSurat(UUID idProvinceSurat) {
        this.idProvinceSurat = idProvinceSurat;
    }

    public UUID getIdCitySurat() {
        return idCitySurat;
    }

    public void setIdCitySurat(UUID idCitySurat) {
        this.idCitySurat = idCitySurat;
    }

    public UUID getIdDistrictSurat() {
        return idDistrictSurat;
    }

    public void setIdDistrictSurat(UUID idDistrictSurat) {
        this.idDistrictSurat = idDistrictSurat;
    }

    public String getAddressSurat() {
        return addressSurat;
    }

    public void setAddressSurat(String addressSurat) {
        this.addressSurat = addressSurat;
    }

    public String getJenisPembayaran() {
        return jenisPembayaran;
    }

    public void setJenisPembayaran(String jenisPembayaran) {
        this.jenisPembayaran = jenisPembayaran;
    }

    public String getGroupCustomer() {
        return groupCustomer;
    }

    public void setGroupCustomer(String groupCustomer) {
        this.groupCustomer = groupCustomer;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public UUID getIdVillage() {
        return idVillage;
    }

    public void setIdVillage(UUID idVillage) {
        this.idVillage = idVillage;
    }

    public UUID getIdVillageSurat() {
        return idVillageSurat;
    }

    public void setIdVillageSurat(UUID idVillageSurat) {
        this.idVillageSurat = idVillageSurat;
    }

    @Override
    public String toString() {
        return "CommunicationEventCDBDTO{" +
            "id=" + getIdCommunicationEvent() +
            ", idCustomer='" + getIdCustomer() + "'" +
            ", hobby='" + getHobby() + "'" +
            ", homeStatus='" + getHomeStatus() + "'" +
            ", expenditureOneMonth='" + getExpenditureOneMonth() + "'" +
            ", job='" + getJob() + "'" +
            ", lastEducation='" + getLastEducation() + "'" +
            ", mobilePhoneOwnershipStatus='" + getMobilePhoneOwnershipStatus() + "'" +
            ", infoAboutHonda='" + getInfoAboutHonda() + "'" +
            ", vehicleBrandOwner='" + getVehicleBrandOwner() + "'" +
            ", vehicleTypeOwner='" + getVehicleTypeOwner() + "'" +
            ", buyFor='" + getBuyFor() + "'" +
            ", vehicleUser ='" + getVehicleUser () + "'" +
            "}";
    }
}
