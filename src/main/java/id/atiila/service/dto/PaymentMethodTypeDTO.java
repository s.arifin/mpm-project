package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the PaymentMethodType entity.
 * BeSmart Team
 */

public class PaymentMethodTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idPaymentMethodType;

    private String refKey;

    private String description;

    public Integer getIdPaymentMethodType() {
        return this.idPaymentMethodType;
    }

    public void setIdPaymentMethodType(Integer id) {
        this.idPaymentMethodType = id;
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.idPaymentMethodType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PaymentMethodTypeDTO paymentMethodTypeDTO = (PaymentMethodTypeDTO) o;
        if (paymentMethodTypeDTO.getIdPaymentMethodType() == null || getIdPaymentMethodType() == null) {
            return false;
        }
        return Objects.equals(getIdPaymentMethodType(), paymentMethodTypeDTO.getIdPaymentMethodType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPaymentMethodType());
    }

    @Override
    public String toString() {
        return "PaymentMethodTypeDTO{" +
            "id=" + getIdPaymentMethodType() +
            ", refKey='" + getRefKey() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
