package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the ElectronicAddress entity.
 * BeSmart Team
 */

public class ElectronicAddressDTO extends ContactMechanismDTO {

    private static final long serialVersionUID = 1L;

    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "ElectronicAddressDTO{" +
            "id=" + getIdContact() +
            ", address='" + getAddress() + "'" +
            "}";
    }
}
