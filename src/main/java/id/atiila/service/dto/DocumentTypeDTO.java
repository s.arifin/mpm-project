package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the DocumentType entity.
 * BeSmart Team
 */

public class DocumentTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idDocumentType;

    private String description;

    private Integer idParent;

    public Integer getIdDocumentType() {
        return this.idDocumentType;
    }

    public void setIdDocumentType(Integer id) {
        this.idDocumentType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIdParent() {
        return idParent;
    }

    public void setIdParent(Integer idParent) {
        this.idParent = idParent;
    }
    
    public Integer getId() {
        return this.idDocumentType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DocumentTypeDTO documentTypeDTO = (DocumentTypeDTO) o;
        if (documentTypeDTO.getIdDocumentType() == null || getIdDocumentType() == null) {
            return false;
        }
        return Objects.equals(getIdDocumentType(), documentTypeDTO.getIdDocumentType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdDocumentType());
    }

    @Override
    public String toString() {
        return "DocumentTypeDTO{" +
            "id=" + getIdDocumentType() +
            ", description='" + getDescription() + "'" +
            ", idParent='" + getIdParent() + "'" +
            "}";
    }
}
