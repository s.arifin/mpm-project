package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WorkType entity.
 * BeSmart Team
 */

public class WorkTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idWorkType;

    private String description;

    public Integer getIdWorkType() {
        return this.idWorkType;
    }

    public void setIdWorkType(Integer id) {
        this.idWorkType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idWorkType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WorkTypeDTO workTypeDTO = (WorkTypeDTO) o;
        if (workTypeDTO.getIdWorkType() == null || getIdWorkType() == null) {
            return false;
        }
        return Objects.equals(getIdWorkType(), workTypeDTO.getIdWorkType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdWorkType());
    }

    @Override
    public String toString() {
        return "WorkTypeDTO{" +
            "id=" + getIdWorkType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
