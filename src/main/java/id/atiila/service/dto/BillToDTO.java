package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the BillTo entity.
 * BeSmart Team
 */

public class BillToDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idBillTo;

    private Integer idRoleType;

    private UUID partyId;

    private String partyName;

    public String getIdBillTo() {
        return this.idBillTo;
    }

    public void setIdBillTo(String id) {
        this.idBillTo = id;
    }

    public Integer getIdRoleType() {
        return idRoleType;
    }

    public void setIdRoleType(Integer idRoleType) {
        this.idRoleType = idRoleType;
    }

    public UUID getPartyId() {
        return partyId;
    }

    public void setPartyId(UUID partyId) {
        this.partyId = partyId;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getId() {
        return this.idBillTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BillToDTO billToDTO = (BillToDTO) o;
        if (billToDTO.getIdBillTo() == null || getIdBillTo() == null) {
            return false;
        }
        return Objects.equals(getIdBillTo(), billToDTO.getIdBillTo());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdBillTo());
    }

    @Override
    public String toString() {
        return "BillToDTO{" +
            "id=" + getIdBillTo() +
            ", idRoleType='" + getIdRoleType() + "'" +
            "}";
    }
}
