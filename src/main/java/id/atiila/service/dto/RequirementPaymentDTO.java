package id.atiila.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the RequirementPayment entity.
 * BeSmart Team
 */

public class RequirementPaymentDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idReqPayment;

    private BigDecimal amount;

    private UUID requirementId;

    private UUID paymentId;

    public UUID getIdReqPayment() {
        return this.idReqPayment;
    }

    public void setIdReqPayment(UUID id) {
        this.idReqPayment = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public UUID getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(UUID requirementId) {
        this.requirementId = requirementId;
    }

    public UUID getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(UUID paymentId) {
        this.paymentId = paymentId;
    }

    public UUID getId() {
        return this.idReqPayment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequirementPaymentDTO requirementPaymentDTO = (RequirementPaymentDTO) o;
        if (requirementPaymentDTO.getIdReqPayment() == null || getIdReqPayment() == null) {
            return false;
        }
        return Objects.equals(getIdReqPayment(), requirementPaymentDTO.getIdReqPayment());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdReqPayment());
    }

    @Override
    public String toString() {
        return "RequirementPaymentDTO{" +
            "id=" + getIdReqPayment() +
            ", amount='" + getAmount() + "'" +
            "}";
    }
}
