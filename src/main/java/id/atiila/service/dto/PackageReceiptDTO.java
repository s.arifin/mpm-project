package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the PackageReceipt entity.
 * BeSmart Team
 */

public class PackageReceiptDTO extends ShipmentPackageDTO {

    private static final long serialVersionUID = 1L;

    private ZonedDateTime dateCreated;

    private String documentNumber;

    private String vendorInvoice;

    private String shipFromId;

    private String shipFromName;

    private Integer shipType;

    public Integer getShipType() {
        return shipType;
    }

    public void setShipType(Integer shipType) {
        this.shipType = shipType;
    }

    private ZonedDateTime dateIssued;

    public ZonedDateTime getDateIssued() {
        return dateIssued;
    }

    public void setDateIssued(ZonedDateTime dateIssued) {
        this.dateIssued = dateIssued;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getVendorInvoice() {
        return vendorInvoice;
    }

    public void setVendorInvoice(String vendorInvoice) {
        this.vendorInvoice = vendorInvoice;
    }

    public String getShipFromId() {
        return shipFromId;
    }

    public void setShipFromId(String shipToId) {
        this.shipFromId = shipToId;
    }

    public String getShipFromName() { return shipFromName; }

    public void setShipFromName(String shipFromName) { this.shipFromName = shipFromName; }

    public UUID getId() {
        return this.getIdPackage();
    }

    @Override
    public String toString() {
        return "PackageReceiptDTO{" +
            "id=" + getIdPackage() +
            ", dateCreated='" + getDateCreated() + "'" +
            ", documentNumber='" + getDocumentNumber() + "'" +
            ", vendorInvoice='" + getVendorInvoice() + "'" +
            "}";
    }
}
