package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the ShipmentItem entity.
 * BeSmart Team
 */

public class ShipmentItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idShipmentItem;

    private String idProduct;

    private Integer idFeature;

    private String itemDescription;

    private Double qty;

    private String contentDescription;

    private String idFrame;

    private String idMachine;

    private UUID shipmentId;

    private Integer featureId;

    private String featureDescription;

    private String featureRefKey;

    public UUID getIdShipmentItem() {
        return this.idShipmentItem;
    }

    public void setIdShipmentItem(UUID id) {
        this.idShipmentItem = id;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public String getContentDescription() {
        return contentDescription;
    }

    public void setContentDescription(String contentDescription) {
        this.contentDescription = contentDescription;
    }

    public String getIdFrame() { return idFrame; }

    public void setIdFrame(String idFrame) { this.idFrame = idFrame; }

    public String getIdMachine() { return idMachine; }

    public void setIdMachine(String idMachine) { this.idMachine = idMachine; }

    public UUID getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(UUID shipmentId) {
        this.shipmentId = shipmentId;
    }

    public Integer getFeatureId() { return featureId; }

    public void setFeatureId(Integer featureId) { this.featureId = featureId; }

    public String getFeatureDescription() { return featureDescription; }

    public void setFeatureDescription(String featureDescription) {
        this.featureDescription = featureDescription;
    }

    public String getFeatureRefKey() {
        return featureRefKey;
    }

    public void setFeatureRefKey(String featureRefKey) {
        this.featureRefKey = featureRefKey;
    }

    public UUID getId() {
        return this.idShipmentItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShipmentItemDTO shipmentItemDTO = (ShipmentItemDTO) o;
        if (shipmentItemDTO.getIdShipmentItem() == null || getIdShipmentItem() == null) {
            return false;
        }
        return Objects.equals(getIdShipmentItem(), shipmentItemDTO.getIdShipmentItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdShipmentItem());
    }

    @Override
    public String toString() {
        return "ShipmentItemDTO{" +
            "id=" + getIdShipmentItem() +
            ", idProduct='" + getIdProduct() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", itemDescription='" + getItemDescription() + "'" +
            ", qty='" + getQty() + "'" +
            ", contentDescription='" + getContentDescription() + "'" +
            "}";
    }
}
