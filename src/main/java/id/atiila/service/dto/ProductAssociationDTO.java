package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the ProductAssociation entity.
 * BeSmart Team
 */

public class ProductAssociationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private UUID idProductAssociation;

	private Double qty;

	private ZonedDateTime dateFrom;

	private ZonedDateTime dateThru;

	private String productToId;

	private String productToName;

	private String productFromId;

	private String productFromName;

	public UUID getIdProductAssociation() {
		return this.idProductAssociation;
	}

	public void setIdProductAssociation(UUID id) {
		this.idProductAssociation = id;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public ZonedDateTime getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(ZonedDateTime dateFrom) {
		this.dateFrom = dateFrom;
	}

	public ZonedDateTime getDateThru() {
		return dateThru;
	}

	public void setDateThru(ZonedDateTime dateThru) {
		this.dateThru = dateThru;
	}

	public String getProductToId() {
		return productToId;
	}

	public void setProductToId(String productId) {
		this.productToId = productId;
	}

	public String getProductToName() {
		return productToName;
	}

	public void setProductToName(String productName) {
		this.productToName = productName;
	}

	public String getProductFromId() {
		return productFromId;
	}

	public void setProductFromId(String productId) {
		this.productFromId = productId;
	}

	public String getProductFromName() {
		return productFromName;
	}

	public void setProductFromName(String productName) {
		this.productFromName = productName;
	}

	public UUID getId() {
		return this.idProductAssociation;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		ProductAssociationDTO productAssociationDTO = (ProductAssociationDTO) o;
		if (productAssociationDTO.getIdProductAssociation() == null || getIdProductAssociation() == null) {
			return false;
		}
		return Objects.equals(getIdProductAssociation(), productAssociationDTO.getIdProductAssociation());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getIdProductAssociation());
	}

	@Override
	public String toString() {
		return "ProductAssociationDTO{" +
			"id=" + getIdProductAssociation() +
			", qty='" + getQty() + "'" +
			", dateFrom='" + getDateFrom() + "'" +
			", dateThru='" + getDateThru() + "'" +
			"}";
	}
}
