package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the UnitShipmentReceipt entity.
 * BeSmart Team
 */

public class UnitShipmentReceiptDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idReceipt;

    private String idProduct;

    private Integer idFeature;

    private String receiptCode;

    private Integer qtyAccept;

    private Integer qtyReject;

    private String itemDescription;

    private ZonedDateTime dateReceipt;

    private String idFrame;

    private String idMachine;

    private Integer yearAssembly;

    private BigDecimal unitPrice;

    private Boolean acc1;

    private Boolean acc2;

    private Boolean acc3;

    private Boolean acc4;

    private Boolean acc5;

    private Boolean acc6;

    private Boolean acc7;

    private Boolean receipt;

    private UUID shipmentPackageId;

    private UUID shipmentItemId;

    private UUID orderItemId;

    private Integer featureId;

    private String featureDescription;

    private String featureRefKey;

    private String idInternal;

    public String getIdInternal() {
        return idInternal;
    }

    public void setIdInternal(String idInternal) {
        this.idInternal = idInternal;
    }

    public UUID getIdReceipt() {
        return this.idReceipt;
    }

    public void setIdReceipt(UUID id) {
        this.idReceipt = id;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public String getReceiptCode() {
        return receiptCode;
    }

    public void setReceiptCode(String receiptCode) {
        this.receiptCode = receiptCode;
    }

    public Integer getQtyAccept() {
        return qtyAccept;
    }

    public void setQtyAccept(Integer qtyAccept) {
        this.qtyAccept = qtyAccept;
    }

    public Integer getQtyReject() {
        return qtyReject;
    }

    public void setQtyReject(Integer qtyReject) {
        this.qtyReject = qtyReject;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public ZonedDateTime getDateReceipt() {
        return dateReceipt;
    }

    public void setDateReceipt(ZonedDateTime dateReceipt) {
        this.dateReceipt = dateReceipt;
    }

    public String getIdFrame() {
        return idFrame;
    }

    public void setIdFrame(String idFrame) {
        this.idFrame = idFrame;
    }

    public String getIdMachine() {
        return idMachine;
    }

    public void setIdMachine(String idMachine) {
        this.idMachine = idMachine;
    }

    public Integer getYearAssembly() {
        return yearAssembly;
    }

    public void setYearAssembly(Integer yearAssembly) {
        this.yearAssembly = yearAssembly;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Boolean isAcc1() {
        return acc1;
    }

    public void setAcc1(Boolean acc1) {
        this.acc1 = acc1;
    }

    public Boolean isAcc2() {
        return acc2;
    }

    public void setAcc2(Boolean acc2) {
        this.acc2 = acc2;
    }

    public Boolean isAcc3() {
        return acc3;
    }

    public void setAcc3(Boolean acc3) {
        this.acc3 = acc3;
    }

    public Boolean isAcc4() {
        return acc4;
    }

    public void setAcc4(Boolean acc4) {
        this.acc4 = acc4;
    }

    public Boolean isAcc5() {
        return acc5;
    }

    public void setAcc5(Boolean acc5) {
        this.acc5 = acc5;
    }

    public Boolean isAcc6() {
        return acc6;
    }

    public void setAcc6(Boolean acc6) {
        this.acc6 = acc6;
    }

    public Boolean isAcc7() {
        return acc7;
    }

    public void setAcc7(Boolean acc7) {
        this.acc7 = acc7;
    }

    public Boolean getReceipt() { return receipt; }

    public void setReceipt(Boolean receipt) { this.receipt = receipt; }

    public UUID getShipmentPackageId() {
        return shipmentPackageId;
    }

    public void setShipmentPackageId(UUID shipmentPackageId) {
        this.shipmentPackageId = shipmentPackageId;
    }

    public UUID getShipmentItemId() {
        return shipmentItemId;
    }

    public void setShipmentItemId(UUID shipmentItemId) {
        this.shipmentItemId = shipmentItemId;
    }

    public UUID getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(UUID orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Integer getFeatureId() { return featureId; }

    public void setFeatureId(Integer featureId) { this.featureId = featureId; }

    public String getFeatureDescription() { return featureDescription; }

    public void setFeatureDescription(String featureDescription) {
        this.featureDescription = featureDescription;
    }

    public String getFeatureRefKey() {
        return featureRefKey;
    }

    public void setFeatureRefKey(String featureRefKey) {
        this.featureRefKey = featureRefKey;
    }

    public UUID getId() {
        return this.idReceipt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UnitShipmentReceiptDTO unitShipmentReceiptDTO = (UnitShipmentReceiptDTO) o;
        if (unitShipmentReceiptDTO.getIdReceipt() == null || getIdReceipt() == null) {
            return false;
        }
        return Objects.equals(getIdReceipt(), unitShipmentReceiptDTO.getIdReceipt());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdReceipt());
    }

    @Override
    public String toString() {
        return "UnitShipmentReceiptDTO{" +
            "id=" + getIdReceipt() +
            ", idProduct='" + getIdProduct() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", receiptCode='" + getReceiptCode() + "'" +
            ", qtyAccept='" + getQtyAccept() + "'" +
            ", qtyReject='" + getQtyReject() + "'" +
            ", itemDescription='" + getItemDescription() + "'" +
            ", dateReceipt='" + getDateReceipt() + "'" +
            ", idFrame='" + getIdFrame() + "'" +
            ", idMachine='" + getIdMachine() + "'" +
            ", yearAssembly='" + getYearAssembly() + "'" +
            ", unitPrice='" + getUnitPrice() + "'" +
            ", acc1='" + isAcc1() + "'" +
            ", acc2='" + isAcc2() + "'" +
            ", acc3='" + isAcc3() + "'" +
            ", acc4='" + isAcc4() + "'" +
            ", acc5='" + isAcc5() + "'" +
            ", acc6='" + isAcc6() + "'" +
            ", acc7='" + isAcc7() + "'" +
            "}";
    }
}
