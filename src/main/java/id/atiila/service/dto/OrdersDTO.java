package id.atiila.service.dto;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.*;

import id.atiila.domain.OrderType;
import id.atiila.domain.OrdersStatus;
import org.springframework.data.annotation.Id;

/**
 * A DTO for the Orders entity.
 * BeSmart Team
 */

public class OrdersDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private UUID idOrder;

    private String orderNumber;

    private ZonedDateTime dateEntry;

    private ZonedDateTime dateOrder;

    private Integer currentStatus;

    private BigDecimal totalAmount;

    private Set<PaymentApplicationDTO> payments = new HashSet<>();

    private Integer orderTypeId;

    private String orderTypeDescription;

    public UUID getIdOrder() {
        return this.idOrder;
    }

    public void setIdOrder(UUID id) {
        this.idOrder = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public ZonedDateTime getDateEntry() {
        return dateEntry;
    }

    public void setDateEntry(ZonedDateTime dateEntry) {
        this.dateEntry = dateEntry;
    }

    public ZonedDateTime getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(ZonedDateTime dateOrder) {
        this.dateOrder = dateOrder;
    }

    public Set<PaymentApplicationDTO> getPayments() {
        return payments;
    }

    public void setPayments(Set<PaymentApplicationDTO> paymentApplications) {
        this.payments = paymentApplications;
    }

    public Integer getOrderTypeId() {
        return orderTypeId;
    }

    public void setOrderTypeId(Integer orderTypeId) {
        this.orderTypeId = orderTypeId;
    }

	public String getOrderTypeDescription() {
        return orderTypeDescription;
    }

    public void setOrderTypeDescription(String orderTypeDescription) {
        this.orderTypeDescription = orderTypeDescription;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public UUID getId() {
        return this.idOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrdersDTO ordersDTO = (OrdersDTO) o;
        if (ordersDTO.getIdOrder() == null || getIdOrder() == null) {
            return false;
        }
        return Objects.equals(getIdOrder(), ordersDTO.getIdOrder());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdOrder());
    }

    @Override
    public String toString() {
        return "OrdersDTO{" +
            "id=" + getIdOrder() +
            ", orderNumber='" + getOrderNumber() + "'" +
            ", dateEntry='" + getDateEntry() + "'" +
            ", dateOrder='" + getDateOrder() + "'" +
            "}";
    }
}
