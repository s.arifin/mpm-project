package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the Dimension entity.
 * atiila consulting
 */

public class DimensionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idDimension;

    private String description;

    private Integer dimensionTypeId;

    private String dimensionTypeDescription;

    public Integer getIdDimension() {
        return this.idDimension;
    }

    public void setIdDimension(Integer id) {
        this.idDimension = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDimensionTypeId() {
        return dimensionTypeId;
    }

    public void setDimensionTypeId(Integer dimensionTypeId) {
        this.dimensionTypeId = dimensionTypeId;
    }

    public String getDimensionTypeDescription() {
        return dimensionTypeDescription;
    }

    public void setDimensionTypeDescription(String dimensionTypeDescription) {
        this.dimensionTypeDescription = dimensionTypeDescription;
    }

    public Integer getId() {
        return this.idDimension;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DimensionDTO dimensionDTO = (DimensionDTO) o;
        if (dimensionDTO.getIdDimension() == null || getIdDimension() == null) {
            return false;
        }
        return Objects.equals(getIdDimension(), dimensionDTO.getIdDimension());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdDimension());
    }

    @Override
    public String toString() {
        return "DimensionDTO{" +
            "id=" + getIdDimension() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
