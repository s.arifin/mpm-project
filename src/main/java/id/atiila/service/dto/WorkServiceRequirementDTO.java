package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the WorkServiceRequirement entity.
 * BeSmart Team
 */

public class WorkServiceRequirementDTO extends WorkEffortDTO {

    private static final long serialVersionUID = 1L;

    private UUID bookingId;

    public UUID getBookingId() {
        return bookingId;
    }

    public void setBookingId(UUID workOrderBookingId) {
        this.bookingId = workOrderBookingId;
    }

    @Override
    public String toString() {
        return "WorkServiceRequirementDTO{" +
            "id=" + getIdWe() +
            "}";
    }
}
