package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the PostalAddress entity.
 * BeSmart Team
 */

public class PostalAddressDTO extends ContactMechanismDTO {

    private static final long serialVersionUID = 1L;

    private String address1;

    private String address2;

    private UUID districtId;

    private String districtCode;

    private String districtName;

    private UUID villageId;

    private String villageCode;

    private String villageName;

    private UUID cityId;

    private String cityCode;

    private String cityName;

    private UUID provinceId;

    private String provinceCode;

    private String provinceName;

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public UUID getDistrictId() { return districtId; }

    public void setDistrictId(UUID districtId) { this.districtId = districtId; }

    public String getDistrictCode() { return districtCode; }

    public void setDistrictCode(String districtCode) { this.districtCode = districtCode; }

    public String getDistrictName() { return districtName; }

    public void setDistrictName(String districtName) { this.districtName = districtName; }

    public UUID getVillageId() { return villageId; }

    public void setVillageId(UUID villageId) { this.villageId = villageId; }

    public String getVillageCode() { return villageCode; }

    public void setVillageCode(String villageCode) { this.villageCode = villageCode; }

    public String getVillageName() { return villageName; }

    public void setVillageName(String villageName) { this.villageName = villageName; }

    public UUID getCityId() { return cityId; }

    public void setCityId(UUID cityId) { this.cityId = cityId; }

    public String getCityCode() { return cityCode; }

    public void setCityCode(String cityCode) { this.cityCode = cityCode; }

    public String getCityName() { return cityName; }

    public void setCityName(String cityName) { this.cityName = cityName; }

    public UUID getProvinceId() { return provinceId; }

    public void setProvinceId(UUID provinceId) { this.provinceId = provinceId; }

    public String getProvinceCode() { return provinceCode; }

    public void setProvinceCode(String provinceCode) { this.provinceCode = provinceCode; }

    public String getProvinceName() { return provinceName; }

    public void setProvinceName(String provinceName) { this.provinceName = provinceName; }

    @Override
    public String toString() {
        return "PostalAddressDTO{" +
            "id=" + getIdContact() +
            ", address1='" + getAddress1() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", district='" + getDistrictId() + "'" +
            ", village='" + getVillageId() + "'" +
            ", city='" + getCityId() + "'" +
            ", province='" + getProvinceId() + "'" +
            "}";
    }
}
