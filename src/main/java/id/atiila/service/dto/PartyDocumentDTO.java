package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;
import java.util.UUID;

/**
 * A DTO for the PartyDocument entity.
 * BeSmart Team
 */

public class PartyDocumentDTO extends DocumentsDTO {

    private static final long serialVersionUID = 1L;

    @Lob
    private byte[] content;
    private String contentContentType;

    private UUID partyId;

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentContentType() {
        return contentContentType;
    }

    public void setContentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
    }

    public UUID getPartyId() {
        return partyId;
    }

    public void setPartyId(UUID partyId) {
        this.partyId = partyId;
    }

    @Override
    public String toString() {
        return "PartyDocumentDTO{" +
            "id=" + getIdDocument() +
            ", content='" + getContent() + "'" +
            "}";
    }
}
