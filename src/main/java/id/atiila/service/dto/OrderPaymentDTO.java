package id.atiila.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the OrderPayment entity.
 * BeSmart Team
 */

public class OrderPaymentDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idOrderPayment;

    private BigDecimal amount;

    private UUID ordersId;

    private UUID paymentId;

    public UUID getIdOrderPayment() {
        return this.idOrderPayment;
    }

    public void setIdOrderPayment(UUID id) {
        this.idOrderPayment = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public UUID getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(UUID ordersId) {
        this.ordersId = ordersId;
    }

    public UUID getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(UUID paymentId) {
        this.paymentId = paymentId;
    }

    public UUID getId() {
        return this.idOrderPayment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderPaymentDTO orderPaymentDTO = (OrderPaymentDTO) o;
        if (orderPaymentDTO.getIdOrderPayment() == null || getIdOrderPayment() == null) {
            return false;
        }
        return Objects.equals(getIdOrderPayment(), orderPaymentDTO.getIdOrderPayment());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdOrderPayment());
    }

    @Override
    public String toString() {
        return "OrderPaymentDTO{" +
            "id=" + getIdOrderPayment() +
            ", amount='" + getAmount() + "'" +
            "}";
    }
}
