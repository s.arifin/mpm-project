package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the UnitReqInternal entity.
 * BeSmart Team
 */

public class UnitReqInternalDTO extends RequirementDTO {

    private static final long serialVersionUID = 1L;

    private String branchId;

    private String branchName;

    private String motorId;

    private String motorName;

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getMotorId() {
        return motorId;
    }

    public void setMotorId(String motorId) {
        this.motorId = motorId;
    }

    public String getMotorName() {
        return motorName;
    }

    public void setMotorName(String motorName) {
        this.motorName = motorName;
    }

    @Override
    public String toString() {
        return "UnitReqInternalDTO{" +
            "id=" + getIdRequirement() +
            ", description='" + getDescription() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", dateRequired='" + getDateRequired() + "'" +
            ", budget='" + getBudget() + "'" +
            ", qty='" + getQty() + "'" +
            "}";
    }
}
