package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the RuleSalesDiscount entity.
 * BeSmart Team
 */

public class RuleSalesDiscountDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idRule;

    private Integer idRuleType;

    private Integer sequenceNumber;

    private String description;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private String role;

    private BigDecimal maxAmount;

    private String idInternal;

    public String getIdInternal() { return idInternal; }

    public void setIdInternal(String idInternal) { this.idInternal = idInternal; }

    public Integer getIdRule() {
        return this.idRule;
    }

    public void setIdRule(Integer id) {
        this.idRule = id;
    }

    public Integer getIdRuleType() {
        return idRuleType;
    }

    public void setIdRuleType(Integer idRuleType) {
        this.idRuleType = idRuleType;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Integer getId() {
        return this.idRule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RuleSalesDiscountDTO ruleSalesDiscountDTO = (RuleSalesDiscountDTO) o;
        if (ruleSalesDiscountDTO.getIdRule() == null || getIdRule() == null) {
            return false;
        }
        return Objects.equals(getIdRule(), ruleSalesDiscountDTO.getIdRule());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRule());
    }

    @Override
    public String toString() {
        return "RuleSalesDiscountDTO{" +
            "id=" + getIdRule() +
            ", idRuleType='" + getIdRuleType() + "'" +
            ", sequenceNumber='" + getSequenceNumber() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            ", role='" + getRole() + "'" +
            ", maxAmount='" + getMaxAmount() + "'" +
            "}";
    }
}
