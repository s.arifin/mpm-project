package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ProspectSource entity.
 * BeSmart Team
 */

public class ProspectSourceDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idProspectSource;

    private String description;

    public Integer getIdProspectSource() {
        return this.idProspectSource;
    }

    public void setIdProspectSource(Integer id) {
        this.idProspectSource = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idProspectSource;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProspectSourceDTO prospectSourceDTO = (ProspectSourceDTO) o;
        if (prospectSourceDTO.getIdProspectSource() == null || getIdProspectSource() == null) {
            return false;
        }
        return Objects.equals(getIdProspectSource(), prospectSourceDTO.getIdProspectSource());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdProspectSource());
    }

    @Override
    public String toString() {
        return "ProspectSourceDTO{" +
            "id=" + getIdProspectSource() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
