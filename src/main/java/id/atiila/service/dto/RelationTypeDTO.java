package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RelationType entity.
 * BeSmart Team
 */

public class RelationTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idRelationType;

    private String description;

    public Integer getIdRelationType() {
        return this.idRelationType;
    }

    public void setIdRelationType(Integer id) {
        this.idRelationType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idRelationType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RelationTypeDTO relationTypeDTO = (RelationTypeDTO) o;
        if (relationTypeDTO.getIdRelationType() == null || getIdRelationType() == null) {
            return false;
        }
        return Objects.equals(getIdRelationType(), relationTypeDTO.getIdRelationType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRelationType());
    }

    @Override
    public String toString() {
        return "RelationTypeDTO{" +
            "id=" + getIdRelationType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
