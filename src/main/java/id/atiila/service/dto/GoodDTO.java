package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Good entity.
 * BeSmart Team
 */

public class GoodDTO extends ProductDTO {

    private static final long serialVersionUID = 1L;

    private String uomId;

    private String uomDescription;

    public GoodDTO() {
    }

    public GoodDTO(String idProduct, String name, String uomId) {
        super(idProduct, name);
        this.uomId = uomId;
    }

    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }

    public String getUomDescription() {
        return uomDescription;
    }

    public void setUomDescription(String uomDescription) {
        this.uomDescription = uomDescription;
    }

    @Override
    public String toString() {
        return "GoodDTO{" +
            "id=" + getIdProduct() +
            ", name='" + getName() + "'" +
            ", dateIntroduction='" + getDateIntroduction() + "'" +
            "}";
    }
}
