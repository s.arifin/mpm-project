package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the PartyRole entity.
 * BeSmart Team
 */

public class PartyRoleDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @org.springframework.data.annotation.Id
    private UUID idPartyRole;

    private ZonedDateTime dateRegister;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;
	
    private Integer currentStatus;
    
    private UUID partyId;
    
    private String partyName;

    public UUID getIdPartyRole() {
        return this.idPartyRole;
    }

    public void setIdPartyRole(UUID id) {
        this.idPartyRole = id;
    }

    public ZonedDateTime getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(ZonedDateTime dateRegister) {
        this.dateRegister = dateRegister;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }
    
    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }
    
    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }
    
    public UUID getPartyId() {
        return partyId;
    }
    
    public void setPartyId(UUID partyId) {
        this.partyId = partyId;
    }
    
    public UUID getId() {
        return this.idPartyRole;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PartyRoleDTO partyRoleDTO = (PartyRoleDTO) o;
        if (partyRoleDTO.getIdPartyRole() == null || getIdPartyRole() == null) {
            return false;
        }
        return Objects.equals(getIdPartyRole(), partyRoleDTO.getIdPartyRole());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPartyRole());
    }

    @Override
    public String toString() {
        return "PartyRoleDTO{" +
            "id=" + getIdPartyRole() +
            ", dateRegister='" + getDateRegister() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
