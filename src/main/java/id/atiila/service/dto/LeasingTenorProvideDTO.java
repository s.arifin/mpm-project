package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the LeasingTenorProvide entity.
 * BeSmart Team
 */

public class LeasingTenorProvideDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idLeasingProvide;

    private String productId;

    private String productName;

    private Integer tenor;

    private BigDecimal installment;

    private BigDecimal downPayment;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private UUID leasingId;

    private String leasingName;

    public String getLeasingName() {
        return leasingName;
    }

    public void setLeasingName(String leasingName) {
        this.leasingName = leasingName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public UUID getIdLeasingProvide() {
        return this.idLeasingProvide;
    }

    public void setIdLeasingProvide(UUID id) {
        this.idLeasingProvide = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String idProduct) {
        this.productId = idProduct;
    }

    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

    public BigDecimal getInstallment() {
        return installment;
    }

    public void setInstallment(BigDecimal installment) {
        this.installment = installment;
    }

    public BigDecimal getDownPayment() {
        return downPayment;
    }

    public void setDownPayment(BigDecimal downPayment) {
        this.downPayment = downPayment;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public UUID getLeasingId() {
        return leasingId;
    }

    public void setLeasingId(UUID leasingCompanyId) {
        this.leasingId = leasingCompanyId;
    }

    public UUID getId() {
        return this.idLeasingProvide;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LeasingTenorProvideDTO leasingTenorProvideDTO = (LeasingTenorProvideDTO) o;
        if (leasingTenorProvideDTO.getIdLeasingProvide() == null || getIdLeasingProvide() == null) {
            return false;
        }
        return Objects.equals(getIdLeasingProvide(), leasingTenorProvideDTO.getIdLeasingProvide());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdLeasingProvide());
    }

    @Override
    public String toString() {
        return "LeasingTenorProvideDTO{" +
            "id=" + getIdLeasingProvide() +
            ", idProduct='" + getProductId() + "'" +
            ", tenor='" + getTenor() + "'" +
            ", installment='" + getInstallment() + "'" +
            ", downPayment='" + getDownPayment() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
