package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the SuspectOrganization entity.
 * BeSmart Team
 */

public class SuspectOrganizationDTO extends SuspectDTO {

    private static final long serialVersionUID = 1L;

    private OrganizationDTO organization = new OrganizationDTO();

    public OrganizationDTO getOrganization() { return organization; }

    public void setOrganization(OrganizationDTO organization) {this.organization = organization; }

    @Override
    public String toString() {
        return "SuspectOrganizationDTO{" +
            "id=" + getIdSuspect() +
            "}";
    }
}
