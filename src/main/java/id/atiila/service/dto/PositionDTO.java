package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Position entity.
 * BeSmart Team
 */

public class PositionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idPosition;

    private Integer sequenceNumber;

    private String description;

    private String userName;

    private Integer currentStatus;

    private Integer positionTypeId;

    private String positionTypeDescription;

    private UUID organizationId;

    private String organizationName;

    private String internalId;

    private String internalName;

    private UUID ownerId;

    private String ownerName;

    public UUID getIdPosition() {
        return this.idPosition;
    }

    public void setIdPosition(UUID id) {
        this.idPosition = id;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getPositionTypeId() {
        return positionTypeId;
    }

    public void setPositionTypeId(Integer positionTypeId) {
        this.positionTypeId = positionTypeId;
    }

    public String getPositionTypeDescription() {
        return positionTypeDescription;
    }

    public void setPositionTypeDescription(String positionTypeDescription) {
        this.positionTypeDescription = positionTypeDescription;
    }

    public UUID getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(UUID organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public UUID getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(UUID userMediatorId) {
        this.ownerId = userMediatorId;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public UUID getId() {
        return this.idPosition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PositionDTO positionDTO = (PositionDTO) o;
        if (positionDTO.getIdPosition() == null || getIdPosition() == null) {
            return false;
        }
        return Objects.equals(getIdPosition(), positionDTO.getIdPosition());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPosition());
    }

    @Override
    public String toString() {
        return "PositionDTO{" +
            "id=" + getIdPosition() +
            ", sequenceNumber='" + getSequenceNumber() + "'" +
            ", description='" + getDescription() + "'" +
            ", userName='" + getUserName() + "'" +
            "}";
    }
}
