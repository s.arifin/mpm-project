package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the StockOpnameInventory entity.
 * BeSmart Team
 */

public class StockOpnameInventoryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idStockOpnameInventory;

    private Double qty;

    private UUID inventoryItemId;

    private UUID itemId;

    public UUID getIdStockOpnameInventory() {
        return this.idStockOpnameInventory;
    }

    public void setIdStockOpnameInventory(UUID id) {
        this.idStockOpnameInventory = id;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public UUID getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(UUID inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    public UUID getItemId() {
        return itemId;
    }

    public void setItemId(UUID stockOpnameItemId) {
        this.itemId = stockOpnameItemId;
    }

    public UUID getId() {
        return this.idStockOpnameInventory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StockOpnameInventoryDTO stockOpnameInventoryDTO = (StockOpnameInventoryDTO) o;
        if (stockOpnameInventoryDTO.getIdStockOpnameInventory() == null || getIdStockOpnameInventory() == null) {
            return false;
        }
        return Objects.equals(getIdStockOpnameInventory(), stockOpnameInventoryDTO.getIdStockOpnameInventory());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdStockOpnameInventory());
    }

    @Override
    public String toString() {
        return "StockOpnameInventoryDTO{" +
            "id=" + getIdStockOpnameInventory() +
            ", qty='" + getQty() + "'" +
            "}";
    }
}
