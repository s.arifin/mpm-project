package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.util.UUID;

public class CustomPreRequestUnitInternalDTO {
    private UUID idRequirement;

    private ZonedDateTime dtCreated;

    private String requirementNumber;

    private String productName;

    private String productCode;

    private String colorName;

    private UUID salesId;

    private String salesName;

    private UUID korsalId;

    private String korsalName;

    public CustomPreRequestUnitInternalDTO(
        UUID idRequirement,
        ZonedDateTime dtCreated,
        String requirementNumber,
        String productName,
        String productCode,
        String colorName,
        UUID salesId,
        UUID korsalId
    ){
        this.idRequirement = idRequirement;
        this.dtCreated = dtCreated;
        this.requirementNumber = requirementNumber;
        this.productName = productName;
        this.productCode = productCode;
        this.colorName = colorName;
        this.salesId = salesId;
        this.korsalId = korsalId;
        this.salesName = null;
        this.korsalName = null;
    }

    //getter and setter
    public UUID getKorsalId() {
        return korsalId;
    }

    public void setKorsalId(UUID korsalId) {
        this.korsalId = korsalId;
    }

    public UUID getSalesId() {
        return salesId;
    }

    public void setSalesId(UUID salesId) {
        this.salesId = salesId;
    }

    public UUID getIdRequirement() {
        return idRequirement;
    }

    public void setIdRequirement(UUID idRequirement) {
        this.idRequirement = idRequirement;
    }

    public ZonedDateTime getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(ZonedDateTime dtCreated) {
        this.dtCreated = dtCreated;
    }

    public String getRequirementNumber() {
        return requirementNumber;
    }

    public void setRequirementNumber(String requirementNumber) {
        this.requirementNumber = requirementNumber;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSalesName() {
        return salesName;
    }

    public void setSalesName(String salesName) {
        this.salesName = salesName;
    }

    public String getKorsalName() {
        return korsalName;
    }

    public void setKorsalName(String korsalName) {
        this.korsalName = korsalName;
    }
}
