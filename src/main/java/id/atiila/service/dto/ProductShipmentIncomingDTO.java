package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the ProductShipmentIncoming entity.
 * BeSmart Team
 */

public class ProductShipmentIncomingDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idShipment;

    private String shipmentNumber;

    private String description;

    private ZonedDateTime dateSchedulle;

    private String reason;

    private Integer shipmentTypeId;

    private String internalId;

    private String internalName;

    private String shipFromId;

    private String shipToId;

    private UUID addressFromId;

    private UUID addressToId;

    public UUID getIdShipment() {
        return this.idShipment;
    }

    public void setIdShipment(UUID id) {
        this.idShipment = id;
    }

    public String getShipmentNumber() {
        return shipmentNumber;
    }

    public void setShipmentNumber(String shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getDateSchedulle() {
        return dateSchedulle;
    }

    public void setDateSchedulle(ZonedDateTime dateSchedulle) {
        this.dateSchedulle = dateSchedulle;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getShipmentTypeId() {
        return shipmentTypeId;
    }

    public void setShipmentTypeId(Integer shipmentTypeId) {
        this.shipmentTypeId = shipmentTypeId;
    }

    public String getShipFromId() {
        return shipFromId;
    }

    public void setShipFromId(String shipToId) {
        this.shipFromId = shipToId;
    }

    public String getShipToId() {
        return shipToId;
    }

    public void setShipToId(String shipToId) {
        this.shipToId = shipToId;
    }

    public UUID getAddressFromId() {
        return addressFromId;
    }

    public void setAddressFromId(UUID postalAddressId) {
        this.addressFromId = postalAddressId;
    }

    public UUID getAddressToId() {
        return addressToId;
    }

    public void setAddressToId(UUID postalAddressId) {
        this.addressToId = postalAddressId;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public UUID getId() {
        return this.idShipment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductShipmentIncomingDTO productShipmentIncomingDTO = (ProductShipmentIncomingDTO) o;
        if (productShipmentIncomingDTO.getIdShipment() == null || getIdShipment() == null) {
            return false;
        }
        return Objects.equals(getIdShipment(), productShipmentIncomingDTO.getIdShipment());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdShipment());
    }

    @Override
    public String toString() {
        return "ProductShipmentIncomingDTO{" +
            "id=" + getIdShipment() +
            ", shipmentNumber='" + getShipmentNumber() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateSchedulle='" + getDateSchedulle() + "'" +
            ", reason='" + getReason() + "'" +
            "}";
    }
}
