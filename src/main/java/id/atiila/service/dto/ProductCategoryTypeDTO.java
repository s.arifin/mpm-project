package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the ProductCategoryType entity.
 * BeSmart Team
 */

public class ProductCategoryTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idCategoryType;

    private String description;

    private String refKey;

    public Integer getIdCategoryType() {
        return this.idCategoryType;
    }

    public void setIdCategoryType(Integer id) {
        this.idCategoryType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public Integer getId() {
        return this.idCategoryType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductCategoryTypeDTO productCategoryTypeDTO = (ProductCategoryTypeDTO) o;
        if (productCategoryTypeDTO.getIdCategoryType() == null || getIdCategoryType() == null) {
            return false;
        }
        return Objects.equals(getIdCategoryType(), productCategoryTypeDTO.getIdCategoryType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdCategoryType());
    }

    @Override
    public String toString() {
        return "ProductCategoryTypeDTO{" +
            "id=" + getIdCategoryType() +
            ", description='" + getDescription() + "'" +
            ", refKey='" + getRefKey() + "'" +
            "}";
    }
}
