package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the WeServiceType entity.
 * BeSmart Team
 */

public class WeServiceTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idWeType;

    private Integer flatRateTime;

    private String serviceId;

    private String serviceName;

    public Integer getIdWeType() {
        return this.idWeType;
    }

    public void setIdWeType(Integer id) {
        this.idWeType = id;
    }

    public Integer getFlatRateTime() {
        return flatRateTime;
    }

    public void setFlatRateTime(Integer flatRateTime) {
        this.flatRateTime = flatRateTime;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
    
    public Integer getId() {
        return this.idWeType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WeServiceTypeDTO weServiceTypeDTO = (WeServiceTypeDTO) o;
        if (weServiceTypeDTO.getIdWeType() == null || getIdWeType() == null) {
            return false;
        }
        return Objects.equals(getIdWeType(), weServiceTypeDTO.getIdWeType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdWeType());
    }

    @Override
    public String toString() {
        return "WeServiceTypeDTO{" +
            "id=" + getIdWeType() +
            ", flatRateTime='" + getFlatRateTime() + "'" +
            "}";
    }
}
