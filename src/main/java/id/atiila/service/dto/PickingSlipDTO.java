package id.atiila.service.dto;

import java.util.UUID;

/**
 * A DTO for the PickingSlip entity.
 * BeSmart Team
 */

public class PickingSlipDTO extends InventoryMovementDTO {

    private static final long serialVersionUID = 1L;

    private String shipToId;

    private String shipToName;

    private UUID inventoryItemId;

    private UUID orderItemId;

    private Boolean acc1;

    private Boolean acc2;

    private Boolean acc3;

    private Boolean acc4;

    private Boolean acc5;

    private Boolean acc6;

    private Boolean acc7;

    private Boolean acc8;

    private Boolean acctambah1;

    private Boolean acctambah2;

    private Boolean promat1;

    private Boolean promat2;

    private UUID mechanicinternal;

    private UUID mechanicexternal;

    public String getShipToId() {
        return shipToId;
    }

    public void setShipToId(String shipToId) {
        this.shipToId = shipToId;
    }

    public String getShipToName() {
        return shipToName;
    }

    public void setShipToName(String shipToName) {
        this.shipToName = shipToName;
    }

    public UUID getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(UUID inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    public UUID getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(UUID orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Boolean getAcc1(){
        return acc1;
    }

    public void setAcc1(Boolean acc1) {
        this.acc1 = acc1;
    }

    public Boolean getAcc2() {
        return acc2;
    }

    public void setAcc2(Boolean acc2) {
        this.acc2 = acc2;
    }

    public Boolean getAcc3() {
        return acc3;
    }

    public void setAcc3(Boolean acc3) {
        this.acc3 = acc3;
    }

    public Boolean getAcc4() {
        return acc4;
    }

    public void setAcc4(Boolean acc4) {
        this.acc4 = acc4;
    }

    public Boolean getAcc5() {
        return acc5;
    }

    public void setAcc5(Boolean acc5) {
        this.acc5 = acc5;
    }

    public Boolean getAcc6() {
        return acc6;
    }

    public void setAcc6(Boolean acc6) {
        this.acc6 = acc6;
    }

    public Boolean getAcc7() {
        return acc7;
    }

    public void setAcc7(Boolean acc7) {
        this.acc7 = acc7;
    }

    public Boolean getAcc8() {
        return acc8;
    }

    public void setAcc8(Boolean acc8) {
        this.acc8 = acc8;
    }

    public Boolean getAcctambah1() {
        return acctambah1;
    }

    public void setAcctambah1(Boolean acctambah1) {
        this.acctambah1 = acctambah1;
    }

    public Boolean getAcctambah2() {
        return acctambah2;
    }

    public void setAcctambah2(Boolean acctambah2) {
        this.acctambah2 = acctambah2;
    }

    public Boolean getPromat1() {
        return promat1;
    }

    public void setPromat1(Boolean promat1) {
        this.promat1 = promat1;
    }

    public Boolean getPromat2() {
        return promat2;
    }

    public void setPromat2(Boolean promat2) {
        this.promat2 = promat2;
    }

    public UUID getMechanicinternal() {
        return mechanicinternal;
    }

    public void setMechanicinternal(UUID mechanicinternal) {
        this.mechanicinternal = mechanicinternal;
    }

    public UUID getMechanicexternal() {
        return mechanicexternal;
    }

    public void setMechanicexternal(UUID mechanicexternal) {
        this.mechanicexternal = mechanicexternal;
    }

    @Override
    public String toString() {
        return "PickingSlipDTO{" +
            "id=" + getIdSlip() +
            ", dateCreate='" + getDateCreate() + "'" +
            ", acc1='" + getAcc1() + "'"+
            ", acc2='" + getAcc2() + "'"+
            ", acc3='" + getAcc3() + "'"+
            ", acc4='" + getAcc4() + "'"+
            ", acc5='" + getAcc5() + "'"+
            ", acc6='" + getAcc6() + "'"+
            ", acc7='" + getAcc7() + "'"+
            ", promat1='" + getPromat1() + "'"+
            ", promat2='" + getPromat2() + "'"+
            ", mechanicinternal='" + getMechanicinternal() + "'"+
            ", mechanicexternal='" + getMechanicexternal() + "'"+
            "}";
    }
}
