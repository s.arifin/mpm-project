package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the VendorRelationship entity.
 * atiila consulting
 */

public class VendorRelationshipDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idPartyRelationship;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private Integer statusTypeId;

    private String statusTypeDescription;

    private Integer relationTypeId;

    private String relationTypeDescription;

    private String internalId;

    private String vendorId;

    public UUID getIdPartyRelationship() {
        return this.idPartyRelationship;
    }

    public void setIdPartyRelationship(UUID id) {
        this.idPartyRelationship = id;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public Integer getStatusTypeId() {
        return statusTypeId;
    }

    public void setStatusTypeId(Integer statusTypeId) {
        this.statusTypeId = statusTypeId;
    }

    public String getStatusTypeDescription() {
        return statusTypeDescription;
    }

    public void setStatusTypeDescription(String statusTypeDescription) {
        this.statusTypeDescription = statusTypeDescription;
    }

    public Integer getRelationTypeId() {
        return relationTypeId;
    }

    public void setRelationTypeId(Integer relationTypeId) {
        this.relationTypeId = relationTypeId;
    }

    public String getRelationTypeDescription() {
        return relationTypeDescription;
    }

    public void setRelationTypeDescription(String relationTypeDescription) {
        this.relationTypeDescription = relationTypeDescription;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public UUID getId() {
        return this.idPartyRelationship;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VendorRelationshipDTO vendorRelationshipDTO = (VendorRelationshipDTO) o;
        if (vendorRelationshipDTO.getIdPartyRelationship() == null || getIdPartyRelationship() == null) {
            return false;
        }
        return Objects.equals(getIdPartyRelationship(), vendorRelationshipDTO.getIdPartyRelationship());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdPartyRelationship());
    }

    @Override
    public String toString() {
        return "VendorRelationshipDTO{" +
            "id=" + getIdPartyRelationship() +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
