package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the FeatureType entity.
 * BeSmart Team
 */

public class FeatureTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idFeatureType;

    private String description;

    private String refKey;

    public Integer getIdFeatureType() {
        return this.idFeatureType;
    }

    public void setIdFeatureType(Integer id) {
        this.idFeatureType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public Integer getId() {
        return this.idFeatureType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FeatureTypeDTO featureTypeDTO = (FeatureTypeDTO) o;
        if (featureTypeDTO.getIdFeatureType() == null || getIdFeatureType() == null) {
            return false;
        }
        return Objects.equals(getIdFeatureType(), featureTypeDTO.getIdFeatureType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdFeatureType());
    }

    @Override
    public String toString() {
        return "FeatureTypeDTO{" +
            "id=" + getIdFeatureType() +
            ", description='" + getDescription() + "'" +
            ", refKey='" + getRefKey() + "'" +
            "}";
    }
}
