package id.atiila.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the OrderBillingItem entity.
 * BeSmart Team
 */

public class OrderBillingItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idOrderBillingItem;

    private Integer qty;

    private BigDecimal amount;

    private UUID orderItemId;

    private UUID billingItemId;

    public UUID getIdOrderBillingItem() {
        return this.idOrderBillingItem;
    }

    public void setIdOrderBillingItem(UUID id) {
        this.idOrderBillingItem = id;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public UUID getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(UUID orderItemId) {
        this.orderItemId = orderItemId;
    }

    public UUID getBillingItemId() {
        return billingItemId;
    }

    public void setBillingItemId(UUID billingItemId) {
        this.billingItemId = billingItemId;
    }

    public UUID getId() {
        return this.idOrderBillingItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderBillingItemDTO orderBillingItemDTO = (OrderBillingItemDTO) o;
        if (orderBillingItemDTO.getIdOrderBillingItem() == null || getIdOrderBillingItem() == null) {
            return false;
        }
        return Objects.equals(getIdOrderBillingItem(), orderBillingItemDTO.getIdOrderBillingItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdOrderBillingItem());
    }

    @Override
    public String toString() {
        return "OrderBillingItemDTO{" +
            "id=" + getIdOrderBillingItem() +
            ", qty='" + getQty() + "'" +
            ", amount='" + getAmount() + "'" +
            "}";
    }
}
