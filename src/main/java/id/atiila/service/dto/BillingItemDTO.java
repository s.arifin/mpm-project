package id.atiila.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the BillingItem entity.
 * BeSmart Team
 */

public class BillingItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idBillingItem;

    private Integer idItemType;

    private Integer sequence;

    private Integer idFeature;

    private String idProduct;

    private String itemDescription;

    private Double qty;

    private BigDecimal unitPrice;

    private BigDecimal basePrice;

    private BigDecimal discount;

    private BigDecimal taxAmount;

    private BigDecimal totalAmount;

    private UUID billingId;

    private UUID inventoryItemId;

    private Double qtyReceipt;

    private Double qtyFilled;

    private Integer featureId;

    private String featureDescription;

    private String featureRefKey;

    private String billingNumber;

    private ZonedDateTime dateCreate;

    private String name;

    private String idInternal;

    public String getIdInternal() {
        return idInternal;
    }

    public void setIdInternal(String idInternal) {
        this.idInternal = idInternal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getBillingNumber() {
        return billingNumber;
    }

    public void setBillingNumber(String billingNumber) {
        this.billingNumber = billingNumber;
    }

    public UUID getIdBillingItem() {
        return this.idBillingItem;
    }

    public void setIdBillingItem(UUID id) {
        this.idBillingItem = id;
    }

    public Integer getIdItemType() {
        return idItemType;
    }

    public void setIdItemType(Integer idItemType) {
        this.idItemType = idItemType;
    }

    public Integer getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public UUID getBillingId() {
        return billingId;
    }

    public void setBillingId(UUID billingId) {
        this.billingId = billingId;
    }

    public UUID getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(UUID inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    public Double getQtyReceipt() {
        return qtyReceipt;
    }

    public void setQtyReceipt(Double qtyReceipt) {
        this.qtyReceipt = qtyReceipt;
    }

    public Double getQtyFilled() {
        return qtyFilled;
    }

    public void setQtyFilled(Double qtyFilled) {
        this.qtyFilled = qtyFilled;
    }

    public Integer getFeatureId() {
        return featureId;
    }

    public void setFeatureId(Integer featureId) {
        this.featureId = featureId;
    }

    public String getFeatureDescription() {
        return featureDescription;
    }

    public void setFeatureDescription(String featureDescription) {
        this.featureDescription = featureDescription;
    }

    public String getFeatureRefKey() {
        return featureRefKey;
    }

    public void setFeatureRefKey(String featureRefKey) {
        this.featureRefKey = featureRefKey;
    }

    public UUID getId() {
        return this.idBillingItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BillingItemDTO billingItemDTO = (BillingItemDTO) o;
        if (billingItemDTO.getIdBillingItem() == null || getIdBillingItem() == null) {
            return false;
        }
        return Objects.equals(getIdBillingItem(), billingItemDTO.getIdBillingItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdBillingItem());
    }

    @Override
    public String toString() {
        return "BillingItemDTO{" +
            "id=" + getIdBillingItem() +
            ", idItemType='" + getIdItemType() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", idProduct='" + getIdProduct() + "'" +
            ", itemDescription='" + getItemDescription() + "'" +
            ", qty='" + getQty() + "'" +
            ", unitPrice='" + getUnitPrice() + "'" +
            ", basePrice='" + getBasePrice() + "'" +
            ", discount='" + getDiscount() + "'" +
            ", taxAmount='" + getTaxAmount() + "'" +
            ", totalAmount='" + getTotalAmount() + "'" +
            "}";
    }
}
