package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the DealerClaimType entity.
 * BeSmart Team
 */

public class DealerClaimTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idClaimType;

    private String description;

    public Integer getIdClaimType() {
        return this.idClaimType;
    }

    public void setIdClaimType(Integer id) {
        this.idClaimType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idClaimType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DealerClaimTypeDTO dealerClaimTypeDTO = (DealerClaimTypeDTO) o;
        if (dealerClaimTypeDTO.getIdClaimType() == null || getIdClaimType() == null) {
            return false;
        }
        return Objects.equals(getIdClaimType(), dealerClaimTypeDTO.getIdClaimType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdClaimType());
    }

    @Override
    public String toString() {
        return "DealerClaimTypeDTO{" +
            "id=" + getIdClaimType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
