package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the VehicleCustomerRequest entity.
 * atiila consulting
 */

public class VehicleCustomerRequestDTO extends RequestDTO {

    private static final long serialVersionUID = 1L;

    private String customerOrder;

    private String customerId;

    private String customerName;

    private UUID salesBrokerId;

    private String salesBrokerName;

    private String salesBrokerCode;

    private Double qtyToShipment;

    private UUID idSalesman;

    public UUID getIdSalesman() {
        return idSalesman;
    }

    public void setIdSalesman(UUID idSalesman) {
        this.idSalesman = idSalesman;
    }

    public String getCustomerOrder() {
        return customerOrder;
    }

    public void setCustomerOrder(String customerOrder) {
        this.customerOrder = customerOrder;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Double getQtyToShipment() {
        return qtyToShipment;
    }

    public void setQtyToShipment(Double qtyToShipment) {
        this.qtyToShipment = qtyToShipment;
    }

    public UUID getSalesBrokerId() {
        return salesBrokerId;
    }

    public void setSalesBrokerId(UUID salesBrokerId) {
        this.salesBrokerId = salesBrokerId;
    }

    public String getSalesBrokerName() {
        return salesBrokerName;
    }

    public void setSalesBrokerName(String salesBrokerName) {
        this.salesBrokerName = salesBrokerName;
    }

    public String getSalesBrokerCode() {
        return salesBrokerCode;
    }

    public void setSalesBrokerCode(String salesBrokerCode) {
        this.salesBrokerCode = salesBrokerCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VehicleCustomerRequestDTO vehicleCustomerRequestDTO = (VehicleCustomerRequestDTO) o;
        if (vehicleCustomerRequestDTO.getIdRequest() == null || getIdRequest() == null) {
            return false;
        }
        return Objects.equals(getIdRequest(), vehicleCustomerRequestDTO.getIdRequest());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRequest());
    }

    @Override
    public String toString() {
        return "VehicleCustomerRequestDTO{" +
            "id=" + getIdRequest() +
            ", requestNumber='" + getRequestNumber() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", dateRequest='" + getDateRequest() + "'" +
            ", description='" + getDescription() + "'" +
            ", customerOrder='" + getCustomerOrder() + "'" +
            "}";
    }
}
