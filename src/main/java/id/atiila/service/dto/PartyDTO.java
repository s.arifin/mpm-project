package id.atiila.service.dto;

import java.io.Serializable;
import java.util.*;

/**
 * A DTO for the Party entity.
 * BeSmart Team
 */

public class PartyDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idParty;

    private String name;

    private Set<PartyCategoryDTO> categories = new HashSet<>();

    private Set<FacilityDTO> facilities = new HashSet<>();

    public PartyDTO(UUID idParty) {
        this.idParty = idParty;
    }

    public UUID getIdParty() {
        return this.idParty;
    }

    public void setIdParty(UUID id) {
        this.idParty = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<PartyCategoryDTO> getCategories() {
        return categories;
    }

    public void setCategories(Set<PartyCategoryDTO> partyCategories) {
        this.categories = partyCategories;
    }

    public Set<FacilityDTO> getFacilities() {
        return facilities;
    }

    public void setFacilities(Set<FacilityDTO> facilities) {
        this.facilities = facilities;
    }


    public PartyDTO() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PartyDTO partyDTO = (PartyDTO) o;
        if (partyDTO.getIdParty() == null || getIdParty() == null) {
            return false;
        }
        return Objects.equals(getIdParty(), partyDTO.getIdParty());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdParty());
    }

    @Override
    public String toString() {
        return "PartyDTO{" +
            "id=" + getIdParty() +
            ", name='" + getName() + "'" +
            "}";
    }
}
