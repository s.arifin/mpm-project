package id.atiila.service.dto;


import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "customvsbdto")
public class CustomVsbDTO {
    private static final long serialVersionUID = 1L;

    private String customerId;

    private Integer idSaleType;

    private String customerName;

    private String billingNumber;

    private String orderNumber;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Integer getIdSaleType() {
        return idSaleType;
    }

    public void setIdSaleType(Integer idSaleType) {
        this.idSaleType = idSaleType;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getBillingNumber() {
        return billingNumber;
    }

    public void setBillingNumber(String billingNumber) {
        this.billingNumber = billingNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }
}
