package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Province entity.
 * BeSmart Team
 */

public class ProvinceDTO extends GeoBoundaryDTO {

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProvinceDTO e = (ProvinceDTO) o;
        if (e.getIdGeobou() == null || getIdGeobou() == null) {
            return false;
        }
        return Objects.equals(getIdGeobou(), e.getIdGeobou());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdGeobou());
    }

    @Override
    public String toString() {
        return "ProvinceDTO{" +
            "id=" + getIdGeobou() +
            ", geocode='" + getGeoCode() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
