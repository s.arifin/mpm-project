package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the InventoryMovement entity.
 * BeSmart Team
 */

public class InventoryMovementDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idSlip;

    private ZonedDateTime dateCreate;

    private Integer currentStatus;

    private String slipNumber;

    private Integer idInventoryMovementType;

    private String refferenceNumber;

    public Integer getIdInventoryMovementType() {
        return idInventoryMovementType;
    }

    public void setIdInventoryMovementType(Integer idInventoryMovementType) {
        this.idInventoryMovementType = idInventoryMovementType;
    }

    public String getSlipNumber() {
        return slipNumber;
    }

    public void setSlipNumber(String slipNumber) {
        this.slipNumber = slipNumber;
    }

    public UUID getIdSlip() {
        return this.idSlip;
    }

    public void setIdSlip(UUID id) {
        this.idSlip = id;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getRefferenceNumber() {
        return refferenceNumber;
    }

    public void setRefferenceNumber(String refferenceNumber) {
        this.refferenceNumber = refferenceNumber;
    }

    public UUID getId() {
        return this.idSlip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InventoryMovementDTO inventoryMovementDTO = (InventoryMovementDTO) o;
        if (inventoryMovementDTO.getIdSlip() == null || getIdSlip() == null) {
            return false;
        }
        return Objects.equals(getIdSlip(), inventoryMovementDTO.getIdSlip());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdSlip());
    }

    @Override
    public String toString() {
        return "InventoryMovementDTO{" +
            "id=" + getIdSlip() +
            ", dateCreate='" + getDateCreate() + "'" +
            "}";
    }
}
