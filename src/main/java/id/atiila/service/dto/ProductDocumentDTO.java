package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;
import java.util.UUID;

/**
 * A DTO for the ProductDocument entity.
 * BeSmart Team
 */

public class ProductDocumentDTO extends DocumentsDTO {

    private static final long serialVersionUID = 1L;

    private UUID idDocument;

    @Lob
    private byte[] content;
    private String contentContentType;

    private String note;

    private ZonedDateTime dateCreate;

    private String productId;

    public UUID getIdDocument() {
        return this.idDocument;
    }

    public void setIdDocument(UUID id) {
        this.idDocument = id;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentContentType() {
        return contentContentType;
    }

    public void setContentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ZonedDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(ZonedDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public UUID getId() {
        return this.idDocument;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductDocumentDTO productDocumentDTO = (ProductDocumentDTO) o;
        if (productDocumentDTO.getIdDocument() == null || getIdDocument() == null) {
            return false;
        }
        return Objects.equals(getIdDocument(), productDocumentDTO.getIdDocument());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdDocument());
    }

    @Override
    public String toString() {
        return "ProductDocumentDTO{" +
            "id=" + getIdDocument() +
            ", content='" + getContent() + "'" +
            ", note='" + getNote() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            "}";
    }
}
