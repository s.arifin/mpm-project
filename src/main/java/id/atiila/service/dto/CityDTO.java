package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the City entity.
 * BeSmart Team
 */

public class CityDTO extends GeoBoundaryDTO {

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CityDTO e = (CityDTO) o;
        if (e.getIdGeobou() == null || getIdGeobou() == null) {
            return false;
        }
        return Objects.equals(getIdGeobou(), e.getIdGeobou());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdGeobou());
    }

    @Override
    public String toString() {
        return "CityDTO{" +
            "id=" + getIdGeobou() +
            ", geocode='" + getGeoCode() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
