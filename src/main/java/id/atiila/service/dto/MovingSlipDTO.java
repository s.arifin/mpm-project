package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the MovingSlip entity.
 * BeSmart Team
 */

public class MovingSlipDTO extends InventoryMovementDTO {

    private static final long serialVersionUID = 1L;

    private Integer qty;

    private UUID inventoryItemToId;

    private UUID containerFromId;

    private String containerFromContainerCode;

    private UUID containerToId;

    private String containerToContainerCode;

    private UUID facilityFromId;

    private String facilityFromDescription;

    private UUID facilityToId;

    private String facilityToDescription;

    private String productId;

    private String productName;

    private UUID inventoryItemId;

    private Integer qtyAwal;


    // getter and setter

    public Integer getQtyAwal() {
        return qtyAwal;
    }

    public void setQtyAwal(Integer qtyAwal) {
        this.qtyAwal = qtyAwal;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public UUID getInventoryItemToId() {
        return inventoryItemToId;
    }

    public void setInventoryItemToId(UUID inventoryItemId) {
        this.inventoryItemToId = inventoryItemId;
    }

    public UUID getContainerFromId() {
        return containerFromId;
    }

    public void setContainerFromId(UUID containerId) {
        this.containerFromId = containerId;
    }

    public String getContainerFromContainerCode() {
        return containerFromContainerCode;
    }

    public void setContainerFromContainerCode(String containerContainerCode) {
        this.containerFromContainerCode = containerContainerCode;
    }

    public UUID getContainerToId() {
        return containerToId;
    }

    public void setContainerToId(UUID containerId) {
        this.containerToId = containerId;
    }

    public String getContainerToContainerCode() {
        return containerToContainerCode;
    }

    public void setContainerToContainerCode(String containerContainerCode) {
        this.containerToContainerCode = containerContainerCode;
    }

    public UUID getFacilityFromId() {
        return facilityFromId;
    }

    public void setFacilityFromId(UUID facilityId) {
        this.facilityFromId = facilityId;
    }

    public String getFacilityFromDescription() {
        return facilityFromDescription;
    }

    public void setFacilityFromDescription(String facilityDescription) {
        this.facilityFromDescription = facilityDescription;
    }

    public UUID getFacilityToId() {
        return facilityToId;
    }

    public void setFacilityToId(UUID facilityId) {
        this.facilityToId = facilityId;
    }

    public String getFacilityToDescription() {
        return facilityToDescription;
    }

    public void setFacilityToDescription(String facilityDescription) {
        this.facilityToDescription = facilityDescription;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public UUID getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(UUID inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    @Override
    public String toString() {
        return "MovingSlipDTO{" +
            "id=" + getIdSlip() +
            ", qty='" + getQty() + "'" +
            "}";
    }
}
