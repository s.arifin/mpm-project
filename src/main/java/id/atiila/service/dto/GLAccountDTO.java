package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the GLAccount entity.
 * atiila consulting
 */

public class GLAccountDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idGLAccount;

    private String name;

    private String description;

    private Integer accountTypeId;

    private String accountTypeDescription;

    public UUID getIdGLAccount() {
        return this.idGLAccount;
    }

    public void setIdGLAccount(UUID id) {
        this.idGLAccount = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(Integer gLAccountTypeId) {
        this.accountTypeId = gLAccountTypeId;
    }

    public String getAccountTypeDescription() {
        return accountTypeDescription;
    }

    public void setAccountTypeDescription(String gLAccountTypeDescription) {
        this.accountTypeDescription = gLAccountTypeDescription;
    }

    public UUID getId() {
        return this.idGLAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GLAccountDTO gLAccountDTO = (GLAccountDTO) o;
        if (gLAccountDTO.getIdGLAccount() == null || getIdGLAccount() == null) {
            return false;
        }
        return Objects.equals(getIdGLAccount(), gLAccountDTO.getIdGLAccount());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdGLAccount());
    }

    @Override
    public String toString() {
        return "GLAccountDTO{" +
            "id=" + getIdGLAccount() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
