package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the SalesUnitLeasing entity.
 * BeSmart Team
 */

public class SalesUnitLeasingDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idSalesLeasing;

    private Integer idStatusType;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private UUID ownerId;

    private UUID leasingCompanyId;

    public UUID getIdSalesLeasing() {
        return this.idSalesLeasing;
    }

    public void setIdSalesLeasing(UUID id) {
        this.idSalesLeasing = id;
    }

    public Integer getIdStatusType() {
        return idStatusType;
    }

    public void setIdStatusType(Integer idStatusType) {
        this.idStatusType = idStatusType;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public UUID getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(UUID salesUnitRequirementId) {
        this.ownerId = salesUnitRequirementId;
    }

    public UUID getLeasingCompanyId() {
        return leasingCompanyId;
    }

    public void setLeasingCompanyId(UUID leasingCompanyId) {
        this.leasingCompanyId = leasingCompanyId;
    }
    
    public UUID getId() {
        return this.idSalesLeasing;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SalesUnitLeasingDTO salesUnitLeasingDTO = (SalesUnitLeasingDTO) o;
        if (salesUnitLeasingDTO.getIdSalesLeasing() == null || getIdSalesLeasing() == null) {
            return false;
        }
        return Objects.equals(getIdSalesLeasing(), salesUnitLeasingDTO.getIdSalesLeasing());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdSalesLeasing());
    }

    @Override
    public String toString() {
        return "SalesUnitLeasingDTO{" +
            "id=" + getIdSalesLeasing() +
            ", idStatusType='" + getIdStatusType() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
