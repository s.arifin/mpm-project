package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the ItemIssuance entity.
 * BeSmart Team
 */

public class ItemIssuanceDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idItemIssuance;

    private Double qty;

    private UUID shipmentItemId;

    private UUID inventoryItemId;

    private UUID pickingId;

    public UUID getIdItemIssuance() {
        return this.idItemIssuance;
    }

    public void setIdItemIssuance(UUID id) {
        this.idItemIssuance = id;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public UUID getShipmentItemId() {
        return shipmentItemId;
    }

    public void setShipmentItemId(UUID shipmentItemId) {
        this.shipmentItemId = shipmentItemId;
    }

    public UUID getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(UUID inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    public UUID getPickingId() {
        return pickingId;
    }

    public void setPickingId(UUID pickingSlipId) {
        this.pickingId = pickingSlipId;
    }
    
    public UUID getId() {
        return this.idItemIssuance;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ItemIssuanceDTO itemIssuanceDTO = (ItemIssuanceDTO) o;
        if (itemIssuanceDTO.getIdItemIssuance() == null || getIdItemIssuance() == null) {
            return false;
        }
        return Objects.equals(getIdItemIssuance(), itemIssuanceDTO.getIdItemIssuance());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdItemIssuance());
    }

    @Override
    public String toString() {
        return "ItemIssuanceDTO{" +
            "id=" + getIdItemIssuance() +
            ", qty='" + getQty() + "'" +
            "}";
    }
}
