package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ShipmentType entity.
 * BeSmart Team
 */

public class ShipmentTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idShipmentType;

    private String description;

    public ShipmentTypeDTO() {
    }

    public ShipmentTypeDTO(Integer idShipmentType, String description) {
        this.idShipmentType = idShipmentType;
        this.description = description;
    }

    public Integer getIdShipmentType() {
        return this.idShipmentType;
    }

    public void setIdShipmentType(Integer id) {
        this.idShipmentType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idShipmentType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShipmentTypeDTO shipmentTypeDTO = (ShipmentTypeDTO) o;
        if (shipmentTypeDTO.getIdShipmentType() == null || getIdShipmentType() == null) {
            return false;
        }
        return Objects.equals(getIdShipmentType(), shipmentTypeDTO.getIdShipmentType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdShipmentType());
    }

    @Override
    public String toString() {
        return "ShipmentTypeDTO{" +
            "id=" + getIdShipmentType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
