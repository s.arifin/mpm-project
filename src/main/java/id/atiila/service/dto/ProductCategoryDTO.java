package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the ProductCategory entity.
 * BeSmart Team
 */

public class ProductCategoryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idCategory;

    private String description;

    private String refKey;

    private Integer categoryTypeId;

    private String categoryTypeDescription;

    public Integer getIdCategory() {
        return this.idCategory;
    }

    public void setIdCategory(Integer id) {
        this.idCategory = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public Integer getCategoryTypeId() {
        return categoryTypeId;
    }

    public void setCategoryTypeId(Integer productCategoryTypeId) {
        this.categoryTypeId = productCategoryTypeId;
    }

    public String getCategoryTypeDescription() {
        return categoryTypeDescription;
    }

    public void setCategoryTypeDescription(String productCategoryTypeDescription) {
        this.categoryTypeDescription = productCategoryTypeDescription;
    }

    public Integer getId() {
        return this.idCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductCategoryDTO productCategoryDTO = (ProductCategoryDTO) o;
        if (productCategoryDTO.getIdCategory() == null || getIdCategory() == null) {
            return false;
        }
        return Objects.equals(getIdCategory(), productCategoryDTO.getIdCategory());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdCategory());
    }

    @Override
    public String toString() {
        return "ProductCategoryDTO{" +
            "id=" + getIdCategory() +
            ", description='" + getDescription() + "'" +
            ", refKey='" + getRefKey() + "'" +
            "}";
    }
}
