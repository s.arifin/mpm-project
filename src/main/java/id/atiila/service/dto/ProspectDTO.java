package id.atiila.service.dto;

import org.springframework.data.elasticsearch.annotations.Document;

import org.springframework.data.annotation.Id;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * A DTO for the Prospect entity.
 * BeSmart Team
 */

@Document(indexName = "prospectdto")
public class ProspectDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private UUID idProspect;

    private String prospectNumber;

    private Integer prospectCount;

    private ZonedDateTime dateFollowUp;

    private Integer currentStatus;

    private String dealerId;

    private String dealerName;

    private UUID brokerId;

    private String brokerName;

    private Integer prospectSourceId;

    private String prospectSourceDescription;

    private Integer eventTypeId;

    private String eventLocation;

    private UUID facilityId;

    private UUID suspectId;

    private UUID salesmanId;

    private String salesmanName;

    private UUID idSalesCoordinator;

    private String salesCoordinatorName;

    private SalesmanDTO salesman = new SalesmanDTO();

    private Integer distribution;

    public Integer getDistribution() {
        return distribution;
    }

    public void setDistribution(Integer distribution) {
        this.distribution = distribution;
    }

    public SalesmanDTO getSalesman() { return salesman; }

    public void setSalesman(SalesmanDTO salesman) { this.salesman = salesman; }

    public UUID getIdProspect() {
        return this.idProspect;
    }

    public void setIdProspect(UUID id) {
        this.idProspect = id;
    }

    public String getProspectNumber() {
        return prospectNumber;
    }

    public void setProspectNumber(String prospectNumber) {
        this.prospectNumber = prospectNumber;
    }

    public Integer getProspectCount() { return prospectCount; }

    public void setProspectCount(Integer prospectCount) { this.prospectCount = prospectCount; }

    public ZonedDateTime getDateFollowUp() { return dateFollowUp; }

    public void setDateFollowUp(ZonedDateTime dateFollowUp) { this.dateFollowUp = dateFollowUp; }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String internalId) {
        this.dealerId = internalId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String internalName) {
        this.dealerName = internalName;
    }

    public UUID getBrokerId() {
        return brokerId;
    }

    public void setBrokerId(UUID salesBrokerId) {
        this.brokerId = salesBrokerId;
    }

    public String getBrokerName() { return brokerName; }

    public void setBrokerName(String brokerName) { this.brokerName = brokerName; }

    public Integer getProspectSourceId() {
        return prospectSourceId;
    }

    public void setProspectSourceId(Integer prospectSourceId) {
        this.prospectSourceId = prospectSourceId;
    }

    public String getProspectSourceDescription() { return prospectSourceDescription; }

    public void setProspectSourceDescription(String prospectSourceDescription) { this.prospectSourceDescription = prospectSourceDescription; }

    public Integer getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(Integer eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public String getEventLocation() { return eventLocation; }

    public void setEventLocation(String eventLocation) { this.eventLocation = eventLocation; }

    public UUID getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(UUID facilityId) {
        this.facilityId = facilityId;
    }

    public UUID getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(UUID salesmanId) {
        this.salesmanId = salesmanId;
    }

    public UUID getSuspectId() {
        return suspectId;
    }

    public void setSuspectId(UUID suspectId) {
        this.suspectId = suspectId;
    }

    public UUID getIdSalesCoordinator() { return idSalesCoordinator; }

    public void setIdSalesCoordinator(UUID idSalesCoordinator) { this.idSalesCoordinator = idSalesCoordinator; }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getSalesCoordinatorName() {
        return salesCoordinatorName;
    }

    public void setSalesCoordinatorName(String salesCoordinatorName) {
        this.salesCoordinatorName = salesCoordinatorName;
    }

    public UUID getId() {
        return this.idProspect;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProspectDTO prospectDTO = (ProspectDTO) o;
        if (prospectDTO.getIdProspect() == null || getIdProspect() == null) {
            return false;
        }
        return Objects.equals(getIdProspect(), prospectDTO.getIdProspect());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdProspect());
    }

    @Override
    public String toString() {
        return "ProspectDTO{" +
            "id=" + getIdProspect() +
            ", prospectNumber='" + getProspectNumber() + "'" +
            "}";
    }
}
