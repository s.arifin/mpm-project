package id.atiila.service.dto;

import org.springframework.data.elasticsearch.annotations.Document;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the VehicleRegistration entity.
 * atiila consulting
 */

@Document(indexName = "vehicleregistration")
public class VehicleRegistrationDTO extends RequirementDTO {

    private static final long serialVersionUID = 1L;

    private String note;

    private String statusFaktur;

    private BigDecimal bbn;

    private BigDecimal otherCost;

    private String fakturATPM;

    private ZonedDateTime fakturDate;

    private ZonedDateTime dateFillName;

    private String requestPoliceNumber;

    private String submissionNo;

    private ZonedDateTime dateSubmission;

    private String registrationNumber;

    private Boolean waitStnk;

    private BigDecimal costHandling;

    private BigDecimal bbnkb;

    private BigDecimal pkb;

    private BigDecimal swdkllj;

    private BigDecimal tnkb;

    private BigDecimal jasa;

    private BigDecimal stnkbpkb;

    private BigDecimal totalCostHandling;

    private UUID idSalesRequirement;

    private String internalId;

    private String rootId;

    private String internalName;

    private String billToId;

    private String billToName;

    private String vendorId;

    private String vendorName;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public BigDecimal getBbn() {
        return bbn;
    }

    public void setBbn(BigDecimal bbn) {
        this.bbn = bbn;
    }

    public BigDecimal getOtherCost() {
        return otherCost;
    }

    public void setOtherCost(BigDecimal otherCost) {
        this.otherCost = otherCost;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getBillToId() {
        return billToId;
    }

    public void setBillToId(String billToId) {
        this.billToId = billToId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getStatusFaktur() {
        return statusFaktur;
    }

    public void setStatusFaktur(String statusFaktur) {
        this.statusFaktur = statusFaktur;
    }

    public String getFakturATPM() {
        return fakturATPM;
    }

    public void setFakturATPM(String fakturATPM) {
        this.fakturATPM = fakturATPM;
    }

    public ZonedDateTime getFakturDate() {
        return fakturDate;
    }

    public void setFakturDate(ZonedDateTime fakturDate) {
        this.fakturDate = fakturDate;
    }

    public ZonedDateTime getDateFillName() {
        return dateFillName;
    }

    public void setDateFillName(ZonedDateTime dateFillName) {
        this.dateFillName = dateFillName;
    }

    public String getRequestPoliceNumber() {
        return requestPoliceNumber;
    }

    public void setRequestPoliceNumber(String requestPoliceNumber) {
        this.requestPoliceNumber = requestPoliceNumber;
    }

    public String getSubmissionNo() {
        return submissionNo;
    }

    public void setSubmissionNo(String submissionNo) {
        this.submissionNo = submissionNo;
    }

    public ZonedDateTime getDateSubmission() {
        return dateSubmission;
    }

    public void setDateSubmission(ZonedDateTime dateSubmission) {
        this.dateSubmission = dateSubmission;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Boolean getWaitStnk() {
        return waitStnk;
    }

    public void setWaitStnk(Boolean waitStnk) {
        this.waitStnk = waitStnk;
    }

    public BigDecimal getCostHandling() {
        return costHandling;
    }

    public void setCostHandling(BigDecimal costHandling) {
        this.costHandling = costHandling;
    }

    public BigDecimal getBbnkb() {
        return bbnkb;
    }

    public void setBbnkb(BigDecimal bbnkb) {
        this.bbnkb = bbnkb;
    }

    public BigDecimal getPkb() {
        return pkb;
    }

    public void setPkb(BigDecimal pkb) {
        this.pkb = pkb;
    }

    public BigDecimal getSwdkllj() {
        return swdkllj;
    }

    public void setSwdkllj(BigDecimal swdkllj) {
        this.swdkllj = swdkllj;
    }

    public BigDecimal getTnkb() {
        return tnkb;
    }

    public void setTnkb(BigDecimal tnkb) {
        this.tnkb = tnkb;
    }

    public BigDecimal getJasa() {
        return jasa;
    }

    public void setJasa(BigDecimal jasa) {
        this.jasa = jasa;
    }

    public BigDecimal getStnkbpkb() {
        return stnkbpkb;
    }

    public void setStnkbpkb(BigDecimal stnkbpkb) {
        this.stnkbpkb = stnkbpkb;
    }

    public BigDecimal getTotalCostHandling() {
        return totalCostHandling;
    }

    public void setTotalCostHandling(BigDecimal totalCostHandling) {
        this.totalCostHandling = totalCostHandling;
    }

    public UUID getIdSalesRequirement() {
        return idSalesRequirement;
    }

    public void setIdSalesRequirement(UUID idSalesRequirement) {
        this.idSalesRequirement = idSalesRequirement;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public String getBillToName() {
        return billToName;
    }

    public void setBillToName(String billToName) {
        this.billToName = billToName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getRootId() {
        return rootId;
    }

    public void setRootId(String rootId) {
        this.rootId = rootId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VehicleRegistrationDTO vehicleRegistrationDTO = (VehicleRegistrationDTO) o;
        if (vehicleRegistrationDTO.getIdRequirement() == null || getIdRequirement() == null) {
            return false;
        }
        return Objects.equals(getIdRequirement(), vehicleRegistrationDTO.getIdRequirement());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRequirement());
    }

    @Override
    public String toString() {
        return "VehicleRegistrationDTO{" +
            "id=" + getIdRequirement() +
            ", requirementNumber='" + getRequirementNumber() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateCreate='" + getDateCreate() + "'" +
            ", dateRequired='" + getDateRequired() + "'" +
            ", note='" + getNote() + "'" +
            ", bbn='" + getBbn() + "'" +
            ", otherCost='" + getOtherCost() + "'" +
            "}";
    }
}
