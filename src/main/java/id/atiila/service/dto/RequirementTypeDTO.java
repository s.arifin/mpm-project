package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RequirementType entity.
 * BeSmart Team
 */

public class RequirementTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idRequirementType;

    private String description;

    public RequirementTypeDTO() {
    }

    public RequirementTypeDTO(Integer idRequirementType, String description) {
        this.idRequirementType = idRequirementType;
        this.description = description;
    }

    public Integer getIdRequirementType() {
        return this.idRequirementType;
    }

    public void setIdRequirementType(Integer id) {
        this.idRequirementType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idRequirementType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequirementTypeDTO requirementTypeDTO = (RequirementTypeDTO) o;
        if (requirementTypeDTO.getIdRequirementType() == null || getIdRequirementType() == null) {
            return false;
        }
        return Objects.equals(getIdRequirementType(), requirementTypeDTO.getIdRequirementType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdRequirementType());
    }

    @Override
    public String toString() {
        return "RequirementTypeDTO{" +
            "id=" + getIdRequirementType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
