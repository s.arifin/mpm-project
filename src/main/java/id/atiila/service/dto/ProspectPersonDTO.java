package id.atiila.service.dto;

import org.springframework.data.elasticsearch.annotations.Document;


/**
 * A DTO for the ProspectPerson entity.
 * BeSmart Team
 */
@Document(indexName = "prospectpersondto")
public class ProspectPersonDTO extends ProspectDTO {

    private static final long serialVersionUID = 1L;

    private PersonDTO person = new PersonDTO();

    private String fName;

    private String lName;

    private String cellphone;

    private String facilityName;

    private String internName;

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getInternName() {
        return internName;
    }

    public void setInternName(String internName) {
        this.internName = internName;
    }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "ProspectPersonDTO{" +
            "id=" + getIdProspect() +
            "}";
    }
}
