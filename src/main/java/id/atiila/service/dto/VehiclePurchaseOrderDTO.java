package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the VehiclePurchaseOrder entity.
 * BeSmart Team
 */

public class VehiclePurchaseOrderDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idOrder;

    private String vendorId;

    private String vendorIdVendor;

    private String internalId;

    private String internalIdInternal;

    private String billToId;

    public UUID getIdOrder() {
        return this.idOrder;
    }

    public void setIdOrder(UUID id) {
        this.idOrder = id;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorIdVendor() {
        return vendorIdVendor;
    }

    public void setVendorIdVendor(String vendorIdVendor) {
        this.vendorIdVendor = vendorIdVendor;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getInternalIdInternal() {
        return internalIdInternal;
    }

    public void setInternalIdInternal(String internalIdInternal) {
        this.internalIdInternal = internalIdInternal;
    }

    public String getBillToId() {
        return billToId;
    }

    public void setBillToId(String billToId) {
        this.billToId = billToId;
    }

    public UUID getId() {
        return this.idOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VehiclePurchaseOrderDTO vehiclePurchaseOrderDTO = (VehiclePurchaseOrderDTO) o;
        if (vehiclePurchaseOrderDTO.getIdOrder() == null || getIdOrder() == null) {
            return false;
        }
        return Objects.equals(getIdOrder(), vehiclePurchaseOrderDTO.getIdOrder());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdOrder());
    }

    @Override
    public String toString() {
        return "VehiclePurchaseOrderDTO{" +
            "id=" + getIdOrder() +
            "}";
    }
}
