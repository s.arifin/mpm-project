package id.atiila.service.dto;

import java.io.Serializable;
import java.time.Year;
import java.time.ZonedDateTime;

public class SLUploadDTO implements Serializable {

    private String shippingListNumber;

    private ZonedDateTime dateCreate;

    private String dealerCode01;

    private String dealerCode02;

    private String idProduct;

    private String idColor;

    private String idFrame;

    private String idMachine;

    private Integer yearAssembly;

    private String invoiceNumber;

    private String expedition;

    private String expeditionVehicleNumber;

    private  Integer shipType;

    public Integer getShipType() {
        return shipType;
    }

    public void setShipType(Integer shipType) {
        this.shipType = shipType;
    }

    public String getShippingListNumber() { return shippingListNumber; }

    public void setShippingListNumber(String shippingListNumber) { this.shippingListNumber = shippingListNumber; }

    public ZonedDateTime getDateCreate() { return dateCreate; }

    public void setDateCreate(ZonedDateTime dateCreate) { this.dateCreate = dateCreate; }

    public String getDealerCode01() { return dealerCode01; }

    public void setDealerCode01(String dealerCode01) { this.dealerCode01 = dealerCode01; }

    public String getDealerCode02() { return dealerCode02; }

    public void setDealerCode02(String dealerCode02) { this.dealerCode02 = dealerCode02; }

    public String getIdProduct() { return idProduct; }

    public void setIdProduct(String idProduct) { this.idProduct = idProduct; }

    public String getIdColor() { return idColor; }

    public void setIdColor(String idColor) { this.idColor = idColor; }

    public String getIdFrame() { return idFrame; }

    public void setIdFrame(String idFrame) { this.idFrame = idFrame; }

    public String getIdMachine() { return idMachine; }

    public void setIdMachine(String idMachine) { this.idMachine = idMachine; }

    public Integer getYearAssembly() { return yearAssembly; }

    public void setYearAssembly(Integer yearAssembly) { this.yearAssembly = yearAssembly; }

    public String getInvoiceNumber() { return invoiceNumber; }

    public void setInvoiceNumber(String invoiceNumber) { this.invoiceNumber = invoiceNumber; }

    public String getExpedition() { return expedition; }

    public void setExpedition(String expedition) { this.expedition = expedition; }

    public String getExpeditionVehicleNumber() { return expeditionVehicleNumber; }

    public void setExpeditionVehicleNumber(String expeditionVehicleNumber) { this.expeditionVehicleNumber = expeditionVehicleNumber; }
}
