package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the PriceComponent entity.
 * BeSmart Team
 */

public class CustomHargaBeliDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String sellerName;

    private String kodeMasterHarga;

    private String dateFrom;

    private String productId;

    private String manfucterPrice;



    public String getManfucterPrice() {
        return manfucterPrice;
    }

    public void setManfucterPrice(String manfucterPrice) {
        this.manfucterPrice = manfucterPrice;
    }

    public String getKodeMasterHarga() {
        return kodeMasterHarga;
    }

    public void setKodeMasterHarga(String kodeMasterHarga) {
        this.kodeMasterHarga = kodeMasterHarga;
    }


    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    @Override
    public String toString() {
        return "CustomHargaBeliDTO{" +
            ", sellerName='" + getSellerName() + "'" +
            ", kodeMasterHarga='" + getKodeMasterHarga() + "'" +
            ", manfucterPrice='" + getManfucterPrice() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            "}";
    }
}
