package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Quote entity.
 * BeSmart Team
 */

public class QuoteDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idQuote;

    private ZonedDateTime dateIssued;

    private String description;
	
    private Integer currentStatus;

    public UUID getIdQuote() {
        return this.idQuote;
    }

    public void setIdQuote(UUID id) {
        this.idQuote = id;
    }

    public ZonedDateTime getDateIssued() {
        return dateIssued;
    }

    public void setDateIssued(ZonedDateTime dateIssued) {
        this.dateIssued = dateIssued;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }
    
    public UUID getId() {
        return this.idQuote;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        QuoteDTO quoteDTO = (QuoteDTO) o;
        if (quoteDTO.getIdQuote() == null || getIdQuote() == null) {
            return false;
        }
        return Objects.equals(getIdQuote(), quoteDTO.getIdQuote());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdQuote());
    }

    @Override
    public String toString() {
        return "QuoteDTO{" +
            "id=" + getIdQuote() +
            ", dateIssued='" + getDateIssued() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
