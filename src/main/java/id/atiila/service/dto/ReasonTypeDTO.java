package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ReasonType entity.
 * BeSmart Team
 */

public class ReasonTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idReason;

    private String description;

    public Integer getIdReason() {
        return this.idReason;
    }

    public void setIdReason(Integer id) {
        this.idReason = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idReason;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReasonTypeDTO reasonTypeDTO = (ReasonTypeDTO) o;
        if (reasonTypeDTO.getIdReason() == null || getIdReason() == null) {
            return false;
        }
        return Objects.equals(getIdReason(), reasonTypeDTO.getIdReason());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdReason());
    }

    @Override
    public String toString() {
        return "ReasonTypeDTO{" +
            "id=" + getIdReason() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
