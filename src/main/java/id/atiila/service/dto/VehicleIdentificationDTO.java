package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the VehicleIdentification entity.
 * BeSmart Team
 */

public class VehicleIdentificationDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idVehicleIdentification;

    private String vehicleNumber;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateThru;

    private UUID vehicleId;

    private String customerId;

    private String customerName;

    public UUID getIdVehicleIdentification() {
        return this.idVehicleIdentification;
    }

    public void setIdVehicleIdentification(UUID id) {
        this.idVehicleIdentification = id;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateThru() {
        return dateThru;
    }

    public void setDateThru(ZonedDateTime dateThru) {
        this.dateThru = dateThru;
    }

    public UUID getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(UUID vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VehicleIdentificationDTO vehicleIdentificationDTO = (VehicleIdentificationDTO) o;
        if (vehicleIdentificationDTO.getIdVehicleIdentification() == null || getIdVehicleIdentification() == null) {
            return false;
        }
        return Objects.equals(getIdVehicleIdentification(), vehicleIdentificationDTO.getIdVehicleIdentification());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdVehicleIdentification());
    }

    @Override
    public String toString() {
        return "VehicleIdentificationDTO{" +
            "id=" + getIdVehicleIdentification() +
            ", vehicleNumber='" + getVehicleNumber() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateThru='" + getDateThru() + "'" +
            "}";
    }
}
