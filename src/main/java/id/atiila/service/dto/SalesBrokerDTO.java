package id.atiila.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the SalesBroker entity.
 * BeSmart Team
 */

public class SalesBrokerDTO extends PartyRoleDTO {

    private static final long serialVersionUID = 1L;

    private String idBroker;

    private Integer brokerTypeId;

    private PersonDTO person = new PersonDTO();

    public String getIdBroker() {
        return idBroker;
    }

    public void setIdBroker(String idBroker) {
        this.idBroker = idBroker;
    }

    public Integer getBrokerTypeId() { return brokerTypeId; }

    public void setBrokerTypeId(Integer brokerTypeId) { this.brokerTypeId = brokerTypeId; }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "SalesBrokerDTO{" +
            "id=" + getIdPartyRole() +
            ", idBroker='" + getIdBroker() + "'" +
            "}";
    }
}
