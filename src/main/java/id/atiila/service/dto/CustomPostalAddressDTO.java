package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the PostalAddress entity.
 * BeSmart Team
 */

public class CustomPostalAddressDTO {

    private static final long serialVersionUID = 1L;

    private String idContact;

    private String idContactType;

    private String address1;

    private String address2;

    private String districtId;

    private String villageId;

    private String cityId;

    private String provinceId;
    private String phone;
    private String phone2;
    private String phone3;

    public String getIdContact() {
        return this.idContact;
    }

    public void setIdContact(String id) {
        this.idContact = id;
    }

     public String getIdContactType() {
        return idContactType;
    }

    public void setIdContactType(String idContactType) {
        this.idContactType = idContactType;
    }


    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getDistrictId() { return districtId; }

    public void setDistrictId(String districtId) { this.districtId = districtId; }

    public String getVillageId() { return villageId; }

    public void setVillageId(String villageId) { this.villageId = villageId; }

    public String getCityId() { return cityId; }

    public void setCityId(String cityId) { this.cityId = cityId; }

    public String getProvinceId() { return provinceId; }

    public void setProvinceId(String provinceId) { this.provinceId = provinceId; }

    public String getPhone() { return phone; }

    public void setPhone(String phone) { this.phone = phone; }

    public String getPhone2() { return phone2; }

    public void setPhone2(String phone2) { this.phone2 = phone2; }

    public String getPhone3() { return phone3; }

    public void setPhone3(String phone3) { this.phone3 = phone3; }

  @Override
    public String toString() {
        return "PostalAddressDTO{" +
            "id=" + getIdContact() +
            ", address1='" + getAddress1() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", district='" + getDistrictId() + "'" +
            ", village='" + getVillageId() + "'" +
            ", city='" + getCityId() + "'" +
            ", province='" + getProvinceId() + "'" +
            "}";
    }
}
