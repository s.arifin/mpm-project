package id.atiila.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the ProspectOrganizationDetails entity.
 * BeSmart Team
 */

public class ProspectOrganizationDetailsDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idProspectOrganizationDetail;

    private UUID idProspect;

    private BigDecimal unitPrice;

    private Float discount;

//    private UUID personId;
    private PersonDTO person = new PersonDTO();


//    private String personName;

    private String productId;

    private String productName;

    private Integer colorId;

    private String colorDescription;

    public UUID getIdProspectOrganizationDetail() {
        return this.idProspectOrganizationDetail;
    }

    public void setIdProspectOrganizationDetail(UUID id) {
        this.idProspectOrganizationDetail = id;
    }

    public UUID getIdProspect() {
        return idProspect;
    }

    public void setIdProspect(UUID idProspect) {
        this.idProspect = idProspect;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

//    public String getPersonName() {
//        return personName;
//    }
//
//    public void setPersonName(String personName) {
//        this.personName = personName;
//    }
//
//    public UUID getPersonId() {
//        return personId;
//    }
//
//    public void setPersonId(UUID personId) {
//        this.personId = personId;
//    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String motorId) {
        this.productId = motorId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String motorName) {
        this.productName = motorName;
    }

    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer featureId) {
        this.colorId = featureId;
    }

    public String getColorDescription() {
        return colorDescription;
    }

    public void setColorDescription(String featureDescription) {
        this.colorDescription = featureDescription;
    }

    public UUID getId() {
        return this.idProspectOrganizationDetail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProspectOrganizationDetailsDTO prospectOrganizationDetailsDTO = (ProspectOrganizationDetailsDTO) o;
        if (prospectOrganizationDetailsDTO.getIdProspectOrganizationDetail() == null || getIdProspectOrganizationDetail() == null) {
            return false;
        }
        return Objects.equals(getIdProspectOrganizationDetail(), prospectOrganizationDetailsDTO.getIdProspectOrganizationDetail());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdProspectOrganizationDetail());
    }

    @Override
    public String toString() {
        return "ProspectOrganizationDetailsDTO{" +
            "id=" + getIdProspectOrganizationDetail() +
            ", idProspect='" + getIdProspect() + "'" +
            ", unitPrice='" + getUnitPrice() + "'" +
            ", discount='" + getDiscount() + "'" +
            "}";
    }
}
