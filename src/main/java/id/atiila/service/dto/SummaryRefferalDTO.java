package id.atiila.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.UUID;

public class SummaryRefferalDTO {
    private ZonedDateTime refDate;
    private String refNumber;
    private String idVendor;
    private Integer receiptQty;
    private Long inputQty;
    private String vendorName;
    private BigDecimal receiptNominal;

    public SummaryRefferalDTO(ZonedDateTime refDate, String refNumber, String idVendor, Integer receiptQty, Long inputQty, BigDecimal receiptNominal){
        this.refDate = refDate;
        this.refNumber = refNumber;
        this.idVendor = idVendor;
        this.receiptQty = receiptQty;
        this.inputQty = inputQty;
        this.receiptNominal = receiptNominal;
        // test
    }

    public SummaryRefferalDTO(ZonedDateTime refDate, String refNumber,BigDecimal receiptNominal){
        this.refDate = refDate;
        this.refNumber = refNumber;
        this.receiptNominal = receiptNominal;
        // test
    }

    public BigDecimal getReceiptNominal() { return receiptNominal; }

    public void setReceiptNominal(BigDecimal receiptNominal) { this.receiptNominal = receiptNominal; }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public ZonedDateTime getRefDate() {
        return refDate;
    }

    public void setRefDate(ZonedDateTime refDate) {
        this.refDate = refDate;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public String getIdVendor() {
        return idVendor;
    }

    public void setIdVendor(String idVendor) {
        this.idVendor = idVendor;
    }

    public Integer getReceiptQty() {
        return receiptQty;
    }

    public void setReceiptQty(Integer receiptQty) {
        this.receiptQty = receiptQty;
    }

    public Long getInputQty() {
        return inputQty;
    }

    public void setInputQty(Long inputQty) {
        this.inputQty = inputQty;
    }
}
