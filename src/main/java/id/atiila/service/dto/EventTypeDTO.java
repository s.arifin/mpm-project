package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the EventType entity.
 * BeSmart Team
 */

public class EventTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idEventType;

    private Integer idParentEventType;

    private String description;

    public EventTypeDTO() {
    }

    public EventTypeDTO(Integer idEventType, String description) {
        this.idEventType = idEventType;
        this.description = description;
    }

    public Integer getIdEventType() {
        return this.idEventType;
    }

    public void setIdEventType(Integer id) {
        this.idEventType = id;
    }

    public Integer getIdParentEventType() { return idParentEventType; }

    public void setIdParentEventType(Integer idParentEventType) { this.idParentEventType = idParentEventType; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.idEventType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EventTypeDTO eventTypeDTO = (EventTypeDTO) o;
        if (eventTypeDTO.getIdEventType() == null || getIdEventType() == null) {
            return false;
        }
        return Objects.equals(getIdEventType(), eventTypeDTO.getIdEventType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdEventType());
    }

    @Override
    public String toString() {
        return "EventTypeDTO{" +
            "id=" + getIdEventType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
