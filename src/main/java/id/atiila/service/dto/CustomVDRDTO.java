package id.atiila.service.dto;

import id.atiila.domain.Orders;
import id.atiila.domain.VehicleDocumentRequirement;
import id.atiila.domain.VehicleSalesOrder;

import java.time.ZonedDateTime;

public class CustomVDRDTO extends CustomRequirementDTO {

    private ZonedDateTime dateBASTSTNK;

    private String pengambilSTNK;

    private ZonedDateTime dateBASTNotice;

    private ZonedDateTime dateBASTPlatNomor;

    private ZonedDateTime dateBASTBPKB;

    public CustomVDRDTO() {
    }

    public CustomVDRDTO(VehicleSalesOrder o, VehicleDocumentRequirement r) {
        super(o, r);
    }

    public ZonedDateTime getDateBASTSTNK() {
        return dateBASTSTNK;
    }

    public void setDateBASTSTNK(ZonedDateTime dateBASTSTNK) {
        this.dateBASTSTNK = dateBASTSTNK;
    }

    public ZonedDateTime getDateBASTNotice() {
        return dateBASTNotice;
    }

    public void setDateBASTNotice(ZonedDateTime dateBASTNotice) {
        this.dateBASTNotice = dateBASTNotice;
    }

    public ZonedDateTime getDateBASTPlatNomor() {
        return dateBASTPlatNomor;
    }

    public void setDateBASTPlatNomor(ZonedDateTime dateBASTPlatNomor) {
        this.dateBASTPlatNomor = dateBASTPlatNomor;
    }

    public ZonedDateTime getDateBASTBPKB() {
        return dateBASTBPKB;
    }

    public void setDateBASTBPKB(ZonedDateTime dateBASTBPKB) {
        this.dateBASTBPKB = dateBASTBPKB;
    }

    public String getPengambilSTNK() {
        return pengambilSTNK;
    }

    public void setPengambilSTNK(String pengambilSTNK) {
        this.pengambilSTNK = pengambilSTNK;
    }
}
