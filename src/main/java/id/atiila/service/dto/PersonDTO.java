package id.atiila.service.dto;

import id.atiila.domain.PostalAddress;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.IntStream;

/**
 * A DTO for the Person entity.
 * BeSmart Team
 */

public class PersonDTO extends PartyDTO {

    private static final long serialVersionUID = 1L;

    private String firstName;

    private String lastName;

    private String pob;

    private String bloodType;

    private String gender;

    private ZonedDateTime dob;

    private String personalIdNumber;

    private String familyIdNumber;

    private String taxIdNumber;

    private Integer religionTypeId;

    private String religionTypeDescription;

    private Integer workTypeId;

    private String workTypeDescription;

    private String cellPhone1;

    private String cellPhone2;

    private String phone;

    private String privateMail;

    private PostalAddressDTO postalAddress;

    public PersonDTO() {
        super();
        this.postalAddress = new PostalAddressDTO();
    }

    public PersonDTO(String firstName, String lastName, String pob, ZonedDateTime dob) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.pob = pob;
        this.dob = dob;
        this.postalAddress = new PostalAddressDTO();
    }

    public PersonDTO(String firstName, String lastName, String cellPhone) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.cellPhone1 = cellPhone;
        this.postalAddress = new PostalAddressDTO();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPob() {
        return pob;
    }

    public void setPob(String pob) {
        this.pob = pob;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ZonedDateTime getDob() {
        return dob;
    }

    public void setDob(ZonedDateTime dob) {
        this.dob = dob;
    }

    public String getPersonalIdNumber() {
        return personalIdNumber;
    }

    public void setPersonalIdNumber(String personalIdNumber) {
        this.personalIdNumber = personalIdNumber;
    }

    public String getFamilyIdNumber() {
        return familyIdNumber;
    }

    public void setFamilyIdNumber(String familyIdNumber) {
        this.familyIdNumber = familyIdNumber;
    }

    public String getTaxIdNumber() {
        return taxIdNumber;
    }

    public void setTaxIdNumber(String taxIdNumber) {
        this.taxIdNumber = taxIdNumber;
    }

    public Integer getReligionTypeId() {
        return religionTypeId == null || religionTypeId.equals(0) ? 6: religionTypeId;
    }

    public void setReligionTypeId(Integer religionTypeId) {
        Integer[] validValues = {1, 2, 3, 4, 5, 6};
        boolean contains = Arrays.asList(validValues).contains(religionTypeId);
        if (contains) this.religionTypeId = religionTypeId;
        else this.religionTypeId = 6;
    }

    public String getReligionTypeDescription() {
        return religionTypeDescription;
    }

    public void setReligionTypeDescription(String religionTypeDescription) {
        this.religionTypeDescription = religionTypeDescription;
    }

    public Integer getWorkTypeId() {
        return workTypeId;
    }

    public void setWorkTypeId(Integer workTypeId) {
        this.workTypeId = workTypeId;
    }

    public String getWorkTypeDescription() {
        return workTypeDescription;
    }

    public void setWorkTypeDescription(String workTypeDescription) {
        this.workTypeDescription = workTypeDescription;
    }

    public PostalAddressDTO getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(PostalAddressDTO postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getCellPhone1() {
        return cellPhone1;
    }

    public void setCellPhone1(String cellPhone1) {
        this.cellPhone1 = cellPhone1;
    }

    public String getCellPhone2() {
        return cellPhone2;
    }

    public void setCellPhone2(String cellPhone2) {
        this.cellPhone2 = cellPhone2;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPrivateMail() {
        return privateMail;
    }

    public void setPrivateMail(String privateMail) {
        this.privateMail = privateMail;
    }

    @Override
    public String toString() {
        return "PersonDTO{" +
            "id=" + getIdParty() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", pob='" + getPob() + "'" +
            ", bloodType='" + getBloodType() + "'" +
            ", gender='" + getGender() + "'" +
            ", dob='" + getDob() + "'" +
            ", personalIdNumber='" + getPersonalIdNumber() + "'" +
            ", familyIdNumber='" + getFamilyIdNumber() + "'" +
            ", taxNumber='" + getTaxIdNumber() + "'" +
            ", workTypeId='" + getWorkTypeId() + "'" +
            "}";
    }
}
