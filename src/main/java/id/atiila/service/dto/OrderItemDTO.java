package id.atiila.service.dto;

import javax.persistence.Column;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the OrderItem entity.
 * BeSmart Team
 */

public class OrderItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @org.springframework.data.annotation.Id
    private UUID idOrderItem;

    private String idProduct;

    private String itemDescription;

    private Integer idFeature;

    private String idShipTo;

    private Double qty;

    private BigDecimal unitPrice;

    private BigDecimal discount;

    private BigDecimal taxAmount;

    private BigDecimal totalAmount;

    private BigDecimal bbn;

    private BigDecimal discountMD;

    private BigDecimal discountFinCoy;

    private BigDecimal discountatpm;

    private Double qtyReceipt;

    private Double qtyFilled;

    private UUID ordersId;

    private String idmachine;

    private String idframe;

    private String shipToName;

    private Number cogs;

    public Number getCogs() {
        return cogs;
    }

    public void setCogs(Number cogs) {
        this.cogs = cogs;
    }

    public String getIdmachine() {
        return idmachine;
    }

    public void setIdmachine(String idmachine) {
        this.idmachine = idmachine;
    }

    public String getIdframe() {
        return idframe;
    }

    public void setIdframe(String idframe) {
        this.idframe = idframe;
    }

    public BigDecimal getDiscountatpm() {
        return discountatpm;
    }

    public void setDiscountatpm(BigDecimal discountatpm) {
        this.discountatpm = discountatpm;
    }

    public UUID getIdOrderItem() {
        return this.idOrderItem;
    }

    public void setIdOrderItem(UUID id) {
        this.idOrderItem = id;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Integer getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Integer idFeature) {
        this.idFeature = idFeature;
    }

    public String getIdShipTo() {
        return idShipTo;
    }

    public void setIdShipTo(String idShipTo) {
        this.idShipTo = idShipTo;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public UUID getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(UUID ordersId) {
        this.ordersId = ordersId;
    }

    public Double getQtyReceipt() {
        return qtyReceipt;
    }

    public void setQtyReceipt(Double qtyReceipt) {
        this.qtyReceipt = qtyReceipt;
    }

    public Double getQtyFilled() {
        return qtyFilled;
    }

    public void setQtyFilled(Double qtyFilled) {
        this.qtyFilled = qtyFilled;
    }

    public BigDecimal getBbn() {
        return bbn;
    }

    public void setBbn(BigDecimal bbn) {
        this.bbn = bbn;
    }

    public BigDecimal getDiscountMD() {
        return discountMD;
    }

    public void setDiscountMD(BigDecimal discountMD) {
        this.discountMD = discountMD;
    }

    public BigDecimal getDiscountFinCoy() {
        return discountFinCoy;
    }

    public void setDiscountFinCoy(BigDecimal discountFinCoy) {
        this.discountFinCoy = discountFinCoy;
    }

    public String getShipToName() {
        return shipToName;
    }

    public void setShipToName(String shipToName) {
        this.shipToName = shipToName;
    }

    public UUID getId() {
        return this.idOrderItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderItemDTO orderItemDTO = (OrderItemDTO) o;
        if (orderItemDTO.getIdOrderItem() == null || getIdOrderItem() == null) {
            return false;
        }
        return Objects.equals(getIdOrderItem(), orderItemDTO.getIdOrderItem());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdOrderItem());
    }

    @Override
    public String toString() {
        return "OrderItemDTO{" +
            "id=" + getIdOrderItem() +
            ", idProduct='" + getIdProduct() + "'" +
            ", itemDescription='" + getItemDescription() + "'" +
            ", idFeature='" + getIdFeature() + "'" +
            ", idShipTo='" + getIdShipTo() + "'" +
            ", qty='" + getQty() + "'" +
            ", unitPrice='" + getUnitPrice() + "'" +
            ", discount='" + getDiscount() + "'" +
            ", taxAmount='" + getTaxAmount() + "'" +
            ", totalAmount='" + getTotalAmount() + "'" +
            "}";
    }
}
