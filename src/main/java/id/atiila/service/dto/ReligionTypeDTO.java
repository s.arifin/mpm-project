package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ReligionType entity.
 * BeSmart Team
 */

public class ReligionTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idReligionType;

    private String description;

    public Integer getIdReligionType() {
        return this.idReligionType;
    }

    public void setIdReligionType(Integer id) {
        this.idReligionType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idReligionType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReligionTypeDTO religionTypeDTO = (ReligionTypeDTO) o;
        if (religionTypeDTO.getIdReligionType() == null || getIdReligionType() == null) {
            return false;
        }
        return Objects.equals(getIdReligionType(), religionTypeDTO.getIdReligionType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdReligionType());
    }

    @Override
    public String toString() {
        return "ReligionTypeDTO{" +
            "id=" + getIdReligionType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
