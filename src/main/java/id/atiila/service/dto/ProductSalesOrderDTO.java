package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the ProductSalesOrder entity.
 * BeSmart Team
 */

public class ProductSalesOrderDTO extends SalesOrderDTO {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "ProductSalesOrderDTO{" +
            "id=" + getIdOrder() +
            "}";
    }
}
