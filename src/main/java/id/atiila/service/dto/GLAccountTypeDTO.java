package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the GLAccountType entity.
 * atiila consulting
 */

public class GLAccountTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idGLAccountType;

    private String description;

    public Integer getIdGLAccountType() {
        return this.idGLAccountType;
    }

    public void setIdGLAccountType(Integer id) {
        this.idGLAccountType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.idGLAccountType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GLAccountTypeDTO gLAccountTypeDTO = (GLAccountTypeDTO) o;
        if (gLAccountTypeDTO.getIdGLAccountType() == null || getIdGLAccountType() == null) {
            return false;
        }
        return Objects.equals(getIdGLAccountType(), gLAccountTypeDTO.getIdGLAccountType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdGLAccountType());
    }

    @Override
    public String toString() {
        return "GLAccountTypeDTO{" +
            "id=" + getIdGLAccountType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
