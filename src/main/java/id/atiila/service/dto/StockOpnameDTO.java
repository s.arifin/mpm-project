package id.atiila.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the StockOpname entity.
 * BeSmart Team
 */

public class StockOpnameDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID idStockOpname;

    private String stockOpnameNumber;

    private ZonedDateTime dateCreated;

    private Integer currentStatus;

    private Integer calendarId;

    private String calendarDescription;

    private String internalId;

    private Integer stockOpnameTypeId;

    private String stockOpnameTypeDescription;

    public UUID getIdStockOpname() {
        return this.idStockOpname;
    }

    public void setIdStockOpname(UUID id) {
        this.idStockOpname = id;
    }

    public String getStockOpnameNumber() {
        return stockOpnameNumber;
    }

    public void setStockOpnameNumber(String stockOpnameNumber) {
        this.stockOpnameNumber = stockOpnameNumber;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Integer getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(Integer standardCalendarId) {
        this.calendarId = standardCalendarId;
    }

    public String getCalendarDescription() {
        return calendarDescription;
    }

    public void setCalendarDescription(String standardCalendarDescription) {
        this.calendarDescription = standardCalendarDescription;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public Integer getStockOpnameTypeId() {
        return stockOpnameTypeId;
    }

    public void setStockOpnameTypeId(Integer stockOpnameTypeId) {
        this.stockOpnameTypeId = stockOpnameTypeId;
    }

    public String getStockOpnameTypeDescription() {
        return stockOpnameTypeDescription;
    }

    public void setStockOpnameTypeDescription(String stockOpnameTypeDescription) {
        this.stockOpnameTypeDescription = stockOpnameTypeDescription;
    }

    public Integer getCurrentStatus() {
        return this.currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    public UUID getId() {
        return this.idStockOpname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StockOpnameDTO stockOpnameDTO = (StockOpnameDTO) o;
        if (stockOpnameDTO.getIdStockOpname() == null || getIdStockOpname() == null) {
            return false;
        }
        return Objects.equals(getIdStockOpname(), stockOpnameDTO.getIdStockOpname());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdStockOpname());
    }

    @Override
    public String toString() {
        return "StockOpnameDTO{" +
            "id=" + getIdStockOpname() +
            ", stockOpnameNumber='" + getStockOpnameNumber() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            "}";
    }
}
