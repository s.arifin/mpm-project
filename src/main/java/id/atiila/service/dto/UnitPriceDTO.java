package id.atiila.service.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Approval entity.
 * BeSmart Team
 */

public class UnitPriceDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idProduct;

    private String idInternal;

    private BigDecimal unitPrice;

    private BigDecimal bbn;

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getIdInternal() {
        return idInternal;
    }

    public void setIdInternal(String idInternal) {
        this.idInternal = idInternal;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getBbn() {
        return bbn;
    }

    public void setBbn(BigDecimal bbn) {
        this.bbn = bbn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        UnitPriceDTO that = (UnitPriceDTO) o;

        return new EqualsBuilder()
            .append(idProduct, that.idProduct)
            .append(idInternal, that.idInternal)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(idProduct)
            .append(idInternal)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "UnitPriceDTO{" +
            "idMotor='" + idProduct + '\'' +
            ", idMSO='" + idInternal + '\'' +
            ", unitPrice=" + unitPrice +
            ", BBN=" + bbn +
            '}';
    }
}
