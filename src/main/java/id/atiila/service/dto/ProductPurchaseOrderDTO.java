package id.atiila.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import java.util.UUID;

/**
 * A DTO for the ProductPurchaseOrder entity.
 * BeSmart Team
 */

public class ProductPurchaseOrderDTO extends PurchaseOrderDTO {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "ProductPurchaseOrderDTO{" +
            "id=" + getIdOrder() +
            "}";
    }
}
