package id.atiila.service.dto;

import id.atiila.domain.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.UUID;

public class CustomShipmentItemDTO implements Serializable {

    private UUID idShipmentItem;

    private String idProduct;

    private String descriptionProduct;

    private Integer idFeature;

    private String descriptionFeature;

    private Double qty;

    private UUID idShipment;

    private UUID idReceipt;

    private String idFrame;

    private String idMachine;

    private Integer yearAssembly;

    private UUID idShipmentPackage;

    public CustomShipmentItemDTO(){ }

    public CustomShipmentItemDTO(ShipmentItem site, UnitShipmentReceipt usr, Product p, Feature f){
        this.idShipmentItem = site.getIdShipmentItem();
        this.idProduct = p.getIdProduct();
        this.descriptionProduct = p.getDescription();
        this.idFeature = f.getIdFeature();
        this.descriptionFeature = f.getDescription();
        this.qty = site.getQty();
        this.idShipment = site.getShipment().getIdShipment();
        this.idReceipt = usr.getIdReceipt();
        this.idFrame = usr.getIdFrame();
        this.idMachine = usr.getIdMachine();
        this.yearAssembly = usr.getYearAssembly();
        this.idShipmentPackage = usr.getShipmentPackage().getIdPackage();
    }

    public UUID getIdShipmentItem() { return idShipmentItem; }

    public void setIdShipmentItem(UUID idShipmentItem) { this.idShipmentItem = idShipmentItem; }

    public String getIdProduct() { return idProduct; }

    public void setIdProduct(String idProduct) { this.idProduct = idProduct; }

    public String getDescriptionProduct() { return descriptionProduct; }

    public void setDescriptionProduct(String descriptionProduct) { this.descriptionProduct = descriptionProduct; }

    public Integer getIdFeature() { return idFeature; }

    public void setIdFeature(Integer idFeature) { this.idFeature = idFeature; }

    public String getDescriptionFeature() { return descriptionFeature; }

    public void setDescriptionFeature(String descriptionFeature) { this.descriptionFeature = descriptionFeature; }

    public Double getQty() { return qty; }

    public void setQty(Double qty) { this.qty = qty; }

    public UUID getIdShipment() { return idShipment; }

    public void setIdShipment(UUID idShipment) { this.idShipment = idShipment; }

    public UUID getIdReceipt() { return idReceipt; }

    public void setIdReceipt(UUID idReceipt) { this.idReceipt = idReceipt; }

    public String getIdFrame() { return idFrame; }

    public void setIdFrame(String idFrame) { this.idFrame = idFrame; }

    public String getIdMachine() { return idMachine; }

    public void setIdMachine(String idMachine) { this.idMachine = idMachine; }

    public Integer getYearAssembly() { return yearAssembly; }

    public void setYearAssembly(Integer yearAssembly) { this.yearAssembly = yearAssembly; }

    public UUID getIdShipmentPackage() { return idShipmentPackage; }

    public void setIdShipmentPackage(UUID idShipmentPackage) { this.idShipmentPackage = idShipmentPackage; }
}
