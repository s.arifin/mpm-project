package id.atiila.service.dto;

public class ImportUnitReceiptDTO extends OrderItemDTO {

    private String idFacility;

    private String idColor;

    private String idFrame;

    private String idMachine;

    private Integer yearAssembly;

    public String getIdFacility() {
        return idFacility;
    }

    public void setIdFacility(String idFacility) {
        this.idFacility = idFacility;
    }

    public String getIdColor() {
        return idColor;
    }

    public void setIdColor(String idColor) {
        this.idColor = idColor;
    }

    public String getIdFrame() {
        return idFrame;
    }

    public void setIdFrame(String idFrame) {
        this.idFrame = idFrame;
    }

    public String getIdMachine() {
        return idMachine;
    }

    public void setIdMachine(String idMachine) {
        this.idMachine = idMachine;
    }

    public Integer getYearAssembly() {
        return yearAssembly;
    }

    public void setYearAssembly(Integer yearAssembly) {
        this.yearAssembly = yearAssembly;
    }
}
