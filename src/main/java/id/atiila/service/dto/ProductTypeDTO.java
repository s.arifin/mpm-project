package id.atiila.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ProductType entity.
 * BeSmart Team
 */

public class ProductTypeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idProductType;

    private String description;

    public ProductTypeDTO() {
    }

    public ProductTypeDTO(Integer idProductType, String description) {
        this.idProductType = idProductType;
        this.description = description;
    }

    public Integer getIdProductType() {
        return this.idProductType;
    }

    public void setIdProductType(Integer id) {
        this.idProductType = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getId() {
        return this.idProductType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductTypeDTO productTypeDTO = (ProductTypeDTO) o;
        if (productTypeDTO.getIdProductType() == null || getIdProductType() == null) {
            return false;
        }
        return Objects.equals(getIdProductType(), productTypeDTO.getIdProductType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getIdProductType());
    }

    @Override
    public String toString() {
        return "ProductTypeDTO{" +
            "id=" + getIdProductType() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
