package id.atiila.service;

import id.atiila.domain.*;

import io.github.jhipster.config.JHipsterProperties;

import org.apache.commons.lang3.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;
import java.time.ZonedDateTime;
import java.util.Locale;

/**
 * Service for sending emails.
 * <p>
 * We use the @Async annotation to send emails asynchronously.
 */
@Service
public class MailService {

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    private static final String USER = "user";

    private static final String SUR = "sur";

    private static final String CUST_DOB = "custDOB";

    private static final String STNK_DOB = "stnkDOB";

    private static final String CUST = "cust";

    private static final String POSTAL_ADDRESS_CUST = "postalAddressCust";

    private static final String POSTAL_ADDRESS_STNK = "postalAddressSTNK";

    private static final String BASE_URL = "baseUrl";

    private final JHipsterProperties jHipsterProperties;

    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    public MailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
            MessageSource messageSource, SpringTemplateEngine templateEngine) {

        this.jHipsterProperties = jHipsterProperties;
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
    }


    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
            message.setTo(to);
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.warn("Email could not be sent to user '{}'", to, e);
            } else {
                log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
            }
        }
    }

    @Async
    public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);

    }

    public void sendLeasingEmailFromTemplate(SalesUnitRequirement sur,
                                             PersonalCustomer personalCustomer,
                                             PostalAddress postalAddressCustomer,
                                             PostalAddress postalAddressStnk,
                                             String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag("id");
        ZonedDateTime customer_dob = personalCustomer.getPerson().getDob();
        ZonedDateTime stnk_dob = sur.getPersonOwner().getDob();
        String custdob = customer_dob.toLocalDate().toString();
        String stnkdob = stnk_dob.toLocalDate().toString();
        Context context = new Context(locale);
        context.setVariable(SUR, sur);
        context.setVariable(CUST, personalCustomer);
        context.setVariable(CUST_DOB, custdob);
        context.setVariable(STNK_DOB, stnkdob);
        context.setVariable(POSTAL_ADDRESS_CUST, postalAddressCustomer);
        context.setVariable(POSTAL_ADDRESS_STNK, postalAddressStnk);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(sur.getLeasingCompany().getOrganization().getOfficeMail(), subject, content, false, true);

    }

    @Async
    public void sendActivationEmail(User user) {
        log.debug("Sending activation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "activationEmail", "email.activation.title");
    }

    @Async
    public void sendCreationEmail(User user) {
        log.debug("Sending creation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "creationEmail", "email.activation.title");
    }

    @Async
    public void sendPasswordResetMail(User user) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "passwordResetEmail", "email.reset.title");
    }


    public void sendApprovalLeasingMail(SalesUnitRequirement salesUnitRequirement,
                                        PersonalCustomer personalCustomer,
                                        PostalAddress postalAddressCust,
                                        PostalAddress postalAddressStnk) {
        log.debug("Sending password reset email to '{}'", salesUnitRequirement.getLeasingCompany().getOrganization().getOfficeMail());
        sendLeasingEmailFromTemplate(salesUnitRequirement, personalCustomer, postalAddressCust, postalAddressStnk, "leasingApproval", "approval.leasing.title");
    }
}
