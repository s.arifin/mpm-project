package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.*;

import id.atiila.domain.Internal;
import id.atiila.domain.Position;
import id.atiila.repository.InternalRepository;
import id.atiila.repository.search.InternalSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.atiila.base.BaseConstants;
import id.atiila.domain.Branch;
import id.atiila.domain.Organization;
import id.atiila.repository.BranchRepository;
import id.atiila.repository.search.BranchSearchRepository;
import id.atiila.service.dto.BranchDTO;
import id.atiila.service.mapper.BranchMapper;

import javax.servlet.http.HttpServletRequest;

/**
 * Service Implementation for managing Branch.
 * BeSmart Team
 */

@Service
@Transactional
public class BranchService {

    private final Logger log = LoggerFactory.getLogger(BranchService.class);

    private final BranchRepository branchRepository;

    private final BranchMapper branchMapper;

    private final BranchSearchRepository branchSearchRepository;

    @Autowired
    private InternalRepository<Internal> internalRepository;

    @Autowired
    private InternalSearchRepository internalSearchRepository;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private PositionUtils positionUtils;

    public BranchService(BranchRepository branchRepository, BranchMapper branchMapper, BranchSearchRepository branchSearchRepository) {
        this.branchRepository = branchRepository;
        this.branchMapper = branchMapper;
        this.branchSearchRepository = branchSearchRepository;
    }

    /**
     * Save a branch.
     *
     * @param dto the entity to save
     * @return the persisted entity
     */
    public BranchDTO save(BranchDTO dto) {
        log.debug("Request to save Branch : {}", dto);
        Branch branch = branchMapper.toEntity(dto);

        Boolean isNewData = branch.getIdInternal() == null;

//        if (isNewData) {
//            branch = internalUtils.buildBranch(dto.getIdInternal(), dto.getOrganization().getName());
//        } else
//            branch = branchRepository.save(branch);

        branch = branchRepository.save(branch);

        /*if (branch.getParty().getFacilities().isEmpty()) {
            faclService.buildFacility(branch.getParty(), branch.getIdInternal(), branch.getParty().getName());
        }*/

        BranchDTO result = branchMapper.toDto(branch);
        branchSearchRepository.save(branch);

        return result;
    }

    /**
     *  Get all the branches.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BranchDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Branches");
        return branchRepository.findAll(pageable)
            .map(branchMapper::toDto);
    }

    /**
     *  Get one branch by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public BranchDTO findOne(String id) {
        log.debug("Request to get Branch : {}", id);
        Branch branch = branchRepository.findOne(id);
        return branchMapper.toDto(branch);
    }

    /**
     *  Delete the  branch by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Branch : {}", id);
        branchRepository.delete(id);
        branchSearchRepository.delete(id);
    }

    /**
     * Search for the branch corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BranchDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Branches for query {}", query);
        Page<Branch> result = branchSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(branchMapper::toDto);
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public BranchDTO processExecuteData(Integer id, String param, BranchDTO dto) {
        BranchDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
        }
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Set<BranchDTO> processExecuteListData(Integer id, String param, Set<BranchDTO> dto) {
        return dto;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Branch buildBranch(String idBranch, String name, String idHonda, Integer cat) {
        return null;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        internalSearchRepository.deleteAll();
        List<Internal> items =  internalRepository.findAll();
        for (Internal m: items) {
            internalSearchRepository.save(m);
            log.debug("Data Internal save !...");
        }

        internalSearchRepository.deleteAll();
        List<Branch> branches =  branchRepository.findAll();
        for (Branch m: branches) {
            branchRepository.save(m);
            log.debug("Save branch !...");
        }
    }

    @Transactional(readOnly = true)
    public Page<BranchDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered VendorDTO");
        String idVendor = request.getParameter("idVendor");
        if (idVendor != null) {
            return branchRepository.queryByVendor(idVendor, pageable).map(branchMapper::toDto);
        }
        return branchRepository.queryNothing(pageable).map(branchMapper::toDto);
    }
}
