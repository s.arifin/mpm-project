package id.atiila.service;

import id.atiila.domain.Payment;
import id.atiila.repository.PaymentRepository;
import id.atiila.repository.search.PaymentSearchRepository;
import id.atiila.service.dto.PaymentDTO;
import id.atiila.service.mapper.PaymentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing Payment.
 * BeSmart Team
 */

@Service
@Transactional
public class PaymentService {

    private final Logger log = LoggerFactory.getLogger(PaymentService.class);

    private final PaymentRepository paymentRepository;

    private final PaymentMapper paymentMapper;

    private final PaymentSearchRepository paymentSearchRepository;

    public PaymentService(PaymentRepository paymentRepository, PaymentMapper paymentMapper, PaymentSearchRepository paymentSearchRepository) {
        this.paymentRepository = paymentRepository;
        this.paymentMapper = paymentMapper;
        this.paymentSearchRepository = paymentSearchRepository;
    }

    /**
     * Save a payment.
     *
     * @param paymentDTO the entity to save
     * @return the persisted entity
     */
    public PaymentDTO save(PaymentDTO paymentDTO) {
        log.debug("Request to save Payment : {}", paymentDTO);
        Payment payment = paymentMapper.toEntity(paymentDTO);
        payment = paymentRepository.save(payment);
        PaymentDTO result = paymentMapper.toDto(payment);
        paymentSearchRepository.save(payment);
        return result;
    }

    /**
     * Get all the payments.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PaymentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Payments");
        return paymentRepository.findActivePayment(pageable)
            .map(paymentMapper::toDto);
    }

    /**
     * Get one payment by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PaymentDTO findOne(UUID id) {
        log.debug("Request to get Payment : {}", id);
        Payment payment = paymentRepository.findOne(id);
        return paymentMapper.toDto(payment);
    }

    /**
     * Delete the payment by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Payment : {}", id);
        paymentRepository.delete(id);
        paymentSearchRepository.delete(id);
    }

    /**
     * Search for the payment corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PaymentDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of Payments for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idPaymentType = request.getParameter("idPaymentType");
        String idMethod = request.getParameter("idMethod");
        String idInternal = request.getParameter("idInternal");
        String idPaidTo = request.getParameter("idPaidTo");
        String idPaidFrom = request.getParameter("idPaidFrom");

        if (idPaymentType != null) {
            q.withQuery(matchQuery("paymentType.idPaymentType", idPaymentType));
        }
        else if (idMethod != null) {
            q.withQuery(matchQuery("method.idPaymentMethod", idMethod));
        }
        else if (idInternal != null) {
            q.withQuery(matchQuery("internal.idInternal", idInternal));
        }
        else if (idPaidTo != null) {
            q.withQuery(matchQuery("paidTo.idBillTo", idPaidTo));
        }
        else if (idPaidFrom != null) {
            q.withQuery(matchQuery("paidFrom.idBillTo", idPaidFrom));
        }

        Page<Payment> result = paymentSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(paymentMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PaymentDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered PaymentDTO");
        String idPaymentType = request.getParameter("idPaymentType");
        String idMethod = request.getParameter("idMethod");
        String idInternal = request.getParameter("idInternal");
        String idPaidTo = request.getParameter("idPaidTo");
        String idPaidFrom = request.getParameter("idPaidFrom");

        if (idPaymentType != null) {
            return paymentRepository.findByIdPaymentType(Integer.valueOf(idPaymentType), pageable).map(paymentMapper::toDto); 
        }
        else if (idMethod != null) {
            return paymentRepository.findByIdMethod(Integer.valueOf(idMethod), pageable).map(paymentMapper::toDto); 
        }
        else if (idInternal != null) {
            return paymentRepository.findByIdInternal(idInternal, pageable).map(paymentMapper::toDto); 
        }
        else if (idPaidTo != null) {
            return paymentRepository.findByIdPaidTo(idPaidTo, pageable).map(paymentMapper::toDto); 
        }
        else if (idPaidFrom != null) {
            return paymentRepository.findByIdPaidFrom(idPaidFrom, pageable).map(paymentMapper::toDto); 
        }

        return paymentRepository.findAll(pageable).map(paymentMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, PaymentDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<PaymentDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public PaymentDTO changePaymentStatus(PaymentDTO dto, Integer id) {
        if (dto != null) {
			Payment e = paymentRepository.findOne(dto.getIdPayment());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        paymentSearchRepository.delete(dto.getIdPayment());
                        break;
                    default:
                        paymentSearchRepository.save(e);
                }
				paymentRepository.save(e);
			}
		}
        return dto;
    }
}
