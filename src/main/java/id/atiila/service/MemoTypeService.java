package id.atiila.service;

import id.atiila.domain.MemoType;
import id.atiila.repository.MemoTypeRepository;
import id.atiila.repository.search.MemoTypeSearchRepository;
import id.atiila.service.dto.MemoTypeDTO;
import id.atiila.service.mapper.MemoTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing MemoType.
 * BeSmart Team
 */

@Service
@Transactional
public class MemoTypeService {

    private final Logger log = LoggerFactory.getLogger(MemoTypeService.class);

    private final MemoTypeRepository memoTypeRepository;

    private final MemoTypeMapper memoTypeMapper;

    private final MemoTypeSearchRepository memoTypeSearchRepository;

    public MemoTypeService(MemoTypeRepository memoTypeRepository, MemoTypeMapper memoTypeMapper, MemoTypeSearchRepository memoTypeSearchRepository) {
        this.memoTypeRepository = memoTypeRepository;
        this.memoTypeMapper = memoTypeMapper;
        this.memoTypeSearchRepository = memoTypeSearchRepository;
    }

    /**
     * Save a memoType.
     *
     * @param memoTypeDTO the entity to save
     * @return the persisted entity
     */
    public MemoTypeDTO save(MemoTypeDTO memoTypeDTO) {
        log.debug("Request to save MemoType : {}", memoTypeDTO);
        MemoType memoType = memoTypeMapper.toEntity(memoTypeDTO);
        memoType = memoTypeRepository.save(memoType);
        MemoTypeDTO result = memoTypeMapper.toDto(memoType);
        memoTypeSearchRepository.save(memoType);
        return result;
    }

    /**
     * Get all the memoTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MemoTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MemoTypes");
        return memoTypeRepository.findAll(pageable)
            .map(memoTypeMapper::toDto);
    }

    /**
     * Get one memoType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public MemoTypeDTO findOne(Integer id) {
        log.debug("Request to get MemoType : {}", id);
        MemoType memoType = memoTypeRepository.findOne(id);
        return memoTypeMapper.toDto(memoType);
    }

    /**
     * Delete the memoType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete MemoType : {}", id);
        memoTypeRepository.delete(id);
        memoTypeSearchRepository.delete(id);
    }

    /**
     * Search for the memoType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MemoTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MemoTypes for query {}", query);
        Page<MemoType> result = memoTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(memoTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public MemoTypeDTO processExecuteData(Integer id, String param, MemoTypeDTO dto) {
        MemoTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional(readOnly = true)
    public Set<MemoTypeDTO> processExecuteListData(Integer id, String param, Set<MemoTypeDTO> dto) {
        Set<MemoTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

}
