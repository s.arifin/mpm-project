package id.atiila.service;

import id.atiila.domain.ProductAssociation;
import id.atiila.repository.ProductAssociationRepository;
import id.atiila.repository.search.ProductAssociationSearchRepository;
import id.atiila.service.dto.ProductAssociationDTO;
import id.atiila.service.mapper.ProductAssociationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ProductAssociation.
 * BeSmart Team
 */

@Service
@Transactional
public class ProductAssociationService {

    private final Logger log = LoggerFactory.getLogger(ProductAssociationService.class);

    private final ProductAssociationRepository productAssociationRepository;

    private final ProductAssociationMapper productAssociationMapper;

    private final ProductAssociationSearchRepository productAssociationSearchRepository;
    public ProductAssociationService(ProductAssociationRepository productAssociationRepository, ProductAssociationMapper productAssociationMapper, ProductAssociationSearchRepository productAssociationSearchRepository) {
        this.productAssociationRepository = productAssociationRepository;
        this.productAssociationMapper = productAssociationMapper;
        this.productAssociationSearchRepository = productAssociationSearchRepository;
    }

    /**
     * Save a productAssociation.
     *
     * @param productAssociationDTO the entity to save
     * @return the persisted entity
     */
    public ProductAssociationDTO save(ProductAssociationDTO productAssociationDTO) {
        log.debug("Request to save ProductAssociation : {}", productAssociationDTO);
        ProductAssociation productAssociation = productAssociationMapper.toEntity(productAssociationDTO);
        productAssociation = productAssociationRepository.save(productAssociation);
        ProductAssociationDTO result = productAssociationMapper.toDto(productAssociation);
        productAssociationSearchRepository.save(productAssociation);
        return result;
    }

    /**
     *  Get all the productAssociations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductAssociationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductAssociations");
        return productAssociationRepository.findAll(pageable)
            .map(productAssociationMapper::toDto);
    }

    /**
     *  Get one productAssociation by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ProductAssociationDTO findOne(UUID id) {
        log.debug("Request to get ProductAssociation : {}", id);
        ProductAssociation productAssociation = productAssociationRepository.findOne(id);
        return productAssociationMapper.toDto(productAssociation);
    }

    /**
     *  Delete the  productAssociation by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ProductAssociation : {}", id);
        productAssociationRepository.delete(id);
        productAssociationSearchRepository.delete(id);
    }

    /**
     * Search for the productAssociation corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductAssociationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ProductAssociations for query {}", query);
        Page<ProductAssociation> result = productAssociationSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(productAssociationMapper::toDto);
    }

    public ProductAssociationDTO processExecuteData(Integer id, String param, ProductAssociationDTO dto) {
        ProductAssociationDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<ProductAssociationDTO> processExecuteListData(Integer id, String param, Set<ProductAssociationDTO> dto) {
        Set<ProductAssociationDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        productAssociationSearchRepository.deleteAll();
        List<ProductAssociation> productAssociations =  productAssociationRepository.findAll();
        for (ProductAssociation m: productAssociations) {
            productAssociationSearchRepository.save(m);
            log.debug("Data productAssociation save !...");
        }
    }

}
