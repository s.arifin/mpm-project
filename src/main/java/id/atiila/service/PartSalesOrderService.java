package id.atiila.service;

import id.atiila.domain.PartSalesOrder;
import id.atiila.repository.PartSalesOrderRepository;
import id.atiila.repository.search.PartSalesOrderSearchRepository;
import id.atiila.service.dto.PartSalesOrderDTO;
import id.atiila.service.mapper.PartSalesOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing PartSalesOrder.
 * BeSmart Team
 */

@Service
@Transactional
public class PartSalesOrderService {

    private final Logger log = LoggerFactory.getLogger(PartSalesOrderService.class);

    private final PartSalesOrderRepository partSalesOrderRepository;

    private final PartSalesOrderMapper partSalesOrderMapper;

    private final PartSalesOrderSearchRepository partSalesOrderSearchRepository;

    public PartSalesOrderService(PartSalesOrderRepository partSalesOrderRepository, PartSalesOrderMapper partSalesOrderMapper, PartSalesOrderSearchRepository partSalesOrderSearchRepository) {
        this.partSalesOrderRepository = partSalesOrderRepository;
        this.partSalesOrderMapper = partSalesOrderMapper;
        this.partSalesOrderSearchRepository = partSalesOrderSearchRepository;
    }

    /**
     * Save a partSalesOrder.
     *
     * @param partSalesOrderDTO the entity to save
     * @return the persisted entity
     */
    public PartSalesOrderDTO save(PartSalesOrderDTO partSalesOrderDTO) {
        log.debug("Request to save PartSalesOrder : {}", partSalesOrderDTO);
        PartSalesOrder partSalesOrder = partSalesOrderMapper.toEntity(partSalesOrderDTO);
        partSalesOrder = partSalesOrderRepository.save(partSalesOrder);
        PartSalesOrderDTO result = partSalesOrderMapper.toDto(partSalesOrder);
        partSalesOrderSearchRepository.save(partSalesOrder);
        return result;
    }

    /**
     * Get all the partSalesOrders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartSalesOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PartSalesOrders");
        return partSalesOrderRepository.findActivePartSalesOrder(pageable)
            .map(partSalesOrderMapper::toDto);
    }

    /**
     * Get one partSalesOrder by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PartSalesOrderDTO findOne(UUID id) {
        log.debug("Request to get PartSalesOrder : {}", id);
        PartSalesOrder partSalesOrder = partSalesOrderRepository.findOne(id);
        return partSalesOrderMapper.toDto(partSalesOrder);
    }

    /**
     * Delete the partSalesOrder by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete PartSalesOrder : {}", id);
        partSalesOrderRepository.delete(id);
        partSalesOrderSearchRepository.delete(id);
    }

    /**
     * Search for the partSalesOrder corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartSalesOrderDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of PartSalesOrders for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idInternal = request.getParameter("idInternal");
        String idCustomer = request.getParameter("idCustomer");
        String idBillTo = request.getParameter("idBillTo");

        if (filterName != null) {
        }
        Page<PartSalesOrder> result = partSalesOrderSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(partSalesOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PartSalesOrderDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered PartSalesOrderDTO");
        String idInternal = request.getParameter("idInternal");
        String idCustomer = request.getParameter("idCustomer");
        String idBillTo = request.getParameter("idBillTo");

        return partSalesOrderRepository.findByParams(idInternal, idCustomer, idBillTo, pageable)
            .map(partSalesOrderMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public PartSalesOrderDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        PartSalesOrderDTO r = null;
        return r;
    }

    @Transactional
    public Set<PartSalesOrderDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<PartSalesOrderDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public PartSalesOrderDTO changePartSalesOrderStatus(PartSalesOrderDTO dto, Integer id) {
        if (dto != null) {
			PartSalesOrder e = partSalesOrderRepository.findOne(dto.getIdOrder());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        partSalesOrderSearchRepository.delete(dto.getIdOrder());
                        break;
                    default:
                        partSalesOrderSearchRepository.save(e);
                }
				partSalesOrderRepository.save(e);
			}
		}
        return dto;
    }
}
