package id.atiila.service;

import id.atiila.domain.VehicleWorkRequirement;
import id.atiila.repository.VehicleWorkRequirementRepository;
import id.atiila.repository.search.VehicleWorkRequirementSearchRepository;
import id.atiila.service.dto.VehicleWorkRequirementDTO;
import id.atiila.service.mapper.VehicleWorkRequirementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing VehicleWorkRequirement.
 * BeSmart Team
 */

@Service
@Transactional
public class VehicleWorkRequirementService {

    private final Logger log = LoggerFactory.getLogger(VehicleWorkRequirementService.class);

    private final VehicleWorkRequirementRepository vehicleWorkRequirementRepository;

    private final VehicleWorkRequirementMapper vehicleWorkRequirementMapper;

    private final VehicleWorkRequirementSearchRepository vehicleWorkRequirementSearchRepository;
    public VehicleWorkRequirementService(VehicleWorkRequirementRepository vehicleWorkRequirementRepository, VehicleWorkRequirementMapper vehicleWorkRequirementMapper, VehicleWorkRequirementSearchRepository vehicleWorkRequirementSearchRepository) {
        this.vehicleWorkRequirementRepository = vehicleWorkRequirementRepository;
        this.vehicleWorkRequirementMapper = vehicleWorkRequirementMapper;
        this.vehicleWorkRequirementSearchRepository = vehicleWorkRequirementSearchRepository;
    }

    /**
     * Save a vehicleWorkRequirement.
     *
     * @param vehicleWorkRequirementDTO the entity to save
     * @return the persisted entity
     */
    public VehicleWorkRequirementDTO save(VehicleWorkRequirementDTO vehicleWorkRequirementDTO) {
        log.debug("Request to save VehicleWorkRequirement : {}", vehicleWorkRequirementDTO);
        VehicleWorkRequirement vehicleWorkRequirement = vehicleWorkRequirementMapper.toEntity(vehicleWorkRequirementDTO);
        vehicleWorkRequirement = vehicleWorkRequirementRepository.save(vehicleWorkRequirement);
        VehicleWorkRequirementDTO result = vehicleWorkRequirementMapper.toDto(vehicleWorkRequirement);
        vehicleWorkRequirementSearchRepository.save(vehicleWorkRequirement);
        return result;
    }

    /**
     *  Get all the vehicleWorkRequirements.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleWorkRequirementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VehicleWorkRequirements");
        return vehicleWorkRequirementRepository.findAll(pageable)
            .map(vehicleWorkRequirementMapper::toDto);
    }

    /**
     *  Get one vehicleWorkRequirement by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public VehicleWorkRequirementDTO findOne(UUID id) {
        log.debug("Request to get VehicleWorkRequirement : {}", id);
        VehicleWorkRequirement vehicleWorkRequirement = vehicleWorkRequirementRepository.findOne(id);
        return vehicleWorkRequirementMapper.toDto(vehicleWorkRequirement);
    }

    /**
     *  Delete the  vehicleWorkRequirement by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete VehicleWorkRequirement : {}", id);
        vehicleWorkRequirementRepository.delete(id);
        vehicleWorkRequirementSearchRepository.delete(id);
    }

    /**
     * Search for the vehicleWorkRequirement corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleWorkRequirementDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of VehicleWorkRequirements for query {}", query);
        Page<VehicleWorkRequirement> result = vehicleWorkRequirementSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(vehicleWorkRequirementMapper::toDto);
    }

    public VehicleWorkRequirementDTO processExecuteData(Integer id, String param, VehicleWorkRequirementDTO dto) {
        VehicleWorkRequirementDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<VehicleWorkRequirementDTO> processExecuteListData(Integer id, String param, Set<VehicleWorkRequirementDTO> dto) {
        Set<VehicleWorkRequirementDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
