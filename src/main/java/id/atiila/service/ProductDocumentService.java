package id.atiila.service;

import id.atiila.domain.ProductDocument;
import id.atiila.repository.ProductDocumentRepository;
import id.atiila.repository.search.ProductDocumentSearchRepository;
import id.atiila.service.dto.ProductDocumentDTO;
import id.atiila.service.mapper.ProductDocumentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing ProductDocument.
 * BeSmart Team
 */

@Service
@Transactional
public class ProductDocumentService {

    private final Logger log = LoggerFactory.getLogger(ProductDocumentService.class);

    private final ProductDocumentRepository productDocumentRepository;

    private final ProductDocumentMapper productDocumentMapper;

    private final ProductDocumentSearchRepository productDocumentSearchRepository;

    public ProductDocumentService(ProductDocumentRepository productDocumentRepository, ProductDocumentMapper productDocumentMapper, ProductDocumentSearchRepository productDocumentSearchRepository) {
        this.productDocumentRepository = productDocumentRepository;
        this.productDocumentMapper = productDocumentMapper;
        this.productDocumentSearchRepository = productDocumentSearchRepository;
    }

    /**
     * Save a productDocument.
     *
     * @param productDocumentDTO the entity to save
     * @return the persisted entity
     */
    public ProductDocumentDTO save(ProductDocumentDTO productDocumentDTO) {
        log.debug("Request to save ProductDocument : {}", productDocumentDTO);
        ProductDocument productDocument = productDocumentMapper.toEntity(productDocumentDTO);
        productDocument = productDocumentRepository.save(productDocument);
        ProductDocumentDTO result = productDocumentMapper.toDto(productDocument);
        productDocumentSearchRepository.save(productDocument);
        return result;
    }

    @Transactional(readOnly = true)
    public List<ProductDocumentDTO> findAllByIdProduct(String idproduct) {
        log.debug("SERVICE to get all ProductDocuments by idproduct");
        List<ProductDocument> l =  productDocumentRepository.findAllProductDocumentByIdProduct(idproduct);
        List<ProductDocumentDTO> l_dto = productDocumentMapper.toDto(l);

        return l_dto;
    }

    /**
     *  Get all the productDocuments.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductDocumentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductDocuments");
        return productDocumentRepository.findAll(pageable)
            .map(productDocumentMapper::toDto);
    }

    /**
     *  Get one productDocument by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ProductDocumentDTO findOne(UUID id) {
        log.debug("Request to get ProductDocument : {}", id);
        ProductDocument productDocument = productDocumentRepository.findOne(id);
        return productDocumentMapper.toDto(productDocument);
    }

    /**
     *  Delete the  productDocument by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ProductDocument : {}", id);
        productDocumentRepository.delete(id);
        productDocumentSearchRepository.delete(id);
    }

    /**
     * Search for the productDocument corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductDocumentDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ProductDocuments for query {}", query);
        Page<ProductDocument> result = productDocumentSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(productDocumentMapper::toDto);
    }

    public ProductDocumentDTO processExecuteData(Integer id, String param, ProductDocumentDTO dto) {
        ProductDocumentDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<ProductDocumentDTO> processExecuteListData(Integer id, String param, Set<ProductDocumentDTO> dto) {
        Set<ProductDocumentDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

}
