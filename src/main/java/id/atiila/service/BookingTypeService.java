package id.atiila.service;

import id.atiila.domain.BookingType;
import id.atiila.repository.BookingTypeRepository;
import id.atiila.repository.search.BookingTypeSearchRepository;
import id.atiila.service.dto.BookingTypeDTO;
import id.atiila.service.mapper.BookingTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.PostConstruct;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing BookingType.
 * BeSmart Team
 */

@Service
@Transactional
public class BookingTypeService {

    private final Logger log = LoggerFactory.getLogger(BookingTypeService.class);

    private final BookingTypeRepository bookingTypeRepository;

    private final BookingTypeMapper bookingTypeMapper;

    private final BookingTypeSearchRepository bookingTypeSearchRepository;

    public BookingTypeService(BookingTypeRepository bookingTypeRepository, BookingTypeMapper bookingTypeMapper, BookingTypeSearchRepository bookingTypeSearchRepository) {
        this.bookingTypeRepository = bookingTypeRepository;
        this.bookingTypeMapper = bookingTypeMapper;
        this.bookingTypeSearchRepository = bookingTypeSearchRepository;
    }

    /**
     * Save a bookingType.
     *
     * @param bookingTypeDTO the entity to save
     * @return the persisted entity
     */
    public BookingTypeDTO save(BookingTypeDTO bookingTypeDTO) {
        log.debug("Request to save BookingType : {}", bookingTypeDTO);
        BookingType bookingType = bookingTypeMapper.toEntity(bookingTypeDTO);
        bookingType = bookingTypeRepository.save(bookingType);
        BookingTypeDTO result = bookingTypeMapper.toDto(bookingType);
        bookingTypeSearchRepository.save(bookingType);
        return result;
    }

    /**
     *  Get all the bookingTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BookingTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BookingTypes");
        return bookingTypeRepository.findAll(pageable)
            .map(bookingTypeMapper::toDto);
    }

    /**
     *  Get one bookingType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public BookingTypeDTO findOne(Integer id) {
        log.debug("Request to get BookingType : {}", id);
        BookingType bookingType = bookingTypeRepository.findOne(id);
        return bookingTypeMapper.toDto(bookingType);
    }

    /**
     *  Delete the  bookingType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete BookingType : {}", id);
        bookingTypeRepository.delete(id);
        bookingTypeSearchRepository.delete(id);
    }

    /**
     * Search for the bookingType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BookingTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of BookingTypes for query {}", query);
        Page<BookingType> result = bookingTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(bookingTypeMapper::toDto);
    }

    public BookingTypeDTO processExecuteData(BookingTypeDTO dto, Integer id) {
        BookingTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @PostConstruct
    @Transactional
    public void initialize() {
//        if (bookingTypeRepository.findOne(10) == null) {
//            save(new BookingTypeDTO(10, "Regular Booking"));
//            save(new BookingTypeDTO(11, "Home Booking"));
//            save(new BookingTypeDTO(12, "Emergency Booking"));
//        }
    }
}
