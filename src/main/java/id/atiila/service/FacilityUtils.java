package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.*;
import id.atiila.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional
public class FacilityUtils {

    private final Logger log = LoggerFactory.getLogger(FacilityUtils.class);

    @Autowired
    private FacilityRepository facilityRepository;

    @Autowired
    private FacilityTypeRepository facilityTypeRepository;

    @Autowired
    private PurposeTypeRepository purposeTypeRepository;

    @Autowired
    private ContainerRepository containerRepository;

    @Autowired
    private ContainerTypeRepository containerTypeRepository;

    @Autowired
    private MasterNumberingService numberingService;

    public ContainerType getContainerType(Integer id, String description) {
        ContainerType r = containerTypeRepository.findOne(id);
        if (r == null) {
            r = new ContainerType();
            r.setIdContainerType(id);
            r.setDescription(description);
            r = containerTypeRepository.save(r);
        }
        return r;
    }

    public ContainerType getContainerType(Integer id) {
        return getContainerType(id, "Unknown");
    }

    public FacilityType getFacilityType(Integer id, String description) {
        FacilityType r = facilityTypeRepository.findOne(id);
        if (r == null) {
            r = new FacilityType();
            r.setIdFacilityType(id);
            r.setDescription(description);
            r = facilityTypeRepository.save(r);
        }
        return r;
    }

    public PurposeType getPurposeType(Integer id, String description) {
        PurposeType r = purposeTypeRepository.findOne(id);
        if (r == null) {
            r = new PurposeType();
            r.setIdPurposeType(id);
            r.setDescription(description);
            r = purposeTypeRepository.save(r);
        }
        return r;
    }

    public PurposeType getPurposeType(Integer id) {
        return getPurposeType(id, "Unknown");
    }

    public FacilityType getFacilityType(Integer id) {
        return getFacilityType(id, "Unknown");
    }

    public Facility buildFacility(Integer idType, String idFacility, String description, Facility partOf) {
        Facility r = facilityRepository.findByFacilityCode(idFacility);
        if (r == null) {
            r = new Facility();
            r.setFacilityCode(idFacility);
            r.setDescription(description);
            r.setFacilityType(getFacilityType(idType));
            r.setStatus(BaseConstants.STATUS_ACTIVE);
            if (partOf != null) {
                r.setPartOf(partOf);
            }
            r = facilityRepository.save(r);
        }
        return r;
    }

    public Facility buildBuilding(String idFacility, String description) {
        getFacilityType(BaseConstants.FACILITY_TYPE_BUILDING, "Building");
        Facility r = buildFacility(BaseConstants.FACILITY_TYPE_BUILDING, idFacility, description, null);
        return r;
    }

    public Facility buildRoom(Facility building, String idFacility, String description) {
        getFacilityType(BaseConstants.FACILITY_TYPE_ROOM, "Room");
        return buildFacility(BaseConstants.FACILITY_TYPE_ROOM, idFacility, description, building);
    }

    public Facility buildWareHouse(Facility building, String idFacility, String description) {
        getFacilityType(BaseConstants.FACILITY_TYPE_WAREHOUSE, "Warehouse");
        return buildFacility(BaseConstants.FACILITY_TYPE_WAREHOUSE, idFacility, description, building);
    }

    public Facility buildStore(Facility building, String idFacility, String description) {
        getFacilityType(BaseConstants.FACILITY_TYPE_STORE, "Store");
        return buildFacility(BaseConstants.FACILITY_TYPE_STORE, idFacility, description, building);
    }

    public Facility buildWorkshop(Facility building, String idFacility, String description) {
        getFacilityType(BaseConstants.FACILITY_TYPE_WORKSHOP, "Workshop");
        return buildFacility(BaseConstants.FACILITY_TYPE_WORKSHOP, idFacility, description, building);
    }

    public Facility buildPit(Facility workshop, String idFacility, String description) {
        getFacilityType(BaseConstants.FACILITY_TYPE_PIT, "Pit");
        return buildFacility(BaseConstants.FACILITY_TYPE_PIT, idFacility, description, workshop);
    }

    public Container buildContainer(Facility facility, Integer type, String id, String description) {
        Container c = containerRepository.findOneByContainerCode(id);
        if (c == null) {
            ContainerType containerType = getContainerType(type);
            c = new Container();
            c.setFacility(facility);
            c.setContainerCode(id);
            c.setDescription(description);
            c.setContainerType(containerType);
            c = containerRepository.save(c);
        }
        return c;
    }

    public Container addRak(Facility facility) {
        String id = numberingService.getContainerNumber(facility);
        return buildContainer(facility, BaseConstants.CONTAINER_TYPE_RAK, id, "Rak " + id);
    }

    public String getFacilityCode(UUID idFacility) {
        Facility facility = facilityRepository.findOne(idFacility);
        if (facility != null) {
            return facility.getFacilityCode();
        }
        return null;
    }

    //TODO @Cacheable() maaf pak saya remark karena setelah saya remark ini baru bisa jalan pak.
//    @Cacheable()
    public String getContainerCode(UUID idContainer) {
        Container container = containerRepository.findOne(idContainer);
        if (container != null) {
            return container.getContainerCode();
        }
        return null;
    }

}
