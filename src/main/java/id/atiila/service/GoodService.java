package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.atiila.domain.Good;
import id.atiila.domain.Product;
import id.atiila.repository.GoodRepository;
import id.atiila.repository.search.GoodSearchRepository;
import id.atiila.service.dto.GoodDTO;
import id.atiila.service.mapper.GoodMapper;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service Implementation for managing Good.
 * BeSmart Team
 */

@Service
@Transactional
public class GoodService extends ProductBaseService {

    private final Logger log = LoggerFactory.getLogger(GoodService.class);

    private final GoodRepository goodRepository;

    private final GoodMapper goodMapper;

    private final GoodSearchRepository goodSearchRepository;

    public GoodService(GoodRepository goodRepository, GoodMapper goodMapper, GoodSearchRepository goodSearchRepository) {
        this.goodRepository = goodRepository;
        this.goodMapper = goodMapper;
        this.goodSearchRepository = goodSearchRepository;
    }

    /**
     * Save a good.
     *
     * @param goodDTO the entity to save
     * @return the persisted entity
     */
    public GoodDTO save(GoodDTO goodDTO) {
        log.debug("Request to save Good : {}", goodDTO);
        Good good = goodMapper.toEntity(goodDTO);

        Boolean isNew = good.getIdProduct() == null;
        if (isNew) {
            String newValue = null;
            while (newValue == null || goodRepository.findOne(newValue) != null) {
                newValue = numbering.nextValue("idgood", 200000l).toString();
            }
            good.setIdProduct(newValue);
        }

        good = goodRepository.save(good);
        GoodDTO result = goodMapper.toDto(good);
        goodSearchRepository.save(good);
        return result;
    }

    /**
     *  Get all the goods.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GoodDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Goods");
        return goodRepository.findAll(pageable)
            .map(goodMapper::toDto);
    }

    /**
     *  Get one good by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public GoodDTO findOne(String id) {
        log.debug("Request to get Good : {}", id);
        Good good = goodRepository.findOneWithEagerRelationships(id);
        return goodMapper.toDto(good);
    }

    /**
     *  Delete the  good by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Good : {}", id);
        goodRepository.delete(id);
        goodSearchRepository.delete(id);
    }

    /**
     * Search for the good corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GoodDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Goods for query {}", query);
        Page<Good> result = goodSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(goodMapper::toDto);
    }

    @Override
    public Product findById(String id) {
        return goodRepository.findOne(id);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }


    @Async
    public void buildIndex() {
        goodSearchRepository.deleteAll();
        List<Good> goods =  goodRepository.findAll();
        for (Good m: goods) {
            goodSearchRepository.save(m);
            log.debug("Data good save !...");
        }
    }
}
