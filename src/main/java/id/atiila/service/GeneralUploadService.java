package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.base.XlsxUtils;
import id.atiila.base.event.UploadDataEvent;
import id.atiila.domain.*;
import id.atiila.repository.GeneralUploadRepository;
import id.atiila.repository.OrderItemRepository;
import id.atiila.repository.PackageReceiptRepository;
import id.atiila.repository.UnitShipmentReceiptRepository;
import id.atiila.repository.search.GeneralUploadSearchRepository;
import id.atiila.service.dto.*;
import id.atiila.service.mapper.GeneralUploadMapper;
import id.atiila.service.mapper.OrderItemMapper;
import id.atiila.service.mapper.UnitShipmentReceiptMapper;
import org.codehaus.jackson.map.Serializers;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing GeneralUpload.
 * atiila consulting
 */

@Service
@Transactional
public class GeneralUploadService {

    private final Logger log = LoggerFactory.getLogger(GeneralUploadService.class);

    private final GeneralUploadRepository generalUploadRepository;

    private final GeneralUploadMapper generalUploadMapper;

    private final GeneralUploadSearchRepository generalUploadSearchRepository;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private OrderItemMapper orderItemMapper;

    @Autowired
    private PackageReceiptRepository packageReceiptRepository;

    @Autowired
    private UnitShipmentReceiptRepository unitShipmentReceiptRepository;

    @Autowired
    private InventoryUtils inventoryUtils;

    @Autowired
    private ProductUtils productUtils;

    @Autowired
    private OrderUtils orderUtils;

    public GeneralUploadService(GeneralUploadRepository generalUploadRepository, GeneralUploadMapper generalUploadMapper, GeneralUploadSearchRepository generalUploadSearchRepository) {
        this.generalUploadRepository = generalUploadRepository;
        this.generalUploadMapper = generalUploadMapper;
        this.generalUploadSearchRepository = generalUploadSearchRepository;
    }

    /**
     * Save a generalUpload.
     *
     * @param generalUploadDTO the entity to save
     * @return the persisted entity
     */
    public GeneralUploadDTO save(GeneralUploadDTO generalUploadDTO) {
        log.debug("Request to save GeneralUpload : {}", generalUploadDTO);
        GeneralUpload generalUpload = generalUploadMapper.toEntity(generalUploadDTO);
        generalUpload = generalUploadRepository.save(generalUpload);
        GeneralUploadDTO result = generalUploadMapper.toDto(generalUpload);
        generalUploadSearchRepository.save(generalUpload);
        return result;
    }

    /**
     * Get all the generalUploads.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GeneralUploadDTO> findAll(Pageable pageable) {
        log.debug("Request to get all GeneralUploads");
        return generalUploadRepository.findActiveGeneralUpload(pageable)
            .map(generalUploadMapper::toDto);
    }

    /**
     * Get one generalUpload by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public GeneralUploadDTO findOne(UUID id) {
        log.debug("Request to get GeneralUpload : {}", id);
        GeneralUpload generalUpload = generalUploadRepository.findOne(id);
        return generalUploadMapper.toDto(generalUpload);
    }

    /**
     * Delete the generalUpload by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete GeneralUpload : {}", id);
        generalUploadRepository.delete(id);
        generalUploadSearchRepository.delete(id);
    }

    /**
     * Search for the generalUpload corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GeneralUploadDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of GeneralUploads for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idInternal = request.getParameter("idInternal");
        String idPurpose = request.getParameter("idPurpose");

        if (idInternal != null) {
            q.withQuery(matchQuery("internal.idInternal", idInternal));
        }
        else if (idPurpose != null) {
            q.withQuery(matchQuery("purpose.idPurposeType", idPurpose));
        }

        Page<GeneralUpload> result = generalUploadSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(generalUploadMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<GeneralUploadDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered GeneralUploadDTO");
        String idInternal = request.getParameter("idInternal");
        String idPurpose = request.getParameter("idPurpose");

        if (idInternal != null) {
            return generalUploadRepository.queryByIdInternal(idInternal, pageable).map(generalUploadMapper::toDto);
        }
        else if (idPurpose != null) {
            return generalUploadRepository.queryByIdPurpose(Integer.valueOf(idPurpose), pageable).map(generalUploadMapper::toDto);
        }

        return generalUploadRepository.queryNothing(pageable).map(generalUploadMapper::toDto);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        String id = (String) item.get("id");
        Integer type = (Integer) item.get("idPurposeType");

        if (id != null && type != null) {
            GeneralUpload gu = generalUploadRepository.findOne(UUID.fromString(id));

            switch (type) {
                case BaseConstants.PURP_TYPE_INIT_STOCK:
                    this.publisher.publishEvent(new UploadDataEvent(this, gu, BaseConstants.PURP_TYPE_INIT_STOCK));
                    break;
                case BaseConstants.PURP_TYPE_INIT_CUSTOMER:
                    this.publisher.publishEvent(new UploadDataEvent(this, gu, BaseConstants.PURP_TYPE_INIT_CUSTOMER));
                    break;
                case BaseConstants.PURP_TYPE_INIT_STNK_BPKB_ON_HAND:
                    this.publisher.publishEvent(new UploadDataEvent(this, gu, BaseConstants.PURP_TYPE_INIT_STNK_BPKB_ON_HAND));
                    break;
                default:
                    this.publisher.publishEvent(new UploadDataEvent(this, gu, gu.getPurpose().getIdPurposeType()));
                    break;
            }
        }
        return r;
    }

    @EventListener
    @Transactional
    public void uploadInitStock(UploadDataEvent e) {
        if (e.getPurposeType().equals(BaseConstants.PURP_TYPE_INIT_STOCK) &&  e.getGeneralUpload() != null && e.getGeneralUpload().getInternal() != null) {
            migrateInitStock(e.getGeneralUpload().getInternal(), new ByteArrayInputStream(e.getGeneralUpload().getValue()));
            e.getGeneralUpload().setStatus(BaseConstants.STATUS_COMPLETED);
            generalUploadRepository.save(e.getGeneralUpload());
        }
    }

    @Transactional
    public void migrateInitStock(Internal internal, InputStream data) {
        try {
            DefaultResourceLoader loader = new DefaultResourceLoader();
            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/init-unit-stock.xml").getInputStream());
            if (templateXML != null && data != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<ImportUnitReceiptDTO> items = XlsxUtils.parseExcelStreamToBeans(data, templateXML);

                ShipTo shipTo = internalUtils.getShipTo(internal);

                PackageReceipt pr = new PackageReceipt();
                pr.setInternal(internal);
                pr.setIdPackageType(BaseConstants.PACK_TYPE_INIT_STOCK_UNIT);
                pr.setShipFrom(shipTo);
                pr.setStatus(BaseConstants.STATUS_COMPLETED);
                packageReceiptRepository.save(pr);

                InternalOrder order = new InternalOrder();
                order.setInternalTo(internal);
                order.setInternalFrom(internal);
                order.setOrderType(orderUtils.getOrderType(BaseConstants.ORDER_TYPE_INIT));
                order.setStatus(BaseConstants.STATUS_COMPLETED);
                order = orderUtils.save(order);

                for (ImportUnitReceiptDTO o: items) {
                    Product product = productUtils.getProduct(o.getIdProduct());
                    Feature feature = productUtils.getFeatureByKey(BaseConstants.FEAT_TYPE_COLOR, o.getIdColor());

                    if (product != null && feature != null) {
                        OrderItem oi = orderItemMapper.toEntity(o);
                        oi.setItemDescription(product != null ? product.getName() : null);
                        oi.setIdFeature(feature != null ? feature.getIdFeature() : null);
                        oi.setQty(1);
                        oi.setUnitPrice(o.getUnitPrice());
                        oi.setDiscount(0);
                        oi.setIdShipTo(shipTo != null ? shipTo.getIdShipTo() : null);
                        oi.setOrders(order);
                        oi = orderUtils.save(oi);

                        UnitShipmentReceipt detail = inventoryUtils.buildUnitShipmentReceipt(pr, oi, o.getIdFrame(), o.getIdMachine(), o.getYearAssembly());
                        unitShipmentReceiptRepository.save(detail);
                    }
                }
            }
        } catch (Exception e) {
            throw new DmsException(e.getMessage());
            // System.out.println(e.getMessage());
        }
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, GeneralUploadDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<GeneralUploadDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public GeneralUploadDTO changeGeneralUploadStatus(GeneralUploadDTO dto, Integer id) {
        if (dto != null) {
			GeneralUpload e = generalUploadRepository.findOne(dto.getIdGeneralUpload());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                    case 17:
                        generalUploadSearchRepository.delete(dto.getIdGeneralUpload());
                        break;
                    default:
                        generalUploadSearchRepository.save(e);
                }
				e = generalUploadRepository.save(e);
                return generalUploadMapper.toDto(e);
			}
		}
        return dto;
    }

    @EventListener
    @Transactional
    public void uploadBpkbOnHand(UploadDataEvent e) {
        if (e.getPurposeType().equals(BaseConstants.PURP_TYPE_INIT_STNK_BPKB_ON_HAND) &&  e.getGeneralUpload() != null && e.getGeneralUpload().getInternal() != null) {
            migrateBpkbOnHand(e.getGeneralUpload().getInternal(), new ByteArrayInputStream(e.getGeneralUpload().getValue()));
            e.getGeneralUpload().setStatus(BaseConstants.STATUS_COMPLETED);
            generalUploadRepository.save(e.getGeneralUpload());
        }
    }

    @Transactional
    public void migrateBpkbOnHand(Internal internal, InputStream data) {
        try {
            DefaultResourceLoader loader = new DefaultResourceLoader();
            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/init-bpkb-on-hand.xml").getInputStream());
            if (templateXML != null && data != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<ImportBpkbOnHandDTO> items = XlsxUtils.parseExcelStreamToBeans(data, templateXML);
                log.debug("data dari bpkb on hand xml== " + templateXML);
                log.debug("data dari bpkb on hand excel== " + data);
                for (ImportBpkbOnHandDTO o: items) {
                    log.debug("data dari bpkb on hand idframe == " + o.getIdFrame());
                    log.debug("data dari bpkb on hand customer == " + o.getNamaCustomer());
                    log.debug("data dari bpkb on hand == " + o);
                    System.out.println();
                }
            }
        } catch (Exception e) {
            throw new DmsException(e.getMessage());
            // System.out.println(e.getMessage());
        }
    }
}
