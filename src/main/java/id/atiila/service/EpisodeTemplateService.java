package id.atiila.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.math.*;
import java.util.concurrent.TimeUnit;

import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.route.EpisodeRoute;
import id.atiila.service.dto.*;

import id.atiila.service.mapper.PersonMapper;
import id.atiila.service.mapper.PersonalCustomerMapper;
import id.atiila.service.util.RandomUtil;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EpisodeTemplateService {

    private final Logger log = LoggerFactory.getLogger(EpisodeTemplateService.class);

    protected PersonDTO personDTO;

    protected SuspectPersonDTO suspectPersonDTO;

    protected ProspectPersonDTO prospectPersonDTO;

    @Autowired
    private ProducerTemplate camelTemplate;

    @Autowired
    protected SuspectPersonService suspectPersonService;

    @Autowired
    protected ProspectPersonService prospectPersonService;

    @Autowired
    protected VendorUtils vendorUtils;

    @Autowired
    protected ProductService productService;

    @Autowired
    protected PackageReceiptRepository packageReceiptRepository;

    @Autowired
    protected ShipmentReceiptRepository shipmentReceiptRepository;

    @Autowired
    protected OrderItemRepository orderItemRepository;

    @Autowired
    protected InventoryItemRepository inventoryItemRepository;

    @Autowired
    protected FeatureRepository featureRepository;

    @Autowired
    protected ShipToService shipToService;

    @Autowired
    protected SalesUnitRequirementService surService;

    @Autowired
    protected VehicleSalesOrderService vsoService;

    @Autowired
    protected ShipmentOutgoingService doService;

    @Autowired
    protected PersonalCustomerService persCustService;

    @Autowired
    protected Faker faker;

    @Autowired
    protected PersonMapper personMapper;

    @Autowired
    protected LeasingCompanyService leasingCompanyService;

    @Autowired
    protected VehicleSalesBillingService vsbService;

    @Autowired
    protected LeasingTenorProvideService leasingTenorProvideService;

    @Autowired
    private OrganizationUtils organizationUtils;

    @Autowired
    private ProductUtils productUtils;

    @Autowired
    private PriceUtils priceUtils;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private DateUtils dateUtils;

    @Autowired
    private RuleSalesDiscountService ruleSalesDiscountService;

    @Autowired
    private StorageUtils storageUtils;

    @Autowired
    private ProductPurchaseOrderRepository productPurchaseOrderRepository;

    @Autowired
    private CustomerUtils customerUtils;

    @Autowired
    private PersonalCustomerMapper personalCustomerMapper;

    @Autowired
    private ActivitiProcessor processor;

    @Autowired
    private VehicleSalesOrderRepository vehicleSalesOrderRepository;

    @Autowired
    private ActivitiProcessor activitiProcessor;

    @Autowired
    private VehicleCustomerRequestRepository vehicleCustomerRequestRepository;

    @Autowired
    private RequestTypeRepository requestTypeRepository;

    @Autowired
    private SaleTypeRepository saleTypeRepository;

    @Autowired
    private SalesUnitRequirementRepository salesUnitRequirementRepository;

    @Autowired
    private MasterNumberingService numberingService;

    @Autowired
    private SalesBrokerRepository salesBrokerRepository;

    @Autowired
    private FacilityUtils facilityUtils;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private OrderUtils orderUtils;

    @Autowired
    private BillingDisbursementRepository billingDisbursementRepository;

    @Autowired
    private UnitShipmentReceiptRepository unitShipmentReceiptRepository;

    @Transactional
    public void assignPerson(PersonDTO p) {
        personDTO = p;
    }

    @Transactional
    public void buildLeasingTenorProvide(String productId, String leasingId, Integer tenor, BigDecimal downpayment, BigDecimal installment){
        ZonedDateTime dtNow = ZonedDateTime.now();
        ZonedDateTime dtAdd = dtNow.plusMonths(1);

        LeasingTenorProvideDTO leasingTenorProvideDTO = new LeasingTenorProvideDTO();
        leasingTenorProvideDTO.setProductId(productId);
        leasingTenorProvideDTO.setLeasingId(UUID.fromString(leasingId));
        leasingTenorProvideDTO.setTenor(tenor);
        leasingTenorProvideDTO.setDownPayment(downpayment);
        leasingTenorProvideDTO.setInstallment(installment);
        leasingTenorProvideDTO.setDateFrom(dtNow);
        leasingTenorProvideDTO.setDateThru(dtAdd);

        leasingTenorProvideService.save(leasingTenorProvideDTO);
    }

    @Transactional
    public void buildLeasingCompany(String name){
        LeasingCompanyDTO leasingCompanyDTO = new LeasingCompanyDTO();
        leasingCompanyDTO.getOrganization().setName(name);

        leasingCompanyService.save(leasingCompanyDTO);
    }

    @Transactional
    public void buildSuspect(String idInternal, Integer idSuspectType){
        Internal intr = internalUtils.findOne(idInternal);
        Random r = new Random();

        if (intr == null) throw new DmsException("Internal tidak di kenal !");

        if (faker.randomInt(5) == 5)
            personDTO = faker.buildeFemale();
        else
            personDTO = faker.buildMale();

        suspectPersonDTO = suspectPersonService.findByPersonAndSuspectType(personDTO, idSuspectType, idInternal);

        if (suspectPersonDTO == null){
            suspectPersonDTO = new SuspectPersonDTO();
            suspectPersonDTO.setPerson(personDTO);
            suspectPersonDTO.setDealerId(intr.getIdInternal());
            suspectPersonDTO.setSuspectTypeId(idSuspectType);
//            if (idSuspectType == 11){
            suspectPersonDTO.setSalesDate(ZonedDateTime.now());

            float purchase1, purchase2;
            purchase1 = 0 + r.nextFloat() * (99 - 0);
            BigDecimal bd = new BigDecimal(Float.toString(purchase1));
            bd = bd.setScale(2,RoundingMode.HALF_UP);
            purchase1 = bd.floatValue();
            suspectPersonDTO.setRepurchaseProbability(purchase1);

            purchase2 = 0 + r.nextFloat() * (99 - 0);
            bd = new BigDecimal(Float.toString(purchase2));
            bd = bd.setScale(2,RoundingMode.HALF_UP);
            purchase2 = bd.floatValue();
            suspectPersonDTO.setPriority(purchase2);


//            } else if (idSuspectType == 12){

//            }
            suspectPersonDTO = suspectPersonService.save(suspectPersonDTO);
        }
    }

    public Person buildFakePerson() {
        PersonDTO p = null;
        if (faker.randomInt(5) == 5)
            p = faker.buildeFemale();
        else
            p = faker.buildMale();

        return partyUtils.buildPerson( p.getFirstName(), p.getLastName(), p.getPob(), p.getDob());
    }


    public PersonDTO buildFakePersonDTO() {
        Person p = buildFakePerson();
        return personMapper.toDto(p);
    }

    @Transactional
    public void buildProspect(String idInternal) {
        Internal intr = internalUtils.findOne(idInternal);
        Person person = buildFakePerson();
        ProspectPerson prospectPerson = null;

        if (intr == null) throw new DmsException("Id Internal tidak di kenal !");

        personDTO = personMapper.toDto(person);
        prospectPerson = prospectPersonService.findByPerson(person, idInternal);

        if (prospectPerson == null) {
            prospectPerson = new ProspectPerson();
            prospectPerson.setPerson(person);
            prospectPerson.setDealer(intr);
            prospectPersonDTO = prospectPersonService.buildData(prospectPerson);
        }
    }

    @Transactional
    public void buildSalesUnitRequirement(String idInternal, String idMotor, String color) {
        Internal intr = internalUtils.findOne(idInternal);
        if (intr == null) throw new DmsException("Id Internal tidak di kenal !");

        //Build Personal Customer
        PersonalCustomer cust = customerUtils.buildPersonalCustomer(intr, buildFakePerson());
        PersonalCustomer owner = customerUtils.buildPersonalCustomer(intr, buildFakePerson());

        PersonalCustomerDTO persCust = personalCustomerMapper.toDto(cust);
        PersonalCustomerDTO custOwner = personalCustomerMapper.toDto(owner);

        Feature colorFeature = featureRepository.findByKey(BaseConstants.FEAT_TYPE_COLOR, color);

        SalesUnitRequirement sur = surService.buildOneFromProspectPersonAndOwner(internalUtils.toDTO(intr), persCust, custOwner, idMotor, colorFeature.getIdFeature());
        VehicleSalesOrderDTO vso = vsoService.buildFromSUR(sur);

        VehicleSalesOrder v = vehicleSalesOrderRepository.findOne(vso.getIdOrder());

        OrderItem oi = v.getDetails().iterator().next();
        if (oi == null) throw new DmsException("VSO Item not found !");

        v.setAdminSalesVerifyValue(1);
        v.setAfcVerifyValue(1);
        v.setStatus(BaseConstants.STATUS_VSO_MATCHING);
        v = vehicleSalesOrderRepository.save(v);

        Map<String, Object> vars = activitiProcessor.getVariables("orderItem", oi);
        vars.put("vso", v);
        // activitiProcessor.startProcess("vso-item-process", oi.getIdOrderItem().toString(), vars);

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildStockUnit(String idInternal, String idMotor, String color, Integer qty) {
        Branch internal = branchRepository.findOne(idInternal);
        //find facility
        Facility facility = internal.getPrimaryFacility();
        if (facility == null){
            Facility building = facilityUtils.buildBuilding( idInternal, "Building " + idInternal);
            Facility store = facilityUtils.buildStore(building, internal.getIdInternal() + "S",
                internal.getOrganization().getName() + " (Store)");
            internal.addFacility(facilityUtils.getPurposeType(BaseConstants.PURP_TYPE_STORE), store);
            facility = facilityUtils.buildWareHouse(building, internal.getIdInternal() + "G", internal.getOrganization().getName() + " (Warehouse)");
            internal.addFacility(facilityUtils.getPurposeType(BaseConstants.PURP_TYPE_WAREHOUSE), facility);
            internal = branchRepository.save(internal);
            // throw new RuntimeException("Facility dengan code tidak ditemukan");
        }

        String idVendor = "OTV";

        // nambah disini
        Internal intr = internalUtils.findOne(idInternal);
        if (intr == null) throw new DmsException("Id Internal tidak di kenal !");

        Vendor vendor = vendorUtils.buildVendor(idVendor, "One Time Vendor");
        ShipTo shipTo = internalUtils.getShipTo(intr);

        //cari feature
        Feature featColor = featureRepository.findByKey(BaseConstants.FEAT_TYPE_COLOR, color);

        //Cari Motor
        Product p = productService.findById(idMotor);
        Motor motor = null;

        if (p != null && p instanceof Motor) motor = (Motor) p;

        /*
        if (vendor == null || internal == null || motor == null || shipTo == null) {
            throw new RuntimeException("Not all data find !");
        }*/

        if (qty <= 0)
            throw new RuntimeException("Qty harus lebih besar dari 0");

        PackageReceipt sp = new PackageReceipt();
        sp.setInternal(intr);
        sp.setShipFrom(vendorUtils.getShipTo(vendor));
        sp = packageReceiptRepository.save(sp);

        List<UnitShipmentReceipt> lsr = new ArrayList<>();

        //Buat Shipment Receipt
        for (int i = 0; i < qty; i++) {
            UnitShipmentReceipt sr = new UnitShipmentReceipt();
            sr.setShipmentPackage(sp);
            sr.setIdProduct(idMotor);
            sr.setQtyAccept(1d);
            sr.setQtyReject(0d);
            sr.setItemDescription(motor.getName());
            sr.setIdFeature(featColor.getIdFeature());
            sr.setIdMachine(RandomStringUtils.randomAlphanumeric(15).toUpperCase());
            sr.setIdFrame(RandomStringUtils.randomAlphanumeric(15).toUpperCase());
            lsr.add(sr);
        }

        log.debug("process buat order item");

        //Buat Order Item
        for (ShipmentReceipt sr: lsr) {
            OrderItem orit = new OrderItem();
            orit.setQty(sr.getQtyAccept() - sr.getQtyReject());
            orit.setIdProduct(sr.getIdProduct());
            orit.setIdFeature(sr.getIdFeature());
            orit.setUnitPrice(BigDecimal.valueOf(12330000L));
            orit.setDiscount(BigDecimal.ZERO);
            orit.setItemDescription(motor.getName());
            orit.setIdShipTo(shipTo.getIdShipTo());
            orit = orderItemRepository.save(orit);

            sr.setOrderItem(orit);
        }

        log.debug("process buat inventory item");

        //Buat Inventory Item
        for (UnitShipmentReceipt sr: lsr) {
            InventoryItem inv = new InventoryItem();
            inv.setShipmentReceipt(sr);
            inv.setIdProduct(sr.getIdProduct());
            inv.setIdFeature(sr.getIdFeature());
            inv.setQty(sr.getQtyAccept() - sr.getQtyReject());
            inv.setQtyBooking(0d);
            inv.setIdOwner(sr.getShipmentPackage().getInternal().getOrganization().getIdParty());
            inv.setIdFrame(sr.getIdFrame());
            inv.setIdMachine(sr.getIdMachine());
            inv.setIdFacility(facility.getIdFacility());
            inv.setYearAssembly(2018);

            inv = inventoryItemRepository.save(inv);

            sr.addInventoryItem(inv);
            shipmentReceiptRepository.save(sr);
        }

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildPrice() {
        List<Internal> litr = organizationUtils.getAllInternal();
        Motor m = productUtils.buildMotor("AAF01", "Motor Listrik I", "UNT", "CBR1000");

        for (Internal o: litr) {
            productUtils.setProductPrice(11, m, o.getOrganization(), new BigDecimal(17500000));
            productUtils.setProductPrice(101, m, o.getOrganization(), new BigDecimal(2450000));
            productUtils.setDiscount(12, m, o.getOrganization(), 12.5f);
        }

        for (Internal o: litr) {
            List<PriceComponent> l = productUtils.getPriceBy(m, o.getOrganization(), ZonedDateTime.now());
            for (PriceComponent p: l) {
                System.out.println("Hasil nya: " + p);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildRuleSalesDiscount() {
        RuleSalesDiscountDTO ruleSalesDiscountDTO;

        ruleSalesDiscountDTO = new RuleSalesDiscountDTO();
        ruleSalesDiscountDTO.setMaxAmount(BigDecimal.valueOf(500000));
        ruleSalesDiscountDTO.setRole(BaseConstants.AUTH_SALESMAN);
        ruleSalesDiscountDTO.setIdRuleType(102);
        ruleSalesDiscountDTO.setSequenceNumber(1);
        ruleSalesDiscountDTO.setDateFrom(ZonedDateTime.now());
        ruleSalesDiscountDTO.setDateThru(dateUtils.addMonthFromNow(1));

        ruleSalesDiscountService.save(ruleSalesDiscountDTO);

        ruleSalesDiscountDTO = new RuleSalesDiscountDTO();
        ruleSalesDiscountDTO.setMaxAmount(BigDecimal.valueOf(1000000));
        ruleSalesDiscountDTO.setRole(BaseConstants.AUTH_KORSAL);
        ruleSalesDiscountDTO.setIdRuleType(102);
        ruleSalesDiscountDTO.setSequenceNumber(2);
        ruleSalesDiscountDTO.setDateFrom(ZonedDateTime.now());
        ruleSalesDiscountDTO.setDateThru(dateUtils.addMonthFromNow(1));

        ruleSalesDiscountService.save(ruleSalesDiscountDTO);

        ruleSalesDiscountDTO = new RuleSalesDiscountDTO();
        ruleSalesDiscountDTO.setMaxAmount(BigDecimal.valueOf(999999999));
        ruleSalesDiscountDTO.setRole(BaseConstants.AUTH_KACAB);
        ruleSalesDiscountDTO.setIdRuleType(102);
        ruleSalesDiscountDTO.setSequenceNumber(3);
        ruleSalesDiscountDTO.setDateFrom(ZonedDateTime.now());
        ruleSalesDiscountDTO.setDateThru(dateUtils.addMonthFromNow(1));

        ruleSalesDiscountService.save(ruleSalesDiscountDTO);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildSampleReceiveing() {
        //Buat Package Receipt
        Vendor v = vendorUtils.buildVendor("PMD01", "Primary Main Dealer");
        Internal intr = internalUtils.findOne("05601");
        if (intr == null) throw new DmsException("Id Internal tidak di kenal !");

        PackageReceipt p = new PackageReceipt();
        p.setDocumentNumber(RandomStringUtils.randomAlphanumeric(7));
        p.setShipFrom(vendorUtils.getShipTo(v));
        p.setInternal(intr);

        for (String idProduct: new String[]{"ABF01", "ACF01", "ADF01", "AFF01"}) {
            Product product = productUtils.getProduct(idProduct);
            BigDecimal unitPrice = priceUtils.getSalesPrice(intr, idProduct);

            UnitShipmentReceipt sr = new UnitShipmentReceipt();
            sr.setIdProduct(idProduct);
            sr.setItemDescription(product.getName());
            sr.setIdFeature(101);
            sr.setYearAssembly(2018);
            sr.setShipmentPackage(p);
            sr.setIdFrame(RandomStringUtils.randomAlphanumeric(10));
            sr.setIdMachine(RandomStringUtils.randomAlphanumeric(10));
            sr.setQtyAccept(1d);
            sr.setQtyReject(0d);
            sr.setUnitPrice(unitPrice);
            p.getShipmentReceipts().add(sr);
        }

        camelTemplate.sendBodyAndHeader(EpisodeRoute.ACTION_BUILD_RECEIVING, p, "currentStatus", p.getStatus());
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildStockProductCase(String idInternal) {
        Internal intr = internalUtils.findOne(idInternal);
        if (intr == null) throw new DmsException("Id Internal tidak di kenal !");

        BillTo billTo = internalUtils.getBillTo(intr);

        Facility facl = intr.getPrimaryFacility();
        Vendor v = vendorUtils.buildVendor("PMD01", "Primary Main Dealer");

        // Build Part
        RemPart part = productUtils.buildRemPart("PART01", "Part 01", "PCS");
        Container container = storageUtils.buildContainer(facl, StorageUtils.RAK, "R01");
        storageUtils.buildGoodContainer(intr.getOrganization(), part, container);
        priceUtils.setVendorSalesPrice(v, "PART01", BigDecimal.valueOf(15450));

        part = productUtils.buildRemPart("PART02", "Part 02", "PCS");
        container = storageUtils.buildContainer(facl, StorageUtils.RAK, "R02");
        storageUtils.buildGoodContainer(intr.getOrganization(), part, container);
        priceUtils.setVendorSalesPrice(v, "PART02", BigDecimal.valueOf(87650));

        part = productUtils.buildRemPart("PART03", "Part 03", "PCS");
        container = storageUtils.buildContainer(facl, StorageUtils.RAK, "R03");
        storageUtils.buildGoodContainer(intr.getOrganization(), part, container);
        priceUtils.setVendorSalesPrice(v, "PART03", BigDecimal.valueOf(120000));

        part = productUtils.buildRemPart("PART04", "Part 04", "PCS");
        container = storageUtils.buildContainer(facl, StorageUtils.RAK, "R04");
        storageUtils.buildGoodContainer(intr.getOrganization(), part, container);
        priceUtils.setVendorSalesPrice(v, "PART04", BigDecimal.valueOf(65400));

        part = productUtils.buildRemPart("PART05", "Part 05", "PCS");
        container = storageUtils.buildContainer(facl, StorageUtils.RAK, "R05");
        storageUtils.buildGoodContainer(intr.getOrganization(), part, container);
        priceUtils.setVendorSalesPrice(v, "PART05", BigDecimal.valueOf(32500));

        part = productUtils.buildRemPart("PART06", "Part 06", "PCS");
        container = storageUtils.buildContainer(facl, StorageUtils.RAK, "R06");
        storageUtils.buildGoodContainer(intr.getOrganization(), part, container);
        priceUtils.setVendorSalesPrice(v, "PART06", BigDecimal.valueOf(187500));

        ProductPurchaseOrder ppo = new ProductPurchaseOrder();
        ppo.setInternal(intr);
        ppo.setVendor(v);
        ppo.setBillTo(billTo);
        ppo.setDateOrder(ZonedDateTime.now().minusDays(10));
        ppo = productPurchaseOrderRepository.save(ppo);

        for (String idProduct: new String[]{"PART01", "PART02", "PART03", "PART04", "PART05", "PART06"}) {
            Product product = productUtils.getProduct(idProduct);
            BigDecimal unitPrice = priceUtils.getSalesPrice(v, idProduct);

            OrderItem oi = new OrderItem();
            oi.setIdProduct(idProduct);
            oi.setItemDescription(product.getName());
            oi.setOrders(ppo);
            oi.setQty(12d);
            oi.setDiscount(0);
            oi.setUnitPrice(unitPrice);
            oi.setTaxAmount(unitPrice.doubleValue() * 10/100);
            oi.setTotalAmount(oi.getQty().doubleValue() * oi.getUnitPrice().doubleValue());
            ppo.getDetails().add(oi);
        }

        ppo.setStatus(BaseConstants.STATUS_ACTIVE);
        ppo = productPurchaseOrderRepository.save(ppo);

        camelTemplate.sendBody(EpisodeRoute.ACTION_ORDER_PART, ppo);
    }

    public PersonalCustomer buildPersonalCustomer(Internal internal) {
        PersonDTO p = faker.buildMale();
        Person person = personMapper.toEntity(p);
        person = partyUtils.buildPerson(person);
        return customerUtils.buildPersonalCustomer(internal, person);
    }

    public Person fakerPerson() {
        PersonDTO p = faker.buildMale();
        Person person = personMapper.toEntity(p);
        return partyUtils.buildPerson(person);
    }

    public void buildVSOgc(String idInternal) {
        Internal internal = internalUtils.findOne(idInternal);
        OrganizationCustomer oc = customerUtils.buildOrganizationCustomer("PT. Ramunda Utama");
        RequestType requestType = requestTypeRepository.findOne(BaseConstants.REQUEST_TYPE_VSO_GC);
        SaleType saleType = saleTypeRepository.findOne(BaseConstants.SALE_TYPE_COD);

        //
        Integer number = RandomUtils.nextInt(1000000, 10000000);
        VehicleCustomerRequest vcr = new VehicleCustomerRequest();
        vcr.setRequestType(requestType);
        vcr.setIdInternal(idInternal);
        vcr.setCustomer(oc);
        vcr.setCustomerOrder(number.toString());
        vcr = vehicleCustomerRequestRepository.save(vcr);

        Feature colorFeature = featureRepository.findByKey(BaseConstants.FEAT_TYPE_COLOR, "BL");
        Motor motor = productUtils.buildMotor("AN1","TEST","TEST", "UNT");

        for (int i=0; i<2; i++) {
            String vsonumber = numberingService.findTransNumberBy(internal.getRoot().getIdInternal(), "REQ");

            SalesUnitRequirement sur = new SalesUnitRequirement();
            sur.setInternal(internal);
            sur.setOrganizationOwner(oc.getOrganization());
            sur.setRequirementType(new RequirementType(BaseConstants.REQ_TYPE_SUR_GC));
            sur.setCustomer(oc);
            sur.setColor(colorFeature);
            sur.setProduct(motor);
            sur.setBillTo(customerUtils.getBillTo(oc));
            sur.setRequirementNumber(vsonumber);

            sur.setSaleType(saleType);
            sur.setIdRequest(vcr.getIdRequest());

            //Ambil Harga Jual
            Double het = 15750000d;
            Double bbn = 1435000d;

            sur.setDownPayment(BigDecimal.valueOf(1500000));
            sur.setHetprice(BigDecimal.valueOf(het));
            sur.setUnitPrice(BigDecimal.valueOf(het * 100/110));
            sur.setBbnprice(BigDecimal.valueOf(bbn));
            sur.setSubsahm(BigDecimal.valueOf(50000));
            sur.setSubsmd(BigDecimal.valueOf(50000));
            sur.setSubsfincomp(BigDecimal.valueOf(50000));
            sur.setSubsown(BigDecimal.valueOf(50000));

            sur = salesUnitRequirementRepository.save(sur);
        }

        // Build Data
        Map<String, Object> vars = activitiProcessor.getVariables("request", vcr);
        activitiProcessor.startProcess("vehicle-customer-request", vcr.getIdRequest().toString(), vars);
    }

    public void buildVSOdirect(String idInternal) {
        String idBroker = "ABC0001";
        SalesBroker salesBroker = salesBrokerRepository.findOneByIdBroker( idBroker );
        if (salesBroker == null) {
            Person person = partyUtils.buildPerson("Adang", "Salikhun", "Jambi", ZonedDateTime.of(1977, 12, 22, 0, 0, 0, 0, ZoneId.systemDefault()));

            salesBroker = new SalesBroker();
            salesBroker.setIdBroker(idBroker);
            salesBroker.setPerson(person);
            salesBroker = salesBrokerRepository.save(salesBroker);
        }

        Internal internal = internalUtils.findOne(idInternal);
        RequestType requestType = requestTypeRepository.findOne(BaseConstants.REQUEST_TYPE_VSO_DIRECT);
        SaleType saleType = saleTypeRepository.findOne(BaseConstants.SALE_TYPE_COD);

        //
        Integer number = RandomUtils.nextInt(1000000, 10000000);
        VehicleCustomerRequest vcr = new VehicleCustomerRequest();
        vcr.setRequestType(requestType);
        vcr.setIdInternal(idInternal);
        vcr.setSalesBroker(salesBroker);
        vcr.setCustomerOrder(number.toString());
        vcr = vehicleCustomerRequestRepository.save(vcr);

        Feature colorFeature = featureRepository.findByKey(BaseConstants.FEAT_TYPE_COLOR, "BL");
        Motor motor = productUtils.buildMotor("AN1","TEST","TEST", "UNT");

        for (int i=0; i<2; i++) {
            Customer cust = customerUtils.buildPersonalCustomer(internal, fakerPerson());
            String vsonumber = numberingService.findTransNumberBy(internal.getRoot().getIdInternal(), "REQ");

            SalesUnitRequirement sur = new SalesUnitRequirement();
            sur.setInternal(internal);
            sur.setRequirementType(new RequirementType(BaseConstants.REQ_TYPE_SUR_DS));
            sur.setPersonOwner(fakerPerson());
            sur.setCustomer(cust);
            sur.setColor(colorFeature);
            sur.setProduct(motor);
            sur.setBillTo(customerUtils.getBillTo(cust));
            sur.setRequirementNumber(vsonumber);

            sur.setSaleType(saleType);
            sur.setIdRequest(vcr.getIdRequest());

            //Ambil Harga Jual
            Double het = 15750000d;
            Double bbn = 1435000d;

            sur.setDownPayment(BigDecimal.valueOf(1500000));
            sur.setHetprice(BigDecimal.valueOf(het));
            sur.setUnitPrice(BigDecimal.valueOf(het * 100/110));
            sur.setBbnprice(BigDecimal.valueOf(bbn));
            sur.setSubsahm(BigDecimal.valueOf(50000));
            sur.setSubsmd(BigDecimal.valueOf(50000));
            sur.setSubsfincomp(BigDecimal.valueOf(50000));
            sur.setSubsown(BigDecimal.valueOf(50000));

            sur = salesUnitRequirementRepository.save(sur);
        }

        // Build Data
         Map<String, Object> vars = activitiProcessor.getVariables("request", vcr);
         activitiProcessor.startProcess("vehicle-customer-request", vcr.getIdRequest().toString(), vars);
    }

    public void buildVSOGP(String idInternal) {
        Internal internal = internalUtils.findOne(idInternal);
        RequestType requestType = requestTypeRepository.findOne(BaseConstants.REQUEST_TYPE_VSO_GP);
        SaleType saleType = saleTypeRepository.findOne(BaseConstants.SALE_TYPE_COD);
        OrganizationCustomer oc = customerUtils.buildOrganizationCustomer("PT. Ramunda Utama");

        //
        Integer number = RandomUtils.nextInt(1000000, 10000000);
        VehicleCustomerRequest vcr = new VehicleCustomerRequest();
        vcr.setRequestType(requestType);
        vcr.setIdInternal(idInternal);
        vcr.setCustomer(oc);
        vcr.setCustomerOrder(number.toString());
        vcr = vehicleCustomerRequestRepository.save(vcr);

        Feature colorFeature = featureRepository.findByKey(BaseConstants.FEAT_TYPE_COLOR, "BL");
        Motor motor = productUtils.buildMotor("AN1","TEST","TEST", "UNT");

        for (int i=0; i<2; i++) {
            Customer cust = customerUtils.buildPersonalCustomer(internal, fakerPerson());
            String vsonumber = numberingService.findTransNumberBy(internal.getRoot().getIdInternal(), "REQ");

            SalesUnitRequirement sur = new SalesUnitRequirement();
            sur.setInternal(internal);
            sur.setRequirementType(new RequirementType(BaseConstants.REQ_TYPE_SUR_GP));
            sur.setPersonOwner(fakerPerson());
            sur.setCustomer(oc);
            sur.setColor(colorFeature);
            sur.setProduct(motor);
            sur.setBillTo(customerUtils.getBillTo(oc));
            sur.setRequirementNumber(vsonumber);

            sur.setSaleType(saleType);
            sur.setIdRequest(vcr.getIdRequest());

            //Ambil Harga Jual
            Double het = 15750000d;
            Double bbn = 1435000d;

            sur.setDownPayment(BigDecimal.valueOf(1500000));
            sur.setHetprice(BigDecimal.valueOf(het));
            sur.setUnitPrice(BigDecimal.valueOf(het * 100/110));
            sur.setBbnprice(BigDecimal.valueOf(bbn));
            sur.setSubsahm(BigDecimal.valueOf(50000));
            sur.setSubsmd(BigDecimal.valueOf(50000));
            sur.setSubsfincomp(BigDecimal.valueOf(50000));
            sur.setSubsown(BigDecimal.valueOf(50000));

            sur = salesUnitRequirementRepository.save(sur);
        }

        // Build Data
        Map<String, Object> vars = activitiProcessor.getVariables("request", vcr);
        activitiProcessor.startProcess("vehicle-customer-request", vcr.getIdRequest().toString(), vars);
    }

    public void demandSupplyPO() {
        Internal internal = internalUtils.findOne("05801");
        Vendor vendor = vendorUtils.buildVendor("M2Z", "PT. MPM");
        OrderType orderType = orderUtils.getOrderType(BaseConstants.ORDER_TYPE_PO_UNIT, "Purchase Order Unit");

        String[] itemToSave = new String[] {"HE0", "DYX", "EK0"};

        PurchaseOrder po = new PurchaseOrder();
        po.setInternal(internal);
        po.setVendor(vendor);
        po.setBillTo(internalUtils.getBillTo(internal.getRoot()));
        po.setOrderNumber(numberingService.getInternalNumber( internal,"POD"));
        po.setDateOrder(ZonedDateTime.now());
        po.setOrderType(orderType);
        po = purchaseOrderRepository.save(po);

        for (String item: itemToSave) {
            Product product = productUtils.getProduct(item);
            OrderItem orderItem = po.getOrderItem(product, 1d, new BigDecimal(14000000));
            orderItem.idShipTo(internalUtils.getShipTo(internal).getIdShipTo());
        }
        po.setStatus(BaseConstants.STATUS_OPEN);
        po = purchaseOrderRepository.saveAndFlush(po);

    }

    public void demandSupplyDO() {
        Internal internal = internalUtils.findOne("05801");
        Vendor vendor = vendorUtils.buildVendor("M2Z", "PT. MPM");
        OrderType orderType = orderUtils.getOrderType(BaseConstants.ORDER_TYPE_PO_UNIT, "Purchase Order Unit");
        Feature color = productUtils.getFeatureByKey(BaseConstants.FEAT_TYPE_COLOR, "AC");

        String[] itemToSave = new String[] {"HE0", "DYX", "EK0"};

        // Create Billing
        BillingDisbursement billing = new BillingDisbursement();
        billing.setInternal(internal);
        billing.setVendor(vendor);
        billing.setBillTo(internalUtils.getBillTo(internal.getRoot()));
        billing.setVendorInvoice(numberingService.getInternalNumber( internal,"VENDOR-PO"));
        billing.setDateDue(ZonedDateTime.now().plusDays(10l) );
        billing = billingDisbursementRepository.save(billing);

        for (String item: itemToSave) {
            Product product = productUtils.getProduct(item);
            BillingItem billingItem = billing.addProduct(product, 1d, BigDecimal.ZERO, new BigDecimal(14000000));
            billingItem.setIdFeature(color.getIdFeature());
        }

        billing.setStatus(BaseConstants.STATUS_OPEN);
        billing = billingDisbursementRepository.saveAndFlush(billing);

        Map<String, Object> vars = activitiProcessor.getVariables("billing", billing);
        ProcessInstance inst = activitiProcessor.startProcess("unit-do", billing.getIdBilling().toString(), vars);

        processTasks("unit-do", billing.getIdBilling().toString());

        // Receipt Good
        PackageReceipt receipt = new PackageReceipt();
        receipt.setInternal(internal);
        receipt.setVendorInvoice(billing.getVendorInvoice());
        receipt.setShipFrom(vendorUtils.getShipTo(vendor));
        receipt.setDocumentNumber(billing.getVendorInvoice());
        receipt.dateCreated(ZonedDateTime.now());
        receipt = packageReceiptRepository.saveAndFlush(receipt);

        for (String item: itemToSave) {
            Product product = productUtils.getProduct(item);
            UnitShipmentReceipt shipmentReceipt = new UnitShipmentReceipt();
            shipmentReceipt.setIdFrame("FRM-" + RandomUtils.nextLong(1000000, 9999999));
            shipmentReceipt.setIdMachine("MCH-" + RandomUtils.nextLong(1000000, 9999999));
            shipmentReceipt.setShipmentPackage(receipt);
            shipmentReceipt.setYearAssembly(ZonedDateTime.now().getYear());
            shipmentReceipt.setDateReceipt(ZonedDateTime.now());
            shipmentReceipt.setIdProduct(product.getIdProduct());
            shipmentReceipt.setItemDescription(product.getName());
            shipmentReceipt.setIdFeature(color.getIdFeature());
            shipmentReceipt.setQtyAccept(1);
            shipmentReceipt.setQtyReject(0);
            shipmentReceipt = unitShipmentReceiptRepository.saveAndFlush(shipmentReceipt);
        }

        receipt = packageReceiptRepository.saveAndFlush(receipt);

        vars = activitiProcessor.getVariables("packageReceipt", receipt);
        activitiProcessor.startProcess("shipment-unit-receipt", receipt.getIdPackage().toString(), vars);
        processTasks("shipment-unit-receipt", receipt.getIdPackage().toString());

    }

    protected void processTasks(String processName, String id) {
        int i = 0;
        List<Task> tasks = new ArrayList<>();
        while (true) {
            tasks = activitiProcessor.getTasks(processName, id);
            if (tasks.isEmpty()) {
                break;
            }
            log.debug("Execute Act: " + processName + " -> " + tasks.get(0).getName());
            activitiProcessor.completeTask(tasks.get(0).getId());

            i++;
            if (i > 200) break;
        }
    }

}
