package id.atiila.service;

import id.atiila.domain.GeoBoundary;
import id.atiila.repository.GeoBoundaryRepository;
import id.atiila.repository.search.GeoBoundarySearchRepository;
import id.atiila.service.dto.GeoBoundaryDTO;
import id.atiila.service.mapper.GeoBoundaryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing GeoBoundary.
 * BeSmart Team
 */

@Service
@Transactional
public class GeoBoundaryService {

    private final Logger log = LoggerFactory.getLogger(GeoBoundaryService.class);

    private final GeoBoundaryRepository geoBoundaryRepository;

    private final GeoBoundaryMapper geoBoundaryMapper;

    private final GeoBoundarySearchRepository geoBoundarySearchRepository;

    public GeoBoundaryService(GeoBoundaryRepository geoBoundaryRepository, GeoBoundaryMapper geoBoundaryMapper, GeoBoundarySearchRepository geoBoundarySearchRepository) {
        this.geoBoundaryRepository = geoBoundaryRepository;
        this.geoBoundaryMapper = geoBoundaryMapper;
        this.geoBoundarySearchRepository = geoBoundarySearchRepository;
    }

    /**
     * Save a geoBoundary.
     *
     * @param geoBoundaryDTO the entity to save
     * @return the persisted entity
     */
    public GeoBoundaryDTO save(GeoBoundaryDTO geoBoundaryDTO) {
        log.debug("Request to save GeoBoundary : {}", geoBoundaryDTO);
        GeoBoundary geoBoundary = geoBoundaryMapper.toEntity(geoBoundaryDTO);
        geoBoundary = geoBoundaryRepository.save(geoBoundary);
        GeoBoundaryDTO result = geoBoundaryMapper.toDto(geoBoundary);
        geoBoundarySearchRepository.save(geoBoundary);
        return result;
    }

    /**
     *  Get all the geoBoundaries.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GeoBoundaryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all GeoBoundaries");
        return geoBoundaryRepository.findAll(pageable)
            .map(geoBoundaryMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<GeoBoundaryDTO> findAllByParent(UUID idparent){
        log.debug("Request to get GeoBoundary by Id Parent", idparent);

        List<GeoBoundary> geoBoundaries = geoBoundaryRepository.findAllByParent(idparent);
        List<GeoBoundaryDTO> geoBoundaryDTOList = geoBoundaryMapper.toDto(geoBoundaries);

        return geoBoundaryDTOList;
    }

    /**
     *  Get one geoBoundary by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public GeoBoundaryDTO findOne(UUID id) {
        log.debug("Request to get GeoBoundary : {}", id);
        GeoBoundary geoBoundary = geoBoundaryRepository.findOne(id);
        return geoBoundaryMapper.toDto(geoBoundary);
    }

    /**
     *  Delete the  geoBoundary by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete GeoBoundary : {}", id);
        geoBoundaryRepository.delete(id);
        geoBoundarySearchRepository.delete(id);
    }

    /**
     * Search for the geoBoundary corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GeoBoundaryDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of GeoBoundaries for query {}", query);
        Page<GeoBoundary> result = geoBoundarySearchRepository.search(queryStringQuery(query), pageable);
        return result.map(geoBoundaryMapper::toDto);
    }

    public GeoBoundaryDTO processExecuteData(Integer id, String param, GeoBoundaryDTO dto) {
        GeoBoundaryDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<GeoBoundaryDTO> processExecuteListData(Integer id, String param, Set<GeoBoundaryDTO> dto) {
        Set<GeoBoundaryDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

}
