package id.atiila.service;

import id.atiila.domain.DealerClaimType;
import id.atiila.repository.DealerClaimTypeRepository;
import id.atiila.repository.search.DealerClaimTypeSearchRepository;
import id.atiila.service.dto.DealerClaimTypeDTO;
import id.atiila.service.mapper.DealerClaimTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing DealerClaimType.
 * BeSmart Team
 */

@Service
@Transactional
public class DealerClaimTypeService {

    private final Logger log = LoggerFactory.getLogger(DealerClaimTypeService.class);

    private final DealerClaimTypeRepository dealerClaimTypeRepository;

    private final DealerClaimTypeMapper dealerClaimTypeMapper;

    private final DealerClaimTypeSearchRepository dealerClaimTypeSearchRepository;
    public DealerClaimTypeService(DealerClaimTypeRepository dealerClaimTypeRepository, DealerClaimTypeMapper dealerClaimTypeMapper, DealerClaimTypeSearchRepository dealerClaimTypeSearchRepository) {
        this.dealerClaimTypeRepository = dealerClaimTypeRepository;
        this.dealerClaimTypeMapper = dealerClaimTypeMapper;
        this.dealerClaimTypeSearchRepository = dealerClaimTypeSearchRepository;
    }

    /**
     * Save a dealerClaimType.
     *
     * @param dealerClaimTypeDTO the entity to save
     * @return the persisted entity
     */
    public DealerClaimTypeDTO save(DealerClaimTypeDTO dealerClaimTypeDTO) {
        log.debug("Request to save DealerClaimType : {}", dealerClaimTypeDTO);
        DealerClaimType dealerClaimType = dealerClaimTypeMapper.toEntity(dealerClaimTypeDTO);
        dealerClaimType = dealerClaimTypeRepository.save(dealerClaimType);
        DealerClaimTypeDTO result = dealerClaimTypeMapper.toDto(dealerClaimType);
        dealerClaimTypeSearchRepository.save(dealerClaimType);
        return result;
    }

    /**
     *  Get all the dealerClaimTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DealerClaimTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DealerClaimTypes");
        return dealerClaimTypeRepository.findAll(pageable)
            .map(dealerClaimTypeMapper::toDto);
    }

    /**
     *  Get one dealerClaimType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public DealerClaimTypeDTO findOne(Integer id) {
        log.debug("Request to get DealerClaimType : {}", id);
        DealerClaimType dealerClaimType = dealerClaimTypeRepository.findOne(id);
        return dealerClaimTypeMapper.toDto(dealerClaimType);
    }

    /**
     *  Delete the  dealerClaimType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete DealerClaimType : {}", id);
        dealerClaimTypeRepository.delete(id);
        dealerClaimTypeSearchRepository.delete(id);
    }

    /**
     * Search for the dealerClaimType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DealerClaimTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DealerClaimTypes for query {}", query);
        Page<DealerClaimType> result = dealerClaimTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(dealerClaimTypeMapper::toDto);
    }

    public DealerClaimTypeDTO processExecuteData(Integer id, String param, DealerClaimTypeDTO dto) {
        DealerClaimTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<DealerClaimTypeDTO> processExecuteListData(Integer id, String param, Set<DealerClaimTypeDTO> dto) {
        Set<DealerClaimTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
