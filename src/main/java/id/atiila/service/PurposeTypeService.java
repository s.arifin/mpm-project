package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import id.atiila.base.BaseConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.atiila.domain.PurposeType;
import id.atiila.repository.PurposeTypeRepository;
import id.atiila.repository.search.PurposeTypeSearchRepository;
import id.atiila.service.dto.PurposeTypeDTO;
import id.atiila.service.mapper.PurposeTypeMapper;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

/**
 * Service Implementation for managing PurposeType.
 * BeSmart Team
 */

@Service
@Transactional
public class PurposeTypeService {

    private final Logger log = LoggerFactory.getLogger(PurposeTypeService.class);

    private final PurposeTypeRepository purposeTypeRepository;

    private final PurposeTypeMapper purposeTypeMapper;

    private final PurposeTypeSearchRepository purposeTypeSearchRepository;
    public PurposeTypeService(PurposeTypeRepository purposeTypeRepository, PurposeTypeMapper purposeTypeMapper, PurposeTypeSearchRepository purposeTypeSearchRepository) {
        this.purposeTypeRepository = purposeTypeRepository;
        this.purposeTypeMapper = purposeTypeMapper;
        this.purposeTypeSearchRepository = purposeTypeSearchRepository;
    }

    /**
     * Save a purposeType.
     *
     * @param purposeTypeDTO the entity to save
     * @return the persisted entity
     */
    public PurposeTypeDTO save(PurposeTypeDTO purposeTypeDTO) {
        log.debug("Request to save PurposeType : {}", purposeTypeDTO);
        PurposeType purposeType = purposeTypeMapper.toEntity(purposeTypeDTO);
        purposeType = purposeTypeRepository.save(purposeType);
        PurposeTypeDTO result = purposeTypeMapper.toDto(purposeType);
        purposeTypeSearchRepository.save(purposeType);
        return result;
    }

    /**
     *  Get all the purposeTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PurposeTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PurposeTypes");
        return purposeTypeRepository.findAll(pageable)
            .map(purposeTypeMapper::toDto);
    }

    /**
     *  Get one purposeType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PurposeTypeDTO findOne(Integer id) {
        log.debug("Request to get PurposeType : {}", id);
        PurposeType purposeType = purposeTypeRepository.findOne(id);
        return purposeTypeMapper.toDto(purposeType);
    }

    /**
     *  Delete the  purposeType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete PurposeType : {}", id);
        purposeTypeRepository.delete(id);
        purposeTypeSearchRepository.delete(id);
    }

    /**
     * Search for the purposeType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PurposeTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PurposeTypes for query {}", query);
        Page<PurposeType> result = purposeTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(purposeTypeMapper::toDto);
    }

    public PurposeTypeDTO processExecuteData(Integer id, String param, PurposeTypeDTO dto) {
        PurposeTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<PurposeTypeDTO> processExecuteListData(Integer id, String param, Set<PurposeTypeDTO> dto) {
        Set<PurposeTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
        }
        return r;
    }

    @PostConstruct
    @Transactional
    public void initialize() {
        save(new PurposeTypeDTO(BaseConstants.PURP_TYPE_WAREHOUSE, "Warehouse"));
        save(new PurposeTypeDTO(BaseConstants.PURP_TYPE_MAIN_FACILITY, "Main Facility"));
        save(new PurposeTypeDTO(BaseConstants.PURP_TYPE_STORE, "Store"));
        save(new PurposeTypeDTO(BaseConstants.PURP_TYPE_WORKSHOP, "Workshop"));
        save(new PurposeTypeDTO(BaseConstants.PURP_TYPE_INIT_STOCK, "Init Stock"));
        save(new PurposeTypeDTO(BaseConstants.PURP_TYPE_INIT_CUSTOMER, "Init Customer"));
        save(new PurposeTypeDTO(BaseConstants.PURP_TYPE_PRODUCT_FEATURE, "Upload Product Feature"));
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        purposeTypeSearchRepository.deleteAll();
        List<PurposeType> purposetypes =  purposeTypeRepository.findAll();
        for (PurposeType m: purposetypes) {
            purposeTypeSearchRepository.save(m);
            log.debug("Data purpose type save !...");
        }
    }

}
