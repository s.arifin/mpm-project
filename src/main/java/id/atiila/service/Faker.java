package id.atiila.service;

import java.time.Period;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;

import id.atiila.domain.Person;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

import id.atiila.service.dto.PersonDTO;

@Component
public class Faker {

    private List<String> maleName = new ArrayList<String>();
    private List<String> femaleName = new ArrayList<String>();
    private List<String> cities = new ArrayList<String>();
    private List<String> streets = new ArrayList<String>();

    static Random rnd = new Random();

    @PostConstruct
    private void initialize() {
        maleName.addAll(Arrays.asList(new String[]
            {
                "Adang", "Abu", "Asad", "Abdul", "Budi",
                "Bima", "Budi",
                "Candra", "Candil", "Coki", "Charlie",
                "Dedi", "Doni", "Drajad", "David", "Dewi",
                "Edy", "Edward", "Erlan", "Eman", "Ersan",
                "Fachri", "Faisal",
                "Gunawan",
                "Herman",
                "Indra",
                "Jarot",
                "Karim",
                "Lukman",
                "Muhamad",
                "Norman",
                "Opick",
                "Pardi",
                "Quraisy",
                "Robin",
                "Sukardi",
                "Tendi",
                "Udin", "Umar",
                "Valy",
                "Wawan",
                "Yandri",
                "Zulkarnain"
            }));

        femaleName.addAll(Arrays.asList(new String[]
            {
                "Andri", "Ani", "Arli", "Airin", "Aira", "Aisyah", "Asma",
                "Dewi", "Denada", "Dian", "Diana", "Dara", "Datiah",
                "Emi", "Erni", "Evi", "Eka", "Erly", "Erna", "Ema",
                "Farah", "Fahira",
                "Harnung", "Harni", "Mardiah",
                "Ida", "Ima", "Iriani", "Ifa", "Indri",
                "Jeni", "Jean", "Jani",
                "Katlin", "Kiara"
            }));

        cities.addAll(Arrays.asList(new String[]
            {
                "Aceh", "Ambon", "Tebing Tinggi", "Lahat", "Pagar Alam",
                "Bali", "Buleleng", "Balik Papan",
                "Cirebon", "Cicalengka",
                "Jakarta", "Depok", "Bekasi",
                "Surabaya", "Sidoarjo",
                "Palembang", "Lubuk Pakam",
                "Rantau Prapat", "Bojo Negoro", "Kuningan", "Lebak", "Serang"
            }));

        streets.addAll(Arrays.asList(new String[]
            {   "Sudirman", "Merdeka", "Kisaran", "Melati", "Amphibi", "Ambon", "Bali",
                "Buleleng", "Balik Papan", "Cirebon", "Cicalengka", "Jakarta", "Depok",
                "Bekasi", "Surabaya", "Sidoarjo", "Bangka", "Cempaka", "Mirah", "Harum", "Margonda",
                "Cipete Utama", "Bangka Raya", "Mirah I", "Indah I", "Gemala I", "Belitung",
                "Jend. Sudirman", "Soekarno Hatta"
            }));

    };

    public String randomStreet() {
        return "Jl." + streets.get(rnd.nextInt(streets.size())) + " No " + rnd.nextInt(5000);
    }

    public String randomCity() {
        return cities.get(rnd.nextInt(cities.size()));
    }

    public String randomValue(List<String> s) {
        return s.get(rnd.nextInt(s.size()));
    }

    public boolean randomBool() {
        return rnd.nextBoolean();
    }

    public Integer randomInt(Integer bound) {
        return rnd.nextInt(bound);
    }

    public ZonedDateTime randomDob() {
        return ZonedDateTime.now().minus(Period.ofDays((rnd.nextInt(365 * 70))));
    }

    public String randomPersonalId() {
        return RandomStringUtils.randomNumeric(15);
    }

    public String randomFamilyId() {
        return RandomStringUtils.randomNumeric(10);
    }

    public String randomCellPhone() {
        long leftLimit  = 800000000000L;
        long rightLimit = 899999999999L;
        long generatedLong = leftLimit + (long) (rnd.nextDouble() * (rightLimit - leftLimit));
        return "0" + generatedLong;
    }

    public PersonDTO buildMale() {
        PersonDTO r = new PersonDTO();
        r.setFirstName(randomValue(maleName));
        r.setLastName(randomValue(maleName));
        r.setFamilyIdNumber(randomFamilyId());
        r.setPob(randomCity());
        r.setDob(randomDob());
        r.setPersonalIdNumber(randomPersonalId());
        r.setGender("P");
        r.getPostalAddress().setAddress1(randomStreet());
        r.setCellPhone1(randomCellPhone());
        return r;
    }

    public PersonDTO buildeFemale() {
        PersonDTO r = new PersonDTO();
        r.setFirstName(randomValue(femaleName));
        if (randomBool()) r.setLastName(randomValue(femaleName));
        else r.setLastName(randomValue(maleName));
        r.setFamilyIdNumber(randomFamilyId());
        r.setPob(randomValue(cities));
        r.setDob(randomDob());
        r.setPersonalIdNumber(randomPersonalId());
        r.setGender("W");
        r.getPostalAddress().setAddress1(randomStreet());
        r.setCellPhone1(randomCellPhone());
        return r;
    }

}
