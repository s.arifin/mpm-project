package id.atiila.service;

import id.atiila.domain.Internal;
import id.atiila.domain.VehiclePurchaseOrder;
import id.atiila.repository.VehiclePurchaseOrderRepository;
import id.atiila.repository.search.VehiclePurchaseOrderSearchRepository;
import id.atiila.service.dto.VehiclePurchaseOrderDTO;
import id.atiila.service.mapper.VehiclePurchaseOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing VehiclePurchaseOrder.
 * BeSmart Team
 */

@Service
@Transactional
public class VehiclePurchaseOrderService {

    private final Logger log = LoggerFactory.getLogger(VehiclePurchaseOrderService.class);

    private final VehiclePurchaseOrderRepository vehiclePurchaseOrderRepository;

    private final VehiclePurchaseOrderMapper vehiclePurchaseOrderMapper;

    private final VehiclePurchaseOrderSearchRepository vehiclePurchaseOrderSearchRepository;

    @Autowired
    private PartyUtils partyUtils;

    public VehiclePurchaseOrderService(VehiclePurchaseOrderRepository vehiclePurchaseOrderRepository, VehiclePurchaseOrderMapper vehiclePurchaseOrderMapper, VehiclePurchaseOrderSearchRepository vehiclePurchaseOrderSearchRepository) {
        this.vehiclePurchaseOrderRepository = vehiclePurchaseOrderRepository;
        this.vehiclePurchaseOrderMapper = vehiclePurchaseOrderMapper;
        this.vehiclePurchaseOrderSearchRepository = vehiclePurchaseOrderSearchRepository;
    }

    /**
     * Save a vehiclePurchaseOrder.
     *
     * @param vehiclePurchaseOrderDTO the entity to save
     * @return the persisted entity
     */
    public VehiclePurchaseOrderDTO save(VehiclePurchaseOrderDTO vehiclePurchaseOrderDTO) {
        log.debug("Request to save VehiclePurchaseOrder : {}", vehiclePurchaseOrderDTO);
        VehiclePurchaseOrder vehiclePurchaseOrder = vehiclePurchaseOrderMapper.toEntity(vehiclePurchaseOrderDTO);
        vehiclePurchaseOrder = vehiclePurchaseOrderRepository.save(vehiclePurchaseOrder);
        VehiclePurchaseOrderDTO result = vehiclePurchaseOrderMapper.toDto(vehiclePurchaseOrder);
        vehiclePurchaseOrderSearchRepository.save(vehiclePurchaseOrder);
        return result;
    }

    /**
     * Get all the vehiclePurchaseOrders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehiclePurchaseOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VehiclePurchaseOrders");
        Internal intr = partyUtils.getCurrentInternal();
        return vehiclePurchaseOrderRepository.findActive(intr, pageable)
            .map(vehiclePurchaseOrderMapper::toDto);
    }

    /**
     * Get one vehiclePurchaseOrder by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public VehiclePurchaseOrderDTO findOne(UUID id) {
        log.debug("Request to get VehiclePurchaseOrder : {}", id);
        VehiclePurchaseOrder vehiclePurchaseOrder = vehiclePurchaseOrderRepository.findOne(id);
        return vehiclePurchaseOrderMapper.toDto(vehiclePurchaseOrder);
    }

    /**
     * Delete the vehiclePurchaseOrder by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete VehiclePurchaseOrder : {}", id);
        vehiclePurchaseOrderRepository.delete(id);
        vehiclePurchaseOrderSearchRepository.delete(id);
    }

    /**
     * Search for the vehiclePurchaseOrder corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehiclePurchaseOrderDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of VehiclePurchaseOrders for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idVendor = request.getParameter("idVendor");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");

        if (filterName != null) {
        }
        Page<VehiclePurchaseOrder> result = vehiclePurchaseOrderSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(vehiclePurchaseOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<VehiclePurchaseOrderDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered VehiclePurchaseOrderDTO");
        String idVendor = request.getParameter("idVendor");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");

        return vehiclePurchaseOrderRepository.findByParams(idVendor, idInternal, idBillTo, pageable)
            .map(vehiclePurchaseOrderMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public VehiclePurchaseOrderDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        VehiclePurchaseOrderDTO r = null;
        return r;
    }

    @Transactional
    public Set<VehiclePurchaseOrderDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<VehiclePurchaseOrderDTO> r = new HashSet<>();
        return r;
    }

}
