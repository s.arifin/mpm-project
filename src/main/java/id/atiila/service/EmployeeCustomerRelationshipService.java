package id.atiila.service;

import id.atiila.domain.EmployeeCustomerRelationship;
import id.atiila.repository.EmployeeCustomerRelationshipRepository;
import id.atiila.repository.search.EmployeeCustomerRelationshipSearchRepository;
import id.atiila.service.dto.EmployeeCustomerRelationshipDTO;
import id.atiila.service.mapper.EmployeeCustomerRelationshipMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing EmployeeCustomerRelationship.
 * atiila consulting
 */

@Service
@Transactional
public class EmployeeCustomerRelationshipService {

    private final Logger log = LoggerFactory.getLogger(EmployeeCustomerRelationshipService.class);

    private final EmployeeCustomerRelationshipRepository employeeCustomerRelationshipRepository;

    private final EmployeeCustomerRelationshipMapper employeeCustomerRelationshipMapper;

    private final EmployeeCustomerRelationshipSearchRepository employeeCustomerRelationshipSearchRepository;

    public EmployeeCustomerRelationshipService(EmployeeCustomerRelationshipRepository employeeCustomerRelationshipRepository, EmployeeCustomerRelationshipMapper employeeCustomerRelationshipMapper, EmployeeCustomerRelationshipSearchRepository employeeCustomerRelationshipSearchRepository) {
        this.employeeCustomerRelationshipRepository = employeeCustomerRelationshipRepository;
        this.employeeCustomerRelationshipMapper = employeeCustomerRelationshipMapper;
        this.employeeCustomerRelationshipSearchRepository = employeeCustomerRelationshipSearchRepository;
    }

    /**
     * Save a employeeCustomerRelationship.
     *
     * @param employeeCustomerRelationshipDTO the entity to save
     * @return the persisted entity
     */
    public EmployeeCustomerRelationshipDTO save(EmployeeCustomerRelationshipDTO employeeCustomerRelationshipDTO) {
        log.debug("Request to save EmployeeCustomerRelationship : {}", employeeCustomerRelationshipDTO);
        EmployeeCustomerRelationship employeeCustomerRelationship = employeeCustomerRelationshipMapper.toEntity(employeeCustomerRelationshipDTO);
        employeeCustomerRelationship = employeeCustomerRelationshipRepository.save(employeeCustomerRelationship);
        EmployeeCustomerRelationshipDTO result = employeeCustomerRelationshipMapper.toDto(employeeCustomerRelationship);
        employeeCustomerRelationshipSearchRepository.save(employeeCustomerRelationship);
        return result;
    }

    /**
     * Get all the employeeCustomerRelationships.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<EmployeeCustomerRelationshipDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EmployeeCustomerRelationships");
        return employeeCustomerRelationshipRepository.findAll(pageable)
            .map(employeeCustomerRelationshipMapper::toDto);
    }

    /**
     * Get one employeeCustomerRelationship by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public EmployeeCustomerRelationshipDTO findOne(UUID id) {
        log.debug("Request to get EmployeeCustomerRelationship : {}", id);
        EmployeeCustomerRelationship employeeCustomerRelationship = employeeCustomerRelationshipRepository.findOne(id);
        return employeeCustomerRelationshipMapper.toDto(employeeCustomerRelationship);
    }

    /**
     * Delete the employeeCustomerRelationship by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete EmployeeCustomerRelationship : {}", id);
        employeeCustomerRelationshipRepository.delete(id);
        employeeCustomerRelationshipSearchRepository.delete(id);
    }

    /**
     * Search for the employeeCustomerRelationship corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<EmployeeCustomerRelationshipDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of EmployeeCustomerRelationships for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idStatusType = request.getParameter("idStatusType");
        String idRelationType = request.getParameter("idRelationType");
        String idEmployee = request.getParameter("idEmployee");
        String idCustomer = request.getParameter("idCustomer");

        if (idStatusType != null) {
            q.withQuery(matchQuery("statusType.idStatusType", idStatusType));
        }
        else if (idRelationType != null) {
            q.withQuery(matchQuery("relationType.idRelationType", idRelationType));
        }
        else if (idEmployee != null) {
            q.withQuery(matchQuery("employee.idEmployee", idEmployee));
        }
        else if (idCustomer != null) {
            q.withQuery(matchQuery("customer.idCustomer", idCustomer));
        }

        Page<EmployeeCustomerRelationship> result = employeeCustomerRelationshipSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(employeeCustomerRelationshipMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<EmployeeCustomerRelationshipDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered EmployeeCustomerRelationshipDTO");
        String idStatusType = request.getParameter("idStatusType");
        String idRelationType = request.getParameter("idRelationType");
        String idEmployee = request.getParameter("idEmployee");
        String idCustomer = request.getParameter("idCustomer");

        if (idStatusType != null) {
            return employeeCustomerRelationshipRepository.queryByIdStatusType(Integer.valueOf(idStatusType), pageable).map(employeeCustomerRelationshipMapper::toDto); 
        }
        else if (idRelationType != null) {
            return employeeCustomerRelationshipRepository.queryByIdRelationType(Integer.valueOf(idRelationType), pageable).map(employeeCustomerRelationshipMapper::toDto); 
        }
        else if (idEmployee != null) {
            return employeeCustomerRelationshipRepository.queryByIdEmployee(idEmployee, pageable).map(employeeCustomerRelationshipMapper::toDto); 
        }
        else if (idCustomer != null) {
            return employeeCustomerRelationshipRepository.queryByIdCustomer(idCustomer, pageable).map(employeeCustomerRelationshipMapper::toDto); 
        }

        return employeeCustomerRelationshipRepository.queryNothing(pageable).map(employeeCustomerRelationshipMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, EmployeeCustomerRelationshipDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<EmployeeCustomerRelationshipDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
