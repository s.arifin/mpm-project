package id.atiila.service;

import id.atiila.domain.InventoryItem;
import id.atiila.domain.SalesBooking;
import id.atiila.repository.SalesBookingRepository;
import id.atiila.repository.search.SalesBookingSearchRepository;
import id.atiila.service.dto.InventoryItemDTO;
import id.atiila.service.dto.SalesBookingDTO;
import id.atiila.service.mapper.SalesBookingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing SalesBooking.
 * BeSmart Team
 */

@Service
@Transactional
public class SalesBookingService {

    private final Logger log = LoggerFactory.getLogger(SalesBookingService.class);

    private final SalesBookingRepository salesBookingRepository;

    private final SalesBookingMapper salesBookingMapper;

    private final SalesBookingSearchRepository salesBookingSearchRepository;

    @Autowired
    private InventoryUtils inventoryUtils;

    public SalesBookingService(SalesBookingRepository salesBookingRepository, SalesBookingMapper salesBookingMapper, SalesBookingSearchRepository salesBookingSearchRepository) {
        this.salesBookingRepository = salesBookingRepository;
        this.salesBookingMapper = salesBookingMapper;
        this.salesBookingSearchRepository = salesBookingSearchRepository;
    }

    public Integer checkAvailability(
        String idinternal,
        String idproduct,
        Integer idfeature,
        Integer yearassembly
    ){
        Integer a = 0;

        a = inventoryUtils.checkAvailabilityBookingUnit(idinternal, idproduct, idfeature, yearassembly);

        return a;
    }

    /**
     * Save a salesBooking.
     *
     * @param salesBookingDTO the entity to save
     * @return the persisted entity
     */
    public SalesBookingDTO save(SalesBookingDTO salesBookingDTO) {
        log.debug("Request to save SalesBooking : {}", salesBookingDTO);

        if (salesBookingDTO.getDateFrom() == null){
            salesBookingDTO.setDateFrom(ZonedDateTime.now());
        }

        if (salesBookingDTO.getDateThru() == null) {
            salesBookingDTO.setDateThru(ZonedDateTime.now().plusHours(6));
        }

        SalesBooking salesBooking = salesBookingMapper.toEntity(salesBookingDTO);
        salesBooking = salesBookingRepository.save(salesBooking);
        SalesBookingDTO result = salesBookingMapper.toDto(salesBooking);
        salesBookingSearchRepository.save(salesBooking);
        return result;
    }

    /**
     * Get all the salesBookings.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesBookingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SalesBookings");
        return salesBookingRepository.findAll(pageable)
            .map(salesBookingMapper::toDto);
    }

    /**
     * Get one salesBooking by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SalesBookingDTO findOne(UUID id) {
        log.debug("Request to get SalesBooking : {}", id);
        SalesBooking salesBooking = salesBookingRepository.findOne(id);
        return salesBookingMapper.toDto(salesBooking);
    }

    /**
     * Delete the salesBooking by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete SalesBooking : {}", id);
        salesBookingRepository.delete(id);
        salesBookingSearchRepository.delete(id);
    }

    /**
     * Search for the salesBooking corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesBookingDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of SalesBookings for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idRequirement = request.getParameter("idRequirement");

        if (filterName != null) {
        }
        Page<SalesBooking> result = salesBookingSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(salesBookingMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<SalesBookingDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered SalesBookingDTO");
        String idRequirement = request.getParameter("idRequirement");

        return salesBookingRepository.findByParams(idRequirement, pageable)
            .map(salesBookingMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public SalesBookingDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        SalesBookingDTO r = null;
        return r;
    }

    @Transactional
    public Set<SalesBookingDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<SalesBookingDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public void closeSalesBookingActive(SalesBooking salesBooking) {
        List<SalesBooking> listsb  = salesBookingRepository.salesBookingSurActive(salesBooking.getRequirement().getIdRequirement());
        if (!listsb.isEmpty()) {
            SalesBooking sb = salesBookingRepository.findOne(listsb.iterator().next().getIdSalesBooking());
            sb.setDateThru(ZonedDateTime.now());
            sb = salesBookingRepository.save(sb);
        }
    }

}
