package id.atiila.service;

import id.atiila.domain.OrderRequestItem;
import id.atiila.repository.OrderRequestItemRepository;
import id.atiila.repository.search.OrderRequestItemSearchRepository;
import id.atiila.service.dto.OrderRequestItemDTO;
import id.atiila.service.mapper.OrderRequestItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing OrderRequestItem.
 * atiila consulting
 */

@Service
@Transactional
public class OrderRequestItemService {

    private final Logger log = LoggerFactory.getLogger(OrderRequestItemService.class);

    private final OrderRequestItemRepository orderRequestItemRepository;

    private final OrderRequestItemMapper orderRequestItemMapper;

    private final OrderRequestItemSearchRepository orderRequestItemSearchRepository;

    public OrderRequestItemService(OrderRequestItemRepository orderRequestItemRepository, OrderRequestItemMapper orderRequestItemMapper, OrderRequestItemSearchRepository orderRequestItemSearchRepository) {
        this.orderRequestItemRepository = orderRequestItemRepository;
        this.orderRequestItemMapper = orderRequestItemMapper;
        this.orderRequestItemSearchRepository = orderRequestItemSearchRepository;
    }

    /**
     * Save a orderRequestItem.
     *
     * @param orderRequestItemDTO the entity to save
     * @return the persisted entity
     */
    public OrderRequestItemDTO save(OrderRequestItemDTO orderRequestItemDTO) {
        log.debug("Request to save OrderRequestItem : {}", orderRequestItemDTO);
        OrderRequestItem orderRequestItem = orderRequestItemMapper.toEntity(orderRequestItemDTO);
        orderRequestItem = orderRequestItemRepository.save(orderRequestItem);
        OrderRequestItemDTO result = orderRequestItemMapper.toDto(orderRequestItem);
        orderRequestItemSearchRepository.save(orderRequestItem);
        return result;
    }

    /**
     * Get all the orderRequestItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrderRequestItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrderRequestItems");
        return orderRequestItemRepository.findAll(pageable)
            .map(orderRequestItemMapper::toDto);
    }

    /**
     * Get one orderRequestItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public OrderRequestItemDTO findOne(UUID id) {
        log.debug("Request to get OrderRequestItem : {}", id);
        OrderRequestItem orderRequestItem = orderRequestItemRepository.findOne(id);
        return orderRequestItemMapper.toDto(orderRequestItem);
    }

    /**
     * Delete the orderRequestItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete OrderRequestItem : {}", id);
        orderRequestItemRepository.delete(id);
        orderRequestItemSearchRepository.delete(id);
    }

    /**
     * Search for the orderRequestItem corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrderRequestItemDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of OrderRequestItems for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idOrderItem = request.getParameter("idOrderItem");
        String idRequestItem = request.getParameter("idRequestItem");

        if (idOrderItem != null) {
            q.withQuery(matchQuery("orderItem.idOrderItem", idOrderItem));
        }
        else if (idRequestItem != null) {
            q.withQuery(matchQuery("requestItem.idRequestItem", idRequestItem));
        }

        Page<OrderRequestItem> result = orderRequestItemSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(orderRequestItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<OrderRequestItemDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered OrderRequestItemDTO");
        String idOrderItem = request.getParameter("idOrderItem");
        String idRequestItem = request.getParameter("idRequestItem");

        if (idOrderItem != null) {
            return orderRequestItemRepository.queryByIdOrderItem(UUID.fromString(idOrderItem), pageable).map(orderRequestItemMapper::toDto); 
        }
        else if (idRequestItem != null) {
            return orderRequestItemRepository.queryByIdRequestItem(UUID.fromString(idRequestItem), pageable).map(orderRequestItemMapper::toDto); 
        }

        return orderRequestItemRepository.queryNothing(pageable).map(orderRequestItemMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, OrderRequestItemDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<OrderRequestItemDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
