package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.Approval;
import id.atiila.repository.ApprovalRepository;
import id.atiila.service.dto.UnitDocumentMessageDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@Transactional
public class ApprovalUtils {
    private final Logger log = LoggerFactory.getLogger(ApprovalUtils.class);

    @Autowired
    private ApprovalRepository approvalRepository;

    @Autowired
    private DateUtils dateUtils;

    private Approval setNewApproval(UUID currentId, Integer newStatus){
        log.debug("START findAndCreate new approval " + currentId);

        LocalDateTime now = LocalDateTime.now();
        Approval currentApproval = approvalRepository.findOne(currentId);
        if (currentApproval == null){
            throw new DmsException("Approval tidak ditemukan");
        }

        currentApproval.setDateThru(now.minusNanos(1));
        approvalRepository.save(currentApproval);

        Approval a = new Approval();
        a.setIdRequirement(currentApproval.getIdRequirement());
        a.setIdApprovalType(currentApproval.getIdApprovalType());
        a.setIdMessage(currentApproval.getIdMessage());
        a.setIdStatusType(newStatus);
        a.setDateFrom(now);
        a.setDateThru(dateUtils.getApocalypseDate());
        return approvalRepository.save(a);
    }

    public void changeStatusApprovalUnitDocumentMessage(UnitDocumentMessageDTO dto, Integer newIdStatusType){
        log.debug("START change status unit document message ===> " + dto);

        Approval a = null;
        if (!dto.getCurrentApprovalStatus().equals(newIdStatusType)){
            a = approvalRepository.findActiveApprovalByIdMessageAndType(dto.getIdMessage(), BaseConstants.SALES_MESSAGE_UNIT);
            if (a == null) throw new DmsException("Approval tidak ditemukan");
            setNewApproval(a.getIdApproval(), newIdStatusType);
        }
    }
}
