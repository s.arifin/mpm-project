package id.atiila.service;

import id.atiila.config.tenant.TenantContext;
import id.atiila.domain.*;
import id.atiila.repository.PersonalCustomerRepository;
import id.atiila.repository.search.PersonalCustomerSearchRepository;
import id.atiila.service.dto.PersonalCustomerDTO;
import id.atiila.service.mapper.PersonalCustomerMapper;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.elasticsearch.index.query.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.accessibility.Accessible;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing PersonalCustomer.
 * BeSmart Team
 */

@Service
@Transactional
public class PersonalCustomerService {

    private final Logger log = LoggerFactory.getLogger(PersonalCustomerService.class);

    public static final String QUEU_REMOVE = "removePersonalCustomer";

    public static final String QUEU_APPEND = "jms:addPersonalCustomer";

    private final PersonalCustomerRepository personalCustomerRepository;

    private final PersonalCustomerMapper personalCustomerMapper;

    private final PersonalCustomerSearchRepository personalCustomerSearchRepository;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private CustomerUtils customerUtils;

    @Autowired
    private JmsTemplate template;

    @Autowired
    private ActivitiProcessor processor;

    @Autowired
    private MasterNumberingService numbering;

    @Autowired
    private CommonUtils commonUtils;

    @Autowired
    private InternalUtils internalUtils;

    public PersonalCustomerService(PersonalCustomerRepository personalCustomerRepository, PersonalCustomerMapper personalCustomerMapper, PersonalCustomerSearchRepository personalCustomerSearchRepository) {
        this.personalCustomerRepository = personalCustomerRepository;
        this.personalCustomerMapper = personalCustomerMapper;
        this.personalCustomerSearchRepository = personalCustomerSearchRepository;
    }

    /**
     * Save a personalCustomer.
     *
     * @param personalCustomerDTO the entity to save
     * @return the persisted entity
     */
    public PersonalCustomerDTO save(PersonalCustomerDTO personalCustomerDTO) {
        log.debug("Request to save PersonalCustomer : {}", personalCustomerDTO);
        Boolean isNew = personalCustomerDTO.getIdCustomer() == null;
        PersonalCustomer personalCustomer = personalCustomerMapper.toEntity(personalCustomerDTO);
        if (isNew) {
            personalCustomer = customerUtils.buildPersonalCustomer(partyUtils.getCurrentInternal(), personalCustomer);

//            String idCustomer = personalCustomer.getIdCustomer();
//            Person p = partyUtils.findPerson(personalCustomer.getPerson());
//            if (idCustomer == null) idCustomer = numbering.nextCustomerValue();
//
//            if (p == null) {
//                personalCustomerDTO.setIdCustomer(idCustomer);
//                processor.startProcess("processCustomer", idCustomer, processor.getVariables("customer", personalCustomerDTO));
//                personalCustomer = personalCustomerRepository.findByIdMPM(idCustomer);
//            } else {
//                PersonalCustomer _personalCustomer = personalCustomerRepository.findByParty(p);
//                if (_personalCustomer != null) personalCustomer = _personalCustomer;
//                personalCustomer.setIdCustomer(idCustomer);
//            }
        }
        personalCustomer = personalCustomerRepository.save(personalCustomer);
        PersonalCustomerDTO result = personalCustomerMapper.toDto(personalCustomer);
        personalCustomerSearchRepository.save(personalCustomer);
        return result;
    }

    /**
     * Get all the personalCustomers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PersonalCustomerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PersonalCustomers");
        return personalCustomerRepository.findAll(pageable)
            .map(personalCustomerMapper::toDto);
    }

    /**
     * Get one personalCustomer by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PersonalCustomerDTO findOne(String id) {
        log.debug("Request to get PersonalCustomer : {}", id);
        PersonalCustomer personalCustomer = personalCustomerRepository.findOne(id);
        return personalCustomerMapper.toDto(personalCustomer);
    }

    @Transactional(readOnly = true)
    public PersonalCustomerDTO findOnebypartyid(UUID idParty) {
        log.debug("Request to get PersonalCustomer : {}", idParty);
        PersonalCustomer personalCustomer = personalCustomerRepository.findByParty(idParty);
        return personalCustomerMapper.toDto(personalCustomer);
    }

    /**
     * Delete the personalCustomer by id.
     *
     * @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete PersonalCustomer : {}", id);
        personalCustomerRepository.delete(id);
        personalCustomerSearchRepository.delete(id);
    }

    /**
     * Search for the personalCustomer corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PersonalCustomerDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PersonalCustomers for query {}", query);

        QueryBuilder boolQueryBuilder = queryStringQuery(query + "*")
                                            .queryName("person.firstName")
                                            .queryName("person.lastName");

        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder();
        q.withQuery(boolQuery().must(boolQueryBuilder));
        Page<PersonalCustomer> result = personalCustomerSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(personalCustomerMapper::toDto);
    }

    @Transactional(readOnly = true)
    public PersonalCustomerDTO processExecuteData(Integer id, String param, PersonalCustomerDTO dto) {
        PersonalCustomerDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional(readOnly = true)
    public Set<PersonalCustomerDTO> processExecuteListData(Integer id, String param, Set<PersonalCustomerDTO> dto) {
        Set<PersonalCustomerDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public void removeWrongData() {
        Set<PersonalCustomer> spc = personalCustomerRepository.queryWrongData();
        for (PersonalCustomer o: spc) {
            template.convertAndSend(PersonalCustomerService.QUEU_REMOVE, o.getIdCustomer());
            template.convertAndSend(BillToService.QUEU_REMOVE, o.getIdCustomer());
            template.convertAndSend(ShipToService.QUEU_REMOVE, o.getIdCustomer());
            log.info("Queu Remove : " + o.toString());
        }
    }

    @JmsListener(destination = QUEU_REMOVE)
    public void removePersonalCustomer(String id) {
        personalCustomerRepository.delete(id);
        log.info("Remove Personal Customer : " + id);
    }

    public void addPersonalCustomer(@Body PersonalCustomerDTO dto, @Header("tenant") String tenant, @Header("internal") String internal) {
        Map<String, Object> variables = processor.getVariables("customer", dto);
        variables.put("tenant", tenant);

        // Handling multi tenant process
        TenantContext.setCurrentTenant(tenant);
        try {
            Internal intr = internalUtils.findOne(internal);
            PersonalCustomer pc = personalCustomerMapper.toEntity(dto);
            customerUtils.buildPersonalCustomer(intr, pc);
        } finally {
            TenantContext.clear();
        }
    }

    @PostConstruct
    public void initElasticSearchData() {
        if (commonUtils.getCurrentProfile("ricky")){
            log.debug("START INIT PERSONAL CUSTOMER for ELASTIC");
            Pageable pageable = new PageRequest(0,50);
            Page<PersonalCustomer> page = personalCustomerRepository.findAll(pageable);
            List<PersonalCustomer> list = page.getContent();
            if (list.size() > 0) {
                for(PersonalCustomer p : list) {
                    personalCustomerSearchRepository.save(p);
                }
            } else {
                log.debug("Personal Customer not found!!");
            }
            log.debug("Success Init Data Personal Customer for Elastic");
        } else {
            log.debug("INIT PERSONAL CUSTOMER for ELASTIC DENIED!!!");
        }
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional(readOnly = true)
    public PersonalCustomerDTO findOneEmail(HttpServletRequest request) {
        String idCust = request.getParameter("idCust");
        log.debug("Request to get PersonalCustomer : {}", request);
        PersonalCustomer personalCustomer = personalCustomerRepository.findByIdCustomer(idCust);
        return personalCustomerMapper.toDto(personalCustomer);
    }

    @Async
    public void buildIndex() {
        personalCustomerSearchRepository.deleteAll();
        List<PersonalCustomer>  personalCustomers =   personalCustomerRepository.findAll();
        for (PersonalCustomer m:  personalCustomers) {
            personalCustomerSearchRepository.save(m);
            log.debug("Data personalCustomer save !...");
        }
    }
}
