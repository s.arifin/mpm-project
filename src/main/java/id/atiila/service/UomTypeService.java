package id.atiila.service;

import id.atiila.domain.UomType;
import id.atiila.repository.UomTypeRepository;
import id.atiila.repository.search.UomTypeSearchRepository;
import id.atiila.service.dto.UomTypeDTO;
import id.atiila.service.mapper.UomTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing UomType.
 * BeSmart Team
 */

@Service
@Transactional
public class UomTypeService {

    private final Logger log = LoggerFactory.getLogger(UomTypeService.class);

    private final UomTypeRepository uomTypeRepository;

    private final UomTypeMapper uomTypeMapper;

    private final UomTypeSearchRepository uomTypeSearchRepository;

    public UomTypeService(UomTypeRepository uomTypeRepository, UomTypeMapper uomTypeMapper, UomTypeSearchRepository uomTypeSearchRepository) {
        this.uomTypeRepository = uomTypeRepository;
        this.uomTypeMapper = uomTypeMapper;
        this.uomTypeSearchRepository = uomTypeSearchRepository;
    }

    /**
     * Save a uomType.
     *
     * @param uomTypeDTO the entity to save
     * @return the persisted entity
     */
    public UomTypeDTO save(UomTypeDTO uomTypeDTO) {
        log.debug("Request to save UomType : {}", uomTypeDTO);
        UomType uomType = uomTypeMapper.toEntity(uomTypeDTO);
        uomType = uomTypeRepository.save(uomType);
        UomTypeDTO result = uomTypeMapper.toDto(uomType);
        uomTypeSearchRepository.save(uomType);
        return result;
    }

    /**
     * Get all the uomTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UomTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UomTypes");
        return uomTypeRepository.findAll(pageable)
            .map(uomTypeMapper::toDto);
    }

    /**
     * Get one uomType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public UomTypeDTO findOne(Integer id) {
        log.debug("Request to get UomType : {}", id);
        UomType uomType = uomTypeRepository.findOne(id);
        return uomTypeMapper.toDto(uomType);
    }

    /**
     * Delete the uomType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete UomType : {}", id);
        uomTypeRepository.delete(id);
        uomTypeSearchRepository.delete(id);
    }

    /**
     * Search for the uomType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UomTypeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of UomTypes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));


        Page<UomType> result = uomTypeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(uomTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<UomTypeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered UomTypeDTO");


        return uomTypeRepository.queryNothing(pageable).map(uomTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, UomTypeDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<UomTypeDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        uomTypeSearchRepository.deleteAll();
        List<UomType> uomtypes = uomTypeRepository.findAll();
        for (UomType m :  uomtypes) {
            uomTypeSearchRepository.save(m);
            log.debug("Data uom type save!...");
        }
    }

}
