package id.atiila.service;

import id.atiila.domain.ShipmentItem;
import id.atiila.repository.ShipmentItemRepository;
import id.atiila.repository.search.ShipmentItemSearchRepository;
import id.atiila.service.dto.CustomShipmentItemDTO;
import id.atiila.service.dto.ShipmentItemDTO;
import id.atiila.service.mapper.ShipmentItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;
//import  java.util.HashMap;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ShipmentItem.
 * BeSmart Team
 */

@Service
@Transactional
public class ShipmentItemService {

    private final Logger log = LoggerFactory.getLogger(ShipmentItemService.class);

    private final ShipmentItemRepository shipmentItemRepository;

    private final ShipmentItemMapper shipmentItemMapper;

    private final ShipmentItemSearchRepository shipmentItemSearchRepository;

    public ShipmentItemService(ShipmentItemRepository shipmentItemRepository, ShipmentItemMapper shipmentItemMapper, ShipmentItemSearchRepository shipmentItemSearchRepository) {
        this.shipmentItemRepository = shipmentItemRepository;
        this.shipmentItemMapper = shipmentItemMapper;
        this.shipmentItemSearchRepository = shipmentItemSearchRepository;
    }

    /**
     * Save a shipmentItem.
     *
     * @param shipmentItemDTO the entity to save
     * @return the persisted entity
     */
    public ShipmentItemDTO save(ShipmentItemDTO shipmentItemDTO) {
        log.debug("Request to save ShipmentItem : {}", shipmentItemDTO);
        ShipmentItem shipmentItem = shipmentItemMapper.toEntity(shipmentItemDTO);
        shipmentItem = shipmentItemRepository.save(shipmentItem);
        ShipmentItemDTO result = shipmentItemMapper.toDto(shipmentItem);
        shipmentItemSearchRepository.save(shipmentItem);
        return result;
    }

    /**
     * Get all the shipmentItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShipmentItems");
        return shipmentItemRepository.findAll(pageable)
            .map(shipmentItemMapper::toDto);
    }

    /**
     * Get one shipmentItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ShipmentItemDTO findOne(UUID id) {
        log.debug("Request to get ShipmentItem : {}", id);
        ShipmentItem shipmentItem = shipmentItemRepository.findOne(id);
        return shipmentItemMapper.toDto(shipmentItem);
    }

    /**
     * Delete the shipmentItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ShipmentItem : {}", id);
        shipmentItemRepository.delete(id);
        shipmentItemSearchRepository.delete(id);
    }

    /**
     * Search for the shipmentItem corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentItemDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of ShipmentItems for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idShipment = request.getParameter("idShipment");

        if (filterName != null) {
        }
        Page<ShipmentItem> result = shipmentItemSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(shipmentItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ShipmentItemDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ShipmentItemDTO");
        String idShipment = request.getParameter("idShipment");

        if (idShipment != null) {
            return shipmentItemRepository.findByParams(UUID.fromString(idShipment), pageable)
                .map(shipmentItemMapper::toDto);
        }
        return shipmentItemRepository.findAll(pageable).map(shipmentItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<CustomShipmentItemDTO> findAllCustomBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ShipmentItemDTO");
        String idShipment = request.getParameter("idShipment");
        return shipmentItemRepository.findCustomByIdShipment(UUID.fromString(idShipment), pageable);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public ShipmentItemDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        ShipmentItemDTO r = null;
        return r;
    }

    @Transactional
    public Set<ShipmentItemDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<ShipmentItemDTO> r = new HashSet<>();
        return r;
    }

    @Async
    public void buildIndex() {
        shipmentItemSearchRepository.deleteAll();
            List<ShipmentItem> shipmentItems = shipmentItemRepository.findAll();
            for (ShipmentItem m: shipmentItems) {
                shipmentItemSearchRepository.save(m);
                log.debug("Data shipment item save!...");
            }
        }
}

