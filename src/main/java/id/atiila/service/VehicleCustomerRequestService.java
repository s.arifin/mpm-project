package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.MasterNumbering;
import id.atiila.domain.VehicleCustomerRequest;
import id.atiila.domain.VehicleSalesOrder;
import id.atiila.repository.VehicleCustomerRequestRepository;
import id.atiila.repository.search.VehicleCustomerRequestSearchRepository;
import id.atiila.service.dto.VehicleCustomerRequestDTO;
import id.atiila.service.mapper.VehicleCustomerRequestMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing VehicleCustomerRequest.
 * atiila consulting
 */

@Service
@Transactional
public class VehicleCustomerRequestService {

    private final Logger log = LoggerFactory.getLogger(VehicleCustomerRequestService.class);

    private final VehicleCustomerRequestRepository vehicleCustomerRequestRepository;

    private final VehicleCustomerRequestMapper vehicleCustomerRequestMapper;

    private final VehicleCustomerRequestSearchRepository vehicleCustomerRequestSearchRepository;

    @Autowired
    private MasterNumberingService numbering;

    @Autowired
    private RequestUtil requestUtil;

    @Autowired
    private ActivitiProcessor activitiProcessor;

    public VehicleCustomerRequestService(VehicleCustomerRequestRepository vehicleCustomerRequestRepository, VehicleCustomerRequestMapper vehicleCustomerRequestMapper, VehicleCustomerRequestSearchRepository vehicleCustomerRequestSearchRepository) {
        this.vehicleCustomerRequestRepository = vehicleCustomerRequestRepository;
        this.vehicleCustomerRequestMapper = vehicleCustomerRequestMapper;
        this.vehicleCustomerRequestSearchRepository = vehicleCustomerRequestSearchRepository;
    }

    /**
     * Save a vehicleCustomerRequest.
     *
     * @param vehicleCustomerRequestDTO the entity to save
     * @return the persisted entity
     */
    public VehicleCustomerRequestDTO save(VehicleCustomerRequestDTO vehicleCustomerRequestDTO) {
        log.debug("Request to save VehicleCustomerRequest : {}", vehicleCustomerRequestDTO);
        VehicleCustomerRequest vehicleCustomerRequest = vehicleCustomerRequestMapper.toEntity(vehicleCustomerRequestDTO);
        if (vehicleCustomerRequest.getRequestNumber() == null) {
            vehicleCustomerRequest.setRequestNumber(numbering.getInternalNumber("REQ"));
        }
        vehicleCustomerRequest = vehicleCustomerRequestRepository.save(vehicleCustomerRequest);
        VehicleCustomerRequestDTO result = vehicleCustomerRequestMapper.toDto(vehicleCustomerRequest);
        vehicleCustomerRequestSearchRepository.save(vehicleCustomerRequest);
        return result;
    }

    /**
     * Get all the vehicleCustomerRequests.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleCustomerRequestDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VehicleCustomerRequests");
        return vehicleCustomerRequestRepository.findActiveVehicleCustomerRequest(pageable)
            .map(vehicleCustomerRequestMapper::toDto);
    }

    /**
     * Get one vehicleCustomerRequest by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public VehicleCustomerRequestDTO findOne(UUID id) {
        log.debug("Request to get VehicleCustomerRequest : {}", id);
        VehicleCustomerRequest vehicleCustomerRequest = vehicleCustomerRequestRepository.findOne(id);
        return vehicleCustomerRequestMapper.toDto(vehicleCustomerRequest);
    }

    /**
     * Delete the vehicleCustomerRequest by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete VehicleCustomerRequest : {}", id);
        vehicleCustomerRequestRepository.delete(id);
        vehicleCustomerRequestSearchRepository.delete(id);
    }

    /**
     * Search for the vehicleCustomerRequest corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleCustomerRequestDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of VehicleCustomerRequests for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idInternal = request.getParameter("idInternal");
        String idCustomer = request.getParameter("idCustomer");
        String idRequestType = request.getParameter("idRequestType");

        if (idInternal != null) {
            q.withQuery(matchQuery("internal.idInternal", idInternal));
        }
        else if (idCustomer != null) {
            q.withQuery(matchQuery("customer.idCustomer", idCustomer));
        }
        else if (idRequestType != null) {
            q.withQuery(matchQuery("requestType.idRequestType", idRequestType));
        }

        Page<VehicleCustomerRequest> result = vehicleCustomerRequestSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(vehicleCustomerRequestMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<VehicleCustomerRequestDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered VehicleCustomerRequestDTO");

        String idInternal = request.getParameter("idInternal");
        String idCustomer = request.getParameter("idCustomer");
        String idRequestType = request.getParameter("idRequestType");

        if (idInternal != null) {
            return vehicleCustomerRequestRepository.queryByIdInternal(idInternal, pageable).map(vehicleCustomerRequestMapper::toDto);
        }
        else if (idCustomer != null) {
            return vehicleCustomerRequestRepository.queryByIdCustomer(idCustomer, pageable).map(vehicleCustomerRequestMapper::toDto);
        }
        else if (idRequestType != null) {
            return vehicleCustomerRequestRepository.queryByIdRequestType(Integer.valueOf(idRequestType), pageable).map(vehicleCustomerRequestMapper::toDto);
        }

        return vehicleCustomerRequestRepository.queryNothing(pageable).map(vehicleCustomerRequestMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        String idRequest = (String) item.get("idRequest");
        String action = (String) request.getParameter("action");

        if ( "completed".equalsIgnoreCase(action) && idRequest != null) {
            VehicleCustomerRequest vcr = vehicleCustomerRequestRepository.findOne(UUID.fromString(idRequest));
            if (vcr != null) {
                Map<String, Object> vars = activitiProcessor.getVariables("request", vcr);
                activitiProcessor.startProcess("vehicle-customer-request", vcr.getIdRequest().toString(), vars);
            }
        }
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, VehicleCustomerRequestDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<VehicleCustomerRequestDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public VehicleCustomerRequestDTO changeVehicleCustomerRequestStatus(VehicleCustomerRequestDTO dto, Integer id) {
        if (dto != null) {
			VehicleCustomerRequest e = vehicleCustomerRequestRepository.findOne(dto.getIdRequest());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case BaseConstants.STATUS_OPEN:
                        requestUtil.buildVSO(e);
                        break;
                    case BaseConstants.STATUS_CANCEL:
                        vehicleCustomerRequestSearchRepository.delete(dto.getIdRequest());
                    case BaseConstants.STATUS_COMPLETED:
                        vehicleCustomerRequestSearchRepository.delete(dto.getIdRequest());
                        break;
                    default:
                        vehicleCustomerRequestSearchRepository.save(e);
                }
				e = vehicleCustomerRequestRepository.save(e);
                return vehicleCustomerRequestMapper.toDto(e);
			}
		}
        return dto;
    }
}
