package id.atiila.service;

import id.atiila.domain.ProductType;
import id.atiila.repository.ProductTypeRepository;
import id.atiila.repository.search.ProductTypeSearchRepository;
import id.atiila.service.dto.ProductTypeDTO;
import id.atiila.service.mapper.ProductTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ProductType.
 * BeSmart Team
 */

@Service
@Transactional
public class ProductTypeService {

    private final Logger log = LoggerFactory.getLogger(ProductTypeService.class);

    private final ProductTypeRepository productTypeRepository;

    private final ProductTypeMapper productTypeMapper;

    private final ProductTypeSearchRepository productTypeSearchRepository;

    public ProductTypeService(ProductTypeRepository productTypeRepository, ProductTypeMapper productTypeMapper, ProductTypeSearchRepository productTypeSearchRepository) {
        this.productTypeRepository = productTypeRepository;
        this.productTypeMapper = productTypeMapper;
        this.productTypeSearchRepository = productTypeSearchRepository;
    }

    /**
     * Save a productType.
     *
     * @param productTypeDTO the entity to save
     * @return the persisted entity
     */
    public ProductTypeDTO save(ProductTypeDTO productTypeDTO) {
        log.debug("Request to save ProductType : {}", productTypeDTO);
        ProductType productType = productTypeMapper.toEntity(productTypeDTO);
        productType = productTypeRepository.save(productType);
        ProductTypeDTO result = productTypeMapper.toDto(productType);
        productTypeSearchRepository.save(productType);
        return result;
    }

    /**
     *  Get all the productTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductTypes");
        return productTypeRepository.findAll(pageable)
            .map(productTypeMapper::toDto);
    }

    /**
     *  Get one productType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ProductTypeDTO findOne(Integer id) {
        log.debug("Request to get ProductType : {}", id);
        ProductType productType = productTypeRepository.findOne(id);
        return productTypeMapper.toDto(productType);
    }

    /**
     *  Delete the  productType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete ProductType : {}", id);
        productTypeRepository.delete(id);
        productTypeSearchRepository.delete(id);
    }

    /**
     * Search for the productType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ProductTypes for query {}", query);
        Page<ProductType> result = productTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(productTypeMapper::toDto);
    }

    // @PostConstruct
    public void initialize() {
        if (productTypeRepository.findAll().isEmpty()) {
            save(new ProductTypeDTO(10, "Good"));
            save(new ProductTypeDTO(11, "Service"));
            save(new ProductTypeDTO(20, "Financial Product"));
            save(new ProductTypeDTO(21, "Motor"));
            save(new ProductTypeDTO(22, "Rem Part"));
        }
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        productTypeSearchRepository.deleteAll();
        List<ProductType> productTypes =  productTypeRepository.findAll();
        for (ProductType m: productTypes) {
            productTypeSearchRepository.save(m);
            log.debug("Data productType save !...");
        }
    }



}
