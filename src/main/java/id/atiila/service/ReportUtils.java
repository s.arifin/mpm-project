package id.atiila.service;

import id.atiila.config.JasperConfiguration;
import id.atiila.service.util.CustomTextUtils;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.export.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class ReportUtils {

    @Autowired
    private JRFileVirtualizer fv;

    @Autowired
    private JRSwapFileVirtualizer sfv;

    @Autowired
    Environment environment;

    @Autowired
    private DataSource datasource;

    public static String getRandomFileName() {
        return System.getProperty("java.io.tmpdir") + "/" + UUID.randomUUID().toString();
    }

    public static JasperReport loadRreport(String name) throws JRException {
        String compileName = System.getProperty("java.io.tmpdir") + "/" + name + ".jasper";
        JasperReport jr = null;

        File f = new File(compileName);
        if (f.exists()) {
            jr = (JasperReport) JRLoader.loadObject(f);
        } else {
            InputStream reportStream = JasperConfiguration.class.getResourceAsStream("/public/report/" + name + ".jrxml");
            jr = JasperCompileManager.compileReport(reportStream);
            JRSaver.saveObject(jr, compileName);
        }
        return jr;
    }

    public Map<String, Object> getReportParam() {
        Map<String, Object> r = new HashMap<>();
        String port = environment.getProperty("server.port");
        String addr = InetAddress.getLoopbackAddress().getHostName();

        r.put(JRParameter.REPORT_VIRTUALIZER, sfv);
        r.put("reportUrl", "/public/report/");
        r.put("imageUrl", "http://" + addr + ":" + port + "/images/");
        return r;
    }

    public Map<String, Object> getReportParam(HttpServletRequest request) {
        Map<String, Object> r = new HashMap<>();
        String scheme = request.getScheme();
        int port = request.getServerPort();
        String addr = request.getServerName();

        r.put(JRParameter.REPORT_VIRTUALIZER, sfv);
        r.put("reportUrl", "/public/report/");
        r.put("imageUrl", scheme + "://" + addr + ":" + port + "/images/");
        return r;
    }

    public ResponseEntity<InputStreamResource> generatePDF(String reportName, String name, Map<String, Object> params) {
        String tmpFileName = ReportUtils.getRandomFileName() + ".pdf";
        FileInputStream fis = null;
        Connection conn = null;
        try {
            conn = datasource.getConnection();
            JasperPrint p = JasperFillManager.fillReport(ReportUtils.loadRreport(reportName), params, conn);

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("atiila");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");

            SimpleOutputStreamExporterOutput output = new SimpleOutputStreamExporterOutput(tmpFileName);

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(new SimpleExporterInput(p));
            exporter.setExporterOutput(output);
            exporter.setConfiguration(reportConfig);
            exporter.setConfiguration(exportConfig);
            exporter.exportReport();

            fis = new FileInputStream(tmpFileName);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.setContentType(MediaType.valueOf("application/pdf"));
            responseHeaders.setContentDispositionFormData("inline", name);
            responseHeaders.setContentLength(fis.available());
            return new ResponseEntity<InputStreamResource>(new InputStreamResource(fis), responseHeaders, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sfv.cleanup();
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return null;
    }

    public ResponseEntity<InputStreamResource> generatePDFCustom(String reportName, String name, Map<String, Object> params) {
        String tmpFileName = ReportUtils.getRandomFileName() + ".pdf";
        FileInputStream fis = null;
        Connection conn = null;

        //Send A Keyword Before Do Customize Parameter, Get IT and Remove IT
        String key = params.get("keyword").toString();
        params.remove("keyword");

        params = customizeParameter(params, key);

        try {
            conn = datasource.getConnection();
            JasperPrint p = JasperFillManager.fillReport(ReportUtils.loadRreport(reportName), params, conn);

            SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("atiila");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");

            SimpleOutputStreamExporterOutput output = new SimpleOutputStreamExporterOutput(tmpFileName);

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(new SimpleExporterInput(p));
            exporter.setExporterOutput(output);
            exporter.setConfiguration(reportConfig);
            exporter.setConfiguration(exportConfig);
            exporter.exportReport();

            fis = new FileInputStream(tmpFileName);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.setContentType(MediaType.valueOf("application/pdf"));
            responseHeaders.setContentDispositionFormData("inline", name);
            responseHeaders.setContentLength(fis.available());
            return new ResponseEntity<InputStreamResource>(new InputStreamResource(fis), responseHeaders, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sfv.cleanup();
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return null;
    }


    public ResponseEntity<InputStreamResource> generateXLSX(String reportName, String name, Map<String, Object> params) {
        String tmpFileName = ReportUtils.getRandomFileName() + ".xlsx";
        FileInputStream fis = null;
        Connection conn = null;
        try {
            conn = datasource.getConnection();
            JasperPrint p = JasperFillManager.fillReport(ReportUtils.loadRreport(reportName), params, conn);

            SimpleXlsxReportConfiguration reportConfig = new SimpleXlsxReportConfiguration();
            reportConfig.setSheetNames(new String[] { name });

            SimpleOutputStreamExporterOutput output = new SimpleOutputStreamExporterOutput(tmpFileName);

            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(p));
            exporter.setExporterOutput(output);
            exporter.setConfiguration(reportConfig);
            exporter.exportReport();

            fis = new FileInputStream(tmpFileName);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.setContentType(MediaType.valueOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
            responseHeaders.setContentDispositionFormData("attachment", name);
            responseHeaders.setContentLength(fis.available());
            return new ResponseEntity<InputStreamResource>(new InputStreamResource(fis), responseHeaders, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sfv.cleanup();
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return null;
    }


    public ResponseEntity<InputStreamResource> generateCSV(String reportName, String name, Map<String, Object> params) {
        String tmpFileName = ReportUtils.getRandomFileName() + ".csv";
        FileInputStream fis = null;
        Connection conn = null;
        try {
            conn = datasource.getConnection();
            JasperPrint p = JasperFillManager.fillReport(ReportUtils.loadRreport(reportName), params, conn);

            SimpleWriterExporterOutput output = new SimpleWriterExporterOutput(tmpFileName);

            JRCsvExporter exporter = new JRCsvExporter();
            exporter.setExporterInput(new SimpleExporterInput(p));
            exporter.setExporterOutput(output);
            exporter.exportReport();

            fis = new FileInputStream(tmpFileName);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.setContentType(MediaType.valueOf("text/csv"));
            responseHeaders.setContentDispositionFormData("file", name);
            responseHeaders.setContentLength(fis.available());
            return new ResponseEntity<InputStreamResource>(new InputStreamResource(fis), responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sfv.cleanup();
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return null;
    }

    public ResponseEntity<InputStreamResource> generateHTML(String reportName, String name, Map<String, Object> params) {
        String tmpFileName = ReportUtils.getRandomFileName() + ".html";
        FileInputStream fis = null;
        Connection conn = null;
        try {
            conn = datasource.getConnection();
            JasperPrint p = JasperFillManager.fillReport(ReportUtils.loadRreport(reportName), params, conn);

            SimpleHtmlExporterOutput output = new SimpleHtmlExporterOutput(tmpFileName);

            HtmlExporter exporter = new HtmlExporter();
            exporter.setExporterInput(new SimpleExporterInput(p));
            exporter.setExporterOutput(output);
            exporter.exportReport();

            fis = new FileInputStream(tmpFileName);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.setContentType(MediaType.valueOf("text/csv"));
            responseHeaders.setContentDispositionFormData("file", name);
            responseHeaders.setContentLength(fis.available());
            return new ResponseEntity<InputStreamResource>(new InputStreamResource(fis), responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sfv.cleanup();
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return null;
    }

    private Map<String, Object> customizeParameter(Map<String, Object> params, String key){
        String id = params.get(key).toString();
        params.remove(key);

        String convertedId = CustomTextUtils.convertUUIDToMSGUIDFormat(id).toUpperCase();
        params.put(key, convertedId);
        return params;
    }
}
