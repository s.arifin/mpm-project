package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.OrderType;
import id.atiila.repository.OrderTypeRepository;
import id.atiila.repository.search.OrderTypeSearchRepository;
import id.atiila.service.dto.OrderTypeDTO;
import id.atiila.service.mapper.OrderTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing OrderType.
 * BeSmart Team
 */

@Service
@Transactional
public class OrderTypeService {

    private final Logger log = LoggerFactory.getLogger(OrderTypeService.class);

    private final OrderTypeRepository orderTypeRepository;

    private final OrderTypeMapper orderTypeMapper;

    private final OrderTypeSearchRepository orderTypeSearchRepository;

    public OrderTypeService(OrderTypeRepository orderTypeRepository, OrderTypeMapper orderTypeMapper, OrderTypeSearchRepository orderTypeSearchRepository) {
        this.orderTypeRepository = orderTypeRepository;
        this.orderTypeMapper = orderTypeMapper;
        this.orderTypeSearchRepository = orderTypeSearchRepository;
    }

    /**
     * Save a orderType.
     *
     * @param orderTypeDTO the entity to save
     * @return the persisted entity
     */
    public OrderTypeDTO save(OrderTypeDTO orderTypeDTO) {
        log.debug("Request to save OrderType : {}", orderTypeDTO);
        OrderType orderType = orderTypeMapper.toEntity(orderTypeDTO);
        orderType = orderTypeRepository.save(orderType);
        OrderTypeDTO result = orderTypeMapper.toDto(orderType);
        orderTypeSearchRepository.save(orderType);
        return result;
    }

    /**
     *  Get all the orderTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrderTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrderTypes");
        return orderTypeRepository.findAll(pageable)
            .map(orderTypeMapper::toDto);
    }

    /**
     *  Get one orderType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public OrderTypeDTO findOne(Integer id) {
        log.debug("Request to get OrderType : {}", id);
        OrderType orderType = orderTypeRepository.findOne(id);
        return orderTypeMapper.toDto(orderType);
    }

    /**
     *  Delete the  orderType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete OrderType : {}", id);
        orderTypeRepository.delete(id);
        orderTypeSearchRepository.delete(id);
    }

    /**
     * Search for the orderType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrderTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of OrderTypes for query {}", query);
        Page<OrderType> result = orderTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(orderTypeMapper::toDto);
    }

    @PostConstruct
    public void initialize() {
        save(new OrderTypeDTO(10, "Regular Purchase Order"));
        save(new OrderTypeDTO(11, "Regular Sales Order"));

        save(new OrderTypeDTO(BaseConstants.ORDER_TYPE_VSO, "Retail VSO"));
        save(new OrderTypeDTO(BaseConstants.ORDER_TYPE_GC, "GC VSO"));
        save(new OrderTypeDTO(BaseConstants.ORDER_TYPE_GP, "GP VSO"));
        save(new OrderTypeDTO(BaseConstants.ORDER_TYPE_INIT, "Init Stock"));
        save(new OrderTypeDTO(BaseConstants.ORDER_TYPE_INTERNAL, "Internal Order"));
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        orderTypeSearchRepository.deleteAll();
        List<OrderType> orderTypes =  orderTypeRepository.findAll();
        for (OrderType m: orderTypes) {
            orderTypeSearchRepository.save(m);
            log.debug("Data orderType save !...");
        }
    }
}
