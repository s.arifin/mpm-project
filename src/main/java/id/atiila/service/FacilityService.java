package id.atiila.service;

import id.atiila.domain.Facility;
import id.atiila.repository.FacilityRepository;
import id.atiila.repository.search.FacilitySearchRepository;
import id.atiila.service.dto.FacilityDTO;
import id.atiila.service.mapper.FacilityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing Facility.
 * BeSmart Team
 */

@Service
@Transactional
public class FacilityService {

    private final Logger log = LoggerFactory.getLogger(FacilityService.class);

    private final FacilityRepository facilityRepository;

    private final FacilityMapper facilityMapper;

    private final FacilitySearchRepository facilitySearchRepository;

    public FacilityService(FacilityRepository facilityRepository, FacilityMapper facilityMapper, FacilitySearchRepository facilitySearchRepository) {
        this.facilityRepository = facilityRepository;
        this.facilityMapper = facilityMapper;
        this.facilitySearchRepository = facilitySearchRepository;
    }

    /**
     * Save a facility.
     *
     * @param facilityDTO the entity to save
     * @return the persisted entity
     */
    public FacilityDTO save(FacilityDTO facilityDTO) {
        log.debug("Request to save Facility : {}", facilityDTO);
        Facility facility = facilityMapper.toEntity(facilityDTO);
        facility = facilityRepository.save(facility);
        FacilityDTO result = facilityMapper.toDto(facility);
        facilitySearchRepository.save(facility);
        return result;
    }

    /**
     * Get all the facilities.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FacilityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Facilities");
        return facilityRepository.findActiveFacility(pageable)
            .map(facilityMapper::toDto);
    }

    /**
     * Get one facility by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public FacilityDTO findOne(UUID id) {
        log.debug("Request to get Facility : {}", id);
        Facility facility = facilityRepository.findOne(id);
        return facilityMapper.toDto(facility);
    }

    /**
     * Delete the facility by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Facility : {}", id);
        facilityRepository.delete(id);
        facilitySearchRepository.delete(id);
    }

    /**
     * Search for the facility corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FacilityDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of Facilities for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idFacilityType = request.getParameter("idFacilityType");
        String idPartOf = request.getParameter("idPartOf");

        if (idFacilityType != null) {
            q.withQuery(matchQuery("facilityType.idFacilityType", idFacilityType));
        }
        else if (idPartOf != null) {
            q.withQuery(matchQuery("partOf.idFacility", idPartOf));
        }

        Page<Facility> result = facilitySearchRepository.search(q.build().getQuery(), pageable);
        return result.map(facilityMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<FacilityDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered FacilityDTO");
        String idFacilityType = request.getParameter("idFacilityType");
        String idPartOf = request.getParameter("idPartOf");

        if (idFacilityType != null) {
            return facilityRepository.queryByIdFacilityType(Integer.valueOf(idFacilityType), pageable).map(facilityMapper::toDto);
        }
        else if (idPartOf != null) {
            return facilityRepository.queryByIdPartOf(UUID.fromString(idPartOf), pageable).map(facilityMapper::toDto);
        }

        return facilityRepository.queryNothing(pageable).map(facilityMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, FacilityDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<FacilityDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public FacilityDTO changeFacilityStatus(FacilityDTO dto, Integer id) {
        if (dto != null) {
			Facility e = facilityRepository.findOne(dto.getIdFacility());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        facilitySearchRepository.delete(dto.getIdFacility());
                        break;
                    default:
                        facilitySearchRepository.save(e);
                }
				facilityRepository.save(e);
			}
		}
        return dto;
    }

    @Transactional(readOnly = true)
    public List<FacilityDTO> getFacilityByFacilityType(Integer id){
        List<Facility> list = facilityRepository.findAllByidFacilityType(id);
        List<FacilityDTO> l_dtos = facilityMapper.toDto(list);
        return l_dtos;
    }

    @Transactional
    public void initialize(){

    }

    @Async
    public void buildIndex() {
        facilitySearchRepository.deleteAll();
        List<Facility> facilitys =  facilityRepository.findAll();
        for (Facility f: facilitys) {
            facilitySearchRepository.save(f);
            log.debug("Data Facility save !...");
        }
    }
}
