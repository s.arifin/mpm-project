package id.atiila.service;

import id.atiila.domain.CommunicationEventDelivery;
import id.atiila.repository.CommunicationEventDeliveryRepository;
import id.atiila.repository.search.CommunicationEventDeliverySearchRepository;
import id.atiila.service.dto.CommunicationEventDeliveryDTO;
import id.atiila.service.mapper.CommunicationEventDeliveryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing CommunicationEventDelivery.
 */
@Service
@Transactional
public class CommunicationEventDeliveryService {

    private final Logger log = LoggerFactory.getLogger(CommunicationEventDeliveryService.class);

    private final CommunicationEventDeliveryRepository communicationEventDeliveryRepository;

    private final CommunicationEventDeliveryMapper communicationEventDeliveryMapper;

    private final CommunicationEventDeliverySearchRepository communicationEventDeliverySearchRepository;

    public CommunicationEventDeliveryService(CommunicationEventDeliveryRepository communicationEventDeliveryRepository, CommunicationEventDeliveryMapper communicationEventDeliveryMapper, CommunicationEventDeliverySearchRepository communicationEventDeliverySearchRepository) {
        this.communicationEventDeliveryRepository = communicationEventDeliveryRepository;
        this.communicationEventDeliveryMapper = communicationEventDeliveryMapper;
        this.communicationEventDeliverySearchRepository = communicationEventDeliverySearchRepository;
    }

    /**
     * Save a communicationEventDelivery.
     *
     * @param communicationEventDeliveryDTO the entity to save
     * @return the persisted entity
     */
    public CommunicationEventDeliveryDTO save(CommunicationEventDeliveryDTO communicationEventDeliveryDTO) {
        log.debug("Request to save CommunicationEventDelivery : {}", communicationEventDeliveryDTO);
        CommunicationEventDelivery communicationEventDelivery = communicationEventDeliveryMapper.toEntity(communicationEventDeliveryDTO);
        communicationEventDelivery = communicationEventDeliveryRepository.save(communicationEventDelivery);
        CommunicationEventDeliveryDTO result = communicationEventDeliveryMapper.toDto(communicationEventDelivery);
        communicationEventDeliverySearchRepository.save(communicationEventDelivery);
        return result;
    }

    /**
     *  Get all the communicationEventDeliveries.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CommunicationEventDeliveryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CommunicationEventDeliveries");
        return communicationEventDeliveryRepository.findAll(pageable)
            .map(communicationEventDeliveryMapper::toDto);
    }

    /**
     *  Get one communicationEventDelivery by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CommunicationEventDeliveryDTO findOne(UUID id) {
        log.debug("Request to get CommunicationEventDelivery : {}", id);
        CommunicationEventDelivery communicationEventDelivery = communicationEventDeliveryRepository.findOne(id);
        return communicationEventDeliveryMapper.toDto(communicationEventDelivery);
    }

    /**
     *  Delete the  communicationEventDelivery by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete CommunicationEventDelivery : {}", id);
        communicationEventDeliveryRepository.delete(id);
        communicationEventDeliverySearchRepository.delete(id);
    }

    /**
     * Search for the communicationEventDelivery corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CommunicationEventDeliveryDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CommunicationEventDeliveries for query {}", query);
        Page<CommunicationEventDelivery> result = communicationEventDeliverySearchRepository.search(queryStringQuery(query), pageable);
        return result.map(communicationEventDeliveryMapper::toDto);
    }
}
