package id.atiila.service;

import id.atiila.domain.LeasingCompany;
import id.atiila.domain.LeasingTenorProvide;
import id.atiila.domain.Organization;
import id.atiila.domain.PartyRole;
import id.atiila.repository.LeasingCompanyRepository;
import id.atiila.repository.LeasingTenorProvideRepository;
import id.atiila.repository.OrganizationRepository;
import id.atiila.repository.PartyRoleRepository;
import id.atiila.repository.search.LeasingTenorProvideSearchRepository;
import id.atiila.service.dto.LeasingTenorProvideDTO;
import id.atiila.service.mapper.LeasingTenorProvideMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing LeasingTenorProvide.
 * BeSmart Team
 */

@Service
@Transactional
public class LeasingTenorProvideService {

    private final Logger log = LoggerFactory.getLogger(LeasingTenorProvideService.class);

    private final LeasingTenorProvideRepository leasingTenorProvideRepository;

    private final LeasingTenorProvideMapper leasingTenorProvideMapper;

    private final LeasingTenorProvideSearchRepository leasingTenorProvideSearchRepository;

    @Autowired
    private LeasingCompanyRepository leasingCompanyRepository;

    @Autowired
    private PartyRoleRepository partyRoleRepository;

    @Autowired
    private OrganizationRepository organizationRepository;

    public LeasingTenorProvideService(LeasingTenorProvideRepository leasingTenorProvideRepository, LeasingTenorProvideMapper leasingTenorProvideMapper, LeasingTenorProvideSearchRepository leasingTenorProvideSearchRepository) {
        this.leasingTenorProvideRepository = leasingTenorProvideRepository;
        this.leasingTenorProvideMapper = leasingTenorProvideMapper;
        this.leasingTenorProvideSearchRepository = leasingTenorProvideSearchRepository;
    }

    /**
     * Save a leasingTenorProvide.
     *
     * @param leasingTenorProvideDTO the entity to save
     * @return the persisted entity
     */
    public LeasingTenorProvideDTO save(LeasingTenorProvideDTO leasingTenorProvideDTO) {
        log.debug("Request to save LeasingTenorProvide : {}", leasingTenorProvideDTO);
        LeasingTenorProvide leasingTenorProvide = leasingTenorProvideMapper.toEntity(leasingTenorProvideDTO);
        leasingTenorProvide = leasingTenorProvideRepository.save(leasingTenorProvide);
        LeasingTenorProvideDTO result = leasingTenorProvideMapper.toDto(leasingTenorProvide);
        leasingTenorProvideSearchRepository.save(leasingTenorProvide);
        return result;
    }

    /**
     *  Get all the leasingTenorProvides.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LeasingTenorProvideDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LeasingTenorProvides");
        return leasingTenorProvideRepository.findAll(pageable)
            .map(leasingTenorProvideMapper::toDto);
    }

    /**
     *  Get one leasingTenorProvide by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public LeasingTenorProvideDTO findOne(UUID id) {
        log.debug("Request to get LeasingTenorProvide : {}", id);
        LeasingTenorProvide leasingTenorProvide = leasingTenorProvideRepository.findOne(id);
        return leasingTenorProvideMapper.toDto(leasingTenorProvide);
    }

    /**
     *  Delete the  leasingTenorProvide by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete LeasingTenorProvide : {}", id);
        leasingTenorProvideRepository.delete(id);
        leasingTenorProvideSearchRepository.delete(id);
    }

    /**
     * Search for the leasingTenorProvide corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LeasingTenorProvideDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of LeasingTenorProvides for query {}", query);
        Page<LeasingTenorProvide> result = leasingTenorProvideSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(leasingTenorProvideMapper::toDto);
    }

    public LeasingTenorProvideDTO processExecuteData(Integer id, String param, LeasingTenorProvideDTO dto) {
        LeasingTenorProvideDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public List<LeasingTenorProvideDTO> getActiveTenorByIdProductAndLeasing(String idproduct, String idleasing){
        log.debug("Request to get active tenor by id product and id leasing");

        UUID castString = UUID.fromString(idleasing);
        List<LeasingTenorProvide> leasingTenorProvides = leasingTenorProvideRepository.findActiveTenorByIdProduct(idproduct, castString);
        List<LeasingTenorProvideDTO> leasingTenorProvideDTOS = new ArrayList<LeasingTenorProvideDTO>();

        for(LeasingTenorProvide item : leasingTenorProvides){
            leasingTenorProvideDTOS.add(leasingTenorProvideMapper.toDto(item));
        }

        return leasingTenorProvideDTOS;
    }

    public Set<LeasingTenorProvideDTO> processExecuteListData(Integer id, String param, Set<LeasingTenorProvideDTO> dto) {
        Set<LeasingTenorProvideDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

}
