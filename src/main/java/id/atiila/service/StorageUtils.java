package id.atiila.service;

import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.ContainerRepository;
import id.atiila.repository.ContainerTypeRepository;
import id.atiila.repository.GoodContainerRepository;
import id.atiila.service.dto.GoodContainerDTO;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.Size;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

@Service
public class StorageUtils {

    public static int RAK = 10;
    public static int PALET = 11;
    public static int BIN_BOX = 12;
    public static int ROOM = 13;

    @Autowired
    private ContainerRepository containerRepository;

    @Autowired
    private ContainerTypeRepository containerTypeRepository;

    @Autowired
    private GoodContainerRepository goodContainerRepository;

    @Transactional
    public Container buildContainer(Facility facl, Integer idType, String idContainer) {
        Container r = containerRepository.findOneByContainerCode(idContainer);
        Log.debug("ini buildContainer" + r);
        if (r == null) {
            r = new Container();
            r.setFacility(facl);
            r.setContainerCode(idContainer);
            r.setContainerType(containerTypeRepository.findOne(idType));
            r = containerRepository.save(r);
        }
        return r;
    }

    @Transactional
    public GoodContainer buildGoodContainer(Organization organization, Good good, Container container) {
        List<GoodContainer> listGoodContainer = goodContainerRepository.getContainer(organization, good);
        GoodContainer r = null ;

        if (listGoodContainer.isEmpty()) {
            r = new GoodContainer();
            Log.debug("new good ==" + r);
            r.setContainer(container);
            Log.debug("new good 1==" + r);
            r.setOrganization(organization);
            Log.debug("new good 2==" + r);
            r.setGood(good);
            Log.debug("new good 3==" + r);
            r.setDateFrom(ZonedDateTime.now());
            Log.debug("new good 4==" + r);
            r.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 999, ZoneId.systemDefault()));
            Log.debug("new good 5==" + r);
            r = goodContainerRepository.save(r);
            Log.debug("new good 6==" + r);
        } else r = listGoodContainer.get(0);
        return r;
    }

    @Transactional
    public GoodContainer getGoodContainer(Organization organization, Good good) {
        List<GoodContainer> ListGoodContainer = goodContainerRepository.getContainer(organization, good);
        GoodContainer r = ListGoodContainer.get(0);
        return r;
    }

    public GoodContainer getGoodContainer(Organization organization, String idProduct) {
        List<GoodContainer> containers = goodContainerRepository.getContainer(organization, idProduct);
        GoodContainer r = containers.isEmpty() ? null : containers.get(0);
        return r;
    }

}
