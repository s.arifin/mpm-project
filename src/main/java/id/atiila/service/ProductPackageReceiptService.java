package id.atiila.service;

import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.ProductPackageReceiptSearchRepository;
import id.atiila.service.dto.ProductPackageReceiptDTO;
import id.atiila.service.mapper.ProductPackageReceiptMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ProductPackageReceipt.
 * BeSmart Team
 */

@Service
@Transactional
public class ProductPackageReceiptService {

    private final Logger log = LoggerFactory.getLogger(ProductPackageReceiptService.class);

    private final ProductPackageReceiptRepository productPackageReceiptRepository;

    private final ProductPackageReceiptMapper productPackageReceiptMapper;

    private final ProductPackageReceiptSearchRepository productPackageReceiptSearchRepository;

    @Autowired
    private PartyUtils partyUtils;

    public ProductPackageReceiptService(ProductPackageReceiptRepository productPackageReceiptRepository, ProductPackageReceiptMapper productPackageReceiptMapper, ProductPackageReceiptSearchRepository productPackageReceiptSearchRepository) {
        this.productPackageReceiptRepository = productPackageReceiptRepository;
        this.productPackageReceiptMapper = productPackageReceiptMapper;
        this.productPackageReceiptSearchRepository = productPackageReceiptSearchRepository;
    }

    /**
     * Save a productPackageReceipt.
     *
     * @param productPackageReceiptDTO the entity to save
     * @return the persisted entity
     */
    public ProductPackageReceiptDTO save(ProductPackageReceiptDTO productPackageReceiptDTO) {
        log.debug("Request to save ProductPackageReceipt : {}", productPackageReceiptDTO);
        ProductPackageReceipt productPackageReceipt = productPackageReceiptMapper.toEntity(productPackageReceiptDTO);
        productPackageReceipt = productPackageReceiptRepository.save(productPackageReceipt);
        ProductPackageReceiptDTO result = productPackageReceiptMapper.toDto(productPackageReceipt);
        productPackageReceiptSearchRepository.save(productPackageReceipt);
        return result;
    }

    /**
     * Get all the productPackageReceipts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductPackageReceiptDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductPackageReceipts");
        Internal intr = partyUtils.getCurrentInternal();
        return productPackageReceiptRepository.findActiveProductPackageReceipt(intr, pageable)
            .map(productPackageReceiptMapper::toDto);
    }

    /**
     * Get one productPackageReceipt by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ProductPackageReceiptDTO findOne(UUID id) {
        log.debug("Request to get ProductPackageReceipt : {}", id);
        ProductPackageReceipt productPackageReceipt = productPackageReceiptRepository.findOne(id);
        return productPackageReceiptMapper.toDto(productPackageReceipt);
    }

    /**
     * Delete the productPackageReceipt by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ProductPackageReceipt : {}", id);
        productPackageReceiptRepository.delete(id);
        productPackageReceiptSearchRepository.delete(id);
    }

    /**
     * Search for the productPackageReceipt corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductPackageReceiptDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of ProductPackageReceipts for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idInternal = request.getParameter("idInternal");
        String idShipFrom = request.getParameter("idShipFrom");

        if (filterName != null) {
        }
        Page<ProductPackageReceipt> result = productPackageReceiptSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(productPackageReceiptMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ProductPackageReceiptDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ProductPackageReceiptDTO");
        String idInternal = request.getParameter("idInternal");
        String idShipFrom = request.getParameter("idShipFrom");

        if (idInternal != null && idShipFrom != null) {
            return productPackageReceiptRepository.findByParams(idInternal, idShipFrom, pageable)
                .map(productPackageReceiptMapper::toDto);
        }
        return productPackageReceiptRepository.queryNothing(pageable).map(productPackageReceiptMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public ProductPackageReceiptDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        ProductPackageReceiptDTO r = null;
        return r;
    }

    @Transactional
    public Set<ProductPackageReceiptDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<ProductPackageReceiptDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public ProductPackageReceiptDTO changeProductPackageReceiptStatus(ProductPackageReceiptDTO dto, Integer id) {
        if (dto != null) {
			ProductPackageReceipt e = productPackageReceiptRepository.findOne(dto.getIdPackage());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        productPackageReceiptSearchRepository.delete(dto.getIdPackage());
                        break;
                    case 17:
                        buildShipmentandBilling(e);
                        break;
                    default:
                        productPackageReceiptSearchRepository.save(e);
                }
				productPackageReceiptRepository.save(e);
			}
		}
        return dto;
    }

    @Autowired
    private ProductShipmentIncomingRepository productShipmentIncomingRepository;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private VendorUtils vendorUtils;

    @Autowired
    private OrderBillingItemRepository orderBillingItemRepository;

    @Autowired
    private OrderShipmentItemRepository orderShipmentItemRepository;

    @Autowired
    private BillingDisbursementRepository billingDisbursementRepository;

    @Autowired
    private ShipmentItemRepository shipmentItemRepository;

    @Autowired
    private ProductShipmentReceiptRepository productShipmentReceiptRepository;

    @Autowired
    private BillingItemRepository billingItemRepository;

    @Autowired
    private InventoryItemRepository inventoryItemRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildShipmentandBilling(ProductPackageReceipt e) {
        ProductShipmentIncoming p = null;
        BillingDisbursement b = null;

        for (ShipmentReceipt o: e.getShipmentReceipts()) {
            ProductShipmentReceipt d = (ProductShipmentReceipt) o;

            // Buat Shipment Header
            if (p == null) {
                p = new ProductShipmentIncoming();
                p.setShipFrom(e.getShipFrom());
                p.setShipTo(internalUtils.getShipTo(e.getInternal()));
                p.setShipmentNumber(e.getDocumentNumber());
                p.setAddressFrom(p.getShipFrom().getParty().getPostalAddress());
                p.setAddressTo(p.getShipTo().getParty().getPostalAddress());
                productShipmentIncomingRepository.save(p);
            }

            // Buat Shipment Item
            if (o.getShipmentItem() == null) {
                ShipmentItem si = new ShipmentItem();
                si.setQty(o.getQtyAccept() - o.getQtyReject());
                si.setIdProduct(o.getIdProduct());
                si.setItemDescription(o.getItemDescription());
                p.addDetail(si);
                si = shipmentItemRepository.save(si);
                d.setShipmentItem(si);
            }

            // Buat Inventory Item
            InventoryItem item = new InventoryItem();
            item.dateCreated(ZonedDateTime.now());
            item.setIdFeature(o.getIdFeature());
            item.setIdProduct(o.getIdProduct());
            item.setQty(o.getQtyAccept() - o.getQtyReject());
            item.setQtyBooking(0d);
            item.setIdOwner(p.getShipTo().getParty().getIdParty());
            inventoryItemRepository.save(item);

            productShipmentReceiptRepository.save(d);


        }


        if (p != null) {
            productShipmentIncomingRepository.save(p);
        }

        for (ShipmentReceipt o: e.getShipmentReceipts()) {
            if (b == null) {
                b = new BillingDisbursement();
                if (o.getOrderItem().getOrders() instanceof VendorOrder) {
                    VendorOrder vo = (VendorOrder) o.getOrderItem().getOrders();
                    b.setBillTo(vo.getBillTo());
                    b.setVendor(vo.getVendor());
                    b.setInternal(vo.getInternal());
                    b.setBillFrom(vendorUtils.getBillTo(vo.getVendor()));
                }
                if (o.getOrderItem().getOrders() instanceof CustomerOrder) {
                    CustomerOrder co = (CustomerOrder) o.getOrderItem().getOrders();
                    b.setBillTo(co.getBillTo());
                    b.setInternal(co.getInternal());
                }
                billingDisbursementRepository.save(b);
            }

            // Buat Billing Item
            BillingItem bi = new BillingItem();
            bi.setIdProduct(o.getIdProduct());
            bi.setQty(o.getQtyAccept() - o.getQtyReject());
            bi.setItemDescription(o.getItemDescription());
            bi.setUnitPrice(o.getOrderItem().getUnitPrice());
            bi.setDiscount(o.getOrderItem().getDiscount());
            b.addDetail(bi);
            bi = billingItemRepository.save(bi);

            OrderBillingItem obi = new OrderBillingItem();
            obi.setBillingItem(bi);
            obi.setOrderItem(o.getOrderItem());
            obi.setQty(bi.getQty());
            obi.setAmount(bi.getTotalAmount());
            orderBillingItemRepository.save(obi);

            OrderShipmentItem osi = new OrderShipmentItem();
            osi.setShipmentItem(o.getShipmentItem());
            osi.setOrderItem(o.getOrderItem());
            osi.setQty(o.getShipmentItem().getQty());
            orderShipmentItemRepository.save(osi);
        }

        if (b != null) {
            billingDisbursementRepository.save(b);
        }
    }
}
