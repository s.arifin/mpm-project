package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.RequestType;
import id.atiila.repository.RequestTypeRepository;
import id.atiila.repository.search.RequestTypeSearchRepository;
import id.atiila.service.dto.RequestTypeDTO;
import id.atiila.service.mapper.RequestTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing RequestType.
 * BeSmart Team
 */

@Service
@Transactional
public class RequestTypeService {

    private final Logger log = LoggerFactory.getLogger(RequestTypeService.class);

    private final RequestTypeRepository requestTypeRepository;

    private final RequestTypeMapper requestTypeMapper;

    private final RequestTypeSearchRepository requestTypeSearchRepository;

    public RequestTypeService(RequestTypeRepository requestTypeRepository, RequestTypeMapper requestTypeMapper, RequestTypeSearchRepository requestTypeSearchRepository) {
        this.requestTypeRepository = requestTypeRepository;
        this.requestTypeMapper = requestTypeMapper;
        this.requestTypeSearchRepository = requestTypeSearchRepository;
    }

    /**
     * Save a requestType.
     *
     * @param requestTypeDTO the entity to save
     * @return the persisted entity
     */
    public RequestTypeDTO save(RequestTypeDTO requestTypeDTO) {
        log.debug("Request to save RequestType : {}", requestTypeDTO);
        RequestType requestType = requestTypeMapper.toEntity(requestTypeDTO);
        requestType = requestTypeRepository.save(requestType);
        RequestTypeDTO result = requestTypeMapper.toDto(requestType);
        requestTypeSearchRepository.save(requestType);
        return result;
    }

    /**
     * Get all the requestTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequestTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RequestTypes");
        return requestTypeRepository.findAll(pageable)
            .map(requestTypeMapper::toDto);
    }

    /**
     * Get one requestType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public RequestTypeDTO findOne(Integer id) {
        log.debug("Request to get RequestType : {}", id);
        RequestType requestType = requestTypeRepository.findOne(id);
        return requestTypeMapper.toDto(requestType);
    }

    /**
     * Delete the requestType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete RequestType : {}", id);
        requestTypeRepository.delete(id);
        requestTypeSearchRepository.delete(id);
    }

    /**
     * Search for the requestType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequestTypeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of RequestTypes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));

        if (filterName != null) {
        }
        Page<RequestType> result = requestTypeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(requestTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<RequestTypeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered RequestTypeDTO");

        return requestTypeRepository.findByParams(pageable)
            .map(requestTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public RequestTypeDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        RequestTypeDTO r = null;
        return r;
    }

    @Transactional
    public Set<RequestTypeDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<RequestTypeDTO> r = new HashSet<>();
        return r;
    }

    public void initilizze() {
        save(new RequestTypeDTO(BaseConstants.REQUEST_TYPE_VSO_DIRECT, "VSO Direct"));
        save(new RequestTypeDTO(BaseConstants.REQUEST_TYPE_VSO_GC, "VSO Group Customer"));
        save(new RequestTypeDTO(BaseConstants.REQUEST_TYPE_VSO_GP, "VSO Group Person"));
    }

}
