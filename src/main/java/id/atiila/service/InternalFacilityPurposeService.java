package id.atiila.service;

import id.atiila.domain.InternalFacilityPurpose;
import id.atiila.repository.InternalFacilityPurposeRepository;
import id.atiila.repository.search.InternalFacilityPurposeSearchRepository;
import id.atiila.service.dto.InternalFacilityPurposeDTO;
import id.atiila.service.mapper.InternalFacilityPurposeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing InternalFacilityPurpose.
 * atiila consulting
 */

@Service
@Transactional
public class InternalFacilityPurposeService {

    private final Logger log = LoggerFactory.getLogger(InternalFacilityPurposeService.class);

    private final InternalFacilityPurposeRepository internalFacilityPurposeRepository;

    private final InternalFacilityPurposeMapper internalFacilityPurposeMapper;

    private final InternalFacilityPurposeSearchRepository internalFacilityPurposeSearchRepository;

    public InternalFacilityPurposeService(InternalFacilityPurposeRepository internalFacilityPurposeRepository, InternalFacilityPurposeMapper internalFacilityPurposeMapper, InternalFacilityPurposeSearchRepository internalFacilityPurposeSearchRepository) {
        this.internalFacilityPurposeRepository = internalFacilityPurposeRepository;
        this.internalFacilityPurposeMapper = internalFacilityPurposeMapper;
        this.internalFacilityPurposeSearchRepository = internalFacilityPurposeSearchRepository;
    }

    /**
     * Save a internalFacilityPurpose.
     *
     * @param internalFacilityPurposeDTO the entity to save
     * @return the persisted entity
     */
    public InternalFacilityPurposeDTO save(InternalFacilityPurposeDTO internalFacilityPurposeDTO) {
        log.debug("Request to save InternalFacilityPurpose : {}", internalFacilityPurposeDTO);
        InternalFacilityPurpose internalFacilityPurpose = internalFacilityPurposeMapper.toEntity(internalFacilityPurposeDTO);
        internalFacilityPurpose = internalFacilityPurposeRepository.save(internalFacilityPurpose);
        InternalFacilityPurposeDTO result = internalFacilityPurposeMapper.toDto(internalFacilityPurpose);
        internalFacilityPurposeSearchRepository.save(internalFacilityPurpose);
        return result;
    }

    /**
     * Get all the internalFacilityPurposes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InternalFacilityPurposeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all InternalFacilityPurposes");
        return internalFacilityPurposeRepository.findAll(pageable)
            .map(internalFacilityPurposeMapper::toDto);
    }

    /**
     * Get one internalFacilityPurpose by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public InternalFacilityPurposeDTO findOne(UUID id) {
        log.debug("Request to get InternalFacilityPurpose : {}", id);
        InternalFacilityPurpose internalFacilityPurpose = internalFacilityPurposeRepository.findOne(id);
        return internalFacilityPurposeMapper.toDto(internalFacilityPurpose);
    }

    /**
     * Delete the internalFacilityPurpose by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete InternalFacilityPurpose : {}", id);
        internalFacilityPurposeRepository.delete(id);
        internalFacilityPurposeSearchRepository.delete(id);
    }

    /**
     * Search for the internalFacilityPurpose corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InternalFacilityPurposeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of InternalFacilityPurposes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idInternal = request.getParameter("idInternal");
        String idFacility = request.getParameter("idFacility");
        String idPurposeType = request.getParameter("idPurposeType");

        if (idInternal != null) {
            q.withQuery(matchQuery("internal.idInternal", idInternal));
        }
        else if (idFacility != null) {
            q.withQuery(matchQuery("facility.idFacility", idFacility));
        }
        else if (idPurposeType != null) {
            q.withQuery(matchQuery("purposeType.idPurposeType", idPurposeType));
        }

        Page<InternalFacilityPurpose> result = internalFacilityPurposeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(internalFacilityPurposeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<InternalFacilityPurposeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered InternalFacilityPurposeDTO");
        String idInternal = request.getParameter("idInternal");
        String idFacility = request.getParameter("idFacility");
        String idPurposeType = request.getParameter("idPurposeType");

        if (idInternal != null) {
            return internalFacilityPurposeRepository.queryByIdInternal(idInternal, pageable).map(internalFacilityPurposeMapper::toDto); 
        }
        else if (idFacility != null) {
            return internalFacilityPurposeRepository.queryByIdFacility(UUID.fromString(idFacility), pageable).map(internalFacilityPurposeMapper::toDto); 
        }
        else if (idPurposeType != null) {
            return internalFacilityPurposeRepository.queryByIdPurposeType(Integer.valueOf(idPurposeType), pageable).map(internalFacilityPurposeMapper::toDto); 
        }

        return internalFacilityPurposeRepository.queryNothing(pageable).map(internalFacilityPurposeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, InternalFacilityPurposeDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<InternalFacilityPurposeDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
