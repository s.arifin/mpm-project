package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.*;
import id.atiila.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class OrderUtils {

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private ProductUtils productUtils;

    @Autowired
    private InventoryUtils inventoryUtils;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private OrderTypeRepository orderTypeRepository;

    @Autowired
    private InternalOrderRepository internalOrderRepository;

    @Autowired
    private RequirementOrderItemRepository requirementOrderItemRepository;

    @Autowired
    private OrderShipmentItemRepository orderShipmentItemRepository;

    @Autowired
    private OrderBillingItemRepository orderBillingItemRepository;

    @Autowired
    private PickingSlipRepository pickingSlipRepository;

    @Autowired
    private VehicleSalesOrderRepository vehicleSalesOrderRepository;

    @Autowired
    private MasterNumberingService numbering;

    @Autowired
    private CustomerUtils customerUtils;

    @Autowired
    private InternalBankMapingRepository internalBankMapingRepository;

    @Autowired
    private InternalRepository internalRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    public List<UnitPreparation> buildFrom(OrderItem oi) {
        List<UnitPreparation> spis = new ArrayList<>();
        Product product = productUtils.getProduct(oi.getIdProduct());

        if (oi.getOrders() instanceof CustomerOrder) {
            CustomerOrder co = (CustomerOrder) oi.getOrders();

            if (product instanceof Motor) {
                List<InventoryItem> items = inventoryUtils.getItems(co.getInternal().getOrganization().getIdParty(), oi.getIdProduct(), oi.getIdFeature());
                spis = inventoryUtils.getUnitPreparation(oi, items);
            }
        }
        return spis;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<UnitPreparation> buildDO(OrderItem oi) {
        List<UnitPreparation> spis = new ArrayList<>();
        Product product = productUtils.getProduct(oi.getIdProduct());
        if (oi.getOrders() instanceof CustomerOrder) {
            CustomerOrder co = (CustomerOrder) oi.getOrders();
            List<InventoryItem> items = inventoryUtils.getItems(co.getInternal().getOrganization().getIdParty(), oi.getIdProduct(), oi.getIdFeature());
            if (items.size() > 0) {
                spis = inventoryUtils.getUnitPreparation(oi, items);
                inventoryUtils.buildDO(co.getInternal(), spis);
            }
        }

        return spis;
    }

    @Transactional
    public OrderType getOrderType(Integer id, String description) {
        OrderType orderType = orderTypeRepository.findOne(id);
        if (orderType == null) {
            orderType = new OrderType();
            orderType.setIdOrderType(id);
            orderType.setDescription(description);
            orderType = orderTypeRepository.save(orderType);
        }
        return orderType;
    }

    @Transactional
    public OrderType getOrderType(Integer id) {
        return getOrderType(id, "Unknown");
    }

    public OrderItem save(OrderItem orderItem) {
        return orderItemRepository.save(orderItem);
    }

    public InternalOrder save(InternalOrder order) {
        return internalOrderRepository.save(order);
    }

    // Force get By Order Item
    public RequirementOrderItem getRequirementOrderItemByOrder(SalesUnitRequirement req, OrderItem o, Integer qty) {
        RequirementOrderItem r = requirementOrderItemRepository.findRequirementOrderItemByOrder(o);
        if (r == null) {
            r = new RequirementOrderItem();
            r.setOrderItem(o);
            r.setRequirement(req);
            r.setQty(qty);
            r = requirementOrderItemRepository.saveAndFlush(r);
        } else {
            r.setRequirement(req);
            r.setQty(qty);
            r = requirementOrderItemRepository.saveAndFlush(r);
        }
        return r;
    }

    public RequirementOrderItem getRequirementOrderItem(SalesUnitRequirement req, OrderItem o, Integer qty) {
        RequirementOrderItem r = requirementOrderItemRepository.findRequirementOrderItem(req, o);
        if (r == null) {
            r = new RequirementOrderItem();
            r.setOrderItem(o);
            r.setRequirement(req);
            r.setQty(qty);
            r = requirementOrderItemRepository.save(r);
        }
        return r;
    }

    public RequirementOrderItem findRequirementOrderItemByOrder(OrderItem o) {
        RequirementOrderItem r = requirementOrderItemRepository.findRequirementOrderItemByOrder(o);
        return r;
    }

    public OrderShipmentItem getOrderShipmentItem(OrderItem o, ShipmentItem s, Double qty) {
        OrderShipmentItem r = orderShipmentItemRepository.getShipmentOrderItem(o, s);
        if (r == null) {
            r = new OrderShipmentItem();
            r.setOrderItem(o);
            r.setShipmentItem(s);
            r.setQty(qty);
            r = orderShipmentItemRepository.saveAndFlush(r);
        }
        return r;
    }

    public OrderBillingItem getOrderBillingItem(OrderItem o, BillingItem b, Double qty, BigDecimal amount) {
        OrderBillingItem r = orderBillingItemRepository.getBillingOrderItem(o, b);
        if (r == null) {
            r = new OrderBillingItem();
            r.setOrderItem(o);
            r.setBillingItem(b);
            r.setQty(qty);
            r.setAmount(amount);
            r = orderBillingItemRepository.saveAndFlush(r);
        }
        return r;
    }

    public List<PickingSlip> getPickingSlip(OrderItem o) {
        List<PickingSlip> r = pickingSlipRepository.findPickingSlip(o);
        return r;
    }

    public List<OrderShipmentItem> getOrderShipmentItems(OrderItem o) {
        List<OrderShipmentItem> r = orderShipmentItemRepository.getShipmentItems(o);
        return r;
    }

    public List<ShipmentItem> getShipmentItems(OrderItem o) {
        List<ShipmentItem> r = orderShipmentItemRepository.getShipmentItems(o).stream()
            .map(d -> d.getShipmentItem())
            .collect(Collectors.toList());
        return r;
    }

    public List<OrderBillingItem> getOrderBillingItems(OrderItem o) {
        List<OrderBillingItem> r = orderBillingItemRepository.getBillingItems(o);
        return r;
    }

    public List<BillingItem> getBillingItems(OrderItem o) {
        List<BillingItem> r = orderBillingItemRepository.getBillingItems(o).stream()
            .map(d -> d.getBillingItem())
            .collect(Collectors.toList());
        return r;
    }

    public VehicleSalesOrder getDraftVSO(Integer idOrderType, Internal internal, BillTo billTo, Customer customer, SalesUnitRequirement sur) {
        Internal intr = internalRepository.findByIdInternal(sur.getInternal().getIdInternal());
        List<InternalBankMaping> internalBankMaping = internalBankMapingRepository.FindByIdRoot(intr.getRoot().getIdInternal());
        SaleType saleType = sur.getSaleType();
        ShipTo sp = customerUtils.getShipTo(sur.getCustomer());
        List<VehicleSalesOrder> vsos = vehicleSalesOrderRepository.queryCustomerDraft(internal, customer, billTo, idOrderType);
        if (!vsos.isEmpty()) return vsos.get(0);
        Product pr = vehicleSalesOrderRepository.findOneByIdProduct(sur.getProduct().getIdProduct());
//        String vsonumber = numbering.findTransNumberBy(internal.getRoot().getIdInternal(), "VSO");
        VehicleSalesOrder v = new VehicleSalesOrder();

        if (sur.getLeasingCompany() != null) v.setLeasing(sur.getLeasingCompany());

        OrderType orderType = orderTypeRepository.findOne(idOrderType);
        v.setDateOrder(ZonedDateTime.now());
        v.setOrderType(orderType);
        v.setCustomer(customer);
        v.setInternal(internal);
        v.setSaleType(saleType);

        v.setIdSalesUnitRequirement(sur.getIdRequirement());
        v.setBillTo(billTo);
        v.setIdLeasingStatusPayment(0);
        v.setInternal(internal);
        if (saleType != null)
            v.setIdSaleType(saleType.getIdSaleType());
            v.setOrderNumber(sur.getRequirementNumber());
            v.setStatus(BaseConstants.STATUS_OPEN);
        v = vehicleSalesOrderRepository.save(v);


        OrderItem o = new OrderItem();
        o.setIdShipTo(sp.getIdShipTo());
        o.setIdProduct(sur.getProduct().getIdProduct());
        o.setItemDescription(pr.getName());
        o.setIdFeature(sur.getColor().getIdFeature());

        //TODO kasus salah harga
        if(!internalBankMaping.isEmpty() && internalBankMaping.iterator().next().getIsppn() == 0){
            o.setUnitPrice(sur.getHetprice().doubleValue());
        } else {
            o.setUnitPrice(sur.getHetprice().doubleValue() / 1.1);
        }
        o.setDiscount(sur.getSubsown());
        o.setDiscountFinCoy(sur.getSubsfincomp());
        o.setDiscountatpm(sur.getSubsahm());
        o.setDiscountMD(sur.getSubsmd());
        o.setBbn(sur.getBbnprice());

        BigDecimal otr = sur.getUnitPrice();
        BigDecimal bbnPrice = sur.getBbnprice();
        BigDecimal OfftheRoad = otr.subtract(bbnPrice);
        BigDecimal salesAmount = OfftheRoad.multiply(BigDecimal.TEN).divide(BigDecimal.valueOf(11),  0, RoundingMode.HALF_UP);
        BigDecimal totalDisc = sur.getSubsfincomp().add(sur.getSubsmd()).add(sur.getSubsahm()).add(sur.getSubsown());
        BigDecimal priceAfterDisc = salesAmount.subtract(totalDisc);
        BigDecimal tax = salesAmount.divide(BigDecimal.TEN, 0, RoundingMode.HALF_UP);

        o.setDiscountMD(sur.getSubsmd());
        o.setDiscountFinCoy(sur.getSubsfincomp());
        o.setDiscountatpm(sur.getSubsahm());
        o.setDiscount(sur.getSubsown());
        o.setBbn(sur.getBbnprice());
        if(!internalBankMaping.isEmpty() && internalBankMaping.iterator().next().getIsppn() == 0){
            o.setTaxAmount(0D);
        } else {
            o.setTaxAmount(tax);
        }
        o.setQty(1d);
        o.setOrders(v);
        o = orderItemRepository.saveAndFlush(o);


        // Marking Sur ke VSO
        getRequirementOrderItemByOrder(sur, o, 1);
        v.getDetails().add(o);
        return v;
    }

}
