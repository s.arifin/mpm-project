package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.base.executeprocess.SuspectPersonConstants;
import id.atiila.domain.*;
import id.atiila.repository.PersonRepository;
import id.atiila.repository.ProspectSourceRepository;
import id.atiila.repository.SuspectPersonRepository;
import id.atiila.repository.search.SuspectPersonSearchRepository;
import id.atiila.service.dto.PersonDTO;
import id.atiila.service.dto.ProspectPersonDTO;
import id.atiila.service.dto.SuspectPersonDTO;
import id.atiila.service.mapper.SuspectPersonMapper;
import id.atiila.service.pto.SuspectPTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SuspectPerson.
 * BeSmart Team
 */

@Service
@Transactional
public class SuspectPersonService {

    private final Logger log = LoggerFactory.getLogger(SuspectPersonService.class);

    private final SuspectPersonRepository suspectPersonRepository;

    private final SuspectPersonMapper suspectPersonMapper;

    private final SuspectPersonSearchRepository suspectPersonSearchRepository;

    @Autowired
    private ProspectPersonService ppSrv;

    @Autowired
    private ProspectSourceRepository prospectSourceRepo;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private GeoBoundaryUtils geoBoundaryUtils;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private PartyUtils partyUtils;

    public SuspectPersonService(SuspectPersonRepository suspectPersonRepository, SuspectPersonMapper suspectPersonMapper, SuspectPersonSearchRepository suspectPersonSearchRepository) {
        this.suspectPersonRepository = suspectPersonRepository;
        this.suspectPersonMapper = suspectPersonMapper;
        this.suspectPersonSearchRepository = suspectPersonSearchRepository;
    }

    /**
     * Save a suspectPerson.
     *
     * @param suspectPersonDTO the entity to save
     * @return the persisted entity
     */
    public SuspectPersonDTO save(SuspectPersonDTO suspectPersonDTO) {
        log.debug("Request to save SuspectPerson : {}", suspectPersonDTO);
        SuspectPerson suspectPerson = suspectPersonMapper.toEntity(suspectPersonDTO);
        suspectPerson = suspectPersonRepository.save(suspectPerson);
        SuspectPersonDTO result = suspectPersonMapper.toDto(suspectPerson);
        suspectPersonSearchRepository.save(suspectPerson);
        return result;
    }

    /**
     *  Get all the suspectPeople.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SuspectPersonDTO> findAll(Pageable pageable, Integer idsuspecttype, String idInternal) {
        log.debug("Request to get all SuspectPeople");

        //        return suspectPersonRepository.findActiveSuspectPerson(pageable)
        //            .map(suspectPersonMapper::toDto);

        return suspectPersonRepository.findActiveSuspectPersonBySuspectType(pageable, idsuspecttype, idInternal)
            .map(suspectPersonMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<SuspectPersonDTO> findFilterBy(Pageable pageable, SuspectPTO params) {
        log.debug("Request to get all SuspectPeople Filters" + params);
        return suspectPersonRepository.findByFilter(pageable, params).map(suspectPersonMapper::toDto);
    }

    /**
     *  Get one suspectPerson by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SuspectPersonDTO findOne(UUID id) {
        log.debug("Request to get SuspectPerson : {}", id);
        SuspectPerson suspectPerson = suspectPersonRepository.findOne(id);
        return suspectPersonMapper.toDto(suspectPerson);
    }

    /**
     *  Delete the  suspectPerson by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete SuspectPerson : {}", id);
        suspectPersonRepository.delete(id);
        suspectPersonSearchRepository.delete(id);
    }

    /**
     * Search for the suspectPerson corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SuspectPersonDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SuspectPeople for query {}", query);
        Page<SuspectPerson> result = suspectPersonSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(suspectPersonMapper::toDto);
    }

    // Function for Finding Suspect based on Person and Suspect Type
    @Transactional
    public SuspectPersonDTO findByPersonAndSuspectType(PersonDTO p, Integer idSuspectType, String idInternal) {

        Person person = null;

        if (p.getPersonalIdNumber() != null) {
            List<Person> persons = personRepository.findAllByPersonalIdNumber(p.getPersonalIdNumber());
            person = persons.isEmpty() ? null : persons.get(0);
        }

        if (person == null) return null;

        List<SuspectPerson> lp = suspectPersonRepository.
            findSuspectPersonByPersonAndSuspectType(person, idSuspectType, idInternal);

        if (lp.isEmpty()) return null;
        return suspectPersonMapper.toDto(lp.get(0));
    }

    // Function for Creating and Validating New Suspect When Uploading Suspect Data
    @Transactional(propagation = Propagation.REQUIRED)
    protected SuspectPersonDTO suspectPersonFincoValidation(SuspectPersonDTO suspectPersonDTO, String idInternal){

        GeoBoundary geo = null;
        SuspectPersonDTO spct = findByPersonAndSuspectType(suspectPersonDTO.getPerson(),
                                                           BaseConstants.SUSPECT_UPLOAD,
                                                           idInternal);
        if (spct != null) {
            updateNullValueExistingData(spct.getIdSuspect(),suspectPersonDTO);
            return null;
        }
        else {
            WorkType job = suspectPersonRepository.getWorkTypeByDescription(suspectPersonDTO.getPerson().getWorkTypeDescription());
            if (job != null) { suspectPersonDTO.getPerson().setWorkTypeId(job.getIdWorkType()); }

            //Insert Postal Address
            //Province
            geo = geoBoundaryUtils.findProvinceByDescription(suspectPersonDTO.getPerson().getPostalAddress().getProvinceName());
            if (geo != null) { suspectPersonDTO.getPerson().getPostalAddress().setProvinceId(geo.getIdGeobou());}

            //City
            geo = geoBoundaryUtils.findCityByDescription(suspectPersonDTO.getPerson().getPostalAddress().getCityName());
            if (geo != null) { suspectPersonDTO.getPerson().getPostalAddress().setCityId(geo.getIdGeobou());}

            //District
            geo = geoBoundaryUtils.findDistrictByDescription(suspectPersonDTO.getPerson().getPostalAddress().getDistrictName());
            if (geo != null) { suspectPersonDTO.getPerson().getPostalAddress().setDistrictId(geo.getIdGeobou());}

            //Village
            geo = geoBoundaryUtils.findVillageByDescription(suspectPersonDTO.getPerson().getPostalAddress().getVillageName());
            if (geo != null) { suspectPersonDTO.getPerson().getPostalAddress().setVillageId(geo.getIdGeobou());}

            suspectPersonDTO.setSuspectTypeId(BaseConstants.SUSPECT_UPLOAD);
            spct = save(suspectPersonDTO);
        }
        return spct;

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public SuspectPersonDTO processExecuteData(Integer id, String param, SuspectPersonDTO dto) {
        SuspectPersonDTO r = dto;

        //Get internal
        Internal internal = internalUtils.findOne(partyUtils.getCurrentInternal().getIdInternal());

        if (r != null) {
            switch (id) {
                case SuspectPersonConstants.ASSIGN_SUSPECT_BUILD_PROSPECT:
                    ProspectPerson pp = new ProspectPerson();
                    pp.setPerson(personRepository.findOne(dto.getPerson().getIdParty()));
                    pp.setIdSuspect(r.getIdSuspect());
                    pp.setProspectCount(0);
                    pp.setProspectSource(prospectSourceRepo.findOne(BaseConstants.SOURCE_SUSPECT));
                    pp.setIdSalesCoordinator(UUID.fromString(param));
                    pp.setDealer(internal);
                    ppSrv.buildData(pp);
                    changeSuspectPersonStatus(r, BaseConstants.STATUS_COMPLETED);
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Set<SuspectPersonDTO> processExecuteListData(Integer id, String param, Set<SuspectPersonDTO> dto) {
        Set<SuspectPersonDTO> r = dto;

        if (r != null) {
            switch (id) {
                case SuspectPersonConstants.UPLOAD_SUSPECT_BULK:
                    Set<SuspectPersonDTO> temp = new HashSet<>();
                    for(SuspectPersonDTO suspectPersonDTO : r) {
                        suspectPersonDTO = suspectPersonFincoValidation(suspectPersonDTO, param);
                        if (suspectPersonDTO != null) temp.add(suspectPersonDTO);
                    }
                    r = temp;
                    break;
                case SuspectPersonConstants.ASSIGN_SUSPECT_BULK:
                    for(SuspectPersonDTO suspectPersonDTO : r){
                        processExecuteData(SuspectPersonConstants.ASSIGN_SUSPECT_BUILD_PROSPECT, param, suspectPersonDTO);
                    }
                    break;
                case 103:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public SuspectPersonDTO changeSuspectPersonStatus(SuspectPersonDTO dto, Integer id) {
        if (dto != null) {
			SuspectPerson e = suspectPersonRepository.findOne(dto.getIdSuspect());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        suspectPersonSearchRepository.delete(dto.getIdSuspect());
                        break;
                    default:
                        suspectPersonSearchRepository.save(e);
                }
				suspectPersonRepository.save(e);
			}
		}
        return dto;
    }

    // Function for Creating New Suspect With Category Other When Prospect Have Been Dropped
    @Transactional(propagation = Propagation.REQUIRED)
    public void buildSuspectOtherWhenProspectDrop(ProspectPersonDTO p, String idInternal){

        Person person = suspectPersonRepository.getPersonById(p.getPerson().getIdParty());

        List<SuspectPerson> lp = suspectPersonRepository.
            findSuspectPersonByPersonAndSuspectType(person, BaseConstants.SUSPECT_OTHER, idInternal);
        if (!lp.isEmpty()) return;


        SuspectPerson suspectPerson = new SuspectPerson();
        suspectPerson.setPerson(person);
        suspectPerson.setDealer(suspectPersonRepository.getInternalById(p.getDealerId()));
        suspectPerson.setSuspectType(suspectPersonRepository.getSuspectTypeById(BaseConstants.SUSPECT_OTHER));
        suspectPersonRepository.save(suspectPerson);
    }

    @Transactional
    protected void updateNullValueExistingData(UUID suspectId, SuspectPersonDTO spDTO){

        SuspectPerson suspectPerson = suspectPersonRepository.findOne(suspectId);

        if (suspectPerson != null){
            if (suspectPerson.getPerson().getLastName() == null){
                suspectPerson.getPerson().setLastName(spDTO.getPerson().getLastName());
            }

            if (suspectPerson.getPerson().getCellPhone1() == null){
                suspectPerson.getPerson().setCellPhone1(spDTO.getPerson().getCellPhone1());
            }

            if (suspectPerson.getPerson().getPostalAddress().getAddress1() == null){
                suspectPerson.getPerson().getPostalAddress().setAddress1(spDTO.getPerson().getPostalAddress().getAddress1());
            }

            if (suspectPerson.getPerson().getPostalAddress().getAddress2() == null){
                suspectPerson.getPerson().getPostalAddress().setAddress2(spDTO.getPerson().getPostalAddress().getAddress2());
            }

            if (suspectPerson.getPerson().getPostalAddress().getCity() == null){
                GeoBoundary geo = geoBoundaryUtils.findCityByDescription(spDTO.getPerson().getPostalAddress().getCityName());
                if (geo != null) { suspectPerson.getPerson().getPostalAddress().setCity((City) geo); }
            }

            if (suspectPerson.getPerson().getPostalAddress().getDistrict() == null){
                GeoBoundary geo = geoBoundaryUtils.findDistrictByDescription(spDTO.getPerson().getPostalAddress().getDistrictName());
                if (geo != null) { suspectPerson.getPerson().getPostalAddress().setDistrict((District) geo); }
            }

            if (suspectPerson.getPerson().getPostalAddress().getVillage() == null){
                GeoBoundary geo = geoBoundaryUtils.findVillageByDescription(spDTO.getPerson().getPostalAddress().getVillageName());
                if (geo != null) { suspectPerson.getPerson().getPostalAddress().setVillage((Village) geo); }
            }

            if (suspectPerson.getMarketName() == null){
                suspectPerson.setMarketName(spDTO.getMarketName());
            }

            suspectPersonRepository.save(suspectPerson);
        }
    }
}
