package id.atiila.service;

import id.atiila.base.XlsxUtils;
import id.atiila.domain.Feature;
import id.atiila.repository.FeatureRepository;
import id.atiila.repository.search.FeatureSearchRepository;
import id.atiila.service.dto.CustomFeatureDTO;
import id.atiila.service.dto.FeatureDTO;
import id.atiila.service.mapper.FeatureMapper;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.*;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing Feature.
 * BeSmart Team
 */

@Service
@Transactional
public class FeatureService {

    private final Logger log = LoggerFactory.getLogger(FeatureService.class);

    private final FeatureRepository featureRepository;

    private final FeatureMapper featureMapper;

    private final FeatureSearchRepository featureSearchRepository;

    public FeatureService(FeatureRepository featureRepository, FeatureMapper featureMapper, FeatureSearchRepository featureSearchRepository) {
        this.featureRepository = featureRepository;
        this.featureMapper = featureMapper;
        this.featureSearchRepository = featureSearchRepository;
    }

    /**
     * Save a feature.
     *
     * @param featureDTO the entity to save
     * @return the persisted entity
     */
    public FeatureDTO save(FeatureDTO featureDTO) {
        log.debug("Request to save Feature : {}", featureDTO);
        Feature feature = featureMapper.toEntity(featureDTO);
        feature = featureRepository.save(feature);
        FeatureDTO result = featureMapper.toDto(feature);
        featureSearchRepository.save(feature);
        return result;
    }

    /**
     * Get all the features.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FeatureDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Features");
        return featureRepository.findAll(pageable)
            .map(featureMapper::toDto);
    }

    /**
     * Get one feature by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public FeatureDTO findOne(Integer id) {
        log.debug("Request to get Feature : {}", id);
        Feature feature = featureRepository.findOne(id);
        return featureMapper.toDto(feature);
    }

    /**
     * Delete the feature by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete Feature : {}", id);
        featureRepository.delete(id);
        featureSearchRepository.delete(id);
    }

    /**
     * Search for the feature corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FeatureDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of Features for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idFeatureType = request.getParameter("idFeatureType");

        if (idFeatureType != null) {
            q.withQuery(matchQuery("featureType.idFeatureType", idFeatureType));
        }

        Page<Feature> result = featureSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(featureMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<FeatureDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered FeatureDTO");
        String idFeatureType = request.getParameter("idFeatureType");

        if (idFeatureType != null) {
            return featureRepository.queryByIdFeatureType(Integer.valueOf(idFeatureType), pageable).map(featureMapper::toDto);
        }

        return featureRepository.queryNothing(pageable).map(featureMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, FeatureDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<FeatureDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public void initialize() {
        try {
            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/feature.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/feature.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<CustomFeatureDTO> feature = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (CustomFeatureDTO o: feature) {
                    log.debug("tes data migrasi feature", feature);
                    // if (o.getParentGeoCode() != null && o.getGeocode() != null) {
                    //     addVillage(o.getGeocode(), o.getDescription());
                    // }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Async
    public void buildIndex() {
        featureSearchRepository.deleteAll();
        List<Feature> features =  featureRepository.findAll();
        for (Feature f: features) {
            featureSearchRepository.save(f);
            log.debug("Data Features save !...");
        }
    }

}
