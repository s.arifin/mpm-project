package id.atiila.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.camel.Body;
import org.apache.camel.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.atiila.security.SecurityUtils;
import org.springframework.util.Assert;

@Service
@Transactional
public class ActivitiProcessor {

    private final Logger log = LoggerFactory.getLogger(ActivitiProcessor.class);

    @Autowired
    protected RepositoryService repositoryService;

    @Autowired
    protected RuntimeService runtimeService;

    @Autowired
    protected TaskService taskService;

    public Map<String, Object> getVariables(String id, Object value) {
        Map<String, Object> result = new HashMap<>();
        result.put(id, value);
        return result;
    }

    public Map<String, Object> getVariables() {
        Map<String, Object> result = new HashMap<>();
        return result;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public ProcessInstance startProcess(String processName, Map<String, Object> variables) {
        ProcessInstance p =  runtimeService.startProcessInstanceByKey(processName, variables);
        return p;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public ProcessInstance getProcessInstance(String processName, String key) {
        ProcessInstance p =  runtimeService.createProcessInstanceQuery()
            .processDefinitionId(processName)
            .processInstanceBusinessKey(key)
            .singleResult();
        return p;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public ProcessInstance cancelProcessInstance(String processName, String key) {
        ProcessInstance p =  runtimeService.createProcessInstanceQuery()
            .processDefinitionId(processName)
            .processInstanceBusinessKey(key)
            .singleResult();

        if (p != null) {
            runtimeService.deleteProcessInstance(p.getId(), "User Canceled");
        }
        return p;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public ProcessInstance startProcess(String processName, String key) {
        ProcessInstance p =  runtimeService.startProcessInstanceByKey(processName, key);
        return p;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public ProcessInstance startProcess(String processName, String key, Map<String, Object> variables) {
        ProcessInstance p =  runtimeService.startProcessInstanceByKey(processName, key, variables);
        return p;
    }

    public List<Task> getTasks(String processName, String bussinesKey) {
        return taskService.createTaskQuery()
            .processDefinitionKey(processName)
            .processInstanceBusinessKey(bussinesKey)
            // .taskCandidateOrAssigned(SecurityUtils.getCurrentUserLogin().get())
            .list();
    }

    public Task getTask(String id) {
        return taskService.createTaskQuery()
            .taskId(id)
            .singleResult();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public boolean completeTask(String taskId) {
        Boolean result = false;
        Task task = taskService.createTaskQuery()
            .taskId(taskId)
            .singleResult();

        if (task != null) {
            if (task.getAssignee() == null)
                taskService.claim(taskId, SecurityUtils.getCurrentUserLogin().get());

            taskService.complete(taskId);
            result = true;
        }
        return result;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void completeTask(String processName, String bussinesKey, String taskName) {
        List<Task> tasks = taskService.createTaskQuery()
            .processDefinitionKey(processName)
            .processInstanceBusinessKey(bussinesKey)
            .taskDefinitionKey(taskName)
            .list();

        if (tasks != null) {
            for (Task task: tasks) {
                if (task.getAssignee() == null)
                    taskService.claim(task.getId(), SecurityUtils.getCurrentUserLogin().get());
                taskService.complete(task.getId());
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void completeSignalEvent(String processName, String bussinesKey, String eventName) {
        List<Execution> executions = runtimeService.createExecutionQuery()
            .processDefinitionKey(processName)
            .processInstanceBusinessKey( bussinesKey, true)
            .signalEventSubscriptionName(eventName)
            .list();

        for (Execution exec : executions) {
            runtimeService.signalEventReceived(eventName, exec.getId());
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void completeMessageEvent(String processName, String bussinesKey, String eventName) {
        List<Execution> executions = runtimeService.createExecutionQuery()
            .processDefinitionKey(processName)
            .processInstanceBusinessKey( bussinesKey, true)
            .messageEventSubscriptionName(eventName)
            .list();

        for (Execution exec : executions) {
            runtimeService.messageEventReceived(eventName, exec.getId());
        }
    }

    public void receiveCamelCreateProcess(@Body String processName, @Headers Map<String, Object> headers) {
        String bussinesKey = (String) headers.get("bussinesKey");
        startProcess(processName, bussinesKey, headers);
    }

}
