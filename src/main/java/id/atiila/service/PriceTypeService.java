package id.atiila.service;

import id.atiila.domain.PriceType;
import id.atiila.repository.PriceTypeRepository;
import id.atiila.repository.search.PriceTypeSearchRepository;
import id.atiila.service.dto.PriceTypeDTO;
import id.atiila.service.mapper.PriceTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PriceType.
 * BeSmart Team
 */

@Service
@Transactional
public class PriceTypeService {

    private final Logger log = LoggerFactory.getLogger(PriceTypeService.class);

    private final PriceTypeRepository priceTypeRepository;

    private final PriceTypeMapper priceTypeMapper;

    private final PriceTypeSearchRepository priceTypeSearchRepository;

    public PriceTypeService(PriceTypeRepository priceTypeRepository, PriceTypeMapper priceTypeMapper, PriceTypeSearchRepository priceTypeSearchRepository) {
        this.priceTypeRepository = priceTypeRepository;
        this.priceTypeMapper = priceTypeMapper;
        this.priceTypeSearchRepository = priceTypeSearchRepository;
    }

    /**
     * Save a priceType.
     *
     * @param priceTypeDTO the entity to save
     * @return the persisted entity
     */
    public PriceTypeDTO save(PriceTypeDTO priceTypeDTO) {
        log.debug("Request to save PriceType : {}", priceTypeDTO);
        PriceType priceType = priceTypeMapper.toEntity(priceTypeDTO);
        priceType = priceTypeRepository.save(priceType);
        PriceTypeDTO result = priceTypeMapper.toDto(priceType);
        priceTypeSearchRepository.save(priceType);
        return result;
    }

    /**
     *  Get all the priceTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PriceTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PriceTypes");
        return priceTypeRepository.findAll(pageable)
            .map(priceTypeMapper::toDto);
    }

    /**
     *  Get one priceType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PriceTypeDTO findOne(Integer id) {
        log.debug("Request to get PriceType : {}", id);
        PriceType priceType = priceTypeRepository.findOne(id);
        return priceTypeMapper.toDto(priceType);
    }

    /**
     *  Delete the  priceType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete PriceType : {}", id);
        priceTypeRepository.delete(id);
        priceTypeSearchRepository.delete(id);
    }

    /**
     * Search for the priceType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PriceTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PriceTypes for query {}", query);
        Page<PriceType> result = priceTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(priceTypeMapper::toDto);
    }

    public PriceTypeDTO processExecuteData(Integer id, String param, PriceTypeDTO dto) {
        PriceTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<PriceTypeDTO> processExecuteListData(Integer id, String param, Set<PriceTypeDTO> dto) {
        Set<PriceTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
