package id.atiila.service;

import id.atiila.domain.OrderPayment;
import id.atiila.repository.OrderPaymentRepository;
import id.atiila.repository.search.OrderPaymentSearchRepository;
import id.atiila.service.dto.OrderPaymentDTO;
import id.atiila.service.mapper.OrderPaymentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing OrderPayment.
 * BeSmart Team
 */

@Service
@Transactional
public class OrderPaymentService {

    private final Logger log = LoggerFactory.getLogger(OrderPaymentService.class);

    private final OrderPaymentRepository orderPaymentRepository;

    private final OrderPaymentMapper orderPaymentMapper;

    private final OrderPaymentSearchRepository orderPaymentSearchRepository;

    public OrderPaymentService(OrderPaymentRepository orderPaymentRepository, OrderPaymentMapper orderPaymentMapper, OrderPaymentSearchRepository orderPaymentSearchRepository) {
        this.orderPaymentRepository = orderPaymentRepository;
        this.orderPaymentMapper = orderPaymentMapper;
        this.orderPaymentSearchRepository = orderPaymentSearchRepository;
    }

    /**
     * Save a orderPayment.
     *
     * @param orderPaymentDTO the entity to save
     * @return the persisted entity
     */
    public OrderPaymentDTO save(OrderPaymentDTO orderPaymentDTO) {
        log.debug("Request to save OrderPayment : {}", orderPaymentDTO);
        OrderPayment orderPayment = orderPaymentMapper.toEntity(orderPaymentDTO);
        orderPayment = orderPaymentRepository.save(orderPayment);
        OrderPaymentDTO result = orderPaymentMapper.toDto(orderPayment);
        orderPaymentSearchRepository.save(orderPayment);
        return result;
    }

    /**
     * Get all the orderPayments.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrderPaymentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrderPayments");
        return orderPaymentRepository.findAll(pageable)
            .map(orderPaymentMapper::toDto);
    }

    /**
     * Get one orderPayment by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public OrderPaymentDTO findOne(UUID id) {
        log.debug("Request to get OrderPayment : {}", id);
        OrderPayment orderPayment = orderPaymentRepository.findOne(id);
        return orderPaymentMapper.toDto(orderPayment);
    }

    /**
     * Delete the orderPayment by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete OrderPayment : {}", id);
        orderPaymentRepository.delete(id);
        orderPaymentSearchRepository.delete(id);
    }

    /**
     * Search for the orderPayment corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrderPaymentDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of OrderPayments for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idOrders = request.getParameter("idOrders");
        String idPayment = request.getParameter("idPayment");

        if (idOrders != null) {
            q.withQuery(matchQuery("orders.idOrder", idOrders));
        }
        else if (idPayment != null) {
            q.withQuery(matchQuery("payment.idPayment", idPayment));
        }

        Page<OrderPayment> result = orderPaymentSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(orderPaymentMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<OrderPaymentDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered OrderPaymentDTO");
        String idOrders = request.getParameter("idOrders");
        String idPayment = request.getParameter("idPayment");

        if (idOrders != null) {
            return orderPaymentRepository.queryByIdOrders(UUID.fromString(idOrders), pageable).map(orderPaymentMapper::toDto); 
        }
        else if (idPayment != null) {
            return orderPaymentRepository.queryByIdPayment(UUID.fromString(idPayment), pageable).map(orderPaymentMapper::toDto); 
        }

        return orderPaymentRepository.queryNothing(pageable).map(orderPaymentMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, OrderPaymentDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<OrderPaymentDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
