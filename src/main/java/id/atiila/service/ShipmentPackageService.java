package id.atiila.service;

import id.atiila.domain.ShipmentPackage;
import id.atiila.repository.ShipmentPackageRepository;
import id.atiila.repository.search.ShipmentPackageSearchRepository;
import id.atiila.service.dto.ShipmentPackageDTO;
import id.atiila.service.mapper.ShipmentPackageMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ShipmentPackage.
 * BeSmart Team
 */

@Service
@Transactional
public class ShipmentPackageService {

    private final Logger log = LoggerFactory.getLogger(ShipmentPackageService.class);

    private final ShipmentPackageRepository shipmentPackageRepository;

    private final ShipmentPackageMapper shipmentPackageMapper;

    private final ShipmentPackageSearchRepository shipmentPackageSearchRepository;

    public ShipmentPackageService(ShipmentPackageRepository shipmentPackageRepository, ShipmentPackageMapper shipmentPackageMapper, ShipmentPackageSearchRepository shipmentPackageSearchRepository) {
        this.shipmentPackageRepository = shipmentPackageRepository;
        this.shipmentPackageMapper = shipmentPackageMapper;
        this.shipmentPackageSearchRepository = shipmentPackageSearchRepository;
    }

    /**
     * Save a shipmentPackage.
     *
     * @param shipmentPackageDTO the entity to save
     * @return the persisted entity
     */
    public ShipmentPackageDTO save(ShipmentPackageDTO shipmentPackageDTO) {
        log.debug("Request to save ShipmentPackage : {}", shipmentPackageDTO);
        ShipmentPackage shipmentPackage = shipmentPackageMapper.toEntity(shipmentPackageDTO);
        shipmentPackage = shipmentPackageRepository.save(shipmentPackage);
        ShipmentPackageDTO result = shipmentPackageMapper.toDto(shipmentPackage);
        shipmentPackageSearchRepository.save(shipmentPackage);
        return result;
    }

    /**
     *  Get all the shipmentPackages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentPackageDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShipmentPackages");
        return shipmentPackageRepository.findActiveShipmentPackage(pageable)
            .map(shipmentPackageMapper::toDto);
    }

    /**
     *  Get one shipmentPackage by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ShipmentPackageDTO findOne(UUID id) {
        log.debug("Request to get ShipmentPackage : {}", id);
        ShipmentPackage shipmentPackage = shipmentPackageRepository.findOne(id);
        return shipmentPackageMapper.toDto(shipmentPackage);
    }

    /**
     *  Delete the  shipmentPackage by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ShipmentPackage : {}", id);
        shipmentPackageRepository.delete(id);
        shipmentPackageSearchRepository.delete(id);
    }

    /**
     * Search for the shipmentPackage corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentPackageDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ShipmentPackages for query {}", query);
        Page<ShipmentPackage> result = shipmentPackageSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(shipmentPackageMapper::toDto);
    }

    public ShipmentPackageDTO processExecuteData(Integer id, String param, ShipmentPackageDTO dto) {
        ShipmentPackageDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<ShipmentPackageDTO> processExecuteListData(Integer id, String param, Set<ShipmentPackageDTO> dto) {
        Set<ShipmentPackageDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public ShipmentPackageDTO changeShipmentPackageStatus(ShipmentPackageDTO dto, Integer id) {
        if (dto != null) {
			ShipmentPackage e = shipmentPackageRepository.findOne(dto.getIdPackage());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        shipmentPackageSearchRepository.delete(dto.getIdPackage());
                        break;
                    default:
                        shipmentPackageSearchRepository.save(e);
                }
				shipmentPackageRepository.save(e);
			}
		}
        return dto;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        shipmentPackageSearchRepository.deleteAll();
        List<ShipmentPackage> shipmentPackages =  shipmentPackageRepository.findAll();
        for (ShipmentPackage m: shipmentPackages) {
            shipmentPackageSearchRepository.save(m);
            log.debug("Data shipment package save !...");
        }
    }
}
