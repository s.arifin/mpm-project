package id.atiila.service;

import id.atiila.domain.WorkServiceRequirement;
import id.atiila.repository.WorkServiceRequirementRepository;
import id.atiila.repository.search.WorkServiceRequirementSearchRepository;
import id.atiila.service.dto.WorkServiceRequirementDTO;
import id.atiila.service.mapper.WorkServiceRequirementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WorkServiceRequirement.
 * BeSmart Team
 */

@Service
@Transactional
public class WorkServiceRequirementService {

    private final Logger log = LoggerFactory.getLogger(WorkServiceRequirementService.class);

    private final WorkServiceRequirementRepository workServiceRequirementRepository;

    private final WorkServiceRequirementMapper workServiceRequirementMapper;

    private final WorkServiceRequirementSearchRepository workServiceRequirementSearchRepository;
    public WorkServiceRequirementService(WorkServiceRequirementRepository workServiceRequirementRepository, WorkServiceRequirementMapper workServiceRequirementMapper, WorkServiceRequirementSearchRepository workServiceRequirementSearchRepository) {
        this.workServiceRequirementRepository = workServiceRequirementRepository;
        this.workServiceRequirementMapper = workServiceRequirementMapper;
        this.workServiceRequirementSearchRepository = workServiceRequirementSearchRepository;
    }

    /**
     * Save a workServiceRequirement.
     *
     * @param workServiceRequirementDTO the entity to save
     * @return the persisted entity
     */
    public WorkServiceRequirementDTO save(WorkServiceRequirementDTO workServiceRequirementDTO) {
        log.debug("Request to save WorkServiceRequirement : {}", workServiceRequirementDTO);
        WorkServiceRequirement workServiceRequirement = workServiceRequirementMapper.toEntity(workServiceRequirementDTO);
        workServiceRequirement = workServiceRequirementRepository.save(workServiceRequirement);
        WorkServiceRequirementDTO result = workServiceRequirementMapper.toDto(workServiceRequirement);
        workServiceRequirementSearchRepository.save(workServiceRequirement);
        return result;
    }

    /**
     *  Get all the workServiceRequirements.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WorkServiceRequirementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WorkServiceRequirements");
        return workServiceRequirementRepository.findAll(pageable)
            .map(workServiceRequirementMapper::toDto);
    }

    /**
     *  Get one workServiceRequirement by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public WorkServiceRequirementDTO findOne(UUID id) {
        log.debug("Request to get WorkServiceRequirement : {}", id);
        WorkServiceRequirement workServiceRequirement = workServiceRequirementRepository.findOne(id);
        return workServiceRequirementMapper.toDto(workServiceRequirement);
    }

    /**
     *  Delete the  workServiceRequirement by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete WorkServiceRequirement : {}", id);
        workServiceRequirementRepository.delete(id);
        workServiceRequirementSearchRepository.delete(id);
    }

    /**
     * Search for the workServiceRequirement corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WorkServiceRequirementDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WorkServiceRequirements for query {}", query);
        Page<WorkServiceRequirement> result = workServiceRequirementSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(workServiceRequirementMapper::toDto);
    }

    public WorkServiceRequirementDTO processExecuteData(Integer id, String param, WorkServiceRequirementDTO dto) {
        WorkServiceRequirementDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<WorkServiceRequirementDTO> processExecuteListData(Integer id, String param, Set<WorkServiceRequirementDTO> dto) {
        Set<WorkServiceRequirementDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        workServiceRequirementSearchRepository.deleteAll();
        List<WorkServiceRequirement> workServiceRequirements =  workServiceRequirementRepository.findAll();
        for (WorkServiceRequirement m: workServiceRequirements) {
            workServiceRequirementSearchRepository.save(m);
            log.debug("Data work service requirement save !...");
        }
    }

}
