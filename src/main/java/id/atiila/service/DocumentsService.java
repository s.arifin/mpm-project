package id.atiila.service;

import id.atiila.domain.Documents;
import id.atiila.repository.DocumentsRepository;
import id.atiila.repository.search.DocumentsSearchRepository;
import id.atiila.service.dto.DocumentsDTO;
import id.atiila.service.mapper.DocumentsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing Documents.
 * BeSmart Team
 */

@Service
@Transactional
public class DocumentsService {

    private final Logger log = LoggerFactory.getLogger(DocumentsService.class);

    private final DocumentsRepository documentsRepository;

    private final DocumentsMapper documentsMapper;

    private final DocumentsSearchRepository documentsSearchRepository;

    public DocumentsService(DocumentsRepository documentsRepository, DocumentsMapper documentsMapper, DocumentsSearchRepository documentsSearchRepository) {
        this.documentsRepository = documentsRepository;
        this.documentsMapper = documentsMapper;
        this.documentsSearchRepository = documentsSearchRepository;
    }

    /**
     * Save a documents.
     *
     * @param documentsDTO the entity to save
     * @return the persisted entity
     */
    public DocumentsDTO save(DocumentsDTO documentsDTO) {
        log.debug("Request to save Documents : {}", documentsDTO);
        Documents documents = documentsMapper.toEntity(documentsDTO);
        documents = documentsRepository.save(documents);
        DocumentsDTO result = documentsMapper.toDto(documents);
        documentsSearchRepository.save(documents);
        return result;
    }

    /**
     *  Get all the documents.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DocumentsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Documents");
        return documentsRepository.findAll(pageable)
            .map(documentsMapper::toDto);
    }

    /**
     *  Get one documents by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public DocumentsDTO findOne(UUID id) {
        log.debug("Request to get Documents : {}", id);
        Documents documents = documentsRepository.findOne(id);
        return documentsMapper.toDto(documents);
    }

    /**
     *  Delete the  documents by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Documents : {}", id);
        documentsRepository.delete(id);
        documentsSearchRepository.delete(id);
    }

    /**
     * Search for the documents corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DocumentsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Documents for query {}", query);
        Page<Documents> result = documentsSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(documentsMapper::toDto);
    }

    public DocumentsDTO processExecuteData(Integer id, String param, DocumentsDTO dto) {
        DocumentsDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<DocumentsDTO> processExecuteListData(Integer id, String param, Set<DocumentsDTO> dto) {
        Set<DocumentsDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
