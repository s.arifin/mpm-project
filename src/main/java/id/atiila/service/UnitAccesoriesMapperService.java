package id.atiila.service;

import id.atiila.domain.UnitAccesoriesMapper;
import id.atiila.repository.UnitAccesoriesMapperRepository;
import id.atiila.repository.search.UnitAccesoriesMapperSearchRepository;
import id.atiila.service.dto.UnitAccesoriesMapperDTO;
import id.atiila.service.mapper.UnitAccesoriesMapperMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing UnitAccesoriesMapper.
 * BeSmart Team
 */

@Service
@Transactional
public class UnitAccesoriesMapperService {

    private final Logger log = LoggerFactory.getLogger(UnitAccesoriesMapperService.class);

    private final UnitAccesoriesMapperRepository unitAccesoriesMapperRepository;

    private final UnitAccesoriesMapperMapper unitAccesoriesMapperMapper;

    private final UnitAccesoriesMapperSearchRepository unitAccesoriesMapperSearchRepository;

    public UnitAccesoriesMapperService(UnitAccesoriesMapperRepository unitAccesoriesMapperRepository, UnitAccesoriesMapperMapper unitAccesoriesMapperMapper, UnitAccesoriesMapperSearchRepository unitAccesoriesMapperSearchRepository) {
        this.unitAccesoriesMapperRepository = unitAccesoriesMapperRepository;
        this.unitAccesoriesMapperMapper = unitAccesoriesMapperMapper;
        this.unitAccesoriesMapperSearchRepository = unitAccesoriesMapperSearchRepository;
    }

    /**
     * Save a unitAccesoriesMapper.
     *
     * @param unitAccesoriesMapperDTO the entity to save
     * @return the persisted entity
     */
    public UnitAccesoriesMapperDTO save(UnitAccesoriesMapperDTO unitAccesoriesMapperDTO) {
        log.debug("Request to save UnitAccesoriesMapper : {}", unitAccesoriesMapperDTO);
        UnitAccesoriesMapper unitAccesoriesMapper = unitAccesoriesMapperMapper.toEntity(unitAccesoriesMapperDTO);
        unitAccesoriesMapper = unitAccesoriesMapperRepository.save(unitAccesoriesMapper);
        UnitAccesoriesMapperDTO result = unitAccesoriesMapperMapper.toDto(unitAccesoriesMapper);
        unitAccesoriesMapperSearchRepository.save(unitAccesoriesMapper);
        return result;
    }

    /**
     *  Get all the unitAccesoriesMappers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UnitAccesoriesMapperDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UnitAccesoriesMappers");
        return unitAccesoriesMapperRepository.findAll(pageable)
            .map(unitAccesoriesMapperMapper::toDto);
    }

    /**
     *  Get one unitAccesoriesMapper by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public UnitAccesoriesMapperDTO findOne(UUID id) {
        log.debug("Request to get UnitAccesoriesMapper : {}", id);
        UnitAccesoriesMapper unitAccesoriesMapper = unitAccesoriesMapperRepository.findOne(id);
        return unitAccesoriesMapperMapper.toDto(unitAccesoriesMapper);
    }

    /**
     *  Delete the  unitAccesoriesMapper by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete UnitAccesoriesMapper : {}", id);
        unitAccesoriesMapperRepository.delete(id);
        unitAccesoriesMapperSearchRepository.delete(id);
    }

    /**
     * Search for the unitAccesoriesMapper corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UnitAccesoriesMapperDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of UnitAccesoriesMappers for query {}", query);
        Page<UnitAccesoriesMapper> result = unitAccesoriesMapperSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(unitAccesoriesMapperMapper::toDto);
    }

    public UnitAccesoriesMapperDTO processExecuteData(Integer id, String param, UnitAccesoriesMapperDTO dto) {
        UnitAccesoriesMapperDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<UnitAccesoriesMapperDTO> processExecuteListData(Integer id, String param, Set<UnitAccesoriesMapperDTO> dto) {
        Set<UnitAccesoriesMapperDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
