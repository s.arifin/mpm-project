package id.atiila.service;

import id.atiila.domain.UnitReqInternal;
import id.atiila.repository.UnitReqInternalRepository;
import id.atiila.repository.search.UnitReqInternalSearchRepository;
import id.atiila.service.dto.UnitReqInternalDTO;
import id.atiila.service.mapper.UnitReqInternalMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing UnitReqInternal.
 * BeSmart Team
 */

@Service
@Transactional
public class UnitReqInternalService {

    private final Logger log = LoggerFactory.getLogger(UnitReqInternalService.class);

    private final UnitReqInternalRepository unitReqInternalRepository;

    private final UnitReqInternalMapper unitReqInternalMapper;

    private final UnitReqInternalSearchRepository unitReqInternalSearchRepository;

    public UnitReqInternalService(UnitReqInternalRepository unitReqInternalRepository, UnitReqInternalMapper unitReqInternalMapper, UnitReqInternalSearchRepository unitReqInternalSearchRepository) {
        this.unitReqInternalRepository = unitReqInternalRepository;
        this.unitReqInternalMapper = unitReqInternalMapper;
        this.unitReqInternalSearchRepository = unitReqInternalSearchRepository;
    }

    /**
     * Save a unitReqInternal.
     *
     * @param unitReqInternalDTO the entity to save
     * @return the persisted entity
     */
    public UnitReqInternalDTO save(UnitReqInternalDTO unitReqInternalDTO) {
        log.debug("Request to save UnitReqInternal : {}", unitReqInternalDTO);
        UnitReqInternal unitReqInternal = unitReqInternalMapper.toEntity(unitReqInternalDTO);
        unitReqInternal = unitReqInternalRepository.save(unitReqInternal);
        UnitReqInternalDTO result = unitReqInternalMapper.toDto(unitReqInternal);
        unitReqInternalSearchRepository.save(unitReqInternal);
        return result;
    }

    /**
     *  Get all the unitReqInternals.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UnitReqInternalDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UnitReqInternals");
        return unitReqInternalRepository.findAll(pageable)
            .map(unitReqInternalMapper::toDto);
    }

    /**
     *  Get one unitReqInternal by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public UnitReqInternalDTO findOne(UUID id) {
        log.debug("Request to get UnitReqInternal : {}", id);
        UnitReqInternal unitReqInternal = unitReqInternalRepository.findOne(id);
        return unitReqInternalMapper.toDto(unitReqInternal);
    }

    /**
     *  Delete the  unitReqInternal by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete UnitReqInternal : {}", id);
        unitReqInternalRepository.delete(id);
        unitReqInternalSearchRepository.delete(id);
    }

    /**
     * Search for the unitReqInternal corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UnitReqInternalDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of UnitReqInternals for query {}", query);
        Page<UnitReqInternal> result = unitReqInternalSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(unitReqInternalMapper::toDto);
    }
}
