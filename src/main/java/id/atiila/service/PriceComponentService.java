package id.atiila.service;

import id.atiila.base.DmsException;
import id.atiila.base.XlsxUtils;
import id.atiila.domain.*;
import id.atiila.repository.PriceComponentRepository;
import id.atiila.repository.VendorRepository;
import id.atiila.repository.search.PriceComponentSearchRepository;
import id.atiila.service.dto.CustomHargaBeliDTO;
import id.atiila.service.dto.CustomHargaJualDTO;
import id.atiila.service.dto.PriceComponentDTO;
import id.atiila.service.mapper.PriceComponentMapper;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PriceComponent.
 * BeSmart Team
 */

@Service
@Transactional
public class PriceComponentService {

    private final Logger log = LoggerFactory.getLogger(PriceComponentService.class);

    private final PriceComponentRepository priceComponentRepository;

    private final PriceComponentMapper priceComponentMapper;

    private final PriceComponentSearchRepository priceComponentSearchRepository;

    @Autowired
    private ProductUtils productUtils;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private VendorRepository vendorRepository;

    public PriceComponentService(PriceComponentRepository priceComponentRepository, PriceComponentMapper priceComponentMapper, PriceComponentSearchRepository priceComponentSearchRepository) {
        this.priceComponentRepository = priceComponentRepository;
        this.priceComponentMapper = priceComponentMapper;
        this.priceComponentSearchRepository = priceComponentSearchRepository;
    }

    @Transactional(readOnly = true)
    public List<PriceComponentDTO> findPriceByProductAndInternal(String idproduct, String idinternal, ZonedDateTime datewhen){
        UUID idparty = partyUtils.getCurrentOrganizationByInternal(idinternal).getIdParty();
        List<PriceComponent> l = productUtils.getPriceByProductAndInternal(idproduct, idparty);
        List<PriceComponentDTO> l_dto = priceComponentMapper.toDto(l);
        return l_dto;
    }

    @Transactional(readOnly = true)
    public List<PriceComponentDTO> findPriceByProductAndVendor(String idproduct, String idvendor, ZonedDateTime datewhen){
        UUID idparty = partyUtils.getCurrentVendorByIdVendor(idvendor).getOrganization().getIdParty();

        List<PriceComponent> l = productUtils.getPriceByProductAndVendor(idproduct, idparty , datewhen);
        List<PriceComponentDTO> l_dto = priceComponentMapper.toDto(l);
        return l_dto;
    }

    @Transactional(readOnly = true)
    public List<PriceComponentDTO> findAllPriceByProductAndInternal(String idproduct, String idinternal){
        log.debug("Request to get price component all by product and internal");

        UUID idparty = partyUtils.getCurrentOrganizationByInternal(idinternal).getIdParty();
        List<PriceComponent> l = productUtils.getAllPriceByProductAndInternal(idproduct, idparty);
        List<PriceComponentDTO> l_dto = priceComponentMapper.toDto(l);
        return l_dto;
    }

    @Transactional(readOnly = true)
    public List<PriceComponentDTO> findAllPriceByProductAndVendor(String idproduct, String idvendor){
        UUID idparty = partyUtils.getCurrentVendorByIdVendor(idvendor).getOrganization().getIdParty();
        List<PriceComponent> l = productUtils.getAllPriceByProductAndVendor(idproduct, idparty);
        List<PriceComponentDTO> l_dto = priceComponentMapper.toDto(l);
        return l_dto;
    }

    /**
     * Save a priceComponent.
     *
     * @param priceComponentDTO the entity to save
     * @return the persisted entity
     */
    public PriceComponentDTO save(PriceComponentDTO priceComponentDTO) {
        log.debug("Request to save PriceComponent : {}", priceComponentDTO);
        PriceComponentDTO dto = priceComponentDTO;
        PriceComponent priceComponent = priceComponentMapper.toEntity(dto);

        if (priceComponentDTO.getIdPriceComponent() == null) {
            setOffPreviousData(priceComponentDTO);
        }

        priceComponent = priceComponentRepository.save(priceComponent);
        PriceComponentDTO result = priceComponentMapper.toDto(priceComponent);
        priceComponentSearchRepository.save(priceComponent);
        return result;
    }

    private void setOffPreviousData(PriceComponentDTO priceComponentDTO) {
        String idproduct = priceComponentDTO.getProductId();
        Integer idpricetype = priceComponentDTO.getIdPriceType();
        UUID idparty = priceComponentDTO.getSellerId();

        try{
            PriceComponent r = priceComponentRepository.getActivePriceByPriceType(idproduct, idpricetype, idparty);
            if (r != null) {
                r.setDateThru(ZonedDateTime.now());
                priceComponentRepository.save(r);
            }
        }catch (Exception e){
            throw new DmsException(e);
        }
    }

    /**
     *  Get all the priceComponents.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PriceComponentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PriceComponents");
        return priceComponentRepository.findAll(pageable)
            .map(priceComponentMapper::toDto);
    }

    /**
     *  Get one priceComponent by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PriceComponentDTO findOne(UUID id) {
        log.debug("Request to get PriceComponent : {}", id);
        PriceComponent priceComponent = priceComponentRepository.findOne(id);
        return priceComponentMapper.toDto(priceComponent);
    }

    /**
     *  Delete the  priceComponent by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete PriceComponent : {}", id);
        priceComponentRepository.delete(id);
        priceComponentSearchRepository.delete(id);
    }

    /**
     * Search for the priceComponent corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PriceComponentDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PriceComponents for query {}", query);
        Page<PriceComponent> result = priceComponentSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(priceComponentMapper::toDto);
    }

    public PriceComponentDTO processExecuteData(Integer id, String param, PriceComponentDTO dto) {
        PriceComponentDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<PriceComponentDTO> processExecuteListData(Integer id, String param, Set<PriceComponentDTO> dto) {
        Set<PriceComponentDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @PostConstruct
    @Transactional
    public void initialize() {
        try {
            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/harga_jual.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/harga_jual.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<CustomHargaJualDTO> hargaJual = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (CustomHargaJualDTO o: hargaJual) {
                    log.debug("Tampilan DTO harga jual", hargaJual);
                    // if (o.getParentGeoCode() != null && o.getGeocode() != null) {
                    //     addVillage(o.getGeocode(), o.getDescription());
                    // }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

//    @PostConstruct
    @Transactional
    public void initialize2() {
        try {
            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/harga_beli.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/harga_beli.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<CustomHargaBeliDTO> hargaBeli = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (CustomHargaBeliDTO o: hargaBeli) {
                    log.debug("Tampilan DTO harga beli", hargaBeli);
                    // if (o.getParentGeoCode() != null && o.getGeocode() != null) {
                    //     addVillage(o.getGeocode(), o.getDescription());
                    // }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        priceComponentSearchRepository.deleteAll();
        List<PriceComponent> priceComponents =  priceComponentRepository.findAll();
        for (PriceComponent m: priceComponents) {
            priceComponentSearchRepository.save(m);
            log.debug("Data priceComponent save !...");
        }
    }

    @Transactional(readOnly = true)
    public Page<PriceComponentDTO> findPriceSTNKB(HttpServletRequest request, Pageable pageable) {
        String idproduct = request.getParameter("idproduct");
        String idvendor = request.getParameter("idvendor");
        log.debug("product stnkb = ", idproduct);
        log.debug("vendor stnkb = ", idvendor);
//        List<PriceComponent> l = priceComponentRepository.getSTNKB(idproduct, idvendor);
//        List<PriceComponentDTO> l_dto = priceComponentMapper.toDto(l);
        return priceComponentRepository.getSTNKB(idproduct, idvendor, pageable)
            .map(priceComponentMapper::toDto);
    }
}
