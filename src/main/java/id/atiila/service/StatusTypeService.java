package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.StatusType;
import id.atiila.repository.StatusTypeRepository;
import id.atiila.repository.search.StatusTypeSearchRepository;
import id.atiila.service.dto.StatusTypeDTO;
import id.atiila.service.mapper.StatusTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.PostConstruct;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing StatusType.
 * BeSmart Team
 */

@Service
@Transactional
public class StatusTypeService {

    private final Logger log = LoggerFactory.getLogger(StatusTypeService.class);

    private final StatusTypeRepository statusTypeRepository;

    private final StatusTypeMapper statusTypeMapper;

    private final StatusTypeSearchRepository statusTypeSearchRepository;

    public StatusTypeService(StatusTypeRepository statusTypeRepository, StatusTypeMapper statusTypeMapper, StatusTypeSearchRepository statusTypeSearchRepository) {
        this.statusTypeRepository = statusTypeRepository;
        this.statusTypeMapper = statusTypeMapper;
        this.statusTypeSearchRepository = statusTypeSearchRepository;
    }

    /**
     * Save a statusType.
     *
     * @param statusTypeDTO the entity to save
     * @return the persisted entity
     */
    public StatusTypeDTO save(StatusTypeDTO statusTypeDTO) {
        log.debug("Request to save StatusType : {}", statusTypeDTO);
        StatusType statusType = statusTypeMapper.toEntity(statusTypeDTO);
        statusType = statusTypeRepository.save(statusType);
        StatusTypeDTO result = statusTypeMapper.toDto(statusType);
        statusTypeSearchRepository.save(statusType);
        return result;
    }

    /**
     *  Get all the statusTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StatusTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all StatusTypes");
        return statusTypeRepository.findAll(pageable)
            .map(statusTypeMapper::toDto);
    }

    /**
     *  Get one statusType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public StatusTypeDTO findOne(Integer id) {
        log.debug("Request to get StatusType : {}", id);
        StatusType statusType = statusTypeRepository.findOne(id);
        return statusTypeMapper.toDto(statusType);
    }

    /**
     *  Delete the  statusType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete StatusType : {}", id);
        statusTypeRepository.delete(id);
        statusTypeSearchRepository.delete(id);
    }

    /**
     * Search for the statusType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StatusTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of StatusTypes for query {}", query);
        Page<StatusType> result = statusTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(statusTypeMapper::toDto);
    }

    //@PostConstruct
    public void initialize() {
        if (statusTypeRepository.findAll().isEmpty()) {

            for (int i = 1; i <= 100; i++) {
                save(new StatusTypeDTO(i, "Reserved"));
            }

            save(new StatusTypeDTO(BaseConstants.STATUS_DRAFT, "Draft"));
            save(new StatusTypeDTO(BaseConstants.STATUS_OPEN, "Open"));
            save(new StatusTypeDTO(BaseConstants.STATUS_HOLD, "Hold"));
            save(new StatusTypeDTO(BaseConstants.STATUS_CANCEL, "Cancel"));
            save(new StatusTypeDTO(BaseConstants.STATUS_COMPLETED, "Completed"));
            save(new StatusTypeDTO(BaseConstants.STATUS_ACTIVE, "Active"));
        }
    }
}
