package id.atiila.service;

import id.atiila.domain.ItemIssuance;
import id.atiila.repository.ItemIssuanceRepository;
import id.atiila.repository.search.ItemIssuanceSearchRepository;
import id.atiila.service.dto.ItemIssuanceDTO;
import id.atiila.service.mapper.ItemIssuanceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing ItemIssuance.
 * BeSmart Team
 */

@Service
@Transactional
public class ItemIssuanceService {

    private final Logger log = LoggerFactory.getLogger(ItemIssuanceService.class);

    private final ItemIssuanceRepository itemIssuanceRepository;

    private final ItemIssuanceMapper itemIssuanceMapper;

    private final ItemIssuanceSearchRepository itemIssuanceSearchRepository;

    public ItemIssuanceService(ItemIssuanceRepository itemIssuanceRepository, ItemIssuanceMapper itemIssuanceMapper, ItemIssuanceSearchRepository itemIssuanceSearchRepository) {
        this.itemIssuanceRepository = itemIssuanceRepository;
        this.itemIssuanceMapper = itemIssuanceMapper;
        this.itemIssuanceSearchRepository = itemIssuanceSearchRepository;
    }

    /**
     * Save a itemIssuance.
     *
     * @param itemIssuanceDTO the entity to save
     * @return the persisted entity
     */
    public ItemIssuanceDTO save(ItemIssuanceDTO itemIssuanceDTO) {
        log.debug("Request to save ItemIssuance : {}", itemIssuanceDTO);
        ItemIssuance itemIssuance = itemIssuanceMapper.toEntity(itemIssuanceDTO);
        itemIssuance = itemIssuanceRepository.save(itemIssuance);
        ItemIssuanceDTO result = itemIssuanceMapper.toDto(itemIssuance);
        itemIssuanceSearchRepository.save(itemIssuance);
        return result;
    }

    /**
     *  Get all the itemIssuances.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ItemIssuanceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ItemIssuances");
        return itemIssuanceRepository.findAll(pageable)
            .map(itemIssuanceMapper::toDto);
    }

    /**
     *  Get one itemIssuance by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ItemIssuanceDTO findOne(UUID id) {
        log.debug("Request to get ItemIssuance : {}", id);
        ItemIssuance itemIssuance = itemIssuanceRepository.findOne(id);
        return itemIssuanceMapper.toDto(itemIssuance);
    }

    /**
     *  Delete the  itemIssuance by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ItemIssuance : {}", id);
        itemIssuanceRepository.delete(id);
        itemIssuanceSearchRepository.delete(id);
    }

    /**
     * Search for the itemIssuance corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ItemIssuanceDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ItemIssuances for query {}", query);
        Page<ItemIssuance> result = itemIssuanceSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(itemIssuanceMapper::toDto);
    }

    public ItemIssuanceDTO processExecuteData(Integer id, String param, ItemIssuanceDTO dto) {
        ItemIssuanceDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<ItemIssuanceDTO> processExecuteListData(Integer id, String param, Set<ItemIssuanceDTO> dto) {
        Set<ItemIssuanceDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
