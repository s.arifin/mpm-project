package id.atiila.service;

import id.atiila.base.XlsxUtils;
import id.atiila.domain.PostalAddress;
import id.atiila.repository.PostalAddressRepository;
import id.atiila.repository.search.PostalAddressSearchRepository;
import id.atiila.service.dto.PostalAddressDTO;
import id.atiila.service.dto.CustomPostalAddressDTO;
import id.atiila.service.mapper.PostalAddressMapper;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

import java.util.UUID;
import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing PostalAddress.
 * BeSmart Team
 */

@Service
@Transactional
public class PostalAddressService {

    private final Logger log = LoggerFactory.getLogger(PostalAddressService.class);

    private final PostalAddressRepository postalAddressRepository;

    private final PostalAddressMapper postalAddressMapper;

    private final PostalAddressSearchRepository postalAddressSearchRepository;

    public PostalAddressService(PostalAddressRepository postalAddressRepository, PostalAddressMapper postalAddressMapper, PostalAddressSearchRepository postalAddressSearchRepository) {
        this.postalAddressRepository = postalAddressRepository;
        this.postalAddressMapper = postalAddressMapper;
        this.postalAddressSearchRepository = postalAddressSearchRepository;
    }

    /**
     * Save a postalAddress.
     *
     * @param postalAddressDTO the entity to save
     * @return the persisted entity
     */
    public PostalAddressDTO save(PostalAddressDTO postalAddressDTO) {
        log.debug("Request to save PostalAddress : {}", postalAddressDTO);
        PostalAddress postalAddress = postalAddressMapper.toEntity(postalAddressDTO);
        postalAddress = postalAddressRepository.save(postalAddress);
        PostalAddressDTO result = postalAddressMapper.toDto(postalAddress);
        postalAddressSearchRepository.save(postalAddress);
        return result;
    }

    /**
     * Get all the postalAddresses.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PostalAddressDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PostalAddresses");
        return postalAddressRepository.findAll(pageable)
            .map(postalAddressMapper::toDto);
    }

    /**
     * Get one postalAddress by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PostalAddressDTO findOne(UUID id) {
        log.debug("Request to get PostalAddress : {}", id);
        PostalAddress postalAddress = postalAddressRepository.findOne(id);
        return postalAddressMapper.toDto(postalAddress);
    }

    /**
     * Delete the postalAddress by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete PostalAddress : {}", id);
        postalAddressRepository.delete(id);
        postalAddressSearchRepository.delete(id);
    }

    /**
     * Search for the postalAddress corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PostalAddressDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PostalAddresses for query {}", query);
        Page<PostalAddress> result = postalAddressSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(postalAddressMapper::toDto);
    }

    @Transactional(readOnly = true)
    public PostalAddressDTO processExecuteData(Integer id, String param, PostalAddressDTO dto) {
        PostalAddressDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional(readOnly = true)
    public Set<PostalAddressDTO> processExecuteListData(Integer id, String param, Set<PostalAddressDTO> dto) {
        Set<PostalAddressDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @PostConstruct
    @Transactional
    public void initialize() {
        try {
            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/address.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/address.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<CustomPostalAddressDTO> address = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (CustomPostalAddressDTO o: address) {
                    log.debug("Tampilan DTO unit deliverable", address);
                    // if (o.getParentGeoCode() != null && o.getGeocode() != null) {
                    //     addVillage(o.getGeocode(), o.getDescription());
                    // }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
