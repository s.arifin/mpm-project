package id.atiila.service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IAtomicLong;
import id.atiila.domain.Feature;
import id.atiila.domain.InventoryItem;
import id.atiila.domain.OrderItem;
import id.atiila.domain.Product;
import id.atiila.repository.FeatureRepository;
import id.atiila.repository.InventoryItemRepository;
import id.atiila.repository.OrderItemRepository;
import id.atiila.repository.search.InventoryItemSearchRepository;
import id.atiila.route.IndexerRoute;
import id.atiila.service.dto.FacilityDTO;
import id.atiila.service.dto.InventoryItemDTO;
import id.atiila.service.dto.SalesUnitRequirementDTO;
import id.atiila.service.mapper.InventoryItemMapper;
import org.apache.camel.Body;
import org.apache.camel.ProducerTemplate;
import org.elasticsearch.index.query.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing InventoryItem.
 * BeSmart Team
 */

@Service
@Transactional
public class InventoryItemService {

    private final Logger log = LoggerFactory.getLogger(InventoryItemService.class);

    private final InventoryItemRepository inventoryItemRepository;

    private final InventoryItemMapper inventoryItemMapper;

    private final InventoryItemSearchRepository inventoryItemSearchRepository;

    private final FeatureRepository featureRepository;

    @Autowired
    private SalesUnitRequirementService salesUnitRequirementService;

    @Autowired
    private FacilityService facilityService;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private HazelcastInstance hz;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private FacilityUtils facilityUtils;

    @Autowired
    private ProductUtils productUtils;

    public InventoryItemService(InventoryItemRepository inventoryItemRepository, InventoryItemMapper inventoryItemMapper, InventoryItemSearchRepository inventoryItemSearchRepository, FeatureRepository featureRepository) {
        this.inventoryItemRepository = inventoryItemRepository;
        this.inventoryItemMapper = inventoryItemMapper;
        this.inventoryItemSearchRepository = inventoryItemSearchRepository;

        this.featureRepository = featureRepository;
    }

    /**
     * Save a inventoryItem.
     *
     * @param inventoryItemDTO the entity to save
     * @return the persisted entity
     */
    public InventoryItemDTO save(InventoryItemDTO inventoryItemDTO) {
        log.debug("Request to save InventoryItem : {}", inventoryItemDTO);
        InventoryItem inventoryItem = inventoryItemMapper.toEntity(inventoryItemDTO);
        inventoryItem = inventoryItemRepository.save(inventoryItem);
        InventoryItemDTO result = inventoryItemMapper.toDto(inventoryItem);
        inventoryItemSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the inventoryItems.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InventoryItemDTO> findAll() {
        return findAll();
    }

    /**
     *  Get all the inventoryItems.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InventoryItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all InventoryItems");
        return inventoryItemRepository.findActiveInventoryItem( pageable)
            .map(inventoryItemMapper::toDto)
            .map(this::assignData);
    }

    @Transactional(readOnly = true)
    public Page<InventoryItemDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered InventoryItemDTO");
        String idInternal = request.getParameter("idInternal");
        UUID idFacility = request.getParameter("idFacility") == null ?
            null : UUID.fromString(request.getParameter("idFacility"));
        UUID idContainer = request.getParameter("idContainer") == null ?
            null : UUID.fromString(request.getParameter("idContainer"));
        UUID idBilling = request.getParameter("idBilling") == null ?
            null : UUID.fromString(request.getParameter("idBilling"));

        if (idInternal != null) {
            return inventoryItemRepository.findAllInventoryItemByIdFacilityAndIdContainer(
                pageable, idInternal, idFacility, idContainer)
                .map(inventoryItemMapper::toDto)
                .map(this::assignData);
        } else if (idBilling != null) {
            return inventoryItemRepository.findIVTByidBilling(idBilling, pageable).map(inventoryItemMapper::toDto);
        }
        return inventoryItemRepository.queryNothing(pageable).map(inventoryItemMapper::toDto).map(this::assignData);
    }
// todo remark karena ga pakai gudang dulu
//    @Transactional(readOnly = true)
//    public Page<InventoryItemDTO> findByReguest(
//            String regNoSin,
//            String idProduct,
//            String idInternal,
//            Integer idFeature,
//            String regNoKa,
//            Integer regYear,
//            UUID regGudang,
//            Pageable pageable) {
//
//        if (regNoSin == null && regNoKa == null){
//            log.debug("warna inventrory item  ===" + idFeature);
//            log.debug("Inventory item service reg nosi dan regnoka ====== null ");
//            log.debug("Inventory item service reg year ====== ", regYear);
//            return inventoryItemRepository.getByIdOwnerAndWithIdFeatureOrderByDateCreatedNative(
//                idProduct, idInternal, idFeature,regYear, regGudang,pageable)
//                .map(inventoryItemMapper::toDto);
//        } else {
//            String criteriaIdMachine = "%";
//            String criteriaIdFrame = "%";
//
//            if (!(regNoSin == null || regNoSin.isEmpty() )){
//                criteriaIdMachine = '%'+regNoSin+'%';
//            }
//            if (!(regNoKa == null || regNoKa.isEmpty() )){
//                criteriaIdFrame = '%'+regNoKa +'%';
//            }
//
//            log.debug("Request to find by request=> nosin = "+ criteriaIdMachine + ", noka=" +criteriaIdFrame+ ", idProduk="+ idProduct+
//                ", idFeat="+idFeature.toString()+", party role="+ idInternal+", tahun rakit = " + regYear);
//            return inventoryItemRepository.getInventoryFiltered(
//                idProduct, idInternal, idFeature, criteriaIdFrame, criteriaIdMachine,regYear,regGudang, pageable).map(inventoryItemMapper::toDto);
//        }
//
//    }

    @Transactional(readOnly = true)
    public Page<InventoryItemDTO> findByReguest(
        String regNoSin,
        String idProduct,
        String idInternal,
        Integer idFeature,
        String regNoKa,
        Integer regYear,
        Pageable pageable) {

        if (regNoSin == null && regNoKa == null){
            log.debug("warna inventrory item  ===" + idFeature);
            log.debug("Inventory item service reg nosi dan regnoka ====== null ");
            log.debug("Inventory item service reg year ====== ", regYear);
            // todo remark karena ga pakai gudnag dulu
//            return inventoryItemRepository.getByIdOwnerAndWithIdFeatureOrderByDateCreatedNative(
//                idProduct, idInternal, idFeature,regYear, regGudang,pageable)
//                .map(inventoryItemMapper::toDto);
            return inventoryItemRepository.getByIdOwnerAndWithIdFeatureOrderByDateCreatedNative(
                idProduct, idInternal, idFeature,regYear,pageable)
                .map(inventoryItemMapper::toDto);
        } else {
            String criteriaIdMachine = "%";
            String criteriaIdFrame = "%";

            if (!(regNoSin == null || regNoSin.isEmpty() )){
                criteriaIdMachine = '%'+regNoSin+'%';
            }
            if (!(regNoKa == null || regNoKa.isEmpty() )){
                criteriaIdFrame = '%'+regNoKa +'%';
            }

            log.debug("Request to find by request=> nosin = "+ criteriaIdMachine + ", noka=" +criteriaIdFrame+ ", idProduk="+ idProduct+
                ", idFeat="+idFeature.toString()+", party role="+ idInternal+", tahun rakit = " + regYear);
//            todo remark karena ga pakai gudang dulu
//            return inventoryItemRepository.getInventoryFiltered(idProduct, idInternal, idFeature, criteriaIdFrame, criteriaIdMachine,regYear,regGudang, pageable).map(inventoryItemMapper::toDto);
            return inventoryItemRepository.getInventoryFiltered(idProduct, idInternal, idFeature, criteriaIdFrame, criteriaIdMachine,regYear, pageable).map(inventoryItemMapper::toDto);
        }

    }


    @Transactional(readOnly = true)
    public Page<InventoryItemDTO> findByReguestForMemo(
        String regNoSin,
        String idProduct,
        String idInternal,
        Integer idFeature,
        String regNoKa,
        Integer cogs,
//        Integer yearAssembly,
        Pageable pageable) {

        log.debug("cogs inventory item service = "+cogs);
        if ( (regNoSin == null && regNoKa == null) && cogs != null){
//            log.debug("filter memo year service == "+ yearAssembly);
            log.debug("warna inventrory item  ===" + idFeature);
            log.debug("Inventory item service reg nosi dan regnoka ====== null ");
//            return inventoryItemRepository.getByIdOwnerAndWithCogsIdFeatureOrderByDateCreatedNative(idProduct, idInternal, idFeature, cogs, yearAssembly, pageable).map(inventoryItemMapper::toDto);
            return inventoryItemRepository.getByIdOwnerAndWithCogsIdFeatureOrderByDateCreatedNative(idProduct, idInternal, idFeature, cogs, pageable).map(inventoryItemMapper::toDto);
        } else {
            String criteriaIdMachine = "%";
            String criteriaIdFrame = "%";

            if (!(regNoSin == null || regNoSin.isEmpty() )){
                criteriaIdMachine = '%'+regNoSin+'%';
            }
            if (!(regNoKa == null || regNoKa.isEmpty() )){
                criteriaIdFrame = '%'+regNoKa +'%';
            }

            log.debug("Request to find by request=> nosin = "+ criteriaIdMachine + ", noka=" +criteriaIdFrame+ ", idProduk="+ idProduct+
                ", idFeat="+idFeature.toString()+", party role="+ idInternal+", cogs = "+ cogs );
//            return inventoryItemRepository.getInventoryFilteredWithCogs(idProduct, idInternal, idFeature, criteriaIdFrame, criteriaIdMachine, cogs, yearAssembly, pageable).map(inventoryItemMapper::toDto);
            return inventoryItemRepository.getInventoryFilteredWithCogs(idProduct, idInternal, idFeature, criteriaIdFrame, criteriaIdMachine, cogs, pageable).map(inventoryItemMapper::toDto);
        }

    }

    /**
     *  Get one inventoryItem by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public InventoryItemDTO findOne(UUID id) {
        log.debug("Request to get InventoryItem : {}", id);
        InventoryItem inventoryItem = inventoryItemRepository.findOne(id);
        return inventoryItemMapper.toDto(inventoryItem);
    }

    /**
     *  Delete the  inventoryItem by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete InventoryItem : {}", id);
        inventoryItemRepository.delete(id);
        inventoryItemSearchRepository.delete(id);
    }

    /**
     * Search for the inventoryItem corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InventoryItemDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of InventoryItems for query {}", query);

        UUID ownerId = partyUtils.getCurrentInternal().getOrganization().getIdParty();

        QueryBuilder boolQueryBuilder = queryStringQuery("*"+query+"*");

        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder();
        q.withQuery(boolQuery().must(boolQueryBuilder).filter(
            boolQuery().must(matchQuery("idOwner", ownerId)))
        );
        Page<InventoryItemDTO> result = inventoryItemSearchRepository.search(q.build().getQuery(), pageable);
        return result;
    }

    public InventoryItemDTO processExecuteData(Integer id, String param, InventoryItemDTO dto) {
        InventoryItemDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<InventoryItemDTO> processExecuteListData(Integer id, String param, Set<InventoryItemDTO> dto) {
        Set<InventoryItemDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public InventoryItemDTO changeInventoryItemStatus(InventoryItemDTO dto, Integer id) {
        if (dto != null) {
			InventoryItem e = inventoryItemRepository.findOne(dto.getIdInventoryItem());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        inventoryItemSearchRepository.delete(dto.getIdInventoryItem());
                        break;
                    default:
                        inventoryItemSearchRepository.save(dto);
                }
				inventoryItemRepository.save(e);
			}
		}
        return dto;
    }

    @Transactional(readOnly = true)
    public Page<InventoryItemDTO> findInventoryByProductOwnerFeature(
            SalesUnitRequirementDTO surDto, Pageable pageable) {
        log.debug("Request to get all InventoryItems by product, owner and feature");

        //internal id-> internal -> party
        log.debug("Find inventory by product[" + surDto.getProductId() + "], owner[" + surDto.getPersonOwner() + "], feature [" + surDto.getColorId()+"] ");

        SalesUnitRequirementDTO salesUnitRequirementDto = salesUnitRequirementService.findOne(surDto.getId());
        return inventoryItemRepository.getByIdProductAndIdOwnerAndIdFeatureOrderByDateCreated(
                surDto.getProductId(),
                surDto.getInternalId(),
                surDto.getColorId(),
                surDto.getIdframe(),
                surDto.getIdmachine(),
                pageable)
            .map(inventoryItemMapper::toDto);
        //return null;

    }

    public List<InventoryItemDTO> checkAvailableStock(String idInternal, String idProduct, Integer idFeature, Integer yearAssembly){
        List<InventoryItem> inventoryItems = inventoryItemRepository.findAvailableStockByInternalAndProductAndFeature(idInternal, idProduct, idFeature, yearAssembly);

        List<InventoryItemDTO> list = new ArrayList<InventoryItemDTO>();
        if (!inventoryItems.isEmpty()) {
            for(InventoryItem a : inventoryItems){
                list.add(inventoryItemMapper.toDto(a));
            }
        }

        return list;
    }

    @Transactional(readOnly = true)
    public Page<InventoryItemDTO> findAvailableStock(String idProduct, String idFacility, String idContainer, Pageable pageable){
        log.debug("SERVICE to get available stock");

        FacilityDTO facilityDTO = facilityService.findOne(UUID.fromString(idFacility));
        UUID _idcontainer = UUID.fromString(idContainer);

        if (idContainer != null){
            return inventoryItemRepository.findAvailableMovingItemByIdFacilityAndIdContainerAndIdProduct(idProduct,facilityDTO.getFacilityCode(), null, _idcontainer, pageable).map(
                inventoryItemMapper::toDto
            );
        } else {
            return inventoryItemRepository.findAvailableMovingItemByIdFacilityAndIdContainerAndIdProduct(idProduct, facilityDTO.getFacilityCode(), facilityDTO.getIdFacility(), null, pageable).map(
                inventoryItemMapper::toDto
            );
        }
    }

    @Transactional
    public void reindexer(@Body InventoryItemDTO iiDTO) {
        inventoryItemSearchRepository.save(iiDTO);
        System.out.println("Data " + iiDTO.toString());
    }

    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String command = (String) item.get("command");
        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    public void buildIndex() {
        inventoryItemSearchRepository.deleteAll();
        List<InventoryItem> inventoryItems =  inventoryItemRepository.findValidInventoryItems();
        for (InventoryItem m: inventoryItems) {
            producerTemplate.sendBody(IndexerRoute.INDEXER_INVENTORY, assignData(inventoryItemMapper.toDto(m)));
            log.debug("Data inventoryItem save !...");
        }
    }

    public InventoryItemDTO assignData(InventoryItemDTO dto) {
        if (dto.getIdContainer() != null) {
            dto.setContainerCode(facilityUtils.getContainerCode(dto.getIdContainer()));
        }
        if (dto.getFacilityCode() != null) {
            dto.setFacilityCode(facilityUtils.getFacilityCode(dto.getIdFacility()));
        }
        if (dto.getIdFeature() != null) {
            Feature f = featureRepository.findOne(dto.getIdFeature());
            if (f != null) {
                dto.setFeatureCode(f.getRefKey());
                dto.setFeatureDescription(f.getDescription());
            }
        }
        if (dto.getIdProduct() != null) {
            Product p = productUtils.findOne(dto.getIdProduct());
            if (p != null) {
                dto.setItemDescription(p.getName());
            }
        }
        return dto;
    }
}
