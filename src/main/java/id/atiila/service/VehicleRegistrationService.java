package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.VehicleDocumentRequirement;
import id.atiila.repository.VehicleRegistrationRepository;
import id.atiila.repository.search.VehicleRegistrationSearchRepository;
import id.atiila.service.dto.VehicleRegistrationDTO;
import id.atiila.service.mapper.VehicleRegistrationMapper;
import org.apache.camel.Body;
import org.apache.camel.ProducerTemplate;
import org.apache.lucene.queryparser.xml.FilterBuilder;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;

/**
 * Service Implementation for managing VehicleRegistration.
 * atiila consulting
 */

@Service
@Transactional
public class VehicleRegistrationService {

    private final Logger log = LoggerFactory.getLogger(VehicleRegistrationService.class);

    private final VehicleRegistrationRepository vehicleRegistrationRepository;

    private final VehicleRegistrationMapper vehicleRegistrationMapper;

    private final VehicleRegistrationSearchRepository vehicleRegistrationSearchRepository;

    @Autowired
    private ProducerTemplate template;

    @Autowired
    private RequirementUtils requirementUtils;

    public VehicleRegistrationService(VehicleRegistrationRepository vehicleRegistrationRepository, VehicleRegistrationMapper vehicleRegistrationMapper, VehicleRegistrationSearchRepository vehicleRegistrationSearchRepository) {
        this.vehicleRegistrationRepository = vehicleRegistrationRepository;
        this.vehicleRegistrationMapper = vehicleRegistrationMapper;
        this.vehicleRegistrationSearchRepository = vehicleRegistrationSearchRepository;
    }

    /**
     * Save a vehicleRegistration.
     *
     * @param vehicleRegistrationDTO the entity to save
     * @return the persisted entity
     */
    public VehicleRegistrationDTO save(VehicleRegistrationDTO vehicleRegistrationDTO) {
        log.debug("Request to save VehicleRegistration : {}", vehicleRegistrationDTO);
        VehicleDocumentRequirement vehicleRegistration = vehicleRegistrationMapper.toEntity(vehicleRegistrationDTO);
        vehicleRegistration = vehicleRegistrationRepository.save(vehicleRegistration);
        VehicleRegistrationDTO result = vehicleRegistrationMapper.toDto(vehicleRegistration);
        vehicleRegistrationSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the vehicleRegistrations.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleRegistrationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VehicleRegistrations");
        return vehicleRegistrationRepository.findActiveVehicleRegistration(pageable)
            .map(vehicleRegistrationMapper::toDto);
    }

    /**
     * Get one vehicleRegistration by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public VehicleRegistrationDTO findOne(UUID id) {
        log.debug("Request to get VehicleRegistration : {}", id);
        VehicleDocumentRequirement vehicleRegistration = vehicleRegistrationRepository.findOne(id);
        return vehicleRegistrationMapper.toDto(vehicleRegistration);
    }

    /**
     * Delete the vehicleRegistration by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete VehicleRegistration : {}", id);
        vehicleRegistrationRepository.delete(id);
        vehicleRegistrationSearchRepository.delete(id);
    }

    /**
     * Search for the vehicleRegistration corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleRegistrationDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of VehicleRegistrations for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));

        String filterName = request.getParameter("filterName");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");
        String idVendor = request.getParameter("idVendor");

        if (filterName != null && "byInternalActive".equalsIgnoreCase(filterName)) {

            BoolQueryBuilder boolQuery = QueryBuilders.boolQuery()
                .must(queryStringQuery(query))
                .filter(QueryBuilders.matchQuery("internalId", idInternal));

            Page<VehicleRegistrationDTO> result = vehicleRegistrationSearchRepository.search(boolQuery, pageable);
            return result;
        }

        Page<VehicleRegistrationDTO> result = vehicleRegistrationSearchRepository.search(q.build().getQuery(), pageable);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<VehicleRegistrationDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered VehicleRegistrationDTO");
        String filterName = request.getParameter("filterName");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");
        String idVendor = request.getParameter("idVendor");

        if (filterName != null && "byInternalActive".equalsIgnoreCase(filterName)) {
            return vehicleRegistrationRepository.queryByInternalActive(idInternal, pageable)
                .map(vehicleRegistrationMapper::toDto);
        }
        return vehicleRegistrationRepository.queryNothing(pageable).map(vehicleRegistrationMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        } else if (command != null && "registerData".equalsIgnoreCase(command)) {
            registerNewData();
        }
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, VehicleRegistrationDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<VehicleRegistrationDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public VehicleRegistrationDTO changeVehicleRegistrationStatus(VehicleRegistrationDTO dto, Integer id) {
        if (dto != null) {
            VehicleDocumentRequirement e = vehicleRegistrationRepository.findOne(dto.getIdRequirement());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                    case 17:
                        vehicleRegistrationSearchRepository.delete(dto.getIdRequirement());
                        break;
                    default:
                        vehicleRegistrationSearchRepository.save(vehicleRegistrationMapper.toDto(e));
                }
				e = vehicleRegistrationRepository.save(e);
                return vehicleRegistrationMapper.toDto(e);
			}
		}
        return dto;
    }

    @Async
    public void buildIndex() {
        vehicleRegistrationSearchRepository.deleteAll();
        List<VehicleDocumentRequirement> items =  vehicleRegistrationRepository.findActiveVehicleRegistration();
        for (VehicleDocumentRequirement m: items) {
            if (m.getRequirementType() == null) {
                m.setRequirementType(requirementUtils.getRequirementType(BaseConstants.REQ_TYPE_VDR_RETAIL, "VDR Retail"));
                m = vehicleRegistrationRepository.save(m);
                log.debug("Update Data registrasi !...");
            }
            template.sendBody("direct:reindex:vehicleregistration", vehicleRegistrationMapper.toDto(m));
            log.debug("Put Data registrasi to save !...");
        }
    }

    public void buildIndexItem(@Body VehicleRegistrationDTO dto) {
        vehicleRegistrationSearchRepository.save(dto);
        log.debug("Data registrasi save !...");
    }

    @Async
    public void registerNewData() {
        List<VehicleDocumentRequirement> items =  vehicleRegistrationRepository.queryImportedVDR();
        for (VehicleDocumentRequirement m: items) {
            if (m.getRequirementType() == null) {
                m.setRequirementType(requirementUtils.getRequirementType(BaseConstants.REQ_TYPE_VDR_EXTERNAL, "VDR External"));
                m = vehicleRegistrationRepository.save(m);
                log.debug("Update Data registrasi !...");
            }
            template.sendBody("direct:reindex:vehicleregistration", vehicleRegistrationMapper.toDto(m));
            log.debug("Put Data registrasi to save !...");
        }
    }


}
