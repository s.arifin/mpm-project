package id.atiila.service;

import id.atiila.domain.BaseCalendar;
import id.atiila.repository.BaseCalendarRepository;
import id.atiila.repository.search.BaseCalendarSearchRepository;
import id.atiila.service.dto.BaseCalendarDTO;
import id.atiila.service.mapper.BaseCalendarMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing BaseCalendar.
 * BeSmart Team
 */

@Service
@Transactional
public class BaseCalendarService {

    private final Logger log = LoggerFactory.getLogger(BaseCalendarService.class);

    private final BaseCalendarRepository baseCalendarRepository;

    private final BaseCalendarMapper baseCalendarMapper;

    private final BaseCalendarSearchRepository baseCalendarSearchRepository;

    public BaseCalendarService(BaseCalendarRepository baseCalendarRepository, BaseCalendarMapper baseCalendarMapper, BaseCalendarSearchRepository baseCalendarSearchRepository) {
        this.baseCalendarRepository = baseCalendarRepository;
        this.baseCalendarMapper = baseCalendarMapper;
        this.baseCalendarSearchRepository = baseCalendarSearchRepository;
    }

    /**
     * Save a baseCalendar.
     *
     * @param baseCalendarDTO the entity to save
     * @return the persisted entity
     */
    public BaseCalendarDTO save(BaseCalendarDTO baseCalendarDTO) {
        log.debug("Request to save BaseCalendar : {}", baseCalendarDTO);
        BaseCalendar baseCalendar = baseCalendarMapper.toEntity(baseCalendarDTO);
        baseCalendar = baseCalendarRepository.save(baseCalendar);
        BaseCalendarDTO result = baseCalendarMapper.toDto(baseCalendar);
        baseCalendarSearchRepository.save(baseCalendar);
        return result;
    }

    /**
     * Get all the baseCalendars.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BaseCalendarDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BaseCalendars");
        return baseCalendarRepository.findAll(pageable)
            .map(baseCalendarMapper::toDto);
    }

    /**
     * Get one baseCalendar by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BaseCalendarDTO findOne(Integer id) {
        log.debug("Request to get BaseCalendar : {}", id);
        BaseCalendar baseCalendar = baseCalendarRepository.findOne(id);
        return baseCalendarMapper.toDto(baseCalendar);
    }

    /**
     * Delete the baseCalendar by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete BaseCalendar : {}", id);
        baseCalendarRepository.delete(id);
        baseCalendarSearchRepository.delete(id);
    }

    /**
     * Search for the baseCalendar corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BaseCalendarDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of BaseCalendars for query {}", query);
        Page<BaseCalendar> result = baseCalendarSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(baseCalendarMapper::toDto);
    }

    @Transactional(readOnly = true)
    public BaseCalendarDTO processExecuteData(Integer id, String param, BaseCalendarDTO dto) {
        BaseCalendarDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional(readOnly = true)
    public Set<BaseCalendarDTO> processExecuteListData(Integer id, String param, Set<BaseCalendarDTO> dto) {
        Set<BaseCalendarDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }


    @Async
    public void buildIndex() {
        baseCalendarSearchRepository.deleteAll();
        List<BaseCalendar> baseCalendars =  baseCalendarRepository.findAll();
        for (BaseCalendar b: baseCalendars) {
            baseCalendarSearchRepository.save(b);
            log.debug("Data Base Calendar save !...");
        }
    }

}
