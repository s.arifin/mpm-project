package id.atiila.service;

import id.atiila.domain.*;
import id.atiila.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RequirementUtils {

    @Autowired
    private RequirementTypeRepository requirementTypeRepository;

    @Transactional
    public RequirementType getRequirementType(Integer id, String description) {
        RequirementType requirementType = requirementTypeRepository.findOne(id);
        if (requirementType == null) {
            requirementType = new RequirementType();
            requirementType.setIdRequirementType(id);
            requirementType.setDescription(description);
            requirementType = requirementTypeRepository.saveAndFlush(requirementType);
        }
        return requirementType;
    }

}
