package id.atiila.service;

import id.atiila.domain.Container;
import id.atiila.domain.ContainerType;
import id.atiila.domain.Facility;
import id.atiila.repository.ContainerRepository;
import id.atiila.repository.ContainerTypeRepository;
import id.atiila.repository.FacilityRepository;
import id.atiila.repository.search.ContainerSearchRepository;
import id.atiila.service.dto.ContainerDTO;
import id.atiila.service.mapper.ContainerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing Container.
 * BeSmart Team
 */

@Service
@Transactional
public class ContainerService {

    private final Logger log = LoggerFactory.getLogger(ContainerService.class);

    private final ContainerRepository containerRepository;

    private final ContainerMapper containerMapper;

    private final ContainerSearchRepository containerSearchRepository;

    @Autowired
    private FacilityRepository facilityRepository;

    @Autowired
    private MasterNumberingService numbering;

    @Autowired
    private ContainerTypeRepository containerTypeRepository;

    public ContainerService(ContainerRepository containerRepository, ContainerMapper containerMapper, ContainerSearchRepository containerSearchRepository) {
        this.containerRepository = containerRepository;
        this.containerMapper = containerMapper;
        this.containerSearchRepository = containerSearchRepository;
    }

    /**
     * Save a container.
     *
     * @param containerDTO the entity to save
     * @return the persisted entity
     */
    public ContainerDTO save(ContainerDTO containerDTO) {
        log.debug("Request to save Container : {}", containerDTO);
        Container container = containerMapper.toEntity(containerDTO);

        Boolean isNew = container.getContainerCode() == null;
        if (isNew) {
            String newValue = null;
            while (newValue == null || containerRepository.findOneByContainerCode(newValue) != null) {
                newValue = numbering.nextValue("idcontainer", 90000l).toString();
            }
            container.setContainerCode(newValue);
        }

        container = containerRepository.save(container);
        ContainerDTO result = containerMapper.toDto(container);
        containerSearchRepository.save(container);
        return result;
    }

    /**
     * Get all the containers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ContainerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Containers");
        return containerRepository.findAll(pageable)
            .map(containerMapper::toDto);
    }

    /**
     * Get one container by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ContainerDTO findOne(UUID id) {
        log.debug("Request to get Container : {}", id);
        Container container = containerRepository.findOne(id);
        return containerMapper.toDto(container);
    }

    /**
     * Delete the container by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Container : {}", id);
        containerRepository.delete(id);
        containerSearchRepository.delete(id);
    }

    /**
     * Search for the container corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ContainerDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of Containers for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idFacility = request.getParameter("idFacility");
        String idContainerType = request.getParameter("idContainerType");

        if (idFacility != null) {
            q.withQuery(matchQuery("facility.idFacility", idFacility));
        }
        else if (idContainerType != null) {
            q.withQuery(matchQuery("containerType.idContainerType", idContainerType));
        }

        Page<Container> result = containerSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(containerMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ContainerDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ContainerDTO");
        String idFacility = request.getParameter("idFacility");
        String idContainerType = request.getParameter("idContainerType");

        if (idFacility != null) {
            return containerRepository.findByIdFacility(UUID.fromString(idFacility), pageable).map(containerMapper::toDto);
        }
        else if (idContainerType != null) {
            return containerRepository.findByIdContainerType(Integer.valueOf(idContainerType), pageable).map(containerMapper::toDto);
        }

        return containerRepository.findNothing(pageable).map(containerMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, ContainerDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<ContainerDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public List<ContainerDTO> findByFacilityId(UUID facilityid){
        List<Container> dto = containerRepository.findAllByFacilityIdFacility(facilityid);
        return containerMapper.toDto(dto);
    }

    @Async
    public void buildIndex() {
        containerSearchRepository.deleteAll();
        List<Container> containers =  containerRepository.findAll();
        for (Container s: containers) {
            containerSearchRepository.save(s);
            log.debug("Data Container save !...");
        }
    }

}
