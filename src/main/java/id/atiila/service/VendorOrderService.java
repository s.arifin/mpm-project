package id.atiila.service;

import id.atiila.domain.VendorOrder;
import id.atiila.repository.VendorOrderRepository;
import id.atiila.repository.search.VendorOrderSearchRepository;
import id.atiila.service.dto.VendorOrderDTO;
import id.atiila.service.mapper.VendorOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing VendorOrder.
 * BeSmart Team
 */

@Service
@Transactional
public class VendorOrderService {

    private final Logger log = LoggerFactory.getLogger(VendorOrderService.class);

    private final VendorOrderRepository vendorOrderRepository;

    private final VendorOrderMapper vendorOrderMapper;

    private final VendorOrderSearchRepository vendorOrderSearchRepository;

    public VendorOrderService(VendorOrderRepository vendorOrderRepository, VendorOrderMapper vendorOrderMapper, VendorOrderSearchRepository vendorOrderSearchRepository) {
        this.vendorOrderRepository = vendorOrderRepository;
        this.vendorOrderMapper = vendorOrderMapper;
        this.vendorOrderSearchRepository = vendorOrderSearchRepository;
    }

    /**
     * Save a vendorOrder.
     *
     * @param vendorOrderDTO the entity to save
     * @return the persisted entity
     */
    public VendorOrderDTO save(VendorOrderDTO vendorOrderDTO) {
        log.debug("Request to save VendorOrder : {}", vendorOrderDTO);
        VendorOrder vendorOrder = vendorOrderMapper.toEntity(vendorOrderDTO);
        vendorOrder = vendorOrderRepository.save(vendorOrder);
        VendorOrderDTO result = vendorOrderMapper.toDto(vendorOrder);
        vendorOrderSearchRepository.save(vendorOrder);
        return result;
    }

    /**
     * Get all the vendorOrders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VendorOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VendorOrders");
        return vendorOrderRepository.findActiveVendorOrder(pageable)
            .map(vendorOrderMapper::toDto);
    }

    /**
     * Get one vendorOrder by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public VendorOrderDTO findOne(UUID id) {
        log.debug("Request to get VendorOrder : {}", id);
        VendorOrder vendorOrder = vendorOrderRepository.findOne(id);
        return vendorOrderMapper.toDto(vendorOrder);
    }

    /**
     * Delete the vendorOrder by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete VendorOrder : {}", id);
        vendorOrderRepository.delete(id);
        vendorOrderSearchRepository.delete(id);
    }

    /**
     * Search for the vendorOrder corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VendorOrderDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of VendorOrders for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idVendor = request.getParameter("idVendor");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");

        if (filterName != null) {
        }
        Page<VendorOrder> result = vendorOrderSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(vendorOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<VendorOrderDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered VendorOrderDTO");
        String idVendor = request.getParameter("idVendor");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");

        return vendorOrderRepository.findByParams(idVendor, idInternal, idBillTo, pageable)
            .map(vendorOrderMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public VendorOrderDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        VendorOrderDTO r = null;
        return r;
    }

    @Transactional
    public Set<VendorOrderDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<VendorOrderDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public VendorOrderDTO changeVendorOrderStatus(VendorOrderDTO dto, Integer id) {
        if (dto != null) {
			VendorOrder e = vendorOrderRepository.findOne(dto.getIdOrder());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        vendorOrderSearchRepository.delete(dto.getIdOrder());
                        break;
                    default:
                        vendorOrderSearchRepository.save(e);
                }
				vendorOrderRepository.save(e);
			}
		}
        return dto;
    }
}
