package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.atiila.domain.Suspect;
import id.atiila.repository.PersonRepository;
import id.atiila.repository.PostalAddressRepository;
import id.atiila.repository.SuspectRepository;
import id.atiila.repository.search.SuspectSearchRepository;
import id.atiila.service.dto.SuspectDTO;
import id.atiila.service.mapper.SuspectMapper;

import javax.servlet.http.HttpServletRequest;

/**
 * Service Implementation for managing Suspect.
 * BeSmart Team
 */

@Service
@Transactional
public class SuspectService {

    private final Logger log = LoggerFactory.getLogger(SuspectService.class);

    private final SuspectRepository suspectRepository;

    private final SuspectMapper suspectMapper;

    private final SuspectSearchRepository suspectSearchRepository;

    @Autowired
    private PersonService personSvc;

    @Autowired
    private PersonRepository personRepo;

    @Autowired
    private PostalAddressRepository postalRepo;

    @Autowired
    private MasterNumberingService numberSvc;

    public SuspectService(SuspectRepository suspectRepository, SuspectMapper suspectMapper, SuspectSearchRepository suspectSearchRepository) {
        this.suspectRepository = suspectRepository;
        this.suspectMapper = suspectMapper;
        this.suspectSearchRepository = suspectSearchRepository;
    }

    /**
     * Save a suspect.
     *
     * @param suspectDTO the entity to save
     * @return the persisted entity
     */
    public SuspectDTO save(SuspectDTO suspectDTO) {
        log.debug("Request to save Suspect : {}", suspectDTO);
        Suspect suspect = suspectMapper.toEntity(suspectDTO);
        suspect = suspectRepository.save(suspect);
        SuspectDTO result = suspectMapper.toDto(suspect);
        suspectSearchRepository.save(suspect);
        return result;
    }

    /**
     *  Get all the suspects.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SuspectDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Suspects");
        return suspectRepository.findActiveSuspect(pageable)
            .map(suspectMapper::toDto);
    }

    /**
     *  Get one suspect by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SuspectDTO findOne(UUID id) {
        log.debug("Request to get Suspect : {}", id);
        Suspect suspect = suspectRepository.findOne(id);
        return suspectMapper.toDto(suspect);
    }

    /**
     *  Delete the  suspect by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Suspect : {}", id);
        suspectRepository.delete(id);
        suspectSearchRepository.delete(id);
    }

    /**
     * Search for the suspect corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SuspectDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Suspects for query {}", query);
        Page<Suspect> result = suspectSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(suspectMapper::toDto);
    }

    public SuspectDTO changeSuspectStatus(SuspectDTO dto, Integer id) {
        if (dto != null) {
			Suspect e = suspectRepository.findOne(dto.getIdSuspect());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        suspectSearchRepository.delete(dto.getIdSuspect());
                        break;
                    default:
                        suspectSearchRepository.save(e);
                }
				suspectRepository.save(e);
			}
		}
        return dto;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }


    public void buildIndex() {
        suspectSearchRepository.deleteAll();
        List<Suspect> suspects =  suspectRepository.findAll();
        for (Suspect m: suspects) {
            suspectSearchRepository.save(m);
            log.debug("Data suspect save !...");
        }
    }
}
