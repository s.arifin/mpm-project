package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.base.executeprocess.SalesUnitRequirementConstants;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.UnitOrderItemSearchRepository;
import id.atiila.repository.search.VehicleSalesOrderSearchRepository;
import id.atiila.service.dto.*;
import id.atiila.service.mapper.UnitOrderItemMapper;
import id.atiila.service.mapper.VehicleSalesOrderMapper;
import id.atiila.service.pto.OrderPTO;
import id.atiila.service.util.CustomTextUtils;
import org.elasticsearch.index.query.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;

import static id.atiila.base.BaseConstants.STATUS_CANCEL;
import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing VehicleSalesOrder.
 * BeSmart Team
 */

@Service
@Transactional
public class VehicleSalesOrderService {

    private final Logger log = LoggerFactory.getLogger(VehicleSalesOrderService.class);

    private final VehicleSalesOrderRepository vehicleSalesOrderRepository;

    private final VehicleSalesOrderMapper vehicleSalesOrderMapper;

    private final VehicleSalesOrderSearchRepository vehicleSalesOrderSearchRepository;


    @Autowired
    private SalesUnitRequirementRepository salesUnitRequirementRepository;

    @Autowired
    private VehicleSalesBillingService vsbService;

    @Autowired
    private MasterNumberingService masterNumberingService;

    @Autowired
    private CustomerUtils customerUtils;

    @Autowired
    protected ShipmentOutgoingService doService;

    @Autowired
    private BillingItemRepository billingItemRepository;

    @Autowired
    private ShipmentItemRepository shipmentItemRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private OrderTypeRepository orderTypeRepository;

    @Autowired
    private ShipmentOutgoingService shipmentOutgoingService;

    @Autowired
    private InternalRepository internalRepository;

    @Autowired
    private SalesUnitRequirementService salesUnitRequirementService;

    @Autowired
    private OrderUtils orderUtils;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private ActivitiProcessor activitiProcessor;

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private UnitOrderItemSearchRepository unitOrderItemSearchRepository;

    @Autowired
    private UnitOrderItemMapper unitOrderItemMapper;

    @Autowired
    private OrderItemService orderItemService;

    @Autowired
    private SalesmanRepository salesmanRepository;

    @Autowired
    private BillingRepository billingRepository;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private InternalBankMapingRepository internalBankMapingRepository;


    public VehicleSalesOrderService(VehicleSalesOrderRepository vehicleSalesOrderRepository,
                                    VehicleSalesOrderMapper vehicleSalesOrderMapper,
                                    VehicleSalesOrderSearchRepository vehicleSalesOrderSearchRepository) {
        this.vehicleSalesOrderRepository = vehicleSalesOrderRepository;
        this.vehicleSalesOrderMapper = vehicleSalesOrderMapper;
        this.vehicleSalesOrderSearchRepository = vehicleSalesOrderSearchRepository;
    }

    /**
     * Save a vehicleSalesOrder.
     *
     * @param vehicleSalesOrderDTO the entity to save
     * @return the persisted entity
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public VehicleSalesOrderDTO save(VehicleSalesOrderDTO vehicleSalesOrderDTO) {
        log.debug("Request to save VehicleSalesOrder : {}", vehicleSalesOrderDTO);
        VehicleSalesOrder vehicleSalesOrder = vehicleSalesOrderMapper.toEntity(vehicleSalesOrderDTO);
        vehicleSalesOrder = vehicleSalesOrderRepository.save(vehicleSalesOrder);
        VehicleSalesOrderDTO result = assignSUR(vehicleSalesOrderMapper.toDto(vehicleSalesOrder));
        vehicleSalesOrderSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the vehicleSalesOrders.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VehicleSalesOrders");
        Internal internal = partyUtils.getCurrentInternal();
        return vehicleSalesOrderRepository.queryAllVso(internal != null ? internal.getIdInternal(): null, pageable)
            .map(vehicleSalesOrderMapper::toDto)
            .map(this::assignSUR)
            .map(this::assingOrderType);
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> findAllVsoForManualMatching(String idInternal, Pageable pageable) {
        log.debug("Request to get all VehicleSalesOrders for manual matching");
        return vehicleSalesOrderRepository.queryAllVsoForManualMatching(idInternal, pageable)
            .map(vehicleSalesOrderMapper::toDto)
            .map(this::assignSUR)
            .map(this::assingOrderType);
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> findAllVsoForApproveKacab(String idInternal, Pageable pageable) {
        log.debug("Request to get all VehicleSalesOrders for manual matching");
        return vehicleSalesOrderRepository.queryForApprovalKacab(idInternal, pageable)
            .map(vehicleSalesOrderMapper::toDto)
            .map(this::assignSUR)
            .map(this::assingOrderType);
    }
    /**
     *  Get all the Bill billFinanceCompany.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> findAllBillFinanceCompany(String idInternal, Pageable pageable) {
        log.debug("Request to get all BillFinanceCompany");
        return vehicleSalesOrderRepository.queryActiveLeasingInvoice(idInternal, pageable)
            .map(vehicleSalesOrderMapper::toDto)
            .map(this::assignSUR)
            .map(this::getIVUNumber)
            .map(this::assingOrderType);
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> findAllBillFinanceCompanySubmit(String idInternal, Pageable pageable) {
        log.debug("Request to get all BillFinanceCompany");
        return vehicleSalesOrderRepository.queryActiveLeasingInvoiceSubmit(idInternal, pageable)
            .map(vehicleSalesOrderMapper::toDto)
            .map(this::assignSUR)
            .map(this::getIVUNumber)
            .map(this::assingOrderType);
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> findFilterBy(Pageable pageable, OrderPTO param) {
        log.debug("Request to get all VSO Filters");
        return vehicleSalesOrderRepository.findByFilter(pageable, param)
            .map(vehicleSalesOrderMapper::toDto)
            .map(this::assignSUR)
            .map(this::assingOrderType);
    }

//    @Transactional(readOnly = true)
//    public Page<VehicleSalesOrderDTO> findFilterAdminHandling(Pageable pageable, OrderPTO param) {
//        log.debug("Request to get all VSO Filters");
//        return vehicleSalesOrderRepository.findFiltersAdminHandling(pageable, param)
//            .map(vehicleSalesOrderMapper::toDto)
//            .map(this::getIVUNumber)
//            .map(this::assignSUR);
//    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> findFilterByForVSB(Pageable pageable, OrderPTO param) {
        log.debug("Request to get all VSO Filters");
        return vehicleSalesOrderRepository.findByFilter(pageable, param)
            .map(vehicleSalesOrderMapper::toDto)
            .map(this::assignSUR)
            .map(this::assingOrderType);
    }

    /**
     *  Get one vehicleSalesOrder by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public VehicleSalesOrderDTO findOne(UUID id) {
        log.debug("Request to get VehicleSalesOrder : {}", id);
        VehicleSalesOrder vehicleSalesOrder = vehicleSalesOrderRepository.findOne(id);
        VehicleSalesOrderDTO dto = vehicleSalesOrderMapper.toDto(vehicleSalesOrder);
        assignSUR(dto);
        assingOrderType(dto);
        return getIVUNumber(dto);
    }

    /**
     *  Get one vehicleSalesOrder by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SalesUnitRequirementDTO findSurByOrderId(UUID id, String idInternal) {
        log.debug("Request to get VehicleSalesOrder : {}", id);
        VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(id);
        if (vso == null) return null;
        return salesUnitRequirementService.findOne(vso.getIdSalesUnitRequirement());
    }

    /**
     *  Delete the  vehicleSalesOrder by id.
     *
     *  @param id the id of the entity
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(UUID id) {
        log.debug("Request to delete VehicleSalesOrder : {}", id);
        vehicleSalesOrderRepository.delete(id);
        vehicleSalesOrderSearchRepository.delete(id);
    }

    /**
     * Search for the vehicleSalesOrder corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> search(HttpServletRequest request, String query, Pageable pageable) {

        log.debug("Request to search for a page of VSOOOO for query {}", query);
        String internalId = partyUtils.getCurrentInternal().getIdInternal();

        query = CustomTextUtils.elasticSearchEscapeString(query);
        log.debug("result of escaping string" + query);

        QueryBuilder boolQueryBuilder = queryStringQuery("*"+query+"*");

        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder();
        q.withQuery(boolQuery().must(boolQueryBuilder).filter(
            boolQuery().must(matchQuery("internalId", internalId)))
        );

        Page<VehicleSalesOrderDTO> result = vehicleSalesOrderSearchRepository.search(q.build().getQuery(), pageable);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> searchAdminHandling(HttpServletRequest request, String query, Pageable pageable) {

        log.debug("Request to search for a page of VSOOOO for query {}", query);
        String internalId = partyUtils.getCurrentInternal().getIdInternal();

        query = CustomTextUtils.elasticSearchEscapeString(query);
        log.debug("result of escaping string" + query);

        QueryBuilder boolQueryBuilder = queryStringQuery("*"+query+"*");

        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder();
        q.withQuery(boolQuery().must(boolQueryBuilder).filter(
            boolQuery().must(matchQuery("internalId", internalId)))
        );

        Page<VehicleSalesOrderDTO> result = vehicleSalesOrderSearchRepository.search(q.build().getQuery(), pageable);
        return result;
    }

    @Transactional(readOnly = true)
    void adminHandlingCheck(VehicleSalesOrderDTO dto) {

        VehicleSalesOrder v = vehicleSalesOrderRepository.findOne(dto.getIdOrder());
        v.setDateBASTUnit(dto.getDateBASTUnit());
        v.setDateCoverNote(dto.getDateCoverNote());
        v.setDateCustomerReceipt(dto.getDateCustomerReceipt());
        v.setDateSwipeMachine(dto.getDateSwipeMachine());
        v.setDateTakePicture(dto.getDateTakePicture());
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public VehicleSalesOrderDTO update (VehicleSalesOrderDTO vehicleSalesOrderDTO) {
        log.debug("Request to save VehicleSalesOrder : {}", vehicleSalesOrderDTO);
        VehicleSalesOrder vehicleSalesOrder = vehicleSalesOrderMapper.toEntity(vehicleSalesOrderDTO);
        vehicleSalesOrder = vehicleSalesOrderRepository.save(vehicleSalesOrder);
        VehicleSalesOrderDTO result = assignSUR(vehicleSalesOrderMapper.toDto(vehicleSalesOrder));
        vehicleSalesOrderSearchRepository.save(result);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered Vehicle Sales Order");
        Internal internal = partyUtils.getCurrentInternal();
        String idCustomer = request.getParameter("idCustomer");
        String orderNumber = request.getParameter("orderNumber");
        String billingNumber = request.getParameter("billingNumber");
        UUID idBilling = UUID.fromString(request.getParameter("idBilling"));


        if (idCustomer != null) {
            return vehicleSalesOrderRepository.queryByCustomer(idCustomer, pageable).map(vehicleSalesOrderMapper::toDto);
        } else if (orderNumber != null) {
            return vehicleSalesOrderRepository.queryByOrderNumber(orderNumber, pageable).map(vehicleSalesOrderMapper::toDto);
        } else if (billingNumber != null) {
            return vehicleSalesOrderRepository.queryByNoIvu(billingNumber, pageable).map(vehicleSalesOrderMapper::toDto);
        } else if (idBilling != null){
            return vehicleSalesOrderRepository.findPageByBillingId(idBilling, pageable).map(vehicleSalesOrderMapper::toDto).map(this::assignSUR).map(this::assingOrderType);
        }
        return vehicleSalesOrderRepository.queryNothing(pageable).map(vehicleSalesOrderMapper::toDto)
            .map(this::assignSUR);
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> toSearch(HttpServletRequest request, Pageable pageable) {
        Internal internal = partyUtils.getCurrentInternal();
        String queryfor = request.getParameter("queryFor");
        String data = request.getParameter("data");
        String dataLike = '%'+ data +'%';
        log.debug("to search data" +data);
        log.debug("to search query for" +dataLike);
        if (data != null) {
            return vehicleSalesOrderRepository.findToReindex(dataLike, internal.getIdInternal(), pageable).map(vehicleSalesOrderMapper::toDto).map(this::assignSUR);
        } else {
            return vehicleSalesOrderRepository.queryNothing(pageable).map(vehicleSalesOrderMapper::toDto)
                .map(this::assignSUR);
        }
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> toSearchAH(HttpServletRequest request, Pageable pageable) {
        Internal internal = partyUtils.getCurrentInternal();
        String queryfor = request.getParameter("queryFor");
        String data = request.getParameter("data");
        String dataLike = '%'+ data +'%';
        log.debug("to search data" +data);
        log.debug("to search query for" +dataLike);
        if (data != null) {
            return vehicleSalesOrderRepository.findtoReIndexAH(dataLike, internal.getIdInternal(), pageable).map(vehicleSalesOrderMapper::toDto).map(this::getIVUNumber).map(this::assignSUR);
        }
        return vehicleSalesOrderRepository.queryNothing(pageable).map(vehicleSalesOrderMapper::toDto)
            .map(this::assignSUR);
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> toSearchAHSubmit(HttpServletRequest request, Pageable pageable) {
        Internal internal = partyUtils.getCurrentInternal();
        String queryfor = request.getParameter("queryFor");
        String data = request.getParameter("data");
        String dataLike = '%'+ data +'%';
        log.debug("to search data" +data);
        log.debug("to search query for" +dataLike);
        if (data != null) {
            return vehicleSalesOrderRepository.findtoReIndexAHSubmit(dataLike, internal.getIdInternal(), pageable).map(vehicleSalesOrderMapper::toDto).map(this::getIVUNumber).map(this::assignSUR);
        }
        return vehicleSalesOrderRepository.queryNothing(pageable).map(vehicleSalesOrderMapper::toDto)
            .map(this::assignSUR);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    protected void updateVerifikasi(UUID id, int verifyBy, Integer veriValue, String noteVeri ) {
        VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(id);
        switch (verifyBy){
            case 1:
                //admin sales
                if (veriValue==2) vso.setAdminSalesNotApprvNote(noteVeri);
                vso.setAdminSalesVerifyValue(veriValue);
                vso.setAdminSalesVeriDate(LocalDate.now());
                log.debug("verifi value " + vso.getAdminSalesVerifyValue() );
                log.debug("admin sales not approve " + vso.getAdminSalesNotApprvNote());
                break;
            case 2:
                // afc
                if (veriValue==2) vso.setAfcNotApprvNote(noteVeri);
                vso.setAfcVerifyValue(veriValue);
                vso.setAfcVeriDate(LocalDate.now());
                break;
        }
        vehicleSalesOrderRepository.save(vso);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void doProcess(VehicleSalesOrder v, OrderItem oi) {
        Map<String, Object> vars = activitiProcessor.getVariables("orderItem", oi);
        vars.put("vso", v);
        activitiProcessor.startProcess("vso-item-process", oi.getIdOrderItem().toString(), vars);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public VehicleSalesOrderDTO processExecuteData(Integer id, String param, VehicleSalesOrderDTO dto) {
        VehicleSalesOrderDTO r = dto;
        VehicleSalesOrder v = vehicleSalesOrderRepository.findOne(dto.getIdOrder());

        String reqNosin = null;
        String reqNoka = null;

        log.info("exec proses id=" + id.toString()+ " vso => " + r);

        if ((r != null && !(v.getCurrentStatus().intValue() == BaseConstants.STATUS_COMPLETED)) ||
            (r != null && !(v.getCurrentStatus().intValue() == BaseConstants.STATUS_CANCEL)) ||
            (r != null && !(v.getCurrentStatus().intValue() == BaseConstants.STATUS_VSO_MATCHING))){
            switch (id) {
                case BaseConstants.STATUS_APPROVED:
                    r = this.changeVehicleSalesOrderStatus(r, BaseConstants.STATUS_APPROVED);
                    break;
                case BaseConstants.STATUS_COMPLETED:
                    doProcess(v, v.getDetails().iterator().next());
                    break;
                case BaseConstants.STATUS_VSO_APPROVAL_ADMIN_HANDLING:
                    r = this.changeVehicleSalesOrderStatus(r, BaseConstants.STATUS_COMPLETED);
                    break;
                case BaseConstants.STATUS_VSO_NOT_APPROVE:
                    this.vso_ganti(r, BaseConstants.STATUS_VSO_NOT_APPROVE);
                    r = this.changeVehicleSalesOrderStatus(r, BaseConstants.STATUS_VSO_NOT_APPROVE);
                    break;
                case BaseConstants.STATUS_CANCEL:
                    this.vso_cancel(r);
                    r = this.changeVehicleSalesOrderStatus(r, BaseConstants.STATUS_CANCEL);
                    break;
                case BaseConstants.STATUS_VSO_AFC_VERIFIKASI:
                    log.debug("masuk ke afc verifikasi");
//                    updateVerifikasi(r.getIdOrder(), 2 , r.getAfcVerifyValue(), r.getAfcNotApprvNote() );
                    validasiApproval(r, BaseConstants.STATUS_VSO_AFC_VERIFIKASI);
                    break;
                case BaseConstants.STATUS_VSO_ADMIN_SALES_VERIFIKASI:
                    validasiApproval(r, BaseConstants.STATUS_VSO_ADMIN_SALES_VERIFIKASI);
                    break;
                case BaseConstants.STATUS_VSO_VERIFIKASI:
                    this.statusVSOVerifikasi(r);
                    break;
                case BaseConstants.STATUS_VSO_MATCHING:
                    this.autoMatching(r);
                    r = this.changeVehicleSalesOrderStatus(r, BaseConstants.STATUS_VSO_MATCHING);
                    break;
                case BaseConstants.STATUS_VSO_MATCHING_STATUS:
                    r = this.changeVehicleSalesOrderStatus(r, BaseConstants.STATUS_VSO_MATCHING);
                    break;
                case 740:
                    // manual matching
                    this.statusVSOmatchingManualDone(r);
                    r = this.changeVehicleSalesOrderStatus(r, BaseConstants.STATUS_VSO_MATCHING);
                    break;
                case BaseConstants.STATUS_VSO_CANCEL_APPROVE_KACAB:
                    this.cancelAutoMatching(r);
                    r = this.changeVehicleSalesOrderStatus(r, BaseConstants.STATUS_DRAFT);
                    break;
                case BaseConstants.STATUS_VSO_ISSUBFINCO:
                    subsidiFincoCheck(r);
                    break;
                case BaseConstants.STATUS_VSO_GANTI:
                    this.vso_ganti(r, BaseConstants.STATUS_VSO_GANTI);
                    r = this.changeVehicleSalesOrderStatus(r, BaseConstants.STATUS_CANCEL);
                    break;
                case BaseConstants.STATUS_VSO_AFC_NOT_APPROVE:
                    v.setAfcVeriDate(LocalDate.now());
                    v.setAfcVerifyValue(r.getAfcVerifyValue());
                    v.setAfcNotApprvNote(r.getAfcNotApprvNote());
                    v = vehicleSalesOrderRepository.save(v);
                    break;
                case BaseConstants.STATUS_VSO_ADMINHANDLING_UPDATE:
                    adminHandlingCheck(r);
                    break;
                default:
                    break;
            }
		} else if ((v.getCurrentStatus().intValue() == BaseConstants.STATUS_COMPLETED) ||
                    (v.getCurrentStatus().intValue() == BaseConstants.STATUS_CANCEL) ||
                    (v.getCurrentStatus().intValue() == BaseConstants.STATUS_VSO_MATCHING)){
            r = vehicleSalesOrderMapper.toDto(v);

        }
        return r;
    }

    @Transactional(readOnly = true)
    void subsidiFincoCheck(VehicleSalesOrderDTO dto) {

        VehicleSalesOrder v = vehicleSalesOrderRepository.findOne(dto.getIdOrder());

        v.setSubFincoyAsDp(dto.getSubFincoyAsDp());
        v.setRefundFinco(dto.getRefundFinco());
        v.setIdBiroJasa(dto.getIdBiroJasa());

        v = vehicleSalesOrderRepository.saveAndFlush(v);
    }



    @Transactional(propagation = Propagation.REQUIRED)
    public VehicleSalesOrderDTO validasiApproval(VehicleSalesOrderDTO dto, Integer id) {
        if (id == BaseConstants.STATUS_VSO_AFC_VERIFIKASI) {
            VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(dto.getIdOrder());
            if (vso.getAfcVerifyValue() == 1) {
                throw new DmsException("Data VSO Sudah di Approve AFC");
            }else {
                updateVerifikasi(dto.getIdOrder(), 2 , dto.getAfcVerifyValue(), dto.getAfcNotApprvNote() );
            }
        }
        if (id == BaseConstants.STATUS_VSO_ADMIN_SALES_VERIFIKASI) {
            updateVerifikasi(dto.getIdOrder(),1, dto.getAdminSalesVerifyValue(), dto.getAdminSalesNotApprvNote());
//            if(dto.getAdminSalesVerifyValue() == 1) {
//                updateVerifikasi(dto.getIdOrder(),1, dto.getAdminSalesVerifyValue(), dto.getAdminSalesNotApprvNote());
//            } else {
//                updateVerifikasi(dto.getIdOrder(),1, dto.getAdminSalesVerifyValue(), dto.getAdminSalesNotApprvNote());
//            }
        }
        return dto;
    }

    /**
     * VSO Done matching
     * - copy noka nosin dari requirement ke    order item
     * @param r
     */
    @Transactional(propagation = Propagation.REQUIRED)
    void statusVSOmatchingManualDone(VehicleSalesOrderDTO r){
        VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(r.getIdOrder());

        PageRequest pageRequest= new PageRequest(0,1);

        log.info("manuat matching done: get order id");
        OrderItem oi = orderItemRepository.findOrderItemByOrders(r.getIdOrder(), pageRequest).iterator().next();

        if ( oi != null){
            log.info("manuat matching done: get sur");
            SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(r.getSalesUnitRequirement().getId());

            if (sur != null){
                oi.setIdframe(sur.getRequestIdFrame());
                oi.setIdmachine(sur.getRequestIdMachine());
                oi = orderItemRepository.save(oi);
            }
            vso.setDoneMatching(true);
            log.debug("done matching " );

            vehicleSalesOrderRepository.save(vso);
        } else {
            new DmsException("Order id not found "+ r.getIdOrder());
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    void statusVSOVerifikasi(VehicleSalesOrderDTO r){
        log.info("vso verifikasi ");
        //  VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(r.getIdOrder());
        VehicleSalesOrder vsoUpdate = vehicleSalesOrderRepository.findOne(r.getId());
        vsoUpdate.setVrangsuran(r.getVrangsuran());
        vsoUpdate.setVrcatatan(r.getVrcatatan());
        vsoUpdate.setVrcatatantambahan(r.getVrcatatantambahan());
        vsoUpdate.setVrdpmurni(r.getVrdpmurni());
        vsoUpdate.setVriscod(r.getVriscod());
        vsoUpdate.setVrisverified(r.getVrisverified());
        vsoUpdate.setVrtglpengiriman(r.getVrtglpengiriman());
        vsoUpdate.setVrjampengiriman(r.getVrjampengiriman());
        vsoUpdate.setVrnama1(r.getVrnama1());
        vsoUpdate.setVrnama2(r.getVrnama2());
        vsoUpdate.setVrleasing(r.getVrleasing());
        vsoUpdate.setVrnamamarket(r.getVrnamamarket());
        vsoUpdate.setVrsisa(r.getVrsisa());
        vsoUpdate.setVrtahunproduksi(r.getVrtahunproduksi());
        vsoUpdate.setVrwarna(r.getVrwarna());
        vsoUpdate.setVrtandajadi(r.getVrtandajadi());
        vsoUpdate.setVrtipepenjualan(r.getVrtipepenjualan());
        vsoUpdate.setVrtglpembayaran(r.getVrtglpembayaran());
        vehicleSalesOrderRepository.save(vsoUpdate);
        log.info("verifikasi updated ");
    }

    @Transactional(propagation = Propagation.REQUIRED)
    void vso_cancel(VehicleSalesOrderDTO r){

        log.debug("vso_cancel:cancel SUR id frame id mach=>");
        // cancel vso
        SalesUnitRequirement surUpd = salesUnitRequirementRepository.findOne(r.getSalesUnitRequirement().getIdRequirement());
        surUpd.setRequestIdFrame(null);
        surUpd.setRequestIdMachine(null);
        salesUnitRequirementRepository.saveAndFlush(surUpd);

        // SUR status backto DRAFT
        log.debug("vso_cancel:exec reset SUR-> DRAFT ");
        SalesUnitRequirementDTO surDto = salesUnitRequirementService.findOne(r.getSalesUnitRequirement().getIdRequirement());
        salesUnitRequirementService.processExecuteData(SalesUnitRequirementConstants.CANCEL_VSO, null, surDto);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    void vso_ganti(VehicleSalesOrderDTO r, Integer id){
        log.debug("vso_cancel:vso get vso dto ==" + r + "== ");
        VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne( r.getId());

        log.debug("vso_cancel:vso get detil ==" + vso.getDetails().size() + "== ");

        // set null id frame id machine
        for(OrderItem oi : vso.getDetails()){
            log.debug("vso_cancel:cancel id frame id mach order id =>{}", oi );
            oi.setIdmachine(null);
            oi.setIdframe(null);
            orderItemRepository.save(oi);
        }


        // SUR status backto DRAFT ////// -->
        log.debug("vso_cancel:exec reset SUR-> DRAFT ");
        SalesUnitRequirementDTO surDto = salesUnitRequirementService.findOne(r.getIdSalesUnitRequirement());
        if (id == BaseConstants.STATUS_VSO_GANTI) {
            salesUnitRequirementService.processExecuteData(SalesUnitRequirementConstants.GANTI_VSO, null, surDto);
        } else if (id == BaseConstants.STATUS_VSO_NOT_APPROVE) {
            salesUnitRequirementService.processExecuteData(SalesUnitRequirementConstants.PROSES_GANTI_VSO_NOT_APPROVE, null, surDto);
        }
    }
    @Transactional(propagation = Propagation.REQUIRED)
    void vso_complete(VehicleSalesOrderDTO r, String reqNoka, String reqNosin){
        log.info("approve vso generate picking");
        log.info("approve vso generate shipment");
        //Buat Shipment
        ShipmentOutgoingDTO sdo = doService.buildFrom(r);
        log.info("approve vso generate invoice");
        //Buat Faktur
        vsbService.buildFrom(r);
        // verifikasi
        this.changeVehicleSalesOrderStatus(r, BaseConstants.STATUS_COMPLETED);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Set<VehicleSalesOrderDTO> processExecuteListData(Integer id, String param, Set<VehicleSalesOrderDTO> dto) {
        Set<VehicleSalesOrderDTO> r = dto;
        if (r != null) {
            switch (id) {
                case BaseConstants.STATUS_APPROVED:
                    break;
                case 18:
                    log.debug(" aaaaaaaaaaaaaaaaaaaaadminhandling");
                    for (VehicleSalesOrderDTO vehicleSalesOrderDTO: r) {
                        log.debug(" aa == ");
                        this.changeVehicleSalesOrderStatus(vehicleSalesOrderDTO, BaseConstants.STATUS_COMPLETED);
                    }
                    break;
                case 19:
                    updateFincoBAST(param, dto);
                    break;
            }
        }
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Set<VehicleSalesOrderDTO> updateFincoBAST(String param,Set<VehicleSalesOrderDTO> dto) {
        String internal = param;
        String bastNumber = null;
        ZonedDateTime dateDelivery = ZonedDateTime.now();

        Set<VehicleSalesOrderDTO> r = dto;
        if (dto.iterator().next().getBastFinco() == null) {
            bastNumber = masterNumberingService.findTransBastFincoNumberBy(param, "FIN");
        }
         for(VehicleSalesOrderDTO vehicleSalesOrderDTO : r) {
            if (vehicleSalesOrderDTO.getIdOrder() != null & vehicleSalesOrderDTO.getBastFinco() == null) {
                VehicleSalesOrder vso = vehicleSalesOrderRepository.findByIdRequirmentAndIdSaleType(vehicleSalesOrderDTO.getBillingNumber());
                log.debug("cek apakah data masuk" + vso);
                vso.setName(vehicleSalesOrderDTO.getName());
                vso.setIdentityNumber(vehicleSalesOrderDTO.getIdentityNumber());
                vso.setBastFinco(bastNumber);
                vso.setDateBastFinco(dateDelivery);
                vso.setIscompleted(true);


                vehicleSalesOrderRepository.save(vso);
            }
         }
         List<VehicleSalesOrderDTO> mainlist = new ArrayList<>(r);
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public VehicleSalesOrderDTO changeVehicleSalesOrderStatus(VehicleSalesOrderDTO dto, Integer id) {

        if (dto != null) {
            log.debug("vso chance vso status");
            VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(dto.getIdOrder());
            OrderItem o = vehicleSalesOrderRepository.findOrderItemByVSO(vso.getIdOrder());
            log.debug("orrrddderrrr ==== " +o);

            // Abaikan bila status sudah completed atau cancel
            if (vso.getCurrentStatus().equals(BaseConstants.STATUS_CANCEL) ||
                vso.getCurrentStatus().equals(BaseConstants.STATUS_COMPLETED)){
                return assignSUR(vehicleSalesOrderMapper.toDto(vso));
            }

            // jika approve kacab, tambah note kacab
            if (id == BaseConstants.STATUS_APPROVED){
                vso.setKacabnote(dto.getKacabnote());
            }

            if (!vso.getCurrentStatus().equals(id)) {
                log.debug("vso chance vso status =[" + id + "] ");
                vso.setStatus(id);
                vso = vehicleSalesOrderRepository.save(vso);
                dto = vehicleSalesOrderMapper.toDto(vso);

            }
        }
        log.debug("vso status changed ");
        return assignSUR(dto);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public VehicleSalesOrderDTO buildFromSUR(SalesUnitRequirement s) {
        VehicleSalesOrder vsoDraft = vehicleSalesOrderRepository.queryFindDraftVSO(s.getIdRequirement());
        VehicleSalesOrder vso = null;
        if (vsoDraft == null ) {
            vso = buildVSOFromSUR(s);
        } else {
            throw new DmsException("DRAFT VSO ini Sebelumnya Sudah diSubmit ke VSO");
        }

        return assignSUR(vehicleSalesOrderMapper.toDto(vso));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public VehicleSalesOrder buildVSOFromSUR(SalesUnitRequirement s) {

        Internal internal = internalRepository.findByIdInternal(s.getInternal().getIdInternal());
        List<InternalBankMaping> internalBankMaping = internalBankMapingRepository.FindByIdRoot(internal.getRoot().getIdInternal());
        log.debug("build from sur " + s);
        log.info("buildFromSUR:get ship to" + s.getCustomer().getIdCustomer());
        ShipTo sp = customerUtils.getShipTo(s.getCustomer());

        log.info("buildFromSUR:get product" + s.getProduct().getIdProduct());
        Product pr = vehicleSalesOrderRepository.findOneByIdProduct(s.getProduct().getIdProduct());

        VehicleSalesOrder v = new VehicleSalesOrder();

        if (s.getLeasingCompany() != null)
            v.setLeasing(s.getLeasingCompany());

        log.info("buildFromSUR:vso = new vso ");
        OrderType orderType = orderTypeRepository.findOne(BaseConstants.ORDER_TYPE_VSO);
        v.setDateOrder(ZonedDateTime.now());
        v.setOrderType(orderType);
        v.setCustomer(s.getCustomer());
        v.setInternal(s.getInternal());
        v.setSaleType(s.getSaleType());


        v.setIdSalesUnitRequirement(s.getIdRequirement());
        v.setBillTo(s.getBillTo());
        v.setIdLeasingStatusPayment(0);
        v.setInternal(s.getInternal());
        v.setIdSaleType(s.getSaleType().getIdSaleType());
        v.setOrderNumber(s.getRequirementNumber());
        v.setIdSalesman(s.getSalesman().getIdPartyRole());
        v = vehicleSalesOrderRepository.saveAndFlush(v);
        vehicleSalesOrderSearchRepository.save(assignSUR(vehicleSalesOrderMapper.toDto(v)));
//        indexVsoToUnitOrderItem(v);
        log.info("buildFromSUR:crete order item obj" );

        OrderItem o = new OrderItem();
        o.setIdShipTo(sp.getIdShipTo());
        o.setIdProduct(s.getProduct().getIdProduct());
        o.setItemDescription(pr.getName());
        o.setIdFeature(s.getColor().getIdFeature());

        //TODO kasus salah harga
        if(!internalBankMaping.isEmpty() && internalBankMaping.iterator().next().getIsppn() == 0){
            o.setUnitPrice(s.getHetprice().doubleValue());
        } else {
            o.setUnitPrice(s.getHetprice().doubleValue() / 1.1);
        }
        o.setDiscount(s.getSubsown());
        o.setDiscountFinCoy(s.getSubsfincomp());
        o.setDiscountatpm(s.getSubsahm());
        o.setDiscountMD(s.getSubsmd());
        o.setBbn(s.getBbnprice());

        BigDecimal otr = s.getUnitPrice();
        BigDecimal bbnPrice = s.getBbnprice();
        BigDecimal OfftheRoad = otr.subtract(bbnPrice);
        BigDecimal salesAmount = OfftheRoad.multiply(BigDecimal.TEN).divide(BigDecimal.valueOf(11),  0, RoundingMode.HALF_UP);
        BigDecimal totalDisc = s.getSubsfincomp().add(s.getSubsmd()).add(s.getSubsahm()).add(s.getSubsown());
        BigDecimal priceAfterDisc = salesAmount.subtract(totalDisc);
        BigDecimal tax = salesAmount.divide(BigDecimal.TEN, 0, RoundingMode.HALF_UP);

        o.setDiscountMD(s.getSubsmd());
        o.setDiscountFinCoy(s.getSubsfincomp());
        o.setDiscountatpm(s.getSubsahm());
        o.setDiscount(s.getSubsown());
        o.setBbn(s.getBbnprice());
        if(!internalBankMaping.isEmpty() && internalBankMaping.iterator().next().getIsppn() == 0){
            o.setTaxAmount(0D);
        } else {
            o.setTaxAmount(tax);
        }

        o.setQty(1d);
        o.setOrders(v);
        o = orderItemRepository.saveAndFlush(o);


        // Marking Sur ke VSO
        orderUtils.getRequirementOrderItemByOrder(s, o, 1);

        log.info("buildFromSUR:new VSO()");
        v.getDetails().add(o);
        return v;
    }

    @Transactional(propagation=Propagation.REQUIRED)
    public void autoMatching(VehicleSalesOrderDTO dto){
        List<InventoryItem> linv = null;

        log.info("automatching : get id internal");
        Internal intr =  internalRepository.findByIdInternal(dto.getInternalId());
        UUID idOwner = intr.getOrganization().getIdParty();

        PageRequest pageRequest= new PageRequest(0,1);
        log.info("automatching: order item");
        OrderItem oi = orderItemRepository.findOrderItemByOrders(dto.getIdOrder(), pageRequest).iterator().next();

        log.info("automatching: get sur");
        SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(dto.getSalesUnitRequirement().getId());
        if (sur.getRequestIdFrame() == null || sur.getRequestIdMachine() == null) {
            linv = shipmentOutgoingService.getInventoryItem( idOwner,  oi.getIdProduct(), oi.getIdFeature(), null,null, dto.getSalesUnitRequirement().getProductYear(), dto.getSalesUnitRequirement().getIdGudang());
            if (linv.size() == 0) throw new DmsException("Stock barang tidak tersedia!");
            if (linv.size() > 0) {
                log.info("automatching: get inv item");

                InventoryItem inv = linv.get(0);
                sur.setRequestIdFrame(inv.getIdFrame());
                sur.setRequestIdMachine(inv.getIdMachine());
                sur.setProductYear(inv.getYearAssembly());
                log.info("automatching: save sur");
                salesUnitRequirementRepository.save(sur);


                oi.setIdframe(inv.getIdFrame());
                oi.setIdmachine(inv.getIdMachine());
                orderItemRepository.save(oi);
            }
        } else if (sur.getRequestIdFrame() != null || sur.getRequestIdMachine() != null) {
            linv = shipmentOutgoingService.getInventoryItem( idOwner,  oi.getIdProduct(), oi.getIdFeature(), sur.getRequestIdFrame(),sur.getRequestIdMachine(), sur.getProductYear(), sur.getIdGudang());
            if (linv.size() == 0) throw new DmsException("Stock barang tidak tersedia!");
            if (linv.size() > 0) {
                log.info("automatching: get inv item");

                InventoryItem inv = linv.get(0);
                sur.setRequestIdFrame(inv.getIdFrame());
                sur.setRequestIdMachine(inv.getIdMachine());
                sur.setProductYear(inv.getYearAssembly());
                log.info("automatching: save sur");
                salesUnitRequirementRepository.save(sur);


                oi.setIdframe(inv.getIdFrame());
                oi.setIdmachine(inv.getIdMachine());
                orderItemRepository.save(oi);
            }
        }



    }

    @Transactional(propagation=Propagation.REQUIRED)
    public void cancelAutoMatching(VehicleSalesOrderDTO dto){

        SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(dto.getSalesUnitRequirement().getId());

        sur.setRequestIdMachine(null);
        sur.setRequestIdFrame(null);

        salesUnitRequirementRepository.save(sur);

    }

    @Transactional(readOnly = true)
    public VehicleSalesOrderDTO assignSUR(VehicleSalesOrderDTO dto) {
        SalesUnitRequirementDTO sur = salesUnitRequirementService.findOne(dto.getIdSalesUnitRequirement());

        String fname = null;
        String lname = null;
        String korsal = null;
        if (sur != null) {
            SalesUnitRequirement surs = salesUnitRequirementRepository.findOne(sur.getIdRequirement());
            dto.setSalesUnitRequirement(sur);
            Number arLeasing = 0;
            BigDecimal unitPrice = surs.getUnitPrice() == null ? BigDecimal.ZERO : surs.getUnitPrice();
            BigDecimal dp = surs.getCreditDownPayment() == null ? BigDecimal.ZERO :surs.getCreditDownPayment();
            arLeasing = unitPrice.doubleValue() - dp.doubleValue();
//            BigDecimal unitPrice = surs.getUnitPrice() == null ? BigDecimal.ZERO : surs.getUnitPrice();
//            BigDecimal dp = surs.getCreditDownPayment() == null ? BigDecimal.ZERO :surs.getCreditDownPayment();
//            arLeasing = surs.getUnitPrice().doubleValue() - surs.getCreditDownPayment().doubleValue();
            dto.setArLeasing(arLeasing);
            Salesman sales = salesmanRepository.findOne(sur.getSalesmanId());
            if (sales != null) {
                dto.setSalesNumber(sales.getIdSalesman());

                if (sales.getCoordinatorSales() != null) {
                    Salesman coorsales = salesmanRepository.findOne(sales.getCoordinatorSales().getIdPartyRole());
                    if (coorsales != null) {
//                        fname = coorsales.getCoordinatorSales().getPerson().getFirstName();
//                        lname = coorsales.getCoordinatorSales().getPerson().getLastName();
//                        korsal =  fname.concat(" ").concat(lname);
                        korsal = coorsales.getPerson().getName();
                        log.debug("koesales vso = " + korsal);
                        dto.setKorsalName(korsal);
                    }
                }

            }
        }

        if (sur.getIdRequest() != null) {
            Request request = requestRepository.findOne(sur.getIdRequest());
            log.debug("requestttt == "+ request);
            if (request != null) {
                dto.setRequestNumber(request.getRequestNumber());
            }
        }

        return dto;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void checkOldData() {
        List<VehicleSalesOrder> vsos = vehicleSalesOrderRepository.findAll();
        for (VehicleSalesOrder vso: vsos) {
            if (vso.getIdSalesUnitRequirement() != null) {
                OrderItem oi = vso.getDetails().iterator().next();
                RequirementOrderItem roi = orderUtils.findRequirementOrderItemByOrder(oi);
                if (roi == null) {
                    SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(vso.getIdSalesUnitRequirement());
                    if (sur != null) {
                        roi = orderUtils.getRequirementOrderItemByOrder(sur, oi, oi.getQty().intValue());
                        log.debug("Make relation " + roi.toString());
                    }
                }
            }
        }
    }

    @Transactional
    public Map<String, Object> processIndex(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String commandIndex = (String) item.get("command");
        if (commandIndex != null && "buildIndexVSO".equalsIgnoreCase(commandIndex)) {
            buildIndex();
        } else
        if (commandIndex != null && "matchingSPG".equalsIgnoreCase(commandIndex)) {
            buildMatchingFromSPG();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processIndexAdminHandling(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String commandIndex = (String) item.get("command");
        if (commandIndex != null && "buildIndexVSO".equalsIgnoreCase(commandIndex)) {
            buildIndexAdminHandling();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }


    @Async
    public void buildIndex() {
//        vehicleSalesOrderSearchRepository.deleteAll();
//        List<VehicleSalesOrder> vsoes = vehicleSalesOrderRepository.findToReindex();
//        for (VehicleSalesOrder vso: vsoes) {
//            vehicleSalesOrderSearchRepository.save(assignSUR(vehicleSalesOrderMapper.toDto(vso)));
//            log.debug("Data Unit Vehicle Sales Order save !.....");
//        }
    }

    @Async
    public void buildIndexAdminHandling() {
        vehicleSalesOrderSearchRepository.deleteAll();
        List<VehicleSalesOrder> vsoes = vehicleSalesOrderRepository.findToReIndexAdminHandling();
        for (VehicleSalesOrder vso: vsoes) {
            vehicleSalesOrderSearchRepository.save(assignSUR(getIVUNumber(vehicleSalesOrderMapper.toDto(vso))));
            log.debug("Data Admin Handling save !.....");
        }
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> findAllVSOSales(Pageable pageable) {
        log.debug("Request to get all VehicleSalesOrders");
        UUID salesmanId = null;
        if (partyUtils.getCurrentSalesman() != null ) {
            salesmanId = partyUtils.getCurrentSalesman().getIdPartyRole() == null ? null : partyUtils.getCurrentSalesman().getIdPartyRole();
            Internal internal = partyUtils.getCurrentInternal();
            return vehicleSalesOrderRepository.queryAllVsoSales(internal != null ? internal.getIdInternal(): null,salesmanId, pageable)
                .map(vehicleSalesOrderMapper::toDto)
                .map(this::assignSUR);
        } else {
            throw new DmsException("Anda Login Bukan Sebagai Sales");
        }

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void vsoSalesEdit(SalesUnitRequirement salesUnitRequirement) {
        List<VehicleSalesOrder> listvso = vehicleSalesOrderRepository.queryVSOByIdRequirement(salesUnitRequirement.getIdRequirement());
        if (!listvso.isEmpty()) {
            Page<OrderItem> orderItem = orderItemRepository.findOrderItemByOrders(listvso.get(0).getIdOrder(), new PageRequest(0, 1));
            if(orderItem != null) {
                OrderItem oi = null;
                oi = orderItem.getContent().get(0);
                oi.setIdFeature(salesUnitRequirement.getColor().getIdFeature());
                oi = orderItemRepository.save(oi);
            }
        }

    }


    @Transactional(readOnly = true)
    public VehicleSalesOrderDTO getIVUNumber(VehicleSalesOrderDTO dto) {
        List<Billing> vsb = billingRepository.findByDescription(dto.getOrderNumber());
        if (!vsb.isEmpty()) {
            dto.setBillingNumber(vsb.iterator().next().getBillingNumber());
            dto.setDateBilling(vsb.iterator().next().getDateCreate());
        }
        return dto;
    }

    @Transactional(readOnly = true)
    public  VehicleSalesOrderDTO findByIdReqandBill( String  billingNumber) {
        log.debug("Masuk Proses Pencarian VSO berdasarkan idunitreq");
        VehicleSalesOrder vso = vehicleSalesOrderRepository.findByIdRequirmentAndIdSaleType( billingNumber);
        return vehicleSalesOrderMapper.toDto(vso);
    }

    @Transactional(readOnly = true)
    public VehicleSalesOrderDTO assingOrderType (VehicleSalesOrderDTO dto) {
        Orders orders = ordersRepository.findOne(dto.getIdOrder());
        if (orders.getOrderType() !=null) {
            dto.setOrderTypeId(orders.getOrderType().getIdOrderType());
        }
        return dto;
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesOrderDTO> queryFilterByy(HttpServletRequest request, Pageable pageable) {
        log.debug ("kenapa ga jalan coba");
        String filterName = request.getParameter("filterName");
        String idInternal = request.getParameter("idInternal");
        String idStatusType = request.getParameter("idStatusType");
        String queryFrom = request.getParameter("queryFrom");

        if (filterName != null && "byInternalStatus".equalsIgnoreCase(filterName)) {
            return vehicleSalesOrderRepository.queryByInternalStatus(idInternal, Integer.valueOf(idStatusType), pageable)
                .map(vehicleSalesOrderMapper::toDto);
        }
        log.debug("check1");

        if (queryFrom != null && "listbast".equalsIgnoreCase(queryFrom)) {
            return vehicleSalesOrderRepository.queryBast(idInternal, pageable)
                .map(vehicleSalesOrderMapper::toDto);

        }
        if (queryFrom != null && "autoMatchingSPG".equalsIgnoreCase(queryFrom)){
            buildMatchingFromSPG();
        }
        log.debug("check2");
        return vehicleSalesOrderRepository.queryNothing(pageable)
            .map(vehicleSalesOrderMapper::toDto);
    }

    @Transactional(propagation=Propagation.REQUIRED)
    public void autoMatchingForspg(VehicleSalesOrder dto){
        VehicleSalesOrderDTO dto1 = vehicleSalesOrderMapper.toDto(dto);
        List<InventoryItem> linv = null;

        log.info("automatching : get id internal");
        Internal intr =  internalRepository.findByIdInternal(dto.getInternal().getIdInternal());
        UUID idOwner = intr.getOrganization().getIdParty();

        PageRequest pageRequest= new PageRequest(0,1);
        log.info("automatching: order item");
        OrderItem oi = orderItemRepository.findOrderItemByOrders(dto.getIdOrder(), pageRequest).iterator().next();

        log.info("automatching: get sur");
        SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(dto.getIdSalesUnitRequirement());

        if (sur.getRequestIdFrame() == null || sur.getRequestIdMachine() == null) {
            linv = shipmentOutgoingService.getInventoryItem( idOwner,  oi.getIdProduct(), oi.getIdFeature(), null,null, sur.getProductYear(), sur.getIdGudang());
            if (linv.size() > 0) {
                log.info("automatching: get inv item");

                InventoryItem inv = linv.get(0);
                sur.setRequestIdFrame(inv.getIdFrame());
                sur.setRequestIdMachine(inv.getIdMachine());
                sur.setProductYear(inv.getYearAssembly());
                log.info("automatching: save sur");
                salesUnitRequirementRepository.save(sur);


                oi.setIdframe(inv.getIdFrame());
                oi.setIdmachine(inv.getIdMachine());
                orderItemRepository.save(oi);
                this.changeVehicleSalesOrderStatus(dto1, BaseConstants.STATUS_VSO_MATCHING);
            }
        } else if (sur.getRequestIdFrame() != null || sur.getRequestIdMachine() != null) {
            linv = shipmentOutgoingService.getInventoryItem( idOwner,  oi.getIdProduct(), oi.getIdFeature(), sur.getRequestIdFrame(),sur.getRequestIdMachine(), sur.getProductYear(), sur.getIdGudang());

            if (linv.size() > 0) {
                log.info("automatching: get inv item");

                InventoryItem inv = linv.get(0);
                sur.setRequestIdFrame(inv.getIdFrame());
                sur.setRequestIdMachine(inv.getIdMachine());
                sur.setProductYear(inv.getYearAssembly());
                log.info("automatching: save sur");
                salesUnitRequirementRepository.save(sur);


                oi.setIdframe(inv.getIdFrame());
                oi.setIdmachine(inv.getIdMachine());
                orderItemRepository.save(oi);
                this.changeVehicleSalesOrderStatus(dto1, BaseConstants.STATUS_VSO_MATCHING);
            }
        }
    }

    public void buildMatchingFromSPG(){
        String idInternal = partyUtils.getCurrentInternal().getIdInternal();
        List<VehicleSalesOrder> lvso = vehicleSalesOrderRepository.findvsoApprove(idInternal);
        if(!lvso.isEmpty()) {
            for (VehicleSalesOrder v : lvso) {
                log.debug("masuk looping  == "+v);
                this.autoMatchingForspg(v);
            }
        }
    }
}
