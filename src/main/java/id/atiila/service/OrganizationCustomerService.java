package id.atiila.service;

import id.atiila.domain.Internal;
import id.atiila.domain.OrganizationCustomer;
import id.atiila.repository.OrganizationCustomerRepository;
import id.atiila.repository.search.OrganizationCustomerSearchRepository;
import id.atiila.service.dto.OrganizationCustomerDTO;
import id.atiila.service.mapper.OrganizationCustomerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing OrganizationCustomer.
 * BeSmart Team
 */

@Service
@Transactional
public class OrganizationCustomerService {

    private final Logger log = LoggerFactory.getLogger(OrganizationCustomerService.class);

    private final OrganizationCustomerRepository organizationCustomerRepository;

    private final OrganizationCustomerMapper organizationCustomerMapper;

    private final OrganizationCustomerSearchRepository organizationCustomerSearchRepository;

    @Autowired
    private CustomerUtils customerUtils;

    @Autowired
    private PartyUtils partyUtils;

    public OrganizationCustomerService(OrganizationCustomerRepository organizationCustomerRepository, OrganizationCustomerMapper organizationCustomerMapper, OrganizationCustomerSearchRepository organizationCustomerSearchRepository) {
        this.organizationCustomerRepository = organizationCustomerRepository;
        this.organizationCustomerMapper = organizationCustomerMapper;
        this.organizationCustomerSearchRepository = organizationCustomerSearchRepository;
    }

    /**
     * Save a organizationCustomer.
     *
     * @param organizationCustomerDTO the entity to save
     * @return the persisted entity
     */
    public OrganizationCustomerDTO save(OrganizationCustomerDTO organizationCustomerDTO) {
        log.debug("Request to save OrganizationCustomer : {}", organizationCustomerDTO);
        Boolean isNew = organizationCustomerDTO.getIdCustomer() == null;
        OrganizationCustomer organizationCustomer = organizationCustomerMapper.toEntity(organizationCustomerDTO);
        if (isNew && organizationCustomer.getIdCustomer() == null) {
            organizationCustomer = customerUtils.buildOrganizationCustomer(organizationCustomer);
        }
        organizationCustomer = organizationCustomerRepository.save(organizationCustomer);
        OrganizationCustomerDTO result = organizationCustomerMapper.toDto(organizationCustomer);
        organizationCustomerSearchRepository.save(organizationCustomer);
        return result;
    }

    /**
     * Get all the organizationCustomers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrganizationCustomerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrganizationCustomers");
        Internal intr = partyUtils.getCurrentInternal();
        return organizationCustomerRepository.findByInternal(intr.getIdInternal(), pageable)
            .map(organizationCustomerMapper::toDto);
    }

    /**
     * Get one organizationCustomer by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public OrganizationCustomerDTO findOne(String id) {
        log.debug("Request to get OrganizationCustomer : {}", id);
        OrganizationCustomer organizationCustomer = organizationCustomerRepository.findOne(id);
        return organizationCustomerMapper.toDto(organizationCustomer);
    }

    /**
     * Delete the organizationCustomer by id.
     *
     * @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete OrganizationCustomer : {}", id);
        organizationCustomerRepository.delete(id);
        organizationCustomerSearchRepository.delete(id);
    }

    /**
     * Search for the organizationCustomer corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrganizationCustomerDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of OrganizationCustomers for query {}", query);
        Page<OrganizationCustomer> result = organizationCustomerSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(organizationCustomerMapper::toDto);
    }

    @Transactional(readOnly = true)
    public OrganizationCustomerDTO processExecuteData(Integer id, String param, OrganizationCustomerDTO dto) {
        OrganizationCustomerDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional(readOnly = true)
    public Set<OrganizationCustomerDTO> processExecuteListData(Integer id, String param, Set<OrganizationCustomerDTO> dto) {
        Set<OrganizationCustomerDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        organizationCustomerSearchRepository.deleteAll();
        List<OrganizationCustomer> organizationCustomers  =  organizationCustomerRepository.findAll();
        for (OrganizationCustomer m: organizationCustomers) {
            organizationCustomerSearchRepository.save(m);
            log.debug("Data organizationCustomer save !...");
        }
    }

}
