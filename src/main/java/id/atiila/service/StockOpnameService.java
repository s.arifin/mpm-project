package id.atiila.service;

import id.atiila.domain.Internal;
import id.atiila.domain.StockOpname;
import id.atiila.repository.StockOpnameRepository;
import id.atiila.repository.search.StockOpnameSearchRepository;
import id.atiila.service.dto.StockOpnameDTO;
import id.atiila.service.mapper.StockOpnameMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing StockOpname.
 * BeSmart Team
 */

@Service
@Transactional
public class StockOpnameService {

    private final Logger log = LoggerFactory.getLogger(StockOpnameService.class);

    private final StockOpnameRepository stockOpnameRepository;

    private final StockOpnameMapper stockOpnameMapper;

    private final StockOpnameSearchRepository stockOpnameSearchRepository;

    @Autowired
    private StockOpnameUtils stockOpnameUtils;

    @Autowired
    private PartyUtils partyUtils;

    public StockOpnameService(StockOpnameRepository stockOpnameRepository, StockOpnameMapper stockOpnameMapper, StockOpnameSearchRepository stockOpnameSearchRepository) {
        this.stockOpnameRepository = stockOpnameRepository;
        this.stockOpnameMapper = stockOpnameMapper;
        this.stockOpnameSearchRepository = stockOpnameSearchRepository;
    }

    /**
     * Save a stockOpname.
     *
     * @param stockOpnameDTO the entity to save
     * @return the persisted entity
     */
    public StockOpnameDTO save(StockOpnameDTO stockOpnameDTO) {
        log.debug("Request to save StockOpname : {}", stockOpnameDTO);
        StockOpname stockOpname = stockOpnameMapper.toEntity(stockOpnameDTO);
        stockOpname = stockOpnameRepository.save(stockOpname);
        StockOpnameDTO result = stockOpnameMapper.toDto(stockOpname);
        stockOpnameSearchRepository.save(stockOpname);
        return result;
    }

    /**
     * Get all the stockOpnames.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StockOpnameDTO> findAll(Pageable pageable) {
        log.debug("Request to get all StockOpnames");
        Internal intr = partyUtils.getCurrentInternal();
        return stockOpnameRepository.findActiveStockOpname(intr, pageable)
            .map(stockOpnameMapper::toDto);
    }

    /**
     * Get one stockOpname by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public StockOpnameDTO findOne(UUID id) {
        log.debug("Request to get StockOpname : {}", id);
        StockOpname stockOpname = stockOpnameRepository.findOne(id);
        return stockOpnameMapper.toDto(stockOpname);
    }

    /**
     * Delete the stockOpname by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete StockOpname : {}", id);
        stockOpnameRepository.delete(id);
        stockOpnameSearchRepository.delete(id);
    }

    /**
     * Search for the stockOpname corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StockOpnameDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of StockOpnames for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idCalendar = request.getParameter("idCalendar");
        String idInternal = request.getParameter("idInternal");
        String idStockOpnameType = request.getParameter("idStockOpnameType");

        if (idCalendar != null) {
            q.withQuery(matchQuery("calendar.idCalendar", idCalendar));
        }
        else if (idInternal != null) {
            q.withQuery(matchQuery("internal.idInternal", idInternal));
        }
        else if (idStockOpnameType != null) {
            q.withQuery(matchQuery("stockOpnameType.idStockOpnameType", idStockOpnameType));
        }

        Page<StockOpname> result = stockOpnameSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(stockOpnameMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<StockOpnameDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered StockOpnameDTO");
        String idCalendar = request.getParameter("idCalendar");
        String idInternal = request.getParameter("idInternal");
        String idStockOpnameType = request.getParameter("idStockOpnameType");

        if (idCalendar != null) {
            return stockOpnameRepository.queryByIdCalendar(Integer.valueOf(idCalendar), pageable).map(stockOpnameMapper::toDto);
        }
        else if (idInternal != null) {
            return stockOpnameRepository.queryByIdInternal(idInternal, pageable).map(stockOpnameMapper::toDto);
        }
        else if (idStockOpnameType != null) {
            return stockOpnameRepository.queryByIdStockOpnameType(Integer.valueOf(idStockOpnameType), pageable).map(stockOpnameMapper::toDto);
        }

        return stockOpnameRepository.queryNothing(pageable).map(stockOpnameMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String idInternal = (String) item.get("idInternal");
        StockOpname s = stockOpnameUtils.buildStockOpname(idInternal);
        Map<String, Object> r = new HashMap<>();
        r.put("idStockOpname", s.getIdStockOpname());
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, StockOpnameDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<StockOpnameDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public StockOpnameDTO changeStockOpnameStatus(StockOpnameDTO dto, Integer id) {
        if (dto != null) {
			StockOpname e = stockOpnameRepository.findOne(dto.getIdStockOpname());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        stockOpnameSearchRepository.delete(dto.getIdStockOpname());
                        break;
                    default:
                        stockOpnameSearchRepository.save(e);
                }
				stockOpnameRepository.save(e);
			}
		}
        return dto;
    }
}
