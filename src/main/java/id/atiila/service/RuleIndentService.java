package id.atiila.service;

import id.atiila.domain.RuleIndent;
import id.atiila.repository.RuleIndentRepository;
import id.atiila.repository.search.RuleIndentSearchRepository;
import id.atiila.service.dto.RuleIndentDTO;
import id.atiila.service.mapper.RuleIndentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing RuleIndent.
 * atiila consulting
 */

@Service
@Transactional
public class RuleIndentService {

    private final Logger log = LoggerFactory.getLogger(RuleIndentService.class);

    private final RuleIndentRepository ruleIndentRepository;

    private final RuleIndentMapper ruleIndentMapper;

    private final RuleIndentSearchRepository ruleIndentSearchRepository;

    public RuleIndentService(RuleIndentRepository ruleIndentRepository, RuleIndentMapper ruleIndentMapper, RuleIndentSearchRepository ruleIndentSearchRepository) {
        this.ruleIndentRepository = ruleIndentRepository;
        this.ruleIndentMapper = ruleIndentMapper;
        this.ruleIndentSearchRepository = ruleIndentSearchRepository;
    }

    public Page<RuleIndentDTO> getRuleIndentByProduct(String idproduct, String idInternal, Pageable pageable){
        return ruleIndentRepository.findAllByIdProductAndIdInternal(idproduct, idInternal, pageable)
            .map(ruleIndentMapper::toDto);
    }

    public Boolean checkIsIndentByProduct(String idproduct, String idinternal){
        log.debug("Request to check is hot item");
        Boolean a = false;
        Integer r = ruleIndentRepository.findTotalRuleIndentByProduct(idproduct, idinternal);

        if (r > 0){
            a = true;
        }

        return a;
    }

    /**
     * Save a ruleIndent.
     *
     * @param ruleIndentDTO the entity to save
     * @return the persisted entity
     */
    public RuleIndentDTO save(RuleIndentDTO ruleIndentDTO) {
        log.debug("Request to save RuleIndent : {}", ruleIndentDTO);
        RuleIndent ruleIndent = ruleIndentMapper.toEntity(ruleIndentDTO);
        ruleIndent = ruleIndentRepository.save(ruleIndent);
        RuleIndentDTO result = ruleIndentMapper.toDto(ruleIndent);
        ruleIndentSearchRepository.save(ruleIndent);
        return result;
    }

    /**
     * Get all the ruleIndents.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RuleIndentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RuleIndents");
        return ruleIndentRepository.findAll(pageable)
            .map(ruleIndentMapper::toDto);
    }

    /**
     * Get one ruleIndent by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public RuleIndentDTO findOne(Integer id) {
        log.debug("Request to get RuleIndent : {}", id);
        RuleIndent ruleIndent = ruleIndentRepository.findOne(id);
        return ruleIndentMapper.toDto(ruleIndent);
    }

    /**
     * Delete the ruleIndent by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete RuleIndent : {}", id);
        ruleIndentRepository.delete(id);
        ruleIndentSearchRepository.delete(id);
    }

    /**
     * Search for the ruleIndent corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RuleIndentDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of RuleIndents for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));

        if (filterName != null) {
        }
        Page<RuleIndent> result = ruleIndentSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(ruleIndentMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<RuleIndentDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered RuleIndentDTO");

        return ruleIndentRepository.findByParams(pageable)
            .map(ruleIndentMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public RuleIndentDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        RuleIndentDTO r = null;
        return r;
    }

    @Transactional
    public Set<RuleIndentDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<RuleIndentDTO> r = new HashSet<>();
        return r;
    }

}
