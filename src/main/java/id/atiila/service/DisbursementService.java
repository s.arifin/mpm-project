package id.atiila.service;

import id.atiila.domain.Disbursement;
import id.atiila.repository.DisbursementRepository;
import id.atiila.repository.search.DisbursementSearchRepository;
import id.atiila.service.dto.DisbursementDTO;
import id.atiila.service.mapper.DisbursementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing Disbursement.
 * BeSmart Team
 */

@Service
@Transactional
public class DisbursementService {

    private final Logger log = LoggerFactory.getLogger(DisbursementService.class);

    private final DisbursementRepository disbursementRepository;

    private final DisbursementMapper disbursementMapper;

    private final DisbursementSearchRepository disbursementSearchRepository;

    public DisbursementService(DisbursementRepository disbursementRepository, DisbursementMapper disbursementMapper, DisbursementSearchRepository disbursementSearchRepository) {
        this.disbursementRepository = disbursementRepository;
        this.disbursementMapper = disbursementMapper;
        this.disbursementSearchRepository = disbursementSearchRepository;
    }

    /**
     * Save a disbursement.
     *
     * @param disbursementDTO the entity to save
     * @return the persisted entity
     */
    public DisbursementDTO save(DisbursementDTO disbursementDTO) {
        log.debug("Request to save Disbursement : {}", disbursementDTO);
        Disbursement disbursement = disbursementMapper.toEntity(disbursementDTO);
        disbursement = disbursementRepository.save(disbursement);
        DisbursementDTO result = disbursementMapper.toDto(disbursement);
        disbursementSearchRepository.save(disbursement);
        return result;
    }

    /**
     * Get all the disbursements.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DisbursementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Disbursements");
        return disbursementRepository.findActiveDisbursement(pageable)
            .map(disbursementMapper::toDto);
    }

    /**
     * Get one disbursement by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public DisbursementDTO findOne(UUID id) {
        log.debug("Request to get Disbursement : {}", id);
        Disbursement disbursement = disbursementRepository.findOne(id);
        return disbursementMapper.toDto(disbursement);
    }

    /**
     * Delete the disbursement by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Disbursement : {}", id);
        disbursementRepository.delete(id);
        disbursementSearchRepository.delete(id);
    }

    /**
     * Search for the disbursement corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DisbursementDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of Disbursements for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idPaymentType = request.getParameter("idPaymentType");
        String idMethod = request.getParameter("idMethod");
        String idInternal = request.getParameter("idInternal");
        String idPaidTo = request.getParameter("idPaidTo");
        String idPaidFrom = request.getParameter("idPaidFrom");

        if (idPaymentType != null) {
            q.withQuery(matchQuery("paymentType.idPaymentType", idPaymentType));
        }
        else if (idMethod != null) {
            q.withQuery(matchQuery("method.idPaymentMethod", idMethod));
        }
        else if (idInternal != null) {
            q.withQuery(matchQuery("internal.idInternal", idInternal));
        }
        else if (idPaidTo != null) {
            q.withQuery(matchQuery("paidTo.idBillTo", idPaidTo));
        }
        else if (idPaidFrom != null) {
            q.withQuery(matchQuery("paidFrom.idBillTo", idPaidFrom));
        }

        Page<Disbursement> result = disbursementSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(disbursementMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<DisbursementDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered DisbursementDTO");
        String idPaymentType = request.getParameter("idPaymentType");
        String idMethod = request.getParameter("idMethod");
        String idInternal = request.getParameter("idInternal");
        String idPaidTo = request.getParameter("idPaidTo");
        String idPaidFrom = request.getParameter("idPaidFrom");

        if (idPaymentType != null) {
            return disbursementRepository.findByIdPaymentType(Integer.valueOf(idPaymentType), pageable).map(disbursementMapper::toDto); 
        }
        else if (idMethod != null) {
            return disbursementRepository.findByIdMethod(Integer.valueOf(idMethod), pageable).map(disbursementMapper::toDto); 
        }
        else if (idInternal != null) {
            return disbursementRepository.findByIdInternal(idInternal, pageable).map(disbursementMapper::toDto); 
        }
        else if (idPaidTo != null) {
            return disbursementRepository.findByIdPaidTo(idPaidTo, pageable).map(disbursementMapper::toDto); 
        }
        else if (idPaidFrom != null) {
            return disbursementRepository.findByIdPaidFrom(idPaidFrom, pageable).map(disbursementMapper::toDto); 
        }

        return disbursementRepository.findAll(pageable).map(disbursementMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, DisbursementDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<DisbursementDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public DisbursementDTO changeDisbursementStatus(DisbursementDTO dto, Integer id) {
        if (dto != null) {
			Disbursement e = disbursementRepository.findOne(dto.getIdPayment());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        disbursementSearchRepository.delete(dto.getIdPayment());
                        break;
                    default:
                        disbursementSearchRepository.save(e);
                }
				disbursementRepository.save(e);
			}
		}
        return dto;
    }
}
