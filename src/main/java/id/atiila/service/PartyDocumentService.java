package id.atiila.service;

import id.atiila.domain.DocumentType;
import id.atiila.domain.Party;
import id.atiila.domain.PartyDocument;
import id.atiila.repository.DocumentTypeRepository;
import id.atiila.repository.PartyDocumentRepository;
import id.atiila.repository.PartyRepository;
import id.atiila.repository.search.PartyDocumentSearchRepository;
import id.atiila.service.dto.PartyDocumentDTO;
import id.atiila.service.mapper.PartyDocumentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing PartyDocument.
 * BeSmart Team
 */

@Service
@Transactional
public class PartyDocumentService {

    private final Logger log = LoggerFactory.getLogger(PartyDocumentService.class);

    private final PartyDocumentRepository partyDocumentRepository;

    private final PartyDocumentMapper partyDocumentMapper;

    private final PartyDocumentSearchRepository partyDocumentSearchRepository;

    @Autowired
    private DocumentTypeRepository documentTypeRepository;

    @Autowired
    private PartyRepository partyRepository;

    public PartyDocumentService(PartyDocumentRepository partyDocumentRepository, PartyDocumentMapper partyDocumentMapper, PartyDocumentSearchRepository partyDocumentSearchRepository) {
        this.partyDocumentRepository = partyDocumentRepository;
        this.partyDocumentMapper = partyDocumentMapper;
        this.partyDocumentSearchRepository = partyDocumentSearchRepository;
    }

    /**
     * Save a partyDocument.
     *
     * @param partyDocumentDTO the entity to save
     * @return the persisted entity
     */
    public PartyDocumentDTO save(PartyDocumentDTO partyDocumentDTO) {
        log.debug("Request to save PartyDocument : {}", partyDocumentDTO);
        PartyDocument partyDocument = partyDocumentMapper.toEntity(partyDocumentDTO);
        partyDocument = partyDocumentRepository.save(partyDocument);
        PartyDocumentDTO result = partyDocumentMapper.toDto(partyDocument);
        return result;
    }

    /**
     *  Get all the partyDocuments.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartyDocumentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PartyDocuments");
        return partyDocumentRepository.findAll(pageable)
            .map(partyDocumentMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PartyDocumentDTO> findByIdParty(UUID idparty, Pageable pageable) {
        log.debug("Request to get all PartyDocuments");
        return partyDocumentRepository.findAllByParty_IdParty(idparty, pageable)
            .map(partyDocumentMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PartyDocumentDTO> findByIdPartyAndIdDoctype(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all PartyDocuments");
        Integer idDocType = Integer.valueOf(request.getParameter("idDocType"));
        UUID idParty = UUID.fromString(request.getParameter("idParty"));
        log.debug("iddoctype rest == " + idDocType);
        log.debug("idparty rest == " + idParty);
        DocumentType documentType = null;
        Party party = null;
        if (idDocType != null) {
            documentType = documentTypeRepository.findOne(idDocType);
            log.debug("doctype rest == " + documentType);
        }
        if (idParty != null) {
            party = partyRepository.findOne(idParty);
            log.debug("party rest == " + party);
        }
        return partyDocumentRepository.findAllByPartyAndDocumentType(party, documentType, pageable)
            .map(partyDocumentMapper::toDto);
    }

    /**
     *  Get one partyDocument by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PartyDocumentDTO findOne(UUID id) {
        log.debug("Request to get PartyDocument : {}", id);
        PartyDocument partyDocument = partyDocumentRepository.findOne(id);
        return partyDocumentMapper.toDto(partyDocument);
    }

    /**
     *  Delete the  partyDocument by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete PartyDocument : {}", id);
        partyDocumentRepository.delete(id);
        partyDocumentSearchRepository.delete(id);
    }

    /**
     * Search for the partyDocument corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartyDocumentDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PartyDocuments for query {}", query);
        Page<PartyDocument> result = partyDocumentSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(partyDocumentMapper::toDto);
    }

    public PartyDocumentDTO processExecuteData(Integer id, String param, PartyDocumentDTO dto) {
        PartyDocumentDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<PartyDocumentDTO> processExecuteListData(Integer id, String param, Set<PartyDocumentDTO> dto) {
        Set<PartyDocumentDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

}
