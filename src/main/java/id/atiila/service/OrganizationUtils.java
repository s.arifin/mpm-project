package id.atiila.service;

import id.atiila.domain.Internal;
import id.atiila.repository.InternalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class OrganizationUtils {

    @Autowired
    private InternalRepository<Internal> internalRepo;

    public List<Internal> getAllInternal() {
        return internalRepo.findAll();
    }
}
