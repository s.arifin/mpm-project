package id.atiila.service;

import id.atiila.domain.RuleHotItem;
import id.atiila.repository.RuleHotItemRepository;
import id.atiila.repository.search.RuleHotItemSearchRepository;
import id.atiila.service.dto.RuleHotItemDTO;
import id.atiila.service.mapper.RuleHotItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing RuleHotItem.
 * BeSmart Team
 */
@Service
@Transactional
public class RuleHotItemService {

    private final Logger log = LoggerFactory.getLogger(RuleHotItemService.class);

    private final RuleHotItemRepository ruleHotItemRepository;

    private final RuleHotItemMapper ruleHotItemMapper;

    private final RuleHotItemSearchRepository ruleHotItemSearchRepository;

    public RuleHotItemService(RuleHotItemRepository ruleHotItemRepository, RuleHotItemMapper ruleHotItemMapper, RuleHotItemSearchRepository ruleHotItemSearchRepository) {
        this.ruleHotItemRepository = ruleHotItemRepository;
        this.ruleHotItemMapper = ruleHotItemMapper;
        this.ruleHotItemSearchRepository = ruleHotItemSearchRepository;
    }

    /**
     * Save a ruleHotItem.
     *
     * @param ruleHotItemDTO the entity to save
     * @return the persisted entity
     */
    public RuleHotItemDTO save(RuleHotItemDTO ruleHotItemDTO) {
        log.debug("Request to save RuleHotItem : {}", ruleHotItemDTO);
        RuleHotItem ruleHotItem = ruleHotItemMapper.toEntity(ruleHotItemDTO);
        ruleHotItem = ruleHotItemRepository.save(ruleHotItem);
        RuleHotItemDTO result = ruleHotItemMapper.toDto(ruleHotItem);
        ruleHotItemSearchRepository.save(ruleHotItem);
        return result;
    }

    public Boolean checkIsHotItemByProduct(String idproduct, String idinternal){
        log.debug("Request to check is hot item");

        Boolean a = false;
        Integer r = ruleHotItemRepository.findTotalHotItemByProductAndInternal(idproduct, idinternal);

        if (r > 0){
            a = true;
        }

        return a;
    }

    /**
     *  Get all the ruleHotItems.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RuleHotItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RuleHotItems");
        return ruleHotItemRepository.findAll(pageable)
            .map(ruleHotItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<RuleHotItemDTO> findAllByIdInternal(String idinternal ,Pageable pageable){
        log.debug("SERVICE to get all RuleHotItems By Id Internal");
        log.debug("idinternal nya ==> " + idinternal);
        return ruleHotItemRepository.findAllByIdInternal(idinternal, pageable)
            .map(ruleHotItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<RuleHotItemDTO> findAllByIdProduct(String idproduct, String idinternal,Pageable pageable){
        log.debug("SERVICE to get all RuleHotItems By Id Product");
        return ruleHotItemRepository.findAllByIdProductAndIdInternal(idproduct, idinternal, pageable)
            .map(ruleHotItemMapper::toDto);
    }

    /**
     *  Get one ruleHotItem by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public RuleHotItemDTO findOne(Integer id) {
        log.debug("Request to get RuleHotItem : {}", id);
        RuleHotItem ruleHotItem = ruleHotItemRepository.findOne(id);
        return ruleHotItemMapper.toDto(ruleHotItem);
    }

    /**
     *  Delete the  ruleHotItem by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete RuleHotItem : {}", id);
        ruleHotItemRepository.delete(id);
        ruleHotItemSearchRepository.delete(id);
    }

    /**
     * Search for the ruleHotItem corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RuleHotItemDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of RuleHotItems for query {}", query);
        Page<RuleHotItem> result = ruleHotItemSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(ruleHotItemMapper::toDto);
    }
}
