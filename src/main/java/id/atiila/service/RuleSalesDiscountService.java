package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.RuleSalesDiscount;
import id.atiila.repository.RuleSalesDiscountRepository;
import id.atiila.repository.search.RuleSalesDiscountSearchRepository;
import id.atiila.service.dto.RuleSalesDiscountDTO;
import id.atiila.service.mapper.RuleSalesDiscountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing RuleSalesDiscount.
 * BeSmart Team
 */

@Service
@Transactional
public class RuleSalesDiscountService {

    private final Logger log = LoggerFactory.getLogger(RuleSalesDiscountService.class);

    private final RuleSalesDiscountRepository ruleSalesDiscountRepository;

    private final RuleSalesDiscountMapper ruleSalesDiscountMapper;

    private final RuleSalesDiscountSearchRepository ruleSalesDiscountSearchRepository;

    @Autowired
    private DateUtils dateUtils;

    public RuleSalesDiscountService(RuleSalesDiscountRepository ruleSalesDiscountRepository, RuleSalesDiscountMapper ruleSalesDiscountMapper, RuleSalesDiscountSearchRepository ruleSalesDiscountSearchRepository) {
        this.ruleSalesDiscountRepository = ruleSalesDiscountRepository;
        this.ruleSalesDiscountMapper = ruleSalesDiscountMapper;
        this.ruleSalesDiscountSearchRepository = ruleSalesDiscountSearchRepository;
    }

    /**
     * Save a ruleSalesDiscount.
     *
     * @param ruleSalesDiscountDTO the entity to save
     * @return the persisted entity
     */
    public RuleSalesDiscountDTO save(RuleSalesDiscountDTO ruleSalesDiscountDTO) {
        log.debug("Request to save RuleSalesDiscount : {}", ruleSalesDiscountDTO);
        RuleSalesDiscount ruleSalesDiscount = ruleSalesDiscountMapper.toEntity(ruleSalesDiscountDTO);
        ruleSalesDiscount = ruleSalesDiscountRepository.save(ruleSalesDiscount);
        RuleSalesDiscountDTO result = ruleSalesDiscountMapper.toDto(ruleSalesDiscount);
        ruleSalesDiscountSearchRepository.save(ruleSalesDiscount);
        return result;
    }

    /**
     *  Get all the ruleSalesDiscounts.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RuleSalesDiscountDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RuleSalesDiscounts");
        return ruleSalesDiscountRepository.findAll(pageable)
            .map(ruleSalesDiscountMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<RuleSalesDiscountDTO> findAllByInternal(String idinternal, Pageable pageable){
        log.debug("Request to get all RuleSalesDiscounts By Internal");
        return ruleSalesDiscountRepository.findAllByIdInternal(idinternal, pageable)
            .map(ruleSalesDiscountMapper::toDto);
    }

    /**
     *  Get one ruleSalesDiscount by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public RuleSalesDiscountDTO findOne(Integer id) {
        log.debug("Request to get RuleSalesDiscount : {}", id);
        RuleSalesDiscount ruleSalesDiscount = ruleSalesDiscountRepository.findOne(id);
        return ruleSalesDiscountMapper.toDto(ruleSalesDiscount);
    }

    /**
     *  Delete the  ruleSalesDiscount by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete RuleSalesDiscount : {}", id);
        ruleSalesDiscountRepository.delete(id);
        ruleSalesDiscountSearchRepository.delete(id);
    }

    /**
     * Search for the ruleSalesDiscount corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RuleSalesDiscountDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of RuleSalesDiscounts for query {}", query);
        Page<RuleSalesDiscount> result = ruleSalesDiscountSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(ruleSalesDiscountMapper::toDto);
    }

    public RuleSalesDiscountDTO processExecuteData(Integer id, String param, RuleSalesDiscountDTO dto) {
        RuleSalesDiscountDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<RuleSalesDiscountDTO> processExecuteListData(Integer id, String param, Set<RuleSalesDiscountDTO> dto) {
        Set<RuleSalesDiscountDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public void initialize(){
        List<RuleSalesDiscount> l_rule= ruleSalesDiscountRepository.findAll();
        if (l_rule.size() == 0){
            RuleSalesDiscountDTO ruleSalesDiscountDTO;

            ruleSalesDiscountDTO = new RuleSalesDiscountDTO();
            ruleSalesDiscountDTO.setMaxAmount(BigDecimal.valueOf(500000));
            ruleSalesDiscountDTO.setRole(BaseConstants.AUTH_SALESMAN);
            ruleSalesDiscountDTO.setIdRuleType(102);
            ruleSalesDiscountDTO.setSequenceNumber(1);
            ruleSalesDiscountDTO.setDateFrom(ZonedDateTime.now());
            ruleSalesDiscountDTO.setDateThru(dateUtils.addMonthFromNow(1));
            save(ruleSalesDiscountDTO);

            ruleSalesDiscountDTO = new RuleSalesDiscountDTO();
            ruleSalesDiscountDTO.setMaxAmount(BigDecimal.valueOf(1000000));
            ruleSalesDiscountDTO.setRole(BaseConstants.AUTH_KORSAL);
            ruleSalesDiscountDTO.setIdRuleType(102);
            ruleSalesDiscountDTO.setSequenceNumber(2);
            ruleSalesDiscountDTO.setDateFrom(ZonedDateTime.now());
            ruleSalesDiscountDTO.setDateThru(dateUtils.addMonthFromNow(1));
            save(ruleSalesDiscountDTO);

            ruleSalesDiscountDTO = new RuleSalesDiscountDTO();
            ruleSalesDiscountDTO.setMaxAmount(BigDecimal.valueOf(999999999));
            ruleSalesDiscountDTO.setRole(BaseConstants.AUTH_KACAB);
            ruleSalesDiscountDTO.setIdRuleType(102);
            ruleSalesDiscountDTO.setSequenceNumber(3);
            ruleSalesDiscountDTO.setDateFrom(ZonedDateTime.now());
            ruleSalesDiscountDTO.setDateThru(dateUtils.addMonthFromNow(1));
            save(ruleSalesDiscountDTO);
        }
    }
}
