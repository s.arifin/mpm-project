package id.atiila.service;

import id.atiila.domain.ProspectSource;
import id.atiila.repository.ProspectSourceRepository;
import id.atiila.repository.search.ProspectSourceSearchRepository;
import id.atiila.service.dto.ProspectSourceDTO;
import id.atiila.service.mapper.ProspectSourceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ProspectSource.
 * BeSmart Team
 */

@Service
@Transactional
public class ProspectSourceService {

    private final Logger log = LoggerFactory.getLogger(ProspectSourceService.class);

    private final ProspectSourceRepository prospectSourceRepository;

    private final ProspectSourceMapper prospectSourceMapper;

    private final ProspectSourceSearchRepository prospectSourceSearchRepository;

    public ProspectSourceService(ProspectSourceRepository prospectSourceRepository, ProspectSourceMapper prospectSourceMapper, ProspectSourceSearchRepository prospectSourceSearchRepository) {
        this.prospectSourceRepository = prospectSourceRepository;
        this.prospectSourceMapper = prospectSourceMapper;
        this.prospectSourceSearchRepository = prospectSourceSearchRepository;
    }

    /**
     * Save a prospectSource.
     *
     * @param prospectSourceDTO the entity to save
     * @return the persisted entity
     */
    public ProspectSourceDTO save(ProspectSourceDTO prospectSourceDTO) {
        log.debug("Request to save ProspectSource : {}", prospectSourceDTO);
        ProspectSource prospectSource = prospectSourceMapper.toEntity(prospectSourceDTO);
        prospectSource = prospectSourceRepository.save(prospectSource);
        ProspectSourceDTO result = prospectSourceMapper.toDto(prospectSource);
        prospectSourceSearchRepository.save(prospectSource);
        return result;
    }

    /**
     *  Get all the prospectSources.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProspectSourceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProspectSources");
        return prospectSourceRepository.findAll(pageable)
            .map(prospectSourceMapper::toDto);
    }

    /**
     *  Get one prospectSource by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ProspectSourceDTO findOne(Integer id) {
        log.debug("Request to get ProspectSource : {}", id);
        ProspectSource prospectSource = prospectSourceRepository.findOne(id);
        return prospectSourceMapper.toDto(prospectSource);
    }

    /**
     *  Delete the  prospectSource by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete ProspectSource : {}", id);
        prospectSourceRepository.delete(id);
        prospectSourceSearchRepository.delete(id);
    }

    /**
     * Search for the prospectSource corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProspectSourceDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ProspectSources for query {}", query);
        Page<ProspectSource> result = prospectSourceSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(prospectSourceMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        prospectSourceSearchRepository.deleteAll();
        List<ProspectSource> prospectSources =  prospectSourceRepository.findAll();
        for (ProspectSource m: prospectSources) {
            prospectSourceSearchRepository.save(m);
            log.debug("Data prospectSource save !...");
        }
    }

}
