package id.atiila.service;

import id.atiila.domain.PurchaseOrder;
import id.atiila.repository.PurchaseOrderRepository;
import id.atiila.repository.search.PurchaseOrderSearchRepository;
import id.atiila.service.dto.PurchaseOrderDTO;
import id.atiila.service.mapper.PurchaseOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing PurchaseOrder.
 * BeSmart Team
 */

@Service
@Transactional
public class PurchaseOrderService {

    private final Logger log = LoggerFactory.getLogger(PurchaseOrderService.class);

    private final PurchaseOrderRepository purchaseOrderRepository;

    private final PurchaseOrderMapper purchaseOrderMapper;

    private final PurchaseOrderSearchRepository purchaseOrderSearchRepository;

    public PurchaseOrderService(PurchaseOrderRepository purchaseOrderRepository, PurchaseOrderMapper purchaseOrderMapper, PurchaseOrderSearchRepository purchaseOrderSearchRepository) {
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.purchaseOrderMapper = purchaseOrderMapper;
        this.purchaseOrderSearchRepository = purchaseOrderSearchRepository;
    }

    /**
     * Save a purchaseOrder.
     *
     * @param purchaseOrderDTO the entity to save
     * @return the persisted entity
     */
    public PurchaseOrderDTO save(PurchaseOrderDTO purchaseOrderDTO) {
        log.debug("Request to save PurchaseOrder : {}", purchaseOrderDTO);
        PurchaseOrder purchaseOrder = purchaseOrderMapper.toEntity(purchaseOrderDTO);
        purchaseOrder = purchaseOrderRepository.save(purchaseOrder);
        PurchaseOrderDTO result = purchaseOrderMapper.toDto(purchaseOrder);
        purchaseOrderSearchRepository.save(purchaseOrder);
        return result;
    }

    /**
     * Get all the purchaseOrders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PurchaseOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PurchaseOrders");
        return purchaseOrderRepository.findActivePurchaseOrder(pageable)
            .map(purchaseOrderMapper::toDto);
    }

    /**
     * Get one purchaseOrder by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PurchaseOrderDTO findOne(UUID id) {
        log.debug("Request to get PurchaseOrder : {}", id);
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findOne(id);
        return purchaseOrderMapper.toDto(purchaseOrder);
    }

    /**
     * Delete the purchaseOrder by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete PurchaseOrder : {}", id);
        purchaseOrderRepository.delete(id);
        purchaseOrderSearchRepository.delete(id);
    }

    /**
     * Search for the purchaseOrder corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PurchaseOrderDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of PurchaseOrders for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idVendor = request.getParameter("idVendor");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");

        if (filterName != null) {
        }
        Page<PurchaseOrder> result = purchaseOrderSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(purchaseOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PurchaseOrderDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered PurchaseOrderDTO");
        String idVendor = request.getParameter("idVendor");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");
        String orderNumber = request.getParameter("orderNumber");

        return purchaseOrderRepository.findByParams(idVendor, idInternal, idBillTo, orderNumber, pageable)
            .map(purchaseOrderMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public PurchaseOrderDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        PurchaseOrderDTO r = null;
        return r;
    }

    @Transactional
    public Set<PurchaseOrderDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<PurchaseOrderDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public PurchaseOrderDTO changePurchaseOrderStatus(PurchaseOrderDTO dto, Integer id) {
        if (dto != null) {
			PurchaseOrder e = purchaseOrderRepository.findOne(dto.getIdOrder());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        purchaseOrderSearchRepository.delete(dto.getIdOrder());
                        break;
                    default:
                        purchaseOrderSearchRepository.save(e);
                }
				purchaseOrderRepository.save(e);
			}
		}
        return dto;
    }

    @Async
    public void buildIndex() {
        purchaseOrderSearchRepository.deleteAll();
        List<PurchaseOrder> purchaseOrders =  purchaseOrderRepository.findAll();
        for (PurchaseOrder m: purchaseOrders) {
            purchaseOrderSearchRepository.save(m);
            log.debug("Data purchaseOrder save !...");
        }
    }

}
