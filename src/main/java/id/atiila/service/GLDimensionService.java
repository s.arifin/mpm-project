package id.atiila.service;

import id.atiila.domain.GLDimension;
import id.atiila.repository.GLDimensionRepository;
import id.atiila.repository.search.GLDimensionSearchRepository;
import id.atiila.service.dto.GLDimensionDTO;
import id.atiila.service.mapper.GLDimensionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing GLDimension.
 * atiila consulting
 */

@Service
@Transactional
public class GLDimensionService {

    private final Logger log = LoggerFactory.getLogger(GLDimensionService.class);

    private final GLDimensionRepository gLDimensionRepository;

    private final GLDimensionMapper gLDimensionMapper;

    private final GLDimensionSearchRepository gLDimensionSearchRepository;

    public GLDimensionService(GLDimensionRepository gLDimensionRepository, GLDimensionMapper gLDimensionMapper, GLDimensionSearchRepository gLDimensionSearchRepository) {
        this.gLDimensionRepository = gLDimensionRepository;
        this.gLDimensionMapper = gLDimensionMapper;
        this.gLDimensionSearchRepository = gLDimensionSearchRepository;
    }

    /**
     * Save a gLDimension.
     *
     * @param gLDimensionDTO the entity to save
     * @return the persisted entity
     */
    public GLDimensionDTO save(GLDimensionDTO gLDimensionDTO) {
        log.debug("Request to save GLDimension : {}", gLDimensionDTO);
        GLDimension gLDimension = gLDimensionMapper.toEntity(gLDimensionDTO);
        gLDimension = gLDimensionRepository.save(gLDimension);
        GLDimensionDTO result = gLDimensionMapper.toDto(gLDimension);
        gLDimensionSearchRepository.save(gLDimension);
        return result;
    }

    /**
     * Get all the gLDimensions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GLDimensionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all GLDimensions");
        return gLDimensionRepository.findAll(pageable)
            .map(gLDimensionMapper::toDto);
    }

    /**
     * Get one gLDimension by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public GLDimensionDTO findOne(Integer id) {
        log.debug("Request to get GLDimension : {}", id);
        GLDimension gLDimension = gLDimensionRepository.findOne(id);
        return gLDimensionMapper.toDto(gLDimension);
    }

    /**
     * Delete the gLDimension by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete GLDimension : {}", id);
        gLDimensionRepository.delete(id);
        gLDimensionSearchRepository.delete(id);
    }

    /**
     * Search for the gLDimension corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GLDimensionDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of GLDimensions for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");
        String idVendor = request.getParameter("idVendor");
        String idPartyCategory = request.getParameter("idPartyCategory");
        String idProductCategory = request.getParameter("idProductCategory");
        String idDimension1 = request.getParameter("idDimension1");
        String idDimension2 = request.getParameter("idDimension2");
        String idDimension3 = request.getParameter("idDimension3");
        String idDimension4 = request.getParameter("idDimension4");
        String idDimension5 = request.getParameter("idDimension5");
        String idDimension6 = request.getParameter("idDimension6");

        if (idInternal != null) {
            q.withQuery(matchQuery("internal.idInternal", idInternal));
        }
        else if (idBillTo != null) {
            q.withQuery(matchQuery("billTo.idBillTo", idBillTo));
        }
        else if (idVendor != null) {
            q.withQuery(matchQuery("vendor.idVendor", idVendor));
        }
        else if (idPartyCategory != null) {
            q.withQuery(matchQuery("partyCategory.idCategory", idPartyCategory));
        }
        else if (idProductCategory != null) {
            q.withQuery(matchQuery("productCategory.idCategory", idProductCategory));
        }
        else if (idDimension1 != null) {
            q.withQuery(matchQuery("dimension1.idDimension", idDimension1));
        }
        else if (idDimension2 != null) {
            q.withQuery(matchQuery("dimension2.idDimension", idDimension2));
        }
        else if (idDimension3 != null) {
            q.withQuery(matchQuery("dimension3.idDimension", idDimension3));
        }
        else if (idDimension4 != null) {
            q.withQuery(matchQuery("dimension4.idDimension", idDimension4));
        }
        else if (idDimension5 != null) {
            q.withQuery(matchQuery("dimension5.idDimension", idDimension5));
        }
        else if (idDimension6 != null) {
            q.withQuery(matchQuery("dimension6.idDimension", idDimension6));
        }

        Page<GLDimension> result = gLDimensionSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(gLDimensionMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<GLDimensionDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered GLDimensionDTO");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");
        String idVendor = request.getParameter("idVendor");
        String idPartyCategory = request.getParameter("idPartyCategory");
        String idProductCategory = request.getParameter("idProductCategory");
        String idDimension1 = request.getParameter("idDimension1");
        String idDimension2 = request.getParameter("idDimension2");
        String idDimension3 = request.getParameter("idDimension3");
        String idDimension4 = request.getParameter("idDimension4");
        String idDimension5 = request.getParameter("idDimension5");
        String idDimension6 = request.getParameter("idDimension6");

        if (idInternal != null) {
            return gLDimensionRepository.queryByIdInternal(idInternal, pageable).map(gLDimensionMapper::toDto);
        }
        else if (idBillTo != null) {
            return gLDimensionRepository.queryByIdBillTo(idBillTo, pageable).map(gLDimensionMapper::toDto);
        }
        else if (idVendor != null) {
            return gLDimensionRepository.queryByIdVendor(idVendor, pageable).map(gLDimensionMapper::toDto);
        }
        else if (idPartyCategory != null) {
            return gLDimensionRepository.queryByIdPartyCategory(Integer.valueOf(idPartyCategory), pageable).map(gLDimensionMapper::toDto);
        }
        else if (idProductCategory != null) {
            return gLDimensionRepository.queryByIdProductCategory(Long.valueOf(idProductCategory), pageable).map(gLDimensionMapper::toDto);
        }
        else if (idDimension1 != null) {
            return gLDimensionRepository.queryByIdDimension1(Integer.valueOf(idDimension1), pageable).map(gLDimensionMapper::toDto);
        }
        else if (idDimension2 != null) {
            return gLDimensionRepository.queryByIdDimension2(Integer.valueOf(idDimension2), pageable).map(gLDimensionMapper::toDto);
        }
        else if (idDimension3 != null) {
            return gLDimensionRepository.queryByIdDimension3(Integer.valueOf(idDimension3), pageable).map(gLDimensionMapper::toDto);
        }
        else if (idDimension4 != null) {
            return gLDimensionRepository.queryByIdDimension4(Integer.valueOf(idDimension4), pageable).map(gLDimensionMapper::toDto);
        }
        else if (idDimension5 != null) {
            return gLDimensionRepository.queryByIdDimension5(Integer.valueOf(idDimension5), pageable).map(gLDimensionMapper::toDto);
        }
        else if (idDimension6 != null) {
            return gLDimensionRepository.queryByIdDimension6(Integer.valueOf(idDimension6), pageable).map(gLDimensionMapper::toDto);
        }

        return gLDimensionRepository.queryNothing(pageable).map(gLDimensionMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, GLDimensionDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<GLDimensionDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
