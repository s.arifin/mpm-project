package id.atiila.service;

import id.atiila.domain.Internal;
import id.atiila.domain.ProductShipmentReceipt;
import id.atiila.repository.ProductShipmentReceiptRepository;
import id.atiila.repository.search.ProductShipmentReceiptSearchRepository;
import id.atiila.service.dto.ProductShipmentReceiptDTO;
import id.atiila.service.mapper.ProductShipmentReceiptMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ProductShipmentReceipt.
 * BeSmart Team
 */

@Service
@Transactional
public class ProductShipmentReceiptService {

    private final Logger log = LoggerFactory.getLogger(ProductShipmentReceiptService.class);

    private final ProductShipmentReceiptRepository productShipmentReceiptRepository;

    private final ProductShipmentReceiptMapper productShipmentReceiptMapper;

    private final ProductShipmentReceiptSearchRepository productShipmentReceiptSearchRepository;

    @Autowired
    private PartyUtils partyUtils;

    public ProductShipmentReceiptService(ProductShipmentReceiptRepository productShipmentReceiptRepository, ProductShipmentReceiptMapper productShipmentReceiptMapper, ProductShipmentReceiptSearchRepository productShipmentReceiptSearchRepository) {
        this.productShipmentReceiptRepository = productShipmentReceiptRepository;
        this.productShipmentReceiptMapper = productShipmentReceiptMapper;
        this.productShipmentReceiptSearchRepository = productShipmentReceiptSearchRepository;
    }

    /**
     * Save a productShipmentReceipt.
     *
     * @param productShipmentReceiptDTO the entity to save
     * @return the persisted entity
     */
    public ProductShipmentReceiptDTO save(ProductShipmentReceiptDTO productShipmentReceiptDTO) {
        log.debug("Request to save ProductShipmentReceipt : {}", productShipmentReceiptDTO);
        ProductShipmentReceipt productShipmentReceipt = productShipmentReceiptMapper.toEntity(productShipmentReceiptDTO);
        productShipmentReceipt = productShipmentReceiptRepository.save(productShipmentReceipt);
        ProductShipmentReceiptDTO result = productShipmentReceiptMapper.toDto(productShipmentReceipt);
        productShipmentReceiptSearchRepository.save(productShipmentReceipt);
        return result;
    }

    /**
     * Get all the productShipmentReceipts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductShipmentReceiptDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductShipmentReceipts");
        return productShipmentReceiptRepository.findActiveProductShipmentReceipt(pageable)
            .map(productShipmentReceiptMapper::toDto);
    }

    /**
     * Get one productShipmentReceipt by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ProductShipmentReceiptDTO findOne(UUID id) {
        log.debug("Request to get ProductShipmentReceipt : {}", id);
        ProductShipmentReceipt productShipmentReceipt = productShipmentReceiptRepository.findOne(id);
        return productShipmentReceiptMapper.toDto(productShipmentReceipt);
    }

    /**
     * Delete the productShipmentReceipt by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ProductShipmentReceipt : {}", id);
        productShipmentReceiptRepository.delete(id);
        productShipmentReceiptSearchRepository.delete(id);
    }

    /**
     * Search for the productShipmentReceipt corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductShipmentReceiptDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of ProductShipmentReceipts for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idShipmentPackage = request.getParameter("idShipmentPackage");
        String idShipmentItem = request.getParameter("idShipmentItem");
        String idOrderItem = request.getParameter("idOrderItem");

        if (filterName != null) {
        }
        Page<ProductShipmentReceipt> result = productShipmentReceiptSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(productShipmentReceiptMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ProductShipmentReceiptDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ProductShipmentReceiptDTO");
        String idShipmentPackage = request.getParameter("idShipmentPackage");

        if (idShipmentPackage != null) {
            return productShipmentReceiptRepository.findByIdPackage(UUID.fromString(idShipmentPackage), pageable)
                .map(productShipmentReceiptMapper::toDto);
        }

        return productShipmentReceiptRepository.queryNothing(pageable).map(productShipmentReceiptMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public ProductShipmentReceiptDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        ProductShipmentReceiptDTO r = null;
        return r;
    }

    @Transactional
    public Set<ProductShipmentReceiptDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<ProductShipmentReceiptDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public ProductShipmentReceiptDTO changeProductShipmentReceiptStatus(ProductShipmentReceiptDTO dto, Integer id) {
        if (dto != null) {
			ProductShipmentReceipt e = productShipmentReceiptRepository.findOne(dto.getIdReceipt());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        productShipmentReceiptSearchRepository.delete(dto.getIdReceipt());
                        break;
                    default:
                        productShipmentReceiptSearchRepository.save(e);
                }
				productShipmentReceiptRepository.save(e);
			}
		}
        return dto;
    }
}
