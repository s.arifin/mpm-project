package id.atiila.service;

import id.atiila.domain.StockOpnameInventory;
import id.atiila.repository.StockOpnameInventoryRepository;
import id.atiila.repository.search.StockOpnameInventorySearchRepository;
import id.atiila.service.dto.StockOpnameInventoryDTO;
import id.atiila.service.mapper.StockOpnameInventoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing StockOpnameInventory.
 * BeSmart Team
 */

@Service
@Transactional
public class StockOpnameInventoryService {

    private final Logger log = LoggerFactory.getLogger(StockOpnameInventoryService.class);

    private final StockOpnameInventoryRepository stockOpnameInventoryRepository;

    private final StockOpnameInventoryMapper stockOpnameInventoryMapper;

    private final StockOpnameInventorySearchRepository stockOpnameInventorySearchRepository;

    public StockOpnameInventoryService(StockOpnameInventoryRepository stockOpnameInventoryRepository, StockOpnameInventoryMapper stockOpnameInventoryMapper, StockOpnameInventorySearchRepository stockOpnameInventorySearchRepository) {
        this.stockOpnameInventoryRepository = stockOpnameInventoryRepository;
        this.stockOpnameInventoryMapper = stockOpnameInventoryMapper;
        this.stockOpnameInventorySearchRepository = stockOpnameInventorySearchRepository;
    }

    /**
     * Save a stockOpnameInventory.
     *
     * @param stockOpnameInventoryDTO the entity to save
     * @return the persisted entity
     */
    public StockOpnameInventoryDTO save(StockOpnameInventoryDTO stockOpnameInventoryDTO) {
        log.debug("Request to save StockOpnameInventory : {}", stockOpnameInventoryDTO);
        StockOpnameInventory stockOpnameInventory = stockOpnameInventoryMapper.toEntity(stockOpnameInventoryDTO);
        stockOpnameInventory = stockOpnameInventoryRepository.save(stockOpnameInventory);
        StockOpnameInventoryDTO result = stockOpnameInventoryMapper.toDto(stockOpnameInventory);
        stockOpnameInventorySearchRepository.save(stockOpnameInventory);
        return result;
    }

    /**
     * Get all the stockOpnameInventories.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StockOpnameInventoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all StockOpnameInventories");
        return stockOpnameInventoryRepository.findAll(pageable)
            .map(stockOpnameInventoryMapper::toDto);
    }

    /**
     * Get one stockOpnameInventory by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public StockOpnameInventoryDTO findOne(UUID id) {
        log.debug("Request to get StockOpnameInventory : {}", id);
        StockOpnameInventory stockOpnameInventory = stockOpnameInventoryRepository.findOne(id);
        return stockOpnameInventoryMapper.toDto(stockOpnameInventory);
    }

    /**
     * Delete the stockOpnameInventory by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete StockOpnameInventory : {}", id);
        stockOpnameInventoryRepository.delete(id);
        stockOpnameInventorySearchRepository.delete(id);
    }

    /**
     * Search for the stockOpnameInventory corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StockOpnameInventoryDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of StockOpnameInventories for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idInventoryItem = request.getParameter("idInventoryItem");
        String idItem = request.getParameter("idItem");

        if (idInventoryItem != null) {
            q.withQuery(matchQuery("inventoryItem.idInventoryItem", idInventoryItem));
        }
        else if (idItem != null) {
            q.withQuery(matchQuery("item.idStockopnameItem", idItem));
        }

        Page<StockOpnameInventory> result = stockOpnameInventorySearchRepository.search(q.build().getQuery(), pageable);
        return result.map(stockOpnameInventoryMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<StockOpnameInventoryDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered StockOpnameInventoryDTO");
        String idInventoryItem = request.getParameter("idInventoryItem");
        String idItem = request.getParameter("idItem");

        if (idInventoryItem != null) {
            return stockOpnameInventoryRepository.queryByIdInventoryItem(UUID.fromString(idInventoryItem), pageable).map(stockOpnameInventoryMapper::toDto); 
        }
        else if (idItem != null) {
            return stockOpnameInventoryRepository.queryByIdItem(UUID.fromString(idItem), pageable).map(stockOpnameInventoryMapper::toDto); 
        }

        return stockOpnameInventoryRepository.queryNothing(pageable).map(stockOpnameInventoryMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, StockOpnameInventoryDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<StockOpnameInventoryDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
