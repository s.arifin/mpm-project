package id.atiila.service;

import id.atiila.domain.ShipmentType;
import id.atiila.repository.ShipmentTypeRepository;
import id.atiila.repository.search.ShipmentTypeSearchRepository;
import id.atiila.service.dto.ShipmentTypeDTO;
import id.atiila.service.mapper.ShipmentTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ShipmentType.
 * BeSmart Team
 */

@Service
@Transactional
public class ShipmentTypeService {

    private final Logger log = LoggerFactory.getLogger(ShipmentTypeService.class);

    private final ShipmentTypeRepository shipmentTypeRepository;

    private final ShipmentTypeMapper shipmentTypeMapper;

    private final ShipmentTypeSearchRepository shipmentTypeSearchRepository;

    public ShipmentTypeService(ShipmentTypeRepository shipmentTypeRepository, ShipmentTypeMapper shipmentTypeMapper, ShipmentTypeSearchRepository shipmentTypeSearchRepository) {
        this.shipmentTypeRepository = shipmentTypeRepository;
        this.shipmentTypeMapper = shipmentTypeMapper;
        this.shipmentTypeSearchRepository = shipmentTypeSearchRepository;
    }

    /**
     * Save a shipmentType.
     *
     * @param shipmentTypeDTO the entity to save
     * @return the persisted entity
     */
    public ShipmentTypeDTO save(ShipmentTypeDTO shipmentTypeDTO) {
        log.debug("Request to save ShipmentType : {}", shipmentTypeDTO);
        ShipmentType shipmentType = shipmentTypeMapper.toEntity(shipmentTypeDTO);
        shipmentType = shipmentTypeRepository.save(shipmentType);
        ShipmentTypeDTO result = shipmentTypeMapper.toDto(shipmentType);
        shipmentTypeSearchRepository.save(shipmentType);
        return result;
    }

    /**
     *  Get all the shipmentTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShipmentTypes");
        return shipmentTypeRepository.findAll(pageable)
            .map(shipmentTypeMapper::toDto);
    }

    /**
     *  Get one shipmentType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ShipmentTypeDTO findOne(Integer id) {
        log.debug("Request to get ShipmentType : {}", id);
        ShipmentType shipmentType = shipmentTypeRepository.findOne(id);
        return shipmentTypeMapper.toDto(shipmentType);
    }

    /**
     *  Delete the  shipmentType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete ShipmentType : {}", id);
        shipmentTypeRepository.delete(id);
        shipmentTypeSearchRepository.delete(id);
    }

    /**
     * Search for the shipmentType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ShipmentTypes for query {}", query);
        Page<ShipmentType> result = shipmentTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(shipmentTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        shipmentTypeSearchRepository.deleteAll();
        List<ShipmentType> shipmentTypes =  shipmentTypeRepository.findAll();
        for (ShipmentType m: shipmentTypes) {
            shipmentTypeSearchRepository.save(m);
            log.debug("Data shipment type save !...");
        }
    }

}
