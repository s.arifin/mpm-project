package id.atiila.service;

import id.atiila.domain.BookingSlotStandard;
import id.atiila.repository.BookingSlotStandardRepository;
import id.atiila.repository.search.BookingSlotStandardSearchRepository;
import id.atiila.service.dto.BookingSlotStandardDTO;
import id.atiila.service.mapper.BookingSlotStandardMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing BookingSlotStandard.
 * BeSmart Team
 */

@Service
@Transactional
public class BookingSlotStandardService {

    private final Logger log = LoggerFactory.getLogger(BookingSlotStandardService.class);

    private final BookingSlotStandardRepository bookingSlotStandardRepository;

    private final BookingSlotStandardMapper bookingSlotStandardMapper;

    private final BookingSlotStandardSearchRepository bookingSlotStandardSearchRepository;
    public BookingSlotStandardService(BookingSlotStandardRepository bookingSlotStandardRepository, BookingSlotStandardMapper bookingSlotStandardMapper, BookingSlotStandardSearchRepository bookingSlotStandardSearchRepository) {
        this.bookingSlotStandardRepository = bookingSlotStandardRepository;
        this.bookingSlotStandardMapper = bookingSlotStandardMapper;
        this.bookingSlotStandardSearchRepository = bookingSlotStandardSearchRepository;
    }

    /**
     * Save a bookingSlotStandard.
     *
     * @param bookingSlotStandardDTO the entity to save
     * @return the persisted entity
     */
    public BookingSlotStandardDTO save(BookingSlotStandardDTO bookingSlotStandardDTO) {
        log.debug("Request to save BookingSlotStandard : {}", bookingSlotStandardDTO);
        BookingSlotStandard bookingSlotStandard = bookingSlotStandardMapper.toEntity(bookingSlotStandardDTO);
        bookingSlotStandard = bookingSlotStandardRepository.save(bookingSlotStandard);
        BookingSlotStandardDTO result = bookingSlotStandardMapper.toDto(bookingSlotStandard);
        bookingSlotStandardSearchRepository.save(bookingSlotStandard);
        return result;
    }

    /**
     *  Get all the bookingSlotStandards.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BookingSlotStandardDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BookingSlotStandards");
        return bookingSlotStandardRepository.findAll(pageable)
            .map(bookingSlotStandardMapper::toDto);
    }

    /**
     *  Get one bookingSlotStandard by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public BookingSlotStandardDTO findOne(UUID id) {
        log.debug("Request to get BookingSlotStandard : {}", id);
        BookingSlotStandard bookingSlotStandard = bookingSlotStandardRepository.findOne(id);
        return bookingSlotStandardMapper.toDto(bookingSlotStandard);
    }

    /**
     *  Delete the  bookingSlotStandard by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete BookingSlotStandard : {}", id);
        bookingSlotStandardRepository.delete(id);
        bookingSlotStandardSearchRepository.delete(id);
    }

    /**
     * Search for the bookingSlotStandard corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BookingSlotStandardDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of BookingSlotStandards for query {}", query);
        Page<BookingSlotStandard> result = bookingSlotStandardSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(bookingSlotStandardMapper::toDto);
    }

    public BookingSlotStandardDTO processExecuteData(Integer id, String param, BookingSlotStandardDTO dto) {
        BookingSlotStandardDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<BookingSlotStandardDTO> processExecuteListData(Integer id, String param, Set<BookingSlotStandardDTO> dto) {
        Set<BookingSlotStandardDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
