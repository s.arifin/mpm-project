package id.atiila.service;

import id.atiila.domain.QuoteItem;
import id.atiila.repository.QuoteItemRepository;
import id.atiila.repository.search.QuoteItemSearchRepository;
import id.atiila.service.dto.QuoteItemDTO;
import id.atiila.service.mapper.QuoteItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing QuoteItem.
 * BeSmart Team
 */

@Service
@Transactional
public class QuoteItemService {

    private final Logger log = LoggerFactory.getLogger(QuoteItemService.class);

    private final QuoteItemRepository quoteItemRepository;

    private final QuoteItemMapper quoteItemMapper;

    private final QuoteItemSearchRepository quoteItemSearchRepository;

    public QuoteItemService(QuoteItemRepository quoteItemRepository, QuoteItemMapper quoteItemMapper, QuoteItemSearchRepository quoteItemSearchRepository) {
        this.quoteItemRepository = quoteItemRepository;
        this.quoteItemMapper = quoteItemMapper;
        this.quoteItemSearchRepository = quoteItemSearchRepository;
    }

    /**
     * Save a quoteItem.
     *
     * @param quoteItemDTO the entity to save
     * @return the persisted entity
     */
    public QuoteItemDTO save(QuoteItemDTO quoteItemDTO) {
        log.debug("Request to save QuoteItem : {}", quoteItemDTO);
        QuoteItem quoteItem = quoteItemMapper.toEntity(quoteItemDTO);
        quoteItem = quoteItemRepository.save(quoteItem);
        QuoteItemDTO result = quoteItemMapper.toDto(quoteItem);
        quoteItemSearchRepository.save(quoteItem);
        return result;
    }

    /**
     *  Get all the quoteItems.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<QuoteItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all QuoteItems");
        return quoteItemRepository.findAll(pageable)
            .map(quoteItemMapper::toDto);
    }

    /**
     *  Get one quoteItem by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public QuoteItemDTO findOne(UUID id) {
        log.debug("Request to get QuoteItem : {}", id);
        QuoteItem quoteItem = quoteItemRepository.findOne(id);
        return quoteItemMapper.toDto(quoteItem);
    }

    /**
     *  Delete the  quoteItem by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete QuoteItem : {}", id);
        quoteItemRepository.delete(id);
        quoteItemSearchRepository.delete(id);
    }

    /**
     * Search for the quoteItem corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<QuoteItemDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of QuoteItems for query {}", query);
        Page<QuoteItem> result = quoteItemSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(quoteItemMapper::toDto);
    }

    public QuoteItemDTO processExecuteData(Integer id, String param, QuoteItemDTO dto) {
        QuoteItemDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<QuoteItemDTO> processExecuteListData(Integer id, String param, Set<QuoteItemDTO> dto) {
        Set<QuoteItemDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
