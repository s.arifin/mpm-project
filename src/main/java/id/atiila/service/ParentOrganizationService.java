package id.atiila.service;

import id.atiila.domain.Organization;
import id.atiila.domain.ParentOrganization;
import id.atiila.repository.ParentOrganizationRepository;
import id.atiila.repository.search.ParentOrganizationSearchRepository;
import id.atiila.service.dto.ParentOrganizationDTO;
import id.atiila.service.mapper.ParentOrganizationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Service Implementation for managing ParentOrganization.
 * BeSmart Team
 */

@Service
@Transactional
public class ParentOrganizationService {

    private final Logger log = LoggerFactory.getLogger(ParentOrganizationService.class);

    private final ParentOrganizationRepository parentOrganizationRepository;

    private final ParentOrganizationMapper parentOrganizationMapper;

    private final ParentOrganizationSearchRepository parentOrganizationSearchRepository;

    @Autowired
    private InternalUtils internalUtils;

    public ParentOrganizationService(ParentOrganizationRepository parentOrganizationRepository, ParentOrganizationMapper parentOrganizationMapper, ParentOrganizationSearchRepository parentOrganizationSearchRepository) {
        this.parentOrganizationRepository = parentOrganizationRepository;
        this.parentOrganizationMapper = parentOrganizationMapper;
        this.parentOrganizationSearchRepository = parentOrganizationSearchRepository;
    }

    /**
     * Save a parentOrganization.
     *
     * @param dto the entity to save
     * @return the persisted entity
     */
    public ParentOrganizationDTO save(ParentOrganizationDTO dto) {
        log.debug("Request to save ParentOrganization : {}", dto);
        ParentOrganization parentOrganization = parentOrganizationMapper.toEntity(dto);

        Boolean isNewData = parentOrganization.getOrganization().getIdParty() == null;
        if (isNewData) {
            parentOrganization = internalUtils.buildParent(parentOrganization.getIdInternal(), parentOrganization.getOrganization().getName());
        }

        parentOrganization = parentOrganizationRepository.save(parentOrganization);
        ParentOrganizationDTO result = parentOrganizationMapper.toDto(parentOrganization);
        parentOrganizationSearchRepository.save(parentOrganization);

        return result;
    }

    /**
     *  Get all the parentOrganizations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ParentOrganizationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ParentOrganizations");
        return parentOrganizationRepository.findAll(pageable)
            .map(parentOrganizationMapper::toDto);
    }

    /**
     *  Get one parentOrganization by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ParentOrganizationDTO findOne(String id) {
        log.debug("Request to get ParentOrganization : {}", id);
        ParentOrganization parentOrganization = parentOrganizationRepository.findOne(id);
        return parentOrganizationMapper.toDto(parentOrganization);
    }

    /**
     *  Delete the  parentOrganization by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete ParentOrganization : {}", id);
        parentOrganizationRepository.delete(id);
        parentOrganizationSearchRepository.delete(id);
    }

    /**
     * Search for the parentOrganization corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ParentOrganizationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ParentOrganizations for query {}", query);
        Page<ParentOrganization> result = parentOrganizationSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(parentOrganizationMapper::toDto);
    }

    public ParentOrganizationDTO processExecuteData(ParentOrganizationDTO dto, Integer id) {
        ParentOrganizationDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        parentOrganizationSearchRepository.deleteAll();
        List<ParentOrganization> parentOrganizations =  parentOrganizationRepository.findAll();
        for (ParentOrganization m: parentOrganizations) {
            parentOrganizationSearchRepository.save(m);
            log.debug("Data parentOrganization save !...");
        }
    }

}
