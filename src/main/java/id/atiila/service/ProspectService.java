package id.atiila.service;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import java.util.*;
import id.atiila.domain.Internal;
import id.atiila.domain.Salesman;
import id.atiila.repository.SalesmanRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import id.atiila.domain.Prospect;
import id.atiila.repository.ProspectRepository;
import id.atiila.repository.search.ProspectSearchRepository;
import id.atiila.service.dto.ProspectDTO;
import id.atiila.service.mapper.ProspectMapper;
import javax.servlet.http.HttpServletRequest;

/**
 * Service Implementation for managing Prospect.
 * BeSmart Team
 */

@Service
@Transactional
public class ProspectService {

    private final Logger log = LoggerFactory.getLogger(ProspectService.class);

    private final ProspectRepository prospectRepository;

    private final ProspectMapper prospectMapper;

    private final ProspectSearchRepository prospectSearchRepository;

    @Autowired
    protected MasterNumberingService numberingService;

    @Autowired
    private SalesmanRepository salesmanRepository;

    @Autowired
    private MasterNumberingService masterNumberingService;

    public ProspectService(ProspectRepository prospectRepository, ProspectMapper prospectMapper, ProspectSearchRepository prospectSearchRepository) {
        this.prospectRepository = prospectRepository;
        this.prospectMapper = prospectMapper;
        this.prospectSearchRepository = prospectSearchRepository;
    }

    /**
     * Save a prospect.
     *
     * @param prospectDTO the entity to save
     * @return the persisted entity
     */
    public ProspectDTO save(ProspectDTO prospectDTO) {
        log.debug("Request to save Prospect : {}", prospectDTO);
        Prospect prospect = prospectMapper.toEntity(prospectDTO);
        prospect = prospectRepository.save(prospect);
        ProspectDTO result = prospectMapper.toDto(prospect);
        prospectSearchRepository.save(result);
        return fillData(result);
    }

    /**
     *  Get all the prospects.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProspectDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Prospects");
        return prospectRepository.findActiveProspect(pageable)
            .map(prospectMapper::toDto)
            .map(this::fillData);
    }

    /**
     *  Get one prospect by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ProspectDTO findOne(UUID id) {
        log.debug("Request to get Prospect : {}", id);
        Prospect prospect = prospectRepository.findOne(id);
        return fillData(prospectMapper.toDto(prospect));
    }

    /**
     *  Delete the  prospect by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Prospect : {}", id);
        prospectRepository.delete(id);
        prospectSearchRepository.delete(id);
    }

    /**
     * Search for the prospect corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProspectDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Prospects for query {}", query);
        Page<ProspectDTO> result = prospectSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(this::fillData);
    }

    public ProspectDTO processExecuteData(Integer id, String param, ProspectDTO dto) {
        ProspectDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

//    @Transactional(propagation = Propagation.REQUIRED)
//    public String getNewProspectNumber() {
//        String pR = null;
//        do {
//            pR = numberingService.nextProspectNumberValue();
//        } while (prospectRepository.findOne(pR) != null);
//        return pR;
//    }

    public Set<ProspectDTO> processExecuteListData(Integer id, String param, Set<ProspectDTO> dto) {
        Set<ProspectDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public ProspectDTO changeProspectStatus(ProspectDTO dto, Integer id) {
        if (dto != null) {
			Prospect e = prospectRepository.findOne(dto.getIdProspect());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        prospectSearchRepository.delete(dto.getIdProspect());
                        break;
                    default:
                        prospectSearchRepository.save(dto);
                }
				prospectRepository.save(e);
			}
		}
        return dto;
    }

    public ProspectDTO fillData(ProspectDTO dto) {
        Salesman salesman = null;
        if (dto.getIdSalesCoordinator() != null) {
            salesman = salesmanRepository.findOne(dto.getIdSalesCoordinator());
            if (salesman.getPerson() != null) dto.setSalesCoordinatorName(salesman.getPerson().getName());
        }
        return dto;
    }

    @Transactional
    public void createProspectNumber(String prospectNumber, Internal internal){
        log.debug("START find or create prospectnumber code");
        Prospect e = prospectRepository.findByProspectNumber(prospectNumber);
        if (e.getProspectNumber() == null) {
            String pnumber = masterNumberingService.nextProspectNumber(internal.getIdInternal(), "IDPRO") ;
            e.setProspectNumber(pnumber);
            prospectRepository.save(e);
        }
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public void closeProspect(Prospect prospect) {
//        List<Prospect> listProspectVsoDraft = prospectRepository.findProspectVsoDraft(prospect.getIdProspect());
        List<Prospect> listProspectVsoComplete = prospectRepository.findProspectVsoComplete(prospect.getIdProspect());
        List<Prospect> listProspectSur = prospectRepository.findProspectSur(prospect.getIdProspect());

        Integer prospectComplete = listProspectVsoComplete.size();
//        Integer prospectDraft = listProspectVsoDraft.size();
        Integer prospectSur = listProspectSur.size();
        log.debug("close status prospcet atas prospectComplete ===== " + prospectComplete);
//        log.debug("close status prospcet atas prospectDraft ===== " + prospectDraft);
        log.debug("close status prospcet atas prospectSur ===== " + prospectSur);
        if (prospectComplete == prospectSur) {
            log.debug("close status prospcet =====");
            Prospect prospects = prospectRepository.findOne(prospect.getIdProspect());
            prospects.setStatus(17);
            prospects = prospectRepository.save(prospects);
        }
    }

    @Async
    public void buildIndex() {
        prospectSearchRepository.deleteAll();
        List<Prospect> prospects =  prospectRepository.findAll();
        for (Prospect m: prospects) {
            prospectSearchRepository.save(m);
            log.debug("Data prospect save !...");
        }
    }

}

