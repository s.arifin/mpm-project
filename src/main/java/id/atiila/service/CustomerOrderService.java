package id.atiila.service;

import id.atiila.domain.CustomerOrder;
import id.atiila.repository.CustomerOrderRepository;
import id.atiila.repository.search.CustomerOrderSearchRepository;
import id.atiila.service.dto.CustomerOrderDTO;
import id.atiila.service.mapper.CustomerOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing CustomerOrder.
 * BeSmart Team
 */

@Service
@Transactional
public class CustomerOrderService {

    private final Logger log = LoggerFactory.getLogger(CustomerOrderService.class);

    private final CustomerOrderRepository customerOrderRepository;

    private final CustomerOrderMapper customerOrderMapper;

    private final CustomerOrderSearchRepository customerOrderSearchRepository;

    public CustomerOrderService(CustomerOrderRepository customerOrderRepository, CustomerOrderMapper customerOrderMapper, CustomerOrderSearchRepository customerOrderSearchRepository) {
        this.customerOrderRepository = customerOrderRepository;
        this.customerOrderMapper = customerOrderMapper;
        this.customerOrderSearchRepository = customerOrderSearchRepository;
    }

    /**
     * Save a customerOrder.
     *
     * @param customerOrderDTO the entity to save
     * @return the persisted entity
     */
    public CustomerOrderDTO save(CustomerOrderDTO customerOrderDTO) {
        log.debug("Request to save CustomerOrder : {}", customerOrderDTO);
        CustomerOrder customerOrder = customerOrderMapper.toEntity(customerOrderDTO);
        customerOrder = customerOrderRepository.save(customerOrder);
        CustomerOrderDTO result = customerOrderMapper.toDto(customerOrder);
        customerOrderSearchRepository.save(customerOrder);
        return result;
    }

    /**
     * Get all the customerOrders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CustomerOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CustomerOrders");
        return customerOrderRepository.findActiveCustomerOrder(pageable)
            .map(customerOrderMapper::toDto);
    }

    /**
     * Get one customerOrder by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CustomerOrderDTO findOne(UUID id) {
        log.debug("Request to get CustomerOrder : {}", id);
        CustomerOrder customerOrder = customerOrderRepository.findOne(id);
        return customerOrderMapper.toDto(customerOrder);
    }

    /**
     * Delete the customerOrder by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete CustomerOrder : {}", id);
        customerOrderRepository.delete(id);
        customerOrderSearchRepository.delete(id);
    }

    /**
     * Search for the customerOrder corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CustomerOrderDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of CustomerOrders for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idInternal = request.getParameter("idInternal");
        String idCustomer = request.getParameter("idCustomer");
        String idBillTo = request.getParameter("idBillTo");

        if (filterName != null) {
        }
        Page<CustomerOrder> result = customerOrderSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(customerOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<CustomerOrderDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered CustomerOrderDTO");
        String idInternal = request.getParameter("idInternal");
        String idCustomer = request.getParameter("idCustomer");
        String idBillTo = request.getParameter("idBillTo");

        return customerOrderRepository.findByParams(idInternal, idCustomer, idBillTo, pageable)
            .map(customerOrderMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public CustomerOrderDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        CustomerOrderDTO r = null;
        return r;
    }

    @Transactional
    public Set<CustomerOrderDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<CustomerOrderDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public CustomerOrderDTO changeCustomerOrderStatus(CustomerOrderDTO dto, Integer id) {
        if (dto != null) {
			CustomerOrder e = customerOrderRepository.findOne(dto.getIdOrder());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        customerOrderSearchRepository.delete(dto.getIdOrder());
                        break;
                    default:
                        customerOrderSearchRepository.save(e);
                }
				customerOrderRepository.save(e);
			}
		}
        return dto;
    }
}
