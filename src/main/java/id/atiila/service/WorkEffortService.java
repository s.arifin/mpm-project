package id.atiila.service;

import id.atiila.domain.WorkEffort;
import id.atiila.repository.WorkEffortRepository;
import id.atiila.repository.search.WorkEffortSearchRepository;
import id.atiila.service.dto.WorkEffortDTO;
import id.atiila.service.mapper.WorkEffortMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Service Implementation for managing WorkEffort.
 * BeSmart Team
 */

@Service
@Transactional
public class WorkEffortService {

    private final Logger log = LoggerFactory.getLogger(WorkEffortService.class);

    private final WorkEffortRepository workEffortRepository;

    private final WorkEffortMapper workEffortMapper;

    private final WorkEffortSearchRepository workEffortSearchRepository;

    public WorkEffortService(WorkEffortRepository workEffortRepository, WorkEffortMapper workEffortMapper, WorkEffortSearchRepository workEffortSearchRepository) {
        this.workEffortRepository = workEffortRepository;
        this.workEffortMapper = workEffortMapper;
        this.workEffortSearchRepository = workEffortSearchRepository;
    }

    /**
     * Save a workEffort.
     *
     * @param workEffortDTO the entity to save
     * @return the persisted entity
     */
    public WorkEffortDTO save(WorkEffortDTO workEffortDTO) {
        log.debug("Request to save WorkEffort : {}", workEffortDTO);
        WorkEffort workEffort = workEffortMapper.toEntity(workEffortDTO);
        workEffort = workEffortRepository.save(workEffort);
        WorkEffortDTO result = workEffortMapper.toDto(workEffort);
        workEffortSearchRepository.save(workEffort);
        return result;
    }

    /**
     *  Get all the workEfforts.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WorkEffortDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WorkEfforts");
        return workEffortRepository.findActiveWorkEffort(pageable)
            .map(workEffortMapper::toDto);
    }

    /**
     *  Get one workEffort by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public WorkEffortDTO findOne(UUID id) {
        log.debug("Request to get WorkEffort : {}", id);
        WorkEffort workEffort = workEffortRepository.findOneWithEagerRelationships(id);
        return workEffortMapper.toDto(workEffort);
    }

    /**
     *  Delete the  workEffort by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete WorkEffort : {}", id);
        workEffortRepository.delete(id);
        workEffortSearchRepository.delete(id);
    }

    /**
     * Search for the workEffort corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WorkEffortDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WorkEfforts for query {}", query);
        Page<WorkEffort> result = workEffortSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(workEffortMapper::toDto);
    }

    public WorkEffortDTO changeWorkEffortStatus(WorkEffortDTO dto, Integer id) {
        if (dto != null) {
			WorkEffort e = workEffortRepository.findOne(dto.getIdWe());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        workEffortSearchRepository.delete(dto.getIdWe());
                        break;
                    default:
                        workEffortSearchRepository.save(e);
                }
				workEffortRepository.save(e);
			}
		}
        return dto;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        workEffortSearchRepository.deleteAll();
        List<WorkEffort> workEfforts =  workEffortRepository.findAll();
        for (WorkEffort m: workEfforts) {
            workEffortSearchRepository.save(m);
            log.debug("Data work effort save !...");
        }
    }
}
