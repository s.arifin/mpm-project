package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.*;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import id.atiila.base.DmsException;
import id.atiila.base.XlsxUtils;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.service.dto.*;
import id.atiila.service.pto.UnitDeliverablePTO;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.atiila.base.BaseConstants;
import id.atiila.repository.search.UnitDeliverableSearchRepository;
import id.atiila.service.mapper.UnitDeliverableMapper;

import javax.servlet.http.HttpServletRequest;

/**
 * Service Implementation for managing UnitDeliverable.
 * BeSmart Team
 */

@Service
@Transactional
public class UnitDeliverableService {

    private final Logger log = LoggerFactory.getLogger(UnitDeliverableService.class);

    private final UnitDeliverableRepository unitDeliverableRepository;

    private final UnitDeliverableMapper unitDeliverableMapper;

    private final UnitDeliverableSearchRepository unitDeliverableSearchRepository;

    @Autowired
    private VehicleDocumentRequirementRepository vehicleDocumentRequirementRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private VehicleSalesOrderRepository vehicleSalesOrderRepository;

    @Autowired
    private SalesUnitRequirementRepository salesUnitRequirementRepository;

    @Autowired
    private MasterNumberingService masterNumberingService;

    @Autowired
    private VehicleDocumentRequirementService vdrService;

    @Autowired
    private VehicleSalesOrderService vsoService;

    @Autowired
    private SalesUnitRequirementService surService;

    @Autowired
    private VendorService vendorService;

    @Autowired
    private OrderItemService orderItemService;

    @Autowired
    private LeasingCompanyService leasingCompanyService;

    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private ActivitiProcessor activitiProcessor;


    public UnitDeliverableService(UnitDeliverableRepository unitDeliverableRepository,
                                  UnitDeliverableMapper unitDeliverableMapper,
                                  UnitDeliverableSearchRepository unitDeliverableSearchRepository) {
        this.unitDeliverableRepository = unitDeliverableRepository;
        this.unitDeliverableMapper = unitDeliverableMapper;
        this.unitDeliverableSearchRepository = unitDeliverableSearchRepository;
    }

    /**
     * Save a unitDeliverable.
     *
     * @param unitDeliverableDTO the entity to save
     * @return the persisted entity
     */
    public UnitDeliverableDTO save(UnitDeliverableDTO unitDeliverableDTO) {
        log.debug("Request to save UnitDeliverable : {}", unitDeliverableDTO);
        UnitDeliverable unitDeliverable = unitDeliverableMapper.toEntity(unitDeliverableDTO);
        log.debug("iiiiiiiiiiiiiiiiiiiiiii" + unitDeliverable);
        unitDeliverable = unitDeliverableRepository.save(unitDeliverable);
        UnitDeliverableDTO result = unitDeliverableMapper.toDto(unitDeliverable);
        unitDeliverableSearchRepository.save(unitDeliverable);
        return result;
    }

    /**
     * Save a migrationDataUnitDeliverable.
     *
     * @param customBpkbDTO the entity to save
     * @return the persisted entity
     */
//    public CustomBpkbDTO migrationData(CustomBpkbDTO customBpkbDTO) {
//        log.debug("Request to save UnitDeliverable : {}", customBpkbDTO);
//        if (customBpkbDTO.getBpkbNumber() != null){
//
//        }
//        return result;
//    }

    /**
     * Get all the unitDeliverables.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UnitDeliverableDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UnitDeliverables");
        return unitDeliverableRepository.findAll(pageable)
            .map(unitDeliverableMapper::toDto);
    }

    /**
     * Get one unitDeliverable by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public UnitDeliverableDTO findOne(UUID id) {
        log.debug("Request to get UnitDeliverable : {}", id);
        UnitDeliverable unitDeliverable = unitDeliverableRepository.findOne(id);
        UnitDeliverableDTO dto = unitDeliverableMapper.toDto(unitDeliverable);
        return assignSur(dto);
    }

    /**
     * Delete the  unitDeliverable by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete UnitDeliverable : {}", id);
        unitDeliverableRepository.delete(id);
        unitDeliverableSearchRepository.delete(id);
    }

    /**
     * Search for the unitDeliverable corresponding to the query.
     *
     * @param query    the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UnitDeliverableDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of UnitDeliverables for query {}", query);
        Page<UnitDeliverable> result = unitDeliverableSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(unitDeliverableMapper::toDto);
    }

    public UnitDeliverableDTO processExecuteData(Integer id, String param, UnitDeliverableDTO dto) {
        UnitDeliverableDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
        }
        return r;
    }

    @Transactional(readOnly = true)
    public Page<UnitDeliverableDTO> findAllByIdDeliverableLeasing(Pageable pageable, Integer idDeliverableType, String idInternal, String idLeasing) {
        log.debug("Request to get all UnitDeliverables by idDeliverableType and idInternal");
        if (idLeasing == null) {
            return unitDeliverableRepository.findAllByIdDeliverableTypeFilterCash(pageable, idDeliverableType, idInternal)
                .map(unitDeliverableMapper::toDto)
                .map(this::assignSur)
                .map(this::assignUD);
        } else {
            UUID leasingId = UUID.fromString(idLeasing);
            return unitDeliverableRepository.findAllByIdDeliverableTypeFilter(pageable, idDeliverableType, idInternal, leasingId)
                .map(unitDeliverableMapper::toDto)
                .map(this::assignSur)
                .map(this::assignUD);
        }
    }

    public Set<UnitDeliverableDTO> processExecuteListData(Integer id, String param, Set<UnitDeliverableDTO> dto) {
        Set<UnitDeliverableDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 101:
                    receiptBPKBSTNK(dto);
                    break;
                case 102:
                    updateBASTSTNK(param, dto);
                    break;
                case 103:
                    updateBASTSTNKSales(param, dto);
                    break;
                case 104:
                    updateBASTBPKBCustomer(param, dto);
                    break;
                case 105:
                    updateBASTBPKBLeasing(param, dto);
                    break;
                case 106:
                    updateBASTFakturOffTheRoad(param, dto);
                    break;
                case 107:
                    BASTSTNKSales(param, dto);
                    break;
                default:
                    break;
            }
        }
        return r;
    }

    @Transactional(readOnly = true)
    public Page<UnitDeliverableDTO> findAllByIdDeliverableType(Pageable pageable, Integer idDeliverableType, String idInternal) {
        log.debug("Request to get all UnitDeliverables by idDeliverableType and idInternal");
        if (idDeliverableType == null) {
            return unitDeliverableRepository.findAll(pageable)
                .map(unitDeliverableMapper::toDto)
                .map(this::assignSur)
                .map(this::assignUD);
        } else {
            return unitDeliverableRepository.findAllByIdDeliverableType(pageable, idDeliverableType, idInternal)
                .map(unitDeliverableMapper::toDto)
                .map(this::assignSur)
                .map(this::assignUD);
        }
    }

    @Transactional(readOnly = true)
    public Page<UnitDeliverableDTO> findAllByIdDeliverableTypeFilterBy(Pageable pageable, UnitDeliverablePTO param) {
        log.debug("Request to get all UnitDeliverables by idDeliverableType and idInternal");
        log.debug("PTO unit deliverable atas == " + param.getIdDeliverableType());
        log.debug("PTO unit deliverable atas == " + param.getInternalId());
        log.debug("PTO unit deliverable dataParam atas == " + param.getDataParam());
        String paramLike = '%' + param.getDataParam() + '%';
        if (param.getIdDeliverableType() == null) {
            return unitDeliverableRepository.findAll(pageable)
                .map(unitDeliverableMapper::toDto)
                .map(this::assignSur)
                .map(this::assignUD);
        }
        if (param.getDataParam() != null && param.getIdDeliverableType() != null) {
            log.debug("PTO unit deliverable dataParam == " + param.getDataParam());
            Page<UnitDeliverable> udto = unitDeliverableRepository.findSearchStnk(paramLike, param.getIdDeliverableType(), pageable);
            return udto
                .map(unitDeliverableMapper::toDto)
                .map(this::assignSur)
                .map(this::assignUD);
        } else {
            log.debug("PTO unit deliverable == " + param);
            return unitDeliverableRepository.findAllByIdDeliverableTypeFilterBy(pageable, param)
                .map(unitDeliverableMapper::toDto)
                .map(this::assignSur)
                .map(this::assignUD);
        }
    }

    @Transactional(readOnly = true)
    public Page<UnitDeliverableDTO> findAllByIdDeliverableTypeandStatusType(Pageable pageable, Integer idDeliverableType, String idInternal, Integer idStatusType) {
        log.debug("Request to get all UnitDeliverables by idDeliverableType and idInternal");
        return unitDeliverableRepository.findAllByIdDeliverableTypeandStatusType(pageable, idDeliverableType, idInternal, idStatusType)
            .map(unitDeliverableMapper::toDto)
            .map(this::assignSur);
    }

//    @Transactional(readOnly = true)
//    public Page<UnitDeliverableDTO> findAllReferences(Pageable pageable) {
//    	Page<UnitDeliverable> results;
//    	ArrayList<UnitDeliverable> unitDeliverables = new ArrayList<>();
//
//    	// return unitDeliverableRepository.findAllReferences(pageable).map(unitDeliverableMapper::toDto);
//    	Page<SummaryRefferalDTO> o = unitDeliverableRepository.findAllReferences(pageable);
//
//		// System.out.println("o: " + o.toString());
//
//    	for (SummaryRefferalDTO objects : o) {
//    		UnitDeliverable ud = new UnitDeliverable();
//    		VehicleDocumentRequirement vdr = new VehicleDocumentRequirement();
//
//    		ud.setRefDate((ZonedDateTime) objects.getRefDate());
//    		ud.setRefNumber((String) objects.getRefNumber());
//    		vdr.setVendor(vendorRepository.getVendorByidVendor(objects.getIdVendor()));
//    		ud.setVehicleDocumentRequirement(vdr);
//    		ud.setReceiptQty((Integer) objects.getReceiptQty());
//
//			System.out.println("objects[4]: " + objects.getCountIdFrame());
//
//			unitDeliverables.add(ud);
//		}
//
//    	results = new PageImpl<>(unitDeliverables);
//
//		return results.map(unitDeliverableMapper::toDto);
//    }

    @Transactional(readOnly = true)
    public Page<SummaryRefferalDTO> findAllReferences(String idInternal, Pageable pageable) {
        // return unitDeliverableRepository.findAllReferences(pageable).map(unitDeliverableMapper::toDto);
        Page<SummaryRefferalDTO> summaryRefferalDTOS = unitDeliverableRepository.findAllReferences(idInternal, pageable);

        // System.out.println("o: " + o.toString());

        return summaryRefferalDTOS;
    }

    public Page<SummaryRefferalDTO> findListReferences(Pageable pageable) {
        String idInternal = partyUtils.getCurrentInternal().getIdInternal();
        System.out.println("--- MASUK FIND LIST REFERENCES AS LOV----: " + idInternal);
        Page<SummaryRefferalDTO> summaryRefferalDTOS = unitDeliverableRepository.findListReferences(idInternal, pageable);
        System.out.println("--- Hasil ListRefefence----: " + summaryRefferalDTOS);
        // System.out.println("o: " + o.toString());

        return summaryRefferalDTOS;

    }
    public Page<SummaryRefferalDTO> findListReferences1(Pageable pageable) {
        String idInternal = partyUtils.getCurrentInternal().getIdInternal();
        System.out.println("--- MASUK FIND LIST REFERENCES AS LOV----: " + idInternal);
        Page<SummaryRefferalDTO> summaryRefferalDTOS = unitDeliverableRepository.findListReferences1(idInternal, pageable);
        System.out.println("--- Hasil ListRefefence----: " + summaryRefferalDTOS);
        // System.out.println("o: " + o.toString());

        return summaryRefferalDTOS;

    }

    public SummaryRefferalDTO findRefferenceByRefNumber(String idInternal, String refNumber) {
        System.out.println("--- MASUK FIND LIST REFERENCES AS LOV----: " + idInternal);
        SummaryRefferalDTO summaryRefferalDTOS = unitDeliverableRepository.findByRefNumber(idInternal, refNumber);
        return summaryRefferalDTOS;
    }

    public void receiptBPKBSTNK(UnitDeliverableDTO dto, Set<UnitDeliverableDTO> list) {
        String idFrame = dto.getVehicleDocumentRequirement().getVehicle().getIdFrame();
        String idMachine = dto.getVehicleDocumentRequirement().getVehicle().getIdMachine();
        String platNomor = dto.getVehicleDocumentRequirement().getRequestPoliceNumber();
        BigDecimal otherCost = BigDecimal.ZERO;
        BigDecimal costHandling = BigDecimal.ZERO;
        BigDecimal totalCostHandling;
        BigDecimal receipt = null;


        for (UnitDeliverableDTO unitDeliverableDTO : list) {
            if (unitDeliverableDTO.getRefNumber() != null) {
                String internal = partyUtils.getCurrentInternal().getIdInternal();
                String ref = unitDeliverableDTO.getRefNumber();

                if (unitDeliverableDTO.getReceiptNominal() != null || unitDeliverableDTO.getReceiptNominal().doubleValue() != 0) {
                    receipt = unitDeliverableDTO.getReceiptNominal();
                    List<UnitDeliverable> ud = unitDeliverableRepository.findRefNumberku(internal, ref);
                    log.debug("cari number edit " + ud);

                    if (!ud.isEmpty()) {
                        for (UnitDeliverable unitDeliverable : ud) {
                            unitDeliverable.setReceiptNominal(receipt);
                            unitDeliverableRepository.save(unitDeliverable);
                        }
                    }
                }

                if (unitDeliverableDTO.getReceiptNominal() == null || unitDeliverableDTO.getReceiptNominal().doubleValue() == 0) {
                    List<UnitDeliverable> ud = unitDeliverableRepository.findRefNumberku(internal, ref);
                    if (!ud.isEmpty()) {
                        for (UnitDeliverable unitDeliverable : ud) {
                            receipt = unitDeliverable.getReceiptNominal();
                        }
                    }
                }
            }
        }

        VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findOneByMachineAndFrame(idMachine, idFrame);


        if (vdr != null) {
            vdr.setRequestPoliceNumber(platNomor);
            if (dto.getVehicleDocumentRequirement().getOtherCost() != null) {
                otherCost = dto.getVehicleDocumentRequirement().getOtherCost();
                vdr.setOtherCost(otherCost);
            }
            if (dto.getVehicleDocumentRequirement().getCostHandling() != null) {
                System.out.println("Biaya Pengurusann: " + dto.getVehicleDocumentRequirement().getCostHandling());
                costHandling = dto.getVehicleDocumentRequirement().getCostHandling();
                vdr.setCostHandling(costHandling);
            }
            totalCostHandling = costHandling.add(otherCost);
            vdr.setTotalCostHandling(totalCostHandling);

            System.out.println("Plat Nomor: " + dto.getPlatNomor());
            if (dto.getPlatNomor() != null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(vdr.getIdRequirement(), BaseConstants.DELIVERABLE_TYPE_NOPOL);
                if (ud != null) {
                    ud.setDateReceipt(dto.getPlatNomor());
                    ud.setRefNumber(dto.getRefNumber());
                    ud.setRefDate(dto.getRefDate());
                    ud.setReceiptQty(dto.getReceiptQty());
                    ud.setReceiptNominal(receipt);
                    ud.setBpkbNumber(dto.getBpkbNumber());
                    unitDeliverableRepository.save(ud);
                } else {
                    ud = new UnitDeliverable();
                    ud.setVehicleDocumentRequirement(vdr);
                    ud.setIdDeliverableType(BaseConstants.DELIVERABLE_TYPE_NOPOL);
                    ud.setDateReceipt(dto.getPlatNomor());
                    ud.setRefNumber(dto.getRefNumber());
                    ud.setRefDate(dto.getRefDate());
                    ud.setReceiptQty(dto.getReceiptQty());
                    ud.setReceiptNominal(receipt);
                    ud.setBpkbNumber(dto.getBpkbNumber());
                    unitDeliverableRepository.save(ud);
                }
            }

            System.out.println("Notice: " + dto.getNotice());
            if (dto.getNotice() != null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(vdr.getIdRequirement(), BaseConstants.DELIVERABLE_TYPE_NOTICE);
                if (ud != null) {
                    ud.setDateReceipt(dto.getNotice());
                    ud.setRefNumber(dto.getRefNumber());
                    ud.setRefDate(dto.getRefDate());
                    ud.setReceiptQty(dto.getReceiptQty());
                    ud.setReceiptNominal(receipt);
                    ud.setBpkbNumber(dto.getBpkbNumber());
                    unitDeliverableRepository.save(ud);
                } else {
                    ud = new UnitDeliverable();
                    ud.setVehicleDocumentRequirement(vdr);
                    ud.setIdDeliverableType(BaseConstants.DELIVERABLE_TYPE_NOTICE);
                    ud.setDateReceipt(dto.getNotice());
                    ud.setRefNumber(dto.getRefNumber());
                    ud.setRefDate(dto.getRefDate());
                    ud.setReceiptQty(dto.getReceiptQty());
                    ud.setReceiptNominal(receipt);
                    ud.setBpkbNumber(dto.getBpkbNumber());
                    unitDeliverableRepository.save(ud);
                }
            }

            System.out.println("STNK: " + dto.getStnk());
            if (dto.getStnk() != null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(vdr.getIdRequirement(), BaseConstants.DELIVERABLE_TYPE_STNK);
                if (ud != null) {
                    ud.setDateReceipt(dto.getStnk());
                    ud.setRefNumber(dto.getRefNumber());
                    ud.setRefDate(dto.getRefDate());
                    ud.setReceiptQty(dto.getReceiptQty());
                    ud.setReceiptNominal(receipt);
                    ud.setBpkbNumber(dto.getBpkbNumber());
                    unitDeliverableRepository.save(ud);
                } else {
                    ud = new UnitDeliverable();
                    ud.setVehicleDocumentRequirement(vdr);
                    ud.setIdDeliverableType(BaseConstants.DELIVERABLE_TYPE_STNK);
                    ud.setDateReceipt(dto.getStnk());
                    ud.setRefNumber(dto.getRefNumber());
                    ud.setRefDate(dto.getRefDate());
                    ud.setReceiptQty(dto.getReceiptQty());
                    ud.setReceiptNominal(receipt);
                    ud.setBpkbNumber(dto.getBpkbNumber());
                    unitDeliverableRepository.save(ud);
                }
            }

            System.out.println("BPKB: " + dto.getBpkb());
            if (dto.getBpkb() != null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(vdr.getIdRequirement(), BaseConstants.DELIVERABLE_TYPE_BPKB);
                if (ud != null) {
                    ud.setDateReceipt(dto.getBpkb());
                    ud.setRefNumber(dto.getRefNumber());
                    ud.setRefDate(dto.getRefDate());
                    ud.setReceiptQty(dto.getReceiptQty());
                    ud.setReceiptNominal(receipt);
                    ud.setBpkbNumber(dto.getBpkbNumber());
                    unitDeliverableRepository.save(ud);
                } else {
                    ud = new UnitDeliverable();
                    ud.setVehicleDocumentRequirement(vdr);
                    ud.setIdDeliverableType(BaseConstants.DELIVERABLE_TYPE_BPKB);
                    ud.setDateReceipt(dto.getBpkb());
                    ud.setRefNumber(dto.getRefNumber());
                    ud.setRefDate(dto.getRefDate());
                    ud.setReceiptQty(dto.getReceiptQty());
                    ud.setReceiptNominal(receipt);
                    ud.setBpkbNumber(dto.getBpkbNumber());
                    unitDeliverableRepository.save(ud);
                }
            }
            // TODO : DI AKTIFIN PAS UDAH FIX SEMUA, KARENA ADA DTO YANG BELUM KEBENTUK UNIT DELIVERABLE

//            Boolean prosesNotice = dto.getNotice() != null;
//            Boolean prosesStnk = dto.getStnk() != null;
//            Boolean prosesNopol = dto.getPlatNomor() != null;
//            Boolean prosesBpkb = dto.getBpkb() != null;
//            String idVdr = vdr.getIdRequirement().toString();
//            Map<String, Object> vars = new HashMap<>();
//            if (prosesNotice) {
//                vars.put("dateNotice", dto.getNotice());
//                activitiProcessor.completeMessageEvent("registrasi", idVdr, "terimaNotice", vars);
//            }
//            if (prosesStnk) {
//                vars.put("dateNotice", dto.getNotice());
//                activitiProcessor.completeMessageEvent("registrasi", idVdr, "terimaSTNK");
//            }
//            if (prosesNopol) {
//                activitiProcessor.completeMessageEvent("registrasi", idVdr, "terimaNOPOL");
//            }
//            if (prosesBpkb) {
//                activitiProcessor.completeMessageEvent("registrasi", idVdr, "terimaBPKB");
//            }
        }
    }

    public void receiptBPKBSTNK(Set<UnitDeliverableDTO> dto) {

        String refNumber = null;
        if (dto.iterator().next().getRefNumber() == null) {
            refNumber = masterNumberingService.nextReferenceValue();
        }
        ZonedDateTime refDate = ZonedDateTime.now();

        for (UnitDeliverableDTO item : dto) {
            System.out.println("------------------------------");
            if (item.getRefNumber() == null) {
                item.setRefNumber(refNumber);
                item.setRefDate(refDate);
            }

            receiptBPKBSTNK(item, dto);

        }
    }

    public Set<UnitDeliverableDTO> updateBASTSTNK(String param, Set<UnitDeliverableDTO> dto) {
        String internal = param;
        String bastNumber = null;
        ZonedDateTime dateDelivery = ZonedDateTime.now();

        Set<UnitDeliverableDTO> r = dto;

        if (dto.iterator().next().getBastNumber() == null) {
            bastNumber = masterNumberingService.findTransBastSTNKNumberBy(param, "STD");
        }

        for(UnitDeliverableDTO unitDeliverableDTO : r) {
            if (unitDeliverableDTO.getBastNumber() == null) {
                if (unitDeliverableDTO.getStnk() != null) {
                    UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_STNK);
                    ud.setName(unitDeliverableDTO.getName());
                    ud.setCellPhone(unitDeliverableDTO.getCellPhone());
                    ud.setIdentityNumber(unitDeliverableDTO.getIdentityNumber());
                    ud.setBastNumber(bastNumber);
                    ud.setDateDelivery(dateDelivery);
                    unitDeliverableRepository.save(ud);
                }
            }
            if (unitDeliverableDTO.getBastNumber() == null) {
                if (unitDeliverableDTO.getNotice() != null) {
                    UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOTICE);
                    ud.setName(unitDeliverableDTO.getName());
                    ud.setCellPhone(unitDeliverableDTO.getCellPhone());
                    ud.setIdentityNumber(unitDeliverableDTO.getIdentityNumber());
                    ud.setBastNumber(bastNumber);
                    ud.setDateDelivery(dateDelivery);
                    unitDeliverableRepository.save(ud);
                }
            }


            if (unitDeliverableDTO.getBastNumber() == null) {
                if (unitDeliverableDTO.getPlatNomor() != null) {
                    UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOPOL);
                    ud.setName(unitDeliverableDTO.getName());
                    ud.setCellPhone(unitDeliverableDTO.getCellPhone());
                    ud.setIdentityNumber(unitDeliverableDTO.getIdentityNumber());
                    ud.setBastNumber(bastNumber);
                    ud.setDateDelivery(dateDelivery);
                    unitDeliverableRepository.save(ud);
                }
            }


            UnitDeliverable uds = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_STNK);
            UnitDeliverable udn = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOTICE);
            UnitDeliverable udp = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOPOL);
            if (uds.getDateDelivery() != null && udn.getDateDelivery() != null && udp.getDateDelivery() != null) {
                uds.setIscompleted(true);
            }

        }
        List<UnitDeliverableDTO> mainlist = new ArrayList<>(r);
        return r;

    }

    public Set<UnitDeliverableDTO> updateBASTSTNKSales(String param, Set<UnitDeliverableDTO> dto) {
        String internal = param;
        String bastNumber = null;
        ZonedDateTime dateDelivery = ZonedDateTime.now();
        Set<UnitDeliverableDTO> r = dto;

        if (dto.iterator().next().getBastNumber() == null) {
            bastNumber = masterNumberingService.findTransBastSTNKNumberBy(param, "STD");
        }

        for(UnitDeliverableDTO unitDeliverableDTO : r) {
            if (unitDeliverableDTO.getStnk() != null && unitDeliverableDTO.getBastNumber() == null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_STNK);

                ud.setName(unitDeliverableDTO.getName());
                ud.setBastNumber(bastNumber);
                ud.setDateBastSales(ZonedDateTime.now());

                unitDeliverableRepository.save(ud);
            }

            if (unitDeliverableDTO.getStnk() != null && unitDeliverableDTO.getBastNumber() != null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_STNK);
                bastNumber = unitDeliverableDTO.getBastNumber();

                ud.setName(unitDeliverableDTO.getName());
                ud.setBastNumber(bastNumber);
                ud.setDateBastSales(ZonedDateTime.now());

                unitDeliverableRepository.save(ud);
            }
//            else {
//                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_STNK);
//                ud.setDateBastSalesBack(ZonedDateTime.now());
//                unitDeliverableRepository.save(ud);
//
//            }

            if (unitDeliverableDTO.getNotice() != null && unitDeliverableDTO.getBastNumber() == null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOTICE);
                ud.setName(unitDeliverableDTO.getName());
                ud.setBastNumber(bastNumber);
                ud.setDateBastSales(ZonedDateTime.now());

                unitDeliverableRepository.save(ud);
            }

            if (unitDeliverableDTO.getNotice() != null && unitDeliverableDTO.getBastNumber() != null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOTICE);
                bastNumber = unitDeliverableDTO.getBastNumber();

                ud.setName(unitDeliverableDTO.getName());
                ud.setBastNumber(bastNumber);
                ud.setDateBastSales(ZonedDateTime.now());

                unitDeliverableRepository.save(ud);
            }
//            else {
//                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOTICE);
//                ud.setDateBastSalesBack(ZonedDateTime.now());
//                unitDeliverableRepository.save(ud);
//            }
                log.debug("cek platnomor receipt" + unitDeliverableDTO.getPlatNomor());
                log.debug("cek platnomor bast" + unitDeliverableDTO.getBastNumber());
            if (unitDeliverableDTO.getPlatNomor() != null && unitDeliverableDTO.getBastNumber() == null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOPOL);

                ud.setName(unitDeliverableDTO.getName());
                ud.setBastNumber(bastNumber);
                ud.setDateBastSales(ZonedDateTime.now());
                unitDeliverableRepository.save(ud);
            }

            if (unitDeliverableDTO.getPlatNomor() != null && unitDeliverableDTO.getBastNumber() != null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOPOL);

                bastNumber = unitDeliverableDTO.getBastNumber();

                ud.setName(unitDeliverableDTO.getName());
                ud.setBastNumber(bastNumber);
                ud.setDateBastSales(ZonedDateTime.now());
                unitDeliverableRepository.save(ud);
            }
//            else {
//                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOPOL);
//                ud.setDateBastSalesBack(ZonedDateTime.now());
//                unitDeliverableRepository.save(ud);
//            }

            UnitDeliverable uds = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_STNK);
            UnitDeliverable udn = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOTICE);
            UnitDeliverable udp = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOPOL);
            if (uds.getDateBastSalesBack() != null && udn.getDateBastSalesBack() != null && udp.getDateBastSalesBack() != null) {
                uds.setIscompleted(true);
            }

        }
        List<UnitDeliverableDTO> mainlist = new ArrayList<>(r);
        return r;

    }

    public Set<UnitDeliverableDTO> BASTSTNKSales(String param, Set<UnitDeliverableDTO> dto) {
        String internal = param;
        String bastNumber = null;
        ZonedDateTime dateDelivery = ZonedDateTime.now();
        Set<UnitDeliverableDTO> r = dto;

        if (dto.iterator().next().getBastNumber() == null) {
            bastNumber = masterNumberingService.findTransBastSTNKNumberBy(param, "STD");
        }

        for(UnitDeliverableDTO unitDeliverableDTO : r) {
            if (unitDeliverableDTO.getStnk() != null && unitDeliverableDTO.getBastNumber() != null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_STNK);
                ud.setDateBastSalesBack(ZonedDateTime.now());
                unitDeliverableRepository.save(ud);

            }

            if (unitDeliverableDTO.getNotice() != null && unitDeliverableDTO.getBastNumber() != null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOTICE);
                ud.setDateBastSalesBack(ZonedDateTime.now());
                unitDeliverableRepository.save(ud);
            }

            if (unitDeliverableDTO.getPlatNomor() != null && unitDeliverableDTO.getBastNumber() != null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOPOL);
                ud.setDateBastSalesBack(ZonedDateTime.now());
                unitDeliverableRepository.save(ud);
            }

            UnitDeliverable uds = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_STNK);
            UnitDeliverable udn = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOTICE);
            UnitDeliverable udp = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOPOL);
            if (uds.getDateBastSalesBack() != null && udn.getDateBastSalesBack() != null && udp.getDateBastSalesBack() != null) {
                uds.setIscompleted(true);
            }

        }
        List<UnitDeliverableDTO> mainlist = new ArrayList<>(r);
        return r;

    }

    public Set<UnitDeliverableDTO> updateBASTBPKBLeasing(String param, Set<UnitDeliverableDTO> dto) {
        String internal = param;
        String bastNumber = null;
        ZonedDateTime dateDelivery = ZonedDateTime.now();

        Set<UnitDeliverableDTO> r = dto;
        if (dto.iterator().next().getBastNumber() == null) {
            bastNumber = masterNumberingService.findTransBastBPKBLeasingNumberBy(param, "BDL");
        }

        for(UnitDeliverableDTO unitDeliverableDTO : r) {
            if (unitDeliverableDTO.getBpkb() != null && unitDeliverableDTO.getBastNumber() == null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_BPKB);

                ud.setName(unitDeliverableDTO.getName());
                ud.setIdentityNumber(unitDeliverableDTO.getIdentityNumber());
                ud.setBastNumber(bastNumber);
                ud.setDateDelivery(dateDelivery);
                ud.setIscompleted(true);

                unitDeliverableRepository.save(ud);
            }
        }
        List<UnitDeliverableDTO> mainlist = new ArrayList<>(r);
        return r;

    }

    public Set<UnitDeliverableDTO> updateBASTBPKBCustomer(String param, Set<UnitDeliverableDTO> dto) {
        String internal = param;
        String bastNumber = null;
        ZonedDateTime dateDelivery = ZonedDateTime.now();

        Set<UnitDeliverableDTO> r = dto;
        if (dto.iterator().next().getBastNumber() == null) {
            bastNumber =  masterNumberingService.findTransBastBPKBCustomerNumberBy(param, "BDC");
        }

        for(UnitDeliverableDTO unitDeliverableDTO : r) {
            if (unitDeliverableDTO.getBpkb() != null && unitDeliverableDTO.getBastNumber() == null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_BPKB);

                ud.setName(unitDeliverableDTO.getName());
                ud.setIdentityNumber(unitDeliverableDTO.getIdentityNumber());
                ud.setBastNumber(bastNumber);
                ud.setDateDelivery(dateDelivery);
                ud.setIscompleted(true);

                unitDeliverableRepository.save(ud);
            }
        }
        List<UnitDeliverableDTO> mainlist = new ArrayList<>(r);
        return r;

    }

    public Set<UnitDeliverableDTO> updateBASTFakturOffTheRoad(String param, Set<UnitDeliverableDTO> dto) {
        String internal = param;
        String bastNumber = null;
        ZonedDateTime dateDelivery = ZonedDateTime.now();

        Set<UnitDeliverableDTO> r = dto;
        if (dto.iterator().next().getBastNumber() == null) {
            bastNumber =  masterNumberingService.findTransBastOffTheRoad(internal, "OTR");
        }


        for(UnitDeliverableDTO unitDeliverableDTO : r) {
            if (unitDeliverableDTO.getBastNumber() == null) {
                UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(unitDeliverableDTO.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_FAKTUR_OFFTHEROAD);
                    log.debug("cek ada datakah" + ud);
                    ud.setName(unitDeliverableDTO.getName());
                    ud.setIdentityNumber(unitDeliverableDTO.getIdentityNumber());
                    ud.setBastNumber(bastNumber);
                    ud.setDateDelivery(dateDelivery);
                    unitDeliverableRepository.save(ud);
            }
        }
        List<UnitDeliverableDTO> mainlist = new ArrayList<>(r);
        return r;

    }

//    @PostConstruct
    @Transactional
    public void initialize() {
        try {
            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/bpkb.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/bpkb.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<CustomBpkbDTO> bpkb = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (CustomBpkbDTO o: bpkb) {

                    log.debug("Tampilan DTO untuk Bpkb", bpkb);
                    // if (o.getParentGeoCode() != null && o.getGeocode() != null) {
                    //     addVillage(o.getGeocode(), o.getDescription());
                    // }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public UnitDeliverableDTO assignSur(UnitDeliverableDTO dto) {
        VehicleDocumentRequirementDTO vdr = vdrService.findOne(dto.getVehicleDocumentRequirementId());
        if (vdr != null) {
            // get vendor name
            if (vdr.getIdVendor() != null) {
                VendorDTO vendor = vendorService.findOne(vdr.getIdVendor());
                 if (vendor.getIdVendor() != null) dto.setVendorName(vendor.getOrganization().getName());
            }
            OrderItemDTO oi = orderItemService.findOne(vdr.getIdOrderItem());
            if (oi != null) {
                // VehicleSalesOrderDTO vso = vsoService.findOne(oi.getOrdersId());
                VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(oi.getOrdersId());
                if (vso != null) {
                    // get billing numberrr
                     if (vso.getBillingNumber() != null) dto.setIvuNumber(vso.getBillingNumber());
                    SalesUnitRequirementDTO sur = surService.findOne(vso.getIdSalesUnitRequirement());
                    if (sur != null) {
                        dto.setCustomerName(sur.getCustomerName());
                        dto.setSalesName(sur.getSalesmanName());
                        if (sur.getLeasingCompanyId() != null) {
                            LeasingCompanyDTO leasing = leasingCompanyService.findOne(sur.getLeasingCompanyId());
                            dto.setFincoyName(leasing.getOrganization().getName());
                        }

                    }
                }
            }
        }
        return dto;
    }

    public UnitDeliverableDTO assignUD (UnitDeliverableDTO dto) {
        if (dto.getPlatNomor() == null) {
            UnitDeliverable udNopol = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(dto.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOPOL);
            if (udNopol != null) {
                dto.setPlatNomor(udNopol.getDateReceipt());
                dto.setDelivPlatNomor(udNopol.getDateDelivery());
            }
        }
        if (dto.getNotice() == null) {
            UnitDeliverable udNotice = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(dto.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_NOTICE);
            if (udNotice != null) {
                dto.setNotice(udNotice.getDateReceipt());
                dto.setDelivNotice(udNotice.getDateDelivery());
            }
        }
        if (dto.getStnk() == null) {
            UnitDeliverable udStnk = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(dto.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_STNK);
            if (udStnk != null) {
                dto.setStnk(udStnk.getDateReceipt());
                dto.setDelivStnk(udStnk.getDateDelivery());
            }
        }
        if (dto.getBpkb() == null) {
            UnitDeliverable udBpkb = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(dto.getVehicleDocumentRequirementId(), BaseConstants.DELIVERABLE_TYPE_BPKB);
            if (udBpkb != null) {
                dto.setBpkb(udBpkb.getDateReceipt());
            }
        }

        return dto;
    }

    @Transactional
    public UnitDeliverableDTO processExecuteFindData(HttpServletRequest request, Map<String, Object> item) {
        String execute = request.getParameter("execute").toString();
        UnitDeliverableDTO r = new UnitDeliverableDTO();
        String internal = partyUtils.getCurrentInternal().getIdInternal();

        if (execute.equals("CheckUnitOnUnitDeliverable")){
            ObjectMapper mapper = new ObjectMapper();
            mapper.findAndRegisterModules();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

            r = mapper.convertValue(item.get("item"), new TypeReference<UnitDeliverableDTO>() {});
            List<UnitDeliverable> p = unitDeliverableRepository.findOneByIdFrameAndIdMachineAndPoliceNumber( r.getVehicleDocumentRequirement().getVehicle().getIdFrame(), r.getVehicleDocumentRequirement().getVehicle().getIdMachine(), r.getVehicleDocumentRequirement().getRequestPoliceNumber(), internal
            );

            if (!p.isEmpty()) {
                UnitDeliverable ud = p.get(0);
                if (ud != null){
                    r = unitDeliverableMapper.toDto(ud);
                    for (UnitDeliverable unitDeliverable : p) {
                        if (unitDeliverable.getIdDeliverableType() == BaseConstants.DELIVERABLE_TYPE_NOPOL && unitDeliverable.getDateReceipt() != null) {
                            // TODO
                            r.setPlatNomor(unitDeliverable.getDateReceipt());
                        }
                        if (unitDeliverable.getIdDeliverableType() == BaseConstants.DELIVERABLE_TYPE_STNK && unitDeliverable.getDateReceipt() != null) {
                            // TODO
                            r.setStnk(unitDeliverable.getDateReceipt());
                        }
                        if (unitDeliverable.getIdDeliverableType() == BaseConstants.DELIVERABLE_TYPE_NOTICE && unitDeliverable.getDateReceipt() != null) {
                            // TODO
                            r.setNotice(unitDeliverable.getDateReceipt());
                        }
                        if (unitDeliverable.getIdDeliverableType() == BaseConstants.DELIVERABLE_TYPE_BPKB && unitDeliverable.getDateReceipt() != null) {
                            // TODO
                            r.setBpkb(unitDeliverable.getDateReceipt());
                        }
                    }
                }
                else {
                    throw new DmsException("Unit Tidak Ditemukan !");
                }

            } else {
                throw new DmsException("Belum ada dokumen yang diterima !");
            }

        }

        return r;
    }

    public UnitDeliverableDTO findByIdReqandIdDeliverableTye(UUID idReq, Integer idDeltype) {
        log.debug("Masuk Proses Pencarian Unit Deliverable berdasarkan idreq dan id deltype");
        UnitDeliverable ud = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(idReq, idDeltype);
        return unitDeliverableMapper.toDto(ud);
    }

    public UnitDeliverableDTO setPrint (HttpServletRequest request, UnitDeliverableDTO dto) {
        String refNumber = request.getParameter("refNumber");
        log.debug("terima refnumber " + refNumber);
        Internal internal = partyUtils.getCurrentInternal();
        log.debug("cari internalprint " + internal);

        List<UnitDeliverable> findRefNumber  = unitDeliverableRepository.findRefNumber(internal.getIdInternal(), refNumber);

        log.debug("cari number " + findRefNumber);

        if (!findRefNumber.isEmpty()) {
            for (UnitDeliverable unitDeliverable : findRefNumber ) {
                unitDeliverable.setDiPrint(true);

                unitDeliverableRepository.save(unitDeliverable);
            }
        }

        return dto;
    }


}
