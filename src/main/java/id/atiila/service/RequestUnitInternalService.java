package id.atiila.service;

import id.atiila.domain.Person;
import id.atiila.domain.RequestUnitInternal;
import id.atiila.repository.PersonRepository;
import id.atiila.repository.RequestUnitInternalRepository;
import id.atiila.repository.search.RequestUnitInternalSearchRepository;
import id.atiila.service.dto.CustomPreRequestUnitInternalDTO;
import id.atiila.service.dto.RequestUnitInternalDTO;
import id.atiila.service.mapper.RequestUnitInternalMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing RequestUnitInternal.
 * atiila consulting
 */

@Service
@Transactional
public class RequestUnitInternalService {

    private final Logger log = LoggerFactory.getLogger(RequestUnitInternalService.class);

    private final RequestUnitInternalRepository requestUnitInternalRepository;

    private final RequestUnitInternalMapper requestUnitInternalMapper;

    private final RequestUnitInternalSearchRepository requestUnitInternalSearchRepository;

    @Autowired
    private PersonRepository personRepository;

    public RequestUnitInternalService(RequestUnitInternalRepository requestUnitInternalRepository, RequestUnitInternalMapper requestUnitInternalMapper, RequestUnitInternalSearchRepository requestUnitInternalSearchRepository) {
        this.requestUnitInternalRepository = requestUnitInternalRepository;
        this.requestUnitInternalMapper = requestUnitInternalMapper;
        this.requestUnitInternalSearchRepository = requestUnitInternalSearchRepository;
    }

    /**
     * Save a requestUnitInternal.
     *
     * @param requestUnitInternalDTO the entity to save
     * @return the persisted entity
     */
    public RequestUnitInternalDTO save(RequestUnitInternalDTO requestUnitInternalDTO) {
        log.debug("Request to save RequestUnitInternal : {}", requestUnitInternalDTO);
        RequestUnitInternal requestUnitInternal = requestUnitInternalMapper.toEntity(requestUnitInternalDTO);
        requestUnitInternal = requestUnitInternalRepository.save(requestUnitInternal);
        RequestUnitInternalDTO result = requestUnitInternalMapper.toDto(requestUnitInternal);
        requestUnitInternalSearchRepository.save(requestUnitInternal);
        return result;
    }

    public Page<CustomPreRequestUnitInternalDTO> findAllPreRequestUnitInternal(String idinternal, Pageable pageable){
        return requestUnitInternalRepository.findPreRequestUnitInternal(idinternal, pageable)
            .map(new Converter<CustomPreRequestUnitInternalDTO, CustomPreRequestUnitInternalDTO>() {
                @Override
                public CustomPreRequestUnitInternalDTO convert(CustomPreRequestUnitInternalDTO customPreRequestUnitInternalDTO) {
                    log.debug("START MAPPING SALESNAME AND KORSAL");
                    //get salesname
                    Person person = personRepository.findOne(customPreRequestUnitInternalDTO.getSalesId());
                    customPreRequestUnitInternalDTO.setSalesName(person.getFirstName() + " " + person.getLastName());

                    //get korsal
                    Person personKorsal = personRepository.findOne(customPreRequestUnitInternalDTO.getKorsalId());
                    customPreRequestUnitInternalDTO.setKorsalName(personKorsal.getFirstName() + " " + personKorsal.getLastName());

                    return customPreRequestUnitInternalDTO;
                }
            });
    }

    /**
     * Get all the requestUnitInternals.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequestUnitInternalDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RequestUnitInternals");
        return requestUnitInternalRepository.findActiveRequestUnitInternal(pageable)
            .map(requestUnitInternalMapper::toDto);
    }

    /**
     * Get one requestUnitInternal by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public RequestUnitInternalDTO findOne(UUID id) {
        log.debug("Request to get RequestUnitInternal : {}", id);
        RequestUnitInternal requestUnitInternal = requestUnitInternalRepository.findOne(id);
        return requestUnitInternalMapper.toDto(requestUnitInternal);
    }

    /**
     * Delete the requestUnitInternal by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete RequestUnitInternal : {}", id);
        requestUnitInternalRepository.delete(id);
        requestUnitInternalSearchRepository.delete(id);
    }

    /**
     * Search for the requestUnitInternal corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequestUnitInternalDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of RequestUnitInternals for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idRequest = request.getParameter("idRequest");
        String idInternalFrom = request.getParameter("idInternalFrom");
        String idInternalTo = request.getParameter("idInternalTo");

        if (filterName != null) {
        }
        Page<RequestUnitInternal> result = requestUnitInternalSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(requestUnitInternalMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<RequestUnitInternalDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered RequestUnitInternalDTO");
        String idRequest = request.getParameter("idRequest");
        String idInternalFrom = request.getParameter("idInternalFrom");
        String idInternalTo = request.getParameter("idInternalTo");

        return requestUnitInternalRepository.findByParams(idRequest, idInternalFrom, idInternalTo, pageable)
            .map(requestUnitInternalMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public RequestUnitInternalDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        RequestUnitInternalDTO r = null;
        return r;
    }

    @Transactional
    public Set<RequestUnitInternalDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<RequestUnitInternalDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public RequestUnitInternalDTO changeRequestUnitInternalStatus(RequestUnitInternalDTO dto, Integer id) {
        if (dto != null) {
			RequestUnitInternal e = requestUnitInternalRepository.findOne(dto.getIdRequest());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        requestUnitInternalSearchRepository.delete(dto.getIdRequest());
                        break;
                    default:
                        requestUnitInternalSearchRepository.save(e);
                }
				requestUnitInternalRepository.save(e);
			}
		}
        return dto;
    }
}
