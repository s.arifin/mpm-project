package id.atiila.service;

import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.service.util.RandomUtil;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StockOpnameUtils {

    @Autowired
    private StockOpnameRepository stockOpnameRepository;

    @Autowired
    private StockOpnameItemRepository stockOpnameItemRepository;

    @Autowired
    private StockOpnameInventoryRepository stockOpnameInventoryRepository;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private ProductUtils productUtils;

    @Autowired
    private MasterNumberingService numbering;

    @Autowired
    private ContainerRepository containerRepository;

    @Autowired
    private FacilityRepository facilityRepository;

    @Autowired
    private PriceUtils priceUtils;

    @Transactional
    public StockOpname buildStockOpname(String idInternal) {
        StockOpname r = null;
        List<InventoryItem> items = stockOpnameRepository.findAllInventory(idInternal);

        if (items.size() > 0) {
            Map<String, StockOpnameItem> maps = new HashMap();
            List<StockOpnameInventory> linv = new ArrayList<>();

            Internal intr = internalUtils.findOne(idInternal);

            r = new StockOpname();
            r.setInternal(intr);
            r.setStockOpnameNumber("STK-" + numbering.nextValue("STK", 1000000l).toString());
            r = stockOpnameRepository.save(r);

            for (InventoryItem inv: items) {
                String id = inv.getIdProduct() + '!' + inv.getIdContainer() + "!" + inv.getIdFacility();
                StockOpnameItem sitem = maps.get(id);

                if (sitem == null) {
                    Container container = null;
                    Facility facility = null;
                    BigDecimal het = BigDecimal.ZERO;

                    String tag = numbering.nextValue("XXX", 1000000l).toString();
                    sitem = new StockOpnameItem();
                    sitem.setTagNumber(tag);

                    Product p = productUtils.getProduct(inv.getIdProduct());
                    if (inv.getIdContainer() != null) container = containerRepository.findOne(inv.getIdContainer());
                    if (inv.getIdFacility() != null) facility = facilityRepository.findOne(inv.getIdFacility());

                    het = priceUtils.getSalesPrice(intr, p.getIdProduct());

                    sitem.setProduct(p);
                    sitem.setItemDescription(p.getName());
                    sitem.setQty(inv.getQty().intValue());
                    sitem.setHasChecked(false);
                    if (container != null) sitem.setContainer(container);
                    sitem.setHet(het);
                    sitem.setStockOpname(r);

                    maps.put(id, sitem);
                } else {
                     sitem.setQty(inv.getQty().intValue() + sitem.getQty());
                }

                StockOpnameInventory stkinv = new StockOpnameInventory();
                stkinv.setInventoryItem(inv);
                stkinv.setItem(sitem);
                stkinv.setQty(inv.getQty());
                linv.add(stkinv);
            }

            if (maps.size() > 0) {
                 stockOpnameItemRepository.save(maps.values());
                 stockOpnameInventoryRepository.save(linv);
            }
        }
        return r;
    }

}
