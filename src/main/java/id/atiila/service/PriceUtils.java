package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.*;
import id.atiila.repository.PriceComponentRepository;
import id.atiila.repository.PriceTypeRepository;
import id.atiila.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class PriceUtils {

    @Autowired
    private PriceComponentRepository repo;

    @Autowired
    private PriceTypeRepository priceTypeRepository;

    @Autowired
    private ProductRepository productRepository;

    public PriceComponent findByType(List<PriceComponent> l, Integer type) {
        PriceComponent r = null;
        for (PriceComponent p: l) {
            // Jika type price sama dengan yang di butuhkan
            if (type == p.getPriceType().getIdPriceType()) {
                return p;
            }
        }
        return r;
    }

    protected PriceComponent addBasePrice(Party seller, Product product, int type, BigDecimal price, Float percent, ZonedDateTime dateWhen) {
        List<PriceComponent> l = repo.getPriceBase(product, seller, dateWhen);
        PriceComponent current = findByType(l, type);
        boolean append = true;

        if (current != null) {
            if ((percent.compareTo(0f) != 0 && percent.compareTo(current.getPercent()) != 0 ||
                 !price.equals(BigDecimal.ZERO) && !price.equals(current.getPrice())))  {
                return current;
            }
            current.setDateThru(dateWhen);
            repo.save(current);
        }

        if (append) {
            current = new PriceComponent();
            current.setSeller(seller);
            current.setProduct(product);

            PriceType priceType = priceTypeRepository.findOne(type);

            current.setPriceType(priceType);
            current.setPercent(percent);
            current.setPrice(price);
            current.setDateFrom(dateWhen);
            current.setDateThru(ZonedDateTime.of(9999, 12, 31, 23, 59, 59, 999, ZoneId.systemDefault()));
            repo.save(current);
        }

        return current;
    }

    public PriceComponent addPrice(Party seller, Product product, int type, BigDecimal price, ZonedDateTime dateWhen) {
        return addBasePrice(seller, product, type, price, 0f, dateWhen);
    }

    public PriceComponent addPercent(Party seller, Product product, int type, Float percent, ZonedDateTime dateWhen) {
        return addBasePrice(seller, product, type, new BigDecimal(0), percent, dateWhen);
    }

    public BigDecimal getManufacturePrice(Internal intr, String idProduct) {
        return getPriceComponentValue(BaseConstants.PRICE_TYPE_HET, intr.getOrganization(), idProduct, ZonedDateTime.now());
    }

    public BigDecimal getSalesPrice(Internal intr, String idProduct) {
        return getPriceComponentValue(BaseConstants.PRICE_TYPE_REGULAR, intr.getOrganization(), idProduct, ZonedDateTime.now());
    }

    public BigDecimal getManufacturePrice(Vendor vendor, String idProduct) {
        return getPriceComponentValue(BaseConstants.PRICE_TYPE_HET, vendor.getOrganization(), idProduct, ZonedDateTime.now());
    }

    public BigDecimal getSalesPrice(Vendor vendor, String idProduct) {
        return getPriceComponentValue(BaseConstants.PRICE_TYPE_REGULAR, vendor.getOrganization(), idProduct, ZonedDateTime.now());
    }

    public BigDecimal getBBN(Internal intr, String idProduct) {
        return getPriceComponentValue(BaseConstants.PRICE_TYPE_BBN, intr.getOrganization(), idProduct, ZonedDateTime.now());
    }


    protected BigDecimal getPriceComponentValue(Integer priceType, Party party, String idProduct, ZonedDateTime dateWhen) {
        List<PriceComponent> lpr = repo.getPriceComponent(party, idProduct, dateWhen);
        for (PriceComponent p: lpr) {
            int pt = p.getPriceType().getIdPriceType();
            if (pt == priceType) {
                switch (pt) {
                    case BaseConstants.PRICE_TYPE_HET:
                        return p.getManfucterPrice();
                    case BaseConstants.PRICE_TYPE_REGULAR:
                        return p.getPrice();
                    case BaseConstants.PRICE_TYPE_DISCOUNT:
                        return BigDecimal.valueOf(p.getPercent());
                    case BaseConstants.PRICE_TYPE_BBN:
                        return p.getPrice();
                    case BaseConstants.PRICE_TYPE_SUBS_AHM:
                        return p.getPrice();
                    case BaseConstants.PRICE_TYPE_SUBS_MD:
                        return p.getPrice();
                    case BaseConstants.PRICE_TYPE_DISC_DEALER:
                        return p.getPrice();
                    case BaseConstants.PRICE_TYPE_SUBS_FINCOY:
                        return p.getPrice();
                    default:
                        return p.getPrice();
                }
            }
        }
        return BigDecimal.ZERO;
    }

    protected PriceComponent setPriceComponentValue(Integer priceType, Party seller, String idProduct, BigDecimal value) {
        List<PriceComponent> lpr = repo.getPriceComponent(seller, idProduct, ZonedDateTime.now());
        boolean found = false;

        for (PriceComponent p: lpr) {
            int pt = p.getPriceType().getIdPriceType();
            if (pt == priceType) {
                switch (pt) {
                    case BaseConstants.PRICE_TYPE_HET:
                        if (p.getManfucterPrice().doubleValue() == value.doubleValue()) return p;
                        break;
                    case BaseConstants.PRICE_TYPE_REGULAR:
                        if (p.getPrice().doubleValue() == value.doubleValue()) return p;
                        break;
                    case BaseConstants.PRICE_TYPE_DISCOUNT:
                        if (p.getPercent().floatValue() == value.floatValue()) return p;
                        break;
                    case BaseConstants.PRICE_TYPE_BBN:
                        if (p.getPrice().doubleValue() == value.doubleValue()) return p;
                        break;
                    case BaseConstants.PRICE_TYPE_SUBS_AHM:
                        if (p.getPrice().doubleValue() == value.doubleValue()) return p;
                        break;
                    case BaseConstants.PRICE_TYPE_SUBS_MD:
                        if (p.getPrice().doubleValue() == value.doubleValue()) return p;
                        break;
                    case BaseConstants.PRICE_TYPE_DISC_DEALER:
                        if (p.getPrice().doubleValue() == value.doubleValue()) return p;
                        break;
                    case BaseConstants.PRICE_TYPE_SUBS_FINCOY:
                        if (p.getPrice().doubleValue() == value.doubleValue()) return p;
                        break;
                    default:
                        break;
                }

                found = true;
                p.setDateThru(ZonedDateTime.now());
                p = repo.save(p);
            }
            if (found) break;
        }

        PriceComponent p = new PriceComponent();
        p.setProduct(productRepository.findOne(idProduct));
        p.setSeller(seller);
        p.setPriceType(priceTypeRepository.findOne(priceType));
        p.setDateFrom(ZonedDateTime.now());
        switch (priceType) {
            case BaseConstants.PRICE_TYPE_HET:
                p.setManfucterPrice(value);
                p.setPrice(0);
                p.setPercent(0);
                break;
            case BaseConstants.PRICE_TYPE_REGULAR:
                p.setManfucterPrice(0);
                p.setPrice(value);
                p.setPercent(0);
                break;
            case BaseConstants.PRICE_TYPE_DISCOUNT:
                p.setManfucterPrice(0);
                p.setPrice(0);
                p.setPercent(value.floatValue());
                break;
            case BaseConstants.PRICE_TYPE_BBN:
                p.setManfucterPrice(0);
                p.setPrice(value);
                p.setPercent(0);
                break;
            case BaseConstants.PRICE_TYPE_SUBS_AHM:
                p.setManfucterPrice(0);
                p.setPrice(value);
                p.setPercent(0);
                break;
            case BaseConstants.PRICE_TYPE_SUBS_MD:
                p.setManfucterPrice(0);
                p.setPrice(value);
                p.setPercent(0);
                break;
            case BaseConstants.PRICE_TYPE_DISC_DEALER:
                p.setManfucterPrice(0);
                p.setPrice(value);
                p.setPercent(0);
                break;
            case BaseConstants.PRICE_TYPE_SUBS_FINCOY:
                p.setManfucterPrice(0);
                p.setPrice(value);
                p.setPercent(0);
                break;
            default:
                p.setManfucterPrice(0);
                p.setPrice(value);
                p.setPercent(0);
                break;
        }
        p = repo.save(p);

        return p;
    }

    public PriceComponent setInternalSalesPrice(Internal internal, String idProduct, BigDecimal value) {
        return setPriceComponentValue(BaseConstants.PRICE_TYPE_REGULAR, internal.getOrganization(), idProduct, value);
    }

    public PriceComponent setInternalManufacturePrice(Internal internal, String idProduct, BigDecimal value) {
        return setPriceComponentValue(BaseConstants.PRICE_TYPE_HET, internal.getOrganization(), idProduct, value);
    }

    public PriceComponent setVendorSalesPrice(Vendor vendor, String idProduct, BigDecimal value) {
        return setPriceComponentValue(BaseConstants.PRICE_TYPE_REGULAR, vendor.getOrganization(), idProduct, value);
    }

    public PriceComponent setVendorManufacturePrice(Vendor vendor, String idProduct, BigDecimal value) {
        return setPriceComponentValue(BaseConstants.PRICE_TYPE_HET, vendor.getOrganization(), idProduct, value);
    }

    public PriceComponent setVendorSubsAHM(Vendor vendor, String idProduct, BigDecimal value) {
        return setPriceComponentValue(BaseConstants.PRICE_TYPE_SUBS_AHM, vendor.getOrganization(), idProduct, value);
    }

    public PriceComponent setVendorSubsMD(Vendor vendor, String idProduct, BigDecimal value) {
        return setPriceComponentValue(BaseConstants.PRICE_TYPE_SUBS_MD, vendor.getOrganization(), idProduct, value);
    }

    public PriceComponent setVendorSubsFinCoy(Vendor vendor, String idProduct, BigDecimal value) {
        return setPriceComponentValue(BaseConstants.PRICE_TYPE_SUBS_FINCOY, vendor.getOrganization(), idProduct, value);
    }

    public PriceComponent setBBN(Internal internal, String idProduct, BigDecimal value) {
        return setPriceComponentValue(BaseConstants.PRICE_TYPE_BBN, internal.getOrganization(), idProduct, value);
    }

    public PriceComponent setBBN(Internal internal, String idProduct, Integer value) {
        return setPriceComponentValue(BaseConstants.PRICE_TYPE_BBN, internal.getOrganization(), idProduct, BigDecimal.valueOf(value));
    }

}
