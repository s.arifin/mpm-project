package id.atiila.service;

import id.atiila.domain.PaymentMethod;
import id.atiila.repository.PaymentMethodRepository;
import id.atiila.repository.search.PaymentMethodSearchRepository;
import id.atiila.service.dto.PaymentMethodDTO;
import id.atiila.service.mapper.PaymentMethodMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing PaymentMethod.
 * BeSmart Team
 */

@Service
@Transactional
public class PaymentMethodService {

    private final Logger log = LoggerFactory.getLogger(PaymentMethodService.class);

    private final PaymentMethodRepository paymentMethodRepository;

    private final PaymentMethodMapper paymentMethodMapper;

    private final PaymentMethodSearchRepository paymentMethodSearchRepository;

    public PaymentMethodService(PaymentMethodRepository paymentMethodRepository, PaymentMethodMapper paymentMethodMapper, PaymentMethodSearchRepository paymentMethodSearchRepository) {
        this.paymentMethodRepository = paymentMethodRepository;
        this.paymentMethodMapper = paymentMethodMapper;
        this.paymentMethodSearchRepository = paymentMethodSearchRepository;
    }

    /**
     * Save a paymentMethod.
     *
     * @param paymentMethodDTO the entity to save
     * @return the persisted entity
     */
    public PaymentMethodDTO save(PaymentMethodDTO paymentMethodDTO) {
        log.debug("Request to save PaymentMethod : {}", paymentMethodDTO);
        PaymentMethod paymentMethod = paymentMethodMapper.toEntity(paymentMethodDTO);
        paymentMethod = paymentMethodRepository.save(paymentMethod);
        PaymentMethodDTO result = paymentMethodMapper.toDto(paymentMethod);
        paymentMethodSearchRepository.save(paymentMethod);
        return result;
    }

    /**
     * Get all the paymentMethods.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PaymentMethodDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PaymentMethods");
        return paymentMethodRepository.findAll(pageable)
            .map(paymentMethodMapper::toDto);
    }

    /**
     * Get one paymentMethod by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PaymentMethodDTO findOne(Integer id) {
        log.debug("Request to get PaymentMethod : {}", id);
        PaymentMethod paymentMethod = paymentMethodRepository.findOne(id);
        return paymentMethodMapper.toDto(paymentMethod);
    }

    /**
     * Delete the paymentMethod by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete PaymentMethod : {}", id);
        paymentMethodRepository.delete(id);
        paymentMethodSearchRepository.delete(id);
    }

    /**
     * Search for the paymentMethod corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PaymentMethodDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of PaymentMethods for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idMethodType = request.getParameter("idMethodType");

        if (idMethodType != null) {
            q.withQuery(matchQuery("methodType.idPaymentMethodType", idMethodType));
        }

        Page<PaymentMethod> result = paymentMethodSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(paymentMethodMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PaymentMethodDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered PaymentMethodDTO");
        String idMethodType = request.getParameter("idMethodType");

        if (idMethodType != null) {
            return paymentMethodRepository.queryByIdMethodType(Integer.valueOf(idMethodType), pageable).map(paymentMethodMapper::toDto);
        }

        return paymentMethodRepository.queryNothing(pageable).map(paymentMethodMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, PaymentMethodDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<PaymentMethodDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }


    @Transactional
    public void initialize(){
        log.debug("START INIT DATA PAYMENT METHOD");
        List<PaymentMethod> list = paymentMethodRepository.findAll();
        if (list.size() == 0) {
            save(new PaymentMethodDTO(10, "Cash"));
            save(new PaymentMethodDTO(11, "Transfer"));
            save(new PaymentMethodDTO(11, "Mobile Banking"));
            save(new PaymentMethodDTO(12, "BCA"));
            save(new PaymentMethodDTO(12, "BNI"));
            save(new PaymentMethodDTO(12, "CIMB Niaga"));
            save(new PaymentMethodDTO(12, "Mandiri"));
            save(new PaymentMethodDTO(12, "BRI"));
            save(new PaymentMethodDTO(13, "Voucher Rp 50.000"));
            save(new PaymentMethodDTO(13, "Voucher Rp 100.000"));
            save(new PaymentMethodDTO(13, "Voucher Rp 20.000"));
            save(new PaymentMethodDTO(13, "Voucher Rp 10.000"));
            save(new PaymentMethodDTO(13, "Voucher Rp 5.000"));
            save(new PaymentMethodDTO(13, "Voucher Rp 2.000"));
            save(new PaymentMethodDTO(13, "Voucher Rp 1.000"));
            save(new PaymentMethodDTO(13, "Voucher Rp 500"));
        }
    }
}
