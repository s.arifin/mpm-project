package id.atiila.service;

import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.InternalRepository;
import id.atiila.repository.PersonalCustomerRepository;
import id.atiila.repository.ReceiptRepository;
import id.atiila.repository.search.ReceiptSearchRepository;
import id.atiila.service.dto.ReceiptDTO;
import id.atiila.service.dto.SalesUnitRequirementDTO;
import id.atiila.service.mapper.ReceiptMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing Receipt.
 * BeSmart Team
 */

@Service
@Transactional
public class ReceiptService {

    private final Logger log = LoggerFactory.getLogger(ReceiptService.class);

    private final ReceiptRepository receiptRepository;

    private final ReceiptMapper receiptMapper;

    private final ReceiptSearchRepository receiptSearchRepository;

    @Autowired
    private SalesUnitRequirementService salesUnitRequirementService;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private InternalRepository internalRepository;

    @Autowired
    private PersonalCustomerRepository personalCustomerRepository;

    public ReceiptService(ReceiptRepository receiptRepository, ReceiptMapper receiptMapper, ReceiptSearchRepository receiptSearchRepository) {
        this.receiptRepository = receiptRepository;
        this.receiptMapper = receiptMapper;
        this.receiptSearchRepository = receiptSearchRepository;
    }

    /**
     * Save a receipt.
     *
     * @param receiptDTO the entity to save
     * @return the persisted entity
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public ReceiptDTO save(ReceiptDTO receiptDTO) {
        log.debug("Request to save Receipt : {}", receiptDTO);
        Receipt receipt = receiptMapper.toEntity(receiptDTO);

        //create requirement number
        if (receiptDTO.getIdRequirement() != null) {
            log.debug("START FOR CHECK REQUIREMENT NUMBER ");
            SalesUnitRequirementDTO salesUnitRequirementDTO = salesUnitRequirementService.findOne(receiptDTO.getIdRequirement());
            log.debug("CHECK PERSONAL CUSTOMER NIK");
            salesUnitRequirementService.findOrCreateVSONumber(salesUnitRequirementDTO);
            Internal intr =  internalRepository.findByIdInternal(salesUnitRequirementDTO.getInternalId());
            // find or create customer code's
            log.debug("internal di receipt " + salesUnitRequirementDTO.getInternalId());
//            log.debug("dealer code di receipt " + receipt.getInternal().getIdDealerCode());
            customerService.findOrCreateCustomerCode(salesUnitRequirementDTO.getCustomerId(), intr);
        }

        receipt = receiptRepository.save(receipt);
        ReceiptDTO result = receiptMapper.toDto(receipt);
        receiptSearchRepository.save(receipt);
        return result;
    }

    /**
     * Get all the receipts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ReceiptDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Receipts");
        Internal intr = partyUtils.getCurrentInternal();
        return receiptRepository.findActiveReceipt(intr, pageable)
            .map(receiptMapper::toDto);
    }

    /**
     * Get one receipt by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ReceiptDTO findOne(UUID id) {
        log.debug("Request to get Receipt : {}", id);
        Receipt receipt = receiptRepository.findOne(id);
        return receiptMapper.toDto(receipt);
    }

    public List<ReceiptDTO> findByPaidFromId(String id) {
        log.debug("Request to get Receipt : {}", id);
        String idPaidFrom = id;
        List<Receipt> receipt = receiptRepository.findByIdPaidFromId(idPaidFrom);
        return receiptMapper.toDto(receipt);
    }

    /**
     * Delete the receipt by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Receipt : {}", id);
        receiptRepository.delete(id);
        receiptSearchRepository.delete(id);
    }

    /**
     * Search for the receipt corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ReceiptDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of Receipts for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idPaymentType = request.getParameter("idPaymentType");
        String idMethod = request.getParameter("idMethod");
        String idInternal = request.getParameter("idInternal");
        String idPaidTo = request.getParameter("idPaidTo");
        String idPaidFrom = request.getParameter("idPaidFrom");

        if (idPaymentType != null) {
            q.withQuery(matchQuery("paymentType.idPaymentType", idPaymentType));
        }
        else if (idMethod != null) {
            q.withQuery(matchQuery("method.idPaymentMethod", idMethod));
        }
        else if (idInternal != null) {
            q.withQuery(matchQuery("internal.idInternal", idInternal));
        }
        else if (idPaidTo != null) {
            q.withQuery(matchQuery("paidTo.idBillTo", idPaidTo));
        }
        else if (idPaidFrom != null) {
            q.withQuery(matchQuery("paidFrom.idBillTo", idPaidFrom));
        }

        Page<Receipt> result = receiptSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(receiptMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ReceiptDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ReceiptDTO");
        String idPaymentType = request.getParameter("idPaymentType");
        String idMethod = request.getParameter("idMethod");
        String idInternal = request.getParameter("idInternal");
        String idPaidTo = request.getParameter("idPaidTo");
        String idPaidFrom = request.getParameter("idPaidFrom");
        String idRequirement = request.getParameter("idRequirement");
        String isSales = request.getParameter("isSales");


        if (idRequirement != null) {
            log.debug("FIND RECEIPT BY ID REQUIREMENT");
            UUID idReq = UUID.fromString(idRequirement);

            return receiptRepository.findAllByIdRequirement(idReq, pageable).map(receiptMapper::toDto);
        } else if (isSales.equalsIgnoreCase("true") && idRequirement != null) {
            log.debug("FIND RECEIPT BY ID REQUIREMENT ISSALES");
            UUID idReq = UUID.fromString(idRequirement);

            return receiptRepository.queryReceiptByIdRequirement(idReq, pageable).map(receiptMapper::toDto);
        } else if (idPaymentType != null) {
            return receiptRepository.findByIdPaymentType(Integer.valueOf(idPaymentType), pageable).map(receiptMapper::toDto);
        }
        else if (idMethod != null) {
            return receiptRepository.findByIdMethod(Integer.valueOf(idMethod), pageable).map(receiptMapper::toDto);
        }
        else if (idInternal != null) {
            return receiptRepository.findByIdInternal(idInternal, pageable).map(receiptMapper::toDto);
        }
        else if (idPaidTo != null) {
            return receiptRepository.findByIdPaidTo(idPaidTo, pageable).map(receiptMapper::toDto);
        }
        else if (idPaidFrom != null) {
            return receiptRepository.findByIdPaidFrom(idPaidFrom, pageable).map(receiptMapper::toDto);
        }
        return receiptRepository.findAll(pageable).map(receiptMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, ReceiptDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<ReceiptDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        r.put("status", "200");
        return r;
    }

    @Transactional
    public ReceiptDTO changeReceiptStatus(ReceiptDTO dto, Integer id) {
        if (dto != null) {
			Receipt e = receiptRepository.findOne(dto.getIdPayment());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        receiptSearchRepository.delete(dto.getIdPayment());
                        break;
                    default:
                        receiptSearchRepository.save(e);
                }
				receiptRepository.save(e);
			}
		}
        return dto;
    }
}
