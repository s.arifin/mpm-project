package id.atiila.service;

import id.atiila.domain.InternalOrder;
import id.atiila.repository.InternalOrderRepository;
import id.atiila.repository.search.InternalOrderSearchRepository;
import id.atiila.service.dto.InternalOrderDTO;
import id.atiila.service.mapper.InternalOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing InternalOrder.
 * BeSmart Team
 */

@Service
@Transactional
public class InternalOrderService {

    private final Logger log = LoggerFactory.getLogger(InternalOrderService.class);

    private final InternalOrderRepository internalOrderRepository;

    private final InternalOrderMapper internalOrderMapper;

    private final InternalOrderSearchRepository internalOrderSearchRepository;
    public InternalOrderService(InternalOrderRepository internalOrderRepository, InternalOrderMapper internalOrderMapper, InternalOrderSearchRepository internalOrderSearchRepository) {
        this.internalOrderRepository = internalOrderRepository;
        this.internalOrderMapper = internalOrderMapper;
        this.internalOrderSearchRepository = internalOrderSearchRepository;
    }

    /**
     * Save a internalOrder.
     *
     * @param internalOrderDTO the entity to save
     * @return the persisted entity
     */
    public InternalOrderDTO save(InternalOrderDTO internalOrderDTO) {
        log.debug("Request to save InternalOrder : {}", internalOrderDTO);
        InternalOrder internalOrder = internalOrderMapper.toEntity(internalOrderDTO);
        internalOrder = internalOrderRepository.save(internalOrder);
        InternalOrderDTO result = internalOrderMapper.toDto(internalOrder);
        internalOrderSearchRepository.save(internalOrder);
        return result;
    }

    /**
     *  Get all the internalOrders.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InternalOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all InternalOrders");
        return internalOrderRepository.findAll(pageable)
            .map(internalOrderMapper::toDto);
    }

    /**
     *  Get one internalOrder by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public InternalOrderDTO findOne(UUID id) {
        log.debug("Request to get InternalOrder : {}", id);
        InternalOrder internalOrder = internalOrderRepository.findOne(id);
        return internalOrderMapper.toDto(internalOrder);
    }

    /**
     *  Delete the  internalOrder by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete InternalOrder : {}", id);
        internalOrderRepository.delete(id);
        internalOrderSearchRepository.delete(id);
    }

    /**
     * Search for the internalOrder corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InternalOrderDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of InternalOrders for query {}", query);
        Page<InternalOrder> result = internalOrderSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(internalOrderMapper::toDto);
    }

    public InternalOrderDTO processExecuteData(Integer id, String param, InternalOrderDTO dto) {
        InternalOrderDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<InternalOrderDTO> processExecuteListData(Integer id, String param, Set<InternalOrderDTO> dto) {
        Set<InternalOrderDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
