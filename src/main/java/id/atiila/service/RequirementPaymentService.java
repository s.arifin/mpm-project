package id.atiila.service;

import id.atiila.domain.RequirementPayment;
import id.atiila.repository.RequirementPaymentRepository;
import id.atiila.repository.search.RequirementPaymentSearchRepository;
import id.atiila.service.dto.RequirementPaymentDTO;
import id.atiila.service.mapper.RequirementPaymentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing RequirementPayment.
 * BeSmart Team
 */

@Service
@Transactional
public class RequirementPaymentService {

    private final Logger log = LoggerFactory.getLogger(RequirementPaymentService.class);

    private final RequirementPaymentRepository requirementPaymentRepository;

    private final RequirementPaymentMapper requirementPaymentMapper;

    private final RequirementPaymentSearchRepository requirementPaymentSearchRepository;

    public RequirementPaymentService(RequirementPaymentRepository requirementPaymentRepository, RequirementPaymentMapper requirementPaymentMapper, RequirementPaymentSearchRepository requirementPaymentSearchRepository) {
        this.requirementPaymentRepository = requirementPaymentRepository;
        this.requirementPaymentMapper = requirementPaymentMapper;
        this.requirementPaymentSearchRepository = requirementPaymentSearchRepository;
    }

    /**
     * Save a requirementPayment.
     *
     * @param requirementPaymentDTO the entity to save
     * @return the persisted entity
     */
    public RequirementPaymentDTO save(RequirementPaymentDTO requirementPaymentDTO) {
        log.debug("Request to save RequirementPayment : {}", requirementPaymentDTO);
        RequirementPayment requirementPayment = requirementPaymentMapper.toEntity(requirementPaymentDTO);
        requirementPayment = requirementPaymentRepository.save(requirementPayment);
        RequirementPaymentDTO result = requirementPaymentMapper.toDto(requirementPayment);
        requirementPaymentSearchRepository.save(requirementPayment);
        return result;
    }

    /**
     * Get all the requirementPayments.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequirementPaymentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RequirementPayments");
        return requirementPaymentRepository.findAll(pageable)
            .map(requirementPaymentMapper::toDto);
    }

    /**
     * Get one requirementPayment by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public RequirementPaymentDTO findOne(UUID id) {
        log.debug("Request to get RequirementPayment : {}", id);
        RequirementPayment requirementPayment = requirementPaymentRepository.findOne(id);
        return requirementPaymentMapper.toDto(requirementPayment);
    }

    /**
     * Delete the requirementPayment by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete RequirementPayment : {}", id);
        requirementPaymentRepository.delete(id);
        requirementPaymentSearchRepository.delete(id);
    }

    /**
     * Search for the requirementPayment corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequirementPaymentDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of RequirementPayments for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idRequirement = request.getParameter("idRequirement");
        String idPayment = request.getParameter("idPayment");

        if (idRequirement != null) {
            q.withQuery(matchQuery("requirement.idRequirement", idRequirement));
        }
        else if (idPayment != null) {
            q.withQuery(matchQuery("payment.idPayment", idPayment));
        }

        Page<RequirementPayment> result = requirementPaymentSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(requirementPaymentMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<RequirementPaymentDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered RequirementPaymentDTO");
        String idRequirement = request.getParameter("idRequirement");
        String idPayment = request.getParameter("idPayment");

        if (idRequirement != null) {
            return requirementPaymentRepository.findByIdRequirement(UUID.fromString(idRequirement), pageable).map(requirementPaymentMapper::toDto); 
        }
        else if (idPayment != null) {
            return requirementPaymentRepository.findByIdPayment(UUID.fromString(idPayment), pageable).map(requirementPaymentMapper::toDto); 
        }

        return requirementPaymentRepository.findAll(pageable).map(requirementPaymentMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, RequirementPaymentDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<RequirementPaymentDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
