package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.RoleType;
import id.atiila.repository.RoleTypeRepository;
import id.atiila.repository.search.RoleTypeSearchRepository;
import id.atiila.service.dto.RoleTypeDTO;
import id.atiila.service.mapper.RoleTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.PostConstruct;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing RoleType.
 * BeSmart Team
 */

@Service
@Transactional
public class RoleTypeService {

    private final Logger log = LoggerFactory.getLogger(RoleTypeService.class);

    private final RoleTypeRepository roleTypeRepository;

    private final RoleTypeMapper roleTypeMapper;

    private final RoleTypeSearchRepository roleTypeSearchRepository;

    public RoleTypeService(RoleTypeRepository roleTypeRepository, RoleTypeMapper roleTypeMapper, RoleTypeSearchRepository roleTypeSearchRepository) {
        this.roleTypeRepository = roleTypeRepository;
        this.roleTypeMapper = roleTypeMapper;
        this.roleTypeSearchRepository = roleTypeSearchRepository;
    }

    /**
     * Save a roleType.
     *
     * @param roleTypeDTO the entity to save
     * @return the persisted entity
     */
    public RoleTypeDTO save(RoleTypeDTO roleTypeDTO) {
        log.debug("Request to save RoleType : {}", roleTypeDTO);
        RoleType roleType = roleTypeMapper.toEntity(roleTypeDTO);
        roleType = roleTypeRepository.save(roleType);
        RoleTypeDTO result = roleTypeMapper.toDto(roleType);
        roleTypeSearchRepository.save(roleType);
        return result;
    }

    /**
     *  Get all the roleTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RoleTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RoleTypes");
        return roleTypeRepository.findAll(pageable)
            .map(roleTypeMapper::toDto);
    }

    /**
     *  Get one roleType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public RoleTypeDTO findOne(Integer id) {
        log.debug("Request to get RoleType : {}", id);
        RoleType roleType = roleTypeRepository.findOne(id);
        return roleTypeMapper.toDto(roleType);
    }

    /**
     *  Delete the  roleType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete RoleType : {}", id);
        roleTypeRepository.delete(id);
        roleTypeSearchRepository.delete(id);
    }

    /**
     * Search for the roleType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RoleTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of RoleTypes for query {}", query);
        Page<RoleType> result = roleTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(roleTypeMapper::toDto);
    }

    // @PostConstruct
    public void initialize() {
        if (roleTypeRepository.findAll().isEmpty()) {
            for (int i = 1; i <= 100; i ++) {
                save(new RoleTypeDTO(  i,  "Reserved"));
            }

            save(new RoleTypeDTO( Integer.valueOf(BaseConstants.ROLE_PARENT_ORGANIZATION), "Parent Organization"));
            save(new RoleTypeDTO( Integer.valueOf(BaseConstants.ROLE_INTERNAL), "Internal"));
            save(new RoleTypeDTO( Integer.valueOf(BaseConstants.ROLE_BRANCH), "Branch"));
            save(new RoleTypeDTO( Integer.valueOf(BaseConstants.ROLE_CUSTOMER), "Customer"));
            save(new RoleTypeDTO( Integer.valueOf(BaseConstants.ROLE_VENDOR), "Vendor"));

            save(new RoleTypeDTO( Integer.valueOf(BaseConstants.ROLE_SALESMAN), "Salesman"));
            save(new RoleTypeDTO( Integer.valueOf(BaseConstants.ROLE_CUSTOMER_PERSONAL), "Personal Customer"));
            save(new RoleTypeDTO( Integer.valueOf(BaseConstants.ROLE_CUSTOMER_ORGANIZATION), "Customer"));

            save(new RoleTypeDTO( Integer.valueOf(BaseConstants.ROLE_SALES_BROKER), "Sales Broker"));
        }
    }
}
