package id.atiila.service;

import id.atiila.domain.PartyRelationship;
import id.atiila.repository.PartyRelationshipRepository;
import id.atiila.repository.search.PartyRelationshipSearchRepository;
import id.atiila.service.dto.PartyRelationshipDTO;
import id.atiila.service.mapper.PartyRelationshipMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing PartyRelationship.
 * atiila consulting
 */

@Service
@Transactional
public class PartyRelationshipService {

    private final Logger log = LoggerFactory.getLogger(PartyRelationshipService.class);

    private final PartyRelationshipRepository partyRelationshipRepository;

    private final PartyRelationshipMapper partyRelationshipMapper;

    private final PartyRelationshipSearchRepository partyRelationshipSearchRepository;

    public PartyRelationshipService(PartyRelationshipRepository partyRelationshipRepository, PartyRelationshipMapper partyRelationshipMapper, PartyRelationshipSearchRepository partyRelationshipSearchRepository) {
        this.partyRelationshipRepository = partyRelationshipRepository;
        this.partyRelationshipMapper = partyRelationshipMapper;
        this.partyRelationshipSearchRepository = partyRelationshipSearchRepository;
    }

    /**
     * Save a partyRelationship.
     *
     * @param partyRelationshipDTO the entity to save
     * @return the persisted entity
     */
    public PartyRelationshipDTO save(PartyRelationshipDTO partyRelationshipDTO) {
        log.debug("Request to save PartyRelationship : {}", partyRelationshipDTO);
        PartyRelationship partyRelationship = partyRelationshipMapper.toEntity(partyRelationshipDTO);
        partyRelationship = partyRelationshipRepository.save(partyRelationship);
        PartyRelationshipDTO result = partyRelationshipMapper.toDto(partyRelationship);
        partyRelationshipSearchRepository.save(partyRelationship);
        return result;
    }

    /**
     * Get all the partyRelationships.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartyRelationshipDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PartyRelationships");
        return partyRelationshipRepository.findAll(pageable)
            .map(partyRelationshipMapper::toDto);
    }

    /**
     * Get one partyRelationship by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PartyRelationshipDTO findOne(UUID id) {
        log.debug("Request to get PartyRelationship : {}", id);
        PartyRelationship partyRelationship = partyRelationshipRepository.findOne(id);
        return partyRelationshipMapper.toDto(partyRelationship);
    }

    /**
     * Delete the partyRelationship by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete PartyRelationship : {}", id);
        partyRelationshipRepository.delete(id);
        partyRelationshipSearchRepository.delete(id);
    }

    /**
     * Search for the partyRelationship corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartyRelationshipDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of PartyRelationships for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idStatusType = request.getParameter("idStatusType");
        String idRelationType = request.getParameter("idRelationType");

        if (idStatusType != null) {
            q.withQuery(matchQuery("statusType.idStatusType", idStatusType));
        }
        else if (idRelationType != null) {
            q.withQuery(matchQuery("relationType.idRelationType", idRelationType));
        }

        Page<PartyRelationship> result = partyRelationshipSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(partyRelationshipMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PartyRelationshipDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered PartyRelationshipDTO");
        String idStatusType = request.getParameter("idStatusType");
        String idRelationType = request.getParameter("idRelationType");

        if (idStatusType != null) {
            return partyRelationshipRepository.queryByIdStatusType(Integer.valueOf(idStatusType), pageable).map(partyRelationshipMapper::toDto); 
        }
        else if (idRelationType != null) {
            return partyRelationshipRepository.queryByIdRelationType(Integer.valueOf(idRelationType), pageable).map(partyRelationshipMapper::toDto); 
        }

        return partyRelationshipRepository.queryNothing(pageable).map(partyRelationshipMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, PartyRelationshipDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<PartyRelationshipDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
