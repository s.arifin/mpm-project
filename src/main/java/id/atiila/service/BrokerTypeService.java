package id.atiila.service;

import id.atiila.domain.BrokerType;
import id.atiila.repository.BrokerTypeRepository;
import id.atiila.repository.search.BrokerTypeSearchRepository;
import id.atiila.service.dto.BrokerTypeDTO;
import id.atiila.service.mapper.BrokerTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing BrokerType.
 * BeSmart Team
 */

@Service
@Transactional
public class BrokerTypeService {

    private final Logger log = LoggerFactory.getLogger(BrokerTypeService.class);

    private final BrokerTypeRepository brokerTypeRepository;

    private final BrokerTypeMapper brokerTypeMapper;

    private final BrokerTypeSearchRepository brokerTypeSearchRepository;

    public BrokerTypeService(BrokerTypeRepository brokerTypeRepository, BrokerTypeMapper brokerTypeMapper, BrokerTypeSearchRepository brokerTypeSearchRepository) {
        this.brokerTypeRepository = brokerTypeRepository;
        this.brokerTypeMapper = brokerTypeMapper;
        this.brokerTypeSearchRepository = brokerTypeSearchRepository;
    }

    /**
     * Save a brokerType.
     *
     * @param brokerTypeDTO the entity to save
     * @return the persisted entity
     */
    public BrokerTypeDTO save(BrokerTypeDTO brokerTypeDTO) {
        log.debug("Request to save BrokerType : {}", brokerTypeDTO);
        BrokerType brokerType = brokerTypeMapper.toEntity(brokerTypeDTO);
        brokerType = brokerTypeRepository.save(brokerType);
        BrokerTypeDTO result = brokerTypeMapper.toDto(brokerType);
        brokerTypeSearchRepository.save(brokerType);
        return result;
    }

    /**
     *  Get all the brokerTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BrokerTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BrokerTypes");
        return brokerTypeRepository.findAll(pageable)
            .map(brokerTypeMapper::toDto);
    }

    /**
     *  Get one brokerType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public BrokerTypeDTO findOne(Integer id) {
        log.debug("Request to get BrokerType : {}", id);
        BrokerType brokerType = brokerTypeRepository.findOne(id);
        return brokerTypeMapper.toDto(brokerType);
    }

    /**
     *  Delete the  brokerType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete BrokerType : {}", id);
        brokerTypeRepository.delete(id);
        brokerTypeSearchRepository.delete(id);
    }

    /**
     * Search for the brokerType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BrokerTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of BrokerTypes for query {}", query);
        Page<BrokerType> result = brokerTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(brokerTypeMapper::toDto);
    }

    public BrokerTypeDTO processExecuteData(Integer id, String param, BrokerTypeDTO dto) {
        BrokerTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<BrokerTypeDTO> processExecuteListData(Integer id, String param, Set<BrokerTypeDTO> dto) {
        Set<BrokerTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
