package id.atiila.service;

import id.atiila.domain.Internal;
import id.atiila.domain.Person;
import id.atiila.repository.PersonRepository;
import id.atiila.repository.search.PersonSearchRepository;
import id.atiila.service.dto.PersonDTO;
import id.atiila.service.mapper.PersonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

/**
 * Service Implementation for managing Person.
 * BeSmart Team
 */

@Service
@Transactional
public class PersonService {

    private final Logger log = LoggerFactory.getLogger(PersonService.class);

    private final PersonRepository personRepository;

    private final PersonMapper personMapper;

    private final PersonSearchRepository personSearchRepository;

    @Autowired
    private PartyUtils partyUtils;

    public PersonService(PersonRepository personRepository, PersonMapper personMapper, PersonSearchRepository personSearchRepository) {
        this.personRepository = personRepository;
        this.personMapper = personMapper;
        this.personSearchRepository = personSearchRepository;
    }

    /**
     * Save a person.
     *
     * @param personDTO the entity to save
     * @return the persisted entity
     */
    public PersonDTO save(PersonDTO personDTO) {
        log.debug("Request to save Person : {}", personDTO);
        Person person = personMapper.toEntity(personDTO);
        person = personRepository.save(person);
        PersonDTO result = personMapper.toDto(person);
        personSearchRepository.save(person);
        return result;
    }

    /**
     *  Get all the people.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PersonDTO> findAll(Pageable pageable) {
        log.debug("Request to get all People");
        return personRepository.findAll(pageable)
            .map(personMapper::toDto);
    }

    /**
     *  Get one person by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PersonDTO findOne(UUID id) {
        log.debug("Request to get Person : {}", id);
        Person person = personRepository.findOne(id);
        return personMapper.toDto(person);
    }

    @Transactional(readOnly = true)
    public PersonDTO findOneByPersonalIdNumber(String id) {
        log.debug("Request to get Person : {}", id);
        List<Person> persons = personRepository.findAllByPersonalIdNumber(id);
        Person person = persons.isEmpty() ? null : persons.get(0);
        return personMapper.toDto(person);
    }

    @Transactional(readOnly = true)
    public Page<PersonDTO> findOneByPersonalCustomerIdNumber(HttpServletRequest request, Pageable pageable) {
        String personalIdNumber = request.getParameter("personalIdNumber");
        Internal internal = partyUtils.getCurrentInternal();
        String idInternal = internal.getIdInternal();
        Page<Person> pperseon = personRepository.findPersonCustomerWithNik(personalIdNumber,internal.getIdInternal(), pageable);
        log.debug("person atas : {}"+ pperseon.getSize());
        log.debug("person bawah : {}"+ pperseon.getContent().size());
        if (pperseon.getContent().size() <= 0) {
            log.debug("internal ==" +internal.getIdInternal());
            log.debug("root internal ==" +internal.getIdInternal().substring(0,3));
            log.debug("nik internal ==" +personalIdNumber);
            return personRepository.findAllPersonCustomer(personalIdNumber,internal.getIdInternal(),internal.getIdInternal().substring(0,3), pageable).map(personMapper::toDto);
        }
        return pperseon.map(personMapper::toDto);
    }

    @Transactional(readOnly = true)
    public PersonDTO findAllByPersonalIdNumber(String id) {
        log.debug("Request to get Person : {}", id);
        List<Person> persons = personRepository.findAllByPersonalIdNumber(id);

        Person person = null; //persons.isEmpty() ? null : persons.get(0);
        for (Person p: persons) {
            person = p;
        }
        return personMapper.toDto(person);
    }

    /**
     *  Delete the  person by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Person : {}", id);
        personRepository.delete(id);
        personSearchRepository.delete(id);
    }

    /**
     * Search for the person corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PersonDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of People for query {}", query);
        Page<Person> result = personSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(personMapper::toDto);
    }

    public Person getOneByNameAndPobAndDob(String firstName, String lastName, String pob, ZonedDateTime dob) {
        List<Person> r = personRepository.findAllByFirstNameAndLastNameAndPobAndDob(firstName, lastName, pob, dob);
        if (r.isEmpty()) return null;
        return r.get(0);
    }

    public Person getWhenExists(PersonDTO p) {
        return getOneByNameAndPobAndDob(p.getFirstName(), p.getLastName(), p.getPob(), p.getDob());
    }

}
