package id.atiila.service;

import id.atiila.domain.ProductCategory;
import id.atiila.repository.ProductCategoryRepository;
import id.atiila.repository.search.ProductCategorySearchRepository;
import id.atiila.service.dto.ProductCategoryDTO;
import id.atiila.service.mapper.ProductCategoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ProductCategory.
 * BeSmart Team
 */

@Service
@Transactional
public class ProductCategoryService {

    private final Logger log = LoggerFactory.getLogger(ProductCategoryService.class);

    private final ProductCategoryRepository productCategoryRepository;

    private final ProductCategoryMapper productCategoryMapper;

    private final ProductCategorySearchRepository productCategorySearchRepository;

    public ProductCategoryService(ProductCategoryRepository productCategoryRepository, ProductCategoryMapper productCategoryMapper, ProductCategorySearchRepository productCategorySearchRepository) {
        this.productCategoryRepository = productCategoryRepository;
        this.productCategoryMapper = productCategoryMapper;
        this.productCategorySearchRepository = productCategorySearchRepository;
    }

    /**
     * Save a productCategory.
     *
     * @param productCategoryDTO the entity to save
     * @return the persisted entity
     */
    public ProductCategoryDTO save(ProductCategoryDTO productCategoryDTO) {
        log.debug("Request to save ProductCategory : {}", productCategoryDTO);
        ProductCategory productCategory = productCategoryMapper.toEntity(productCategoryDTO);
        productCategory = productCategoryRepository.save(productCategory);
        ProductCategoryDTO result = productCategoryMapper.toDto(productCategory);
        productCategorySearchRepository.save(productCategory);
        return result;
    }

    /**
     * Get all the productCategories.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductCategoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductCategories");
        return productCategoryRepository.findAll(pageable)
            .map(productCategoryMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ProductCategoryDTO> findByProductCategoryType(Integer id, Pageable pageable) {
        log.debug("Request to get all Product Categories by Product Category Type");
        return productCategoryRepository.findAllByCategoryTypeIdCategoryType(id, pageable)
                .map(productCategoryMapper::toDto);
    }

    /**
     * Get one productCategory by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ProductCategoryDTO findOne(Integer id) {
        log.debug("Request to get ProductCategory : {}", id);
        ProductCategory productCategory = productCategoryRepository.findOne(id);
        return productCategoryMapper.toDto(productCategory);
    }

    /**
     * Delete the productCategory by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete ProductCategory : {}", id);
        productCategoryRepository.delete(id);
        productCategorySearchRepository.delete(id);
    }

    /**
     * Search for the productCategory corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductCategoryDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of ProductCategories for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idCategoryType = request.getParameter("idCategoryType");

        if (idCategoryType != null) {
            q.withQuery(matchQuery("categoryType.idCategoryType", idCategoryType));
        }

        Page<ProductCategory> result = productCategorySearchRepository.search(q.build().getQuery(), pageable);
        return result.map(productCategoryMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ProductCategoryDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ProductCategoryDTO");
        String idCategoryType = request.getParameter("idCategoryType");

        if (idCategoryType != null) {
            return productCategoryRepository.queryByIdCategoryType(Integer.valueOf(idCategoryType), pageable).map(productCategoryMapper::toDto);
        }

        return productCategoryRepository.queryNothing(pageable).map(productCategoryMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, ProductCategoryDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<ProductCategoryDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        productCategorySearchRepository.deleteAll();
        List<ProductCategory> productCategorys =  productCategoryRepository.findAll();
        for (ProductCategory m: productCategorys) {
            productCategorySearchRepository.save(m);
            log.debug("Data productCategory save !...");
        }
    }

}
