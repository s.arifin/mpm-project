package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.BranchSearchRepository;
import id.atiila.repository.search.InternalSearchRepository;
import id.atiila.service.dto.InternalDTO;
import id.atiila.service.mapper.InternalMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;

@Service
@Transactional
public class InternalUtils {

    private final Logger log = LoggerFactory.getLogger(InternalUtils.class);

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private BranchSearchRepository branchSearchRepository;

    @Autowired
    private ParentOrganizationRepository parentOrganizationRepository;

    @Autowired
    protected MasterNumberingService numberingService;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private BillToRepository billToRepository;

    @Autowired
    private ShipToRepository shipToRepository;

    @Autowired
    private FacilityRepository facilityRepository;

    @Autowired
    private CalendarUtils calendarUtils;

    @Autowired
    private PositionUtils positionUtils;

    @Autowired
    private InternalRepository<Internal> internalRepository;

    @Autowired
    private InternalSearchRepository internalSearchRepository;

    @Autowired
    private InternalMapper internalMapper;

    @Autowired
    private FacilityUtils facilityUtils;

    private Branch _buildBranch(Internal parent, Integer role, String id, String name) {
        Organization organization = null;
        Branch r = null;

        // Jika ada ID
        if (id != null) {
            r = branchRepository.findByIdInternal(id);
        }
        // Jika ID tidak ditemukan atau ID == null
        if (r == null) {
            organization = partyUtils.buildOrganization(name);
            r = branchRepository.findByOrganization(organization);
        }
        // Jika Organisasi dengan nama ini tidak di temukan
        if (r == null) {

            partyUtils.getPartyRoleType(organization, role);
            partyUtils.getPartyRoleType(organization, BaseConstants.ROLE_INTERNAL);

            r = new Branch();
            r.setIdRoleType(role);
            r.setOrganization(organization);
            r.setIdInternal(id);

            if (id == null) {
                String idBranch = null;
                do {
                    idBranch = numberingService.nextValue("idinternal", 10000l).toString();
                } while (branchRepository.findByIdInternal(idBranch) != null);
                r.setIdInternal(idBranch);
            }

            if (parent != null) {
                r.setParent(parent);
                r.setRoot(parent.getRoot());
            }

            r = branchRepository.save(r);
            //Build Internal bill To dan Ship To
            buildShipTo(r, role);
            buildBillTo(r, role);

            r = branchRepository.save(r);
            branchSearchRepository.save(r);
            internalSearchRepository.save(r);

            calendarUtils.buildReqularCalendar(r, ZonedDateTime.now().getYear() - 1);
            calendarUtils.buildReqularCalendar(r, ZonedDateTime.now().getYear());
        }
        return r;
    }

    private ParentOrganization _buildParentOrganization(String id, String name) {
        Organization organization = null;
        ParentOrganization r = null;

        // Jika ada ID
        if (id != null) {
            r = parentOrganizationRepository.findByIdInternal(id);
        }
        // Jika ID tidak ditemukan atau ID == null
        if (r == null) {
            organization = partyUtils.buildOrganization(name);
            r = parentOrganizationRepository.findByOrganization(organization);
        }
        // Jika Organisasi dengan nama ini tidak di temukan
        if (r == null) {

            partyUtils.getPartyRoleType(organization, BaseConstants.ROLE_PARENT_ORGANIZATION);
            partyUtils.getPartyRoleType(organization, BaseConstants.ROLE_INTERNAL);

            r = new ParentOrganization();
            r.setIdRoleType(BaseConstants.ROLE_PARENT_ORGANIZATION);
            r.setOrganization(organization);
            r.setIdInternal(id);
            if (id == null) {
                String idParent = null;
                do {
                    idParent = numberingService.nextValue("idinternal", 100l).toString();
                } while (parentOrganizationRepository.findByIdInternal(idParent) != null);
                r.setIdInternal(idParent);
            }
            r = parentOrganizationRepository.save(r);
            //Build Internal bill To dan Ship To
            buildShipTo(r, BaseConstants.ROLE_PARENT_ORGANIZATION);
            buildBillTo(r, BaseConstants.ROLE_PARENT_ORGANIZATION);

            r = parentOrganizationRepository.save(r);
            internalSearchRepository.save(r);

            calendarUtils.buildReqularCalendar(r, ZonedDateTime.now().getYear() - 1);
            calendarUtils.buildReqularCalendar(r, ZonedDateTime.now().getYear());
        }
        return r;
    }

    private ShipTo buildShipTo(Internal intr, Integer role) {
        ShipTo shipTo = shipToRepository.findByParty(intr.getOrganization());
        if (shipTo == null) {
            Boolean idHasFound = shipToRepository.findOne(intr.getIdInternal()) != null;
            partyUtils.getPartyRoleType(intr.getOrganization(), role);

            shipTo = new ShipTo();
            shipTo.setIdRoleType(role);
            shipTo.setParty(intr.getOrganization());
            shipTo.setIdShipTo(intr.getIdInternal());
            if (idHasFound) {
                String idShipTo = null;
                do {
                    idShipTo = numberingService.nextShipToValue();
                } while (shipToRepository.findOne(idShipTo) != null);
                shipTo.setIdShipTo(idShipTo);
            }
            shipToRepository.save(shipTo);
        }
        return shipTo;
    }

    private BillTo buildBillTo(Internal intr, Integer role) {
        BillTo billTo = billToRepository.findByParty(intr.getOrganization());
        if (billTo == null) {
            Boolean idHasFound = billToRepository.findOne(intr.getIdInternal()) != null;
            partyUtils.getPartyRoleType(intr.getOrganization(), role);

            billTo = new BillTo();
            billTo.setParty(intr.getOrganization());
            billTo.setIdRoleType(role);
            billTo.setIdBillTo(intr.getIdInternal());
            if (idHasFound) {
                String idBillTo = null;
                do {
                    idBillTo = numberingService.nextBillToValue();
                } while (billToRepository.findOne(idBillTo) != null);
                billTo.setIdBillTo(idBillTo);
            }
            billToRepository.save(billTo);
        }
        return billTo;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Branch buildBranch(Internal p, Integer role, String name) {
        return _buildBranch(p, role, null, name);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Branch buildBranch(Internal p, Integer role, String id, String name) {
        return _buildBranch(p, role, id, name);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public ParentOrganization buildParent(String id, String name) {
        return _buildParentOrganization(id, name);
    }

    public BillTo getBillTo(Internal intr) {
        return buildBillTo(intr, BaseConstants.ROLE_INTERNAL);
    }

    public ShipTo getShipTo(Internal intr) {
        return buildShipTo(intr, BaseConstants.ROLE_INTERNAL);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void prepareOnStart() {
        List<Internal> internals = internalRepository.findAll();
        for (Internal internal: internals) {
            // Build Calendar
            calendarUtils.buildReqularCalendar(internal, ZonedDateTime.now().getYear() + 1);
            calendarUtils.buildReqularCalendar(internal, ZonedDateTime.now().getYear() + 2);
            calendarUtils.buildReqularCalendar(internal, ZonedDateTime.now().getYear() + 3);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Internal buildBranchH1Position(Internal branch) {
        //Posisi Default, 1 Kabeng, 1 Kacab, 3 Korsal, 30 Salesman, 3 Service Analyst, 30 Mekanik, 1 Driver, 1 AFC, 1 Admin Sales, 1 PDI Man, 1 Kasir, 3 Front Desk
        // Check Kacab
        Position kacab = positionUtils.buildPosition(branch, BaseConstants.POSITION_KACAB, 1);
        partyUtils.buildUser(branch,
            kacab,
            "kacab" + branch.getIdInternal(),
            "kacab" + branch.getIdInternal() + "@localhost",
            "kacab", branch.getOrganization().getName());
        // Position AFC
        Position afc = positionUtils.buildPosition(branch, BaseConstants.POSITION_AFC, 1);
        positionUtils.buildStructure(kacab, afc);
        partyUtils.buildUser(branch,
            afc,
            "afc" + branch.getIdInternal(),
            "afc" + branch.getIdInternal() + "@localhost",
            "afc", branch.getOrganization().getName());
        // Position Admin Sales
        Position adm = positionUtils.buildPosition(branch, BaseConstants.POSITION_ADMSALES, 1);
        partyUtils.buildUser(branch,
            adm,
            "adm" + branch.getIdInternal(),
            "adm" + branch.getIdInternal() + "@localhost",
            "adm", branch.getOrganization().getName());

        positionUtils.buildStructure(kacab, adm);
        // Position Driver
        Position driver = positionUtils.buildPosition(branch, BaseConstants.POSITION_DRIVER, 1);
        partyUtils.buildUser(branch,
            driver,
            "driver" + branch.getIdInternal(),
            "driver" + branch.getIdInternal() + "@localhost",
            "driver", branch.getOrganization().getName());

        positionUtils.buildStructure(kacab, driver);
        // Position Kasir
        Position kasir = positionUtils.buildPosition(branch, BaseConstants.POSITION_KASIR, 1);
        partyUtils.buildUser(branch,
            kasir,
            "kasir" + branch.getIdInternal(),
            "kasir" + branch.getIdInternal() + "@localhost",
            "kasir", branch.getOrganization().getName());

        positionUtils.buildStructure(kacab, kasir);

        // Position PDI Man
        Position pdi = positionUtils.buildPosition(branch, BaseConstants.POSITION_PDIMAN, 1);
        partyUtils.buildUser(branch,
            pdi,
            "pdi" + branch.getIdInternal(),
            "pdi" + branch.getIdInternal() + "@localhost",
            "pdi", branch.getOrganization().getName());

        positionUtils.buildStructure(kacab, pdi);

        // for (1..3) korsal
        for (Integer i: new Integer[]{1,2,3}) {
            Position korsal = positionUtils.buildPosition(branch, BaseConstants.POSITION_KORSAL, i);
            positionUtils.buildStructure(kacab, korsal);
            // for (1..10) Salesman
            for (Integer j: new Integer[]{1,2,3,4,5,6,7,8,9,10}) {
                Position salesman = positionUtils.buildPosition(branch, BaseConstants.POSITION_SALESMAN, j);
                positionUtils.buildStructure(korsal, salesman);
                partyUtils.buildUser(branch,
                    salesman,
                    "salesman" + i + branch.getIdInternal(),
                    "salesman" + i + branch.getIdInternal() + "@localhost",
                    "salesman" + i , branch.getOrganization().getName());
            }
        }
        return branch;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Internal buildBranchH2Position(Internal branch) {
        // Check Kabeng
        Position kabeng = positionUtils.buildPosition(branch, BaseConstants.POSITION_KABENG, 1);
        partyUtils.buildUser(branch,
            kabeng,
            "kabeng" + branch.getIdInternal(),
            "kabeng" + branch.getIdInternal() + "@localhost",
            "Kabeng", branch.getOrganization().getName());

        // Position Front Desk
        for (Integer i: new Integer[]{1,2,3}) {
            Position fd = positionUtils.buildPosition(branch, BaseConstants.POSITION_FRONTDESK, i);
            positionUtils.buildStructure(kabeng, fd);
            partyUtils.buildUser(branch,
                fd,
                "fd" + i + branch.getIdInternal(),
                "fd" + i + branch.getIdInternal() + "@localhost",
                "fd" + i , branch.getOrganization().getName());
        }
        // for (1..3) Service Analyst
        for (Integer i: new Integer[]{1,2,3}) {
            Position servan = positionUtils.buildPosition(branch, BaseConstants.POSITION_SERV_ANALYS, i);
            positionUtils.buildStructure(kabeng, servan);
            // for (1..10) Mekanik
            for (Integer j: new Integer[]{1,2,3,4,5,6,7,8,9,10}) {
                Position mekanik = positionUtils.buildPosition(branch, BaseConstants.POSITION_MEKANIK, j);
                positionUtils.buildStructure(servan, mekanik);
                partyUtils.buildUser(branch,
                    mekanik,
                    "mekanik" + i + branch.getIdInternal(),
                    "mekanik" + i + branch.getIdInternal() + "@localhost",
                    "mekanik" + i , branch.getOrganization().getName());
            }
        }
        return branch;
    }

    public InternalDTO toDTO(Internal intr) {
        return internalMapper.toDto(intr);
    }

    @Transactional
    public Internal findOne(String id) {
        return internalRepository.findOne(id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildMSO(String id, String name) {
        Organization organization = null;
        ParentOrganization r = null;

        // Jika ada ID
        if (id != null) {
            r = parentOrganizationRepository.findByIdInternal(id);
        }
        // Jika ID tidak ditemukan atau ID == null
        if (r == null) {
            organization = partyUtils.buildOrganization(name);
            r = parentOrganizationRepository.findByOrganization(organization);
        }
        // Jika Organisasi dengan nama ini tidak di temukan
        if (r == null) {

            partyUtils.getPartyRoleType(organization, BaseConstants.ROLE_PARENT_ORGANIZATION);
            partyUtils.getPartyRoleType(organization, BaseConstants.ROLE_INTERNAL);

            r = new ParentOrganization();
            r.setIdRoleType(BaseConstants.ROLE_PARENT_ORGANIZATION);
            r.setOrganization(organization);
            r.setIdInternal(id);
            r.setRoot(r);
            if (id == null) {
                String idParent = null;
                do {
                    idParent = numberingService.nextValue("idinternal", 100l).toString();
                } while (parentOrganizationRepository.findByIdInternal(idParent) != null);
                r.setIdInternal(idParent);
            }
            r = parentOrganizationRepository.save(r);

            //Build Internal bill To dan Ship To
            buildShipTo(r, BaseConstants.ROLE_PARENT_ORGANIZATION);
            buildBillTo(r, BaseConstants.ROLE_PARENT_ORGANIZATION);

            //Build Facility
            Facility building = facilityUtils.buildBuilding(id, name);
            r.addFacility(facilityUtils.getPurposeType(BaseConstants.PURP_TYPE_MAIN_FACILITY), building);
            r = parentOrganizationRepository.save(r);
            internalSearchRepository.save(r);

            calendarUtils.buildReqularCalendar(r, ZonedDateTime.now().getYear() - 1);
            calendarUtils.buildReqularCalendar(r, ZonedDateTime.now().getYear());

            // ***************
            Branch h1 = buildBranch( r, BaseConstants.ROLE_DEALER, id + "01", name + " (H1)");
            buildBranchH1Position(h1);

            // Tambahkan Dealer Outlet
            Facility store = facilityUtils.buildStore(building, h1.getIdInternal() + "S", h1.getOrganization().getName() + " (Store)");
            h1.addFacility(facilityUtils.getPurposeType(BaseConstants.PURP_TYPE_STORE), store);
            Facility wh = facilityUtils.buildWareHouse(building, h1.getIdInternal() + "G", h1.getOrganization().getName() + " (Warehouse)");
            h1.addFacility(facilityUtils.getPurposeType(BaseConstants.PURP_TYPE_WAREHOUSE), wh);
            branchRepository.save(h1);

            // Add Container at WH
            facilityUtils.buildContainer(wh, BaseConstants.CONTAINER_TYPE_RAK, wh.getFacilityCode() + "-A01", "Container " + wh.getFacilityCode() + "-A01");
            facilityUtils.buildContainer(wh, BaseConstants.CONTAINER_TYPE_RAK, wh.getFacilityCode() + "-A02", "Container " + wh.getFacilityCode() + "-A02");
            facilityUtils.buildContainer(wh, BaseConstants.CONTAINER_TYPE_RAK, wh.getFacilityCode() + "-A03", "Container " + wh.getFacilityCode() + "-A03");
            facilityUtils.buildContainer(wh, BaseConstants.CONTAINER_TYPE_RAK, wh.getFacilityCode() + "-A04", "Container " + wh.getFacilityCode() + "-A04");

            // ***************
            Branch h2 = buildBranch( r, BaseConstants.ROLE_WORKSHOP, id + "02", name + " (H23)");
            buildBranchH2Position(h2);

            // Build workshop
            Facility workshop = facilityUtils.buildStore(building, h2.getIdInternal() + "W", h2.getOrganization().getName() + " (Workshop)");
            h2.addFacility(facilityUtils.getPurposeType(BaseConstants.PURP_TYPE_WORKSHOP), workshop);
            wh = facilityUtils.buildWareHouse(building, h2.getIdInternal() + "G", h2.getOrganization().getName() + " (Warehouse)");
            h2.addFacility(facilityUtils.getPurposeType(BaseConstants.PURP_TYPE_WAREHOUSE), wh);
            branchRepository.save(h2);

            // Add Container at WH
            facilityUtils.buildContainer(wh, BaseConstants.CONTAINER_TYPE_RAK, wh.getFacilityCode() + "-A01", "Container " + wh.getFacilityCode() + "-A01");
            facilityUtils.buildContainer(wh, BaseConstants.CONTAINER_TYPE_RAK, wh.getFacilityCode() + "-A02", "Container " + wh.getFacilityCode() + "-A02");
            facilityUtils.buildContainer(wh, BaseConstants.CONTAINER_TYPE_RAK, wh.getFacilityCode() + "-A03", "Container " + wh.getFacilityCode() + "-A03");
            facilityUtils.buildContainer(wh, BaseConstants.CONTAINER_TYPE_RAK, wh.getFacilityCode() + "-A04", "Container " + wh.getFacilityCode() + "-A04");

        }
    }

}
