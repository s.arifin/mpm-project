package id.atiila.service;

import id.atiila.domain.Service;
import id.atiila.repository.ServiceRepository;
import id.atiila.repository.search.ServiceSearchRepository;
import id.atiila.service.dto.ServiceDTO;
import id.atiila.service.mapper.ServiceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Service.
 * BeSmart Team
 */

@org.springframework.stereotype.Service
@Transactional
public class ServiceService {

    private final Logger log = LoggerFactory.getLogger(ServiceService.class);

    private final ServiceRepository serviceRepository;

    private final ServiceMapper serviceMapper;

    private final ServiceSearchRepository serviceSearchRepository;

    @Autowired
    private MasterNumberingService numbering;


    public ServiceService(ServiceRepository serviceRepository, ServiceMapper serviceMapper, ServiceSearchRepository serviceSearchRepository) {
        this.serviceRepository = serviceRepository;
        this.serviceMapper = serviceMapper;
        this.serviceSearchRepository = serviceSearchRepository;
    }

    /**
     * Save a service.
     *
     * @param serviceDTO the entity to save
     * @return the persisted entity
     */
    public ServiceDTO save(ServiceDTO serviceDTO) {
        log.debug("Request to save Service : {}", serviceDTO);
        Service service = serviceMapper.toEntity(serviceDTO);

        Boolean isNew = service.getIdProduct() == null;
        if (isNew) {
            String newValue = null;
            while (newValue == null || serviceRepository.findOne(newValue) != null) {
                newValue = numbering.nextValue("idservice", 30000l).toString();
            }
            service.setIdProduct(newValue);
        }


        service = serviceRepository.save(service);
        ServiceDTO result = serviceMapper.toDto(service);
        serviceSearchRepository.save(service);
        return result;
    }

    /**
     *  Get all the services.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ServiceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Services");
        return serviceRepository.findAll(pageable)
            .map(serviceMapper::toDto);
    }

    /**
     *  Get one service by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ServiceDTO findOne(String id) {
        log.debug("Request to get Service : {}", id);
        Service service = serviceRepository.findOneWithEagerRelationships(id);
        return serviceMapper.toDto(service);
    }

    /**
     *  Delete the  service by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Service : {}", id);
        serviceRepository.delete(id);
        serviceSearchRepository.delete(id);
    }

    /**
     * Search for the service corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ServiceDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Services for query {}", query);
        Page<Service> result = serviceSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(serviceMapper::toDto);
    }

    public ServiceDTO processExecuteData(Integer id, String param, ServiceDTO dto) {
        ServiceDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<ServiceDTO> processExecuteListData(Integer id, String param, Set<ServiceDTO> dto) {
        Set<ServiceDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }


    public void buildIndex() {
        serviceSearchRepository.deleteAll();
        List<Service> services =  serviceRepository.findAll();
        for (Service m: services) {
            serviceSearchRepository.save(m);
            log.debug("Data service save !....");
        }
    }

}
