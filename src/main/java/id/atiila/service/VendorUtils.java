package id.atiila.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.atiila.base.BaseConstants;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.OrganizationCustomerSearchRepository;
import id.atiila.repository.search.PersonalCustomerSearchRepository;
import id.atiila.repository.search.VendorSearchRepository;
import id.atiila.service.ax.dto.AxCustomerDTO;
import id.atiila.service.ax.dto.AxVendorDTO;
import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

@Service
@Transactional
public class VendorUtils {

    @Autowired
    protected MasterNumberingService numberingService;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private BillToRepository billToRepository;

    @Autowired
    private ShipToRepository shipToRepository;

    @Autowired
    private VendorSearchRepository vendorSearchRepository;

    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private ActivitiProcessor processor;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private VendorRelationshipRepository vendorRelationshipRepository;

    ReentrantLock lock = new ReentrantLock();

    @Transactional(propagation = Propagation.REQUIRED)
    protected Vendor _buildVendor(String id, Organization organization, Integer role) {
        Vendor r = vendorRepository.findByOrganization(organization);
        try {
            if (r == null) {

                partyUtils.getPartyRoleType(organization, role);
                partyUtils.getPartyRoleType(organization, BaseConstants.ROLE_VENDOR);

                r = new Vendor();
                r.setIdVendor(id);
                r.setOrganization(organization);
                r.setIdRoleType(role);

                if (id == null || vendorRepository.findOne(id) != null) {
                    String idVend = null;
                    do {
                        idVend = numberingService.nextVendorValue();
                    } while (vendorRepository.findOne(idVend) != null);
                    r.setIdVendor(idVend);
                }
                r = vendorRepository.save(r);
                vendorSearchRepository.save(r);

                Map<String, Object> vars = processor.getVariables("party", r.getOrganization());
                vars.put("idRoleType", BaseConstants.ROLE_VENDOR);
                vars.put("idGeneral", r.getIdVendor());
                processor.startProcess("vendor-verification", r.getIdVendor(), vars);

                // Add Customer Relationship
                Internal internal = partyUtils.getCurrentInternal();
                if (internal != null) {
                    getRelationhip(partyUtils.getRelationType(BaseConstants.REL_TYPE_VENDOR, "Vendor"), internal, r);
                }

            }
        } finally {
        }
        return r;
    }

    public Vendor buildVendor(String name) {
        Organization organization = partyUtils.buildOrganization(name);
        return _buildVendor(null, organization, BaseConstants.ROLE_VENDOR);
    }

    public Vendor buildVendor(String id, String name) {
        Organization organization = partyUtils.buildOrganization(name);
        return _buildVendor(id, organization, BaseConstants.ROLE_VENDOR);
    }

    public Vendor buildVendor(String id, Organization organization) {
        return _buildVendor(id, organization, BaseConstants.ROLE_VENDOR);
    }

    public Vendor buildVendor(String id, Organization organization, Integer role) {
        return _buildVendor(id, organization, role);
    }

    public Vendor buildVendor(String id, String name, Integer role) {
        Organization organization = partyUtils.buildOrganization(name);
        return _buildVendor(id, organization, role);
    }

    public Vendor getVendor(Party organization) {
        return vendorRepository.getVendorByParty(organization);
    }

    public Vendor buildVendor(Organization organization) {
        return _buildVendor( null, organization, BaseConstants.ROLE_VENDOR);
    }

    public BillTo getBillTo(Vendor c) {
        return billToRepository.findByParty(c.getOrganization());
    }

    public ShipTo getShipTo(Vendor c) {
        return shipToRepository.findByParty(c.getOrganization());
    }

    public void buildVendorData(DelegateExecution execution) {
    }

    public void buildUplinkData(DelegateExecution execution) {
        String key = execution.getProcessInstanceBusinessKey();
        Vendor vendor = vendorRepository.findOne(key);
        if (vendor != null) {
            Internal internal = partyUtils.getCurrentInternal();
            if (internal != null) {

                AxVendorDTO dto = new AxVendorDTO();
                dto.setMainAccount(vendor.getIdVendor());
                dto.setName(vendor.getOrganization().getName());
                // dto.setAddress(vendor.getOrganization().getPostalAddress().getAddress1());

                dto.setDataArea(internal.getRoot().getIdInternal());
                // dto = restTemplate.postForObject("/api/ax_vendor", dto, AxVendorDTO.class);
                ObjectMapper objectMapper = new ObjectMapper();
                try {
                    objectMapper.writeValue(new File("d:/tmp/vendor.json"), dto);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public VendorRelationship getRelationhip(RelationType relationType, Internal internal, Vendor vendor) {
        VendorRelationship r = vendorRelationshipRepository.findRelationship(relationType.getIdRelationType(), internal.getIdInternal(), vendor.getIdVendor());
        if (r == null) {
            r = new VendorRelationship();
            r.setVendor(vendor);
            r.setInternal(internal);
            r.setRelationType(relationType);
            r.setStatusType(partyUtils.getStatusType(BaseConstants.STATUS_ACTIVE, "Actived"));
            r = vendorRelationshipRepository.save(r);
        }
        return r;
    }
}
