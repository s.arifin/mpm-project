package id.atiila.service;

import id.atiila.domain.FacilityContactMechanism;
import id.atiila.repository.FacilityContactMechanismRepository;
import id.atiila.repository.search.FacilityContactMechanismSearchRepository;
import id.atiila.service.dto.FacilityContactMechanismDTO;
import id.atiila.service.mapper.FacilityContactMechanismMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing FacilityContactMechanism.
 * BeSmart Team
 */

@Service
@Transactional
public class FacilityContactMechanismService {

    private final Logger log = LoggerFactory.getLogger(FacilityContactMechanismService.class);

    private final FacilityContactMechanismRepository facilityContactMechanismRepository;

    private final FacilityContactMechanismMapper facilityContactMechanismMapper;

    private final FacilityContactMechanismSearchRepository facilityContactMechanismSearchRepository;

    public FacilityContactMechanismService(FacilityContactMechanismRepository facilityContactMechanismRepository, FacilityContactMechanismMapper facilityContactMechanismMapper, FacilityContactMechanismSearchRepository facilityContactMechanismSearchRepository) {
        this.facilityContactMechanismRepository = facilityContactMechanismRepository;
        this.facilityContactMechanismMapper = facilityContactMechanismMapper;
        this.facilityContactMechanismSearchRepository = facilityContactMechanismSearchRepository;
    }

    /**
     * Save a facilityContactMechanism.
     *
     * @param facilityContactMechanismDTO the entity to save
     * @return the persisted entity
     */
    public FacilityContactMechanismDTO save(FacilityContactMechanismDTO facilityContactMechanismDTO) {
        log.debug("Request to save FacilityContactMechanism : {}", facilityContactMechanismDTO);
        FacilityContactMechanism facilityContactMechanism = facilityContactMechanismMapper.toEntity(facilityContactMechanismDTO);
        facilityContactMechanism = facilityContactMechanismRepository.save(facilityContactMechanism);
        FacilityContactMechanismDTO result = facilityContactMechanismMapper.toDto(facilityContactMechanism);
        facilityContactMechanismSearchRepository.save(facilityContactMechanism);
        return result;
    }

    /**
     * Get all the facilityContactMechanisms.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FacilityContactMechanismDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FacilityContactMechanisms");
        return facilityContactMechanismRepository.findAll(pageable)
            .map(facilityContactMechanismMapper::toDto);
    }

    /**
     * Get one facilityContactMechanism by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public FacilityContactMechanismDTO findOne(UUID id) {
        log.debug("Request to get FacilityContactMechanism : {}", id);
        FacilityContactMechanism facilityContactMechanism = facilityContactMechanismRepository.findOne(id);
        return facilityContactMechanismMapper.toDto(facilityContactMechanism);
    }

    /**
     * Delete the facilityContactMechanism by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete FacilityContactMechanism : {}", id);
        facilityContactMechanismRepository.delete(id);
        facilityContactMechanismSearchRepository.delete(id);
    }

    /**
     * Search for the facilityContactMechanism corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FacilityContactMechanismDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of FacilityContactMechanisms for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idFacility = request.getParameter("idFacility");
        String idContact = request.getParameter("idContact");

        if (idFacility != null) {
            q.withQuery(matchQuery("facility.idFacility", idFacility));
        }
        else if (idContact != null) {
            q.withQuery(matchQuery("contact.idContact", idContact));
        }

        Page<FacilityContactMechanism> result = facilityContactMechanismSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(facilityContactMechanismMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<FacilityContactMechanismDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered FacilityContactMechanismDTO");
        String idFacility = request.getParameter("idFacility");
        String idContact = request.getParameter("idContact");

        if (idFacility != null) {
            return facilityContactMechanismRepository.findByIdFacility(UUID.fromString(idFacility), pageable).map(facilityContactMechanismMapper::toDto); 
        }
        else if (idContact != null) {
            return facilityContactMechanismRepository.findByIdContact(UUID.fromString(idContact), pageable).map(facilityContactMechanismMapper::toDto); 
        }

        return facilityContactMechanismRepository.findAll(pageable).map(facilityContactMechanismMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, FacilityContactMechanismDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<FacilityContactMechanismDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
