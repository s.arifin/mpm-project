package id.atiila.service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IAtomicLong;
import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.base.executeprocess.ProspectPersonConstants;
import id.atiila.domain.*;
import id.atiila.repository.InternalRepository;
import id.atiila.repository.PersonRepository;
import id.atiila.repository.PostalAddressRepository;
import id.atiila.repository.ProspectPersonRepository;
import id.atiila.repository.search.ProspectPersonSearchRepository;
import id.atiila.route.IndexerRoute;
import id.atiila.service.dto.*;
import id.atiila.service.mapper.PersonMapper;
import id.atiila.service.mapper.ProspectPersonMapper;
import id.atiila.service.pto.ProspectPTO;
import org.apache.camel.Body;
import org.apache.camel.ProducerTemplate;
import org.elasticsearch.index.query.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ProspectPerson.
 * BeSmart Team
 */

@Service
@Transactional
public class ProspectPersonService {

    private final Logger log = LoggerFactory.getLogger(ProspectPersonService.class);

    private final ProspectPersonRepository prospectPersonRepository;

    private final ProspectPersonMapper prospectPersonMapper;

    private final ProspectPersonSearchRepository prospectPersonSearchRepository;


    @Autowired
    private SalesUnitRequirementService surSvc;

    @Autowired
    private MasterNumberingService masterNumberingService;

    @Autowired
    private SuspectPersonService suspectPersonSvc;

    @Autowired
    private PostalAddressRepository postalAddressRepo;

    @Autowired
    private PersonRepository personRepo;

    @Autowired
    private PersonService personSrv;

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private DateUtils dateUtils;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private CustomerUtils customerUtils;

    @Autowired
    private HazelcastInstance hz;

    @Autowired
    private InternalRepository internalRepository;

    public ProspectPersonService(ProspectPersonRepository prospectPersonRepository,
                                 ProspectPersonMapper prospectPersonMapper,
                                 ProspectPersonSearchRepository prospectPersonSearchRepository) {

        this.prospectPersonRepository = prospectPersonRepository;
        this.prospectPersonMapper = prospectPersonMapper;
        this.prospectPersonSearchRepository = prospectPersonSearchRepository;
    }

    /**
     * Save a prospectPerson.
     *
     * @param prospectPersonDTO the entity to save
     * @return the persisted entity
     */
    @Transactional
    public ProspectPersonDTO save(ProspectPersonDTO prospectPersonDTO) {
        log.debug("Request to save ProspectPerson: {} " + prospectPersonDTO);

        //Validation On New Data Prospecct
        ProspectPerson prospectPerson = validationProspect(prospectPersonDTO);

        prospectPerson.setfName(prospectPerson.getPerson().getFirstName());
        prospectPerson.setlName(prospectPerson.getPerson().getFirstName());
        prospectPerson.setCellphone(prospectPerson.getPerson().getCellPhone1());
        prospectPerson = prospectPersonRepository.save(prospectPerson);
        ProspectPersonDTO result = prospectPersonMapper.toDto(prospectPerson);
        prospectPersonSearchRepository.save(result);
        return result;
    }

    @Transactional
    public ProspectPersonDTO saveFollowUp(ProspectPersonDTO prospectPersonDTO) {
        log.debug("Request to save ProspectPerson: {} " + prospectPersonDTO);
        Internal internal = internalRepository.findByIdInternal(prospectPersonDTO.getDealerId());
        ProspectPerson prospectPerson = validationProspect(prospectPersonDTO);

        prospectPerson.setfName(prospectPerson.getPerson().getFirstName());
        prospectPerson.setlName(prospectPerson.getPerson().getFirstName());
        prospectPerson.setCellphone(prospectPerson.getPerson().getCellPhone1());
        if (prospectPerson.getProspectNumber() == null) {
            String pnumber = masterNumberingService.nextProspectNumber(prospectPersonDTO.getDealerId(), "IDPRO") ;
            prospectPerson.setProspectNumber(pnumber);
        }
        log.debug("number prospect = "+ prospectPerson.getProspectNumber());
        prospectPerson = prospectPersonRepository.save(prospectPerson);
        ProspectPersonDTO result = prospectPersonMapper.toDto(prospectPerson);
        prospectPersonSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the prospectPeople.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProspectPersonDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProspectPeople");
        return prospectPersonRepository.findActiveProspectPerson(pageable)
            .map(prospectPersonMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ProspectPersonDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ProspectPeople");
        Integer idStatusType = request.getParameter("idStatusType") == null ?
            null : Integer.parseInt(request.getParameter("idStatusType"));
        String idInternal = request.getParameter("idInternal");

        Page<ProspectPersonDTO> prospectPersonDTOS;

        if (idStatusType != null){
            prospectPersonDTOS =  prospectPersonRepository.findProspectPersonByStatusType(
                pageable,idStatusType, partyUtils.getCurrentSalesman(), idInternal)
                .map(prospectPersonMapper::toDto);
        }
        else {
            prospectPersonDTOS =  prospectPersonRepository.findActiveProspectPerson(pageable).map(prospectPersonMapper::toDto);

        }
        return prospectPersonDTOS;
    }



    /**
     *  Get one prospectPerson by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ProspectPersonDTO findOne(UUID id) {
        log.debug("Request to get ProspectPerson : {}", id);
        ProspectPerson prospectPerson = prospectPersonRepository.findOne(id);
        return prospectPersonMapper.toDto(prospectPerson);
    }

    /**
     *  Delete the  prospectPerson by id.
     *
     *  @param id the id of the entity
     */
    @Transactional
    public void delete(UUID id) {
        log.debug("Request to delete ProspectPerson : {}", id);
        prospectPersonRepository.delete(id);
        prospectPersonSearchRepository.delete(id);
    }

    /**
     * Search for the prospectPerson corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public Page<ProspectPersonDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of ProspectPeople for query {}", query);
        String statusType = request.getParameter("idStatusType");

        UUID salesmanId = partyUtils.getCurrentSalesman().getIdPartyRole() == null ?
            null : partyUtils.getCurrentSalesman().getIdPartyRole();

        QueryBuilder boolQueryBuilder = queryStringQuery(query + "*")
                                        .queryName("person.firstName")
                                        .queryName("person.lastName")
                                        .queryName("person.cellphone1");

        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder();
        q.withQuery(boolQuery().must(boolQueryBuilder).filter(
            boolQuery().must(matchQuery("salesmanId", salesmanId))
                       .must(matchQuery("currentStatus", Integer.parseInt(statusType))))
        );
        Page<ProspectPersonDTO> result = prospectPersonSearchRepository.search(q.build().getQuery(), pageable);
        return result;
    }

    @Transactional(readOnly = true)
    public ProspectPerson findByPerson(Person p, String idInternal) {
        List<ProspectPerson> lp = prospectPersonRepository.findProspectPersonByPerson(p.getIdParty(), idInternal);
        if (lp.isEmpty()) return null;
        return lp.get(0);
    }

    @Transactional(readOnly = true)
    public Page<CustomProspectDTO> findSummarizeProspect(Pageable pageable, String idInternal) {
        List<CustomProspectDTO> customs = new ArrayList<>();

        UUID idSalesCoordinator = partyUtils.getCurrentSalesman() == null ?
            null : partyUtils.getCurrentSalesman().getIdPartyRole();

        Page<Salesman> salesmen = prospectPersonRepository.findSalesmenByIdCoordinator(idSalesCoordinator, pageable);

        for( Salesman s : salesmen.getContent()){
            CustomProspectDTO custom = new CustomProspectDTO();

            custom.setSalesmanId(s.getIdPartyRole());
            custom.setSalesmanName(s.getParty().getName());
            custom.setHotCount((long)0);
            custom.setMediumCount((long)0);
            custom.setLowCount((long)0);

            List<SummaryProspectDTO> summaries = prospectPersonRepository.findProspectSummaryByIdSalesman(
                s.getIdPartyRole(), idInternal);

            for(SummaryProspectDTO sum : summaries){
                if (sum.getStatustypeId().equals(53)){
                    custom.setHotCount(sum.getStatusTotal());
                }
                else if (sum.getStatustypeId().equals(52)){
                    custom.setMediumCount(sum.getStatusTotal());
                }
                else if (sum.getStatustypeId().equals(51)){
                    custom.setLowCount(sum.getStatusTotal());
                }
            }

            customs.add(custom);
        }

        Page<CustomProspectDTO> result = new PageImpl<>(customs, pageable, customs.size());
        return result;
    }

    @Transactional
    protected void changeSalesman(UUID param, UUID id) {
        ProspectPerson p = prospectPersonRepository.findOne(id);
        p.setSalesman(prospectPersonRepository.findSalesmanById(param));
        prospectPersonRepository.save(p);
    }

//    @Transactional(readOnly = true)
//    public Page<VehicleSalesOrder> toSearch(HttpServletRequest request, Pageable pageable) {
//        Internal internal = partyUtils.getCurrentInternal();
//        String queryfor = request.getParameter("queryFor");
//        String data = request.getParameter("data");
//        String dataLike = '%'+ data +'%';
//        log.debug("to search data" +data);
//        log.debug("to search query for" +dataLike);
//        if (data != null) {
//            return prospectPersonRepository.findToReindex(dataLike, internal.getIdInternal(), pageable);
//        }
//        return prospectPersonRepository.queryNothing(pageable);
//    }

    @Transactional
    protected void updateDateFollowUp(ZonedDateTime param, UUID id){
        ProspectPerson p = prospectPersonRepository.findOne(id);
        p.setDateFollowUp(param);
        if (p.getProspectCount().equals(2)) {p.setProspectCount(3);}
        prospectPersonRepository.save(p);
    }

    @Transactional
    protected Integer findLastStatusBeforeRequestDrop(UUID id, List<Integer> statuses){
        List<ProspectStatus> prospectStatuses = prospectPersonRepository.findLastStatusBeforeRequestDrop(
            new PageRequest(0,1, Sort.Direction.DESC, "dateThru"), id).getContent();
        Integer result = BaseConstants.STATUS_NOT_DEFINED;
        for (ProspectStatus stat: prospectStatuses) {
            if (statuses.contains(stat.getIdStatusType())) return stat.getIdStatusType();
        }
        return result;
    }

    @Transactional
    protected void updateStatusWhenRejected(ProspectPersonDTO p){
        List<Integer> statuses =
            Arrays.asList(
                BaseConstants.STATUS_DRAFT,
                BaseConstants.STATUS_LOW,
                BaseConstants.STATUS_MEDIUM,
                BaseConstants.STATUS_HOT);
        changeProspectPersonStatus(p,findLastStatusBeforeRequestDrop(p.getIdProspect(),statuses));
    }

    @Transactional
    protected ProspectPersonDTO findProspectWhenExist(PersonDTO personDTO, String idInternal){

        //TODO: HANYA SEMENTARA SEBELUM MENGGUNAKAN EXTRA ROLE
        OnlySalesmanAllow();

        Person p = null;

        if (p == null){
            p = personMapper.toEntity(personDTO);
//            p = partyUtils.findPersonByPhone(p);
        }

        if (p == null) return new ProspectPersonDTO();

        ProspectPerson findProspect = findByPerson(p, idInternal);

        if (findProspect == null) {
            if (p != null) {
                ProspectPersonDTO pDTO = new ProspectPersonDTO();
                pDTO.setPerson(personMapper.toDto(p));
                return pDTO;
            }
            else {
                return new ProspectPersonDTO();
            }
        }

        return prospectPersonMapper.toDto(findProspect);
    }


    @Transactional
    protected ProspectPersonDTO findProspectPersonWhenExist(PersonDTO personDTO, String idInternal){

        //TODO: HANYA SEMENTARA SEBELUM MENGGUNAKAN EXTRA ROLE
        OnlySalesmanAllow();

        Person p = null;
        ProspectPerson findProspect = null;

        log.debug("phoneee === " + personDTO.getCellPhone1());
        if (personDTO != null){
            List<ProspectPerson> ListFindProspect = prospectPersonRepository.findProspectByCellphone( personDTO.getCellPhone1(), idInternal );
            if (!ListFindProspect.isEmpty()) {
                findProspect = ListFindProspect.get(0);
            }
        }
        if (personDTO == null) return new ProspectPersonDTO();

//        findProspect = findByPerson(p, idInternal);

        if (findProspect == null) {
            if (p != null) {
                ProspectPersonDTO pDTO = new ProspectPersonDTO();
                pDTO.setPerson(personMapper.toDto(p));
                return pDTO;
            }
            else {
                return new ProspectPersonDTO();
            }
        }

        return prospectPersonMapper.toDto(findProspect);
    }

//    @Transactional
//    public ProspectPersonDTO pro (PersonDTO personDTO, String personalIdNumber){
//
//        //TODO: HANYA SEMENTARA SEBELUM MENGGUNAKAN EXTRA ROLE
//        OnlySalesmanAllow();
//
//        Person p = null;
//        ProspectPerson findPerson = null;
//
//        log.debug("NIK ====" +personDTO.getPersonalIdNumber());
//        if (personDTO != null){
//            List<ProspectPerson> ListFindNIK = prospectPersonRepository.findPersonBypersonalIdNumber( personDTO.getPersonalIdNumber());
//            if (!ListFindNIK.isEmpty()) {
//                ProspectPerson pp = null;
//                pp = ListFindNIK.get(0);
//            }
//        }
//        if (personDTO == null) return new ProspectPersonDTO();
//
//        if (findPerson == null) {
//            if (p !=null)   {
//                ProspectPersonDTO pDTO = new ProspectPersonDTO();
//                pDTO.setPerson(personMapper.toDto(p));
//                return pDTO;
//            }
//            else {
//                return new ProspectPersonDTO();
//            }
//        }
//        return prospectPersonMapper.toDto(findPerson);
//    }


    @Transactional(propagation = Propagation.REQUIRED)
    public ProspectPersonDTO processExecuteData(Integer id, String param, ProspectPersonDTO dto) {
        ProspectPersonDTO r = dto;
        if (r != null) {
            switch (id) {
                case ProspectPersonConstants.COMPLETION_PROSPECT:
                    r = changeProspectPersonStatus(r,BaseConstants.STATUS_COMPLETED);
                    break;
                case ProspectPersonConstants.CHANGE_PROSPECT_SALESMAN:
                    changeSalesman(UUID.fromString(param), r.getIdProspect());
                    break;
                case ProspectPersonConstants.TL_REQUEST_DROP_PROSPECT:
                    updateDateFollowUp(dateUtils.parse(param), r.getIdProspect());
                    r = changeProspectPersonStatus(r,BaseConstants.STATUS_REQUEST_DROP_TL);
                    break;
                case ProspectPersonConstants.DROP_PROSPECT_BUILD_SUSPECT:
                    suspectPersonSvc.buildSuspectOtherWhenProspectDrop(r,param);
                    r = changeProspectPersonStatus(r,BaseConstants.STATUS_COMPLETED_DROP);
                    break;
                case ProspectPersonConstants.SEARCH_EXISTING_PROSPECT:
                    r = findProspectPersonWhenExist(r.getPerson(), param);
                    break;
                case ProspectPersonConstants.KORSAL_REQUEST_DROP_PROSPECT:
                    r = changeProspectPersonStatus(r,BaseConstants.STATUS_REQUEST_DROP_KORSAL);
                    break;
                case ProspectPersonConstants.REVERT_STATUS_PROSPECT:
                    updateStatusWhenRejected(r);
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Set<ProspectPersonDTO> processExecuteListData(Integer id, String param, Set<ProspectPersonDTO> dto) {
        Set<ProspectPersonDTO> r = dto;
        if (r != null) {
            switch (id) {
                case ProspectPersonConstants.ASSIGN_SALESMAN_PROSPECT_BULK:
                    for(ProspectPersonDTO prospectPersonDTO : r) {
                        processExecuteData(ProspectPersonConstants.CHANGE_PROSPECT_SALESMAN, param, prospectPersonDTO);
                    }
                    break;
                case ProspectPersonConstants.DROP_PROSPECT_BULK:
                    for(ProspectPersonDTO prospectPersonDTO : r) {
                        processExecuteData(ProspectPersonConstants.DROP_PROSPECT_BUILD_SUSPECT, param, prospectPersonDTO);
                    }
                    break;
                case ProspectPersonConstants.REQUEST_DROP_PROSPECT_BULK:
                    for(ProspectPersonDTO prospectPersonDTO : r) {
                        processExecuteData(ProspectPersonConstants.KORSAL_REQUEST_DROP_PROSPECT, null, prospectPersonDTO);
                    }
                    break;
                case ProspectPersonConstants.REJECT_DROP_PROSPECT_BULK:
                    for(ProspectPersonDTO prospectPersonDTO : r) {
                        processExecuteData(ProspectPersonConstants.REVERT_STATUS_PROSPECT, null, prospectPersonDTO);
                    }
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public ProspectPersonDTO changeProspectPersonStatus(ProspectPersonDTO dto, Integer id) {
        if (dto != null) {
			ProspectPerson e = prospectPersonRepository.findOne(dto.getIdProspect());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        prospectPersonSearchRepository.delete(dto.getIdProspect());
                        break;
                    default:
                        prospectPersonSearchRepository.save(dto);
                }
				prospectPersonRepository.save(e);
			}
		}
        return dto;
    }

    @Transactional
    public void buildProspectPerson (PersonDTO pp) {
        Person p = personSrv.getWhenExists(pp);
        Boolean isNew = p == null;

        if (isNew) {
            ProspectPersonDTO pr = new ProspectPersonDTO();
            pr.setPerson(pp);
            save(pr);
        }
    }

    @Transactional
    public void buildPersonalCustomerSURBaseOnStatus(Internal internal, CommunicationEventProspectDTO ce){
        ProspectPerson p = prospectPersonRepository.findOne(ce.getIdProspect());
        PersonalCustomer pc = new PersonalCustomer();
        pc.setPerson(p.getPerson());
        pc = customerUtils.buildPersonalCustomer(internal, pc);
        for(int i=0; i<ce.getQty(); i++){
            surSvc.buildOneFromProspectPerson(p, pc);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updateStatusProspectBasedOnPurchasePlan(CommunicationEventProspectDTO ce){
        ProspectPerson p = prospectPersonRepository.findOne(ce.getIdProspect());
        ProspectPersonDTO pDTO = prospectPersonMapper.toDto(p);
        // buildPersonalCustomerSURBaseOnStatus(pDTO);

        if (ce.getPurchasePlan().contains("< 2 Minggu")){
            changeProspectPersonStatus(pDTO,BaseConstants.STATUS_HOT);
        }
        else if (ce.getPurchasePlan().contains("< 1 Bulan") &&
                !pDTO.getCurrentStatus().equals(BaseConstants.STATUS_HOT)){
            changeProspectPersonStatus(pDTO,BaseConstants.STATUS_MEDIUM);
        }
        else if (ce.getPurchasePlan().contains("> 1 Bulan") &&
                !pDTO.getCurrentStatus().equals(BaseConstants.STATUS_HOT) &&
                !pDTO.getCurrentStatus().equals(BaseConstants.STATUS_MEDIUM)){
            changeProspectPersonStatus(pDTO,BaseConstants.STATUS_LOW);
        }
    }

    /* =============================== K O R S A L - S E R V I C E =============================== */

    /**
     *  Get all the prospectPeople by Korsal.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProspectPersonDTO> findAllByKorsal(Pageable pageable, Integer idstatustype, String idInternal) {
        log.debug("Request to get all ProspectPeople");
        Page<ProspectPersonDTO> prospectPersonDTOS = null;

        if (idstatustype != null){
            prospectPersonDTOS =  prospectPersonRepository.findProspectPersonByStatusType(
                pageable, idstatustype, null, idInternal).map(prospectPersonMapper::toDto);
        }
        else {
            UUID idSalesCoordinator = partyUtils.getCurrentSalesman() == null ?
                null : partyUtils.getCurrentSalesman().getIdPartyRole();

            prospectPersonDTOS =   prospectPersonRepository.findActiveProspectPersonByKorsal(
                pageable, idSalesCoordinator, idInternal).map(prospectPersonMapper::toDto);

        }
        return prospectPersonDTOS;
    }

    @Transactional(readOnly = true)
    public Page<ProspectPersonDTO> findAllByFilterKorsal(Pageable pageable, ProspectPTO params, String idInternal) {
        log.debug("Request to get all ProspectPeople Filters");
        return prospectPersonRepository.findByFilterKorsal(
            pageable, params, partyUtils.getCurrentSalesman().getIdPartyRole(), idInternal)
            .map(prospectPersonMapper::toDto);
    }

    /* =============================== T L - S E R V I C E =============================== */

    @Transactional(readOnly = true)
    public Page<ProspectPersonDTO> findAllByTeamLeader(Pageable pageable, Integer idstatustype, String idInternal) {
        log.debug("Request to get all ProspectPeople");
        Page<ProspectPersonDTO> prospectPersonDTOS = null;

        if (idstatustype != null){
            prospectPersonDTOS =  prospectPersonRepository.findProspectPersonByStatusType(
                pageable,idstatustype, null, idInternal).map(prospectPersonMapper::toDto);
        }
        else {
            prospectPersonDTOS =   prospectPersonRepository.findActiveProspectPersonByTeamLeader(
                pageable, partyUtils.getCurrentSalesman(), idInternal).map(prospectPersonMapper::toDto);

        }
        return prospectPersonDTOS;
    }

    public void OnlySalesmanAllow(){
        Boolean notAllowed = partyUtils.getCurrentSalesman() == null || partyUtils.getCurrentSalesman().isCoordinator() == true;

        if (notAllowed){
            throw new DmsException("Hanya Sales Yang Boleh Akses");
        }
    }

    @Transactional
    protected ProspectPerson validationProspect(ProspectPersonDTO prospectPersonDTO){
        Person p = null;

        ProspectPerson prospectPerson = prospectPersonMapper.toEntity(prospectPersonDTO);

        //Validation On New Data Prospect
        if (prospectPerson.getIdProspect() == null ){

            log.debug("SYSTEM VALIDATION NEW DATA PROSPECT!!!");

//            if (prospectPerson.getPerson().getIdParty() == null ) {
//                log.debug("ID PARTY NULL DETECTED!!!");
//                p = partyUtils.findPersonByPhone(prospectPerson.getPerson());
//            } else

                if ( prospectPerson.getPerson().getIdParty() != null) {
                log.debug("ID PARTY IS NOT NULL!!!");
                Person tempPerson = personMapper.toEntity(prospectPersonDTO.getPerson());
                p = personRepo.findOne(prospectPerson.getPerson().getIdParty());
                prospectPersonSearchRepository.save(prospectPersonDTO);

                //TODO: IF U HAVE BETTER SOLUTION PLEASE CHANGE THIS METHOD
                log.debug("HOW TO SOLVE DETACH ENTITY PERSIST WHILE UPDATING DATA WITH COMPLEX REQUIREMENT");
                p.setWorkType(tempPerson.getWorkType());
                p.setReligionType(tempPerson.getReligionType());
                p.setFirstName( tempPerson.getFirstName() );
                p.setLastName( tempPerson.getLastName() );
                p.setPob( tempPerson.getPob() );
                p.setBloodType( tempPerson.getBloodType() );
                p.setGender( tempPerson.getGender() );
                p.setDob( tempPerson.getDob() );
                p.setPersonalIdNumber( tempPerson.getPersonalIdNumber() );
                p.setFamilyIdNumber( tempPerson.getFamilyIdNumber() );
                p.setTaxIdNumber( tempPerson.getTaxIdNumber() );
                p.setCellPhone1( tempPerson.getCellPhone1() );
                p.setCellPhone2( tempPerson.getCellPhone2() );
                p.setPhone( tempPerson.getPhone() );
                p.setPrivateMail( tempPerson.getPrivateMail() );

//                prospectPerson.setCellphone(Integer.valueOf(prospectPersonDTO.getPerson().getCellPhone1()));
//                prospectPerson.setlName(prospectPersonDTO.getPerson().getLastName());
//                prospectPerson.setfName(prospectPersonDTO.getPerson().getFirstName());

                PostalAddress postadd = postalAddressRepo.findOne(tempPerson.getPostalAddress().getIdContact());
                postadd.setProvince(tempPerson.getPostalAddress().getProvince());
                postadd.setVillage(tempPerson.getPostalAddress().getVillage());
                postadd.setCity(tempPerson.getPostalAddress().getCity());
                postadd.setDistrict(tempPerson.getPostalAddress().getDistrict());
                postadd.setAddress1(tempPerson.getPostalAddress().getAddress1());
                postadd.setAddress2(tempPerson.getPostalAddress().getAddress2());

                p.setPostalAddress(postadd);
            }

            if (p != null) {
                prospectPerson.setPerson(p);
            }

            if (partyUtils.getCurrentSalesman() != null && partyUtils.getCurrentSalesman().isCoordinator() == false){
                prospectPerson.setSalesman(partyUtils.getCurrentSalesman());
                prospectPerson.setIdSalesCoordinator(partyUtils.getCurrentSalesman().getCoordinatorSales().getIdPartyRole());
            }
            log.debug("...VALIDATION DONE");
        }

        return prospectPerson;
    }

    @Transactional
    public ProspectPersonDTO buildData(ProspectPerson p) {
        ProspectPerson prospectPerson = prospectPersonRepository.save(p);
        ProspectPersonDTO result = prospectPersonMapper.toDto(prospectPerson);
        prospectPersonSearchRepository.save(result);
        return result;
    }


    public void cronUpdate() {
        IAtomicLong v = hz.getAtomicLong("prospect-cron-reindexer-"+ LocalDate.now().toString());
        // Bila belum di reindex
        if (v.get() == 0) {
            v.set(1);
            List<ProspectPerson> l = prospectPersonRepository.findAllEager();
            for (ProspectPerson pp : l) {
                ProspectPersonDTO pDTO = prospectPersonMapper.toDto(pp);
                producerTemplate.sendBody(IndexerRoute.INDEXER_PROSPECT, pDTO);
                System.out.println("Append " + pDTO.toString());
            }
        }
    }

    @Transactional
    public void reindexer(@Body ProspectPersonDTO pDto) {
        prospectPersonSearchRepository.save(pDto);
        System.out.println("Data " + pDto.toString());
    }



    //TODO: REMARK JIKA INGIN COBA ELASTIC DI LOCAL
//    @Transactional
//    @PostConstruct
//    public void initForMe() {
//        List<ProspectPerson> l = prospectPersonRepository.findAllEager();
//        for (ProspectPerson pp : l) {
//            ProspectPersonDTO pDto = prospectPersonMapper.toDto(pp);
//            prospectPersonSearchRepository.save(pDto);
//            System.out.println("Data " + pDto.toString());
//        }
//    }
}
