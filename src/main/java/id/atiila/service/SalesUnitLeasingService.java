package id.atiila.service;

import id.atiila.domain.SalesUnitLeasing;
import id.atiila.repository.SalesUnitLeasingRepository;
import id.atiila.repository.search.SalesUnitLeasingSearchRepository;
import id.atiila.service.dto.SalesUnitLeasingDTO;
import id.atiila.service.mapper.SalesUnitLeasingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing SalesUnitLeasing.
 * BeSmart Team
 */

@Service
@Transactional
public class SalesUnitLeasingService {

    private final Logger log = LoggerFactory.getLogger(SalesUnitLeasingService.class);

    private final SalesUnitLeasingRepository salesUnitLeasingRepository;

    private final SalesUnitLeasingMapper salesUnitLeasingMapper;

    private final SalesUnitLeasingSearchRepository salesUnitLeasingSearchRepository;

    public SalesUnitLeasingService(SalesUnitLeasingRepository salesUnitLeasingRepository, SalesUnitLeasingMapper salesUnitLeasingMapper, SalesUnitLeasingSearchRepository salesUnitLeasingSearchRepository) {
        this.salesUnitLeasingRepository = salesUnitLeasingRepository;
        this.salesUnitLeasingMapper = salesUnitLeasingMapper;
        this.salesUnitLeasingSearchRepository = salesUnitLeasingSearchRepository;
    }

    /**
     * Save a salesUnitLeasing.
     *
     * @param salesUnitLeasingDTO the entity to save
     * @return the persisted entity
     */
    public SalesUnitLeasingDTO save(SalesUnitLeasingDTO salesUnitLeasingDTO) {
        log.debug("Request to save SalesUnitLeasing : {}", salesUnitLeasingDTO);
        SalesUnitLeasing salesUnitLeasing = salesUnitLeasingMapper.toEntity(salesUnitLeasingDTO);
        salesUnitLeasing = salesUnitLeasingRepository.save(salesUnitLeasing);
        SalesUnitLeasingDTO result = salesUnitLeasingMapper.toDto(salesUnitLeasing);
        salesUnitLeasingSearchRepository.save(salesUnitLeasing);
        return result;
    }

    /**
     *  Get all the salesUnitLeasings.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesUnitLeasingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SalesUnitLeasings");
        return salesUnitLeasingRepository.findAll(pageable)
            .map(salesUnitLeasingMapper::toDto);
    }

    /**
     *  Get one salesUnitLeasing by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SalesUnitLeasingDTO findOne(UUID id) {
        log.debug("Request to get SalesUnitLeasing : {}", id);
        SalesUnitLeasing salesUnitLeasing = salesUnitLeasingRepository.findOne(id);
        return salesUnitLeasingMapper.toDto(salesUnitLeasing);
    }

    /**
     *  Delete the  salesUnitLeasing by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete SalesUnitLeasing : {}", id);
        salesUnitLeasingRepository.delete(id);
        salesUnitLeasingSearchRepository.delete(id);
    }

    /**
     * Search for the salesUnitLeasing corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesUnitLeasingDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SalesUnitLeasings for query {}", query);
        Page<SalesUnitLeasing> result = salesUnitLeasingSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(salesUnitLeasingMapper::toDto);
    }

    public SalesUnitLeasingDTO processExecuteData(Integer id, String param, SalesUnitLeasingDTO dto) {
        SalesUnitLeasingDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<SalesUnitLeasingDTO> processExecuteListData(Integer id, String param, Set<SalesUnitLeasingDTO> dto) {
        Set<SalesUnitLeasingDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
