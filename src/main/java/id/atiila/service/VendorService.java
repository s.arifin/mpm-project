package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.base.XlsxUtils;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.VendorSearchRepository;
import id.atiila.service.dto.*;
import id.atiila.service.mapper.OrganizationMapper;
import id.atiila.service.mapper.VendorMapper;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.*;

/**
 * Service Implementation for managing Vendor.
 * BeSmart Team
 */

@DependsOn("organizationService")
@Service
@Transactional
public class VendorService {

    private final Logger log = LoggerFactory.getLogger(VendorService.class);

    private final VendorRepository vendorRepository;

    private final VendorMapper vendorMapper;

    private final VendorSearchRepository vendorSearchRepository;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private VendorUtils vendorUtils;

    @Autowired
    private OrganizationMapper organizationMapper;

    public VendorService(VendorRepository vendorRepository, VendorMapper vendorMapper, VendorSearchRepository vendorSearchRepository) {
        this.vendorRepository = vendorRepository;
        this.vendorMapper = vendorMapper;
        this.vendorSearchRepository = vendorSearchRepository;
    }

    /**
     * Save a vendor.
     *
     * @param dto the entity to save
     * @return the persisted entity
     */
    public VendorDTO save(VendorDTO dto) {
        log.debug("Request to save Vendor : {}", dto);
        Vendor vendor = vendorMapper.toEntity(dto);
        Boolean isNewData = vendor.getIdVendor() == null;

        if (isNewData) {
            Organization organization = partyUtils.buildOrganization(organizationMapper.toEntity(dto.getOrganization()));
            vendor = vendorUtils.buildVendor(dto.getIdVendor(), organization);
        }

        vendor = vendorRepository.save(vendor);
        VendorDTO result = vendorMapper.toDto(vendor);
        vendorSearchRepository.save(vendor);
        return result;
    }

    /**
     * Get all the vendors.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VendorDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Vendors");
        return vendorRepository.findAll(pageable)
            .map(vendorMapper::toDto);
    }
    @Transactional(readOnly = true)
    public Page<VendorDTO> findAllBiroJasa(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all Vendors");
        String idInternal = request.getParameter("idInternal");
        return vendorRepository.queryFindAllBiroJasa(idInternal, pageable)
            .map(vendorMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<VendorDTO> findAllMainDealer(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all Vendors");
        String idInternal = request.getParameter("idInternal");
        return vendorRepository.queryFindAllMainDealer(idInternal, pageable)
            .map(vendorMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<VendorDTO> findByRoleTypeAll(Integer roleType, Pageable pageable) {
        log.debug("Request to get Vendors role type = [] ",roleType);
        return vendorRepository.findByIdRoleType(roleType, pageable)
            .map(vendorMapper::toDto);
    }

    /**
     * Get one vendor by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public VendorDTO findOne(String id) {
        log.debug("Request to get Vendor : {}", id);
        Vendor vendor = vendorRepository.findOne(id);
        return vendorMapper.toDto(vendor);
    }

    /**
     * Delete the vendor by id.
     *
     * @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Vendor : {}", id);
        vendorRepository.delete(id);
        vendorSearchRepository.delete(id);
    }

    /**
     * Search for the vendor corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VendorDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of Vendors for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idOrganization = request.getParameter("idOrganization");

        if (idOrganization != null) {
            q.withQuery(matchQuery("organization.idParty", idOrganization));
        }

        Page<Vendor> result = vendorSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(vendorMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<VendorDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered VendorDTO");
        String idOrganization = request.getParameter("idOrganization");
        return vendorRepository.queryNothing(pageable).map(vendorMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, VendorDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<VendorDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    public VendorDTO processExecuteData(VendorDTO dto, Integer id) {
        VendorDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
        }
        return r;
    }

    @Transactional
    public void migrateVendor(){
        try {
            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/vendor.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/vendor.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<CustomVendorDTO> vendors = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (CustomVendorDTO o: vendors) {
                    Integer role = BaseConstants.ROLE_VENDOR;
                    if (o.getVendorName() != null && o.getVendorName().length() > 30) o.setVendorName(o.getVendorName().substring(0, 29));
                    if (o.getVendorClassName() != null) role = Integer.parseInt(o.getVendorClassName().substring(0, 2));
                    vendorUtils.buildVendor(o.getVendorCode(), o.getVendorName(), role);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Transactional
    public void initialize() {
        migrateVendor();
    }

}
