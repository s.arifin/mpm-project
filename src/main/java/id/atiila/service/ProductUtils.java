package id.atiila.service;

import id.atiila.domain.*;
import id.atiila.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@Service
@Transactional(readOnly = true)
public class ProductUtils {

    private static PriceUtils priceUtils;

    private final Logger log = LoggerFactory.getLogger(ProductUtils.class);

    @Autowired
    protected ProductRepository productRepository;

    @Autowired
    private MotorRepository motorRepo;

    @Autowired
    private RemPartRepository remPartRepository;

    @Autowired
    private UomRepository uomRepo;

    @Autowired
    private PriceComponentRepository priceRepo;

    @Autowired
    private FeatureRepository featureRepository;

    @Autowired
    private FeatureApplicableRepository featureApplicableRepository;

    @Autowired
    private FeatureTypeRepository featureTypeRepository;

    public ProductUtils(PriceUtils pr) {
        this.priceUtils = pr;
    }

    public Motor buildMotor(String idProduct, String name, String description, String uom) {
        Motor m = motorRepo.findOne(idProduct);
        if (m == null) {
            m = new Motor();
            m.setName(name != null && name.length() > 30 ? name.substring(0, 29) : name);
            m.setDescription(description);
            m.setIdProduct(idProduct);
            m.setUom(uomRepo.findOne(uom));
            m.setDateIntroduction(ZonedDateTime.now());
            m.setSerialized(true);
            m = motorRepo.save(m);
        }
        return m;
    }

    public RemPart buildRemPart(String idProduct, String name, String uom) {
        RemPart m = remPartRepository.findOne(idProduct);
        if (m == null) {
            m = new RemPart();
            m.setName(name);
            m.setDescription(name);
            m.setIdProduct(idProduct);
            m.setUom(uomRepo.findOne(uom));
            m.setSerialized(false);
            m.setDateIntroduction(ZonedDateTime.now());
            m = remPartRepository.save(m);
        }
        return m;
    }

    public Product getProduct(String id) {
        return productRepository.findOne(id);
    }

    public PriceComponent setProductPrice(Integer type, Product p, Party seller, BigDecimal price) {
        return priceUtils.addPrice(seller, p, type, price, ZonedDateTime.now());
    }

    public PriceComponent setDiscount(Integer type, Product p, Party seller, float percent) {
        return priceUtils.addPercent(seller, p, type, percent, ZonedDateTime.now());
    }

    public List<PriceComponent> getPriceBy(Product p, Party seller, ZonedDateTime dateWhen) {
        return priceRepo.getPriceBase(p, seller, dateWhen);
    }

    public List<PriceComponent> getPriceByProductAndInternal(String idproduct, UUID idinternal){
        return priceRepo.getPriceBaseByProductAndInternal(idinternal, idproduct);
    }

    public List<PriceComponent> getPriceByProductAndInternal(Internal intr, String idproduct){
        return priceRepo.getAllPriceBaseByProductAndInternal(intr.getIdInternal(), idproduct);
    }

    public List<PriceComponent> getPriceByProductAndVendor(String idproduct, UUID idvendor, ZonedDateTime datewhen){
        return priceRepo.getPriceBaseByProductAndVendor(idproduct, idvendor, datewhen);
    }

    public List<PriceComponent> getAllPriceByProductAndInternal(String idproduct, UUID idinternal){
        return priceRepo.getAllPriceBaseByProductAndInternal(idproduct, idinternal);
    }

    public List<PriceComponent> getAllPriceByProductAndVendor(String idproduct, UUID idvendor){
        return priceRepo.getAllPriceBaseByProductAndVendor(idproduct, idvendor);
    }

    public Product findOne(String id) {
        return productRepository.findOne(id);
    }

    public Feature getFeatureByKey(Integer featureType, String key) {
        Feature feat = featureRepository.findByKey(featureType, key);
        return feat;
    }

    public Feature getFeatureById(Integer id) {
        Feature feat = featureRepository.findOne(id);
        return feat;
    }

    public void buildProductFeatureApplicable(Product r, Integer ...featureTypes) {
        for (Integer c: featureTypes) {
            FeatureType p = getFeatureType(c, "Reserved");
            List<Feature> fa = featureRepository.queryByFeatureType(p);
            for (Feature f: fa) {
                FeatureApplicable fapp = featureApplicableRepository.queryByProductAndFeature(r, f);
                if (fapp == null) {
                    fapp = new FeatureApplicable();
                    fapp.setProduct(r);
                    fapp.setFeature(f);
                    fapp.setFeatureType(f.getFeatureType());
                    fapp.setBoolValue(false);
                    featureApplicableRepository.save(fapp);
                }
            }
        }
    }

    public FeatureType getFeatureType(Integer c, String description) {
        FeatureType p = featureTypeRepository.findOne(c);
        if (p == null) {
            p = new FeatureType();
            p.setIdFeatureType(c);
            p.setDescription(description);
            p = featureTypeRepository.save(p);
        }
        return p;
    }

    public boolean hasFeatured(Product p, Integer featureType, String refKey) {
        boolean r = false;
        FeatureApplicable fa = featureApplicableRepository.queryByProductIdFeature(p, featureType, refKey);

        if (fa == null) {
            Feature f = getFeature(featureType, refKey, "Reserved");
            fa = new FeatureApplicable();
            fa.setProduct(p);
            fa.setFeature(f);
            fa.setFeatureType(f.getFeatureType());
            fa.setBoolValue(false);
            fa = featureApplicableRepository.save(fa);
        } else r = fa.isBoolValue();

        return r;
    }

    public void enableFeatured(Product p, Integer featureType, String refKey) {
        FeatureApplicable fa = featureApplicableRepository.queryByProductIdFeature(p, featureType, refKey);

        if (fa == null) {
            Feature f = getFeature(featureType, refKey, "Reserved");
            fa = new FeatureApplicable();
            fa.setProduct(p);
            fa.setFeature(f);
            fa.setFeatureType(f.getFeatureType());
            fa.setBoolValue(true);
            fa = featureApplicableRepository.save(fa);
        } else {
            if (!fa.isBoolValue()) {
                fa.setBoolValue(true);
                fa = featureApplicableRepository.save(fa);
            };
        }
    }

    public void enableFeatured(Product p, Feature feature) {
        FeatureApplicable fa = featureApplicableRepository.queryByProductAndFeature(p, feature);

        if (fa == null) {
            fa = new FeatureApplicable();
            fa.setProduct(p);
            fa.setFeature(feature);
            fa.setFeatureType(feature.getFeatureType());
            fa.setBoolValue(true);
            fa = featureApplicableRepository.save(fa);
        } else {
            if (!fa.isBoolValue()) {
                fa.setBoolValue(true);
                fa = featureApplicableRepository.save(fa);
            };
        }
    }

    public Feature getFeature(Integer featureType, String refKey, String description) {
        Feature f = featureRepository.queryByRefKey(featureType, refKey);
        if (f == null) {
            f = new Feature();
            f.setFeatureType(getFeatureType(featureType, "Reserved"));
            f.setRefKey(refKey);
            f.setDescription(description);
            f = featureRepository.save(f);
        }
        return f;
    }

    public void buildInitMotor() {
        buildMotor("ELZ", "NEW CBR 250RR ABS ACCS", "R5F04R24L0CZ M/T", "UNT");
        buildMotor("ELV", "NEW CBR 250RR ABS ACCS LP", "R5F04R24L0DV M/T", "UNT");
        buildMotor("GHQ", "BLADE 125 FI R RE LP", "AFP12W22C08AQ M/T", "UNT");
        buildMotor("HR0", "SH 150", "SH150ADH IN A/T", "UNT");
        buildMotor("HMK", "NEW BEAT ESP CW PLUS", "D1B02N26S1 A/T Plus", "UNT");
        buildMotor("HD4", "SONIC 150R", "Y3B02R17S2 M/T", "UNT");
        buildMotor("HDS", "SONIC 150R LP", "Y3B02R17S2S M/T", "UNT");
        buildMotor("HDG", "SONIC 150R HRR", "Y3B02R17S2A M/T", "UNT");
        buildMotor("HDH", "SONIC 150R MATTE BLACK", "Y3B02R17S2B M/T", "UNT");
        buildMotor("HDU", "SONIC 150R MATTE BLACK LP", "Y3B02R17S2BU M/T", "UNT");
        buildMotor("HNK", "BEAT STREET CBS PLUS", "D1I02N27S A/T PLUS", "UNT");
        buildMotor("Z68", "BEAT POP CBS ACC", "YG02NLAA A/T", "UNT");
        buildMotor("DN2", "GL15B1DF2 M/T", "GL15B1DF2 M/T", "UNT");
        buildMotor("DM2", "VERZA CAST WHEEL", "GL15B1CF2 M/T", "UNT");
        buildMotor("DV0", "ALL NEW CB150R STREETFIRE", "H5C02R20M1 M/T", "UNT");
        buildMotor("HE0", "New Vario eSP CBS", "D1A02N18M1 A/T", "UNT");
        buildMotor("HF0", "New Vario eSP CBS ISS", "D1A02N19M1 A/T", "UNT");
        buildMotor("HEA", "New Vario eSP CBS Adv", "D1A02N18M1A A/T", "UNT");
        buildMotor("HFA", "New Vario eSP CBS ISS Adv", "D1A02N19M1A A/T", "UNT");
        buildMotor("Z60", "NEW VARIO 110 ESP CBS ISS ACC", "D1A2N19A", "UNT");
        buildMotor("HDA", "ALL NEW SONIC 150 R REPSOL", "Y3B02R17L0A M/T", "UNT");
        buildMotor("GZQ", "K1H02N14L0M P", "K1H02N14L0M P", "UNT");
        buildMotor("HCJ", "C1C02N16M2 A/T CHROME KREM", "C1C02N16M2 A/T CHROME KREM", "UNT");
        buildMotor("DVP", "ALL NEW CB150R STREETFIRE PLUS", "H5C02R20S1P M/T", "UNT");
        buildMotor("Z59", "NEW VARIO 110 ESP CBS ISS MONOTON", "D1A2N9AA", "UNT");
        buildMotor("Z61", "NEW VERZA ACC", "GL15BF2A", "UNT");
        buildMotor("Z51", "ALL NEW CBR 150 R ACC", "P5E02M1J M/T", "UNT");
        buildMotor("Z50", "ALL NEW CBR 150 R ACC", "P5E02MAJ M/T", "UNT");
        buildMotor("DYQ", "ALL NEW CB 150 R STREETFIRE SPESIAL EDITION PLUS", "H5C02R20S1BQ M/T", "UNT");
        buildMotor("DYL", "ALL NEW CB 150 R STREETFIRE SPESIAL EDITION PLUS", "H5C02R0M1A M/T SINGLE SEA", "UNT");
        buildMotor("HMJ", "New BeAT esp CW PLUS", "D1B02N26L2 A/T PLUS", "UNT");
        buildMotor("HKJ", "New BeAT esp CBS PLUS", "D1B02N12L2 A/T PLUS", "UNT");
        buildMotor("HLJ", "New BeAT esp CBS ISS PLUS", "D1B02N13L2 A/T PLUS", "UNT");
        buildMotor("HEJ", "NEW VARIO ESP CBS PLUS", "D1A02N18M1 A/T PLUS", "UNT");
        buildMotor("HEK", "NEW VARIO ESP CBS ADVANCE PLUS", "D1A02N18M1A A/T PLUS", "UNT");
        buildMotor("HFJ", "NEW VARIO ESP CBS ISS PLUS", "D1A02N19M1 A/T PLUS", "UNT");
        buildMotor("HFK", "NEW VARIO ESP CBS ISS ADVANCE PLUS", "D1A02N19M1A A/T PLUS", "UNT");
        buildMotor("HDN", "New Sonic 150R Special Plus", "Y3B02R17S1B M/T Plus", "UNT");
        buildMotor("DY9", "ALL NEW CB150R STREETFIRE SE ACCS", "H5C02R20M1A9 M/T", "UNT");
        buildMotor("HDT", "New Sonic 150 R Repsol Plus", "Y3B02R17S1AP M/T", "UNT");
        buildMotor("DYB", "NEW CB150R STREETFIRE", "H5C02R20S1B M/T", "UNT");
        buildMotor("DY1", "NEW CB150R STREETFIRE SE", "H5C02R20S1A M/T", "UNT");
        buildMotor("DV2", "NEW CB150R STREETFIRE", "H5C02R20S1 M/T", "UNT");
        buildMotor("Z48", "CB150R BUNDLING JAKET", "H5C2R25J", "UNT");
        buildMotor("Z47", "CB150R BUNDLING JAKET", "H5C2R2JA", "UNT");
        buildMotor("Z46", "CB150R SPECIAL EDITION", "H5C2R2AJ", "UNT");
        buildMotor("HJQ", "NEW SUPRA GTR 150 EXCLUSIVE PLUS", "G2E02R21L0APM/T", "UNT");
        buildMotor("DYR", "ALL NEW CB 150 R STREETFIRE SPECIAL EDITION PLUS", "H5C02R20S1AP M/T", "UNT");
        buildMotor("HN0", "BEAT STREET CBS", "D1I02N27M1 A/T", "UNT");
        buildMotor("DYX", "NEW CB150R STREETFIRE ACC", "H5C02R20S1BX M/T", "UNT");
        buildMotor("DVS", "CB150R STREETFIRE SC PLUS", "H5C02R0S1 M/T PLUS", "UNT");
        buildMotor("DY8", "NEW CB150R STREETFIRE ACC", "H5C02R20S1A8 M/T", "UNT");
        buildMotor("EK0", "NEW CBR 250RR STD", "R5F04R25L0 M/T", "UNT");
        buildMotor("DV8", "NEW CB150R STREETFIRE ACC", "H5C02R20S18 M/T", "UNT");
        buildMotor("DVQ", "ALL NEW CB 150 R STREETFIRE PLUS", "H5C02R20S2Q M/T", "UNT");
        buildMotor("DYS", "ALL NEW CB 150 R STREETFIRE SPECIAL EDITION PLUS", "H5C02R20S1BP M/T", "UNT");
        buildMotor("HE1", "VARIO ESP CBS MMC", "D1A02N18S1 A/T", "UNT");
        buildMotor("HEB", "VARIO ESP CBS ADV MMC", "D1A02N18S1A A/T", "UNT");
        buildMotor("HF1", "VARIO ESP CBS ISS MMC", "D1A02N19S1 A/T", "UNT");
        buildMotor("HFB", "VARIO ESP CBS ISS ADV MMC", "D1A02N19S1A A/T", "UNT");
        buildMotor("HNJ", "BEAT STREET CBS PLUS", "D1I02N27M A/T", "UNT");
        buildMotor("GZ6", "VARIO 150 PLUS BAN FDR STICKER", "K1H02N14S16 A/T", "UNT");
        buildMotor("Z39", "BEAT CBS ISS ACC", "D1B2N3LA", "UNT");
        buildMotor("HH2", "PCX 150", "WW150EXJ 2IN A/T", "UNT");
        buildMotor("GE2", "NEW SUPRA X 125 SPOKE FI MMC", "AFX12U23C07", "UNT");
        buildMotor("EE0", "CRF250RLH IN M/T", "CRF250RALLY", "UNT");
        buildMotor("GF2", "SUPRA X 125 CW MMC", "AFX12U23C08 M/T", "UNT");
        buildMotor("HPR", "ALL NEW SCOOPY PLAYFUL LP", "F1C02N28L0CR A/T", "UNT");
        buildMotor("HPA", "ALL NEW SCOOPY SPORTY", "F1C02N28L0B A/T", "UNT");
        buildMotor("HPB", "ALL NEW SCOOPY PLAYFUL", "F1C02N28L0C A/T", "UNT");
        buildMotor("HPQ", "ALL NEW SCOOPY SPORTY LP", "F1C02N28L0BQ A/T", "UNT");
        buildMotor("HP0", "ALL NEW SCOOPY STYLISH", "F1C02N28L0 A/T", "UNT");
        buildMotor("HPP", "ALL NEW SCOOPY STYLISH LP", "F1C02N28L0AP A/T", "UNT");
        buildMotor("HB9", "Vario 125 CBS ISS MMC  LP", "E1F02N12S1 LP A/T", "UNT");
        buildMotor("HA9", "Vario 125 CBS MMC LP", "E1F02N11S1 LP A/T", "UNT");
        buildMotor("HDQ", "SONIC 150R REPSOL LP", "Y3B02R17S1AQ M/T", "UNT");
        buildMotor("HA3", "NEW VARIO 125 CBS", "E1F02N11S2 A/T", "UNT");
        buildMotor("GZ3", "NEW VARIO 150", "K1H02N14S2 A/T", "UNT");
        buildMotor("ED3", "NEW CBR 150R", "P5E02R22S1 M/T", "UNT");
        buildMotor("HAL", "NEW VARIO 125 CBS PLUS", "E1F02N11S A/T PLUS", "UNT");
        buildMotor("GZH", "VARIO 150 PLUS", "K1H02N14S2 A/T CRM PLUS", "UNT");
        buildMotor("GZS", "VARIO 150 MMC PLUS", "K1H02N14S2 A/T GLD PLUS", "UNT");
        buildMotor("HBL", "VARIO 125 CBS ISS MMC PLUS", "E1F02N12S2 A/T PLUS", "UNT");
        buildMotor("EDY", "ALL NEW CBR150R STANDAR PLUS PREMIUM", "P5E02R22S1Q M/T", "UNT");
        buildMotor("HCL", "C1C02N16M2 A/T PLUS", "C1C02N16M2 A/T PLUS", "UNT");
        buildMotor("HCM", "C1C02N16M2S A/T PLUS", "C1C02N16M2S A/T PLUS", "UNT");
        buildMotor("HCR", "C01C02N16M2S A/T  PLUS", "C01C02N16M2S A/T PLUS", "UNT");
        buildMotor("HDK", "NULL", "Y3B02R17L0A M/T", "UNT");
        buildMotor("GH1", "BLADE R 125 FI", "AFP12W22C08 M/T", "UNT");
        buildMotor("GHB", "BLADE REPSOL 125 FI", "AFP12W22C08A M/T", "UNT");
        buildMotor("GE1", "SUPRA X 125 SPOKE FI", "AFX 12U22C07 M/T", "UNT");
        buildMotor("GF1", "SUPRA X 125 CW FI", "AFX12U22C08 M/T", "UNT");
        buildMotor("GFA", "SUPRA X 125 CW FI PLUS", "AFX 12U22C08A M/T", "UNT");
        buildMotor("GW8", "Beat Pop CW Accs", "Y1G02N02L0BZ A/T", "UNT");
        buildMotor("GY8", "Beat Pop CBS ISS Accs", "Y1G02N15L0B A/T", "UNT");
        buildMotor("GX8", "Beat Pop CBS Accs", "Y1G02N02L0AB A/T", "UNT");
        buildMotor("DVJ", "NEW CB150R STREETFIRE  PLUS (WL)", "H5C02R20M1 M/T PLUS", "UNT");
        buildMotor("DVK", "NEW CB150R STREETFIRE PLUS (BS)", "H5C02R20M1 M/T PLUS", "UNT");
        buildMotor("DVL", "NEW CB150R STREETFIRE PLUS (RS)", "H5C02R20M1 M/T PLUS", "UNT");
        buildMotor("DVM", "NEW CB150R STREETFIRE PLUS (RD)", "H5C02R20M1 M/T PLUS", "UNT");
        buildMotor("HCK", "NULL", "C1C02N16M2S A/T CHROME", "UNT");
        buildMotor("HBP", "ALL NEW VARIO 125 FI ISS", "E1F02N12S2P A/T", "UNT");
        buildMotor("Z57", "SUPRA X 125 CW FI LUXURY ACC", "AFX12A8C M/T", "UNT");
        buildMotor("HAP", "NEW VARIO 125 ESP CBS FI PLUS", "E1F02N11S2P A/T", "UNT");
        buildMotor("DY0", "ALL NEW CB150R STREETFIRE SE", "H5C02R20M1A M/T", "UNT");
        buildMotor("DYA", "ALL NEW CB150R STREETFIRE SE", "H5C02R20M1B M/T", "UNT");
        buildMotor("HDP", "ALL NEW SONIC 150 PLUS", "Y3B02R17S1P", "UNT");
        buildMotor("EC0", "CB500X", "CB500XAF ED M/T", "UNT");
        buildMotor("EB0", "CB500F", "CB500FF 3ED M/T", "UNT");
        buildMotor("DW0", "CB650F", "CB650FAE ED M/T", "UNT");
        buildMotor("DX0", "CBR650F", "CBR650FAE ED M/T", "UNT");
        buildMotor("HG0", "NM4 VULTUS", "NC750JF ED M/T", "UNT");
        buildMotor("DZ0", "CBR1000RR", "CBR1000SAF ED M/T", "UNT");
        buildMotor("HDJ", "NULL", "Y3B02R17L0 M/T PLUS", "UNT");
        buildMotor("GY1", "BEAT POP ESP CBS ISS PIXEL", "Y1G02N15L1 A/T", "UNT");
        buildMotor("GYA", "BEAT POP ESP CBS ISS", "Y1G02N15L1A A/T", "UNT");
        buildMotor("GX1", "BEAT POP ESP CBS PIXEL", "Y1G02N02L1AA A/T", "UNT");
        buildMotor("GWA", "BEAT POP ESP CW COMIC", "Y1G02N02L1A A/T", "UNT");
        buildMotor("GXA", "BEAT POP ESP CBS COMIC", "Y1G02N02L1AB A/T", "UNT");
        buildMotor("GW1", "BEAT POP ESP CW PIXEL", "Y1G02N02L1 A/T", "UNT");
        buildMotor("GWY", "BEAT POP ESP CW PIXEL ACC", "Y1G02N02L1Y A/T", "UNT");
        buildMotor("GWZ", "BEAT POP ESP CW COMIC ACC", "Y1G02N02L1AZ A/T", "UNT");
        buildMotor("GXY", "BEAT POP ESP CBS PIXEL ACC", "Y1G02N02L1AAY A/T", "UNT");
        buildMotor("GXZ", "BEAT POP ESP CBS COMIC ACC", "Y1G02N02L1ABZ A/T", "UNT");
        buildMotor("GYY", "BEAT POP ESP CBS ISS PIXEL ACC", "Y1G02N15L1Y A/T", "UNT");
        buildMotor("GYZ", "BEAT POP ESP CBS ISS ACC", "Y1G02N15L1AZ A/T", "UNT");
        buildMotor("GWL", "Y1G02N02L1AZ A/T", "Y1G02N02L1AZ A/T", "UNT");
        buildMotor("GWQ", "BEAT POP CW COMIC PLUS", "Y1G02N02L1AP A/T", "UNT");
        buildMotor("GWR", "BEAT POP CW PIXEL PLUS", "Y1G02N02L1P A/T", "UNT");
        buildMotor("GXR", "BEAT POP ESP CBS PIXEL PLUS", "Y1G02N02L1AAP A/T", "UNT");
        buildMotor("GXQ", "BEAT POP ESP CBS COMIC PLUS", "Y1G02N02L1ABP A/T", "UNT");
        buildMotor("GYR", "BEAT POP ESP ISS PIXEL PLUS", "Y1G02N15L1P A/T", "UNT");
        buildMotor("GYQ", "BEAT POP ESP ISS COMIC PLUS", "Y1G02N15L1AP A/T", "UNT");
        buildMotor("GYK", "Y1G02N15L1 A/T PLUS", "Y1G02N15L1 A/T PLUS", "UNT");
        buildMotor("Z54", "CB150R BANDLING JAKET", "H5C2R2JI", "UNT");
        buildMotor("Z55", "CB150R BANDLING JAKET SE", "H5C2R2AB", "UNT");
        buildMotor("DPZ", "ALL NEW CBR150R", "ALL NEW CBR150R", "UNT");
        buildMotor("DPY", "ALL NEW CBR150R", "ALL NEW CBR150R", "UNT");
        buildMotor("DP9", "ALL NEW CBR150R", "ALL NEW CBR150R", "UNT");
        buildMotor("HA1", "New Vario 125 ESP CBS MMC", "E1F02N11S1", "UNT");
        buildMotor("HB1", "New Vario 125 ESP CBS ISS MMC", "E1F02N12S1", "UNT");
        buildMotor("HAR", "New Vario 125 Esp CBS MMC Plus", "E1F02N11S1 P A/T", "UNT");
        buildMotor("HBR", "New Vario 125 Esp CBS ISS MMC Plus", "E1F02N12S1 P A/T", "UNT");
        buildMotor("GZX", "VARIO 150 MMC ACC", "K1H02N14S1X A/T", "UNT");
        buildMotor("HH1", "PCX 150", "WW150EXH IN A/T", "UNT");
        buildMotor("GZ1", "VARIO 150 MMC", "K1H02N14S1 A/T", "UNT");
        buildMotor("GWK", "Y1G02N02L1 A/T PLUS", "Y1G02N02L1 A/T PLUS", "UNT");
        buildMotor("ED0", "ALL NEW CBR 150R STD", "P5E02R22M1 M/T", "UNT");
        buildMotor("EDA", "ALL NEW CBR 150R Red Special Edition", "P5E02R22M1A M/T", "UNT");
        buildMotor("EDB", "ALL NEW CBR 150R Repsol Special Edition", "P5E02R22M1B M/T", "UNT");
        buildMotor("HAK", "E1F02N11S1 A/T PLUS", "E1F02N11S1 A/T PLUS", "UNT");
        buildMotor("HBK", "VARIO 125 CBS ISS MMC PLUS", "E1F02N12S1 A/T PLUS", "UNT");
        buildMotor("GZN", "VARIO 150 MMC PLUS", "K1H02N14S1 A/T PLUS", "UNT");
        buildMotor("GZP", "VARIO 150 MMC PLUS", "K1H02N14S2P A/T", "UNT");
        buildMotor("GXK", "NEW BEAT POP ESP CBS PLUS", "Y1G02N02L1AA A/T PLUS", "UNT");
        buildMotor("GXL", "NEW BEAT POP ESP CBS PLUS", "Y1G02N02L1AB A/T PLUS", "UNT");
        buildMotor("GYL", "NEW BEAT POP ESP CBS ISS PLUS", "Y1G02N15L1 A/T PLUS", "UNT");
        buildMotor("DVN", "H5C02R20M1 M/T EXCLUSIVE", "ALL NEW CB150R EXCLUSIVE", "UNT");
        buildMotor("GZT", "New Vario 150 Esp Exclusive MMC Plus", "K1H02N14S1P A/T", "UNT");
        buildMotor("Z56", "CB150R BANDLING JAKET", "H5C2R2AA", "UNT");
        buildMotor("Z53", "VARIO 150 CBS ISS MMC ACC", "K1H2N14A", "UNT");
        buildMotor("DYP", "ALL NEW CB 150 R STREETFIRE SPESIAL EDITION PLUS", "H5C02R20S1AP M/T", "UNT");
        buildMotor("DYM", "ALL NEW CB 150 R STREETFIRE SPESIAL EDITION PLUS", "H5C02R20M1B M/T SINGLE SEA", "UNT");
        buildMotor("HJA", "ALL NEW SUPRA GTR 150 EXCLUSIVE", "G2E02R21L0A M/T", "UNT");
        buildMotor("SX-GTR-L", "BACK MIRROR SUPRA GTR L", "BACK MIRROR", "UNT");
        buildMotor("SX-GTR-R", "BACK MIRROR SUPRA GTR R", "BACK MIRROR", "UNT");
        buildMotor("HJ0", "ALL NEW SUPRA GTR 150 SPORTY", "G2E02R21L0 M/T", "UNT");
        buildMotor("Z49", "CBR 150R REPSOL", "P5E02MBJ", "UNT");
        buildMotor("Z40", "BEAT SPORTY CBS ACC", "X1B2NLAA", "UNT");
        buildMotor("EDS", "ALL NEW CBR150R STANDAR PLUS PREMIUM", "P5E02R22S1S M/T", "UNT");
        buildMotor("EDT", "ALL NEW CBR150R RED COLOUR PLUS PREMIUM", "P5E02R22S1BT M/T", "UNT");
        buildMotor("EDU", "ALL NEW CBR150R REPSOL PLUS PREMIUM", "P5E02R22S1AU M/T", "UNT");
        buildMotor("GZV", "VARIO 150 MMC ACCS", "K1H02N14S1V A/T", "UNT");
        buildMotor("HD2", "New Sonic 150R Standar", "Y3B02R17S1 M/T", "UNT");
        buildMotor("HDD", "New Sonic 150R Special", "Y3B02R17S1B M/T", "UNT");
        buildMotor("HDC", "NEW SONIC 150 R REPSOL", "Y3B02R17S1A", "UNT");
        buildMotor("HM0", "BEAT SPORTY CW", "D1B02N26L2 A/T", "UNT");
        buildMotor("HL0", "BEAT SPORTY CBS ISS", "D1B02N13L2 A/T", "UNT");
        buildMotor("HK0", "BEAT SPORTY CBS", "D1B02N12L2 A/T", "UNT");
        buildMotor("HMP", "NEW BEAT eSP CW PLUS", "D1B02N26L2 P A/T", "UNT");
        buildMotor("HKP", "NEW BEAT eSP CBS PLUS", "D1B02N12L2P A/T", "UNT");
        buildMotor("HLP", "NEW BEAT eSP CBS ISS PLUS", "D1B02N13L2 P A/T", "UNT");
        buildMotor("HDM", "NEW SONIC 150R REPSOL PLUS", "Y3B02R17S1A M/T", "UNT");
        buildMotor("HDL", "NEW SONIC 150R STANDARD PLUS", "Y3B02R17S1 M/T", "UNT");
        buildMotor("AAH", "ASTREA GRAND YOUNG", "ASTREA GRAND YOUNG", "UNT");
        buildMotor("VRZ", "NULL", "VERZA", "UNT");
        buildMotor("AB9", "ASTREA GRAND YOUNG", "ASTREA GRAND YOUNG", "UNT");
        buildMotor("ABC", "ASTREA GRAND YOUNG", "ASTREA GRAND YOUNG", "UNT");
        buildMotor("ABD", "ASTREA GRAND FAMILY", "ASTREA GRAND FAMILY", "UNT");
        buildMotor("ABF", "ASTREA GRAND YOUNG", "ASTREA GRAND YOUNG", "UNT");
        buildMotor("ABG", "ASTREA GRAND FAMILY", "ASTREA GRAND FAMILY", "UNT");
        buildMotor("ABH", "ASTREA GRAND YOUNG", "ASTREA GRAND YOUNG", "UNT");
        buildMotor("ABJ", "ASTREA GRAND FAMILY", "ASTREA GRAND FAMILY", "UNT");
        buildMotor("ABK", "ASTREA GRAND YOUNG", "ASTREA GRAND YOUNG", "UNT");
        buildMotor("ABL", "ASTREA GRAND", "ASTREA GRAND", "UNT");
        buildMotor("ABM", "ASTREA GRAND", "ASTREA GRAND", "UNT");
        buildMotor("ABN", "ASTREA GRAND LEGENDA", "ASTREA GRAND LEGENDA", "UNT");
        buildMotor("ABP", "LEGENDA", "LEGENDA", "UNT");
        buildMotor("AC0", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AC1", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AC2", "CB 150", "CB 150", "UNT");
        buildMotor("AC3", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AC4", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AC5", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AC6", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AC7", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AD0", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AD1", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AD2", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AE0", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AE1", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AE2", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AE3", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AE4", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AE5", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AF0", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AF1", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AF2", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AF3", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AF4", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AF5", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AGA", "SONIC", "SONIC", "UNT");
        buildMotor("AGL", "REV MODEL", "REV MODEL", "UNT");
        buildMotor("AH2", "REV MODEL", "REV MODEL", "UNT");
        buildMotor("AH3", "REV MODEL", "REV MODEL", "UNT");
        buildMotor("AH4", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AJ0", "PANTHOM", "PANTHOM", "UNT");
        buildMotor("AK0", "HONDA TIGER", "HONDA TIGER", "UNT");
        buildMotor("AK1", "TIGER 2000", "TIGER 2000", "UNT");
        buildMotor("AK2", "TIGER 2000", "TIGER 2000", "UNT");
        buildMotor("AKA", "TIGER 2000", "TIGER 2000", "UNT");
        buildMotor("AKB", "TIGER 2000", "TIGER 2000", "UNT");
        buildMotor("AKC", "TIGER 2000", "TIGER 2000", "UNT");
        buildMotor("AKD", "TIGER 2000", "TIGER 2000", "UNT");
        buildMotor("AKE", "TIGER", "TIGER", "UNT");
        buildMotor("AKF", "TIGER", "TIGER", "UNT");
        buildMotor("AKG", "TIGER", "TIGER", "UNT");
        buildMotor("AKH", "TIGER", "TIGER", "UNT");
        buildMotor("AKJ", "TIGER", "TIGER", "UNT");
        buildMotor("AL0", "TIGER 2000", "TIGER 2000", "UNT");
        buildMotor("AL1", "TIGER 2000", "TIGER 2000", "UNT");
        buildMotor("AL2", "TIGER", "TIGER", "UNT");
        buildMotor("AL3", "TIGER", "TIGER", "UNT");
        buildMotor("AL4", "TIGER", "TIGER", "UNT");
        buildMotor("AL5", "TIGER", "TIGER", "UNT");
        buildMotor("AN0", "NSR 150 R", "NSR 150 R", "UNT");
        buildMotor("AN1", "N S R", "N S R", "UNT");
        buildMotor("AN2", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AP1", "REV MODEL", "REV MODEL", "UNT");
        buildMotor("AP2", "REV MODEL", "REV MODEL", "UNT");
        buildMotor("AP9", "REV MODEL", "REV MODEL", "UNT");
        buildMotor("AQ2", "REV MODEL", "REV MODEL", "UNT");
        buildMotor("AQ3", "REV MODEL", "REV MODEL", "UNT");
        buildMotor("AQ4", "REV MODEL", "REV MODEL", "UNT");
        buildMotor("AR2", "REV MODEL", "REV MODEL", "UNT");
        buildMotor("AR3", "REV MODEL", "REV MODEL", "UNT");
        buildMotor("AR4", "REV MODEL", "REV MODEL", "UNT");
        buildMotor("AR5", "REV MODEL", "REV MODEL", "UNT");
        buildMotor("AR6", "ASTREA STAR", "ASTREA STAR", "UNT");
        buildMotor("AR7", "ASTREA STAR", "ASTREA STAR", "UNT");
        buildMotor("AR8", "ASTREA STAR", "ASTREA STAR", "UNT");
        buildMotor("AT3", "REV MODEL", "REV MODEL", "UNT");
        buildMotor("AT4", "WIN SEMI TRAIL", "WIN SEMI TRAIL", "UNT");
        buildMotor("AT8", "HONDA WIN", "HONDA WIN", "UNT");
        buildMotor("AT9", "HONDA WIN", "HONDA WIN", "UNT");
        buildMotor("ATA", "WIN SEMI TRAIL", "WIN SEMI TRAIL", "UNT");
        buildMotor("ATB", "HONDA WIN", "HONDA WIN", "UNT");
        buildMotor("ATD", "HONDA WIN", "HONDA WIN", "UNT");
        buildMotor("ATE", "WIN SEMI TRAIL", "WIN SEMI TRAIL", "UNT");
        buildMotor("ATF", "HONDA WIN", "HONDA WIN", "UNT");
        buildMotor("ATG", "HONDA WIN", "HONDA WIN", "UNT");
        buildMotor("ATH", "HONDA WIN", "HONDA WIN", "UNT");
        buildMotor("ATI", "HONDA WIN", "HONDA WIN", "UNT");
        buildMotor("ATJ", "HONDA WIN", "HONDA WIN", "UNT");
        buildMotor("AU0", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AU1", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AU2", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AU3", "SUPRA FIT", "SUPRA FIT", "UNT");
        buildMotor("AU4", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AU5", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("AV0", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AV1", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AY0", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("AY1", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("BA0", "NICE", "NICE", "UNT");
        buildMotor("BD0", "KIRANA", "KIRANA", "UNT");
        buildMotor("BD1", "KIRANA", "KIRANA", "UNT");
        buildMotor("BE0", "KHARISMA", "KHARISMA", "UNT");
        buildMotor("BE1", "KHARISMA", "KHARISMA", "UNT");
        buildMotor("BE2", "KHARISMA", "KHARISMA", "UNT");
        buildMotor("BF0", "KHARISMA", "KHARISMA", "UNT");
        buildMotor("BF1", "KHARISMA", "KHARISMA", "UNT");
        buildMotor("BF2", "KHARISMA", "KHARISMA", "UNT");
        buildMotor("BF3", "KHARISMA", "KHARISMA", "UNT");
        buildMotor("BM0", "SUPRA FIT", "SUPRA FIT", "UNT");
        buildMotor("BM1", "SUPRA FIT", "SUPRA FIT", "UNT");
        buildMotor("BN0", "SUPRA FIT", "SUPRA FIT", "UNT");
        buildMotor("BN1", "SUPRA FIT", "SUPRA FIT", "UNT");
        buildMotor("BP0", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("BR0", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("BR1", "NF 125 SD1", "NF 125 SD1", "UNT");
        buildMotor("BS0", "HONDA SUPRA", "HONDA SUPRA", "UNT");
        buildMotor("BS1", "SUPRA X CW", "SUPRA X CW", "UNT");
        buildMotor("BU0", "HONDA SPORT", "HONDA SPORT", "UNT");
        buildMotor("BV0", "KTLM", "KTLM", "UNT");
        buildMotor("BV1", "KTLM", "KTLM", "UNT");
        buildMotor("BV2", "NF 100 SLF", "NF 100 SLF", "UNT");
        buildMotor("BV3", "SUPRA FIT", "SUPRA FIT", "UNT");
        buildMotor("BV4", "SUPRA FIT", "SUPRA FIT", "UNT");
        buildMotor("BV5", "SUPRA FIT", "SUPRA FIT", "UNT");
        buildMotor("BW0", "KTLM D", "KTLM D", "UNT");
        buildMotor("BW1", "KTLM D1", "KTLM D1", "UNT");
        buildMotor("BX0", "NF 125 SF", "NF 125 SF", "UNT");
        buildMotor("BX1", "INJECTION SPOKE", "INJECTION SPOKE", "UNT");
        buildMotor("BY0", "NF 125 SFC", "NF 125 SFC", "UNT");
        buildMotor("BY1", "SUPRA", "SUPRA", "UNT");
        buildMotor("CA0", "SUPRA FIT", "SUPRA FIT", "UNT");
        buildMotor("CA1", "SUPRA FIT", "SUPRA FIT", "UNT");
        buildMotor("CAA", "SUPRA FIT", "SUPRA FIT", "UNT");
        buildMotor("CB0", "KVBF", "KVBF", "UNT");
        buildMotor("CB1", "KVBF", "KVBF", "UNT");
        buildMotor("CC0", "KVBF CW", "KVBF CW", "UNT");
        buildMotor("CC1", "KVBF CW1", "KVBF CW1", "UNT");
        buildMotor("CC2", "KVBF CW2", "KVBF CW2", "UNT");
        buildMotor("CD0", "SUPRA X", "SUPRA X", "UNT");
        buildMotor("CE0", "NF 100 TD", "NF 100 TD", "UNT");
        buildMotor("CE1", "NF 100 TD1", "NF 100 TD1", "UNT");
        buildMotor("CF0", "NF 100 TC", "NF 100 TC", "UNT");
        buildMotor("CF1", "NF 100 TC1", "NF 100 TC1", "UNT");
        buildMotor("CG0", "NF 125 TD", "NF 125 TD", "UNT");
        buildMotor("CG1", "NF 125 TD1", "NF 125 TD1", "UNT");
        buildMotor("CG2", "NF 125 TD2", "NF 125 TD2", "UNT");
        buildMotor("CG3", "NF125TD3 M/T", "NF125TD3 M/T", "UNT");
        buildMotor("CG4", "NF125TD4 M/T", "NF125TD4 M/T", "UNT");
        buildMotor("CG5", "NF125TD5 M/T", "NF125TD5 M/T", "UNT");
        buildMotor("CH0", "NF 125 TR", "NF 125 TR", "UNT");
        buildMotor("CH1", "NF 125 TR1", "NF 125 TR1", "UNT");
        buildMotor("CH2", "NF 125 TR2", "NF 125 TR2", "UNT");
        buildMotor("CH3", "NF125TR3 M/T", "NF125TR3 M/T", "UNT");
        buildMotor("CH4", "NF125TR4 M/T", "NF125TR4 M/T", "UNT");
        buildMotor("CH5", "NF125TR5 M/T", "NF125TR5 M/T", "UNT");
        buildMotor("CH8", "SUPRA X-TR2 PLUS", "SUPRA X-TR2 PLUS", "UNT");
        buildMotor("CJ0", "NF 125 TRF", "NF 125 TRF", "UNT");
        buildMotor("CJ1", "NF 125 TRF1", "NF 125 TRF1", "UNT");
        buildMotor("CJ2", "NF 125 TRF2", "NF 125 TRF2", "UNT");
        buildMotor("CK0", "NF 100 SE", "NF 100 SE", "UNT");
        buildMotor("CL0", "CS12A1RR", "CS12A1RR", "UNT");
        buildMotor("CL1", "CS12A1RR1 M/T", "CS12A1RR1 M/T", "UNT");
        buildMotor("CM0", "NC11B1C A/T", "NC11B1C A/T", "UNT");
        buildMotor("CM1", "NC11B2C A/T", "NC11B2C A/T", "UNT");
        buildMotor("CN0", "NF11A1C M/T", "NF11A1C M/T", "UNT");
        buildMotor("CN1", "NF11A2C M/T", "NF11A2C M/T", "UNT");
        buildMotor("CNA", "NF11A1CA M/T", "NF11A1CA M/T", "UNT");
        buildMotor("CNB", "NF11A2CB M/T", "NF11A2CB M/T", "UNT");
        buildMotor("CP0", "NF11T11C01 M/T", "NF11T11C01", "UNT");
        buildMotor("CP1", "NF11B1D1 M/T", "NF11B1D1 M/T", "UNT");
        buildMotor("CR0", "NF11B1C M/T", "NF11B1C M/T", "UNT");
        buildMotor("CR1", "NF11B1C1 M/T", "NF11B1C1 M/T", "UNT");
        buildMotor("CRA", "NF11B1CA M/T", "NF11B1CA M/T", "UNT");
        buildMotor("CRB", "NF11B1CA1 M/T", "NF11B1CA1 M/T", "UNT");
        buildMotor("CT0", "NC11A2CB A/T", "NC11A2CB A/T", "UNT");
        buildMotor("CV0", "BEAT SPOKE", "BLADE", "UNT");
        buildMotor("CV1", "NC11B3D1A/T", "NC11B3D1A/T", "UNT");
        buildMotor("CV2", "NC11B3D2A/T", "NC11B3D2A/T", "UNT");
        buildMotor("CX0", "NC11B3CA/T", "NC11B3CA/T", "UNT");
        buildMotor("CX1", "NC11B3C1A/T", "NC11B3C1A/T", "UNT");
        buildMotor("CX2", "NC11B3C2 A/T", "NC11B3C2 A/T", "UNT");
        buildMotor("CX9", "BEAT PLUS", "BEAT PLUS", "UNT");
        buildMotor("CXV", "BEAT NEW", "BEAT NEW", "UNT");
        buildMotor("CY0", "NCF11A1CF A/T", "NCF11A1CF A/T", "UNT");
        buildMotor("CZ0", "NC11C1C A/T", "NC11C1C A/T", "UNT");
        buildMotor("CZ1", "NC11C1C1 A/T", "NC11C1C1 A/T", "UNT");
        buildMotor("CZ7", "SCOOPY NEW", "SCOOPY NEW", "UNT");
        buildMotor("CZ8", "SCOOPY PLUS", "SCOOPY PLUS", "UNT");
        buildMotor("DA0", "GL PRO", "GL PRO", "UNT");
        buildMotor("DA1", "GL PRO", "GL PRO", "UNT");
        buildMotor("DA2", "GL 160 D2", "GL 160 D2", "UNT");
        buildMotor("DB0", "GL PRO", "GL PRO", "UNT");
        buildMotor("DB1", "GL PRO", "GL PRO", "UNT");
        buildMotor("DB2", "GL 160 CW2", "GL 160 CW2", "UNT");
        buildMotor("DBA", "GL PRO", "GL PRO", "UNT");
        buildMotor("DC0", "HONDA TIGER", "HONDA TIGER", "UNT");
        buildMotor("DD0", "GL 200 R", "GL 200 R", "UNT");
        buildMotor("DD1", "GL 200 R1", "GL 200 R1", "UNT");
        buildMotor("DD2", "GL 200 R2", "GL 200 R2", "UNT");
        buildMotor("DDA", "GL200RA1 M/T", "GL200RA1 M/T", "UNT");
        buildMotor("DDB", "GL200RA2 M/T", "GL200RA2 M/T", "UNT");
        buildMotor("DE0", "GL15A1D M/T", "GL15A1D M/T", "UNT");
        buildMotor("DE1", "GL15A1D1 M/T", "GL15A1D1 M/T", "UNT");
        buildMotor("DF0", "GL15A1RR M/T", "GL15A1RR M/T", "UNT");
        buildMotor("DF1", "GL15A1RR1 M/T", "GL15A1RR1 M/T", "UNT");
        buildMotor("DG0", "CBR250RB(IN) M/T", "CBR250RB(IN) M/T", "UNT");
        buildMotor("DG1", "CBR250RB1(IN) M/T", "CBR250RB1(IN) M/T", "UNT");
        buildMotor("DGA", "CBR250RB1(IN)R M/T", "CBR250RB1(IN)R M/T", "UNT");
        buildMotor("DH0", "CBR250RAB(IN) M/T", "CBR250RAB(IN) M/T", "UNT");
        buildMotor("DH1", "CBR250RAB1(IN) M/T", "CBR250RAB1(IN) M/T", "UNT");
        buildMotor("DHA", "CBR250RAB1(IN)R M/T", "CBR250RAB1(IN)R M/T", "UNT");
        buildMotor("DJ0", "CBR150RC(IN) M/T", "CBR150RC(IN) M/T", "UNT");
        buildMotor("DJ1", "CBR 150RE IN M/T", "CBR 150RE IN M/T", "UNT");
        buildMotor("DJA", "CBR150RC(IN)R M/T", "CBR150RC(IN)R M/T", "UNT");
        buildMotor("DJB", "CBR 150 RE 3IN M/T", "CBR 150 RE 3IN M/T", "UNT");
        buildMotor("DJC", "CBR 150 RE 5IN M/T", "CBR 150 RE 5IN M/T", "UNT");
        buildMotor("DK0", "CB15A1RRF M/T", "CB15A1RRF M/T", "UNT");
        buildMotor("DK5", "CB15A1RRF M/T", "CB15A1RRF M/T", "UNT");
        buildMotor("DK6", "CB15A1RRF M/T", "CB15A1RRF M/T", "UNT");
        buildMotor("DK7", "CB15A1RRF M/T", "CB15A1RRF M/T", "UNT");
        buildMotor("DM0", "GL15B1CF M/T", "GL15B1CF M/T", "UNT");
        buildMotor("DN0", "GL15B1DF M/T", "GL15B1DF M/T", "UNT");
        buildMotor("FA0", "PCX", "PCX", "UNT");
        buildMotor("FC0", "NC11A3C A/T", "NC11A3C A/T", "UNT");
        buildMotor("FC1", "NC11A3C1 A/T", "NC11A3C1 A/T", "UNT");
        buildMotor("FC8", "REVO110", "REVO 110", "UNT");
        buildMotor("FD0", "NC11A3CB A/T", "NC11A3CB A/T", "UNT");
        buildMotor("FD1", "NEW VARIO TEHNO CBS", "NEW VARIO TEHNO CBS", "UNT");
        buildMotor("FD8", "VARIO CBS PLUS", "VARIO CBS PLUS", "UNT");
        buildMotor("FDB", "VARIO CBS PLUS", "VARIO CBS PLUS", "UNT");
        buildMotor("FE0", "NC110A1C A/T", "NC110A1C A/T", "UNT");
        buildMotor("FE1", "NC110A1C1 A/T", "NC110A1C1 A/T", "UNT");
        buildMotor("FE2", "NC110A1C2 A/T", "NC110A1C2 A/T", "UNT");
        buildMotor("FE8", "VARIO CW PLUS", "VARIO CW PLUS", "UNT");
        buildMotor("FE9", "NEW VARIO", "NEW VARIO", "UNT");
        buildMotor("FF0", "NF11B2D1 M/T", "NF11B2D1 M/T", "UNT");
        buildMotor("FF1", "NF11B2D11M/T", "NF11B2D11M/T", "UNT");
        buildMotor("FF2", "NF11B2D12 M/T", "NF11B2D12 M/T", "UNT");
        buildMotor("FG0", "NF11B2DA1 M/T", "NF11B2DA1 M/T", "UNT");
        buildMotor("FG1", "NF11B2DA11M/T", "NF11B2DA11M/T", "UNT");
        buildMotor("FG2", "NF11B2DA12 M/T", "NF11B2DA12 M/T", "UNT");
        buildMotor("FH0", "NF11B2C1 M/T", "NF11B2C1 M/T", "UNT");
        buildMotor("FH1", "NF11B2C11M/T", "NF11B2C11M/T", "UNT");
        buildMotor("FH2", "NF11B2C12 M/T", "NF11B2C12 M/T", "UNT");
        buildMotor("FJ0", "NC11D1D A/T", "NC11D1D A/T", "UNT");
        buildMotor("FK0", "NC11D1C A/T", "NC11D1C A/T", "UNT");
        buildMotor("FL0", "NF12A1C M/T", "NF12A1C M/T", "UNT");
        buildMotor("FM0", "NF12A1CF M/T", "NF12A1CF M/T", "UNT");
        buildMotor("FM1", "NF12A1CF1 M/T", "NF12A1CF1 M/T", "UNT");
        buildMotor("FM2", "NF12A1CF2 M/T", "NF12A1CF2 M/T", "UNT");
        buildMotor("FN0", "NC11CF1C A/T", "NC11CF1C A/T", "UNT");
        buildMotor("FNA", "NC11CF1CS A/T", "NC11CF1CS A/T", "UNT");
        buildMotor("FP0", "NF11C1C M/T", "NF11C1C M/T", "UNT");
        buildMotor("FP1", "NF11C1C1 M/T", "NF11C1C1 M/T", "UNT");
        buildMotor("FPA", "NF11C1CA M/T", "NF11C1CA M/T", "UNT");
        buildMotor("FR0", "NC11D1CF A/T", "NC11D1CF A/T", "UNT");
        buildMotor("FR1", "NC11D1CF1 A/T", "NC11D1CF1 A/T", "UNT");
        buildMotor("FS0", "NC12A1CF A/T", "NC12A1CF A/T", "UNT");
        buildMotor("FS1", "NC12A1CF1 A/T", "NC12A1CF1 A/T", "UNT");
        buildMotor("FS8", "VARIO TECHNO PGM-FI PLUS", "VARIO TECHNO PGM-FI PLUS", "UNT");
        buildMotor("FS9", "NEW VARIO TECHNO PGM FI", "NEW VARIO TECHNO PGM FI", "UNT");
        buildMotor("FT0", "NC12A1CBF A/T", "NEW VARIO CBS PGMFI", "UNT");
        buildMotor("FT8", "VARIO CBS PGM-FI PLUS", "VARIO CBS PGM-FI PLUS", "UNT");
        buildMotor("FV0", "NC11BF1D A/T", "NC11BF1D A/T", "UNT");
        buildMotor("FW0", "NC11BF1C A/T", "NC11BF1C A/T", "UNT");
        buildMotor("FW8", "NC11BF1C A/T P", "NC11BF1C A/T P", "UNT");
        buildMotor("FX0", "NC11BF1CB A/T", "NC11BF1CB A/T", "UNT");
        buildMotor("FX8", "BEAT CBS PGM FI PLUS", "BEAT CBS PGM FI PLUS", "UNT");
        buildMotor("FY0", "WW150D(IN) A/T", "WW150D(IN) A/T", "UNT");
        buildMotor("FZ0", "NF11C1CD M/T", "NF11C1CD M/T", "UNT");
        buildMotor("GA0", "NC12AF2CBI A/T", "NC12AF2CBI A/T", "UNT");
        buildMotor("GA8", "VARIO 125 CBS IDLING STOP PLUS", "VARIO 125 CBS IDLING STOP PLUS", "UNT");
        buildMotor("Z93", "GL15BCFA", "GL15BCFA", "UNT");
        buildMotor("Z94", "NC11B3 A/T", "NC11B3 A/T", "UNT");
        buildMotor("DK1", "CB15A1RRF1 M/T", "CB15A1RRF1 M/T", "UNT");
        buildMotor("GB0", "NF11T11C01 M/T", "NF11T11C01 M/T", "UNT");
        buildMotor("GC0", "NF11T11C02 M/T", "NF11T11C02 M/T", "UNT");
        buildMotor("GD0", "NF11T11C03  M/T", "NF11T11C03  M/T", "UNT");
        buildMotor("DL0", "MEGAPRO CW FI", "GL15C21A07 M/T", "UNT");
        buildMotor("DN1", "VERZA SPOKE FI", "GL15B1DF1 M/T", "UNT");
        buildMotor("DM1", "VERZA CW FI", "GL15B1CF1 M/T", "UNT");
        buildMotor("GE0", "SUPRA TD SPOKE FI", "AFX12U21C07 M/T", "UNT");
        buildMotor("GF0", "NEW SUPRA CW FI", "AFX12U21C08 M/T", "UNT");
        buildMotor("GG0", "NEW BLADE S 125 FI", "AFP12W21C03 M/T", "UNT");
        buildMotor("GH0", "NEW BLADE R 125 FI", "AFP12W21C08 M/T", "UNT");
        buildMotor("GHA", "NEW BLADE R 125 FI", "AFP12W21C08A M/T", "UNT");
        buildMotor("GJ0", "NEW VARIO FI", "ATI1I21B01 A/T", "UNT");
        buildMotor("GK0", "NEW VARIO TECHNO 125 FI CBS", "ACB2J21B02 A/T", "UNT");
        buildMotor("GL0", "NEW VARIO TECHNO 125 FI ISS", "ACB2J22B03 A/T", "UNT");
        buildMotor("GJJ", "New Vario FI PLUS", "ATI1I21B01 A/T PLUS", "UNT");
        buildMotor("GKJ", "Vario Techno 125 CBS PLUS", "ACB2J21B02 A/T PLUS", "UNT");
        buildMotor("GLJ", "Vario Techno 125 CBS ISS PLUS", "ACB2J21B03 A/T PLUS", "UNT");
        buildMotor("GL9", "NEW VARIO TECHNO 125 FI ISS PLUS", "ACB2J22B03A A/T", "UNT");
        buildMotor("GM0", "BEAT SPOKE FI MMC", "ACH1M21B04 A/T", "UNT");
        buildMotor("GN0", "BEAT CW FI MMC", "ACH1M21B04A A/T", "UNT");
        buildMotor("GNA", "BEAT CW FI MMC PLUS", "ACH1M21B04S A/T", "UNT");
        buildMotor("GP0", "BEAT CBS FI MMC", "ACH1M21B05 A/T", "UNT");
        buildMotor("GPA", "BEAT CBS FI MMC PLUS", "ACH1M21B05S A/T", "UNT");
        buildMotor("GNJ", "ACH1M21B04A A/T PLUS", "BEAT CW FI MMC PLUS", "UNT");
        buildMotor("GNK", "ACH1M21B04S A/T PLUS", "BEAT CW FI SPORTY PLUS", "UNT");
        buildMotor("GPK", "ACH1M21B05S A/T PLUS", "BEAT CW FI SPORTY PLUS", "UNT");
        buildMotor("GPJ", "ACH1M21B05 A/T PLUS", "BEAT CBS FI MMC PLUS", "UNT");
        buildMotor("DR0", "NULL", "NEW CBR 250R STD", "UNT");
        buildMotor("DRA", "NULL", "NEW CBR 250R STD 2 IN", "UNT");
        buildMotor("DRB", "NULL", "NEW CBR 250R STD 4 IN", "UNT");
        buildMotor("DS0", "NULL", "NEW CBR 250R ABS", "UNT");
        buildMotor("DSA", "NULL", "NEW CBR 250R ABS 2 IN", "UNT");
        buildMotor("DSB", "NULL", "NEW CBR 250R ABS 4 IN", "UNT");
        buildMotor("FR2", "NULL", "NC11D1CF2 A/T", "UNT");
        buildMotor("GR0", "NULL", "ACF1L21B06 A/T", "UNT");
        buildMotor("GRA", "NULL", "ACF1L21B06S A/T", "UNT");
        buildMotor("HBU", "New Vario 125 ESP CBS ISS MMC Plus", "E1F02N12S2P A/T", "UNT");
        buildMotor("Z22", "NEW CBR 150 R – REPSOL MMC", "P5E02SBJ  M/T", "UNT");
        buildMotor("Z23", "NEW CBR 150 R – RED MMC", "P5E02SAJ  M/T", "UNT");
        buildMotor("Z24", "NEW CBR 150 R – STANDARD MMC", "P5E02S1J M/T", "UNT");
        buildMotor("Z25", "NEW VARIO 150 MMC", "K1H2N4A2 A/T", "UNT");
        buildMotor("Z19", "ALL NEW SCOOPY PLAYFULL ACC", "F1C2N2AC", "UNT");
        buildMotor("Z20", "ALL NEW SCOOPY SPORTY ACC", "F1C2N2AB", "UNT");
        buildMotor("Z21", "ALL NEW SCOOPY STYLISH ACC", "F1C2N2AA", "UNT");
        buildMotor("HPS", "All New Scoopy Stylish GRY PLUS", "F1C02N28L0A A/T GRY PLUS", "UNT");
        buildMotor("HPU", "ALL NEW SCOOPY STYLISH LP SF", "F1C02N28L0AU A/T", "UNT");
        buildMotor("HPT", "All New Scoopy Sporty GRY Plus", "F1C02N28L0B A/T GRY PLUS", "UNT");
        buildMotor("DVR", "ALL NEW CB 150 R STREETFIRE SPESIAL EDITION PLUS PREMIUM", "H5C02R20S1Q M/T", "UNT");
        buildMotor("HAU", "NEW VARIO 125 ESP CBS MMC PLUS", "E1F02N11S2P A/T", "UNT");
        buildMotor("DY2", "CB150R STRETTFIRE", "H5C02R20S2A M/T", "UNT");
        buildMotor("DYC", "CB150R STREETFIRE", "H5C02R20S2B M/T", "UNT");
        buildMotor("DYW", "ALL NEW CB 150R STREETFIRE SPECIAL EDITION MMC PLUS", "H5C02R20S2BP M/T", "UNT");
        buildMotor("DV3", "CB150R STREETFIRE", "H5C02R20S2 M/T", "UNT");
        buildMotor("HM3", "BEAT SPORTY CW", "D1B02N26S1 A/T", "UNT");
        buildMotor("HRP", "SH 150 LP", "SH150ADHP IN A/T", "UNT");
        buildMotor("DYV", "ALL NEW CB150R STREETFIRE SPECIAL EDITION MMC PLUS", "H5C02R20S2AP M/T", "UNT");
        buildMotor("Z14", "BEAT ISS ACC", "D1B2N3SA", "UNT");
        buildMotor("Z17", "CB 150R BUNDLING ACC", "H5C2RACA", "UNT");
        buildMotor("Z18", "CB 150R BUNDLING JAKET", "H5C2R2AC", "UNT");
        buildMotor("FM3", "SUPRA X 125 HELM IN FI", "NF12A1CF3 M/T", "UNT");
        buildMotor("Z88", "BEAT CBS FI MMC PLUS", "ACH1M21B05 A/T PLUS", "UNT");
        buildMotor("Z89", "BEAT CBS FI SPORTY PLUS", "ACH1M21B05S A/T PLUS", "UNT");
        buildMotor("Z90", "NEW VARIO TECHNO 125 FI ISS", "ACB2J22B03 A/T PLUS", "UNT");
        buildMotor("Z91", "VERZA CW FI PLUS", "GL15B1CF1 M/T PLUS", "UNT");
        buildMotor("GHA8", "NEW BLADE R 125 FI PLUS", "AFP12W21C08A M/T PLUS", "UNT");
        buildMotor("GB8", "NF11T11C01 M/T PLUS", "REVO FIT PLUS", "UNT");
        buildMotor("GC8", "NF11T11C02 M/T PLUS", "REVO SPOKE FI PLUS", "UNT");
        buildMotor("GD8", "NF11T11C03  M/T", "REVO", "UNT");
        buildMotor("GE8", "AFX12U21C07 M/T", "SUPRA X 125", "UNT");
        buildMotor("GF8", "AFX12U21C08 M/T", "SUPRA X125 CW FI PLUS", "UNT");
        buildMotor("GG8", "AFP12W21C03 M/T PLUS", "NEW BLADE S FI PLUS", "UNT");
        buildMotor("GH8", "AFP12W21C08 M/T PLUS", "NEW BLADE R 125 FI PLUS", "UNT");
        buildMotor("FM9", "NF12A1CF3 M/T PLUS", "NF12A1CF3 M/T PLUS", "UNT");
        buildMotor("DP0", "T5E02R11L0", "T5E02R11L0", "UNT");
        buildMotor("DPA", "T5E02R11L0A", "T5E02R11L0A", "UNT");
        buildMotor("DPB", "NEW CBR 150R T5E02R11L0B M/T", "T5E02R11L0B M/T", "UNT");
        buildMotor("DT0", "NEW PCX 150 WW150EXF", "WW150EXF", "UNT");
        buildMotor("Z87", "NEW SUPRA CW FI PLUS", "AFX12U21C08A", "UNT");
        buildMotor("DK2", "CB15A1RRF2 M/T", "CB15A1RRF2 M/T", "UNT");
        buildMotor("DK9", "CB15A1RRF2A M/T", "CB15A1RRF2A M/T", "UNT");
        buildMotor("GS0", "BEAT SPORTY CW", "X1B02N04L0 A/T", "UNT");
        buildMotor("GT0", "X1B02R07L0 A/T", "X1B02R07L0 A/T", "UNT");
        buildMotor("GV0", "X1B02N04L0A A/T", "X1B02N04L0A A/T", "UNT");
        buildMotor("GW0", "Y1G02N02L0 A/T", "Y1G02N02L0 A/T", "UNT");
        buildMotor("GX0", "Y1G02N02L0A A/T", "Y1G02N02L0A A/T", "UNT");
        buildMotor("GY0", "Y1G02N15L0 A/T", "Y1G02N15L0 A/T", "UNT");
        buildMotor("GZ0", "K1H02N14L0 A/T", "K1H02N14L0 A/T", "UNT");
        buildMotor("DK2SE", "CB15A1RRF2SE", "CB15A1RRF2SE", "UNT");
        buildMotor("DKM", "CB15A1RRF2 M/T SE", "CB15A1RRF2 M/T SE", "UNT");
        buildMotor("GZA", "K1H02N14L0M A/T", "K1H02N14L0M A/T", "UNT");
        buildMotor("HA0", "ALL NEW VARIO 125 FI CBS", "E1F02N11M2 A/T", "UNT");
        buildMotor("HB0", "ALL NEW VARIO 125 FI ISS", "E1F02N12M2", "UNT");
        buildMotor("GSJ", "BEAT F1 SPORTY CW PLUS", "X1B02N04L0 A/T PLUS", "UNT");
        buildMotor("GVJ", "X1B02N04L0A A/T PLUS", "X1B02N04L0A A/T PLUS", "UNT");
        buildMotor("GTJ", "X1B02R07L0 A/T PLUS", "X1B02R07L0 A/T PLUS", "UNT");
        buildMotor("GWJ", "Y1G02N02L0 A/T PLUS", "Y1G02N02L0 A/T PLUS", "UNT");
        buildMotor("GXJ", "Y1G02N02L0A A/T PLUS", "Y1G02N02L0A A/T PLUS", "UNT");
        buildMotor("GYJ", "Y1G02N15L0 A/T PLUS", "Y1G02N15L0 A/T PLUS", "UNT");
        buildMotor("GJP", "NEW VARIO 110 FI PLUS", "ATI1I21B01 A/T PLUS", "UNT");
        buildMotor("GXP", "Y1G02N02L0A P", "Y1G02N02L0A P", "UNT");
        buildMotor("GZZ", "K1H02N14L0MA A/T", "K1H02N14L0MA A/T", "UNT");
        buildMotor("GYP", "Y1G02N15L0 A/T PLUS", "Y1G02N15L0 A/T PLUS", "UNT");
        buildMotor("GRQ", "ACF1L21B06 SP A/T FI  PLUS", "ACF1L21B06 SP A/T FI  PLUS", "UNT");
        buildMotor("Z74", "X1B2N7LA", "X1B2N7LA", "UNT");
        buildMotor("Z76", "CB15ARRJ", "CB15ARRJ", "UNT");
        buildMotor("Z78", "TE502RJA", "TE502RJA", "UNT");
        buildMotor("HAJ", "E1F02N11M2 A/T PLUS", "E1F02N11M2 A/T PLUS", "UNT");
        buildMotor("GVP", "X1B02N04L0A P", "X1B02N04L0A P", "UNT");
        buildMotor("GWP", "Y1G02N02L0 P", "Y1G02N02L0 P", "UNT");
        buildMotor("HB8", "VARIO ESP 125 CBS ISS PLUS", "E1F02N12M2 A/T", "UNT");
        buildMotor("Z70", "K1H2NALM A/T", "K1H2NALM A/T", "UNT");
        buildMotor("Z79", "T5E02RJ0 M/T", "T5E02RJ0 M/T", "UNT");
        buildMotor("Z77", "T5E02RJB M/T", "T5E02RJB M/T", "UNT");
        buildMotor("DSC", "CBR250RAF 5IN M/T", "CBR250RAF 5IN M/T", "UNT");
        buildMotor("DSZ", "NEW CBR 250R ABS ACC", "CBR250RAFA 5IN M/T", "UNT");
        buildMotor("GRP", "ACFL1L21B06 P", "ACFL1L21B06 P", "UNT");
        buildMotor("HBJ", "ALL NEW VARIO 125 FI ISS", "E1F02N12M2 A/T PLUS", "UNT");
        buildMotor("GZK", "NEW VARIO 150 - EXCLUSIVE CHROME", "K1H02N14L0M A/T CHROME", "UNT");
        buildMotor("GZM", "NEW VARIO 150 - EXCLUSIVE GOLD", "K1H02N14L0M A/T GOLD", "UNT");
        buildMotor("GSP", "X1B02N04L0 P", "X1B02N04L0 P", "UNT");
        buildMotor("HC0", "SCOOPY ESP STYLISH", "C1C02N16M2 A/T", "UNT");
        buildMotor("HCA", "SCOOPY ESP SPORTY", "C1C02N16M2S A/T", "UNT");
        buildMotor("GTP", "X1B02R07L0 P", "X1B02R07L0 P", "UNT");
        buildMotor("GB1", "NF11T13C01 M/T", "NF11T13C01 M/T", "UNT");
        buildMotor("GC1", "NF11T13C02 M/T", "NF11T13C02 M/T", "UNT");
        buildMotor("GD1", "NF11T13C03 M/T", "NF11T13C03 M/T", "UNT");
        buildMotor("HCP", "C1C02N16M2 P", "C1C02N16M2 P", "UNT");
        buildMotor("HCQ", "C1C02N16 M2 S PLUS", "C1C02N16 M2 S PLUS", "UNT");
        buildMotor("Z75", "CB 150 R SE ACC", "CB15AR2J", "UNT");
        buildMotor("GY9", "Beat Pop CBS ISS Accs", "Y1G02N15L0A A/T", "UNT");
        buildMotor("GW9", "Beat Pop CW Accs", "Y1G02N02L0AZ A/T", "UNT");
        buildMotor("GX9", "Beat Pop CBS Accs", "Y1G02N02L0AA A/T", "UNT");
        buildMotor("GV9", "NEW BEAT SPORTY CBS  ACCS", "X1B02N04L0 AZ A/T", "UNT");
        buildMotor("GT9", "NEW BEAT SPORTY ISS ACCS", "X1B02R07L0 AZ A/T", "UNT");
        buildMotor("GZY", "VARIO 150 EXCLUSIVE  ACCS", "K1H02N14L0M AZ A/T", "UNT");
        buildMotor("Z69", "BEAT POP CW FI ACC", "Y1G02N02L0A A/T", "UNT");
        buildMotor("Z64", "CBR 150 R ACC", "T5E02RAB", "UNT");
        buildMotor("Z65", "CBR 150 REPSOL ACC", "T5E02RAA", "UNT");
        buildMotor("HD0", "ALL NEW SONIC 150 R", "Y3B02R17L0 M/T", "UNT");
        buildMotor("HH0", "NEW PCX 150", "WW150EXG IN A/T", "UNT");
        buildMotor("Z63", "VARIO 150 ESP EXCLUSIVE ACC BATIK", "K1H2NA2M A/T", "UNT");
        buildMotor("Z66", "CBR 150 R ACC", "T5E02RA0", "UNT");
        buildMotor("HN1", "BEAT STREET CBS", "D1I02N27S1 A/T", "UNT");
        buildMotor("HK2", "BEAT SPORTY CBS", "D1B02N12S1 A/T", "UNT");
        buildMotor("HL4", "BEAT SPORTY CBS ISS", "D1B02N13S1 A/T", "UNT");
        buildMotor("ELD", "NEW CBR 250RR ABS", "R5F04R24L0D M/T", "UNT");
        buildMotor("HMQ", "BEAT SPORTY CW LP", "D1B02N26S1Q A/T", "UNT");
        buildMotor("HLQ", "BEAT SPORTY CBS ISS LP", "D1B02N13S1Q A/T", "UNT");
        buildMotor("ELU", "NEW CBR 250RR ABS LP", "R5F04R24L0DU M/T", "UNT");
        buildMotor("HNQ", "BEAT STREET CBS LP", "D1I02N27S1Q A/T", "UNT");
        buildMotor("HKQ", "BEAT SPORTY CBS LP", "D1B02N12L2 A/T", "UNT");
        buildMotor("DKJ", "CB15A1RRF1  M/T DX", "CB15A1RRF1 M/T DX", "UNT");
        buildMotor("DKK", "CB15A1RRF1 M/T DX", "CB15A1RRF1 M/T DX", "UNT");
        buildMotor("DKL", "CB15A1RRF1 M/T DX", "CB15A1RRF1 M/T DX", "UNT");
        buildMotor("Z86", "NEW VARIO FI MMC PLUS", "AT1I121B01 A/T MMC PLUS", "UNT");
        buildMotor("EKA", "NEW CBR 250RR STD", "R5F04R25L0A M/T", "UNT");
        buildMotor("EKC", "NEW CBR 250RR STD", "R5F04R25L0B M/T", "UNT");
        buildMotor("EL0", "NEW CBR 250RR ABS", "R5F04R24L0 M/T", "UNT");
        buildMotor("ELA", "NEW CBR 250RR ABS", "R5F04R24L0A M/T", "UNT");
        buildMotor("ELC", "NEW CBR 250RR ABS", "R5F04R24L0B M/T", "UNT");
        buildMotor("ELB", "NEW CBR 250RR ABS", "R5F04R24L0C M/T", "UNT");
        buildMotor("ELT", "CBR 250RR ABS Repsol ACC LP", "R5F04R24L0CT M/T", "UNT");
        buildMotor("GZW", "Vario 150 Eksklusif MMC Plus Premium", "K1H02N14S1 LP A/T", "UNT");
        buildMotor("HNP", "BEAT STREET CBS LP", "D1I02N27M1P A/T", "UNT");
        buildMotor("DNP", "VERZA SPOKE WHEEL LP", "GL15B1DF2P M/T", "UNT");
        buildMotor("DMP", "VERZA CAST WHEEL LP", "GL15B1CF2P M/T", "UNT");
        buildMotor("GB9", "REVO FIT FI LP", "NF11T13C01 LP M/T", "UNT");
        buildMotor("GC9", "REVO SP FI LP", "NF11T13C02 LP M/T", "UNT");
        buildMotor("GD9", "REVO FI CW LP", "NF11T13C03 LP M/T", "UNT");
        buildMotor("DLP", "MEGA PRO CW FI LP", "GL15C21A07P M/T", "UNT");
        buildMotor("GHP", "BLADE 125 FI R STD LP", "AFP12W22C08P M/T", "UNT");
        buildMotor("EEP", "CRF 250 RALLY LP", "CRF250RLH INP M/T", "UNT");
        buildMotor("EKP", "CBR 250RR STD LP", "R5F04R25L0P M/T", "UNT");
        buildMotor("EKQ", "CBR 250RR STD LP", "R5F04R25L0AQ M/T", "UNT");
        buildMotor("EKR", "CBR 250RR STD LP", "R5F04R25L0BR M/T", "UNT");
        buildMotor("ELP", "CBR 250RR ABS LP", "R5F04R24L0P M/T", "UNT");
        buildMotor("ELS", "CBR 250RR ABS Repsol LP", "R5F04R24L0CS M/T", "UNT");
        buildMotor("ELR", "CBR 250RR ABS LP", "R5F04R24L0BR M/T", "UNT");
        buildMotor("ELQ", "CBR 250RR ABS LP", "R5F04R24L0AQ M/T", "UNT");
        buildMotor("EDP", "CBR 150R LP", "P5E02R22M1P M/T", "UNT");
        buildMotor("EDQ", "CBR 150R LP", "P5E02R22M1AQ M/T", "UNT");
        buildMotor("EDR", "CBR 150R LP", "P5E02R22M1BR M/T", "UNT");
        buildMotor("GW7", "New BeAT Pop Esp CW Pixel LP", "Y1G02N02L1 LP A/T", "UNT");
        buildMotor("GWX", "New BeAT Pop Esp CW Comic LP", "Y1G02N02L1A LP A/T", "UNT");
        buildMotor("GX7", "New BeAT Pop Esp CBS Pixel LP", "Y1G02N02L1AA LP A/T", "UNT");
        buildMotor("GXX", "New BeAT Pop Esp CBS Comic LP", "Y1G02N02L1AB LP A/T", "UNT");
        buildMotor("GY7", "New BeAT Pop Esp CBS ISS Pixel LP", "Y1G02N15L1 LP A/T", "UNT");
        buildMotor("GYX", "New BeAT Pop Esp CBS ISS Comic LP", "Y1G02N15L1A LP A/T", "UNT");
        buildMotor("GE9", "SUPRA X 125 Spoke FI MMC LP", "AFX12U22C07 LP M/T", "UNT");
        buildMotor("GFZ", "SUPRA X 125 CW Luxuy LP", "AFX12U22C08A LP M/T", "UNT");
        buildMotor("GF9", "SUPRA X 125 CW Sporty FI MMC LP", "AFX12U22C08 LP M/T", "UNT");
        buildMotor("FR9", "SPACY FI LP", "NC11D1CF2 LP A/T", "UNT");
        buildMotor("HH9", "PCX 150 MMC LP", "WW150EXH IN LP A/T", "UNT");
        buildMotor("HEP", "VARIO ESP CBS MMC LP", "D1A02N18S1P A/T", "UNT");
        buildMotor("HEQ", "VARIO ESP CBS ADV MMC LP", "D1A02N18S1AQ A/T", "UNT");
        buildMotor("HFP", "VARIO ESP CBS ISS MMC LP", "D1A02N19S1P A/T", "UNT");
        buildMotor("HFQ", "VARIO ESP CBS ISS ADV MMC LP", "D1A02N19S1AQ A/T", "UNT");
        buildMotor("HJP", "SUPRA GTR 150 SPORTY LP", "G2E02R21L0P M/T", "UNT");
        buildMotor("HDR", "SONIC 150R SPECIAL LP", "Y3B02R17S1BR M/T", "UNT");
        buildMotor("HPJ", "ALL NEW SCOOPY STYLISH PLUS", "F1C02N28L0AP A/T", "UNT");
        buildMotor("HPK", "ALL NEW SCOOPY SPORTY PLUS", "F1C02N28L0BP A/T", "UNT");
        buildMotor("HPL", "ALL NEW SCOOPY PLAYFUL PLUS", "F1C02N28L0CP A/T", "UNT");
        buildMotor("HJ3", "NEW SUPRA GTR150 SPORTY", "G2E02R21S1 M/T", "UNT");
        buildMotor("HJR", "NEW SUPRA GTR 150 SPORTY LP", "G2E02R21S1R M/T", "UNT");
        buildMotor("HJD", "NEW SUPRA GTR150 EXCLUSIVE", "G2E02R21S1A M/T", "UNT");
        buildMotor("HJS", "NEW SUPRA GTR 150 EXCLUSIVE LP", "G2E02R21S1AS M/T", "UNT");
        buildMotor("EDF", "NEW CBR 150R", "P5E02R22S1A M/T", "UNT");
        buildMotor("EDE", "NEW CBR 150R", "P5E02R22S1B M/T", "UNT");
        buildMotor("HB2", "NEW VARIO 125 CBS-ISS", "E1F02N12S2 A/T", "UNT");
        buildMotor("DV9", "ALL NEW CB150R STREETFIRE ACC", "H5C02R20M19 M/T", "UNT");
        buildMotor("HKK", "NEW BEAT ESP CBS PLUS", "D1B02N12S1 A/T PLUS", "UNT");
        buildMotor("HLK", "NEW BEAT ESP CBS ISS PLUS", "D1B02N13S1 A/T PLUS", "UNT");
        buildMotor("DYJA", "ALL NEW CB150R", "H5C02R20S2A M/T SSC BLA PLUS", "UNT");
        buildMotor("GD2", "NEW REVO X", "NF11T14C03 M/T", "UNT");
        buildMotor("GB2", "NEW REVO FIT MC", "NF11T14C01 M/T", "UNT");
        buildMotor("GDP", "REVO X LP", "NF11T14C03P M/T", "UNT");
        buildMotor("GBP", "REVO FIT LP", "NF11T14C01P M/T", "UNT");
        buildMotor("GW2", "BEAT POP ESP CW", "Y1G02N02S1 A/T", "UNT");
        buildMotor("GX2", "BEAT POP ESP CBS", "Y1G02N02S1AA A/T", "UNT");
        buildMotor("GY2", "BEAT POP ESP CBS-ISS", "Y1G02N15S1 A/T", "UNT");
        buildMotor("HDX", "SONIC ACCESSORIES", "Y3B02R17S2B M/T PLUS", "UNT");
        buildMotor("HA2", "VARIO 125 CBS MMC", "E1F02N11S2 AT", "UNT");
        buildMotor("ESP", "CRF150L LP", "T4G02T31L0P M/T", "UNT");
        buildMotor("Z16", "CB150R BUNDLING ACC-JAKET", "H5C2RACB", "UNT");
        buildMotor("ES0", "ALL NEW CRF150L", "T4G02T31L0 M/T", "UNT");
        buildMotor("GWS", "BEAT POP CW MMC PLUS", "Y1G02N02S1P A/T", "UNT");
        buildMotor("HE2", "VARIO 110 CBS", "D1A02N18S2 A/T", "UNT");
        buildMotor("HEC", "VARIO 110 CBS", "D1A02N18S2A A/T", "UNT");
        buildMotor("HFC", "VARIO 110 CBS ISS", "D1A02N19S2A A/T", "UNT");
        buildMotor("HER", "VARIO 110 CBS LP", "D1A02N18S2R A/T", "UNT");
        buildMotor("HES", "VARIO 110 CBS LP", "D1A02N18S2AS A/T", "UNT");
        buildMotor("HF2", "VARIO 110 CBS ISS", "D1A02N19S2 A/T", "UNT");
        buildMotor("HFR", "VARIO 110 CBS ISS LP", "D1A02N19S2R A/T", "UNT");
        buildMotor("HFS", "VARIO 110 CBS ISS LP", "D1A02N19S2AS A/T", "UNT");
        buildMotor("EF0", "REBEL MCX500AH IN M/T", "MCX500AH IN M/T", "UNT");
        buildMotor("HK4", "BEAT SPORTY CBS", "D1B02N12S2 A/T", "UNT");
        buildMotor("HL6", "BEAT SPORTY CBS ISS", "D1B02N13S2 A/T", "UNT");
        buildMotor("HM5", "BEAT SPORTY CW", "D1B02N26S2 A/T", "UNT");
        buildMotor("HN2", "BEAT STREET CBS", "D1I02N27S2 A/T", "UNT");
        buildMotor("HKR", "BEAT SPORTY CBS LP", "D1B02N12S2R A/T", "UNT");
        buildMotor("HLR", "BEAT SPORTY CBS ISS LP", "D1B02N13S2R A/T", "UNT");
        buildMotor("HMR", "BEAT SPORTY CW LP", "D1B02N26S2R A/T", "UNT");
        buildMotor("GXS", "NEW BEAT POP CBS MMC PLUS", "Y1G02N02S1AAPAT A/T", "UNT");
        buildMotor("HNR", "BEAT STREET CBS LP", "D1I02N27S2R A/T", "UNT");
        buildMotor("HKL", "BEAT SPORTY CBS PLUS", "D1B02N12S2 A/T PLUS", "UNT");
        buildMotor("HLL", "BEAT SPORTY CBS ISS PLUS", "D1B02N13S2 A/T PLUS", "UNT");
        buildMotor("Z10", "BEAT SPORTY CBS ISS ACC", "D1B2N3A2", "UNT");
        buildMotor("HML", "Beat Sporty Esp CW Plus", "D1B02N26S2 A/T PLUS", "UNT");
        buildMotor("Z11", "SONIC 150R MATTE BLACK", "Y3B2R7JB", "UNT");
        buildMotor("HNL", "BEAT STREET CBS PLUS", "D1I02N27S2 A/T Plus", "UNT");
        buildMotor("HGC", "VARIO 110 CBS", "D1A02N18S2A A/T", "UNT");
        buildMotor("HPV", "ALL NEW SCOOPY SPORTY LP SF", "F1C02N28L0BV A/T", "UNT");
        buildMotor("HPW", "ALL NEW SCOOPY PLAYFUL LP SF", "F1C02N28L0CW A/T", "UNT");
        buildMotor("EDH", "CBR 150R", "P5E02R22S2A M/T", "UNT");
        buildMotor("ED4", "CBR 150R", "P5E02R22S2 M/T", "UNT");
        buildMotor("EDV", "CBR 150R LP", "P5E02R22S2V M/T", "UNT");
        buildMotor("EDX", "CBR 150R LP", "P5E02R22S2AX M/T", "UNT");
        buildMotor("EDG", "CBR 150R", "P5E02R22S2B M/T", "UNT");
        buildMotor("EDW", "CBR 150R LP", "P5E02R22S2BW M/T", "UNT");
        buildMotor("HS0", "NEW PCX 150 CBS", "V1J02Q32L0 A/T", "UNT");
        buildMotor("HSP", "NEW PCX 150 CBS LP", "V1J02Q32L0P A/T", "UNT");
        buildMotor("HT0", "NEW PCX 150 ABS", "V1J02Q33L0 A/T", "UNT");
        buildMotor("HTP", "NEW PCX 150 ABS LP", "V1J02Q33L0P A/T", "UNT");
        buildMotor("Z05", "ALL NEW SCOOPY SPORTY ACC", "F1C2N2B2", "UNT");
        buildMotor("Z06", "NEW SCOOPY STYLISH ACC", "F1C2N2A2", "UNT");
        buildMotor("Z07", "CBR 150R JKT", "P5E02A2J", "UNT");
        buildMotor("HP1", "ALL NEW SCOOPY STYLISTH", "F1C02N28S1A A/T", "UNT");
        buildMotor("HPC", "ALL NEW SCOOPY SPORTY", "F1C02N28S1B A/T", "UNT");
        buildMotor("Z33", "CBR 250 ADV.STANDART(RED)", "R5F025AB M/T", "UNT");
        buildMotor("Z09", "NEW CBR 150 R", "P5E02S2J M/T", "UNT");
        buildMotor("EG0", "NEW CB150 VERZA CW", "B5D02M29M2 M/T", "UNT");
        buildMotor("EH0", "New CB150 Verza SW", "B5D02K29M2 M/T", "UNT");
        buildMotor("EHP", "CB150 VERSA SW LP", "B5D02K29M2P M/T", "UNT");
        buildMotor("EGP", "CB150 VERSA CW LP", "B5D02M29M2P M/T", "UNT");
        buildMotor("HPJA", "New Scoopy Stylish GRY Plus", "F1C02N28S1A A/T Plus", "UNT");
        buildMotor("HPJB", "New Scoopy Sporty GRY Plus", "F1C02N28S1B A/T Plus", "UNT");
        buildMotor("HPJC", "New Scoopy Stylish CRM Plus", "F1C02N28S1A A/T Plus", "UNT");
        buildMotor("HPJD", "New Scoopy Sporty CRM Plus", "F1C02N28S1B A/T Plus", "UNT");
        buildMotor("GE3", "Supra X 125 SW", "AFX12U24C07 M/T", "UNT");
        buildMotor("GEP", "Supra X 125 SW LP", "AFX12U24C07P M/T", "UNT");
        buildMotor("GF3", "Supra X 125 CW", "AFX12U24C08 M/T", "UNT");
        buildMotor("GFP", "Supra X 125 CW LP", "AFX12U24C08P M/T", "UNT");
    }
}
