package id.atiila.service;

import id.atiila.domain.Position;
import id.atiila.repository.PositionRepository;
import id.atiila.repository.search.PositionSearchRepository;
import id.atiila.service.dto.PositionDTO;
import id.atiila.service.mapper.PositionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Set;

import java.util.UUID;
import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Position.
 * BeSmart Team
 */

@Service
@Transactional
public class PositionService {

    private final Logger log = LoggerFactory.getLogger(PositionService.class);

    private final PositionRepository positionRepository;

    private final PositionMapper positionMapper;

    private final PositionSearchRepository positionSearchRepository;

    public PositionService(PositionRepository positionRepository, PositionMapper positionMapper, PositionSearchRepository positionSearchRepository) {
        this.positionRepository = positionRepository;
        this.positionMapper = positionMapper;
        this.positionSearchRepository = positionSearchRepository;
    }

    /**
     * Save a position.
     *
     * @param positionDTO the entity to save
     * @return the persisted entity
     */
    public PositionDTO save(PositionDTO positionDTO) {
        log.debug("Request to save Position : {}", positionDTO);
        Position position = positionMapper.toEntity(positionDTO);
        position = positionRepository.save(position);
        PositionDTO result = positionMapper.toDto(position);
        positionSearchRepository.save(position);
        return result;
    }

    /**
     * Get all the positions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PositionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Positions");
        return positionRepository.findActivePosition(pageable)
            .map(positionMapper::toDto);
    }

    /**
     * Get one position by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PositionDTO findOne(UUID id) {
        log.debug("Request to get Position : {}", id);
        Position position = positionRepository.findOne(id);
        return positionMapper.toDto(position);
    }

    /**
     * Delete the position by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Position : {}", id);
        positionRepository.delete(id);
        positionSearchRepository.delete(id);
    }

    /**
     * Search for the position corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PositionDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Positions for query {}", query);
        Page<Position> result = positionSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(positionMapper::toDto);
    }

    @Transactional(readOnly = true)
    public PositionDTO processExecuteData(Integer id, String param, PositionDTO dto) {
        PositionDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional(readOnly = true)
    public Set<PositionDTO> processExecuteListData(Integer id, String param, Set<PositionDTO> dto) {
        Set<PositionDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

    @Transactional(readOnly = true)
    public PositionDTO changePositionStatus(PositionDTO dto, Integer id) {
        if (dto != null) {
			Position e = positionRepository.findOne(dto.getIdPosition());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        positionSearchRepository.delete(dto.getIdPosition());
                        break;
                    default:
                        positionSearchRepository.save(e);
                }
				positionRepository.save(e);
			}
		}
        return dto;
    }
}
