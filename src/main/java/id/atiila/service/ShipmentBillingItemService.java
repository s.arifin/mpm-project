package id.atiila.service;

import id.atiila.domain.ShipmentBillingItem;
import id.atiila.repository.ShipmentBillingItemRepository;
import id.atiila.repository.search.ShipmentBillingItemSearchRepository;
import id.atiila.service.dto.ShipmentBillingItemDTO;
import id.atiila.service.mapper.ShipmentBillingItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ShipmentBillingItem.
 * BeSmart Team
 */

@Service
@Transactional
public class ShipmentBillingItemService {

    private final Logger log = LoggerFactory.getLogger(ShipmentBillingItemService.class);

    private final ShipmentBillingItemRepository shipmentBillingItemRepository;

    private final ShipmentBillingItemMapper shipmentBillingItemMapper;

    private final ShipmentBillingItemSearchRepository shipmentBillingItemSearchRepository;

    public ShipmentBillingItemService(ShipmentBillingItemRepository shipmentBillingItemRepository, ShipmentBillingItemMapper shipmentBillingItemMapper, ShipmentBillingItemSearchRepository shipmentBillingItemSearchRepository) {
        this.shipmentBillingItemRepository = shipmentBillingItemRepository;
        this.shipmentBillingItemMapper = shipmentBillingItemMapper;
        this.shipmentBillingItemSearchRepository = shipmentBillingItemSearchRepository;
    }

    /**
     * Save a shipmentBillingItem.
     *
     * @param shipmentBillingItemDTO the entity to save
     * @return the persisted entity
     */
    public ShipmentBillingItemDTO save(ShipmentBillingItemDTO shipmentBillingItemDTO) {
        log.debug("Request to save ShipmentBillingItem : {}", shipmentBillingItemDTO);
        ShipmentBillingItem shipmentBillingItem = shipmentBillingItemMapper.toEntity(shipmentBillingItemDTO);
        shipmentBillingItem = shipmentBillingItemRepository.save(shipmentBillingItem);
        ShipmentBillingItemDTO result = shipmentBillingItemMapper.toDto(shipmentBillingItem);
        shipmentBillingItemSearchRepository.save(shipmentBillingItem);
        return result;
    }

    /**
     * Get all the shipmentBillingItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentBillingItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShipmentBillingItems");
        return shipmentBillingItemRepository.findAll(pageable)
            .map(shipmentBillingItemMapper::toDto);
    }

    /**
     * Get one shipmentBillingItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ShipmentBillingItemDTO findOne(UUID id) {
        log.debug("Request to get ShipmentBillingItem : {}", id);
        ShipmentBillingItem shipmentBillingItem = shipmentBillingItemRepository.findOne(id);
        return shipmentBillingItemMapper.toDto(shipmentBillingItem);
    }

    /**
     * Delete the shipmentBillingItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ShipmentBillingItem : {}", id);
        shipmentBillingItemRepository.delete(id);
        shipmentBillingItemSearchRepository.delete(id);
    }

    /**
     * Search for the shipmentBillingItem corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentBillingItemDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of ShipmentBillingItems for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idShipmentItem = request.getParameter("idShipmentItem");
        String idBillingItem = request.getParameter("idBillingItem");

        if (filterName != null) {
        }
        Page<ShipmentBillingItem> result = shipmentBillingItemSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(shipmentBillingItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ShipmentBillingItemDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ShipmentBillingItemDTO");
        String idShipmentItem = request.getParameter("idShipmentItem");
        String idBillingItem = request.getParameter("idBillingItem");

        return shipmentBillingItemRepository.findByParams(idShipmentItem, idBillingItem, pageable)
            .map(shipmentBillingItemMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public ShipmentBillingItemDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        ShipmentBillingItemDTO r = null;
        return r;
    }

    @Transactional
    public Set<ShipmentBillingItemDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<ShipmentBillingItemDTO> r = new HashSet<>();
        return r;
    }

}
