package id.atiila.service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IAtomicLong;
import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.VehicleSalesBillingSearchRepository;
import id.atiila.route.IndexerRoute;
import id.atiila.service.dto.SalesUnitRequirementDTO;
import id.atiila.service.dto.VehicleSalesBillingDTO;
import id.atiila.service.dto.VehicleSalesOrderDTO;
import id.atiila.service.mapper.VehicleSalesBillingMapper;
import id.atiila.service.pto.OrderPTO;
import id.atiila.service.util.CustomTextUtils;
import org.apache.camel.Body;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.lang.StringEscapeUtils;
import org.elasticsearch.index.query.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;

import static id.atiila.base.BaseConstants.STATUS_CANCEL;
import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing VehicleSalesBilling.
 * BeSmart Team
 */

@Service
@Transactional
public class VehicleSalesBillingService {

    private final Logger log = LoggerFactory.getLogger(VehicleSalesBillingService.class);

    private final VehicleSalesBillingRepository vehicleSalesBillingRepository;

    private final VehicleSalesBillingMapper vehicleSalesBillingMapper;

    private final VehicleSalesBillingSearchRepository vehicleSalesBillingSearchRepository;

    @Autowired private VehicleSalesOrderRepository vehicleSalesOrderRepository;

    @Autowired
    private MasterNumberingService masterNumberingService;

    @Autowired
    private VehicleSalesOrderRepository vsoRepo;

    @Autowired
    private PickingSlipRepository repoPis;

    @Autowired private ShipmentItemRepository shipmentItemRepository;

    @Autowired private ItemIssuanceRepository issuanceRepository;

    @Autowired private BillingItemRepository billingItemRepository;

    @Autowired private OrderItemRepository orderItemRepository;

    @Autowired private VehicleDocumentServices vehicleDocumentService;

    @Autowired private OrderBillingItemRepository orderBillingItemService;

    @Autowired private BillingRepository billingRepository;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private BillingTypeRepository billingTypeService;

    @Autowired
    private ActivitiProcessor processor;

    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private HazelcastInstance hz;

    @Autowired
    private SalesmanRepository salesmanRepository;

    public VehicleSalesBillingService(VehicleSalesBillingRepository vehicleSalesBillingRepository, VehicleSalesBillingMapper vehicleSalesBillingMapper, VehicleSalesBillingSearchRepository vehicleSalesBillingSearchRepository) {
        this.vehicleSalesBillingRepository = vehicleSalesBillingRepository;
        this.vehicleSalesBillingMapper = vehicleSalesBillingMapper;
        this.vehicleSalesBillingSearchRepository = vehicleSalesBillingSearchRepository;
    }

    /**
     * Save a vehicleSalesBilling.
     *
     * @param vehicleSalesBillingDTO the entity to save
     * @return the persisted entity
     */
    public VehicleSalesBillingDTO save(VehicleSalesBillingDTO vehicleSalesBillingDTO) {
        log.debug("Request to save VehicleSalesBilling : {}", vehicleSalesBillingDTO);
        VehicleSalesBilling vehicleSalesBilling = vehicleSalesBillingMapper.toEntity(vehicleSalesBillingDTO);


        vehicleSalesBilling = vehicleSalesBillingRepository.save(vehicleSalesBilling);
        VehicleSalesBillingDTO result = vehicleSalesBillingMapper.toDto(vehicleSalesBilling);
        vehicleSalesBillingSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the vehicleSalesBillings.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleSalesBillingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VehicleSalesBillings  ");
        Internal intr = partyUtils.getCurrentInternal();
        return vehicleSalesBillingRepository.findActiveVehicleSalesBilling(intr, pageable)
            .map(vehicleSalesBillingMapper::toDto)
            .map(this::assignVSO)
            .map(this::assingBillType);
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesBillingDTO> findAllLov(Pageable pageable) {
        log.debug("Request to get all VehicleSalesBillings  ");
        Internal intr = partyUtils.getCurrentInternal();
        return vehicleSalesBillingRepository.findActiveVehicleSalesBillingLOV(intr, pageable)
            .map(vehicleSalesBillingMapper::toDto)
            .map(this::assignVSO)
            .map(this::assingBillType);
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesBillingDTO> findAllLovForMemo(Pageable pageable) {
        log.debug("Request to get all VehicleSalesBillings  ");
        Internal intr = partyUtils.getCurrentInternal();
        return vehicleSalesBillingRepository.findForMemo(intr, pageable)
            .map(vehicleSalesBillingMapper::toDto)
            .map(this::assignVSO)
            .map(this::assingBillType);
    }


    @Transactional(readOnly = true)
    public Page<VehicleSalesBillingDTO> findAllTaxListInv(Pageable pageable) {
        log.debug("Request to get all VehicleSalesBillings Tax List ");
        return vehicleSalesBillingRepository.findActiveVehicleSalesBillingTaxList(pageable)
            .map(vehicleSalesBillingMapper::toDto)
            .map(this::assignVSO)
            .map(this::assingBillType)
            .map(this::assignSur);
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesBillingDTO> findByNoIvu(String noIvu, Pageable pageable) {
        log.debug("Request to get all VehicleSalesBillings by no ivu " + noIvu);

        Page<VehicleSalesBilling> pageResult =vehicleSalesBillingRepository.findActiveVehicleSalesBillingByBillingNumber('%'+noIvu+'%', pageable);
        log.debug("Total hasil nya ====> " + pageable.getPageSize());
        return pageResult.map(vehicleSalesBillingMapper::toDto).map(this::assignVSO).map(this::assingBillType);
    }

    /**
     *  Get one vehicleSalesBilling by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public VehicleSalesBillingDTO findOne(UUID id) {
        log.debug("Request to get VehicleSalesBilling : {}", id);
        VehicleSalesBilling vehicleSalesBilling = vehicleSalesBillingRepository.findOne(id);
        return assingBillType(vehicleSalesBillingMapper.toDto(vehicleSalesBilling));
    }

    /**
     *  Delete the  vehicleSalesBilling by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete VehicleSalesBilling : {}", id);
        vehicleSalesBillingRepository.delete(id);
        vehicleSalesBillingSearchRepository.delete(id);
    }

    /**
     * Search for the vehicleSalesBilling corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleSalesBillingDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of VehicleSalesBillings for query {}", query);
        String internalId = partyUtils.getCurrentInternal().getIdInternal();

        query = CustomTextUtils.elasticSearchEscapeString(query);
        log.debug("result of escaping string" + query);

        QueryBuilder boolQueryBuilder = queryStringQuery("*"+query+"*")
            .queryName("billingNumber")
            .queryName("customerName");

        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder();
        q.withQuery(boolQuery().must(boolQueryBuilder).filter(
            boolQuery().must(matchQuery("internalId", internalId)))
        );
        Page<VehicleSalesBillingDTO> result = vehicleSalesBillingSearchRepository.search(q.build().getQuery(), pageable);

        return result;
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesBillingDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered Vehicle Sales Order");
        Internal internal = partyUtils.getCurrentInternal();
        String idCustomer = request.getParameter("idCustomer");
        String billingNumber = request.getParameter("billingNumber");
        String queryFor = request.getParameter("queryFor");
        String dataParam = request.getParameter("dataParam");
        String paramLike = '%' + dataParam +'%';

        log.debug("queryFor ivu" + queryFor);
        if (idCustomer != null) {
            return vehicleSalesBillingRepository.queryByCustomer(idCustomer, pageable).map(vehicleSalesBillingMapper::toDto).map(this::assignVSO).map(this::assingBillType);
        } else if (billingNumber != null) {
            return vehicleSalesBillingRepository.queryByCustomer(billingNumber, pageable).map(vehicleSalesBillingMapper::toDto).map(this::assignVSO).map(this::assingBillType);
        } else if ("faktur".equalsIgnoreCase(queryFor)){
            return vehicleSalesBillingRepository.querySearchFaktur(pageable,paramLike).map(vehicleSalesBillingMapper::toDto).map(this::assignVSO).map(this::assingBillType).map(this::assignSur);
        } else if ("search".equalsIgnoreCase(queryFor)) {
            log.debug("search ivu" + paramLike);
            return vehicleSalesBillingRepository.queryForSearch(paramLike, internal, pageable).map(vehicleSalesBillingMapper::toDto).map(this::assignVSO).map(this::assingBillType);
        }
        return vehicleSalesBillingRepository.queryNothing(pageable).map(vehicleSalesBillingMapper::toDto).map(this::assignVSO).map(this::assingBillType);
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesBillingDTO> findFilterBy(Pageable pageable, OrderPTO param) {
        log.debug("Request to get all filtered Vehicle Sales Order");
        log.debug("ini adalah pto di vsb ==> "+ param.getSalesmanId());
//        Salesman s = salesmanRepository.findOne(param.getSalesId());
//        log.debug("ini adalah salesman di vsb di vsb ==> "+ s);
//        log.debug("ini adalah nama salesman di vsb di vsb ==> "+ s.getPerson().getFirstName());
        return vehicleSalesBillingRepository.queryvsbPTO(pageable, param)
            .map(vehicleSalesBillingMapper::toDto)
            .map(this::assignVSO)
            .map(this::assingBillType);
    }

    @Transactional(readOnly = true)
    public Page<VehicleSalesBillingDTO> findFakturFilterBy(Pageable pageable, OrderPTO param) {
        log.debug("Request to get all filtered Tax Request" + param.getStatusFaktur());
        return vehicleSalesBillingRepository.queryFakturPTO(pageable, param)
            .map(vehicleSalesBillingMapper::toDto)
            .map(this::assignVSO)
            .map(this::assingBillType)
            .map(this::assignSur);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public VehicleSalesBillingDTO changeVSBStatus(VehicleSalesBillingDTO dto, Integer id) {

        if (dto != null) {
            log.debug("vsb chance vso status started");
            VehicleSalesBilling vsb = vehicleSalesBillingRepository.findOne(dto.getIdBilling());
                vsb.setStatus(id);
            vehicleSalesBillingRepository.save(vsb);
        }
        log.debug("vsb status changed finish ");
        return dto;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public VehicleSalesBillingDTO processExecuteData(Integer id, String param, VehicleSalesBillingDTO dto) {
        VehicleSalesBillingDTO r = dto;
        if (r != null) {
            switch (id) {
                case BaseConstants.STATUS_CANCEL:
                    String processName = "ivu-cancel";
                    Map<String, Object> vars = processor.getVariables();
                    processor.startProcess(processName, dto.getIdBilling().toString(), vars);
                    break;
                case 15:
                    printCounter(r);
                    log.debug("id biling print " + r.getIdBilling());
                    break;
                case 16:
                    log.debug("request tax vehiclesalesbilling proses == " + r);
                    requestTax(r);
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public VehicleSalesBillingDTO printCounter (VehicleSalesBillingDTO dto) {
        log.debug("id biling  print " + dto.getIdBilling());
        VehicleSalesBilling vsb = vehicleSalesBillingRepository.findOne(dto.getIdBilling());
        Integer print;
        if (vsb != null) {
            if (dto.getPrintCounter() == null){
                print = dto.setPrintCounter(0);
            }
            Integer sum = dto.getPrintCounter() + 1;
            vsb.setPrintCounter(sum);
        }

        return dto;
    }

    public Set<VehicleSalesBillingDTO> processExecuteListData(Integer id, String param, Set<VehicleSalesBillingDTO> dto) {
        Set<VehicleSalesBillingDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                case 2:
                    for (VehicleSalesBillingDTO vsbDTO : r){
                        VehicleSalesBilling vsb = vehicleSalesBillingRepository.findOne(vsbDTO.getIdBilling());

                        if (vsb != null){
                            log.debug("idrequest tax vehiclesalesbilling == " + vsbDTO.getRequestTaxInvoice());
                            vsb.setRequestTaxInvoice(2);
                            log.debug("save tax vehiclesalesbilling == " + vsb.getRequestTaxInvoice());
                            vsb = vehicleSalesBillingRepository.save(vsb);
                        }
                    }
                    break;
                case 3:
                    for (VehicleSalesBillingDTO vsbDTO : r){
                        VehicleSalesBilling vsb = vehicleSalesBillingRepository.findOne(vsbDTO.getIdBilling());

                        if (vsb != null){
                            log.debug("idrequest tax vehiclesalesbilling == " + vsbDTO.getRequestTaxInvoice());
                            vsb.setRequestTaxInvoice(3);
                            vsb.setTaxInvoiceNote(vsbDTO.getTaxInvoiceNote());
                            log.debug("save tax vehiclesalesbilling == " + vsb.getRequestTaxInvoice());
                            vsb = vehicleSalesBillingRepository.save(vsb);
                        }
                    }
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public VehicleSalesBillingDTO changeVehicleSalesBillingStatus(VehicleSalesBillingDTO dto, Integer id) {
        if (dto != null) {
			VehicleSalesBilling e = vehicleSalesBillingRepository.findOne(dto.getIdBilling());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        vehicleSalesBillingSearchRepository.delete(dto.getIdBilling());
                        break;
                    default:
                        vehicleSalesBillingSearchRepository.save(vehicleSalesBillingMapper.toDto(e));
                }
				vehicleSalesBillingRepository.save(e);
			}
		}
        return dto;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildFrom(VehicleSalesOrderDTO dto) {

        BigDecimal axunitprice;
        BigDecimal axbbn;
        BigDecimal axsalesamount;
        BigDecimal axdisc;
        BigDecimal axprice;
        BigDecimal axppn;
        BigDecimal axaramt;

        List<VehicleSalesBilling> lvsb = new ArrayList<>();
        VehicleSalesBilling vsb = null;

        VehicleSalesOrder v = vsoRepo.findOne(dto.getIdOrder());


        // Cek apakah order bisa di proses
        log.info("vsb:build_from -> get details");
        for (OrderItem oi: v.getDetails()) {
            log.info("vsb:build_from -> get details");
            double QtyBilled = 0;
            for (OrderBillingItem bi: oi.getBillingItems()) {
                QtyBilled = QtyBilled + bi.getBillingItem().getQty();
            }
            // Jika ada Qty yang belum di billed
            log.info("vsb:build_from -> ordr item qty " + oi.getQty() + " qty billed " + QtyBilled );
            if (oi.getQty() > QtyBilled) {
                log.info("vsb:build_from -> ordr item qty > qty billed " );
                if (vsb == null || !vsb.getBillTo().equals(v.getBillTo())) {
                    log.debug("vsb:generate bill number");
                    String billnumber = masterNumberingService.findTransNumberVSBBy(dto.getInternalId(), "IVU");

                    BillingType bilType = billingTypeService.findOne(BaseConstants.BILLING_TYPE_VEHICLE_SALES);

                    log.debug("vsb:new Vehicle sales biling");
                    vsb = new VehicleSalesBilling();
                    vsb.setBillingNumber(billnumber);
                    vsb.setInternal(v.getInternal());
                    vsb.setBillTo(v.getBillTo());
                    vsb.setIdSaleType(v.getIdSaleType());
                    vsb.setCustomer(v.getCustomer());
                    vsb.setBillingType(bilType);
                    vsb.setIdSaleType(v.getIdSaleType());

                    lvsb.add(vsb);
                }
                log.info("vsb:build_from -> generate billing item -> Id Billing[" + vsb.getIdBilling() + "] = order item =[" + oi.getIdOrderItem() );

                List<PickingSlip> lpis = repoPis.findPisByOrderItem(oi);
                if (lpis.size() == 0) throw new DmsException("Delivery Order belum di buat !");

                // Tambahkan Billing Item
                log.debug("vsb:build_from -> add billing item by oi [" + oi.getIdProduct() + "]");
                BillingItem bi = new BillingItem();
                bi.setIdProduct(oi.getIdProduct());
                bi.setIdFeature(oi.getIdFeature());
                bi.setQty(oi.getQty());
                bi.setUnitPrice(oi.getUnitPrice());
                bi.setItemDescription(oi.getItemDescription());
                bi.setDiscount(oi.getDiscount());
                bi.setTaxAmount(oi.getTaxAmount());
                axunitprice = oi.getUnitPrice();

                //Ambil Inventory Item dari product
                log.debug("vsb:build_from -> ambil inventory item [" + lpis.get(0).getInventoryItem() +"] " );
                bi.setInventoryItem(lpis.get(0).getInventoryItem());
                bi.setIdItemType(BaseConstants.ITEM_TYPE_PRODUCT);
                bi.setBasePrice(lpis.get(0).getInventoryItem().getShipmentReceipt().getOrderItem().getUnitPrice());

                log.debug("vsb:build_from -> cek total amount " );
                if (oi.getTotalAmount() == null || oi.getTotalAmount().equals(0)) {
                    oi.setTotalAmount(new BigDecimal(oi.getQty() * (oi.getUnitPrice().doubleValue() - oi.getDiscount().doubleValue())));
                    orderItemRepository.save(oi);
                }

                log.debug("vsb:build_from -> set total amount " + oi.getTotalAmount() );
                bi.setTotalAmount(oi.getTotalAmount());
                bi = billingItemRepository.save(bi);

                log.debug("vsb:build_from -> vsb add billing item [" + bi +"]");
                vsb.addDetail(bi);

                // tambah
                // Save dulu vsb habis add detil
                vehicleSalesBillingRepository.save(vsb);

                //Set Order Item sudah di billing
                //  this.getDiscount().doubleValue() + this.getTaxAmount().doubleValue()));
                log.debug("qty = " + bi.getQty());
                log.debug("Unit Price = " + oi.getUnitPrice() );
                log.debug("Disc = " + oi.getDiscount() );
                log.debug("Tax Amount = " + oi.getTaxAmount() );
                if (bi.getBasePrice()==null){
                    bi.setBasePrice(BigDecimal.ZERO);
                }
                log.debug("vsb:build_from -> order item add billing item =[" + bi + "]" + bi );
                oi.addBillingItem(bi);

                // Build Order billing Item
//                OrderBillingItem obi = new OrderBillingItem();
//                obi.setBillingItem(bi);
//                obi.setOrderItem(oi);
//                obi.setQty(bi.getQty());
//                obi.setAmount(bi.getTotalAmount());
//                orderBillingItemService.save(obi);

                //Buat Pengurusan Dokumen
                Internal intr = v.getInternal();
                log.debug("vsb:build_from -> generate Internal " );
                VehicleSalesOrder vsod = vsoRepo.findOne(oi.getOrders().getIdOrder());
                vehicleDocumentService.buildVehicleDocument(intr, vsod.getDetails().iterator().next());

                log.debug("Document saved...");

                // Component billing
                /*
                TODO:
                1. iterate
                - SUBSIDI AHM, MD, DL, FINCO, BBN
                 */


                log.debug("vsb:build_from -> get SUR [" + dto + "]" );
                // SalesUnitRequirement sur = vsoRepo.findSurByOrderId(dto.getIdOrder());

                BigDecimal subsidiAHM =oi.getDiscountatpm()==null ? BigDecimal.ZERO : oi.getDiscountatpm();
                BillingItem biAHM = new BillingItem();
                biAHM.setIdItemType(BaseConstants.ITEM_TYPE_SUBSIDI_AHM);
                biAHM.setItemDescription("SUBSIDI AHM");
                biAHM.setQty(1D);
                biAHM.setUnitPrice(subsidiAHM);
                biAHM.setTotalAmount(subsidiAHM);
                biAHM.setDiscount(BigDecimal.ZERO);
                biAHM.setTaxAmount(BigDecimal.ZERO);
                biAHM.setBasePrice(BigDecimal.ZERO);
                log.debug("vsb:build_from -> order item add AHM billing item =[" + biAHM + "]" );
                biAHM = billingItemRepository.save(biAHM);


                log.debug("vsb:build_from -> order item VSB add AHM billing item =[" + biAHM + "]" );
                vsb.addDetail(biAHM);
                vehicleSalesBillingRepository.save(vsb);

                // log.debug("vsb:build_from -> order item add OI AHM billing item =[" + bi + "]" );
                // oi.addBillingItem(biAHM);

                BigDecimal subsidiDealer =oi.getDiscount() ==null ? BigDecimal.ZERO : oi.getDiscount();
                BillingItem biDealaer = new BillingItem();
                biDealaer.setIdItemType(BaseConstants.ITEM_TYPE_SUBSIDI_DEALER);
                biDealaer.setItemDescription("SUBSIDI DEALER");
                biDealaer.setQty(1D);
                biDealaer.setUnitPrice(subsidiDealer);
                biDealaer.setTotalAmount(subsidiDealer);
                biDealaer.setDiscount(BigDecimal.ZERO);
                biDealaer.setTaxAmount(BigDecimal.ZERO);
                biDealaer.setBasePrice(BigDecimal.ZERO);
                log.debug("vsb:build_from -> order item add DEALER billing item =[" + biDealaer + "]" );
                biDealaer = billingItemRepository.save(biDealaer);
                vsb.addDetail(biDealaer);
                vehicleSalesBillingRepository.save(vsb);


                BigDecimal subsidiMD =oi.getDiscountMD() ==null ? BigDecimal.ZERO : oi.getDiscountMD();
                BillingItem biMD = new BillingItem();
                biMD.setIdItemType(BaseConstants.ITEM_TYPE_SUBSIDI_MD);
                biMD.setItemDescription("SUBSIDI MD");
                biMD.setQty(1D);
                biMD.setUnitPrice(subsidiMD);
                biMD.setTotalAmount(subsidiMD);
                biMD.setDiscount(BigDecimal.ZERO);
                biMD.setTaxAmount(BigDecimal.ZERO);
                biMD.setBasePrice(BigDecimal.ZERO);
                log.debug("vsb:build_from -> order item add MD billing item =[" + biMD + "]" );
                biMD = billingItemRepository.save(biMD);
                vsb.addDetail(biMD);
                vehicleSalesBillingRepository.save(vsb);

                BigDecimal subsidifincomp =oi.getDiscountFinCoy() ==null ? BigDecimal.ZERO : oi.getDiscountFinCoy();
                BillingItem biFin = new BillingItem();
                biFin.setIdItemType(BaseConstants.ITEM_TYPE_SUBSIDI_FIN_CO);
                biFin.setItemDescription("SUBSIDI FINANCIAL COMPANY");
                biFin.setQty(1D);
                biFin.setUnitPrice(subsidifincomp);
                biFin.setTotalAmount(subsidifincomp);
                biFin.setDiscount(BigDecimal.ZERO);
                biFin.setTaxAmount(BigDecimal.ZERO);
                biFin.setBasePrice(BigDecimal.ZERO);
                log.debug("vsb:build_from -> order item add FIN CO billing item =[" + biFin + "]" );
                biFin = billingItemRepository.save(biFin);
                vsb.addDetail(biFin);
                vehicleSalesBillingRepository.save(vsb);

                BigDecimal biayaBBN =oi.getBbn() ==null ? BigDecimal.ZERO : oi.getBbn();
                BillingItem biBbn = new BillingItem();
                biBbn.setIdItemType(BaseConstants.ITEM_TYPE_BBN_PRICE);
                biBbn.setItemDescription("BBN PRICE");
                biBbn.setQty(1D);
                biBbn.setUnitPrice(biayaBBN);
                biBbn.setTotalAmount(biayaBBN);
                biBbn.setDiscount(BigDecimal.ZERO);
                biBbn.setTaxAmount(BigDecimal.ZERO);
                biBbn.setBasePrice(BigDecimal.ZERO);
                log.debug("vsb:build_from -> order item add BBN billing item ::[" + biBbn + "]" );
                biBbn = billingItemRepository.save(biBbn);
                vsb.addDetail(biBbn);

                 /*
                        OTR = dimaster motor(Regular Price)
                        BBN = dimaster motor(BBN)
                        OFF THE ROAD = OTR - BBN
                        SALES AMOUNT =  OFF THE ROAD / 1,1
                        DISCOUNT = total discount (subsidi MD, subsidi AHM, Diskon Dealer)
                        PRICE = Sales Amount - discount
                        PPN = sales amt * 10%
                        AR = BBN + Price + PPN

                    */
                axbbn = biayaBBN ;
                // off the road
                axsalesamount = axunitprice.subtract(axbbn );
                axsalesamount = axsalesamount.multiply(BigDecimal.TEN).divide(BigDecimal.valueOf(11), 1, RoundingMode.HALF_UP);
                axdisc = subsidiAHM.add(subsidiDealer).add(subsidiMD).add(subsidifincomp) ;
                axprice = axsalesamount.subtract(axdisc);
                axppn = axsalesamount.divide(BigDecimal.valueOf(10), 1, RoundingMode.HALF_UP);
                axaramt = axbbn.add(axprice).add(axppn);

                vsb.setAxunitprice(axunitprice);
                vsb.setAxbbn(axbbn);
                vsb.setAxsalesamount(axsalesamount);
                vsb.setAxdisc(axdisc);
                vsb.setAxprice(axprice);
                vsb.setAxppn(axppn);
                vsb.setAxaramt(axaramt);
                vehicleSalesBillingRepository.save(vsb);

                // end Component billing
                System.out.println("XXXXXXXXXXXXXXXXXXX " +  oi.toString());
            }
        }

        if (lvsb.size() > 0) {
            log.debug("vsb:build_from -> save vehicleSalesBillingRepository "  );
            for (VehicleSalesBilling o : lvsb) {
                vehicleSalesBillingRepository.save(o);
            }
            // Update VSO
            log.debug("vsb:build_from -> save vso "  );
            vsoRepo.save(v);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void cancelByVSO(UUID vsoId){

        VehicleSalesOrder vso = vehicleSalesOrderRepository.findByIdWithEagerDetail(vsoId);
        for(OrderItem oi : vso.getDetails()){
            for( OrderShipmentItem si : oi.getShipmentItems() ){
                si.setQty(0D);
                si.getShipmentItem().getShipment().setStatus(STATUS_CANCEL);
                shipmentItemRepository.save(si.getShipmentItem());
                log.debug("cancel shipment=>");

                // ItemIssuance issuance = issuanceRepository.findByShipmentItem(si.getShipmentItem());
                List<ItemIssuance> itemIssuances = issuanceRepository.findByShipmentList(si.getShipmentItem());
                itemIssuances.stream().forEach(issuance -> {
                    log.debug("iterant issuance =>{} ", issuance);
                    issuance.setQty(0D);
                    issuance.setInventoryItem(null);
                    issuanceRepository.save(issuance);
                    log.debug("save issuance {} ", issuance);
                });

                log.debug("cancel issuance=>");
            }

            log.debug("cancel picking start ");
            List<PickingSlip> pses = repoPis.findPisByOrderItem(oi);
            for(PickingSlip ps : pses){
                ps.setStatus(BaseConstants.STATUS_CANCEL);
                repoPis.save(ps);
                log.debug("cancel picking=>");
            }
        }
    }

    public VehicleSalesBillingDTO assignVSO (VehicleSalesBillingDTO vsbDTO) {
        List<VehicleSalesOrder> listvso =  vehicleSalesOrderRepository.findListByBillingId(vsbDTO.getIdBilling());
        if (!listvso.isEmpty()) {
            vsbDTO.setDateOrder(listvso.get(0).getDateOrder());
        }
        return vsbDTO;
    }

    public VehicleSalesBillingDTO assingBillType (VehicleSalesBillingDTO dto) {
        Billing bill = billingRepository.findOne(dto.getIdBilling());
        if (bill.getBillingType() !=null) {
            dto.setTypeId(bill.getBillingType().getIdBillingType());
        }
        return dto;
    }

    public void cronUpdate() {
        IAtomicLong v = hz.getAtomicLong("vsb-cron-reindexer-"+ LocalDate.now().toString());
        if (v.get() == 0) {
            v.set(1);
            List<VehicleSalesBilling> l = vehicleSalesBillingRepository.findAllEager();
            for (VehicleSalesBilling vsb: l) {
                VehicleSalesBillingDTO vsbd = vehicleSalesBillingMapper.toDto(vsb);
                producerTemplate.sendBody(IndexerRoute.INDEXER_VSB, vsbd);
                System.out.println("Append vsb" + vsbd.toString());
            }
        }
    }

    public void reindexer(@Body VehicleSalesBillingDTO vsbd) {
        vehicleSalesBillingSearchRepository.delete(vsbd.getIdBilling());
        vehicleSalesBillingSearchRepository.save(vsbd);
        System.out.println("Save Data vsb " + vsbd.toString());
    }

    public VehicleSalesBillingDTO assignSur (VehicleSalesBillingDTO dto) {
        Page<VehicleSalesOrder> vso = vehicleSalesOrderRepository.queryByNoIvu(dto.getBillingNumber(), new PageRequest(0,1));

        if (vso.getSize() > 0) {
            SalesUnitRequirement sur = vehicleSalesOrderRepository.getSurById(vso.getContent().iterator().next().getIdSalesUnitRequirement());
            dto.setOffTheRoad(sur.getHetprice());
        }


        return dto;
    }

    @Transactional
    public void requestTax (VehicleSalesBillingDTO dto) {
        log.debug("request tax vehiclesalesbilling == " + dto);
        VehicleSalesBilling vsb = vehicleSalesBillingRepository.findOne(dto.getIdBilling());

        if (vsb != null){
            log.debug("idrequest tax vehiclesalesbilling == " + dto.getRequestTaxInvoice());
            vsb.setRequestTaxInvoice(1);
            vehicleSalesBillingRepository.save(vsb);
            log.debug("save tax vehiclesalesbilling == " + vsb.getRequestTaxInvoice());
        }
    }

    //TODO: REMARK JIKA INGIN COBA ELASTIC DI LOCAL
//    @Transactional
//    @PostConstruct
//    public void initForMe() {
//        List<VehicleSalesBilling> l = vehicleSalesBillingRepository.findAllEager();
//        for (VehicleSalesBilling vsb: l) {
//            VehicleSalesBillingDTO vsbd = vehicleSalesBillingMapper.toDto(vsb);
//            vehicleSalesBillingSearchRepository.save(vsbd);
//            System.out.println("Data " + vsbd.toString());
//        }
//    }
}
