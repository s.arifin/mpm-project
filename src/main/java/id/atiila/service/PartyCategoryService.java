package id.atiila.service;

import id.atiila.domain.PartyCategory;
import id.atiila.repository.PartyCategoryRepository;
import id.atiila.repository.search.PartyCategorySearchRepository;
import id.atiila.service.dto.PartyCategoryDTO;
import id.atiila.service.mapper.PartyCategoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing PartyCategory.
 * BeSmart Team
 */

@Service
@Transactional
public class PartyCategoryService {

    private final Logger log = LoggerFactory.getLogger(PartyCategoryService.class);

    private final PartyCategoryRepository partyCategoryRepository;

    private final PartyCategoryMapper partyCategoryMapper;

    private final PartyCategorySearchRepository partyCategorySearchRepository;

    public PartyCategoryService(PartyCategoryRepository partyCategoryRepository, PartyCategoryMapper partyCategoryMapper, PartyCategorySearchRepository partyCategorySearchRepository) {
        this.partyCategoryRepository = partyCategoryRepository;
        this.partyCategoryMapper = partyCategoryMapper;
        this.partyCategorySearchRepository = partyCategorySearchRepository;
    }

    /**
     * Save a partyCategory.
     *
     * @param partyCategoryDTO the entity to save
     * @return the persisted entity
     */
    public PartyCategoryDTO save(PartyCategoryDTO partyCategoryDTO) {
        log.debug("Request to save PartyCategory : {}", partyCategoryDTO);
        PartyCategory partyCategory = partyCategoryMapper.toEntity(partyCategoryDTO);
        partyCategory = partyCategoryRepository.save(partyCategory);
        PartyCategoryDTO result = partyCategoryMapper.toDto(partyCategory);
        partyCategorySearchRepository.save(partyCategory);
        return result;
    }

    /**
     * Get all the partyCategories.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartyCategoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PartyCategories");
        return partyCategoryRepository.findAll(pageable)
            .map(partyCategoryMapper::toDto);
    }

    /**
     * Get one partyCategory by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PartyCategoryDTO findOne(Integer id) {
        log.debug("Request to get PartyCategory : {}", id);
        PartyCategory partyCategory = partyCategoryRepository.findOne(id);
        return partyCategoryMapper.toDto(partyCategory);
    }

    /**
     * Delete the partyCategory by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete PartyCategory : {}", id);
        partyCategoryRepository.delete(id);
        partyCategorySearchRepository.delete(id);
    }

    /**
     * Search for the partyCategory corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartyCategoryDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of PartyCategories for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idCategoryType = request.getParameter("idCategoryType");

        if (idCategoryType != null) {
            q.withQuery(matchQuery("categoryType.idCategoryType", idCategoryType));
        }

        Page<PartyCategory> result = partyCategorySearchRepository.search(q.build().getQuery(), pageable);
        return result.map(partyCategoryMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PartyCategoryDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered PartyCategoryDTO");
        String idCategoryType = request.getParameter("idCategoryType");

        if (idCategoryType != null) {
            return partyCategoryRepository.queryByIdCategoryType(Integer.valueOf(idCategoryType), pageable).map(partyCategoryMapper::toDto);
        }

        return partyCategoryRepository.queryNothing(pageable).map(partyCategoryMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, PartyCategoryDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<PartyCategoryDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        partyCategorySearchRepository.deleteAll();
        List<PartyCategory> partyCategorys = partyCategoryRepository.findAll();
        for (PartyCategory m: partyCategorys) {
            partyCategorySearchRepository.save(m);
            log.debug("Data partyCategory save !...");
        }
    }

}
