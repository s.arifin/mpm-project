package id.atiila.service.ax.dto;

import java.util.UUID;

public class AxCustomerDTO {

    private String dataArea;

    private String accountNum;

    private String name;

    private String custGroup;

    private String paymentTermId;

    private String city;

    private String state;

    private String locationName;

    private String address;

    private String zipCode;

    private String mainAccount;

    private String dimensi1;

    private String dimensi2;

    private String dimensi3;

    private String dimensi4;

    private String dimensi5;

    private String dimensi6;

    private String taxRegId;

    private String telephone;

    public String getDataArea() {
        return dataArea;
    }

    public void setDataArea(String dataArea) {
        this.dataArea = dataArea;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustGroup() {
        return custGroup;
    }

    public void setCustGroup(String custGroup) {
        this.custGroup = custGroup;
    }

    public String getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(String paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(String mainAccount) {
        this.mainAccount = mainAccount;
    }

    public String getDimensi1() {
        return dimensi1;
    }

    public void setDimensi1(String dimensi1) {
        this.dimensi1 = dimensi1;
    }

    public String getDimensi2() {
        return dimensi2;
    }

    public void setDimensi2(String dimensi2) {
        this.dimensi2 = dimensi2;
    }

    public String getDimensi3() {
        return dimensi3;
    }

    public void setDimensi3(String dimensi3) {
        this.dimensi3 = dimensi3;
    }

    public String getDimensi4() {
        return dimensi4;
    }

    public void setDimensi4(String dimensi4) {
        this.dimensi4 = dimensi4;
    }

    public String getDimensi5() {
        return dimensi5;
    }

    public void setDimensi5(String dimensi5) {
        this.dimensi5 = dimensi5;
    }

    public String getDimensi6() {
        return dimensi6;
    }

    public void setDimensi6(String dimensi6) {
        this.dimensi6 = dimensi6;
    }

    public String getTaxRegId() {
        return taxRegId;
    }

    public void setTaxRegId(String taxRegId) {
        this.taxRegId = taxRegId;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
