package id.atiila.service.ax.dto;

public class AxVendorDTO {

    private String address;

    private String apReqPONum;

    private String city;

    private String dataArea;

    private String dimensi1;

    private String dimensi2;

    private String dimensi3;

    private String dimensi4;

    private String dimensi5;

    private String dimensi6;

    private String locationName;

    private String mainAccount;

    private String mainDealer;

    private String name;

    private String paymentTermId;

    private String state;

    private String taxRegId;

    private String telephone;

    private String vendAccount;

    private String vendGroup;

    private String zipCode;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getApReqPONum() {
        return apReqPONum;
    }

    public void setApReqPONum(String apReqPONum) {
        this.apReqPONum = apReqPONum;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDataArea() {
        return dataArea;
    }

    public void setDataArea(String dataArea) {
        this.dataArea = dataArea;
    }

    public String getDimensi1() {
        return dimensi1;
    }

    public void setDimensi1(String dimensi1) {
        this.dimensi1 = dimensi1;
    }

    public String getDimensi2() {
        return dimensi2;
    }

    public void setDimensi2(String dimensi2) {
        this.dimensi2 = dimensi2;
    }

    public String getDimensi3() {
        return dimensi3;
    }

    public void setDimensi3(String dimensi3) {
        this.dimensi3 = dimensi3;
    }

    public String getDimensi4() {
        return dimensi4;
    }

    public void setDimensi4(String dimensi4) {
        this.dimensi4 = dimensi4;
    }

    public String getDimensi5() {
        return dimensi5;
    }

    public void setDimensi5(String dimensi5) {
        this.dimensi5 = dimensi5;
    }

    public String getDimensi6() {
        return dimensi6;
    }

    public void setDimensi6(String dimensi6) {
        this.dimensi6 = dimensi6;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(String mainAccount) {
        this.mainAccount = mainAccount;
    }

    public String getMainDealer() {
        return mainDealer;
    }

    public void setMainDealer(String mainDealer) {
        this.mainDealer = mainDealer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaymentTermId() {
        return paymentTermId;
    }

    public void setPaymentTermId(String paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTaxRegId() {
        return taxRegId;
    }

    public void setTaxRegId(String taxRegId) {
        this.taxRegId = taxRegId;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getVendAccount() {
        return vendAccount;
    }

    public void setVendAccount(String vendAccount) {
        this.vendAccount = vendAccount;
    }

    public String getVendGroup() {
        return vendGroup;
    }

    public void setVendGroup(String vendGroup) {
        this.vendGroup = vendGroup;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
