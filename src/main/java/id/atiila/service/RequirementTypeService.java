package id.atiila.service;

import id.atiila.domain.RequirementType;
import id.atiila.repository.RequirementTypeRepository;
import id.atiila.repository.search.RequirementTypeSearchRepository;
import id.atiila.service.dto.RequirementTypeDTO;
import id.atiila.service.mapper.RequirementTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing RequirementType.
 * BeSmart Team
 */

@Service
@Transactional
public class RequirementTypeService {

    private final Logger log = LoggerFactory.getLogger(RequirementTypeService.class);

    private final RequirementTypeRepository requirementTypeRepository;

    private final RequirementTypeMapper requirementTypeMapper;

    private final RequirementTypeSearchRepository requirementTypeSearchRepository;

    public RequirementTypeService(RequirementTypeRepository requirementTypeRepository, RequirementTypeMapper requirementTypeMapper, RequirementTypeSearchRepository requirementTypeSearchRepository) {
        this.requirementTypeRepository = requirementTypeRepository;
        this.requirementTypeMapper = requirementTypeMapper;
        this.requirementTypeSearchRepository = requirementTypeSearchRepository;
    }

    /**
     * Save a requirementType.
     *
     * @param requirementTypeDTO the entity to save
     * @return the persisted entity
     */
    public RequirementTypeDTO save(RequirementTypeDTO requirementTypeDTO) {
        log.debug("Request to save RequirementType : {}", requirementTypeDTO);
        RequirementType requirementType = requirementTypeMapper.toEntity(requirementTypeDTO);
        requirementType = requirementTypeRepository.save(requirementType);
        RequirementTypeDTO result = requirementTypeMapper.toDto(requirementType);
        requirementTypeSearchRepository.save(requirementType);
        return result;
    }

    /**
     *  Get all the requirementTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequirementTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RequirementTypes");
        return requirementTypeRepository.findAll(pageable)
            .map(requirementTypeMapper::toDto);
    }

    /**
     *  Get one requirementType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public RequirementTypeDTO findOne(Integer id) {
        log.debug("Request to get RequirementType : {}", id);
        RequirementType requirementType = requirementTypeRepository.findOne(id);
        return requirementTypeMapper.toDto(requirementType);
    }

    /**
     *  Delete the  requirementType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete RequirementType : {}", id);
        requirementTypeRepository.delete(id);
        requirementTypeSearchRepository.delete(id);
    }

    /**
     * Search for the requirementType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequirementTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of RequirementTypes for query {}", query);
        Page<RequirementType> result = requirementTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(requirementTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        requirementTypeSearchRepository.deleteAll();
        List<RequirementType> requirementtypes =  requirementTypeRepository.findAll();
        for (RequirementType m: requirementtypes) {
            requirementTypeSearchRepository.save(m);
            log.debug("Data requirement save !...");
        }
    }

}
