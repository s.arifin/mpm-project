package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.*;
import id.atiila.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;

@Service
@Transactional
public class PositionUtils {

    @Autowired
    private PositionRepository positionRepository;

    @Autowired
    private PositionReportingStructureRepository positionStructureRepository;

    @Autowired
    private PositionFullfillmentRepository positionFillRepository;

    @Autowired
    private PositionTypeRepository positionTypeRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    public Position buildPosition(Internal internal, Integer idPosType, Integer sequence) {
        Position pos = positionRepository.findPositionByInternalAndPositionType_IdPositionTypeAndSequenceNumber(internal, idPosType, sequence);
        if (pos == null) {
            PositionType posType = positionTypeRepository.findOne(idPosType);
            Assert.notNull(posType, "Position " + idPosType + " not defined !");

            pos = new Position();
            pos.setInternal(internal);
            pos.setPositionType(posType);
            pos.setSequenceNumber(sequence);
            positionRepository.save(pos);

            // Buat Posisi untuk person
            buildPositionFilled(pos, null);
        }
        return pos;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void assignPosition(Position position, UserMediator um) {
        position.setOwner(um);
        positionRepository.save(position);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public PositionReportingStructure buildStructure(Position to, Position from) {
        PositionReportingStructure post = positionStructureRepository.findStructure(to, from);
        if (post == null) {
            post = new PositionReportingStructure();
            post.setPositionFrom(from);
            post.setPositionTo(to);
            positionStructureRepository.save(post);
        }
        return post;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public PositionFullfillment buildPositionFilled(Position post, Person person) {
        PositionFullfillment posf = positionFillRepository.findPosition(post);
        if (posf == null) {
            posf = new PositionFullfillment();
            posf.setPosition(post);
            posf.setPerson(person);
            positionFillRepository.save(posf);
        }
        else if (posf != null && posf.getPerson() == null){
            posf.setPerson(person);
            positionFillRepository.save(posf);
        }
        return posf;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public PositionType buildPositionType(Integer idPosType, String title, Authority ...auth) {
        PositionType r = positionTypeRepository.findOne(idPosType);
        if (r == null) {
            r = new PositionType();
            r.setIdPositionType(idPosType);
            r.setDescription(title);
            r.setTitle(title);
            for (Authority o: auth) {
                r.getAuthorities().add(o);
            }
            positionTypeRepository.save(r);
        }
        return r;
    }

    public Authority getAuthority(String id) {
        Authority authority = authorityRepository.findOne(id);
        if (authority == null) {
            authority = new Authority();
            authority.setName(id);
            authorityRepository.save(authority);
        }
        return authority;
    }

    @PostConstruct
    @Transactional(propagation = Propagation.REQUIRED)
    public void buildDefaultPositionType() {
        if (positionTypeRepository.findAll().isEmpty()) {
            buildPositionType(BaseConstants.POSITION_KABENG, "Kepala Bengkel", getAuthority(BaseConstants.AUTH_KABENG));
            buildPositionType(BaseConstants.POSITION_KACAB, "Kepala Cabang", getAuthority(BaseConstants.AUTH_KACAB));
            buildPositionType(BaseConstants.POSITION_KORSAL, "Koordinator Sales", getAuthority(BaseConstants.AUTH_KORSAL));
            buildPositionType(BaseConstants.POSITION_SALESMAN, "Salesman", getAuthority(BaseConstants.AUTH_SALESMAN));
            buildPositionType(BaseConstants.POSITION_SERV_ANALYS, "Kepala Bengkel", getAuthority(BaseConstants.AUTH_SERV_ANALYS));
            buildPositionType(BaseConstants.POSITION_MEKANIK, "Mekanik", getAuthority(BaseConstants.AUTH_MEKANIK));
            buildPositionType(BaseConstants.POSITION_AFC, "AFC", getAuthority(BaseConstants.AUTH_AFC));
            buildPositionType(BaseConstants.POSITION_ADMSALES, "Admin Sales", getAuthority(BaseConstants.AUTH_ADMSALES));
            buildPositionType(BaseConstants.POSITION_FRONTDESK, "Front Desk", getAuthority(BaseConstants.AUTH_FRONTDESK));
            buildPositionType(BaseConstants.POSITION_DRIVER, "Driver", getAuthority(BaseConstants.AUTH_DRIVER));
            buildPositionType(BaseConstants.POSITION_PDIMAN, "PDI Man", getAuthority(BaseConstants.AUTH_PDIMAN));
            buildPositionType(BaseConstants.POSITION_KASIR, "Kasir", getAuthority(BaseConstants.AUTH_KASIR));
        }
    }

}
