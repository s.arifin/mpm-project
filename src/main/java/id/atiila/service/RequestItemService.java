package id.atiila.service;

import id.atiila.base.DmsException;
import id.atiila.base.executeprocess.RequestItemConstants;
import id.atiila.domain.RequestItem;
import id.atiila.repository.RequestItemRepository;
import id.atiila.repository.search.RequestItemSearchRepository;
import id.atiila.service.dto.RequestItemDTO;
import id.atiila.service.mapper.RequestItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing RequestItem.
 * BeSmart Team
 */

@Service
@Transactional
public class RequestItemService {

    private final Logger log = LoggerFactory.getLogger(RequestItemService.class);

    private final RequestItemRepository requestItemRepository;

    private final RequestItemMapper requestItemMapper;

    private final RequestItemSearchRepository requestItemSearchRepository;

    public RequestItemService(RequestItemRepository requestItemRepository, RequestItemMapper requestItemMapper, RequestItemSearchRepository requestItemSearchRepository) {
        this.requestItemRepository = requestItemRepository;
        this.requestItemMapper = requestItemMapper;
        this.requestItemSearchRepository = requestItemSearchRepository;
    }

    /**
     * Save a requestItem.
     *
     * @param requestItemDTO the entity to save
     * @return the persisted entity
     */
    public RequestItemDTO save(RequestItemDTO requestItemDTO) {
        log.debug("Request to save RequestItem : {}", requestItemDTO);
        RequestItem requestItem = requestItemMapper.toEntity(requestItemDTO);
        requestItem = requestItemRepository.save(requestItem);
        RequestItemDTO result = requestItemMapper.toDto(requestItem);
        requestItemSearchRepository.save(requestItem);
        return result;
    }

    /**
     * Get all the requestItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequestItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RequestItems");
        return requestItemRepository.findAll(pageable)
            .map(requestItemMapper::toDto);
    }

    /**
     * Get one requestItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public RequestItemDTO findOne(UUID id) {
        log.debug("Request to get RequestItem : {}", id);
        RequestItem requestItem = requestItemRepository.findOne(id);
        return requestItemMapper.toDto(requestItem);
    }

    /**
     * Delete the requestItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete RequestItem : {}", id);
        requestItemRepository.delete(id);
        requestItemSearchRepository.delete(id);
    }

    private void validationQtyTransfer(Set<RequestItemDTO> dtos){
        for(RequestItemDTO a : dtos){
            if (a.getQtyTransfer() > a.getQtyReq()) {
                throw new DmsException("Qty yang di transfer lebih besar dari yang di request");
            }
        }
    }

    /**
     * Search for the requestItem corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequestItemDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of RequestItems for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idRequest = request.getParameter("idRequest");

        if (filterName != null) {
        }
        Page<RequestItem> result = requestItemSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(requestItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<RequestItemDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered RequestItemDTO");
        UUID idRequest = UUID.fromString(request.getParameter("idRequest"));

        return requestItemRepository.findByParams(idRequest, pageable)
            .map(requestItemMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public RequestItemDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        RequestItemDTO r = null;
        return r;
    }

    @Transactional
    public void saveBulk(Set<RequestItemDTO> list){
        for(RequestItemDTO a : list){
            this.save(a);
        }
    }

    @Transactional
    public Set<RequestItemDTO> processExecuteListData(Integer id, String param, Set<RequestItemDTO> dto) {
        Set<RequestItemDTO> r = dto;
        if (r != null) {
            switch (id) {
                case RequestItemConstants.BULK_SAVE:
                    validationQtyTransfer(r);
                    saveBulk(r);
                    break;
                default:
                    break;
            }
        }
        return r;
    }

}
