package id.atiila.service;

import id.atiila.domain.CommunicationEventCDB;
import id.atiila.repository.CommunicationEventCDBRepository;
import id.atiila.repository.search.CommunicationEventCDBSearchRepository;
import id.atiila.service.dto.CommunicationEventCDBDTO;
import id.atiila.service.mapper.CommunicationEventCDBMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing CommunicationEventCDB.
 * BeSmart Team
 */

@Service
@Transactional
public class CommunicationEventCDBService {

    private final Logger log = LoggerFactory.getLogger(CommunicationEventCDBService.class);

    private final CommunicationEventCDBRepository communicationEventCDBRepository;

    private final CommunicationEventCDBMapper communicationEventCDBMapper;

    private final CommunicationEventCDBSearchRepository communicationEventCDBSearchRepository;

    public CommunicationEventCDBService(CommunicationEventCDBRepository communicationEventCDBRepository, CommunicationEventCDBMapper communicationEventCDBMapper, CommunicationEventCDBSearchRepository communicationEventCDBSearchRepository) {
        this.communicationEventCDBRepository = communicationEventCDBRepository;
        this.communicationEventCDBMapper = communicationEventCDBMapper;
        this.communicationEventCDBSearchRepository = communicationEventCDBSearchRepository;
    }

    /**
     * Save a communicationEventCDB.
     *
     * @param communicationEventCDBDTO the entity to save
     * @return the persisted entity
     */
    public CommunicationEventCDBDTO save(CommunicationEventCDBDTO communicationEventCDBDTO) {
        log.debug("Request to save CommunicationEventCDB : {}", communicationEventCDBDTO);
        CommunicationEventCDB communicationEventCDB = communicationEventCDBMapper.toEntity(communicationEventCDBDTO);
        communicationEventCDB = communicationEventCDBRepository.save(communicationEventCDB);
        CommunicationEventCDBDTO result = communicationEventCDBMapper.toDto(communicationEventCDB);
        communicationEventCDBSearchRepository.save(communicationEventCDB);
        log.debug("save to DB: "+ communicationEventCDB);
        log.debug("save to DB(result): "+ result);
        return result;
    }

    /**
     *  Get all the communicationEventCDBS.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CommunicationEventCDBDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CommunicationEventCDBS");
        return communicationEventCDBRepository.findActiveCommunicationEventCDB(pageable)
            .map(communicationEventCDBMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<CommunicationEventCDBDTO> findAllByIdCustomer(Pageable pageable, String idCustomer) {
        log.debug("Request to get all CommunicationEventCDBS by IdCustomer");
        return communicationEventCDBRepository.findActiveCommunicationEventCDBByIdCustomer(idCustomer, pageable)
            .map(communicationEventCDBMapper::toDto);
    }

    /**
     *  Get one communicationEventCDB by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CommunicationEventCDBDTO findOne(UUID id) {
        log.debug("Request to get CommunicationEventCDB = {}", id);
        CommunicationEventCDB communicationEventCDB = communicationEventCDBRepository.findOne(id);
        return communicationEventCDBMapper.toDto(communicationEventCDB);
    }

    /**
     *  Delete the  communicationEventCDB by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete CommunicationEventCDB ===== {}", id);
        communicationEventCDBRepository.delete(id);
        communicationEventCDBSearchRepository.delete(id);
    }

    /**
     * Search for the communicationEventCDB corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CommunicationEventCDBDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CommunicationEventCDBS for query {}", query);
        Page<CommunicationEventCDB> result = communicationEventCDBSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(communicationEventCDBMapper::toDto);
    }

    public CommunicationEventCDBDTO processExecuteData(Integer id, String param, CommunicationEventCDBDTO dto) {
        CommunicationEventCDBDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<CommunicationEventCDBDTO> processExecuteListData(Integer id, String param, Set<CommunicationEventCDBDTO> dto) {
        Set<CommunicationEventCDBDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public CommunicationEventCDBDTO changeCommunicationEventCDBStatus(CommunicationEventCDBDTO dto, Integer id) {
        if (dto != null) {
			CommunicationEventCDB e = communicationEventCDBRepository.findOne(dto.getIdCommunicationEvent());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        communicationEventCDBSearchRepository.delete(dto.getIdCommunicationEvent());
                        break;
                    default:
                        communicationEventCDBSearchRepository.save(e);
                }
				communicationEventCDBRepository.save(e);
			}
		}
        return dto;
    }
}
