package id.atiila.service;

import id.atiila.domain.Internal;
import id.atiila.domain.SalesTarget;
import id.atiila.domain.Salesman;
import id.atiila.repository.SalesTargetRepository;
import id.atiila.repository.SalesmanRepository;
import id.atiila.repository.search.SalesTargetSearchRepository;
import id.atiila.service.dto.SalesTargetDTO;
import id.atiila.service.mapper.SalesTargetMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SalesTarget.
 */
@Service
@Transactional
public class SalesTargetService {

    private final Logger log = LoggerFactory.getLogger(SalesTargetService.class);

    private final SalesTargetRepository salesTargetRepository;

    private final SalesTargetMapper salesTargetMapper;

    private final SalesTargetSearchRepository salesTargetSearchRepository;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private SalesmanRepository salesmanRepository;

    public SalesTargetService(SalesTargetRepository salesTargetRepository, SalesTargetMapper salesTargetMapper, SalesTargetSearchRepository salesTargetSearchRepository) {
        this.salesTargetRepository = salesTargetRepository;
        this.salesTargetMapper = salesTargetMapper;
        this.salesTargetSearchRepository = salesTargetSearchRepository;
    }

    /**
     * Save a salesTarget.
     *
     * @param salesTargetDTO the entity to save
     * @return the persisted entity
     */
    public SalesTargetDTO save(SalesTargetDTO salesTargetDTO) {
        log.debug("Request to save SalesTarget : {}", salesTargetDTO);
        SalesTarget salesTarget = salesTargetMapper.toEntity(salesTargetDTO);
        salesTarget = salesTargetRepository.save(salesTarget);
        SalesTargetDTO result = salesTargetMapper.toDto(salesTarget);
        salesTargetSearchRepository.save(salesTarget);
        return result;
    }

    /**
     * Get all the salesTargets.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesTargetDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SalesTargets");
        String idInternal = partyUtils.getCurrentInternal().getIdInternal();
        log.debug("internal login" + idInternal);
        return salesTargetRepository.findAllActive(idInternal, pageable)
            .map(salesTargetMapper::toDto)
            .map(this::assignSales);
    }

    /**
     * Get one salesTarget by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SalesTargetDTO findOne(UUID id) {
        log.debug("Request to get SalesTarget : {}", id);
        SalesTarget salesTarget = salesTargetRepository.findOne(id);
        return salesTargetMapper.toDto(salesTarget);
    }

    /**
     * Delete the  salesTarget by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete SalesTarget : {}", id);
        salesTargetRepository.delete(id);
        salesTargetSearchRepository.delete(id);
    }

    /**
     * Search for the salesTarget corresponding to the query.
     *
     * @param query    the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SalesTargetDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SalesTargets for query {}", query);
        Page<SalesTarget> result = salesTargetSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(salesTargetMapper::toDto);
    }

    @Transactional(readOnly = true)
    public SalesTargetDTO assignSales(SalesTargetDTO dto) {
        log.debug("salesman assign sales : " + dto.getIdsalesman());
        if (dto.getIdsalesman() != null) {
            Salesman salesman = salesmanRepository.findOne(dto.getIdsalesman());
            if (salesman != null) {
                dto.setNamaSales(salesman.getPerson().getName());
            }
        }
        return  dto;
    }
}
