package id.atiila.service;

import id.atiila.base.XlsxUtils;
import id.atiila.domain.VendorType;
import id.atiila.repository.VendorTypeRepository;
import id.atiila.repository.search.VendorTypeSearchRepository;
import id.atiila.service.dto.CustomVendorTypeDTO;
import id.atiila.service.dto.VendorTypeDTO;
import id.atiila.service.mapper.VendorTypeMapper;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing VendorType.
 * BeSmart Team
 */

@Service
@Transactional
public class VendorTypeService {

    private final Logger log = LoggerFactory.getLogger(VendorTypeService.class);

    private final VendorTypeRepository vendorTypeRepository;

    private final VendorTypeMapper vendorTypeMapper;

    private final VendorTypeSearchRepository vendorTypeSearchRepository;

    public VendorTypeService(VendorTypeRepository vendorTypeRepository, VendorTypeMapper vendorTypeMapper, VendorTypeSearchRepository vendorTypeSearchRepository) {
        this.vendorTypeRepository = vendorTypeRepository;
        this.vendorTypeMapper = vendorTypeMapper;
        this.vendorTypeSearchRepository = vendorTypeSearchRepository;
    }

    /**
     * Save a vendorType.
     *
     * @param vendorTypeDTO the entity to save
     * @return the persisted entity
     */
    public VendorTypeDTO save(VendorTypeDTO vendorTypeDTO) {
        log.debug("Request to save VendorType : {}", vendorTypeDTO);
        VendorType vendorType = vendorTypeMapper.toEntity(vendorTypeDTO);
        vendorType = vendorTypeRepository.save(vendorType);
        VendorTypeDTO result = vendorTypeMapper.toDto(vendorType);
        vendorTypeSearchRepository.save(vendorType);
        return result;
    }

    /**
     *  Get all the vendorTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VendorTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VendorTypes");
        return vendorTypeRepository.findAll(pageable)
            .map(vendorTypeMapper::toDto);
    }

    /**
     *  Get one vendorType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public VendorTypeDTO findOne(Integer id) {
        log.debug("Request to get VendorType : {}", id);
        VendorType vendorType = vendorTypeRepository.findOne(id);
        return vendorTypeMapper.toDto(vendorType);
    }

    /**
     *  Delete the  vendorType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete VendorType : {}", id);
        vendorTypeRepository.delete(id);
        vendorTypeSearchRepository.delete(id);
    }

    /**
     * Search for the vendorType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VendorTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of VendorTypes for query {}", query);
        Page<VendorType> result = vendorTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(vendorTypeMapper::toDto);
    }

    public VendorTypeDTO processExecuteData(Integer id, String param, VendorTypeDTO dto) {
        VendorTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<VendorTypeDTO> processExecuteListData(Integer id, String param, Set<VendorTypeDTO> dto) {
        Set<VendorTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }
    @PostConstruct
    @Transactional
    public void initialize() {
        try {
            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/vendor_type.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/vendor_type.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<CustomVendorTypeDTO> vendors = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (CustomVendorTypeDTO o: vendors) {
                    // if (o.getParentGeoCode() != null && o.getGeocode() != null) {
                    //     addVillage(o.getParentGeoCode(), o.getGeocode(), o.getDescription());
                    // }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
