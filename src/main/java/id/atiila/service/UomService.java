package id.atiila.service;

import id.atiila.domain.Uom;
import id.atiila.repository.UomRepository;
import id.atiila.repository.search.UomSearchRepository;
import id.atiila.service.dto.UomDTO;
import id.atiila.service.mapper.UomMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing Uom.
 * BeSmart Team
 */

@Service
@Transactional
public class UomService {

    private final Logger log = LoggerFactory.getLogger(UomService.class);

    private final UomRepository uomRepository;

    private final UomMapper uomMapper;

    private final UomSearchRepository uomSearchRepository;

    public UomService(UomRepository uomRepository, UomMapper uomMapper, UomSearchRepository uomSearchRepository) {
        this.uomRepository = uomRepository;
        this.uomMapper = uomMapper;
        this.uomSearchRepository = uomSearchRepository;
    }

    /**
     * Save a uom.
     *
     * @param uomDTO the entity to save
     * @return the persisted entity
     */
    public UomDTO save(UomDTO uomDTO) {
        log.debug("Request to save Uom : {}", uomDTO);
        Uom uom = uomMapper.toEntity(uomDTO);
        uom = uomRepository.save(uom);
        UomDTO result = uomMapper.toDto(uom);
        uomSearchRepository.save(uom);
        return result;
    }

    /**
     * Get all the uoms.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UomDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Uoms");
        return uomRepository.findAll(pageable)
            .map(uomMapper::toDto);
    }

    /**
     * Get one uom by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public UomDTO findOne(String id) {
        log.debug("Request to get Uom : {}", id);
        Uom uom = uomRepository.findOne(id);
        return uomMapper.toDto(uom);
    }

    /**
     * Delete the uom by id.
     *
     * @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Uom : {}", id);
        uomRepository.delete(id);
        uomSearchRepository.delete(id);
    }

    /**
     * Search for the uom corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UomDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of Uoms for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idUomType = request.getParameter("idUomType");

        if (idUomType != null) {
            q.withQuery(matchQuery("uomType.idUomType", idUomType));
        }

        Page<Uom> result = uomSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(uomMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<UomDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered UomDTO");
        String idUomType = request.getParameter("idUomType");

        if (idUomType != null) {
            return uomRepository.queryByIdUomType(Integer.valueOf(idUomType), pageable).map(uomMapper::toDto);
        }

        return uomRepository.queryNothing(pageable).map(uomMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, UomDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<UomDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        uomSearchRepository.deleteAll();
        List<Uom> uoms =  uomRepository.findAll();
        for (Uom m: uoms) {
            uomSearchRepository.save(m);
            log.debug("Data uom save !...");
        }
    }
}
