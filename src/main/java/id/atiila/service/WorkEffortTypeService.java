package id.atiila.service;

import id.atiila.domain.WorkEffortType;
import id.atiila.repository.WorkEffortTypeRepository;
import id.atiila.repository.search.WorkEffortTypeSearchRepository;
import id.atiila.service.dto.WorkEffortTypeDTO;
import id.atiila.service.mapper.WorkEffortTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WorkEffortType.
 * BeSmart Team
 */

@Service
@Transactional
public class WorkEffortTypeService {

    private final Logger log = LoggerFactory.getLogger(WorkEffortTypeService.class);

    private final WorkEffortTypeRepository workEffortTypeRepository;

    private final WorkEffortTypeMapper workEffortTypeMapper;

    private final WorkEffortTypeSearchRepository workEffortTypeSearchRepository;

    public WorkEffortTypeService(WorkEffortTypeRepository workEffortTypeRepository, WorkEffortTypeMapper workEffortTypeMapper, WorkEffortTypeSearchRepository workEffortTypeSearchRepository) {
        this.workEffortTypeRepository = workEffortTypeRepository;
        this.workEffortTypeMapper = workEffortTypeMapper;
        this.workEffortTypeSearchRepository = workEffortTypeSearchRepository;
    }

    /**
     * Save a workEffortType.
     *
     * @param workEffortTypeDTO the entity to save
     * @return the persisted entity
     */
    public WorkEffortTypeDTO save(WorkEffortTypeDTO workEffortTypeDTO) {
        log.debug("Request to save WorkEffortType : {}", workEffortTypeDTO);
        WorkEffortType workEffortType = workEffortTypeMapper.toEntity(workEffortTypeDTO);
        workEffortType = workEffortTypeRepository.save(workEffortType);
        WorkEffortTypeDTO result = workEffortTypeMapper.toDto(workEffortType);
        workEffortTypeSearchRepository.save(workEffortType);
        return result;
    }

    /**
     *  Get all the workEffortTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WorkEffortTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WorkEffortTypes");
        return workEffortTypeRepository.findAll(pageable)
            .map(workEffortTypeMapper::toDto);
    }

    /**
     *  Get one workEffortType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public WorkEffortTypeDTO findOne(Integer id) {
        log.debug("Request to get WorkEffortType : {}", id);
        WorkEffortType workEffortType = workEffortTypeRepository.findOne(id);
        return workEffortTypeMapper.toDto(workEffortType);
    }

    /**
     *  Delete the  workEffortType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete WorkEffortType : {}", id);
        workEffortTypeRepository.delete(id);
        workEffortTypeSearchRepository.delete(id);
    }

    /**
     * Search for the workEffortType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WorkEffortTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WorkEffortTypes for query {}", query);
        Page<WorkEffortType> result = workEffortTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(workEffortTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        workEffortTypeSearchRepository.deleteAll();
        List<WorkEffortType> workEffortTypes =  workEffortTypeRepository.findAll();
        for (WorkEffortType m: workEffortTypes) {
            workEffortTypeSearchRepository.save(m);
            log.debug("Data work effort type save !...");
        }
    }

}
