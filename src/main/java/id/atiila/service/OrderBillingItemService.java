package id.atiila.service;

import id.atiila.domain.OrderBillingItem;
import id.atiila.repository.OrderBillingItemRepository;
import id.atiila.repository.search.OrderBillingItemSearchRepository;
import id.atiila.service.dto.OrderBillingItemDTO;
import id.atiila.service.mapper.OrderBillingItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing OrderBillingItem.
 * BeSmart Team
 */

@Service
@Transactional
public class OrderBillingItemService {

    private final Logger log = LoggerFactory.getLogger(OrderBillingItemService.class);

    private final OrderBillingItemRepository orderBillingItemRepository;

    private final OrderBillingItemMapper orderBillingItemMapper;

    private final OrderBillingItemSearchRepository orderBillingItemSearchRepository;

    public OrderBillingItemService(OrderBillingItemRepository orderBillingItemRepository, OrderBillingItemMapper orderBillingItemMapper, OrderBillingItemSearchRepository orderBillingItemSearchRepository) {
        this.orderBillingItemRepository = orderBillingItemRepository;
        this.orderBillingItemMapper = orderBillingItemMapper;
        this.orderBillingItemSearchRepository = orderBillingItemSearchRepository;
    }

    /**
     * Save a orderBillingItem.
     *
     * @param orderBillingItemDTO the entity to save
     * @return the persisted entity
     */
    public OrderBillingItemDTO save(OrderBillingItemDTO orderBillingItemDTO) {
        log.debug("Request to save OrderBillingItem : {}", orderBillingItemDTO);
        OrderBillingItem orderBillingItem = orderBillingItemMapper.toEntity(orderBillingItemDTO);
        orderBillingItem = orderBillingItemRepository.save(orderBillingItem);
        OrderBillingItemDTO result = orderBillingItemMapper.toDto(orderBillingItem);
        orderBillingItemSearchRepository.save(orderBillingItem);
        return result;
    }

    /**
     * Get all the orderBillingItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrderBillingItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrderBillingItems");
        return orderBillingItemRepository.findAll(pageable)
            .map(orderBillingItemMapper::toDto);
    }

    /**
     * Get one orderBillingItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public OrderBillingItemDTO findOne(UUID id) {
        log.debug("Request to get OrderBillingItem : {}", id);
        OrderBillingItem orderBillingItem = orderBillingItemRepository.findOne(id);
        return orderBillingItemMapper.toDto(orderBillingItem);
    }

    /**
     * Delete the orderBillingItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete OrderBillingItem : {}", id);
        orderBillingItemRepository.delete(id);
        orderBillingItemSearchRepository.delete(id);
    }

    /**
     * Search for the orderBillingItem corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrderBillingItemDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of OrderBillingItems for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idOrderItem = request.getParameter("idOrderItem");
        String idBillingItem = request.getParameter("idBillingItem");

        if (filterName != null) {
        }
        Page<OrderBillingItem> result = orderBillingItemSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(orderBillingItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<OrderBillingItemDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered OrderBillingItemDTO");
        UUID idOrderItem = request.getParameter("idOrderItem") == null ?
            null : UUID.fromString(request.getParameter("idOrderItem"));
        UUID idBillingItem = request.getParameter("idBillingItem") == null ?
            null : UUID.fromString(request.getParameter("idBillingItem"));

        return orderBillingItemRepository.findByParams(idOrderItem, idBillingItem, pageable)
            .map(orderBillingItemMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public OrderBillingItemDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        OrderBillingItemDTO r = null;
        return r;
    }

    @Transactional
    public Set<OrderBillingItemDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<OrderBillingItemDTO> r = new HashSet<>();
        return r;
    }

}
