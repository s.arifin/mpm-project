package id.atiila.service;

import id.atiila.domain.RelationType;
import id.atiila.repository.RelationTypeRepository;
import id.atiila.repository.search.RelationTypeSearchRepository;
import id.atiila.service.dto.RelationTypeDTO;
import id.atiila.service.mapper.RelationTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.*;


/**
 * Service Implementation for managing RelationType.
 * BeSmart Team
 */

@Service
@Transactional
public class RelationTypeService {

    private final Logger log = LoggerFactory.getLogger(RelationTypeService.class);

    private final RelationTypeRepository relationTypeRepository;

    private final RelationTypeMapper relationTypeMapper;

    private final RelationTypeSearchRepository relationTypeSearchRepository;

    public RelationTypeService(RelationTypeRepository relationTypeRepository, RelationTypeMapper relationTypeMapper, RelationTypeSearchRepository relationTypeSearchRepository) {
        this.relationTypeRepository = relationTypeRepository;
        this.relationTypeMapper = relationTypeMapper;
        this.relationTypeSearchRepository = relationTypeSearchRepository;
    }

    /**
     * Save a relationType.
     *
     * @param relationTypeDTO the entity to save
     * @return the persisted entity
     */
    public RelationTypeDTO save(RelationTypeDTO relationTypeDTO) {
        log.debug("Request to save RelationType : {}", relationTypeDTO);
        RelationType relationType = relationTypeMapper.toEntity(relationTypeDTO);
        relationType = relationTypeRepository.save(relationType);
        RelationTypeDTO result = relationTypeMapper.toDto(relationType);
        relationTypeSearchRepository.save(relationType);
        return result;
    }

    /**
     *  Get all the relationTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RelationTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RelationTypes");
        return relationTypeRepository.findAll(pageable)
            .map(relationTypeMapper::toDto);
    }

    /**
     *  Get one relationType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public RelationTypeDTO findOne(Integer id) {
        log.debug("Request to get RelationType : {}", id);
        RelationType relationType = relationTypeRepository.findOne(id);
        return relationTypeMapper.toDto(relationType);
    }

    /**
     *  Delete the  relationType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete RelationType : {}", id);
        relationTypeRepository.delete(id);
        relationTypeSearchRepository.delete(id);
    }

    /**
     * Search for the relationType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RelationTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of RelationTypes for query {}", query);
        Page<RelationType> result = relationTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(relationTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        relationTypeSearchRepository.deleteAll();
        List<RelationType> relationtypes = relationTypeRepository.findAll();
        for (RelationType m: relationtypes) {
            relationTypeSearchRepository.save(m);
            log.debug("Data relation type save !...");
        }
    }
}
