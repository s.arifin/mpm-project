package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import id.atiila.base.XlsxUtils;
import id.atiila.domain.District;
import id.atiila.domain.GeoBoundary;
import id.atiila.repository.DistrictRepository;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import id.atiila.domain.Village;
import id.atiila.repository.GeoBoundaryRepository;
import id.atiila.repository.VillageRepository;
import id.atiila.repository.search.VillageSearchRepository;
import id.atiila.service.dto.VillageDTO;
import id.atiila.service.mapper.VillageMapper;
import javax.annotation.PostConstruct;

/**
 * Service Implementation for managing Village.
 * BeSmart Team
 */

@Service
@Transactional
public class VillageService {
    private final Logger log = LoggerFactory.getLogger(VillageService.class);

    private final VillageRepository villageRepository;

    private final VillageMapper villageMapper;

    private final VillageSearchRepository villageSearchRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private GeoBoundaryRepository geoRepository;

    public VillageService(VillageRepository villageRepository, VillageMapper villageMapper, VillageSearchRepository villageSearchRepository, GeoBoundaryRepository geoBoundaryRepository) {
        this.villageRepository = villageRepository;
        this.villageMapper = villageMapper;
        this.villageSearchRepository = villageSearchRepository;
    }

    /**
     * Save a village.
     *
     * @param villageDTO the entity to save
     * @return the persisted entity
     */
    public VillageDTO save(VillageDTO villageDTO) {
        log.debug("Request to save Village : {}", villageDTO);
        Village village = villageMapper.toEntity(villageDTO);
        village = villageRepository.save(village);
        VillageDTO result = villageMapper.toDto(village);
        villageSearchRepository.save(village);
        return result;
    }

    /**
     *  Get all the villages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VillageDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Villages");
        return villageRepository.findAll(pageable)
            .map(villageMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<VillageDTO> findAllByDistrict(UUID idDistrict) {
        log.debug("Request to get all Villages");

        List<? extends GeoBoundary> villageGeos = geoRepository.findAllByParent(idDistrict);
        List<Village> villages = (List<Village>) villageGeos;

        return villageMapper.toDto(villages);
    }


    /**
     *  Get one village by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public VillageDTO findOne(UUID id) {
        log.debug("Request to get Village : {}", id);
        Village village = villageRepository.findOne(id);
        return villageMapper.toDto(village);
    }

    /**
     *  Delete the  village by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Village : {}", id);
        villageRepository.delete(id);
        villageSearchRepository.delete(id);
    }

    /**
     * Search for the village corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VillageDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Villages for query {}", query);
        Page<Village> result = villageSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(villageMapper::toDto);
    }

    public VillageDTO processExecuteData(Integer id, String param, VillageDTO dto) {
        VillageDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<VillageDTO> processExecuteListData(Integer id, String param, Set<VillageDTO> dto) {
        Set<VillageDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public void addVillage(String idDistrict, String id, String name) {
        Village p = villageRepository.findByGeoCode(id);
        if (p == null) {
            District parent = districtRepository.findByGeoCode(idDistrict);
            if (parent != null) {
                p = new Village();
                p.setGeoCode(id);
                p.setDescription(name);
                p.setParent(parent);
                villageRepository.save(p);
            }
        }
    }

    @Transactional
    public void initialize() {
        try {

            // Jika data sudah ada, abaikan
            if (!villageRepository.findAll().isEmpty()) return;

            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/village.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/village.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<VillageDTO> villages = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (VillageDTO o: villages) {
                    if (o.getParentGeoCode() != null && o.getGeoCode() != null) {
                        addVillage(o.getParentGeoCode(), o.getGeoCode(), o.getDescription());
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
