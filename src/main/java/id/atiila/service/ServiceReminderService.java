package id.atiila.service;

import id.atiila.domain.ServiceReminder;
import id.atiila.repository.ServiceReminderRepository;
import id.atiila.repository.search.ServiceReminderSearchRepository;
import id.atiila.service.dto.ServiceReminderDTO;
import id.atiila.service.mapper.ServiceReminderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ServiceReminder.
 * BeSmart Team
 */

@Service
@Transactional
public class ServiceReminderService {

    private final Logger log = LoggerFactory.getLogger(ServiceReminderService.class);

    private final ServiceReminderRepository serviceReminderRepository;

    private final ServiceReminderMapper serviceReminderMapper;

    private final ServiceReminderSearchRepository serviceReminderSearchRepository;
    public ServiceReminderService(ServiceReminderRepository serviceReminderRepository, ServiceReminderMapper serviceReminderMapper, ServiceReminderSearchRepository serviceReminderSearchRepository) {
        this.serviceReminderRepository = serviceReminderRepository;
        this.serviceReminderMapper = serviceReminderMapper;
        this.serviceReminderSearchRepository = serviceReminderSearchRepository;
    }

    /**
     * Save a serviceReminder.
     *
     * @param serviceReminderDTO the entity to save
     * @return the persisted entity
     */
    public ServiceReminderDTO save(ServiceReminderDTO serviceReminderDTO) {
        log.debug("Request to save ServiceReminder : {}", serviceReminderDTO);
        ServiceReminder serviceReminder = serviceReminderMapper.toEntity(serviceReminderDTO);
        serviceReminder = serviceReminderRepository.save(serviceReminder);
        ServiceReminderDTO result = serviceReminderMapper.toDto(serviceReminder);
        serviceReminderSearchRepository.save(serviceReminder);
        return result;
    }

    /**
     *  Get all the serviceReminders.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ServiceReminderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ServiceReminders");
        return serviceReminderRepository.findAll(pageable)
            .map(serviceReminderMapper::toDto);
    }

    /**
     *  Get one serviceReminder by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ServiceReminderDTO findOne(Integer id) {
        log.debug("Request to get ServiceReminder : {}", id);
        ServiceReminder serviceReminder = serviceReminderRepository.findOne(id);
        return serviceReminderMapper.toDto(serviceReminder);
    }

    /**
     *  Delete the  serviceReminder by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete ServiceReminder : {}", id);
        serviceReminderRepository.delete(id);
        serviceReminderSearchRepository.delete(id);
    }

    /**
     * Search for the serviceReminder corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ServiceReminderDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ServiceReminders for query {}", query);
        Page<ServiceReminder> result = serviceReminderSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(serviceReminderMapper::toDto);
    }

    public ServiceReminderDTO processExecuteData(Integer id, String param, ServiceReminderDTO dto) {
        ServiceReminderDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<ServiceReminderDTO> processExecuteListData(Integer id, String param, Set<ServiceReminderDTO> dto) {
        Set<ServiceReminderDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
