package id.atiila.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

@Service
@Transactional
public class DateUtils {
    public ZonedDateTime parse(String date){
        try {
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date dtWhen = s.parse(date);
            ZonedDateTime zdt = ZonedDateTime.ofInstant(dtWhen.toInstant(), ZoneId.systemDefault());
            return zdt;
        }catch (Exception e){
            return null;
        }
    }

    public ZonedDateTime addMonthFromNow(Integer month){
        try{
            String localDate = LocalDate.now().plusMonths(month).toString();
            ZonedDateTime zdt = parse(localDate);
            return zdt;
        }catch(Exception e){
            return null;
        }
    }

    public LocalDateTime getApocalypseDate(){
        return LocalDateTime.of(9999, 12, 31, 23, 59, 59, 0);
    }
}
