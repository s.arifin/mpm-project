package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.MemosSearchRepository;
import id.atiila.repository.search.VehicleSalesBillingSearchRepository;
import id.atiila.service.dto.*;
import id.atiila.service.mapper.BillingItemMapper;
import id.atiila.service.mapper.MemosMapper;
import id.atiila.service.mapper.VehicleSalesOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Memos.
 * BeSmart Team
 */

@Service
@Transactional
public class MemosService {

    @Autowired private SalesUnitRequirementRepository surRepository;

    @Autowired private VehicleSalesBillingRepository vehicleSalesBillingRepository;

    @Autowired private VehicleSalesBillingSearchRepository vehicleSalesBillingSearchRepository;

    @Autowired private BillingItemService billingItemService;

    @Autowired private BillingItemRepository billingItemRepository;

    @Autowired private VehicleSalesOrderRepository vehicleSalesOrderRepository;

    @Autowired private VehicleSalesOrderMapper vehicleSalesOrderMapper;

    @Autowired private OrderItemRepository orderItemRepository;

    @Autowired private BillingItemMapper billingItemMapper;

    @Autowired private InventoryItemRepository inventoryItemRepository;

    @Autowired private FeatureRepository featureRepository;

    @Autowired private VehicleSalesBillingService vehicleSalesBillingService;

    @Autowired protected ShipmentOutgoingService shipmentOutgoingService;

    @Autowired private MasterNumberingService masterNumberingService;

    @Autowired private PartyUtils partyUtils;

    @Autowired private VehicleDocumentRequirementRepository vehicleDocumentRequirementRepository;

    @Autowired private VehicleRepository vehicleRepository;

    @Autowired
    private SalesUnitRequirementRepository salesUnitRequirementRepository;

    @Autowired
    private ActivitiProcessor processor;

    private final Logger log = LoggerFactory.getLogger(MemosService.class);

    private final MemosRepository memosRepository;

    private final MemosMapper memosMapper;

    private final MemosSearchRepository memosSearchRepository;

    public MemosService(MemosRepository memosRepository, MemosMapper memosMapper, MemosSearchRepository memosSearchRepository) {
        this.memosRepository = memosRepository;
        this.memosMapper = memosMapper;
        this.memosSearchRepository = memosSearchRepository;
    }

    /**
     * Save a memos.
     *
     * @param memosDTO the entity to save
     * @return the persisted entity
     */
    public MemosDTO save(MemosDTO memosDTO) {
        log.debug("Request to save Memos : {}", memosDTO);
        Memos memos = memosMapper.toEntity(memosDTO);
        if ((memos.getMemoNumber()=="")||( memos.getMemoNumber()== null)){
            String memNumber = masterNumberingService.findTransNumberMemoBy(memosDTO.getDealerId(), "MKO");

            memos.setMemoNumber(memNumber);
        }
        memos = memosRepository.save(memos);
        MemosDTO result = memosMapper.toDto(memos);
        memosSearchRepository.save(memos);
        return result;
    }

    /**
     *  Get all the memos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MemosDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Memos");
        Internal intr = partyUtils.getCurrentInternal();
        return memosRepository.findActiveMemos(intr.getIdInternal(), pageable)
            .map(memosMapper::toDto);
    }

    /**
     *  Get one memos by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public MemosDTO findOne(UUID id) {
        log.debug("Request to get Memos : {}", id);
        Memos memos = memosRepository.findOne(id);
        return memosMapper.toDto(memos);
    }

    /**
     *  Delete the  memos by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Memos : {}", id);
        memosRepository.delete(id);
        memosSearchRepository.delete(id);
    }

    /**
     * Search for the memos corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MemosDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Memos for query {}", query);
        Page<Memos> result = memosSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(memosMapper::toDto);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public MemosDTO processExecuteData(Integer id, String param, MemosDTO dto) {
        MemosDTO r = dto;
        if (r != null) {
            switch (id) {
                case 100:
                    // approve memo update motor
                    updateMemosGantiKendaraan(r);
                    changeMemosStatus(r, BaseConstants.STATUS_COMPLETED);
                    break;

                case 200:
                    // approve memo update motor
                    updateKomposisiHarga(r);
                    changeMemosStatus(r, BaseConstants.STATUS_COMPLETED);
                    break;
                   //todo di remark dulu biar ngga lupa nih   .....
                case 300:
                    // approve memo cancel ivu
                    Memos memo = memosRepository.findOne(r.getIdMemo());
                    if ( memo != null) {
                        Map<String, Object> vars = processor.getVariables("memos", memo);
                        processor.startProcess("memos", memo.getIdMemo().toString(), vars);
                    }
                    break;
                case 400:
                    changeMemosStatus(r, BaseConstants.STATUS_CANCEL);
                default:
                    break;
            }
		}
        return r;
    }

    // TODO : List
    //   warna beda, tipe sama, harga sama
    //   - sur ubah -> feature,
    //   - vso -> order item -> feature ->shipment item =>remove set 0
    //   - picking batal ->
    //   - item isuance batal  -> shipemnt item batal -> do batal
    //   - stock - inventory item qty +1
    //   generate ulang
    //
    //   pembatalan vso
    //   - pembatalan mengakibatkan uang DP di sur


    //   - batal semua, reopen sur


    @Transactional(propagation = Propagation.REQUIRED)
    void updateKomposisiHarga(MemosDTO memosDTO) {
        PageRequest page = new PageRequest(0,1);
        Memos memos = memosRepository.findOne(memosDTO.getIdMemo());
//        cari vsb dan di update
        VehicleSalesBilling vsb = vehicleSalesBillingRepository.findOne(memos.getBilling().getIdBilling());

            vehicleSalesBillingRepository.save(vsb);
            vehicleSalesBillingSearchRepository.delete(vsb.getIdBilling());

    }

    //todo nanti di cek lagi
//    @Transactional(propagation = Propagation.REQUIRED)
//    void cancelIvu(MemosDTO memosDTO) {
//        String processName = "ivu-cancel";
//        Map<String, Object> vars = processor.getVariables();
//        processor.startProcess(processName, memosDTO.getBillingId().toString(), vars);
//
//    }




    /**
     * TODO
     *
     * @param memosDTO
     */
    @Transactional(propagation = Propagation.REQUIRED)
    void updateMemosGantiKendaraan(MemosDTO memosDTO){
        BillingItem billingItem = null;
        Set<BillingItem> billingItems = new HashSet<>();
        PageRequest page = new PageRequest(0,1);
        log.debug("Memos dto to entity " + memosDTO);
        Memos memos = memosRepository.findOne(memosDTO.getIdMemo());

        log.debug("old Inventroy ::" + memos.getOldInventoryItem().getIdInventoryItem());
        log.debug("Vsb find one by id billing =" + memos.getBilling().getIdBilling());
        VehicleSalesBilling vsb =  vehicleSalesBillingRepository.findOne(memos.getBilling().getIdBilling());

        log.debug("Billing item find by id billing=" + vsb.getIdBilling());
        List<BillingItem> b = billingItemRepository.findByIdInvite(memosDTO.getOldInventoryItemId());      /// billingItemRepository.findItemByIdBilling(vsb.getIdBilling(),page).iterator().next();
        if (!b.isEmpty()) {
            billingItem = b.get(0);
            billingItems.add(billingItem);
        }

        log.debug("Order item find by id bill item =" + billingItems);
        OrderItem orderItem = orderItemRepository.findOrderItemByBillItem(billingItems)
                .iterator()
                .next();


        // SALES UNIT REQUIREMENT
        // id inv ganti dg yg baru
        log.debug("Inventory item new =" + memos.getInventoryItem());
        InventoryItem inventoryItemNew = inventoryItemRepository.findOne(memos.getInventoryItem().getIdInventoryItem()) ;

        log.debug("New Feature by id feature =" + inventoryItemNew.getIdFeature());
        Feature newFeature = featureRepository.findOne(inventoryItemNew.getIdFeature());

        log.debug("ganti order item  == "+ orderItem);
        orderItem.setIdframe(inventoryItemNew.getIdFrame());
        orderItem.setIdmachine(inventoryItemNew.getIdMachine());
        orderItem.setIdFeature(newFeature.getIdFeature());
        orderItemRepository.save(orderItem);

        log.debug("SUR by order id =" + orderItem.getOrders().getIdOrder());
        //
        //SalesUnitRequirement sur = vehicleSalesOrderRepository.findSurByOrderId(vsb.getInternal().getIdInternal(), orderItem.getOrders().getIdOrder());

        SalesUnitRequirement sur = salesUnitRequirementRepository.queryByIdOrderItem(orderItem.getIdOrderItem());
        sur.setIdmachine(inventoryItemNew.getIdMachine());
        sur.setIdframe(inventoryItemNew.getIdFrame());
        sur.setRequestIdFrame(inventoryItemNew.getIdFrame());
        sur.setRequestIdMachine(inventoryItemNew.getIdMachine());
        sur.setColor(newFeature);
        surRepository.save(sur);

        // INVENTORY ITEM OLD diNULLkan
        log.debug("Inv old item ::" + memos.getOldInventoryItem());
        InventoryItem inventoryItemOld = memos.getOldInventoryItem();
        inventoryItemOld.setQtyBooking(0D);
        inventoryItemRepository.save(inventoryItemOld);

        log.debug("vso with id Order = " + orderItem.getOrders().getIdOrder());
        VehicleSalesOrder vso = vehicleSalesOrderRepository.findByIdWithEagerDetail(orderItem.getOrders().getIdOrder());

        log.debug("cancel vso by id order = " + vso.getIdOrder());
        vehicleSalesBillingService.cancelByVSO( vso.getIdOrder());

        // BILLING ITEM    -> ganti inv item + bill number
        // ORDER ITEM -> ganti feature
        log.debug("Order item : get details= " + vso.getDetails());
        log.debug("vso detail ::" + vso.getDetails().size());

//        for(OrderItem oi : vso.getDetails()){
            log.debug("Billing item : get billing item = " +  orderItem.getBillingItems());
            log.debug("total billing item ::" + orderItem.getBillingItems().size());

//            Page<OrderItem> oii = orderItemRepository.findOrderItemByOrders(vso.getIdOrder(), new PageRequest(0,1));
            List<BillingItem> lBillite = billingItemRepository.findByIdInvite(memosDTO.getOldInventoryItemId()); //billingItemRepository.findBilliteByOrdite(orderItem.getIdOrderItem());
            log.debug("billingitem===" + lBillite);
            if (!lBillite.isEmpty()) {
                for(BillingItem bi:lBillite) {
                    log.debug("billingitemsatuan===" + bi);
                    bi.setInventoryItem(inventoryItemNew);
                    bi.setIdFeature(inventoryItemNew.getIdFeature());
                    bi.setItemDescription(orderItem.getItemDescription());
                    billingItemRepository.save(bi);
                }
            }

//            Set<OrderBillingItem> ordBillItems = oi.getBillingItems();
//            for(OrderBillingItem obi : ordBillItems){
//                BillingItem bi = obi.getBillingItem();
//                bi.setInventoryItem(inventoryItemNew);
//                bi.setIdFeature(inventoryItemNew.getIdFeature());
//                bi.setItemDescription(oi.getItemDescription());
//                billingItemRepository.save(bi);
//
////                oi.setIdFeature(inventoryItemNew.getIdFeature());
////                orderItemRepository.save(oi);
//            }
//        }



        // generate ulang shipment
        log.debug("Generate ulang shipment" );
        VehicleSalesOrderDTO vsoDto = vehicleSalesOrderMapper.toDto(vso);
        log.debug("vsoo to shipment out going " + vsoDto);
        ShipmentOutgoingDTO sdo = shipmentOutgoingService.buildFrom(vsoDto);

        //Inventory item baru dibuat 1
//        inventoryItemNew.setQtyBooking(0d);
        log.debug("before save inventory item new test " + inventoryItemNew);
        inventoryItemNew.setQtyBooking(1D);
        inventoryItemRepository.save(inventoryItemNew);
        log.debug("after save inventoryitem new");

        // cari vehicle_document_requirement
        VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findByOrderItem(orderItem);
        log.debug("vehicle document requiremet == "+ vdr);

        Vehicle v = vehicleRepository.findOne(vdr.getVehicle().getIdVehicle());
        log.debug("vehicle di memo === " + v);

        v.setIdFrame(inventoryItemNew.getIdFrame());
        v.setIdMachine(inventoryItemNew.getIdMachine());
        v.setIdColor(newFeature.getIdFeature());
        vehicleRepository.save(v);

        //hapus di elastic nya
        vehicleSalesBillingSearchRepository.delete(vsb.getIdBilling());
    }

    private String genNewBillNumn(String currBillNumb){
        log.debug("start gen new bil numb");
        String newBillNumb = currBillNumb.trim();
        int lenCurBillNumb =newBillNumb.length();
        log.debug("start gen new bil numb: cur ={} len={}", currBillNumb, lenCurBillNumb );
        if ( lenCurBillNumb >23){
            log.debug("bill num > 23");
            Integer curNumb = Integer.parseInt(currBillNumb.substring(currBillNumb.lastIndexOf("-")+1 , lenCurBillNumb)) + 1;
            log.debug("start gen new bil numb: new numb =" + curNumb);
            newBillNumb = currBillNumb.substring(0, 23) + "-" + curNumb;
            log.debug("start gen new bil numb: new bill =" + newBillNumb);
        } else {
            log.debug("bill num <= 23");
            newBillNumb = currBillNumb + "-1";
        }
        return newBillNumb;
    }

    public Set<MemosDTO> processExecuteListData(Integer id, String param, Set<MemosDTO> dto) {
        Set<MemosDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public MemosDTO changeMemosStatus(MemosDTO dto, Integer id) {
        if (dto != null) {
			Memos e = memosRepository.findOne(dto.getIdMemo());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        memosSearchRepository.delete(dto.getIdMemo());
                        break;
                    default:
                        memosSearchRepository.save(e);
                }
				memosRepository.save(e);
			}
		}
        return dto;
    }


}
