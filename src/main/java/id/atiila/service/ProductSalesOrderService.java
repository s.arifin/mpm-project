package id.atiila.service;

import id.atiila.domain.Internal;
import id.atiila.domain.ProductSalesOrder;
import id.atiila.repository.ProductSalesOrderRepository;
import id.atiila.repository.search.ProductSalesOrderSearchRepository;
import id.atiila.service.dto.ProductSalesOrderDTO;
import id.atiila.service.mapper.ProductSalesOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ProductSalesOrder.
 * BeSmart Team
 */

@Service
@Transactional
public class ProductSalesOrderService {

    private final Logger log = LoggerFactory.getLogger(ProductSalesOrderService.class);

    private final ProductSalesOrderRepository productSalesOrderRepository;

    private final ProductSalesOrderMapper productSalesOrderMapper;

    private final ProductSalesOrderSearchRepository productSalesOrderSearchRepository;

    @Autowired
    private PartyUtils partyUtils;

    public ProductSalesOrderService(ProductSalesOrderRepository productSalesOrderRepository, ProductSalesOrderMapper productSalesOrderMapper, ProductSalesOrderSearchRepository productSalesOrderSearchRepository) {
        this.productSalesOrderRepository = productSalesOrderRepository;
        this.productSalesOrderMapper = productSalesOrderMapper;
        this.productSalesOrderSearchRepository = productSalesOrderSearchRepository;
    }

    /**
     * Save a productSalesOrder.
     *
     * @param productSalesOrderDTO the entity to save
     * @return the persisted entity
     */
    public ProductSalesOrderDTO save(ProductSalesOrderDTO productSalesOrderDTO) {
        log.debug("Request to save ProductSalesOrder : {}", productSalesOrderDTO);
        ProductSalesOrder productSalesOrder = productSalesOrderMapper.toEntity(productSalesOrderDTO);
        productSalesOrder = productSalesOrderRepository.save(productSalesOrder);
        ProductSalesOrderDTO result = productSalesOrderMapper.toDto(productSalesOrder);
        productSalesOrderSearchRepository.save(productSalesOrder);
        return result;
    }

    /**
     * Get all the productSalesOrders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductSalesOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductSalesOrders");
        Internal intr = partyUtils.getCurrentInternal();
        return productSalesOrderRepository.findActive(intr, pageable)
            .map(productSalesOrderMapper::toDto);
    }

    /**
     * Get one productSalesOrder by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ProductSalesOrderDTO findOne(UUID id) {
        log.debug("Request to get ProductSalesOrder : {}", id);
        ProductSalesOrder productSalesOrder = productSalesOrderRepository.findOne(id);
        return productSalesOrderMapper.toDto(productSalesOrder);
    }

    /**
     * Delete the productSalesOrder by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ProductSalesOrder : {}", id);
        productSalesOrderRepository.delete(id);
        productSalesOrderSearchRepository.delete(id);
    }

    /**
     * Search for the productSalesOrder corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductSalesOrderDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of ProductSalesOrders for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idInternal = request.getParameter("idInternal");
        String idCustomer = request.getParameter("idCustomer");
        String idBillTo = request.getParameter("idBillTo");
        String idSaleType = request.getParameter("idSaleType");

        if (filterName != null) {
        }
        Page<ProductSalesOrder> result = productSalesOrderSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(productSalesOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ProductSalesOrderDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ProductSalesOrderDTO");
        String idInternal = request.getParameter("idInternal");
        String idCustomer = request.getParameter("idCustomer");
        String idBillTo = request.getParameter("idBillTo");
        String idSaleType = request.getParameter("idSaleType");

        return productSalesOrderRepository.findByParams(idInternal, idCustomer, idBillTo, idSaleType, pageable)
            .map(productSalesOrderMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public ProductSalesOrderDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        ProductSalesOrderDTO r = null;
        return r;
    }

    @Transactional
    public Set<ProductSalesOrderDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<ProductSalesOrderDTO> r = new HashSet<>();
        return r;
    }

}
