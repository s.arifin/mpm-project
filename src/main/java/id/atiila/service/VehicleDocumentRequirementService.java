package id.atiila.service;

import com.google.common.collect.Lists;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IAtomicLong;
import com.hazelcast.core.ILock;
import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.base.XlsxUtils;
import id.atiila.config.tenant.TenantContext;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.VehicleDocumentRequirementSearchRepository;
import id.atiila.service.dto.*;
import id.atiila.service.mapper.CustomVDRMapper;
import id.atiila.service.mapper.VehicleDocumentRequirementMapper;

import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.elasticsearch.common.Strings;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.persistence.DiscriminatorType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.springframework.data.jpa.domain.Specifications.*;

/**
 * Service Implementation for managing VehicleDocumentRequirement.
 * BeSmart Team
 */

@Service
@Transactional
public class VehicleDocumentRequirementService {

    private final Logger log = LoggerFactory.getLogger(VehicleDocumentRequirementService.class);

    private final VehicleDocumentRequirementRepository vehicleDocumentRequirementRepository;

    private final VehicleDocumentRequirementMapper vehicleDocumentRequirementMapper;

    private final VehicleDocumentRequirementSearchRepository vehicleDocumentRequirementSearchRepository;

    @Autowired
    private SalesUnitRequirementRepository salesUnitRequirementRepository;

    @Autowired
    private UnitDeliverableRepository unitDeliverableRepository;

    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private PriceComponentRepository priceComponentRepository;


    @Autowired
    private MasterNumberingService masterNumberingService;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private VehicleSalesOrderService vehicleSalesOrderService;

    @Autowired
    private SalesUnitRequirementService salesUnitRequirementService;

    @Autowired
    private SalesmanService salesmanService;

    @Autowired
    private LeasingCompanyService leasingCompanyService;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private VehicleSalesOrderRepository vehicleSalesOrderRepository;

    @Autowired
    private PersonalCustomerRepository personalCustomerRepository;

    @Autowired
    private OrganizationCustomerRepository organizationCustomerRepository;

    @Autowired
    private CustomerUtils customerUtils;

    @Autowired
    private CustomVDRMapper customVDRMapper;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private BillingRepository billingRepository;

    @Autowired
    private VillageRepository villageRepository;

    @Autowired
    private CommunicationEventCDBRepository communicationEventCDBRepository;

    @Autowired
    private OrderItemService orderItemService;

    @Autowired
    private ActivitiProcessor activitiProcessor;

    @Autowired
    private InternalRepository<Internal> internalRepository;

    @Autowired
    private HazelcastInstance hz;

    @Autowired
    private MasterNumberingRepository masterNumberingRepository;

    public VehicleDocumentRequirementService(VehicleDocumentRequirementRepository vehicleDocumentRequirementRepository, VehicleDocumentRequirementMapper vehicleDocumentRequirementMapper, VehicleDocumentRequirementSearchRepository vehicleDocumentRequirementSearchRepository) {
        this.vehicleDocumentRequirementRepository = vehicleDocumentRequirementRepository;
        this.vehicleDocumentRequirementMapper = vehicleDocumentRequirementMapper;
        this.vehicleDocumentRequirementSearchRepository = vehicleDocumentRequirementSearchRepository;
    }

    /**
     * Save a vehicleDocumentRequirement.
     *
     * @param vehicleDocumentRequirementDTO the entity to save
     * @return the persisted entity
     */
    public VehicleDocumentRequirementDTO save(VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO) {
        log.debug("Request to save VehicleDocumentRequirement : {}", vehicleDocumentRequirementDTO);
        VehicleDocumentRequirement vehicleDocumentRequirement = vehicleDocumentRequirementMapper.toEntity(vehicleDocumentRequirementDTO);
        vehicleDocumentRequirement = vehicleDocumentRequirementRepository.save(vehicleDocumentRequirement);
        VehicleDocumentRequirementDTO result = vehicleDocumentRequirementMapper.toDto(vehicleDocumentRequirement);
        vehicleDocumentRequirementSearchRepository.save(vehicleDocumentRequirement);
        return result;
    }

    /**
     * Update a vehicleDocumentRequirement.
     *
     * @param vehicleDocumentRequirementDTO the entity to update
     * @return the persisted entity
     */
    public VehicleDocumentRequirementDTO update(VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO) {
        log.debug("Request to updateeeeee VehicleDocumentRequirement : {}", vehicleDocumentRequirementDTO);
        VehicleDocumentRequirement vehicleDocumentRequirement = vehicleDocumentRequirementMapper.toEntity(vehicleDocumentRequirementDTO);
        log.debug("cek status stnk geeeng" + vehicleDocumentRequirement.getWaitStnk());

        vehicleDocumentRequirement = vehicleDocumentRequirementRepository.save(vehicleDocumentRequirement);
        VehicleDocumentRequirementDTO result = vehicleDocumentRequirementMapper.toDto(vehicleDocumentRequirement);
        vehicleDocumentRequirementSearchRepository.save(vehicleDocumentRequirement);
        return result;
    }

    /**
     *  Get all the vehicleDocumentRequirements.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleDocumentRequirementDTO> findAll(String internal, Pageable pageable) {
        log.debug("Request to get all VehicleDocumentRequirements");
        Internal intr = partyUtils.getCurrentInternal();
        return vehicleDocumentRequirementRepository.findActiveVehicleDocumentRequirement(internal, pageable)
            .map(vehicleDocumentRequirementMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<VehicleDocumentRequirementDTO> findQueryItems(Pageable pageable, HttpServletRequest request, HttpServletResponse response) {
        log.debug("Request to get all VehicleDocumentRequirements");
        return null;
    }

    /**
     *  Get one vehicleDocumentRequirement by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public VehicleDocumentRequirementDTO findOne(UUID id) {
        log.debug("Request to get VehicleDocumentRequirement : {}", id);
        VehicleDocumentRequirement vehicleDocumentRequirement = vehicleDocumentRequirementRepository.findOne(id);
        return vehicleDocumentRequirementMapper.toDto(vehicleDocumentRequirement);
    }

    /**
     *  Get one vehicleDocumentRequirement by idFrame.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public VehicleDocumentRequirementDTO findOnebyFrame(String id) {
        log.debug("Request to get Vehicle : {}", id);
        VehicleDocumentRequirement vehicleDocumentRequirement = vehicleDocumentRequirementRepository.findOnebyidFrame(id);
        return vehicleDocumentRequirementMapper.toDto(vehicleDocumentRequirement);
    }

    /**
     *  Delete the  vehicleDocumentRequirement by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete VehicleDocumentRequirement : {}", id);
        vehicleDocumentRequirementRepository.delete(id);
        vehicleDocumentRequirementSearchRepository.delete(id);
    }

    /**
     * Search for the vehicleDocumentRequirement corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VehicleDocumentRequirementDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of VehicleDocumentRequirements for query {}", query);
        Page<VehicleDocumentRequirement> result = vehicleDocumentRequirementSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(vehicleDocumentRequirementMapper::toDto);
    }

    public VehicleDocumentRequirementDTO processExecuteData(Integer id, String param, VehicleDocumentRequirementDTO dto) {
        VehicleDocumentRequirementDTO r = dto;
        if (r != null) {
            switch (id) {
                case 101:
                    pengurusanBiroJasa(dto);
                    break;
                case 102:
                    receiptFaktur(dto);
                    break;
                case 103:
                    kirimBiroJasa(dto);
                    break;
                case 104:
                    cancelFaktur(dto);
                    break;
                case 105:
                    cancelIsiNama(dto);
                    break;
                case 107:
                    saveByMachineAndFrame(id, dto);
                    break;
                case 41:
                    changeVehicleDocumentRequirementStatus(r,BaseConstants.STATUS_REQUEST_CANCEL_PENGAJUAN);
                    break;
                default:
                    break;
            }
        }
        return r;
    }

    public Set<VehicleDocumentRequirementDTO> processExecuteListData(Integer id, String param, Set<VehicleDocumentRequirementDTO> dto) {

        Set<VehicleDocumentRequirementDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                case 101:
                    for(VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO : r) {
                    requestFaktur(param, r);
                    }
                    break;
                case 105:
                    for(VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO : r){
                        cancelIsiNama(vehicleDocumentRequirementDTO);
                    }
                    break;
                case 106:
                    for(VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO : r){
                        List<VehicleDocumentRequirement> vdr = vehicleDocumentRequirementRepository.findByRegistrationNumber(vehicleDocumentRequirementDTO.getRegistrationNumber());
                        for(VehicleDocumentRequirement result: vdr){
                            VehicleDocumentRequirementDTO paramDTO = vehicleDocumentRequirementMapper.toDto(result);
                            cancelIsiNama(paramDTO);
                        }
                    }
                    break;
                case 32:
                    for(VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO : r){
                        saveFakturUpload(vehicleDocumentRequirementDTO, param);
                    }
                    break;
                case 108:
                    prosesTerimaFaktur(r);
                    break;
                case 33:
                    for(VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO : r){
                        revisiFaktur(vehicleDocumentRequirementDTO);
                    }
                    break;
                case 109:
                    listKirimBiroJasa(param, r);
                    break;
                case 104:
                    for(VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO : r){
                        cancelFaktur(vehicleDocumentRequirementDTO);
                    }
                    break;
                default:
                    break;
            }
        }
        return r;
    }

    protected void prosesTerimaFaktur(Set<VehicleDocumentRequirementDTO> r) {
        for(VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO : r){
            //
            VehicleDocumentRequirement vd = vehicleDocumentRequirementRepository.findOne(vehicleDocumentRequirementDTO.getIdRequirement());
            ProcessInstance p = activitiProcessor.getProcessInstance("registrasi", vd.getIdRequirement().toString());
            if (p == null) {
                Map<String, Object> params = activitiProcessor.getVariables("vdr", vd);
                p = activitiProcessor.startProcess("registrasi", vd.getIdRequirement().toString(), params);
            }

            List<Task> items = activitiProcessor.getTasks("registrasi", vd.getIdRequirement().toString());
            if (items.size() > 0) {
                activitiProcessor.completeTask(items.get(0).getId());
            }
            //
            receiptFaktur(vehicleDocumentRequirementDTO);
        }
    }


    public VehicleDocumentRequirementDTO saveByMachineAndFrame(Integer id, VehicleDocumentRequirementDTO dto) {

        VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findOneByMachineAndFrame(dto.getVehicle().getIdMachine(), dto.getVehicle().getIdFrame());
        if ( id == 107 && vdr != null){
            if (vdr.getCurrentStatus() == BaseConstants.STATUS_REQUEST_FAKTUR) {
                vdr.setStatus(BaseConstants.STATUS_RECEIPT_FAKTUR_UPLOAD_NON);
                vdr.setFakturATPM(dto.getFakturATPM());
                vdr.setFakturDate(dto.getFakturDate());
                vehicleDocumentRequirementRepository.save(vdr);
            } else {
                log.debug("gagal matching");
            }
        } else if (id == 108 && vdr != null){
        } else {
            log.debug("gagal matching");
        }
        return dto;
    }

    public VehicleDocumentRequirementDTO changeVehicleDocumentRequirementStatus(VehicleDocumentRequirementDTO dto, Integer id) {
        if (dto != null) {
            VehicleDocumentRequirement e = vehicleDocumentRequirementRepository.findOne(dto.getIdRequirement());
            if (!e.getCurrentStatus().equals(id)) {
                e.setStatus(id);
                switch (id) {
                    case 13:
                        vehicleDocumentRequirementSearchRepository.delete(dto.getIdRequirement());
                        break;
                    default:
                        vehicleDocumentRequirementSearchRepository.save(e);
                }
                vehicleDocumentRequirementRepository.save(e);
            }
        }
        return dto;
    }

    @Transactional(readOnly = true)
    public Page<VehicleDocumentRequirementDTO> findAll(Pageable pageable, Integer idStatusType, Integer idSaleType, Boolean statusKonfirmasi, String idInternal) {
        log.debug("Request to get all VehicleDocumentRequirements");
        Page<VehicleDocumentRequirementDTO> vehicleDocumentRequirementDTOS;
        if (idStatusType != null && statusKonfirmasi == null){
            vehicleDocumentRequirementDTOS = vehicleDocumentRequirementRepository.findActiveByStatusVehicleDocumentRequirement(pageable, idStatusType, idInternal)
                .map(vehicleDocumentRequirementMapper::toDto)
                .map(this::assignIVU);
        } else if (idStatusType != null && statusKonfirmasi == true){
            vehicleDocumentRequirementDTOS = vehicleDocumentRequirementRepository.findActiveByStatusKOnfirmasiSur(pageable, idStatusType, idInternal)
                .map(vehicleDocumentRequirementMapper::toDto)
                .map(this::assignIVU);
        } else {
            vehicleDocumentRequirementDTOS = vehicleDocumentRequirementRepository.findActiveVehicleDocumentRequirement(idInternal, pageable)
                .map(vehicleDocumentRequirementMapper::toDto);
        }

        return vehicleDocumentRequirementDTOS;
    }

    @Transactional(readOnly = true)
    public Page<CustomRequirementDTO> findCustom(Pageable pageable, Integer idStatusType, Boolean statusKonfirmasi, String idInternal, Boolean statusPenerimaan) {
        log.debug("Request to get all VehicleDocumentRequirements");
        Boolean statusStnk;
        Boolean statusSTNKBPKB;
        if (statusKonfirmasi == null || statusKonfirmasi == false ){
            statusStnk = false;
        } else {
            statusStnk = true;
        }

        if (statusPenerimaan == null || statusPenerimaan == false ) {
            statusSTNKBPKB = false;
        } else {
            statusSTNKBPKB = true;
        }

        Page<CustomRequirementDTO> customRequirementDTOS;
        if (statusStnk == true && statusSTNKBPKB == false ){
            customRequirementDTOS = vehicleDocumentRequirementRepository.findCustomActiveByStatusKonfirmasiSur(pageable, idStatusType, idInternal)
                .map(this::assignBilling);
        } else if (statusStnk == false && statusSTNKBPKB == false){
            if (idStatusType == BaseConstants.STATUS_REQUEST_FAKTUR) {
                customRequirementDTOS = vehicleDocumentRequirementRepository.findCustomActiveByStatusVehicleDocumentRequirement(pageable, idStatusType, idInternal)
                    .map(this::assignSUR)
                    .map(this::assignSPG);
            } else {
                log.debug("masuk case jika status stnk false dan status penerimaan false id 10");
                customRequirementDTOS = vehicleDocumentRequirementRepository.findCustomActiveByStatusVehicleDocumentRequirement(pageable, idStatusType, idInternal)
                    .map(this::assignSUR)
                    .map(this::assignSPG)
                    .map(this::assignGeo);
            }
        } else {
            customRequirementDTOS = vehicleDocumentRequirementRepository.queryCustomActivePenerimaanStnkBpkb(pageable, idStatusType, idInternal)
                .map(this::assignSUR)
                .map(this::assignUD);
        }
        return customRequirementDTOS;
    }


    public Set<VehicleDocumentRequirementDTO> requestFaktur(String param, Set<VehicleDocumentRequirementDTO> dto) {
        String internalID = param;
        String numberGenerator = masterNumberingService.findTransNumberRegistrationBy(internalID, "REG");

        Set<VehicleDocumentRequirementDTO> r = dto;

        for(VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO : r) {
            VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findOne(vehicleDocumentRequirementDTO.getIdRequirement());
            vdr.setStatus(BaseConstants.STATUS_REQUEST_FAKTUR);
            if (vdr.getRegistrationNumber() == null) vdr.setRegistrationNumber(numberGenerator);
            if (vdr.getDateFillName() == null) vdr.setDateFillName(ZonedDateTime.now());

            // build customer
            buildCustomerfromSTNK(vdr);

            vehicleDocumentRequirementRepository.save(vdr);
        }
        List<VehicleDocumentRequirementDTO> mainlist = new ArrayList<>(r);
        return r;
    }

    public CustomRequirementDTO findCustom(UUID idReg){
        CustomRequirementDTO cvdr = vehicleDocumentRequirementRepository.findCustomOne(idReg);
        return cvdr;
    }

    public VehicleDocumentRequirementDTO receiptFaktur(VehicleDocumentRequirementDTO dto) {

        VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findOneByMachineAndFrame(dto.getVehicle().getIdMachine(), dto.getVehicle().getIdFrame());
        log.debug("sale typeeeeenyaa" + vdr.getSaleType().getIdSaleType());
        if (vdr.getSaleType().getIdSaleType() == 22 || vdr.getSaleType().getIdSaleType() == 54 || vdr.getSaleType().getIdSaleType() == 55) {
            // jika pembelian off the road
            log.debug("sale typeeeee harusnya off the road" + vdr.getSaleType().getDescription());
            vdr.setStatus(BaseConstants.STATUS_BAST_OFFTHEROAD);
            vdr.setFakturATPM(dto.getFakturATPM());
            vdr.setFakturDate(dto.getFakturDate());

            UnitDeliverable ud = new UnitDeliverable();
            ud.setVehicleDocumentRequirement(vdr);
            ud.setIdDeliverableType(BaseConstants.DELIVERABLE_TYPE_FAKTUR_OFFTHEROAD);
            unitDeliverableRepository.save(ud);
        } else {
            vdr.setStatus(BaseConstants.STATUS_REQUEST_BIRO_JASA);
            vdr.setFakturATPM(dto.getFakturATPM());
            vdr.setFakturDate(dto.getFakturDate());
        }

        vehicleDocumentRequirementRepository.save(vdr);

        return dto;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public String findTransSubmissionNumberBy(String id, String idtrans){
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        int month = cal.get(Calendar.MONTH);
        month = month + 1;
        log.debug("master numbering system: findTransSubmissionNumberByY" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);

        if (internal == null){
            return null;
        }
        String r = internal.getRoot().getIdInternal() + "-FDB-" +LocalDateTime.now().getYear() + "-" + month + "-";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 5, '0');
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Long nextValue(String idTag, String idValue) {
        return nextValue(idTag, idValue, 0l);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Long nextValue(String idTag, String idValue, Long startFrom) {
        long result = 0l;
        IAtomicLong number = null;
        ILock lock = hz.getLock(idTag + "-" + idValue);
        lock.lock();
        try {
            number = hz.getAtomicLong(TenantContext.getCurrentTenant() + "numbering" + idTag + "-" + idValue);
            if (number.get() == 0) {
                MasterNumbering numb = getNumbering(idTag, idValue, startFrom);
                number.set(numb.getNextValue());
            }
            result = number.incrementAndGet();
            MasterNumbering numb = getNumbering(idTag, idValue, startFrom);
            numb.setNextValue(result);
            masterNumberingRepository.save(numb);
        } finally {
            lock.unlock();
            number.set(0l);

        }
        return result;
    }

    @Transactional
    protected MasterNumbering getNumbering(String idTag, String idValue, Long startFrom) {
        MasterNumbering r = masterNumberingRepository.findOneByIdTagAndIdValue(idTag, idValue);
        if (r == null) {
            r = new MasterNumbering();
            r.setIdTag(idTag);
            r.setIdValue(idValue);
            r.setNextValue(startFrom);
            r.setNextValue(0l);
            r = masterNumberingRepository.saveAndFlush(r);
        }
        return r;
    }


    public Set<VehicleDocumentRequirementDTO> listKirimBiroJasa (String param, Set<VehicleDocumentRequirementDTO> dto) {
        String internal = param;
        Internal i = internalRepository.findByIdInternal(internal);
        BigDecimal swdkllj_price = BigDecimal.ZERO;
        BigDecimal pkb_price = BigDecimal.ZERO;
        BigDecimal tnkb_price = BigDecimal.ZERO;
        BigDecimal bbnkb_price = BigDecimal.ZERO;
        BigDecimal stnk_price = BigDecimal.ZERO;
        BigDecimal jasa_price = BigDecimal.ZERO;
        BigDecimal parkir_price = BigDecimal.ZERO;
        BigDecimal biaya_price = BigDecimal.ZERO;
        BigDecimal costHandling = BigDecimal.ZERO;

        String numberGenerator = findTransSubmissionNumberBy(internal,"BAST");

        Set<VehicleDocumentRequirementDTO> r = dto;

        for(VehicleDocumentRequirementDTO vehicleDocumentRequirementDTO : r) {
            VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findOne(vehicleDocumentRequirementDTO.getIdRequirement());
            String idProduct = vehicleDocumentRequirementDTO.getVehicle().getProductId();
            PriceComponent swdkllj = priceComponentRepository.getActivePriceByPriceType(idProduct,BaseConstants.PRICE_TYPE_SWDKLLJ,i.getOrganization().getIdParty());
            PriceComponent pkb = priceComponentRepository.getActivePriceByPriceType(idProduct,BaseConstants.PRICE_TYPE_PKB,i.getOrganization().getIdParty());
            PriceComponent tnkb = priceComponentRepository.getActivePriceByPriceType(idProduct,BaseConstants.PRICE_TYPE_TNKB,i.getOrganization().getIdParty());
            PriceComponent bbnkb = priceComponentRepository.getActivePriceByPriceType(idProduct,BaseConstants.PRICE_TYPE_BBNKB,i.getOrganization().getIdParty());
            PriceComponent stnk = priceComponentRepository.getActivePriceByPriceType(idProduct,BaseConstants.PRICE_TYPE_STNK,i.getOrganization().getIdParty());
            PriceComponent jasa = priceComponentRepository.getActivePriceByPriceType(idProduct,BaseConstants.PRICE_TYPE_BIROJASA,i.getOrganization().getIdParty());
            PriceComponent parkir = priceComponentRepository.getActivePriceByPriceType(idProduct,BaseConstants.PRICE_TYPE_PARKIR,i.getOrganization().getIdParty());
            PriceComponent biaya = priceComponentRepository.getActivePriceByPriceType(idProduct,BaseConstants.PRICE_TYPE_BIAYA,i.getOrganization().getIdParty());

            if (swdkllj != null) {
                swdkllj_price = swdkllj.getPrice() == null ? BigDecimal.ZERO : swdkllj.getPrice();
                vdr.setSwdkllj(swdkllj_price);
            }
            if (pkb != null) {
                pkb_price = pkb.getPrice() == null ? BigDecimal.ZERO : pkb.getPrice();
                vdr.setPkb(pkb_price);
            }
            if (tnkb != null) {
                tnkb_price = tnkb.getPrice() == null ? BigDecimal.ZERO : tnkb.getPrice();
                vdr.setTnkb(tnkb_price);
            }
            if (bbnkb != null) {
                bbnkb_price = bbnkb.getPrice() == null ? BigDecimal.ZERO : bbnkb.getPrice();
                vdr.setBbnkb(bbnkb_price);
            }
            if (stnk != null) {
                stnk_price = stnk.getPrice() == null ? BigDecimal.ZERO : stnk.getPrice();
                vdr.setStnkbpkb(stnk_price);
            }
            if (jasa != null) {
                jasa_price = jasa.getPrice() == null ? BigDecimal.ZERO : jasa.getPrice();
                vdr.setJasa(jasa_price);
            }
            if (parkir != null) {
                parkir_price = parkir.getPrice() == null ? BigDecimal.ZERO : parkir.getPrice();
                vdr.setBiayaParkir(parkir_price);
            }
            if (biaya != null) {
                biaya_price = biaya.getPrice() == null ? BigDecimal.ZERO : biaya.getPrice();
                vdr.setBiaya(biaya_price);
            }

            costHandling = swdkllj_price.add(pkb_price).add(tnkb_price).add(bbnkb_price).add(stnk_price).add(jasa_price).add(parkir_price).add(biaya_price);
            if (vdr.getSubmissionNo() == null) {
                vdr.setStatus(BaseConstants.STATUS_RECEIPT_BPKB_STNK);
                vdr.setVendor(vendorRepository.findOne(vehicleDocumentRequirementDTO.getIdVendor()));
                vdr.setDateSubmission(ZonedDateTime.now());
                vdr.setSubmissionNo(numberGenerator);
                vdr.setCostHandling(costHandling);
                log.debug("cek submission" + vdr.getSubmissionNo());
            }
            vdr = vehicleDocumentRequirementRepository.save(vdr);

            //
            List<Task> items = activitiProcessor.getTasks("registrasi", vdr.getIdRequirement().toString());
            if (items.size() > 0) {
                activitiProcessor.completeTask(items.get(0).getId());
            }
            //
        }
        List<VehicleDocumentRequirementDTO> mainlist = new ArrayList<>(r);
        return r;

    }

    public VehicleDocumentRequirementDTO kirimBiroJasa(VehicleDocumentRequirementDTO dto) {
        VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findOne(dto.getIdRequirement());

        vdr.setStatus(BaseConstants.STATUS_RECEIPT_BPKB_STNK);
        vdr.setVendor(vendorRepository.findOne(dto.getIdVendor()));
        vdr.setDateSubmission(ZonedDateTime.now());
        vehicleDocumentRequirementRepository.save(vdr);

        return dto;
    }

    public VehicleDocumentRequirementDTO revisiFaktur(VehicleDocumentRequirementDTO dto) {
        VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findOneByMachineAndFrame(dto.getVehicle().getIdMachine(), dto.getVehicle().getIdFrame());
        vdr.setStatus(BaseConstants.STATUS_RECEIPT_REV_FAKTUR);
        vdr.setFakturATPM(dto.getFakturATPM());
        vdr.setFakturDate(dto.getFakturDate());
        vdr.setNote(dto.getNote());
        vehicleDocumentRequirementRepository.save(vdr);

        return dto;
    }

    public VehicleDocumentRequirementDTO pengurusanBiroJasa(VehicleDocumentRequirementDTO dto) {
        Vendor vendor = vendorRepository.findOne(dto.getIdVendor());
        VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findOne(dto.getIdRequirement());
        vdr.setVendor(vendor);
        vdr.setStatus(BaseConstants.STATUS_BAST);
        vdr = vehicleDocumentRequirementRepository.save(vdr);

        StatusType vendorStatus = new StatusType();
        vendorStatus.setIdStatusType(BaseConstants.STATUS_DRAFT);
        return dto;
    }

    public VehicleDocumentRequirementDTO cancelFaktur(VehicleDocumentRequirementDTO dto) {
        VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findOneByMachineAndFrame(dto.getVehicle().getIdMachine(), dto.getVehicle().getIdFrame());

        if (vdr.getCurrentStatus() == BaseConstants.STATUS_REQUEST_FAKTUR || vdr.getCurrentStatus() == BaseConstants.STATUS_RECEIPT_FAKTUR_UPLOAD_NON || vdr.getCurrentStatus() == BaseConstants.STATUS_RECEIPT_FAKTUR_UPLOAD ) {
            vdr.setStatus(BaseConstants.STATUS_RECEIPT_REV_FAKTUR);
            vdr.setFakturATPM(dto.getFakturATPM());
            vdr.setFakturDate(dto.getFakturDate());
            vdr.setNote(dto.getNote());
            vehicleDocumentRequirementRepository.save(vdr);
        }

        return dto;
    }

    public VehicleDocumentRequirementDTO cancelIsiNama(VehicleDocumentRequirementDTO dto) {
        VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findOne(dto.getIdRequirement());

        if (vdr.getCurrentStatus() == BaseConstants.STATUS_REQUEST_FAKTUR || vdr.getCurrentStatus() == BaseConstants.STATUS_RECEIPT_REV_FAKTUR) {
            vdr.setStatus(BaseConstants.STATUS_DRAFT);
            vdr.setFakturATPM(null);
            vdr.setFakturDate(null);
            vdr.setRegistrationNumber(null);
            vdr.setDateFillName(null);
            vehicleDocumentRequirementRepository.save(vdr);
        }

        return dto;
    }

    public VehicleDocumentRequirementDTO revisiIsiNama(VehicleDocumentRequirementDTO dto) {
        log.debug("vdr" + dto);
        VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findOne(dto.getIdRequirement());
        SalesUnitRequirement sur = salesUnitRequirementRepository.findSurbyFrame(dto.getVehicle().getIdFrame());
        log.debug("vdr1" + vdr);
        log.debug("sur" + sur);
        return dto;
    }

    public VehicleDocumentRequirementDTO saveFakturUpload(VehicleDocumentRequirementDTO dto, String param) {
        VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findOnebyidFrame(dto.getVehicle().getIdFrame());

        if (vdr != null){
            if (vdr.getCurrentStatus() == BaseConstants.STATUS_REQUEST_FAKTUR) {

                vdr.setStatus(BaseConstants.STATUS_RECEIPT_FAKTUR_UPLOAD);
                vdr.setNote(dto.getNote());
                vdr.setFakturATPM(dto.getFakturATPM());
                vdr.setFakturDate(dto.getFakturDate());
                vdr.setInvoicePrint(dto.getInvoicePrint());
                vehicleDocumentRequirementRepository.save(vdr);
            }
        }
        return dto;
    }

    public CustomRequirementDTO assignSUR(CustomRequirementDTO dto) {
        System.out.println("---- MASUK ASSIGN SUR-----");
        Internal intr = partyUtils.getCurrentInternal();
        Billing billing = billingRepository.findByOrderItem(dto.getIdOrderItem());
        dto.setIvuNumber(billing.getBillingNumber());
        SalesUnitRequirementDTO sur = vehicleSalesOrderService.findSurByOrderId(dto.getIdOrder(),intr.getIdInternal());
        if (sur != null){
            BigDecimal dp = sur.getCreditDownPayment() == null ? BigDecimal.ZERO : sur.getCreditDownPayment();
            BigDecimal subsfincomp = sur.getSubsfincomp() == null ? BigDecimal.ZERO : sur.getSubsfincomp();
            BigDecimal subsmd = sur.getSubsmd() == null ? BigDecimal.ZERO : sur.getSubsmd();
            BigDecimal subsown = sur.getSubsown() == null ? BigDecimal.ZERO : sur.getSubsown();
            BigDecimal subsahm = sur.getSubsahm() == null ? BigDecimal.ZERO : sur.getSubsahm();
            BigDecimal totaldisc = subsfincomp.add(subsmd).add(subsown).add(subsahm);
            BigDecimal totaldp = dp.subtract(totaldisc);

            dto.setCreditInstallment(sur.getCreditInstallment());
           // if (sur.getCreditDownPayment() == BigDecimal.ZERO || sur.getCreditDownPayment() == null)
            System.out.println("---- CEK SUUUR----->" + sur.getSaleTypeId());
            if (sur.getSaleTypeId() == 50 || sur.getSaleTypeId() == 51 || sur.getSaleTypeId() == 54 || sur.getSaleTypeId() == 55 || sur.getSaleTypeId() == 20 || sur.getSaleTypeId() == 22) {
                dto.setCreditDownPayment(BigDecimal.ZERO);
            } else {
                dto.setCreditDownPayment(totaldp);
            }
            dto.setCreditTenor(sur.getCreditTenor());
            SalesmanDTO salesmanDTO = salesmanService.findOne(sur.getSalesmanId());
            log.debug("dto" + salesmanDTO.getAhmCode());
            dto.setSalesCode(salesmanDTO.getAhmCode());
            dto.setSalesname(salesmanDTO.getPartyName());
            if (sur.getLeasingCompanyId() != null) {
                LeasingCompanyDTO leasingCompanyDTO = leasingCompanyService.findOne(sur.getLeasingCompanyId());
                log.debug("dto fincoooooooy" + leasingCompanyDTO.getIdLeasingFincoy());
                dto.setFincoy(leasingCompanyDTO.getIdLeasingFincoy());
            }

            if (sur.getSaleTypeDescription() != null) {
                dto.setSaleTypeDesc(sur.getSaleTypeDescription());
            }
        }
//        if (billing != null) {
//            dto.setIvuNumber(billing.getBillingNumber());
//        }
        return dto;
    }

    public CustomRequirementDTO assignUD (CustomRequirementDTO dto) {
        UnitDeliverable udNotice = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(dto.getIdRequirement(),BaseConstants.DELIVERABLE_TYPE_NOTICE);
        if (udNotice != null) {
            dto.setNotice(udNotice.getDateReceipt());
            dto.setRefDate(udNotice.getRefDate());
            dto.setRefNumber(udNotice.getRefNumber());
        }

        UnitDeliverable udPlatNomor = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(dto.getIdRequirement(),BaseConstants.DELIVERABLE_TYPE_NOPOL);
        if (udPlatNomor != null) {
            dto.setPlatNomor(udPlatNomor.getDateReceipt());
        }

        UnitDeliverable udSTNK = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(dto.getIdRequirement(),BaseConstants.DELIVERABLE_TYPE_STNK);
        if (udSTNK != null) {
            dto.setStnk(udSTNK.getDateReceipt());
        }

        UnitDeliverable udBPKB = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(dto.getIdRequirement(),BaseConstants.DELIVERABLE_TYPE_BPKB);
        if (udBPKB != null) {
            dto.setBpkb(udBPKB.getDateReceipt());
            dto.setBpkbNumber(udBPKB.getBpkbNumber());

        }
        return  dto;
    }


    public CustomRequirementDTO assignSPG(CustomRequirementDTO dto) {
        System.out.println("---- MASUK ASSIGN SPG-----");
        ShipmentIncoming shipmentIncoming = orderItemRepository.findPackageReceiptbyIdFrameOrderItem(dto.getIdFrame());
        if (shipmentIncoming != null){
            System.out.println("---- MASUK ASSIGN SPG SET-----" + shipmentIncoming.getDateSchedulle());
            dto.setShippingListDate(shipmentIncoming.getDateSchedulle());
        }

        return dto;
    }

    public CustomRequirementDTO assignBilling(CustomRequirementDTO dto) {
        Billing billing = billingRepository.findByOrderItem(dto.getIdOrderItem());
        dto.setIvuNumber(billing.getBillingNumber());
        return dto;
    }


    public void buildCustomerfromSTNK(VehicleDocumentRequirement vdr){
        //build customer
        if (vdr.getPersonOwner() != null) {
            Person person = vdr.getPersonOwner();
            PersonalCustomer personSTNK = personalCustomerRepository.findByParty(person.getIdParty());
            if (personSTNK == null) {
                log.debug("CEK BUAT BUILD CUSTOMER DARI NAMA STNK");
                personSTNK = new PersonalCustomer();
                personSTNK.setPerson(vdr.getPersonOwner());
                personSTNK = customerUtils.buildPersonalCustomer(vdr.getInternal(), personSTNK);
            } else {
                log.debug("SUDAH ADA DI PERSONAL CUSTOMER ID CUSTNYA" + personSTNK.getIdCustomer());
            }
        } else {
            Organization organization = vdr.getOrganizationOwner();
            OrganizationCustomer organizationSTNK = organizationCustomerRepository.findByIdParty(organization.getIdParty());
            if (organizationSTNK == null) {
                organizationSTNK = new OrganizationCustomer();
                organizationSTNK.setOrganization(vdr.getOrganizationOwner());
                organizationSTNK = customerUtils.buildOrganizationCustomer(organizationSTNK);
            } else {
                log.debug("SUDAH ADA DI PERSONAL CUSTOMER ID CUSTNYA" + organizationSTNK.getIdCustomer());
            }
        }

    }

    public CustomRequirementDTO assignGeo(CustomRequirementDTO dto) {
        log.debug("masuuk asign geooooo");
        if (dto.getIdCustomer() != null) {
            log.debug("masuk jika ada idcustomerrrrr" + dto.getIdCustomer());

            List<CommunicationEventCDB> listCdb = communicationEventCDBRepository.queryFindCommunicationEventCDBByIdCustomer(dto.getIdCustomer());
            CommunicationEventCDB cdb = null;
            if (!listCdb.isEmpty()) cdb = listCdb.get(0);

            if (cdb != null) {
                Province provinceSurat = provinceRepository.findOne(cdb.getIdProvinceSurat());
                if (provinceSurat != null) {
                    dto.setCodeProvinceSurat(provinceSurat.getGeoCode());
                }

                City citySurat = cityRepository.findOne(cdb.getIdCitySurat());
                if (citySurat != null) {
                    dto.setCodeCitySurat(citySurat.getGeoCode());
                }

                District districtSurat = districtRepository.findOne(cdb.getIdDistrictSurat());
                if (districtSurat != null) {
                    dto.setCodeDistrictSurat(districtSurat.getGeoCode());
                }

                Village villageSurat = villageRepository.findOne(cdb.getIdVillageSurat());
                if (villageSurat != null){
                    dto.setCodeVillageSurat(villageSurat.getGeoCode());
                }
            }
        }
        else {
            log.debug("else mapping geo");
        }
        return dto;
    }

    public VehicleDocumentRequirementDTO assignIVU(VehicleDocumentRequirementDTO dto) {
        OrderItemDTO orderItemDTO = orderItemService.findOne(dto.getIdOrderItem());
        System.out.println("------------> ORDER ITEM" + orderItemDTO);
//        if (orderItemDTO.getOrdersId() != null) {
//            VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(orderItemDTO.getOrdersId());
//            System.out.println("------------> VSO" + vso);
//            System.out.println("------------> SUR IDD" + vso.getIdSalesUnitRequirement());
//            if (vso.getBillingNumber() != null) {
//                dto.setIvuNumber(vso.getBillingNumber());
//            }
//
//            SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(vso.getIdSalesUnitRequirement());
//            System.out.println("------------> SURRR" + sur);
//            System.out.println("------------> ADA ID SALESMAN GAK" + sur.getSalesman().getIdSalesman());
//            if (sur.getSalesman().getIdSalesman() != null){
//                dto.setSalesName(sur.getSalesman().getPerson().getName());
//                System.out.println("------------> NAMA SALESMAN" + sur.getSalesman().getPerson().getName());
//            }
//        }
        // cari billing
        Billing billing = billingRepository.findByOrderItem(orderItemDTO.getIdOrderItem());
        if (billing != null) {
            dto.setIvuNumber(billing.getBillingNumber());
        }

        // cari sales
        SalesUnitRequirement sur = salesUnitRequirementRepository.queryByIdOrderItem(orderItemDTO.getIdOrderItem());
        if (sur.getSalesman().getIdSalesman() != null) {
            dto.setSalesName(sur.getSalesman().getPerson().getName());
        }
        return dto;
    }

    public VehicleDocumentRequirementDTO updateNote(VehicleDocumentRequirementDTO dto) {
        VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findOne(dto.getIdRequirement());
        if (dto.getNote() != null) {
            vdr.setNote(dto.getNote());
        }
        vehicleDocumentRequirementRepository.save(vdr);
        return dto;
    }

    @Transactional(readOnly = true)
    public Page<VehicleDocumentRequirementDTO> queryFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered VehicleDocumentRequirementDTO");

        String filterName = request.getParameter("filterName");
        String idInternal = request.getParameter("idInternal");
        String idStatusType = request.getParameter("idStatusType");
        String queryFrom = request.getParameter("queryFrom");

        if (filterName != null && "byInternalStatus".equalsIgnoreCase(filterName)) {
            return vehicleDocumentRequirementRepository.queryByInternalStatus(idInternal, Integer.valueOf(idStatusType), pageable)
                .map(vehicleDocumentRequirementMapper::toDto);
        }
        if(queryFrom != null && "listbast".equalsIgnoreCase(queryFrom)) {
            return vehicleDocumentRequirementRepository.queryBast(idInternal, pageable)
                .map(vehicleDocumentRequirementMapper::toDto);
        }

        return vehicleDocumentRequirementRepository.queryNothing(pageable)
            .map(vehicleDocumentRequirementMapper::toDto);
    }


    @PostConstruct
    @Transactional
    public void initialize() {
        try {
            DefaultResourceLoader loader = new DefaultResourceLoader();

            InputStream templateXML = new BufferedInputStream(loader.getResource("classpath:/templates/excell/init-bpkb-on-hand.xml").getInputStream());
            InputStream initFile = new BufferedInputStream(loader.getResource("classpath:/templates/init/bpkbonhand.xlsx").getInputStream());

            if (templateXML != null && initFile != null) {
                ReaderConfig.getInstance().setSkipErrors( true );
                List<ImportBpkbOnHandDTO> bpkbOnHand = XlsxUtils.parseExcelStreamToBeans(initFile, templateXML);
                for (ImportBpkbOnHandDTO o: bpkbOnHand) {
                    log.debug("cek data migrasi untuk begball bpkb on hand", bpkbOnHand);
                    Map<String, Object> vars = activitiProcessor.getVariables();
                    vars.put("data", o);
                    activitiProcessor.startProcess("import-vdr", vars);
                }
            }
        }   catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        String command = (String) item.get("command");
        String idReq = (String) item.get("idRequirement");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            // buildIndex();
        } else if (command != null && "registerData".equalsIgnoreCase(command)) {
            // registerNewData();
        } else if (command != null && "doTaskCheckVDR".equalsIgnoreCase(command)) {

            ProcessInstance p = activitiProcessor.getProcessInstance("registrasi", idReq);
            if (p == null) {
                VehicleDocumentRequirement vd = vehicleDocumentRequirementRepository.findOne(UUID.fromString(idReq));
                if (vd == null) throw new DmsException("Id Requirement Tidak di kenal !");
                Map<String, Object> params = activitiProcessor.getVariables("vdr", vd);
                p = activitiProcessor.startProcess("registrasi", vd.getIdRequirement().toString(), params);
            }

            activitiProcessor.completeTask("registrasi", idReq, "act-isi-nama");
        }
        return r;
    }
}
