package id.atiila.service;

import id.atiila.domain.AssociationType;
import id.atiila.domain.BillingType;
import id.atiila.repository.BillingTypeRepository;
import id.atiila.repository.search.BillingTypeSearchRepository;
import id.atiila.route.IndexerRoute;
import id.atiila.service.dto.BillingTypeDTO;
import id.atiila.service.mapper.BillingTypeMapper;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing BillingType.
 * BeSmart Team
 */

@Service
@Transactional
public class BillingTypeService {

    private final Logger log = LoggerFactory.getLogger(BillingTypeService.class);

    private final BillingTypeRepository billingTypeRepository;

    private final BillingTypeMapper billingTypeMapper;

    private final BillingTypeSearchRepository billingTypeSearchRepository;

    @Autowired
    private ProducerTemplate producerTemplate;

    public BillingTypeService(BillingTypeRepository billingTypeRepository, BillingTypeMapper billingTypeMapper, BillingTypeSearchRepository billingTypeSearchRepository) {
        this.billingTypeRepository = billingTypeRepository;
        this.billingTypeMapper = billingTypeMapper;
        this.billingTypeSearchRepository = billingTypeSearchRepository;
    }

    /**
     * Save a billingType.
     *
     * @param billingTypeDTO the entity to save
     * @return the persisted entity
     */
    public BillingTypeDTO save(BillingTypeDTO billingTypeDTO) {
        log.debug("Request to save BillingType : {}", billingTypeDTO);
        BillingType billingType = billingTypeMapper.toEntity(billingTypeDTO);
        billingType = billingTypeRepository.save(billingType);
        BillingTypeDTO result = billingTypeMapper.toDto(billingType);
        billingTypeSearchRepository.save(billingType);
        return result;
    }

    /**
     *  Get all the billingTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BillingTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BillingTypes");
        return billingTypeRepository.findAll(pageable)
            .map(billingTypeMapper::toDto);
    }

    /**
     *  Get one billingType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public BillingTypeDTO findOne(Integer id) {
        log.debug("Request to get BillingType : {}", id);
        BillingType billingType = billingTypeRepository.findOne(id);
        return billingTypeMapper.toDto(billingType);
    }

    /**
     *  Delete the  billingType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete BillingType : {}", id);
        billingTypeRepository.delete(id);
        billingTypeSearchRepository.delete(id);
    }

    /**
     * Search for the billingType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BillingTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of BillingTypes for query {}", query);
        Page<BillingType> result = billingTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(billingTypeMapper::toDto);
    }

    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    public void buildIndex() {
        billingTypeSearchRepository.deleteAll();
        List<BillingType> billingTypes =  billingTypeRepository.findAll();
        for (BillingType b: billingTypes) {
                billingTypeSearchRepository.save(b);
                log.debug("Data Billing Type save !...");
        }
    }

}
