package id.atiila.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.xml.bind.v2.runtime.reflect.opt.Const;
import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.BillingDisbursement;
import id.atiila.domain.BillingItem;
import id.atiila.domain.Internal;
import id.atiila.domain.InternalBankMaping;
import id.atiila.repository.BillingDisbursementRepository;
import id.atiila.repository.InternalBankMapingRepository;
import id.atiila.repository.InternalRepository;
import id.atiila.repository.search.BillingDisbursementSearchRepository;
import id.atiila.route.DemandSupplyRoute;
import id.atiila.service.dto.BillingDisbursementDTO;
import id.atiila.service.dto.InvoiceUploadDTO;
import id.atiila.service.impl.BillingBeanImpl;
import id.atiila.service.impl.OrderBeanImpl;
import id.atiila.service.mapper.BillingDisbursementMapper;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing BillingDisbursement.
 * BeSmart Team
 */

@Service
@Transactional
public class BillingDisbursementService {

    private final Logger log = LoggerFactory.getLogger(BillingDisbursementService.class);

    private final BillingDisbursementRepository billingDisbursementRepository;

    private final BillingDisbursementMapper billingDisbursementMapper;

    private final BillingDisbursementSearchRepository billingDisbursementSearchRepository;

    public static final String ID_PROCESS = "unit-do";

    @Autowired
    private ProducerTemplate camelTemplate;

    @Autowired
    private BillingBeanImpl billingBean;

    @Autowired
    private OrderBeanImpl orderBean;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private ActivitiProcessor activitiProcessor;

    @Autowired
    private InternalBankMapingRepository internalBankMapingRepository;

    @Autowired
    private InternalRepository internalRepository;

    public BillingDisbursementService(BillingDisbursementRepository billingDisbursementRepository, BillingDisbursementMapper billingDisbursementMapper, BillingDisbursementSearchRepository billingDisbursementSearchRepository) {
        this.billingDisbursementRepository = billingDisbursementRepository;
        this.billingDisbursementMapper = billingDisbursementMapper;
        this.billingDisbursementSearchRepository = billingDisbursementSearchRepository;
    }

    /**
     * Save a billingDisbursement.
     *
     * @param billingDisbursementDTO the entity to save
     * @return the persisted entity
     */
    public BillingDisbursementDTO save(BillingDisbursementDTO billingDisbursementDTO) {
        log.debug("Request to save BillingDisbursement : {}", billingDisbursementDTO);
        if(billingDisbursementDTO.getVendorInvoice() == null){
            throw new DmsException("No Invoice Belum Diisi");
        }
        if(billingDisbursementDTO.getDiscountLine() == null){
            billingDisbursementDTO.setDiscountLine(BigDecimal.valueOf(0));
        }
        if(billingDisbursementDTO.getFreightCost() == null){
            billingDisbursementDTO.setFreightCost(BigDecimal.valueOf(0));
        }
        BillingDisbursement billingDisbursement = billingDisbursementMapper.toEntity(billingDisbursementDTO);
        billingDisbursement = billingDisbursementRepository.save(billingDisbursement);
        BillingDisbursementDTO result = billingDisbursementMapper.toDto(billingDisbursement);
        billingDisbursementSearchRepository.save(billingDisbursement);
        return result;
    }

    /**
     * Get all the billingDisbursements.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BillingDisbursementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BillingDisbursements");
        return billingDisbursementRepository.findActiveBillingDisbursement(pageable)
            .map(billingDisbursementMapper::toDto);
    }

    /**
     * Get one billingDisbursement by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BillingDisbursementDTO findOne(UUID id) {
        log.debug("Request to get BillingDisbursement : {}", id);
        BillingDisbursement billingDisbursement = billingDisbursementRepository.findOne(id);
        return billingDisbursementMapper.toDto(billingDisbursement);
    }

    /**
     * Delete the billingDisbursement by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete BillingDisbursement : {}", id);
        billingDisbursementRepository.delete(id);
        billingDisbursementSearchRepository.delete(id);
    }

    /**
     * Search for the billingDisbursement corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BillingDisbursementDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of BillingDisbursements for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idBillingType = request.getParameter("idBillingType");
        String idInternal = request.getParameter("idInternal");
        String idBillTo = request.getParameter("idBillTo");
        String idBillFrom = request.getParameter("idBillFrom");
        String idVendor = request.getParameter("idVendor");

        if (filterName != null) {
        }
        Page<BillingDisbursement> result = billingDisbursementSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(billingDisbursementMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<BillingDisbursementDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered jancuk");
        String idBillingType = request.getParameter("idBillingType");
        String idInternal = request.getParameter("idInternal");
        log.debug("internal jancukkk==  " + idInternal);
        String idBillTo = request.getParameter("idBillTo");
        String idBillFrom = request.getParameter("idBillFrom");
        String idVendor = request.getParameter("idVendor");
        if (idInternal != null) {
            log.debug("internal dongahhhh===  " + idInternal);
            log.debug("jacukkkk " , idInternal);
            return billingDisbursementRepository.findActiveBilling(idInternal, pageable).map(billingDisbursementMapper::toDto);
        }

        return billingDisbursementRepository.findByParams(idBillingType, idInternal, idBillTo, idBillFrom, idVendor, pageable)
            .map(billingDisbursementMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public BillingDisbursementDTO calculate(BillingDisbursementDTO dto){
        List<BillingItem> bi = billingDisbursementRepository.findBillingItemByBillingDisbursement(dto.getIdBilling());
        Internal internal = internalRepository.findByIdInternal(dto.getInternalId());
        List<InternalBankMaping> internalBankMaping = internalBankMapingRepository.FindByIdRoot(internal.getRoot().getIdInternal());
        BigDecimal jumlah = BigDecimal.valueOf(0);
        Double totalFeigh = 0d;
        Double total = 0d;
        if(bi != null){
            for(BillingItem bit: bi) {
                jumlah = bit.getTotalAmount();
                total = jumlah.doubleValue() + total.doubleValue();
            }
            if(dto.getDiscountLine() == null){
                dto.setDiscountLine(BigDecimal.valueOf(0));
            }
            if(dto.getFreightCost() == null){
                dto.setFreightCost(BigDecimal.valueOf(0));
            }
            Double ppnDiscount = 0D;
            Double ppnFreightCost = 0D;
            if(!internalBankMaping.isEmpty() && internalBankMaping.iterator().next().getIsppn() == 0){
                ppnDiscount = 0D;
                ppnFreightCost = 0D;
            } else {
                ppnDiscount = dto.getDiscountLine().doubleValue() * 0.1;
                ppnFreightCost = dto.getFreightCost().doubleValue() * 0.1;
            }
            totalFeigh = total - dto.getDiscountLine().doubleValue() + dto.getFreightCost().doubleValue() + ppnFreightCost - ppnDiscount;
            dto.setTotalFeightCost(totalFeigh);
        }
        return dto;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public BillingDisbursementDTO processCalData(Integer id, String param, BillingDisbursementDTO dto) {
        BillingDisbursementDTO r = dto;
        switch (id){
            case 10:
                r = this.calculate(r);
                break;
            default:
                break;
        }
          return r;
    }

    @Transactional
    public BillingDisbursementDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        String execute = request.getParameter("execute").toString();
        BillingDisbursementDTO r = null;

        if (execute.equals("BuildAndValidateBillingDisbursement")){
            ObjectMapper mapper = new ObjectMapper();
            mapper.findAndRegisterModules();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

            r = mapper.convertValue(item.get("item"), new TypeReference<BillingDisbursementDTO>() {});

            List<BillingDisbursement> items = billingDisbursementRepository.findOneByVendorInvoice(r.getVendorInvoice());
            BillingDisbursement e = items.size() > 0 ? items.get(0) : null;

            if (e == null){
                r = save(r);
            }
            else {
                throw new DmsException("Data Invoice Sudah Ada");
            }
        }
        return r;
    }

    @Transactional
    public Set<BillingDisbursementDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        String execute = request.getParameter("execute").toString();
        Set<BillingDisbursementDTO> r = new HashSet<>();
        Set<InvoiceUploadDTO> dtos;

        if (execute.equals("UploadInvoice")){

            ObjectMapper mapper = new ObjectMapper();
            mapper.findAndRegisterModules();

            List<Object> objects = (List<Object>) item.get("items");
            dtos = mapper.convertValue(objects, new TypeReference<Set<InvoiceUploadDTO>>() { });

//            //get internal
//            Internal internal = internalUtils.findOne(dtos.iterator().next().getCustomerCode());
//            if (internal == null){
//                throw new DmsException("Id Internal tidak di kenal !");
//            }

            //Find existing billing disbursement
            List<BillingDisbursement> items = billingDisbursementRepository.findOneByVendorInvoice(dtos.iterator().next().getInvoiceNumber());
            BillingDisbursement billing = items.size() > 0 ? items.get(0) : null;

            if (billing != null) {
                throw new DmsException("Data Invoice Sudah Ada");
            }

            Map<String, Object> headers = new HashMap<>();
            headers.put("billingTypeId", 12);
            headers.put("billingItemTypeId", 101);
            headers.put("poNumber", dtos.iterator().next().getPoNumber());

            billing = billingBean.convertUploadInvoiceToBillingDisbursement(dtos,headers);
            orderBean.convertBillingDisbursementToPurchaseOrder(billing, headers);

            // camelTemplate.sendBodyAndHeader(DemandSupplyRoute.ACTION_UPLOAD_INVOICE, dtos, "parameters", headers);
        }

        return r;
    }

    @Transactional
    public BillingDisbursementDTO changeBillingDisbursementStatus(BillingDisbursementDTO dto, Integer id) {
        BillingDisbursement e = null;
        if (dto != null) {

            e = billingDisbursementRepository.findOne(dto.getIdBilling());
            Integer currrentState = e.getCurrentStatus();

            // Kalau akan di cancel
            if (id == BaseConstants.STATUS_CANCEL) {
                e.setStatus(id);
                e = billingDisbursementRepository.saveAndFlush(e);
                activitiProcessor.cancelProcessInstance(ID_PROCESS, e.getIdBilling().toString());
                return billingDisbursementMapper.toDto(e);
            };

            // Proses Normal
            switch (currrentState) {
                case BaseConstants.STATUS_DRAFT: {
                    Map<String, Object> params = activitiProcessor.getVariables("billing", e);
                    ProcessInstance p = activitiProcessor.startProcess(ID_PROCESS, e.getIdBilling().toString(), params);

                    List<Task> items = activitiProcessor.getTasks(ID_PROCESS, e.getIdBilling().toString());
                    if (items.size() > 0) {
                        activitiProcessor.completeTask(items.get(0).getId());
                    }
                    break;
                }
                default: {
                    List<Task> items = activitiProcessor.getTasks(ID_PROCESS, e.getIdBilling().toString());
                    if (items.size() > 0) {
                        activitiProcessor.completeTask(items.get(0).getId());
                    }
                    break;
                }
            }
		}
        return billingDisbursementMapper.toDto(e);
    }

}
