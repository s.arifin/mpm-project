package id.atiila.service;

import id.atiila.domain.DimensionType;
import id.atiila.repository.DimensionTypeRepository;
import id.atiila.repository.search.DimensionTypeSearchRepository;
import id.atiila.service.dto.DimensionTypeDTO;
import id.atiila.service.mapper.DimensionTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing DimensionType.
 * atiila consulting
 */

@Service
@Transactional
public class DimensionTypeService {

    private final Logger log = LoggerFactory.getLogger(DimensionTypeService.class);

    private final DimensionTypeRepository dimensionTypeRepository;

    private final DimensionTypeMapper dimensionTypeMapper;

    private final DimensionTypeSearchRepository dimensionTypeSearchRepository;

    public DimensionTypeService(DimensionTypeRepository dimensionTypeRepository, DimensionTypeMapper dimensionTypeMapper, DimensionTypeSearchRepository dimensionTypeSearchRepository) {
        this.dimensionTypeRepository = dimensionTypeRepository;
        this.dimensionTypeMapper = dimensionTypeMapper;
        this.dimensionTypeSearchRepository = dimensionTypeSearchRepository;
    }

    /**
     * Save a dimensionType.
     *
     * @param dimensionTypeDTO the entity to save
     * @return the persisted entity
     */
    public DimensionTypeDTO save(DimensionTypeDTO dimensionTypeDTO) {
        log.debug("Request to save DimensionType : {}", dimensionTypeDTO);
        DimensionType dimensionType = dimensionTypeMapper.toEntity(dimensionTypeDTO);
        dimensionType = dimensionTypeRepository.save(dimensionType);
        DimensionTypeDTO result = dimensionTypeMapper.toDto(dimensionType);
        dimensionTypeSearchRepository.save(dimensionType);
        return result;
    }

    /**
     * Get all the dimensionTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DimensionTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DimensionTypes");
        return dimensionTypeRepository.findAll(pageable)
            .map(dimensionTypeMapper::toDto);
    }

    /**
     * Get one dimensionType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public DimensionTypeDTO findOne(Integer id) {
        log.debug("Request to get DimensionType : {}", id);
        DimensionType dimensionType = dimensionTypeRepository.findOne(id);
        return dimensionTypeMapper.toDto(dimensionType);
    }

    /**
     * Delete the dimensionType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete DimensionType : {}", id);
        dimensionTypeRepository.delete(id);
        dimensionTypeSearchRepository.delete(id);
    }

    /**
     * Search for the dimensionType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DimensionTypeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of DimensionTypes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));


        Page<DimensionType> result = dimensionTypeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(dimensionTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<DimensionTypeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered DimensionTypeDTO");


        return dimensionTypeRepository.queryNothing(pageable).map(dimensionTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, DimensionTypeDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<DimensionTypeDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
