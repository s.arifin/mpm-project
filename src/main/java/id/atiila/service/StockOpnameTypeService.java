package id.atiila.service;

import id.atiila.domain.StockOpnameType;
import id.atiila.repository.StockOpnameTypeRepository;
import id.atiila.repository.search.StockOpnameTypeSearchRepository;
import id.atiila.service.dto.StockOpnameTypeDTO;
import id.atiila.service.mapper.StockOpnameTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing StockOpnameType.
 * BeSmart Team
 */

@Service
@Transactional
public class StockOpnameTypeService {

    private final Logger log = LoggerFactory.getLogger(StockOpnameTypeService.class);

    private final StockOpnameTypeRepository stockOpnameTypeRepository;

    private final StockOpnameTypeMapper stockOpnameTypeMapper;

    private final StockOpnameTypeSearchRepository stockOpnameTypeSearchRepository;

    public StockOpnameTypeService(StockOpnameTypeRepository stockOpnameTypeRepository, StockOpnameTypeMapper stockOpnameTypeMapper, StockOpnameTypeSearchRepository stockOpnameTypeSearchRepository) {
        this.stockOpnameTypeRepository = stockOpnameTypeRepository;
        this.stockOpnameTypeMapper = stockOpnameTypeMapper;
        this.stockOpnameTypeSearchRepository = stockOpnameTypeSearchRepository;
    }

    /**
     * Save a stockOpnameType.
     *
     * @param stockOpnameTypeDTO the entity to save
     * @return the persisted entity
     */
    public StockOpnameTypeDTO save(StockOpnameTypeDTO stockOpnameTypeDTO) {
        log.debug("Request to save StockOpnameType : {}", stockOpnameTypeDTO);
        StockOpnameType stockOpnameType = stockOpnameTypeMapper.toEntity(stockOpnameTypeDTO);
        stockOpnameType = stockOpnameTypeRepository.save(stockOpnameType);
        StockOpnameTypeDTO result = stockOpnameTypeMapper.toDto(stockOpnameType);
        stockOpnameTypeSearchRepository.save(stockOpnameType);
        return result;
    }

    /**
     * Get all the stockOpnameTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StockOpnameTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all StockOpnameTypes");
        return stockOpnameTypeRepository.findAll(pageable)
            .map(stockOpnameTypeMapper::toDto);
    }

    /**
     * Get one stockOpnameType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public StockOpnameTypeDTO findOne(Integer id) {
        log.debug("Request to get StockOpnameType : {}", id);
        StockOpnameType stockOpnameType = stockOpnameTypeRepository.findOne(id);
        return stockOpnameTypeMapper.toDto(stockOpnameType);
    }

    /**
     * Delete the stockOpnameType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete StockOpnameType : {}", id);
        stockOpnameTypeRepository.delete(id);
        stockOpnameTypeSearchRepository.delete(id);
    }

    /**
     * Search for the stockOpnameType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StockOpnameTypeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of StockOpnameTypes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));


        Page<StockOpnameType> result = stockOpnameTypeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(stockOpnameTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<StockOpnameTypeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered StockOpnameTypeDTO");


        return stockOpnameTypeRepository.queryNothing(pageable).map(stockOpnameTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, StockOpnameTypeDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<StockOpnameTypeDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
