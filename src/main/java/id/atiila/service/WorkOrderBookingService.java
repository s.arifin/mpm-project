package id.atiila.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.time.ZonedDateTime;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.atiila.base.BaseConstants;
import id.atiila.domain.PersonalCustomer;
import id.atiila.domain.VehicleIdentification;
import id.atiila.domain.WorkOrderBooking;
import id.atiila.repository.VehicleIdentificationRepository;
import id.atiila.repository.WorkOrderBookingRepository;
import id.atiila.repository.search.WorkOrderBookingSearchRepository;
import id.atiila.service.dto.PersonalCustomerDTO;
import id.atiila.service.dto.VehicleDTO;
import id.atiila.service.dto.WorkOrderBookingDTO;
import id.atiila.service.mapper.WorkOrderBookingMapper;

import javax.servlet.http.HttpServletRequest;

/**
 * Service Implementation for managing WorkOrderBooking.
 * BeSmart Team
 */

@Service
@Transactional
public class WorkOrderBookingService {

    private final Logger log = LoggerFactory.getLogger(WorkOrderBookingService.class);

    private final WorkOrderBookingRepository workOrderBookingRepository;

    private final WorkOrderBookingMapper workOrderBookingMapper;

    private final WorkOrderBookingSearchRepository workOrderBookingSearchRepository;

    @Autowired
    private MasterNumberingService numberSvc;

    @Autowired
    private PersonalCustomerService custServ;

    @Autowired
    private VehicleService veServ;

    @Autowired
    private VehicleIdentificationRepository veidRepo;

    public WorkOrderBookingService(WorkOrderBookingRepository workOrderBookingRepository, WorkOrderBookingMapper workOrderBookingMapper, WorkOrderBookingSearchRepository workOrderBookingSearchRepository) {
        this.workOrderBookingRepository = workOrderBookingRepository;
        this.workOrderBookingMapper = workOrderBookingMapper;
        this.workOrderBookingSearchRepository = workOrderBookingSearchRepository;
    }

    /**
     * Save a workOrderBooking.
     *
     * @param dto the entity to save
     * @return the persisted entity
     */
    public WorkOrderBookingDTO save(WorkOrderBookingDTO dto) {
        log.debug("Request to save WorkOrderBooking : {}", dto);

        WorkOrderBooking workOrderBooking = null;

        if (dto.getCustomer().getIdCustomer() == null) {
            PersonalCustomerDTO pcd = custServ.save(dto.getCustomer());
            dto.setCustomer(pcd);
        }

        if (dto.getVehicle().getIdVehicle() == null) {
            VehicleDTO vd = veServ.save(dto.getVehicle());
            dto.setVehicle(vd);
        }

        workOrderBooking = workOrderBookingMapper.toEntity(dto);
        Boolean isNewData = workOrderBooking.getIdBooking() == null;

        if (isNewData) {
            WorkOrderBooking wob = null;

            //Cek Jika Booking sudah mempunyai ID
            if (dto.getBookingNumber() != null) {
                wob = workOrderBookingRepository.findOneByBookingNumber(dto.getBookingNumber());
                if (wob != null) return workOrderBookingMapper.toDto(wob);
            } else {
                String idWob = null;
                Integer year = ZonedDateTime.now().getYear();
                int bookingType = dto.getBookingTypeId() != null ? dto.getBookingTypeId() : 0;
                while (idWob == null || workOrderBookingRepository.findOneByBookingNumber(idWob) != null) {
                    switch (bookingType) {
                        case 10: {
                            idWob = "B" +  String.format("%05d", numberSvc.nextValue("idwobooking", year.toString()));
                            break;
                        }case 11: {
                            idWob = "H" +  String.format("%05d", numberSvc.nextValue("idwobooking", year.toString()));
                            break;
                        }case 12: {
                            idWob = "E" +  String.format("%05d", numberSvc.nextValue("idwobooking", year.toString()));
                            break;
                        }
                        default:
                            idWob = "B/" + year + "/" +  String.format("%05d", numberSvc.nextValue("idwobooking", year.toString()));
                    }

                }
                workOrderBooking.setBookingNumber(idWob);
                workOrderBooking.setStatus(BaseConstants.STATUS_DRAFT);
            }
        }

        workOrderBooking = workOrderBookingRepository.save(workOrderBooking);
        WorkOrderBookingDTO result = workOrderBookingMapper.toDto(workOrderBooking);
        workOrderBookingSearchRepository.save(workOrderBooking);
        return result;
    }

    /**
     *  Get all the workOrderBookings.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WorkOrderBookingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WorkOrderBookings");
        return workOrderBookingRepository.findActiveWorkOrderBooking(pageable)
            .map(workOrderBookingMapper::toDto);
    }

    /**
     *  Get one workOrderBooking by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public WorkOrderBookingDTO findOne(UUID id) {
        log.debug("Request to get WorkOrderBooking : {}", id);
        WorkOrderBooking workOrderBooking = workOrderBookingRepository.findOne(id);
        return workOrderBookingMapper.toDto(workOrderBooking);
    }

    /**
     *  Delete the  workOrderBooking by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete WorkOrderBooking : {}", id);
        workOrderBookingRepository.delete(id);
        workOrderBookingSearchRepository.delete(id);
    }

    /**
     * Search for the workOrderBooking corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WorkOrderBookingDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WorkOrderBookings for query {}", query);
        Page<WorkOrderBooking> result = workOrderBookingSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(workOrderBookingMapper::toDto);
    }

    public WorkOrderBookingDTO processExecuteData(Integer id, String param, WorkOrderBookingDTO dto) {
        WorkOrderBookingDTO r = dto;
        if (r != null) {
            switch (id) {
                // Build new Booking base on VehicleIdentification
                case 101: {
                    List<VehicleIdentification> lvid = veidRepo.findAllByVehicleNumber(param);
                    if (!lvid.isEmpty()) {
                        VehicleIdentification vid = lvid.get(0);
                        WorkOrderBooking w = new WorkOrderBooking();
                        w.setVehicleNumber(param);
                        w.setCustomer((PersonalCustomer) vid.getCustomer());
                        w.setVehicle(vid.getVehicle());
                        r = workOrderBookingMapper.toDto(w);
                        r.setBookingTypeId(10);
                        r.setEventTypeId(10);
                        r = save(r);
                    }
                }
                default:
                    break;
            }
		}
        return r;
    }

    public Set<WorkOrderBookingDTO> processExecuteListData(Integer id, String param, Set<WorkOrderBookingDTO> dto) {
        Set<WorkOrderBookingDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public WorkOrderBookingDTO changeWorkOrderBookingStatus(WorkOrderBookingDTO dto, Integer id) {
        if (dto != null) {
			WorkOrderBooking e = workOrderBookingRepository.findOne(dto.getIdBooking());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        workOrderBookingSearchRepository.delete(dto.getIdBooking());
                        break;
                    default:
                        workOrderBookingSearchRepository.save(e);
                }
				workOrderBookingRepository.save(e);
			}
		}
        return dto;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        workOrderBookingSearchRepository.deleteAll();
        List<WorkOrderBooking> workOrderBookings =  workOrderBookingRepository.findAll();
        for (WorkOrderBooking m: workOrderBookings) {
            workOrderBookingSearchRepository.save(m);
            log.debug("Data work order booking save !...");
        }
    }
}
