package id.atiila.service;

import id.atiila.domain.RequestRequirement;
import id.atiila.repository.RequestRequirementRepository;
import id.atiila.repository.search.RequestRequirementSearchRepository;
import id.atiila.service.dto.CustomRequestRequirementDTO;
import id.atiila.service.dto.RequestRequirementDTO;
import id.atiila.service.mapper.RequestRequirementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing RequestRequirement.
 * BeSmart Team
 */

@Service
@Transactional
public class RequestRequirementService {

    private final Logger log = LoggerFactory.getLogger(RequestRequirementService.class);

    private final RequestRequirementRepository requestRequirementRepository;

    private final RequestRequirementMapper requestRequirementMapper;

    private final RequestRequirementSearchRepository requestRequirementSearchRepository;

    public RequestRequirementService(RequestRequirementRepository requestRequirementRepository, RequestRequirementMapper requestRequirementMapper, RequestRequirementSearchRepository requestRequirementSearchRepository) {
        this.requestRequirementRepository = requestRequirementRepository;
        this.requestRequirementMapper = requestRequirementMapper;
        this.requestRequirementSearchRepository = requestRequirementSearchRepository;
    }

    public List<CustomRequestRequirementDTO> getAllCustomRequestRequirement(){
        return requestRequirementRepository.getCustomRequestRequirement("10001");
    }

    /**
     * Save a requestRequirement.
     *
     * @param requestRequirementDTO the entity to save
     * @return the persisted entity
     */
    public RequestRequirementDTO save(RequestRequirementDTO requestRequirementDTO) {
        log.debug("Request to save RequestRequirement : {}", requestRequirementDTO);
        RequestRequirement requestRequirement = requestRequirementMapper.toEntity(requestRequirementDTO);
        requestRequirement = requestRequirementRepository.save(requestRequirement);
        RequestRequirementDTO result = requestRequirementMapper.toDto(requestRequirement);
        requestRequirementSearchRepository.save(requestRequirement);
        return result;
    }

    /**
     * Get all the requestRequirements.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequestRequirementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RequestRequirements");
        return requestRequirementRepository.findActiveRequestRequirement(pageable)
            .map(requestRequirementMapper::toDto);
    }

    /**
     * Get one requestRequirement by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public RequestRequirementDTO findOne(UUID id) {
        log.debug("Request to get RequestRequirement : {}", id);
        RequestRequirement requestRequirement = requestRequirementRepository.findOne(id);
        return requestRequirementMapper.toDto(requestRequirement);
    }

    /**
     * Delete the requestRequirement by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete RequestRequirement : {}", id);
        requestRequirementRepository.delete(id);
        requestRequirementSearchRepository.delete(id);
    }

    /**
     * Search for the requestRequirement corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequestRequirementDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of RequestRequirements for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idRequirement = request.getParameter("idRequirement");

        if (filterName != null) {
        }
        Page<RequestRequirement> result = requestRequirementSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(requestRequirementMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<RequestRequirementDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered RequestRequirementDTO");
        String idRequirement = request.getParameter("idRequirement");

        return requestRequirementRepository.findByParams(idRequirement, pageable)
            .map(requestRequirementMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public RequestRequirementDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        RequestRequirementDTO r = null;
        return r;
    }

    @Transactional
    public Set<RequestRequirementDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<RequestRequirementDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public RequestRequirementDTO changeRequestRequirementStatus(RequestRequirementDTO dto, Integer id) {
        if (dto != null) {
			RequestRequirement e = requestRequirementRepository.findOne(dto.getIdRequest());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        requestRequirementSearchRepository.delete(dto.getIdRequest());
                        break;
                    default:
                        requestRequirementSearchRepository.save(e);
                }
				requestRequirementRepository.save(e);
			}
		}
        return dto;
    }
}
