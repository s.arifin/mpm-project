package id.atiila.service;

import id.atiila.domain.ReturnSalesOrder;
import id.atiila.repository.ReturnSalesOrderRepository;
import id.atiila.repository.search.ReturnSalesOrderSearchRepository;
import id.atiila.service.dto.ReturnSalesOrderDTO;
import id.atiila.service.mapper.ReturnSalesOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ReturnSalesOrder.
 * BeSmart Team
 */

@Service
@Transactional
public class ReturnSalesOrderService {

    private final Logger log = LoggerFactory.getLogger(ReturnSalesOrderService.class);

    private final ReturnSalesOrderRepository returnSalesOrderRepository;

    private final ReturnSalesOrderMapper returnSalesOrderMapper;

    private final ReturnSalesOrderSearchRepository returnSalesOrderSearchRepository;

    public ReturnSalesOrderService(ReturnSalesOrderRepository returnSalesOrderRepository, ReturnSalesOrderMapper returnSalesOrderMapper, ReturnSalesOrderSearchRepository returnSalesOrderSearchRepository) {
        this.returnSalesOrderRepository = returnSalesOrderRepository;
        this.returnSalesOrderMapper = returnSalesOrderMapper;
        this.returnSalesOrderSearchRepository = returnSalesOrderSearchRepository;
    }

    /**
     * Save a returnSalesOrder.
     *
     * @param returnSalesOrderDTO the entity to save
     * @return the persisted entity
     */
    public ReturnSalesOrderDTO save(ReturnSalesOrderDTO returnSalesOrderDTO) {
        log.debug("Request to save ReturnSalesOrder : {}", returnSalesOrderDTO);
        ReturnSalesOrder returnSalesOrder = returnSalesOrderMapper.toEntity(returnSalesOrderDTO);
        returnSalesOrder = returnSalesOrderRepository.save(returnSalesOrder);
        ReturnSalesOrderDTO result = returnSalesOrderMapper.toDto(returnSalesOrder);
        returnSalesOrderSearchRepository.save(returnSalesOrder);
        return result;
    }

    /**
     * Get all the returnSalesOrders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ReturnSalesOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ReturnSalesOrders");
        return returnSalesOrderRepository.findActiveReturnSalesOrder(pageable)
            .map(returnSalesOrderMapper::toDto);
    }

    /**
     * Get one returnSalesOrder by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ReturnSalesOrderDTO findOne(UUID id) {
        log.debug("Request to get ReturnSalesOrder : {}", id);
        ReturnSalesOrder returnSalesOrder = returnSalesOrderRepository.findOne(id);
        return returnSalesOrderMapper.toDto(returnSalesOrder);
    }

    /**
     * Delete the returnSalesOrder by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ReturnSalesOrder : {}", id);
        returnSalesOrderRepository.delete(id);
        returnSalesOrderSearchRepository.delete(id);
    }

    /**
     * Search for the returnSalesOrder corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ReturnSalesOrderDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of ReturnSalesOrders for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idInternal = request.getParameter("idInternal");
        String idCustomer = request.getParameter("idCustomer");
        String idBillTo = request.getParameter("idBillTo");

        if (filterName != null) {
        }
        Page<ReturnSalesOrder> result = returnSalesOrderSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(returnSalesOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ReturnSalesOrderDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ReturnSalesOrderDTO");
        String idInternal = request.getParameter("idInternal");
        String idCustomer = request.getParameter("idCustomer");
        String idBillTo = request.getParameter("idBillTo");

        return returnSalesOrderRepository.findByParams(idInternal, idCustomer, idBillTo, pageable)
            .map(returnSalesOrderMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public ReturnSalesOrderDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        ReturnSalesOrderDTO r = null;
        return r;
    }

    @Transactional
    public Set<ReturnSalesOrderDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<ReturnSalesOrderDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public ReturnSalesOrderDTO changeReturnSalesOrderStatus(ReturnSalesOrderDTO dto, Integer id) {
        if (dto != null) {
			ReturnSalesOrder e = returnSalesOrderRepository.findOne(dto.getIdOrder());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        returnSalesOrderSearchRepository.delete(dto.getIdOrder());
                        break;
                    default:
                        returnSalesOrderSearchRepository.save(e);
                }
				returnSalesOrderRepository.save(e);
			}
		}
        return dto;
    }
}
