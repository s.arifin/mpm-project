package id.atiila.service;

import id.atiila.domain.BookingSlot;
import id.atiila.repository.BookingSlotRepository;
import id.atiila.repository.search.BookingSlotSearchRepository;
import id.atiila.service.dto.BookingSlotDTO;
import id.atiila.service.mapper.BookingSlotMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.PostConstruct;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing BookingSlot.
 * BeSmart Team
 */

@Service
@Transactional
public class BookingSlotService {

    private final Logger log = LoggerFactory.getLogger(BookingSlotService.class);

    private final BookingSlotRepository bookingSlotRepository;

    private final BookingSlotMapper bookingSlotMapper;

    private final BookingSlotSearchRepository bookingSlotSearchRepository;

    public BookingSlotService(BookingSlotRepository bookingSlotRepository, BookingSlotMapper bookingSlotMapper, BookingSlotSearchRepository bookingSlotSearchRepository) {
        this.bookingSlotRepository = bookingSlotRepository;
        this.bookingSlotMapper = bookingSlotMapper;
        this.bookingSlotSearchRepository = bookingSlotSearchRepository;
    }

    /**
     * Save a bookingSlot.
     *
     * @param bookingSlotDTO the entity to save
     * @return the persisted entity
     */
    public BookingSlotDTO save(BookingSlotDTO bookingSlotDTO) {
        log.debug("Request to save BookingSlot : {}", bookingSlotDTO);
        BookingSlot bookingSlot = bookingSlotMapper.toEntity(bookingSlotDTO);
        bookingSlot = bookingSlotRepository.save(bookingSlot);
        BookingSlotDTO result = bookingSlotMapper.toDto(bookingSlot);
        bookingSlotSearchRepository.save(bookingSlot);
        return result;
    }

    /**
     *  Get all the bookingSlots.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BookingSlotDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BookingSlots");
        return bookingSlotRepository.findAll(pageable)
            .map(bookingSlotMapper::toDto);
    }

    /**
     *  Get one bookingSlot by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public BookingSlotDTO findOne(UUID id) {
        log.debug("Request to get BookingSlot : {}", id);
        BookingSlot bookingSlot = bookingSlotRepository.findOne(id);
        return bookingSlotMapper.toDto(bookingSlot);
    }

    /**
     *  Delete the  bookingSlot by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete BookingSlot : {}", id);
        bookingSlotRepository.delete(id);
        bookingSlotSearchRepository.delete(id);
    }

    /**
     * Search for the bookingSlot corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BookingSlotDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of BookingSlots for query {}", query);
        Page<BookingSlot> result = bookingSlotSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(bookingSlotMapper::toDto);
    }

    public BookingSlotDTO processExecuteData(BookingSlotDTO dto, Integer id) {
        BookingSlotDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @PostConstruct
    public void initialize() {
    }

}
