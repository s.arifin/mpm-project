package id.atiila.service;

import id.atiila.domain.PartyContactMechanism;
import id.atiila.repository.PartyContactMechanismRepository;
import id.atiila.repository.search.PartyContactMechanismSearchRepository;
import id.atiila.service.dto.PartyContactMechanismDTO;
import id.atiila.service.mapper.PartyContactMechanismMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing PartyContactMechanism.
 * BeSmart Team
 */

@Service
@Transactional
public class PartyContactMechanismService {

    private final Logger log = LoggerFactory.getLogger(PartyContactMechanismService.class);

    private final PartyContactMechanismRepository PartyContactMechanismRepository;

    private final PartyContactMechanismMapper PartyContactMechanismMapper;

    private final PartyContactMechanismSearchRepository PartyContactMechanismSearchRepository;

    public PartyContactMechanismService(PartyContactMechanismRepository PartyContactMechanismRepository, PartyContactMechanismMapper PartyContactMechanismMapper, PartyContactMechanismSearchRepository PartyContactMechanismSearchRepository) {
        this.PartyContactMechanismRepository = PartyContactMechanismRepository;
        this.PartyContactMechanismMapper = PartyContactMechanismMapper;
        this.PartyContactMechanismSearchRepository = PartyContactMechanismSearchRepository;
    }

    /**
     * Save a PartyContactMechanism.
     *
     * @param PartyContactMechanismDTO the entity to save
     * @return the persisted entity
     */
    public PartyContactMechanismDTO save(PartyContactMechanismDTO PartyContactMechanismDTO) {
        log.debug("Request to save PartyContactMechanism : {}", PartyContactMechanismDTO);
        PartyContactMechanism PartyContactMechanism = PartyContactMechanismMapper.toEntity(PartyContactMechanismDTO);
        PartyContactMechanism = PartyContactMechanismRepository.save(PartyContactMechanism);
        PartyContactMechanismDTO result = PartyContactMechanismMapper.toDto(PartyContactMechanism);
        PartyContactMechanismSearchRepository.save(PartyContactMechanism);
        return result;
    }

    /**
     *  Get all the PartyContactMechanisms.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartyContactMechanismDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PartyContactMechanisms");
        return PartyContactMechanismRepository.findAll(pageable)
            .map(PartyContactMechanismMapper::toDto);
    }

    /**
     *  Get one PartyContactMechanism by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PartyContactMechanismDTO findOne(Long id) {
        log.debug("Request to get PartyContactMechanism : {}", id);
        PartyContactMechanism PartyContactMechanism = PartyContactMechanismRepository.findOne(id);
        return PartyContactMechanismMapper.toDto(PartyContactMechanism);
    }

    /**
     *  Delete the  PartyContactMechanism by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PartyContactMechanism : {}", id);
        PartyContactMechanismRepository.delete(id);
        PartyContactMechanismSearchRepository.delete(id);
    }

    /**
     * Search for the PartyContactMechanism corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartyContactMechanismDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PartyContactMechanisms for query {}", query);
        Page<PartyContactMechanism> result = PartyContactMechanismSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(PartyContactMechanismMapper::toDto);
    }

}
