package id.atiila.service;

import id.atiila.domain.PositionType;
import id.atiila.repository.PositionTypeRepository;
import id.atiila.repository.search.PositionTypeSearchRepository;
import id.atiila.service.dto.PositionTypeDTO;
import id.atiila.service.mapper.PositionTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PositionType.
 * BeSmart Team
 */

@Service
@Transactional
public class PositionTypeService {

    private final Logger log = LoggerFactory.getLogger(PositionTypeService.class);

    private final PositionTypeRepository positionTypeRepository;

    private final PositionTypeMapper positionTypeMapper;

    private final PositionTypeSearchRepository positionTypeSearchRepository;
    public PositionTypeService(PositionTypeRepository positionTypeRepository, PositionTypeMapper positionTypeMapper, PositionTypeSearchRepository positionTypeSearchRepository) {
        this.positionTypeRepository = positionTypeRepository;
        this.positionTypeMapper = positionTypeMapper;
        this.positionTypeSearchRepository = positionTypeSearchRepository;
    }

    /**
     * Save a positionType.
     *
     * @param positionTypeDTO the entity to save
     * @return the persisted entity
     */
    public PositionTypeDTO save(PositionTypeDTO positionTypeDTO) {
        log.debug("Request to save PositionType : {}", positionTypeDTO);
        PositionType positionType = positionTypeMapper.toEntity(positionTypeDTO);
        positionType = positionTypeRepository.save(positionType);
        PositionTypeDTO result = positionTypeMapper.toDto(positionType);
        positionTypeSearchRepository.save(positionType);
        return result;
    }

    /**
     *  Get all the positionTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PositionTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PositionTypes");
        return positionTypeRepository.findAll(pageable)
            .map(positionTypeMapper::toDto);
    }

    /**
     *  Get one positionType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PositionTypeDTO findOne(Integer id) {
        log.debug("Request to get PositionType : {}", id);
        PositionType positionType = positionTypeRepository.findOne(id);
        return positionTypeMapper.toDto(positionType);
    }

    /**
     *  Delete the  positionType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete PositionType : {}", id);
        positionTypeRepository.delete(id);
        positionTypeSearchRepository.delete(id);
    }

    /**
     * Search for the positionType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PositionTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PositionTypes for query {}", query);
        Page<PositionType> result = positionTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(positionTypeMapper::toDto);
    }

    public PositionTypeDTO processExecuteData(Integer id, String param, PositionTypeDTO dto) {
        PositionTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<PositionTypeDTO> processExecuteListData(Integer id, String param, Set<PositionTypeDTO> dto) {
        Set<PositionTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        positionTypeSearchRepository.deleteAll();
        List<PositionType> positionTypes =  positionTypeRepository.findAll();
        for (PositionType m: positionTypes) {
            positionTypeSearchRepository.save(m);
            log.debug("Data position type save !...");
        }
    }

}
