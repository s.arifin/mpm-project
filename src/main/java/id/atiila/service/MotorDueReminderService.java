package id.atiila.service;

import id.atiila.domain.MotorDueReminder;
import id.atiila.repository.MotorDueReminderRepository;
import id.atiila.repository.search.MotorDueReminderSearchRepository;
import id.atiila.service.dto.MotorDueReminderDTO;
import id.atiila.service.mapper.MotorDueReminderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing MotorDueReminder.
 * BeSmart Team
 */

@Service
@Transactional
public class MotorDueReminderService {

    private final Logger log = LoggerFactory.getLogger(MotorDueReminderService.class);

    private final MotorDueReminderRepository motorDueReminderRepository;

    private final MotorDueReminderMapper motorDueReminderMapper;

    private final MotorDueReminderSearchRepository motorDueReminderSearchRepository;
    public MotorDueReminderService(MotorDueReminderRepository motorDueReminderRepository, MotorDueReminderMapper motorDueReminderMapper, MotorDueReminderSearchRepository motorDueReminderSearchRepository) {
        this.motorDueReminderRepository = motorDueReminderRepository;
        this.motorDueReminderMapper = motorDueReminderMapper;
        this.motorDueReminderSearchRepository = motorDueReminderSearchRepository;
    }

    /**
     * Save a motorDueReminder.
     *
     * @param motorDueReminderDTO the entity to save
     * @return the persisted entity
     */
    public MotorDueReminderDTO save(MotorDueReminderDTO motorDueReminderDTO) {
        log.debug("Request to save MotorDueReminder : {}", motorDueReminderDTO);
        MotorDueReminder motorDueReminder = motorDueReminderMapper.toEntity(motorDueReminderDTO);
        motorDueReminder = motorDueReminderRepository.save(motorDueReminder);
        MotorDueReminderDTO result = motorDueReminderMapper.toDto(motorDueReminder);
        motorDueReminderSearchRepository.save(motorDueReminder);
        return result;
    }

    /**
     *  Get all the motorDueReminders.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MotorDueReminderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MotorDueReminders");
        return motorDueReminderRepository.findAll(pageable)
            .map(motorDueReminderMapper::toDto);
    }

    /**
     *  Get one motorDueReminder by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public MotorDueReminderDTO findOne(Integer id) {
        log.debug("Request to get MotorDueReminder : {}", id);
        MotorDueReminder motorDueReminder = motorDueReminderRepository.findOne(id);
        return motorDueReminderMapper.toDto(motorDueReminder);
    }

    /**
     *  Delete the  motorDueReminder by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete MotorDueReminder : {}", id);
        motorDueReminderRepository.delete(id);
        motorDueReminderSearchRepository.delete(id);
    }

    /**
     * Search for the motorDueReminder corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MotorDueReminderDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MotorDueReminders for query {}", query);
        Page<MotorDueReminder> result = motorDueReminderSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(motorDueReminderMapper::toDto);
    }

    public MotorDueReminderDTO processExecuteData(Integer id, String param, MotorDueReminderDTO dto) {
        MotorDueReminderDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<MotorDueReminderDTO> processExecuteListData(Integer id, String param, Set<MotorDueReminderDTO> dto) {
        Set<MotorDueReminderDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

}
