package id.atiila.service;

import id.atiila.domain.PartyRole;
import id.atiila.repository.PartyRoleRepository;
import id.atiila.repository.search.PartyRoleSearchRepository;
import id.atiila.service.dto.PartyRoleDTO;
import id.atiila.service.mapper.PartyRoleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

import java.util.UUID;


/**
 * Service Implementation for managing PartyRole.
 * BeSmart Team
 */

@Service
@Transactional
public class PartyRoleService {

    private final Logger log = LoggerFactory.getLogger(PartyRoleService.class);

    private final PartyRoleRepository partyRoleRepository;

    private final PartyRoleMapper partyRoleMapper;

    private final PartyRoleSearchRepository partyRoleSearchRepository;

    public PartyRoleService(PartyRoleRepository partyRoleRepository, PartyRoleMapper partyRoleMapper, PartyRoleSearchRepository partyRoleSearchRepository) {
        this.partyRoleRepository = partyRoleRepository;
        this.partyRoleMapper = partyRoleMapper;
        this.partyRoleSearchRepository = partyRoleSearchRepository;
    }

    /**
     * Save a partyRole.
     *
     * @param partyRoleDTO the entity to save
     * @return the persisted entity
     */
    public PartyRoleDTO save(PartyRoleDTO partyRoleDTO) {
        log.debug("Request to save PartyRole : {}", partyRoleDTO);
        PartyRole partyRole = partyRoleMapper.toEntity(partyRoleDTO);
        partyRole = partyRoleRepository.save(partyRole);
        PartyRoleDTO result = partyRoleMapper.toDto(partyRole);
        partyRoleSearchRepository.save(partyRole);
        return result;
    }

    /**
     *  Get all the partyRoles.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartyRoleDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PartyRoles");
        return partyRoleRepository.findAll(pageable)
            .map(partyRoleMapper::toDto);
    }

    /**
     *  Get one partyRole by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PartyRoleDTO findOne(UUID id) {
        log.debug("Request to get PartyRole : {}", id);
        PartyRole partyRole = partyRoleRepository.findOne(id);
        return partyRoleMapper.toDto(partyRole);
    }

    /**
     *  Delete the  partyRole by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete PartyRole : {}", id);
        partyRoleRepository.delete(id);
        partyRoleSearchRepository.delete(id);
    }

    /**
     * Search for the partyRole corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartyRoleDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PartyRoles for query {}", query);
        Page<PartyRole> result = partyRoleSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(partyRoleMapper::toDto);
    }
}
