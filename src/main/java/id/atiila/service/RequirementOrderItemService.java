package id.atiila.service;

import id.atiila.domain.RequirementOrderItem;
import id.atiila.repository.RequirementOrderItemRepository;
import id.atiila.repository.search.RequirementOrderItemSearchRepository;
import id.atiila.service.dto.RequirementOrderItemDTO;
import id.atiila.service.mapper.RequirementOrderItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing RequirementOrderItem.
 * atiila consulting
 */

@Service
@Transactional
public class RequirementOrderItemService {

    private final Logger log = LoggerFactory.getLogger(RequirementOrderItemService.class);

    private final RequirementOrderItemRepository requirementOrderItemRepository;

    private final RequirementOrderItemMapper requirementOrderItemMapper;

    private final RequirementOrderItemSearchRepository requirementOrderItemSearchRepository;

    public RequirementOrderItemService(RequirementOrderItemRepository requirementOrderItemRepository, RequirementOrderItemMapper requirementOrderItemMapper, RequirementOrderItemSearchRepository requirementOrderItemSearchRepository) {
        this.requirementOrderItemRepository = requirementOrderItemRepository;
        this.requirementOrderItemMapper = requirementOrderItemMapper;
        this.requirementOrderItemSearchRepository = requirementOrderItemSearchRepository;
    }

    /**
     * Save a requirementOrderItem.
     *
     * @param requirementOrderItemDTO the entity to save
     * @return the persisted entity
     */
    public RequirementOrderItemDTO save(RequirementOrderItemDTO requirementOrderItemDTO) {
        log.debug("Request to save RequirementOrderItem : {}", requirementOrderItemDTO);
        RequirementOrderItem requirementOrderItem = requirementOrderItemMapper.toEntity(requirementOrderItemDTO);
        requirementOrderItem = requirementOrderItemRepository.save(requirementOrderItem);
        RequirementOrderItemDTO result = requirementOrderItemMapper.toDto(requirementOrderItem);
        requirementOrderItemSearchRepository.save(requirementOrderItem);
        return result;
    }

    /**
     * Get all the requirementOrderItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequirementOrderItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RequirementOrderItems");
        return requirementOrderItemRepository.findAll(pageable)
            .map(requirementOrderItemMapper::toDto);
    }

    /**
     * Get one requirementOrderItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public RequirementOrderItemDTO findOne(UUID id) {
        log.debug("Request to get RequirementOrderItem : {}", id);
        RequirementOrderItem requirementOrderItem = requirementOrderItemRepository.findOne(id);
        return requirementOrderItemMapper.toDto(requirementOrderItem);
    }

    /**
     * Delete the requirementOrderItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete RequirementOrderItem : {}", id);
        requirementOrderItemRepository.delete(id);
        requirementOrderItemSearchRepository.delete(id);
    }

    /**
     * Search for the requirementOrderItem corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequirementOrderItemDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of RequirementOrderItems for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idOrderItem = request.getParameter("idOrderItem");
        String idRequirement = request.getParameter("idRequirement");

        if (idOrderItem != null) {
            q.withQuery(matchQuery("orderItem.idOrderItem", idOrderItem));
        }
        else if (idRequirement != null) {
            q.withQuery(matchQuery("requirement.idRequirement", idRequirement));
        }

        Page<RequirementOrderItem> result = requirementOrderItemSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(requirementOrderItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<RequirementOrderItemDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered RequirementOrderItemDTO");
        String idOrderItem = request.getParameter("idOrderItem");
        String idRequirement = request.getParameter("idRequirement");

        if (idOrderItem != null) {
            return requirementOrderItemRepository.queryByIdOrderItem(UUID.fromString(idOrderItem), pageable).map(requirementOrderItemMapper::toDto); 
        }
        else if (idRequirement != null) {
            return requirementOrderItemRepository.queryByIdRequirement(UUID.fromString(idRequirement), pageable).map(requirementOrderItemMapper::toDto); 
        }

        return requirementOrderItemRepository.queryNothing(pageable).map(requirementOrderItemMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, RequirementOrderItemDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<RequirementOrderItemDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
