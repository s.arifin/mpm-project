package id.atiila.service.specification;

import id.atiila.domain.VehicleDocumentRequirement;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class VehicleDocumentRequirementSpecification implements Specification<VehicleDocumentRequirement> {

    private VehicleDocumentRequirement filter;

    public VehicleDocumentRequirementSpecification(VehicleDocumentRequirement filter) {
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<VehicleDocumentRequirement> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        Predicate p = cb.disjunction();

        p.getExpressions()
            .add(
                cb.isTrue(root.get("statuses.idStatusType").in( 10, 11, 12)));

        if (filter.getNote() != null) {
            p.getExpressions()
                .add(cb.equal(root.get("name"), filter.getNote()));
        }

        return p;
    }
}
