package id.atiila.service;

import id.atiila.domain.Internal;
import id.atiila.domain.ProductShipmentIncoming;
import id.atiila.repository.ProductShipmentIncomingRepository;
import id.atiila.repository.search.ProductShipmentIncomingSearchRepository;
import id.atiila.service.dto.ProductShipmentIncomingDTO;
import id.atiila.service.mapper.ProductShipmentIncomingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing ProductShipmentIncoming.
 * BeSmart Team
 */

@Service
@Transactional
public class ProductShipmentIncomingService {

    private final Logger log = LoggerFactory.getLogger(ProductShipmentIncomingService.class);

    private final ProductShipmentIncomingRepository productShipmentIncomingRepository;

    private final ProductShipmentIncomingMapper productShipmentIncomingMapper;

    private final ProductShipmentIncomingSearchRepository productShipmentIncomingSearchRepository;

    @Autowired
    private PartyUtils partyUtils;

    public ProductShipmentIncomingService(ProductShipmentIncomingRepository productShipmentIncomingRepository, ProductShipmentIncomingMapper productShipmentIncomingMapper, ProductShipmentIncomingSearchRepository productShipmentIncomingSearchRepository) {
        this.productShipmentIncomingRepository = productShipmentIncomingRepository;
        this.productShipmentIncomingMapper = productShipmentIncomingMapper;
        this.productShipmentIncomingSearchRepository = productShipmentIncomingSearchRepository;
    }

    /**
     * Save a productShipmentIncoming.
     *
     * @param productShipmentIncomingDTO the entity to save
     * @return the persisted entity
     */
    public ProductShipmentIncomingDTO save(ProductShipmentIncomingDTO productShipmentIncomingDTO) {
        log.debug("Request to save ProductShipmentIncoming : {}", productShipmentIncomingDTO);
        ProductShipmentIncoming productShipmentIncoming = productShipmentIncomingMapper.toEntity(productShipmentIncomingDTO);
        productShipmentIncoming = productShipmentIncomingRepository.save(productShipmentIncoming);
        ProductShipmentIncomingDTO result = productShipmentIncomingMapper.toDto(productShipmentIncoming);
        productShipmentIncomingSearchRepository.save(productShipmentIncoming);
        return result;
    }

    /**
     * Get all the productShipmentIncomings.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductShipmentIncomingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductShipmentIncomings");
        return productShipmentIncomingRepository.findActiveProductShipmentIncoming(partyUtils.getCurrentInternal().getOrganization(), pageable)
            .map(productShipmentIncomingMapper::toDto);
    }

    /**
     * Get one productShipmentIncoming by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ProductShipmentIncomingDTO findOne(UUID id) {
        log.debug("Request to get ProductShipmentIncoming : {}", id);
        ProductShipmentIncoming productShipmentIncoming = productShipmentIncomingRepository.findOne(id);
        return productShipmentIncomingMapper.toDto(productShipmentIncoming);
    }

    /**
     * Delete the productShipmentIncoming by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ProductShipmentIncoming : {}", id);
        productShipmentIncomingRepository.delete(id);
        productShipmentIncomingSearchRepository.delete(id);
    }

    /**
     * Search for the productShipmentIncoming corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductShipmentIncomingDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of ProductShipmentIncomings for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idShipmentType = request.getParameter("idShipmentType");
        String idShipFrom = request.getParameter("idShipFrom");
        String idShipTo = request.getParameter("idShipTo");
        String idAddressFrom = request.getParameter("idAddressFrom");
        String idAddressTo = request.getParameter("idAddressTo");

        if (filterName != null) {
        }
        Page<ProductShipmentIncoming> result = productShipmentIncomingSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(productShipmentIncomingMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ProductShipmentIncomingDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered ProductShipmentIncomingDTO");
        String idShipmentType = request.getParameter("idShipmentType");
        String idShipFrom = request.getParameter("idShipFrom");
        String idShipTo = request.getParameter("idShipTo");
        String idAddressFrom = request.getParameter("idAddressFrom");
        String idAddressTo = request.getParameter("idAddressTo");

        return productShipmentIncomingRepository.findByParams(idShipmentType, idShipFrom, idShipTo, idAddressFrom, idAddressTo, pageable)
            .map(productShipmentIncomingMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public ProductShipmentIncomingDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        ProductShipmentIncomingDTO r = null;
        return r;
    }

    @Transactional
    public Set<ProductShipmentIncomingDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<ProductShipmentIncomingDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public ProductShipmentIncomingDTO changeProductShipmentIncomingStatus(ProductShipmentIncomingDTO dto, Integer id) {
        if (dto != null) {
			ProductShipmentIncoming e = productShipmentIncomingRepository.findOne(dto.getIdShipment());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        productShipmentIncomingSearchRepository.delete(dto.getIdShipment());
                        break;
                    default:
                        productShipmentIncomingSearchRepository.save(e);
                }
				productShipmentIncomingRepository.save(e);
			}
		}
        return dto;
    }
}
