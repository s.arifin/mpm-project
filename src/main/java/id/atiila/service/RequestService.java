package id.atiila.service;

import id.atiila.domain.Request;
import id.atiila.repository.RequestRepository;
import id.atiila.repository.search.RequestSearchRepository;
import id.atiila.service.dto.RequestDTO;
import id.atiila.service.mapper.RequestMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.UUID;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing Request.
 * BeSmart Team
 */

@Service
@Transactional
public class RequestService {

    private final Logger log = LoggerFactory.getLogger(RequestService.class);

    private final RequestRepository requestRepository;

    private final RequestMapper requestMapper;

    private final RequestSearchRepository requestSearchRepository;

    public RequestService(RequestRepository requestRepository, RequestMapper requestMapper, RequestSearchRepository requestSearchRepository) {
        this.requestRepository = requestRepository;
        this.requestMapper = requestMapper;
        this.requestSearchRepository = requestSearchRepository;
    }

    /**
     * Save a request.
     *
     * @param requestDTO the entity to save
     * @return the persisted entity
     */
    public RequestDTO save(RequestDTO requestDTO) {
        log.debug("Request to save Request : {}", requestDTO);
        Request request = requestMapper.toEntity(requestDTO);
        request = requestRepository.save(request);
        RequestDTO result = requestMapper.toDto(request);
        requestSearchRepository.save(request);
        return result;
    }

    /**
     * Get all the requests.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequestDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Requests");
        return requestRepository.findActiveRequest(pageable)
            .map(requestMapper::toDto);
    }

    /**
     * Get one request by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public RequestDTO findOne(UUID id) {
        log.debug("Request to get Request : {}", id);
        Request request = requestRepository.findOne(id);
        return requestMapper.toDto(request);
    }

    /**
     * Delete the request by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete Request : {}", id);
        requestRepository.delete(id);
        requestSearchRepository.delete(id);
    }

    /**
     * Search for the request corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RequestDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of Requests for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idRequestType = request.getParameter("idRequestType");

        if (filterName != null) {
        }
        Page<Request> result = requestSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(requestMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<RequestDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered RequestDTO");
        String idRequestType = request.getParameter("idRequestType");

        return requestRepository.findByParams(idRequestType, pageable)
            .map(requestMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        return item;
    }

    @Transactional
    public RequestDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        RequestDTO r = null;
        return r;
    }

    @Transactional
    public Set<RequestDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<RequestDTO> r = new HashSet<>();
        return r;
    }

    @Transactional
    public RequestDTO changeRequestStatus(RequestDTO dto, Integer id) {
        if (dto != null) {
			Request e = requestRepository.findOne(dto.getIdRequest());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        requestSearchRepository.delete(dto.getIdRequest());
                        break;
                    default:
                        requestSearchRepository.save(e);
                }
				requestRepository.save(e);
			}
		}
        return dto;
    }
}
