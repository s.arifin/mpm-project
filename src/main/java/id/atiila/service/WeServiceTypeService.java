package id.atiila.service;

import id.atiila.domain.WeServiceType;
import id.atiila.repository.WeServiceTypeRepository;
import id.atiila.repository.search.WeServiceTypeSearchRepository;
import id.atiila.service.dto.WeServiceTypeDTO;
import id.atiila.service.mapper.WeServiceTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WeServiceType.
 * BeSmart Team
 */

@Service
@Transactional
public class WeServiceTypeService {

    private final Logger log = LoggerFactory.getLogger(WeServiceTypeService.class);

    private final WeServiceTypeRepository weServiceTypeRepository;

    private final WeServiceTypeMapper weServiceTypeMapper;

    private final WeServiceTypeSearchRepository weServiceTypeSearchRepository;
    public WeServiceTypeService(WeServiceTypeRepository weServiceTypeRepository, WeServiceTypeMapper weServiceTypeMapper, WeServiceTypeSearchRepository weServiceTypeSearchRepository) {
        this.weServiceTypeRepository = weServiceTypeRepository;
        this.weServiceTypeMapper = weServiceTypeMapper;
        this.weServiceTypeSearchRepository = weServiceTypeSearchRepository;
    }

    /**
     * Save a weServiceType.
     *
     * @param weServiceTypeDTO the entity to save
     * @return the persisted entity
     */
    public WeServiceTypeDTO save(WeServiceTypeDTO weServiceTypeDTO) {
        log.debug("Request to save WeServiceType : {}", weServiceTypeDTO);
        WeServiceType weServiceType = weServiceTypeMapper.toEntity(weServiceTypeDTO);
        weServiceType = weServiceTypeRepository.save(weServiceType);
        WeServiceTypeDTO result = weServiceTypeMapper.toDto(weServiceType);
        weServiceTypeSearchRepository.save(weServiceType);
        return result;
    }

    /**
     *  Get all the weServiceTypes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WeServiceTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WeServiceTypes");
        return weServiceTypeRepository.findAll(pageable)
            .map(weServiceTypeMapper::toDto);
    }

    /**
     *  Get one weServiceType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public WeServiceTypeDTO findOne(Integer id) {
        log.debug("Request to get WeServiceType : {}", id);
        WeServiceType weServiceType = weServiceTypeRepository.findOne(id);
        return weServiceTypeMapper.toDto(weServiceType);
    }

    /**
     *  Delete the  weServiceType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete WeServiceType : {}", id);
        weServiceTypeRepository.delete(id);
        weServiceTypeSearchRepository.delete(id);
    }

    /**
     * Search for the weServiceType corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WeServiceTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WeServiceTypes for query {}", query);
        Page<WeServiceType> result = weServiceTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(weServiceTypeMapper::toDto);
    }

    public WeServiceTypeDTO processExecuteData(Integer id, String param, WeServiceTypeDTO dto) {
        WeServiceTypeDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<WeServiceTypeDTO> processExecuteListData(Integer id, String param, Set<WeServiceTypeDTO> dto) {
        Set<WeServiceTypeDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        weServiceTypeSearchRepository.deleteAll();
        List<WeServiceType> weServiceTypes =  weServiceTypeRepository.findAll();
        for (WeServiceType m: weServiceTypes) {
            weServiceTypeSearchRepository.save(m);
            log.debug("Data we service type save !...");
        }
    }

}
