package id.atiila.service;

import id.atiila.domain.SuspectOrganization;
import id.atiila.repository.SuspectOrganizationRepository;
import id.atiila.repository.search.SuspectOrganizationSearchRepository;
import id.atiila.service.dto.SuspectOrganizationDTO;
import id.atiila.service.mapper.SuspectOrganizationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing SuspectOrganization.
 * BeSmart Team
 */

@Service
@Transactional
public class SuspectOrganizationService {

    private final Logger log = LoggerFactory.getLogger(SuspectOrganizationService.class);

    private final SuspectOrganizationRepository suspectOrganizationRepository;

    private final SuspectOrganizationMapper suspectOrganizationMapper;

    private final SuspectOrganizationSearchRepository suspectOrganizationSearchRepository;

    public SuspectOrganizationService(SuspectOrganizationRepository suspectOrganizationRepository, SuspectOrganizationMapper suspectOrganizationMapper, SuspectOrganizationSearchRepository suspectOrganizationSearchRepository) {
        this.suspectOrganizationRepository = suspectOrganizationRepository;
        this.suspectOrganizationMapper = suspectOrganizationMapper;
        this.suspectOrganizationSearchRepository = suspectOrganizationSearchRepository;
    }

    /**
     * Save a suspectOrganization.
     *
     * @param suspectOrganizationDTO the entity to save
     * @return the persisted entity
     */
    public SuspectOrganizationDTO save(SuspectOrganizationDTO suspectOrganizationDTO) {
        log.debug("Request to save SuspectOrganization : {}", suspectOrganizationDTO);
        SuspectOrganization suspectOrganization = suspectOrganizationMapper.toEntity(suspectOrganizationDTO);
        suspectOrganization = suspectOrganizationRepository.save(suspectOrganization);
        SuspectOrganizationDTO result = suspectOrganizationMapper.toDto(suspectOrganization);
        suspectOrganizationSearchRepository.save(suspectOrganization);
        return result;
    }

    /**
     *  Get all the suspectOrganizations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SuspectOrganizationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SuspectOrganizations");
        return suspectOrganizationRepository.findActiveSuspectOrganization(pageable)
            .map(suspectOrganizationMapper::toDto);
    }

    /**
     *  Get one suspectOrganization by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SuspectOrganizationDTO findOne(UUID id) {
        log.debug("Request to get SuspectOrganization : {}", id);
        SuspectOrganization suspectOrganization = suspectOrganizationRepository.findOne(id);
        return suspectOrganizationMapper.toDto(suspectOrganization);
    }

    /**
     *  Delete the  suspectOrganization by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete SuspectOrganization : {}", id);
        suspectOrganizationRepository.delete(id);
        suspectOrganizationSearchRepository.delete(id);
    }

    /**
     * Search for the suspectOrganization corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SuspectOrganizationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SuspectOrganizations for query {}", query);
        Page<SuspectOrganization> result = suspectOrganizationSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(suspectOrganizationMapper::toDto);
    }

    public SuspectOrganizationDTO processExecuteData(Integer id, String param, SuspectOrganizationDTO dto) {
        SuspectOrganizationDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<SuspectOrganizationDTO> processExecuteListData(Integer id, String param, Set<SuspectOrganizationDTO> dto) {
        Set<SuspectOrganizationDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }    

    public SuspectOrganizationDTO changeSuspectOrganizationStatus(SuspectOrganizationDTO dto, Integer id) {
        if (dto != null) {
			SuspectOrganization e = suspectOrganizationRepository.findOne(dto.getIdSuspect());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        suspectOrganizationSearchRepository.delete(dto.getIdSuspect());
                        break;
                    default:
                        suspectOrganizationSearchRepository.save(e);
                }
				suspectOrganizationRepository.save(e);
			}
		}
        return dto;
    }
}
