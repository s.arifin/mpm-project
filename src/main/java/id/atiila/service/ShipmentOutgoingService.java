package id.atiila.service;

import com.google.common.collect.Lists;
import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.ShipmentOutgoingSearchRepository;
import id.atiila.service.dto.*;
import id.atiila.service.mapper.BillingMapper;
import id.atiila.service.mapper.CustomShipmentMapper;
import id.atiila.service.mapper.InventoryItemMapper;
import id.atiila.service.mapper.ShipmentOutgoingMapper;
import id.atiila.service.pto.ShipmentOutgoingPTO;
import org.apache.commons.collections.IteratorUtils;
import org.hibernate.criterion.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import java.time.ZonedDateTime;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ShipmentOutgoing.
 * BeSmart Team
 */

@Service
@Transactional
public class ShipmentOutgoingService {

    private final Logger log = LoggerFactory.getLogger(ShipmentOutgoingService.class);

    private final ShipmentOutgoingRepository shipmentOutgoingRepository;

    private final ShipmentOutgoingMapper shipmentOutgoingMapper;

    private final ShipmentOutgoingSearchRepository shipmentOutgoingSearchRepository;

    @Autowired
    private ShipmentRepository shipmentRepository;

    @Autowired
    private ShipmentItemRepository shipmentItemRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private ItemIssuanceRepository itemIssuanceRepository;

    @Autowired private SalesUnitRequirementService salesUnitRequirementService;

    @Autowired
    private PickingSlipRepository pickingRepo;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private MasterNumberingService masterNumberingService;

    private final InventoryItemMapper inventoryItemMapper;

    @Autowired private InventoryItemRepository inventoryItemRepository;

    @Autowired private BillingMapper billingMapper;

    @Autowired private OrderBillingItemRepository orderBillingItemRepository;

    @Autowired private BillingRepository billingRepository;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired private BillingItemRepository billingItemRepository;

    @Autowired
    private InventoryItemService inventoryItemService;

    @Autowired
    private CustomShipmentMapper customShipmentMapper;

    public ShipmentOutgoingService(ShipmentOutgoingRepository shipmentOutgoingRepository, ShipmentOutgoingMapper shipmentOutgoingMapper,
                                   ShipmentOutgoingSearchRepository shipmentOutgoingSearchRepository, InventoryItemMapper inventoryItemMapper) {
        this.shipmentOutgoingRepository = shipmentOutgoingRepository;
        this.shipmentOutgoingMapper = shipmentOutgoingMapper;
        this.shipmentOutgoingSearchRepository = shipmentOutgoingSearchRepository;
        this.inventoryItemMapper = inventoryItemMapper;
    }

    /**
     * Save a shipmentOutgoing.
     *
     * @param shipmentOutgoingDTO the entity to save
     * @return the persisted entity
     */
    public ShipmentOutgoingDTO save(ShipmentOutgoingDTO shipmentOutgoingDTO) {
        log.debug("Request to save ShipmentOutgoing : {}", shipmentOutgoingDTO);
        ShipmentOutgoing shipmentOutgoing = shipmentOutgoingMapper.toEntity(shipmentOutgoingDTO);
        shipmentOutgoing = shipmentOutgoingRepository.save(shipmentOutgoing);
        ShipmentOutgoingDTO result = shipmentOutgoingMapper.toDto(shipmentOutgoing);
        shipmentOutgoingSearchRepository.save(shipmentOutgoing);
        return result;
    }

    /**
     *  Get all the shipmentOutgoings.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentOutgoingDTO> findAll(Pageable pageable, Set<Integer> idstatustype ) {
        log.debug("Request to get all ShipmentOutgoings");
        Page<ShipmentOutgoingDTO> shipmentOutgoingDTOS;
        if (idstatustype != null ){
            shipmentOutgoingDTOS =  shipmentOutgoingRepository.findShipmentOutgoingByStatusType(idstatustype,pageable).map(shipmentOutgoingMapper::toDto);
        }
        else {
            shipmentOutgoingDTOS = shipmentOutgoingRepository.findActiveShipmentOutgoing(pageable).map(shipmentOutgoingMapper::toDto);

        }
        return shipmentOutgoingDTOS;

//        return shipmentOutgoingRepository.findActiveShipmentOutgoing(pageable)
//            .map(shipmentOutgoingMapper::toDto);
    }

    /**
     *  Get one shipmentOutgoing by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ShipmentOutgoingDTO findOne(UUID id) {
        log.debug("Request to get ShipmentOutgoing : {}", id);
        ShipmentOutgoing shipmentOutgoing = shipmentOutgoingRepository.findOne(id);
        return shipmentOutgoingMapper.toDto(shipmentOutgoing);
    }

    /**
     *  Delete the  shipmentOutgoing by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ShipmentOutgoing : {}", id);
        shipmentOutgoingRepository.delete(id);
        shipmentOutgoingSearchRepository.delete(id);
    }

    /**
     * Search for the shipmentOutgoing corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentOutgoingDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ShipmentOutgoings for query {}", query);
        Page<ShipmentOutgoing> result = shipmentOutgoingSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(shipmentOutgoingMapper::toDto);
    }

    @Transactional
    public ShipmentOutgoingDTO processExecuteData(Integer id, String param, ShipmentOutgoingDTO dto) {
        ShipmentOutgoingDTO r = dto;
        if (r != null) {
           switch (id) {
                case 101: {
                    changeShipmentOutgoingStatus(r, BaseConstants.STATUS_KIRIM_GUDANG);
                    break;
                }
                case 102:
                    changeShipmentOutgoingStatus(r,BaseConstants.STATUS_CANCEL_SHIPMENT);
                    break;
                case 103:
                    changeShipmentOutgoingStatus(r,BaseConstants.STATUS_ASSIGN_DRIVER);
                    break;
                case 104 :
                    changeShipmentOutgoingStatus(r,BaseConstants.STATUS_COMPLETE_SHIPMENT);
					break;
                case 105 :
                    String internalID = param;
                    ShipmentOutgoing so = shipmentOutgoingRepository.findOne(dto.getIdShipment());
                    if (dto.getShipmentNumber() == null){
                        String shipmentnumber = masterNumberingService.findTransNumberVDOBy(internalID, "VDO");
//                    r.shipmentNumber = shipmentnumber;
                        log.debug("sebelumcetakbast "+ r);
                        changeShipmentNumber(r, shipmentnumber);
                        // ganti status shipment
                        changeShipmentOutgoingStatus(r,BaseConstants.STATUS_PRINT_BAST);
                        changePickingStatus(so, BaseConstants.STATUS_PRINT_BAST);
                    }
                    int jumlah = 0;
                    if (so.getJumlahprint() == null) {
                        so.setJumlahprint(1);
                    } else {
                        jumlah = so.getJumlahprint() + 1;
                    }
                    log.debug("testcetakbasthasil " + so.getJumlahprint());
                    so.setJumlahprint(jumlah);
                    shipmentOutgoingRepository.save(so);

					break;
               case 106 :
                   changeShipmentOutgoingStatus(r,BaseConstants.STATUS_COMPLETE_GUDANG);
                   ShipmentOutgoing shipout = shipmentOutgoingRepository.findOne(r.getIdShipment());
                   changePickingStatus(shipout, BaseConstants.STATUS_ACTIVE);
                   break;
               case 107 :
                   changeShipmentAddressandSchedule(r);
                   break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<ShipmentOutgoingDTO> processExecuteListData(Integer id, String param, Set<ShipmentOutgoingDTO> dto) {
        Set<ShipmentOutgoingDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 101:
                    for(ShipmentOutgoingDTO shipmentOutgoingDTO : r){
                        ShipmentOutgoing so = shipmentOutgoingRepository.findOne(shipmentOutgoingDTO.getIdShipment());
                        ShipmentOutgoingDTO paramDTO = shipmentOutgoingMapper.toDto(so);
                        changeShipmentOutgoingStatus(paramDTO,BaseConstants.STATUS_KIRIM_GUDANG);
                        changePickingStatus(so, BaseConstants.STATUS_OPEN);
                    }
                    break;
                case 103:
                    for(ShipmentOutgoingDTO shipmentOutgoingDTO : r){
                        ShipmentOutgoing so = shipmentOutgoingRepository.findOne(shipmentOutgoingDTO.getIdShipment());
                        ShipmentOutgoingDTO paramDTO = shipmentOutgoingMapper.toDto(so);
                        changeShipmentOutgoingStatus(paramDTO,BaseConstants.STATUS_ASSIGN_DRIVER);
                    }
                    break;
                case 109:
                    for(ShipmentOutgoingDTO shipmentOutgoingDTO : r){
                        ShipmentOutgoing so = shipmentOutgoingRepository.findOne(shipmentOutgoingDTO.getIdShipment());
                        so.setIddriver(shipmentOutgoingDTO.getIdDriver());
                        so.setOtherName(shipmentOutgoingDTO.getOtherName());
//                        ShipmentOutgoingDTO paramDTO = shipmentOutgoingMapper.toDto(so);
                        shipmentOutgoingRepository.save(so);
//                        save(shipmentOutgoingDTO);

                    }
                    break;
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public ShipmentOutgoingDTO changeShipmentOutgoingStatus(ShipmentOutgoingDTO dto, Integer id) {
        if (dto != null) {
			ShipmentOutgoing e = shipmentOutgoingRepository.findOne(dto.getIdShipment());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        shipmentOutgoingSearchRepository.delete(dto.getIdShipment());
                        break;
                    default:
                        shipmentOutgoingSearchRepository.save(e);
                }
				shipmentOutgoingRepository.save(e);
			}
		}
        return dto;
    }

    public ShipmentDTO setShipmentSchedulefromSur(String dataParam, ShipmentOutgoingDTO dto) {
        if (dto != null) {
            Shipment s = shipmentRepository.findOne(dto.getIdShipment());
            s.setDateSchedulle(ZonedDateTime.parse(dataParam));
            log.debug("ssssssssssssssssssssssssssss"+ s);
            shipmentRepository.save(s);

        }
        return dto;

    }

    public ShipmentDTO changeShipmentNumber (ShipmentOutgoingDTO dto, String shipmentNumber) {
        if (dto != null) {
            Shipment s = shipmentRepository.findOne(dto.getIdShipment());
            s.setShipmentNumber(shipmentNumber);
            s.setDateCreateNumber(ZonedDateTime.now());
            log.debug("ssssssssssssssssssssssssssss"+ s);
            shipmentRepository.save(s);

        }
        return dto;
    }

    protected double getQtyOH(UUID owner, String idProduct, Integer idFeature) {
        Double value = shipmentOutgoingRepository.getQtyOH(owner, idProduct, idFeature);
        log.debug("Hasil shipmentOutgoingRepository Qty OH [" + owner + "][" + idProduct + "][" + idFeature + "] => " + value);
        return value == null ? 0d : value;
    }

    protected List<InventoryItem> getInventoryItem(UUID owner, String idProduct, Integer idFeature, String noka, String nosin, Integer year, UUID idFacility) {
        List<InventoryItem> hasil;
        if (noka == null || noka.trim().isEmpty()){
            log.debug("shipment out going -> get inv item native");
            Internal intr = partyUtils.getCurrentInternal();
//            List<InventoryItem> invItmDto= shipmentOutgoingRepository.getInventoryItemNative(intr.getIdInternal(), idProduct, idFeature);
//            return invItmDto;

//            PageRequest pageRequest = new PageRequest(0,100);
//            Page<InventoryItem> inventoryItemPage = inventoryItemRepository
//                .getByIdOwnerAndIdFeatureNative( idProduct, intr.getIdInternal(), idFeature, pageRequest);
//            log.debug("get inv item success");
//            return IteratorUtils.toList(inventoryItemPage.iterator());
            // todo di remark karene ga pakai gudang dulu
//                return inventoryItemRepository.getByIdOwnerAndIdFeatureNative( idProduct, intr.getIdInternal(), idFeature, year, idFacility);
            return inventoryItemRepository.getByIdOwnerAndIdFeatureNative( idProduct, intr.getIdInternal(), idFeature, year);
//            return shipmentOutgoingRepository.getInventoryItem(owner, idProduct, idFeature).map(inventoryItemMapper::toDto);
        } else {
            log.info("Shipment Outgoing Repo => getInventoryItem: owner[" + owner + "]id product[" + idProduct + "] id feature[" + idFeature + "] noka [" + noka + "] nosin [" + nosin + "] ");
            // todo di remark karene ga pakai gudang dulu
//            hasil = shipmentOutgoingRepository.getInventoryItemNoka(owner, idProduct, idFeature, noka, nosin, idFacility);
            hasil = shipmentOutgoingRepository.getInventoryItemNoka(owner, idProduct, idFeature, noka, nosin);
            return hasil;
        }

    }

    protected PickingSlip buildPicking(ShipTo s, OrderItem oi, InventoryItem o, Double qty, Billing b, SalesUnitRequirementDTO sur) {
        PickingSlip p = new PickingSlip();
        p.setInventoryItem(o);
        p.setShipTo(s);
        p.setQty(qty);
        p.setOrderItem(oi);
        Orders orders = oi.getOrders();
        p.setIdInventoryMovementType(orders.getOrderType().getIdOrderType());
        if (orders.getOrderType().getIdOrderType().equals(BaseConstants.ORDER_TYPE_VSO)) {
            p.setRefferenceNumber(b.getBillingNumber());
        } else {
            p.setRefferenceNumber(sur.getRequirementNumber());
        }
        o.setQtyBooking(o.getQtyBooking()+ qty);
        return p;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public ShipmentOutgoingDTO buildFrom(VehicleSalesOrderDTO vso) {

        //Check apakah order bisa di penuhi
        VehicleSalesOrder v = shipmentOutgoingRepository.findVSO(vso.getIdOrder());
        Internal intr = shipmentOutgoingRepository.findInternal(vso.getInternalId());
        UUID idOwner = intr.getOrganization().getIdParty();
        ShipTo shipFrom = shipmentOutgoingRepository.findShipToFromIdParty(idOwner);
        List<ShipmentOutgoing> lso = new ArrayList<>();
        Billing billing = null;
        Page<OrderItem> ordit = orderItemRepository.findOrderItemByOrders(v.getIdOrder(), new PageRequest(0,1));
        if (ordit.hasContent()) {
            List<OrderBillingItem> obi = orderBillingItemRepository.queryByOrderItem(ordit.getContent().get(0));
            if (obi != null) {
                BillingItem bi = billingItemRepository.findOne(obi.iterator().next().getBillingItem().getIdBillingItem());
                if (bi != null) {
                    billing = billingRepository.getOne(bi.getBilling().getIdBilling());
                }
            }
        }


        // ambilNoka/nosin
        SalesUnitRequirementDTO sur = salesUnitRequirementService.findOne(vso.getIdSalesUnitRequirement());
        log.info("so:sur id="+sur.getId());
        ShipmentOutgoing ou = null;
        log.info("so:orderitem=" + v.getDetails().size());
        for (OrderItem oi: v.getDetails()) {
            log.info("so:order item => " + oi.getIdProduct());
            double qtyShipped = 0d;
            ShipTo shipTo = shipmentOutgoingRepository.findShipTo(oi.getIdShipTo());

            log.info("so:get shipment items size [" + oi.getShipmentItems().size() +"] " );
            for (OrderShipmentItem si: oi.getShipmentItems()) {
                qtyShipped = qtyShipped + si.getQty();
                log.info("so:Qty Shipment item generated");
            }

            double QtyOH = getQtyOH(idOwner, oi.getIdProduct(), oi.getIdFeature());
            double QtyRequest = Math.min(oi.getQty() - qtyShipped, QtyOH);
            log.debug("OI qty = " + oi.getQty());
            /*System.out.println("**********************************************************************************");
            System.out.println("Qty di Request === " + QtyRequest + " >> " + oi.getQty() + " >> " + QtyOH);
            System.out.println("**********************************************************************************");*/
            log.debug("so:Qty OH = " + QtyOH + " Qty request = " + QtyRequest);
            //Jika ada Order Item yang belum di penuhi, proses
            if (QtyRequest > 0.0d) {
                log.info("so: qty req > 0");
                Double QtyFill = 0d;
                List<PickingSlip> lpi = new ArrayList<>();

                if (ou == null || !ou.getShipTo().equals(shipTo)) {

                    ou = new ShipmentOutgoing();
                    ou.setShipTo(shipTo);
                    ou.setShipFrom(shipFrom);
                    ou.setStatus(BaseConstants.STATUS_DRAFT);
                    lso.add(ou);
                    log.info("so: add to list");
                }

                // Build Picking
                List<InventoryItem> linv = getInventoryItem(idOwner, oi.getIdProduct(), oi.getIdFeature(), sur.getIdframe(), sur.getIdmachine(), sur.getProductYear(), sur.getIdGudang());
                log.debug("inventory Item" + linv);
                if (linv.size() == 0) throw new DmsException("Stock barang tidak tersedia!");
                for (InventoryItem inv: linv) {
                    log.info("so: get inventor");
                    Double QtyOnInv = inv.getQty() - inv.getQtyBooking();
                    Double QtyToPick = Math.min(QtyOnInv, QtyRequest - QtyFill);

                    PickingSlip pis = buildPicking(shipTo, oi, inv, QtyToPick, billing, sur);
                    lpi.add(pis);

                    QtyFill = QtyFill + QtyToPick;
                    if (QtyFill >= QtyRequest) break;
                }

                //Simpan Picking ke Database
                for (PickingSlip pis: lpi) {
                    //Buat Item Issuance
                    ItemIssuance issuance = buildItemIssuance(ou, oi, pis);

                    //Simpan data picking
                    pis.addIssuance(issuance);
                    pis.setStatus(BaseConstants.STATUS_DRAFT);
                    pickingRepo.save(pis);
                    log.info("so: save picking" + pis.getIdSlip().toString());
                }

                //Simpan Shipment Outgoind (DO)
                for (ShipmentOutgoing s: lso) {
                    shipmentOutgoingRepository.save(s);
                    log.info("so: save so" + s.getIdShipment().toString());
                }
            }

        }

        return null;
    }

    private ItemIssuance buildItemIssuance(ShipmentOutgoing ou, OrderItem oi, PickingSlip pis) {
        //Buat Shipment Item
        ShipmentItem shi = new ShipmentItem();
        shi.setQty(pis.getQty());
        // shi.addOrderItem(oi);
        shi.setIdProduct(oi.getIdProduct());
        shi.setIdFeature(oi.getIdFeature());
        shi.setContentDescription(oi.getItemDescription());
        shi.setShipment(ou);
        shi = shipmentItemRepository.save(shi);

        ou.addDetail(shi);
        ou = shipmentOutgoingRepository.save(ou);

        oi.addShipmentItem(shi);
        oi = orderItemRepository.save(oi);

        //Buat Item Issuance
        ItemIssuance issuance = new ItemIssuance();
        issuance.setInventoryItem(pis.getInventoryItem());
        issuance.setPicking(pis);
        issuance.setQty(pis.getQty());
        issuance.setShipmentItem(shi);
        issuance = itemIssuanceRepository.save(issuance);
        return issuance;
    }

    public ShipmentOutgoingDTO changeShipmentAddressandSchedule(ShipmentOutgoingDTO r){
        if (r != null){
            Shipment s = shipmentRepository.findOne(r.getIdShipment());
            s.setDateSchedulle(r.getDateSchedulle());
            shipmentRepository.save(s);

            ShipmentOutgoing so = shipmentOutgoingRepository.findOne(r.getIdShipment());
            so.setDeliveryAddress(r.getDeliveryAddress());
            so.setDeliveryOpt(r.getDeliveryOpt());
            shipmentOutgoingRepository.save(so);
        }
        return r;
    }

    @Transactional(readOnly = true)
    public Page<CustomShipmentDTO> findCustomFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered OrderItemDTO");

        String queryFor = request.getParameter("queryFor");
        String idInternal = request.getParameter("idInternal");
        String idStatusType = request.getParameter("idstatustype");

        if ("picking".equalsIgnoreCase(queryFor) && idInternal != null && idStatusType != null) {
            return shipmentOutgoingRepository.queryPickingByStatusAndInternal(
                    idInternal,
                    Integer.valueOf(idStatusType), pageable)
                .map(customShipmentMapper::toDto)
                .map(customShipmentMapper::assignUnitPrep);
        } else if ("pickingdo".equalsIgnoreCase(queryFor) && idInternal != null && idStatusType != null) {
            return shipmentOutgoingRepository.queryPickingByStatusAndInternal(
                idInternal,
                Integer.valueOf(idStatusType), pageable)
                .map(customShipmentMapper::toDto)
                .map(customShipmentMapper::assignPis);
        } else if ("jadwal".equalsIgnoreCase(queryFor) && idInternal != null && idStatusType != null) {
            return shipmentOutgoingRepository.queryFindShipmentOutgoing(idInternal, Integer.valueOf(idStatusType), pageable)
                .map(customShipmentMapper::toDto)
                .map(customShipmentMapper::assignJadwal);
        } else if ("driver".equalsIgnoreCase(queryFor) && idInternal != null && idStatusType != null) {
            String username = request.getParameter("username");
            log.debug("Username all : "+ username);
            return shipmentOutgoingRepository.queryFindShipmentOutgoingDriver(idInternal, Integer.valueOf(idStatusType),username, pageable)
                .map(customShipmentMapper::toDto)
                .map(customShipmentMapper::assignDriver);
        } else if ("driverToday".equalsIgnoreCase(queryFor) && idInternal != null && idStatusType != null) {
            ZonedDateTime dateAwal = ZonedDateTime.parse(request.getParameter("dateAwal"));
            ZonedDateTime dateAkhir = ZonedDateTime.parse(request.getParameter("dateAkhir"));
            String username = request.getParameter("username");
            log.debug("Username today : "+ username);
            return shipmentOutgoingRepository.queryShipmentOutgoingByDateSchedule(idInternal,Integer.valueOf(idStatusType),dateAwal,dateAkhir,username,pageable)
                .map(customShipmentMapper::toDto)
                .map(customShipmentMapper::assignDriver);
        }
        return shipmentOutgoingRepository.queryNothing(pageable).map(customShipmentMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<CustomShipmentDTO> searchQuery(Pageable pageable, ShipmentOutgoingPTO param) {
        log.debug("parampampam :"+ param.getDataParam());
        log.debug("idStatus Type : " + param.getIdStatusType());
        log.debug("Internal Id : " + param.getInternalId());
        log.debug("Query For : " + param.getQueryFor());
        log.debug("True or false : " + param.getFilterType());
        String paramLike = "% %";
        if (param.getDataParam() != null) {
            paramLike = '%' + param.getDataParam() +'%';
        }
        String paramReffnumber = '%' + param.getReffNumber() +'%';
        if ("picking".equalsIgnoreCase(param.getQueryFor())) {
            log.debug("Masuk picking");
            if(param.getFilterType() == true)
            {
                if(param.getType() == 1)
                {
                    Page<CustomShipmentDTO> result = shipmentOutgoingRepository.queryPickingByStatusAndInternalSearch(paramLike, paramReffnumber , param.getIdStatusType(), param.getInternalId(), pageable)
                        .map(customShipmentMapper::toDto)
                        .map(customShipmentMapper::assignUnitPrep);
                    return result;
                }
                else {
                    Page<CustomShipmentDTO> result = shipmentOutgoingRepository.queryPickingByStatusAndInternalNotReffNumber(paramLike, paramReffnumber, param.getIdStatusType(), param.getInternalId(), pageable)
                        .map(customShipmentMapper::toDto)
                        .map(customShipmentMapper::assignUnitPrep);
                    return result;
                }

            }
            else {
                Page<CustomShipmentDTO> result = shipmentOutgoingRepository.queryPickingByStatusAndInternalSearch(paramLike, paramReffnumber, param.getIdStatusType(), param.getInternalId(), pageable)
                    .map(customShipmentMapper::toDto)
                    .map(customShipmentMapper::assignUnitPrep);
                return result;
            }

        } else if ("gudang".equalsIgnoreCase(param.getQueryFor())) {
            log.debug("Masuk pickingdo");
            Page<CustomShipmentDTO> result = shipmentOutgoingRepository.queryGudangStatusAndInternalSearch(paramLike, param.getIdStatusType(), param.getInternalId(), pageable)
                .map(customShipmentMapper::toDto)
                .map(customShipmentMapper::assignUnitPrep);
            return result;
        } else if ("pickingdo".equalsIgnoreCase(param.getQueryFor())) {
            log.debug("Masuk Delivery Order");
            Page<CustomShipmentDTO> result = shipmentOutgoingRepository.queryDeliveryOrderStatusAndInternalSearch(paramLike, param.getInternalId(), pageable)
                .map(customShipmentMapper::toDto)
                .map(customShipmentMapper::assignPis);
            return result;
        } else if ("jadwal".equalsIgnoreCase(param.getQueryFor())) {
            log.debug("Masuk Jadwal Pengiriman");
            Page<CustomShipmentDTO> result = shipmentOutgoingRepository.queryFindShipmentOutgoingSearch(paramLike, param.getInternalId(), pageable)
                .map(customShipmentMapper::toDto)
                .map(customShipmentMapper::assignJadwal);
            return result;
        }
        log.debug("Masuk sini");
        return shipmentOutgoingRepository.queryNothing(pageable).map(customShipmentMapper::toDto);
    }

    public void changePickingStatus(ShipmentOutgoing so, Integer status) {
        PickingSlip pis = shipmentItemRepository.getPickingFromShipent(so.getIdShipment());
        //for (PickingSlip pi: pis ) {
            if (pis.getCurrentStatus().equals(BaseConstants.STATUS_DRAFT)) {
                pis.setStatus(status);
                pickingRepo.save(pis);
            } else if (pis.getCurrentStatus().equals(BaseConstants.STATUS_OPEN)) {
                pis.setStatus(status);
                pickingRepo.save(pis);
            } else if (pis.getCurrentStatus().equals(BaseConstants.STATUS_ACTIVE)) {
                pis.setStatus(status);
                pickingRepo.save(pis);
            }
       // }
    }

    public Page<BillingDTO> findBillingByOrderItem(HttpServletRequest request, Pageable pageable){
        UUID idOrderItem = UUID.fromString(request.getParameter("idOrderItem"));
        Page<BillingDTO> billingDTO = null;
        if (idOrderItem != null){
            billingDTO = shipmentOutgoingRepository.queryFindBilling(idOrderItem,pageable).map(billingMapper::toDto);
        }
        return billingDTO;
    }

}
