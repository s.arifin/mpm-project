package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.OrderItemSearchRepository;
import id.atiila.repository.search.UnitOrderItemSearchRepository;
import id.atiila.service.dto.OrderItemDTO;
import id.atiila.service.dto.SalesUnitRequirementDTO;
import id.atiila.service.dto.UnitOrderItemDTO;
import id.atiila.service.mapper.OrderItemMapper;
import id.atiila.service.mapper.UnitOrderItemMapper;
import id.atiila.service.util.CustomTextUtils;
import net.bytebuddy.implementation.bytecode.Throw;
import org.apache.commons.lang3.RandomUtils;
import org.elasticsearch.index.query.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing OrderItem.
 * BeSmart Team
 */

@Service
@Transactional
public class OrderItemService {

    private final Logger log = LoggerFactory.getLogger(OrderItemService.class);

    private final OrderItemRepository orderItemRepository;

    private final OrderItemMapper orderItemMapper;

    private final OrderItemSearchRepository orderItemSearchRepository;

    @Autowired
    private PriceUtils priceUtils;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private CustomerUtils customerUtils;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private UnitOrderItemMapper unitOrderItemMapper;

    @Autowired
    private RequirementOrderItemRepository requirementOrderItemRepository;

    @Autowired
    private ActivitiProcessor activitiProcessor;

    @Autowired
    private UnitOrderItemSearchRepository unitOrderItemSearchRepository;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private VehicleSalesOrderRepository vehicleSalesOrderRepository;

    @Autowired
    private SalesUnitRequirementRepository salesUnitRequirementRepository;


    public OrderItemService(OrderItemRepository orderItemRepository, OrderItemMapper orderItemMapper, OrderItemSearchRepository orderItemSearchRepository, UnitOrderItemSearchRepository unitOrderItemSearchRepository) {
        this.orderItemRepository = orderItemRepository;
        this.orderItemMapper = orderItemMapper;
        this.orderItemSearchRepository = orderItemSearchRepository;
        this.unitOrderItemSearchRepository = unitOrderItemSearchRepository;
    }

    /**
     * Save a orderItem.
     *
     * @param orderItemDTO the entity to save
     * @return the persisted entity
     */
    public OrderItemDTO save(OrderItemDTO orderItemDTO) {
        log.debug("Request to save OrderItem : {}", orderItemDTO);
        OrderItem orderItem = orderItemMapper.toEntity(orderItemDTO);

        ShipTo shipTo = null;
        BigDecimal currentPrice = null;

        Orders orders = ordersRepository.findOne(orderItemDTO.getOrdersId());
        if (orders != null) {
            if (orders instanceof VendorOrder) {
                VendorOrder vorder = (VendorOrder) orders;
                currentPrice = priceUtils.getSalesPrice(vorder.getVendor(), orderItem.getIdProduct());
                shipTo = internalUtils.getShipTo(vorder.getInternal());

                if (currentPrice.doubleValue() == BigDecimal.ZERO.doubleValue()) {
                    priceUtils.setVendorSalesPrice(vorder.getVendor(), orderItem.getIdProduct(), BigDecimal.valueOf(RandomUtils.nextInt(37, 125) * 275));
                }
            } else if (orders instanceof CustomerOrder) {
                CustomerOrder corder = (CustomerOrder) orders;
                currentPrice = priceUtils.getSalesPrice(corder.getInternal(), orderItem.getIdProduct());
                shipTo = customerUtils.getShipTo(corder.getCustomer());

                if (currentPrice.doubleValue() == BigDecimal.ZERO.doubleValue()) {
                    priceUtils.setInternalSalesPrice(corder.getInternal(), orderItem.getIdProduct(), BigDecimal.valueOf(RandomUtils.nextInt(37, 125) * 275));
                }
            }
        }

        // Jika tidak di temukan, buat angkanya menjadi 0
        if (currentPrice == null) currentPrice = BigDecimal.ZERO;

        // find Unit Price and Discount
        double qty = orderItem.getQty() == null ? 1 : orderItem.getQty().doubleValue();
        double price = orderItem.getUnitPrice() == null || orderItem.getUnitPrice().doubleValue() == 0d ?
            currentPrice.doubleValue() :
            orderItem.getUnitPrice().doubleValue() + orderItem.getTaxAmount().doubleValue();
        double discount = orderItem.getDiscount() == null ? 0d: orderItem.getDiscount().doubleValue();
        double netPrice = price - discount;

        orderItem.setUnitPrice(BigDecimal.valueOf(netPrice - (netPrice * 10/100)));
        orderItem.setDiscount(BigDecimal.valueOf(discount));
        orderItem.setQty(qty);
        orderItem.setTaxAmount(BigDecimal.valueOf(netPrice * 10/100));
        orderItem.setTotalAmount(BigDecimal.valueOf(qty * netPrice));

        if (orderItem.getIdShipTo() == null) {
            orderItem.setIdShipTo(shipTo.getIdShipTo());
        }

        orderItem = orderItemRepository.save(orderItem);
        OrderItemDTO result = orderItemMapper.toDto(orderItem);
        orderItemSearchRepository.save(orderItem);

        unitOrderItemSearchRepository.save(unitOrderItemMapper.toDto(orderItem));
        return result;
    }

    /**
     * Get all the orderItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrderItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrderItems");
        Page<OrderItemDTO> orderItemDTOS = orderItemRepository.findAll(pageable).map(orderItemMapper::toDto).map(this::findCogsUnit);
        return orderItemDTOS;
    }

    /**
     * Get one orderItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public OrderItemDTO findOne(UUID id) {
        log.debug("Request to get OrderItem : {}", id);
        OrderItem orderItem = orderItemRepository.findOne(id);
        return orderItemMapper.toDto(orderItem);
    }

    /**
     * Delete the orderItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete OrderItem : {}", id);
        orderItemRepository.delete(id);
        orderItemSearchRepository.delete(id);
    }

    /**
     * Search for the orderItem corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrderItemDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of OrderItems for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idOrders = request.getParameter("idOrders");

        if (idOrders != null) {
            q.withQuery(matchQuery("orders.idOrder", idOrders));
        }

        Page<OrderItem> result = orderItemSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(orderItemMapper::toDto).map(this::findCogsUnit);
    }

    @Transactional(readOnly = true)
    public Page<UnitOrderItemDTO> searchUnit(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of OrderItems for query {}", query);
//        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
//        String idOrders = request.getParameter("idOrders");
//        String orderNumber = request.getParameter("orderNumber");
//        String idInternal = request.getParameter("idInternal");

//        if (idOrders != null) {
//            q.withQuery(matchQuery("orders.idOrder", idOrders));
//        }

//        if (orderNumber != null) {
//            q.withQuery(matchQuery("orders.orderNumber", orderNumber));
//        }

//        if (idInternal != null) {
//            q.withQuery(matchQuery("idInternal", idInternal));
//        }
        log.debug("Request to search for a page of VehicleSalesBillings for query {}", query);
        String idInternal = partyUtils.getCurrentInternal().getIdInternal();

        query = CustomTextUtils.elasticSearchEscapeString(query);
        log.debug("result of escaping string" + query);

        QueryBuilder boolQueryBuilder = queryStringQuery("*"+query+"*")
            .queryName("orderNumber");

        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder();
        q.withQuery(boolQuery().must(boolQueryBuilder).filter(
            boolQuery().must(matchQuery("idInternal", idInternal)))
        );

        Page<UnitOrderItemDTO> result = unitOrderItemSearchRepository.search(q.build().getQuery(), pageable);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<OrderItemDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered OrderItemDTO");
        String idOrders = request.getParameter("idOrders");
        String idShipTo = request.getParameter("idShipTo");
        String idShipFrom = request.getParameter("idShipFrom");
        String orderNumber = request.getParameter("orderNumber");
        String idBill = request.getParameter("idBill");
        String idFrame = request.getParameter("idFrame");
        Internal internal = partyUtils.getCurrentInternal();
        String queryFor = request.getParameter("queryFor");
        String idProduct = request.getParameter("idProduct");
        log.debug("bill and product " + idBill + "  " + idProduct);
        String idbilling = request.getParameter("idbilling");



        if (idOrders != null) {
            return orderItemRepository.findByParams(UUID.fromString(idOrders), pageable).map(orderItemMapper::toDto).map(this::findCogsUnit);
        } else
        if (idShipTo != null && idShipFrom != null) {
            return orderItemRepository.findByPurchaseUnfilled(idShipFrom, idShipTo, pageable).map(orderItemMapper::toDto).map(this::findCogsUnit);
        } else
        if (orderNumber != null) {
            return orderItemRepository.queryByOrderNumber(orderNumber, pageable).map(orderItemMapper::toDto).map(this::findCogsUnit);
        } else
        if (idBill != null) {
            return orderItemRepository.findOrderItemByIdBill(UUID.fromString(idBill), pageable).map(orderItemMapper::toDto).map(this::findCogsUnit);
        } else
        if (idFrame != null) {
            return orderItemRepository.findCogsBynoka(idFrame, pageable).map(orderItemMapper::toDto).map(this::findCogsUnit);
        } else
        if (idbilling != null && idProduct != null){
            log.debug("bill and product daad " + idbilling + "  " + idProduct);
            return orderItemRepository.findOrderItemByIdBillAndIdProduct(UUID.fromString(idbilling), idProduct, pageable).map(orderItemMapper::toDto).map(this::findCogsUnit);
        }
        return orderItemRepository.queryNothing(pageable).map(orderItemMapper::toDto).map(this::findCogsUnit);
    }

    @Transactional(readOnly = true)
    public Page<UnitOrderItemDTO> findUnitFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered OrderItemDTO");
        String idInternal = request.getParameter("idInternal");
        if (idInternal != null) {
            return orderItemRepository.findActiveOrderItem(idInternal, pageable)
                .map(unitOrderItemMapper::toDto)
                .map(this::assignROI);
        }
        return orderItemRepository.queryNothing(pageable)
            .map(unitOrderItemMapper::toDto)
            .map(this::assignROI);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        String command = (String) item.get("command");
        String idOrderItem = (String) item.get("idOrderItem");

        if (command != null && "buildBilling".equalsIgnoreCase(command) && idOrderItem != null) {
            // Buat IVU
            buildIVU(UUID.fromString(idOrderItem));
        }

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public void buildIVU(UUID idOrderItem) {
        OrderItem o = orderItemRepository.findOne(idOrderItem);
        if (o != null) {
            log.debug("1");
            VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(o.getOrders().getIdOrder());
            log.debug("2");
            List<VehicleSalesOrder> vsoexists = vehicleSalesOrderRepository.vsoExistBilling(o.getOrders().getIdOrder());
            log.debug("3 =" + vsoexists);

            if (vso.getCustomer().getParty().getPostalAddress().getAddress1() == null ||
                vso.getCustomer().getParty().getPostalAddress().getAddress2() == null ||
                vso.getCustomer().getParty().getPostalAddress().getProvince() == null ||
                vso.getCustomer().getParty().getPostalAddress().getCity() == null ||
                vso.getCustomer().getParty().getPostalAddress().getDistrict() == null ||
                vso.getCustomer().getParty().getPostalAddress().getVillage() == null ||
                vso.getCustomer().getParty().getPostalAddress().getAddress1().length() < 0 ||
                vso.getCustomer().getParty().getPostalAddress().getAddress2().length() < 0) {
                throw new DmsException(" Alamat Customer Tidak Boleh Kosong");
            }
            if (vso.getCurrentStatus().intValue() == BaseConstants.STATUS_COMPLETED ||
                vso.getCurrentStatus().intValue() == BaseConstants.STATUS_VSO_WAITING_ADMIN_HANDLING ||
                !vsoexists.isEmpty()) {
                throw new DmsException("Data VSO sudah di Create IVU");
            }
            if((vso.getRefundFinco() == null ||vso.getRefundFinco() == null) && (vso.getIdSaleType() == BaseConstants.SALE_TYPE_CREDIT || vso.getIdSaleType() == BaseConstants.SALE_TYPE_CREDIT_CBD || vso.getIdSaleType() == BaseConstants.SALE_TYPE_CREDIT_COD)){
                throw new DmsException("Refund Leasing Tidak Boleh Kosong Karena Penjualan ini Kredit.");
            }
//            if (vso.getOrderType().getIdOrderType() == BaseConstants.ORDER_TYPE_VSO) {
                Map<String, Object> vars = activitiProcessor.getVariables("orderItem", o);
                vars.put("vso", o.getOrders());
                activitiProcessor.startProcess("vso-item-process", o.getIdOrderItem().toString(), vars);
//            } else {
//                Map<String, Object> vars = activitiProcessor.getVariables("orderItem", o);
//                vars.put("vso", o.getOrders());
//                activitiProcessor.startProcess("vso-item-gc-process", o.getIdOrderItem().toString(), vars);
//            }
        }
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, OrderItemDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<OrderItemDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();

        String command = request.getParameter("command");
        if (command != null && command.equals("save")) {
            for (OrderItemDTO d: items) {
                save(d);
            }
            r.put("result", "success");
        }
        return r;
    }

    public UnitOrderItemDTO assignROI(UnitOrderItemDTO dto) {
        RequirementOrderItem roi = requirementOrderItemRepository.findRequirementOrderItemByOrder(dto.getIdOrderItem());
        ShipTo shipTo = null;
        if (roi.getOrderItem() != null) shipTo = partyUtils.getShipTo(roi.getOrderItem().getIdShipTo());
        if (roi != null) dto.setRequirementOrderItem(roi, shipTo);
        if (roi.getOrderItem().getOrders() != null) {
            VehicleSalesOrder vso = (VehicleSalesOrder) roi.getOrderItem().getOrders();
            dto.setIdInternal(vso.getInternal().getIdInternal());
        }
        return dto;
    }

    @Async
    public void buildIndex() {
        unitOrderItemSearchRepository.deleteAll();
        List<OrderItem> orderitems = orderItemRepository.findToReindex();
        for (OrderItem oi: orderitems) {
            unitOrderItemSearchRepository.save(assignROI(unitOrderItemMapper.toDto(oi)));
            log.debug("Data Unit OrderItem save !.....");
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public OrderItemDTO processExecuteData(Integer id, String param, OrderItemDTO dto) {
        OrderItemDTO r = dto;
        switch (id) {
            case 1000:
                r = updateHarga(dto);
                break;
            default:
                break;
        }

        return r;
    }

    public OrderItemDTO findCogsUnit (OrderItemDTO dto) {
        Internal internal = partyUtils.getCurrentInternal();
        List<OrderItem> loi = null;
        if (dto.getIdframe() != null) {
            loi = orderItemRepository.findCogs(null, dto.getIdframe(), internal.getIdInternal());
        } else {
            loi = orderItemRepository.findCogs(dto.getIdProduct(), null, internal.getIdInternal());
        }

        if (!loi.isEmpty()) {
            OrderItem oi = loi.get(0);
            dto.setCogs(oi.getUnitPrice());
        }

        return dto;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public OrderItemDTO updateHarga (OrderItemDTO dto){
        OrderItem o = orderItemRepository.findOneWithEagerRelationships(dto.getIdOrderItem());
        VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(dto.getOrdersId());
        if ( vso != null) {
            SalesUnitRequirement sur = salesUnitRequirementRepository.findOne(vso.getIdSalesUnitRequirement());
            if (sur != null) {
                if (dto != null) {
                    log.debug("oooooooooooooooooooo == " + o );
                    log.debug("getDiscount == " + dto.getDiscount());
                    o.setDiscount(dto.getDiscount());
                    log.debug("getDiscountFinCoy    == " + dto.getDiscountFinCoy());
                    o.setDiscountFinCoy(dto.getDiscountFinCoy());
                    orderItemRepository.saveAndFlush(o);
                    sur.setSubsown(dto.getDiscount());
                    sur.setSubsfincomp(dto.getDiscountFinCoy());
                    salesUnitRequirementRepository.saveAndFlush(sur);
                }
            }
        }

        return dto;
    }

    //TODO: REMARK JIKA INGIN COBA ELASTIC DI LOCAL
//    @Transactional
//    @PostConstruct
//    public void initForMe() {
//        List<OrderItem> l = orderItemRepository.findToReindex();
//        for (OrderItem oi: l) {
//            UnitOrderItemDTO vsbd = unitOrderItemMapper.toDto(oi);
//            unitOrderItemSearchRepository.save(assignROI(unitOrderItemMapper.toDto(oi)));
//            System.out.println("Data unit orderitem " + vsbd.toString());
//        }
//    }

}
