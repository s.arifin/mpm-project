package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.CommunicationEventProspect;
import id.atiila.domain.Internal;
import id.atiila.domain.Party;
import id.atiila.domain.Prospect;
import id.atiila.repository.CommunicationEventProspectRepository;
import id.atiila.repository.ProspectRepository;
import id.atiila.repository.search.CommunicationEventProspectSearchRepository;
import id.atiila.service.dto.CommunicationEventProspectDTO;
import id.atiila.service.mapper.CommunicationEventProspectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import id.atiila.repository.ProspectRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import java.util.UUID;

/**
 * Service Implementation for managing CommunicationEventProspect.
 * BeSmart Team
 */

@Service
@Transactional
public class CommunicationEventProspectService {

    private final Logger log = LoggerFactory.getLogger(CommunicationEventProspectService.class);

    private final CommunicationEventProspectRepository communicationEventProspectRepository;

    private final CommunicationEventProspectMapper communicationEventProspectMapper;

    private final CommunicationEventProspectSearchRepository communicationEventProspectSearchRepository;

    private final ProspectRepository prospectRepository;

    @Autowired
    private ProspectPersonService prospectPersonService;

    @Autowired
    private ProspectOrganizationService prospectOrganizationService;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private MasterNumberingService masterNumberingService;

    public CommunicationEventProspectService(ProspectRepository prospectRepository,CommunicationEventProspectRepository communicationEventProspectRepository, CommunicationEventProspectMapper communicationEventProspectMapper, CommunicationEventProspectSearchRepository communicationEventProspectSearchRepository) {
        this.communicationEventProspectRepository = communicationEventProspectRepository;
        this.prospectRepository = prospectRepository;
        this.communicationEventProspectMapper = communicationEventProspectMapper;
        this.communicationEventProspectSearchRepository = communicationEventProspectSearchRepository;
    }

    /**
     * Save a communicationEventProspect.
     *
     * @param communicationEventProspectDTO the entity to save
     * @return the persisted entity
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public CommunicationEventProspectDTO save(CommunicationEventProspectDTO communicationEventProspectDTO) {
        log.debug("Request to save CommunicationEventProspect : {}", communicationEventProspectDTO);
        CommunicationEventProspect communicationEventProspect = communicationEventProspectMapper.toEntity(communicationEventProspectDTO);
        communicationEventProspect = communicationEventProspectRepository.save(communicationEventProspect);
        CommunicationEventProspectDTO result = communicationEventProspectMapper.toDto(communicationEventProspect);
        log.debug("get Note : {}", communicationEventProspect.getNote()) ;
        communicationEventProspectSearchRepository.save(communicationEventProspect);
        return result;
    }

    /**
     *  Get all the communicationEventProspects.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CommunicationEventProspectDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CommunicationEventProspects");
        return communicationEventProspectRepository.findActiveCommunicationEventProspect(pageable)
            .map(communicationEventProspectMapper::toDto);
    }

    /**
     *  Get all the communicationEventProspects by Id Prospect
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<CommunicationEventProspectDTO> findAllByIdProspect(UUID idprospect) {
        log.debug("Request to get all CommunicationEventProspects");
        List<CommunicationEventProspect> list = communicationEventProspectRepository.findActiveCommunicationEventByIdProspect(idprospect);
        return communicationEventProspectMapper.toDto(list);
    }

    @Transactional
    public void createProspectNumber(String prospectNumber, Internal internal){
        log.debug("START find or create prospectnumber code");
        Prospect e = prospectRepository.findByProspectNumber(prospectNumber);
        if (e.getProspectNumber() == null) {
            String pnumber = masterNumberingService.nextProspectNumber (internal.getIdInternal(), "IDPRO") ;
            e.setProspectNumber(pnumber);
            prospectRepository.save(e);
        }
    }

    @Transactional(readOnly = true)
    public Page<CommunicationEventProspectDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered OrderItemDTO");
        String idProspect = request.getParameter("idProspect");
        String interest = request.getParameter("interest");

        if (idProspect != null) {
            return communicationEventProspectRepository.findByParams(UUID.fromString(idProspect), Boolean.parseBoolean(interest), pageable).map(communicationEventProspectMapper::toDto);
        }
        return communicationEventProspectRepository.queryNothing(pageable).map(communicationEventProspectMapper::toDto);
    }

    /**
     *  Get one communicationEventProspect by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CommunicationEventProspectDTO findOne(UUID id) {
        log.debug("Request to get CommunicationEventProspect : {}", id);
        CommunicationEventProspect communicationEventProspect = communicationEventProspectRepository.findOne(id);
        return communicationEventProspectMapper.toDto(communicationEventProspect);
    }

    /**
     *  Delete the  communicationEventProspect by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete CommunicationEventProspect : {}", id);
        communicationEventProspectRepository.delete(id);
        communicationEventProspectSearchRepository.delete(id);
    }

    /**
     * Search for the communicationEventProspect corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CommunicationEventProspectDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CommunicationEventProspects for query {}", query);
        Page<CommunicationEventProspect> result = communicationEventProspectSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(communicationEventProspectMapper::toDto);
    }

    public CommunicationEventProspectDTO processExecuteData(Integer id, String param, CommunicationEventProspectDTO dto) {
        CommunicationEventProspectDTO r = dto;
        if (r != null) {
            switch (id) {
                case 101:
                    checkInterestCommunicationEvent(r,id);
                    save(r);
                    break;
                case 102:
                    checkInterestCommunicationEvent(r,id);
                    break;
                case 103:
                    changeCommunicationEventProspectStatus(r, BaseConstants.STATUS_COMPLETED);
                    prospectPersonService.buildPersonalCustomerSURBaseOnStatus(partyUtils.getCurrentInternal(), r);
                default:
                    break;
            }
		}
        return r;
    }

    public Set<CommunicationEventProspectDTO> processExecuteListData(Integer id, String param, Set<CommunicationEventProspectDTO> dto) {
        Set<CommunicationEventProspectDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public CommunicationEventProspectDTO changeCommunicationEventProspectStatus(CommunicationEventProspectDTO dto, Integer id) {
        if (dto != null) {
			CommunicationEventProspect e = communicationEventProspectRepository.findOne(dto.getIdCommunicationEvent());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        communicationEventProspectSearchRepository.delete(dto.getIdCommunicationEvent());
                        break;
                    default:
                        communicationEventProspectSearchRepository.save(e);
                }
				communicationEventProspectRepository.save(e);
			}
		}
        return dto;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void checkInterestCommunicationEvent(CommunicationEventProspectDTO ce, Integer id){
        if (id == 101) {
            if (ce.getId() != null && ce.getIdCommunicationEvent() != null) {
                if (ce.getInterest() == true) {
                    prospectPersonService.updateStatusProspectBasedOnPurchasePlan(ce);
                }
            }
        } else if (id == 102){
            if (ce.getId() != null && ce.getIdCommunicationEvent() != null) {
                if (ce.getInterest() == true) {
                    prospectOrganizationService.updateStatusProspectBasedOnPurchasePlan(ce);
                }
            }
        }
    }
}
