package id.atiila.service;

import id.atiila.domain.PartyCategoryType;
import id.atiila.repository.PartyCategoryTypeRepository;
import id.atiila.repository.search.PartyCategoryTypeSearchRepository;
import id.atiila.service.dto.PartyCategoryTypeDTO;
import id.atiila.service.mapper.PartyCategoryTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing PartyCategoryType.
 * BeSmart Team
 */

@Service
@Transactional
public class PartyCategoryTypeService {

    private final Logger log = LoggerFactory.getLogger(PartyCategoryTypeService.class);

    private final PartyCategoryTypeRepository partyCategoryTypeRepository;

    private final PartyCategoryTypeMapper partyCategoryTypeMapper;

    private final PartyCategoryTypeSearchRepository partyCategoryTypeSearchRepository;

    public PartyCategoryTypeService(PartyCategoryTypeRepository partyCategoryTypeRepository, PartyCategoryTypeMapper partyCategoryTypeMapper, PartyCategoryTypeSearchRepository partyCategoryTypeSearchRepository) {
        this.partyCategoryTypeRepository = partyCategoryTypeRepository;
        this.partyCategoryTypeMapper = partyCategoryTypeMapper;
        this.partyCategoryTypeSearchRepository = partyCategoryTypeSearchRepository;
    }

    /**
     * Save a partyCategoryType.
     *
     * @param partyCategoryTypeDTO the entity to save
     * @return the persisted entity
     */
    public PartyCategoryTypeDTO save(PartyCategoryTypeDTO partyCategoryTypeDTO) {
        log.debug("Request to save PartyCategoryType : {}", partyCategoryTypeDTO);
        PartyCategoryType partyCategoryType = partyCategoryTypeMapper.toEntity(partyCategoryTypeDTO);
        partyCategoryType = partyCategoryTypeRepository.save(partyCategoryType);
        PartyCategoryTypeDTO result = partyCategoryTypeMapper.toDto(partyCategoryType);
        partyCategoryTypeSearchRepository.save(partyCategoryType);
        return result;
    }

    /**
     * Get all the partyCategoryTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartyCategoryTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PartyCategoryTypes");
        return partyCategoryTypeRepository.findAll(pageable)
            .map(partyCategoryTypeMapper::toDto);
    }

    /**
     * Get one partyCategoryType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PartyCategoryTypeDTO findOne(Integer id) {
        log.debug("Request to get PartyCategoryType : {}", id);
        PartyCategoryType partyCategoryType = partyCategoryTypeRepository.findOne(id);
        return partyCategoryTypeMapper.toDto(partyCategoryType);
    }

    /**
     * Delete the partyCategoryType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete PartyCategoryType : {}", id);
        partyCategoryTypeRepository.delete(id);
        partyCategoryTypeSearchRepository.delete(id);
    }

    /**
     * Search for the partyCategoryType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PartyCategoryTypeDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        log.debug("Request to search for a page of PartyCategoryTypes for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idParent = request.getParameter("idParent");

        if (idParent != null) {
            q.withQuery(matchQuery("parent.idCategoryType", idParent));
        }

        Page<PartyCategoryType> result = partyCategoryTypeSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(partyCategoryTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<PartyCategoryTypeDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered PartyCategoryTypeDTO");
        String idParent = request.getParameter("idParent");

        if (idParent != null) {
            return partyCategoryTypeRepository.queryByIdParent(Integer.valueOf(idParent), pageable).map(partyCategoryTypeMapper::toDto); 
        }

        return partyCategoryTypeRepository.queryNothing(pageable).map(partyCategoryTypeMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteData(HttpServletRequest request, PartyCategoryTypeDTO item) {
        log.debug("Execute any Process, with entity as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public Map<String, Object> processExecuteListData(HttpServletRequest request, Set<PartyCategoryTypeDTO> items) {
        log.debug("Execute any Process, with entities as param, request from front end");
        Map<String, Object> r = new HashMap<>();
        return r;
    }

}
