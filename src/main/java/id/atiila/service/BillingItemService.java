package id.atiila.service;

import id.atiila.domain.BillingItem;
import id.atiila.repository.BillingItemRepository;
import id.atiila.repository.search.BillingItemSearchRepository;
import id.atiila.service.dto.BillingItemDTO;
import id.atiila.service.mapper.BillingItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import javax.servlet.http.HttpServletRequest;

import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;

/**
 * Service Implementation for managing BillingItem.
 * BeSmart Team
 */

@Service
@Transactional
public class BillingItemService {

    private final Logger log = LoggerFactory.getLogger(BillingItemService.class);

    private final BillingItemRepository billingItemRepository;

    private final BillingItemMapper billingItemMapper;

    private final BillingItemSearchRepository billingItemSearchRepository;

    public BillingItemService(BillingItemRepository billingItemRepository, BillingItemMapper billingItemMapper, BillingItemSearchRepository billingItemSearchRepository) {
        this.billingItemRepository = billingItemRepository;
        this.billingItemMapper = billingItemMapper;
        this.billingItemSearchRepository = billingItemSearchRepository;
    }

    /**
     * Save a billingItem.
     *
     * @param billingItemDTO the entity to save
     * @return the persisted entity
     */
    public BillingItemDTO save(BillingItemDTO billingItemDTO) {
        log.debug("Request to save BillingItem : {}", billingItemDTO);
        if (billingItemDTO.getUnitPrice() == null){
            billingItemDTO.setUnitPrice(BigDecimal.valueOf(0));
        }
        if (billingItemDTO.getTaxAmount() == null){
            billingItemDTO.setTaxAmount(BigDecimal.valueOf(0));
        }
        if(billingItemDTO.getQty() == null){
            billingItemDTO.setQty(Double.valueOf(0));
        }
        if(billingItemDTO.getDiscount() == null){
            billingItemDTO.setDiscount(BigDecimal.valueOf(0));
        }
        if(billingItemDTO.getTotalAmount() == null){
            billingItemDTO.setTotalAmount(BigDecimal.valueOf(0));
        }
        BillingItem billingItem = billingItemMapper.toEntity(billingItemDTO);
        billingItem = billingItemRepository.save(billingItem);
        BillingItemDTO result = billingItemMapper.toDto(billingItem);
        billingItemSearchRepository.save(billingItem);
        return result;
    }

    /**
     * Get all the billingItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BillingItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BillingItems");
        return billingItemRepository.findAll(pageable).map(billingItemMapper::toDto);
    }


    @Transactional(readOnly = true)
    public Page<BillingItemDTO> findItemForTaxInvoice(Pageable pageable) {
        log.debug("Request to get BillingItems for tax invoice");

        return billingItemRepository.findItemForTaxInvList(pageable)
            .map(billingItemMapper::toDto);

    }

    /**
     * Get one billingItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BillingItemDTO findOne(UUID id) {
        log.debug("Request to get BillingItem : {}", id);
        BillingItem billingItem = billingItemRepository.findOne(id);
        return billingItemMapper.toDto(billingItem);
    }

    /**
     * Delete the billingItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete BillingItem : {}", id);
        billingItemRepository.delete(id);
        billingItemSearchRepository.delete(id);
    }

    /**
     * Search for the billingItem corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BillingItemDTO> search(HttpServletRequest request, String query, Pageable pageable) {
        String filterName = request.getParameter("filterName");
        log.debug("Request to search for a page of BillingItems for query {}", query);
        NativeSearchQueryBuilder q = new NativeSearchQueryBuilder().withQuery(queryStringQuery(query));
        String idBilling = request.getParameter("idBilling");
        String idInventoryItem = request.getParameter("idInventoryItem");

        if (filterName != null) {
        }
        Page<BillingItem> result = billingItemSearchRepository.search(q.build().getQuery(), pageable);
        return result.map(billingItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<BillingItemDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered BillingItemDTO");
        String idBilling = request.getParameter("idBilling");
        String billingNumber = request.getParameter("billingNumber");
        if (idBilling != null) {
            return billingItemRepository.findByByIdBilling(UUID.fromString(idBilling), pageable)
                .map(billingItemMapper::toDto);
        } else if (billingNumber != null) {
            return billingItemRepository.queryItemByIVU(billingNumber, pageable)
                .map(billingItemMapper::toDto);
        }
        return billingItemRepository.queryNone(pageable).map(billingItemMapper::toDto);
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }
        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Transactional
    public BillingItemDTO processExecuteData(HttpServletRequest request, Map<String, Object> item) {
        BillingItemDTO r = null;
        return r;
    }

    @Transactional
    public Set<BillingItemDTO> processExecuteListData(HttpServletRequest request, Map<String, Object> item) {
        Set<BillingItemDTO> r = new HashSet<>();
        return r;
    }

    @Async
    public void buildIndex() {
        billingItemSearchRepository.deleteAll();
        List<BillingItem> billingItems =  billingItemRepository.findAll();
        for (BillingItem b: billingItems) {
            billingItemSearchRepository.save(b);
            log.debug("Data Billing Item save !...");
        }
    }

}
