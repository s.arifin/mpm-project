package id.atiila.service;

import id.atiila.domain.PositionFullfillment;
import id.atiila.repository.PositionFullfillmentRepository;
import id.atiila.repository.search.PositionFullfillmentSearchRepository;
import id.atiila.service.dto.PositionFullfillmentDTO;
import id.atiila.service.mapper.PositionFullfillmentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PositionFullfillment.
 * BeSmart Team
 */

@Service
@Transactional
public class PositionFullfillmentService {

    private final Logger log = LoggerFactory.getLogger(PositionFullfillmentService.class);

    private final PositionFullfillmentRepository positionFullfillmentRepository;

    private final PositionFullfillmentMapper positionFullfillmentMapper;

    private final PositionFullfillmentSearchRepository positionFullfillmentSearchRepository;

    public PositionFullfillmentService(PositionFullfillmentRepository positionFullfillmentRepository, PositionFullfillmentMapper positionFullfillmentMapper, PositionFullfillmentSearchRepository positionFullfillmentSearchRepository) {
        this.positionFullfillmentRepository = positionFullfillmentRepository;
        this.positionFullfillmentMapper = positionFullfillmentMapper;
        this.positionFullfillmentSearchRepository = positionFullfillmentSearchRepository;
    }

    /**
     * Save a positionFullfillment.
     *
     * @param positionFullfillmentDTO the entity to save
     * @return the persisted entity
     */
    public PositionFullfillmentDTO save(PositionFullfillmentDTO positionFullfillmentDTO) {
        log.debug("Request to save PositionFullfillment : {}", positionFullfillmentDTO);
        PositionFullfillment positionFullfillment = positionFullfillmentMapper.toEntity(positionFullfillmentDTO);
        positionFullfillment = positionFullfillmentRepository.save(positionFullfillment);
        PositionFullfillmentDTO result = positionFullfillmentMapper.toDto(positionFullfillment);
        positionFullfillmentSearchRepository.save(positionFullfillment);
        return result;
    }

    /**
     *  Get all the positionFullfillments.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PositionFullfillmentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PositionFullfillments");
        return positionFullfillmentRepository.findAll(pageable)
            .map(positionFullfillmentMapper::toDto);
    }

    /**
     *  Get one positionFullfillment by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PositionFullfillmentDTO findOne(Integer id) {
        log.debug("Request to get PositionFullfillment : {}", id);
        PositionFullfillment positionFullfillment = positionFullfillmentRepository.findOne(id);
        return positionFullfillmentMapper.toDto(positionFullfillment);
    }

    /**
     *  Delete the  positionFullfillment by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete PositionFullfillment : {}", id);
        positionFullfillmentRepository.delete(id);
        positionFullfillmentSearchRepository.delete(id);
    }

    /**
     * Search for the positionFullfillment corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PositionFullfillmentDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PositionFullfillments for query {}", query);
        Page<PositionFullfillment> result = positionFullfillmentSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(positionFullfillmentMapper::toDto);
    }

    public PositionFullfillmentDTO processExecuteData(Integer id, String param, PositionFullfillmentDTO dto) {
        PositionFullfillmentDTO r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<PositionFullfillmentDTO> processExecuteListData(Integer id, String param, Set<PositionFullfillmentDTO> dto) {
        Set<PositionFullfillmentDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }

    @Async
    public void buildIndex() {
        positionFullfillmentSearchRepository.deleteAll();
        List<PositionFullfillment> positionFullfillments =  positionFullfillmentRepository.findAll();
        for (PositionFullfillment m: positionFullfillments) {
            positionFullfillmentSearchRepository.save(m);
            log.debug("Data position fullfillment save !...");
        }
    }

}
