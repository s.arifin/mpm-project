package id.atiila.service.impl;

import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.VehicleDocumentRequirementSearchRepository;
import id.atiila.service.*;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class VdrImpl {

    private final Logger log = LoggerFactory.getLogger(VdrImpl.class);

    @Autowired
    private VehicleDocumentRequirementRepository vehicleDocumentRequirementRepository;

    @Autowired
    private VehicleDocumentRequirementSearchRepository vehicleDocumentRequirementSearchRepository;

    @Autowired
    private UnitDeliverableRepository unitDeliverableRepository;

    @Autowired
    private CommunicationEventCDBRepository communicationEventCDBRepository;

    @Autowired
    private SalesUnitRequirementRepository salesUnitRequirementRepository;

    @Autowired
    private OrderUtils orderUtils;

    public void prosesVerifikasiIsiNama(DelegateExecution execution) {
        VehicleDocumentRequirement vdr = execution.getVariable("vdr", VehicleDocumentRequirement.class);
        CommunicationEventCDB cdb = execution.getVariable("cdb", CommunicationEventCDB.class);
        SalesUnitRequirement sur = execution.getVariable("sur", SalesUnitRequirement.class);
        RequirementOrderItem roi = execution.getVariable("roi", RequirementOrderItem.class);
        Customer customer = execution.getVariable("customer", Customer.class);


        Boolean isiNamaValid = false;
        if (vdr == null) {
            String idReq = execution.getProcessInstanceBusinessKey();
            vdr = vehicleDocumentRequirementRepository.findOne(UUID.fromString(idReq));
        }
        if (sur == null) {
            if (vdr.getIdSalesRequirement() != null) {
                sur = salesUnitRequirementRepository.findOne(vdr.getIdSalesRequirement());
                execution.setVariable("sur", sur);
            } else {
                // Data data lama
                if (roi == null) {
                    roi = orderUtils.findRequirementOrderItemByOrder(vdr.getOrderItem());
                    if (roi != null) {
                        sur = roi.getRequirement();
                        execution.setVariable("sur", sur);
                        execution.setVariable("roi", roi);
                    }
                }
                if (roi == null) throw new DmsException("Error, sur tidak di temukan !");
            }
        }


        if (customer == null) {
            customer = sur.getCustomer();
            execution.setVariable("customer", customer);
        }

        if (cdb == null) {
            List<CommunicationEventCDB> items = communicationEventCDBRepository.queryFindCommunicationEventCDBByIdCustomer(customer.getIdCustomer());
            if (items.isEmpty()) {
                // Buat CDB Baru untuk customer
                cdb = new CommunicationEventCDB();
                cdb.setIdCustomer(customer.getIdCustomer());
                cdb = communicationEventCDBRepository.save(cdb);
            } else {
                cdb = items.get(0);
            }
            execution.setVariable("cdb", cdb);
        }

        Person pcheck = vdr.getPersonOwner();
        Organization ocheck = vdr.getOrganizationOwner();
        if (pcheck == null && ocheck != null) {
            isiNamaValid = (ocheck.getNpwp() != null) && (ocheck.getDateOrganizationEstablish() != null) && (ocheck.getNpwp().length() == 16) &&
                (ocheck.getPostalAddress() != null) && (ocheck.getPostalAddress().getAddress1() != null) && (ocheck.getPostalAddress().getProvince() != null) &&
                (ocheck.getPostalAddress().getCity() != null) && (ocheck.getPostalAddress().getDistrict() != null) && (ocheck.getPostalAddress().getVillage() != null) &&
                (cdb.getIdProvinceSurat() != null);
        } else if (pcheck != null && ocheck == null) {
            // Jika isi nama untuk perorangann
            isiNamaValid = (pcheck.getPersonalIdNumber() != null) && (pcheck.getFamilyIdNumber() != null) &&
                (pcheck.getPersonalIdNumber().length() == 16) && (pcheck.getFamilyIdNumber().length() == 16) &&
                (pcheck.getFirstName() != null) && (pcheck.getDob() != null) && (pcheck.getPob() != null) && (pcheck.getReligionType() != null) &&
                (pcheck.getGender() != null) && (pcheck.getPostalAddress() != null) && (pcheck.getPostalAddress().getAddress1() != null) && (pcheck.getPostalAddress().getAddress2() != null) &&
                (pcheck.getPostalAddress().getProvince() != null) && (pcheck.getPostalAddress().getCity() != null) &&
                (pcheck.getPostalAddress().getDistrict() != null) && (pcheck.getPostalAddress().getVillage() != null) &&
                (cdb.getIdProvinceSurat() != null);
        }

        if (isiNamaValid) {
            vdr.setWaitStnk(false);
            vehicleDocumentRequirementRepository.saveAndFlush(vdr);
        }
        execution.setVariable("isiNamaValid", isiNamaValid);
    }

    public void prosesFakturATPM(DelegateExecution execution) {
        VehicleDocumentRequirement vdr = execution.getVariable("vdr", VehicleDocumentRequirement.class);
        if (vdr == null) {
            String idReq = execution.getProcessInstanceBusinessKey();
            vdr = vehicleDocumentRequirementRepository.findOne(UUID.fromString(idReq));
        }
        Boolean offTheRoad = false;
        int saleType = vdr.getSaleType().getIdSaleType();
        if (vdr.getSaleType().equals(saleType == 22 || saleType == 55 || saleType == 54)) {
            offTheRoad = true;
        }
        execution.setVariable("offTheRoad", offTheRoad);

        if (offTheRoad) {
            vdr.setStatus(BaseConstants.STATUS_COMPLETED);
            vehicleDocumentRequirementRepository.save(vdr);
            vehicleDocumentRequirementSearchRepository.delete(vdr.getIdRequirement());
        }
    }

    public void prosesFakturBiroJasa(DelegateExecution execution) {
        VehicleDocumentRequirement vdr = execution.getVariable("vdr", VehicleDocumentRequirement.class);
    }

    public void prosesTerimaNotice(DelegateExecution execution) {
        VehicleDocumentRequirement vdr = execution.getVariable("vdr", VehicleDocumentRequirement.class);
        UnitDeliverable notice = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(vdr.getIdRequirement(), BaseConstants.DELIVERABLE_TYPE_NOTICE);
        execution.setVariable("notice", notice);
    }

    public void prosesTerimaSTNK(DelegateExecution execution) {
        VehicleDocumentRequirement vdr = execution.getVariable("vdr", VehicleDocumentRequirement.class);
        UnitDeliverable stnk = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(vdr.getIdRequirement(), BaseConstants.DELIVERABLE_TYPE_STNK);
        execution.setVariable("stnk", stnk);
    }

    public void prosesBASTStnk(DelegateExecution execution) {
        VehicleDocumentRequirement vdr = execution.getVariable("vdr", VehicleDocumentRequirement.class);
        UnitDeliverable stnk = execution.getVariable("stnk", UnitDeliverable.class);
        // stnk.setStatus(BaseConstants.COMPLETED)
        // UnitDeliverableRepository.save(stnk);
    }

    public void prosesTerimaNoPol(DelegateExecution execution) {
        VehicleDocumentRequirement vdr = execution.getVariable("vdr", VehicleDocumentRequirement.class);
        UnitDeliverable nopol = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(vdr.getIdRequirement(), BaseConstants.DELIVERABLE_TYPE_NOPOL);
        execution.setVariable("nopol", nopol);
    }

    public void prosesBASTNoPol(DelegateExecution execution) {
        VehicleDocumentRequirement vdr = execution.getVariable("vdr", VehicleDocumentRequirement.class);
        UnitDeliverable nopol = execution.getVariable("nopol", UnitDeliverable.class);
    }

    public void prosesTerimaBPKB(DelegateExecution execution) {
        VehicleDocumentRequirement vdr = execution.getVariable("vdr", VehicleDocumentRequirement.class);
        UnitDeliverable bpkb = unitDeliverableRepository.findByIdRequirmentAndIdDeliverableType(vdr.getIdRequirement(), BaseConstants.DELIVERABLE_TYPE_BPKB);
        execution.setVariable("bpkb", bpkb);
    }


    public void prosesBASTBpkb(DelegateExecution execution) {
        VehicleDocumentRequirement vdr = execution.getVariable("vdr", VehicleDocumentRequirement.class);
        UnitDeliverable bpkb = execution.getVariable("bpkb", UnitDeliverable.class);
    }

    public void prosesVDRClose(DelegateExecution execution) {
        VehicleDocumentRequirement vdr = execution.getVariable("vdr", VehicleDocumentRequirement.class);
        CommunicationEventCDB cdb = execution.getVariable("cdb", CommunicationEventCDB.class);
        vdr.setStatus(BaseConstants.STATUS_COMPLETED);
        vehicleDocumentRequirementRepository.save(vdr);
        // Tandai CDB sudah selesai
        cdb.setStatus(BaseConstants.STATUS_COMPLETED);
        communicationEventCDBRepository.saveAndFlush(cdb);
    }

}
