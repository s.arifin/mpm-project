package id.atiila.service.impl;

import id.atiila.base.BaseConstants;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.service.*;
import org.activiti.engine.ActivitiObjectNotFoundException;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class VcrImpl {

    private final Logger log = LoggerFactory.getLogger(VcrImpl.class);

    @Autowired
    private VehicleCustomerRequestRepository vehicleCustomerRequestRepository;

    @Autowired
    private SalesUnitRequirementRepository salesUnitRequirementRepository;

    @Autowired
    private VehicleSalesOrderService vehicleSalesOrderService;

    @Autowired
    private VehicleSalesOrderRepository vehicleSalesOrderRepository;

    @Autowired
    private MasterNumberingService numbering;

    @Autowired
    private SalesBrokerRepository salesBrokerRepository;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private CustomerUtils customerUtils;

    @Autowired
    private OrderTypeRepository orderTypeRepository;

    @Autowired
    private OrderUtils orderUtils;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private CustomerService customerService;

    public VehicleSalesOrder addSURToVSO(Customer customer, VehicleSalesOrder v, SalesUnitRequirement s) {

        RequirementOrderItem r = null;
        ShipTo sp = customerUtils.getShipTo(customer);
        if (v.getOrderType().equals(BaseConstants.ORDER_TYPE_GP)) {
            customerUtils.buildPersonalCustomer(v.getInternal(), s.getPersonOwner());
            sp = partyUtils.getShipTo(s.getPersonOwner());
        }
        Product pr = vehicleSalesOrderRepository.findOneByIdProduct(s.getProduct().getIdProduct());

//        OrderItem o = new OrderItem();
//        o.setIdShipTo(sp.getIdShipTo());
//        o.setIdProduct(s.getProduct().getIdProduct());
//        o.setItemDescription(pr.getName());
//        if (s.getColor() != null )
//        o.setIdFeature(s.getColor().getIdFeature());
//
//        //TODO kasus salah harga
//        o.setUnitPrice(s.getHetprice().doubleValue() / 1.1);
//        o.setDiscount(s.getSubsown());
//        o.setDiscountFinCoy(s.getSubsfincomp());
//        o.setDiscountatpm(s.getSubsahm());
//        o.setDiscountMD(s.getSubsmd());
//        o.setBbn(s.getBbnprice());
//
//        BigDecimal otr = s.getUnitPrice();
//        BigDecimal bbnPrice = s.getBbnprice();
//        BigDecimal OfftheRoad = s.getHetprice();
//        BigDecimal salesAmount = OfftheRoad.multiply(BigDecimal.TEN).divide(BigDecimal.valueOf(11),  0, RoundingMode.HALF_UP);
//        BigDecimal totalDisc = s.getSubsfincomp().add(s.getSubsmd()).add(s.getSubsahm()).add(s.getSubsown());
//        BigDecimal priceAfterDisc = salesAmount.subtract(totalDisc);
//        BigDecimal tax = salesAmount.divide(BigDecimal.TEN, 0, RoundingMode.HALF_UP);
//
//        o.setDiscountMD(s.getSubsmd());
//        o.setDiscountFinCoy(s.getSubsfincomp());
//        o.setDiscountatpm(s.getSubsahm());
//        o.setDiscount(s.getSubsown());
//        o.setBbn(s.getBbnprice());
//        o.setTaxAmount(tax);
//        o.setQty(1d);
//        o.setOrders(v);
//        o = orderItemRepository.save(o);
//
//        //   Marking Sur ke VSO
//        r = orderUtils.getRequirementOrderItemByOrder(s, o, 1);
//        v.getDetails().add(o);
//        v = vehicleSalesOrderRepository.save(v);
        return v;
    }

    public void process(DelegateExecution execution) {
        VehicleCustomerRequest vcr = execution.getVariable("request", VehicleCustomerRequest.class);
        List<SalesUnitRequirement> surs = salesUnitRequirementRepository.quesryByIdRequest(vcr.getIdRequest());
        Internal internal = internalUtils.findOne(vcr.getIdInternal());
        Customer customer = vcr.getCustomer();

        List<SalesUnitRequirement> LSUR = salesUnitRequirementRepository.quesryByIdRequest(vcr.getIdRequest());


        log.debug("innnnnnnnnnnn  " + internal);
        String vsonumber = numbering.findTransNumberBy(vcr.getInternal().getIdInternal(), "VSO");
        for(SalesUnitRequirement s: LSUR) {
            log.debug("ooooooooooooooo  " + vsonumber);
            s.setRequirementNumber(vsonumber);
        }

        execution.setVariable("customer", customer);
        execution.setVariable("listItem", surs);
        execution.setVariable("internal", internal);
    }

    public void processGC(DelegateExecution execution) {
        VehicleCustomerRequest vcr = execution.getVariable("request", VehicleCustomerRequest.class);
        Internal internal = execution.getVariable("internal", Internal.class);
        SalesUnitRequirement sur = execution.getVariable("item", SalesUnitRequirement.class);
        Customer customer = execution.getVariable("customer", Customer.class);
        BillTo billTo = customerUtils.getBillTo(customer);

        customerService.findOrCreateCustomerCode(customer.getIdCustomer(), sur.getInternal());

        if (sur.getCurrentStatus() != BaseConstants.STATUS_CANCEL && sur.getCurrentStatus() != BaseConstants.STATUS_COMPLETED){
            // Update SUR customer dan SUR organization Owner
            if (sur.getCustomer() == null || sur.getOrganizationOwner() == null) {
                sur.setCustomer(customer);
                sur.setOrganizationOwner((Organization) customer.getParty());
                salesUnitRequirementRepository.save(sur);
            }

            VehicleSalesOrder vso = orderUtils.getDraftVSO(BaseConstants.ORDER_TYPE_GC, internal, billTo, customer, sur);
            addSURToVSO(customer, vso, sur);

//            String vsonumber = numbering.findTransNumberBy(internal.getRoot().getIdInternal(), "VSO");
//            vso.setOrderNumber(vsonumber);
//            vso.setAfcVerifyValue(1);
//            vso.setAdminSalesVerifyValue(1);
//            vso.setOrderNumber(sur.getRequirementNumber());
//            vso.setStatus(BaseConstants.STATUS_OPEN);
//            vso = vehicleSalesOrderRepository.save(vso);
            execution.setVariable("vso", vso);
        }
    }

    public void processGP(DelegateExecution execution) {
        log.debug("PROCESSING GROUP PERSON");
        VehicleCustomerRequest vcr = execution.getVariable("request", VehicleCustomerRequest.class);
        Internal internal = execution.getVariable("internal", Internal.class);
        SalesUnitRequirement sur = execution.getVariable("item", SalesUnitRequirement.class);
        Customer customer = execution.getVariable("customer", Customer.class);
        BillTo billTo = customerUtils.getBillTo(customer);

        customerService.findOrCreateCustomerCode(customer.getIdCustomer(), sur.getInternal());

        if (sur.getCurrentStatus() != BaseConstants.STATUS_CANCEL && sur.getCurrentStatus() != BaseConstants.STATUS_COMPLETED) {
            // Update SUR customer dan SUR Person Owner
            if (sur.getCustomer() == null) {
                sur.setCustomer(customer);
                salesUnitRequirementRepository.save(sur);
            }

            VehicleSalesOrder vso = orderUtils.getDraftVSO(BaseConstants.ORDER_TYPE_GP, internal, billTo, customer, sur);
            addSURToVSO(customer, vso, sur);

//            String vsonumber = numbering.findTransNumberBy(internal.getRoot().getIdInternal(), "VSO");
//            vso.setOrderNumber(vsonumber);
//            vso.setAfcVerifyValue(1);
//            vso.setAdminSalesVerifyValue(1);
//            vso.setOrderNumber(sur.getRequirementNumber());
//            vso.setStatus(BaseConstants.STATUS_OPEN);
//            vso = vehicleSalesOrderRepository.save(vso);
            execution.setVariable("vso", vso);
        }
    }

    public void processDS(DelegateExecution execution) {
        log.debug("PROCESSING Direct Sales");
        VehicleCustomerRequest vcr = execution.getVariable("request", VehicleCustomerRequest.class);
        Internal internal = execution.getVariable("internal", Internal.class);
        SalesUnitRequirement sur = execution.getVariable("item", SalesUnitRequirement.class);
        Customer customer = sur.getCustomer();
        BillTo billTo = customerUtils.getBillTo(sur.getCustomer());

        customerService.findOrCreateCustomerCode(customer.getIdCustomer(), sur.getInternal());

        if (sur.getCurrentStatus() != BaseConstants.STATUS_CANCEL && sur.getCurrentStatus() != BaseConstants.STATUS_COMPLETED) {
            if (sur.getBrokerId() == null) {
                sur.setBrokerId(vcr.getSalesBroker().getIdPartyRole());
                sur = salesUnitRequirementRepository.save(sur);
            }

            VehicleSalesOrder vso = orderUtils.getDraftVSO(BaseConstants.ORDER_TYPE_DS, internal, billTo, customer, sur);
            addSURToVSO(customer, vso, sur);
//            vso.setOrderNumber(sur.getRequirementNumber());
//            vso.setStatus(BaseConstants.STATUS_OPEN);
//            vso = vehicleSalesOrderRepository.save(vso);
            execution.setVariable("vso", vso);
        }
    }

    public void processRekanan(DelegateExecution execution) {
        VehicleCustomerRequest vcr = execution.getVariable("request", VehicleCustomerRequest.class);
        Internal internal = execution.getVariable("internal", Internal.class);
        SalesUnitRequirement sur = execution.getVariable("item", SalesUnitRequirement.class);
        Customer customer = execution.getVariable("customer", Customer.class);
        BillTo billTo = customerUtils.getBillTo(customer);

        customerService.findOrCreateCustomerCode(customer.getIdCustomer(), internal);

        if (sur.getCurrentStatus() != BaseConstants.STATUS_CANCEL && sur.getCurrentStatus() != BaseConstants.STATUS_COMPLETED){
            // Update SUR customer dan SUR organization Owner
            if (sur.getCustomer() == null || sur.getOrganizationOwner() == null) {
                sur.setCustomer(customer);
                sur.setOrganizationOwner((Organization) customer.getParty());
                salesUnitRequirementRepository.save(sur);
            }

            VehicleSalesOrder vehicleSalesOrder = orderUtils.getDraftVSO(BaseConstants.ORDER_TYPE_REKANAN, internal, billTo, customer, sur);
            addSURToVSO(customer, vehicleSalesOrder, sur);

//            vehicleSalesOrder.setOrderNumber(sur.getRequirementNumber());
//            vehicleSalesOrder.setStatus(BaseConstants.STATUS_OPEN);
//            vehicleSalesOrder = vehicleSalesOrderRepository.save(vehicleSalesOrder);
            execution.setVariable("vso", vehicleSalesOrder);
        }
    }

    public void processInternal(DelegateExecution execution) {
        VehicleCustomerRequest vcr = execution.getVariable("request", VehicleCustomerRequest.class);
        Internal internal = execution.getVariable("internal", Internal.class);
        SalesUnitRequirement sur = execution.getVariable("item", SalesUnitRequirement.class);
        Customer customer = execution.getVariable("customer", Customer.class);
        BillTo billTo = customerUtils.getBillTo(customer);

        customerService.findOrCreateCustomerCode(customer.getIdCustomer(), internal);

        if (sur.getCurrentStatus() != BaseConstants.STATUS_CANCEL && sur.getCurrentStatus() != BaseConstants.STATUS_COMPLETED){
            // Update SUR customer dan SUR organization Owner
            if (sur.getCustomer() == null || sur.getOrganizationOwner() == null) {
                sur.setCustomer(customer);
                sur.setOrganizationOwner((Organization) customer.getParty());
                salesUnitRequirementRepository.save(sur);
            }

            VehicleSalesOrder vso = orderUtils.getDraftVSO(BaseConstants.ORDER_TYPE_INTERNAL, internal, billTo, customer, sur);
            addSURToVSO(customer, vso, sur);

//            vso.setOrderNumber(sur.getRequirementNumber());
//            vso.setStatus(BaseConstants.STATUS_OPEN);
//            vso = vehicleSalesOrderRepository.save(vso);
            execution.setVariable("vso", vso);
        }
    }

    public void close(DelegateExecution execution) {
        VehicleCustomerRequest vcr = execution.getVariable("request", VehicleCustomerRequest.class);
        vcr.setStatus(BaseConstants.STATUS_COMPLETED);
        vehicleCustomerRequestRepository.save(vcr);
    }


}
