package id.atiila.service.impl;

import id.atiila.base.BaseConstants;
import id.atiila.domain.*;
import id.atiila.repository.BillingDisbursementRepository;
import id.atiila.repository.OrderBillingItemRepository;
import id.atiila.repository.OrderItemRepository;
import id.atiila.repository.PurchaseOrderRepository;
import id.atiila.service.InternalUtils;
import id.atiila.service.OrderUtils;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Set;

@Service
@Transactional
public class UnitDoImpl {

    private final Logger log = LoggerFactory.getLogger(UnitDoImpl.class);

    @Autowired
    private BillingDisbursementRepository billingDisbursementRepository;

    @Autowired
    private OrderUtils orderUtils;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private OrderBillingItemRepository orderBillingItemRepository;

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    public void approveBilling(DelegateExecution execution) {
        BillingDisbursement billing = execution.getVariable("billing", BillingDisbursement.class);

        // Rubah ke Open = 11
        billing.setStatus(BaseConstants.STATUS_OPEN);
        billing = billingDisbursementRepository.saveAndFlush(billing);

//        // Rubah ke Hold = 12
//        billing.setStatus(BaseConstants.STATUS_HOLD);
//        billing = billingDisbursementRepository.saveAndFlush(billing);
    }

    public void activateBilling(DelegateExecution execution) {
        BillingDisbursement billing = execution.getVariable("billing", BillingDisbursement.class);
    }

    public void buildPayment(DelegateExecution execution) {
        BillingDisbursement billing = execution.getVariable("billing", BillingDisbursement.class);
        billing.setStatus(BaseConstants.STATUS_COMPLETED);
        billingDisbursementRepository.save(billing);
    }

    public void buildPurchasePrice(DelegateExecution execution) {
        BillingDisbursement billing = execution.getVariable("billing", BillingDisbursement.class);
    }

    public void buildPO(DelegateExecution execution) {
        boolean parentCreated = false;

        PurchaseOrder po = null;

        BillingDisbursement billing = execution.getVariable("billing", BillingDisbursement.class);
        Set<BillingItem> items = billing.getDetails();

        for (BillingItem item: items) {

            // Check Jika item, dari orderItem
            if (item.getIdProduct() != null) {

                // Check Jika Belum di buatkan PO
                List<OrderBillingItem> orderItems = orderBillingItemRepository.findByIdBilling(item.getIdBillingItem());
                if (orderItems.isEmpty()) {
                    if (!parentCreated) {
                        po = buildNewPO(billing);
                        parentCreated = true;
                    }

                    OrderItem oi = buildOrderItem(po, billing, item);
                    po.addDetail(oi);

                    // Tambahkan Link
                    OrderBillingItem obi = new OrderBillingItem();
                    obi.setQty(oi.getQty());
                    obi.setAmount(oi.getTotalAmount());
                    obi.setOrderItem(oi);
                    obi.setBillingItem(item);

                    orderBillingItemRepository.save(obi);
                }
            }
        }

    }

    private PurchaseOrder buildNewPO(BillingDisbursement billing) {
        OrderType orderType = orderUtils.getOrderType(BaseConstants.ORDER_TYPE_PO_UNIT, "Order PO Unit");
        PurchaseOrder po = new PurchaseOrder();
        po.setVendor(billing.getVendor());
        po.setInternal(billing.getInternal());
        po.setBillTo(billing.getBillTo());
        po.setOrderType(orderType);
        po.setOrderNumber(billing.getVendorInvoice());
        po.setDateOrder(billing.getDateCreate());
        po.setStatus(BaseConstants.STATUS_ACTIVE);
        return purchaseOrderRepository.save(po);
    }


    private OrderItem buildOrderItem(PurchaseOrder po, BillingDisbursement billing, BillingItem billingItem) {
        OrderItem item = new OrderItem();

        item.setOrders(po);
        item.setUnitPrice(billingItem.getUnitPrice());
        item.setQty(billingItem.getQty());
        item.setIdProduct(billingItem.getIdProduct());
        item.setIdFeature(billingItem.getIdFeature());
        item.setDiscount(0);
        item.setItemDescription(billingItem.getItemDescription());


        if (internalUtils.getShipTo(billing.getInternal()) != null)
            item.setIdShipTo(internalUtils.getShipTo(billing.getInternal()).getIdShipTo());

        item.setTaxAmount(billingItem.getUnitPrice().doubleValue() * 10/100);
        item.setTotalAmount((item.getUnitPrice().doubleValue() + item.getTaxAmount().doubleValue()) * billingItem.getQty().doubleValue());

        return orderItemRepository.save(item);
    }

}
