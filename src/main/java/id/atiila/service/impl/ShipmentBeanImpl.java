package id.atiila.service.impl;

import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.service.*;
import id.atiila.service.dto.SLUploadDTO;
import org.apache.camel.Body;
import org.apache.camel.Headers;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class ShipmentBeanImpl {

    private final Logger log = LoggerFactory.getLogger(ShipmentBeanImpl.class);

    @Autowired
    protected MasterNumberingService numberingService;

    @Autowired
    private BillingDisbursementRepository billingDisbursementRepository;

    @Autowired
    private BillingItemRepository billingItemRepository;

    @Autowired
    private PackageReceiptRepository packageReceiptRepository;

    @Autowired
    private ShipmentReceiptRepository shipmentReceiptRepository;

    @Autowired
    private ShipmentIncomingRepository shipmentIncomingRepository;

    @Autowired
    private ShipmentItemRepository shipmentItemRepository;

    @Autowired
    private ShipmentBillingItemRepository shipmentBillingItemRepository;

    @Autowired
    private InventoryItemRepository inventoryItemRepository;

    @Autowired
    private FeatureRepository featureRepository;

    @Autowired
    private FeatureTypeRepository featureTypeRepository;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private ProductUtils productUtils;

    @Autowired
    private PriceUtils priceUtils;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private VendorUtils vendorUtils;

    @Autowired
    private StorageUtils storageUtils;

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private OrderBillingItemRepository orderBillingItemRepository;

    @Autowired
    private OrderShipmentItemRepository orderShipmentItemRepository;


    @Transactional(propagation = Propagation.REQUIRED)
    public PackageReceipt convertUploadShippingListToPackageReceipt(@Body Set<SLUploadDTO> r, @Headers Map<String, Object> header) {

        // Get billing dibursement by invoice number
        List<BillingDisbursement> ListBillingDisbursment = billingDisbursementRepository.findByVendorInvoice(r.iterator().next().getInvoiceNumber());
        BillingDisbursement b = ListBillingDisbursment.get(0);

        //Get internal
        Internal internal = internalUtils.findOne(partyUtils.getCurrentInternal().getIdInternal());

        log.debug("INI ORGANIZATION?" + internal.getOrganization());

        PackageReceipt p = new PackageReceipt();
        p.setDocumentNumber(r.iterator().next().getShippingListNumber());
        p.setVendorInvoice(r.iterator().next().getInvoiceNumber());
        p.setShipFrom(vendorUtils.getShipTo(b.getVendor()));
        p.setInternal(internal);
        p.setDateIssued(r.iterator().next().getDateCreate());
        log.debug("udah sampe internal=" + p);

        for (SLUploadDTO sl : r) {
            log.debug("sebelum product" + r);
            Product product = productUtils.getProduct(sl.getIdProduct());
            log.debug("sebelum unit price" + product);
            BigDecimal unitPrice = priceUtils.getSalesPrice(internal, sl.getIdProduct());
            log.debug("sebelum container" + unitPrice);
            Container container = storageUtils.buildContainer(internal.getPrimaryFacility(), StorageUtils.ROOM, "R-"+internal.getIdInternal());
            log.debug("se container good" + container);
            storageUtils.buildGoodContainer(internal.getOrganization(), (Good)product, container);
            log.debug("product di good===" + product);

            //get feature
            List<Feature> lf = featureRepository.findByRefKey(sl.getIdColor());
            Feature f;

            if (!lf.isEmpty()) {
                f = lf.get(0);
            }
            else {
                f = new Feature();
                f.setDescription("");

                //get featuretype
                FeatureType ft = featureTypeRepository.findOne(100);

                //get last id
                Feature featureLast = featureRepository.findTop1ByOrderByIdFeatureDesc();
                Integer newIdFeature = featureLast.getIdFeature() + 1;

                f.setIdFeature(newIdFeature);
                f.setFeatureType(ft);
                f.setDescription(sl.getIdColor());
                f.setRefKey(sl.getIdColor());

                f = featureRepository.save(f);
            }

            //Find Billing Item by Id Billing, Id Product, And Id Feature
            BillingItem bItem = billingItemRepository.findOneByBillingIdBillingAndIdProductAndIdFeature(b.getIdBilling(), sl.getIdProduct(), f.getIdFeature());

            //Find Order Billing Item
            if (bItem != null) {

                List<OrderBillingItem> obItems = orderBillingItemRepository.findByParams(null,
                    bItem.getIdBillingItem(),
                    new PageRequest(0,1)).getContent();

                OrderBillingItem obItem = obItems.isEmpty() ? null : obItems.get(0);

                // Create Unit Shipment Receipt
                UnitShipmentReceipt sr = new UnitShipmentReceipt();
                sr.setIdProduct(sl.getIdProduct());
                sr.setItemDescription(product.getName());
                sr.setIdFeature(f.getIdFeature());
                sr.setYearAssembly(sl.getYearAssembly());
                sr.setShipmentPackage(p);
                sr.setIdFrame(sl.getIdFrame());
                sr.setIdMachine(sl.getIdMachine());
                sr.setQtyAccept(1d);
                sr.setQtyReject(0d);
                sr.setOrderItem(obItem.getOrderItem());
                sr.setReceipt(false);
                sr.setUnitPrice(unitPrice);
                p.getShipmentReceipts().add(sr);
            }
        }

        p = packageReceiptRepository.save(p);

        return p;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildPackageReceipt(@Body PackageReceipt r, @Headers Map<String, Object> header) {
        r.setStatus(BaseConstants.STATUS_OPEN);
        packageReceiptRepository.save(r);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public PackageReceipt completePackageReceipt(@Body PackageReceipt r, @Headers Map<String, Object> header){

        //COMPLETED PACKAGE RECEIPT
        Boolean complete = checkCompletionOfPackageReceipt(r);
        log.debug("COMPLETE KAH INI?" +complete);

        if (complete){
            r.setStatus(BaseConstants.STATUS_COMPLETED);
            r = packageReceiptRepository.save(r);
            log.debug("harus complit ", r);
        }

        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public PackageReceipt buildShipmentIncoming(@Body PackageReceipt r, @Headers Map<String, Object> header) {

        ShipmentIncoming s = null;
        ShipTo sh = internalUtils.getShipTo(r.getInternal());
        log.debug("shipto nya == " + sh);
        List<BillingDisbursement> lb = billingDisbursementRepository.findByVendorInvoice(r.getVendorInvoice());
        BillingDisbursement b = lb.get(0);
        log.debug("billingDis==" + b);

        // Create Shipment Incoming
        if (s == null || !s.getShipFrom().equals(r.getShipFrom())) {
            log.debug("ssss == " + s);
           s = buildNewShipmentIncoming(r, sh);
        }

        for (ShipmentReceipt o: r.getShipmentReceipts()) {

            if (o instanceof UnitShipmentReceipt) {
                UnitShipmentReceipt usr = (UnitShipmentReceipt) o;
                if (usr.getQtyAccept() - usr.getQtyReject() > 0 && usr.getReceipt() == true && o.getShipmentItem() == null) {

                    //Create Shipment Item
                    ShipmentItem item = buildNewShipmentItem(s, usr);
                    s.getDetails().add(item);

                    //Find Billing Item by Id Billing, Id Product, And Id Feature
                    BillingItem bItem = billingItemRepository.findOneByBillingIdBillingAndIdProductAndIdFeature(b.getIdBilling(), o.getIdProduct(), o.getIdFeature());

                    //Create Shipment Billing Item
                    ShipmentBillingItem sbItem = buildNewShipmentBillingItem(bItem, item, usr);

                    //Find Order Billing Item
                    List<OrderBillingItem> obIteams = orderBillingItemRepository.findByParams(null,
                        bItem.getIdBillingItem(),
                        new PageRequest(0, 1)).getContent();

                    OrderBillingItem obIteam = obIteams.isEmpty() ? null : obIteams.get(0);

                    //Create Order Shipment Item
                    OrderShipmentItem osItem = buildNewOrderShipmentItem(obIteam.getOrderItem(), item, usr);

                    //buat Inventory Item
                    InventoryItem inviite = buildInventoryItem(r, (UnitShipmentReceipt) o);
                    o.setOrderItem(obIteam.getOrderItem());
                    o.setShipmentItem(item);
                    o.addInventoryItem(inviite);
                    log.debug("maskk kaga nihhh" + o);
                    shipmentReceiptRepository.save(o);
                    log.debug("save dong ahh" + o);
                }
            }
        }

        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public PackageReceipt buildPurchaseOrder(@Body PackageReceipt r, @Headers Map<String, Object> header) {
        PurchaseOrder s = null;
        log.debug("hasil s" + s);

        Vendor v = vendorUtils.getVendor(r.getShipFrom().getParty());
        ShipTo sh = internalUtils.getShipTo(r.getInternal());
        BillTo bt = internalUtils.getBillTo(r.getInternal());

        if (s == null || !s.getVendor().getOrganization().equals(r.getShipFrom().getParty())) {
            s = new PurchaseOrder();
            s.setVendor(v);
            s.setInternal(r.getInternal());
            s.setBillTo(bt);
            s.dateEntry(ZonedDateTime.now());
            s.dateOrder(ZonedDateTime.now());
            s = purchaseOrderRepository.save(s);
        }

        for (ShipmentReceipt o: r.getShipmentReceipts()) {

            if (o.getQtyAccept() - o.getQtyReject() > 0) {
                UnitShipmentReceipt d = (UnitShipmentReceipt) o;
                OrderItem item = new OrderItem();
                item.setOrders(s);
                item.setIdProduct(o.getIdProduct());
                item.setItemDescription(o.getItemDescription());
                item.setQty(o.getQtyAccept() - o.getQtyReject());
                item.setUnitPrice(d.getUnitPrice());
                item.setTotalAmount(BigDecimal.valueOf(d.getUnitPrice().doubleValue() * (o.getQtyAccept() - o.getQtyReject())));
                item.setIdShipTo(sh.getIdShipTo());
                s.addDetail(item);
                orderItemRepository.save(item);
                log.debug("save orderitem" + item);

                o.setOrderItem(item);
            }
        }

        r = packageReceiptRepository.save(r);
        return r;
    }

    @Transactional
    public PackageReceipt buildPurchaseInvoice(@Body PackageReceipt r, @Headers Map<String, Object> header) {
        return r;
    }

    @Transactional
    public ShipmentItem buildShipmentItem(@Body ShipmentItem item, @Headers Map<String, Object> headers) {
        ShipmentItem rs = null;
        return rs;
    }

    @Transactional
    public InventoryItem buildInventoryItem(PackageReceipt p, UnitShipmentReceipt r) {
        if (r.getInventoryItems().size() > 0) return r.getInventoryItems().iterator().next();

        InventoryItem item = new InventoryItem();
        item.assignFrom(r);
        item.dateCreated(ZonedDateTime.now());
        item.setShipmentReceipt(r);

        GoodContainer container = storageUtils.getGoodContainer(p.getInternal().getOrganization(), r.getIdProduct());
        item.setIdContainer(container != null ? container.getContainer().getIdContainer() : null);

        // Find Good Container / Facility
        if (item.getIdContainer() == null) {
            Facility facl = p.getInternal().getPrimaryFacility();
            if (facl != null) item.setIdFacility(facl.getIdFacility());
        }

        return inventoryItemRepository.save(item);
    }

    @Transactional
    public ShipmentIncoming buildNewShipmentIncoming(PackageReceipt r, ShipTo sh){
        ShipmentIncoming s = new ShipmentIncoming();
        s.setShipFrom(r.getShipFrom());
        log.debug("1 ===" + s.getShipFrom());
        s.setShipTo(sh);
        log.debug("2 ===" + sh);
        s.setInternal(r.getInternal());
        log.debug("3 ===" + r.getInternal());
        s.setDateSchedulle(ZonedDateTime.now());
        log.debug("3 ====");
        s.setAddressFrom(r.getShipFrom().getParty().getPostalAddress());
        log.debug("4 ===" + s.getAddressFrom());
        s.setAddressTo(r.getInternal().getOrganization().getPostalAddress());
        log.debug("5 ===" + s.getAddressTo());
        if (r.getDocumentNumber() != null) s.setDescription("SPG " + r.getDocumentNumber());
        log.debug("6 ===" + s.getDescription());
        s.setShipmentNumber(numberingService.buildSPGNumber(r.getInternal()));
        log.debug("7 ===" + s.getShipmentNumber());
        s.setStatus(BaseConstants.STATUS_COMPLETED);
        log.debug("8 ===" + s.getStatus());
        return shipmentIncomingRepository.save(s);

    }

    @Transactional
    public ShipmentItem buildNewShipmentItem(ShipmentIncoming s, ShipmentReceipt sReceipt){
        ShipmentItem item = new ShipmentItem();
        item.setShipment(s);
        item.setIdProduct(sReceipt.getIdProduct());
        item.setItemDescription(sReceipt.getItemDescription());
        item.setIdFeature(sReceipt.getIdFeature());
        item.setContentDescription(sReceipt.getItemDescription());
        item.setQty(sReceipt.getQtyAccept() - sReceipt.getQtyReject());
        return shipmentItemRepository.save(item);
    }

    @Transactional
    public ShipmentBillingItem buildNewShipmentBillingItem(BillingItem bItem, ShipmentItem sItem, ShipmentReceipt sReceipt){
        ShipmentBillingItem sbItem = new ShipmentBillingItem();
        sbItem.setBillingItem(bItem);
        sbItem.setShipmentItem(sItem);
        sbItem.setQty(sReceipt.getQtyAccept() - sReceipt.getQtyReject());
        return shipmentBillingItemRepository.save(sbItem);
    }

    @Transactional
    public OrderShipmentItem buildNewOrderShipmentItem(OrderItem oItem, ShipmentItem sItem, ShipmentReceipt sReceipt){
        OrderShipmentItem osItem = new OrderShipmentItem();
        osItem.setOrderItem(oItem);
        osItem.setShipmentItem(sItem);
        osItem.setQty(sReceipt.getQtyAccept() - sReceipt.getQtyReject());
        return orderShipmentItemRepository.save(osItem);
    }

    @Transactional
    public Boolean checkCompletionOfPurchaseOrder(PackageReceipt r, BillingDisbursement b){

        Double qtyReceipt, qtyFilled, qtyAccept, qtyReject;

        if (!r.getShipmentReceipts().isEmpty()){
            //Find Billing Item by Id Billing, Id Product, And Id Feature
            BillingItem bItem = billingItemRepository.findOneByBillingIdBillingAndIdProductAndIdFeature(
                b.getIdBilling(), r.getShipmentReceipts().get(0).getIdProduct(), r.getShipmentReceipts().get(0).getIdFeature());

            if (bItem == null) {
                log.debug("BILLING ITEM NOT FOUND!");
                return false;
            }
            //Find Order Billing Item
            OrderBillingItem obIteam = orderBillingItemRepository.findByParams(null,
                bItem.getIdBillingItem(),
                new PageRequest(0,1)).getContent().get(0);

            if (obIteam == null) {
                log.debug("ORDER BILLING ITEM NOT FOUND!");
                return false;
            }

            PurchaseOrder po = purchaseOrderRepository.findOne(obIteam.getOrderItem().getOrders().getIdOrder());

            if (po == null){
                log.debug("PURCHASE ORDER NOT FOUND!");
                return false;
            }

            for(OrderItem orderItem : po.getDetails()){

                qtyAccept = shipmentReceiptRepository.qtyAcceptByOrderItem(orderItem.getIdOrderItem());
                qtyReject = shipmentReceiptRepository.qtyRejectByOrderItem(orderItem.getIdOrderItem());
                qtyReceipt = (qtyAccept == null ? 0.0 : qtyAccept) - (qtyReject == null ? 0.0 : qtyReject);
                qtyFilled = orderShipmentItemRepository.qtyFilledByOrderItem(orderItem.getIdOrderItem());

                log.debug("QTY RECEIPT" + qtyReceipt);
                log.debug("QTY FILLED" + qtyFilled);

                if (qtyFilled == null || qtyFilled.equals(0.0)){
                    return false;
                } else if (qtyReceipt.equals(0.0)){
                    return false;
                } else if ( !qtyReceipt.equals(qtyFilled)) {
                    return false;
                }


//                if (!orderItem.getQtyFilled().equals(orderItem.getQtyReceipt()) || orderItem.getQtyFilled().equals(0.0)){
//                    return false;
//                }
            }
        }

        return true;
    }

    @Transactional
    public Boolean checkCompletionOfPackageReceipt (PackageReceipt r){

        Double qtyReceipt, qtyFilled, qtyAccept, qtyReject;

        if (!r.getShipmentReceipts().isEmpty()){
            //Find Billing Item by Id Billing, Id Product, And Id Feature

            for(ShipmentReceipt shipmentReceipt : r.getShipmentReceipts()) {
                if (shipmentReceipt.getOrderItem() != null) {
                    qtyAccept = shipmentReceiptRepository.qtyAcceptByOrderItem(shipmentReceipt.getOrderItem().getIdOrderItem());
                    qtyReject = shipmentReceiptRepository.qtyRejectByOrderItem(shipmentReceipt.getOrderItem().getIdOrderItem());
                    qtyReceipt = (qtyAccept == null ? 0.0 : qtyAccept) - (qtyReject == null ? 0.0 : qtyReject);
                    qtyFilled = orderShipmentItemRepository.qtyFilledByOrderItem(shipmentReceipt.getOrderItem().getIdOrderItem());

                    log.debug("QTY RECEIPT" + qtyReceipt);
                    log.debug("QTY FILLED" + qtyFilled);

                    if (qtyFilled == null || qtyFilled.equals(0.0)) {
                        return false;
                    } else if (qtyReceipt.equals(0.0)) {
                        return false;
                    } else if (!qtyReceipt.equals(qtyFilled)) {
                        return false;
                    } else if (shipmentReceipt.getInventoryItems().isEmpty()) {
                        return false;
                    }


//                if (!orderItem.getQtyFilled().equals(orderItem.getQtyReceipt()) || orderItem.getQtyFilled().equals(0.0)){
//                    return false;
//                }
                } else {
                    return false;
                }
            }
        }

        return true;
    }

}
