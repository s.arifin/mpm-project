package id.atiila.service.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IAtomicLong;
import com.hazelcast.core.ILock;
import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.config.tenant.TenantContext;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.MasterNumberingSearchRepository;
import id.atiila.service.*;
import id.atiila.service.dto.OrderItemDTO;
import id.atiila.service.mapper.MasterNumberingMapper;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.delegate.DelegateExecution;
import org.apache.camel.ProducerTemplate;
import org.elasticsearch.common.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class VsoItemImpl {

    private final Logger log = LoggerFactory.getLogger(VsoItemImpl.class);

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private RequirementOrderItemRepository requirementOrderItemRepository;

    @Autowired
    private InventoryItemRepository inventoryItemRepository;

    @Autowired
    private ShipToRepository shipToRepository;

    @Autowired
    private PickingSlipRepository pickingSlipRepository;

    @Autowired
    private ShipmentOutgoingRepository shipmentOutgoingRepository;

    @Autowired
    private ShipmentItemRepository shipmentItemRepository;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private CustomerUtils customerUtils;

    @Autowired
    private VehicleSalesBillingRepository vehicleSalesBillingRepository;

    @Autowired
    private MasterNumberingService numbering;

    @Autowired
    private BillingItemRepository billingItemRepository;

    @Autowired
    private BillingItemTypeRepository billingItemTypeRepository;

    @Autowired
    private OrderUtils orderUtils;

    @Autowired
    private ShipmentUtils shipmentUtils;

    @Autowired
    private VehicleSalesOrderRepository vehicleSalesOrderRepository;

    @Autowired
    private VehicleDocumentRequirementRepository vehicleDocumentRequirementRepository;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private SalesUnitRequirementRepository salesUnitRequirementRepository;

    @Autowired
    private UnitDeliverableRepository unitDeliverableRepository;

    @Autowired
    private VehicleIdentificationRepository vehicleIdentificationRepository;

    @Autowired
    private BillingTypeRepository billingTypeRepository;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private RequirementUtils requirementUtils;

    @Autowired
    private ReceiptRepository receiptRepository;

    @Autowired
    private PersonalCustomerRepository personalCustomerRepository;

    @Autowired
    private PaymentUtils paymentUtils;

    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private CommunicationEventCDBRepository communicationEventCDBRepository;

    @Autowired
    private ProspectRepository prospectRepository;

    @Autowired
    private SalesBookingRepository salesBookingRepository;

    @Autowired
    private SalesBookingService salesBookingService;

    @Autowired
    private MasterNumberingRepository masterNumberingRepository;

    @Autowired
    private InternalRepository<Internal> internalRepository;

    @Autowired
    private UserMediatorRepository userMediatorRepository;

    @Autowired
    private HazelcastInstance hz;

    @Autowired
    private ApprovalRepository approvalRepository;

    @Autowired
    private SalesUnitRequirementService salesUnitRequirementService;

    @Autowired
    private CustomerService customerService;

    @Transactional(propagation = Propagation.REQUIRED)
    public InventoryItem findUnit(String idInternal, String idProduct, Integer feature) {
        Page<InventoryItem> invs = inventoryItemRepository.queryByOrderItemAndInternal(idInternal, idProduct, feature, new PageRequest(0,1));
        if (invs.getContent().isEmpty()) return null;
        return invs.getContent().iterator().next();
    }

    @Transactional
    protected MasterNumbering getNumbering(String idTag, String idValue, Long startFrom) {
        MasterNumbering r = masterNumberingRepository.findOneByIdTagAndIdValue(idTag, idValue);
        if (r == null) {
            r = new MasterNumbering();
            r.setIdTag(idTag);
            r.setIdValue(idValue);
            r.setNextValue(startFrom);
            r.setNextValue(0l);
            r = masterNumberingRepository.saveAndFlush(r);
        }
        return r;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Long nextValue(String idTag, String idValue) {
        return nextValue(idTag, idValue, 0l);
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public Long nextValue(String idTag, String idValue, Long startFrom) {
        long result = 0l;
        IAtomicLong number = null;
        ILock lock = hz.getLock(idTag + "-" + idValue);
        lock.lock();
        try {
            number = hz.getAtomicLong(TenantContext.getCurrentTenant() + "numbering" + idTag + "-" + idValue);
            if (number.get() == 0) {
                MasterNumbering numb = getNumbering(idTag, idValue, startFrom);
                number.set(numb.getNextValue());
            }
            result = number.incrementAndGet();
            MasterNumbering numb = getNumbering(idTag, idValue, startFrom);
            numb.setNextValue(result);
            masterNumberingRepository.save(numb);
        } finally {
            lock.unlock();
            number.set(0l);

        }
        return result;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public String findTransNumberVSBBy(String id, String idtrans){
        log.debug("master numbering system: findTransNumberVSBBy" + id.toString() + "==>trans==>" + idtrans);
        Internal internal = internalRepository.findOne(id);
        if (internal == null){
            log.debug("Internal null " );
            return null;
        }
        // String r = internal.getIdInternal() + "-IVU-" + LocalDateTime.now().getYear() + "-";
        String r = internal.getRoot().getIdInternal() + "-" + idtrans + "-" + LocalDateTime.now().getYear() + "-" + "DMS";
        r = r + Strings.padStart(nextValue(idtrans, r) .toString(), 8, '0');
        log.debug("hasil ==>" + r );
        return r;
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public void prepare(DelegateExecution execution) {
        log.debug("belok kanan prepare");
        VehicleSalesOrder vso = execution.getVariable("vso", VehicleSalesOrder.class);
        OrderItem o = execution.getVariable("orderItem", OrderItem.class);
        BillTo billTo = execution.getVariable("billTo", BillTo.class);
        SalesUnitRequirement sur = execution.getVariable("sur", SalesUnitRequirement.class);

        // set Internal
        Internal internal = execution.getVariable("internal", Internal.class);
        if (internal == null) {
            execution.setVariable("internal", vso.getInternal());
        }
        // Cek Bill To
        if (billTo == null) {
            execution.setVariable("billTo", vso.getBillTo());
            if (vso.getBillTo() == null) {
                execution.setVariable("billTo", customerUtils.getBillTo(vso.getCustomer()));
            }
        }

        // Cek Bill From
        BillTo billFrom = execution.getVariable("billFrom", BillTo.class);
        if (billFrom == null) {
            execution.setVariable("billFrom", internalUtils.getBillTo(vso.getInternal()));
        }

        // Jika Sur Tidak di assign, cari sur dari order item.
        if (sur == null) {
            //
            RequirementOrderItem roi = requirementOrderItemRepository.findRequirementOrderItemByOrder(o);
            if (roi == null) {
                sur = salesUnitRequirementRepository.findOne(vso.getIdSalesUnitRequirement());
                if (sur == null) throw new ActivitiException("Sales unit requirement tidak terdefinisi !");
                roi = orderUtils.getRequirementOrderItemByOrder(sur, o, o.getQty().intValue());
            };
            sur = (SalesUnitRequirement) roi.getRequirement();
            execution.setVariable("sur", sur);
        }

        // Simpan Customer untuk di pakai di area lain
        Customer customer = execution.getVariable("customer", Customer.class);
        if (customer == null) {
            execution.setVariable("customer", vso.getCustomer());
        }

        ShipTo shipTo = execution.getVariable("shipTo", ShipTo.class);
        if (shipTo == null) {
            execution.setVariable("shipTo", shipToRepository.findOne(o.getIdShipTo()));
        }

        execution.setVariable("orderType", vso.getOrderType().getIdOrderType());
        execution.setVariable("saleType", vso.getIdSaleType());
        execution.setVariable("idFrame", sur.getRequestIdFrame());
        execution.setVariable("idMachine", sur.getRequestIdMachine());
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildPicking(DelegateExecution execution) {
        log.debug("belok kanan buildPicking");
        String idFrame = execution.getVariable("idFrame", String.class);
        String idMachine = execution.getVariable("idMachine", String.class);
        Internal internal = execution.getVariable("internal", Internal.class);
        ShipTo shipTo = execution.getVariable("shipTo", ShipTo.class);
        Integer orderType = execution.getVariable("orderType", Integer.class);
        VehicleSalesOrder vso = execution.getVariable("vso", VehicleSalesOrder.class);
        OrderItem o = execution.getVariable("orderItem", OrderItem.class);
        InventoryItem inv = execution.getVariable("inventoryItem", InventoryItem.class);
        PickingSlip pis = execution.getVariable("pis", PickingSlip.class);
        SalesUnitRequirement sur = execution.getVariable("sur", SalesUnitRequirement.class);

        // Jika sudah di penuhi keluar
        double totalPicking = 0d;
        List<PickingSlip> orderPis = orderUtils.getPickingSlip(o);
        if (!orderPis.isEmpty()) totalPicking = orderPis.stream().mapToDouble(d -> d.getQty()).sum();

        // Jika Qty Picking > 0, data ini sudah pernah di buatkan picking, batalkan semua pickingnya
        if (totalPicking >= o.getQty()) {
            for (PickingSlip pickSlip: orderPis) {
                // Batalkan Picking
                pickSlip.setStatus(BaseConstants.STATUS_CANCEL);
                pickingSlipRepository.saveAndFlush(pickSlip);

                // Kembalikan Booking Stock
                inv = pickSlip.getInventoryItem();
                inv.setQtyBooking(inv.getQtyBooking() - 1);
                inventoryItemRepository.saveAndFlush(inv);
            }
        }

        // Cari Inventory Item
        if (idFrame != null || idMachine != null) {
            inv = inventoryItemRepository.queryByFrameOrMachine(o.getIdProduct(), idFrame, idMachine);
            // Tidak ada stock yang tersedia
            if (inv == null) {
                    throw new DmsException("Stock Motor Tidak Tersedia.. !");
                }
        }


        pis = new PickingSlip();
        pis.setInventoryItem(inv);
        pis.setOrderItem(o);
        pis.setQty(1);
        pis.setShipTo(shipTo);
        pis.setIdInventoryMovementType(orderType);
        pis.setRefferenceNumber(sur.getRequirementNumber());
        pis = pickingSlipRepository.saveAndFlush(pis);

        inv.setQtyBooking(inv.getQtyBooking() + 1);
        inv = inventoryItemRepository.saveAndFlush(inv);

        execution.setVariable("picking", pis);
        execution.setVariable("inventoryItem", inv);
        execution.setVariable("pickingCreated", true);
        execution.setVariable("basePrice", 0D);

        // Bila ada harga beli, update dengan harga beli
        if (inv.getShipmentReceipt() != null && inv.getShipmentReceipt().getOrderItem() != null) {
            execution.setVariable("basePrice", inv.getShipmentReceipt().getOrderItem().getUnitPrice());
        }

        // Send To Process Picking
        Map<String, Object> vars = new HashMap<>();
        vars.putAll(execution.getVariables());
        vars.put("bussinesKey", pis.getIdSlip().toString());
        producerTemplate.sendBodyAndHeaders("direct:activiti-process", "picking-unit", vars);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildDO(DelegateExecution execution) {
        log.debug("belok kanan build DO");
        Internal internal = execution.getVariable("internal", Internal.class);
        PickingSlip pis = execution.getVariable("picking", PickingSlip.class);
        OrderItem oi = execution.getVariable("orderItem", OrderItem.class);

        ShipTo shipFrom = internalUtils.getShipTo(internal);
        ShipTo shipTo = pis.getShipTo();

        // satu DO satu Unit
        ShipmentOutgoing shipment = null;

        // Satu DO maksimum 4 unit
//        Page<ShipmentOutgoing> shipments = shipmentOutgoingRepository.getDraftShipment(internal, shipFrom, shipTo, new PageRequest(0,1));
//        if (!shipments.getContent().isEmpty() && shipments.getContent().size() < 4)
//            shipment = shipments.getContent().get(0);

        if (shipment == null) {
            shipment = new ShipmentOutgoing();
            shipment.setShipFrom(shipFrom);
            shipment.setShipTo(shipTo);
            shipment.setInternal(internal);
            shipment.setAddressFrom(shipFrom.getParty().getPostalAddress());
            shipment.setAddressTo(shipTo.getParty().getPostalAddress());
            shipment = shipmentOutgoingRepository.saveAndFlush(shipment);
            execution.setVariable("shipment", shipment);

             Map<String, Object> vars = new HashMap<>();
             vars.putAll(execution.getVariables());
             vars.put("bussinesKey", shipment.getIdShipment().toString());
             producerTemplate.sendBodyAndHeaders("direct:activiti-process", "do-sales-unit", vars);
        }

        //Buat Shipment Item
        ShipmentItem shi = shipmentUtils.addItem(shipment, oi, pis.getQty());
        // Fixed Bug Shipment Item Billing yang null
        shi = shipmentItemRepository.saveAndFlush(shi);
        execution.setVariable("shipmentItem", shi);

        shipment.addDetail(shi);
        shipment = shipmentOutgoingRepository.saveAndFlush(shipment);

        //Buat Item Issuance
        ItemIssuance issuance = shipmentUtils.buildIssuance(shi, pis);

        orderUtils.getOrderShipmentItem(oi, shi, shi.getQty());

        execution.setVariable("shipment", shipment);
        execution.setVariable("itemIssuance", issuance);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildIVU(DelegateExecution execution) {
        log.debug("belok kanan build IVU");
        Internal internal = execution.getVariable("internal", Internal.class);
        Customer customer = execution.getVariable("customer", Customer.class);
        OrderItem oi = execution.getVariable("orderItem", OrderItem.class);
        ShipmentItem si = execution.getVariable("shipmentItem", ShipmentItem.class);
        Integer saleType = execution.getVariable("saleType", Integer.class);
        InventoryItem inv = execution.getVariable("inventoryItem", InventoryItem.class);
        SalesUnitRequirement sur = execution.getVariable("sur", SalesUnitRequirement.class);
        BigDecimal basePrice = execution.getVariable("basePrice", BigDecimal.class);
        Integer orderType = execution.getVariable("orderType", Integer.class);
        VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(oi.getOrders().getIdOrder());
        BillingItemType itemType = null;

        BillTo billFrom = internalUtils.getBillTo(internal);
        BillTo billTo = customerUtils.getBillTo(customer);

        VehicleSalesBilling billing = null;
        List<VehicleSalesOrder> vsoexists = vehicleSalesOrderRepository.vsoExistBilling(oi.getOrders().getIdOrder());
        if (vso.getCurrentStatus().intValue() == BaseConstants.STATUS_COMPLETED ||
            vso.getCurrentStatus().intValue() == BaseConstants.STATUS_VSO_WAITING_ADMIN_HANDLING ||
            !vsoexists.isEmpty()) {
            throw new DmsException("Data VSO sudah di Create IVU");
        }
        if ( vso.getOrderType().getIdOrderType() != BaseConstants.ORDER_TYPE_VSO) {
            String refKey = sur.getIdRequest().toString();
            Page<VehicleSalesBilling> billings = vehicleSalesBillingRepository.getDraftBilling(internal, billFrom, billTo, customer, refKey, new PageRequest(0,1));
            if (!billings.getContent().isEmpty()) {
                billing = billings.getContent().get(0);
            }
        }

        // Buat Billing Baru
        if (billing == null) {
            String billnumber = findTransNumberVSBBy(internal.getIdInternal(), "IVU");
            System.out.println("ini nomor ivu yang di create = " + billnumber);

            billing = new VehicleSalesBilling();
            billing.setBillingNumber(billnumber);
            billing.setDescription(sur.getRequirementNumber());
            billing.setInternal(internal);
            billing.setBillTo(billTo);
            billing.setBillFrom(billFrom);
            billing.setIdSaleType(saleType);
            billing.setCustomer(customer);
            billing.setRequestTaxInvoice(0);
            billing.setBillingType(new BillingType(BaseConstants.BILLING_TYPE_VEHICLE_SALES));
            // Jika bukan retail
            if ( vso.getOrderType().getIdOrderType() != BaseConstants.ORDER_TYPE_VSO) {
                billing.setRefKey(sur.getIdRequest().toString());
            }
            billing = vehicleSalesBillingRepository.saveAndFlush(billing);
            execution.setVariable("billing", billing);

            Map<String, Object> vars = new HashMap<>();
            vars.putAll(execution.getVariables());
            vars.put("bussinesKey", billing.getIdBilling().toString());
            producerTemplate.sendBodyAndHeaders("direct:activiti-process", "billing-sales-unit", vars);
        }

        // Tambahkan Billing Item
        BillingItem bi = billing.addMotorCycle(oi);
        bi.setInventoryItem(inv);
        bi.setBasePrice(basePrice);
        bi = billingItemRepository.saveAndFlush(bi);

        itemType = billingItemTypeRepository.findOne(BaseConstants.ITEM_TYPE_PPN);
        BillingItem bitax = billing.addItem(itemType.getIdItemType(), itemType.getDescription(), oi.getTaxAmount());
        if (bitax != null) billingItemRepository.saveAndFlush(bitax);

        itemType = billingItemTypeRepository.findOne(BaseConstants.ITEM_TYPE_BBN_PRICE);
        BillingItem bibbn = billing.addItem(itemType.getIdItemType(), itemType.getDescription(), sur.getBbnprice());
        if (bibbn != null) billingItemRepository.saveAndFlush(bibbn);

        // Subsidi AHM
        itemType = billingItemTypeRepository.findOne(BaseConstants.ITEM_TYPE_SUBSIDI_AHM);
        BillingItem bisahm = billing.addItem(itemType.getIdItemType(), itemType.getDescription(), sur.getSubsahm());
        if (bisahm != null) billingItemRepository.saveAndFlush(bisahm);

        itemType = billingItemTypeRepository.findOne(BaseConstants.ITEM_TYPE_SUBSIDI_MD);
        BillingItem bismd = billing.addItem(itemType.getIdItemType(), itemType.getDescription(), sur.getSubsmd());
        if (bismd != null) billingItemRepository.saveAndFlush(bismd);

        itemType = billingItemTypeRepository.findOne(BaseConstants.ITEM_TYPE_SUBSIDI_FIN_CO);
        BillingItem bisfinco = billing.addItem(itemType.getIdItemType(), itemType.getDescription(), sur.getSubsfincomp());
        if (bisfinco != null) billingItemRepository.saveAndFlush(bisfinco);

        itemType = billingItemTypeRepository.findOne(BaseConstants.ITEM_TYPE_SUBSIDI_DEALER);
        BillingItem bisdlr = billing.addItem(itemType.getIdItemType(), itemType.getDescription(), sur.getSubsown());
        if (bisdlr != null) billingItemRepository.saveAndFlush(bisdlr);

        Double disc = 0d;
        BillingType bilType = null;

        if (vso != null) {
            log.debug("ceklist dp fincoooooo == " + vso.getSubFincoyAsDp());
            if ( vso.getSubFincoyAsDp() != null && vso.getSubFincoyAsDp().equals(true)) {
                disc = sur.getSubsahm().doubleValue() + sur.getSubsmd().doubleValue() + sur.getSubsown().doubleValue();
            } else {
                disc = sur.getSubsahm().doubleValue() + sur.getSubsfincomp().doubleValue() + sur.getSubsmd().doubleValue() + sur.getSubsown().doubleValue();
            }
            if (orderType.equals(BaseConstants.ORDER_TYPE_GC)) {
                bilType = billingTypeRepository.findOne(BaseConstants.BILLING_TYPE_VEHICLE_GC);
                billing.setBillingType(bilType);
            }

            if (orderType.equals(BaseConstants.ORDER_TYPE_GP)) {
                bilType = billingTypeRepository.findOne(BaseConstants.BILLING_TYPE_VEHICLE_GP);
                billing.setBillingType(bilType);
            }

            if (orderType.equals(BaseConstants.ORDER_TYPE_INTERNAL)) {
                bilType = billingTypeRepository.findOne(BaseConstants.BILLING_TYPE_VEHICLE_INTERNAL);
                billing.setBillingType(bilType);
            }

            if (orderType.equals(BaseConstants.ORDER_TYPE_REKANAN)) {
                bilType = billingTypeRepository.findOne(BaseConstants.BILLING_TYPE_VEHICLE_INTERNAL);
                billing.setBillingType(bilType);
            }
        }

        billing.setAxunitprice(oi.getUnitPrice());
        billing.setAxbbn(sur.getBbnprice());
        billing.setAxsalesamount(sur.getUnitPrice());
        billing.setAxdisc(BigDecimal.valueOf(disc));
        billing.setAxprice(BigDecimal.valueOf(sur.getUnitPrice().doubleValue() - disc));
        billing.setAxppn(BigDecimal.valueOf(sur.getHetprice().doubleValue() - sur.getUnitPrice().doubleValue()));
        billing.setAxaramt(BigDecimal.valueOf(sur.getHetprice().doubleValue() + sur.getBbnprice().doubleValue() - disc));
        billing = vehicleSalesBillingRepository.saveAndFlush(billing);

        // Set Link Order Billing
        orderUtils.getOrderBillingItem(oi, bi, bi.getQty(), bi.getTotalAmount());

        // Set Link Shipment Billing
        shipmentUtils.getShipmentBillingItem(si, bi, bi.getQty());

        // set Reff Number, cukup begini saja, proses per orderitem = per picking
        if (orderType.equals(BaseConstants.ORDER_TYPE_VSO)) {
            PickingSlip pis = execution.getVariable("picking", PickingSlip.class);
            if (pis != null) {
                pis.setRefferenceNumber(billing.getBillingNumber());
                pickingSlipRepository.saveAndFlush(pis);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void buildDocumentRegistration(DelegateExecution execution) {
        log.debug("belok kanan buildDocumentRegistration");
        OrderItem o = execution.getVariable("orderItem", OrderItem.class);
        SalesUnitRequirement sur = execution.getVariable("sur", SalesUnitRequirement.class);
        VehicleSalesOrder vso = execution.getVariable("vso", VehicleSalesOrder.class);
        InventoryItem inv = execution.getVariable("inventoryItem", InventoryItem.class);
        Internal internal = execution.getVariable("internal", Internal.class);
        VehicleSalesBilling billing = execution.getVariable("billing", VehicleSalesBilling.class);
        CommunicationEventCDB cdb = execution.getVariable("cdb", CommunicationEventCDB.class);
        Customer customer = execution.getVariable("customer", Customer.class);

        if (cdb == null) {
            List<CommunicationEventCDB> items = communicationEventCDBRepository.queryFindCommunicationEventCDBByIdCustomer(customer.getIdCustomer());
            if (items.isEmpty()) {
                // Buat CDB Baru untuk customer
                cdb = new CommunicationEventCDB();
                cdb.setIdCustomer(customer.getIdCustomer());
                cdb.setIdSalesUnitRequirement(sur.getIdRequirement());
                cdb = communicationEventCDBRepository.saveAndFlush(cdb);
            } else {
                cdb = items.get(0);
            }
            execution.setVariable("cdb", cdb);
//            throw new DmsException("stop dsini");
        }

        SaleType saleType = vso.getSaleType();
        ShipTo shipTo = shipToRepository.findOne(o.getIdShipTo());
        BillTo billTo = vso.getBillTo();

        VehicleDocumentRequirement vd = vehicleDocumentRequirementRepository.findByOrderItem(o);
        if (vd == null) {

            Vehicle vh = new Vehicle();
            vh.setIdFrame(inv.getIdFrame());
            vh.setIdMachine(inv.getIdMachine());
            vh.setYearOfAssembly(inv.getYearAssembly());
            vh.setIdColor(inv.getIdFeature());
            vh.setIdProduct(o.getIdProduct());
            vh.setInternal(internal);
            vh = vehicleRepository.saveAndFlush(vh);

            vd = new VehicleDocumentRequirement();
            vd.setOrderItem(o);
            vd.setIdSalesRequirement(sur.getIdRequirement());

            // vd.setWaitStnk(sur.getWaitStnk());
            // semua VDR masuk ke status isi nama tidak valid, jadi di perlukan verifikasi manual

            vd.setWaitStnk(true);
            vd.setInternal(internal);
            vd.setBillTo(billTo);
            vd.setShipTo(shipTo);
            vd.setPersonOwner(sur.getPersonOwner());
            vd.setOrganizationOwner(sur.getOrganizationOwner());
            vd.setVehicle(vh);
            vd.setSaleType(saleType);
            // vd.setRequestPoliceNumber(sur.getRequestpoliceid());
            vd.setNote(sur.getNote());
            vd.setBbn(sur.getBbnprice());

            // untuk mendeteksi apakah dari internal atau external
            vd.setRequirementType(requirementUtils.getRequirementType(BaseConstants.REQ_TYPE_VDR_RETAIL, "VDR Detail"));
            if (billing != null) {
                vd.setRequirementNumber(billing.getBillingNumber());
            }

            vd = vehicleDocumentRequirementRepository.saveAndFlush(vd);

            VehicleIdentification vi = new VehicleIdentification();
            vi.setVehicle(vh);
            vi.setCustomer(sur.getCustomer());
            vi = vehicleIdentificationRepository.saveAndFlush(vi);

            int[] types = { BaseConstants.DELIVERABLE_TYPE_NOTICE, BaseConstants.DELIVERABLE_TYPE_STNK,
                            BaseConstants.DELIVERABLE_TYPE_NOPOL, BaseConstants.DELIVERABLE_TYPE_BPKB};
            for (int docType: types ) {
                UnitDeliverable deliverable = new UnitDeliverable();
                deliverable.setVehicleDocumentRequirement(vd);
                deliverable.setIdDeliverableType(docType);
                deliverable = unitDeliverableRepository.saveAndFlush(deliverable);
            }

            execution.setVariable("vdr", vd);
            execution.setVariable("vehicle", vi);

            Map<String, Object> vars = new HashMap<>();
            vars.putAll(execution.getVariables());
            vars.put("bussinesKey", vd.getIdRequirement().toString());
            producerTemplate.sendBodyAndHeaders("direct:activiti-process", "registrasi", vars);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updateFinalVSO(DelegateExecution execution) {
        log.debug("belok kanan updateFinalVSO");
        VehicleSalesOrder vso = execution.getVariable("vso", VehicleSalesOrder.class);
        Integer orderType = execution.getVariable("orderType", Integer.class);

        boolean completed = true;

        // Untuk retail, langsung completed
        if (!orderType.equals(BaseConstants.ORDER_TYPE_VSO)) {
            for (OrderItem o : vso.getDetails()) {
                double totalPicking = 0d;

                // Jika Picking ada
                List<PickingSlip> pis = orderUtils.getPickingSlip(o);
                if (!pis.isEmpty())
                    totalPicking = orderUtils.getPickingSlip(o).stream().mapToDouble(d -> d.getQty()).sum();

                log.debug("order item qty vs picking= " + o.getQty() + " - " + totalPicking);
                if (o.getQty() > totalPicking) {
                    completed = false;
                    break;
                }
                log.debug("completeeeeaahhh == " + completed);
            }
        }

        if (completed == true) {
            if (vso.getLeasing() == null || vso.getLeasing().getIdLeasingCompany().isEmpty() ) {
                log.debug("belok kanan cash");
                vso.setStatus(BaseConstants.STATUS_COMPLETED);
                vso = vehicleSalesOrderRepository.saveAndFlush(vso);
            }

            if (vso.getLeasing() != null ) {
                log.debug("belok kanan credit");
                vso.setStatus(BaseConstants.STATUS_VSO_WAITING_ADMIN_HANDLING);
                vso = vehicleSalesOrderRepository.saveAndFlush(vso);
            }

            // Close All Data
            if (vso.getOrderType().getIdOrderType().equals(BaseConstants.ORDER_TYPE_VSO)) {

                VehicleSalesBilling billing = execution.getVariable("billing", VehicleSalesBilling.class);
                if (billing != null && billing.getCurrentStatus().equals(BaseConstants.STATUS_DRAFT)) {
                    billing.setStatus(BaseConstants.STATUS_OPEN);
                    billing = vehicleSalesBillingRepository.saveAndFlush(billing);
                }

                ShipmentOutgoing shipment = execution.getVariable("shipment", ShipmentOutgoing.class);
                if (shipment != null && shipment.getCurrentStatus().equals(BaseConstants.STATUS_DRAFT)) {
                    shipment.setStatus(BaseConstants.STATUS_OPEN);
                    shipment = shipmentOutgoingRepository.saveAndFlush(shipment);
                }
            }

            SalesUnitRequirement sur  = salesUnitRequirementRepository.findOne(vso.getIdSalesUnitRequirement());
            if (sur != null) {
                List<SalesBooking> sb  = salesBookingRepository.listSalesBookingByIdReq(sur.getIdRequirement());
                if (!sb.isEmpty()) {
                    for (SalesBooking salesBooking : sb) {
                        salesBookingService.closeSalesBookingActive(salesBooking);
                    }
                }
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updateIVU(DelegateExecution execution) {
        // Buat kwitansi payment yang belum lunas.
        Integer orderType = (Integer) execution.getVariable("orderType");
        if (!orderType.equals(BaseConstants.ORDER_TYPE_VSO)) return;

        SalesUnitRequirement sur = execution.getVariable("sur", SalesUnitRequirement.class);
        Internal internal = execution.getVariable("internal", Internal.class);
        Customer customer = execution.getVariable("customer", Customer.class);
        BillTo billFrom = execution.getVariable("billFrom", BillTo.class);
        BillTo billTo = execution.getVariable("billTo", BillTo.class);
        Billing billing = execution.getVariable("billing", Billing.class);

        List<Receipt> receipts = receiptRepository.queryByIdRequirement(sur.getIdRequirement());

        Double totalDP = sur.getDownPayment().doubleValue();
        Double userPaid = 0d;
        Double hargaMotor = sur.getUnitPrice().doubleValue();
        Double amount = 0d;
        for (Receipt r: receipts) {
            if (r.getAmount() != null) {
                amount = r.getAmount().doubleValue();
            }
            log.debug("amount == " + amount);
            userPaid = userPaid + amount;
            if (!r.getCurrentStatus().equals(BaseConstants.STATUS_COMPLETED)) {
                r.setStatus(BaseConstants.STATUS_COMPLETED);
                r = receiptRepository.save(r);
            }
            Double outstanding = paymentUtils.paymentOutstanding(r.getIdPayment());
            if (outstanding > 0) {
                paymentUtils.assignBillingToPayment(billing, r, outstanding);
            }
        }

        // Buat ID MPM
        if (customer.getIdMPM() == null) {
            PersonalCustomer pc = personalCustomerRepository.findOne(customer.getIdCustomer());
            pc.setIdMPM(customerService.nextIdMPM(internal));
            personalCustomerRepository.save(pc);
        }

        // Kwitansi Sisa DP
        if (totalDP > userPaid) {
            Shipment shipment = execution.getVariable("shipment", Shipment.class);
            VehicleSalesOrder vso = execution.getVariable("vso", VehicleSalesOrder.class);

            // Tandai can close dp, sebagai false, jika belum lunas
            vso.setCanCloseDP(false);
            vehicleSalesOrderRepository.save(vso);

            Receipt r = new Receipt();
            r.setIdRequirement(sur.getIdRequirement());
            r.setAmount(BigDecimal.valueOf(totalDP - userPaid));
            r.setInternal(internal);
            r.setPaidFrom(customerUtils.getBillTo(customer));
            r.setPaidTo(billFrom);
            r.setPaymentNumber(numbering.getInternalNumber("KWT"));
            r.setPaymentType(paymentUtils.getPaymentType(BaseConstants.PAYMENT_TYPE_DP));
            r.setMethod(paymentUtils.getPaymentMethod(BaseConstants.PAYMENT_METHOD_CASH));
            r = receiptRepository.saveAndFlush(r);
            // Assign billing to payment
            paymentUtils.assignBillingToPayment(billing, r, totalDP - userPaid);

            // Set Status Payment Active
            r.setStatus(BaseConstants.STATUS_ACTIVE);
            receiptRepository.saveAndFlush(r);

            Map<String, Object> vars = new HashMap<>();
            vars.putAll(execution.getVariables());
            vars.put("receipt", r);
            vars.put("bussinesKey", r.getIdPayment().toString());
            producerTemplate.sendBodyAndHeaders("direct:activiti-process", "payment-receipt", vars);

        }

        // Kwitansi Pelunasan
        Receipt l = new Receipt();
        l.setAmount(BigDecimal.valueOf(hargaMotor - totalDP));
        l.setInternal(internal);
        l.setPaidFrom(billTo);
        l.setPaidTo(billFrom);
        l.setPaymentNumber(numbering.getInternalNumber("KWT"));
        l.setPaymentType(paymentUtils.getPaymentType(BaseConstants.PAYMENT_TYPE_PELUNASAN));
        l.setMethod(paymentUtils.getPaymentMethod(BaseConstants.PAYMENT_METHOD_CASH));
        l = receiptRepository.saveAndFlush(l);
        // Assign billing to payment
        paymentUtils.assignBillingToPayment(billing, l, hargaMotor - totalDP);

        // Set Status Payment Active
        l.setStatus(BaseConstants.STATUS_ACTIVE);
        receiptRepository.saveAndFlush(l);

        Map<String, Object> vars = new HashMap<>();
        vars.putAll(execution.getVariables());
        vars.put("receipt", l);
        vars.put("bussinesKey", l.getIdPayment().toString());
        producerTemplate.sendBodyAndHeaders("direct:activiti-process", "payment-receipt", vars);

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void closeProspect (DelegateExecution execution) {
        SalesUnitRequirement sur = execution.getVariable("sur", SalesUnitRequirement.class);
        List<Prospect> listProspectVsoComplete = prospectRepository.findProspectVsoComplete(sur.getIdProspect());
        List<Prospect> listProspectSur = prospectRepository.findProspectSur(sur.getIdProspect());
        VehicleSalesOrder vso = execution.getVariable("vso", VehicleSalesOrder.class);

        Integer prospectComplete = listProspectVsoComplete.size();
        Integer prospectSur = listProspectSur.size();
        if (vso.getOrderType().getIdOrderType() == 110 ) {
            log.debug("close status prospcet atas prospectComplete ===== " +prospectComplete);
            System.out.println("close status prospcet atas prospectComplete ===== " +prospectComplete);
            log.debug("close status prospcet atas prospectSur ===== "+prospectSur);
            System.out.println("close status prospcet atas prospectSur ===== "+prospectSur);
            if (prospectComplete == prospectSur) {
                log.debug("close status prospcet =====");
                Prospect prospect = prospectRepository.findOne(sur.getIdProspect());
                prospect.setStatus(17);
                prospect = prospectRepository.save(prospect);
            }
        }

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void closeApprovalLeasing(SalesUnitRequirement dto) {
        if (dto.getSaleType().getIdSaleType() == BaseConstants.SALE_TYPE_CREDIT ||
            dto.getSaleType().getIdSaleType() == BaseConstants.SALE_TYPE_CREDIT_COD ||
            dto.getSaleType().getIdSaleType() == BaseConstants.SALE_TYPE_CREDIT_CBD) {
            List<Approval> listApproval = approvalRepository.findApprovalLeasingByIdReq(dto.getIdRequirement());
            for(Approval approval: listApproval) {
                salesUnitRequirementService.setNewApproval(dto.getIdRequirement(), 104, 17);
            }
        }

    }

}
