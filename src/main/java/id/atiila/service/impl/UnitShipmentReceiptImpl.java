package id.atiila.service.impl;

import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.service.InternalUtils;
import id.atiila.service.MasterNumberingService;
import id.atiila.service.VendorUtils;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

@Service
@Transactional
public class UnitShipmentReceiptImpl {

    private final Logger log = LoggerFactory.getLogger(UnitShipmentReceiptImpl.class);

    @Autowired
    private PackageReceiptRepository packageReceiptRepository;

    @Autowired
    private UnitShipmentReceiptRepository unitShipmentReceiptRepository;

    @Autowired
    private OrderBillingItemRepository orderBillingItemRepository;

    @Autowired
    private ShipmentReceiptRepository shipmentReceiptRepository;

    @Autowired
    private InventoryItemRepository inventoryItemRepository;

    @Autowired
    private ShipmentItemRepository shipmentItemRepository;

    @Autowired
    private ShipmentOutgoingRepository shipmentOutgoingRepository;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private VendorUtils vendorUtils;

    @Autowired
    private MasterNumberingService numberingService;

    @Autowired
    private OrderShipmentItemRepository orderShipmentItemRepository;

    public void activate(DelegateExecution execution) {
        boolean isValid = false;

        PackageReceipt p = execution.getVariable("packageReceipt", PackageReceipt.class);
        List<UnitShipmentReceipt> shipmentReceipts = unitShipmentReceiptRepository.findByIdPackage(p.getIdPackage());

        // Cek Unit Shipment Receipt, PO dan Billing Itemnya
        for (UnitShipmentReceipt s: shipmentReceipts) {
            if (s.getOrderItem() == null && p.getVendorInvoice() != null) {
                List<OrderBillingItem> obis = orderBillingItemRepository.queryByInvoiceProductType(p.getVendorInvoice(), s.getIdProduct(), s.getIdFeature());
                if (!obis.isEmpty()) {
                    for (OrderBillingItem obi: obis) {
                        Double qtyFilled = shipmentReceiptRepository.sumFilledOrder(obi.getOrderItem().getIdOrderItem());
                        qtyFilled = qtyFilled != null ? qtyFilled : 0d;
                        if (qtyFilled < obi.getBillingItem().getQty()) {
                            s.setOrderItem(obi.getOrderItem());
                            unitShipmentReceiptRepository.save(s);
                            break;
                        }
                    }
                }
            }
        }

        //
        for (UnitShipmentReceipt s: shipmentReceipts) {
            isValid = s.getOrderItem() != null;
            if (!isValid) break;
        }

        //
        execution.setVariable("isValid", isValid);
        execution.setVariable("shipmentReceipts", shipmentReceipts);
    }

    public void approve(DelegateExecution execution) {
        PackageReceipt p = execution.getVariable("packageReceipt", PackageReceipt.class);
        List<UnitShipmentReceipt> shipmentReceipts = (List<UnitShipmentReceipt>) execution.getVariable("shipmentReceipts");

        //
        for (UnitShipmentReceipt s: shipmentReceipts) {
            if (s.getInventoryItems().isEmpty()) {
                // Cari Container Default

                // Buat Inventory Item
                InventoryItem inv = new InventoryItem();
                inv.setIdProduct(s.getIdProduct());
                inv.setIdFeature(s.getIdFeature());
                inv.setIdFrame(s.getIdFrame());
                inv.setIdMachine(s.getIdMachine());
                inv.setYearAssembly(s.getYearAssembly());
                inv.setIdOwner(p.getInternal().getRoot().getOrganization().getIdParty());
                inv.setQtyBooking(0);
                inv.setQty(s.getQtyAccept().doubleValue() - s.getQtyReject().doubleValue());
                inv = inventoryItemRepository.save(inv);

                s.addInventoryItem(inv);
            }
        }
    }

    public void buildSPG(DelegateExecution execution) {
        PackageReceipt p = execution.getVariable("packageReceipt", PackageReceipt.class);
        List<UnitShipmentReceipt> shipmentReceipts = (List<UnitShipmentReceipt>) execution.getVariable("shipmentReceipts");

        ShipmentOutgoing so = new ShipmentOutgoing();
        so.setInternal(p.getInternal());
        so.setShipTo(internalUtils.getShipTo(p.getInternal().getRoot()));
        so.setShipFrom(p.getShipFrom());
        so.setShipmentNumber(numberingService.getInternalNumber(p.getInternal(), "RECV"));
        so = shipmentOutgoingRepository.save(so);

        //
        for (UnitShipmentReceipt s: shipmentReceipts) {
            if (s.getShipmentItem() == null) {
                // Buat Inventory Item
                ShipmentItem si = new ShipmentItem();
                si.setIdProduct(s.getIdProduct());
                si.setIdFeature(s.getIdFeature());
                si.setItemDescription(s.getItemDescription());
                si.setIdFrame(s.getIdFrame());
                si.setIdMachine(s.getIdMachine());
                si.setQty(s.getQtyAccept().doubleValue() - s.getQtyReject().doubleValue());
                si.setShipment(so);
                si = shipmentItemRepository.save(si);

                s.setShipmentItem(si);
                unitShipmentReceiptRepository.saveAndFlush(s);

                OrderShipmentItem osi = new OrderShipmentItem();
                osi.setOrderItem(s.getOrderItem());
                osi.setShipmentItem(si);
                osi.setQty(si.getQty());
                orderShipmentItemRepository.save(osi);

            }
        }
    }
}
