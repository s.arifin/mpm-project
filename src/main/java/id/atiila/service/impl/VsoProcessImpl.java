package id.atiila.service.impl;

import id.atiila.base.BaseConstants;
import id.atiila.domain.OrderItem;
import id.atiila.domain.VehicleSalesBilling;
import id.atiila.domain.VehicleSalesOrder;
import id.atiila.repository.VehicleSalesBillingRepository;
import id.atiila.repository.VehicleSalesOrderRepository;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.ActivitiObjectNotFoundException;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class VsoProcessImpl {

    private final Logger log = LoggerFactory.getLogger(VsoProcessImpl.class);

    @Autowired
    private VehicleSalesOrderRepository vehicleSalesOrderRepository;

    public void buildPicking(DelegateExecution execution) {
    }

    public void prepare(DelegateExecution execution) {
        String value = execution.getVariable("value", String.class);
        String idOrder = execution.getVariable("idOrder", String.class);
        VehicleSalesOrder vso = vehicleSalesOrderRepository.findOne(UUID.fromString(idOrder));
        if (vso == null) {
            throw new ActivitiObjectNotFoundException("VSO tidak di kenal !");
        }
        // Cari Order Item yang belum di penuhi, masukan kedalam list OrderItems
        List<OrderItem> items = new ArrayList<>();
        for (OrderItem oi: vso.getDetails()) {
            if (oi.getQtyFilled().intValue() == 0) {
                // Check di Picking, apakah sedang di proses.
                items.add(oi);
            }
        }
        if (items.isEmpty())
            throw new ActivitiObjectNotFoundException("VSO sudah di penuhi semua !");

        execution.setVariable("items", items);
        System.out.println("Current Value: " + value);
    }

    public void buildItemIssuance(DelegateExecution execution) {
    }

    public void buildShipment(DelegateExecution execution) {
    }

    public void buildBilling(DelegateExecution execution) {
    }

    public void buildDocumentRegistration(DelegateExecution execution) {
    }


}
