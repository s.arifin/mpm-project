package id.atiila.service.impl;

import id.atiila.base.BaseConstants;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.service.InternalUtils;
import id.atiila.service.ProductUtils;
import id.atiila.service.VendorUtils;
import id.atiila.service.dto.InvoiceUploadDTO;
import org.apache.camel.Body;
import org.apache.camel.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class OrderBeanImpl {
    private final Logger log = LoggerFactory.getLogger(ShipmentBeanImpl.class);

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private VendorUtils vendorUtils;

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private BillingDisbursementRepository billingDisbursementRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private OrderBillingItemRepository orderBillingItemRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    public PurchaseOrder convertBillingDisbursementToPurchaseOrder(@Body BillingDisbursement billingDisbursement, @Headers Map<String, Object> headers){

        //Map<String, Object> params = (Map<String, Object>) headers.get("parameters");

        List <PurchaseOrder> pos = purchaseOrderRepository
            .findByParams(billingDisbursement.getVendor().getIdVendor(),
                          billingDisbursement.getInternal().getIdInternal(),
                         null, headers.get("poNumber").toString(),
                          new PageRequest(0,1)).getContent();

        PurchaseOrder po = pos.isEmpty() ? null : pos.get(0);

        //Find existing billing disbursement
        BillingDisbursement b = billingDisbursementRepository.findOne(billingDisbursement.getIdBilling());

        ShipTo sh = internalUtils.getShipTo(b.getInternal());
        BillTo bt = internalUtils.getBillTo(b.getInternal());

        if (po == null) {

            po = new PurchaseOrder();
            po.setVendor(b.getVendor());
            po.setInternal(b.getInternal());
            po.setOrderNumber(headers.get("poNumber").toString());
            po.setBillTo(bt);
            po.dateEntry(ZonedDateTime.now());
            po.dateOrder(ZonedDateTime.now());
            po = purchaseOrderRepository.save(po);

        }

        for (BillingItem o: b.getDetails()) {

            //TODO: Perihal Indent Find Terlebih Dahulu Order Item yg Gantung

            OrderItem item = new OrderItem();
            item.setOrders(po);
            item.setIdProduct(o.getIdProduct());
            item.setIdFeature(o.getIdFeature());
            item.setItemDescription(o.getItemDescription());
            item.setQty(o.getQty());
            item.setUnitPrice(o.getUnitPrice());
            item.setTotalAmount(o.getTotalAmount());
            item.setIdShipTo(sh.getIdShipTo());
            item = orderItemRepository.save(item);

            po.getDetails().add(item);

            OrderBillingItem obItem = new OrderBillingItem();
            obItem.setBillingItem(o);
            obItem.setOrderItem(item);
            obItem.setQty(obItem.getQty());
            obItem.setAmount(obItem.getAmount());
            orderBillingItemRepository.save(obItem);
        }

        return po;
    }

    @Transactional
    public void buildPurchaseOrder(@Body PurchaseOrder po){
        purchaseOrderRepository.save(po);
    }
}
