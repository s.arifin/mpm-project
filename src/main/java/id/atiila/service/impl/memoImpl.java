package id.atiila.service.impl;

import id.atiila.base.BaseConstants;
import id.atiila.domain.Memos;
import id.atiila.domain.VehicleSalesBilling;
import id.atiila.repository.MemosRepository;
import id.atiila.repository.VehicleSalesBillingRepository;
import id.atiila.service.ActivitiProcessor;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
@Transactional
public class memoImpl {
    private final Logger log = LoggerFactory.getLogger(Memos.class);
    @Autowired
    private ActivitiProcessor processor;

    @Autowired
    private MemosRepository memosRepository;

    @Autowired
    private VehicleSalesBillingRepository vehicleSalesBillingRepository;

    public void cancelIVU(DelegateExecution execution) {

        Memos memos = execution.getVariable("memos", Memos.class);

        VehicleSalesBilling vsb = vehicleSalesBillingRepository.findOne(memos.getBilling().getIdBilling());
        log.debug("ivu di cancel memo = " + vsb);
        String processName = "ivu-cancel";
        Map<String, Object> vars = processor.getVariables();
        processor.startProcess(processName, vsb.getIdBilling().toString(), vars);
    }

    public void updateMemo(DelegateExecution execution) {
        Memos memos = execution.getVariable("memos", Memos.class);

        memos.setStatus(BaseConstants.STATUS_COMPLETED);
        memos = memosRepository.saveAndFlush(memos);
    }

    public void gantiUnit(DelegateExecution execution) {

    }
}
