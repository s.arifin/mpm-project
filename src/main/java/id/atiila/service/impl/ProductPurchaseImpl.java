package id.atiila.service.impl;

import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.service.*;
import org.apache.camel.Body;
import org.apache.camel.Headers;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.time.ZonedDateTime;
import java.util.Map;

@Service
@Transactional
public class ProductPurchaseImpl {

    private final Logger log = LoggerFactory.getLogger(ProductPurchaseImpl.class);

    @Autowired
    private ProductPackageReceiptRepository productPackageReceiptRepository;

    @Autowired
    private VendorUtils vendorUtils;

    @Autowired
    private ProductUtils productUtils;

    @Autowired
    private InventoryUtils inventoryUtils;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private StorageUtils storageUtils;

    @Autowired
    private ShipmentItemRepository shipmentItemRepository;

    @Autowired
    private ProductShipmentReceiptRepository productShipmentReceiptRepository;

    @Autowired
    private ProductShipmentIncomingRepository productShipmentIncomingRepository;

    @Autowired
    private BillingDisbursementRepository billingDisbursementRepository;

    @Autowired
    private BillingItemRepository billingItemRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    public ProductPurchaseOrder readOrder(@Body ProductPurchaseOrder po, @Headers Map<String, Object> header) {
        // Catat PO original ke header, agar dapat di ambil lagi oleh EndPoint yang memerlukan
        header.put("po", po);
        return po;
    }

    public InventoryItem buildInventoryItem(ProductPackageReceipt p, ProductShipmentReceipt r) {
        if (r.getInventoryItems().size() > 0) return r.getInventoryItems().iterator().next();

        InventoryItem item = new InventoryItem();
        item.assignFrom(r);
        item.dateCreated(ZonedDateTime.now());
        item.setShipmentReceipt(r);

        GoodContainer container = storageUtils.getGoodContainer(p.getInternal().getOrganization(), r.getIdProduct());
        item.setIdContainer(container != null ? container.getContainer().getIdContainer() : null);

        // Find Good Container / Facility
        if (item.getIdContainer() == null) {
            Facility facl = p.getInternal().getPrimaryFacility();
            if (facl != null) item.setIdFacility(facl.getIdFacility());
        }

        //
        return item;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public ProductPackageReceipt receiveOrder(@Body ProductPurchaseOrder po, @Headers Map<String, Object> header) {
        ProductPackageReceipt pr = new ProductPackageReceipt();
        pr.setDocumentNumber(RandomStringUtils.randomAlphanumeric(7));
        pr.setShipFrom(vendorUtils.getShipTo(po.getVendor()));
        pr.setInternal(po.getInternal());

        for (OrderItem oi: po.getDetails()) {
            ProductShipmentReceipt sr = inventoryUtils.buildShipmentReceipt(pr, oi, 12d);
            sr.setShipmentPackage(pr);
            pr.getShipmentReceipts().add(sr);
        }
        pr = productPackageReceiptRepository.save(pr);
        return pr;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public ProductPackageReceipt buildShipment(@Body ProductPackageReceipt pr, @Headers Map<String, Object> header) {
        ProductShipmentIncoming psc = null;

        for (ShipmentReceipt sr: pr.getShipmentReceipts()) {
            ProductShipmentReceipt psr = (ProductShipmentReceipt) sr;
            if (psr.getShipmentItem() == null) {
                if (psc == null) {
                    psc = new ProductShipmentIncoming();
                    psc.setShipFrom(pr.getShipFrom());
                    psc.setShipTo(internalUtils.getShipTo(pr.getInternal()));
                    psc.setAddressFrom(psc.getShipFrom().getParty().getPostalAddress());
                    psc.setAddressTo(psc.getShipTo().getParty().getPostalAddress());
                    psc.setShipmentNumber(pr.getDocumentNumber());
                    psc.setDateSchedulle(ZonedDateTime.now());
                    psc = productShipmentIncomingRepository.save(psc);
                }

                ShipmentItem si = new ShipmentItem();
                si.assignFrom(sr);
                si.setShipment(psc);
                psc.addDetail(si);
                si = shipmentItemRepository.save(si);

                sr.setShipmentItem(si);
                productShipmentReceiptRepository.save(psr);
            }
        }

        if (psc != null) {
            psc = productShipmentIncomingRepository.save(psc);
        }

        return productPackageReceiptRepository.findOne(pr.getIdPackage());
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public ProductPurchaseOrder buildBilling(@Body ProductPackageReceipt pack, @Headers Map<String, Object> header) {
        BillingDisbursement billing = null;
        for (ShipmentReceipt sr: pack.getShipmentReceipts()) {
            OrderItem oi = sr.getOrderItem();
            ShipmentItem si = sr.getShipmentItem();

            if (si.getBillingItems().isEmpty()) {
                PurchaseOrder po = (PurchaseOrder) sr.getOrderItem().getOrders();

                if (billing == null) {
                    billing = new BillingDisbursement();
                    billing.setVendor(po.getVendor());
                    billing.setBillFrom(vendorUtils.getBillTo(po.getVendor()));
                    billing.setInternal(po.getInternal());
                    billing.setBillTo(po.getBillTo());
                    billing.setDateDue(ZonedDateTime.now().plusDays(10));
                    billing = billingDisbursementRepository.save(billing);
                }

                BillingItem bi = new BillingItem().addOrderItem(oi, sr.getQtyAccept() - sr.getQtyReject());
                bi.setBilling(billing);
                billing.addDetail(bi);

                oi.addShipmentItem(si);
                oi.addBillingItem(bi);
                si.addBillingItem(bi);

                billingItemRepository.save(bi);
                shipmentItemRepository.save(si);
                orderItemRepository.save(oi);
            }
        }
        return (ProductPurchaseOrder) header.get("po");
    }
}
