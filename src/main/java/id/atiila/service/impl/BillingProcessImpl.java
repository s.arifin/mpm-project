package id.atiila.service.impl;

import id.atiila.base.BaseConstants;
import id.atiila.base.executeprocess.SalesUnitRequirementConstants;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.repository.search.VehicleSalesBillingSearchRepository;
import id.atiila.service.MasterNumberingService;
import id.atiila.service.SalesUnitRequirementService;
import id.atiila.service.dto.SalesUnitRequirementDTO;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class BillingProcessImpl {

    private final Logger log = LoggerFactory.getLogger(BillingProcessImpl.class);

    @Autowired
    private VehicleSalesBillingRepository vehicleSalesBillingRepository;

    @Autowired
    private OrderBillingItemRepository orderBillingItemRepository;

    @Autowired
    private ShipmentBillingItemRepository shipmentBillingItemRepository;

    @Autowired
    private SalesUnitRequirementService salesUnitRequirementService;

    @Autowired
    private ShipmentOutgoingRepository shipmentOutgoingRepository;

    @Autowired
    private ItemIssuanceRepository itemIssuanceRepository;

    @Autowired
    private PickingSlipRepository pickingSlipRepository;

    @Autowired
    private InventoryItemRepository inventoryItemRepository;

    @Autowired
    private VehicleSalesOrderRepository vehicleSalesOrderRepository;

    @Autowired
    private OrderShipmentItemRepository orderShipmentItemRepository;

    @Autowired
    private VehicleSalesBillingSearchRepository vehicleSalesBillingSearchRepository;

    @Autowired
    private VehicleDocumentRequirementRepository vehicleDocumentRequirementRepository;

    @Autowired
    private MasterNumberingService masterNumberingService;

    public void prepareCancelIvu(DelegateExecution execution) {
        UUID idBilling = UUID.fromString(execution.getProcessInstanceBusinessKey());

        VehicleSalesBilling vsb = vehicleSalesBillingRepository.findOne(idBilling);

        String iruNumber = masterNumberingService.findTransNumberIRUBy(vsb.getInternal().getIdInternal(), "IRU");

        if (vsb == null) {
            execution.setVariable("dataFound", false);
        }

        // Mark VSB status cancel
        vsb.setStatus(BaseConstants.STATUS_CANCEL);
        vsb.setIruNumber(iruNumber);
        vsb = vehicleSalesBillingRepository.save(vsb);
        vehicleSalesBillingSearchRepository.delete(idBilling);
        execution.setVariable("vsb", vsb);

        // List<InventoryItem> inventories = vehicleSalesBillingRepository.getInventories(vsb.getIdBilling());
        List<InventoryItem> inventories = new ArrayList<>();
        for (BillingItem bi: vsb.getDetails()) {
            if (bi.getInventoryItem() != null) inventories.add(bi.getInventoryItem());
        }
        execution.setVariable("inventories", inventories);
        log.debug("list inventory item " + inventories.size());

        // Ambil daftar order item yang akan di batalkan
        List<OrderBillingItem> orderItems = orderBillingItemRepository.findByIdBilling(idBilling);
        execution.setVariable("orderItems", orderItems);

        // Ambil daftar Shipment item yang akan di batalkan
        List<ShipmentBillingItem> shipmentItems = shipmentBillingItemRepository.findByIdBilling(idBilling);
        execution.setVariable("shipmentItems", shipmentItems);

        Assert.isTrue( !shipmentItems.isEmpty(), "Shipment items tidak ada !");
        // Jika Order Item dan shipment item kosong, batal proses
        if (shipmentItems.isEmpty() && orderItems.isEmpty()) {
            execution.setVariable("dataFound", false);
        }
    }

    public void cancelDO(DelegateExecution execution) {
        ShipmentBillingItem o = execution.getVariable("shipmentItem", ShipmentBillingItem.class);
        ShipmentOutgoing cdo = (ShipmentOutgoing) o.getShipmentItem().getShipment();
        if (!cdo.getStatus().equals(BaseConstants.STATUS_CANCEL)) {
            cdo.setStatus(BaseConstants.STATUS_CANCEL);
            shipmentOutgoingRepository.save(cdo);
        }

        // remove link shipment to billing
        o.setQty(0d);
        shipmentBillingItemRepository.save(o);
    }

    public void rollbackItemIssuance(DelegateExecution execution) {
        ShipmentBillingItem o = execution.getVariable("shipmentItem", ShipmentBillingItem.class);
        List<ItemIssuance> items = itemIssuanceRepository.findByShipmentList(o.getShipmentItem());
        for (ItemIssuance d: items) {
            PickingSlip pis = d.getPicking();
            // Buat Status Picking Cancel
            pis.setStatus(BaseConstants.STATUS_CANCEL);
            pickingSlipRepository.save(pis);

            d.setQty(0d);
            itemIssuanceRepository.save(d);
        }
    }

    public void rollbackInventoryItem(DelegateExecution execution) {
        InventoryItem inv = execution.getVariable("inventoryItem", InventoryItem.class);
        // Kembalikan Qty 1
        //qtybooking 0
        log.debug("eksekusi inventory item ==  " + inv);
         inv.setQty(1d);
         inv.setQtyBooking(0d);
         inventoryItemRepository.save(inv);
    }

    public void resetVSOToApprove(DelegateExecution execution) {
         OrderBillingItem o = execution.getVariable("orderItem", OrderBillingItem.class);

        if (o.getOrderItem().getOrders() != null && o.getOrderItem().getOrders() instanceof VehicleSalesOrder) {
            VehicleSalesOrder v = (VehicleSalesOrder) o.getOrderItem().getOrders();
            if (!v.getStatus().equals(BaseConstants.STATUS_CANCEL)) {

                SalesUnitRequirementDTO sur = salesUnitRequirementService.findOne(v.getIdSalesUnitRequirement());
                salesUnitRequirementService.processExecuteData(SalesUnitRequirementConstants.CANCEL_VSO, null, sur);

                v.setStatus(BaseConstants.STATUS_CANCEL);
                vehicleSalesOrderRepository.save(v);
            }
        }

        List<OrderShipmentItem> osi = orderShipmentItemRepository.queryByOrderItem(o.getOrderItem());
        for (OrderShipmentItem d: osi) {
            d.setQty(0d);
            orderShipmentItemRepository.save(d);
        }

        List<OrderBillingItem> obi = orderBillingItemRepository.queryByOrderItem(o.getOrderItem());
        for (OrderBillingItem d: obi) {
            d.setQty(0d);
            d.setAmount(0d);
            orderBillingItemRepository.save(d);
        }

        if (o != null) {
            VehicleDocumentRequirement vdr = vehicleDocumentRequirementRepository.findByOrderItem(o.getOrderItem());
            if (vdr != null){
                vdr.setStatus(BaseConstants.STATUS_CANCEL);
                vehicleDocumentRequirementRepository.save(vdr);
            }
        }

    }
}
