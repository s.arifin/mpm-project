package id.atiila.service.impl;

import id.atiila.domain.InventoryItem;
import id.atiila.domain.InventoryMovement;
import id.atiila.repository.InventoryItemRepository;
import id.atiila.service.MasterNumberingService;
import id.atiila.service.MovingSlipService;
import id.atiila.service.dto.MovingSlipDTO;
import org.apache.camel.Body;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.Map;

@Service
@Transactional
public class MovingSlipBeanImpl {
    private final Logger log = LoggerFactory.getLogger(MovingSlipBeanImpl.class);

    @Autowired
    private InventoryItemRepository inventoryItemRepository;

    @Autowired
    private MasterNumberingService masterNumberingService;

    @Autowired
    private MovingSlipService movingSlipService;

    @Transactional
    public void setQtyBookingInventoryItem(@Body MovingSlipDTO dto, Map<String, Object> header){
        InventoryItem inventoryItem = inventoryItemRepository.findOne(dto.getInventoryItemId());
        inventoryItem.setQtyBooking((double) dto.getQty());
        inventoryItemRepository.save(inventoryItem);
    }

    @Transactional
    public MovingSlipDTO buildNewSplitInventoryItem(@Body MovingSlipDTO dto, Map<String, Object> header){
        InventoryItem inventoryItem = inventoryItemRepository.findOne(dto.getInventoryItemId());

        InventoryItem invItemNew = new InventoryItem();
        invItemNew.setQtyBooking(0d);
        invItemNew.setQty((double)dto.getQty());
        invItemNew.setIdFeature(inventoryItem.getIdFeature());
        invItemNew.setIdProduct(dto.getProductId());
        invItemNew.setIdFacility(dto.getFacilityToId());
        invItemNew.setIdContainer(dto.getContainerToId());
        invItemNew.setYearAssembly(inventoryItem.getYearAssembly());
        invItemNew.setShipmentReceipt(inventoryItem.getShipmentReceipt());

        //jika motor maka set id rangka
        if (inventoryItem.getIdFrame() != null){
            invItemNew.setIdFrame(inventoryItem.getIdFrame());
        }

        //jika motor maka set id mesin
        if (inventoryItem.getIdMachine() != null){
            invItemNew.setIdMachine(inventoryItem.getIdMachine());
        }
        invItemNew.setIdOwner(inventoryItem.getIdOwner());
        invItemNew = inventoryItemRepository.save(invItemNew);

        //set data untuk moving slip
        dto.setProductId(inventoryItem.getIdProduct());
        dto.setInventoryItemToId(invItemNew.getIdInventoryItem());
        dto.setSlipNumber(masterNumberingService.findTransNumberMovingSlipBy("MOV"));

        //aneh ga bisa di set dtcreate nya?
        dto.setDateCreate(ZonedDateTime.now());

        return dto;
    }

    @Transactional
    public void buildMovingSlip(@Body MovingSlipDTO dto, Map<String, Object> header){
        movingSlipService.save(dto);
    }
}
