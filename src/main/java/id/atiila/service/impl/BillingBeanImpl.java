package id.atiila.service.impl;

import id.atiila.base.BaseConstants;
import id.atiila.base.DmsException;
import id.atiila.domain.*;
import id.atiila.repository.*;
import id.atiila.service.*;
import id.atiila.service.dto.InvoiceUploadDTO;
import org.apache.camel.Body;
import org.apache.camel.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class BillingBeanImpl {
    private final Logger log = LoggerFactory.getLogger(BillingBeanImpl.class);

    @Autowired
    private BillingDisbursementRepository billingDisbursementRepository;

    @Autowired
    private BillingItemRepository billingItemRepository;

    @Autowired
    private BillingTypeRepository billingTypeRepository;

    @Autowired
    private InternalUtils internalUtils;

    @Autowired
    private PartyUtils partyUtils;

    @Autowired
    private VendorUtils vendorUtils;

    @Autowired
    private FeatureRepository featureRepository;

    @Autowired
    private FeatureService featureService;

    @Autowired
    private FeatureTypeRepository featureTypeRepository;

    @Autowired
    private ProductUtils productUtils;


    @Transactional(propagation = Propagation.REQUIRED)
    public BillingDisbursement convertUploadInvoiceToBillingDisbursement(@Body Set<InvoiceUploadDTO> r, @Headers Map<String, Object> headers) {

        // Map<String, Object> params = (Map<String, Object>) headers.get("parameters");

        BillingType billingType = billingTypeRepository.findOne(Integer.parseInt(headers.get("billingTypeId").toString()));

        BillingDisbursement b;
        BillingItem bi;

        //get internal
        Internal internal = internalUtils.findOne(partyUtils.getCurrentInternal().getIdInternal());

        //get vendor
        Vendor v = vendorUtils.buildVendor(r.iterator().next().getMainDealerCode(),r.iterator().next().getMainDealerCode());

        //Find existing billing disbursement
        List<BillingDisbursement> items = billingDisbursementRepository.findOneByVendorInvoice(r.iterator().next().getInvoiceNumber());
        b = items.size() > 0 ? items.get(0) : null;

        if (b == null) {

            //start transaction
            b = new BillingDisbursement();
            //Set No Invoice, tanggal Invoice, Customer Code, MD/Vendor Code, Due Date
            b.setVendorInvoice(r.iterator().next().getInvoiceNumber());
            b.setDateCreate(r.iterator().next().getDateCreate());
            b.setInternal(internal);
            b.setVendor(v);
            b.setDateDue(r.iterator().next().getDueDate());
            b.setBillingType(billingType);
            b.setDateIssued(r.iterator().next().getDateIssued());
            b = billingDisbursementRepository.save(b);


            for (InvoiceUploadDTO inv : r){

                //calculate tax
                BigDecimal tax = inv.getUnitPrice().movePointLeft(1).setScale(0, RoundingMode.UP);

                //get feature
                List<Feature> lf = featureRepository.findByRefKey(inv.getIdColor());
                Feature f;

                if (!lf.isEmpty()) {
                    f = lf.get(0);
                }
                else {
                    f = new Feature();
                    f.setDescription("");

                    //get featuretype
                    FeatureType ft = featureTypeRepository.findOne(100);

                    //get last id
                    Feature featureLast = featureRepository.findTop1ByOrderByIdFeatureDesc();
                    Integer newIdFeature = featureLast.getIdFeature() + 1;

                    f.setIdFeature(newIdFeature);
                    f.setFeatureType(ft);
                    f.setDescription(inv.getIdColor());
                    f.setRefKey(inv.getIdColor());

                    f = featureRepository.save(f);
                }

                //Find Product
                Product product = productUtils.findOne(inv.getIdProduct());

                if (product == null) {
                    throw new DmsException("Produk dengan id "+ inv.getIdProduct() + " tidak ditemukan di sistem");
                }

                bi = new BillingItem();
                // set kode unit, warna, qty, Unit Price - PPN, Discount
                bi.setBilling(b);
                bi.setIdProduct(inv.getIdProduct());
                bi.setItemDescription(product == null ? null : product.getDescription());
                bi.setQty(inv.getQty().doubleValue());
                bi.setDiscount(inv.getUnitDiscount());
                bi.setIdFeature(f.getIdFeature());
                bi.setTaxAmount(tax);
                bi.setIdItemType(Integer.parseInt(headers.get("billingItemTypeId").toString()));
                bi.setUnitPrice(inv.getUnitPrice());
                billingItemRepository.save(bi);

                b.getDetails().add(bi);
            }
        }
        return b;
    }

    @Transactional
    public BillingDisbursement buildBillingDisbursement(@Body BillingDisbursement item){
        return billingDisbursementRepository.save(item);
    }


}
