package id.atiila.service;

import id.atiila.domain.*;
import id.atiila.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GeoBoundaryUtils {

    private final Logger log = LoggerFactory.getLogger(GeoBoundaryUtils.class);

    @Autowired
    private GeoBoundaryRepository geoBoundaryRepository;

    @Autowired
    private ProvinceService provinceService;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private CityService cityService;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private DistrictService districtService;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private VillageService villageService;

    @Autowired
    private VillageRepository villageRepository;

    @Transactional
    public GeoBoundary findOneByDescription(String desc){
        if (desc == null) return null;
        List<GeoBoundary> geos =  geoBoundaryRepository.findOneByDescription(desc.toUpperCase());
        if (geos.isEmpty()) return null;
        return geos.get(0);
    }

    @Transactional
    public Province findProvinceByDescription(String desc){
        if (desc == null) return null;
        List<Province> provinces =  provinceRepository.findOneByDescription(desc.toUpperCase());
        if (provinces.isEmpty()) return null;
        return provinces.get(0);
    }

    @Transactional
    public City findCityByDescription(String desc){
        if (desc == null) return null;
        List<City> cities =  cityRepository.findOneByDescription(desc.toUpperCase());
        if (cities.isEmpty()) return null;
        return cities.get(0);
    }

    @Transactional
    public District findDistrictByDescription(String desc){
        if (desc == null) return null;
        List<District> districts =  districtRepository.findOneByDescription(desc.toUpperCase());
        if (districts.isEmpty()) return null;
        return districts.get(0);
    }

    @Transactional
    public Village findVillageByDescription(String desc){
        if (desc == null) return null;
        List<Village> villages =  villageRepository.findOneByDescription(desc.toUpperCase());
        if (villages.isEmpty()) return null;
        return villages.get(0);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void initialize(){
        log.debug("THERE ARE 4 STEPS FOR BUILD GEOBOUNDARY!!!");

        //Firstly, Build Province
        log.debug("Firstly, Build Province");
        provinceService.initialize();

        //Secondly, Build City
        log.debug("Secondly, Build City");
        cityService.initialize();

        //Thirdly, Build District
        log.debug("Thirdly, Build District");
        districtService.initialize();

        //Finally, Build Village
        log.debug("Finally, Build Village");
        villageService.initialize();

        log.debug("Hooray all geoboundary was built");
    }
}
