package id.atiila.service;

import id.atiila.base.BaseConstants;
import id.atiila.domain.PackageReceipt;
import id.atiila.domain.ShipmentIncoming;
import id.atiila.domain.ShipmentItem;
import id.atiila.domain.UnitShipmentReceipt;
import id.atiila.repository.PackageReceiptRepository;
import id.atiila.repository.ShipmentIncomingRepository;
import id.atiila.repository.ShipmentItemRepository;
import id.atiila.repository.UnitShipmentReceiptRepository;
import id.atiila.repository.search.ShipmentIncomingSearchRepository;
import id.atiila.service.dto.CustomShipmentIncomingDTO;
import id.atiila.service.dto.ShipmentIncomingDTO;
import id.atiila.service.mapper.ShipmentIncomingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ShipmentIncoming.
 * BeSmart Team
 */

@Service
@Transactional
public class ShipmentIncomingService {

    private final Logger log = LoggerFactory.getLogger(ShipmentIncomingService.class);

    private final ShipmentIncomingRepository shipmentIncomingRepository;

    private final ShipmentIncomingMapper shipmentIncomingMapper;

    private final ShipmentIncomingSearchRepository shipmentIncomingSearchRepository;

    @Autowired
    private PackageReceiptRepository packageReceiptRepository;

    @Autowired
    private UnitShipmentReceiptRepository unitShipmentReceiptRepository;

    @Autowired
    private ShipmentItemService shipmentItemService;

    public ShipmentIncomingService(ShipmentIncomingRepository shipmentIncomingRepository, ShipmentIncomingMapper shipmentIncomingMapper, ShipmentIncomingSearchRepository shipmentIncomingSearchRepository) {
        this.shipmentIncomingRepository = shipmentIncomingRepository;
        this.shipmentIncomingMapper = shipmentIncomingMapper;
        this.shipmentIncomingSearchRepository = shipmentIncomingSearchRepository;
    }

    /**
     * Save a shipmentIncoming.
     *
     * @param shipmentIncomingDTO the entity to save
     * @return the persisted entity
     */
    public ShipmentIncomingDTO save(ShipmentIncomingDTO shipmentIncomingDTO) {
        log.debug("Request to save ShipmentIncoming : {}", shipmentIncomingDTO);
        ShipmentIncoming shipmentIncoming = shipmentIncomingMapper.toEntity(shipmentIncomingDTO);
        shipmentIncoming = shipmentIncomingRepository.save(shipmentIncoming);
        ShipmentIncomingDTO result = shipmentIncomingMapper.toDto(shipmentIncoming);
        shipmentIncomingSearchRepository.save(shipmentIncoming);
        return result;
    }

    /**
     *  Get all the shipmentIncomings.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentIncomingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShipmentIncomings");
        return shipmentIncomingRepository.findActiveShipmentIncoming(pageable)
            .map(shipmentIncomingMapper::toDto);
    }

    /**
     *  Get one shipmentIncoming by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ShipmentIncomingDTO findOne(UUID id) {
        log.debug("Request to get ShipmentIncoming : {}", id);
        ShipmentIncoming shipmentIncoming = shipmentIncomingRepository.findOne(id);
        return shipmentIncomingMapper.toDto(shipmentIncoming);
    }

    @Transactional(readOnly = true)
    public CustomShipmentIncomingDTO findOneCustomBy(UUID id) {
        List<CustomShipmentIncomingDTO> r = shipmentIncomingRepository.findCustomShipmentIncomingByIdShipment(id);
        if (r.isEmpty()) return null;
        return r.get(0);
    }

    /**
     *  Delete the  shipmentIncoming by id.
     *
     *  @param id the id of the entity
     */
    public void delete(UUID id) {
        log.debug("Request to delete ShipmentIncoming : {}", id);
        shipmentIncomingRepository.delete(id);
        shipmentIncomingSearchRepository.delete(id);
    }

    /**
     * Search for the shipmentIncoming corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentIncomingDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ShipmentIncomings for query {}", query);
        Page<ShipmentIncoming> result = shipmentIncomingSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(shipmentIncomingMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ShipmentIncomingDTO> findFilterBy(HttpServletRequest request, Pageable pageable) {
        log.debug("Request to get all filtered PackageReceiptDTO");
        String idInternal = request.getParameter("idInternal");
        String idShipFrom = request.getParameter("idShipFrom");
        Set<Integer> statuses = request.getParameter("idStatusType") == null ?
            new HashSet<>(Arrays.asList(10, 11, 17)) : convertStringToSetInteger(request.getParameter("idStatusType"));

        return shipmentIncomingRepository.findByParams(idInternal, idShipFrom, statuses, pageable)
            .map(shipmentIncomingMapper::toDto);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public ShipmentIncomingDTO processExecuteData(Integer id, String param, ShipmentIncomingDTO dto) {
        ShipmentIncomingDTO r = dto;
        if (r != null) {
            switch (id) {
                case 101:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public Set<ShipmentIncomingDTO> processExecuteListData(Integer id, String param, Set<ShipmentIncomingDTO> dto) {
        Set<ShipmentIncomingDTO> r = dto;
        if (r != null) {
            switch (id) {
                case 1:
                    break;
                default:
                    break;
            }
		}
        return r;
    }

    public ShipmentIncomingDTO changeShipmentIncomingStatus(ShipmentIncomingDTO dto, Integer id) {
        if (dto != null) {
			ShipmentIncoming e = shipmentIncomingRepository.findOne(dto.getIdShipment());
			if (!e.getCurrentStatus().equals(id)) {
				e.setStatus(id);
                switch (id) {
                    case 13:
                        shipmentIncomingSearchRepository.delete(dto.getIdShipment());
                        break;
                    default:
                        shipmentIncomingSearchRepository.save(e);
                }
				shipmentIncomingRepository.save(e);
			}
		}
        return dto;
    }

    private Set<Integer> convertStringToSetInteger(String text){
        String[] items = text.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\s", "").split(",");
        Set<Integer> results = new HashSet<>();

        for (int i = 0; i < items.length; i++) {
            try {
                results.add(Integer.parseInt(items[i]));
            } catch (NumberFormatException nfe) {
                //NOTE: write something here if you need to recover from formatting errors
            };
        }
        return results;
    }

    @Transactional
    public Map<String, Object> process(HttpServletRequest request, Map<String, Object> item) {
        log.debug("Execute any Process, request from front end");

        String command = (String) item.get("command");

        if (command != null && "buildIndex".equalsIgnoreCase(command)) {
            buildIndex();
        }

        Map<String, Object> r = new HashMap<>();
        return r;
    }


    @Async
    public void buildIndex() {
        shipmentIncomingSearchRepository.deleteAll();
        List<ShipmentIncoming> shipmentIncomings =  shipmentIncomingRepository.findAll();
        for (ShipmentIncoming m: shipmentIncomings) {
            shipmentIncomingSearchRepository.save(m);
            log.debug("Data shipmentIncoming save !...");
        }
    }
}
