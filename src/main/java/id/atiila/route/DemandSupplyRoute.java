package id.atiila.route;

import id.atiila.base.BaseConstants;
import id.atiila.service.impl.BillingBeanImpl;
import id.atiila.service.impl.MovingSlipBeanImpl;
import id.atiila.service.impl.OrderBeanImpl;
import id.atiila.service.impl.ShipmentBeanImpl;
import id.atiila.service.mapper.ShipmentMapper;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DemandSupplyRoute extends RouteBuilder {

    public static String ACTION_UPLOAD_SHIPPING_LIST = "direct-vm:execute:100";
    public static String ACTION_UPLOAD_INVOICE = "direct-vm:execute:101";
    public static String ACTION_APPROVE_PACKAGE_RECEIPT = "direct-vm:execute:102";
    public static String ACTION_BUILD_PURCHASE_ORDER_FROM_UPLOAD_INVOICE = "direct-vm:episode:build_purchase_order";
    public static String ACTION_BUILD_BILLING_DISBURSEMENT_FROM_UPLOAD_INVOICE = "direct-vm:episode:build_billing_disbursement";
    public static String ACTION_BUILD_MOVING_SLIP = "vm:episode:build_moving_slip";
    public static String ACTION_START_BUILD_MOVING_SLIP = "direct-vm:execute:start_build_moving_slip";

    @Autowired
    private ShipmentBeanImpl shipmentBean;

    @Autowired
    private OrderBeanImpl orderBean;

    @Autowired
    private BillingBeanImpl billingBean;

    @Autowired
    private MovingSlipBeanImpl movingSlipBean;

    @Override
    public void configure() throws Exception {
        from(ACTION_BUILD_MOVING_SLIP)
            .log("start build moving slip")
            .split(body())
                .choice()
                .when()
                .simple("${body.qty} > 0")
                    .log("start set qty booking")
                        .bean(movingSlipBean, "setQtyBookingInventoryItem")
                    .log("finish set qty booking")
                    .log("start split inventory item")
                        .bean(movingSlipBean, "buildNewSplitInventoryItem")
                    .log("finish split inventory item")
                    .log("start create moving slip")
                        .bean(movingSlipBean, "buildMovingSlip")
                    .log("finish create moving slip")
                .end()
            .log("finish build moving slip");

        from(ACTION_UPLOAD_SHIPPING_LIST)
            .log("start convert UploadSLDTO to PackageReceipt with data ${body}")
                .bean(shipmentBean, "convertUploadShippingListToPackageReceipt")
            .log("finish convert UploadSLDTO to PackageReceipt with data ${body}")
            .log("start build package receipt")
                .bean(shipmentBean, "buildPackageReceipt")
            .log("finish build package receipt")
            .log("process upload shipping list done");

        from(ACTION_APPROVE_PACKAGE_RECEIPT)
            .log("cek ${header[currentStatus]}")
            .choice()
                .when(header("currentStatus").in(BaseConstants.STATUS_OPEN))
                    .log("start build Shipment Incoming")
                    .bean(shipmentBean, "buildShipmentIncoming")
                    .log("finish build Shipment Incoming")
                    .log("start checking completion")
                    .bean(shipmentBean, "completePackageReceipt")
                    .log("finish checking completion")
                .otherwise()
                    .log("cannot be processed")
            .end();

        from(ACTION_UPLOAD_INVOICE)
            .log("start process upload invoice")
                .log("start build billing disbursement")
                    .to(ACTION_BUILD_BILLING_DISBURSEMENT_FROM_UPLOAD_INVOICE)
                .log("finish build billing disbursement")
            .log("process upload invoice done");

        from(ACTION_BUILD_PURCHASE_ORDER_FROM_UPLOAD_INVOICE)
            .log("start convert UploadInvoiceDTO to PurchaseOrder with data ${body}")
                .bean(orderBean, "convertBillingDisbursementToPurchaseOrder")
            .log("finish convert dto to PurchaseOrder");

        from(ACTION_BUILD_BILLING_DISBURSEMENT_FROM_UPLOAD_INVOICE)
            .log("start convert and build UploadInvoiceDTO to BillingDisbursement with data ${body}")
                .bean(billingBean, "convertUploadInvoiceToBillingDisbursement")
                .onCompletion()
            .log("finish convert and build dto to BillingDisbursement")
            .log("start build purchase order")
                .to(ACTION_BUILD_PURCHASE_ORDER_FROM_UPLOAD_INVOICE)
            .log("finish build purchase order");
    }
}
