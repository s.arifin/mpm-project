package id.atiila.route;

import id.atiila.base.BaseConstants;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class PersonSalesUnitRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("timer:foo?period=15m")
            .transform()
            .simple("Check Is Camel Running ${date:now:yyyy-MM-dd HH:mm:ss}")
            .to("stream:out");

        from("vm:sur:state:change")
            .choice()
                // Status = Draft
                .when(header("currentStatus").in(BaseConstants.STATUS_NOT_DEFINED, BaseConstants.STATUS_DRAFT))
                    .log("Sales Processed to Process")
                // Status = Draft
                .when(header("currentStatus").in(BaseConstants.STATUS_NOT_DEFINED))
                    .log("Sales Processed to Activated")
                .otherwise()
                    .log("Sur tidak di proses")
            .end();


        from("direct:firstRoute")
            .to("stream:out");

    }
}
