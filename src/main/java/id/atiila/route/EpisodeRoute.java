package id.atiila.route;

import id.atiila.base.BaseConstants;
import id.atiila.service.EpisodeTemplateService;
import id.atiila.service.impl.ProductPurchaseImpl;
import id.atiila.service.impl.ShipmentBeanImpl;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EpisodeRoute extends RouteBuilder {

    public static String ACTION_BUILD_RECEIVING = "direct:episode:100";
    public static String ACTION_BUILD_PART_RECEIVING = "direct:episode:101";
    public static String ACTION_ORDER_PART = "direct:episode:102";

    @Autowired
    private ProductPurchaseImpl productPurchase;

    @Autowired
    private ShipmentBeanImpl shipmentBean;

    @Override
    public void configure() throws Exception {

        from(ACTION_BUILD_RECEIVING)
            .choice()
            // Status = Draft
            .when(header("currentStatus").in(BaseConstants.STATUS_NOT_DEFINED, BaseConstants.STATUS_DRAFT))
                .bean(shipmentBean, "buildPackageReceipt")
                .bean(shipmentBean, "buildShipmentIncoming")
                .bean(shipmentBean, "buildPurchaseOrder")
                .bean(shipmentBean, "buildPurchaseInvoice")
                .log("Sales Processed to Process")
            // Status = Active
            .when(header("currentStatus").in(BaseConstants.STATUS_ACTIVE))
                .log("Sales Processed to Activated")
            .otherwise()
                .log("Sur tidak di proses")
            .end()
        .log("Process build unit receiving done");


        from(ACTION_BUILD_PART_RECEIVING)
            .bean(shipmentBean, "buildPackageReceipt")
            .bean(shipmentBean, "buildShipmentIncoming")
            .bean(shipmentBean, "buildPurchaseOrder")
            .bean(shipmentBean, "buildPurchaseInvoice")
            .log("Sales Processed to Process");

        from(ACTION_ORDER_PART)
            .bean(productPurchase, "readOrder")
            .bean(productPurchase, "receiveOrder")
            .bean(productPurchase, "buildShipment")
            .bean(productPurchase, "buildBilling")
            .log("Sales Processed to Process");
    }


}
