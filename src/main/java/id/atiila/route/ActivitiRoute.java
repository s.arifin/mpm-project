package id.atiila.route;

import id.atiila.service.ActivitiProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ActivitiRoute extends RouteBuilder {

    @Autowired
    private ActivitiProcessor activitiProcessor;

    @Override
    public void configure() throws Exception {

        from("direct:activiti-process").to("jms:queu:activiti-process");
        from("jms:queu:activiti-process").bean(activitiProcessor, "receiveCamelCreateProcess");
    }

}
