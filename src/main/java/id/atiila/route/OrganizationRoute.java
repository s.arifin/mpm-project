package id.atiila.route;

import id.atiila.service.InternalService;
import id.atiila.service.PersonalCustomerService;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrganizationRoute extends RouteBuilder {

    // public static String BUILD_CUSTOMER = "direct:build:100";
    public static String BUILD_MSO = "direct:build:101";

    @Autowired
    private PersonalCustomerService personalCustomerService;

    @Override
    public void configure() throws Exception {

        from(BUILD_MSO)
            .to("jms:" + InternalService.QUEU_APPEND);

        from(PersonalCustomerService.QUEU_APPEND)
            .bean(personalCustomerService, "addPersonalCustomer");

    }

}
