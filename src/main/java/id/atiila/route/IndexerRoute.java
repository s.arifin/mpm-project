package id.atiila.route;

import id.atiila.base.BaseConstants;
import id.atiila.domain.VehicleSalesBilling;
import id.atiila.service.InventoryItemService;
import id.atiila.service.ProspectPersonService;
import id.atiila.service.VehicleSalesBillingService;
import id.atiila.service.VehicleSalesOrderService;
import id.atiila.service.impl.ProductPurchaseImpl;
import id.atiila.service.impl.ShipmentBeanImpl;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IndexerRoute extends RouteBuilder {

    public static String INDEXER_VSB = "direct:indexer:100";
    public static String QUEU_INDEXER_VSB = "jms:queu:vsb-100";

    public static String INDEXER_VSO = "direct:indexer:101";

    public static String INDEXER_PROSPECT = "direct:indexer:102";
    public static String QUEU_INDEXER_PROSPECT = "jms:queu:vsb-102";

    public static String INDEXER_INVENTORY = "direct:indexer:103";
    public static String QUEU_INDEXER_INVENTORY = "jms:queu:inv-item-101";

    @Autowired
    private VehicleSalesBillingService vehicleSalesBillingService;

    @Autowired
    private VehicleSalesOrderService vehicleSalesOrderService;

    @Autowired
    private ProspectPersonService prospectPersonService;

    @Autowired
    private InventoryItemService inventoryItemService;

    @Override
    public void configure() throws Exception {
        from("quartz2://sdms/checkQuartz2?cron=0+0/10+*+?+*+*")
            .transform()
            .simple("Check Is Cron Running ${date:now:yyyy-MM-dd HH:mm:ss}")
            .to("stream:out");

        from("quartz2://sdms/vsbindexer?cron=0+0/5+*+?+*+*")
            .bean(vehicleSalesBillingService, "cronUpdate");

        from("quartz2://sdms/prospectindexer?cron=0+0/5+*+?+*+*")
            .bean(prospectPersonService, "cronUpdate");

//        from("quartz2://sdms/inventoryindexer?cron=0+0/5+*+?+*+*")
//            .bean(inventoryItemService, "cronUpdate");

        from(INDEXER_VSB)
            .to(QUEU_INDEXER_VSB);

        from(QUEU_INDEXER_VSB)
            .bean(vehicleSalesBillingService, "reindexer");

        from(INDEXER_VSO)
            .to("jms:" + INDEXER_VSO);

        from("jms:" + INDEXER_VSO)
            .bean(vehicleSalesBillingService, "reindexer");

        from(INDEXER_PROSPECT)
            .to(QUEU_INDEXER_PROSPECT);

        from(QUEU_INDEXER_PROSPECT)
            .bean(prospectPersonService, "reindexer");

        from(INDEXER_INVENTORY)
            .to(QUEU_INDEXER_INVENTORY);

        from(QUEU_INDEXER_INVENTORY)
            .bean(inventoryItemService, "reindexer");

        from("direct:reindex:vehicleregistration").to("jms:queu:indexer:vehicleregistration");

        from("jms:queu:indexer:vehicleregistration").to("bean:vehicleRegistrationService?method=buildIndexItem(${body})");
    }

}
