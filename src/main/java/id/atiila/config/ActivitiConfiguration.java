package id.atiila.config;

import id.atiila.config.tenant.AtiilaTenantInfoHolder;
import org.activiti.engine.impl.cfg.multitenant.TenantInfoHolder;
import org.activiti.spring.SpringAsyncExecutor;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.activiti.spring.boot.AbstractProcessEngineAutoConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.io.IOException;

/*
   look setting at org.activiti.spring.boot.ActivitiProperties
 */


@Configuration
public class ActivitiConfiguration extends AbstractProcessEngineAutoConfiguration {

    private final Logger log = LoggerFactory.getLogger(ActivitiConfiguration.class);

    @Bean
    public TenantInfoHolder tenantInfoHolder() {
        return new AtiilaTenantInfoHolder();
    }

    /*
    @Bean(name = "activitiDS")
    @ConfigurationProperties(prefix = "spring.activiti.datasource")
    public DataSource activitiDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "activitiTX")
    public PlatformTransactionManager activitiPlatformTransactionManager(@Qualifier("activitiDS") DataSource ds) {
        return new DataSourceTransactionManager(ds);
    }

    @Bean
    public SpringProcessEngineConfiguration springProcessEngineConfiguration(
        @Qualifier("activitiDS") DataSource ds,
        @Qualifier("activitiTX") PlatformTransactionManager transactionManager,
        SpringAsyncExecutor springAsyncExecutor) throws IOException {
        return baseSpringProcessEngineConfiguration(
            ds,
            transactionManager,
            springAsyncExecutor);
    }
    */

}
