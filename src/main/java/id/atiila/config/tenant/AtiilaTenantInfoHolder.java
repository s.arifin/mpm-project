package id.atiila.config.tenant;

import org.activiti.engine.impl.cfg.multitenant.TenantInfoHolder;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class AtiilaTenantInfoHolder implements TenantInfoHolder {

    private Set<String> tenants = new HashSet<>();

    private String currentTenant;

    @Override
    public Collection<String> getAllTenants() {
        return this.tenants;
    }

    @Override
    public void setCurrentTenantId(String tenantid) {
        this.currentTenant = tenantid;
    }

    @Override
    public String getCurrentTenantId() {
        return this.currentTenant;
    }

    @Override
    public void clearCurrentTenantId() {
        this.currentTenant = null;
    }
}
