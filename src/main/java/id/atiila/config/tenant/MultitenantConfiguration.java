package id.atiila.config.tenant;

import id.atiila.config.ApplicationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Configuration
public class MultitenantConfiguration {

    @Autowired
    private DataSourceProperties properties;

    @Autowired
    private ApplicationProperties applicationProperties;

    /**
     * Defines the data source for the application
     *
     * @return
     */

    //@Bean
    //@Primary
    //@ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource() {

        Map<Object, Object> resolvedDataSources = new HashMap<>();

        if (new File(applicationProperties.getTenantPath()).exists()) {
            File[] files = Paths.get(applicationProperties.getTenantPath()).toFile().listFiles();

            if (files != null) {
                for (File propertyFile : files) {
                    Properties tenantProperties = new Properties();

                    DataSourceBuilder dsb = new DataSourceBuilder(this.getClass().getClassLoader());
                    try {
                        tenantProperties.load(new FileInputStream(propertyFile));
                        String tenantId = tenantProperties.getProperty("name");
                        dsb
                            .driverClassName(properties.getDriverClassName())
                            .url(tenantProperties.getProperty("datasource.url"))
                            .username(tenantProperties.getProperty("datasource.username"))
                            .password(tenantProperties.getProperty("datasource.password"));

                        //Mengikuti Type Datasource default
                        if (properties.getType() != null) {
                            dsb.type(properties.getType());
                        }

                        //Put pada tenant Datasource
                        resolvedDataSources.put(tenantId, dsb.build());

                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            }
        }

        //Pilih Database dari Set yang di definisikan

        DataSourceBuilder dsb = new DataSourceBuilder(this.getClass().getClassLoader());
        dsb.driverClassName(properties.getDriverClassName())
           .url(properties.getUrl())
           .username(properties.getUsername())
           .password(properties.getPassword());

        if (properties.getType() != null) {
            dsb.type(properties.getType());
        }

        MultitenantDataSource dataSource = new MultitenantDataSource();
        dataSource.setDefaultTargetDataSource(dsb.build());
        dataSource.setTargetDataSources(resolvedDataSources);
        dataSource.afterPropertiesSet();
        return dataSource;
    }

    /**
     * Creates the default data source for the application
     *
     * @return
     */
    private DataSource defaultDataSource() {
        DataSourceBuilder dataSourceBuilder =
                new DataSourceBuilder(this.getClass().getClassLoader())
                        .driverClassName(properties.getDriverClassName())
                        .url(properties.getUrl())
                        .username(properties.getUsername())
                        .password(properties.getPassword());

        if (properties.getType() != null) {
            dataSourceBuilder.type(properties.getType());
        }

        return dataSourceBuilder.build();
    }

}
