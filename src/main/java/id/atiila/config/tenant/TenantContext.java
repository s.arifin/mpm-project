package id.atiila.config.tenant;

public class TenantContext {
    private static ThreadLocal<String> currentTenant = new ThreadLocal<>();

    public static void setCurrentTenant(String tenant) {
        currentTenant.set(tenant);
    }

    public static Object getCurrentTenant() {
        return currentTenant == null ? "default" : currentTenant.get();
    }

    public static void clear() {
        currentTenant.remove();
    }
}
