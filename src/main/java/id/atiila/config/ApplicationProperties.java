package id.atiila.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;

/**
 * Properties specific to Mpm.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private String tenantPath;

    private String hzConfig;

    private String uploadPath;

    private String axUrl;

    public String getTenantPath() {
        return tenantPath;
    }

    public void setTenantPath(String tenantPath) {
        this.tenantPath = tenantPath;
    }

    public String getHzConfig() {
        return hzConfig;
    }

    public void setHzConfig(String hzConfig) {
        this.hzConfig = hzConfig;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public String getAxUrl() {
        return axUrl;
    }

    public void setAxUrl(String axUrl) {
        this.axUrl = axUrl;
    }
}
