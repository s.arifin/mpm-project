package id.atiila.repository;

import id.atiila.domain.MasterNumbering;

import java.util.UUID;

import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MasterNumbering entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface MasterNumberingRepository extends JpaRepository<MasterNumbering, UUID> {

    MasterNumbering findOneByIdTagAndIdValue(String idTag, String idValue);

}
