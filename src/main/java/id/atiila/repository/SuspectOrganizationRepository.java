package id.atiila.repository;

import id.atiila.domain.SuspectOrganization;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the SuspectOrganization entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface SuspectOrganizationRepository extends JpaRepository<SuspectOrganization, UUID> {
    @Query("select r from SuspectOrganization r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<SuspectOrganization> findActiveSuspectOrganization(Pageable pageable);
    
}

