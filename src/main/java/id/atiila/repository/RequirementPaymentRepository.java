package id.atiila.repository;

import id.atiila.domain.RequirementPayment;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the RequirementPayment entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface RequirementPaymentRepository extends JpaRepository<RequirementPayment, UUID> {
    
    @Query("select r from RequirementPayment r where (r.requirement.idRequirement = :idRequirement)")
    Page<RequirementPayment> findByIdRequirement(@Param("idRequirement") UUID idRequirement, Pageable pageable); 
    
    @Query("select r from RequirementPayment r where (r.payment.idPayment = :idPayment)")
    Page<RequirementPayment> findByIdPayment(@Param("idPayment") UUID idPayment, Pageable pageable); 

}

