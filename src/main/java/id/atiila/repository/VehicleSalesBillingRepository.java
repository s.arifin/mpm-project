package id.atiila.repository;

import id.atiila.domain.*;

import java.util.List;
import java.util.UUID;

import id.atiila.service.pto.OrderPTO;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.annotation.Nullable;

/**
 * Spring Data JPA repository for the VehicleSalesBilling entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface VehicleSalesBillingRepository extends JpaRepository<VehicleSalesBilling, UUID> {

    @Query("select r from VehicleSalesBilling r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (17)) " +
        "      and (r.internal = :internal) " +
        "      order by r.dateCreate desc ")
    Page<VehicleSalesBilling> findActiveVehicleSalesBilling(@Param("internal") Internal intr, Pageable pageable);


    @Query("select r from VehicleSalesBilling r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13,17)) " +
        "      and (r.internal = :internal) " +
        "      order by r.dateCreate desc ")
    Page<VehicleSalesBilling> findActiveVehicleSalesBillingLOV(@Param("internal") Internal intr, Pageable pageable);

    @Query(" select r from VehicleSalesBilling r join r.statuses s " +
           " where (current_timestamp between s.dateFrom and s.dateThru) " +
           " and (s.idStatusType not in (13, 17))" +
           " and (r.billingNumber like :billingNumber) ")
    Page<VehicleSalesBilling> findActiveVehicleSalesBillingByBillingNumber(@Param("billingNumber") String billingNumber, Pageable pageable);

    @Query("select r from VehicleSalesBilling r where (r.billingNumber = :billingNumber)")
    List<VehicleSalesBilling> findByBillingNumber(@Param("billingNumber") String billingNumber);

    @Query("select r from VehicleSalesBilling r where (r.billTo = :billTo)")
    List<VehicleSalesBilling> findByBillTo(@Param("billTo") BillTo billTo);

    // search untuk req faktur pajak
    @Query("select b from VehicleSalesBilling  b left join b.details d " +
           " where d.idItemType = 10 ")
    Page<VehicleSalesBilling> findVsbForInvTax(Pageable pageable);

    @Query("select r from VehicleSalesBilling r where (1 = 2)")
    Page<VehicleSalesBilling> queryNothing(Pageable page);

    @Query(" select r from VehicleSalesBilling r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "    and (r.dateCreate between :#{#param.orderDateFrom} and :#{#param.orderDateThru}) " +
//        "    and (s.idStatusType in :#{#param.status}) " +
//        "    and (s.idStatusType not in :#{#param.statusNotIn}) " +
        "    and ((r.internal.idInternal = :#{#param.internalId}) or (null = :#{#param.internalId}))" +
        "    order by r.dateCreate desc")
    Page<VehicleSalesBilling> queryByOrderPTO(Pageable page, @Param("param") OrderPTO params);

    @Query(" select r from VehicleSalesBilling r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "    and ((r.requestTaxInvoice = :#{#param.statusFaktur}) or (null = :#{#param.statusFaktur}))" +
        "    and (r.dateCreate between :#{#param.orderDateFrom} and :#{#param.orderDateThru}) " +
        "    and ((r.internal.idInternal = :#{#param.internalId}) or (null = :#{#param.internalId}))" +
        "    order by r.dateCreate desc")
    Page<VehicleSalesBilling> queryFakturPTO(Pageable page, @Param("param") OrderPTO params);

    @Query(" select r from VehicleSalesBilling r join r.statuses s " +
        "   join Person p on r.customer.party.idParty = p.idParty" +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "    and ((r.billingNumber like :param) or (r.internal.idInternal like :param) or (p.firstName like :param) or (p.lastName like :param))" +
        "    order by r.dateCreate desc")
    Page<VehicleSalesBilling> querySearchFaktur(Pageable page, @Param("param") String param);

    @Query("select r from VehicleSalesBilling r where (r.customer.idCustomer = :idCustomer)")
    Page<VehicleSalesBilling> queryByCustomer(@Param("idCustomer") String idCustomer, Pageable page);

    @Query("select r from VehicleSalesBilling r where (r.billingNumber = :billingNumber)")
    Page<VehicleSalesBilling> queryByOrderNumber(@Param("billingNumber") String billingNumber, Pageable page);

    @Query(" select r from VehicleSalesBilling r left join fetch r.statuses s join fetch r.customer cus" +
           " left join fetch r.internal intrn left join fetch cus.party p join PersonalCustomer q on (q = cus)" +
           " where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType not in (17))")
    List<VehicleSalesBilling> findAllEager();

    @Query(" select d.inventoryItem from VehicleSalesBilling r join r.statuses s join r.details d " +
        " where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType not in (13, 17))" +
        "   and (r.idBilling = :idBilling)")
    List<InventoryItem> getInventories(@Param("idBilling") UUID idBilling);

    @Query("select r from VehicleSalesBilling r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru ) and (s.idStatusType = 10) " +
        "      and (r.refKey = :refKey) " +
        "      and (r.customer = :customer) " +
        "      and (r.billFrom = :billFrom) " +
        "      and (r.billTo = :billTo) " +
        "      and (r.internal = :internal) ")
    Page<VehicleSalesBilling> getDraftBilling(@Param("internal") Internal internal,
                                              @Param("billFrom") BillTo billFrom,
                                              @Param("billTo") BillTo billTo,
                                              @Param("customer") Customer customer,
                                              @Param("refKey") String refkey,
                                              Pageable pageable);

    @Query("select r from VehicleSalesBilling r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13)) " +
        "      order by r.dateCreate desc ")
    Page<VehicleSalesBilling> findActiveVehicleSalesBillingTaxList(Pageable pageable);

    @Query("select r from VehicleSalesBilling r " +
        "   join r.statuses rs" +
        "   where rs.idStatusType not in (13) " +
        "   and current_timestamp between rs.dateFrom and rs.dateThru" +
        "   and r.internal = :internal" +
        "   and r not in ( select m.billing from Memos m join m.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and s.idStatusType in (10))" +
        "   order by r.dateCreate desc")
    Page<VehicleSalesBilling> findForMemo(@Param("internal") Internal internal, Pageable pageable);

//    @Query(" select vso from VehicleSalesOrder vso" +
//        " join OrderItem oi on vso.idOrder = oi.orders  " +
//        " join oi.billingItems ob join ob.billingItem bi " +
//        " join oi.orders o " +
//        " where bi.billing.billingNumber = :billingNumber ")
//    Page<VehicleSalesOrder> queryByNoIvu(@Param("billingNumber") String billingNumber, Pageable page);

    @Query("select r from VehicleSalesBilling  r " +
        "   join r.statuses s" +
        "   join BillingItem bi on r = bi.billing " +
        "   join OrderBillingItem obi on bi = obi.billingItem " +
        "   join OrderItem oi on obi.orderItem = oi " +
        "   join VehicleSalesOrder vso on oi.orders = vso " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "    and ((vso.idSalesman = :#{#param.salesmanId}) or (null = :#{#param.salesmanId})) " +
        "    and (r.dateCreate between :#{#param.orderDateFrom} and :#{#param.orderDateThru}) " +
        "    and ((vso.internal.idInternal = :#{#param.internalId}) or (null = :#{#param.internalId}))" +
        "    and ((vso.leasing.idPartyRole = :#{#param.leasingCompanyId}) or (null = :#{#param.leasingCompanyId}))" +
        "    order by r.dateCreate desc")
    Page<VehicleSalesBilling> queryvsbPTO(Pageable page, @Param("param") OrderPTO params);

    @Query("select r from VehicleSalesBilling  r " +
        "   join r.statuses s " +
        "   where current_timestamp between s.dateFrom and s.dateThru and r.internal =:internal " +
        "   and (r.customer.party.name like :data or r.billingNumber like :data or r.description like :data)")
    Page<VehicleSalesBilling> queryForSearch(@Param("data") String data, @Param("internal") Internal internal, Pageable pageable);
}

