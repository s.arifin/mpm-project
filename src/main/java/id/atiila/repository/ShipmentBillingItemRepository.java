package id.atiila.repository;

import id.atiila.domain.BillingItem;
import id.atiila.domain.ShipmentBillingItem;

import java.util.List;
import java.util.UUID;

import id.atiila.domain.ShipmentItem;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ShipmentBillingItem entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ShipmentBillingItemRepository extends JpaRepository<ShipmentBillingItem, UUID> {

    @Query("select r from ShipmentBillingItem r "
	    + " where ((r.shipmentItem.idShipmentItem = :idShipmentItem) or (:idShipmentItem is null)) "
	    + " and ((r.billingItem.idBillingItem = :idBillingItem) or (:idBillingItem is null)) ")
    Page<ShipmentBillingItem> findByParams(@Param("idShipmentItem") String idShipmentItem, @Param("idBillingItem") String idBillingItem, Pageable pageable);

    @Query(value = "select r from ShipmentBillingItem r where r.billingItem.billing.idBilling = :idBilling ")
    List<ShipmentBillingItem> findByIdBilling(@Param("idBilling") UUID idBilling);

    @Query("select r from ShipmentBillingItem r where (r.shipmentItem = :shipmentItem) and (r.billingItem = :billingItem) ")
    ShipmentBillingItem getShipmentBillingItem(@Param("shipmentItem") ShipmentItem shipmentItem, @Param("billingItem") BillingItem billingItem);
}

