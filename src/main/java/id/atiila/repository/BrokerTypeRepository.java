package id.atiila.repository;

import id.atiila.domain.BrokerType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the BrokerType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface BrokerTypeRepository extends JpaRepository<BrokerType, Integer> {
}

