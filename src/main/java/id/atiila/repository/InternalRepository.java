package id.atiila.repository;

import id.atiila.domain.Branch;
import id.atiila.domain.Internal;

import id.atiila.domain.Organization;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Internal entity.
 * BeSmart Team
 */
@Repository
public interface InternalRepository<T extends Internal> extends JpaRepository<T, String> {

    T findByIdInternal(String idInternal);

    T findByOrganization(Organization o);

    @Query("select r.internal from VendorRelationship r where (r.vendor.idVendor = :idVendor)")
    Page<Branch> queryByVendor(@Param("idVendor") String idVendor, Pageable pageable);

    @Query("select r from Internal r where (1=2)")
    Page<Branch> queryNothing(Pageable pageable);

    @Query("select r from Internal r where r.root.idInternal =:idRoot")
    Page<Internal> FindByRoot(@Param("idRoot") String idRoot, Pageable pageable);

}

