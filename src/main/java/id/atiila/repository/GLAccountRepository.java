package id.atiila.repository;

import id.atiila.domain.GLAccount;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the GLAccount entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface GLAccountRepository extends JpaRepository<GLAccount, UUID> {
    
    @Query("select r from GLAccount r where (r.accountType.idGLAccountType = :idGLAccountType)")
    Page<GLAccount> queryByIdGLAccountType(@Param("idGLAccountType") Integer idGLAccountType, Pageable pageable); 

	@Query("select r from GLAccount r where (1=2)")
    Page<GLAccount> queryNothing(Pageable pageable); 
}

