package id.atiila.repository;

import id.atiila.domain.FacilityContactMechanism;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the FacilityContactMechanism entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface FacilityContactMechanismRepository extends JpaRepository<FacilityContactMechanism, UUID> {
    
    @Query("select r from FacilityContactMechanism r where (r.facility.idFacility = :idFacility)")
    Page<FacilityContactMechanism> findByIdFacility(@Param("idFacility") UUID idFacility, Pageable pageable); 
    
    @Query("select r from FacilityContactMechanism r where (r.contact.idContact = :idContact)")
    Page<FacilityContactMechanism> findByIdContact(@Param("idContact") UUID idContact, Pageable pageable); 

}

