package id.atiila.repository;

import id.atiila.domain.ProductDocument;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ProductDocument entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ProductDocumentRepository extends JpaRepository<ProductDocument, UUID> {
    @Query("select p from ProductDocument p where p.product.idProduct = :idproduct")
    List<ProductDocument> findAllProductDocumentByIdProduct(
        @Param("idproduct") String idproduct
    );
}

