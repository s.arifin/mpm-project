package id.atiila.repository;

import id.atiila.domain.InternalOrder;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the InternalOrder entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface InternalOrderRepository extends JpaRepository<InternalOrder, UUID> {
}

