package id.atiila.repository;

import id.atiila.domain.SalesUnitLeasing;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SalesUnitLeasing entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface SalesUnitLeasingRepository extends JpaRepository<SalesUnitLeasing, UUID> {
}

