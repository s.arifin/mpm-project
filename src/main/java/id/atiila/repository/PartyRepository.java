package id.atiila.repository;

import id.atiila.domain.Party;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Party entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface PartyRepository extends JpaRepository<Party, UUID> {

    @Query("select distinct party from Party party left join fetch party.categories")
    List<Party> findAllWithEagerRelationships();

    @Query("select party from Party party left join fetch party.categories where party.idParty = :id")
    Party findOneWithEagerRelationships(@Param("id") UUID id);

}

