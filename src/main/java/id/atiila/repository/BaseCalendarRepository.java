package id.atiila.repository;

import id.atiila.domain.BaseCalendar;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the BaseCalendar entity.
 * BeSmart Team
 */@SuppressWarnings("unused")
@Repository
public interface BaseCalendarRepository extends JpaRepository<BaseCalendar, Integer> {
}

