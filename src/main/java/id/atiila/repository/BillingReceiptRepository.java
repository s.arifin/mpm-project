package id.atiila.repository;

import id.atiila.domain.BillTo;
import id.atiila.domain.BillingReceipt;

import java.util.List;
import java.util.UUID;

import id.atiila.domain.Internal;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the BillingReceipt entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface BillingReceiptRepository extends JpaRepository<BillingReceipt, UUID> {

    @Query("select r from BillingReceipt r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (r.internal = :internal)" +
        "      and (s.idStatusType not in (13, 17))")
    Page<BillingReceipt> findActiveBillingReceipt(@Param("internal") Internal intr, Pageable pageable);

    @Query("select r from BillingReceipt r "
	    + "  where ((r.billingType.idBillingType = :idBillingType) or (:idBillingType is null)) "
	    + "    and ((r.internal.idInternal = :idInternal) or (:idInternal is null)) "
	    + "    and ((r.billTo.idBillTo = :idBillTo) or (:idBillTo is null)) "
	    + "    and ((r.billFrom.idBillTo = :idBillFrom) or (:idBillFrom is null)) ")
    Page<BillingReceipt> findByParams(@Param("idBillingType") String idBillingType, @Param("idInternal") String idInternal, @Param("idBillTo") String idBillTo, @Param("idBillFrom") String idBillFrom, Pageable pageable);

    @Query("select r from BillingReceipt r join r.statuses s left join fetch r.details d" +
        "    where (current_timestamp between s.dateFrom and s.dateThru ) and (s.idStatusType = 10) " +
        "      and (r.billFrom = :billFrom) " +
        "      and (r.billTo = :billTo) ")
    List<BillingReceipt> getDraftBilling(@Param("billFrom") BillTo billFrom, @Param("billTo") BillTo billTo);

}
