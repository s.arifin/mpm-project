package id.atiila.repository;

import id.atiila.domain.BookingSlotStandard;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the BookingSlotStandard entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface BookingSlotStandardRepository extends JpaRepository<BookingSlotStandard, UUID> {
}

