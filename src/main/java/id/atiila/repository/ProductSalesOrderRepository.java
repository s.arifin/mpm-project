package id.atiila.repository;

import id.atiila.domain.Internal;
import id.atiila.domain.ProductSalesOrder;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ProductSalesOrder entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ProductSalesOrderRepository extends JpaRepository<ProductSalesOrder, UUID> {

    @Query("select r from ProductSalesOrder r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (r.internal = :internal)" +
        "      and (s.idStatusType not in (13, 17))")
    Page<ProductSalesOrder> findActive(@Param("internal") Internal intr, Pageable pageable);

    @Query("select r from ProductSalesOrder r "
	    + " where ((r.internal.idInternal = :idInternal) or (:idInternal is null)) "
	    + " and ((r.customer.idCustomer = :idCustomer) or (:idCustomer is null)) "
	    + " and ((r.billTo.idBillTo = :idBillTo) or (:idBillTo is null)) "
	    + " and ((r.saleType.idSaleType = :idSaleType) or (:idSaleType is null)) ")
    Page<ProductSalesOrder> findByParams(@Param("idInternal") String idInternal, @Param("idCustomer") String idCustomer, @Param("idBillTo") String idBillTo, @Param("idSaleType") String idSaleType, Pageable pageable);

}

