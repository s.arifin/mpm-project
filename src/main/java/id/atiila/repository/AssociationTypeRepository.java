package id.atiila.repository;

import id.atiila.domain.AssociationType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

  
/**
 * Spring Data JPA repository for the AssociationType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface AssociationTypeRepository extends JpaRepository<AssociationType, Integer> {
}

