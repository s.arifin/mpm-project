package id.atiila.repository;

import id.atiila.domain.Branch;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Branch entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface BranchRepository extends InternalRepository<Branch> {

    @Query("select b from VendorRelationship r join Branch b on (b = r.internal) where (r.vendor.idVendor = :idVendor)")
    Page<Branch> queryByVendor(@Param("idVendor") String idVendor, Pageable pageable);

    @Query("select r from Branch r where (1=2)")
    Page<Branch> queryNothing(Pageable pageable);
}

