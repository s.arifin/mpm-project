package id.atiila.repository;

import id.atiila.domain.Requirement;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Requirement entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface RequirementRepository extends JpaRepository<Requirement, UUID> {


    @Query("select distinct requirement from Requirement requirement")
    List<Requirement> findAllWithEagerRelationships();

    @Query("select requirement from Requirement requirement where requirement.idRequirement = :id")
    Requirement findOneWithEagerRelationships(@Param("id") UUID id);

    @Query("select r from Requirement r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<Requirement> findActiveRequirement(Pageable pageable);

    Requirement findOneByRequirementNumber(String id);

}

