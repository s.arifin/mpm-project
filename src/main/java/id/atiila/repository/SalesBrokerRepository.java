package id.atiila.repository;

import id.atiila.domain.Party;
import id.atiila.domain.SalesBroker;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SalesBroker entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface SalesBrokerRepository extends JpaRepository<SalesBroker, UUID> {

    SalesBroker findOneByIdBroker(String id);

    SalesBroker findOneByParty(Party p);

    @Query("select r from SalesBroker r where (r.brokerType.idBrokerType = :idbrokertype)")
    Page<SalesBroker> findAllByIdBrokerType(@Param("idbrokertype") Integer idBrokerType, Pageable pageable);

}

