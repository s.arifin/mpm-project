package id.atiila.repository;

import id.atiila.domain.VehicleCustomerRequest;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the VehicleCustomerRequest entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface VehicleCustomerRequestRepository extends JpaRepository<VehicleCustomerRequest, UUID> {

    @Query("select r from VehicleCustomerRequest r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17))")
    Page<VehicleCustomerRequest> findActiveVehicleCustomerRequest(Pageable pageable);

    @Query("select r from VehicleCustomerRequest r join r.statuses s " +
        "      where (r.internal.idInternal = :idInternal) " +
        "      and (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType not in (13, 17))")
    Page<VehicleCustomerRequest> queryByIdInternal(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from VehicleCustomerRequest r where (r.customer.idCustomer = :idCustomer)")
    Page<VehicleCustomerRequest> queryByIdCustomer(@Param("idCustomer") String idCustomer, Pageable pageable);

    @Query("select r from VehicleCustomerRequest r where (r.requestType.idRequestType = :idRequestType)")
    Page<VehicleCustomerRequest> queryByIdRequestType(@Param("idRequestType") Integer idRequestType, Pageable pageable);

	@Query("select r from VehicleCustomerRequest r where (1=2)")
    Page<VehicleCustomerRequest> queryNothing(Pageable pageable);
}

