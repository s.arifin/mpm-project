package id.atiila.repository;

import id.atiila.domain.MovingSlip;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the MovingSlip entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface MovingSlipRepository extends JpaRepository<MovingSlip, UUID> {
    @Query("select r from MovingSlip r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<MovingSlip> findActiveMovingSlip(Pageable pageable);

}

