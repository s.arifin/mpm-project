package id.atiila.repository;

import id.atiila.domain.UomConversion;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the UomConversion entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface UomConversionRepository extends JpaRepository<UomConversion, Integer> {
    
    @Query("select r from UomConversion r where (r.uomTo.idUom = :idUomTo)")
    Page<UomConversion> queryByIdUomTo(@Param("idUomTo") String idUomTo, Pageable pageable); 
    
    @Query("select r from UomConversion r where (r.uomFrom.idUom = :idUomFrom)")
    Page<UomConversion> queryByIdUomFrom(@Param("idUomFrom") String idUomFrom, Pageable pageable); 

	@Query("select r from UomConversion r where (1=2)")
    Page<UomConversion> queryNothing(Pageable pageable); 
}

