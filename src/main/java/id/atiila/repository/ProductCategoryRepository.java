package id.atiila.repository;

import id.atiila.domain.ProductCategory;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ProductCategory entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Integer> {

    @Query("select r from ProductCategory r where (r.categoryType.idCategoryType = :idCategoryType)")
    Page<ProductCategory> queryByIdCategoryType(@Param("idCategoryType") Integer idCategoryType, Pageable pageable);

	@Query("select r from ProductCategory r where (1=2)")
    Page<ProductCategory> queryNothing(Pageable pageable);

    Page<ProductCategory> findAllByCategoryTypeIdCategoryType(Integer id, Pageable pageable);
}

