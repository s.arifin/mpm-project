package id.atiila.repository;

import id.atiila.domain.Disbursement;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Disbursement entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface DisbursementRepository extends JpaRepository<Disbursement, UUID> {

    @Query("select r from Disbursement r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<Disbursement> findActiveDisbursement(Pageable pageable);
    
    @Query("select r from Disbursement r where (r.paymentType.idPaymentType = :idPaymentType)")
    Page<Disbursement> findByIdPaymentType(@Param("idPaymentType") Integer idPaymentType, Pageable pageable); 
    
    @Query("select r from Disbursement r where (r.method.idPaymentMethod = :idMethod)")
    Page<Disbursement> findByIdMethod(@Param("idMethod") Integer idMethod, Pageable pageable); 
    
    @Query("select r from Disbursement r where (r.internal.idInternal = :idInternal)")
    Page<Disbursement> findByIdInternal(@Param("idInternal") String idInternal, Pageable pageable); 
    
    @Query("select r from Disbursement r where (r.paidTo.idBillTo = :idPaidTo)")
    Page<Disbursement> findByIdPaidTo(@Param("idPaidTo") String idPaidTo, Pageable pageable); 
    
    @Query("select r from Disbursement r where (r.paidFrom.idBillTo = :idPaidFrom)")
    Page<Disbursement> findByIdPaidFrom(@Param("idPaidFrom") String idPaidFrom, Pageable pageable); 

}

