package id.atiila.repository;

import id.atiila.domain.Party;
import id.atiila.domain.Salesman;

import org.springframework.data.domain.Pageable;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Salesman entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface SalesmanRepository extends JpaRepository<Salesman, UUID> {

    Salesman findOneByParty(Party party);

    Salesman findOneByIdSalesman(String idCustomer);

    @Query("SELECT r FROM Salesman r WHERE " +
        "r.coordinatorSales.idPartyRole = :idcoordinator")
    Page<Salesman> findAllSalesmanByIdCoordinator(
        @Param("idcoordinator") UUID idcoordinator,
        Pageable pageable
    );

    @Query("SELECT r FROM Salesman r WHERE " +
        "r.coordinatorSales.idPartyRole = :idcoordinator")
    Page<Salesman> findAllSalesmanByIdkKorsal(
        @Param("idcoordinator") UUID idcoordinator,
        Pageable pageable
    );

    @Query("SELECT r FROM Salesman r WHERE " +
        "r.userName = :username")
    Page<Salesman> findAllSalesmanByUsername(
        @Param("username") String userName,
        Pageable pageable
    );

    @Query(" select r from Salesman r join r.party per join UserMediator um on um.person.idParty = per.idParty" +
           " join Position pos on pos.owner.idUserMediator = um.idUserMediator" +
           " where ((pos.internal.idInternal = :internal) or (:internal is null))" +
           " and ((pos.positionType.idPositionType = :positiontype) or (:positiontype is null))")
    Page<Salesman> findAllByInternal(@Param("internal") String idInternal, @Param("positiontype") Integer idPositionType, Pageable pageable);

    @Query("select r from Salesman r where r.idPartyRole = :idPartyRole")
    Salesman findCoordinator (@Param("idPartyRole") UUID idPartyRole);
}

