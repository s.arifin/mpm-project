package id.atiila.repository;

import id.atiila.domain.SalesSource;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SalesSource entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface SalesSourceRepository extends JpaRepository<SalesSource, Long> {


}

