package id.atiila.repository;

import id.atiila.domain.BillingItem;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the BillingItem entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface BillingItemRepository extends JpaRepository<BillingItem, UUID> {

    @Query("select r from BillingItem r where (r.billing.idBilling = :idBilling)")
    Page<BillingItem> findByByIdBilling(@Param("idBilling") UUID idBilling, Pageable pageable);

    @Query("select r from BillingItem r where (r.billing.idBilling = :idBilling) and r.idItemType=10 ")
    Page<BillingItem> findItemByIdBilling(@Param("idBilling") UUID idBilling, Pageable pageable);

    @Query("select r from BillingItem r where (r.billing.billingNumber like :billingNumber)")
    Page<BillingItem> queryItemByIVU(@Param("billingNumber") String billingNumber, Pageable pageable);

    @Query("select r from BillingItem r left join r.billing b left join b.billTo bt left join bt.party p " +
           "left join b.internal " +
           "where  r.idItemType=10 ")
    Page<BillingItem> findItemForTaxInvList(Pageable pageable);

    BillingItem findOneByBillingIdBillingAndIdProductAndIdFeature(UUID IdBilling, String IdProduct, Integer IdFeature);

    @Query("select r from BillingItem r " +
        "   join OrderBillingItem obi on r = obi.billingItem" +
        "   join OrderItem oi on obi.orderItem = oi" +
        "   where oi.idOrderItem = :idordite and r.sequence = 10")
    List<BillingItem> findBilliteByOrdite(@Param("idordite") UUID idordite);

    @Query("select r from BillingItem r where (1=2)")
    Page<BillingItem> queryNone(Pageable pageable);

    @Query("select r from BillingItem r " +
        "   where r.inventoryItem.idInventoryItem =:idInvite")
    List<BillingItem> findByIdInvite(@Param("idInvite") UUID idInvite);


}

