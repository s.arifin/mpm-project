package id.atiila.repository;

import id.atiila.domain.Requirement;
import id.atiila.domain.UnitReqInternal;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the UnitReqInternal entity.
 * BeSmart Team
 */

@Repository
public interface UnitReqInternalRepository extends JpaRepository<UnitReqInternal, UUID> {

    @Query("select r from UnitReqInternal r where (r.getCurrentStatus() = 11) and (r.qty > r.getQtyFilled())")
    List<UnitReqInternal> getUnfilledRequirement();

}

