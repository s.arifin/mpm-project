package id.atiila.repository.search;

import id.atiila.domain.Billing;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Billing entity.
 */
 
public interface BillingSearchRepository extends ElasticsearchRepository<Billing, UUID> {
}
