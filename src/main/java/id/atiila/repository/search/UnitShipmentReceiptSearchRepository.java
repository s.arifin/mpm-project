package id.atiila.repository.search;

import id.atiila.domain.UnitShipmentReceipt;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UnitShipmentReceipt entity.
 */
 
public interface UnitShipmentReceiptSearchRepository extends ElasticsearchRepository<UnitShipmentReceipt, UUID> {
}
