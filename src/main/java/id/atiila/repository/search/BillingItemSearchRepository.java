package id.atiila.repository.search;

import id.atiila.domain.BillingItem;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the BillingItem entity.
 */
 
public interface BillingItemSearchRepository extends ElasticsearchRepository<BillingItem, UUID> {
}
