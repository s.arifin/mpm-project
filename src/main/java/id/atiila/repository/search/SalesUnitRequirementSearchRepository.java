package id.atiila.repository.search;

import id.atiila.domain.SalesUnitRequirement;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SalesUnitRequirement entity.
 */
public interface SalesUnitRequirementSearchRepository extends ElasticsearchRepository<SalesUnitRequirement, UUID> {
}
