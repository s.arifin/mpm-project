package id.atiila.repository.search;

import id.atiila.domain.SalesUnitLeasing;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SalesUnitLeasing entity.
 */
public interface SalesUnitLeasingSearchRepository extends ElasticsearchRepository<SalesUnitLeasing, UUID> {
}
