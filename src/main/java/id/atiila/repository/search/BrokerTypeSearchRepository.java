package id.atiila.repository.search;

import id.atiila.domain.BrokerType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the BrokerType entity.
 */
public interface BrokerTypeSearchRepository extends ElasticsearchRepository<BrokerType, Integer> {
}
