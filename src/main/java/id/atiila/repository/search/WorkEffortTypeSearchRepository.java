package id.atiila.repository.search;

import id.atiila.domain.WorkEffortType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WorkEffortType entity.
 */
public interface WorkEffortTypeSearchRepository extends ElasticsearchRepository<WorkEffortType, Integer> {
}
