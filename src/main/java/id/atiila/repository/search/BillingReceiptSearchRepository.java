package id.atiila.repository.search;

import id.atiila.domain.BillingReceipt;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the BillingReceipt entity.
 */
 
public interface BillingReceiptSearchRepository extends ElasticsearchRepository<BillingReceipt, UUID> {
}
