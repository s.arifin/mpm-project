package id.atiila.repository.search;

import id.atiila.domain.GLDimension;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the GLDimension entity.
 */
 
public interface GLDimensionSearchRepository extends ElasticsearchRepository<GLDimension, Integer> {
}
