package id.atiila.repository.search;

import id.atiila.domain.PaymentApplication;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PaymentApplication entity.
 */
 
public interface PaymentApplicationSearchRepository extends ElasticsearchRepository<PaymentApplication, UUID> {
}
