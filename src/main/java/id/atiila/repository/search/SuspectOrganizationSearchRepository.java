package id.atiila.repository.search;

import id.atiila.domain.SuspectOrganization;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SuspectOrganization entity.
 */
public interface SuspectOrganizationSearchRepository extends ElasticsearchRepository<SuspectOrganization, UUID> {
}
