package id.atiila.repository.search;

import id.atiila.domain.VehicleSalesBilling;
import java.util.UUID;

import id.atiila.service.dto.VehicleSalesBillingDTO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the VehicleSalesBilling entity.
 */
public interface VehicleSalesBillingSearchRepository extends ElasticsearchRepository<VehicleSalesBillingDTO, UUID> {
}
