package id.atiila.repository.search;

import id.atiila.domain.Good;
import id.atiila.domain.InventoryItem;
import java.util.UUID;

import id.atiila.domain.Party;
import id.atiila.service.dto.InventoryItemDTO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data Elasticsearch repository for the InventoryItem entity.
 */
public interface InventoryItemSearchRepository extends ElasticsearchRepository<InventoryItemDTO, UUID> {


}


