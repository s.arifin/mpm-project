package id.atiila.repository.search;

import id.atiila.domain.OrderItem;
import id.atiila.service.dto.UnitOrderItemDTO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.UUID;

/**
 * Spring Data Elasticsearch repository for the OrderItem entity.
 */

public interface UnitOrderItemSearchRepository extends ElasticsearchRepository<UnitOrderItemDTO, UUID> {
}
