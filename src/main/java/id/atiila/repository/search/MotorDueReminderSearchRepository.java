package id.atiila.repository.search;

import id.atiila.domain.MotorDueReminder;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the MotorDueReminder entity.
 */
public interface MotorDueReminderSearchRepository extends ElasticsearchRepository<MotorDueReminder, Integer> {
}
