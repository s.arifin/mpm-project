package id.atiila.repository.search;

import id.atiila.domain.Feature;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Feature entity.
 */
 
public interface FeatureSearchRepository extends ElasticsearchRepository<Feature, Integer> {
}
