package id.atiila.repository.search;

import id.atiila.domain.ProspectSource;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ProspectSource entity.
 */
public interface ProspectSourceSearchRepository extends ElasticsearchRepository<ProspectSource, Integer> {
}
