package id.atiila.repository.search;

import id.atiila.domain.VehicleCustomerRequest;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the VehicleCustomerRequest entity.
 */
 
public interface VehicleCustomerRequestSearchRepository extends ElasticsearchRepository<VehicleCustomerRequest, UUID> {
}
