package id.atiila.repository.search;

import id.atiila.domain.UnitDocumentMessage;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UnitDocumentMessage entity.
 */

public interface UnitDocumentMessageSearchRepository extends ElasticsearchRepository<UnitDocumentMessage, Integer> {
}
