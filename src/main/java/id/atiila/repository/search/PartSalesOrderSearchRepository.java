package id.atiila.repository.search;

import id.atiila.domain.PartSalesOrder;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PartSalesOrder entity.
 */
 
public interface PartSalesOrderSearchRepository extends ElasticsearchRepository<PartSalesOrder, UUID> {
}
