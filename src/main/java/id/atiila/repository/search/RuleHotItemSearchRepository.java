package id.atiila.repository.search;

import id.atiila.domain.RuleHotItem;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RuleHotItem entity.
 */
public interface RuleHotItemSearchRepository extends ElasticsearchRepository<RuleHotItem, Integer> {
}
