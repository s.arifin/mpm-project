package id.atiila.repository.search;

import id.atiila.domain.GeneralUpload;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the GeneralUpload entity.
 */
 
public interface GeneralUploadSearchRepository extends ElasticsearchRepository<GeneralUpload, UUID> {
}
