package id.atiila.repository.search;

import id.atiila.domain.PartyRole;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PartyRole entity.
 */
public interface PartyRoleSearchRepository extends ElasticsearchRepository<PartyRole, UUID> {
}
