package id.atiila.repository.search;

import id.atiila.domain.SalesTarget;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.UUID;

/**
 * Spring Data Elasticsearch repository for the SalesTarget entity.
 */
public interface SalesTargetSearchRepository extends ElasticsearchRepository<SalesTarget, UUID> {
}
