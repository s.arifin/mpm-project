package id.atiila.repository.search;

import id.atiila.domain.WorkOrderBooking;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WorkOrderBooking entity.
 */
public interface WorkOrderBookingSearchRepository extends ElasticsearchRepository<WorkOrderBooking, UUID> {
}
