package id.atiila.repository.search;

import id.atiila.domain.ReturnSalesOrder;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ReturnSalesOrder entity.
 */
 
public interface ReturnSalesOrderSearchRepository extends ElasticsearchRepository<ReturnSalesOrder, UUID> {
}
