package id.atiila.repository.search;

import id.atiila.domain.StockOpnameType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the StockOpnameType entity.
 */
 
public interface StockOpnameTypeSearchRepository extends ElasticsearchRepository<StockOpnameType, Integer> {
}
