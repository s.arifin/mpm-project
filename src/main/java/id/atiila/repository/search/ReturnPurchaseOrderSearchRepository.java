package id.atiila.repository.search;

import id.atiila.domain.ReturnPurchaseOrder;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ReturnPurchaseOrder entity.
 */
 
public interface ReturnPurchaseOrderSearchRepository extends ElasticsearchRepository<ReturnPurchaseOrder, UUID> {
}
