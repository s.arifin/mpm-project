package id.atiila.repository.search;

import id.atiila.domain.StockOpnameItem;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the StockOpnameItem entity.
 */
 
public interface StockOpnameItemSearchRepository extends ElasticsearchRepository<StockOpnameItem, UUID> {
}
