package id.atiila.repository.search;

import id.atiila.domain.CommunicationEventCDB;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CommunicationEventCDB entity.
 */
public interface CommunicationEventCDBSearchRepository extends ElasticsearchRepository<CommunicationEventCDB, UUID> {
}
