package id.atiila.repository.search;

import id.atiila.domain.Prospect;
import java.util.UUID;

import id.atiila.service.dto.ProspectDTO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Prospect entity.
 */
public interface ProspectSearchRepository extends ElasticsearchRepository<ProspectDTO, UUID> {
    void save(Prospect p);
}

