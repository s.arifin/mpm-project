package id.atiila.repository.search;

import id.atiila.domain.ItemIssuance;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ItemIssuance entity.
 */
public interface ItemIssuanceSearchRepository extends ElasticsearchRepository<ItemIssuance, UUID> {
}
