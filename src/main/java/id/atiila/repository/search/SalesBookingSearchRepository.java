package id.atiila.repository.search;

import id.atiila.domain.SalesBooking;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SalesBooking entity.
 */
 
public interface SalesBookingSearchRepository extends ElasticsearchRepository<SalesBooking, UUID> {
}
