package id.atiila.repository.search;

import id.atiila.domain.Good;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Good entity.
 */
public interface GoodSearchRepository extends ElasticsearchRepository<Good, String> {
}
