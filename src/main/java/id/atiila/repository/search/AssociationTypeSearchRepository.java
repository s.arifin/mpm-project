package id.atiila.repository.search;

import id.atiila.domain.AssociationType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AssociationType entity.
 */
public interface AssociationTypeSearchRepository extends ElasticsearchRepository<AssociationType, Integer> {
}
