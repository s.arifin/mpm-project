package id.atiila.repository.search;

import id.atiila.domain.RemPart;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RemPart entity.
 */
public interface RemPartSearchRepository extends ElasticsearchRepository<RemPart, String> {
}
