package id.atiila.repository.search;

import id.atiila.domain.PositionReportingStructure;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PositionReportingStructure entity.
 */
public interface PositionReportingStructureSearchRepository extends ElasticsearchRepository<PositionReportingStructure, Integer> {
}
