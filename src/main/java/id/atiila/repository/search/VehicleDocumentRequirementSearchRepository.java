package id.atiila.repository.search;

import id.atiila.domain.VehicleDocumentRequirement;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Spring Data Elasticsearch repository for the VehicleDocumentRequirement entity.
 */
public interface VehicleDocumentRequirementSearchRepository extends ElasticsearchRepository<VehicleDocumentRequirement, UUID> {
}
