package id.atiila.repository.search;

import id.atiila.domain.ShipmentIncoming;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ShipmentIncoming entity.
 */
public interface ShipmentIncomingSearchRepository extends ElasticsearchRepository<ShipmentIncoming, UUID> {
}
