package id.atiila.repository.search;

import id.atiila.domain.GLAccount;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the GLAccount entity.
 */
 
public interface GLAccountSearchRepository extends ElasticsearchRepository<GLAccount, UUID> {
}
