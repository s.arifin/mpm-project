package id.atiila.repository.search;

import id.atiila.domain.PartyCategory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PartyCategory entity.
 */
 
public interface PartyCategorySearchRepository extends ElasticsearchRepository<PartyCategory, Integer> {
}
