package id.atiila.repository.search;

import id.atiila.domain.ShipTo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ShipTo entity.
 */
 
public interface ShipToSearchRepository extends ElasticsearchRepository<ShipTo, String> {
}
