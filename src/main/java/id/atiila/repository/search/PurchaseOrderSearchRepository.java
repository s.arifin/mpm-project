package id.atiila.repository.search;

import id.atiila.domain.PurchaseOrder;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PurchaseOrder entity.
 */
 
public interface PurchaseOrderSearchRepository extends ElasticsearchRepository<PurchaseOrder, UUID> {
}
