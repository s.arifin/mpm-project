package id.atiila.repository.search;

import id.atiila.domain.VehicleIdentification;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the VehicleIdentification entity.
 */
public interface VehicleIdentificationSearchRepository extends ElasticsearchRepository<VehicleIdentification, UUID> {
}
