package id.atiila.repository.search;

import id.atiila.domain.ProductPurchaseOrder;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ProductPurchaseOrder entity.
 */
 
public interface ProductPurchaseOrderSearchRepository extends ElasticsearchRepository<ProductPurchaseOrder, UUID> {
}
