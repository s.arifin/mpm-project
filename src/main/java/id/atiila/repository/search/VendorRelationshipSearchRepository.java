package id.atiila.repository.search;

import id.atiila.domain.VendorRelationship;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the VendorRelationship entity.
 */
 
public interface VendorRelationshipSearchRepository extends ElasticsearchRepository<VendorRelationship, UUID> {
}
