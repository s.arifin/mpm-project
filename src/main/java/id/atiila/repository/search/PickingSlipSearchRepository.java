package id.atiila.repository.search;

import id.atiila.domain.PickingSlip;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PickingSlip entity.
 */
public interface PickingSlipSearchRepository extends ElasticsearchRepository<PickingSlip, UUID> {
}
