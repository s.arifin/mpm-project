package id.atiila.repository.search;

import id.atiila.domain.OrderBillingItem;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the OrderBillingItem entity.
 */
 
public interface OrderBillingItemSearchRepository extends ElasticsearchRepository<OrderBillingItem, UUID> {
}
