package id.atiila.repository.search;

import id.atiila.domain.Mechanic;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Mechanic entity.
 */
public interface MechanicSearchRepository extends ElasticsearchRepository<Mechanic, UUID> {
}
