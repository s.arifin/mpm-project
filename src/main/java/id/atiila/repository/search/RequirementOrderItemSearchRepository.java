package id.atiila.repository.search;

import id.atiila.domain.RequirementOrderItem;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RequirementOrderItem entity.
 */
 
public interface RequirementOrderItemSearchRepository extends ElasticsearchRepository<RequirementOrderItem, UUID> {
}
