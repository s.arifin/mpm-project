package id.atiila.repository.search;

import id.atiila.domain.WorkEffort;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WorkEffort entity.
 */
public interface WorkEffortSearchRepository extends ElasticsearchRepository<WorkEffort, UUID> {
}
