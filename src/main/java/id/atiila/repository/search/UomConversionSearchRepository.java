package id.atiila.repository.search;

import id.atiila.domain.UomConversion;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UomConversion entity.
 */
 
public interface UomConversionSearchRepository extends ElasticsearchRepository<UomConversion, Integer> {
}
