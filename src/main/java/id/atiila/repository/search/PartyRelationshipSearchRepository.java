package id.atiila.repository.search;

import id.atiila.domain.PartyRelationship;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PartyRelationship entity.
 */
 
public interface PartyRelationshipSearchRepository extends ElasticsearchRepository<PartyRelationship, UUID> {
}
