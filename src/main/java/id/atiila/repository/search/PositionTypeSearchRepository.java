package id.atiila.repository.search;

import id.atiila.domain.PositionType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PositionType entity.
 */
public interface PositionTypeSearchRepository extends ElasticsearchRepository<PositionType, Integer> {
}
