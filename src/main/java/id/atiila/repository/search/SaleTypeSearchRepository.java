package id.atiila.repository.search;

import id.atiila.domain.SaleType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SaleType entity.
 */
public interface SaleTypeSearchRepository extends ElasticsearchRepository<SaleType, Integer> {
}
