package id.atiila.repository.search;

import java.util.UUID;

import id.atiila.service.dto.VehicleRegistrationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data Elasticsearch repository for the VehicleRegistration entity.
 */
 
public interface VehicleRegistrationSearchRepository extends ElasticsearchRepository<VehicleRegistrationDTO, UUID> {

//    @Query("select r from VehicleRegistrationDTO r where (r.internalId = :idInternal) or (r.rootId = :idInternal)")
//    Page<VehicleRegistrationDTO> queryByInternal(@Param("idInternal") String idInternal, Pageable page);

}
