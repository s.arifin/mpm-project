package id.atiila.repository.search;

import id.atiila.domain.ProductPackageReceipt;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ProductPackageReceipt entity.
 */
 
public interface ProductPackageReceiptSearchRepository extends ElasticsearchRepository<ProductPackageReceipt, UUID> {
}
