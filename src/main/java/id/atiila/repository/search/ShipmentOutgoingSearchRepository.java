package id.atiila.repository.search;

import id.atiila.domain.ShipmentOutgoing;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ShipmentOutgoing entity.
 */
public interface ShipmentOutgoingSearchRepository extends ElasticsearchRepository<ShipmentOutgoing, UUID> {
}
