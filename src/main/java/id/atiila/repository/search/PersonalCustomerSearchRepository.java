package id.atiila.repository.search;

import id.atiila.domain.PersonalCustomer;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PersonalCustomer entity.
 */
 
public interface PersonalCustomerSearchRepository extends ElasticsearchRepository<PersonalCustomer, String> {
}
