package id.atiila.repository.search;

import id.atiila.domain.WorkServiceRequirement;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WorkServiceRequirement entity.
 */
public interface WorkServiceRequirementSearchRepository extends ElasticsearchRepository<WorkServiceRequirement, UUID> {
}
