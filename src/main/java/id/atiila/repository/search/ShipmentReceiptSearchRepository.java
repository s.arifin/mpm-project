package id.atiila.repository.search;

import id.atiila.domain.ShipmentReceipt;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ShipmentReceipt entity.
 */
 
public interface ShipmentReceiptSearchRepository extends ElasticsearchRepository<ShipmentReceipt, UUID> {
}
