package id.atiila.repository.search;

import id.atiila.domain.VendorType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the VendorType entity.
 */
public interface VendorTypeSearchRepository extends ElasticsearchRepository<VendorType, Integer> {
}
