package id.atiila.repository.search;

import id.atiila.domain.Dimension;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Dimension entity.
 */
 
public interface DimensionSearchRepository extends ElasticsearchRepository<Dimension, Integer> {
}
