package id.atiila.repository.search;

import id.atiila.domain.CustomerRequestItem;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CustomerRequestItem entity.
 */
 
public interface CustomerRequestItemSearchRepository extends ElasticsearchRepository<CustomerRequestItem, UUID> {
}
