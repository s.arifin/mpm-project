package id.atiila.repository.search;

import id.atiila.domain.ProductShipmentIncoming;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ProductShipmentIncoming entity.
 */
 
public interface ProductShipmentIncomingSearchRepository extends ElasticsearchRepository<ProductShipmentIncoming, UUID> {
}
