package id.atiila.repository.search;

import id.atiila.domain.PurposeType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PurposeType entity.
 */
public interface PurposeTypeSearchRepository extends ElasticsearchRepository<PurposeType, Integer> {
}
