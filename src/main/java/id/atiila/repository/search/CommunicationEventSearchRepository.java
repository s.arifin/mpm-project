package id.atiila.repository.search;

import id.atiila.domain.CommunicationEvent;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CommunicationEvent entity.
 */
public interface CommunicationEventSearchRepository extends ElasticsearchRepository<CommunicationEvent, UUID> {
}
