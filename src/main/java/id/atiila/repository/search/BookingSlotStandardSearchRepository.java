package id.atiila.repository.search;

import id.atiila.domain.BookingSlotStandard;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the BookingSlotStandard entity.
 */
public interface BookingSlotStandardSearchRepository extends ElasticsearchRepository<BookingSlotStandard, UUID> {
}
