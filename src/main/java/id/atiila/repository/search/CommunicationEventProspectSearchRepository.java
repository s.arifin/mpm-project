package id.atiila.repository.search;

import id.atiila.domain.CommunicationEventProspect;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CommunicationEventProspect entity.
 */
public interface CommunicationEventProspectSearchRepository extends ElasticsearchRepository<CommunicationEventProspect, UUID> {
}
