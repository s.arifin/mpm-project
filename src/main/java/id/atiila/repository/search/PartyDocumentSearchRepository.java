package id.atiila.repository.search;

import id.atiila.domain.PartyDocument;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PartyDocument entity.
 */
public interface PartyDocumentSearchRepository extends ElasticsearchRepository<PartyDocument, UUID> {
}
