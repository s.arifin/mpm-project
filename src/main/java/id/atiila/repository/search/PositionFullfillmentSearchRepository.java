package id.atiila.repository.search;

import id.atiila.domain.PositionFullfillment;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PositionFullfillment entity.
 */
public interface PositionFullfillmentSearchRepository extends ElasticsearchRepository<PositionFullfillment, Integer> {
}
