package id.atiila.repository.search;

import id.atiila.domain.EventType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the EventType entity.
 */
public interface EventTypeSearchRepository extends ElasticsearchRepository<EventType, Integer> {
}
