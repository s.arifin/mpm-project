package id.atiila.repository.search;

import id.atiila.domain.Approval;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Approval entity.
 */
public interface ApprovalSearchRepository extends ElasticsearchRepository<Approval, UUID> {
}
