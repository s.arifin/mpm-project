package id.atiila.repository.search;

import id.atiila.domain.RequirementType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RequirementType entity.
 */
public interface RequirementTypeSearchRepository extends ElasticsearchRepository<RequirementType, Integer> {
}
