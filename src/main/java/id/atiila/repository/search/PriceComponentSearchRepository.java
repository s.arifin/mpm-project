package id.atiila.repository.search;

import id.atiila.domain.PriceComponent;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PriceComponent entity.
 */
public interface PriceComponentSearchRepository extends ElasticsearchRepository<PriceComponent, UUID> {
}
