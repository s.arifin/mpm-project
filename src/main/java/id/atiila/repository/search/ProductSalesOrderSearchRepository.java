package id.atiila.repository.search;

import id.atiila.domain.ProductSalesOrder;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ProductSalesOrder entity.
 */
 
public interface ProductSalesOrderSearchRepository extends ElasticsearchRepository<ProductSalesOrder, UUID> {
}
