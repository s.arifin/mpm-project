package id.atiila.repository.search;

import id.atiila.domain.Disbursement;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Disbursement entity.
 */
 
public interface DisbursementSearchRepository extends ElasticsearchRepository<Disbursement, UUID> {
}
