package id.atiila.repository.search;

import id.atiila.domain.OrderRequestItem;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the OrderRequestItem entity.
 */
 
public interface OrderRequestItemSearchRepository extends ElasticsearchRepository<OrderRequestItem, UUID> {
}
