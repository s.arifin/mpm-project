package id.atiila.repository.search;

import id.atiila.domain.ProductCategoryType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ProductCategoryType entity.
 */
 
public interface ProductCategoryTypeSearchRepository extends ElasticsearchRepository<ProductCategoryType, Integer> {
}
