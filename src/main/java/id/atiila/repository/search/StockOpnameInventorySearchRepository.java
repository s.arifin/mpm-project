package id.atiila.repository.search;

import id.atiila.domain.StockOpnameInventory;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the StockOpnameInventory entity.
 */
 
public interface StockOpnameInventorySearchRepository extends ElasticsearchRepository<StockOpnameInventory, UUID> {
}
