package id.atiila.repository.search;

import id.atiila.domain.PartyFacilityPurpose;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PartyFacilityPurpose entity.
 */
 
public interface PartyFacilityPurposeSearchRepository extends ElasticsearchRepository<PartyFacilityPurpose, UUID> {
}
