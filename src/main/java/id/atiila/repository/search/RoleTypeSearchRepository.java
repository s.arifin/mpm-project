package id.atiila.repository.search;

import id.atiila.domain.RoleType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RoleType entity.
 */
public interface RoleTypeSearchRepository extends ElasticsearchRepository<RoleType, Integer> {
}
