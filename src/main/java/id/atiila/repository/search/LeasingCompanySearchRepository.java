package id.atiila.repository.search;

import id.atiila.domain.LeasingCompany;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the LeasingCompany entity.
 */
public interface LeasingCompanySearchRepository extends ElasticsearchRepository<LeasingCompany, UUID> {
}
