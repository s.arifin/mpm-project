package id.atiila.repository.search;

import id.atiila.domain.RequestRequirement;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RequestRequirement entity.
 */
 
public interface RequestRequirementSearchRepository extends ElasticsearchRepository<RequestRequirement, UUID> {
}
