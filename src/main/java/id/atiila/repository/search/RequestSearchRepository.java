package id.atiila.repository.search;

import id.atiila.domain.Request;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Request entity.
 */
 
public interface RequestSearchRepository extends ElasticsearchRepository<Request, UUID> {
}
