package id.atiila.repository.search;

import id.atiila.domain.District;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the District entity.
 */
public interface DistrictSearchRepository extends ElasticsearchRepository<District, UUID> {
}
