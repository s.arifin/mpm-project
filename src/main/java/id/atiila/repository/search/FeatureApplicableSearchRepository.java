package id.atiila.repository.search;

import id.atiila.domain.FeatureApplicable;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the FeatureApplicable entity.
 */
 
public interface FeatureApplicableSearchRepository extends ElasticsearchRepository<FeatureApplicable, UUID> {
}
