package id.atiila.repository.search;

import id.atiila.domain.ShipmentPackage;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ShipmentPackage entity.
 */
public interface ShipmentPackageSearchRepository extends ElasticsearchRepository<ShipmentPackage, UUID> {
}
