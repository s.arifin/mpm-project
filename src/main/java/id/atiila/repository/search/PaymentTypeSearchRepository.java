package id.atiila.repository.search;

import id.atiila.domain.PaymentType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PaymentType entity.
 */
 
public interface PaymentTypeSearchRepository extends ElasticsearchRepository<PaymentType, Integer> {
}
