package id.atiila.repository.search;

import id.atiila.domain.RuleIndent;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RuleIndent entity.
 */
 
public interface RuleIndentSearchRepository extends ElasticsearchRepository<RuleIndent, Integer> {
}
