package id.atiila.repository.search;

import id.atiila.domain.GLAccountType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the GLAccountType entity.
 */
 
public interface GLAccountTypeSearchRepository extends ElasticsearchRepository<GLAccountType, Integer> {
}
