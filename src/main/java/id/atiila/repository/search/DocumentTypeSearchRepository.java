package id.atiila.repository.search;

import id.atiila.domain.DocumentType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the DocumentType entity.
 */
public interface DocumentTypeSearchRepository extends ElasticsearchRepository<DocumentType, Integer> {
}
