package id.atiila.repository.search;

import id.atiila.domain.OrderShipmentItem;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the OrderShipmentItem entity.
 */
 
public interface OrderShipmentItemSearchRepository extends ElasticsearchRepository<OrderShipmentItem, UUID> {
}
