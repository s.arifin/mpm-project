package id.atiila.repository.search;

import id.atiila.domain.GeoBoundary;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the GeoBoundary entity.
 */
public interface GeoBoundarySearchRepository extends ElasticsearchRepository<GeoBoundary, UUID> {
}
