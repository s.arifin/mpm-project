package id.atiila.repository.search;

import id.atiila.domain.UnitPreparation;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UnitPreparation entity.
 */
 
public interface UnitPreparationSearchRepository extends ElasticsearchRepository<UnitPreparation, UUID> {
}
