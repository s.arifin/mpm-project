package id.atiila.repository.search;

import id.atiila.domain.VehicleSalesOrder;
import java.util.UUID;

import id.atiila.service.dto.VehicleSalesOrderDTO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the VehicleSalesOrder entity.
 */
public interface VehicleSalesOrderSearchRepository extends ElasticsearchRepository<VehicleSalesOrderDTO, UUID> {
}
