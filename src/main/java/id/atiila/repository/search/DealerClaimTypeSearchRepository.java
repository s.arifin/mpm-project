package id.atiila.repository.search;

import id.atiila.domain.DealerClaimType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the DealerClaimType entity.
 */
public interface DealerClaimTypeSearchRepository extends ElasticsearchRepository<DealerClaimType, Integer> {
}
