package id.atiila.repository.search;

import id.atiila.domain.WorkProductRequirement;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WorkProductRequirement entity.
 */
public interface WorkProductRequirementSearchRepository extends ElasticsearchRepository<WorkProductRequirement, UUID> {
}
