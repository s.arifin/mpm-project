package id.atiila.repository.search;

import id.atiila.domain.SalesBroker;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SalesBroker entity.
 */
public interface SalesBrokerSearchRepository extends ElasticsearchRepository<SalesBroker, UUID> {
}
