package id.atiila.repository.search;

import id.atiila.domain.RuleSalesDiscount;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RuleSalesDiscount entity.
 */
public interface RuleSalesDiscountSearchRepository extends ElasticsearchRepository<RuleSalesDiscount, Integer> {
}
