package id.atiila.repository.search;

import id.atiila.domain.Vendor;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Vendor entity.
 */
 
public interface VendorSearchRepository extends ElasticsearchRepository<Vendor, String> {
}
