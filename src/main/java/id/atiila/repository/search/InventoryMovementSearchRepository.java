package id.atiila.repository.search;

import id.atiila.domain.InventoryMovement;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the InventoryMovement entity.
 */
public interface InventoryMovementSearchRepository extends ElasticsearchRepository<InventoryMovement, UUID> {
}
