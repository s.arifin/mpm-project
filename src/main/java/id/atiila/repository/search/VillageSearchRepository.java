package id.atiila.repository.search;

import id.atiila.domain.Village;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Village entity.
 */
public interface VillageSearchRepository extends ElasticsearchRepository<Village, UUID> {
}
