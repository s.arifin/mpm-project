package id.atiila.repository.search;

import id.atiila.domain.BillingType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the BillingType entity.
 */
public interface BillingTypeSearchRepository extends ElasticsearchRepository<BillingType, Integer> {
}
