package id.atiila.repository.search;

import id.atiila.domain.ShipmentItem;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ShipmentItem entity.
 */
 
public interface ShipmentItemSearchRepository extends ElasticsearchRepository<ShipmentItem, UUID> {
}
