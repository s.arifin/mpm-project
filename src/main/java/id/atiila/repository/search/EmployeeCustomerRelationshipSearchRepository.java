package id.atiila.repository.search;

import id.atiila.domain.EmployeeCustomerRelationship;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the EmployeeCustomerRelationship entity.
 */
 
public interface EmployeeCustomerRelationshipSearchRepository extends ElasticsearchRepository<EmployeeCustomerRelationship, UUID> {
}
