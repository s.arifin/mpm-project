package id.atiila.repository.search;

import id.atiila.domain.ReligionType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ReligionType entity.
 */
public interface ReligionTypeSearchRepository extends ElasticsearchRepository<ReligionType, Integer> {
}
