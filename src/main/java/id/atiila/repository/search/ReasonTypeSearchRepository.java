package id.atiila.repository.search;

import id.atiila.domain.ReasonType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ReasonType entity.
 */
public interface ReasonTypeSearchRepository extends ElasticsearchRepository<ReasonType, Integer> {
}
