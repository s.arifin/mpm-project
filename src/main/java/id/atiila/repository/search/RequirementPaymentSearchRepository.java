package id.atiila.repository.search;

import id.atiila.domain.RequirementPayment;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RequirementPayment entity.
 */
 
public interface RequirementPaymentSearchRepository extends ElasticsearchRepository<RequirementPayment, UUID> {
}
