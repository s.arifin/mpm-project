package id.atiila.repository.search;

import id.atiila.domain.FacilityType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the FacilityType entity.
 */
 
public interface FacilityTypeSearchRepository extends ElasticsearchRepository<FacilityType, Integer> {
}
