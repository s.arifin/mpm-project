package id.atiila.repository.search;

import id.atiila.domain.Suspect;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Suspect entity.
 */
public interface SuspectSearchRepository extends ElasticsearchRepository<Suspect, UUID> {
}
