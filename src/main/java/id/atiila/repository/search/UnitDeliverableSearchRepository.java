package id.atiila.repository.search;

import id.atiila.domain.UnitDeliverable;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UnitDeliverable entity.
 */
public interface UnitDeliverableSearchRepository extends ElasticsearchRepository<UnitDeliverable, UUID> {
}
