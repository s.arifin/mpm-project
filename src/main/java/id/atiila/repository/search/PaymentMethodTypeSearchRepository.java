package id.atiila.repository.search;

import id.atiila.domain.PaymentMethodType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PaymentMethodType entity.
 */
 
public interface PaymentMethodTypeSearchRepository extends ElasticsearchRepository<PaymentMethodType, Integer> {
}
