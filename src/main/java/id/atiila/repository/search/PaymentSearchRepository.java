package id.atiila.repository.search;

import id.atiila.domain.Payment;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Payment entity.
 */
 
public interface PaymentSearchRepository extends ElasticsearchRepository<Payment, UUID> {
}
