package id.atiila.repository.search;

import id.atiila.domain.GeoBoundaryType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the GeoBoundaryType entity.
 */
public interface GeoBoundaryTypeSearchRepository extends ElasticsearchRepository<GeoBoundaryType, Integer> {
}
