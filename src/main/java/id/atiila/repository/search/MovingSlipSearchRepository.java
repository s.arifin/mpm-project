package id.atiila.repository.search;

import id.atiila.domain.MovingSlip;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the MovingSlip entity.
 */
public interface MovingSlipSearchRepository extends ElasticsearchRepository<MovingSlip, UUID> {
}
