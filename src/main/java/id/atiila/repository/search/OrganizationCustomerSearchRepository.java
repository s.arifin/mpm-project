package id.atiila.repository.search;

import id.atiila.domain.OrganizationCustomer;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the OrganizationCustomer entity.
 */
public interface OrganizationCustomerSearchRepository extends ElasticsearchRepository<OrganizationCustomer, String> {
}
