package id.atiila.repository.search;

import id.atiila.domain.Salesman;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Salesman entity.
 */
public interface SalesmanSearchRepository extends ElasticsearchRepository<Salesman, UUID> {
}
