package id.atiila.repository.search;

import id.atiila.domain.GoodContainer;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the GoodContainer entity.
 */
 
public interface GoodContainerSearchRepository extends ElasticsearchRepository<GoodContainer, UUID> {
}
