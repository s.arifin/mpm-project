package id.atiila.repository.search;

import id.atiila.domain.ShipmentPackageType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ShipmentPackageType entity.
 */
public interface ShipmentPackageTypeSearchRepository extends ElasticsearchRepository<ShipmentPackageType, Integer> {
}
