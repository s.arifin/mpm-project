package id.atiila.repository.search;

import id.atiila.domain.RequestType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RequestType entity.
 */
 
public interface RequestTypeSearchRepository extends ElasticsearchRepository<RequestType, Integer> {
}
