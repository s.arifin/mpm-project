package id.atiila.repository.search;

import id.atiila.domain.Memos;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Memos entity.
 */
public interface MemosSearchRepository extends ElasticsearchRepository<Memos, UUID> {
}
