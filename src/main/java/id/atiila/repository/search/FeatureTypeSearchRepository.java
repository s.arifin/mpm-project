package id.atiila.repository.search;

import id.atiila.domain.FeatureType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the FeatureType entity.
 */
 
public interface FeatureTypeSearchRepository extends ElasticsearchRepository<FeatureType, Integer> {
}
