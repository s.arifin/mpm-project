package id.atiila.repository.search;

import id.atiila.domain.LeasingTenorProvide;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the LeasingTenorProvide entity.
 */
public interface LeasingTenorProvideSearchRepository extends ElasticsearchRepository<LeasingTenorProvide, UUID> {
}
