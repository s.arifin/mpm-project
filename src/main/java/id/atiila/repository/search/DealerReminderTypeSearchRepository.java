package id.atiila.repository.search;

import id.atiila.domain.DealerReminderType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the DealerReminderType entity.
 */
public interface DealerReminderTypeSearchRepository extends ElasticsearchRepository<DealerReminderType, Integer> {
}
