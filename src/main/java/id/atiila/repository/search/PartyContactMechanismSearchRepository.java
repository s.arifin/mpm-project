package id.atiila.repository.search;

import java.util.UUID;

import id.atiila.domain.PartyContactMechanism;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PartyContactMechanism entity.
 */
public interface PartyContactMechanismSearchRepository extends ElasticsearchRepository<PartyContactMechanism, Long> {
}
