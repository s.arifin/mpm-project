package id.atiila.repository.search;

import id.atiila.domain.BookingType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the BookingType entity.
 */
public interface BookingTypeSearchRepository extends ElasticsearchRepository<BookingType, Integer> {
}
