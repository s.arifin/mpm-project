package id.atiila.repository.search;

import id.atiila.domain.RelationType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RelationType entity.
 */
public interface RelationTypeSearchRepository extends ElasticsearchRepository<RelationType, Integer> {
}
