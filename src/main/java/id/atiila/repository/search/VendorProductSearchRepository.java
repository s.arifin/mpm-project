package id.atiila.repository.search;

import id.atiila.domain.VendorProduct;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the VendorProduct entity.
 */
public interface VendorProductSearchRepository extends ElasticsearchRepository<VendorProduct, UUID> {
}
