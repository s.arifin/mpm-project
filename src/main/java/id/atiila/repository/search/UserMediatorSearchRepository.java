package id.atiila.repository.search;

import id.atiila.domain.UserMediator;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UserMediator entity.
 */
 
public interface UserMediatorSearchRepository extends ElasticsearchRepository<UserMediator, UUID> {
}
