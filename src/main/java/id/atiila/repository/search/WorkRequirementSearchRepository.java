package id.atiila.repository.search;

import id.atiila.domain.WorkRequirement;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WorkRequirement entity.
 */
public interface WorkRequirementSearchRepository extends ElasticsearchRepository<WorkRequirement, UUID> {
}
