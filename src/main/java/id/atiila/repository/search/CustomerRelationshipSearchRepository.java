package id.atiila.repository.search;

import id.atiila.domain.CustomerRelationship;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CustomerRelationship entity.
 */
 
public interface CustomerRelationshipSearchRepository extends ElasticsearchRepository<CustomerRelationship, UUID> {
}
