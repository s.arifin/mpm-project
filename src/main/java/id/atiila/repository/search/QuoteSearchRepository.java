package id.atiila.repository.search;

import id.atiila.domain.Quote;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Quote entity.
 */
public interface QuoteSearchRepository extends ElasticsearchRepository<Quote, UUID> {
}
