package id.atiila.repository.search;

import id.atiila.domain.StandardCalendar;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the StandardCalendar entity.
 */
 
public interface StandardCalendarSearchRepository extends ElasticsearchRepository<StandardCalendar, Integer> {
}
