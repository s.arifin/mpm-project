package id.atiila.repository.search;

import id.atiila.domain.CommunicationEventDelivery;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.UUID;

/**
 * Spring Data Elasticsearch repository for the CommunicationEventDelivery entity.
 * BeSmart Team
 */
public interface CommunicationEventDeliverySearchRepository extends ElasticsearchRepository<CommunicationEventDelivery, UUID> {
}
