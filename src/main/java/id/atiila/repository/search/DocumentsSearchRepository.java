package id.atiila.repository.search;

import id.atiila.domain.Documents;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Documents entity.
 */
public interface DocumentsSearchRepository extends ElasticsearchRepository<Documents, UUID> {
}
