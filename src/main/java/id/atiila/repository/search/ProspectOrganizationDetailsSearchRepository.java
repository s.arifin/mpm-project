package id.atiila.repository.search;

import id.atiila.domain.ProspectOrganizationDetails;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ProspectOrganizationDetails entity.
 */
public interface ProspectOrganizationDetailsSearchRepository extends ElasticsearchRepository<ProspectOrganizationDetails, UUID> {
}
