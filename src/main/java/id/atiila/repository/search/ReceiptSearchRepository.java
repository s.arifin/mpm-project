package id.atiila.repository.search;

import id.atiila.domain.Receipt;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Receipt entity.
 */
 
public interface ReceiptSearchRepository extends ElasticsearchRepository<Receipt, UUID> {
}
