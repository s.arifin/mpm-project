package id.atiila.repository.search;

import id.atiila.domain.VendorOrder;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the VendorOrder entity.
 */
 
public interface VendorOrderSearchRepository extends ElasticsearchRepository<VendorOrder, UUID> {
}
