package id.atiila.repository.search;

import id.atiila.domain.RuleType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RuleType entity.
 */
public interface RuleTypeSearchRepository extends ElasticsearchRepository<RuleType, Integer> {
}
