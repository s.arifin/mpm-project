package id.atiila.repository.search;

import id.atiila.domain.ServiceReminder;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ServiceReminder entity.
 */
public interface ServiceReminderSearchRepository extends ElasticsearchRepository<ServiceReminder, Integer> {
}
