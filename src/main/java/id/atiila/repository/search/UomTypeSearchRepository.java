package id.atiila.repository.search;

import id.atiila.domain.UomType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UomType entity.
 */
 
public interface UomTypeSearchRepository extends ElasticsearchRepository<UomType, Integer> {
}
