package id.atiila.repository.search;

import id.atiila.domain.Employee;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Employee entity.
 */
 
public interface EmployeeSearchRepository extends ElasticsearchRepository<Employee, String> {
}
