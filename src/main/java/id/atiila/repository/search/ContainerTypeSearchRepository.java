package id.atiila.repository.search;

import id.atiila.domain.ContainerType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ContainerType entity.
 */
 
public interface ContainerTypeSearchRepository extends ElasticsearchRepository<ContainerType, Integer> {
}
