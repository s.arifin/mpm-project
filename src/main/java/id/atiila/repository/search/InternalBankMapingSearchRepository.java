package id.atiila.repository.search;

import id.atiila.domain.InternalBankMaping;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the InternalBankMaping entity.
 */
public interface InternalBankMapingSearchRepository extends ElasticsearchRepository<InternalBankMaping, String> {
}
