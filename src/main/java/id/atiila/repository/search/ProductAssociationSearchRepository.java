package id.atiila.repository.search;

import id.atiila.domain.ProductAssociation;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ProductAssociation entity.
 */
public interface ProductAssociationSearchRepository extends ElasticsearchRepository<ProductAssociation, UUID> {
}
