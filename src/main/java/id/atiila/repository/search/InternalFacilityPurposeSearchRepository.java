package id.atiila.repository.search;

import id.atiila.domain.InternalFacilityPurpose;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the InternalFacilityPurpose entity.
 */
 
public interface InternalFacilityPurposeSearchRepository extends ElasticsearchRepository<InternalFacilityPurpose, UUID> {
}
