package id.atiila.repository.search;

import id.atiila.domain.FacilityContactMechanism;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the FacilityContactMechanism entity.
 */
 
public interface FacilityContactMechanismSearchRepository extends ElasticsearchRepository<FacilityContactMechanism, UUID> {
}
