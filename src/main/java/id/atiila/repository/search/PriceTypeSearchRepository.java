package id.atiila.repository.search;

import id.atiila.domain.PriceType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PriceType entity.
 */
public interface PriceTypeSearchRepository extends ElasticsearchRepository<PriceType, Integer> {
}
