package id.atiila.repository.search;

import id.atiila.domain.BillingItemType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the BillingItemType entity.
 */
 
public interface BillingItemTypeSearchRepository extends ElasticsearchRepository<BillingItemType, Integer> {
}
