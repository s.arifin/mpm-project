package id.atiila.repository.search;

import id.atiila.domain.RequestItem;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RequestItem entity.
 */
 
public interface RequestItemSearchRepository extends ElasticsearchRepository<RequestItem, UUID> {
}
