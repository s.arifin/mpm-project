package id.atiila.repository.search;

import id.atiila.domain.ProductShipmentReceipt;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ProductShipmentReceipt entity.
 */
 
public interface ProductShipmentReceiptSearchRepository extends ElasticsearchRepository<ProductShipmentReceipt, UUID> {
}
