package id.atiila.repository.search;

import id.atiila.domain.ShipmentType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ShipmentType entity.
 */
public interface ShipmentTypeSearchRepository extends ElasticsearchRepository<ShipmentType, Integer> {
}
