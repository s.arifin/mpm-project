package id.atiila.repository.search;

import id.atiila.domain.StockOpname;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the StockOpname entity.
 */
 
public interface StockOpnameSearchRepository extends ElasticsearchRepository<StockOpname, UUID> {
}
