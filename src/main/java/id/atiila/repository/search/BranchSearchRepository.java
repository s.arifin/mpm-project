package id.atiila.repository.search;

import id.atiila.domain.Branch;
import java.util.UUID;

import id.atiila.domain.Internal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data Elasticsearch repository for the Branch entity.
 */

public interface BranchSearchRepository extends ElasticsearchRepository<Branch, String> {
}
