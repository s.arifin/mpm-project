package id.atiila.repository.search;

import id.atiila.domain.WorkOrder;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WorkOrder entity.
 */
public interface WorkOrderSearchRepository extends ElasticsearchRepository<WorkOrder, UUID> {
}
