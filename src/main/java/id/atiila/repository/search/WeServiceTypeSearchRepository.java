package id.atiila.repository.search;

import id.atiila.domain.WeServiceType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WeServiceType entity.
 */
public interface WeServiceTypeSearchRepository extends ElasticsearchRepository<WeServiceType, Integer> {
}
