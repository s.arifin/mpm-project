package id.atiila.repository.search;

import id.atiila.domain.InternalOrder;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the InternalOrder entity.
 */
public interface InternalOrderSearchRepository extends ElasticsearchRepository<InternalOrder, UUID> {
}
