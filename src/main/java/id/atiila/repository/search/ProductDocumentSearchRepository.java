package id.atiila.repository.search;

import id.atiila.domain.ProductDocument;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ProductDocument entity.
 */
public interface ProductDocumentSearchRepository extends ElasticsearchRepository<ProductDocument, UUID> {
}
