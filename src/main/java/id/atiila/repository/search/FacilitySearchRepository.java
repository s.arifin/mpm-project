package id.atiila.repository.search;

import id.atiila.domain.Facility;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Facility entity.
 */
 
public interface FacilitySearchRepository extends ElasticsearchRepository<Facility, UUID> {
}
