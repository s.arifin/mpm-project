package id.atiila.repository.search;

import id.atiila.domain.SalesOrder;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SalesOrder entity.
 */
 
public interface SalesOrderSearchRepository extends ElasticsearchRepository<SalesOrder, UUID> {
}
