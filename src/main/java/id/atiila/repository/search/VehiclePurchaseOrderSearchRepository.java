package id.atiila.repository.search;

import id.atiila.domain.VehiclePurchaseOrder;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the VehiclePurchaseOrder entity.
 */
 
public interface VehiclePurchaseOrderSearchRepository extends ElasticsearchRepository<VehiclePurchaseOrder, UUID> {
}
