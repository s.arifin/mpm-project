package id.atiila.repository.search;

import id.atiila.domain.ProductCategory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ProductCategory entity.
 */

public interface ProductCategorySearchRepository extends ElasticsearchRepository<ProductCategory, Integer> {
}
