package id.atiila.repository.search;

import id.atiila.domain.Motor;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Motor entity.
 */
public interface MotorSearchRepository extends ElasticsearchRepository<Motor, String> {
}
