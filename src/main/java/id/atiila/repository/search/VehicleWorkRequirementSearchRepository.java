package id.atiila.repository.search;

import id.atiila.domain.VehicleWorkRequirement;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the VehicleWorkRequirement entity.
 */
public interface VehicleWorkRequirementSearchRepository extends ElasticsearchRepository<VehicleWorkRequirement, UUID> {
}
