package id.atiila.repository.search;

import id.atiila.domain.StatusType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the StatusType entity.
 */
public interface StatusTypeSearchRepository extends ElasticsearchRepository<StatusType, Integer> {
}
