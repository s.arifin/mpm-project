package id.atiila.repository.search;

import id.atiila.domain.PackageReceipt;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PackageReceipt entity.
 */
 
public interface PackageReceiptSearchRepository extends ElasticsearchRepository<PackageReceipt, UUID> {
}
