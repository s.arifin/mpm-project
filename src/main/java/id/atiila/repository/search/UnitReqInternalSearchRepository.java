package id.atiila.repository.search;

import id.atiila.domain.UnitReqInternal;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UnitReqInternal entity.
 */
public interface UnitReqInternalSearchRepository extends ElasticsearchRepository<UnitReqInternal, UUID> {
}
