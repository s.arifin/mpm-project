package id.atiila.repository.search;

import id.atiila.domain.OrderPayment;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the OrderPayment entity.
 */
 
public interface OrderPaymentSearchRepository extends ElasticsearchRepository<OrderPayment, UUID> {
}
