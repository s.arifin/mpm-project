package id.atiila.repository.search;

import id.atiila.domain.PaymentMethod;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PaymentMethod entity.
 */
 
public interface PaymentMethodSearchRepository extends ElasticsearchRepository<PaymentMethod, Integer> {
}
