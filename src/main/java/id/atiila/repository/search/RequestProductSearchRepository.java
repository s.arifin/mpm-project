package id.atiila.repository.search;

import id.atiila.domain.RequestProduct;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RequestProduct entity.
 */
 
public interface RequestProductSearchRepository extends ElasticsearchRepository<RequestProduct, UUID> {
}
