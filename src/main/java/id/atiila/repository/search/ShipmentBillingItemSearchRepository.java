package id.atiila.repository.search;

import id.atiila.domain.ShipmentBillingItem;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ShipmentBillingItem entity.
 */
 
public interface ShipmentBillingItemSearchRepository extends ElasticsearchRepository<ShipmentBillingItem, UUID> {
}
