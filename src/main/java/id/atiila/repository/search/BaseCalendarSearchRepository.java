package id.atiila.repository.search;

import id.atiila.domain.BaseCalendar;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the BaseCalendar entity.
 */
 
public interface BaseCalendarSearchRepository extends ElasticsearchRepository<BaseCalendar, Integer> {
}
