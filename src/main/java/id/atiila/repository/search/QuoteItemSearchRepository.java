package id.atiila.repository.search;

import id.atiila.domain.QuoteItem;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the QuoteItem entity.
 */
public interface QuoteItemSearchRepository extends ElasticsearchRepository<QuoteItem, UUID> {
}
