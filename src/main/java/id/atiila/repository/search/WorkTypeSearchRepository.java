package id.atiila.repository.search;

import id.atiila.domain.WorkType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WorkType entity.
 */
public interface WorkTypeSearchRepository extends ElasticsearchRepository<WorkType, Integer> {
}
