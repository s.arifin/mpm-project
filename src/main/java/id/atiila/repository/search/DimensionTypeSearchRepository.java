package id.atiila.repository.search;

import id.atiila.domain.DimensionType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the DimensionType entity.
 */
 
public interface DimensionTypeSearchRepository extends ElasticsearchRepository<DimensionType, Integer> {
}
