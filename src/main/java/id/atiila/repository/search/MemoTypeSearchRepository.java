package id.atiila.repository.search;

import id.atiila.domain.MemoType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the MemoType entity.
 */
 
public interface MemoTypeSearchRepository extends ElasticsearchRepository<MemoType, Integer> {
}
