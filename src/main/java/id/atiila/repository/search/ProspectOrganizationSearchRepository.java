package id.atiila.repository.search;

import id.atiila.domain.ProspectOrganization;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ProspectOrganization entity.
 */
public interface ProspectOrganizationSearchRepository extends ElasticsearchRepository<ProspectOrganization, UUID> {
}
