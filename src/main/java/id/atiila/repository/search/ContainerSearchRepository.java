package id.atiila.repository.search;

import id.atiila.domain.Container;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Container entity.
 */
 
public interface ContainerSearchRepository extends ElasticsearchRepository<Container, UUID> {
}
