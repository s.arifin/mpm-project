package id.atiila.repository.search;

import id.atiila.domain.OrderType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the OrderType entity.
 */
public interface OrderTypeSearchRepository extends ElasticsearchRepository<OrderType, Integer> {
}
