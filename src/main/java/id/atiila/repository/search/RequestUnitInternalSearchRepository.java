package id.atiila.repository.search;

import id.atiila.domain.RequestUnitInternal;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RequestUnitInternal entity.
 */
 
public interface RequestUnitInternalSearchRepository extends ElasticsearchRepository<RequestUnitInternal, UUID> {
}
