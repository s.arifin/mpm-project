package id.atiila.repository.search;

import id.atiila.domain.MasterNumbering;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the MasterNumbering entity.
 */
public interface MasterNumberingSearchRepository extends ElasticsearchRepository<MasterNumbering, UUID> {
}
