package id.atiila.repository.search;

import id.atiila.domain.Position;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Position entity.
 */
 
public interface PositionSearchRepository extends ElasticsearchRepository<Position, UUID> {
}
