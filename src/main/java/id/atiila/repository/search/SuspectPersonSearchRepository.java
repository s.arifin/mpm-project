package id.atiila.repository.search;

import id.atiila.domain.SuspectPerson;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SuspectPerson entity.
 */
public interface SuspectPersonSearchRepository extends ElasticsearchRepository<SuspectPerson, UUID> {
}
