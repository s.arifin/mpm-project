package id.atiila.repository.search;

import id.atiila.domain.OrderItem;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the OrderItem entity.
 */
 
public interface OrderItemSearchRepository extends ElasticsearchRepository<OrderItem, UUID> {
}
