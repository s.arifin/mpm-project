package id.atiila.repository.search;

import id.atiila.domain.SuspectType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SuspectType entity.
 */
public interface SuspectTypeSearchRepository extends ElasticsearchRepository<SuspectType, Integer> {
}
