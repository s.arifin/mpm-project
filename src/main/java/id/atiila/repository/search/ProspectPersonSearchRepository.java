package id.atiila.repository.search;

import id.atiila.domain.ProspectPerson;
import java.util.UUID;

import id.atiila.service.dto.ProspectPersonDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ProspectPerson entity.
 */
public interface ProspectPersonSearchRepository extends ElasticsearchRepository<ProspectPersonDTO, UUID> {
}
