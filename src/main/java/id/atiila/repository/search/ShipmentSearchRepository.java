package id.atiila.repository.search;

import id.atiila.domain.Shipment;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Shipment entity.
 */
 
public interface ShipmentSearchRepository extends ElasticsearchRepository<Shipment, UUID> {
}
