package id.atiila.repository.search;

import id.atiila.domain.BookingSlot;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the BookingSlot entity.
 */
public interface BookingSlotSearchRepository extends ElasticsearchRepository<BookingSlot, UUID> {
}
