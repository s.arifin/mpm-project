package id.atiila.repository.search;

import id.atiila.domain.CalendarType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CalendarType entity.
 */
public interface CalendarTypeSearchRepository extends ElasticsearchRepository<CalendarType, Integer> {
}
