package id.atiila.repository.search;

import id.atiila.domain.UnitAccesoriesMapper;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UnitAccesoriesMapper entity.
 */
public interface UnitAccesoriesMapperSearchRepository extends ElasticsearchRepository<UnitAccesoriesMapper, UUID> {
}
