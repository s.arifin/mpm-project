package id.atiila.repository.search;

import id.atiila.domain.BillingDisbursement;
import java.util.UUID;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the BillingDisbursement entity.
 */
 
public interface BillingDisbursementSearchRepository extends ElasticsearchRepository<BillingDisbursement, UUID> {
}
