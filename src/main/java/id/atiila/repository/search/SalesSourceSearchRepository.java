package id.atiila.repository.search;

import id.atiila.domain.SalesSource;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SalesSource entity.
 */
public interface SalesSourceSearchRepository extends ElasticsearchRepository<SalesSource, Long> {
}
