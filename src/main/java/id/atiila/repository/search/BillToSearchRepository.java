package id.atiila.repository.search;

import id.atiila.domain.BillTo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the BillTo entity.
 */
 
public interface BillToSearchRepository extends ElasticsearchRepository<BillTo, String> {
}
