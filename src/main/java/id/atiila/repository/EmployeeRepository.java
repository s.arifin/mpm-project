package id.atiila.repository;

import id.atiila.domain.Employee;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

/**
 * Spring Data JPA repository for the Employee entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, String> {

    @Query("select r from Employee r where (r.person.idParty = :id)")
    Page<Employee> queryById(@Param("id") UUID id, Pageable pageable);

	@Query("select r from Employee r where (1=2)")
    Page<Employee> queryNothing(Pageable pageable);
}

