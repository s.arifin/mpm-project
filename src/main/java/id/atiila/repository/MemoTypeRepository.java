package id.atiila.repository;

import id.atiila.domain.MemoType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MemoType entity.
 * BeSmart Team
 */@SuppressWarnings("unused")
@Repository
public interface MemoTypeRepository extends JpaRepository<MemoType, Integer> {
}

