package id.atiila.repository;

import id.atiila.domain.ProductCategoryType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ProductCategoryType entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ProductCategoryTypeRepository extends JpaRepository<ProductCategoryType, Integer> {

	@Query("select r from ProductCategoryType r where (1=2)")
    Page<ProductCategoryType> queryNothing(Pageable pageable); 
}

