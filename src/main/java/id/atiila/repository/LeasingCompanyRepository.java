package id.atiila.repository;

import id.atiila.domain.LeasingCompany;
import java.util.UUID;

import id.atiila.domain.Party;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the LeasingCompany entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface LeasingCompanyRepository extends JpaRepository<LeasingCompany, UUID> {

    LeasingCompany findOneByParty(Party party);

    LeasingCompany findOneByIdLeasingCompany(String idVendor);
}

