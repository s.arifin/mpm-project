package id.atiila.repository;

import id.atiila.domain.Internal;
import id.atiila.domain.Memos;
import java.util.UUID;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Memos entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface MemosRepository extends JpaRepository<Memos, UUID> {
    @Query("select r from Memos r join r.statuses s " +
           " where (current_timestamp between s.dateFrom and s.dateThru) " +
           "  and (s.idStatusType in (10, 13, 17)) " +
           "  and r.dealer.idInternal = :intr  ")
    Page<Memos> findActiveMemos(@Param("intr")String intr, Pageable pageable);

}

