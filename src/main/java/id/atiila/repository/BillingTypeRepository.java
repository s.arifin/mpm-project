package id.atiila.repository;

import id.atiila.domain.BillingType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the BillingType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface BillingTypeRepository extends JpaRepository<BillingType, Integer> {


}

