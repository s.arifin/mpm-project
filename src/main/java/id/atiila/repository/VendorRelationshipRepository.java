package id.atiila.repository;

import id.atiila.domain.VendorRelationship;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the VendorRelationship entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface VendorRelationshipRepository extends JpaRepository<VendorRelationship, UUID> {

    @Query("select r from VendorRelationship r where (r.statusType.idStatusType = :idStatusType)")
    Page<VendorRelationship> queryByIdStatusType(@Param("idStatusType") Integer idStatusType, Pageable pageable);

    @Query("select r from VendorRelationship r where (r.relationType.idRelationType = :idRelationType)")
    Page<VendorRelationship> queryByIdRelationType(@Param("idRelationType") Integer idRelationType, Pageable pageable);

    @Query("select r from VendorRelationship r where (r.internal.idInternal = :idInternal)")
    Page<VendorRelationship> queryByIdInternal(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from VendorRelationship r where (r.vendor.idVendor = :idVendor)")
    Page<VendorRelationship> queryByIdVendor(@Param("idVendor") String idVendor, Pageable pageable);

    @Query("select r from VendorRelationship r where (r.vendor.idVendor = :idVendor) and (r.internal.idInternal = :idInternal)")
    Page<VendorRelationship> queryByInternalVendor(@Param("idInternal") String idInternal, @Param("idVendor") String idVendor, Pageable pageable);

    @Query("select r from VendorRelationship r where (1=2)")
    Page<VendorRelationship> queryNothing(Pageable pageable);

    @Query("select r from VendorRelationship r " +
        "    where (r.vendor.idVendor = :idVendor) " +
        "      and (r.internal.idInternal = :idInternal) " +
        "      and (r.relationType.idRelationType = :idRelationType)" +
        "      and (current_timestamp between r.dateFrom and r.dateThru) ")
    VendorRelationship findRelationship(@Param("idRelationType") Integer idRelationType,
                                        @Param("idInternal") String idInternal,
                                        @Param("idVendor") String idVendor);
}

