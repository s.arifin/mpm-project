package id.atiila.repository;

import id.atiila.domain.FeatureType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the FeatureType entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface FeatureTypeRepository extends JpaRepository<FeatureType, Integer> {

	@Query("select r from FeatureType r where (1=2)")
    Page<FeatureType> queryNothing(Pageable pageable); 
}

