package id.atiila.repository;

import id.atiila.domain.Approval;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import id.atiila.domain.SalesUnitRequirement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Approval entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ApprovalRepository extends JpaRepository<Approval, UUID> {
    @Query("SELECT r FROM Approval r WHERE (idMessage = :idmessage) " +
        "AND (r.idApprovalType = :idapprovaltype) " +
        "AND (CURRENT_TIMESTAMP BETWEEN r.dateFrom AND r.dateThru)")
    Approval findActiveApprovalByIdMessageAndType(
        @Param("idmessage") Integer idmessage,
        @Param("idapprovaltype") Integer idapprovaltype
    );

    @Query("select a from Approval a where (idRequirement = :idreq)" +
        "and (current_timestamp between a.dateFrom and a.dateThru)")
    List<Approval> findApprovalByIdReq(
        @Param("idreq")UUID idreq
    );

    @Query("select a from Approval a " +
        "   where (idRequirement = :idreq)" +
        "   and a.idApprovalType = 104" +
        "   and (current_timestamp between a.dateFrom and a.dateThru)")
    List<Approval> findApprovalLeasingByIdReq(
        @Param("idreq")UUID idreq
    );

    @Query("select a from SalesUnitRequirement a join a.approvals b join a.statuses s " +
        "where 1=1 " +
        "and (b.idApprovalType in(101))" +
        "and (b.idStatusType = 60)" +
        "and (a.internal.idInternal= :idinternal) " +
        "and (a.subsown <= :maxamount)" +
        "and (current_timestamp between b.dateFrom and b.dateThru)")
    Page<SalesUnitRequirement> findWaitingForApprovalSurForKorsal(
        @Param("idinternal") String idinternal,
        @Param("maxamount") BigDecimal maxamount,
        Pageable pageable
    );

    @Query("select a from SalesUnitRequirement a join a.approvals b join a.statuses s " +
        "where (b.idStatusType= 60)" +
        "and (current_timestamp between b.dateFrom and b.dateThru)" +
        "and (a.internal.idInternal = :idinternal) " +
        "and (b.idApprovalType in (101,103))")
    Page<SalesUnitRequirement> findWaitingForApprovalSURForKacab(
        @Param("idinternal") String idinternal,
        Pageable pageable
    );

    @Query("select a from SalesUnitRequirement a join a.approvals b join a.statuses s " +
        "where (b.idStatusType= 60)" +
        "and (current_timestamp between b.dateFrom and b.dateThru)" +
        "and (a.internal.idInternal = :idinternal) " +
        "and (b.idApprovalType in (101,103))")
    Page<SalesUnitRequirement> findWaitingTerritorialForApprovalSURForKacab(
        @Param("idinternal") String idinternal,
        Pageable pageable
    );

    @Query("select r from Approval r " +
        "   where (current_timestamp between r.dateFrom and r.dateThru) " +
        "   and (r.idApprovalType in (104)) " +
        "   and (r.idStatusType not in (1))" +
        "   and r.idRequirement = :idReq")
    Page<Approval> findActiveLeasingApprovalByIdReq(@Param("idReq") UUID idReq,Pageable pageable);
}

