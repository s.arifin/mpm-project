package id.atiila.repository;

import id.atiila.domain.ShipmentPackageType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ShipmentPackageType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ShipmentPackageTypeRepository extends JpaRepository<ShipmentPackageType, Integer> {
}

