package id.atiila.repository;

import id.atiila.domain.BookingSlot;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the BookingSlot entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface BookingSlotRepository extends JpaRepository<BookingSlot, UUID> {


}

