package id.atiila.repository;

import id.atiila.domain.Vehicle;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Vehicle entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, UUID> {

    @Query("select r from Vehicle r where (r.idMachine = :id)")
    List<Vehicle> findByIdMachine(@Param("id") String id);

    @Query("select r from Vehicle r where (r.idFrame = :id)")
    List<Vehicle> findOnebyidFrame(@Param("id") String id);
}

