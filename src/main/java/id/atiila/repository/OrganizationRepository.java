package id.atiila.repository;

import id.atiila.domain.Organization;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Organization entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface OrganizationRepository extends JpaRepository<Organization, UUID> {

    List<Organization> findAllByName(String name);

    @Query("select distinct organization from Organization organization left join fetch organization.categories")
    List<Organization> findAllWithEagerRelationships();

    @Query("select organization from Organization organization left join fetch organization.categories where organization.idParty = :id")
    Organization findOneWithEagerRelationships(@Param("id") UUID id);

}

