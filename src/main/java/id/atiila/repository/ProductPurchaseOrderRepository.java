package id.atiila.repository;

import id.atiila.domain.Internal;
import id.atiila.domain.ProductPurchaseOrder;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ProductPurchaseOrder entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ProductPurchaseOrderRepository extends JpaRepository<ProductPurchaseOrder, UUID> {

    @Query("select r from ProductPurchaseOrder r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (r.internal = :internal)" +
        "      and (s.idStatusType not in (13, 17))")
    Page<ProductPurchaseOrder> findActiveProductPurchaseOrder(@Param("internal") Internal intr, Pageable pageable);

    @Query("select r from ProductPurchaseOrder r "
	    + " where ((r.vendor.idVendor = :idVendor) or (:idVendor is null)) "
	    + " and ((r.internal.idInternal = :idInternal) or (:idInternal is null)) "
	    + " and ((r.billTo.idBillTo = :idBillTo) or (:idBillTo is null)) "
)
    Page<ProductPurchaseOrder> findByParams(@Param("idVendor") String idVendor, @Param("idInternal") String idInternal, @Param("idBillTo") String idBillTo, Pageable pageable);

}

