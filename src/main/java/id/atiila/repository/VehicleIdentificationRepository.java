package id.atiila.repository;

import id.atiila.domain.VehicleIdentification;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the VehicleIdentification entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface VehicleIdentificationRepository extends JpaRepository<VehicleIdentification, UUID> {
    
    List<VehicleIdentification> findAllByVehicleNumber(String id);


}

