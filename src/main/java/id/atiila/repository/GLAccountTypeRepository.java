package id.atiila.repository;

import id.atiila.domain.GLAccountType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the GLAccountType entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface GLAccountTypeRepository extends JpaRepository<GLAccountType, Integer> {

	@Query("select r from GLAccountType r where (1=2)")
    Page<GLAccountType> queryNothing(Pageable pageable); 
}

