package id.atiila.repository;

import id.atiila.domain.Product;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Product entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ProductRepository extends JpaRepository<Product, String> {

    @Query("select distinct product from Product product left join fetch product.features left join fetch product.categories")
    List<Product> findAllWithEagerRelationships();

    @Query("select product from Product product left join fetch product.features left join fetch product.categories where product.id =:id")
    Product findOneWithEagerRelationships(@Param("id") String id);
}

