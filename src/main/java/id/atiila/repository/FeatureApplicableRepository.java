package id.atiila.repository;

import id.atiila.domain.Feature;
import id.atiila.domain.FeatureApplicable;
import java.util.UUID;

import id.atiila.domain.Product;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the FeatureApplicable entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface FeatureApplicableRepository extends JpaRepository<FeatureApplicable, UUID> {

    @Query("select r from FeatureApplicable r " +
        "    where (r.featureType.idFeatureType = :idFeatureType)" +
        "      and (current_timestamp between r.dateFrom and r.dateThru)" +
        "       or (r.dateFrom > current_timestamp )")
    Page<FeatureApplicable> queryByIdFeatureType(@Param("idFeatureType") Integer idFeatureType, Pageable pageable);

    @Query("select r from FeatureApplicable r " +
        "    where (r.feature.idFeature = :idFeature)" +
        "      and (current_timestamp between r.dateFrom and r.dateThru)" +
        "       or (r.dateFrom > current_timestamp )")
    Page<FeatureApplicable> queryByIdFeature(@Param("idFeature") Integer idFeature, Pageable pageable);

    @Query("select r from FeatureApplicable r " +
        "    where (r.feature.refKey = :refKey) " +
        "      and (r.product = :product) " +
        "      and (r.featureType.idFeatureType = :idFeatureType) " +
        "      and (current_timestamp  between r.dateFrom and r.dateThru)")
    FeatureApplicable queryByProductIdFeature(@Param("product") Product p, @Param("idFeatureType") Integer idFeatureType, @Param("refKey") String refKey);

    @Query("select r from FeatureApplicable r " +
        "    where (r.featureType.idFeatureType = :idFeatureType) " +
        "      and (current_timestamp between r.dateFrom and r.dateThru) ")
    Page<FeatureApplicable> queryByFeatureType(@Param("idFeatureType") Integer idFeatureType, Pageable pageable);

    @Query("select r from FeatureApplicable r " +
        "    where (r.product.idProduct = :idProduct) " +
        "      and (current_timestamp between r.dateFrom and r.dateThru)" +
        "       or (r.dateFrom > current_timestamp )")
    Page<FeatureApplicable> queryByIdProduct(@Param("idProduct") String idProduct, Pageable pageable);

    @Query("select r from FeatureApplicable r where (1=2)")
    Page<FeatureApplicable> queryNothing(Pageable pageable);

    @Query("select r from FeatureApplicable r " +
        "    where (r.feature = :feature) " +
        "      and (r.product = :product)" +
        "      and (current_timestamp  between r.dateFrom and r.dateThru)")
    FeatureApplicable queryByProductAndFeature(@Param("product") Product product,
                                               @Param("feature") Feature feature);
}

