package id.atiila.repository;

import id.atiila.domain.LeasingTenorProvide;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Pageable;


/**
 * Spring Data JPA repository for the LeasingTenorProvide entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface LeasingTenorProvideRepository extends JpaRepository<LeasingTenorProvide, UUID> {

    @Query("select ltp from LeasingTenorProvide ltp where (:start_date between ltp.dateFrom and ltp.dateThru) and (ltp.product.idProduct) = :idproduct and(ltp.leasing.idLeasingCompany) = :idleasing")
    List<LeasingTenorProvide> findExistingTenorProvide(
      @Param("start_date") Date start_date,
      @Param("idproduct") String idproduct,
      @Param("idleasing") String idleasing
    );

    @Query("select l from LeasingTenorProvide l where (current_timestamp between l.dateFrom and l.dateThru) and (l.product.idProduct) = :idproduct and (l.leasing.idPartyRole)= :idleasing")
    List<LeasingTenorProvide> findActiveTenorByIdProduct(
        @Param("idproduct") String idproduct,
        @Param("idleasing") UUID idleasing
    );
}

