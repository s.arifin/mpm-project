package id.atiila.repository;

import id.atiila.domain.Village;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Village entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface VillageRepository extends JpaRepository<Village, UUID> {

    Village findByGeoCode(String id);

    List<Village> findOneByDescription(String description);
}

