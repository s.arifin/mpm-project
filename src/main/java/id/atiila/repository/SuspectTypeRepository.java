package id.atiila.repository;

import id.atiila.domain.SuspectType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SuspectType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface SuspectTypeRepository extends JpaRepository<SuspectType, Integer> {
}

