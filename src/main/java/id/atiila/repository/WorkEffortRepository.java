package id.atiila.repository;

import id.atiila.domain.WorkEffort;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the WorkEffort entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface WorkEffortRepository extends JpaRepository<WorkEffort, UUID> {


    @Query("select distinct work_effort from WorkEffort work_effort")
    List<WorkEffort> findAllWithEagerRelationships();

    @Query("select work_effort from WorkEffort work_effort where work_effort.idWe = :id")
    WorkEffort findOneWithEagerRelationships(@Param("id") UUID id);

    @Query("select r from WorkEffort r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<WorkEffort> findActiveWorkEffort(Pageable pageable);

}

