package id.atiila.repository;

import id.atiila.domain.UnitDeliverable;
import id.atiila.domain.VehicleDocumentRequirement;

import java.util.List;
import java.util.UUID;

import id.atiila.service.dto.SummaryRefferalDTO;
import id.atiila.service.pto.UnitDeliverablePTO;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;


/**
 * Spring Data JPA repository for the UnitDeliverable entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface UnitDeliverableRepository extends JpaRepository<UnitDeliverable, UUID> {

	@Query("select r from UnitDeliverable r " +
        " join VehicleDocumentRequirement v on (v = r.vehicleDocumentRequirement)" +
        " join v.statuses s" +
        " where r.idDeliverableType = :idDeliverableType" +
        " and (v.internal.idInternal = :idInternal)" +
        " and (s.idStatusType in (36))" +
        " and r.dateReceipt is not null " +
        " and (r.iscompleted = false or r.iscompleted is null) " +
        " and r.dateDelivery is null")
	Page<UnitDeliverable> findAllByIdDeliverableType(Pageable pageable, @Param("idDeliverableType") Integer idDeliverableType,
                                                     @Param("idInternal") String idInternal);

    @Query("select r from UnitDeliverable r " +
        " join VehicleDocumentRequirement v on (v = r.vehicleDocumentRequirement)" +
        " join v.statuses s" +
        " join v.internal i" +
        " where (r.idDeliverableType = :idDeliverableType and (s.idStatusType in (36)) and r.dateReceipt is not null) " +
        " and ( v.vehicle.idMachine like :data " +
        " or v.vehicle.idFrame like :data ) ")
    Page<UnitDeliverable> findSearchStnk(@Param("data") String data,
                                         @Param("idDeliverableType") Integer idDeliverableType,
                                         Pageable pageable);


    @Query("select r from UnitDeliverable r " +
        " join VehicleDocumentRequirement v on (v = r.vehicleDocumentRequirement)" +
        " join OrderItem oi on (oi = v.orderItem)" +
        " join VehicleSalesOrder vso on (vso = oi.orders)" +
        " join v.statuses s" +
        " where r.idDeliverableType = :#{#param.idDeliverableType}" +
        " and (v.internal.idInternal = :#{#param.internalId})" +
        " and (s.idStatusType in (36))" +
        " and r.dateReceipt is not null " +
        " and (r.iscompleted = false or r.iscompleted is null) " +
        " and (vso.idSalesman = :#{#param.salesmanId})")
    Page<UnitDeliverable> findAllByIdDeliverableTypeFilterBy(Pageable pageable, @Param("param") UnitDeliverablePTO param);

    @Query("select r from UnitDeliverable r " +
        " join VehicleDocumentRequirement v on (v = r.vehicleDocumentRequirement)" +
        " join v.statuses s" +
        " where r.idDeliverableType = :idDeliverableType" +
        " and (v.internal.idInternal = :idInternal)" +
        " and (s.idStatusType = :idStatusType)")
    Page<UnitDeliverable> findAllByIdDeliverableTypeandStatusType(Pageable pageable, @Param("idDeliverableType") Integer idDeliverableType, @Param("idInternal") String idInternal, @Param("idStatusType") Integer idStatusType);

//	@Query(" select new id.atiila.service.dto.SummaryRefferalDTO(r.refDate, r.refNumber, r.vehicleDocumentRequirement.vendor.idVendor, r.receiptQty, count(r.vehicleDocumentRequirement.vehicle.idFrame))" +
//           " from UnitDeliverable r group by r.vehicleDocumentRequirement.vehicle.idFrame, r.refDate, r.refNumber, r.vehicleDocumentRequirement.vendor.idVendor, r.receiptQty, r.idDeliverable")
//	Page<SummaryRefferalDTO> findAllReferences(Pageable pageable);

    @Query(" select new id.atiila.service.dto.SummaryRefferalDTO(r.refDate, r.refNumber, r.vehicleDocumentRequirement.vendor.idVendor, r.receiptQty, count(r.refNumber),r.receiptNominal)" +
        " from UnitDeliverable r " +
        " join VehicleDocumentRequirement v on (v = r.vehicleDocumentRequirement)" +
        "   where r.refNumber is not null " +
        "   and r.idDeliverableType = 104 " +
        "   and (v.internal.idInternal = :idInternal)" +
        "   group by r.refDate, r.refNumber, r.vehicleDocumentRequirement.vendor.idVendor, r.receiptQty, r.receiptNominal" +
        "   having count(r.refNumber) < r.receiptQty")
        // TEST SUMMARY REFERAL
    Page<SummaryRefferalDTO> findAllReferences(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from UnitDeliverable r " +
        " join VehicleDocumentRequirement v on (v = r.vehicleDocumentRequirement)" +
        " join OrderItem oi on (oi = v.orderItem)" +
        " join VehicleSalesOrder vso on (vso = oi.orders)" +
        " join v.statuses s " +
        " where r.idDeliverableType = :idDeliverableType" +
        " and (v.internal.idInternal = :idInternal)" +
        " and (s.idStatusType in (36))" +
        " and r.dateReceipt is not null " +
        " and vso.leasing.idPartyRole = :idLeasing " +
        " and (r.iscompleted = false or r.iscompleted is null) ")
    Page<UnitDeliverable> findAllByIdDeliverableTypeFilter(Pageable pageable, @Param("idDeliverableType") Integer idDeliverableType,
                                                           @Param("idInternal") String idInternal, @Param("idLeasing") UUID idLeasing);

    @Query("select r from UnitDeliverable r " +
        " join VehicleDocumentRequirement v on (v = r.vehicleDocumentRequirement)" +
        " join OrderItem oi on (oi = v.orderItem)" +
        " join VehicleSalesOrder vso on (vso = oi.orders)" +
        " join v.statuses s " +
        " where r.idDeliverableType = :idDeliverableType" +
        " and (v.internal.idInternal = :idInternal)" +
        " and (s.idStatusType in (36))" +
        " and r.dateReceipt is not null " +
        " and vso.leasing = null " +
        " and (r.iscompleted = false or r.iscompleted is null) ")
    Page<UnitDeliverable> findAllByIdDeliverableTypeFilterCash(Pageable pageable, @Param("idDeliverableType") Integer idDeliverableType,
                                                               @Param("idInternal") String idInternal);


    @Query(" select new id.atiila.service.dto.SummaryRefferalDTO(r.refDate, r.refNumber, r.vehicleDocumentRequirement.vendor.idVendor, r.receiptQty, count(r.refNumber), r.receiptNominal)" +
        " from UnitDeliverable r " +
        " join VehicleDocumentRequirement v on (v = r.vehicleDocumentRequirement)" +
        "   where r.refNumber is not null " +
        "   and r.idDeliverableType = 104 " +
        "   and (v.internal.idInternal = :idInternal)" +
        "   and r.diPrint is null " +
        "   group by r.refDate, r.refNumber, r.vehicleDocumentRequirement.vendor.idVendor, r.receiptQty, r.receiptNominal" +
        "   having count(r.refNumber) = r.receiptQty")
        // TEST SUMMARY REFERAL
    Page<SummaryRefferalDTO> findListReferences(@Param("idInternal") String idInternal, Pageable pageable);

    @Query(" select new id.atiila.service.dto.SummaryRefferalDTO(r.refDate, r.refNumber, r.vehicleDocumentRequirement.vendor.idVendor, r.receiptQty, count(r.refNumber), r.receiptNominal)" +
        " from UnitDeliverable r " +
        " join VehicleDocumentRequirement v on (v = r.vehicleDocumentRequirement)" +
        "   where r.refNumber is not null " +
        "   and r.idDeliverableType = 104 " +
        "   and (v.internal.idInternal = :idInternal)" +
        "   and r.diPrint = 1 " +
        "   group by r.refDate, r.refNumber, r.vehicleDocumentRequirement.vendor.idVendor, r.receiptQty, r.receiptNominal" +
        "   having count(r.refNumber) = r.receiptQty")
        // TEST SUMMARY REFERAL
    Page<SummaryRefferalDTO> findListReferences1(@Param("idInternal") String idInternal, Pageable pageable);

    @Query(" select r from UnitDeliverable r " +
        " join VehicleDocumentRequirement v on (v = r.vehicleDocumentRequirement)" +
        "   where r.refNumber is not null " +
        "   and r.idDeliverableType = 104 " +
        "   and (v.internal.idInternal = :idInternal)" +
        "   and r.refNumber = :refNumber")
    List<UnitDeliverable> findRefNumber(@Param("idInternal") String idInternal,@Param("refNumber") String refNumber);

    @Query(" select r from UnitDeliverable r " +
        " join VehicleDocumentRequirement v on (v = r.vehicleDocumentRequirement)" +
        "   where r.refNumber is not null " +
//        "   and r.idDeliverableType = 104 " +
        "   and (v.internal.idInternal = :idInternal)" +
        "   and r.refNumber = :refNumber")
    List<UnitDeliverable> findRefNumberku(@Param("idInternal") String idInternal,@Param("refNumber") String refNumber);

    @Query(" select new id.atiila.service.dto.SummaryRefferalDTO(r.refDate, r.refNumber, r.receiptNominal)" +
        " from UnitDeliverable r " +
        " join VehicleDocumentRequirement v on (v = r.vehicleDocumentRequirement)" +
        "   where r.refNumber is not null " +
        "   and r.idDeliverableType = 104 " +
        "   and (v.internal.idInternal = :idInternal)" +
        "   and r.refNumber = :refNumber" +
        "   group by r.refDate, r.refNumber, r.receiptNominal")
    SummaryRefferalDTO findByRefNumber(@Param("idInternal") String idInternal,@Param("refNumber") String refNumber);

	@Query("select r from UnitDeliverable r " +
        "   where r.vehicleDocumentRequirement.idRequirement = :idRequirement " +
        "   and r.idDeliverableType = :idDeliverableType")
    UnitDeliverable findByIdRequirmentAndIdDeliverableType(@Param("idRequirement") UUID idRequirement,
                                                           @Param("idDeliverableType") Integer idDeliverableType);
    @Query("select r from UnitDeliverable r " +
        "    where r.vehicleDocumentRequirement.idRequirement = :idRequirement ")
    Page<UnitDeliverable> queryByIdRequirement(@Param("idRequirement") UUID idRequirement, Pageable page);

    @Query(" select r from UnitDeliverable r" +
        " join VehicleDocumentRequirement v on (v = r.vehicleDocumentRequirement)" +
        " join v.statuses s" +
        " where ((v.vehicle.idFrame = :idFrame) or (:idFrame is null))" +
        " and ((v.vehicle.idMachine = :idMachine) or (:idMachine is null))" +
        " and ((v.requestPoliceNumber = :requestPoliceNumber) or (:requestPoliceNumber is null))" +
        " and (s.idStatusType in (36))" +
        " and (v.internal.idInternal = :idInternal)" +
        " order by  r.refNumber desc ")
    List<UnitDeliverable> findOneByIdFrameAndIdMachineAndPoliceNumber(
        @Param("idFrame") String idFrame,
        @Param("idMachine") String idMachine,
        @Param("requestPoliceNumber") String requestPoliceNumber,
        @Param("idInternal") String idInternal);


//    @Query("select r from UnitDeliverable r where r.vehicleDocumentRequirement. = :BpkbNumber")
//    UnitDeliverable findByBpkbNumber(@Param("BpkbNumber") String BpkbNumber);

}

