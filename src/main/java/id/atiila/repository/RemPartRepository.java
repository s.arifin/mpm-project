package id.atiila.repository;

import id.atiila.domain.RemPart;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the RemPart entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface RemPartRepository extends JpaRepository<RemPart, String> {


    @Query("select distinct rem_part from RemPart rem_part left join fetch rem_part.features left join fetch rem_part.categories")
    List<RemPart> findAllWithEagerRelationships();

    @Query("select rem_part from RemPart rem_part left join fetch rem_part.features left join fetch rem_part.categories where rem_part.idProduct = :id")
    RemPart findOneWithEagerRelationships(@Param("id") String id);
    
}

