package id.atiila.repository;

import id.atiila.domain.PartyCategory;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the PartyCategory entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface PartyCategoryRepository extends JpaRepository<PartyCategory, Integer> {
    
    @Query("select r from PartyCategory r where (r.categoryType.idCategoryType = :idCategoryType)")
    Page<PartyCategory> queryByIdCategoryType(@Param("idCategoryType") Integer idCategoryType, Pageable pageable); 

	@Query("select r from PartyCategory r where (1=2)")
    Page<PartyCategory> queryNothing(Pageable pageable); 
}

