package id.atiila.repository;

import id.atiila.domain.Uom;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Uom entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface UomRepository extends JpaRepository<Uom, String> {
    
    @Query("select r from Uom r where (r.uomType.idUomType = :idUomType)")
    Page<Uom> queryByIdUomType(@Param("idUomType") Integer idUomType, Pageable pageable); 

	@Query("select r from Uom r where (1=2)")
    Page<Uom> queryNothing(Pageable pageable); 
}

