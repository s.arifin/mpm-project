package id.atiila.repository;

import id.atiila.domain.SalesBooking;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the SalesBooking entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface SalesBookingRepository extends JpaRepository<SalesBooking, UUID> {

    @Query("select r from SalesBooking r "
	    + " where ((r.requirement.idRequirement = :idRequirement) or (:idRequirement is null)) "
)
    Page<SalesBooking> findByParams(@Param("idRequirement") String idRequirement, Pageable pageable);

    @Query("select r from SalesBooking r " +
        " where (current_timestamp between r.dateFrom and r.dateThru) " +
        " and (r.idProduct = :idproduct) " +
        " and (r.idFeature = :idfeature)" +
        " and (r.yearAssembly = :yearassembly)" +
        " and (r.idInternal = :idinternal)")
    List<SalesBooking> getTotalBooking(
        @Param("idinternal") String idinternal,
        @Param("idproduct") String idproduct,
        @Param("idfeature") Integer idfeature,
        @Param("yearassembly") Integer yearassembly
    );

    @Query("select r from SalesBooking r " +
        "   where r.requirement.idRequirement = :idReq " +
        "   and current_timestamp between r.dateFrom and r.dateThru")
    List<SalesBooking> salesBookingSurActive(@Param("idReq") UUID idReq);

    @Query("select r from SalesBooking r " +
        " where ((r.requirement.idRequirement = :idRequirement)) ")
    List<SalesBooking> listSalesBookingByIdReq(@Param("idRequirement") UUID idRequirement);
}

