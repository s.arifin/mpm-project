package id.atiila.repository;

import id.atiila.domain.PurposeType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PurposeType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface PurposeTypeRepository extends JpaRepository<PurposeType, Integer> {
}

