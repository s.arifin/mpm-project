package id.atiila.repository;

import id.atiila.domain.Internal;
import id.atiila.domain.ProductPackageReceipt;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ProductPackageReceipt entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ProductPackageReceiptRepository extends JpaRepository<ProductPackageReceipt, UUID> {

    @Query("select r from ProductPackageReceipt r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (r.internal = :internal)" +
        "      and (s.idStatusType not in (13, 17))")
    Page<ProductPackageReceipt> findActiveProductPackageReceipt(@Param("internal") Internal intr, Pageable pageable);

    @Query("select r from ProductPackageReceipt r "
	    + " where ((r.internal.idInternal = :idInternal) or (:idInternal is null)) "
	    + " and ((r.shipFrom.idShipTo = :idShipFrom) or (:idShipFrom is null)) ")
    Page<ProductPackageReceipt> findByParams(@Param("idInternal") String idInternal, @Param("idShipFrom") String idShipFrom, Pageable pageable);

    @Query("select r from ProductPackageReceipt r where (1=2)")
    Page<ProductPackageReceipt> queryNothing(Pageable pageable);
}

