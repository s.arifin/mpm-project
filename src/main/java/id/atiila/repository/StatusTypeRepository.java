package id.atiila.repository;

import id.atiila.domain.StatusType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the StatusType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface StatusTypeRepository extends JpaRepository<StatusType, Integer> {
    StatusType findByIdStatusType(Integer id);
}
