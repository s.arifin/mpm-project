package id.atiila.repository;

import id.atiila.domain.OrderItem;
import id.atiila.domain.PickingSlip;
import id.atiila.domain.ShipmentItem;
import id.atiila.domain.Shipment;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the PickingSlip entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface PickingSlipRepository extends JpaRepository<PickingSlip, UUID> {

    @Query("select r from PickingSlip r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<PickingSlip> findActivePickingSlip(Pageable pageable);

    @Query("select r from PickingSlip r where (r.orderItem = :oi) ")
    List<PickingSlip> findPisByOrderItem(@Param("oi") OrderItem oi);

    @Query("select p from PickingSlip p left  join OrderItem oi on p.orderItem = oi " +
        " left join oi.shipmentItems s left join s.shipmentItem os left join os.shipment a left join a.statuses b where (b.idStatusType = 25) and (current_timestamp between b.dateFrom and b.dateThru)")
    Page<PickingSlip> findActivePickingSlipbyShipmentStatus(Pageable pageable);

    @Query("select p from PickingSlip p left  join OrderItem oi on p.orderItem = oi " +
        " left join oi.shipmentItems s left join s.shipmentItem os left join os.shipment a left join a.statuses b where (b.idStatusType = 25) and (current_timestamp between b.dateFrom and b.dateThru) ")
    Page<PickingSlip> findActivePickingSlipbyShipmentStatusandID(Pageable pageable);

    @Query("select r from PickingSlip r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13))" +
        "      and (r.orderItem = :orderItem) ")
    List<PickingSlip> findPickingSlip(@Param("orderItem") OrderItem orderItem);

    @Query("select r from PickingSlip r where (r.orderItem.idOrderItem = :idOrdite)")
    List<PickingSlip> findOneByidOrderItem(@Param("idOrdite") UUID idOrdite);

    @Query("select r from PickingSlip r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13))" +
        "      and (r.orderItem.idOrderItem = :idOrdite) ")
    List<PickingSlip> findPickingSlipByidOrderItem(@Param("idOrdite") UUID idOrdite);

}

