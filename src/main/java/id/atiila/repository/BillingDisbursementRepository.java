package id.atiila.repository;

import id.atiila.domain.BillingDisbursement;

import java.util.List;
import java.util.UUID;

import id.atiila.domain.BillingItem;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the BillingDisbursement entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface BillingDisbursementRepository extends JpaRepository<BillingDisbursement, UUID> {

    @Query("select r from BillingDisbursement r " +
        "     join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17))")
    Page<BillingDisbursement> findActiveBillingDisbursement(Pageable pageable);

    @Query("select r from BillingDisbursement r "
	    + " where ((r.billingType.idBillingType = :idBillingType) or (:idBillingType is null)) "
	    + " and ((r.internal.idInternal = :idInternal) or (:idInternal is null)) "
	    + " and ((r.billTo.idBillTo = :idBillTo) or (:idBillTo is null)) "
	    + " and ((r.billFrom.idBillTo = :idBillFrom) or (:idBillFrom is null)) "
	    + " and ((r.vendor.idVendor = :idVendor) or (:idVendor is null)) ")
    Page<BillingDisbursement> findByParams(@Param("idBillingType") String idBillingType, @Param("idInternal") String idInternal, @Param("idBillTo") String idBillTo, @Param("idBillFrom") String idBillFrom, @Param("idVendor") String idVendor, Pageable pageable);

    @Query("select r from BillingDisbursement r " +
        "    where (r.vendorInvoice = :vendorInvoice)")
    List<BillingDisbursement> findOneByVendorInvoice(@Param("vendorInvoice") String VendorInvoice);

    @Query("select r from BillingDisbursement r where r.vendorInvoice = :vendorInvoice")
    List<BillingDisbursement> findByVendorInvoice (@Param("vendorInvoice")String vendorInvoice);

    @Query("select r from BillingDisbursement r " +
        "     join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17))" +
        "      and (r.internal.idInternal = :idInternal)")
    Page<BillingDisbursement> findActiveBilling(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from BillingItem r " +
        "   join BillingDisbursement c on c.idBilling = r.billing.idBilling " +
        "   where(c.idBilling = :idBilling)")
    List<BillingItem> findBillingItemByBillingDisbursement(@Param("idBilling") UUID idBilling);

}

