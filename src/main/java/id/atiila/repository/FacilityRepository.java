package id.atiila.repository;

import id.atiila.domain.Facility;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Facility entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface FacilityRepository extends JpaRepository<Facility, UUID> {

    @Query("select r from Facility r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11, 18))")
    Page<Facility> findActiveFacility(Pageable pageable);

    @Query("select r from Facility r where (r.facilityType.idFacilityType = :idFacilityType)")
    Page<Facility> queryByIdFacilityType(@Param("idFacilityType") Integer idFacilityType, Pageable pageable);

    @Query("select r from Facility r where (r.partOf.idFacility = :idPartOf)")
    Page<Facility> queryByIdPartOf(@Param("idPartOf") UUID idPartOf, Pageable pageable);

	@Query("select r from Facility r where (1=2)")
    Page<Facility> queryNothing(Pageable pageable);

    Facility findByFacilityCode(String code);

    @Query("select r from Facility r where (r.facilityType.idFacilityType = :id)")
    List<Facility> findAllByidFacilityType(@Param("id") Integer id);

}

