package id.atiila.repository;

import id.atiila.domain.DealerReminderType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DealerReminderType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface DealerReminderTypeRepository extends JpaRepository<DealerReminderType, Integer> {
}

