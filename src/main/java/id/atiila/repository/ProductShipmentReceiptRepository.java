package id.atiila.repository;

import id.atiila.domain.ProductShipmentReceipt;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ProductShipmentReceipt entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ProductShipmentReceiptRepository extends JpaRepository<ProductShipmentReceipt, UUID> {

    @Query("select r from ProductShipmentReceipt r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17))")
    Page<ProductShipmentReceipt> findActiveProductShipmentReceipt(Pageable pageable);

    @Query("select r from ProductShipmentReceipt r where (2=1) ")
    Page<ProductShipmentReceipt> queryNothing(Pageable pageable);

    @Query("select r from ProductShipmentReceipt r where (r.shipmentPackage.idPackage = :idShipmentPackage) ")
    Page<ProductShipmentReceipt> findByIdPackage(@Param("idShipmentPackage") UUID idShipmentPackage, Pageable pageable);

}

