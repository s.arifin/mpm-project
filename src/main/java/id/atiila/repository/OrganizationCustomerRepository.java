package id.atiila.repository;

import id.atiila.domain.OrganizationCustomer;
import java.util.UUID;

import id.atiila.domain.Party;
import id.atiila.domain.Vendor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the OrganizationCustomer entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface OrganizationCustomerRepository extends CustomerRepository<OrganizationCustomer> {

    @Query("select distinct r from OrganizationCustomer r where (r.party.idParty = :idParty)")
    OrganizationCustomer findByIdParty(@Param("idParty") UUID idParty);

    @Query("select distinct r from OrganizationCustomer r " +
        "join CustomerRelationship cr on (cr.customer.idCustomer = r.idCustomer)" +
        "where (cr.internal.idInternal = :idInternal)")
    Page<OrganizationCustomer> findByInternal(@Param("idInternal") String idInternal, Pageable pageable);

}

