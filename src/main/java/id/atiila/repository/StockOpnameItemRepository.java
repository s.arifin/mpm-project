package id.atiila.repository;

import id.atiila.domain.StockOpnameItem;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the StockOpnameItem entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface StockOpnameItemRepository extends JpaRepository<StockOpnameItem, UUID> {
    
    @Query("select r from StockOpnameItem r where (r.stockOpname.idStockOpname = :idStockOpname)")
    Page<StockOpnameItem> queryByIdStockOpname(@Param("idStockOpname") UUID idStockOpname, Pageable pageable); 
    
    @Query("select r from StockOpnameItem r where (r.product.idProduct = :idProduct)")
    Page<StockOpnameItem> queryByIdProduct(@Param("idProduct") String idProduct, Pageable pageable); 
    
    @Query("select r from StockOpnameItem r where (r.container.idContainer = :idContainer)")
    Page<StockOpnameItem> queryByIdContainer(@Param("idContainer") UUID idContainer, Pageable pageable); 

	@Query("select r from StockOpnameItem r where (1=2)")
    Page<StockOpnameItem> queryNothing(Pageable pageable); 
}

