package id.atiila.repository;

import id.atiila.domain.CommunicationEventCDB;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the CommunicationEventCDB entity.
 * BeSmart  Team
 */

@Repository
public interface CommunicationEventCDBRepository extends JpaRepository<CommunicationEventCDB, UUID> {

    @Query("select r from CommunicationEventCDB r join r.statuses s where " +
           " (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<CommunicationEventCDB> findActiveCommunicationEventCDB(Pageable pageable);

    @Query("select r from CommunicationEventCDB r join r.statuses s " +
        "    where (r.idCustomer = :idCustomer) " +
        "      and (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17))")
    Page<CommunicationEventCDB> findActiveCommunicationEventCDBByIdCustomer(@Param("idCustomer") String idCustomer, Pageable pageable);

    @Query("select r from CommunicationEventCDB r join r.statuses s " +
        "    where (r.idCustomer = :idCustomer) " +
        "      and (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17))")
    List<CommunicationEventCDB> queryFindCommunicationEventCDBByIdCustomer(@Param("idCustomer") String idCustomer);

}

