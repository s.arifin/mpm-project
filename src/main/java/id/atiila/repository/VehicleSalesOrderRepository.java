package id.atiila.repository;

import id.atiila.domain.*;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import id.atiila.service.pto.OrderPTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import id.atiila.service.dto.SummaryFincoDTO;



/**
 *
 *
 * Spring Data JPA repository for the VehicleSalesOrder entity.
 *
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface VehicleSalesOrderRepository extends JpaRepository<VehicleSalesOrder, UUID> {

    @Query("select distinct r from VehicleSalesOrder r ")
    List<VehicleSalesOrder> findAllWithEagerRelationships();

    @Query("select r from Product r where r.idProduct = :idProduct  ")
    Product findOneByIdProduct(@Param("idProduct") String id);

    @Query("select r from VehicleSalesOrder r join r.statuses s " +
        "   join SalesUnitRequirement sur on r.idSalesUnitRequirement = sur.idRequirement " +
        "   where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and (s.idStatusType in (88) ) " +
        "   and (r.orderType.idOrderType in (11, 110, 111, 112, 113,114, 202)) " +
        "   and r.internal.idInternal like :idInternal " +
        "   order by r.dateEntry asc ")
    Page<VehicleSalesOrder> queryActiveLeasingInvoice(@Param("idInternal") String idInternal, Pageable page);

    @Query("select r from VehicleSalesOrder r join r.statuses s " +
        "   join SalesUnitRequirement sur on r.idSalesUnitRequirement = sur.idRequirement " +
        "   where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and (s.idStatusType in (17, 62) ) " +
        "   and (r.leasing is not null) " +
        "   and (r.bastFinco is null) " +
        "   and r.internal.idInternal like :idInternal " +
        "   order by r.dateEntry asc ")
    Page<VehicleSalesOrder> queryActiveLeasingInvoiceSubmit(@Param("idInternal") String idInternal, Pageable page);


    @Query("select r from VehicleSalesOrder r join r.statuses s " +
        "   join SalesUnitRequirement sur on r.idSalesUnitRequirement = sur.idRequirement " +
        " where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and (s.idStatusType in (10, 11)) " +
        "   and (r.orderType.idOrderType in (11, 110, 111, 112, 113,114, 202)) " +
        "   and r.internal.idInternal like :idInternal " +
        "   and ((r.afcVerifyValue = 1 ) or (r.afcVerifyValue = 2 ))" +
        "   order by r.dateEntry desc ")
    Page<VehicleSalesOrder> queryForApprovalKacab(@Param("idInternal") String idInternal, Pageable page);


    //TODO: Ricky [ganti search internal cukup pakai idinternal sehingga mengurangi traffic mencari internal dahulu]
    @Query("select r from VehicleSalesOrder r join r.statuses s " +
        " where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and (r.internal.idInternal = :idinternal) " +
        "   and (s.idStatusType not in (13, 17, 62, 78, 88)) " +
        "   and (r.orderType.idOrderType in (11, 110, 111, 112, 113,114, 202)) " +
        "   order by r.dateEntry desc ")
    Page<VehicleSalesOrder> queryAllVso(@Param("idinternal") String idinternal, Pageable page);

    @Query("select r from VehicleSalesOrder r join r.statuses s " +
        "     join SalesUnitRequirement sur on (sur.idRequirement = r.idSalesUnitRequirement)" +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17, 62, 78, 88)) " +
        "      and (r.orderType.idOrderType in (11, 110, 111, 112, 113,114, 202)) " +
        "      and ((sur.requestIdMachineAndFrame <> null) or (sur.requestIdMachineAndFrame = '' )) " +
        "      and r.internal.idInternal like :idInternal " +
        "      order by r.dateEntry desc ")
    Page<VehicleSalesOrder> queryAllVsoForManualMatching(@Param("idInternal") String idInternal, Pageable page);

    @Query("select r from SalesUnitRequirement r where (r.idRequirement = :id)")
    SalesUnitRequirement getSurById(@Param("id") UUID id);

    @Query(" select bi from OrderItem oi  " +
        " join oi.billingItems ob join ob.billingItem bi " +
        " join oi.orders o " +
        " where bi.billing.billingNumber = :bilNum ")
    List<BillingItem> findByBillingNumber(String bilNum);

    @Query(" select v from VehicleSalesOrder  v left join fetch v.details " +
           "  where v.idOrder = :id  and v.orderType.idOrderType in (11, 110, 111, 112, 113,114, 202) " )
    VehicleSalesOrder findByIdWithEagerDetail(@Param("id") UUID id);
    //        "    and v.internal.idInternal like :idInternal")

    @Query("select r from VehicleSalesOrder r join r.statuses s join PersonalCustomer q on (q = r.customer) " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType in :#{#param.status}) " +
//        "      and (s.idStatusType not in :#{#param.statusNotIn}) " +
        "      and (s.dateFrom between :#{#param.orderDateFrom} and :#{#param.orderDateThru}) " +
        "      and (r.orderType.idOrderType in (11, 110, 111, 112, 113,114, 202)) " +
        "      and ((r.idSalesman = :#{#param.salesmanId}) or (null = :#{#param.salesmanId}))" +
        "      and ((r.internal.idInternal = :#{#param.internalId}) or (null = :#{#param.internalId})) " +
        "      and ((r.leasing.idPartyRole = :#{#param.leasingCompanyId}) or (null = :#{#param.leasingCompanyId}))" +
        "       order by r.dateEntry desc ")
    Page<VehicleSalesOrder> findByFilter(Pageable pageable, @Param("param") OrderPTO params);

//    @Query("select r from VehicleSalesOrder r join r.statuses s join PersonalCustomer q on (q = r.customer) " +
//        "      join OrderItem oi on r.idOrder = oi.orders  " +
//        "      join oi.billingItems ob join ob.billingItem bi " +
//        "      join Billing b on ob.idBilling = b.idBilling" +
//        "      where (current_timestamp between s.dateFrom and s.dateThru) " +
//        "      and (s.idStatusType in :#{#param.status}) " +
//        "      and (s.dateFrom between :#{#param.orderDateFrom} and :#{#param.orderDateThru}) " +
//        "      and (b.dateCreate between :#{#param.ivuDateFrom} and :#{#param.ivuDateThru}) " +
//        "      and (r.orderType.idOrderType in (11, 110, 111, 112, 113,114, 202)) " +
//        "      and ((r.idSalesman = :#{#param.salesmanId}) or (null = :#{#param.salesmanId}))" +
//        "      and ((r.internal.idInternal = :#{#param.internalId}) or (null = :#{#param.internalId})) " +
//        "      and ((r.leasing.idPartyRole = :#{#param.leasingCompanyId}) or (null = :#{#param.leasingCompanyId}))" +
//        "       order by r.dateEntry desc ")
//    Page<VehicleSalesOrder> findFiltersAdminHandling(Pageable pageable, @Param("param") OrderPTO params);

    @Query("select sur from VehicleSalesOrder r join SalesUnitRequirement sur on r.idSalesUnitRequirement = sur.idRequirement " +
        "    where r.idOrder = :orderId " +
           "   and r.internal.idInternal = :idInternal ")
    SalesUnitRequirement findSurByOrderId(@Param("idInternal") String idInternal, @Param("orderId") UUID orderId );

    @Query("select v from RequirementOrderItem a join a.orderItem b join VehicleSalesOrder v on (v = b.orders) " +
           " where (a.requirement.idRequirement = :idReq )")
    VehicleSalesOrder findVSObyReg(@Param("idReq") UUID idReq);

    @Query("select r from VehicleSalesOrder r where (1 = 2)")
    Page<VehicleSalesOrder> queryNothing(Pageable page);

    @Query("select r from VehicleSalesOrder r where (r.customer.idCustomer = :idCustomer)")
    Page<VehicleSalesOrder> queryByCustomer(@Param("idCustomer") String idCustomer, Pageable page);

    @Query("select r from VehicleSalesOrder r where (r.orderNumber = :orderNumber)")
    Page<VehicleSalesOrder> queryByOrderNumber(@Param("orderNumber") String orderNumber, Pageable page);

    @Query(" select vso from VehicleSalesOrder vso" +
        " join OrderItem oi on vso.idOrder = oi.orders  " +
        " join oi.billingItems ob join ob.billingItem bi " +
        " join oi.orders o " +
        " where bi.billing.billingNumber = :billingNumber ")
    Page<VehicleSalesOrder> queryByNoIvu(@Param("billingNumber") String billingNumber, Pageable page);

    @Query("select r from VehicleSalesOrder r " +
        " join SalesUnitRequirement sur on r.idSalesUnitRequirement = sur.idRequirement " +
        " join OrderItem oi on r.idOrder = oi.orders  " +
        " join oi.billingItems ob join ob.billingItem bi " +
        " join oi.orders o " +
        " where bi.billing.billingNumber  = :billingNumber")
    VehicleSalesOrder findByIdRequirmentAndIdSaleType(@Param("billingNumber") String billingNumber);

    @Query(" select vso from VehicleSalesOrder vso" +
        " join OrderItem oi on vso.idOrder = oi.orders  " +
        " join oi.billingItems ob join ob.billingItem bi " +
        " join oi.orders o " +
        " where bi.billing.idBilling = :idBilling ")
    VehicleSalesOrder findByBillingId(@Param("idBilling") UUID idBilling);

    @Query(" select vso from VehicleSalesOrder vso" +
        " join OrderItem oi on vso.idOrder = oi.orders  " +
        " join oi.billingItems ob join ob.billingItem bi " +
        " join oi.orders o " +
        " where bi.billing.idBilling = :idBilling ")
    List<VehicleSalesOrder> findListByBillingId(@Param("idBilling") UUID idBilling);

    @Query(" select vso from VehicleSalesOrder vso" +
        " join OrderItem oi on vso.idOrder = oi.orders  " +
        " join oi.billingItems ob join ob.billingItem bi " +
        " join oi.orders o " +
        " where bi.billing.idBilling = :idBilling ")
    Page<VehicleSalesOrder> findPageByBillingId(@Param("idBilling") UUID idBilling, Pageable pageable);

    @Query("select r from VehicleSalesOrder r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType = 10) " +
        "      and (r.customer = :customer) " +
        "      and (r.billTo = :billTo) " +
        "      and (r.internal = :internal) " +
        "      and (r.orderType.idOrderType = :idOrderType) ")
    List<VehicleSalesOrder> queryCustomerDraft( @Param("internal") Internal internal,
                                                @Param("customer") Customer customer,
                                                @Param("billTo") BillTo billTo,
                                                @Param("idOrderType") Integer idOrderType);

    @Query("select r from VehicleSalesOrder r join r.statuses s join r.internal intrn" +
        "   where (current_timestamp between s.dateFrom and s.dateThru)" +
        // "   and (s.idStatusType not in (13, 17, 62, 78, 88)) " +
        "   and (r.orderType.idOrderType in (11, 110, 111, 112, 113,114, 202)) " +
        "   and r.orderNumber like :data " +
        "   and r.internal.idInternal like :idInternal ")
    Page<VehicleSalesOrder> findToReindex(@Param("data") String data, @Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from VehicleSalesOrder r join r.statuses s join r.internal intrn" +
        " join SalesUnitRequirement sur on r.idSalesUnitRequirement = sur.idRequirement " +
        " join OrderItem oi on r.idOrder = oi.orders  " +
        " join oi.billingItems ob join ob.billingItem bi " +
        " join oi.orders o " +
        "   where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and (s.idStatusType in (17, 62, 88) ) " +
        "   and (r.orderType.idOrderType in (11, 110, 111, 112, 113,114, 202)) " +
        "   and bi.billing.billingNumber  like :data " +
        "   and r.internal.idInternal like :idInternal ")
    Page<VehicleSalesOrder> findtoReIndexAH(@Param("data") String data, @Param("idInternal") String idInternal, Pageable page);

    @Query("select r from VehicleSalesOrder r join r.statuses s join r.internal intrn" +
        " join SalesUnitRequirement sur on r.idSalesUnitRequirement = sur.idRequirement " +
        " join OrderItem oi on r.idOrder = oi.orders  " +
        " join oi.billingItems ob join ob.billingItem bi " +
        " join oi.orders o " +
        "   where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and (s.idStatusType in (17) ) " +
        "   and (r.orderType.idOrderType in (11, 110, 111, 112, 113,114, 202)) " +
        "   and bi.billing.billingNumber  like :data " +
        "   and r.internal.idInternal like :idInternal ")
    Page<VehicleSalesOrder> findtoReIndexAHSubmit(@Param("data") String data, @Param("idInternal") String idInternal, Pageable page);

    @Query("select r from VehicleSalesOrder r join r.statuses s " +
        "   join SalesUnitRequirement sur on r.idSalesUnitRequirement = sur.idRequirement " +
        "   where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and (s.idStatusType in (88) ) " +
        "   and (r.orderType.idOrderType in (11, 110, 111, 112, 113,114, 202)) " +
        "   order by r.dateEntry asc ")
    List<VehicleSalesOrder> findToReIndexAdminHandling();


    @Query(" select oi from VehicleSalesOrder vso" +
        " join OrderItem oi on vso.idOrder = oi.orders  " +
        " where vso.idOrder = :idOrder ")
    OrderItem findOrderItemByVSO(@Param("idOrder") UUID idOrder);


    @Query("select r from VehicleSalesOrder r join r.statuses s " +
        " where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and (r.idSalesUnitRequirement = :idReq) " +
        "   and (s.idStatusType in (10)) " +
        "   and (r.orderType.idOrderType in (11, 110, 111, 112, 113,114, 202)) " +
        "   order by r.dateEntry desc ")
    VehicleSalesOrder queryFindDraftVSO(@Param("idReq") UUID idReq);

    @Query("select r from VehicleSalesOrder r join r.statuses s " +
        " where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and (r.internal.idInternal = :idinternal) " +
        "   and (s.idStatusType not in (13, 17, 62, 78, 88)) " +
        "   and (r.orderType.idOrderType in (11, 110, 111, 112, 113,114, 202)) " +
        "   and r.idSalesman =:idSales" +
        "   order by r.dateEntry desc ")
    Page<VehicleSalesOrder> queryAllVsoSales(@Param("idinternal") String idinternal, @Param("idSales") UUID idSales, Pageable page);

    @Query("select r from VehicleSalesOrder r join r.statuses s " +
        " where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and r.idSalesUnitRequirement = :idRequirement")
    List<VehicleSalesOrder> queryVSOByIdRequirement(@Param("idRequirement") UUID idRequirement);

    @Query("select r from VehicleSalesOrder r " +
        "   join SalesUnitRequirement sur on r.idSalesUnitRequirement = sur.idRequirement" +
        "   where sur.idRequest = :idRequest")
    List<VehicleSalesOrder> getVsoByIdRequest (@Param("idRequest") UUID idRequest);


    @Query("select r from VehicleSalesOrder r join r.statuses s" +
        "   join SalesUnitRequirement sur on r.idSalesUnitRequirement = sur.idRequirement" +
        "   where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and sur.idRequest = :idRequest" +
        "   and sur.requestIdMachine is not null " +
        "   and sur.requestIdFrame is not null" +
        "   and s.idStatusType = 74")
    List<VehicleSalesOrder> getVsoMatchingByIdRequest (@Param("idRequest") UUID idRequest);

    @Query("select r from VehicleSalesOrder r join r.statuses s" +
        "   where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and r.idOrder = :idorder")
    List<VehicleSalesOrder> getVSOList (@Param("idorder") UUID idorder);

    @Query("select new id.atiila.service.dto.SummaryFincoDTO(r.bastFinco) " +
        "   from VehicleSalesOrder r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "    and (r.bastFinco is not null)" +
        "    and (r.internal.idInternal = :internal)" +
        "    group by r.bastFinco")
    Page<SummaryFincoDTO> queryBast(@Param("internal") String idInternal, Pageable pageable);

    @Query("select r " +
        "    from VehicleSalesOrder r " +
        "    join r.statuses s " +
        "   where (s.idStatusType = :status) " +
        "     and (current_timestamp between s.dateFrom and s.dateThru) "+
        "     and (r.internal.idInternal = :idInternal)" )
    Page<VehicleSalesOrder> queryByInternalStatus(@Param("idInternal") String idInternal, @Param("status") Integer status, Pageable pageable);

//    @Query (value = "select vso.* from vehicle_sales_order vso" +
//        "   join order_item oi on vso.idorder = oi.idorder " +
//        "   join order_billing_item obi on oi.idordite = obi.idordite " +
//        "   join billing_item bi on obi.idbilite = bi.idbilite " +
//        "   join vehicle_sales_billing vsb on bi.idbilling = vsb.idbilling " +
//        "   where vso.idorder = ?1" +
//        "   ",
//        countQuery = "select count(*) from vehicle_sales_order vso with (NOLOCK) " +
//            "        join order_item oi on vso.idorder = oi.idorder " +
//            "        join order_billing_item obi on oi.idordite = obi.idordite " +
//            "        join billing_item bi on obi.idbilite = bi.idbilite " +
//            "        join vehicle_sales_billing vsb on bi.idbilling = vsb.idbilling " +
//            "        where vso.idorder = ?1",
//        nativeQuery = true)
    @Query(" select r from VehicleSalesOrder  r " +
        "    join OrderItem oi on r = oi.orders " +
        "    join OrderBillingItem obi on obi.orderItem = oi" +
        "    join BillingItem bi on obi.billingItem = bi" +
        "    join VehicleSalesBilling vsb on bi.billing = vsb" +
        "    where r.idOrder =:idOrder")
    List<VehicleSalesOrder> vsoExistBilling(@Param("idOrder") UUID idOrder);

    @Query(" select r from VehicleSalesOrder r " +
        "   join r.statuses s " +
        "   where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and s.idStatusType = 61" +
        "   and r.internal.idInternal = :idInternal" +
        "   order by r.dateEntry asc")
    List<VehicleSalesOrder> findvsoApprove(@Param("idInternal") String idInternal);
}
