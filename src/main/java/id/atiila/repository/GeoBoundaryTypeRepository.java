package id.atiila.repository;

import id.atiila.domain.GeoBoundaryType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the GeoBoundaryType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface GeoBoundaryTypeRepository extends JpaRepository<GeoBoundaryType, Integer> {
}

