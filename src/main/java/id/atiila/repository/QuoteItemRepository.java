package id.atiila.repository;

import id.atiila.domain.QuoteItem;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the QuoteItem entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface QuoteItemRepository extends JpaRepository<QuoteItem, UUID> {
}

