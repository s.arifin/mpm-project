package id.atiila.repository;

import id.atiila.domain.OrderItem;
import id.atiila.domain.PackageReceipt;
import id.atiila.domain.UnitShipmentReceipt;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the UnitShipmentReceipt entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface UnitShipmentReceiptRepository extends JpaRepository<UnitShipmentReceipt, UUID> {

    @Query("select r from UnitShipmentReceipt r " +
        "     join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<UnitShipmentReceipt> findActiveUnitShipmentReceipt(Pageable pageable);

    @Query(" select r from UnitShipmentReceipt r where (r.shipmentPackage.idPackage = :idShipmentPackage) " +
           " and ((r.qtyAccept = :qtyAccept) or (:qtyAccept is null))")
    Page<UnitShipmentReceipt> findByParams(@Param("idShipmentPackage") UUID idShipmentPackage,
                                           @Param("qtyAccept") Double qtyAccept,
                                           Pageable pageable);

    @Query(" select r from UnitShipmentReceipt r" +
           " where ((r.idFrame = :idFrame) or (:idFrame is null))" +
           " and ((r.idMachine = :idMachine) or (:idMachine is null))" +
           " and (r.shipmentPackage.idPackage = :idPackage)")
    UnitShipmentReceipt findOneByIdFrameAndIdMachineAndShipmentPackage(
        @Param("idFrame") String idFrame,
        @Param("idMachine") String idMachine,
        @Param("idPackage") UUID idPackage);

    @Query (" select r from PackageReceipt r where r.idPackage = :idPackage")
    PackageReceipt findPackageReceipt(@Param("idPackage") UUID idPackage);

    @Query (" select r from OrderItem r" +
            " join r.orders o" +
            " where r.idframe = :idFrame" +
            " and o.orderType = 20 ")
    OrderItem findOrderItem (@Param("idFrame") String idFrame);

    @Query("select r from UnitShipmentReceipt r where r.shipmentPackage.idPackage = :idPackage")
    List<UnitShipmentReceipt> findByIdPackage(@Param("idPackage") UUID idPackage);



}

