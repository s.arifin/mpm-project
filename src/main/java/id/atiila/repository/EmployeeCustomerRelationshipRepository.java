package id.atiila.repository;

import id.atiila.domain.EmployeeCustomerRelationship;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the EmployeeCustomerRelationship entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface EmployeeCustomerRelationshipRepository extends JpaRepository<EmployeeCustomerRelationship, UUID> {
    
    @Query("select r from EmployeeCustomerRelationship r where (r.statusType.idStatusType = :idStatusType)")
    Page<EmployeeCustomerRelationship> queryByIdStatusType(@Param("idStatusType") Integer idStatusType, Pageable pageable); 
    
    @Query("select r from EmployeeCustomerRelationship r where (r.relationType.idRelationType = :idRelationType)")
    Page<EmployeeCustomerRelationship> queryByIdRelationType(@Param("idRelationType") Integer idRelationType, Pageable pageable); 
    
    @Query("select r from EmployeeCustomerRelationship r where (r.employee.idEmployee = :idEmployee)")
    Page<EmployeeCustomerRelationship> queryByIdEmployee(@Param("idEmployee") String idEmployee, Pageable pageable); 
    
    @Query("select r from EmployeeCustomerRelationship r where (r.customer.idCustomer = :idCustomer)")
    Page<EmployeeCustomerRelationship> queryByIdCustomer(@Param("idCustomer") String idCustomer, Pageable pageable); 

	@Query("select r from EmployeeCustomerRelationship r where (1=2)")
    Page<EmployeeCustomerRelationship> queryNothing(Pageable pageable); 
}

