package id.atiila.repository;

import id.atiila.domain.BookingType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the BookingType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface BookingTypeRepository extends JpaRepository<BookingType, Integer> {


}

