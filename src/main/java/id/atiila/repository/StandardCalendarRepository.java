package id.atiila.repository;

import id.atiila.domain.Internal;
import id.atiila.domain.StandardCalendar;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

import java.time.ZonedDateTime;

/**
 * Spring Data JPA repository for the StandardCalendar entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface StandardCalendarRepository extends JpaRepository<StandardCalendar, Integer> {

    @Query("select r from StandardCalendar r where (1=2)")
    Page<StandardCalendar> queryNothing(Pageable pageable);

    @Query("select r from StandardCalendar r where (r.internal.idInternal = :idInternal)")
    Page<StandardCalendar> findByIdInternal(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from StandardCalendar r where (r.internal.idInternal = :idInternal) and (r.idCalendarType = :idType) and (r.dateKey >= :dateRequest) ")
    Page<StandardCalendar> findByTypeByIdInternal(@Param("idType") Integer idType,
                                                  @Param("idInternal") String idInternal,
                                                  @Param("dateRequest") ZonedDateTime dateRequest,
                                                  Pageable pageable);


    @Query("select r from StandardCalendar r where (r.internal.idInternal = :idInternal) ")
    Page<StandardCalendar> findByParams(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from StandardCalendar r where (r.internal.idInternal = :idInternal) and (r.idCalendarType = :idCalendarType) ")
    Page<StandardCalendar> findByParamsCalType(@Param("idInternal") String idInternal,
                                               @Param("idCalendarType") Integer idCalendarType,
                                               Pageable pageable);

    @Query("select r from StandardCalendar r where (r.internal = :internal) " +
        "      and (r.idCalendarType = :calType) " +
        "      and (r.dateKey = :dateKey) ")
    StandardCalendar findOneByDateKeyAndIdCalendarType(@Param("internal") Internal internal, @Param("dateKey") ZonedDateTime key, @Param("calType") Integer idCalType);

    @Query("select r from StandardCalendar r " +
        "    where (:date between r.dateFrom and r.dateThru) " +
        "      and (r.idCalendarType = :caltype) " +
        "      and (r.internal = :internal) ")
    StandardCalendar findMonthlyCalendar(@Param("internal") Internal internal, @Param("caltype") Integer calType, @Param("date") ZonedDateTime date);
}

