package id.atiila.repository;

import id.atiila.domain.StockOpnameInventory;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the StockOpnameInventory entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface StockOpnameInventoryRepository extends JpaRepository<StockOpnameInventory, UUID> {
    
    @Query("select r from StockOpnameInventory r where (r.inventoryItem.idInventoryItem = :idInventoryItem)")
    Page<StockOpnameInventory> queryByIdInventoryItem(@Param("idInventoryItem") UUID idInventoryItem, Pageable pageable); 
    
    @Query("select r from StockOpnameInventory r where (r.item.idStockopnameItem = :idItem)")
    Page<StockOpnameInventory> queryByIdItem(@Param("idItem") UUID idItem, Pageable pageable); 

	@Query("select r from StockOpnameInventory r where (1=2)")
    Page<StockOpnameInventory> queryNothing(Pageable pageable); 
}

