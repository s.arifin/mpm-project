package id.atiila.repository;

import id.atiila.domain.*;

import java.util.List;
import java.util.UUID;

import id.atiila.service.pto.SuspectPTO;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the SuspectPerson entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface SuspectPersonRepository extends JpaRepository<SuspectPerson, UUID> {
    @Query(" select r from SuspectPerson r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru)" +
           " and (s.idStatusType in (10, 11))" +
           " and ((r.dealer.idInternal = :internal) or (:internal is null))")
    Page<SuspectPerson> findActiveSuspectPerson(Pageable pageable, @Param("internal") String idInternal);

    @Query(" select r from SuspectPerson r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru)" +
           " and (r.suspectType.idSuspectType = :idsuspecttype) and (s.idStatusType in (10, 11))" +
           " and ((r.dealer.idInternal = :internal) or (:internal is null))")
    Page<SuspectPerson> findActiveSuspectPersonBySuspectType(Pageable pageable,
                                                             @Param("idsuspecttype") Integer idSuspectType,
                                                             @Param("internal") String idInternal);

    @Query(" select r from SuspectPerson r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru)" +
           " and (s.idStatusType in (10, 11)) and (r.person = :person)" +
           " and ((r.dealer.idInternal = :internal) or (:internal is null))")
    List<SuspectPerson> findSuspectPersonByPerson(@Param("person") Person person,
                                                  @Param("internal") String idInternal);

    @Query(" select r from SuspectPerson r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru)" +
           " and (s.idStatusType in (10, 11)) and (r.person = :person) and (r.suspectType.idSuspectType = :idsuspecttype)" +
           " and ((r.dealer.idInternal = :internal) or (:internal is null))")
    List<SuspectPerson> findSuspectPersonByPersonAndSuspectType(@Param("person") Person person,
                                                                @Param("idsuspecttype") Integer idSuspectType,
                                                                @Param("internal") String idInternal);

    @Query(" select r from SuspectPerson r join r.statuses s " +
        " where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in :#{#param.status}) " +
        " and ((r.marketName = :#{#param.marketName}) or (null = :#{#param.marketName})) " +
        " and ((r.person.workType.idWorkType = :#{#param.workTypeId}) or (null = :#{#param.workTypeId})) " +
        " and ((r.person.postalAddress.district.idGeobou = :#{#param.districtId}) or (null = :#{#param.districtId})) " +
        " and ((r.salesCoordinator.idPartyRole = :#{#param.coordinatorSalesId}) or (null = :#{#param.coordinatorSalesId})) " +
        " and ((r.salesman.idPartyRole = :#{#param.salesmanId}) or (null = :#{#param.salesmanId})) " +
        " and ((r.saleType.idSaleType = :#{#param.saleTypeId}) or (null = :#{#param.saleTypeId})) " +
        " and ((r.suspectType.idSuspectType = :#{#param.suspectTypeId}) or (null = :#{#param.suspectTypeId})) " +
        " and ((r.dealer.idInternal = :#{#param.internalId}) or (null = :#{#param.internalId}))")
    Page<SuspectPerson> findByFilter(Pageable pageable, @Param("param") SuspectPTO params);

    @Query("select r from Person r where (r.idParty = :id)")
    Person getPersonById(@Param("id") UUID id);

    @Query("select r from SuspectType r where (r.idSuspectType = :id)")
    SuspectType getSuspectTypeById(@Param("id") Integer id);

    @Query("select r from WorkType r where (r.description = :description)")
    WorkType getWorkTypeByDescription(@Param("description") String description);

    @Query("select r from Internal r where (r.idInternal = :internal)")
    Internal getInternalById(@Param("internal") String idInternal);
}

