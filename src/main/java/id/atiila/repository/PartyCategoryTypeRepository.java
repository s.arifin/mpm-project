package id.atiila.repository;

import id.atiila.domain.PartyCategoryType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the PartyCategoryType entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface PartyCategoryTypeRepository extends JpaRepository<PartyCategoryType, Integer> {
    
    @Query("select r from PartyCategoryType r where (r.parent.idCategoryType = :idParent)")
    Page<PartyCategoryType> queryByIdParent(@Param("idParent") Integer idParent, Pageable pageable); 

	@Query("select r from PartyCategoryType r where (1=2)")
    Page<PartyCategoryType> queryNothing(Pageable pageable); 
}

