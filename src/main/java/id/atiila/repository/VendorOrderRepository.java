package id.atiila.repository;

import id.atiila.domain.VendorOrder;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the VendorOrder entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface VendorOrderRepository extends JpaRepository<VendorOrder, UUID> {

    @Query("select r from VendorOrder r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<VendorOrder> findActiveVendorOrder(Pageable pageable);

    @Query("select r from VendorOrder r "
	    + " where ((r.vendor.idVendor = :idVendor) or (:idVendor is null)) "
	    + " and ((r.internal.idInternal = :idInternal) or (:idInternal is null)) "
	    + " and ((r.billTo.idBillTo = :idBillTo) or (:idBillTo is null)) "
)
    Page<VendorOrder> findByParams(@Param("idVendor") String idVendor, @Param("idInternal") String idInternal, @Param("idBillTo") String idBillTo, Pageable pageable);

}

