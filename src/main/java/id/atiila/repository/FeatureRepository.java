package id.atiila.repository;

import id.atiila.domain.Feature;
import id.atiila.domain.FeatureType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Spring Data JPA repository for the Feature entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface FeatureRepository extends JpaRepository<Feature, Integer> {

    @Query("select r from Feature r where (r.featureType.idFeatureType = :idFeatureType)")
    Page<Feature> queryByIdFeatureType(@Param("idFeatureType") Integer idFeatureType, Pageable pageable);

	@Query("select r from Feature r where (1=2)")
    Page<Feature> queryNothing(Pageable pageable);

    List<Feature> findByRefKey(String refKey);

    Feature findTop1ByOrderByIdFeatureDesc();

    @Query("select r from Feature r where (r.featureType.idFeatureType = :idCat) and (r.refKey = :key) ")
    Feature findByKey(@Param("idCat") Integer idCat, @Param("key") String id);

    @Query("select r from Feature r where (r.featureType = :featureType)")
    List<Feature> queryByFeatureType(@Param("featureType") FeatureType featureType);

    @Query("select r from Feature r where (r.featureType.idFeatureType = :idFeatureType) and (r.refKey = :refKey)")
    Feature queryByRefKey(@Param("idFeatureType") Integer idFeatureType, @Param("refKey") String refKey);
}

