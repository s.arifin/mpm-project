package id.atiila.repository;

import id.atiila.domain.Dimension;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Dimension entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface DimensionRepository extends JpaRepository<Dimension, Integer> {
    
    @Query("select r from Dimension r where (r.dimensionType.idDimensionType = :idDimensionType)")
    Page<Dimension> queryByIdDimensionType(@Param("idDimensionType") Integer idDimensionType, Pageable pageable); 

	@Query("select r from Dimension r where (1=2)")
    Page<Dimension> queryNothing(Pageable pageable); 
}

