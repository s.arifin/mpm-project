package id.atiila.repository;

import id.atiila.domain.InventoryMovement;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the InventoryMovement entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface InventoryMovementRepository extends JpaRepository<InventoryMovement, UUID> {
    @Query("select r from InventoryMovement r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<InventoryMovement> findActiveInventoryMovement(Pageable pageable);
    
}

