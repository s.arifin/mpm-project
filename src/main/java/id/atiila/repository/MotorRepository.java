package id.atiila.repository;

import id.atiila.domain.Motor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Motor entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface MotorRepository extends JpaRepository<Motor, String> {

    @Query("SELECT motor FROM Motor motor WHERE idProduct LIKE '%:keyword%' OR description LIKE '%:keyword%'")
    List<Motor> findByIdProductOrDescription(String keyword);

    @Query("select distinct motor from Motor motor left join fetch motor.features left join fetch motor.categories")
    List<Motor> findAllWithEagerRelationships();

    @Query("select motor from Motor motor left join fetch motor.features left join fetch motor.categories where motor.idProduct = :id")
    Motor findOneWithEagerRelationships(@Param("id") String id);

    @Query("select m from Motor m inner join RuleHotItem rhi on m.idProduct = rhi.idProduct " +
        "where (current_timestamp between rhi.dateFrom and rhi.dateThru)")
    List<Motor> findHotItem();

//    @Query("select m, " +
//        "(select sum(p.price) from PriceComponent p where (p.product.idProduct = m.idProduct) " +
//        "and (p.priceType.idPriceType in(10,101)) and (current_timestamp between p.dateFrom and p.dateThru) " +
//        "and p.idParty = :idparty) as price from Motor m")
//    List<Motor> findAllMotor(
//        @Param("idparty") UUID idparty,
//        Pageable pageable
//    );
}

