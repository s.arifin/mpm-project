package id.atiila.repository;

import id.atiila.domain.Internal;
import id.atiila.domain.InventoryItem;
import id.atiila.domain.StockOpname;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the StockOpname entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface StockOpnameRepository extends JpaRepository<StockOpname, UUID> {

    @Query("select r from StockOpname r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17))" +
        "      and (r.internal = :internal)")
    Page<StockOpname> findActiveStockOpname(@Param("internal") Internal intr, Pageable pageable);

    @Query("select r from StockOpname r where (r.calendar.idCalendar = :idCalendar)")
    Page<StockOpname> queryByIdCalendar(@Param("idCalendar") Integer idCalendar, Pageable pageable);

    @Query("select r from StockOpname r where (r.internal.idInternal = :idInternal)")
    Page<StockOpname> queryByIdInternal(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from StockOpname r where (r.stockOpnameType.idStockOpnameType = :idStockOpnameType)")
    Page<StockOpname> queryByIdStockOpnameType(@Param("idStockOpnameType") Integer idStockOpnameType, Pageable pageable);

	@Query("select r from StockOpname r where (1=2)")
    Page<StockOpname> queryNothing(Pageable pageable);

    @Query("select r from InventoryItem r " +
        "     join Internal i on (i.organization.idParty = r.idOwner ) " +
        "     left join Container c on (c.idContainer = r.idContainer) " +
        "     left join Facility f on (f.idFacility = r.idFacility) " +
        "    where (r.qty > 0) " +
        "      and (i.idInternal = :idInternal) ")
    List<InventoryItem> findAllInventory(@Param("idInternal") String idInternal);

}

