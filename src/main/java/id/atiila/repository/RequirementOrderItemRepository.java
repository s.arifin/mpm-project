package id.atiila.repository;

import id.atiila.domain.OrderItem;
import id.atiila.domain.Requirement;
import id.atiila.domain.RequirementOrderItem;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the RequirementOrderItem entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface RequirementOrderItemRepository extends JpaRepository<RequirementOrderItem, UUID> {

    @Query("select r from RequirementOrderItem r where (r.orderItem.idOrderItem = :idOrderItem)")
    Page<RequirementOrderItem> queryByIdOrderItem(@Param("idOrderItem") UUID idOrderItem, Pageable pageable);

    @Query("select r from RequirementOrderItem r where (r.requirement.idRequirement = :idRequirement)")
    Page<RequirementOrderItem> queryByIdRequirement(@Param("idRequirement") UUID idRequirement, Pageable pageable);

	@Query("select r from RequirementOrderItem r where (1=2)")
    Page<RequirementOrderItem> queryNothing(Pageable pageable);

    @Query("select r from RequirementOrderItem r where (r.requirement = :requirement) and (r.orderItem = :orderItem)")
    RequirementOrderItem findRequirementOrderItem(@Param("requirement") Requirement r, @Param("orderItem") OrderItem orderItem);

    @Query("select r from RequirementOrderItem r where (r.orderItem = :orderItem)")
    RequirementOrderItem findRequirementOrderItemByOrder(@Param("orderItem") OrderItem orderItem);

    @Query("select r from RequirementOrderItem r where (r.orderItem.idOrderItem = :idOrderItem)")
    RequirementOrderItem findRequirementOrderItemByOrder(@Param("idOrderItem") UUID idOrderItem);

}

