package id.atiila.repository;

import id.atiila.domain.OrderPayment;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the OrderPayment entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface OrderPaymentRepository extends JpaRepository<OrderPayment, UUID> {
    
    @Query("select r from OrderPayment r where (r.orders.idOrder = :idOrders)")
    Page<OrderPayment> queryByIdOrders(@Param("idOrders") UUID idOrders, Pageable pageable); 
    
    @Query("select r from OrderPayment r where (r.payment.idPayment = :idPayment)")
    Page<OrderPayment> queryByIdPayment(@Param("idPayment") UUID idPayment, Pageable pageable); 

	@Query("select r from OrderPayment r where (1=2)")
    Page<OrderPayment> queryNothing(Pageable pageable); 
}

