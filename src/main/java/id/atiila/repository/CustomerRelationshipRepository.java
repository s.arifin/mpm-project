package id.atiila.repository;

import id.atiila.domain.CustomerRelationship;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the CustomerRelationship entity.
 * atiila consulting
 */

@Repository
public interface CustomerRelationshipRepository extends JpaRepository<CustomerRelationship, UUID> {

    @Query("select r from CustomerRelationship r where (r.statusType.idStatusType = :idStatusType)")
    Page<CustomerRelationship> queryByIdStatusType(@Param("idStatusType") Integer idStatusType, Pageable pageable);

    @Query("select r from CustomerRelationship r where (r.relationType.idRelationType = :idRelationType)")
    Page<CustomerRelationship> queryByIdRelationType(@Param("idRelationType") Integer idRelationType, Pageable pageable);

    @Query("select r from CustomerRelationship r where (r.internal.idInternal = :idInternal)")
    Page<CustomerRelationship> queryByIdInternal(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from CustomerRelationship r where (r.customer.idCustomer = :idCustomer)")
    Page<CustomerRelationship> queryByIdCustomer(@Param("idCustomer") String idCustomer, Pageable pageable);

	@Query("select r from CustomerRelationship r where (1=2)")
    Page<CustomerRelationship> queryNothing(Pageable pageable);

    @Query("select r from CustomerRelationship r where (r.internal.idInternal = :idInternal) and (r.customer.idCustomer = :idCustomer)")
    Page<CustomerRelationship> queryByInternalCustomer(@Param("idInternal") String idInternal, @Param("idCustomer") String idCustomer, Pageable pageable);


    @Query("select r from CustomerRelationship r " +
        "    where (r.customer.idCustomer = :idCustomer) " +
        "      and (r.internal.idInternal = :idInternal) " +
        "      and (r.relationType.idRelationType = :idRelationType)" +
        "      and (current_timestamp between r.dateFrom and r.dateThru) ")
    CustomerRelationship findRelationship(@Param("idRelationType") Integer idRelationType,
                                          @Param("idInternal") String idInternal,
                                          @Param("idCustomer") String idCustomer);

}

