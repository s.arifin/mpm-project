package id.atiila.repository;

import id.atiila.domain.OrderRequestItem;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the OrderRequestItem entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface OrderRequestItemRepository extends JpaRepository<OrderRequestItem, UUID> {

    @Query("select r from OrderRequestItem r where (r.orderItem.idOrderItem = :idOrderItem)")
    Page<OrderRequestItem> queryByIdOrderItem(@Param("idOrderItem") UUID idOrderItem, Pageable pageable);

    @Query("select r from OrderRequestItem r where (r.requestItem.idRequestItem = :idRequestItem)")
    Page<OrderRequestItem> queryByIdRequestItem(@Param("idRequestItem") UUID idRequestItem, Pageable pageable);

    @Query("select r from OrderRequestItem r where (r.requestItem.idRequestItem = :idRequestItem)")
    List<OrderRequestItem> queryByRequestItem(@Param("idRequestItem") UUID idRequestItem);

    @Query("select r from OrderRequestItem r where (1=2)")
    Page<OrderRequestItem> queryNothing(Pageable pageable);
}

