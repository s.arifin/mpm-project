package id.atiila.repository;

import id.atiila.domain.FacilityType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the FacilityType entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface FacilityTypeRepository extends JpaRepository<FacilityType, Integer> {

	@Query("select r from FacilityType r where (1=2)")
    Page<FacilityType> queryNothing(Pageable pageable); 
}

