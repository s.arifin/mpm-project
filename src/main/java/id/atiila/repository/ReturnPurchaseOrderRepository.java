package id.atiila.repository;

import id.atiila.domain.ReturnPurchaseOrder;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ReturnPurchaseOrder entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ReturnPurchaseOrderRepository extends JpaRepository<ReturnPurchaseOrder, UUID> {

    @Query("select r from ReturnPurchaseOrder r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<ReturnPurchaseOrder> findActiveReturnPurchaseOrder(Pageable pageable);

    @Query("select r from ReturnPurchaseOrder r "
	    + " where ((r.vendor.idVendor = :idVendor) or (:idVendor is null)) "
	    + " and ((r.internal.idInternal = :idInternal) or (:idInternal is null)) "
	    + " and ((r.billTo.idBillTo = :idBillTo) or (:idBillTo is null)) "
)
    Page<ReturnPurchaseOrder> findByParams(@Param("idVendor") String idVendor, @Param("idInternal") String idInternal, @Param("idBillTo") String idBillTo, Pageable pageable);

}

