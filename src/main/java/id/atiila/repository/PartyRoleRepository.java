package id.atiila.repository;

import id.atiila.domain.PartyRole;

import java.util.UUID;

import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PartyRole entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface PartyRoleRepository extends JpaRepository<PartyRole,UUID> {


}
