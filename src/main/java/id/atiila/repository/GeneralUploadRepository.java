package id.atiila.repository;

import id.atiila.domain.GeneralUpload;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the GeneralUpload entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface GeneralUploadRepository extends JpaRepository<GeneralUpload, UUID> {

    @Query("select r from GeneralUpload r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType not in (13, 17))")
    Page<GeneralUpload> findActiveGeneralUpload(Pageable pageable);

    @Query("select r from GeneralUpload r where (r.internal.idInternal = :idInternal)")
    Page<GeneralUpload> queryByIdInternal(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from GeneralUpload r where (r.purpose.idPurposeType = :idPurpose)")
    Page<GeneralUpload> queryByIdPurpose(@Param("idPurpose") Integer idPurpose, Pageable pageable);

	@Query("select r from GeneralUpload r where (1=2)")
    Page<GeneralUpload> queryNothing(Pageable pageable);
}

