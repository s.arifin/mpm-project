package id.atiila.repository;

import id.atiila.domain.ShipmentPackage;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ShipmentPackage entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ShipmentPackageRepository extends JpaRepository<ShipmentPackage, UUID> {
    @Query("select r from ShipmentPackage r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<ShipmentPackage> findActiveShipmentPackage(Pageable pageable);
    
}

