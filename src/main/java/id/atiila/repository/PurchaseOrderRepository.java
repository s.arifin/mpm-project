package id.atiila.repository;

import id.atiila.domain.PurchaseOrder;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the PurchaseOrder entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, UUID> {

    @Query("select r from PurchaseOrder r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17))")
    Page<PurchaseOrder> findActivePurchaseOrder(Pageable pageable);

    @Query("select r from PurchaseOrder r "
	    + " where ((r.vendor.idVendor = :idVendor) or (:idVendor is null)) "
	    + "   and ((r.internal.idInternal = :idInternal) or (:idInternal is null)) "
	    + "   and ((r.billTo.idBillTo = :idBillTo) or (:idBillTo is null)) "
        + "   and ((r.orderNumber = :orderNumber) or (:orderNumber is null))")
    Page<PurchaseOrder> findByParams(@Param("idVendor") String idVendor,
                                     @Param("idInternal") String idInternal,
                                     @Param("idBillTo") String idBillTo,
                                     @Param("orderNumber") String orderNumber,
                                     Pageable pageable);

    PurchaseOrder findPurchaseOrderByOrderNumber(String OrderNumber);
}

