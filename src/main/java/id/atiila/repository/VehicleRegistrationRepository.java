package id.atiila.repository;

import java.util.List;
import java.util.UUID;

import id.atiila.domain.VehicleDocumentRequirement;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the VehicleRegistration entity.
 * atiila consulting
 */

@Repository
public interface VehicleRegistrationRepository extends JpaRepository<VehicleDocumentRequirement, UUID> {

    @Query("select r from VehicleDocumentRequirement r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17))")
    Page<VehicleDocumentRequirement> findActiveVehicleRegistration(Pageable pageable);

    @Query("select r from VehicleDocumentRequirement r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17))")
    List<VehicleDocumentRequirement> findActiveVehicleRegistration();
    
    @Query("select r from VehicleDocumentRequirement r where (r.internal.idInternal = :idInternal)")
    Page<VehicleDocumentRequirement> queryByIdInternal(@Param("idInternal") String idInternal, Pageable pageable);
    
    @Query("select r from VehicleDocumentRequirement r where (r.billTo.idBillTo = :idBillTo)")
    Page<VehicleDocumentRequirement> queryByIdBillTo(@Param("idBillTo") String idBillTo, Pageable pageable);
    
    @Query("select r from VehicleDocumentRequirement r where (r.vendor.idVendor = :idVendor)")
    Page<VehicleDocumentRequirement> queryByIdVendor(@Param("idVendor") String idVendor, Pageable pageable);

	@Query("select r from VehicleDocumentRequirement r where (1=2)")
    Page<VehicleDocumentRequirement> queryNothing(Pageable pageable);

    @Query("select r from VehicleDocumentRequirement r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17) or 1=1) " +
        "      and (r.internal.idInternal = :idInternal or r.internal.root.idInternal = :idInternal)")
    Page<VehicleDocumentRequirement> queryByInternalActive(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from VehicleDocumentRequirement r  " +
        "    where not exists (select s from RequirementStatus s where (s.owner = r))")
    List<VehicleDocumentRequirement> queryImportedVDR();
}

