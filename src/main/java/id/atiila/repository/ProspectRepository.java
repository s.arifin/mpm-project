package id.atiila.repository;

import id.atiila.domain.Prospect;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Prospect entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ProspectRepository extends JpaRepository<Prospect, UUID> {

    @Query("select r from Prospect r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<Prospect> findActiveProspect(Pageable pageable);


    @Query("select r from Prospect r " +
        "   join SalesUnitRequirement sur on sur.idProspect = r.idProspect " +
        "   join sur.statuses s " +
        "   where s.idStatusType not in (13)" +
        "   and CURRENT_TIMESTAMP between s.dateFrom and s.dateThru" +
        "   and r.idProspect = :idProspect")
    List<Prospect> findProspectSur(@Param("idProspect") UUID idProspect);

    @Query("select r from Prospect r " +
        "   join SalesUnitRequirement sur on sur.idProspect = r.idProspect " +
        "   join VehicleSalesOrder v on sur.idRequirement = v.idSalesUnitRequirement" +
        "   join v.statuses s " +
        "   where s.idStatusType not in (13)" +
        "   and CURRENT_TIMESTAMP between s.dateFrom and s.dateThru" +
        "   and r.idProspect = :idProspect")
    List<Prospect> findProspectVsoDraft(@Param("idProspect") UUID idProspect);

    @Query("select r from Prospect r " +
        "   join SalesUnitRequirement sur on sur.idProspect = r.idProspect " +
        "   join VehicleSalesOrder v on sur.idRequirement = v.idSalesUnitRequirement" +
        "   join v.statuses s " +
        "   where s.idStatusType in (17, 88)" +
        "   and CURRENT_TIMESTAMP between s.dateFrom and s.dateThru" +
        "   and r.idProspect = :idProspect")
    List<Prospect> findProspectVsoComplete(@Param("idProspect") UUID idProspect);

    Prospect findByProspectNumber(String personalnumber);
}


