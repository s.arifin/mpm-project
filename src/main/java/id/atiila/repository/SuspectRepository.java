package id.atiila.repository;

import id.atiila.domain.Suspect;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Suspect entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface SuspectRepository extends JpaRepository<Suspect, UUID> {


    @Query("select r from Suspect r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<Suspect> findActiveSuspect(Pageable pageable);

    Suspect findOneBySuspectNumber(String id);
}

