package id.atiila.repository;

import id.atiila.domain.WeServiceType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the WeServiceType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface WeServiceTypeRepository extends JpaRepository<WeServiceType, Integer> {
}

