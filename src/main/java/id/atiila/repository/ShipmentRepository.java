package id.atiila.repository;

import id.atiila.domain.Shipment;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Shipment entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ShipmentRepository extends JpaRepository<Shipment, UUID> {

    @Query("select r from Shipment r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<Shipment> findActiveShipment(Pageable pageable);

    @Query("select r from Shipment r "
	    + " where ((r.shipmentType.idShipmentType = :idShipmentType) or (:idShipmentType is null)) "
	    + " and ((r.shipFrom.idShipTo = :idShipFrom) or (:idShipFrom is null)) "
	    + " and ((r.shipTo.idShipTo = :idShipTo) or (:idShipTo is null)) "
	    + " and ((r.addressFrom.idContact = :idAddressFrom) or (:idAddressFrom is null)) "
	    + " and ((r.addressTo.idContact = :idAddressTo) or (:idAddressTo is null)) "
)
    Page<Shipment> findByParams(@Param("idShipmentType") String idShipmentType, @Param("idShipFrom") String idShipFrom, @Param("idShipTo") String idShipTo, @Param("idAddressFrom") String idAddressFrom, @Param("idAddressTo") String idAddressTo, Pageable pageable);

}

