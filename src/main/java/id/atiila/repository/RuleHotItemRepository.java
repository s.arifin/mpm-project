package id.atiila.repository;

import id.atiila.domain.RuleHotItem;
import org.dom4j.rule.Rule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the RuleHotItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RuleHotItemRepository extends JpaRepository<RuleHotItem, Integer> {
    Page<RuleHotItem> findAllByIdProductAndIdInternal(String idProduct, String idinternal, Pageable pageable);

    Page<RuleHotItem> findAllByIdInternal(String idinternal, Pageable pageable);

    @Query("select count(r) as total from RuleHotItem r where (r.idProduct = :idproduct) and (r.idInternal = :idinternal) and (current_timestamp between r.dateFrom and r.dateThru)")
    Integer findTotalHotItemByProductAndInternal(
        @Param("idproduct") String idproduct,
        @Param("idinternal") String idinternal
    );

    @Query("select r from RuleHotItem r where (r.idProduct = :idproduct) and (r.idInternal = :idinternal) and (current_timestamp between r.dateFrom and r.dateThru)")
    List <RuleHotItem> findRoleHotItem(
        @Param("idproduct") String idproduct,
        @Param("idinternal") String idinternal
    );
}
