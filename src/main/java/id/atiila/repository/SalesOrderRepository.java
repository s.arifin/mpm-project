package id.atiila.repository;

import id.atiila.domain.SalesOrder;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the SalesOrder entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface SalesOrderRepository extends JpaRepository<SalesOrder, UUID> {

    @Query("select r from SalesOrder r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<SalesOrder> findActiveSalesOrder(Pageable pageable);

    @Query("select r from SalesOrder r "
	    + " where ((r.internal.idInternal = :idInternal) or (:idInternal is null)) "
	    + " and ((r.customer.idCustomer = :idCustomer) or (:idCustomer is null)) "
	    + " and ((r.billTo.idBillTo = :idBillTo) or (:idBillTo is null)) "
	    + " and ((r.saleType.idSaleType = :idSaleType) or (:idSaleType is null)) "
)
    Page<SalesOrder> findByParams(@Param("idInternal") String idInternal, @Param("idCustomer") String idCustomer, @Param("idBillTo") String idBillTo, @Param("idSaleType") String idSaleType, Pageable pageable);

}

