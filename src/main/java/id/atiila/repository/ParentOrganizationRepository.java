package id.atiila.repository;

import id.atiila.domain.ParentOrganization;

import java.util.UUID;

import id.atiila.domain.Party;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ParentOrganization entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ParentOrganizationRepository extends InternalRepository<ParentOrganization> {

}
