package id.atiila.repository;

import id.atiila.domain.PaymentMethodType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Spring Data JPA repository for the PaymentMethodType entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface PaymentMethodTypeRepository extends JpaRepository<PaymentMethodType, Integer> {

    List<PaymentMethodType> findAllByDescription(String desc);

    PaymentMethodType findByRefKey(String refkey);

	@Query("select r from PaymentMethodType r where (1=2)")
    Page<PaymentMethodType> queryNothing(Pageable pageable);
}

