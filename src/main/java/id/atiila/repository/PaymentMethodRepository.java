package id.atiila.repository;

import id.atiila.domain.PaymentMethod;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Spring Data JPA repository for the PaymentMethod entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface PaymentMethodRepository extends JpaRepository<PaymentMethod, Integer> {
    
    @Query("select r from PaymentMethod r where (r.methodType.idPaymentMethodType = :idMethodType)")
    Page<PaymentMethod> queryByIdMethodType(@Param("idMethodType") Integer idMethodType, Pageable pageable); 

	@Query("select r from PaymentMethod r where (1=2)")
    Page<PaymentMethod> queryNothing(Pageable pageable);

    @Query("select r from PaymentMethod r where (r.refKey = :refKey)")
    List<PaymentMethod> queryByRefKey(@Param("refKey") String refKey);
}

