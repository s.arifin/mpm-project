package id.atiila.repository;

import id.atiila.domain.RequestUnitInternal;
import java.util.UUID;

import id.atiila.service.dto.CustomPreRequestUnitInternalDTO;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the RequestUnitInternal entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface RequestUnitInternalRepository extends JpaRepository<RequestUnitInternal, UUID> {

    @Query("select r from RequestUnitInternal r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<RequestUnitInternal> findActiveRequestUnitInternal(Pageable pageable);

    @Query(" select r from RequestUnitInternal r "
	    + " where ((r.parent.idRequest = :idRequest) or (:idRequest is null)) "
	    + " and ((r.internal.idInternal = :idInternalFrom) or (:idInternalFrom is null)) "
	    + " and ((r.internalTo.idInternal = :idInternalTo) or (:idInternalTo is null)) ")
    Page<RequestUnitInternal> findByParams(@Param("idRequest") String idRequest,
                                           @Param("idInternalFrom") String idInternalFrom,
                                           @Param("idInternalTo") String idInternalTo, Pageable pageable);

    @Query("select new id.atiila.service.dto.CustomPreRequestUnitInternalDTO(udm.idRequirement, sur.dateCreate, sur.requirementNumber, sur.product.name, sur.product.idProduct, sur.color.description, sur.salesman.party.idParty, sur.salesman.coordinatorSales.party.idParty) " +
        "   from UnitDocumentMessage udm join SalesUnitRequirement sur on sur.idRequirement = udm.idRequirement " +
        "   join udm.statuses a " +
        "   where (sur.internal.idInternal = :idinternal)" +
        "   and (current_timestamp between a.dateFrom and a.dateThru)" +
        "   and (a.idStatusType = 11)")
    Page<CustomPreRequestUnitInternalDTO> findPreRequestUnitInternal(
        @Param("idinternal") String idinternal,
        Pageable pageable
    );

}

