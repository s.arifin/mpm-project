package id.atiila.repository;

import id.atiila.domain.Party;
import id.atiila.domain.ShipTo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

/**
 * Spring Data JPA repository for the ShipTo entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ShipToRepository extends JpaRepository<ShipTo, String> {

    @Query("select r from ShipTo r where (r.party.idParty = :idParty)")
    Page<ShipTo> queryByIdParty(@Param("idParty") UUID idParty, Pageable pageable);

    @Query("select r from ShipTo r where (r.idRoleType = :idRoleType)")
    Page<ShipTo> queryByIdRoleType(@Param("idRoleType") Integer idRoleType, Pageable pageable);

    @Query("select r from ShipTo r where (1=2)")
    Page<ShipTo> queryNothing(Pageable pageable);

    @Query("select r from ShipTo r where (r.party = :party)")
    ShipTo findByParty(@Param("party") Party p);

    @Query("select r from ShipTo r where (r.party.idParty = :party)")
    ShipTo findByParty(@Param("party") UUID idParty);

}

