package id.atiila.repository;

import id.atiila.domain.PartyRelationship;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the PartyRelationship entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface PartyRelationshipRepository extends JpaRepository<PartyRelationship, UUID> {
    
    @Query("select r from PartyRelationship r where (r.statusType.idStatusType = :idStatusType)")
    Page<PartyRelationship> queryByIdStatusType(@Param("idStatusType") Integer idStatusType, Pageable pageable); 
    
    @Query("select r from PartyRelationship r where (r.relationType.idRelationType = :idRelationType)")
    Page<PartyRelationship> queryByIdRelationType(@Param("idRelationType") Integer idRelationType, Pageable pageable); 

	@Query("select r from PartyRelationship r where (1=2)")
    Page<PartyRelationship> queryNothing(Pageable pageable); 
}

