package id.atiila.repository;

import id.atiila.domain.Internal;
import id.atiila.domain.InternalFacilityPurpose;
import id.atiila.domain.PurposeType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Spring Data JPA repository for the InternalFacilityPurpose entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface InternalFacilityPurposeRepository extends JpaRepository<InternalFacilityPurpose, UUID> {

    @Query("select r from InternalFacilityPurpose r where (r.internal.idInternal = :idInternal)")
    Page<InternalFacilityPurpose> queryByIdInternal(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from InternalFacilityPurpose r where (r.facility.idFacility = :idFacility)")
    Page<InternalFacilityPurpose> queryByIdFacility(@Param("idFacility") UUID idFacility, Pageable pageable);

    @Query("select r from InternalFacilityPurpose r where (r.purposeType.idPurposeType = :idPurposeType)")
    Page<InternalFacilityPurpose> queryByIdPurposeType(@Param("idPurposeType") Integer idPurposeType, Pageable pageable);

	@Query("select r from InternalFacilityPurpose r where (1=2)")
    Page<InternalFacilityPurpose> queryNothing(Pageable pageable);

    @Query("select r from InternalFacilityPurpose r " +
        "    where (r.internal = :internal) and (r.purposeType = :purposeType) " +
        "      and (current_timestamp  between r.dateFrom and r.dateThru) ")
    InternalFacilityPurpose queyByInternalAndPurposeType(@Param("internal") Internal intr, @Param("purposeType") PurposeType purposeType);
}

