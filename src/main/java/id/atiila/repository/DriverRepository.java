package id.atiila.repository;

import id.atiila.domain.Driver;
import id.atiila.domain.Party;
import id.atiila.domain.Salesman;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.UUID;


/**
 * Spring Data JPA repository for the Driver entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DriverRepository extends JpaRepository<Driver, UUID> {

    Driver findOneByParty(Party party);

    Driver findOneByIdDriver(String idDriver);

    @Query(" select r from Driver r join r.party per join UserMediator um on um.person.idParty = per.idParty" +
        " join Position pos on pos.owner.idUserMediator = um.idUserMediator" +
        " where ((pos.internal.idInternal = :internal) or (:internal is null))" +
        " and ((pos.positionType.idPositionType = :positiontype) or (:positiontype is null))")
    Page<Driver> findAllByInternal(@Param("internal") String idInternal, @Param("positiontype") Integer idPositionType, Pageable pageable);

}
