package id.atiila.repository;

import id.atiila.domain.Position;
import id.atiila.domain.PositionFullfillment;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the PositionFullfillment entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface PositionFullfillmentRepository extends JpaRepository<PositionFullfillment, Integer> {
    @Query("select r from PositionFullfillment r where (current_timestamp between r.dateFrom and r.dateThru) and (r.position = :position)")
    PositionFullfillment findPosition(@Param("position") Position position);

}

