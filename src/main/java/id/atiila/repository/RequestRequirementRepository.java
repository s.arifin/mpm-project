package id.atiila.repository;

import id.atiila.domain.RequestRequirement;

import java.util.List;
import java.util.UUID;

import id.atiila.service.dto.CustomRequestRequirementDTO;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the RequestRequirement entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface RequestRequirementRepository extends JpaRepository<RequestRequirement, UUID> {

    @Query("select r from RequestRequirement r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<RequestRequirement> findActiveRequestRequirement(Pageable pageable);

    @Query("select r from RequestRequirement r "
	    + " where ((r.idRequirement = :idRequirement) or (:idRequirement is null)) "
)
    Page<RequestRequirement> findByParams(@Param("idRequirement") String idRequirement, Pageable pageable);


    @Query("select new id.atiila.service.dto.CustomRequestRequirementDTO(r.idRequest, req.requirementNumber, r.idRequirement, reqitem.feature.description, reqitem.product.name, r.dateCreate, r.requestNumber, a.idStatusType) " +
        "from RequestRequirement r join Requirement req on (r.idRequirement = req.idRequirement) " +
        "join RequestItem reqitem on (reqitem.request.idRequest = r.idRequest) join r.statuses a " +
        "where (current_timestamp between a.dateFrom and a.dateThru) and (r.idInternal = :idinternal)")
    List<CustomRequestRequirementDTO> getCustomRequestRequirement(
        @Param("idinternal") String idinternal
    );
}

