package id.atiila.repository;

import java.time.ZonedDateTime;
import java.util.Set;
import java.util.UUID;

import id.atiila.domain.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Person entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface PersonRepository extends JpaRepository<Person, UUID> {

    List<Person> findAllByPersonalIdNumber(String id);

    @Query("select r from Person r where (r.firstName = :fName) and (r.lastName = :lName) and (r.cellPhone1 = :cellPhone)")
    Set<Person> findByFirstNameAndLastNameAndCellPhone1(@Param("fName") String firstName, @Param("lName") String lastName,
                                                        @Param("cellPhone") String cellPhone1);

    @Query("select r from Person r " +
        "   join Customer c on r = c.party" +
        "   join CustomerRelationship cr on cr.customer = c " +
        "   join VehicleIdentification vi on c = vi.customer" +
        "   where r.personalIdNumber = :nik and cr.internal.idInternal = :idInternal" +
        "   and c.idMPM is not null" +
        "   group by r.idParty, r.firstName, r.lastName, r.idType, r.personalIdNumber, r.bloodType, r.cellPhone1, r.cellPhone2, r.dob, r.familyIdNumber, r.workType, r.religionType, r.privateMail, r.postalAddress, r.pob, r.gender, r.phone, r.taxIdNumber")
    Page<Person> findPersonCustomerWithNik(@Param("nik") String nik, @Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from Person r where (r.cellPhone1 = :cellPhone)")
    Set<Person> findByCellPhone1(@Param("cellPhone") String cellPhone1);

    @Query("select distinct r from Person r where (r.firstName = :fName) and (r.lastName = :lName) and (r.pob = :pob) and (r.dob = :dob)")
    Person findOneByFirstNameAndLastNameAndPobAndDob(@Param("fName") String fName, @Param("lName") String lName,
                                                     @Param("pob") String pob, @Param("dob") ZonedDateTime dob);

    List<Person> findAllByFirstNameAndLastNameAndPobAndDob(String fName, String lName, String pob, ZonedDateTime dob);

    @Query("select r.personalIdNumber from Person r group by r.personalIdNumber having count(r.personalIdNumber) > 1")
    List<String> queryDoubleNik();

    @Query("select r from Person r where (r.personalIdNumber = :ktp)")
    List<Person> findByKtp(@Param("ktp") String ktp);

    @Query("select r from Person r " +
        "   join Customer c on r = c.party" +
        "   join CustomerRelationship cr on cr.customer = c " +
        "   where r.personalIdNumber = :nik and (cr.internal.idInternal = :idInternal or cr.internal.idInternal = :idroot)" +
        "   and c.idMPM is not null" )
    Page<Person> findAllPersonCustomer(@Param("nik") String nik,
                                       @Param("idInternal") String idInternal,
                                       @Param("idroot") String idroot,
                                       Pageable pageable);

    @Query("select r from Person r where r.idParty in (:idParty)")
    Page<Person> findOnePersonPage (@Param("idParty") UUID idParty, Pageable pageable);


}

