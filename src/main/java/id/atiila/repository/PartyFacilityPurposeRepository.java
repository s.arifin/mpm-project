package id.atiila.repository;

import id.atiila.domain.PartyFacilityPurpose;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the PartyFacilityPurpose entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface PartyFacilityPurposeRepository extends JpaRepository<PartyFacilityPurpose, UUID> {
    
    @Query("select r from PartyFacilityPurpose r where (r.organization.idParty = :idOrganization)")
    Page<PartyFacilityPurpose> findByIdOrganization(@Param("idOrganization") UUID idOrganization, Pageable pageable); 
    
    @Query("select r from PartyFacilityPurpose r where (r.facility.idFacility = :idFacility)")
    Page<PartyFacilityPurpose> findByIdFacility(@Param("idFacility") UUID idFacility, Pageable pageable); 
    
    @Query("select r from PartyFacilityPurpose r where (r.purposeType.idPurposeType = :idPurposeType)")
    Page<PartyFacilityPurpose> findByIdPurposeType(@Param("idPurposeType") Integer idPurposeType, Pageable pageable); 

}

