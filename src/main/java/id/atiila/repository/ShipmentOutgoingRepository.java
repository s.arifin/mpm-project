package id.atiila.repository;

import id.atiila.domain.*;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import id.atiila.service.dto.CustomShipmentDTO;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ShipmentOutgoing entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ShipmentOutgoingRepository extends JpaRepository<ShipmentOutgoing, UUID> {
//    status shipment
//    and (s.idStatusType in (10, 11)

    @Query("select r from ShipmentOutgoing r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru )")
    Page<ShipmentOutgoing> findActiveShipmentOutgoing(Pageable pageable);

    @Query("select r from ShipmentOutgoing r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru ) and (s.idStatusType in :idstatustype) ")
    Page<ShipmentOutgoing> findShipmentOutgoingByStatusType(@Param("idstatustype") Set<Integer> idStatusType, Pageable pageable);

//    @Query("select p from PickingSlip p left  join OrderItem oi on p.orderItem = oi " +
//        " left join oi.shipmentItems s  left join s.shipment a left join a.statuses b where (b.idStatusType = 25) and (current_timestamp between b.dateFrom and b.dateThru) ")

//    @Query("select so from ShipmentOutgoing so left join so.Shipment s left join s.ShipmentItem si left join si.OrderItem oi left join oi.PickingSlip ps join s.statuses ss " +
//        "where (current_timestamp between ss.dateFrom and ss.dateThru ) and (ps.idSlip = :idPicking ) ")
//    Page<ShipmentOutgoing> findShipmentOutgoingByOrderItem(@Param("idPicking") String idPicking, Pageable pageable);

   @Query("select new id.atiila.service.dto.CustomShipmentDTO(v, s, pis, so, ivi) " +
       " from VehicleSalesOrder v left join v.details oi left join oi.shipmentItems os left join os.shipmentItem si  " +
       " left join si.shipment s left join s.statuses b left join PickingSlip pis on (pis.orderItem = oi)" +
       " left join ShipmentOutgoing so on (so.idShipment = s.idShipment)" +
       " left join ItemIssuance ii on (ii.shipmentItem = si.idShipmentItem) " +
       " left join InventoryItem ivi on (ivi.idInventoryItem = ii.inventoryItem)" +
       "where (b.idStatusType in :idstatustype ) and (current_timestamp between b.dateFrom and b.dateThru)" +
       " and ((v.internal.idInternal = :internal) or (:internal is null))")
   Page<CustomShipmentDTO> findActiveVSObyShipmentStatusandID(Pageable pageable,@Param("idstatustype") Set<Integer> idstatustype, @Param("internal") String idInternal);
//    ------ /\ dto dari sample

    @Query("select r from VehicleSalesOrder r where r.idOrder = :id")
    VehicleSalesOrder findVSO(@Param("id") UUID id);

    @Query("select r from Internal r where r.idInternal = :id")
    Internal findInternal(@Param("id") String id);

    @Query("select r from ShipTo r where r.idShipTo = :id")
    ShipTo findShipTo(@Param("id") String id);

    @Query("select r from ShipTo r where r.party.idParty = :id")
    ShipTo findShipToFromIdParty(@Param("id") UUID id);

    @Query("select sum(r.qty - r.qtyBooking) from InventoryItem r where (r.idOwner = :owner) and (r.idProduct = :idProduct) and (r.idFeature = :idFeature) and (r.qty > r.qtyBooking) ")
    Double getQtyOH(@Param("owner") UUID owner, @Param("idProduct") String idProduct, @Param("idFeature") Integer idFeature);

    @Query("select r from InventoryItem r " +
           " where (r.idOwner = :owner) and (r.idProduct = :idProduct) " +
           " and (r.idFeature = :idFeature) and (r.qty > r.qtyBooking) " +
           " and ( concat( r.idFrame , r.idMachine ) <> any ( select concat(s.idframe , s.idmachine) from SalesUnitRequirement s )) ")
    List<InventoryItem> getInventoryItem(@Param("owner") UUID owner, @Param("idProduct") String idProduct, @Param("idFeature") Integer idFeature);

//    @Query(value = " select r.* from Inventory_Item r " +
//        "    inner join internal i on r.idOwner = i.idparty " +
//        " where (i.idInternal = ?1 ) and (r.qty - r.qtybooking > 0) " +
//        "    and r.idProduct = ?2  " +
//        "    and r.idFeature = ?3 " +
//        "    and not exists " +
//        "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
//        "            and (z.idcolor = r.idFeature) " +
//        "            and (z.idMachine = r.idMachine))" +
//        "             \n#pageable\n      ",
//        nativeQuery = true)
    @Query(value = " select r.* from Inventory_Item r " +
        "    inner join internal i on r.idOwner = i.idparty " +
        " where (i.idInternal = ?1 ) and (r.qty - r.qtybooking > 0) " +
        "    and r.idProduct = ?2  " +
        "    and r.idFeature = ?3 " +
        "    and not exists " +
        "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
        "            and (z.idframe = r.idframe) " +
        "            and (z.idMachine = r.idMachine)" +
        "            and (z.idcolor = r.idFeature)) ",
        nativeQuery = true)
    List<InventoryItem> getInventoryItemNative(String intr,
                                               String idProduct,
                                               Integer idFeature);
//         "    inner join internal i on r.idOwner = i.organization.idParty " +

    // todo di remark karene ga pakai gudang dulu
//    @Query(" select r from InventoryItem r " +
//           " where (r.idOwner = :owner) and (r.idProduct = :idProduct) and (r.idFeature = :idFeature) " +
//           " and (r.qty > r.qtyBooking) and (r.idFrame = :noka ) and (r.idMachine = :nosin ) and (r.idFacility = :idFacility) ")
//    List<InventoryItem> getInventoryItemNoka(
//        @Param("owner") UUID owner,
//        @Param("idProduct") String idProduct,
//        @Param("idFeature") Integer idFeature,
//        @Param("noka") String noka,
//        @Param("nosin") String nosin,
//        @Param("idFacility") UUID idFacility
//    );

    @Query(" select r from InventoryItem r " +
        " where (r.idOwner = :owner) and (r.idProduct = :idProduct) and (r.idFeature = :idFeature) " +
        " and (r.qty > r.qtyBooking) and (r.idFrame = :noka ) and (r.idMachine = :nosin )")
    List<InventoryItem> getInventoryItemNoka(
        @Param("owner") UUID owner,
        @Param("idProduct") String idProduct,
        @Param("idFeature") Integer idFeature,
        @Param("noka") String noka,
        @Param("nosin") String nosin
    );

    @Query("select r from ShipmentOutgoing r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru ) and (s.idStatusType = 10) " +
        "      and (r.shipFrom = :shipFrom) " +
        "      and (r.shipTo = :shipTo) ")
    List<ShipmentOutgoing> getDraftSO(@Param("shipFrom") ShipTo shipFrom, @Param("shipTo") ShipTo shipTo);

    @Query("select r from ShipmentOutgoing r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru ) and (s.idStatusType = 10) " +
        "      and (r.shipFrom = :shipFrom) " +
        "      and (r.shipTo = :shipTo) " +
        "      and (r.internal = :internal) ")
    Page<ShipmentOutgoing> getDraftShipment(@Param("internal") Internal internal,
                                            @Param("shipFrom") ShipTo shipFrom,
                                            @Param("shipTo") ShipTo shipTo, Pageable pageable);

    @Query("select r from ShipmentOutgoing r where (1=2)")
    Page<ShipmentOutgoing> queryNothing(Pageable page);

    @Query("select r from PickingSlip r join r.statuses s " +
        "     join VehicleSalesOrder v on (v = r.orderItem.orders)" +
        "    where (current_timestamp  between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType = :idStatusType) " +
        "      and (v.internal.idInternal = :idInternal)" +
        "      order by r.orderItem.orders.orderNumber")
    Page<PickingSlip> queryPickingByStatusAndInternal(@Param("idInternal") String idInternal,
                                                      @Param("idStatusType") Integer idStatusType, Pageable page);

    @Query("select r from PickingSlip r join r.statuses s " +
        "     join VehicleSalesOrder v on (v = r.orderItem.orders)" +
        "     join SalesUnitRequirement sur on (v.idSalesUnitRequirement = sur.idRequirement)" +
        "     join Salesman sl on (sl = sur.salesman)" +
        "     join Salesman ks on (ks = sl.coordinatorSales)" +
        "     join Person p on (p = sl.party)" +
        "     join Person pp on (pp = ks.party)" +
        "     join InventoryItem ii on (ii = r.inventoryItem)" +
        "    where (current_timestamp  between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType = :idStatusType) " +
        "      and (v.internal.idInternal = :idInternal)" +
        "      and ((r.refferenceNumber like :data) or (p.firstName like :data) " +
        "       or (p.lastName like :data) or (ii.idFrame like :data)" +
        "       or (pp.firstName like :data) or (pp.lastName like :data) " +
        "       or (r.inventoryItem.idFrame like :data)) and (r.refferenceNumber like :reffNumber)" +
        "      order by r.orderItem.orders.orderNumber")
    Page<PickingSlip> queryPickingByStatusAndInternalSearch(@Param("data") String data,
                                                            @Param("reffNumber") String reffNumber,
                                                            @Param("idStatusType") Integer idStatusType,
                                                            @Param("idInternal") String idInternal, Pageable page);

    @Query("select r from PickingSlip r join r.statuses s " +
        "     join VehicleSalesOrder v on (v = r.orderItem.orders)" +
        "     join SalesUnitRequirement sur on (v.idSalesUnitRequirement = sur.idRequirement)" +
        "     join Salesman sl on (sl = sur.salesman)" +
        "     join Salesman ks on (ks = sl.coordinatorSales)" +
        "     join Person p on (p = sl.party)" +
        "     join Person pp on (pp = ks.party)" +
        "     join InventoryItem ii on (ii = r.inventoryItem)" +
        "    where (current_timestamp  between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType = :idStatusType) " +
        "      and (v.internal.idInternal = :idInternal)" +
        "      and ((r.refferenceNumber like :data) or (p.firstName like :data) " +
        "       or (p.lastName like :data) or (ii.idFrame like :data)" +
        "       or (pp.firstName like :data) or (pp.lastName like :data) " +
        "       or (r.inventoryItem.idFrame like :data)) and (r.refferenceNumber not like :reffNumber)" +
        "      order by r.orderItem.orders.orderNumber")
    Page<PickingSlip> queryPickingByStatusAndInternalNotReffNumber(@Param("data") String data,
                                                            @Param("reffNumber") String reffNumber,
                                                            @Param("idStatusType") Integer idStatusType,
                                                            @Param("idInternal") String idInternal, Pageable page);

    @Query("select r from PickingSlip r join r.statuses s " +
        "     join VehicleSalesOrder v on (v = r.orderItem.orders)" +
        "     join SalesUnitRequirement sur on (v.idSalesUnitRequirement = sur.idRequirement)" +
        "     join Salesman sl on (sl = sur.salesman)" +
        "     join Person p on (p = sl.party)" +
        "     join InventoryItem ii on (ii = r.inventoryItem)" +
        "    where (current_timestamp  between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType = :idStatusType) " +
        "      and (v.internal.idInternal = :idInternal)" +
        "      and ((r.refferenceNumber like :data) or (p.firstName like :data) " +
        "       or (p.lastName like :data) or (ii.idFrame like :data))" +
        "      order by r.orderItem.orders.orderNumber")
    Page<PickingSlip> queryGudangStatusAndInternalSearch(@Param("data") String data,
                                                            @Param("idStatusType") Integer idStatusType,
                                                            @Param("idInternal") String idInternal, Pageable page);

    @Query("select r from PickingSlip r join r.statuses s " +
        "     join VehicleSalesOrder v on (v = r.orderItem.orders)" +
        "     join SalesUnitRequirement sur on (v.idSalesUnitRequirement = sur.idRequirement)" +
        "     join InventoryItem ii on (ii = r.inventoryItem)" +
        "     join Feature f on (f.idFeature = ii.idFeature)" +
        "    where (current_timestamp  between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType in (18,24)) " +
        "      and (v.internal.idInternal = :idInternal)" +
        "      and ((ii.idProduct like :data) or (f.description like :data) " +
        "       or (ii.idFrame like :data))" +
        "      order by r.orderItem.orders.orderNumber")
    Page<PickingSlip> queryDeliveryOrderStatusAndInternalSearch(@Param("data") String data,
                                                         @Param("idInternal") String idInternal, Pageable page);

    @Query("select r from ShipmentOutgoing r join r.statuses s" +
        "   where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and (s.idStatusType = :idStatusType) " +
        "   and (r.internal.idInternal = :idInternal)")
    Page<ShipmentOutgoing> queryAllShipmentOutgoing11(@Param("idInternal") String idInternal,
                                                    @Param("idStatusType") Integer idStatusType, Pageable page);

    @Query("select r from OrderShipmentItem r " +
        "   join ShipmentItem si on (r.shipmentItem = si)" +
        "   join ShipmentOutgoing so on (si.shipment = so)" +
        "   join VehicleSalesOrder v on (v = r.orderItem.orders)" +
        "   join so.statuses s " +
        "   where (current_timestamp between s.dateFrom and s.dateThru)" +
        "   and (s.idStatusType = :idStatusType)" +
        "   and (v.internal.idInternal = :idInternal)" +
        "   order by so.shipmentNumber" )
    Page<OrderShipmentItem> queryFindShipmentOutgoing(@Param("idInternal") String idInternal,
                                                     @Param("idStatusType") Integer idStatusType, Pageable page);

    @Query("select r from OrderShipmentItem r " +
        "   join ShipmentItem si on (r.shipmentItem = si)" +
        "   join ShipmentOutgoing so on (si.shipment = so)" +
        "   join VehicleSalesOrder v on (v = r.orderItem.orders)" +
        "   join SalesUnitRequirement sur on (v.idSalesUnitRequirement = sur.idRequirement)" +
        "   join Salesman sl on (sl = sur.salesman)" +
        "   join Salesman ks on (ks = sl.coordinatorSales)" +
        "   join Person p on (p = sl.party)" +
        "   join Person pp on (pp = ks.party)" +
        "   join ItemIssuance isu on (si = isu.shipmentItem)" +
        "   join InventoryItem ii on (ii = isu.inventoryItem)" +
        "   join so.statuses s " +
        "   where (current_timestamp between s.dateFrom and s.dateThru)" +
        "   and (s.idStatusType in (13,24,22,23,21))" +
        "   and (v.internal.idInternal = :idInternal)" +
        "   and ((so.shipmentNumber like :data) or (so.deliveryAddress like :data)" +
        "   or (ii.idFrame like :data) or (p.firstName like :data) or (p.lastName like :data)" +
        "   or (pp.firstName like :data) or (pp.lastName like :data))" +
        "   order by so.shipmentNumber")
    Page<OrderShipmentItem> queryFindShipmentOutgoingSearch(@Param("data") String data,
                                                            @Param("idInternal") String idInternal, Pageable page);

    @Query("select r from OrderShipmentItem r " +
        "   join ShipmentItem si on (r.shipmentItem = si)" +
        "   join ShipmentOutgoing so on (si.shipment = so)" +
        "   join VehicleSalesOrder v on (v = r.orderItem.orders)" +
        "   join so.statuses s " +
        "   join Driver d on so.idDriver = d.idPartyRole" +
        "   where (current_timestamp between s.dateFrom and s.dateThru)" +
        "   and (s.idStatusType = :idStatusType)" +
        "   and (v.internal.idInternal = :idInternal)" +
        "   and (d.username = :username)" +
        "   order by so.shipmentNumber" )
    Page<OrderShipmentItem> queryFindShipmentOutgoingDriver(@Param("idInternal") String idInternal,
                                                            @Param("idStatusType") Integer idStatusType,
                                                            @Param("username") String username, Pageable page);

    @Query("select r from OrderShipmentItem r " +
        "   join ShipmentItem si on (r.shipmentItem = si)" +
        "   join ShipmentOutgoing so on (si.shipment = so)" +
        "   join VehicleSalesOrder v on (v = r.orderItem.orders)" +
        "   join so.statuses s " +
        "   join Driver d on so.idDriver = d.idPartyRole" +
        "   where (current_timestamp between s.dateFrom and s.dateThru)" +
        "   and (s.idStatusType = :idStatusType)" +
        "   and (v.internal.idInternal = :idInternal)" +
        "   and (so.dateSchedulle between :dateAwal and :dateAkhir)" +
        "   and (d.username = :username)" +
        "   order by so.shipmentNumber" )
    Page<OrderShipmentItem> queryShipmentOutgoingByDateSchedule(@Param("idInternal") String idInternal,
                                                                @Param("idStatusType") Integer idStatusType,
                                                                @Param("dateAwal") ZonedDateTime dateAwal,
                                                                @Param("dateAkhir") ZonedDateTime dateAkhir,
                                                                @Param("username") String username, Pageable page);

    @Query("select r from Billing r" +
        "   join BillingItem bi on (r.idBilling = bi.billing.idBilling)" +
        "   join OrderBillingItem obi on (bi.idBillingItem = obi.billingItem.idBillingItem)" +
        "   join OrderItem oi on (obi.orderItem = oi)" +
        "   where oi.idOrderItem = :idordite")
    Page<Billing> queryFindBilling(@Param("idordite") UUID idordite, Pageable page);
}

