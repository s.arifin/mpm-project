package id.atiila.repository;

import id.atiila.domain.ShipmentReceipt;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ShipmentReceipt entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ShipmentReceiptRepository extends JpaRepository<ShipmentReceipt, UUID> {

    @Query("select r from ShipmentReceipt r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<ShipmentReceipt> findActiveShipmentReceipt(Pageable pageable);

    @Query("select r from ShipmentReceipt r "
	    + " where ((r.shipmentPackage.idPackage = :idShipmentPackage) or (:idShipmentPackage is null)) "
	    + " and ((r.shipmentItem.idShipmentItem = :idShipmentItem) or (:idShipmentItem is null)) "
	    + " and ((r.orderItem.idOrderItem = :idOrderItem) or (:idOrderItem is null)) "
)
    Page<ShipmentReceipt> findByParams(@Param("idShipmentPackage") String idShipmentPackage, @Param("idShipmentItem") String idShipmentItem, @Param("idOrderItem") String idOrderItem, Pageable pageable);

    @Query("select sum(r.qtyAccept) from ShipmentReceipt r where r.orderItem.idOrderItem = :idOrderItem")
    Double qtyAcceptByOrderItem(@Param("idOrderItem") UUID idOrderItem);

    @Query("select sum(r.qtyReject) from ShipmentReceipt r where r.orderItem.idOrderItem = :idOrderItem")
    Double qtyRejectByOrderItem(@Param("idOrderItem") UUID idOrderItem);

    @Query("select sum(r.qtyAccept - r.qtyReject) from ShipmentReceipt r " +
        "    where (r.orderItem.idOrderItem = :idOrderItem) ")
    Double sumFilledOrder(@Param("idOrderItem") UUID idOrderItem);
}

