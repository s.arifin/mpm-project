package id.atiila.repository;

import id.atiila.domain.WorkServiceRequirement;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the WorkServiceRequirement entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface WorkServiceRequirementRepository extends JpaRepository<WorkServiceRequirement, UUID> {
}

