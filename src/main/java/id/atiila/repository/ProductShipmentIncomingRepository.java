package id.atiila.repository;

import id.atiila.domain.Internal;
import id.atiila.domain.Party;
import id.atiila.domain.ProductShipmentIncoming;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ProductShipmentIncoming entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ProductShipmentIncomingRepository extends JpaRepository<ProductShipmentIncoming, UUID> {

    @Query("select r from ProductShipmentIncoming r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (r.shipTo.party = :party)" +
        "      and (s.idStatusType not in (13, 17))")
    Page<ProductShipmentIncoming> findActiveProductShipmentIncoming(@Param("party") Party party, Pageable pageable);

    @Query("select r from ProductShipmentIncoming r "
	    + " where ((r.shipmentType.idShipmentType = :idShipmentType) or (:idShipmentType is null)) "
	    + " and ((r.shipFrom.idShipTo = :idShipFrom) or (:idShipFrom is null)) "
	    + " and ((r.shipTo.idShipTo = :idShipTo) or (:idShipTo is null)) "
	    + " and ((r.addressFrom.idContact = :idAddressFrom) or (:idAddressFrom is null)) "
	    + " and ((r.addressTo.idContact = :idAddressTo) or (:idAddressTo is null)) ")
    Page<ProductShipmentIncoming> findByParams(@Param("idShipmentType") String idShipmentType, @Param("idShipFrom") String idShipFrom, @Param("idShipTo") String idShipTo, @Param("idAddressFrom") String idAddressFrom, @Param("idAddressTo") String idAddressTo, Pageable pageable);

}

