package id.atiila.repository;

import id.atiila.domain.SaleType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SaleType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface SaleTypeRepository extends JpaRepository<SaleType, Integer> {
}

