package id.atiila.repository;

import id.atiila.domain.OrderItem;
import id.atiila.domain.OrderShipmentItem;

import java.util.List;
import java.util.UUID;

import id.atiila.domain.ShipmentItem;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the OrderShipmentItem entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface OrderShipmentItemRepository extends JpaRepository<OrderShipmentItem, UUID> {

    @Query("select r from OrderShipmentItem r "
	    + " where ((r.orderItem.idOrderItem = :idOrderItem) or (:idOrderItem is null)) "
	    + " and ((r.shipmentItem.idShipmentItem = :idShipmentItem) or (:idShipmentItem is null)) ")
    Page<OrderShipmentItem> findByParams(@Param("idOrderItem") String idOrderItem, @Param("idShipmentItem") String idShipmentItem, Pageable pageable);

    @Query("select sum(r.qty) from OrderShipmentItem r where r.orderItem.idOrderItem = :idOrderItem")
    Double qtyFilledByOrderItem(@Param("idOrderItem") UUID idOrderItem);

    @Query("select r from OrderShipmentItem r where r.orderItem = :orderItem")
    List<OrderShipmentItem> queryByOrderItem(@Param("orderItem") OrderItem orderItem);

    @Query("select r from OrderShipmentItem r where (r.orderItem = :orderItem) and (r.shipmentItem = :shipmentItem) ")
    OrderShipmentItem getShipmentOrderItem(@Param("orderItem") OrderItem orderItem, @Param("shipmentItem") ShipmentItem shipmentItem);

    @Query("select r from OrderShipmentItem r where (r.orderItem = :orderItem) and (r.qty > 0)")
    List<OrderShipmentItem> getShipmentItems(@Param("orderItem") OrderItem orderItem);
}

