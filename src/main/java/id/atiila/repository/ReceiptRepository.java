package id.atiila.repository;

import id.atiila.domain.Internal;
import id.atiila.domain.Receipt;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Receipt entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ReceiptRepository extends JpaRepository<Receipt, UUID> {

    @Query("select r from Receipt r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (r.internal = :internal) " +
        "      and (s.idStatusType not in (13, 17))")
    Page<Receipt> findActiveReceipt(@Param("internal") Internal intr, Pageable pageable);

    Page<Receipt> findAllByIdRequirement(UUID idrequirement, Pageable pageable);

    @Query("select r from Receipt r where (r.paymentType.idPaymentType = :idPaymentType)")
    Page<Receipt> findByIdPaymentType(@Param("idPaymentType") Integer idPaymentType, Pageable pageable);

    @Query("select r from Receipt r where (r.method.idPaymentMethod = :idMethod)")
    Page<Receipt> findByIdMethod(@Param("idMethod") Integer idMethod, Pageable pageable);

    @Query("select r from Receipt r where (r.internal.idInternal = :idInternal)")
    Page<Receipt> findByIdInternal(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from Receipt r where (r.paidTo.idBillTo = :idPaidTo)")
    Page<Receipt> findByIdPaidTo(@Param("idPaidTo") String idPaidTo, Pageable pageable);

    @Query("select r from Receipt r where (r.paidFrom.idBillTo = :idPaidFrom)")
    Page<Receipt> findByIdPaidFrom(@Param("idPaidFrom") String idPaidFrom, Pageable pageable);

    @Query("select r from Receipt r where (r.paidFrom.idBillTo = :idPaidFrom)")
    List<Receipt> findByIdPaidFromId(@Param("idPaidFrom") String idPaidFrom);

    @Query("select r from Receipt r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13)) " +
        "      and (r.idRequirement = :idRequirement) ")
    List<Receipt> queryByIdRequirement(@Param("idRequirement") UUID idRequirement);

    @Query("select r from Receipt r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (18, 13)) " +
        "      and (r.idRequirement = :idRequirement) ")
    Page<Receipt> queryReceiptByIdRequirement(@Param("idRequirement") UUID idRequirement, Pageable pageable);
}

