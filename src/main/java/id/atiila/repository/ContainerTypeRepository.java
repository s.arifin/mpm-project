package id.atiila.repository;

import id.atiila.domain.ContainerType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ContainerType entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ContainerTypeRepository extends JpaRepository<ContainerType, Integer> {
    ContainerType findTop1ByOrderByIdContainerTypeDesc();
}

