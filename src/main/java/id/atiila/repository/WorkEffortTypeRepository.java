package id.atiila.repository;

import id.atiila.domain.WorkEffortType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the WorkEffortType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface WorkEffortTypeRepository extends JpaRepository<WorkEffortType, Integer> {


}

