package id.atiila.repository;

import id.atiila.domain.PackageReceipt;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import id.atiila.service.pto.ShipmentPTO;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the PackageReceipt entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface PackageReceiptRepository extends JpaRepository<PackageReceipt, UUID> {

    @Query("select r from PackageReceipt r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<PackageReceipt> findActivePackageReceipt(Pageable pageable);

    @Query(" select r from PackageReceipt r join r.statuses s"
	    +  " where ((r.internal.idInternal = :idInternal) or (:idInternal is null))"
	    +  " and ((r.shipFrom.idShipTo = :idShipFrom) or (:idShipFrom is null))"
        +  " and (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in :statuses)"
        +  " and (r.shipType is null)")
    Page<PackageReceipt> findByParams(@Param("idInternal") String idInternal,
                                      @Param("idShipFrom") String idShipFrom,
                                      @Param("statuses") Set<Integer> statuses,
                                      Pageable pageable);

    @Query(" select r from PackageReceipt r join r.statuses s"
        +  " where ((r.internal.idInternal = :idInternal) or (:idInternal is null))"
        +  " and ((r.shipFrom.idShipTo = :idShipFrom) or (:idShipFrom is null))"
        +  " and (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in :statuses)"
        +  " order by r.dateCreated desc")
    Page<PackageReceipt> findByParamsLov(@Param("idInternal") String idInternal,
                                      @Param("idShipFrom") String idShipFrom,
                                      @Param("statuses") Set<Integer> statuses,
                                      Pageable pageable);

    @Query(" select r from PackageReceipt r join r.statuses s" +
           " where (current_timestamp between s.dateFrom and s.dateThru)" +
           " and (r.dateCreated between :#{#param.packageReceiptDateFrom} and :#{#param.packageReceiptDateThru}) " +
           " and (s.idStatusType in :#{#param.status})" +
           " and (s.idStatusType not in :#{#param.statusNotIn})")
    Page<PackageReceipt> queryByShipmentPTO(Pageable pageable, @Param("param") ShipmentPTO params);

//    PackageReceipt findOneByDocumentNumber(String documentNumber);
    @Query ("select r from PackageReceipt r " +
    "   where r.documentNumber = :documentNumber")
    List<PackageReceipt> findOneByDocumentNumber(@Param("documentNumber") String documentNumber);


    @Query(" select r from PackageReceipt r join r.statuses s " +
        "     where (r.internal.idInternal = :idInternal) " +
        "       and (current_timestamp between s.dateFrom and s.dateThru) " +
        "       and (s.idStatusType not in (13, 17))" +
        "       and (r.shipType is null)")
    Page<PackageReceipt> queryByInternalByStatus(@Param("idInternal") String idInternal, Pageable pageable);
}

