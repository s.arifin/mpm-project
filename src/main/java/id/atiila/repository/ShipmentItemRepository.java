package id.atiila.repository;

import id.atiila.domain.PickingSlip;
import id.atiila.domain.ShipmentItem;

import java.util.List;
import java.util.UUID;

import id.atiila.service.dto.CustomShipmentItemDTO;
import org.apache.cxf.jaxrs.ext.PATCH;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ShipmentItem entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ShipmentItemRepository extends JpaRepository<ShipmentItem, UUID> {

    @Query("select r from ShipmentItem r where (r.shipment.idShipment = :idShipment) ")
    Page<ShipmentItem> findByParams(@Param("idShipment") UUID idShipment, Pageable pageable);

    @Query("select new id.atiila.service.dto.CustomShipmentItemDTO(site, usr, p, f) " +
        " from ShipmentItem site " +
        " join UnitShipmentReceipt usr on (usr.shipmentItem.idShipmentItem = site.idShipmentItem) " +
        " join Product p on (site.idProduct = p.idProduct) " +
        " join Feature f on (site.idFeature = f.idFeature) " +
        " where (site.shipment.idShipment = :idShipment) ")
    Page<CustomShipmentItemDTO> findCustomByIdShipment(@Param("idShipment") UUID idShipment, Pageable pageable);

    @Query("select i.picking from ShipmentItem r join ItemIssuance i on (i.shipmentItem = r) " +
        " where (r.shipment.idShipment = :idShipment)")
    PickingSlip getPickingFromShipent(@Param("idShipment") UUID idShipment);

    @Query("select r from ShipmentItem  r " +
        "   join ItemIssuance iis on r = iis.shipmentItem " +
        "   join PickingSlip pis on iis.picking = pis" +
        "   where pis.idSlip = :idslip")
    List<ShipmentItem> findShipmentItemByPicking (@Param("idslip") UUID idslip);


}

