package id.atiila.repository;

import id.atiila.domain.UomType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the UomType entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface UomTypeRepository extends JpaRepository<UomType, Integer> {

	@Query("select r from UomType r where (1=2)")
    Page<UomType> queryNothing(Pageable pageable); 
}

