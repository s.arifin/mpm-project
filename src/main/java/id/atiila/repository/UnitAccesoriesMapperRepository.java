package id.atiila.repository;

import id.atiila.domain.UnitAccesoriesMapper;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the UnitAccesoriesMapper entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface UnitAccesoriesMapperRepository extends JpaRepository<UnitAccesoriesMapper, UUID> {
}

