package id.atiila.repository;

import id.atiila.domain.InternalBankMaping;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the InternalBankMaping entity.
 */

@Repository
public interface InternalBankMapingRepository<T extends InternalBankMaping> extends JpaRepository<T, String > {
    @Query("select r from InternalBankMaping r where r.idinternal =:idRoot")
    List<InternalBankMaping> FindByIdRoot(@Param("idRoot") String IdRoot);

    List<InternalBankMaping> findInternalBankMapingsByIdinternal(String idRoot);
}
