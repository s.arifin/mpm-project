package id.atiila.repository;

import id.atiila.domain.UnitPreparation;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the UnitPreparation entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface UnitPreparationRepository extends JpaRepository<UnitPreparation, UUID> {

    @Query("select r from UnitPreparation r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11, 12))")
    Page<UnitPreparation> findActiveUnitPreparation(Pageable pageable);
    
    @Query("select r from UnitPreparation r where (r.shipTo.idShipTo = :idShipTo)")
    Page<UnitPreparation> queryByIdShipTo(@Param("idShipTo") String idShipTo, Pageable pageable); 
    
    @Query("select r from UnitPreparation r where (r.internal.idInternal = :idInternal)")
    Page<UnitPreparation> queryByIdInternal(@Param("idInternal") String idInternal, Pageable pageable); 
    
    @Query("select r from UnitPreparation r where (r.facility.idFacility = :idFacility)")
    Page<UnitPreparation> queryByIdFacility(@Param("idFacility") UUID idFacility, Pageable pageable); 
    
    @Query("select r from UnitPreparation r where (r.inventoryItem.idInventoryItem = :idInventoryItem)")
    Page<UnitPreparation> queryByIdInventoryItem(@Param("idInventoryItem") UUID idInventoryItem, Pageable pageable); 
    
    @Query("select r from UnitPreparation r where (r.orderItem.idOrderItem = :idOrderItem)")
    Page<UnitPreparation> queryByIdOrderItem(@Param("idOrderItem") UUID idOrderItem, Pageable pageable); 

	@Query("select r from UnitPreparation r where (1=2)")
    Page<UnitPreparation> queryNothing(Pageable pageable); 
}

