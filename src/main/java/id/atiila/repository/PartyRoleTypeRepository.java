package id.atiila.repository;

import id.atiila.domain.Approval;
import id.atiila.domain.PartyRoleType;
import id.atiila.domain.PartyRoleTypeId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Spring Data JPA repository for the Approval entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface PartyRoleTypeRepository extends JpaRepository<PartyRoleType, PartyRoleTypeId> {

}

