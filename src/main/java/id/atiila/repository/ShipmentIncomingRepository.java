package id.atiila.repository;

import id.atiila.domain.ShipmentIncoming;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import id.atiila.service.dto.CustomShipmentIncomingDTO;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ShipmentIncoming entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ShipmentIncomingRepository extends JpaRepository<ShipmentIncoming, UUID> {

    @Query("select r from ShipmentIncoming r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11, 17))")
    Page<ShipmentIncoming> findActiveShipmentIncoming(Pageable pageable);

    @Query(" select new id.atiila.service.dto.CustomShipmentIncomingDTO(si,pr,count(site.idShipmentItem)) " +
        " from ShipmentItem site left join ShipmentIncoming si on (site.shipment.idShipment = si.idShipment) " +
        " join UnitShipmentReceipt usr on (usr.shipmentItem.idShipmentItem = site.idShipmentItem) " +
        " left join PackageReceipt pr on (pr.idPackage = usr.shipmentPackage.idPackage) " +
        " join si.statuses pstat where (current_timestamp between pstat.dateFrom and pstat.dateThru) and (pstat.idStatusType in (10,11,17)) " +
        " and si.idShipment = :id group by si.idShipment, pr.idPackage")
    List<CustomShipmentIncomingDTO> findCustomShipmentIncomingByIdShipment(@Param("id") UUID id);

    @Query(" select r from ShipmentIncoming r join r.statuses s"
        +  " where ((r.internal.idInternal = :idInternal) or (:idInternal is null))"
        +  " and ((r.shipFrom.idShipTo = :idShipFrom) or (:idShipFrom is null))"
        +  " and (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in :statuses)"
        +  " order by r.dateSchedulle desc")
    Page<ShipmentIncoming> findByParams(@Param("idInternal") String idInternal,
                                      @Param("idShipFrom") String idShipFrom,
                                      @Param("statuses") Set<Integer> statuses,
                                      Pageable pageable);

}

