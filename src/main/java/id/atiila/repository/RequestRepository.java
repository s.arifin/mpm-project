package id.atiila.repository;

import id.atiila.domain.Request;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Request entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface RequestRepository extends JpaRepository<Request, UUID> {

    @Query("select r from Request r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<Request> findActiveRequest(Pageable pageable);

    @Query("select r from Request r "
	    + " where ((r.requestType.idRequestType = :idRequestType) or (:idRequestType is null)) "
)
    Page<Request> findByParams(@Param("idRequestType") String idRequestType, Pageable pageable);

}

