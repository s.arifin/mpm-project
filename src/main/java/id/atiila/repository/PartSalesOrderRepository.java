package id.atiila.repository;

import id.atiila.domain.PartSalesOrder;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the PartSalesOrder entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface PartSalesOrderRepository extends JpaRepository<PartSalesOrder, UUID> {

    @Query("select r from PartSalesOrder r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<PartSalesOrder> findActivePartSalesOrder(Pageable pageable);

    @Query("select r from PartSalesOrder r where (r.internal.idInternal = :idInternal) and (r.customer.idCustomer = :idCustomer) "
	    + " and (r.billTo.idBillTo = :idBillTo) ")
    Page<PartSalesOrder> findByParams(@Param("idInternal") String idInternal, @Param("idCustomer") String idCustomer, @Param("idBillTo") String idBillTo, Pageable pageable);

}

