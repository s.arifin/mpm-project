package id.atiila.repository;

import id.atiila.domain.ProspectOrganization;
import id.atiila.domain.ProspectOrganizationDetails;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ProspectOrganizationDetails entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ProspectOrganizationDetailsRepository extends JpaRepository<ProspectOrganizationDetails, UUID> {

    @Query("select r from ProspectOrganizationDetails r where r.prospectOrganization.idProspect = :idProspects")
    Page<ProspectOrganizationDetails> findAllbyProspect(@Param("idProspects") UUID idProspects, Pageable pageable);

//    @Query("select d from  ProspectOrganizationDetails d join ProspectOrganization r on (d.idProspect = r.idProspect) " +
//        "join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (r.idProspect = :idProspect)")
//    Page<ProspectOrganizationDetails> findAllbyidProspect(@Param("idProspect") UUID idProspect, Pageable pageable);
}

