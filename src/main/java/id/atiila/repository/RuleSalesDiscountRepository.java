package id.atiila.repository;

import id.atiila.domain.RuleSalesDiscount;
import org.dom4j.rule.Rule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.math.BigDecimal;
import java.util.List;


/**
 * Spring Data JPA repository for the RuleSalesDiscount entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface RuleSalesDiscountRepository extends JpaRepository<RuleSalesDiscount, Integer> {
    @Query("select distinct r from RuleSalesDiscount r where (current_timestamp between r.dateFrom and r.dateThru)" +
        "and (r.role=:role) " +
        "and (r.idInternal=:idinternal)")
    List<RuleSalesDiscount> findActiveByRoleAndIdInternal(
        @Param("role") String role,
        @Param("idinternal") String idinternal
    );

    Page<RuleSalesDiscount> findAllByIdInternal(String idinternal, Pageable pageable);
}

