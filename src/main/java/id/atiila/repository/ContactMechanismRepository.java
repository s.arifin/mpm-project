package id.atiila.repository;

import id.atiila.domain.ContactMechanism;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ContactMechanism entity.
 * BeSmart Team
 */@SuppressWarnings("unused")
@Repository
public interface ContactMechanismRepository extends JpaRepository<ContactMechanism, UUID> {
}

