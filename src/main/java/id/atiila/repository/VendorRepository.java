package id.atiila.repository;

import id.atiila.domain.Organization;
import id.atiila.domain.Party;
import id.atiila.domain.Vendor;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

/**
 * Spring Data JPA repository for the Vendor entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface VendorRepository extends JpaRepository<Vendor, String> {

    @Query("select r from Vendor r where (r.organization.idParty = :idOrganization)")
    Page<Vendor> queryByIdOrganization(@Param("idOrganization") UUID idOrganization, Pageable pageable);

	@Query("select r from Vendor r where (1=2)")
    Page<Vendor> queryNothing(Pageable pageable);

    @Query("select r from Organization r where (r.name = :name) ")
    Organization findOneByName(@Param("name") String name);

    @Query("select r from Vendor r where (r.organization = :party) ")
    Vendor getVendorByParty(@Param("party") Party party);

    @Query("select r from Vendor r where (r.idVendor = :idVendor) ")
    Vendor getVendorByidVendor(@Param("idVendor") String idVendor);

    Vendor findByOrganization(Organization party);

    Page<Vendor> findByIdRoleType(Integer idRoleType, Pageable pageable);

    @Query("select r from Vendor r " +
        "   join VendorRelationship vr on r.idVendor = vr.vendor.idVendor" +
        "   where r.isBiroJasa = true and vr.internal.idInternal = :idInternal ")
    Page<Vendor> queryFindAllBiroJasa(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from Vendor r " +
        "   join VendorRelationship vr on r.idVendor = vr.vendor.idVendor" +
        "   where r.isMainDealer = true and vr.internal.idInternal = :idInternal ")
    Page<Vendor> queryFindAllMainDealer(@Param("idInternal") String idInternal, Pageable pageable);
}
