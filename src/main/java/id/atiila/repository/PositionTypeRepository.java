package id.atiila.repository;

import id.atiila.domain.PositionType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PositionType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface PositionTypeRepository extends JpaRepository<PositionType, Integer> {
}

