package id.atiila.repository;

import id.atiila.domain.Container;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Container entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ContainerRepository extends JpaRepository<Container, UUID> {

    @Query("select r from Container r where (r.facility.idFacility = :idFacility)")
    Page<Container> findByIdFacility(@Param("idFacility") UUID idFacility, Pageable pageable);

    @Query("select r from Container r where (r.containerType.idContainerType = :idContainerType)")
    Page<Container> findByIdContainerType(@Param("idContainerType") Integer idContainerType, Pageable pageable);

    @Query("select r from Container r where (1=2)")
    Page<Container> findNothing(Pageable pageable);

    Container findOneByContainerCode(String id);

    List<Container> findAllByFacilityIdFacility(UUID id);

}

