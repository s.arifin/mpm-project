package id.atiila.repository;

import id.atiila.domain.Internal;
import id.atiila.domain.Position;

import java.util.List;
import java.util.UUID;

import id.atiila.domain.PositionType;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Position entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface PositionRepository extends JpaRepository<Position, UUID> {

    @Query("select r from Position r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<Position> findActivePosition(Pageable pageable);

    Position findPositionByInternalAndPositionType_IdPositionTypeAndSequenceNumber(Internal internal, Integer idPosition, Integer number);

    @Query("select r from Position r" +
        "   order by r.sequenceNumber desc")
    List<Position> listAllPosition();

}

