package id.atiila.repository;

import id.atiila.domain.VehicleWorkRequirement;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the VehicleWorkRequirement entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface VehicleWorkRequirementRepository extends JpaRepository<VehicleWorkRequirement, UUID> {
}

