package id.atiila.repository;

import id.atiila.domain.Party;
import id.atiila.domain.PriceComponent;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import id.atiila.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PriceComponent entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface PriceComponentRepository extends JpaRepository<PriceComponent, UUID> {

    PriceComponent findByProduct(Product product);

    @Query("SELECT r FROM PriceComponent r WHERE (r.product.idProduct = :idproduct) " +
        "AND (r.priceType.idPriceType = :idpricetype) " +
        "AND (r.seller.idParty = :idparty) " +
        "AND (CURRENT_TIMESTAMP BETWEEN r.dateFrom AND r.dateThru)")
    PriceComponent getActivePriceByPriceType(
        @Param("idproduct") String idproduct,
        @Param("idpricetype") Integer idpricetype,
        @Param("idparty") UUID idparty
    );

    @Query("select r from PriceComponent r where (r.product = :product) and (r.seller = :seller) " +
        " and (:datewhen between r.dateFrom and r.dateThru)")
    List<PriceComponent> getPriceBase(
        @Param("product") Product product,
        @Param("seller") Party seller,
        @Param("datewhen") ZonedDateTime dateWhen
    );

    @Query("select r from PriceComponent r join r.seller s " +
        " where (r.product.idProduct = :idproduct)" +
        " and (s.idParty = :idinternal)" +
        " and (current_timestamp between r.dateFrom and r.dateThru)" +
        "")
    List<PriceComponent> getPriceBaseByProductAndInternal(
      @Param("idproduct") String idproduct,
      @Param("idinternal") UUID idinternal
    );

    @Query("select r from PriceComponent r join r.seller s " +
           " where (r.product.idProduct = :idproduct)" +
           " and (s.idParty = :idinternal)" +
           " and (current_timestamp between r.dateFrom and r.dateThru) ")
    List<PriceComponent> getPriceBaseByProductAndInternal(@Param("idinternal") UUID idinternal, @Param("idproduct") String idproduct);

    @Query("select r from PriceComponent r left join Internal s on (r.seller = s.organization)" +
        " where (r.product.idProduct = :idProduct) " +
        " and (s.idInternal = :idInternal)" +
        " and (current_timestamp between r.dateFrom and r.dateThru) ")
    List<PriceComponent> getAllPriceBaseByProductAndInternal(@Param("idInternal") String idinternal, @Param("idProduct") String idproduct);

    @Query("select r from PriceComponent r join r.seller s where (r.product.idProduct = :idproduct) " +
        "and(s.idParty = :idvendor) " +
        "and (:datewhen between r.dateFrom and r.dateThru)")
    List<PriceComponent> getPriceBaseByProductAndVendor(
        @Param("idproduct")String idproduct,
        @Param("idvendor")UUID idvendor,
        @Param("datewhen") ZonedDateTime datewhen
    );

    @Query("select r from PriceComponent r join r.seller s " +
        "where (r.product.idProduct = :idproduct) " +
        "and (s.idParty = :idparty)")
    List<PriceComponent> getAllPriceBaseByProductAndInternal(
        @Param("idproduct") String idproduct,
        @Param("idparty") UUID idparty
    );

    @Query("select r from PriceComponent r join r.seller s where (r.product.idProduct = :idproduct) " +
        " and(s.idParty = :idvendor)")
    List<PriceComponent> getAllPriceBaseByProductAndVendor(
        @Param("idproduct")String idproduct,
        @Param("idvendor")UUID idvendor
    );

    @Query("select r from PriceComponent r where (r.product.idProduct = :idproduct) and ((:dtfrom between r.dateFrom and r.dateThru) or (:dtthru between r.dateFrom and r.dateThru))")
    List<PriceComponent> getDataByProductAndDate(
        @Param("idproduct") String idproduct,
        @Param("dtfrom")ZonedDateTime dtfrom,
        @Param("dtthru")ZonedDateTime dtthru
    );

    @Query("select r from PriceComponent r " +
        "    where (r.product.idProduct = :idproduct) " +
        "      and (r.seller = :seller) " +
        "      and (:dateWhen between r.dateFrom and r.dateThru)")
    List<PriceComponent> getPriceComponent(
        @Param("seller") Party seller,
        @Param("idproduct") String idproduct,
        @Param("dateWhen") ZonedDateTime dtWhen
    );

    @Query("select r from PriceComponent r join Internal s on (r.seller = s.organization)" +
        "   join VendorRelationship vr on vr.internal = s " +
        "   join Vendor v on v = vr.vendor " +
        "   where (r.product.idProduct = :idProduct and v.idVendor = :idVendor)" +
        "   and (current_timestamp between r.dateFrom and r.dateThru)")
    Page<PriceComponent> getSTNKB(@Param("idProduct") String idProduct , @Param("idVendor") String idVendor, Pageable pageable);


}

