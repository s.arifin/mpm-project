package id.atiila.repository;

import id.atiila.domain.Good;
import id.atiila.domain.InventoryItem;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import id.atiila.domain.Party;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the InventoryItem entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface InventoryItemRepository extends JpaRepository<InventoryItem, UUID> {

    @Query("select r from InventoryItem r join r.statuses s " +
        "   where (current_timestamp between s.dateFrom and s.dateThru) " +
        "     and (s.idStatusType not in ( 13, 17)) " +
        "     and (r.qty - r.qtyBooking > 0)")
    Page<InventoryItem> findActiveInventoryItem(Pageable pageable);

    @Query("select sum(i.qty) from InventoryItem i Where i.idProduct = :idProduct and i.idOwner = :idparty and i.idFeature = :idFeature ")
    Integer findSumQty(@Param("idProduct") String idProduct, @Param("idparty") UUID idparty, @Param("idFeature")Integer idFeature);

    @Query(" select i from InventoryItem i , Internal r  " +
        " where i.idProduct = :idProduct and i.idOwner = r.organization.idParty  and i.idFeature = :idFeature " +
        " and i.idFrame + i.idMachine not in ( select s.idframe+ s.idmachine from SalesUnitRequirement s where s.idframe like :reqNoKa and s.idmachine like :reqNoSin) " +
        " and  r.idInternal = :idInternal " +
        " order By i.dateCreated ")
    Page<InventoryItem> getByIdProductAndIdOwnerAndIdFeatureOrderByDateCreated(
        @Param("idProduct") String idProduct,
        @Param("idInternal") String idInternal,
        @Param("idFeature")Integer idFeature,
        @Param("reqNoKa") String reqNoKa,
        @Param("reqNoSin") String reqNoSin,
        Pageable pageable
    );

    List<InventoryItem> findByIdProductAndIdOwnerAndIdFeatureOrderByDateCreated(
        @Param("idProduct") String idProduct,
        @Param("idOwner") UUID idParty,
        @Param("idFeature")Integer idFeature);


    // ///////untuk list manual matching => ada isi reg noka dan nosin
//    @Query(" select i from InventoryItem i join i.statuses s, Internal r  " +
//        " where i.idProduct = :idProduct and i.idOwner = r.organization.idParty  and (:idFeature in (i.idFeature, 0)) " +
//        " and i.idFrame like :reqNoka  and i.idMachine like :reqNosin  " +
//        " and  r.idInternal = :idInternal " +
//        " and i.idFrame + i.idMachine not in ( select s.idframe+ s.idmachine from SalesUnitRequirement s where s.idframe like :reqNoka and s.idmachine like :reqNosin) " +
//        " and (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11)) " +
//        " " +
//        " order By i.dateCreated ")

//    @Query(" select i from InventoryItem i join i.statuses s  , Internal r  " +
//        " where i.idProduct = :idProduct and i.idOwner = r.organization.idParty " +
//        " and  r.idInternal = :idInternal" +
//        " and  ( (i.qty - i.qtyBooking) >0 ) " +
//        " and i.idFrame like :reqNoka  and i.idMachine like :reqNosin " +
//        " and (:idFeature in (i.idFeature, 0)) " +
//        " and (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11)) " +
//        " order By i.dateCreated ")
//    todo remark karena ga pakai gudang dulu
//    @Query(value = " select r.* from Inventory_Item r " +
//        "    inner join internal i on r.idOwner = i.idparty " +
//        " where (i.idInternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
//        "    and r.idProduct = ?1  " +
//        "    and r.idFeature = ?3  " +
//        "    and r.idFrame like ?4  " +
//        "    and r.idMachine like ?5 " +
//        "    and r.yearAssembly = ?6" +
//        "    and r.idfacility = ?7" +
//        "    and not exists " +
//        "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
//        "            and (z.requestidframe = r.idframe) " +
//        "            and (z.requestidmachine = r.idMachine)" +
//        "            and (z.facility = r.idfacility))" +
//        "     \n-- #pageable\n  ",
//    countQuery = " " +
//        "select count(*) from Inventory_Item r " +
//        "    inner join internal i on r.idOwner = i.idparty " +
//        " where (i.idInternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
//        "    and r.idProduct = ?1  " +
//        "    and r.idFeature = ?3  " +
//        "    and r.idFrame like ?4  " +
//        "    and r.idMachine like ?5 " +
//        "    and r.yearAssembly = ?6" +
//        "    and not exists " +
//        "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
//        "            and (z.requestidframe = r.idframe) " +
//        "            and (z.requestidmachine = r.idMachine)" +
//        "            and (z.facility = r.idfacility))" +
//        "  "  ,
//    nativeQuery = true)
//    Page<InventoryItem> getInventoryFiltered(
//        String idProduct,
//        String idInternal,
//        int idFeature,
//        String reqNoka,
//        String reqNosin,
//        int regYear,
//        UUID regGudang,
//        Pageable pageable
//    );
@Query(value = " select r.* from Inventory_Item r " +
    "    inner join internal i on r.idOwner = i.idparty " +
    " where (i.idInternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
    "    and r.idProduct = ?1  " +
    "    and r.idFeature = ?3  " +
    "    and r.idFrame like ?4  " +
    "    and r.idMachine like ?5 " +
    "    and r.yearAssembly = ?6" +
    "    and not exists " +
    "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
    "            and (z.requestidframe = r.idframe) " +
    "            and (z.requestidmachine = r.idMachine))" +
    "     \n-- #pageable\n  ",
    countQuery = " " +
        "select count(*) from Inventory_Item r " +
        "    inner join internal i on r.idOwner = i.idparty " +
        " where (i.idInternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
        "    and r.idProduct = ?1  " +
        "    and r.idFeature = ?3  " +
        "    and r.idFrame like ?4  " +
        "    and r.idMachine like ?5 " +
        "    and r.yearAssembly = ?6" +
        "    and not exists " +
        "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
        "            and (z.requestidframe = r.idframe) " +
        "            and (z.requestidmachine = r.idMachine))" +
        "  "  ,
    nativeQuery = true)
Page<InventoryItem> getInventoryFiltered(
    String idProduct,
    String idInternal,
    int idFeature,
    String reqNoka,
    String reqNosin,
    int regYear,
    Pageable pageable
);

    // untuk list manual matching => TANPA isi reg noka dan nosin
//    @Query(" select i from InventoryItem i join i.statuses s  , Internal r  " +
//        " where i.idProduct = :idProduct and i.idOwner = r.organization.idParty " +
//        " and  r.idInternal = :idInternal" +
//        " and  ( (i.qty - i.qtyBooking) >0 ) " +
//        " and not exists (select null from SalesUnitRequirement z where (z.product.idProduct = i.idProduct ) and (z.idframe = i.idFrame) and (z.idmachine = i.idMachine)) " +
//        " and (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11)) " +
//        " order By i.dateCreated ")
//    Page<InventoryItem> getByIdOwnerAndIdFeatureOrderByDateCreated(
//        @Param("idProduct") String idProduct,
//        @Param("idInternal") String idInternal,
//        Pageable pageable
//    );

    @Query(value = " select r.* from Inventory_Item r " +
                   "    inner join internal i on r.idOwner = i.idparty " +
                   " where (i.idInternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
                   "    and r.idProduct = ?1  " +
                   "    and not exists " +
                   "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
                   "            and (z.idframe = r.idframe) " +
                   "            and (z.idMachine = r.idMachine))" +
                   "     \n-- #pageable\n  ",
        countQuery = " " +
                    "select count(*) from Inventory_Item r " +
                    "    inner join internal i on r.idOwner = i.idparty " +
                    " where (i.idInternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
                    "    and r.idProduct = ?1  " +
                    "    and not exists " +
                    "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
                    "            and (z.idframe = r.idframe) " +
                    "            and (z.idMachine = r.idMachine))" +
                    "  "  ,
           nativeQuery = true)
    Page<InventoryItem> getByIdOwnerAndIdFeatureOrderByDateCreatedNative(
        String idProduct,
        String idInternal,
        Pageable pageable
    );
// todo remark karena ga pakai gudang dulu
//    @Query(value = " select r.* from Inventory_Item r " +
//        "    inner join internal i on r.idOwner = i.idparty " +
//        " where (i.idInternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
//        "    and r.idProduct = ?1  " +
//        "    and r.idFeature = ?3  " +
//        "    and r.yearassembly = ?4 " +
//        "    and r.idfacility = ?5 " +
//        "    and not exists " +
//        "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
//        "            and (z.requestidframe = r.idframe) " +
//        "            and (z.requestidmachine = r.idMachine)" +
//        "            and (z.facility = r.idfacility))" +
//        "     \n-- #pageable\n  ",
//        countQuery = " " +
//            "select count(*) from Inventory_Item r " +
//            "    inner join internal i on r.idOwner = i.idparty " +
//            " where (i.idInternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
//            "    and r.idProduct = ?1  " +
//            "    and r.idFeature = ?3  " +
//            "    and r.yearassembly = ?4 " +
//            "    and r.idfacility = ?5 " +
//            "    and not exists " +
//            "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
//            "            and (z.requestidframe = r.idframe) " +
//            "            and (z.requestidmachine = r.idMachine)" +
//            "            and (z.facility = r.idfacility))" +
//            "  "  ,
//        nativeQuery = true)
//    Page<InventoryItem> getByIdOwnerAndWithIdFeatureOrderByDateCreatedNative(
//        String idProduct,
//        String idInternal,
//        Integer idFeature,
//        Integer regYear,
//        UUID regGudang,
//        Pageable pageable
//    );


    @Query(value = " select r.* from Inventory_Item r " +
        "    inner join internal i on r.idOwner = i.idparty " +
        " where (i.idInternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
        "    and r.idProduct = ?1  " +
        "    and r.idFeature = ?3  " +
        "    and r.yearassembly = ?4 " +
        "    and not exists " +
        "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
        "            and (z.requestidframe = r.idframe) " +
        "            and (z.requestidmachine = r.idMachine))" +
        "     \n-- #pageable\n  ",
        countQuery = " " +
            "select count(*) from Inventory_Item r " +
            "    inner join internal i on r.idOwner = i.idparty " +
            " where (i.idInternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
            "    and r.idProduct = ?1  " +
            "    and r.idFeature = ?3  " +
            "    and r.yearassembly = ?4 " +
            "    and not exists " +
            "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
            "            and (z.requestidframe = r.idframe) " +
            "            and (z.requestidmachine = r.idMachine))" +
            "  "  ,
        nativeQuery = true)
    Page<InventoryItem> getByIdOwnerAndWithIdFeatureOrderByDateCreatedNative(
        String idProduct,
        String idInternal,
        Integer idFeature,
        Integer regYear,
        Pageable pageable
    );



    //
    //#pageable
    //

// todo di remark karene ga pakai gudang dulu
//    @Query(value = " select r.* from inventory_item r " +
//        "    inner join internal i on r.idowner = i.idparty " +
//        " where (i.idinternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
//        "    and r.idproduct = ?1  " +
//        "    and r.idfeature = ?3 " +
//        "    and r.yearassembly = ?4" +
//        "    and r.idFacility = ?5" +
//        "    and not exists " +
//        "      (select null from sales_unit_requirement z where (z.idproduct = r.idproduct ) " +
//        "            and (z.requestidmachine = r.idmachine) " +
//        "            and (z.requestidframe = r.idframe)" +
//        "            and (z.idcolor = r.idfeature)  " +
//        "            and (z.prodyear = r.yearassembly))" +
//        "            and (z.facility = r.idfacility))" +
//        "            order by r.dtcreated " +
//        "       ",
//        nativeQuery = true)
//    List<InventoryItem> getByIdOwnerAndIdFeatureNative(
//        String idProduct,
//        String idInternal,
//        Integer idFeature,
//        Integer yearAssembly,
//        UUID idFacility
//    );

    @Query(value = " select r.* from inventory_item r " +
        "    inner join internal i on r.idowner = i.idparty " +
        " where (i.idinternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
        "    and r.idproduct = ?1  " +
        "    and r.idfeature = ?3 " +
        "    and r.yearassembly = ?4" +
        "    and not exists " +
        "      (select null from sales_unit_requirement z where (z.idproduct = r.idproduct ) " +
        "            and (z.requestidmachine = r.idmachine) " +
        "            and (z.requestidframe = r.idframe)" +
        "            and (z.idcolor = r.idfeature)  " +
        "            and (z.prodyear = r.yearassembly))" +
        "            order by r.dtcreated " +
        "       ",
        nativeQuery = true)
    List<InventoryItem> getByIdOwnerAndIdFeatureNative(
        String idProduct,
        String idInternal,
        Integer idFeature,
        Integer yearAssembly
    );


    @Query(value = " SELECT r.* FROM Inventory_Item r " +
        "    INNER JOIN internal i ON r.idOwner = i.idparty " +
        " INNER JOIN inventory_item_status s " +
        "   ON s.idinvite = r.idinvite AND (CURRENT_TIMESTAMP BETWEEN s.dtfrom AND s.dtthru) AND (s.idstatustype IN (10, 11)) " +
        " WHERE 1=1 " +
        "   AND (i.idInternal = ?2 ) AND (r.qty - r.qtybooking > 0) " +
        "   AND r.idProduct = ?1  " +
        "   AND r.idFeature = ?3 " +
        "   AND r.yearassembly = ?4 " +
        "   AND NOT EXISTS " +
        "      (SELECT z.idReq FROM Sales_Unit_Requirement z WHERE (z.idProduct = r.idProduct ) " +
        "            AND (z.idframe = r.idframe) " +
        "            AND (z.idMachine = r.idMachine)" +
        "            AND (z.idColor = r.idFeature))" +
        "       ",nativeQuery = true)
    List<InventoryItem> getByIdOwnerAndIdFeatureAndYearAssemblyNative(
        String idProduct,
        String idInternal,
        Integer idFeature,
        Integer yearassembly
    );

    // untuk list barang yang available untuk di moving
    @Query(" select i from InventoryItem i join i.statuses s  , Internal r  " +
        " where (:idProduct is null or i.idProduct = :idProduct) " +
        " and i.idOwner = r.organization.idParty " +
        " and  (r.idInternal = :idInternal)" +
        " and  ( (i.qty - i.qtyBooking) >0 ) " +
        " and (:idContainer is null or i.idContainer = :idContainer) " +
        " and (:idFacility is null or i.idFacility = :idFacility)" +
        " and (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11)) " +
        " order By i.dateCreated ")
    Page<InventoryItem> findAvailableMovingItemByIdFacilityAndIdContainerAndIdProduct(
        @Param("idProduct") String idProduct,
        @Param("idInternal") String idInternal,
        @Param("idFacility") UUID idFacility,
        @Param("idContainer") UUID idContainer,
        Pageable pageable
    );

//    @Query(" select i from InventoryItem i join i.statuses s  " +
//        "join Internal i on i.organization.idParty = r.idOwner" +
////        " left join SalesUnitRequirement sur on (sur.idframe=i.idFrame) and (sur.idmachine=i.idMachine) and (sur.color.idFeature=i.idFeature)" +
//        " where (r.idInternal = :idInternal)" +
//        " and (i.idFeature = :idFeature)" +
//        " and (i.idProduct = :idProduct)" +
//        " and (i.yearAssembly = :yearAssembly)" +
//        " and ((i.qty - i.qtyBooking) >0) " +
////        " and ( concat( i.idFrame , i.idMachine ) <> any ( select concat(r.idframe , r.idmachine) from SalesUnitRequirement r )) " +
////        " and (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11)) " +
////        " and (sur.idRequirement is null)" +
//        "")

    @Query(" select i from InventoryItem i join i.statuses s  " +
        "join Internal r on r.organization.idParty = i.idOwner" +
        " where (r.idInternal = :idInternal)" +
        " and (i.idFeature = :idFeature)" +
        " and (i.idProduct = :idProduct)" +
        " and (i.yearAssembly = :yearAssembly)" +
        " and ((i.qty - i.qtyBooking) >0) ")
    List<InventoryItem> findAvailableStockByInternalAndProductAndFeature(
        @Param("idInternal") String idInternal,
        @Param("idProduct") String idProduct,
        @Param("idFeature") Integer idFeature,
        @Param("yearAssembly") Integer yearAssembly
    );

    @Query(" select r from InventoryItem r join r.statuses s" +
           " join Internal i on i.organization.idParty = r.idOwner" +
           " left join Container c on (c.idContainer = r.idContainer)" +
           " left join Facility f on (f.idFacility = r.idFacility)" +
           " where (i.idInternal like :idInternal) " +
           " and ((c.idContainer = :idContainer) or (:idContainer is null))" +
           " and ((f.idFacility = :idFacility) or (:idFacility is null))" +
           " and (r.qty - r.qtyBooking > 0)" +
           " and (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11)) " +
           " order By r.dateCreated ")
    Page<InventoryItem> findAllInventoryItemByIdFacilityAndIdContainer(
        Pageable pageable,
        @Param("idInternal") String idInternal,
        @Param("idFacility") UUID idFacility,
        @Param("idContainer") UUID idContainer
    );

    // hitung qty utk manual matching
    @Query(" select sum(i.qty) from InventoryItem i join i.statuses s  , Internal r  " +
        " where i.idProduct = :idProduct and i.idOwner = r.organization.idParty  and i.idFeature = :idFeature " +
        " and i.idFrame like :reqNoka  and i.idMachine like :reqNosin  " +
        " and  r.idInternal = :idInternal" +
        " and i.idFrame + i.idMachine not in ( select s.idframe+ s.idmachine from SalesUnitRequirement s " +
        "      where s.idframe like :reqNoka and s.idmachine like :reqNosin) " +
        " and (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11)) ")
    Integer findSumQty(
        @Param("idProduct") String idProduct,
        @Param("idInternal") String idInternal,
        @Param("idFeature") Integer idFeature,
        @Param("reqNoka") String reqNoka,
        @Param("reqNosin") String reqNosin);

    // utk generate picking list manual matching => ada isi regNoka dan Nosin
    @Query(" select i from InventoryItem i , Internal r  " +
        " where i.idProduct = :idProduct and i.idOwner = r.organization.idParty  and i.idFeature = :idFeature " +
        " and i.idFrame= :reqNoka  and i.idMachine = :reqNosin  " +
        " and  r.idInternal = :idInternal" +
        " order By i.dateCreated ")
    List<InventoryItem> getByIdProductAndIdOwnerAndIdFeatureAndRegNokaAndRegNosinOrderByDateCreated(
        @Param("idProduct") String idProduct,
        @Param("idInternal") String idInternal,
        @Param("idFeature")Integer idFeature,
        @Param("reqNoka") String reqNoka,
        @Param("reqNosin") String reqNosin
    );

    // not exsist in vso

    @Query(" select r from InventoryItem r " +
        "     where (r.idOwner = :idOwner) " +
        "       and (r.idProduct = :idProduct) " +
        "       and (r.idFeature = :idFeature) " +
        "       and (r.qty - r.qtyBooking > 0) ")
    List<InventoryItem> getInventoryItems(@Param("idOwner") UUID idParty,
                                         @Param("idProduct") String idProduct,
                                         @Param("idFeature") Integer idFeature);


    @Query("select r from InventoryItem r where (1=2)")
    Page<InventoryItem> queryNothing(Pageable pageable);

    // New Added by EAP

    @Query("select r from InventoryItem r " +
        "    where (r.idProduct = :idProduct )" +
        "      and ((r.idFrame = :idFrame) or (r.idMachine = :idMachine)) " +
        "      and (r.qty - r.qtyBooking > 0)")
    InventoryItem queryByFrameOrMachine(@Param("idProduct") String idProduct,
                                        @Param("idFrame") String idFrame,
                                        @Param("idMachine") String idMachine);

    @Query("select r from InventoryItem r " +
        "     join Internal i on (i.organization.idParty = r.idOwner) " +
        "    where (r.qty - r.qtyBooking > 0)" +
        "      and (r.idProduct = :idProduct)" +
        "      and (r.idFeature = :idColor)" +
        "      and (i.idInternal = :idInternal) " +
        "   order by r.dateCreated asc ")
    Page<InventoryItem> queryByOrderItemAndInternal(@Param("idInternal") String idInternal,
                                                    @Param("idProduct") String idProduct,
                                                    @Param("idColor") Integer idColor,
                                                    Pageable pageable);

    @Query(" select r from InventoryItem r join fetch r.statuses s join fetch r.shipmentReceipt sr" +
           " where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType not in (13, 17))")
    List<InventoryItem> findAllEager();

    @Query(" select r from InventoryItem r where r.qty > 0")
    List<InventoryItem> findValidInventoryItems();

    @Query(value = "  select r.* from Inventory_Item r " +
        "    inner join internal i on r.idOwner = i.idparty " +
        "    join shipment_receipt sr on r.idreceipt = sr.idreceipt " +
        "    join package_receipt pr on sr.idpackage = pr.idpackage " +
        "    join order_item oi on sr.idordite = oi.idordite"+
        " where (i.idInternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
        "    and r.idProduct = ?1  " +
        "    and r.idFeature = ?3  " +
//        "    and r.yearAssembly = ?5" + todo remark biar ga lupa
        "    and not exists " +
        "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
        "            and (z.idframe = r.idframe) " +
        "            and (z.idMachine = r.idMachine))" +
        "       and oi.unitprice = ?4" +
        "     \n-- #pageable\n  ",
        countQuery = " " +
            "  select COUNT(*) from Inventory_Item r " +
            "    inner join internal i on r.idOwner = i.idparty " +
            "    join shipment_receipt sr on r.idreceipt = sr.idreceipt " +
            "    join package_receipt pr on sr.idpackage = pr.idpackage " +
            "    join order_item oi on sr.idordite = oi.idordite"+
            " where (i.idInternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
            "    and r.idProduct = ?1  " +
            "    and r.idFeature = ?3  " +
//            "    and r.yearAssembly = ?5" + todo remark biar ga lupa
            "    and not exists " +
            "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
            "            and (z.idframe = r.idframe) " +
            "            and (z.idMachine = r.idMachine))" +
            "       and oi.unitprice = ?4" +
            "  "  ,
        nativeQuery = true)
    Page<InventoryItem> getByIdOwnerAndWithCogsIdFeatureOrderByDateCreatedNative(
        String idProduct,
        String idInternal,
        Integer idFeature,
        Number cogs,
//        Integer yearAssembly,todo remark biar ga lupa
        Pageable pageable
    );

    @Query(value = " select r.* from Inventory_Item r " +
        "    inner join internal i on r.idOwner = i.idparty " +
        "    join shipment_receipt sr on r.idreceipt = sr.idreceipt " +
        "    join package_receipt pr on sr.idpackage = pr.idpackage " +
        "    join order_item oi on sr.idordite = oi.idordite"+
        " where (i.idInternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
        "    and r.idProduct = ?1  " +
        "    and r.idFeature = ?3  " +
        "    and r.idFrame like ?4  " +
        "    and r.idMachine like ?5 " +
//        "    and r.yearAssembly = ?7" + todo remark biar ga lupa
        "    and not exists " +
        "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
        "            and (z.idframe = r.idframe) " +
        "            and (z.idMachine = r.idMachine))" +
        "       and oi.unitprice = ?6" +
        "     \n-- #pageable\n  ",
        countQuery = " " +
            " select count(*) from Inventory_Item r " +
            "    inner join internal i on r.idOwner = i.idparty " +
            "    join shipment_receipt sr on r.idreceipt = sr.idreceipt " +
            "    join package_receipt pr on sr.idpackage = pr.idpackage "+
            "    join order_item oi on sr.idordite = oi.idordite"+
            " where (i.idInternal = ?2 ) and (r.qty - r.qtybooking > 0) " +
            "    and r.idProduct = ?1  " +
            "    and r.idFeature = ?3  " +
            "    and r.idFrame like ?4  " +
            "    and r.idMachine like ?5 " +
//            "    and r.yearAssembly = ?7" + todo remark biar ga lupa
            "    and not exists " +
            "      (select null from Sales_Unit_Requirement z where (z.idProduct = r.idProduct ) " +
            "            and (z.idframe = r.idframe) " +
            "            and (z.idMachine = r.idMachine))" +
            "       and oi.unitprice = ?6" +
            "  "  ,
        nativeQuery = true)
    Page<InventoryItem> getInventoryFilteredWithCogs(
        String idProduct,
        String idInternal,
        int idFeature,
        String reqNoka,
        String reqNosin,
        Number cogs,
//        Integer yearAssembly, todo remark biar ga lupa
        Pageable pageable
    );

    @Query("select r from InventoryItem r " +
        "    where ((r.idFrame = :idFrame) or (r.idMachine = :idMachine))")
    List<InventoryItem> queryByFrameOrMachineForPackageReceipt(@Param("idFrame") String idFrame,
                                                               @Param("idMachine") String idMachine);

    @Query("select r from InventoryItem r " +
        "    join Internal i on (i.organization.idParty = r.idOwner) " +
        "    where ((r.idFrame = :idFrame) or (r.idMachine = :idMachine)) and (i.idInternal = :idInternal)")
    List<InventoryItem> queryForTransferUnit(@Param("idFrame") String idFrame,
                                             @Param("idMachine") String idMachine,
                                             @Param("idInternal") String idInternal);

    @Query("select r from InventoryItem r " +
        "    where ((r.idFrame = :idFrame) or (r.idMachine = :idMachine))")
    InventoryItem queryByFrameOrMachineForBuilBilitem(@Param("idFrame") String idFrame,
                                                               @Param("idMachine") String idMachine);

    @Query("select r from InventoryItem  r " +
        "   join BillingItem bi on r = bi.inventoryItem " +
        "   where bi.billing.idBilling = :idBilling ")
    Page<InventoryItem> findIVTByidBilling (@Param("idBilling") UUID idBilling, Pageable pageable);

}

