package id.atiila.repository;

import id.atiila.domain.GLDimension;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the GLDimension entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface GLDimensionRepository extends JpaRepository<GLDimension, Integer> {

    @Query("select r from GLDimension r where (r.internal.idInternal = :idInternal)")
    Page<GLDimension> queryByIdInternal(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from GLDimension r where (r.billTo.idBillTo = :idBillTo)")
    Page<GLDimension> queryByIdBillTo(@Param("idBillTo") String idBillTo, Pageable pageable);

    @Query("select r from GLDimension r where (r.vendor.idVendor = :idVendor)")
    Page<GLDimension> queryByIdVendor(@Param("idVendor") String idVendor, Pageable pageable);

    @Query("select r from GLDimension r where (r.partyCategory.idCategory = :idPartyCategory)")
    Page<GLDimension> queryByIdPartyCategory(@Param("idPartyCategory") Integer idPartyCategory, Pageable pageable);

    @Query("select r from GLDimension r where (r.productCategory.idCategory = :idProductCategory)")
    Page<GLDimension> queryByIdProductCategory(@Param("idProductCategory") Long idProductCategory, Pageable pageable);

    @Query("select r from GLDimension r where (r.dimension1.idDimension = :idDimension)")
    Page<GLDimension> queryByIdDimension1(@Param("idDimension") Integer idDimension, Pageable pageable);

    @Query("select r from GLDimension r where (r.dimension2.idDimension = :idDimension)")
    Page<GLDimension> queryByIdDimension2(@Param("idDimension") Integer idDimension, Pageable pageable);

    @Query("select r from GLDimension r where (r.dimension3.idDimension = :idDimension)")
    Page<GLDimension> queryByIdDimension3(@Param("idDimension") Integer idDimension, Pageable pageable);

    @Query("select r from GLDimension r where (r.dimension4.idDimension = :idDimension)")
    Page<GLDimension> queryByIdDimension4(@Param("idDimension") Integer idDimension, Pageable pageable);

    @Query("select r from GLDimension r where (r.dimension5.idDimension = :idDimension)")
    Page<GLDimension> queryByIdDimension5(@Param("idDimension") Integer idDimension, Pageable pageable);

    @Query("select r from GLDimension r where (r.dimension6.idDimension = :idDimension)")
    Page<GLDimension> queryByIdDimension6(@Param("idDimension") Integer idDimension, Pageable pageable);

	@Query("select r from GLDimension r where (1=2)")
    Page<GLDimension> queryNothing(Pageable pageable);
}

