package id.atiila.repository;

import id.atiila.domain.DealerClaimType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DealerClaimType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface DealerClaimTypeRepository extends JpaRepository<DealerClaimType, Integer> {
}

