package id.atiila.repository;

import id.atiila.domain.RequestItem;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the RequestItem entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface RequestItemRepository extends JpaRepository<RequestItem, UUID> {

    @Query("select r from RequestItem r "
	    + " where ((r.request.idRequest = :idRequest) or (:idRequest is null)) "
)
    Page<RequestItem> findByParams(@Param("idRequest") UUID idRequest, Pageable pageable);

}

