package id.atiila.repository;

import id.atiila.domain.*;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import id.atiila.service.dto.SummaryProspectDTO;
import id.atiila.service.pto.ProspectPTO;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ProspectPerson entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ProspectPersonRepository extends JpaRepository<ProspectPerson, UUID> {

    @Query("select r from ProspectPerson r where (1 = 2)")
    Page<ProspectPerson> queryNothing(Pageable page);

    @Query(" select r from ProspectPerson r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru)" +
           " and (s.idStatusType in (10, 11))")
    Page<ProspectPerson> findActiveProspectPerson(Pageable pageable);

    @Query(" select r from ProspectPerson r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) " +
           " and (s.idStatusType in (10, 51, 52, 53, 54, 55)) and (r.salesman is null)" +
           " and ((r.idSalesCoordinator = :korsal) or (:korsal is null))" +
           " and ((r.dealer.idInternal = :internal) or (:internal is null))")
    Page<ProspectPerson> findActiveProspectPersonByKorsal(Pageable pageable,
                                                          @Param("korsal") UUID idSalesCoordinator,
                                                          @Param("internal") String idInternal);

    @Query(" select r from ProspectPerson r join r.statuses s join r.salesman sal" +
           " where (current_timestamp between s.dateFrom and s.dateThru) " +
           " and (s.idStatusType in (10)) " +
           " and (sal.teamLeader = true) and ((r.salesman = :teamleader) or (:teamleader is null))" +
           " and ((r.dealer.idInternal = :internal) or (:internal is null))")
    Page<ProspectPerson> findActiveProspectPersonByTeamLeader(Pageable pageable,
                                                              @Param("teamleader") Salesman salesman,
                                                              @Param("internal") String idInternal);

    @Query(" select r from ProspectPerson r join r.statuses s " +
           " where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in :#{#param.status}) " +
           " and ((r.person.workType.idWorkType = :#{#param.workTypeId}) or (null = :#{#param.workTypeId})) " +
           " and ((r.person.postalAddress.district.idGeobou = :#{#param.districtId}) or (null = :#{#param.districtId})) " +
           " and (r.salesman is null) and ((r.idSalesCoordinator = :#{#korsal}) or (:#{#korsal} is null))" +
           " and ((r.dealer.idInternal = :#{#internal}) or (:#{#internal} is null))")
    Page<ProspectPerson> findByFilterKorsal(Pageable pageable,
                                            @Param("param") ProspectPTO params,
                                            @Param("korsal") UUID idSalesCoordinator,
                                            @Param("internal") String idInternal);

    @Query(" select r from ProspectPerson r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) " +
           " and (s.idStatusType in (10, 51, 52, 53)) and (r.person.idParty = :id)" +
           " and ((r.dealer.idInternal = :internal) or (:internal is null))")
    List<ProspectPerson> findProspectPersonByPerson(@Param("id") UUID id, @Param("internal") String idInternal);

    @Query(" select r from ProspectPerson r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru)" +
           " and (s.idStatusType = :idstatustype) and ((r.salesman = :sales) or (:sales is null))" +
           " and ((r.dealer.idInternal = :internal) or (:internal is null))" +
           " and r.distribution = 0")
    Page<ProspectPerson> findProspectPersonByStatusType(Pageable pageable,
                                                        @Param("idstatustype") Integer idStatusType,
                                                        @Param("sales") Salesman salesman,
                                                        @Param("internal") String idInternal);

    @Query(" select r from ProspectPerson r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru)" +
        " and (s.idStatusType = :idstatustype) and ((r.salesman.userName = :username) or (:username is null))" +
        " and ((r.dealer.idInternal = :internal) or (:internal is null))")
    Page<ProspectPerson> findProspectPersonByStatusTypeKorsal(Pageable pageable,
                                                        @Param("idstatustype") Integer idStatusType,
                                                        @Param("username") String username,
                                                        @Param("internal") String idInternal);

    @Query("select r from Salesman r where (r.idPartyRole = :id)")
    Salesman findSalesmanById(@Param("id") UUID id);

    @Query("select r from Salesman r where (r.coordinatorSales.idPartyRole = :id or :id is null)")
    Page<Salesman> findSalesmenByIdCoordinator(@Param("id")UUID Id, Pageable pageable);

    @Query(" select new id.atiila.service.dto.SummaryProspectDTO(s.idPartyRole, pstat.idStatusType, count(pstat.idStatusType))" +
           " from Salesman s left join ProspectPerson p on (p.salesman.idPartyRole = s.idPartyRole)" +
           " left join p.statuses pstat where (current_timestamp between pstat.dateFrom and pstat.dateThru)" +
           " and (pstat.idStatusType in (51, 52, 53))" +
           " and ((p.dealer.idInternal = :internal) or (:internal is null))" +
           " group by s.idPartyRole, pstat.idStatusType having s.idPartyRole = :id")
    List<SummaryProspectDTO> findProspectSummaryByIdSalesman(@Param("id") UUID id, @Param("internal") String idInternal);

    @Query(" select r from ProspectStatus r where (r.owner.idProspect = :id) and (r.idStatusType in (10, 51, 52, 53))")
    Page<ProspectStatus> findLastStatusBeforeRequestDrop(Pageable pageable, @Param("id") UUID id);

    @Query(" select r from ProspectPerson r join fetch r.statuses s join fetch r.person person" +
           " left join fetch person.postalAddress pa left join fetch person.categories parcat" +
           " where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType not in (13, 17))")
    List<ProspectPerson> findAllEager();

    @Query("select r from ProspectPerson r join r.statuses s " +
        "   join r.dealer d"+
        "   where (current_timestamp between s.dateFrom and s.dateThru)" +
        "   and (s.idStatusType not in (56, 17))" +
        "   and r.dealer.idInternal = :idInternal "+
        "   and r.cellphone = :cellphone")
    List<ProspectPerson> findProspectByCellphone (@Param("cellphone") String cellphone, @Param("idInternal") String idInternal);

    @Query("select r from ProspectPerson r join r.statuses s " +
        "   join r.dealer d"+
        "   where (current_timestamp between s.dateFrom and s.dateThru)" +
        "   and r.dealer.idInternal = :idInternal " +
        "   and r.cellphone = :data ")
    Page<ProspectPerson> findToReindex(@Param("data") String data, @Param("idInternal") String idInternal, Pageable pageable);

    @Query ("select r from Person r " +
        "   where r.personalIdNumber = :personalIdNumber ")
    List<Person> findPersonBypersonalIdNumber (@Param("personalIdNumber") String personalIdNumber);

}


