package id.atiila.repository;

import id.atiila.domain.CommunicationEventProspect;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the CommunicationEventProspect entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface CommunicationEventProspectRepository extends JpaRepository<CommunicationEventProspect, UUID> {
    @Query("select r from CommunicationEventProspect r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<CommunicationEventProspect> findActiveCommunicationEventProspect(Pageable pageable);

    @Query("select r from CommunicationEventProspect r join r.statuses s where (r.idProspect = :idprospect) and (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    List<CommunicationEventProspect> findActiveCommunicationEventByIdProspect(@Param("idprospect") UUID idProspect);

    @Query("select r from CommunicationEventProspect r where (1=2)")
    Page<CommunicationEventProspect> queryNothing(Pageable pageable);

    @Query("select r from CommunicationEventProspect r where (r.idProspect = :idProspect) and (r.interest = :interest) and (r.qty is not null)")
    Page<CommunicationEventProspect> findByParams(@Param("idProspect") UUID idProspect, @Param("interest") Boolean interest, Pageable pageable);
}

