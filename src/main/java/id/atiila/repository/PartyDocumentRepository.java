package id.atiila.repository;

import id.atiila.domain.DocumentType;
import id.atiila.domain.Party;
import id.atiila.domain.PartyDocument;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PartyDocument entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface PartyDocumentRepository extends JpaRepository<PartyDocument, UUID> {

    Page<PartyDocument> findAllByParty_IdParty(UUID idParty, Pageable page);

    @Query(" select r from PartyDocument r where r.documentType = :documentType and r.party = :party")
    Page<PartyDocument> findAllByPartyAndDocumentType(@Param("party") Party party,@Param("documentType") DocumentType documentType, Pageable pageable);
}

