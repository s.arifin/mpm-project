package id.atiila.repository;

import id.atiila.domain.*;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import id.atiila.service.dto.CustomShipmentDTO;
import org.hibernate.criterion.Order;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the OrderItem entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem, UUID> {

    @Query("select r from OrderItem r where (r.orders.idOrder = :idOrders)")
    Page<OrderItem> queryByIdOrders(@Param("idOrders") UUID idOrders, Pageable pageable);

    @Query("select r from OrderItem r where (r.orders.orderNumber = :orderNumber)")
    Page<OrderItem> queryByOrderNumber(@Param("orderNumber") String orderNumber, Pageable pageable);

    @Query("select r from OrderItem r where (1=2)")
    Page<OrderItem> queryNothing(Pageable pageable);

    @Query("select distinct order_item from OrderItem order_item ")
    List<OrderItem> findAllWithEagerRelationships();

    @Query("select order_item from OrderItem order_item where order_item.id =:id")
    OrderItem findOneWithEagerRelationships(@Param("id") UUID id);

    @Query("select r from Product r where r.idProduct = :idProduct ")
    Product findOneByIdProduct(@Param("idProduct") String id);

    @Query("select oi from OrderItem oi join oi.orders o  where o.idOrder = :paramID")
    Page<OrderItem> findOrderItemByOrders(@Param("paramID") UUID paramID, Pageable pageable);

    @Query("select r from OrderItem r where (r.orders.idOrder = :idOrders) ")
    Page<OrderItem> findByParams(@Param("idOrders") UUID idOrders, Pageable pageable);

    @Query("select r from OrderItem r " +
        "     join PurchaseOrder p on (p = r.orders) " +
        "     join ShipTo q on (q.party = p.vendor.organization)" +
        "    where (q.idShipTo = :idShipFrom) " +
        "      and (r.idShipTo = :idShipTo) " +
        "      and (r.qtyReceipt is null or r.qtyReceipt < r.qty)")
    Page<OrderItem> findByPurchaseUnfilled(@Param("idShipFrom") String idShipFrom, @Param("idShipTo") String idShipTo, Pageable pageable);

    @Query("select new id.atiila.service.dto.CustomShipmentDTO(v, s, pis, so, ivi) " +
        "from OrderItem oi join oi.orders o join oi.shipmentItems os join os.shipmentItem si  "+
        " left join si.shipment s " +
        " left join ShipmentOutgoing so on (so.idShipment = s.idShipment)" +
        " left join VehicleSalesOrder v on(v.idOrder = o.idOrder) "+
        " left join s.statuses b  " +
        " left join v.statuses vs " +
        " left join ItemIssuance ii on (ii.shipmentItem = si.idShipmentItem) " +
        " left join InventoryItem ivi on (ivi.idInventoryItem = ii.inventoryItem)" +
        " left join PickingSlip pis on (pis.orderItem = oi) " +
        "left join InventoryMovement im on (im.idSlip = pis)" +
        "left join im.statuses ims " +
        " where (b.idStatusType in :idstatustype ) and(vs.idStatusType = 17 or vs.idStatusType = 88) " +
        "  and (v.orderType.idOrderType in (11, 110, 111)) " +
        "  and (current_timestamp between b.dateFrom and b.dateThru)" +
        "  and (current_timestamp between ims.dateFrom and ims.dateThru) and (ims.idStatusType <> 13) " +
        "  and ((v.internal.idInternal = :internal) or (:internal is null))" +
        "   order by s.shipmentNumber")
    Page<CustomShipmentDTO> findActiveShipmentbyStatus(Pageable pageable, @Param("idstatustype") Set<Integer> idstatustype, @Param("internal") String idInternal);

    @Query("select new id.atiila.service.dto.CustomShipmentDTO(v, s, pis, so, ivi) " +
        "from OrderItem oi join oi.orders o join oi.shipmentItems os join os.shipmentItem si  "+
        " left join si.shipment s " +
        " left join ShipmentOutgoing so on (so.idShipment = s.idShipment)" +
        " left join VehicleSalesOrder v on(v.idOrder = o.idOrder) "+
        " left join s.statuses b  " +
        " left join v.statuses vs " +
        " left join ItemIssuance ii on (ii.shipmentItem = si.idShipmentItem) " +
        " left join InventoryItem ivi on (ivi.idInventoryItem = ii.inventoryItem)" +
        " left join PickingSlip pis on (pis.orderItem = oi) " +
        "left join InventoryMovement im on (im.idSlip = pis)" +
        "left join im.statuses ims " +
        " where (b.idStatusType in :idstatustype ) and (vs.idStatusType = 17 or vs.idStatusType = 88) " +
        "   and (v.orderType.idOrderType in (11, 110, 111)) and (current_timestamp between b.dateFrom and b.dateThru)" +
        "   and (s.dateSchedulle between :dateAwal and :dateAkhir)" +
        "   and (current_timestamp between ims.dateFrom and ims.dateThru) and (ims.idStatusType <> 13) " +
        "   and ((v.internal.idInternal = :internal) or (:internal is null))")
    Page<CustomShipmentDTO> findActiveShipmentbyStatusAssign(Pageable pageable, @Param("idstatustype") Set<Integer> idstatustype, @Param("internal") String idInternal, @Param("dateAwal")ZonedDateTime dateAwal, @Param("dateAkhir")ZonedDateTime dateAkhir);

    @Query("select oi from OrderItem oi join oi.orders o join oi.billingItems bi join bi.billingItem bilItm where bilItm in :bilIt ")
    List<OrderItem> findOrderItemByBillItem(@Param("bilIt") Set<BillingItem> bilIt);

    @Query("select si from OrderItem  oi " +
        "join oi.shipmentItems os " +
        " join os.shipmentItem site  " +
        " join site.shipment s  " +
        " join ShipmentIncoming si on (s.idShipment = si.idShipment) " +
        " join Internal i on i = s.internal  " +
        " where oi.idframe = :idFrame")
    ShipmentIncoming findPackageReceiptbyIdFrameOrderItem(@Param("idFrame") String idFrame);

    @Query("select r from OrderItem r join r.orders o join o.statuses s join VehicleSalesOrder v on (v.idOrder = o.idOrder) " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17)) " +
        "      and ( " +
        "            (not exists (select 1 from OrderShipmentItem z where (z.orderItem = r))) or " +
        "            (coalesce(r.qty, 1) > (select sum(coalesce(z.qty, 0)) from OrderShipmentItem z where (z.orderItem = r)))" +
        "           ) " +
        "      and (v.internal.idInternal = :idInternal)" +
        "       order by o.dateEntry desc")
    Page<OrderItem> findActiveOrderItem(@Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from OrderItem r join r.orders o join o.statuses s join VehicleSalesOrder v on (v.idOrder = o.idOrder) join v.internal intrl" +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17)) ")
    List<OrderItem> findToReindex();

    @Query("select r from OrderItem r" +
        "       join ShipmentReceipt sr on sr.orderItem = r" +
        "       join InventoryItem ii on ii.shipmentReceipt = sr " +
        "       join Internal i on ii.idOwner = i.organization.idParty" +
        "       where (r.idProduct = :idProduct) or (ii.idFrame = :idFrame)" +
        "       and i.idInternal = :idInternal" +
        "       order by sr.dateReceipt desc")
    List<OrderItem>findCogs (@Param("idProduct") String idProduct, @Param("idFrame") String idFrame, @Param("idInternal") String idInternal);

    @Query("select r from OrderItem r" +
//        "       join ShipmentReceipt sr on sr.orderItem = r" +
//        "       join InventoryItem ii on ii.shipmentReceipt = sr " +
//        "       join Internal i on ii.idOwner = i.organization.idParty" +
        "       where (r.idframe = :idFrame)")
    Page<OrderItem>findCogsBynoka (@Param("idFrame") String idFrame, Pageable pageable);

    @Query("select r from OrderItem r " +
        "   join OrderBillingItem obi on r = obi.orderItem" +
        "   where obi.billingItem.billing.idBilling = :idBill")
    Page<OrderItem> findOrderItemByIdBill (@Param("idBill") UUID idBill, Pageable pageable);

    @Query("select r from OrderItem r " +
        "   join OrderBillingItem obi on r = obi.orderItem" +
        "   where (obi.billingItem.billing.idBilling = :idBill " +
        "   and r.idProduct =:idProduct)")
    Page<OrderItem> findOrderItemByIdBillAndIdProduct (@Param("idBill") UUID idBill, @Param("idProduct") String idProduct, Pageable pageable);

    @Query("select r.idProduct from OrderItem r " +
        "   join OrderBillingItem obi on r = obi.orderItem " +
        "   where obi.billingItem.billing.idBilling = :idBill " +
        "   order by r.idProduct")
    List<String> findProductidOrderItem (@Param("idBill") UUID idBill);

}

