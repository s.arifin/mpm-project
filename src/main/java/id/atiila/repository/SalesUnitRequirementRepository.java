package id.atiila.repository;

import id.atiila.domain.*;

import java.util.List;
import java.util.UUID;

import id.atiila.service.pto.OrderPTO;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import id.atiila.service.pto.SalesUnitRequirementPTO;

/**
 * Spring Data JPA repository for the SalesUnitRequirement entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface SalesUnitRequirementRepository extends JpaRepository<SalesUnitRequirement, UUID> {

    @Query("select a from Approval a where (a.idRequirement = :idreq) and (current_timestamp between a.dateFrom and a.dateThru) " +
        "and (a.idApprovalType = :idapprovaltype)")
    Approval findActiveApprovalByApprovalType(
        @Param("idreq") UUID idreq,
        @Param("idapprovaltype") Integer idapprovaltype
    );

    @Query("select a from RuleSalesDiscount a where (a.idInternal=:idinternal) and (current_timestamp between a.dateFrom and a.dateThru) and (a.idRuleType=102) order by a.sequenceNumber asc")
    List<RuleSalesDiscount> findActiveRuleSalesDiscount(
        @Param("idinternal") String idinternal
    );

    @Query("select r from SalesUnitRequirement r join r.statuses s " +
        "   where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))" +
        "   and r.internal.idInternal = :idInternal")
    Page<SalesUnitRequirement> findActiveSalesUnitRequirement(@Param("idInternal") String idInternal,Pageable pageable);

    @Query("select r from SalesUnitRequirement r join r.approvals s " +
        "   where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and (s.idApprovalType in (104)) " +
        "   and (s.idStatusType not in (1, 61, 62))" +
        "   and r.internal.idInternal = :idInternal" +
        "   order by r.requirementNumber ")
    Page<SalesUnitRequirement> findActiveLeasingApproval(@Param("idInternal") String idInternal,Pageable pageable);

    @Query("select r from SalesUnitRequirement r join r.approvals s " +
        "   join Customer v on (v = r.customer)" +
        "   join LeasingCompany l on r.leasingCompany = l" +
        "   join Organization o on l.party.idParty = o.idParty" +
        "   join Person p on (p.idParty = v.party.idParty)" +
        "   where ((current_timestamp between s.dateFrom and s.dateThru) and (s.idApprovalType in (104)) and (s.idStatusType not in (1))  and (r.internal.idInternal = :idInternal))" +
        "   and ((r.requirementNumber like :data) or (p.firstName like :data) or (p.lastName like :data) or (o.name like :data))")
    Page<SalesUnitRequirement> searchActiveLeasingApproval(@Param("idInternal") String idInternal, @Param("data") String data,Pageable pageable);

    @Query("select r from SalesUnitRequirement r join r.approvals s " +
        "   where (current_timestamp between s.dateFrom and s.dateThru) " +
        "   and r.salesman.coordinatorSales.idPartyRole = :idcoordinator" +
        "   and (s.idApprovalType in (104)) " +
        "   and (s.idStatusType in (75))" +
        "   and r.internal.idInternal = :idInternal")
    Page<SalesUnitRequirement> findActiveLeasingApprovalRequestAttachment(@Param("idInternal") String idInternal,@Param("idcoordinator") UUID idcoordinator,Pageable pageable);

    @Query("select r from Product r where r.idProduct = :idProduct ")
    Product findOneByIdProduct(@Param("idProduct") String id);

    @Query("select r from Person r where r.idParty = :id ")
    Person findPersonByIdParty(@Param("id") String id);

    @Query("select distinct a from SalesUnitRequirement a " +
        "join a.approvals appone join a.approvals apptwo join a.approvals appthree join a.statuses s " +
        "where (current_timestamp between s.dateFrom and s.dateThru and (s.idStatusType in(10))) " +
        "and (a.internal.idInternal = :idinternal) " +
        "and (a.idProspect = :idprospect) " +
        "and (appone.idStatusType = :idstatus) and (appone.idApprovalType = 103) " +
        "and (apptwo.idStatusType = :idstatus) and (apptwo.idApprovalType = 101) " +
        "and (appthree.idStatusType = 1) and (appthree.idApprovalType = 104)")
    Page<SalesUnitRequirement> findSalesUnitRequirementByStatusApprovalDiscountAndPelanggaranWilayahByProspect(
        @Param("idstatus") Integer idstatus,
        @Param("idinternal") String idinternal,
        @Param("idprospect") UUID idprospect,
        Pageable pageable
    );

    @Query("select r from SalesUnitRequirement r join r.statuses s " +
        "join r.approvals appone join r.approvals apptwo join r.approvals appthree " +
        "where (current_timestamp between s.dateFrom and s.dateThru and (s.idStatusType in(10))) " +
        "and (r.internal.idInternal = :idinternal) " +
        "and (r.salesman.idPartyRole = :idsalesman) " +
        "and (appone.idStatusType = :idstatus) and (appone.idApprovalType = 103) " +
        "and (apptwo.idStatusType = :idstatus) and (apptwo.idApprovalType = 101) " +
        "and (appthree.idStatusType = 1) and (appthree.idApprovalType = 104)")
    Page<SalesUnitRequirement> findByApprovalDiscountAndPelanggaranWilayahBySales(
        @Param("idstatus") int idstatus,
        @Param("idinternal") String idinternal,
        @Param("idsalesman") UUID idsalesman,
        Pageable pageable
    );

    @Query("select distinct a from SalesUnitRequirement a " +
        "join a.approvals appone join a.approvals apptwo join a.approvals appthree join a.statuses s " +
        "where (current_timestamp between s.dateFrom and s.dateThru and (s.idStatusType in(10))) " +
        "and (a.internal.idInternal = :idinternal) " +
        "and (a.idProspect = :idprospect) " +
        "and ((appone.idStatusType = :idstatus) and (appone.idApprovalType = 103) " +
        "or (apptwo.idStatusType = :idstatus) and (apptwo.idApprovalType = 101)" +
        "or (appthree.idStatusType = :idstatus) and (appthree.idApprovalType = 104))")
    Page<SalesUnitRequirement> findSalesUnitRequirementByStatusApprovalDiscountOrPelanggaranWilayahByProspect(
        @Param("idstatus") Integer idstatus,
        @Param("idinternal") String idinternal,
        @Param("idprospect") UUID idprospect,
        Pageable pageable
    );

    // approve not approve draft vso di sales
    @Query("select distinct a from SalesUnitRequirement a " +
        "join a.approvals appone join a.approvals apptwo join a.approvals appthree join a.statuses s " +
        "where (current_timestamp between s.dateFrom and s.dateThru and (s.idStatusType in(10))) " +
        "and (a.internal.idInternal = :idinternal) " +
        "and (a.salesman.idPartyRole = :idsalesman)" +
        "and ((appone.idStatusType = :idstatus) and (appone.idApprovalType = 103) " +
        "or (apptwo.idStatusType = :idstatus) and (apptwo.idApprovalType = 101)" +
        "or (appthree.idStatusType = :idstatus) and (appthree.idApprovalType = 104))")
    Page<SalesUnitRequirement> findByApprovalDiscountOrPelanggaranWilayahBySales(
        @Param("idstatus") int idstatus,
        @Param("idinternal") String idinternal,
        @Param("idsalesman") UUID idsalesman,
        Pageable pageable
    );

    @Query("select distinct a from SalesUnitRequirement a join a.approvals b join a.statuses s where(b.idStatusType = :idstatus) " +
        "and (s.idStatusType = :idstatustype) " +
        "and (a.internal.idInternal = :idinternal)" +
        "")
    Page<SalesUnitRequirement> findSalesUnitRequirementByStatusApprovalAndStatusType(
        @Param("idinternal") String idinternal,
        @Param("idstatustype") Integer idstatustype,
        @Param("idstatus") Integer idstatus,
        Pageable pageable
    );

    @Query("select a from SalesUnitRequirement a join a.approvals b join a.statuses s " +
        "where ((b.idStatusType = :idstatus) or (s.idStatusType= :requirement_status)) " +
        "and (a.internal.idInternal= :idinternal)")
    Page<SalesUnitRequirement> findSalesUnitRequirementByStatusSubsidiAndRequirementStatus(
        @Param("idstatus") int idstatus,
        @Param("requirement_status") int requirement_status,
        @Param("idinternal") String idinternal,
        Pageable pageable
    );

    @Query("select r from SalesUnitRequirement r join r.approvals b join r.statuses c " +
        "where (b.idLeasingCompany = :idleasing) " +
        "and (b.idApprovalType = 104) " +
        "and (current_timestamp between b.dateFrom and b.dateThru) " +
        "and (b.idStatusType = 60)" +
        "and (c.idStatusType not in(13, 17))")
    Page<SalesUnitRequirement> findByApprovalActiveForLeasing(
        @Param("idleasing") UUID idleasing,
        Pageable pageable
    );

    @Query("select r from SalesUnitRequirement r join r.approvals a join r.statuses c " +
        "where (r.internal.idInternal = :idinternal) " +
        "and (a.idStatusType = 61 and a.idApprovalType in(101, 103))" +
        "and (a.idStatusType = 60 and a.idApprovalType = 104) " +
        "and (c.idStatusType  = 10)")
    Page<SalesUnitRequirement> findByApprovalForCredit(
        @Param("idinternal") String idinternal,
        Pageable pageable
    );

    @Query(value = " select plw.idinternal from pelanggaran_luar_wilayah plw " +
        " join geo_boundary gb on gb.idgeobou = plw.idgeobou " +
        " where plw.idinternal = ?1 " +
        " and gb.idgeoboutype = 12 " +
        " and gb.geocode = ?2 " +
        " \n-- #pageable\n  ",
        countQuery = " " +
            "  select COUNT (plw.idinternal) from pelanggaran_luar_wilayah plw " +
            " join geo_boundary gb on gb.idgeobou = plw.idgeobou " +
            " where plw.idinternal = ?1 " +
            " and gb.idgeoboutype = 12 " +
            " and gb.geocode = ?2 ",
        nativeQuery = true)
    List<String> findCheckRegion(String idInternal, String idGeobou);

    @Query("select r from SalesUnitRequirement r join r.statuses s where r.idframe = :idframe")
    SalesUnitRequirement findSurbyFrame(@Param("idframe") String idframe);

    @Query("select r from SalesUnitRequirement r where r.idRequest = :idRequest")
    List<SalesUnitRequirement> quesryByIdRequest(@Param("idRequest") UUID idRequest);

    @Query("select r from SalesUnitRequirement r where r.idRequest = :idRequest and r.requirementNumber is not null")
    List<SalesUnitRequirement> quesryByIdRequestAndReqNumber(@Param("idRequest") UUID idRequest);

    @Query("SELECT r FROM SalesUnitRequirement r WHERE " +
        "1=1 " +
        "AND (r.internal.idInternal = :idinternal)")
    Page<SalesUnitRequirement> findAllByInternal(
        @Param("idinternal") String idinternal,
        Pageable pageable
    );

    @Query("SELECT r FROM SalesUnitRequirement r JOIN r.statuses s WHERE " +
        "1=1 " +
        "AND (s.idStatusType NOT IN(13,17)) " +
        "AND (current_timestamp BETWEEN s.dateFrom AND s.dateThru) " +
        "AND (r.internal.idInternal = :idinternal) " +
        "AND (r.idRequest = :idrequest)")
    Page<SalesUnitRequirement> findAllByIdRequest(
        @Param("idrequest") UUID idrequest,
        @Param("idinternal") String idinternal,
        Pageable pageable
     );

    @Query("SELECT r FROM SalesUnitRequirement r join r.statuses s " +
        "    where (r.idRequest = :idrequest) " +
        "      and (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17)) ")
    Page<SalesUnitRequirement> queryByIdRequest(@Param("idrequest") UUID idrequest, Pageable pageable);

    @Query("SELECT r FROM SalesUnitRequirement r where (1=2)")
    Page<SalesUnitRequirement> queryNothing(Pageable pageable);

    @Query("SELECT r FROM SalesUnitRequirement r join RequirementOrderItem o on (r = o.requirement)" +
        "    where (o.orderItem.idOrderItem = :idOrderItem)")
    SalesUnitRequirement queryByIdOrderItem(@Param("idOrderItem") UUID idOrderItem);

    @Query("select r from SalesUnitRequirement r " +
        " join r.statuses s" +
        " join Customer v on (v = r.customer)" +
        " join Person p on (p.idParty = v.party.idParty)" +
        " join r.internal i" +
        " join Salesman sal on (sal = r.salesman)" +
        " where (r.internal.idInternal = :idinternal) " +
        "and (s.idStatusType = 10) and ( p.firstName like :data  or p.lastName like :data ) " +
        " and sal = :sales ")
    Page<SalesUnitRequirement> findSearchNama(@Param("data") String data, @Param("idinternal") String idinternal,  @Param("sales") Salesman sales, Pageable pageable);

    @Query("SELECT r FROM SalesUnitRequirement r " +
        "   WHERE r.resetKey = :resetKey")
    SalesUnitRequirement findResetKey(@Param("resetKey") String resetKey);

    @Query("select distinct a from SalesUnitRequirement a " +
        "join a.approvals appone join a.approvals apptwo join a.approvals appthree join a.statuses s " +
        "where (current_timestamp between s.dateFrom and s.dateThru and (s.idStatusType in(62))) " +
        "and (a.internal.idInternal = :idinternal) " +
        "and (a.salesman.coordinatorSales.idPartyRole = :idsalesman)" +
        "and ((appone.idStatusType = :idstatus) and (appone.idApprovalType = 103) " +
        "and (apptwo.idStatusType = :idstatus) and (apptwo.idApprovalType = 101)" +
        "and (appthree.idStatusType = 1) and (appthree.idApprovalType = 104))")
    Page<SalesUnitRequirement> findSalesUnitRequirementByStatusApprovalDiscountAndPelanggaranWilayahByProspectByKorsal(
        @Param("idstatus") Integer idstatus,
        @Param("idinternal") String idinternal,
        @Param("idsalesman") UUID idsalesman,
        Pageable pageable
    );

    @Query("select r from SalesUnitRequirement r " +
        "   join VehicleSalesOrder vso on r.idRequirement = vso.idSalesUnitRequirement " +
        "   join OrderItem oi on vso = oi.orders " +
        "   join OrderBillingItem obi on oi = obi.orderItem" +
        "   where (obi.billingItem.billing.idBilling = :idBill " +
        "   and oi.idProduct =:idProduct)")
    Page<SalesUnitRequirement> findOrderItemByIdBillAndIdProduct (@Param("idBill") UUID idBill, @Param("idProduct") String idProduct, Pageable pageable);

    @Query("select r from SalesUnitRequirement r join PersonalCustomer q on (q = r.customer) " +
        "    join r.approvals a" +
        "    where (current_timestamp between a.dateFrom and a.dateThru)" +
        "      and (r.dateCreate between :#{#param.orderDateFrom} and :#{#param.orderDateThru}) " +
        "      and ((r.salesman.idPartyRole = :#{#param.salesmanId}) or (null = :#{#param.salesmanId}))" +
        "      and ((r.internal.idInternal = :#{#param.internalId}) or (null = :#{#param.internalId})) " +
        "      and ((r.leasingCompany.idPartyRole = :#{#param.leasingCompanyId}) or (null = :#{#param.leasingCompanyId}))" +
        "      and (a.idApprovalType in (104))" +
//        "      and (a.idStatusType in (70, 61, 62))" +
        "      and ((a.idStatusType in :#{#param.statusAL}) or (null in :#{#param.statusAL})) " +
        "       order by r.requirementNumber ")
    Page<SalesUnitRequirement> findApprovalLeasingByFilter(Pageable pageable, @Param("param") OrderPTO params);

    @Query(value = "select sur.* from sales_unit_requirement sur" +
        "   join unit_document_message udm on sur.idreq = udm.idreq" +
        "   join unit_document_message_request udmr on udm.idmessage = udmr.idmessage" +
        "   join request_notes rqn on udmr.idreqnot = rqn.idreqnot" +
        "   join request_notes_status rqns on rqn.idreqnot = rqns.idreqnot" +
        "   where CURRENT_TIMESTAMP between rqns.dtfrom and rqns.dtthru and idstatustype not in(13,17 ) and sur.idreq = ?1" +
        "     \n-- #pageable\n  ",
        countQuery = " " +
            "   select COUNT(*) from sales_unit_requirement sur" +
            "   join unit_document_message udm on sur.idreq = udm.idreq" +
            "   join unit_document_message_request udmr on udm.idmessage = udmr.idmessage" +
            "   join request_notes rqn on udmr.idreqnot = rqn.idreqnot" +
            "   join request_notes_status rqns on rqn.idreqnot = rqns.idreqnot" +
            "   where CURRENT_TIMESTAMP between rqns.dtfrom and rqns.dtthru and idstatustype not in(13,17 ) and sur.idreq = ?1" +
            "  "  ,
        nativeQuery = true)
    List<SalesUnitRequirement> getSurOnRequestNote(
        UUID idreq
    );

}

