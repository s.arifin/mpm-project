package id.atiila.repository;

import id.atiila.domain.VendorProduct;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the VendorProduct entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface VendorProductRepository extends JpaRepository<VendorProduct, UUID> {


}

