package id.atiila.repository;

import id.atiila.domain.Payment;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Payment entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface PaymentRepository extends JpaRepository<Payment, UUID> {

    @Query("select r from Payment r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<Payment> findActivePayment(Pageable pageable);
    
    @Query("select r from Payment r where (r.paymentType.idPaymentType = :idPaymentType)")
    Page<Payment> findByIdPaymentType(@Param("idPaymentType") Integer idPaymentType, Pageable pageable); 
    
    @Query("select r from Payment r where (r.method.idPaymentMethod = :idMethod)")
    Page<Payment> findByIdMethod(@Param("idMethod") Integer idMethod, Pageable pageable); 
    
    @Query("select r from Payment r where (r.internal.idInternal = :idInternal)")
    Page<Payment> findByIdInternal(@Param("idInternal") String idInternal, Pageable pageable); 
    
    @Query("select r from Payment r where (r.paidTo.idBillTo = :idPaidTo)")
    Page<Payment> findByIdPaidTo(@Param("idPaidTo") String idPaidTo, Pageable pageable); 
    
    @Query("select r from Payment r where (r.paidFrom.idBillTo = :idPaidFrom)")
    Page<Payment> findByIdPaidFrom(@Param("idPaidFrom") String idPaidFrom, Pageable pageable); 

}

