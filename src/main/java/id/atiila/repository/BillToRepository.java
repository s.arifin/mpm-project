package id.atiila.repository;

import id.atiila.domain.BillTo;
import id.atiila.domain.Party;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

/**
 * Spring Data JPA repository for the BillTo entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface BillToRepository extends JpaRepository<BillTo, String> {

    @Query("select r from BillTo r where (r.party.idParty = :idParty)")
    Page<BillTo> queryByIdParty(@Param("idParty") UUID idParty, Pageable pageable);

    @Query("select r from BillTo r where (r.idRoleType = :idRoleType)")
    Page<BillTo> queryByIdRoleType(@Param("idRoleType") Integer idRoleType, Pageable pageable);

    @Query("select r from BillTo r where (1=2)")
    Page<BillTo> queryNothing(Pageable pageable);

    BillTo findByParty(Party p);
}

