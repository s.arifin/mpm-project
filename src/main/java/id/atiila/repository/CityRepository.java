package id.atiila.repository;

import id.atiila.domain.City;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the City entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface CityRepository extends JpaRepository<City, UUID> {

    City findByGeoCode(String id);

    List<City> findOneByDescription(String description);
}


