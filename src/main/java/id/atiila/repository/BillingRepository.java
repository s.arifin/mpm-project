package id.atiila.repository;

import id.atiila.domain.Billing;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Billing entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface BillingRepository extends JpaRepository<Billing, UUID> {

    @Query("select r from Billing r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<Billing> findActiveBilling(Pageable pageable);

    @Query("select r from Billing r "
	    + " where ((r.billingType.idBillingType = :idBillingType) or (:idBillingType is null)) "
	    + " and ((r.internal.idInternal = :idInternal) or (:idInternal is null)) "
	    + " and ((r.billTo.idBillTo = :idBillTo) or (:idBillTo is null)) "
	    + " and ((r.billFrom.idBillTo = :idBillFrom) or (:idBillFrom is null)) "
)
    Page<Billing> findByParams(@Param("idBillingType") String idBillingType, @Param("idInternal") String idInternal, @Param("idBillTo") String idBillTo, @Param("idBillFrom") String idBillFrom, Pageable pageable);

    @Query("select r from Billing r " +
        "   join BillingItem bi on r.idBilling = bi.billing.idBilling " +
        "   join OrderBillingItem obi on bi.idBillingItem = obi.billingItem.idBillingItem " +
        "   join r.statuses s" +
        "   where ((obi.orderItem.idOrderItem = :idOrderItem)) " +
        "   and (current_timestamp between s.dateFrom and s.dateThru)"
    )
    Billing findByOrderItem(@Param("idOrderItem") UUID idOrderItem);

    List<Billing> findByDescription(String description);
}

