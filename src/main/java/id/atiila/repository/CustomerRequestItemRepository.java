package id.atiila.repository;

import id.atiila.domain.CustomerRequestItem;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the CustomerRequestItem entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface CustomerRequestItemRepository extends JpaRepository<CustomerRequestItem, UUID> {
    
    @Query("select r from CustomerRequestItem r where (r.request.idRequest = :idRequest)")
    Page<CustomerRequestItem> queryByIdRequest(@Param("idRequest") UUID idRequest, Pageable pageable); 
    
    @Query("select r from CustomerRequestItem r where (r.product.idProduct = :idProduct)")
    Page<CustomerRequestItem> queryByIdProduct(@Param("idProduct") String idProduct, Pageable pageable); 
    
    @Query("select r from CustomerRequestItem r where (r.feature.idFeature = :idFeature)")
    Page<CustomerRequestItem> queryByIdFeature(@Param("idFeature") Integer idFeature, Pageable pageable); 
    
    @Query("select r from CustomerRequestItem r where (r.customer.idCustomer = :idCustomer)")
    Page<CustomerRequestItem> queryByIdCustomer(@Param("idCustomer") String idCustomer, Pageable pageable); 

	@Query("select r from CustomerRequestItem r where (1=2)")
    Page<CustomerRequestItem> queryNothing(Pageable pageable); 
}

