package id.atiila.repository;

import id.atiila.domain.ProspectOrganization;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ProspectOrganization entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ProspectOrganizationRepository extends JpaRepository<ProspectOrganization, UUID> {
    @Query("select r from ProspectOrganization r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<ProspectOrganization> findActiveProspectOrganization(Pageable pageable);

    @Query("select r from ProspectOrganization r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 51, 52, 53)) and (r.organization.idParty = :id)")
    List<ProspectOrganization> findProspectOrganizationByOrganization(@Param("id") UUID id);

    @Query("select r from ProspectOrganization r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType = :idstatustype)")
    Page<ProspectOrganization> findProspectOrganizationByStatusType(@Param("idstatustype") Integer idStatusType, Pageable pageable);
}

