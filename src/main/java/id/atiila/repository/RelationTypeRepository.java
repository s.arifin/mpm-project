package id.atiila.repository;

import id.atiila.domain.RelationType;

import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RelationType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface RelationTypeRepository extends JpaRepository<RelationType, Integer> {


}
