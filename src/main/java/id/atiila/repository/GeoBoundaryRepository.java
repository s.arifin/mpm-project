package id.atiila.repository;

import id.atiila.domain.GeoBoundary;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the GeoBoundary entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface GeoBoundaryRepository extends JpaRepository<GeoBoundary, UUID> {

    @Query(" select r from GeoBoundary r where (r.parent.idGeobou = :parent)")
    List<GeoBoundary> findAllByParent(@Param("parent") UUID idParent);

    List<GeoBoundary> findOneByDescription(String description);
}

