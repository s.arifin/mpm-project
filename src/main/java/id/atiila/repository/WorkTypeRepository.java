package id.atiila.repository;

import id.atiila.domain.WorkType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the WorkType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface WorkTypeRepository extends JpaRepository<WorkType, Integer> {


}

