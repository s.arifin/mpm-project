package id.atiila.repository;

import id.atiila.domain.RequestProduct;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the RequestProduct entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface RequestProductRepository extends JpaRepository<RequestProduct, UUID> {

    @Query("select r from RequestProduct r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11, 60, 61, 62))")
    Page<RequestProduct> findActiveRequestProduct(Pageable pageable);

    @Query("select r from RequestProduct r "
	    + " where ((r.facilityFrom.idFacility = :idFacilityFrom) or (:idFacilityFrom is null)) "
	    + " and ((r.facilityTo.idFacility = :idFacilityTo) or (:idFacilityTo is null)) "
)
    Page<RequestProduct> findByParams(@Param("idFacilityFrom") String idFacilityFrom, @Param("idFacilityTo") String idFacilityTo, Pageable pageable);

    @Query("select r from RequestProduct r join r.statuses s " +
        "where (s.idStatusType = :idStatusType)")
    Page<RequestProduct> findByStatusType(@Param("idStatusType")Integer idStatusType, Pageable pageable);
}

