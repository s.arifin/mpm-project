package id.atiila.repository;

import id.atiila.domain.WorkOrder;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the WorkOrder entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface WorkOrderRepository extends JpaRepository<WorkOrder, UUID> {


}

