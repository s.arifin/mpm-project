package id.atiila.repository;

import id.atiila.domain.ProspectSource;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ProspectSource entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ProspectSourceRepository extends JpaRepository<ProspectSource, Integer> {


}

