package id.atiila.repository;

import id.atiila.domain.RequestType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the RequestType entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface RequestTypeRepository extends JpaRepository<RequestType, Integer> {

    @Query("select r from RequestType r "
)
    Page<RequestType> findByParams(Pageable pageable);

}

