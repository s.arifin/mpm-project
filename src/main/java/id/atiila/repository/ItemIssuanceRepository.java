package id.atiila.repository;

import id.atiila.domain.ItemIssuance;

import java.util.List;
import java.util.UUID;

import id.atiila.domain.ShipmentItem;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ItemIssuance entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ItemIssuanceRepository extends JpaRepository<ItemIssuance, UUID> {

    ItemIssuance findByShipmentItem(ShipmentItem shipmentItem);

    @Query(value = "select i from ItemIssuance i where i.shipmentItem = :shipmentItem")
    List<ItemIssuance> findByShipmentList(@Param("shipmentItem") ShipmentItem shipmentItem);

}

