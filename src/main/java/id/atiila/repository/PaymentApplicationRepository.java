package id.atiila.repository;

import id.atiila.domain.PaymentApplication;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the PaymentApplication entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface PaymentApplicationRepository extends JpaRepository<PaymentApplication, UUID> {
    
    @Query("select r from PaymentApplication r where (r.payment.idPayment = :idPayment)")
    Page<PaymentApplication> findByIdPayment(@Param("idPayment") UUID idPayment, Pageable pageable); 
    
    @Query("select r from PaymentApplication r where (r.billing.idBilling = :idBilling)")
    Page<PaymentApplication> findByIdBilling(@Param("idBilling") UUID idBilling, Pageable pageable);

    @Query("select r from PaymentApplication r where (r.payment.idPayment = :idPayment)")
    List<PaymentApplication> queryByIdPayment(@Param("idPayment") UUID idPayment);

}

