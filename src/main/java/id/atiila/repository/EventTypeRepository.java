package id.atiila.repository;

import id.atiila.domain.EventType;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the EventType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface EventTypeRepository extends JpaRepository<EventType, Integer> {

    @Query("select r from EventType r where (r.idParentEventType = :idparent)")
    List<EventType> findAllByIdParentEvent(@Param("idparent") Integer idParent);

}

