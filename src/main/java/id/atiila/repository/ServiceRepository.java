package id.atiila.repository;

import id.atiila.domain.Service;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Service entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ServiceRepository extends JpaRepository<Service, String> {
    @Query("select distinct service from Service service left join fetch service.features left join fetch service.categories")
    List<Service> findAllWithEagerRelationships();

    @Query("select service from Service service left join fetch service.features left join fetch service.categories where service.id =:id")
    Service findOneWithEagerRelationships(@Param("id") String id);
}

