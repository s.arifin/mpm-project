package id.atiila.repository;

import id.atiila.domain.Position;
import id.atiila.domain.PositionReportingStructure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the PositionReportingStructure entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface PositionReportingStructureRepository extends JpaRepository<PositionReportingStructure, Integer> {
    @Query("select r from PositionReportingStructure r where (r.positionFrom = :from) and (r.positionTo = :to) and (current_timestamp between r.dateFrom and r.dateThru)")
    PositionReportingStructure findStructure(@Param("to") Position to, @Param("from") Position from);

}

