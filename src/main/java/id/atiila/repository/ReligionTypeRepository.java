package id.atiila.repository;

import id.atiila.domain.ReligionType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ReligionType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ReligionTypeRepository extends JpaRepository<ReligionType, Integer> {


}

