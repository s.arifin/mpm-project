package id.atiila.repository;

import id.atiila.domain.ProductAssociation;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ProductAssociation entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ProductAssociationRepository extends JpaRepository<ProductAssociation, UUID> {
}

