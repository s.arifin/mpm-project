package id.atiila.repository;

import id.atiila.domain.WorkOrderBooking;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the WorkOrderBooking entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface WorkOrderBookingRepository extends JpaRepository<WorkOrderBooking, UUID> {
    @Query("select r from WorkOrderBooking r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<WorkOrderBooking> findActiveWorkOrderBooking(Pageable pageable);
    
    WorkOrderBooking findOneByBookingNumber(String id);
    
}

