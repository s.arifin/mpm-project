package id.atiila.repository;

import id.atiila.domain.DocumentType;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the DocumentType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface DocumentTypeRepository extends JpaRepository<DocumentType, Integer> {
    List<DocumentType> findAllByIdParent(Integer idparent);
}

