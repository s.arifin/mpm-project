package id.atiila.repository;

import id.atiila.domain.Province;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Province entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ProvinceRepository extends JpaRepository<Province, UUID> {

    Province findByGeoCode(String id);

    List<Province> findOneByDescription(String description);

}

