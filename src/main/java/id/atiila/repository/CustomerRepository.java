package id.atiila.repository;

import id.atiila.domain.Customer;
import id.atiila.domain.Party;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Customer entity.
 * BeSmart Team
 */@SuppressWarnings("unused")
@Repository
public interface CustomerRepository<T extends Customer> extends JpaRepository<T, String> {

     T findByParty(Party p);

     T findByIdCustomer(String idcustomer);

     @Query("select r from Customer r where r.party = :party")
    List<Customer> findListCustomerByParty (@Param("party") Party party);

     @Query("select r from Customer r where r.idMPM = :idMPM")
    List<Customer> findCustByIdMPM(@Param("idMPM") String idMPM);
}

