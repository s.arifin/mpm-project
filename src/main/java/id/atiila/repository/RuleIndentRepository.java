package id.atiila.repository;

import id.atiila.domain.RuleIndent;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the RuleIndent entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface RuleIndentRepository extends JpaRepository<RuleIndent, Integer> {

    @Query("select r from RuleIndent r ")
    Page<RuleIndent> findByParams(Pageable pageable);

    @Query("select count(r) as total from RuleIndent r where (r.idProduct = :idproduct) and (r.idInternal = :idinternal) and (current_timestamp between r.dateFrom and r.dateThru)")
    Integer findTotalRuleIndentByProduct(
        @Param("idproduct") String idproduct,
        @Param("idinternal") String idinternal
    );

    Page<RuleIndent> findAllByIdProductAndIdInternal(String idproduct, String idinternal, Pageable pageable);
}

