package id.atiila.repository;

import id.atiila.domain.PriceType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PriceType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface PriceTypeRepository extends JpaRepository<PriceType, Integer> {
}

