package id.atiila.repository;

import id.atiila.domain.WorkProductRequirement;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the WorkProductRequirement entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface WorkProductRequirementRepository extends JpaRepository<WorkProductRequirement, UUID> {


}

