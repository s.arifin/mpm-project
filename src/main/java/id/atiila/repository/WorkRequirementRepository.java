package id.atiila.repository;

import id.atiila.domain.WorkRequirement;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the WorkRequirement entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface WorkRequirementRepository extends JpaRepository<WorkRequirement, UUID> {


    @Query("select distinct work_req from WorkRequirement work_req left join fetch work_req.services")
    List<WorkRequirement> findAllWithEagerRelationships();

    @Query("select work_req from WorkRequirement work_req left join fetch work_req.services where work_req.idRequirement = :id")
    WorkRequirement findOneWithEagerRelationships(@Param("id") UUID id);
    
}

