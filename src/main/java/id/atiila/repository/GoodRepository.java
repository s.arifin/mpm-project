package id.atiila.repository;

import id.atiila.domain.Good;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Good entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface GoodRepository extends JpaRepository<Good, String> {
    
    @Query("select distinct good from Good good left join fetch good.features left join fetch good.categories")
    List<Good> findAllWithEagerRelationships();

    @Query("select good from Good good left join fetch good.features left join fetch good.categories where good.idProduct = :id")
    Good findOneWithEagerRelationships(@Param("id") String id);
    
}
