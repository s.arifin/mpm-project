package id.atiila.repository;

import id.atiila.domain.MotorDueReminder;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MotorDueReminder entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface MotorDueReminderRepository extends JpaRepository<MotorDueReminder, Integer> {
}

