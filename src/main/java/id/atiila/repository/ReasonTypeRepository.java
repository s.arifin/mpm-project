package id.atiila.repository;

import id.atiila.domain.ReasonType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ReasonType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ReasonTypeRepository extends JpaRepository<ReasonType, Integer> {
}

