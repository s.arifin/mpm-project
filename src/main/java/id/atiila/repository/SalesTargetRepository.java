package id.atiila.repository;

import id.atiila.domain.Internal;
import id.atiila.domain.SalesTarget;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.UUID;


/**
 * Spring Data JPA repository for the SalesTarget entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SalesTargetRepository extends JpaRepository<SalesTarget, UUID> {

    @Query("select r from SalesTarget r where r.internal= :internal")
    Page<SalesTarget> findAllActive(@Param("internal") String idInternal, Pageable pageable);

}
