package id.atiila.repository;

import id.atiila.domain.CalendarType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CalendarType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface CalendarTypeRepository extends JpaRepository<CalendarType, Integer> {


}

