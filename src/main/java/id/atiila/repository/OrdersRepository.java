package id.atiila.repository;

import id.atiila.domain.Orders;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the Orders entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface OrdersRepository extends JpaRepository<Orders, UUID> {


    @Query("select distinct orders from Orders orders")
    List<Orders> findAllWithEagerRelationships();

    @Query("select orders from Orders orders where orders.idOrder = :id")
    Orders findOneWithEagerRelationships(@Param("id") UUID id);
    
    @Query("select r from Orders r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<Orders> findActiveOrders(Pageable pageable);
    
}

