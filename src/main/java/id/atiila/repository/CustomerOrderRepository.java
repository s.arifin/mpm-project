package id.atiila.repository;

import id.atiila.domain.CustomerOrder;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the CustomerOrder entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface CustomerOrderRepository extends JpaRepository<CustomerOrder, UUID> {

    @Query("select r from CustomerOrder r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<CustomerOrder> findActiveCustomerOrder(Pageable pageable);

    @Query("select r from CustomerOrder r "
	    + " where ((r.internal.idInternal = :idInternal) or (:idInternal is null)) "
	    + " and ((r.customer.idCustomer = :idCustomer) or (:idCustomer is null)) "
	    + " and ((r.billTo.idBillTo = :idBillTo) or (:idBillTo is null)) "
)
    Page<CustomerOrder> findByParams(@Param("idInternal") String idInternal, @Param("idCustomer") String idCustomer, @Param("idBillTo") String idBillTo, Pageable pageable);

}

