package id.atiila.repository;

import id.atiila.domain.BillingItemType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the BillingItemType entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface BillingItemTypeRepository extends JpaRepository<BillingItemType, Integer> {

	@Query("select r from BillingItemType r where (1=2)")
    Page<BillingItemType> queryNothing(Pageable pageable); 
}

