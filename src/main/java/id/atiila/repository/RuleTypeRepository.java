package id.atiila.repository;

import id.atiila.domain.RuleType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RuleType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface RuleTypeRepository extends JpaRepository<RuleType, Integer> {
}

