package id.atiila.repository;

import id.atiila.domain.Good;
import id.atiila.domain.GoodContainer;

import java.util.List;
import java.util.UUID;

import id.atiila.domain.Organization;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the GoodContainer entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface GoodContainerRepository extends JpaRepository<GoodContainer, UUID> {

    @Query("select r from GoodContainer r where (r.good.idProduct = :idGood)")
    Page<GoodContainer> findByIdGood(@Param("idGood") String idGood, Pageable pageable);

    @Query("select r from GoodContainer r where (r.container.idContainer = :idContainer)")
    Page<GoodContainer> findByIdContainer(@Param("idContainer") UUID idContainer, Pageable pageable);

    @Query("select r from GoodContainer r where (r.organization.idParty = :idOrganization)")
    Page<GoodContainer> findByIdOrganization(@Param("idOrganization") UUID idOrganization, Pageable pageable);

    @Query("select r from GoodContainer r where (r.good = :good) and (r.organization = :organization) " +
        "      and (current_timestamp between r.dateFrom and r.dateThru)")
    List<GoodContainer> getContainer(@Param("organization") Organization organization, @Param("good") Good good);

    @Query("select r from GoodContainer r where (r.good.idProduct = :idProduct) and (r.organization = :organization) " +
        "      and (current_timestamp between r.dateFrom and r.dateThru)")
    List<GoodContainer> getContainer(@Param("organization") Organization organization, @Param("idProduct") String idProduct);

}

