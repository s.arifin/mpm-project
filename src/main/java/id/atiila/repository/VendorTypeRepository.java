package id.atiila.repository;

import id.atiila.domain.VendorType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the VendorType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface VendorTypeRepository extends JpaRepository<VendorType, Integer> {

    VendorType findOneByDescription(String description);
}

