package id.atiila.repository;

import id.atiila.domain.Party;
import id.atiila.domain.PartyContactMechanism;
import java.util.UUID;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PartyContactMechanism entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface PartyContactMechanismRepository extends JpaRepository<PartyContactMechanism, Long> {

    @Query("select r from PartyContactMechanism r where (r.party = :party) and (r.idPurposeType = :idPurposeType) ")
    PartyContactMechanism getContactMechanism(@Param("party") Party party, @Param("idPurposeType") Integer idPurposeType);
}

