package id.atiila.repository;

import id.atiila.domain.ReturnSalesOrder;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ReturnSalesOrder entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ReturnSalesOrderRepository extends JpaRepository<ReturnSalesOrder, UUID> {

    @Query("select r from ReturnSalesOrder r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<ReturnSalesOrder> findActiveReturnSalesOrder(Pageable pageable);

    @Query("select r from ReturnSalesOrder r "
	    + " where ((r.internal.idInternal = :idInternal) or (:idInternal is null)) "
	    + " and ((r.customer.idCustomer = :idCustomer) or (:idCustomer is null)) "
	    + " and ((r.billTo.idBillTo = :idBillTo) or (:idBillTo is null)) "
)
    Page<ReturnSalesOrder> findByParams(@Param("idInternal") String idInternal, @Param("idCustomer") String idCustomer, @Param("idBillTo") String idBillTo, Pageable pageable);

}

