package id.atiila.repository;

import id.atiila.domain.DimensionType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the DimensionType entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface DimensionTypeRepository extends JpaRepository<DimensionType, Integer> {

	@Query("select r from DimensionType r where (1=2)")
    Page<DimensionType> queryNothing(Pageable pageable); 
}

