package id.atiila.repository;

import id.atiila.domain.ShipmentOutgoing;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the ShipmentOutgoing entity.
 * atiila consulting
 */

@SuppressWarnings("unused")
@Repository
public interface UnitShipmentOutgoingRepository extends JpaRepository<ShipmentOutgoing, UUID> {

    @Query("select r from ShipmentOutgoing r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11, 12))")
    Page<ShipmentOutgoing> findActiveUnitShipmentOutgoing(Pageable pageable);

    @Query("select r from ShipmentOutgoing r where (r.shipmentType.idShipmentType = :idShipmentType)")
    Page<ShipmentOutgoing> queryByIdShipmentType(@Param("idShipmentType") Integer idShipmentType, Pageable pageable);

    @Query("select r from ShipmentOutgoing r where (r.shipFrom.idShipTo = :idShipFrom)")
    Page<ShipmentOutgoing> queryByIdShipFrom(@Param("idShipFrom") String idShipFrom, Pageable pageable);

    @Query("select r from ShipmentOutgoing r where (r.shipTo.idShipTo = :idShipTo)")
    Page<ShipmentOutgoing> queryByIdShipTo(@Param("idShipTo") String idShipTo, Pageable pageable);

    @Query("select r from ShipmentOutgoing r where (r.addressFrom.idContact = :idAddressFrom)")
    Page<ShipmentOutgoing> queryByIdAddressFrom(@Param("idAddressFrom") UUID idAddressFrom, Pageable pageable);

    @Query("select r from ShipmentOutgoing r where (r.addressTo.idContact = :idAddressTo)")
    Page<ShipmentOutgoing> queryByIdAddressTo(@Param("idAddressTo") UUID idAddressTo, Pageable pageable);

	@Query("select r from ShipmentOutgoing r where (1=2)")
    Page<ShipmentOutgoing> queryNothing(Pageable pageable);
}

