package id.atiila.repository;

import id.atiila.domain.RoleType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RoleType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface RoleTypeRepository extends JpaRepository<RoleType, Integer> {


}
