package id.atiila.repository;

import id.atiila.domain.Party;
import id.atiila.domain.PersonalCustomer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Set;
import java.util.UUID;


/**
 * Spring Data JPA repository for the PersonalCustomer entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface PersonalCustomerRepository extends CustomerRepository<PersonalCustomer> {

    @Query("select r from PersonalCustomer r where (r.idCustomer like '%/____')")
    Set<PersonalCustomer> queryWrongData();

    @Query("select distinct r from PersonalCustomer r where (r.party.idParty = :idParty)")
    PersonalCustomer findByParty(@Param("idParty") UUID idParty);

    @Query("select r from PersonalCustomer r where (r.idCustomer = :idCustomer)")
    PersonalCustomer findByIdCustomer(@Param("idCustomer") String idCustomer);

    @Query("select r from PersonalCustomer r where (r.idMPM = :idMPM)")
    List<PersonalCustomer> findByIdMPM(@Param("idMPM") String idCustomer);

    @Query("select r from PersonalCustomer r where (1=2)")
    Page<PersonalCustomer> queryNone(Pageable pageable);

}

