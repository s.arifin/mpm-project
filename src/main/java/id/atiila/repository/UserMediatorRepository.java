package id.atiila.repository;

import id.atiila.domain.Internal;
import id.atiila.domain.Person;
import id.atiila.domain.UserMediator;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the UserMediator entity.
 * BeSmart Team
 */@SuppressWarnings("unused")
@Repository
public interface UserMediatorRepository extends JpaRepository<UserMediator, UUID> {
    @Query("select r from UserMediator r join r.statuses s where (current_timestamp between s.dateFrom and s.dateThru) and (s.idStatusType in (10, 11))")
    Page<UserMediator> findActiveUserMediator(Pageable pageable);

    @Query("select r from UserMediator r " +
        "   where r.person = :person and r.internal =:internal")
    List<UserMediator> findByPersonAndInternal (@Param("person")Person person, @Param("internal")Internal internal);

    @Query("select r from UserMediator r " +
        "   where r.email = :email and r.internal =:internal")
    List<UserMediator> findByEmailAndInternal (@Param("email")String email, @Param("internal")Internal internal);

    @Query("select r from UserMediator r " +
        "   where r.userName = :username and r.internal =:internal")
    List<UserMediator> findByUsernameAndInternal (@Param("username")String username, @Param("internal")Internal internal);

    UserMediator findByUserName(String userName);

}

