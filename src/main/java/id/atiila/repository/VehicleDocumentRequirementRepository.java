package id.atiila.repository;

import id.atiila.domain.*;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import id.atiila.service.dto.CustomRequirementDTO;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import id.atiila.service.dto.SummaryBastDTO;

/**
 * Spring Data JPA repository for the VehicleDocumentRequirement entity.
 * BeSmart Team
 */

@Repository
public interface VehicleDocumentRequirementRepository extends JpaRepository<VehicleDocumentRequirement, UUID> {

    @Query("select r from VehicleDocumentRequirement r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (13, 17))" +
        "      and (r.internal.idInternal = :internal)")
    Page<VehicleDocumentRequirement> findActiveVehicleDocumentRequirement(@Param("internal") String idInternal, Pageable pageable);

    @Query("select new id.atiila.service.dto.SummaryBastDTO(r.submissionNo) " +
        "   from VehicleDocumentRequirement r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "    and (r.submissionNo is not null)" +
        "    and (r.internal.idInternal = :internal)" +
        "    group by r.submissionNo")
    Page<SummaryBastDTO> queryBast(@Param("internal") String idInternal, Pageable pageable);

    @Query("select r from VehicleDocumentRequirement r " +
        "     join r.statuses s " +
        "    where (s.idStatusType = :idStatusType) " +
        "      and (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and r.internal.idInternal = :internal")
    Page<VehicleDocumentRequirement> findActiveByStatusVehicleDocumentRequirement(Pageable pageable, @Param("idStatusType") Integer idStatusType, @Param("internal") String idInternal);

    @Query("select r from VehicleDocumentRequirement r " +
        "     join r.statuses s " +
        "    where (s.idStatusType = :idStatusType) " +
        "      and (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and r.internal.idInternal = :internal")
    Page<VehicleDocumentRequirement> findActiveByStatusKOnfirmasiSur(Pageable pageable, @Param("idStatusType") Integer idStatusType, @Param("internal") String idInternal);

    @Query("select r from VehicleDocumentRequirement r " +
        "     join r.statuses s join r.saleType " +
        "    where (s.idStatusType = :idStatusType) " +
        "      and (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (r.saleType.idSaleType in (12, 13))")
    Page<VehicleDocumentRequirement> findActiveByStatusVehicleDocumentRequirementOffTheRoad(Pageable pageable, @Param("idStatusType") Integer idStatusType);

    @Query("select r from VehicleDocumentRequirement r " +
        "     join r.orderItem oi " +
        "     join oi.billingItems ob join ob.billingItem bi " +
        "     join oi.shipmentItems os join os.shipmentItem si " +
        "     join bi.billing b " +
        "     join si.shipment s " +
        "     join r.statuses st " +
        "    where (current_timestamp between st.dateFrom and st.dateThru) " +
        "      and (st.idStatusType not in (13, 17)) " +
        "      and ((:dateIVU is null) or (b.dateCreate = :dateIVU))" +
        " "  )
    Page<VehicleDocumentRequirement> findActiveVehicleDocumentRequirementByParam(@Param("dateIVU") ZonedDateTime dateIVU, Pageable pageable);

    @Query("select new id.atiila.service.dto.CustomRequirementDTO(v, r) " +
        "    from VehicleDocumentRequirement r " +
        "    join VehicleSalesOrder v on (v = r.orderItem.orders)  " +
        "    join CommunicationEventCDB cdb on (cdb.idCustomer = v.customer.idCustomer) " +
        "    join cdb.statuses cdbstatus " +
        "    join r.statuses st " +
        "   where (st.idStatusType = :idStatusType) " +
        "     and (r.waitStnk = false) " +
        "     and (current_timestamp between st.dateFrom and st.dateThru) "+
        "     and (cdbstatus.idStatusType not in (13, 17)) " +
        "     and (current_timestamp between cdbstatus.dateFrom and cdbstatus.dateThru) "+
        "     and (r.internal.idInternal = :internal) ")
    Page<CustomRequirementDTO> findCustomActiveByStatusVehicleDocumentRequirement(Pageable pageable, @Param("idStatusType") Integer idStatusType, @Param("internal") String idInternal);

    @Query("select new id.atiila.service.dto.CustomRequirementDTO(v, r) " +
        "     from VehicleDocumentRequirement r " +
        "     join VehicleSalesOrder v on (v = r.orderItem.orders)  " +
        "     left join CommunicationEventCDB cdb on (cdb.idCustomer = v.customer.idCustomer)" +
        "     join cdb.statuses cdbstatus " +
        "     join r.statuses st " +
        "    where (st.idStatusType = :idStatusType) " +
        "      and (r.waitStnk = true or cdb.idCustomer is null ) " +
        "      and (current_timestamp between st.dateFrom and st.dateThru) "+
        "      and (cdbstatus.idStatusType not in (13, 17)) " +
        "      and (current_timestamp between cdbstatus.dateFrom and cdbstatus.dateThru) "+
        "      and (r.internal.idInternal = :internal) " )
    Page<CustomRequirementDTO> findCustomActiveByStatusKonfirmasiSur(Pageable pageable,
                                                                     @Param("idStatusType") Integer idStatusType,
                                                                     @Param("internal") String idInternal);

    @Query("select new id.atiila.service.dto.CustomRequirementDTO(v, r) " +
        "    from VehicleDocumentRequirement r " +
        "    join VehicleSalesOrder v on (v = r.orderItem.orders)  " +
        "    join r.statuses st " +
        "    where (st.idStatusType = :idStatusType) " +
        "      and (current_timestamp between st.dateFrom and st.dateThru) "+
        "      and (r.internal.idInternal = :internal) " )
    Page<CustomRequirementDTO> queryCustomActivePenerimaanStnkBpkb(Pageable pageable,
                                                                   @Param("idStatusType") Integer idStatusType,
                                                                   @Param("internal") String idInternal);

    @Query("select new id.atiila.service.dto.CustomRequirementDTO(v, r) " +
        "    from VehicleDocumentRequirement r " +
        "    join VehicleSalesOrder v on (v = r.orderItem.orders)  " +
        "    join r.statuses st " +
        "    where r.idRequirement = :idReg " )
    CustomRequirementDTO findCustomOne(@Param("idReg") UUID idReg);

    @Query("select r from VehicleDocumentRequirement r where (r.idRequirement in :idReq)")
    VehicleDocumentRequirement findBymanyReq(@Param("idReq") UUID idReq);

    @Query("select r from VehicleDocumentRequirement r  join r.statuses st where (r.orderItem = :orderItem) and st.idStatusType <> 13 ")
    VehicleDocumentRequirement findByOrderItem(@Param("orderItem") OrderItem o);

    @Query("select r from VehicleDocumentRequirement r join r.statuses st " +
        "where (r.vehicle.idMachine = :idMachine) " +
        "and (r.vehicle.idFrame = :idFrame) " +
        "and st.idStatusType <> 13  " +
        "and (current_timestamp between st.dateFrom and st.dateThru) ")
    VehicleDocumentRequirement findOneByMachineAndFrame(@Param("idMachine") String idMachine, @Param("idFrame") String idFrame);

    @Query("select r from VehicleDocumentRequirement r " +
        " join r.statuses st " +
        " where (r.vehicle.idFrame = :id) " +
        " and st.idStatusType <> 13  " +
        " and (current_timestamp between st.dateFrom and st.dateThru)")
    VehicleDocumentRequirement findOnebyidFrame(@Param("id") String id);

    @Query("select r from VehicleDocumentRequirement r join r.statuses st where (r.registrationNumber = :registrationNumber) and st.idStatusType <> 13 ")
    List<VehicleDocumentRequirement> findByRegistrationNumber(@Param("registrationNumber") String registrationNumber);

    @Query(" select r from OrderBillingItem obi join obi.billingItem r join obi.orderItem oi " +
        "   where (oi.idOrderItem = :idOrderItem) and r.idItemType = 10 ")
    List<BillingItem> findBillingContainItem(@Param("idOrderItem") UUID idOrderItem);


    @Query("select r " +
        "    from VehicleDocumentRequirement r " +
        "    join r.statuses s " +
        "    where (s.idStatusType = :idStatusType) " +
        "      and (r.waitStnk = true) " +
        "      and (current_timestamp between s.dateFrom and s.dateThru) "+
        "      and (r.internal.idInternal = :internal) " )
    Page<VehicleDocumentRequirement> queryVDRByStatus(@Param("idStatusType") Integer idStatusType,
                                                      @Param("internal") String idInternal,
                                                      Pageable pageable);
    @Query("select r " +
        "    from VehicleDocumentRequirement r " +
        "    join r.statuses s " +
        "   where (s.idStatusType = :idStatusType) " +
        "     and (r.waitStnk = false) " +
        "     and (current_timestamp between s.dateFrom and s.dateThru) "+
        "     and (r.internal.idInternal = :internal)" )
    Page<VehicleDocumentRequirement> queryActiveByStatusVehicleDocumentRequirement(@Param("idStatusType") Integer idStatusType,
                                                                                   @Param("internal") String idInternal,
                                                                                   Pageable pageable);

    @Query("select r from VehicleDocumentRequirement r where (1=2)")
    Page<VehicleDocumentRequirement> queryNothing(Pageable pageable);

    @Query("select r " +
        "    from VehicleDocumentRequirement r " +
        "    join r.statuses s " +
        "   where (s.idStatusType = :status) " +
        "     and (current_timestamp between s.dateFrom and s.dateThru) "+
        "     and (r.internal.idInternal = :idInternal)" )
    Page<VehicleDocumentRequirement> queryByInternalStatus(@Param("idInternal") String idInternal, @Param("status") Integer status, Pageable pageable);

    @Query("select r from VehicleDocumentRequirement r join r.statuses s " +
        "    where (current_timestamp between s.dateFrom and s.dateThru) " +
        "      and (s.idStatusType not in (:idStatus))" +
        "      and (r.internal.idInternal = :internal)")
    List<VehicleDocumentRequirement> queryByStatus(@Param("idStatus") Integer idStatus);
}

