package id.atiila.repository;

import id.atiila.domain.District;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the District entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface DistrictRepository extends JpaRepository<District, UUID> {

    District findByGeoCode(String id);

    List<District> findOneByDescription(String description);
}

