package id.atiila.repository;

import id.atiila.domain.ServiceReminder;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ServiceReminder entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface ServiceReminderRepository extends JpaRepository<ServiceReminder, Integer> {
}

