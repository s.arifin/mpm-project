package id.atiila.repository;

import id.atiila.domain.CommunicationEventDelivery;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.UUID;


/**
 * Spring Data JPA repository for the CommunicationEventDelivery entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface CommunicationEventDeliveryRepository extends JpaRepository<CommunicationEventDelivery, UUID> {

}
