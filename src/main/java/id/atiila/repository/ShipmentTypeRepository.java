package id.atiila.repository;

import id.atiila.domain.ShipmentType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ShipmentType entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface ShipmentTypeRepository extends JpaRepository<ShipmentType, Integer> {


}

