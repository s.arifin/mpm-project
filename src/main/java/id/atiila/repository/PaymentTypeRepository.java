package id.atiila.repository;

import id.atiila.domain.PaymentType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the PaymentType entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface PaymentTypeRepository extends JpaRepository<PaymentType, Integer> {

    @Query(" select r from PaymentType r where r.idPaymentType = :idPaytype")
    Page<PaymentType> findByIdPaymentType (@Param("idPaytype") Integer idPaytype, Pageable pageable);
}

