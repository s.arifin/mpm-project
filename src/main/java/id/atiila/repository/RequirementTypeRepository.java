package id.atiila.repository;

import id.atiila.domain.RequirementType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RequirementType entity.
 * BeSmart Team
 */
@SuppressWarnings("unused")
@Repository
public interface RequirementTypeRepository extends JpaRepository<RequirementType, Integer> {


}

