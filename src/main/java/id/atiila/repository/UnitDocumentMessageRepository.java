package id.atiila.repository;

import id.atiila.domain.UnitDocumentMessage;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

import id.atiila.service.dto.CustomUnitDocumentMessageDTO;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the UnitDocumentMessage entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface UnitDocumentMessageRepository extends JpaRepository<UnitDocumentMessage, Integer> {

    @Query("select r from UnitDocumentMessage r "
	    + " where ((r.parent.idMessage = :idMessage) or (:idMessage is null)) "
)
    Page<UnitDocumentMessage> findByParams(@Param("idMessage") String idMessage, Pageable pageable);

    List<UnitDocumentMessage> findAllByIdRequirement(UUID idRequirement);

    @Query("select r from UnitDocumentMessage r join r.statuses s where " +
        "(current_timestamp between s.dateFrom and s.dateThru) " +
        "and (s.idStatusType = :idstatustype) " +
        "and (r.idRequirement = :idrequirement)")
    List<UnitDocumentMessage> findByRequirementAndApprovalStatus(
        @Param("idrequirement") UUID idrequirement,
        @Param("idstatustype") Integer idstatustype
    );

    @Query("select r from UnitDocumentMessage r join r.statuses s where " +
        "(s.idStatusType = :idstatustype)")
    List<UnitDocumentMessage> findByStatusApproval(
      @Param("idstatustype") Integer idstatustype
    );

    @Query("select new id.atiila.service.dto.CustomUnitDocumentMessageDTO(r.idMessage, r.content, r.dateCreated, sur.idRequirement, " +
        "   sur.requirementNumber, p.name, p.idProduct, p.description, f.refKey) " +
        "   from UnitDocumentMessage r join r.statuses s " +
        "   join SalesUnitRequirement sur on r.idRequirement = sur.idRequirement " +
        "   join Product p on p.idProduct = sur.product.idProduct " +
        "   join Feature f on sur.color.idFeature = f.idFeature" +
        "   where s.idStatusType in (10) " +
        "   and r.dateCreated between :startDate and :endDate " +
        "   and current_timestamp between s.dateFrom and s.dateThru" +
        "   and sur.internal.idInternal = :idInternal")
    Page<CustomUnitDocumentMessageDTO> findByStatusJoinRequirement(
        @Param("idInternal") String idInternal,
        @Param("startDate") ZonedDateTime startDate,
        @Param("endDate") ZonedDateTime endDate, Pageable pageable);

    @Query("select new id.atiila.service.dto.CustomUnitDocumentMessageDTO(r.idMessage, r.content, r.dateCreated, sur.idRequirement, " +
        "   sur.requirementNumber, p.name, p.idProduct, p.description, f.refKey) " +
        "   from UnitDocumentMessage r join r.statuses s " +
        "   join SalesUnitRequirement sur on r.idRequirement = sur.idRequirement " +
        "   join Product p on p.idProduct = sur.product.idProduct " +
        "   join Feature f on sur.color.idFeature = f.idFeature" +
        "   where s.idStatusType in (11, 12) " +
        "   and current_timestamp between s.dateFrom and s.dateThru" +
        "   and sur.internal.idInternal = :idInternal")
    Page<CustomUnitDocumentMessageDTO> findByStatusJoinRequirementPending(
        @Param("idInternal") String idInternal, Pageable pageable);

    @Query("select new id.atiila.service.dto.CustomUnitDocumentMessageDTO(r.idMessage, r.content, r.dateCreated, sur.idRequirement, " +
        "   sur.requirementNumber, p.name, p.idProduct, p.description, f.refKey) " +
        "   from UnitDocumentMessage r join r.statuses s " +
        "   join SalesUnitRequirement sur on r.idRequirement = sur.idRequirement " +
        "   join Product p on p.idProduct = sur.product.idProduct " +
        "   join Feature f on sur.color.idFeature = f.idFeature" +
        "   where s.idStatusType in (14, 16, 17, 20) " +
        "   and current_timestamp between s.dateFrom and s.dateThru" +
        "   and sur.internal.idInternal = :idInternal")
    Page<CustomUnitDocumentMessageDTO> findByStatusJoinRequirementApprove(
        @Param("idInternal") String idInternal, Pageable pageable);

    @Query("select new id.atiila.service.dto.CustomUnitDocumentMessageDTO(r.idMessage, r.content, r.dateCreated, sur.idRequirement, " +
        "   sur.requirementNumber, p.name, p.idProduct, p.description, f.refKey) " +
        "   from UnitDocumentMessage r join r.statuses s " +
        "   join SalesUnitRequirement sur on r.idRequirement = sur.idRequirement " +
        "   join Product p on p.idProduct = sur.product.idProduct " +
        "   join Feature f on sur.color.idFeature = f.idFeature " +
        "   where s.idStatusType in (62, 15) " +
        "   and current_timestamp between s.dateFrom and s.dateThru" +
        "   and sur.internal.idInternal = :idInternal")
    Page<CustomUnitDocumentMessageDTO> findByStatusJoinRequirementNotApprove(
        @Param("idInternal") String idInternal, Pageable pageable);

    @Query("select new id.atiila.service.dto.CustomUnitDocumentMessageDTO(r.idMessage, r.content, r.dateCreated, sur.idRequirement, " +
        "   sur.requirementNumber, p.name, p.idProduct, p.description, f.refKey) " +
        "   from UnitDocumentMessage r join r.statuses s " +
        "   join SalesUnitRequirement sur on r.idRequirement = sur.idRequirement " +
        "   join Product p on p.idProduct = sur.product.idProduct " +
        "   join Feature f on sur.color.idFeature = f.idFeature " +
        "   where s.idStatusType in (69) " +
        "   and current_timestamp between s.dateFrom and s.dateThru" +
        "   and sur.internal.idInternal = :idInternal")
    Page<CustomUnitDocumentMessageDTO> findByStatusJoinRequirementNotAvailable(
        @Param("idInternal") String idInternal, Pageable pageable);

    @Query("select r from UnitDocumentMessage r " +
        "   join r.statuses s " +
        "   where (current_timestamp between s.dateFrom and s.dateThru)" +
        "   and s.idStatusType not in (62)" +
        "   and r.idRequirement =:idReq ")
    Page<UnitDocumentMessage> findByIdReq(@Param("idReq") UUID idReq, Pageable pageable);

}

