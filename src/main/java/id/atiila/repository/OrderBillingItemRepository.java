package id.atiila.repository;

import id.atiila.domain.BillingItem;
import id.atiila.domain.OrderBillingItem;

import java.util.List;
import java.util.UUID;

import id.atiila.domain.OrderItem;
import id.atiila.domain.OrderShipmentItem;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data JPA repository for the OrderBillingItem entity.
 * BeSmart Team
 */

@SuppressWarnings("unused")
@Repository
public interface OrderBillingItemRepository extends JpaRepository<OrderBillingItem, UUID> {

    @Query("select r from OrderBillingItem r "
	    + " where ((r.orderItem.idOrderItem = :idOrderItem) or (:idOrderItem is null)) "
	    + " and ((r.billingItem.idBillingItem = :idBillingItem) or (:idBillingItem is null)) "
)
    Page<OrderBillingItem> findByParams(@Param("idOrderItem") UUID idOrderItem,
                                        @Param("idBillingItem") UUID idBillingItem,
                                        Pageable pageable);


    @Query(value = "select obi from OrderBillingItem obi " +
                   "  join obi.billingItem bi " +
                   "  join bi.billing b " +
                   " where b.idBilling = :idBilling ")
    List<OrderBillingItem> findByIdBilling(@Param("idBilling") UUID idBilling);

    @Modifying
    @Transactional
    @Query("delete from OrderBillingItem e where e.idOrderBillingItem = ?1")
    void deleteOrderBillingItem(UUID id);

    @Query("select r from OrderBillingItem r where r.orderItem = :orderItem")
    List<OrderBillingItem> queryByOrderItem(@Param("orderItem") OrderItem orderItem);

    @Query("select r from OrderBillingItem r where (r.orderItem = :orderItem) and (r.billingItem = :billingItem) ")
    OrderBillingItem getBillingOrderItem(@Param("orderItem") OrderItem orderItem, @Param("billingItem") BillingItem billingItem);

    @Query("select r from OrderBillingItem r where (r.orderItem = :orderItem)")
    List<OrderBillingItem> getBillingItems(@Param("orderItem") OrderItem orderItem);

    @Query("select r from OrderBillingItem r where r.orderItem.idOrderItem = :idOrderItem")
    List<OrderBillingItem> queryByOrderItem(@Param("idOrderItem") UUID idOrderItem);

    @Query("select r from OrderBillingItem r " +
        "     join BillingDisbursement b on (r.billingItem.billing = b)" +
        "    where (b.vendorInvoice = :vendorInvoice) " +
        "      and (r.billingItem.idProduct = :idProduct) " +
        "      and (r.billingItem.idFeature = :idFeature) ")
    List<OrderBillingItem> queryByInvoiceProductType(@Param("vendorInvoice") String vendorInvoice,
                                                @Param("idProduct") String idProduct,
                                                @Param("idFeature") Integer idFeature);

}

