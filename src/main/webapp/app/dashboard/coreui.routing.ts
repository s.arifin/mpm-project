import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// Layouts
import {FullLayoutComponent} from './';

export const routes: Routes = [
  {
    path: 'dashboardd',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'homelayout',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes)],
  exports: [ RouterModule ]
})
export class CoreUIRoutingModule {}
