import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {VendorProduct} from './vendor-product.model';
import {VendorProductPopupService} from './vendor-product-popup.service';
import {VendorProductService} from './vendor-product.service';
import {ToasterService} from '../../shared';
import { Product, ProductService } from '../product';
import { Internal, InternalService } from '../internal';
import { Vendor, VendorService } from '../vendor';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-vendor-product-dialog',
    templateUrl: './vendor-product-dialog.component.html'
})
export class VendorProductDialogComponent implements OnInit {

    vendorProduct: VendorProduct;
    isSaving: boolean;

    products: Product[];

    internals: Internal[];

    vendors: Vendor[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private vendorProductService: VendorProductService,
        private productService: ProductService,
        private internalService: InternalService,
        private vendorService: VendorService,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.vendorService.query()
            .subscribe((res: ResponseWrapper) => { this.vendors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.vendorProduct.idVendorProduct !== undefined) {
            this.subscribeToSaveResponse(
                this.vendorProductService.update(this.vendorProduct));
        } else {
            this.subscribeToSaveResponse(
                this.vendorProductService.create(this.vendorProduct));
        }
    }

    private subscribeToSaveResponse(result: Observable<VendorProduct>) {
        result.subscribe((res: VendorProduct) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: VendorProduct) {
        this.eventManager.broadcast({ name: 'vendorProductListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vendorProduct saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'vendorProduct Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackProductById(index: number, item: Product) {
        return item.idProduct;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackVendorById(index: number, item: Vendor) {
        return item.idVendor;
    }
}

@Component({
    selector: 'jhi-vendor-product-popup',
    template: ''
})
export class VendorProductPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vendorProductPopupService: VendorProductPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vendorProductPopupService
                    .open(VendorProductDialogComponent as Component, params['id']);
            } else {
                this.vendorProductPopupService
                    .open(VendorProductDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
