import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { JhiDateUtils } from 'ng-jhipster';

import { VendorProduct } from './vendor-product.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class VendorProductService {
    private itemValues: VendorProduct[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = 'api/vendor-products';
    private resourceSearchUrl = 'api/_search/vendor-products';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(vendorProduct: VendorProduct): Observable<VendorProduct> {
        const copy = this.convert(vendorProduct);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(vendorProduct: VendorProduct): Observable<VendorProduct> {
        const copy = this.convert(vendorProduct);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<VendorProduct> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, vendorProduct: VendorProduct): Observable<VendorProduct> {
        const copy = this.convert(vendorProduct);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, vendorProducts: VendorProduct[]): Observable<VendorProduct[]> {
        const copy = this.convertList(vendorProducts);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        if (entity.dateFrom) {
            entity.dateFrom = new Date(entity.dateFrom);
        }
        if (entity.dateThru) {
            entity.dateThru = new Date(entity.dateThru);
        }
    }

    private convert(vendorProduct: VendorProduct): VendorProduct {
        if (vendorProduct === null || vendorProduct === {}) {
            return {};
        }
        // const copy: VendorProduct = Object.assign({}, vendorProduct);
        const copy: VendorProduct = JSON.parse(JSON.stringify(vendorProduct));

        // copy.dateFrom = this.dateUtils.toDate(vendorProduct.dateFrom);

        // copy.dateThru = this.dateUtils.toDate(vendorProduct.dateThru);
        return copy;
    }

    private convertList(vendorProducts: VendorProduct[]): VendorProduct[] {
        const copy: VendorProduct[] = vendorProducts;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: VendorProduct[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
