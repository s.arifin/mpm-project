import { BaseEntity } from './../../shared';

export class VendorProduct implements BaseEntity {
    constructor(
        public id?: any,
        public idVendorProduct?: any,
        public orderRatio?: number,
        public dateFrom?: any,
        public dateThru?: any,
        public productId?: any,
        public internalId?: any,
        public vendorId?: any,
    ) {
    }
}
