import {Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Motor, MotorService } from '../motor';
import { ProductDocument, ProductDocumentService } from '../product-document';
import { PriceComponentService } from '../price-component/price-component.service';
import { Feature, FeatureService } from '../feature';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService, CommonUtilService} from '../../shared';
import * as FeatureTypeConstants from '../../shared/constants/feature-type.constants';
import * as _ from 'lodash';
import { ProspectPerson } from '../prospect-person/prospect-person.model';
import { ProspectPersonService } from '../prospect-person/prospect-person.service';
@Component({
    selector: 'jhi-motor-catalog-detail',
    templateUrl: './motor-catalog-detail.component.html'
})

export class MotorCatalogDetailComponent implements OnInit {
    public motor: Motor;
    prospectPerson: ProspectPerson;
    public images: any[];
    public price: number;
    printModal: boolean;
    public colors: Feature[];
    newProspect: Boolean;
    protected productDocuments: ProductDocument[];
    protected idproduct: string;
    protected objQueryProductDocument: any = {};
    name: String = null;
    cellPhone: String = null;
    nik: String = null;
    findProspect: Boolean = false;
    findPerson: Boolean = false;
    findNik: Boolean = false;

    constructor(
        protected priceComponentService: PriceComponentService,
        protected motorService: MotorService,
        protected prospectPersonService: ProspectPersonService,
        protected router: Router,
        protected route: ActivatedRoute,
        protected commonUtilService: CommonUtilService,
        protected featureService: FeatureService,
        protected principal: Principal,
        protected productDocumentService: ProductDocumentService,
    ) {
        this.prospectPerson = new ProspectPerson();
        this.images = [];
        this.colors = new Array<Feature>();
    }

    ngOnInit() {
        this.motor = new Motor();
        this.route.params.subscribe(
            (params) => {
                if (params['idproduct']) {
                    this.idproduct = params['idproduct'];
                    this.loadGallery();
                    this.loadMotor();
                    this.getMotorPrice();
                }
            }
        )
    }

    protected getMotorPrice(): void {
        const obj = {
            idproduct : this.idproduct,
            idinternal : null
        };

        this.motorService.getPrice(obj).subscribe(
            (res) => {
                this.price = this.priceComponentService.countMotorPrice(res.json);
            }
        )
    }

    protected loadMotor(): void {
        this.motorService.find(this.idproduct).subscribe(
            (res) => {
                this.motor = res;
                this.colors = this.featureService.filterFeatureByTypeId(res.features, FeatureTypeConstants.COLOR);
            }
        )
    }

    protected loadGallery(): void {
        this.objQueryProductDocument.idProduct = this.idproduct;
        this.objQueryProductDocument.page = 0;
        this.objQueryProductDocument.size = 100;

        this.productDocumentService.findByIdProduct(this.objQueryProductDocument).subscribe(
            (res) => {
                this.productDocuments = res.json;
                if (this.productDocuments.length > 0) {
                    this.setArrayForImage();
                }
            }
        )
    }

    public checkColorLength(): boolean {
        if (this.colors.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    submitBAST() {
        this.prospectPerson = new ProspectPerson();
        this.newProspect = true;
    }

    doPrintBAST() {
        this.newProspect = false;
        this.doCheckProspect()
    }

    doCheckProspect() {
        const checkProspect = new ProspectPerson();

        if (this.name.split(' ').length === 1) {
            checkProspect.person.firstName = this.name;
            checkProspect.person.lastName = '';
        } else {
            checkProspect.person.firstName = this.name.split(' ').slice(0, -1).join(' ');
            checkProspect.person.lastName = this.name.split(' ').slice(-1).join(' ');
        }

        checkProspect.person.cellPhone1 = this.cellPhone;

        this.prospectPersonService.executeProcess(105, this.principal.getIdInternal(), checkProspect).subscribe(
        (response) => {
            // Jika Prpspeck Ditemukan
            if (response.person.getIdInternal != null && response.person.getIdInternal !== undefined) {
                this.prospectPerson = response;
                if (this.prospectPerson.person.lastName == null) {
                    this.name = this.prospectPerson.person.firstName;
                } else {
                    this.name = this.prospectPerson.person.firstName + ' ' + this.prospectPerson.person.lastName;
                }
                this.cellPhone = this.prospectPerson.person.cellPhone1;
                this.findPerson = true;
                this.newProspect = false;
            // Jika Prospect Tidak Ditemukan Namun, Data Orang Ditemukan
            } else if (response.idProspect != null) {
                    this.prospectPerson = response;
                    if (this.prospectPerson.person.lastName == null) {
                        this.name = this.prospectPerson.person.firstName;
                    } else {
                        this.name = this.prospectPerson.person.firstName + ' ' + this.prospectPerson.person.lastName;
                    }
                    this.cellPhone = this.prospectPerson.person.cellPhone1;
                    this.findProspect = true;
                    this.newProspect = false;
                } else if (response.idProspect != null) {
                    this.prospectPerson = response;
                    if (this.prospectPerson.person.lastName == null) {
                        this.name = this.prospectPerson.person.firstName;
                    } else {
                        this.name = this.prospectPerson.person.firstName + ' ' + this.prospectPerson.person.lastName;
                    }
                    this.cellPhone = this.prospectPerson.person.cellPhone1;
                    this.findNik = true;
                    this.newProspect = false;
            // Jika Data Tidak Ditemukan
            } else {
                const newProspectData = new Array<ProspectPerson>();
                newProspectData.push(checkProspect);
                this.prospectPersonService.pushItems(newProspectData);
                this.clearOldData();
                this.router.navigate(['../prospect-person-new']);
            }
        },
        (err) => {
            this.commonUtilService.showError(err);
        });
        // () => {
            // const newProspectData = new Array<ProspectPerson>();
            // newProspectData.push(checkProspect);
            // this.prospectPersonService.pushItems(newProspectData);
            // this.clearOldData();
            // this.router.navigate(['../prospect-person-new']);
        // });
    }

    public doFollowUp(): void {
        this.clearOldData();
        this.router.navigate(['../prospect-person/' + this.prospectPerson.idProspect + '/step']);
    }

    public doEdit(): void {
        this.clearOldData();
        this.router.navigate(['../prospect-person/' + this.prospectPerson.idProspect + '/edit']);
    }

    public backToFind(): void {
        this.clearOldData();
        this.newProspect = true;
    }

    clearOldData() {
        this.newProspect = false;
        this.findProspect = false;
        this.findPerson = false;
        this.findNik = false;
        this.name = null;
        this.cellPhone = null;
        this.nik = null;
    }

    public checkImageLength(): boolean {
        if (this.images.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    protected setArrayForImage(): void {
        this.images = [];

        for (let i = 0; i < this.productDocuments.length; i++) {
            const item = this.productDocuments[i];
            if (_.includes(item.contentContentType, 'image')) {
                let obj: any;
                obj = {};

                obj.source = 'data:' + item.contentContentType + ';base64,' + item.content;
                obj.alt = item.note;
                obj.title = 'Image ' + (i + 1);

                this.images.push(obj);
            }
        }
    }

    public previousState(): void {
        this.router.navigate(['motor-catalog']);
    }

    public trackByFeature(index: number, item: Feature): number {
        return item.idFeature;
    }
}
