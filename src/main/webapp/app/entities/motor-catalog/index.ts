export * from './motor-catalog.component';
export * from './motor-catalog.route';
export * from './include/motor-catalog-all.component';
export * from './include/motor-catalog-hot-item.component';
export * from './motor-catalog-detail.component';
export * from './motor-catalog.module';
