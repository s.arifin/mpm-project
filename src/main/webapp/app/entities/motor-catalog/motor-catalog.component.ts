import {Component, OnInit} from '@angular/core';

import { Motor } from '../motor';

@Component({
    selector: 'jhi-motor-catalog',
    templateUrl: './motor-catalog.component.html'
})

export class MotorCatalogComponent implements OnInit {
    motors: Motor[];

    constructor() {

    }

    ngOnInit() {
    }
}
