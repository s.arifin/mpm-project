import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import { MpmSharedModule } from '../../shared';
import { MpmSharedEntityModule } from '../shared-entity.module';
import {
    ButtonModule,
    TabViewModule,
    DataListModule,
    PanelModule,
    GalleriaModule,
    DialogModule,
    ConfirmDialogModule,
} from 'primeng/primeng';

import {
    MotorCatalogAllComponent,
    MotorCatalogHotItemComponent,
    MotorCatalogComponent,
    motorCatalogRoute,
    MotorCatalogResolvePagingParams,
    MotorCatalogDetailComponent
 } from './';

const ENTITY_STATES = [
    ...motorCatalogRoute,
];

@NgModule({
    imports: [
        GalleriaModule,
        PanelModule,
        ButtonModule,
        DialogModule,
        ConfirmDialogModule,
        MpmSharedEntityModule,
        MpmSharedModule,
        DataListModule,
        TabViewModule,
        RouterModule.forChild(ENTITY_STATES),
    ],
    declarations: [
        MotorCatalogDetailComponent,
        MotorCatalogAllComponent,
        MotorCatalogHotItemComponent,
        MotorCatalogComponent
    ],
    entryComponents: [
        MotorCatalogDetailComponent,
        MotorCatalogAllComponent,
        MotorCatalogHotItemComponent,
        MotorCatalogComponent
    ],
    providers: [
        MotorCatalogResolvePagingParams
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMotorCatalogModule {}
