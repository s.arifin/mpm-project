import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ITEMS_PER_PAGE, Principal, ResponseWrapper} from '../../../shared';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { Motor, MotorService } from '../../motor';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { LazyLoadEvent } from 'primeng/primeng';
import { Product } from '../../shared-component/entity/product.model';

@Component({
    selector: 'jhi-motor-catalog-all',
    templateUrl: './motor-catalog-all-or-hot-item.component.html'
})

export class MotorCatalogAllComponent implements OnInit {
    motors: Motor[];
    product: Product;
    itemsPerPage: any;
    currentSearch: string;
    routeData: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    links: any;
    totalItems: any;
    queryCount: any;
    isLazyLoadingFirst: Boolean = false;
    isShow: boolean;
    filtered: any;
    isFiltered: boolean;

    constructor(
        private alertService: JhiAlertService,
        private parseLinks: JhiParseLinks,
        private motorService: MotorService,
        protected principal: Principal,
        private activatedRoute: ActivatedRoute,
        protected loadingService: LoadingService,
        protected router: Router
    ) {
        this.isShow = true;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
    }

    ngOnInit() {
        this.loadAll();
    }

    filter() {
        this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered );
        // this.page = 0;
        this.product = new Product();
        this.product.idProduct = this.principal.getIdInternal();
        this.product.productTypeId = 21;

        this.motorService.query({
            shipmentOutgoingPTO: this.product,
            idInternal: this.principal.getIdInternal(),
            // sort: this.sort()
        })
            .toPromise()
            .then((res) => {
                console.log('filter data isi nama == ', res)
                this.loadingService.loadingStop(),
                this.onSuccessFilter(res.json, res.headers)
            }
        );

        console.log('tanggal', this.product.dateIntroduction);
        console.log('name', this.product.name);
    }

    public refineAll(): void {
        this.isShow = false;
        this.loadAll().then(
            (resolve) => {
                this.isShow = true;
            }
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        if (this.isLazyLoadingFirst === false) {
            this.isLazyLoadingFirst = true;
            return ;
        }
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    private loadAll(): Promise<void> {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.motorService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => {
                        this.loadingService.loadingStop();
                    }
                );
            return;
        }
        return new Promise<void> (
            (resolve) => {
                this.motorService.query({
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    // sort: this.sort()
                }).subscribe(
                        (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json)
                    );
                return;
            }
        )
    }

    protected onSuccessFilter(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.motors = data;
    }

    private sort(): string[] {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idProduct') {
            result.push('idProduct');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.loadingService.loadingStop();
        this.motors = new Array<Motor>();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.motors = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    public trackId(index: number, item: Motor): string {
        return item.idProduct;
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/motor-catalog', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/motor-catalog', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
}
