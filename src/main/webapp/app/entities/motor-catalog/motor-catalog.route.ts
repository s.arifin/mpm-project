import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MotorCatalogComponent } from './motor-catalog.component';
import { MotorCatalogDetailComponent } from './motor-catalog-detail.component';

@Injectable()
export class MotorCatalogResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idProduct,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const motorCatalogRoute: Routes = [
    {
        path: 'motor-catalog',
        component: MotorCatalogComponent,
        resolve: {
            'pagingParams': MotorCatalogResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.motor.catalog.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'motor-catalog/:idproduct/detail',
        component: MotorCatalogDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.motor.catalog.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
