import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { WorkEffortTypeComponent } from './work-effort-type.component';
import { WorkEffortTypeLovPopupComponent } from './work-effort-type-as-lov.component';
import { WorkEffortTypePopupComponent } from './work-effort-type-dialog.component';

@Injectable()
export class WorkEffortTypeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idWeType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const workEffortTypeRoute: Routes = [
    {
        path: 'work-effort-type',
        component: WorkEffortTypeComponent,
        resolve: {
            'pagingParams': WorkEffortTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workEffortType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const workEffortTypePopupRoute: Routes = [
    {
        path: 'work-effort-type-lov',
        component: WorkEffortTypeLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workEffortType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'work-effort-type-new',
        component: WorkEffortTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workEffortType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'work-effort-type/:id/edit',
        component: WorkEffortTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workEffortType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
