import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { WorkEffortType } from './work-effort-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class WorkEffortTypeService {
    private itemValues: WorkEffortType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = 'api/work-effort-types';
    private resourceSearchUrl = 'api/_search/work-effort-types';

    constructor(private http: Http) { }

    create(workEffortType: WorkEffortType): Observable<WorkEffortType> {
        const copy = this.convert(workEffortType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(workEffortType: WorkEffortType): Observable<WorkEffortType> {
        const copy = this.convert(workEffortType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<WorkEffortType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, workEffortType: WorkEffortType): Observable<WorkEffortType> {
        const copy = this.convert(workEffortType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, workEffortTypes: WorkEffortType[]): Observable<WorkEffortType[]> {
        const copy = this.convertList(workEffortTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(workEffortType: WorkEffortType): WorkEffortType {
        if (workEffortType === null || workEffortType === {}) {
            return {};
        }
        // const copy: WorkEffortType = Object.assign({}, workEffortType);
        const copy: WorkEffortType = JSON.parse(JSON.stringify(workEffortType));
        return copy;
    }

    private convertList(workEffortTypes: WorkEffortType[]): WorkEffortType[] {
        const copy: WorkEffortType[] = workEffortTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: WorkEffortType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
