import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {WorkEffortType} from './work-effort-type.model';
import {WorkEffortTypePopupService} from './work-effort-type-popup.service';
import {WorkEffortTypeService} from './work-effort-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-work-effort-type-dialog',
    templateUrl: './work-effort-type-dialog.component.html'
})
export class WorkEffortTypeDialogComponent implements OnInit {

    workEffortType: WorkEffortType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private workEffortTypeService: WorkEffortTypeService,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.workEffortType.idWeType !== undefined) {
            this.subscribeToSaveResponse(
                this.workEffortTypeService.update(this.workEffortType));
        } else {
            this.subscribeToSaveResponse(
                this.workEffortTypeService.create(this.workEffortType));
        }
    }

    private subscribeToSaveResponse(result: Observable<WorkEffortType>) {
        result.subscribe((res: WorkEffortType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: WorkEffortType) {
        this.eventManager.broadcast({ name: 'workEffortTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'workEffortType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'workEffortType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-work-effort-type-popup',
    template: ''
})
export class WorkEffortTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private workEffortTypePopupService: WorkEffortTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.workEffortTypePopupService
                    .open(WorkEffortTypeDialogComponent as Component, params['id']);
            } else {
                this.workEffortTypePopupService
                    .open(WorkEffortTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
