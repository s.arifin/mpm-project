import { BaseEntity } from './../../shared';

export class WorkEffortType implements BaseEntity {
    constructor(
        public id?: number,
        public idWeType?: number,
        public description?: string,
    ) {
    }
}
