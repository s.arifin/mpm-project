import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    MotorDueReminderService,
    MotorDueReminderPopupService,
    MotorDueReminderComponent,
    MotorDueReminderDialogComponent,
    MotorDueReminderPopupComponent,
    motorDueReminderRoute,
    motorDueReminderPopupRoute,
    MotorDueReminderResolvePagingParams,
    MotorDueReminderAsLovComponent,
    MotorDueReminderLovPopupComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule
        } from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...motorDueReminderRoute,
    ...motorDueReminderPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,

        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule
    ],
    exports: [
        MotorDueReminderComponent,
        MotorDueReminderAsLovComponent,
        MotorDueReminderLovPopupComponent,
    ],
    declarations: [
        MotorDueReminderComponent,
        MotorDueReminderDialogComponent,
        MotorDueReminderPopupComponent,
        MotorDueReminderAsLovComponent,
        MotorDueReminderLovPopupComponent,
    ],
    entryComponents: [
        MotorDueReminderComponent,
        MotorDueReminderDialogComponent,
        MotorDueReminderPopupComponent,
        MotorDueReminderAsLovComponent,
        MotorDueReminderLovPopupComponent,
    ],
    providers: [
        MotorDueReminderService,
        MotorDueReminderPopupService,
        MotorDueReminderResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMotorDueReminderModule {}
