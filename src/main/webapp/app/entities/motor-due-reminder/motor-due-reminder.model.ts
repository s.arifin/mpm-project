import { BaseEntity } from './../../shared';

export class MotorDueReminder implements BaseEntity {
    constructor(
        public id?: number,
        public idReminder?: number,
        public service1?: number,
        public service2?: number,
        public service3?: number,
        public service4?: number,
        public service5?: number,
        public motorId?: any,
        public km?: number,
        public ndays?: number,
    ) {
    }
}
