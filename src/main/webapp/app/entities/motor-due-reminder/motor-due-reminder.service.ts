import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { MotorDueReminder } from './motor-due-reminder.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class MotorDueReminderService {
    protected itemValues: MotorDueReminder[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/motor-due-reminders';
    protected resourceSearchUrl = 'api/_search/motor-due-reminders';

    constructor(protected http: Http) { }

    create(motorDueReminder: MotorDueReminder): Observable<MotorDueReminder> {
        const copy = this.convert(motorDueReminder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(motorDueReminder: MotorDueReminder): Observable<MotorDueReminder> {
        const copy = this.convert(motorDueReminder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<MotorDueReminder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, motorDueReminder: MotorDueReminder): Observable<MotorDueReminder> {
        const copy = this.convert(motorDueReminder);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, motorDueReminders: MotorDueReminder[]): Observable<MotorDueReminder[]> {
        const copy = this.convertList(motorDueReminders);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(motorDueReminder: MotorDueReminder): MotorDueReminder {
        if (motorDueReminder === null || motorDueReminder === {}) {
            return {};
        }
        // const copy: MotorDueReminder = Object.assign({}, motorDueReminder);
        const copy: MotorDueReminder = JSON.parse(JSON.stringify(motorDueReminder));
        return copy;
    }

    protected convertList(motorDueReminders: MotorDueReminder[]): MotorDueReminder[] {
        const copy: MotorDueReminder[] = motorDueReminders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: MotorDueReminder[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
