import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MotorDueReminderComponent } from './motor-due-reminder.component';
import { MotorDueReminderLovPopupComponent } from './motor-due-reminder-as-lov.component';
import { MotorDueReminderPopupComponent } from './motor-due-reminder-dialog.component';

@Injectable()
export class MotorDueReminderResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idReminder,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const motorDueReminderRoute: Routes = [
    {
        path: 'motor-due-reminder',
        component: MotorDueReminderComponent,
        resolve: {
            'pagingParams': MotorDueReminderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.motorDueReminder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const motorDueReminderPopupRoute: Routes = [
    {
        path: 'motor-due-reminder-lov',
        component: MotorDueReminderLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.motorDueReminder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'motor-due-reminder-new',
        component: MotorDueReminderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.motorDueReminder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'motor-due-reminder/:id/edit',
        component: MotorDueReminderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.motorDueReminder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
