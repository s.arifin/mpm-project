import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {MotorDueReminder} from './motor-due-reminder.model';
import {MotorDueReminderPopupService} from './motor-due-reminder-popup.service';
import {MotorDueReminderService} from './motor-due-reminder.service';
import {ToasterService} from '../../shared';
import { Motor, MotorService } from '../motor';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-motor-due-reminder-dialog',
    templateUrl: './motor-due-reminder-dialog.component.html'
})
export class MotorDueReminderDialogComponent implements OnInit {

    motorDueReminder: MotorDueReminder;
    isSaving: boolean;

    motors: Motor[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected motorDueReminderService: MotorDueReminderService,
        protected motorService: MotorService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.motorService.query()
            .subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.motorDueReminder.idReminder !== undefined) {
            this.subscribeToSaveResponse(
                this.motorDueReminderService.update(this.motorDueReminder));
        } else {
            this.subscribeToSaveResponse(
                this.motorDueReminderService.create(this.motorDueReminder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<MotorDueReminder>) {
        result.subscribe((res: MotorDueReminder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: MotorDueReminder) {
        this.eventManager.broadcast({ name: 'motorDueReminderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'motorDueReminder saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'motorDueReminder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }
}

@Component({
    selector: 'jhi-motor-due-reminder-popup',
    template: ''
})
export class MotorDueReminderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected motorDueReminderPopupService: MotorDueReminderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.motorDueReminderPopupService
                    .open(MotorDueReminderDialogComponent as Component, params['id']);
            } else {
                this.motorDueReminderPopupService
                    .open(MotorDueReminderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
