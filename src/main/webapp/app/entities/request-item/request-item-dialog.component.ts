import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RequestItem } from './request-item.model';
import { RequestItemPopupService } from './request-item-popup.service';
import { RequestItemService } from './request-item.service';
import { ToasterService } from '../../shared';
import { Request, RequestService } from '../request';
import { ResponseWrapper, CommonUtilService } from '../../shared';
import { GoodService, Good } from '../good';
import { LoadingService } from '../../layouts/loading/loading.service';
import { Product } from '../product';

@Component({
    selector: 'jhi-request-item-dialog',
    templateUrl: './request-item-dialog.component.html'
})
export class RequestItemDialogComponent implements OnInit {

    requestItem: RequestItem;
    isSaving: boolean;
    idRequest: any;

    requests: Request[];

    public goods: Array<Good>;

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected requestItemService: RequestItemService,
        protected requestService: RequestService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected goodService: GoodService,
        protected commonUtilService: CommonUtilService,
        protected loadingService: LoadingService
    ) {
    }

    ngOnInit() {
        this.goodService.query().subscribe((res) => { this.goods = res.json; }, (err) => { this.commonUtilService.showError(err); });

        this.isSaving = false;
        this.requestService.query()
            .subscribe((res: ResponseWrapper) => { this.requests = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.requestItem.idRequestItem !== undefined) {
            this.subscribeToSaveResponse(
                this.requestItemService.update(this.requestItem));
        } else {
            this.subscribeToSaveResponse(
                this.requestItemService.create(this.requestItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RequestItem>) {
        result.subscribe((res: RequestItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: RequestItem) {
        this.eventManager.broadcast({ name: 'requestItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'requestItem saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'requestItem Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRequestById(index: number, item: Request) {
        return item.idRequest;
    }

    trackProductById(index: number, item: Product) {
        return item.idProduct;
    }
}

@Component({
    selector: 'jhi-request-item-popup',
    template: ''
})
export class RequestItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected requestItemPopupService: RequestItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.requestItemPopupService
                    .open(RequestItemDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.requestItemPopupService.idRequest = params['parent'];
                this.requestItemPopupService
                    .open(RequestItemDialogComponent as Component);
            } else {
                this.requestItemPopupService
                    .open(RequestItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
