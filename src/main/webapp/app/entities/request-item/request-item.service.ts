import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { RequestItem } from './request-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class RequestItemService {
    protected itemValues: RequestItem[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/request-items';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/request-items';

    constructor(protected http: Http) { }

    create(requestItem: RequestItem): Observable<RequestItem> {
        const copy = this.convert(requestItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(requestItem: RequestItem): Observable<RequestItem> {
        const copy = this.convert(requestItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<RequestItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<RequestItem> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, requestItems: Array<RequestItem>): Observable<RequestItem[]> {
        const copy = this.convertList(requestItems);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to RequestItem.
     */
    protected convertItemFromServer(json: any): RequestItem {
        const entity: RequestItem = Object.assign(new RequestItem(), json);
        return entity;
    }

    /**
     * Convert a RequestItem to a JSON which can be sent to the server.
     */
    protected convert(requestItem: RequestItem): RequestItem {
        if (requestItem === null || requestItem === {}) {
            return {};
        }
        // const copy: RequestItem = Object.assign({}, requestItem);
        const copy: RequestItem = JSON.parse(JSON.stringify(requestItem));
        return copy;
    }

    protected convertList(requestItems: RequestItem[]): RequestItem[] {
        const copy: RequestItem[] = requestItems;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: RequestItem[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
