import { BaseEntity } from './../../shared';

export class RequestItem implements BaseEntity {
    constructor(
        public id?: any,
        public idRequestItem?: any,
        public sequence?: number,
        public idProduct?: string,
        public description?: string,
        public qtyReq?: number,
        public qtyTransfer?: number,
        public requestId?: any,
    ) {
    }
}
