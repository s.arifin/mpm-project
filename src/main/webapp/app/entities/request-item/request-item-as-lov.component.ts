import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable, Subscription } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiParseLinks, JhiPaginationUtil, JhiLanguageService } from 'ng-jhipster';
import { LazyLoadEvent } from 'primeng/primeng';
import { RequestItem } from './request-item.model';
import { RequestItemPopupService } from './request-item-popup.service';
import { RequestItemService } from './request-item.service';
import { ToasterService } from '../../shared';
import { Request, RequestService } from '../request';
import { ResponseWrapper, CommonUtilService, ITEMS_PER_PAGE, Principal, } from '../../shared';
import { GoodService, Good } from '../good';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-request-item-as-lov',
    templateUrl: './request-item-as-lov.component.html'
})

export class RequestItemAsLovComponent implements OnInit {
    public idRequest: string;
    public requestItems: Array<RequestItem>;

    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: Number;

    constructor(
        public activeModal: NgbActiveModal,
        protected requestItemService: RequestItemService,
        protected loadingService: LoadingService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idRequestItem';
        this.reverse = 'asc';
    }

    ngOnInit() {
        this.loadAll();
    }

    protected loadAll(): void {
        this.loadingService.loadingStart();
        this.requestItemService.queryFilterBy({
            filterName: 'relation',
            idRequest: this.idRequest,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        console.log('data dialog', data);
        this.requestItems = data;
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    protected sort(): any {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequestItem') {
            result.push('idRequestItem');
        }
        return result;
    }

    public loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    public clearPage(): void {
        this.activeModal.dismiss('cancel');
    }
}

@Component({
    selector: 'jhi-request-item-as-lov-popup',
    template: ''
})

export class RequestItemAsLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected requestItemPopupService: RequestItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['idRequest']) {
                this.requestItemPopupService.idRequest = params['idRequest'];
                this.requestItemPopupService
                    .open(RequestItemAsLovComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
