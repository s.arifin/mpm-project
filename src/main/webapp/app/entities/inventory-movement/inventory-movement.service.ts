import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';

import { InventoryMovement } from './inventory-movement.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class InventoryMovementService {
    protected itemValues: InventoryMovement[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/inventory-movements';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/inventory-movements';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(inventoryMovement: InventoryMovement): Observable<InventoryMovement> {
        const copy = this.convert(inventoryMovement);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(inventoryMovement: InventoryMovement): Observable<InventoryMovement> {
        const copy = this.convert(inventoryMovement);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<InventoryMovement> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(inventoryMovement: InventoryMovement, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(inventoryMovement);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, inventoryMovement: InventoryMovement): Observable<InventoryMovement> {
        const copy = this.convert(inventoryMovement);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, inventoryMovements: InventoryMovement[]): Observable<InventoryMovement[]> {
        const copy = this.convertList(inventoryMovements);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateCreate) {
            entity.dateCreate = new Date(entity.dateCreate);
        }
    }

    protected convert(inventoryMovement: InventoryMovement): InventoryMovement {
        if (inventoryMovement === null || inventoryMovement === {}) {
            return {};
        }
        // const copy: InventoryMovement = Object.assign({}, inventoryMovement);
        const copy: InventoryMovement = JSON.parse(JSON.stringify(inventoryMovement));

        // copy.dateCreate = this.dateUtils.toDate(inventoryMovement.dateCreate);
        return copy;
    }

    protected convertList(inventoryMovements: InventoryMovement[]): InventoryMovement[] {
        const copy: InventoryMovement[] = inventoryMovements;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: InventoryMovement[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
