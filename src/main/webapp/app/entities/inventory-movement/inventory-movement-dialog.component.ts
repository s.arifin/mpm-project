import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {InventoryMovement} from './inventory-movement.model';
import {InventoryMovementPopupService} from './inventory-movement-popup.service';
import {InventoryMovementService} from './inventory-movement.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-inventory-movement-dialog',
    templateUrl: './inventory-movement-dialog.component.html'
})
export class InventoryMovementDialogComponent implements OnInit {

    inventoryMovement: InventoryMovement;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected inventoryMovementService: InventoryMovementService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.inventoryMovement.idSlip !== undefined) {
            this.subscribeToSaveResponse(
                this.inventoryMovementService.update(this.inventoryMovement));
        } else {
            this.subscribeToSaveResponse(
                this.inventoryMovementService.create(this.inventoryMovement));
        }
    }

    protected subscribeToSaveResponse(result: Observable<InventoryMovement>) {
        result.subscribe((res: InventoryMovement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: InventoryMovement) {
        this.eventManager.broadcast({ name: 'inventoryMovementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'inventoryMovement saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'inventoryMovement Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-inventory-movement-popup',
    template: ''
})
export class InventoryMovementPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected inventoryMovementPopupService: InventoryMovementPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.inventoryMovementPopupService
                    .open(InventoryMovementDialogComponent as Component, params['id']);
            } else {
                this.inventoryMovementPopupService
                    .open(InventoryMovementDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
