import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { InventoryMovementComponent } from './inventory-movement.component';
import { InventoryMovementPopupComponent } from './inventory-movement-dialog.component';

@Injectable()
export class InventoryMovementResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idSlip,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const inventoryMovementRoute: Routes = [
    {
        path: 'inventory-movement',
        component: InventoryMovementComponent,
        resolve: {
            'pagingParams': InventoryMovementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.inventoryMovement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const inventoryMovementPopupRoute: Routes = [
    {
        path: 'inventory-movement-new',
        component: InventoryMovementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.inventoryMovement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'inventory-movement/:id/edit',
        component: InventoryMovementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.inventoryMovement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
