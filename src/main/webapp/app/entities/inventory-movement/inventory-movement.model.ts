import { BaseEntity } from './../../shared';

export class InventoryMovement implements BaseEntity {
    constructor(
        public id?: any,
        public idSlip?: any,
        public dateCreate?: any,
    ) {
    }
}
