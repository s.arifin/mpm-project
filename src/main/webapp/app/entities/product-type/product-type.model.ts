import { BaseEntity } from './../../shared';

export class ProductType implements BaseEntity {
    constructor(
        public id?: number,
        public idProductType?: number,
        public description?: string,
    ) {
    }
}
