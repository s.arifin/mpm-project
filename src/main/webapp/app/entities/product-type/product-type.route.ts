import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ProductTypeComponent } from './product-type.component';
import { ProductTypePopupComponent } from './product-type-dialog.component';

@Injectable()
export class ProductTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idProductType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const productTypeRoute: Routes = [
    {
        path: 'product-type',
        component: ProductTypeComponent,
        resolve: {
            'pagingParams': ProductTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const productTypePopupRoute: Routes = [
    {
        path: 'product-type-new',
        component: ProductTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-type/:id/edit',
        component: ProductTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
