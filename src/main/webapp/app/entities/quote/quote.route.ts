import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { QuoteComponent } from './quote.component';
import { QuoteEditComponent } from './quote-edit.component';
import { QuotePopupComponent } from './quote-dialog.component';

@Injectable()
export class QuoteResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idQuote,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const quoteRoute: Routes = [
    {
        path: 'quote',
        component: QuoteComponent,
        resolve: {
            'pagingParams': QuoteResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.quote.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const quotePopupRoute: Routes = [
    {
        path: 'quote-new',
        component: QuoteEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.quote.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'quote/:id/edit',
        component: QuoteEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.quote.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'quote-popup-new',
        component: QuotePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.quote.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'quote/:id/popup-edit',
        component: QuotePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.quote.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
