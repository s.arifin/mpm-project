import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';

import { Quote } from './quote.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class QuoteService {
    protected itemValues: Quote[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/quotes';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/quotes';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(quote: Quote): Observable<Quote> {
        const copy = this.convert(quote);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(quote: Quote): Observable<Quote> {
        const copy = this.convert(quote);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<Quote> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(quote: Quote, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(quote);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, quote: Quote): Observable<Quote> {
        const copy = this.convert(quote);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, quotes: Quote[]): Observable<Quote[]> {
        const copy = this.convertList(quotes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateIssued) {
            entity.dateIssued = new Date(entity.dateIssued);
        }
    }

    protected convert(quote: Quote): Quote {
        if (quote === null || quote === {}) {
            return {};
        }
        // const copy: Quote = Object.assign({}, quote);
        const copy: Quote = JSON.parse(JSON.stringify(quote));

        // copy.dateIssued = this.dateUtils.toDate(quote.dateIssued);
        return copy;
    }

    protected convertList(quotes: Quote[]): Quote[] {
        const copy: Quote[] = quotes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: Quote[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
