import { BaseEntity } from './../../shared';

export class Quote implements BaseEntity {
    constructor(
        public id?: any,
        public idQuote?: any,
        public dateIssued?: any,
        public description?: string,
    ) {
    }
}
