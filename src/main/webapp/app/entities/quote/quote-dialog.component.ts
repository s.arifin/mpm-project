import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {Quote} from './quote.model';
import {QuotePopupService} from './quote-popup.service';
import {QuoteService} from './quote.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-quote-dialog',
    templateUrl: './quote-dialog.component.html'
})
export class QuoteDialogComponent implements OnInit {

    quote: Quote;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected quoteService: QuoteService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.quote.idQuote !== undefined) {
            this.subscribeToSaveResponse(
                this.quoteService.update(this.quote));
        } else {
            this.subscribeToSaveResponse(
                this.quoteService.create(this.quote));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Quote>) {
        result.subscribe((res: Quote) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: Quote) {
        this.eventManager.broadcast({ name: 'quoteListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'quote saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'quote Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-quote-popup',
    template: ''
})
export class QuotePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected quotePopupService: QuotePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.quotePopupService
                    .open(QuoteDialogComponent as Component, params['id']);
            } else {
                this.quotePopupService
                    .open(QuoteDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
