import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Quote } from './quote.model';
import { QuoteService } from './quote.service';
import { ToasterService} from '../../shared';

@Component({
    selector: 'jhi-quote-edit',
    templateUrl: './quote-edit.component.html'
})
export class QuoteEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    quote: Quote;
    isSaving: boolean;

    constructor(
        protected alertService: JhiAlertService,
        protected quoteService: QuoteService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.quote = new Quote();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.quoteService.find(id).subscribe((quote) => {
            this.quote = quote;
        });
    }

    previousState() {
        this.router.navigate(['quote']);
    }

    save() {
        this.isSaving = true;
        if (this.quote.idQuote !== undefined) {
            this.subscribeToSaveResponse(
                this.quoteService.update(this.quote));
        } else {
            this.subscribeToSaveResponse(
                this.quoteService.create(this.quote));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Quote>) {
        result.subscribe((res: Quote) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Quote) {
        this.eventManager.broadcast({ name: 'quoteListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'quote saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'quote Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}
