import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Facility } from './facility.model';
import { FacilityPopupService } from './facility-popup.service';
import { FacilityService } from './facility.service';
import { ToasterService, Principal } from '../../shared';
import { FacilityType, FacilityTypeService } from '../facility-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-facility-dialog',
    templateUrl: './facility-dialog.component.html'
})
export class FacilityDialogComponent implements OnInit {

    facility: Facility;
    isSaving: boolean;
    idFacilityType: any;
    idPartOf: any;
    dateNow: any;

    facilitytypes: FacilityType[];

    facilities: Facility[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected facilityService: FacilityService,
        protected facilityTypeService: FacilityTypeService,
        protected eventManager: JhiEventManager,
        protected principal: Principal,
        protected toaster: ToasterService
    ) {
        this.dateNow = new Date();
    }

    ngOnInit() {
        this.isSaving = false;
        this.facilityTypeService.getAll()
            .subscribe((res: ResponseWrapper) => { this.facilitytypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.facilityService.getAll(this.principal.getIdInternal())
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        this.facility.internalId = this.principal.getIdInternal();
        if (this.facility.idFacility !== undefined) {
            this.subscribeToSaveResponse(
                this.facilityService.update(this.facility));
        } else {
            this.subscribeToSaveResponse(
                this.facilityService.create(this.facility));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Facility>) {
        result.subscribe((res: Facility) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: Facility) {
        this.eventManager.broadcast({ name: 'facilityListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'facility saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'facility Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackFacilityTypeById(index: number, item: FacilityType) {
        return item.idFacilityType;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }
}

@Component({
    selector: 'jhi-facility-popup',
    template: ''
})
export class FacilityPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected facilityPopupService: FacilityPopupService
    ) {}

    ngOnInit() {
        this.facilityPopupService.idFacilityType = undefined;
        this.facilityPopupService.idPartOf = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.facilityPopupService
                    .open(FacilityDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.facilityPopupService.parent = params['parent'];
                this.facilityPopupService
                    .open(FacilityDialogComponent as Component);
            } else {
                this.facilityPopupService
                    .open(FacilityDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
