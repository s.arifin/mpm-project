import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Facility } from './facility.model';
import { FacilityService } from './facility.service';

@Injectable()
export class FacilityPopupService {
    protected ngbModalRef: NgbModalRef;
    idFacilityType: any;
    idPartOf: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected facilityService: FacilityService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.facilityService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.facilityModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new Facility();
                    data.facilityTypeId = this.idFacilityType;
                    data.partOfId = this.idPartOf;
                    this.ngbModalRef = this.facilityModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    facilityModalRef(component: Component, facility: Facility): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.facility = facility;
        modalRef.componentInstance.idFacilityType = this.idFacilityType;
        modalRef.componentInstance.idPartOf = this.idPartOf;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.facilityLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    facilityLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idFacilityType = this.idFacilityType;
        modalRef.componentInstance.idPartOf = this.idPartOf;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
