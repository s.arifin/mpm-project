import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';
import { Subject } from 'rxjs/Subject';
import { Facility } from './facility.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { AbstractEntityService } from '../../shared/base/abstract-entity.service';

@Injectable()
export class FacilityService {
    protected itemValues: Facility[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl = process.env.API_C_URL + '/api/facilities';

    constructor(protected http: Http) { }

    create(facilityType: Facility): Observable<Facility> {
        const copy = this.convert(facilityType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(facilityType: Facility): Observable<Facility> {
        const copy = this.convert(facilityType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<Facility> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    getAll(req: any): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/getAll/' + req)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    getAllStockOpnamebyInternal(idinternal: string): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/by-idinternal-dropdown/' + idinternal)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterByinternalC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceUrl}/byidinternal`, options)
        .map((res: Response) => this.convertResponse(res));
    }

    queryC(idInternal?: any): Observable<ResponseWrapper> {
        const options = createRequestOption();
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceUrl + '/byIdInternal/?IdInternal=' + idInternal, options)
            .map((res: Response) => this.convertResponse(res));
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    protected convertItemFromServer(json: any): Facility {
        const entity: Facility = Object.assign(new Facility(), json);
        return entity;
    }

    protected convert(facility: Facility): Facility {
        if (facility === null || facility === {}) {
            return {};
        }
        // const copy: Facility = Object.assign({}, facility);
        const copy: Facility = JSON.parse(JSON.stringify(facility));
        return copy;
    }

    pushItems(data: Facility[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
