import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { FacilityComponent } from './facility.component';
import { FacilityEditComponent } from './facility-edit.component';
import { FacilityLovPopupComponent } from './facility-as-lov.component';
import { FacilityPopupComponent } from './facility-dialog.component';

@Injectable()
export class FacilityResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'facilityCode,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const facilityRoute: Routes = [
    {
        path: 'facility',
        component: FacilityComponent,
        resolve: {
            'pagingParams': FacilityResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facility.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const facilityPopupRoute: Routes = [
    {
        path: 'facility-lov',
        component: FacilityLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facility.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'facility-popup-new-list/:parent',
        component: FacilityPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facility.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'facility-popup-new',
        component: FacilityPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facility.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'facility-new',
        component: FacilityEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facility.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'facility/:id/edit',
        component: FacilityEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facility.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'facility/:route/:page/:id/edit',
        component: FacilityEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facility.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'facility/:id/popup-edit',
        component: FacilityPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facility.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
