import { BaseEntity } from './../../shared';

export class Facility implements BaseEntity {
    constructor(
        public id?: any,
        public idFacility?: any,
        public facilityCode?: string,
        public description?: string,
        public facilityTypeId?: any,
        public partOfId?: any,
        public internalId?: any,
        public dateFrom?: any,
        public dateThru?: any,
        public status?: string,
        public completed?: Boolean,
    ) {
    }
}

export class FacilityNet implements BaseEntity {
    constructor(
        public id?: any,
        public idFacility?: string,
        public code?: string,
        public description?: string,
        public idOwner?: string,
        public name?: string,
    ) {
    }
}
