import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Facility } from './facility.model';
import { FacilityService } from './facility.service';
import { ToasterService, Principal} from '../../shared';
import { FacilityType, FacilityTypeService } from '../facility-type';
import { ResponseWrapper } from '../../shared';

@Component({
   selector: 'jhi-facility-edit',
   templateUrl: './facility-edit.component.html'
})
export class FacilityEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   facility: Facility;
   isSaving: boolean;
   idFacility: any;
   paramPage: number;
   routeId: number;
   dateNow: any;
   selectedDateFrom: any;
   selectedDateThru: any;

   facilitytypes: FacilityType[];

   facilities: Facility[];

   constructor(
       protected alertService: JhiAlertService,
       protected facilityService: FacilityService,
       protected facilityTypeService: FacilityTypeService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected principal: Principal,
       protected eventManager: JhiEventManager,
       protected toaster: ToasterService
   ) {
       this.facility = new Facility();
       this.routeId = 0;
       this.dateNow = new Date();
    }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idFacility = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
       this.facilityTypeService.getAll()
            .subscribe((res: ResponseWrapper) => { this.facilitytypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.facilityService.getAll(this.principal.getIdInternal())
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

    load() {
        this.facilityService.find(this.idFacility).subscribe((facility) => {
            this.facility = facility;
            this.selectedDateFrom = new Date(this.facility.dateFrom);
            this.selectedDateThru = new Date(this.facility.dateThru);
        });
    }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['facility', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       this.facility.dateFrom = this.selectedDateFrom;
       this.facility.dateThru = this.selectedDateThru;
       if (this.facility.idFacility !== undefined) {
           this.subscribeToSaveResponse(
               this.facilityService.update(this.facility));
       } else {
           this.subscribeToSaveResponse(
               this.facilityService.create(this.facility));
       }
   }

   protected subscribeToSaveResponse(result: Observable<Facility>) {
       result.subscribe((res: Facility) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: Facility) {
       this.eventManager.broadcast({ name: 'facilityListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'facility saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'facility Changed', error.message);
       this.alertService.error(error.message, null, null);
   }

   trackFacilityTypeById(index: number, item: FacilityType) {
       return item.idFacilityType;
   }

   trackFacilityById(index: number, item: Facility) {
       return item.idFacility;
   }
}
