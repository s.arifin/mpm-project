import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { AxCustomerService } from './';

@NgModule({
    providers: [
        AxCustomerService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmAxCustomerModule {}
