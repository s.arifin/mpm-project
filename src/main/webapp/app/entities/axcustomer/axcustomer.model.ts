import { BaseEntity } from './../../shared';

export class AxCustomer {
    constructor(
        public DataArea?: string,
        public AccountNum?: string,
        public Name?: string,
        public CustGroup?: string,
        public PaymentTermId?: string,
        public City?: string,
        public State?: string,
        public LocationName?: string,
        public Address?: string,
        public ZipCode?: string,
        public MainAccount?: string,
        public Dimensi1?: string,
        public Dimensi2?: string,
        public Dimensi3?: string,
        public Dimensi4?: string,
        public Dimensi5?: string,
        public Dimensi6?: string,
        public TaxReGId?: string,
        public Telephone?: string
    ) {
    }
}
