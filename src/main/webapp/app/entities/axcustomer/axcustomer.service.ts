import { Injectable } from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import { RequestOptions } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { AxCustomer } from './axcustomer.model';
import { ResponseWrapper } from '../../shared';

// import { SERVER_API_URL } from '../../app.constants';
import { SERVER_API_C_URL } from '../../app.constants';

@Injectable()
export class AxCustomerService {

    // protected resourceUrl =  'http://10.10.104.127:8084/api/ax_customer';
    protected resourceUrl =  process.env.API_C_URL + '/api/ax_customer';
    constructor(
        protected http: Http
    ) { }

    protected convert(axcustomer: AxCustomer): string {
        if (axcustomer === null || axcustomer === {}) {
            return '';
        }
        return JSON.stringify(axcustomer);
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json(); // 8084
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    send(axcustomer: AxCustomer): Observable<ResponseWrapper> {
        const copy = this.convert(axcustomer);

        const options = new RequestOptions();
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');

        console.log('axcustomer : ', copy);
        return this.http.post(this.resourceUrl, copy, options).map((res: Response) => this.convertResponse(res));
    }
}
