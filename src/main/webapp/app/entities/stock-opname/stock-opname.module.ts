import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    StockOpnameService,
    StockOpnamePopupService,
    StockOpnameApprovalPopupService,
    StockOpnameComponent,
    StockOpnameDialogComponent,
    StockOpnameApprovalDialogComponent,
    StockOpnameUploadComponent,
    StockOpnamePopupComponent,
    StockOpnameApprovalPopupComponent,
    stockOpnameRoute,
    stockOpnamePopupRoute,
    StockOpnameResolvePagingParams,
    StockOpnameEditComponent,
    StockOpnameUnitComponent,
    StockOpnameUnitMemberComponent,
    StockOpnameUnitMemberInputComponent,
    StockOpnameUnitMemberDetailComponent,
    StockOpnameUnitPICComponent,
    StockOpnameUnitAssignComponent,
    StockOpnameUnitDetailComponent,
    StockOpnameUnitResultComponent,
    StockOpnameUnitUploadComponent
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
    AutoCompleteModule,
    InputTextModule,
    InputTextareaModule,
    DataScrollerModule,
    PaginatorModule,
    ConfirmDialogModule,
    ConfirmationService,
    GrowlModule,
    SharedModule,
    FieldsetModule,
    ScheduleModule,
    ChartModule,
    DragDropModule,
    LightboxModule,
    FileUploadModule
} from 'primeng/primeng';

import { DataTableModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { MenuModule } from 'primeng/primeng';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { MpmSharedEntityModule } from '../shared-entity.module';

import { MpmStockOpnameItemModule } from '../stock-opname-item/stock-opname-item.module';

const ENTITY_STATES = [
    ...stockOpnameRoute,
    ...stockOpnamePopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        TabsModule.forRoot(),
        TooltipModule.forRoot(),
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        FileUploadModule,
        CurrencyMaskModule,
        DataTableModule,
        SliderModule,
        RadioButtonModule,
        MpmStockOpnameItemModule,
        MenuModule
    ],
    exports: [
        StockOpnameComponent,
        StockOpnameEditComponent,
        StockOpnameUnitComponent,
        StockOpnameUnitMemberComponent,
        StockOpnameUnitMemberDetailComponent,
        StockOpnameUnitMemberInputComponent,
        StockOpnameUnitPICComponent,
        StockOpnameUnitAssignComponent,
        StockOpnameUnitDetailComponent,
        StockOpnameUnitResultComponent,
        StockOpnameUnitUploadComponent
    ],
    declarations: [
        StockOpnameComponent,
        StockOpnameDialogComponent,
        StockOpnameApprovalDialogComponent,
        StockOpnameUploadComponent,
        StockOpnamePopupComponent,
        StockOpnameApprovalPopupComponent,
        StockOpnameEditComponent,
        StockOpnameUnitComponent,
        StockOpnameUnitMemberComponent,
        StockOpnameUnitMemberDetailComponent,
        StockOpnameUnitMemberInputComponent,
        StockOpnameUnitPICComponent,
        StockOpnameUnitAssignComponent,
        StockOpnameUnitDetailComponent,
        StockOpnameUnitResultComponent,
        StockOpnameUnitUploadComponent
    ],
    entryComponents: [
        StockOpnameComponent,
        StockOpnameDialogComponent,
        StockOpnameApprovalDialogComponent,
        StockOpnameUploadComponent,
        StockOpnamePopupComponent,
        StockOpnameApprovalPopupComponent,
        StockOpnameEditComponent,
        StockOpnameUnitComponent,
        StockOpnameUnitMemberComponent,
        StockOpnameUnitMemberDetailComponent,
        StockOpnameUnitMemberInputComponent,
        StockOpnameUnitPICComponent,
        StockOpnameUnitAssignComponent,
        StockOpnameUnitDetailComponent,
        StockOpnameUnitResultComponent,
        StockOpnameUnitUploadComponent
    ],
    providers: [
        StockOpnameService,
        StockOpnamePopupService,
        StockOpnameApprovalPopupService,
        StockOpnameResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmStockOpnameModule { }
