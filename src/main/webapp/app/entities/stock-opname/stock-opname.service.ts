import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { StockOpname, StockOpnameAssign, StockOpnameNet, StockOpnameResult, StockOpnameItem } from './stock-opname.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class StockOpnameService {
    protected itemValues: StockOpname[];
    values: Subject<any> = new Subject<any>();

    // for Modal
    protected selectedItems = new BehaviorSubject<any>({});
    currentItems = this.selectedItems.asObservable();

    protected resourceUrl =  SERVER_API_URL + 'api/stock-opnames';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/stock-opnames';
    protected resourceCUrl = process.env.API_C_URL + '/api/stockopname';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    selecItems(data: any) {
        this.selectedItems.next(data);
    }

    create(stockOpname: StockOpname): Observable<StockOpname> {
        const copy = this.convert(stockOpname);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    newStockOpname(stockOpname: StockOpname, req?: any): Observable<StockOpname> {
        const copy = this.convert(stockOpname);
        const options = createRequestOption(req);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(stockOpname: StockOpname): Observable<StockOpname> {
        const copy = this.convert(stockOpname);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    setHold(assign: StockOpnameAssign): Observable<StockOpnameAssign> {
        const options = this.convert(assign);
        return this.http.post(this.resourceCUrl + '/unit/sethold', options).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    savebymember(id: any): Observable<Response> {
        return this.http.post(this.resourceCUrl + '/unit/savebymember/' + id, null)
    }

    find(id: any): Observable<StockOpname> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    checkUnitOnLocation(idstockopname: any, idfacility: any, idcontainer: any): Promise<Boolean> {
        return this.http
                   .get(this.resourceCUrl + '/unit/check-unit-on-location?idstockopname=' + idstockopname + '&idfacility=' + idfacility + '&idcontainer=' + idcontainer)
                   .map((res: Response) => res.json())
                   .toPromise();
    }

    getAssignStatus(id: any): Promise<Boolean> {
        return this.http
                   .get(this.resourceCUrl + '/isassigncompleted/' + id)
                   .map((res: Response) => res.json())
                   .toPromise();
    }

    getStatusHold(id: any): Promise<Boolean> {
        return this.http
                   .get(this.resourceCUrl + '/isstatushold/' + id)
                   .map((res: Response) => res.json())
                   .toPromise();
    }

    getStatusCompleted(id: any): Promise<Boolean> {
        return this.http
                   .get(this.resourceCUrl + '/isstatuscompleted/' + id)
                   .map((res: Response) => res.json())
                   .toPromise();
    }

    getOpenStatus(id: any): Promise<Boolean> {
        return this.http
                   .get(this.resourceCUrl + '/isassignmentopen/' + id)
                   .map((res: Response) => res.json())
                   .toPromise();
    }

    getMemberStatus(id: any, username: string): Promise<Boolean> {
        return this.http
                   .get(this.resourceCUrl + '/ismember/' + id + '/' + username)
                   .map((res: Response) => res.json())
                   .toPromise();
    }

    getMemberCompletedStatus(id: any, username: string): Promise<Boolean> {
        return this.http
                   .get(this.resourceCUrl + '/ismembercompleted/' + id + '/' + username)
                   .map((res: Response) => res.json())
                   .toPromise();
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/getdata', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllbyMember(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/getdatabymember', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAssign(req?: any): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrl + '/assignment/' + req)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllAssign(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/getassigndata', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllAssignByMember(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/getassigndatabymember', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllAssignByPIC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/getassigndatabypic', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAssignByMember(id?: any): Observable<StockOpnameAssign> {
        return this.http.get(this.resourceCUrl + '/getassigndatabymember/' + id)
            .map((res: Response) => {
            return res.json();
        });
    }

    getStockOpname(id?: any): Observable<StockOpnameNet> {
        return this.http.get(this.resourceCUrl + '/getstockopname/' + id)
            .map((res: Response) => {
            return res.json();
        });
    }

    getResult(id?: any): Observable<StockOpnameResult> {
        return this.http.get(this.resourceCUrl + '/unit/itemresult/' + id)
            .map((res: Response) => {
            return res.json();
        });
    }

    getUnitByAssign(req?: any, id?: any, issave?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/getitemdatabyassign/' + issave + '/' + id, options)
            .map((res: Response) => this.convertResponse(res));
    }

    cresate(stockOpname: StockOpname): Observable<StockOpname> {
        const copy = this.convert(stockOpname);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    unitCutOff(stockOpname: StockOpnameNet): Observable<ResponseWrapper> {
        const options = this.convertNet(stockOpname);
        return this.http.post(this.resourceCUrl + '/unit/cutoff', options)
        .map((res: Response) => this.convertResponse(res));
    }

    stockOpnameFacility(idStockOpname: any): Observable<ResponseWrapper> {
        const options = new RequestOptions();
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/FacilityGetData/?IdStockOpname=' + idStockOpname, options)
        .map((res: Response) => this.convertResponse(res));
    }

    submitAssignment(each: any): Observable<ResponseWrapper> {
        return this.http.post(this.resourceCUrl + '/assignment/submit/' + each, null)
    }

    stockOpnameAssignment(pic: string, assignmentList: any, each: any): Observable<ResponseWrapper> {
        const options = new RequestOptions();
        const copy = JSON.stringify(assignmentList);
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(this.resourceCUrl + '/AssignPIC/?CreatedBy=' + pic + '&each=' + each, copy, options)
        .map((res: Response) => this.convertResponse(res));
    }

    setPICCompleted(assignmentList: any): Observable<ResponseWrapper> {
        const options = new RequestOptions();
        const copy = JSON.stringify(assignmentList);
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(this.resourceCUrl + '/setpiccompleted/', copy, options)
        .map((res: Response) => this.convertResponse(res));
    }

    uploadUnit(id: string, created: String, item: StockOpnameItem[]): Observable<StockOpnameItem[]> {
        const copy = this.convertItemList(item);
        return this.http.post(this.resourceCUrl + '/unit/upload/' + id + '/' + created, copy)
            .map((res: Response) => res.json());
    }

    stockOpnameAddItem(stockOpnameItem: any, pic: string): Observable<Response> {
        const options = new RequestOptions();
        const copy = JSON.stringify(stockOpnameItem);
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(this.resourceCUrl + '/AddItem/?CreatedBy=' + pic, copy, options);
    }

    stockOpnameAddItemByAuthorized(stockOpnameItem: any, pic: string): Observable<Response> {
        const options = new RequestOptions();
        const copy = JSON.stringify(stockOpnameItem);
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(this.resourceCUrl + '/AddItemByAuthorized/?CreatedBy=' + pic, copy, options);
    }

    stockOpnameDeleteItem(IdItem: any): Observable<Response> {
        return this.http.delete(this.resourceCUrl + '/unit/deleteitem/' + IdItem);
    }

    stockOpnameSubmitResult(stockOpname: StockOpnameNet): Observable<Response> {
        const options = this.convertNet(stockOpname);
        return this.http.post(this.resourceCUrl + '/unit/submit', options)
    }

    stockOpnameCompleted(IdStockOpname: any, userId: string): Observable<Response> {
        return this.http.post(this.resourceCUrl + '/unit/setcompleted/' + IdStockOpname + '/' + userId, null)
    }

    stockOpnameGetResult(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/GetResultData', options)
            .map((res: Response) => this.convertResponse(res));
    }

    stockOpnameGetResultByFilter(req: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/GetResultData/filter', options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(stockOpname: StockOpname, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(stockOpname);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    openEdit(id: any, username: string): Observable<ResponseWrapper> {
        return this.http.post(this.resourceCUrl + '/unit/openedit/' + id + '/' + username, null)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(this.resourceCUrl + '/unit/deleteitem/' + id);
    }

    deleteAssign(id?: any): Observable<Response> {
        return this.http.delete(this.resourceCUrl + '/assignment/' + id);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(item?: StockOpname, req?: any): Observable<Response> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, item, options);
    }

    executeListProcess(items?: StockOpname[], req?: any): Observable<Response> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to StockOpname.
     */
    protected convertItemFromServer(json: any): StockOpname {
        const entity: StockOpname = Object.assign(new StockOpname(), json);
        if (entity.dateCreated) {
            entity.dateCreated = new Date(entity.dateCreated);
        }
        return entity;
    }

    /**
     * Convert a StockOpname to a JSON which can be sent to the server.
     */
    protected convert(stockOpname: StockOpname): StockOpname {
        if (stockOpname === null || stockOpname === {}) {
            return {};
        }
        // const copy: StockOpname = Object.assign({}, stockOpname);
        const copy: StockOpname = JSON.parse(JSON.stringify(stockOpname));

        // copy.dateCreated = this.dateUtils.toDate(stockOpname.dateCreated);
        return copy;
    }

    protected convertNet(stockOpname: StockOpnameNet): StockOpnameNet {
        if (stockOpname === null || stockOpname === {}) {
            return {};
        }
        // const copy: StockOpname = Object.assign({}, stockOpname);
        const copy: StockOpnameNet = JSON.parse(JSON.stringify(stockOpname));

        // copy.dateCreated = this.dateUtils.toDate(stockOpname.dateCreated);
        return copy;
    }

    protected convertItemList(stockOpnames: StockOpnameItem[]): StockOpnameItem[] {
        const copy: StockOpnameItem[] = stockOpnames;
        copy.forEach((item) => {
            item = this.convertItem(item);
        });
        return copy;
    }

    protected convertItem(stockOpname: StockOpnameItem): StockOpnameItem {
        if (stockOpname === null || stockOpname === {}) {
            return {};
        }
        // const copy: StockOpname = Object.assign({}, stockOpname);
        const copy: StockOpnameItem = JSON.parse(JSON.stringify(stockOpname));

        // copy.dateCreated = this.dateUtils.toDate(stockOpname.dateCreated);
        return copy;
    }

    protected convertList(stockOpnames: StockOpname[]): StockOpname[] {
        const copy: StockOpname[] = stockOpnames;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: StockOpname[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
