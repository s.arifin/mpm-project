import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { StockOpnameItem } from '../stock-opname-item/stock-opname-item.model';
import { StockOpnameItemService } from '../stock-opname-item/stock-opname-item.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

import * as XLSX from 'xlsx';
import * as _ from 'lodash';

@Component({
    selector: 'jhi-stock-opname-upload',
    templateUrl: './stock-opname-upload.component.html'
})
export class StockOpnameUploadComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    stockOpnameItems: StockOpnameItem[];
    containerData: any;
    uploadData: any[] = [];
    fileUpload: any;
    idStockOpname: any;

    constructor(
        protected stockOpnameItemService: StockOpnameItemService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected confirmationService: ConfirmationService,
        protected toasterService: ToasterService
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idStockOpname = params['id'];
                this.stockOpnameItemService.query().subscribe(
                    (res: ResponseWrapper) => {
                        console.log('load stock opname:', res);
                        this.onSuccess(res.json, res.headers)
                    },
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        });
    }

    ngOnDestroy() {

    }

    upload() {
        this.convertXLXStoListPackageReceipt();
    }

    protected convertXLXStoListPackageReceipt(): void {
        if (this.containerData.length > 0) {
            this.uploadData = [];
            this.containerData.forEach((element) => {
                this.uploadData = [...this.uploadData, element];
            });
            this.toaster.showToaster('success', 'Upload', 'Succes Load !');
        } else {
            this.toaster.showToaster('warning', 'Upload', 'File Empty !');
        }
        this.fileUpload = '';
        this.containerData = [];
    }

    onFileChange(evt: any) {
        /* wire up file reader */
        const target: DataTransfer = <DataTransfer>(evt.target);
        // if (target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
            /* read workbooka */
            const bstr: string = e.target.result;
            const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
            /* grab first sheet */
            const wsname: string = wb.SheetNames[0];
            const ws: XLSX.WorkSheet = wb.Sheets[wsname];
            /* save data */
            this.containerData = (XLSX.utils.sheet_to_json(ws, { header: ['tag_number', 'parts_number', 'parts_name', 'container', 'qty', 'unit'], range: 1 }));
        };
        reader.readAsBinaryString(target.files[0]);
    }

    onSubmit() {
        console.log('Uploading...');
        console.log('List:', this.uploadData);
        console.log('Datagrid:', this.stockOpnameItems);
        for (const item of this.uploadData) {
            const updateData: any = _.cloneDeep(_.find(this.stockOpnameItems, { productId: item.parts_number }));
            console.log('updateData:', updateData)
            if (updateData !== undefined) {
                console.log('Updating...');
                this.subscribeToSaveResponse(
                    this.stockOpnameItemService.update(item));
            } else {
                console.log('New...');
                this.subscribeToSaveResponse(
                    this.stockOpnameItemService.create(item));
            }
        }
    }

    protected onSuccess(data, headers) {
        console.log('All Data', data);
        this.stockOpnameItems = data;
    }

    protected onError(error) {
        console.log('Error', error);
    }

    protected subscribeToSaveResponse(result: Observable<StockOpnameItem>) {
        result.subscribe((res: StockOpnameItem) => this.onSaveSuccess(res));
    }

    protected onSaveSuccess(result: StockOpnameItem) {
        this.eventManager.broadcast({ name: 'stockOpnameItemListModification', content: 'OK' });
        this.toasterService.showToaster('info', 'Save', 'stockOpnameItem saved !');
        this.router.navigate(['/stock-opname/0/1/' + this.idStockOpname + '/edit']);
    }

}
