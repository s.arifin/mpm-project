import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { StockOpname } from './stock-opname.model';
import { StockOpnamePopupService } from './stock-opname-popup.service';
import { StockOpnameService } from './stock-opname.service';
import { ToasterService } from '../../shared';
import { StandardCalendar, StandardCalendarService } from '../standard-calendar';
import { Internal, InternalService } from '../internal';
import { StockOpnameType, StockOpnameTypeService } from '../stock-opname-type';
import { ResponseWrapper } from '../../shared';

import { FacilityService } from '../facility/facility.service';
import { ContainerService } from '../container/container.service';
import { Facility } from '../facility';

@Component({
    selector: 'jhi-stock-opname-dialog',
    templateUrl: './stock-opname-dialog.component.html'
})
export class StockOpnameDialogComponent implements OnInit {

    stockOpname: StockOpname;
    isSaving: boolean;
    idCalendar: any;
    idInternal: any;
    idStockOpnameType: any;
    idStockOpnameContainer: any;
    calendarType: number;
    standardcalendars: StandardCalendar[];

    facilityList: any[] = [];
    containerList: any[] = [];

    internals: Internal[];

    stockopnametypes: StockOpnameType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected stockOpnameService: StockOpnameService,
        protected standardCalendarService: StandardCalendarService,
        protected internalService: InternalService,
        protected stockOpnameTypeService: StockOpnameTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected facilityService: FacilityService,
        protected containerService: ContainerService
    ) {

    }

    ngOnInit() {
        this.idInternal = 10002;
        this.calendarType = 3;
        this.isSaving = false;
        this.standardCalendarService.queryFilterBy({ calendarType: this.calendarType, idInternal: this.idInternal, size: 999999 })
            .subscribe((res: ResponseWrapper) => {
                this.standardcalendars = res.json;
                console.log('dataCalendar', res.json)
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => {
                this.internals = res.json;
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.stockOpnameTypeService.query()
            .subscribe((res: ResponseWrapper) => {
                this.stockopnametypes = res.json;
            }, (res: ResponseWrapper) => this.onError(res.json));

        // Load Gudang
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => {
                console.log('Facility:', res.json);
                this.facilityList = res.json;
            }, (res: ResponseWrapper) => this.onError(res.json));
        // Load Gudang
        // Load Rak
        this.containerService.query()
            .subscribe((res: ResponseWrapper) => {
                console.log('Container:', res.json);
                this.containerList = res.json;
            }, (res: ResponseWrapper) => this.onError(res.json));
        // Load Rak

    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        this.subscribeToSaveResponse(
            this.stockOpnameService.newStockOpname(this.stockOpname, { idInternal: '', idFacility: '', idContainer: '' }));
    }

    protected subscribeToSaveResponse(result: Observable<StockOpname>) {
        result.subscribe((res: StockOpname) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: StockOpname) {
        this.eventManager.broadcast({ name: 'stockOpnameListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'stockOpname saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'stockOpname Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackStandardCalendarById(index: number, item: StandardCalendar) {
        return item.idCalendar;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackStockOpnameTypeById(index: number, item: StockOpnameType) {
        return item.idStockOpnameType;
    }

    trackStockOpnameFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }
}

@Component({
    selector: 'jhi-stock-opname-popup',
    template: ''
})
export class StockOpnamePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected stockOpnamePopupService: StockOpnamePopupService
    ) { }

    ngOnInit() {
        this.stockOpnamePopupService.idCalendar = undefined;
        this.stockOpnamePopupService.idInternal = undefined;
        this.stockOpnamePopupService.idStockOpnameType = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.stockOpnamePopupService
                    .open(StockOpnameDialogComponent as Component, params['id']);
            } else if (params['parent']) {
                // this.stockOpnamePopupService.parent = params['parent'];
                this.stockOpnamePopupService
                    .open(StockOpnameDialogComponent as Component);
            } else {
                this.stockOpnamePopupService
                    .open(StockOpnameDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
