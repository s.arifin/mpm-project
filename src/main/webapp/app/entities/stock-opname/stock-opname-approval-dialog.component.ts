import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { StockOpname } from './stock-opname.model';
import { StockOpnameService } from './stock-opname.service';
import { StockOpnameApprovalPopupService } from './stock-opname-approval-popup.service';
import { ToasterService } from '../../shared';
import { StandardCalendar, StandardCalendarService } from '../standard-calendar';
import { Branch, BranchService } from '../branch';
import { StockOpnameType, StockOpnameTypeService } from '../stock-opname-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-stock-opname-approval-dialog',
    templateUrl: './stock-opname-approval-dialog.component.html'
})
export class StockOpnameApprovalDialogComponent implements OnInit, AfterViewInit {

    protected subscription: Subscription;
    routeSub: any;

    constructor(
        public activeModal: NgbActiveModal,
        protected route: ActivatedRoute,
        protected router: Router,
        protected stockOpnameService: StockOpnameService,
        protected stockOpnameApprovalPopupService: StockOpnameApprovalPopupService
    ) {

    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            console.log('Component:', params);
        });
    }

    ngAfterViewInit() {

        // this returns null
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    updateStatus() {
        this.stockOpnameService.currentItems.subscribe((res) => {
            console.log('Data:', res.currentStatus);
            const status = 122;
            console.log('Move Status:', status);
            // this.stockOpnameService.changeStatus({ 'idStockOpname': res }, status).subscribe((res) => {
            //     console.log('Res:', res);
            // })
            this.router.navigate(['stock-opname']);
            this.activeModal.dismiss('cancel');
        })
    }

}

@Component({
    selector: 'jhi-stock-opname-approval-popup',
    template: ''
})
export class StockOpnameApprovalPopupComponent implements OnInit, OnDestroy {

    routeSub: any;
    params: any;

    constructor(
        protected route: ActivatedRoute,
        protected stockOpnameService: StockOpnameService,
        protected stockOpnameApprovalPopupService: StockOpnameApprovalPopupService
    ) { }

    ngOnInit() {
        // console.log('ViewChild', this.StockOpnameEdit);
        this.routeSub = this.route.params.subscribe((params) => {
            this.params = params;
            console.log('Param:', params);
            if (params['opname']) {
                this.stockOpnameService.find(params['opname']).subscribe((res) => {
                    console.log('Res:', res);
                    this.stockOpnameService.selecItems(res);
                })
                this.stockOpnameApprovalPopupService
                    .open(StockOpnameApprovalDialogComponent as Component, params['opname']);
            } else {
                this.stockOpnameApprovalPopupService
                    .open(StockOpnameApprovalDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
