import { BaseEntity } from './../../shared';

export class StockOpname implements BaseEntity {
    constructor(
        public id?: any,
        public idStockOpname?: any,
        public stockOpnameNumber?: string,
        public dateCreated?: any,
        public calendarId?: any,
        public internalId?: any,
        public stockOpnameTypeId?: any,
        public stockOpnameFacilityId?: any,
        public stockOpnameContainerId?: any,
        public items?: BaseEntity[],
    ) {
    }
}

export class StockOpnameNet {
    constructor(
        public IdStockOpname?: any,
        public StockOpnameNumber?: string,
        public IdInternal?: string,
        public FacilityId?: any,
        public FacilityName?: string,
        public ContainerId?: any,
        public ContainerName?: string,
        public Status?: string,
        public dtCreated?: any,
        public CreatedBy?: string,
        public AssignStatus?: string,
        public TotalItem?: number,
        public TotalChecked?: number,
        public note?: string,
    ) {
    }
}

export class StockOpnamePICMember {
    constructor(
        public IdStockOpname?: any,
        public IdPerson?: any,
        public PersonName?: any,
        public PersonType?: number,
        public IdFacility?: any,
        public IdContainer?: any,
    ) {
    }
}

export class StockOpnameItem {
    constructor(
        public IdItem?: any,
        public IdAssign?: any,
        public IdStockOpname?: any,
        public IdFacility?: any,
        public FacilityName?: string,
        public IdContainer?: any,
        public ContainerName?: string,
        public IdMachineFrame?: string,
        public IdProduct?: string,
        public IdMachine?: string,
        public IdFrame?: string,
        public Color?: string,
        public Year?: string
    ) {

    }
}

export class StockOpnameAssign {
    constructor(
        public idAssign?: any,
        public idStockOpname?: any,
        public stockOpnameNumber?: string,
        public facilityId?: any,
        public facilityName?: string,
        public containerId?: any,
        public containerName?: string,
        public picId?: any,
        public picName?: string,
        public person?: string,
        public memberName?: string,
        public dtCreated?: any,
        public createdBy?: string,
    ) {

    }
}

export class StockOpnameResult {
    constructor(
        public totalItem?: number,
        public sudahCheck?: number,
        public terdaftar?: number,
        public tidakTerdaftar?: number,
        public lebih?: number,
        public kurang?: number,
    ) {

    }
}
