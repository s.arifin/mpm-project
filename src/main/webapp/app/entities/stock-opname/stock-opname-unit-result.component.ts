import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { StockOpname, StockOpnameNet, StockOpnameItem, StockOpnameResult } from './stock-opname.model';
import { StockOpnameService } from './stock-opname.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { FacilityNet, FacilityService } from '../facility';
import { ContainerNet, ContainerService } from '../container';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-stock-opname-unit-result',
    templateUrl: './stock-opname-unit-result.component.html'
})
export class StockOpnameUnitResultComponent implements OnInit, OnDestroy {

    result: StockOpnameResult;
    currentAccount: any;
    stockOpnames: StockOpname[];
    detailStockOpname: StockOpnameNet;
    stockOpnamesNet: StockOpnameNet[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: Boolean = false;
    routeData: any;
    links: any;
    totalItems: any;
    queryCountNotFound: any;
    queryCountChecked: any;
    queryCountExceeded: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    facility: FacilityNet[];
    container: ContainerNet[];
    selectedfacility: any;
    selectedcontainer: any;
    dataweb: any;
    idStockOpname: any;
    statusStockOpname: any;
    stockOpnameItem: StockOpnameItem;
    TextIdFrameMachine: any;
    itemsNotFound: StockOpnameItem[];
    itemsChecked: StockOpnameItem[];
    itemsExceeded: StockOpnameItem[];
    isAssignCompleted: Boolean = false;
    isStockOpnameCompleted: Boolean = false;
    isStockOpnameHold: Boolean = false;
    isNull: Boolean = true;

    protected subscription: Subscription;

    constructor(
        private stockOpnameService: StockOpnameService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private confirmationService: ConfirmationService,
        private report: ReportUtilService,
        private facilityService: FacilityService,
        private containerService: ContainerService,
        protected loadingService: LoadingService,
        protected reportUtilService: ReportUtilService,
    ) {
        this.detailStockOpname = new StockOpnameNet;
        this.result = new StockOpnameResult;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    ngOnInit() {
        // this.principal.identity().then((account) => {
        //     this.currentAccount = account;
        // });
        this.registerChangeInPackageAssociations();
        this.subscription = this.activatedRoute.params.subscribe((params) => {
            // console.log('param id stock opname', params['idstockopname']);
            this.idStockOpname = params['idstockopname'];
            this.statusStockOpname = params['status'];
            this.stockOpnameService.getStockOpname(this.idStockOpname).subscribe(
                (res) => {
                    this.detailStockOpname = res;
                    if (this.detailStockOpname.FacilityId == null) {
                        this.loadStockOpnameFacility();
                    } else {
                        this.isNull = false;
                        this.facility = new Array<FacilityNet>();
                        this.facility.push({idFacility: this.detailStockOpname.FacilityId, description: this.detailStockOpname.FacilityName});
                        this.selectedfacility = this.detailStockOpname.FacilityId;
                        if (this.detailStockOpname.ContainerId == null) {
                            // console.log('container null');
                            this.loadContainer(this.detailStockOpname.FacilityId);
                        } else {
                            // console.log('container not null');
                            this.container = new Array<ContainerNet>();
                            this.container.push({idContainer: this.detailStockOpname.ContainerId, description: this.detailStockOpname.ContainerName});
                            this.selectedcontainer = this.detailStockOpname.ContainerId;
                        }
                    }
                }
            );
        });

        this.loadStockOpnameFacility();
        this.stockOpnameService.getAssignStatus(this.idStockOpname)
        .then((res) => this.isAssignCompleted = res);
        this.stockOpnameService.getStatusHold(this.idStockOpname)
        .then((res) => this.isStockOpnameHold = res);
        this.itemsChecked = new Array<StockOpnameItem>();
        this.stockOpnameService.getStatusCompleted(this.idStockOpname)
            .then((res) => {
                this.isStockOpnameCompleted = res;
                this.loadAll();
            });
    }

    loadAll() {
        if (this.currentSearch) {
            this.stockOpnameService.stockOpnameGetResultByFilter({ // get checked data
                page: this.page - 1,
                size: this.itemsPerPage,
                idstockopname: this.idStockOpname,
                type: 1,
                idfacility: this.selectedfacility,
                idcontainer: this.selectedcontainer
            }).subscribe(
                (res: ResponseWrapper) => {
                    // console.log('Data:', res);
                    this.onSuccessChecked(res.json, res.headers)
                },
                (res: ResponseWrapper) => this.onError(res.json)
            );
            this.stockOpnameService.stockOpnameGetResultByFilter({ // get not found data
                page: this.page - 1,
                size: this.itemsPerPage,
                idstockopname: this.idStockOpname,
                type: 2,
                idfacility: this.selectedfacility,
                idcontainer: this.selectedcontainer
            }).subscribe(
                (res: ResponseWrapper) => {
                    // console.log('Data:', res);
                    this.onSuccessNotFound(res.json, res.headers)
                },
                (res: ResponseWrapper) => this.onError(res.json)
            );
            this.stockOpnameService.stockOpnameGetResultByFilter({ // get exceed data
                page: this.page - 1,
                size: this.itemsPerPage,
                idstockopname: this.idStockOpname,
                type: 3,
                idfacility: this.selectedfacility,
                idcontainer: this.selectedcontainer
            }).subscribe(
                (res: ResponseWrapper) => {
                    // console.log('Exceeded:', res);
                    this.onSuccessExceeded(res.json, res.headers)
                },
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.stockOpnameService.stockOpnameGetResult({ // get checked data
                page: this.page - 1,
                size: this.itemsPerPage,
                idstockopname: this.idStockOpname,
                type: 1
            }).subscribe(
                (res: ResponseWrapper) => {
                    // console.log('Data:', res);
                    this.onSuccessChecked(res.json, res.headers)
                },
                (res: ResponseWrapper) => this.onError(res.json)
            );
            this.stockOpnameService.stockOpnameGetResult({ // get not found data
                page: this.page - 1,
                size: this.itemsPerPage,
                idstockopname: this.idStockOpname,
                type: 2
            }).subscribe(
                (res: ResponseWrapper) => {
                    // console.log('Data:', res);
                    this.onSuccessNotFound(res.json, res.headers)
                },
                (res: ResponseWrapper) => this.onError(res.json)
            );
            this.stockOpnameService.stockOpnameGetResult({ // get exceed data
                page: this.page - 1,
                size: this.itemsPerPage,
                idstockopname: this.idStockOpname,
                type: 3
            }).subscribe(
                (res: ResponseWrapper) => {
                    // console.log('Exceeded:', res);
                    this.onSuccessExceeded(res.json, res.headers)
                },
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
        this.stockOpnameService.getResult(this.idStockOpname).subscribe((res) => this.result = res);
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['stock-opname-unit-result/' + this.idStockOpname], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 1;
        this.currentSearch = false;
        this.router.navigate(['stock-opname-unit-result/' + this.idStockOpname, {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search() {
        this.page = 1;
        if (this.selectedfacility === 'ALL') {
            this.currentSearch = false;
        } else {
            this.currentSearch = true;
            this.router.navigate(['stock-opname-unit-result/' + this.idStockOpname, {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }]);
        }
        this.loadAll();
    }

    back() {
        this.router.navigate(['/stock-opname-unit']);
    }

    upload() {
        this.router.navigate(['/stock-opname-unit-upload/' + this.idStockOpname]);
    }

    FacilityChange(): void {
        this.selectedcontainer = null;
        this.loadContainer(this.selectedfacility);
    }

    loadContainer(selected: any): void {
        if (selected === 'ALL') {
            this.container = new Array<ContainerNet>();
            this.selectedcontainer = 'ALL';
        } else {
            this.containerService.getByFacilityDropdown(selected
                ).subscribe(
                    (res: ResponseWrapper) => {
                        this.container = res.json;
                        this.selectedcontainer = 'ALL';
                    },
                    (res: ResponseWrapper) => this.onError(res.json),
                );
        }
    }

    DeleteItem(DeleteIdItem: StockOpnameItem) {
        this.stockOpnameService.stockOpnameDeleteItem(DeleteIdItem.IdItem).subscribe(
            (res: ResponseWrapper) => {
                this.loadAll();
                this.toasterService.showToaster('info', 'Delete Item', 'Data Deleted');
            },
            (res: ResponseWrapper) => this.toasterService.showToaster('info', 'Delete Item', 'Data Failed to delete'),
        );
        const DeletedIndex = this.itemsChecked.indexOf(DeleteIdItem, 0);
        this.itemsChecked.splice(DeletedIndex, 1);
    }

    AddItem(): void {
        if (this.TextIdFrameMachine) {
            this.isAssignCompleted = false;
            this.stockOpnameItem = new StockOpnameItem();
            this.stockOpnameItem.IdStockOpname = this.idStockOpname;
            this.stockOpnameItem.IdFacility = this.selectedfacility;
            this.stockOpnameItem.IdContainer = this.selectedcontainer;
            this.stockOpnameItem.IdMachineFrame = this.TextIdFrameMachine.replace('MH1', '');

            this.stockOpnameService.stockOpnameAddItemByAuthorized(this.stockOpnameItem, this.principal.getUserLogin()).subscribe(
                (res: ResponseWrapper) => {
                    this.stockOpnameItem = res.json();
                    this.isAssignCompleted = true;
                    this.TextIdFrameMachine = null;
                    this.toasterService.showToaster('info', 'Add Item', 'Data Saved');
                    this.loadAll();
                },
                (res: ResponseWrapper) => {
                    this.isAssignCompleted = true;
                    if (res.status === 404) {
                        this.toasterService.showToaster('danger', 'Add Unit Error', 'Data Tidak Ada di System!');
                    } else {
                        this.toasterService.showToaster('danger', 'Add Unit Error', 'Unit Sudah Ada!');
                    }
                }
            );
        } else {
            this.toasterService.showToaster('danger', 'Add Unit Error', 'Field Inputan Kosong!');
        }
    }

    openEdit() {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to open member/pic to edit?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.stockOpnameService.openEdit(this.idStockOpname, this.principal.getUserLogin().replace(/\./g, '$')).subscribe(
                    (res: ResponseWrapper) => {
                                                this.ngOnInit();
                                                this.loadingService.loadingStop();
                                                },
                    (res: ResponseWrapper) => this.toasterService.showToaster('warning', 'Submit', 'Failed to Save')
                );
            }
        });
    }

    cetak() {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to print BAST?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.stockOpnameService.stockOpnameCompleted(this.idStockOpname, this.principal.getUserLogin().replace(/\./g, '$')).subscribe(
                    (res: ResponseWrapper) => {
                                                this.ngOnInit();
                                                this.print();
                                                this.loadingService.loadingStop();
                                            },
                    (res: ResponseWrapper) => this.toasterService.showToaster('warning', 'Print BAST', 'Internal Server Error!')
                );
            }
        });
    }

    SubmitStockOpname(): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to submit?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                const stockOpname = new StockOpnameNet;
                stockOpname.IdStockOpname = this.idStockOpname;
                stockOpname.CreatedBy = this.principal.getUserLogin();
                stockOpname.note = this.detailStockOpname.note;
                this.stockOpnameService.stockOpnameSubmitResult(stockOpname).subscribe(
                    (res: ResponseWrapper) => {
                                            this.stockOpnameService.getAssignStatus(this.idStockOpname)
                                            .then((a) => this.isAssignCompleted = a);
                                            this.stockOpnameService.getStatusHold(this.idStockOpname)
                                            .then((b) => this.isStockOpnameHold = b);
                                            this.itemsChecked = new Array<StockOpnameItem>();
                                            this.stockOpnameService.getStatusCompleted(this.idStockOpname)
                                                .then((c) => {
                                                    this.isStockOpnameCompleted = c;
                                                    this.loadAll();
                                                });
                                                this.loadingService.loadingStop();
                                            },
                    (res: ResponseWrapper) => {
                        this.loadingService.loadingStop();
                        this.toasterService.showToaster('warning', 'Submit', 'Failed to Save');
                    }
                );
            }
        });
    }

    loadStockOpnameFacility() {
        this.facilityService.getAllStockOpnamebyInternal(this.principal.getIdInternal()).subscribe(
            (res: ResponseWrapper) => {
                this.facility = res.json;
            }
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPackageAssociations() {
        this.eventSubscriber = this.eventManager.subscribe('packageAssociationListModification', (response) => {});
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccessChecked(data, headers) {
        if (data[0] === undefined) {
            this.totalItems = null;
        } else {
            this.totalItems = data[0].TotalData;
        }
        this.queryCountChecked = this.totalItems;
        this.itemsChecked = data;
    }

    private onSuccessNotFound(data, headers) {
        if (data[0] === undefined) {
            this.totalItems = null;
        } else {
            this.totalItems = data[0].TotalData;
        }
        this.queryCountNotFound = this.totalItems;
        this.itemsNotFound = data;
    }

    private onSuccessExceeded(data, headers) {
        if (data[0] === undefined) {
            this.totalItems = null;
        } else {
            this.totalItems = data[0].TotalData;
        }
        this.queryCountExceeded = this.totalItems;
        this.itemsExceeded = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    loadDataLazySubmit(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAllSubmit();
    }

    print() {
        const opnamenumber = this.detailStockOpname.StockOpnameNumber ;
        const user = this.principal.getUserLogin();

        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.viewFile('/api/report/bast_stockopnameunit/pdf', {stockopnamenumber: opnamenumber, username: user});
        this.reportUtilService.downloadFile('/api/report/bast_stockopnameunit/pdf', {stockopnamenumber: opnamenumber, username: user});
        this.reportUtilService.downloadFile('/api/report/bast_stockopnameunit_lampiran1/pdf', {stockopnamenumber: opnamenumber, username: user});
        this.reportUtilService.downloadFile('/api/report/bast_stockopnameunit_lampiran2/pdf', {stockopnamenumber: opnamenumber, username: user});

    }
}
