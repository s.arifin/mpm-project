import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { StockOpname } from './stock-opname.model';
import { StockOpnameService } from './stock-opname.service';

@Injectable()
export class StockOpnameApprovalPopupService {
    protected ngbModalRef: NgbModalRef;
    idCalendar: any;
    idInternal: any;
    idStockOpnameType: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected stockOpnameService: StockOpnameService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.stockOpnameService.find(id).subscribe((data) => {
                    // if (data.dateCreated) {
                    //    data.dateCreated = this.datePipe
                    //        .transform(data.dateCreated, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.stockOpnameModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new StockOpname();
                    data.calendarId = this.idCalendar;
                    data.internalId = this.idInternal;
                    data.stockOpnameTypeId = this.idStockOpnameType;
                    this.ngbModalRef = this.stockOpnameModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    stockOpnameModalRef(component: Component, stockOpname: StockOpname): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.stockOpname = stockOpname;
        modalRef.componentInstance.idCalendar = this.idCalendar;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idStockOpnameType = this.idStockOpnameType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.stockOpnameLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    stockOpnameLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
