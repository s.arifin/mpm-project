import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { StockOpname } from './stock-opname.model';
import { StockOpnameService } from './stock-opname.service';
import { ToasterService } from '../../shared';
// import { ReportUtilService} from '../../shared/report/report-util.service';
import { StandardCalendar, StandardCalendarService } from '../standard-calendar';
import { Internal, InternalService } from '../internal';
import { StockOpnameType, StockOpnameTypeService } from '../stock-opname-type';
import { ResponseWrapper } from '../../shared';
import * as _ from 'lodash';

@Component({
    selector: 'jhi-stock-opname-edit',
    templateUrl: './stock-opname-edit.component.html'
})
export class StockOpnameEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    stockOpname: StockOpname;
    isSaving: boolean;
    idStockOpname: any;
    paramPage: number;
    routeId: number;
    standardcalendars: StandardCalendar[];
    internals: Internal[];
    stockopnametypes: StockOpnameType[];
    idInternal: String;
    namaInternal: any;
    internal: Internal; // dealer
    soItemsInParent: any;
    inputData: any;
    statusOpname: any = 121; // 120 121 122 61 get from currentStatus

    constructor(
        protected alertService: JhiAlertService,
        protected stockOpnameService: StockOpnameService,
        protected standardCalendarService: StandardCalendarService,
        protected internalService: InternalService,
        protected stockOpnameTypeService: StockOpnameTypeService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        //    protected reportUtilService: ReportUtilService,
        protected toaster: ToasterService
    ) {
        this.stockOpname = new StockOpname();
        this.routeId = 0;
        this.idInternal = '10002';
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idStockOpname = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        this.internalService.find(this.idInternal)
            .subscribe((res: Internal) => {
                this.internal = res;
                this.namaInternal = res.organization.name
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.stockOpnameTypeService.query()
            .subscribe((res: ResponseWrapper) => {
                this.stockopnametypes = res.json;
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.standardCalendarService.queryFilterBy({ idCalendarType: 3, idInternal: this.idInternal, size: 9999999 })
            .subscribe((res: ResponseWrapper) => {
                this.standardcalendars = res.json;
            }, (res: ResponseWrapper) => this.onError(res.json));

    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.stockOpnameService.find(this.idStockOpname).subscribe((stockOpname) => {
            console.log('Stock Opname', stockOpname);
            console.log('stockopnametypes', this.stockopnametypes)
            console.log('standardcalendars', this.standardcalendars)
            this.stockOpname = stockOpname;
            this.stockOpname.stockOpnameTypeId = _.cloneDeep(_.find(this.stockopnametypes, { id: this.stockOpname.stockOpnameTypeId }));
            this.stockOpname.calendarId = _.cloneDeep(_.find(this.standardcalendars, { id: this.stockOpname.calendarId }));
            console.log('Type Selceted:', this.stockOpname.stockOpnameTypeId);
            console.log('Calendar Selceted:', this.stockOpname.calendarId);
            // this.statusOpname = stockOpname.currentStatus;
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['stock-opname', { page: this.paramPage }]);
        }
    }

    save() {
        this.isSaving = true;
        if (this.stockOpname.idStockOpname !== undefined) {
            this.subscribeToSaveResponse(
                this.stockOpnameService.update(this.stockOpname));
        } else {
            this.subscribeToSaveResponse(
                this.stockOpnameService.create(this.stockOpname));
        }
    }

    protected subscribeToSaveResponse(result: Observable<StockOpname>) {
        result.subscribe((res: StockOpname) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: StockOpname) {
        this.eventManager.broadcast({ name: 'stockOpnameListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'stockOpname saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'stockOpname Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackStandardCalendarById(index: number, item: StandardCalendar) {
        return item.idCalendar;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackStockOpnameTypeById(index: number, item: StockOpnameType) {
        return item.idStockOpnameType;
    }
    print() {
        this.toaster.showToaster('info', 'Print Data', 'Printing.....');
        //    this.reportUtilService.viewFile('/api/report/sample/pdf');
    }

    getItemData(data) {
        console.log('ItemData:', data);
        this.soItemsInParent = data;
    }

    getInputData(data) {
        console.log('InputData', data)
        this.inputData = data;
    }

    updateStatus() {
        console.log('Data:', this.stockOpname);
        console.log('Status:', this.statusOpname);

        const status = this.statusOpname === 120 ? 121 : this.statusOpname === 122 ? 61 : 17;

        console.log('Next Status:', status);
        // this.stockOpnameService.changeStatus({ 'idStockOpname': this.stockOpname }, status).subscribe((res) => {
        //     console.log('Res:', res);
        // })
        this.router.navigate(['stock-opname']);
    }

    cekUlang() {
        console.log('Data:', this.stockOpname);
        console.log('Status:', this.statusOpname);
        if (this.statusOpname === 121) {
            console.log('Cek Ulang');
            console.log('Back to Status:', 120);
            const status = 120;
            // this.stockOpnameService.changeStatus({ 'idStockOpname': this.statusOpname }, status).subscribe((res) => {
            //     console.log('Res:', res);
            // })
            this.router.navigate(['stock-opname']);
        }
    }
}
