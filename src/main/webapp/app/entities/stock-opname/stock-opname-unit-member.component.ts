import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiPaginationUtil, JhiAlertService } from 'ng-jhipster';
import { LazyLoadEvent, MenuItem } from 'primeng/primeng';
import { StockOpnameNet } from './stock-opname.model';
import { StockOpnameService } from './stock-opname.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-stock-opname-unit-member',
    templateUrl: './stock-opname-unit-member.component.html'
})
export class StockOpnameUnitMemberComponent implements OnInit, OnDestroy {

    currentAccount: any;
    stockOpnames: StockOpnameNet[];

    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    eachId: any;
    isMember: Boolean;

    MenuItem: MenuItem[];

    constructor(
        private stockOpnameService: StockOpnameService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toasterService: ToasterService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChange();
        this.MenuItem = [
            {label: 'Detail', command: (event: any) => { this.detail(this.eachId) }},
            {label: 'Result', command: (event: any) => { this.result(this.eachId) }}
        ];
        this.loadAll();
    }

    loadAll() {
        this.stockOpnameService.getAllbyMember({
            page: this.page - 1,
            size: this.itemsPerPage,
            idinternal: this.principal.getIdInternal(),
            type: 10,
            username: this.principal.getUserLogin()
        }).subscribe(
            (res: ResponseWrapper) => {
                // console.log('Data:', res);
                this.onSuccess(res.json, res.headers)
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/stock-opname-unit-member'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChange() {
        this.eventSubscriber = this.eventManager.subscribe('Modification', (response) => {});
    }

    search(currentSearch): void {

    }

    clear(): void {

    }

    setId(idstokopname: any) {
        this.eachId = idstokopname;
    }

    detail(idstokopname: any) {
        this.router.navigate(['/stock-opname-unit-member/' + idstokopname + '/detail']);
    }

    result(idstokopname: any) {
        this.stockOpnameService.getMemberStatus(idstokopname, this.principal.getUserLogin().replace(/\./g, '$'))
            .then((res) => {
                console.log('res = ', res);
                if (!res) {
                    this.router.navigate(['/stock-opname-unit-pic/' + idstokopname + '/result']);
                } else {
                    this.toasterService.showToaster('warning', 'Caution', 'Just for PIC!');
                }
            });
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.stockOpnames = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    loadDataLazySubmit(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAllSubmit();
    }
}
