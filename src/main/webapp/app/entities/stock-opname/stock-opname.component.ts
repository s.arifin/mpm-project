import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { StockOpname, StockOpnameNet } from './stock-opname.model';
import { StockOpnameService } from './stock-opname.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import { StockOpnameType, StockOpnameTypeService } from '../stock-opname-type';
import { Internal, InternalService } from '../internal';
import { StatusType, StatusTypeService } from '../status-type'

@Component({
    selector: 'jhi-stock-opname',
    templateUrl: './stock-opname.component.html'
})
export class StockOpnameComponent implements OnInit, OnDestroy {

    currentAccount: any;
    stockOpnames: StockOpname[];
    stockOpnamesNet: StockOpnameNet[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    filter = {
        dealer: '',
        from: '',
        to: '',
        stock: '',
        status: '',
        type: ''
    };
    stockopnametypes: StockOpnameType[];
    internal: Internal[]; // dealer
    statusType: StatusType[]; // status
    stockSuggest: any;
    first: number;

    constructor(
        protected stockOpnameService: StockOpnameService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected stockOpnameTypeService: StockOpnameTypeService,
        protected internalService: InternalService,
        protected statusService: StatusTypeService,
        protected toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
    }

    loadAll() {
        // if (this.currentSearch) {
        //     this.stockOpnameService.search({
        //         page: this.page - 1,
        //         query: this.currentSearch,
        //         size: this.itemsPerPage,
        //         sort: this.sort()
        //     }).subscribe(
        //         (res: ResponseWrapper) => {
        //             console.log('Data:', res);
        //             this.onSuccess(res.json, res.headers)
        //         },
        //         (res: ResponseWrapper) => this.onError(res.json)
        //     );
        //     return;
        // }
        this.stockOpnameService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            query: 'idInternal:' + this.principal.getIdInternal() + '|stockOpnameType:10',
        }).subscribe(
            (res: ResponseWrapper) => {
                console.log('Data:', res);
                this.onSuccess(res.json, res.headers)
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/stock-opname'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    search: this.currentSearch,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/stock-opname', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/stock-opname', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInStockOpnames();

        this.stockOpnameTypeService.query()
            .subscribe((res: ResponseWrapper) => {
                console.log('Type:', res);
                this.stockopnametypes = res.json;
            }, (res: ResponseWrapper) => this.onError(res.json));

        this.internalService.query()
            .subscribe((res: ResponseWrapper) => {
                console.log('Internal:', res);
                this.internal = res.json;
                this.filter.dealer = res.json[2].organization.name;
            }, (res: ResponseWrapper) => this.onError(res.json));

        this.statusService.query()
            .subscribe((res: ResponseWrapper) => {
                console.log('Status:', res);
                this.statusType = res.json;
            }, (res: ResponseWrapper) => this.onError(res.json));

    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: StockOpname) {
        return item.idStockOpname;
    }

    registerChangeInStockOpnames() {
        this.eventSubscriber = this.eventManager.subscribe('stockOpnameListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idStockOpname') {
            result.push('idStockOpname');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        // this.totalItems = headers.get('X-Total-Count');
        // this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.queryCount = data[0].TotalData;
        this.stockOpnames = data;
    }

    protected onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.stockOpnameService.executeProcess(item).subscribe(
            (value) => console.log('this: ', value),
            (err) => console.log(err),
            () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.stockOpnameService.update(event.data)
                .subscribe((res: StockOpname) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.stockOpnameService.create(event.data)
                .subscribe((res: StockOpname) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: StockOpname) {
        this.toasterService.showToaster('info', 'StockOpname Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.stockOpnameService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'stockOpnameListModification',
                        content: 'Deleted an stockOpname'
                    });
                });
            }
        });
    }

    process() {
        this.stockOpnameService.process({ idInternal: '10001', idFacility: 'all' }).subscribe((r) => {
            this.eventManager.broadcast({
                name: 'stockOpnameListModification',
                content: 'Stock Opname : ' + r.idStockOpname
            });
            console.log('result', r);
            this.toasterService.showToaster('info', 'Data Processed', 'Stock Opname : ' + r.idStockOpname);
        });
    }

    filterOpname() {
        console.log('Data Filter:', this.filter);
    }

    filterStock(e) {
        console.log('Filter:', e);
        this.stockOpnameService.query({
            query: e.query,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => {
                console.log('Res:', res);
                this.stockSuggest = res.json;
            },
            (res: ResponseWrapper) => {
                console.log('Error:', res);
            }
        );
    }

    public processCutOff(data) {
        console.log('Data:', data);
        const status = 120;
        this.stockOpnameService.changeStatus({ 'idStockOpname': data }, status).subscribe((res) => {
            console.log('Res:', res);
        })
    }

}
