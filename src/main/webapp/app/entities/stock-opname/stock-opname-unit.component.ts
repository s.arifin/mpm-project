import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { StockOpname, StockOpnameNet } from './stock-opname.model';
import { StockOpnameService } from './stock-opname.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { FacilityNet, FacilityService } from '../facility';
import { ContainerNet, ContainerService } from '../container';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-stock-opname-unit',
    templateUrl: './stock-opname-unit.component.html'
})
export class StockOpnameUnitComponent implements OnInit, OnDestroy {

    currentAccount: any;
    stockOpnames: StockOpname[];
    stockOpnamesNet: StockOpnameNet[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    facility: FacilityNet[];
    container: ContainerNet[];
    selectedfacility: {};
    selectedcontainer: {};
    dataweb: any;

    constructor(
        private stockOpnameService: StockOpnameService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private confirmationService: ConfirmationService,
        private report: ReportUtilService,
        private facilityService: FacilityService,
        private containerService: ContainerService,
        protected loadingService: LoadingService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.loadingService.loadingStart();
        this.stockOpnameService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            idinternal: this.principal.getIdInternal(),
            sort: this.sort(),
        }).subscribe(
            (res: ResponseWrapper) => {
                // console.log('Data:', res);
                this.onSuccessGetData(res.json, res.headers)
                this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
                this.loadingService.loadingStop();
            }
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/package-association'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/package-association', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/package-association', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.facilityService.getAllStockOpnamebyInternal(this.principal.getIdInternal()).subscribe(
                (res: ResponseWrapper) => {
                    this.facility = res.json;
                }
            );
        });
        this.registerChangeInPackageAssociations();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPackageAssociations() {
        this.eventSubscriber = this.eventManager.subscribe('packageAssociationListModification', (response) => {});
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccessGetData(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.stockOpnamesNet = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    loadDataLazySubmit(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAllSubmit();
    }

    FacilityChange(): void {
        this.loadContainer();
    }

    loadContainer(): void {
        if (this.selectedfacility === 'ALL') {
            this.container = new Array<ContainerNet>();
        } else {
            this.containerService.getByFacilityDropdown(this.selectedfacility
                ).subscribe(
                    (res: ResponseWrapper) => this.container = res.json,
                    (res: ResponseWrapper) => this.onError(res.json),
                );
        }
    }

    print() {
        this.report.viewFile('/api/report/test/pdf');
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.stockOpnameService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'packageAssociationListModification',
                    content: 'Deleted an packageAssociation'
                    });
                });
            }
        });
    }

    stockopnamecutoff() {
        const paramInput = new StockOpnameNet;
        paramInput.ContainerId = this.selectedcontainer;
        paramInput.FacilityId = this.selectedfacility;
        paramInput.IdInternal = this.principal.getIdInternal();
        paramInput.CreatedBy = this.principal.getUserLogin();
        this.loadingService.loadingStart();
        this.stockOpnameService.unitCutOff(paramInput).subscribe(
            (response: ResponseWrapper) => {
                this.toasterService.showToaster('info', 'StockOpname', 'Cuted Off');
                this.loadingService.loadingStop();
                this.loadAll();
            },
            (response: ResponseWrapper) => {
                console.log('error = ', response.status);
                if (response.status === 404) {
                    this.toasterService.showToaster('danger', 'Error', 'Data unit tidak tersedia!');
                } else {
                    this.toasterService.showToaster('danger', 'Error', 'Data sudah ada!');
                }
                this.loadingService.loadingStop();
            }
        );
    }
}
