import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { StockOpname, StockOpnamePICMember, StockOpnameNet, StockOpnameAssign } from './stock-opname.model';
import { StockOpnameService } from './stock-opname.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { FacilityNet, FacilityService } from '../facility';
import { ContainerNet, ContainerService } from '../container';
import { UserMediatorNet, UserMediatorService } from '../user-mediator';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-stock-opname-unit-assign',
    templateUrl: './stock-opname-unit-assign.component.html'
})
export class StockOpnameUnitAssignComponent implements OnInit, OnDestroy {
    @Input() idstockopname: any;

    currentAccount: any;
    idStockOpname: any;
    stockOpname: StockOpnameNet;
    stockOpnamesAssign: StockOpnameAssign[];
    userMediator: UserMediatorNet[];
    facility: FacilityNet[];
    container: ContainerNet[];
    ideach: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    itemsStockOpnamePerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    printModal: boolean;
    printModalEdit: boolean;
    selectedfacility: {};
    selectedcontainer: {};
    selectedpic: {};
    selectedMember: {};
    stockOpnamePICMember: StockOpnamePICMember[];
    isNull: Boolean = true;

    protected subscription: Subscription;

    constructor(
        private stockOpnameService: StockOpnameService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private confirmationService: ConfirmationService,
        private report: ReportUtilService,
        private containerService: ContainerService,
        private userMediatorService: UserMediatorService,
        private facilityService: FacilityService,
        protected loadingService: LoadingService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.stockOpname = new StockOpnameNet;
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.stockOpnamePICMember = new Array<StockOpnamePICMember>();
    }

    loadAll() {
        if (this.currentSearch) {
            this.stockOpnameService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                id: this.idStockOpname}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.stockOpnameService.getAllAssign({
            page: this.page - 1,
            size: this.itemsPerPage,
            id: this.idStockOpname}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadStockOpnameFacility() {
        this.facilityService.getAllStockOpnamebyInternal(this.principal.getIdInternal()).subscribe(
            (res: ResponseWrapper) => {
                this.facility = res.json;
            }
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/package-association'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/package-association', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/package-association', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });

        this.subscription = this.activatedRoute.params.subscribe((params) => {
            this.idStockOpname = params['idstockopname'];
            this.stockOpnameService.getStockOpname(this.idStockOpname).subscribe(
                (res) => {
                    this.stockOpname = res
                    if (this.stockOpname.FacilityId == null) {
                        this.loadStockOpnameFacility();
                    } else {
                        this.isNull = false;
                        this.facility = new Array<FacilityNet>();
                        this.facility.push({idFacility: this.stockOpname.FacilityId, description: this.stockOpname.FacilityName});
                        this.selectedfacility = this.stockOpname.FacilityId;
                        if (this.stockOpname.ContainerId == null) {
                            // console.log('container null');
                            this.loadContainer(this.stockOpname.FacilityId);
                        } else {
                            // console.log('container not null');
                            this.container = new Array<ContainerNet>();
                            this.container.push({idContainer: this.stockOpname.ContainerId, description: this.stockOpname.ContainerName});
                            this.selectedcontainer = this.stockOpname.ContainerId;
                        }
                    }
                }
            );
        });

        this.userMediatorService.getAllByInternal(this.principal.getIdInternal())
        .subscribe(
            (res: ResponseWrapper) => this.userMediator = res.json,
            (res: ResponseWrapper) => this.onError(res.json),
            );
        this.itemsStockOpnamePerPage = 0;
        this.registerChangeInPackageAssociations();
        this.loadAll();
    }

    back() {
        this.router.navigate(['/stock-opname-unit']);
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    Assign(): void {
        this.loadingService.loadingStart();
        const items = new StockOpnamePICMember();
        items.IdStockOpname = this.idStockOpname;
        items.IdPerson = this.selectedpic;
        items.PersonType = 1;

        this.stockOpnamePICMember.push(items);
        this.stockOpnamePICMember.forEach((PicMemberItem) => {
            PicMemberItem.IdFacility = this.selectedfacility === 'ALL' ? null : this.selectedfacility;
            PicMemberItem.IdContainer = this.selectedcontainer === 'ALL' ? null : this.selectedcontainer;
        });
        this.stockOpnameService.stockOpnameAssignment(this.principal.getUserLogin(), this.stockOpnamePICMember, null).subscribe(
            (res: ResponseWrapper) => {
                this.toasterService.showToaster('info', 'Member Assignment', 'Data Saved');
                this.printModal = false;
                this.selectedfacility = null;
                this.selectedcontainer = null;
                this.selectedpic = null;
                this.stockOpnamePICMember = new Array<StockOpnamePICMember>();
                this.loadingService.loadingStop();
                this.loadAll();
            },
            (res: ResponseWrapper) => {
                console.log('length = ', this.stockOpnamePICMember.length);
                this.stockOpnamePICMember.splice((this.stockOpnamePICMember.length - 1), 1);
                this.toasterService.showToaster('danger', 'Error', 'Assignment Sudah Ada!');
                this.loadingService.loadingStop();
            }
        )
    }

    submitAssign(id: any): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to submit Assignment?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.stockOpnameService.submitAssignment(id).subscribe(
                    (res: ResponseWrapper) => {
                        this.loadingService.loadingStop();
                        this.toasterService.showToaster('info', 'Assignment', 'Submit Success!');
                        this.loadAll();
                    },
                    (res: ResponseWrapper) => {
                        this.loadingService.loadingStop();
                        this.toasterService.showToaster('danger', 'Error', 'Internal Server Error!');
                    }
                )
            }
        });
    }

    FacilityChange(): void {
        this.loadContainer(this.selectedfacility);
    }

    deleteMember(user: StockOpnamePICMember): void {
        const DeletedIndex = this.stockOpnamePICMember.indexOf(user, 0);
        this.stockOpnamePICMember.splice(DeletedIndex, 1);
    }

    addMember(): void {
        if (this.selectedMember === this.selectedpic) {
            this.toasterService.showToaster('danger', 'Error', 'User Already in PIC!');
        } else {
            const a = this.stockOpnamePICMember.find((element) => {
                return (element.IdPerson === this.selectedMember);
            });

            if (a === undefined) {
                const items = new StockOpnamePICMember();
                items.IdStockOpname = this.idStockOpname;
                items.IdPerson = this.selectedMember;
                items.PersonType = 2;

                const selectedData = this.userMediator.find(
                    (x) =>
                    x.idperson === this.selectedMember
                );

                items.PersonName = selectedData.name;
                this.itemsStockOpnamePerPage = this.itemsStockOpnamePerPage + 1;

                this.stockOpnamePICMember.push(items);
                this.selectedMember = null;
            } else {
                this.toasterService.showToaster('danger', 'Error', 'User Already in Member!');
            }
        }
    }

    loadContainer(selected: any): void {
        if (selected === 'ALL') {
            this.container = new Array<ContainerNet>();
        } else {
            this.containerService.getByFacilityDropdown(selected
                ).subscribe(
                    (res: ResponseWrapper) => this.container = res.json,
                    (res: ResponseWrapper) => this.onError(res.json),
                );
        }
    }

    registerChangeInPackageAssociations() {
        this.eventSubscriber = this.eventManager.subscribe('packageAssociationListModification', (response) => {});
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        if (data[0] === undefined) {
            this.totalItems = null;
        } else {
            this.totalItems = data[0].TotalData;
        }
        this.queryCount = this.totalItems;
        this.stockOpnamesAssign = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    loadDataLazySubmit(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAllSubmit();
    }

    showPrintAssign() {
        this.stockOpnamePICMember = new Array<StockOpnamePICMember>();
        this.selectedcontainer = null;
        this.selectedfacility = null;
        this.selectedMember = null;
        this.printModal = true;
    }

    showPrintEdit(id: any) {
        this.selectedcontainer = null;
        this.selectedfacility = null;
        this.selectedMember = null;
        this.ideach = id;
        this.stockOpnameService.getAssign(id).subscribe(
            (res: ResponseWrapper) => {
                    this.stockOpnamePICMember = new Array<StockOpnamePICMember>();
                    this.stockOpnamePICMember = res.json;
                    this.selectedpic = this.stockOpnamePICMember[0].IdPerson;
                    this.selectedfacility = this.stockOpnamePICMember[0].IdFacility;
                    this.loadContainer(this.selectedfacility);
                    this.selectedcontainer = this.stockOpnamePICMember[0].IdContainer;
                    this.stockOpnamePICMember.splice(0, 1);
                    this.printModalEdit = true;
                },
            (res: ResponseWrapper) => {
                this.toasterService.showToaster('danger', 'Error', 'Internal Server Error!');
            }
        )
    }

    editAssign() {
        const items = new StockOpnamePICMember();
        items.IdStockOpname = this.idStockOpname;
        items.IdPerson = this.selectedpic;
        items.PersonType = 1;

        this.stockOpnamePICMember.push(items);
        this.stockOpnamePICMember.forEach((PicMemberItem) => {
            PicMemberItem.IdFacility = this.selectedfacility === 'ALL' ? null : this.selectedfacility;
            PicMemberItem.IdContainer = this.selectedcontainer === 'ALL' ? null : this.selectedcontainer;
        });
        this.stockOpnameService.stockOpnameAssignment(this.principal.getUserLogin(), this.stockOpnamePICMember, this.ideach).subscribe(
            (res: ResponseWrapper) => {
                    this.toasterService.showToaster('info', 'Member Assignment', 'Data Saved');
                    this.printModalEdit = false;
                    this.selectedfacility = null;
                    this.selectedcontainer = null;
                    this.selectedpic = null;
                    this.stockOpnamePICMember = new Array<StockOpnamePICMember>();
                    this.loadAll();
                },
            (res: ResponseWrapper) => {
                if (res.status === 404) {
                    this.toasterService.showToaster('danger', 'Error', 'Data Tidak Ada di System!');
                } else {
                    this.toasterService.showToaster('danger', 'Error', 'Assignment Sudah Ada!');
                }
            }
        )
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to cancel PIC and Member?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart()
                this.stockOpnameService.deleteAssign(id).subscribe(
                    (response) => {
                                        this.loadingService.loadingStop();
                                        this.toasterService.showToaster('success', 'Assign', 'Assignment Deleted');
                                        this.loadAll();
                                    },
                    (response) => {
                                        this.loadingService.loadingStop();
                                        this.toasterService.showToaster('danger', 'Error', 'Internal Server Error');
                                    }
                );
            }
        });
    }
}
