import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiAlertService } from 'ng-jhipster';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { StockOpnameNet, StockOpnameItem, StockOpnameAssign } from './stock-opname.model';
import { StockOpnameService } from './stock-opname.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-stock-opname-unit-member-input',
    templateUrl: './stock-opname-unit-member-input.component.html'
})
export class StockOpnameUnitMemberInputComponent implements OnInit, OnDestroy {

    @ViewChild('inputNoka') inputNoka: ElementRef;
    currentAccount: any;
    assignData: StockOpnameAssign;
    unitDataNotSave: StockOpnameNet[];
    unitDataSave: StockOpnameNet[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    queryCountSave: any;
    queryCountNotSave: any;
    totalItemsSave: any;
    totalItemsNotSave: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    preview: Boolean = false;
    isSave: Boolean = false;
    isOpen: Boolean = true;
    totalData: number;

    idAssign: any;
    statusStockOpname: any;
    stockOpnameItem: StockOpnameItem;
    TextIdFrameMachine: any;

    protected subscription: Subscription;

    constructor(
        private stockOpnameService: StockOpnameService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toasterService: ToasterService,
        private confirmationService: ConfirmationService,
        protected loadingService: LoadingService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.assignData = new StockOpnameAssign;
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.stockOpnameService.getUnitByAssign({ // save
            page: this.page,
            size: this.itemsPerPage}, this.idAssign, 1).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccessSave(res.json, res.headers);
                this.stockOpnameService.getUnitByAssign({ // not save
                    page: this.page,
                    size: this.itemsPerPage}, this.idAssign, 0).subscribe(
                    (a: ResponseWrapper) => this.onSuccessNotSave(a.json, a.headers),
                    (a: ResponseWrapper) => this.onError(a.json)
                );
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/package-association'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/package-association', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/package-association', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.registerChange();
        this.subscription = this.activatedRoute.params.subscribe((params) => {
            // console.log('param id Assign', params['idassign']);
            this.idAssign = params['idassign'];
        });
        this.stockOpnameService.getOpenStatus(this.idAssign)
            .then((res) => this.isOpen = res);
        this.stockOpnameService.getAssignByMember(this.idAssign).subscribe(
            (res) => {
                        this.assignData = res;
                        // console.log('assign data = ', this.assignData);
                    }
        );
        this.loadAll();
    }

    back() {
        this.router.navigate(['/stock-opname-unit-member/' + this.assignData.idStockOpname + '/detail']);
    }

    AddItem(): void {
        this.isOpen = false;
        if (this.TextIdFrameMachine) {
            this.stockOpnameItem = new StockOpnameItem();
            this.stockOpnameItem.IdStockOpname = this.assignData.idStockOpname;
            this.stockOpnameItem.IdFacility = this.assignData.facilityId;
            this.stockOpnameItem.IdContainer = this.assignData.containerId;
            this.stockOpnameItem.IdMachineFrame = this.TextIdFrameMachine.replace('MH1', '');
            this.stockOpnameItem.IdAssign = this.idAssign;

            // console.log('stockOpnameItem', this.stockOpnameItem);
            this.stockOpnameService.stockOpnameAddItem(this.stockOpnameItem, this.principal.getUserLogin()).subscribe(
                (res: ResponseWrapper) => {
                    this.isOpen = true;
                    this.TextIdFrameMachine = null;
                    // this.stockOpnameItemsChecked.push(this.stockOpnameItem);
                    // console.log('stockOpnameItemResult :', this.stockOpnameItem);
                    this.toasterService.showToaster('info', 'Add Item', 'Data Saved');
                    this.loadAll();
                },
                (res: ResponseWrapper) => {
                    this.isOpen = true;
                    if (res.status === 404) {
                        this.toasterService.showToaster('danger', 'Add Unit Error', 'Data Tidak Ada di System!');
                    } else {
                        this.toasterService.showToaster('danger', 'Add Unit Error', 'Unit Sudah Ada!');
                    }
                }
            );
        } else {
            this.isOpen = true;
            this.toasterService.showToaster('danger', 'Add Unit Error', 'Field Inputan Kosong!');
        }
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChange() {
        this.eventSubscriber = this.eventManager.subscribe('packageAssociationListModification', (response) => {});
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccessSave(data, headers) {
        this.unitDataSave = new Array<StockOpnameItem>();
        // this.totalItemsSave = headers.get('X-Total-Count');
        if (data[0] === undefined) {
            this.totalItemsSave = '0';
        } else {
            this.totalItemsSave = data[0].TotalData;
        }
        this.queryCountSave = this.totalItemsSave;
        this.unitDataSave = data;
    }

    private onSuccessNotSave(data, headers) {
        this.unitDataNotSave = new Array<StockOpnameItem>();
        // this.totalItemsNotSave = headers.get('X-Total-Count');
        if (data[0] === undefined) {
            this.totalItemsNotSave = '0';
        } else {
            this.totalItemsNotSave = data[0].TotalData;
        }
        this.queryCountNotSave = this.totalItemsNotSave;
        this.unitDataNotSave = data;

        // this.totalData = Number(this.queryCountNotSave) + Number(this.queryCountSave);
        const save: number = Number(this.queryCountSave);
        const notSave: number = Number(this.queryCountNotSave);
        this.totalData = save + notSave;

        if (notSave === 0) {
            this.isSave = true;
        } else {
            this.isSave = false;
        }

        // console.log('save = ', save);

        // console.log('notsave = ', notSave);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    loadDataLazySubmit(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAllSubmit();
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.stockOpnameService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'stockOpnameItemDeleted',
                        content: 'Deleted an Stock Opname Item'
                        });
                    this.loadAll();
                });
            }
        });
    }

    scan() {
        this.inputNoka.nativeElement.focus();
    }

    seePreview() {
        this.preview = this.preview ? false : true;
    }

    save() {
        this.loadingService.loadingStart();
        this.stockOpnameService.savebymember(this.idAssign).subscribe(
            (res) => {
                this.toasterService.showToaster('info', 'Save Success', this.queryCountNotSave + ' data unit berhasil disimpan!');
                this.loadAll();
                this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.toasterService.showToaster('warning', 'Submit', 'Failed to Save!');
                this.loadingService.loadingStop();
            }
        );
    }

    complete() {
        const assign = new StockOpnameAssign();
        assign.idAssign = this.idAssign;
        this.confirmationService.confirm({
            message: 'Are you sure?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.stockOpnameService.setHold(assign).subscribe(
                    (res) => {
                        this.loadingService.loadingStop();
                        this.back();
                    },
                    (res: ResponseWrapper) => {
                        this.loadingService.loadingStop();
                        this.toasterService.showToaster('warning', 'Submit', 'Failed to Complete!');
                    }
                );
            }
        });
    }
}
