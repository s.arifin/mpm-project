import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { StockOpnameComponent } from './stock-opname.component';
import { StockOpnameEditComponent } from './stock-opname-edit.component';
import { StockOpnamePopupComponent } from './stock-opname-dialog.component';
import { StockOpnameApprovalPopupComponent } from './stock-opname-approval-dialog.component';
import { StockOpnameUploadComponent } from './stock-opname-upload.component';
import { StockOpnameUnitComponent } from './stock-opname-unit.component';
import { StockOpnameUnitMemberComponent } from './stock-opname-unit-member.component';
import { StockOpnameUnitMemberDetailComponent } from './stock-opname-unit-member-detail.component';
import { StockOpnameUnitMemberInputComponent } from './stock-opname-unit-member-input.component';
import { StockOpnameUnitPICComponent } from './stock-opname-unit-pic.component';
import { StockOpnameUnitAssignComponent } from './stock-opname-unit-assign.component';
import { StockOpnameUnitDetailComponent } from './stock-opname-unit-detail.component';
import { StockOpnameUnitResultComponent } from './stock-opname-unit-result.component';
import { StockOpnameUnitUploadComponent } from './stock-opname-unit-upload.component';

@Injectable()
export class StockOpnameResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idStockOpname,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const stockOpnameRoute: Routes = [
    {
        path: 'stock-opname',
        component: StockOpnameComponent,
        resolve: {
            'pagingParams': StockOpnameResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stock-opname-unit',
        component: StockOpnameUnitComponent,
        resolve: {
            'pagingParams': StockOpnameResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stock-opname-unit-member',
        component: StockOpnameUnitMemberComponent,
        resolve: {
            'pagingParams': StockOpnameResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stock-opname-unit-member/:idstockopname/detail',
        component: StockOpnameUnitMemberDetailComponent,
        resolve: {
            'pagingParams': StockOpnameResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stock-opname-unit-member/:idassign/input',
        component: StockOpnameUnitMemberInputComponent,
        resolve: {
            'pagingParams': StockOpnameResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stock-opname-unit-pic/:idstockopname/result',
        component: StockOpnameUnitPICComponent,
        resolve: {
            'pagingParams': StockOpnameResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stock-opname-unit-assign/:idstockopname/assign',
        component: StockOpnameUnitAssignComponent,
        resolve: {
            'pagingParams': StockOpnameResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stock-opname-unit-detail/:idstockopname/detail',
        component: StockOpnameUnitDetailComponent,
        resolve: {
            'pagingParams': StockOpnameResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stock-opname-unit-result/:idstockopname',
        component: StockOpnameUnitResultComponent,
        resolve: {
            'pagingParams': StockOpnameResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stock-opname-unit-upload/:idstockopname',
        component: StockOpnameUnitUploadComponent,
        resolve: {
            'pagingParams': StockOpnameResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const stockOpnamePopupRoute: Routes = [
    {
        path: 'stock-opname-popup-approval/:opname',
        component: StockOpnameApprovalPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stock-opname-popup-new',
        component: StockOpnamePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stock-opname-new',
        component: StockOpnameEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'stock-opname/:id/edit',
        component: StockOpnameEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'stock-opname/:id/upload',
        component: StockOpnameUploadComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'stock-opname/:route/:page/:id/edit',
        component: StockOpnameEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'stock-opname/:id/popup-edit',
        component: StockOpnamePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpname.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
