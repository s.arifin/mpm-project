import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { StockOpnameItem } from './stock-opname.model';
import { StockOpnameService } from './stock-opname.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ConvertUtilService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService, FileUploadModule} from 'primeng/primeng';

import { LoadingService } from '../../layouts/loading/loading.service';

import * as XLSX from 'xlsx';
import { calendarFormat, localeData } from 'moment';

@Component({
    selector: 'jhi-stock-opname-unit-upload',
    templateUrl: './stock-opname-unit-upload.component.html'
})
export class StockOpnameUnitUploadComponent implements OnInit, OnDestroy {

    itemUpload: StockOpnameItem[];
    item: StockOpnameItem;
    idStockOpname: any;
    isSaving: boolean;
    invalidFormat: Boolean;
    isCompleted: Boolean;
    summaryMessage: String;
    total: any;
    korsal: any;
    currentAccount: any;
    error: any;
    success: any;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    successSave: number;
    errorSave: number;

    uploadData: any;

    protected subscription: Subscription;

    constructor(
        protected alertService: JhiAlertService,
        protected stockOpnameService: StockOpnameService,
        protected route: ActivatedRoute,
        protected router: Router,
        private principal: Principal,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected loadingService: LoadingService,
        protected convertUtilService: ConvertUtilService
    ) {
        this.itemUpload = new Array<StockOpnameItem>();
        this.itemsPerPage = ITEMS_PER_PAGE;
    }

    ngOnInit() {
        this.subscription = this.activatedRoute.params.subscribe((params) => {
            // console.log('param id stock opname', params['idstockopname']);
            this.idStockOpname = params['idstockopname'];
        });
        this.invalidFormat = false;
        this.isCompleted = false;
    }

    ngOnDestroy() {
    }

    previousState() {
        this.router.navigate(['../stock-opname-unit-result/' + this.idStockOpname]);
    }

    backToPreviousState() {
        this.isCompleted = false;
        this.previousState();
    }

    save() {
        this.loadingService.loadingStart();
        this.isSaving = true;
        setTimeout(
            () => {
                this.stockOpnameService.uploadUnit(this.idStockOpname, this.principal.getUserLogin().replace(/\./g, '$'), this.itemUpload).subscribe(
                    (res) => {
                            this.errorSave = res.length;
                            this.successSave = this.itemUpload.length - this.errorSave;
                            this.itemUpload = res;
                            this.isCompleted = true;
                            this.loadingService.loadingStop();
                        },
                    (res) => {
                        this.toasterService.showToaster('info', 'Error Upload', 'Internal Server Error!')
                        this.loadingService.loadingStop();
                    }
                );
            }, 1000);
    }

    onTxtFileChange(event: any): void {

        this.itemUpload = new Array<StockOpnameItem>();

        this.convertUtilService.csvToArray(event, ';').then(
            (result) => {
                for (let i = 0; i < result.length; i++) {
                    const each = result[i];

                    this.item = new StockOpnameItem();
                    this.item.IdFrame = each[0];
                    this.item.FacilityName = each[1];
                    this.item.ContainerName = each[2];
                    this.item.IdStockOpname = this.idStockOpname;

                    this.itemUpload = [...this.itemUpload, this.item];
                }

            },
            (rej) => {
                console.log('reject');
            }
        )
    }

    protected convertXLXStoList(): void {
        this.itemUpload = new Array<StockOpnameItem>();

        this.uploadData.forEach( (element) => {
            this.item = new StockOpnameItem();
            this.item.IdFrame = element['idframe'];
            this.item.FacilityName = element['facilityname'];
            this.item.ContainerName = element['containername'];
            this.item.IdStockOpname = this.idStockOpname;
            this.itemUpload = [...this.itemUpload, this.item];
        });
    }

    onFileChange(evt: any) {
        /* wire up file reader */
        const target: DataTransfer = <DataTransfer>(evt.target);
        if (target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
          /* read workbooka */
          const bstr: string = e.target.result;
          const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});
          /* grab first sheet */
          const wsname: string = wb.SheetNames[0];
          const ws: XLSX.WorkSheet = wb.Sheets[wsname];
          /* save data */
          this.uploadData = (XLSX.utils.sheet_to_json(ws));

          this.convertXLXStoList();
        };
        reader.readAsBinaryString(target.files[0]);
      }
}
