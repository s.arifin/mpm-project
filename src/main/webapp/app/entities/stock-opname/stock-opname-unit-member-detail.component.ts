import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { LazyLoadEvent } from 'primeng/primeng';
import { StockOpnameAssign } from './stock-opname.model';
import { StockOpnameService } from './stock-opname.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-stock-opname-unit-member-detail',
    templateUrl: './stock-opname-unit-member-detail.component.html'
})
export class StockOpnameUnitMemberDetailComponent implements OnInit, OnDestroy {
    @Input()

    currentAccount: any;
    idStockOpname: any;
    stockOpnamesAssign: StockOpnameAssign[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    each: any;

    protected subscription: Subscription;

    constructor(
        private stockOpnameService: StockOpnameService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.stockOpnameService.getAllAssignByMember({
            page: this.page - 1,
            size: this.itemsPerPage,
            username: this.principal.getUserLogin(),
            id: this.idStockOpname}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/stock-opname-unit-member/' + this.idStockOpname + '/detail'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/stock-opname-unit-member/' + this.idStockOpname + '/detail', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/stock-opname-unit-member/' + this.idStockOpname + '/detail', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });

        this.subscription = this.activatedRoute.params.subscribe((params) => {
            this.idStockOpname = params['idstockopname'];
        });

        this.registerChangeInPackageAssociations();
        this.loadAll();
    }

    input(idAssign: any) {
        this.router.navigate(['/stock-opname-unit-member/' + idAssign + '/input']);
    }

    back() {
        this.router.navigate(['/stock-opname-unit-member']);
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPackageAssociations() {
        this.eventSubscriber = this.eventManager.subscribe('packageAssociationListModification', (response) => {});
    }

    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.stockOpnamesAssign = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    loadDataLazySubmit(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAllSubmit();
    }
}
