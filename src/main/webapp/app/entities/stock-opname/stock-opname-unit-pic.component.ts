import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { StockOpnameAssign, StockOpnameNet, StockOpnameItem } from './stock-opname.model';
import { StockOpnameService } from './stock-opname.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-stock-opname-unit-pic',
    templateUrl: './stock-opname-unit-pic.component.html'
})
export class StockOpnameUnitPICComponent implements OnInit, OnDestroy {
    @Input()

    currentAccount: any;
    idStockOpname: any;
    stockOpname: StockOpnameNet;
    detailAssign: StockOpnameAssign;
    stockOpnamesAssign: StockOpnameAssign[];
    stockOpnamesItem: StockOpnameItem[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    queryCountUnit: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    each: any;
    isDetail: Boolean= false;
    isCompleted: Boolean= false;
    idAssign: any;

    protected subscription: Subscription;

    constructor(
        private stockOpnameService: StockOpnameService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toasterService: ToasterService,
        protected loadingService: LoadingService,
        private confirmationService: ConfirmationService,
    ) {
        this.stockOpname = new StockOpnameNet;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });

        this.subscription = this.activatedRoute.params.subscribe((params) => {
            this.idStockOpname = params['idstockopname'];
            this.stockOpnameService.getStockOpname(this.idStockOpname).subscribe((res) => this.stockOpname = res);
        });

        this.stockOpnameService.getMemberCompletedStatus(this.idStockOpname, this.principal.getUserLogin().replace(/\./g, '$'))
            .then((res) => this.isCompleted = res);

        this.registerChange();
        this.loadAll();
    }

    loadAll() {
        if (this.isDetail) {
            this.stockOpnameService.getUnitByAssign({
                page: this.page,
                size: this.itemsPerPage}, this.idAssign, 1).subscribe(
                (res: ResponseWrapper) => this.onSuccessUnit(res.json),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.stockOpnameService.getAllAssignByPIC({
                page: this.page - 1,
                size: this.itemsPerPage,
                username: this.principal.getUserLogin(),
                id: this.idStockOpname}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    loadUnit(idAssign: any) {
        this.stockOpnameService.getUnitByAssign({
            page: this.page - 1,
            size: this.itemsPerPage}, idAssign).subscribe(
            (res: ResponseWrapper) => this.onSuccessUnit(res.json),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/stock-opname-unit-pic/' + this.idStockOpname + '/result'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/stock-opname-unit-pic/' + this.idStockOpname + '/result', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/stock-opname-unit-pic/' + this.idStockOpname + '/result', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    detail(idAssign: any) {
        this.stockOpnameService.getAssignByMember(idAssign).subscribe(
            (res) => {
                this.detailAssign = res;
                this.isDetail = true;
                this.idAssign = idAssign;
                this.loadAll();
            }
        );
    }

    submit() {
        this.confirmationService.confirm({
            message: 'Are you sure?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.stockOpnameService.setPICCompleted(this.stockOpnamesAssign).subscribe(
                    (res: ResponseWrapper) => {
                            this.loadingService.loadingStop();
                            this.toasterService.showToaster('info', 'Submit Assignment', 'Submit Success');
                            this.isCompleted = false;
                            this.loadAll();
                        },
                    (res: ResponseWrapper) => {
                        this.loadingService.loadingStop();
                        this.toasterService.showToaster('warning', 'Submit Assignment', 'Failed to Save');
                    }
                )
            }
        });
    }

    close() {
        this.stockOpnamesItem = new Array<StockOpnameItem>();
        this.queryCountUnit = null;
        this.isDetail = false;
    }

    back() {
        this.router.navigate(['/stock-opname-unit-member/']);
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChange() {
        this.eventSubscriber = this.eventManager.subscribe('packageAssociationListModification', (response) => {});
    }

    private onSuccess(data) {
        if (data[0] === undefined) {
            this.totalItems = null;
        } else {
            this.totalItems = data[0].TotalData;
        }
        this.queryCount = this.totalItems;
        this.stockOpnamesAssign = data;
    }

    private onSuccessUnit(data) {
        if (data[0] === undefined) {
            this.totalItems = null;
        } else {
            this.totalItems = data[0].TotalData;
        }
        this.queryCountUnit = this.totalItems;
        this.stockOpnamesItem = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    loadDataLazySubmit(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAllSubmit();
    }
}
