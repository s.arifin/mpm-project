import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ApprovalKacabRequest } from './approval-kacab-request.model';
import { ApprovalKacabRequestService } from './approval-kacab-request.service';

@Injectable()
export class ApprovalKacabRequestPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private approvalKacabRequestService: ApprovalKacabRequestService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.approvalKacabRequestService.find(id).subscribe((approvalKacabRequest) => {
                    this.ngbModalRef = this.approvalKacabRequestModalRef(component, approvalKacabRequest);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.approvalKacabRequestModalRef(component, new ApprovalKacabRequest());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    approvalKacabRequestModalRef(component: Component, approvalKacabRequest: ApprovalKacabRequest): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.approvalKacabRequest = approvalKacabRequest;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
