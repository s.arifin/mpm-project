import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ApprovalKacabRequest } from './approval-kacab-request.model';
import { ApprovalKacabRequestPopupService } from './approval-kacab-request-popup.service';
import { ApprovalKacabRequestService } from './approval-kacab-request.service';

@Component({
    selector: 'jhi-approval-kacab-request-delete-dialog',
    templateUrl: './approval-kacab-request-delete-dialog.component.html'
})
export class ApprovalKacabRequestDeleteDialogComponent {

    approvalKacabRequest: ApprovalKacabRequest;

    constructor(
        private approvalKacabRequestService: ApprovalKacabRequestService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.approvalKacabRequestService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'approvalKacabRequestListModification',
                content: 'Deleted an approvalKacabRequest'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-approval-kacab-request-delete-popup',
    template: ''
})
export class ApprovalKacabRequestDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private approvalKacabRequestPopupService: ApprovalKacabRequestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.approvalKacabRequestPopupService
                .open(ApprovalKacabRequestDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
