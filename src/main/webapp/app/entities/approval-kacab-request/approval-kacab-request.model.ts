import { BaseEntity } from './../../shared';

export class ApprovalKacabRequest implements BaseEntity {
    constructor(
        public id?: number,
    ) {
    }
}
