import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ApprovalKacabRequest } from './approval-kacab-request.model';
import { ApprovalKacabRequestPopupService } from './approval-kacab-request-popup.service';
import { ApprovalKacabRequestService } from './approval-kacab-request.service';

@Component({
    selector: 'jhi-approval-kacab-request-dialog',
    templateUrl: './approval-kacab-request-dialog.component.html'
})
export class ApprovalKacabRequestDialogComponent implements OnInit {

    approvalKacabRequest: ApprovalKacabRequest;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private approvalKacabRequestService: ApprovalKacabRequestService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.approvalKacabRequest.id !== undefined) {
            this.subscribeToSaveResponse(
                this.approvalKacabRequestService.update(this.approvalKacabRequest));
        } else {
            this.subscribeToSaveResponse(
                this.approvalKacabRequestService.create(this.approvalKacabRequest));
        }
    }

    private subscribeToSaveResponse(result: Observable<ApprovalKacabRequest>) {
        result.subscribe((res: ApprovalKacabRequest) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ApprovalKacabRequest) {
        this.eventManager.broadcast({ name: 'approvalKacabRequestListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-approval-kacab-request-popup',
    template: ''
})
export class ApprovalKacabRequestPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private approvalKacabRequestPopupService: ApprovalKacabRequestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.approvalKacabRequestPopupService
                    .open(ApprovalKacabRequestDialogComponent as Component, params['id']);
            } else {
                this.approvalKacabRequestPopupService
                    .open(ApprovalKacabRequestDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
