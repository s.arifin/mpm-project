import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ApprovalKacabRequest } from './approval-kacab-request.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ApprovalKacabRequestService {

    private resourceUrl = SERVER_API_URL + 'api/approval-kacab-requests';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/approval-kacab-request';
    protected resourceCUrl = process.env.API_C_URL + '/api/requestNote';
    protected dummy = 'http://localhost:52374/api/requestNote';

    valueUnitDocumentMessage = {};
    protected enheritancedata = new BehaviorSubject<any>(this.valueUnitDocumentMessage);
    passingData = this.enheritancedata.asObservable();
    constructor(private http: Http) { }

    create(approvalKacabRequest: ApprovalKacabRequest): Observable<ApprovalKacabRequest> {
        const copy = this.convert(approvalKacabRequest);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(approvalKacabRequest: ApprovalKacabRequest): Observable<ApprovalKacabRequest> {
        const copy = this.convert(approvalKacabRequest);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<ApprovalKacabRequest> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterByinternalCdua(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/request`, options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryDetail(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/requestDetail`, options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryApprove(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/approveRequest`, options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryReject(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/notApproveRequest`, options)
            .map((res: Response) => this.convertResponse(res));
    }
    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(approvalKacabRequest: ApprovalKacabRequest): ApprovalKacabRequest {
        const copy: ApprovalKacabRequest = Object.assign({}, approvalKacabRequest);
        return copy;
    }

    passingCustomData(custom) {
        this.enheritancedata.next(custom);
    }
}
