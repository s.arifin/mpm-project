import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    ApprovalKacabRequestService,
    ApprovalKacabRequestPopupService,
    ApprovalKacabRequestComponent,
    ApprovalKacabRequestDetailComponent,
    ApprovalKacabRequestDialogComponent,
    ApprovalKacabRequestPopupComponent,
    ApprovalKacabRequestDeletePopupComponent,
    ApprovalKacabRequestDeleteDialogComponent,
    approvalKacabRequestRoute,
    approvalKacabRequestPopupRoute,
    ApprovalKacabRequestResolvePagingParams,
    ApprovalKacabRequestApprovedComponent,
    ApprovalKacabRequestNotApprovedComponent,
    ApprovalKacabRequestNewComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
    MessagesModule,
    CheckboxModule,
    InputTextModule,
    InputTextareaModule,
    CalendarModule,
    DropdownModule,
    EditorModule,
    ButtonModule,
    DataTableModule,
    DataListModule,
    DataGridModule,
    DataScrollerModule,
    CarouselModule,
    PickListModule,
    PaginatorModule,
    DialogModule,
    ConfirmDialogModule,
    ConfirmationService,
    GrowlModule,
    SharedModule,
    AccordionModule,
    TabViewModule,
    FieldsetModule,
    ScheduleModule,
    PanelModule,
    ListboxModule,
    ChartModule,
    DragDropModule,
    LightboxModule,
    RadioButtonModule
   } from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';
import { MessagePipe } from '../../shared/pipe/message.pipe';

const ENTITY_STATES = [
    ...approvalKacabRequestRoute,
    ...approvalKacabRequestPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        MessagesModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        RadioButtonModule
    ],
    declarations: [
        ApprovalKacabRequestComponent,
        ApprovalKacabRequestDetailComponent,
        ApprovalKacabRequestDialogComponent,
        ApprovalKacabRequestDeleteDialogComponent,
        ApprovalKacabRequestPopupComponent,
        ApprovalKacabRequestDeletePopupComponent,
        ApprovalKacabRequestApprovedComponent,
        ApprovalKacabRequestNotApprovedComponent,
        ApprovalKacabRequestNewComponent,
    ],
    entryComponents: [
        ApprovalKacabRequestComponent,
        ApprovalKacabRequestDialogComponent,
        ApprovalKacabRequestPopupComponent,
        ApprovalKacabRequestDeleteDialogComponent,
        ApprovalKacabRequestDeletePopupComponent,
        ApprovalKacabRequestApprovedComponent,
        ApprovalKacabRequestNotApprovedComponent,
        ApprovalKacabRequestNewComponent,
    ],
    providers: [
        ApprovalKacabRequestService,
        ApprovalKacabRequestPopupService,
        ApprovalKacabRequestResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmApprovalKacabRequestModule {}
