import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ApprovalKacabRequestComponent } from './approval-kacab-request.component';
import { ApprovalKacabRequestDetailComponent } from './approval-kacab-request-detail.component';
import { ApprovalKacabRequestPopupComponent } from './approval-kacab-request-dialog.component';
import { ApprovalKacabRequestDeletePopupComponent } from './approval-kacab-request-delete-dialog.component';

@Injectable()
export class ApprovalKacabRequestResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const approvalKacabRequestRoute: Routes = [
    {
        path: 'approval-kacab-request',
        component: ApprovalKacabRequestComponent,
        resolve: {
            'pagingParams': ApprovalKacabRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approvalKacabRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'approval-kacab-request/:idReqNot',
        component: ApprovalKacabRequestDetailComponent,
        resolve: {
            'pagingParams': ApprovalKacabRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approvalKacabRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const approvalKacabRequestPopupRoute: Routes = [
    {
        path: 'approval-kacab-request-new',
        component: ApprovalKacabRequestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approvalKacabRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'approval-kacab-request-detail/:idReqNot/detail',
        component: ApprovalKacabRequestDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approvalKacabRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'approval-kacab-request/:id/edit',
        component: ApprovalKacabRequestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approvalKacabRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'approval-kacab-request/:id/delete',
        component: ApprovalKacabRequestDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approvalKacabRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
