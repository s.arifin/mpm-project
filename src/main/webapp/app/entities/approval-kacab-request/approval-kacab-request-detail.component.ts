import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiPaginationUtil, JhiAlertService } from 'ng-jhipster';

import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { ApprovalKacabRequest } from './approval-kacab-request.model';
import { ApprovalKacabRequestService } from './approval-kacab-request.service';
import { UnitDocumentMessage } from '../unit-document-message/unit-document-message.model';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LoadingService } from '../../layouts';
import { ToasterService } from '../../shared/alert/toaster.service';

@Component({
    selector: 'jhi-approval-kacab-request-detail',
    templateUrl: './approval-kacab-request-detail.component.html'
})
export class ApprovalKacabRequestDetailComponent implements OnInit, OnDestroy {

    approvalKacabRequest: ApprovalKacabRequest;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    public unitDocumentMessage: UnitDocumentMessage[];
    isLazyLoadingFirst: Boolean = false;
    predicate: any;
    previousPage: any;
    reverse: any;
    page: any;
    itemsPerPage: any;
    routeData: any;
    currentAccount: any;
    currentSearch: string;
    totalItems: any;
    queryCount: any;
    unitDocumentMessagePassing: UnitDocumentMessage;
    idReqNot: any;

    constructor(
        private eventManager: JhiEventManager,
        private approvalKacabRequestService: ApprovalKacabRequestService,
        private activatedRoute: ActivatedRoute,
        private principal: Principal,
        private router: Router,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        protected confirmationService: ConfirmationService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        private alertService: JhiAlertService,
        private toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.unitDocumentMessagePassing = new UnitDocumentMessage;
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }
    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.approvalKacabRequestService.passingData.subscribe( (dataPassing) => {
                this.unitDocumentMessagePassing = dataPassing;
                this.idReqNot = dataPassing.idreqnot;
            });
            this.loadAll();
        });
        this.registerChangeInApprovalKacabRequests();
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    loadAll() {
        if (this.currentSearch) {
            this.approvalKacabRequestService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.approvalKacabRequestService.queryDetail({
            page: this.page - 1,
            query: 'idReqNot:' + this.idReqNot,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    transition() {
        this.router.navigate(['/approval-kacab-receive'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/approval-kacab-receive', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/approval-kacab-receive', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.unitDocumentMessage = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }
    previousState() {
        this.router.navigate(['/approval-kacab-request']);
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
        // this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInApprovalKacabRequests() {
        this.eventSubscriber = this.eventManager.subscribe('approvalKacabReceiveListModification', (response) => this.loadAll());
    }

    loadDataLazy(event: LazyLoadEvent) {
        if (this.isLazyLoadingFirst === false) {
            this.isLazyLoadingFirst = true;
            return ;
        }
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    gotoDetail(rowData)  {
        console.log('datadriverdetail', rowData);
        this.approvalKacabRequestService.passingCustomData(rowData);
    }

    public confirmApprove(): void {
        this.confirmationService.confirm({
            header : 'Confirmation',
            message : 'Do you want to submit this <b>Approval</b>?',
            accept: () => {
                this.approvalKacabRequestService.queryApprove({
                    query: 'idReqNot:' + this.idReqNot
                }).subscribe(
                    (res) => {
                        this.previousState();
                    }
                );
            }
        });
    }

    public submit(type: string): void {
        console.log('masukl');
        if (type === 'approve') {
            console.log('approve');
            this.confirmationService.confirm({
                header : 'Konfirmasi',
                message : 'Apakah Anda Yakin Approve Note?',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.approvalKacabRequestService.queryApprove({
                        query: 'idReqNot:' + this.idReqNot,
                        account: 'username:' + this.principal.getUserLogin()
                    }).subscribe(
                        (res) => {
                            this.loadingService.loadingStop();
                            this.toasterService.showToaster('success', 'Sucess', 'Request Note Telah di Approve');
                            this.previousState();
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        }else if (type === 'reject') {
            console.log('reject');
            this.confirmationService.confirm({
                header : 'Konfirmasi',
                message : 'Apakah Anda Yakin Tidak Approve Note?',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.approvalKacabRequestService.queryReject({
                        query: 'idReqNot:' + this.idReqNot,
                        account: 'username:' + this.principal.getUserLogin()
                    }).subscribe(
                        (res) => {
                            this.loadingService.loadingStop();
                            this.previousState();
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        }
    }
}
