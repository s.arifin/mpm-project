import { BaseEntity } from './../../shared';

export class VehicleIdentification implements BaseEntity {
    constructor(
        public id?: any,
        public idVehicleIdentification?: any,
        public vehicleNumber?: string,
        public customerName?: string,
        public dateFrom?: any,
        public dateThru?: any,
        public vehicleId?: any,
        public customerId?: any
    ) {
    }
}
