import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { VehicleIdentificationComponent } from './vehicle-identification.component';
import { VehicleIdentificationDetailComponent } from './vehicle-identification-detail.component';
import { VehicleIdentificationPopupComponent } from './vehicle-identification-dialog.component';

@Injectable()
export class VehicleIdentificationResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idVehicleIdentification,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const vehicleIdentificationRoute: Routes = [
    {
        path: 'vehicle-identification',
        component: VehicleIdentificationComponent,
        resolve: {
            'pagingParams': VehicleIdentificationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleIdentification.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'vehicle-identification/:id',
        component: VehicleIdentificationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleIdentification.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const vehicleIdentificationPopupRoute: Routes = [
    {
        path: 'vehicle-identification-new',
        component: VehicleIdentificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleIdentification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'vehicle-identification/:id/edit',
        component: VehicleIdentificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleIdentification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
