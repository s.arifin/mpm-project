import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { VehicleIdentification } from './vehicle-identification.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class VehicleIdentificationService {

    protected resourceUrl = 'api/vehicle-identifications';
    protected resourceSearchUrl = 'api/_search/vehicle-identifications';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(vehicleIdentification: VehicleIdentification): Observable<VehicleIdentification> {
        const copy = this.convert(vehicleIdentification);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(vehicleIdentification: VehicleIdentification): Observable<VehicleIdentification> {
        const copy = this.convert(vehicleIdentification);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<VehicleIdentification> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findByNumber(id: any): Observable<VehicleIdentification> {
        return this.http.get(`${this.resourceUrl}/number/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, vehicleIdentification: any): Observable<VehicleIdentification> {
        const copy = this.convert(vehicleIdentification);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, vehicleIdentifications: VehicleIdentification[]): Observable<VehicleIdentification[]> {
        const copy = this.convertList(vehicleIdentifications);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateFrom) {
            entity.dateFrom = new Date(entity.dateFrom);
        }
        if (entity.dateThru) {
            entity.dateThru = new Date(entity.dateThru);
        }
    }

    protected convert(vehicleIdentification: VehicleIdentification): VehicleIdentification {
        const copy: VehicleIdentification = Object.assign({}, vehicleIdentification);
        return copy;
    }

    protected convertList(vehicleIdentifications: VehicleIdentification[]): VehicleIdentification[] {
        const copy = vehicleIdentifications;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
