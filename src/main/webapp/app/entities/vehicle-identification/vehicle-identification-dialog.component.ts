import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {VehicleIdentification} from './vehicle-identification.model';
import {VehicleIdentificationPopupService} from './vehicle-identification-popup.service';
import {VehicleIdentificationService} from './vehicle-identification.service';
import {ToasterService} from '../../shared';
import { Vehicle, VehicleService } from '../vehicle';
import { Customer, CustomerService } from '../customer';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-vehicle-identification-dialog',
    templateUrl: './vehicle-identification-dialog.component.html'
})
export class VehicleIdentificationDialogComponent implements OnInit {

    vehicleIdentification: VehicleIdentification;
    isSaving: boolean;

    vehicles: Vehicle[];

    customers: Customer[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected vehicleIdentificationService: VehicleIdentificationService,
        protected vehicleService: VehicleService,
        protected customerService: CustomerService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.vehicleService.query()
            .subscribe((res: ResponseWrapper) => { this.vehicles = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.vehicleIdentification.idVehicleIdentification !== undefined) {
            this.subscribeToSaveResponse(
                this.vehicleIdentificationService.update(this.vehicleIdentification));
        } else {
            this.subscribeToSaveResponse(
                this.vehicleIdentificationService.create(this.vehicleIdentification));
        }
    }

    protected subscribeToSaveResponse(result: Observable<VehicleIdentification>) {
        result.subscribe((res: VehicleIdentification) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: VehicleIdentification) {
        this.eventManager.broadcast({ name: 'vehicleIdentificationListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vehicleIdentification saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'vehicleIdentification Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackVehicleById(index: number, item: Vehicle) {
        return item.idVehicle;
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }
}

@Component({
    selector: 'jhi-vehicle-identification-popup',
    template: ''
})
export class VehicleIdentificationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected vehicleIdentificationPopupService: VehicleIdentificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vehicleIdentificationPopupService
                    .open(VehicleIdentificationDialogComponent as Component, params['id']);
            } else {
                this.vehicleIdentificationPopupService
                    .open(VehicleIdentificationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
