export * from './vehicle-identification.model';
export * from './vehicle-identification-popup.service';
export * from './vehicle-identification.service';
export * from './vehicle-identification-dialog.component';
export * from './vehicle-identification-detail.component';
export * from './vehicle-identification.component';
export * from './vehicle-identification.route';
