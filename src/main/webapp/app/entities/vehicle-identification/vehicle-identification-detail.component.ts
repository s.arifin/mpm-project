import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import {VehicleIdentification} from './vehicle-identification.model';
import {VehicleIdentificationService} from './vehicle-identification.service';

@Component({
    selector: 'jhi-vehicle-identification-detail',
    templateUrl: './vehicle-identification-detail.component.html'
})
export class VehicleIdentificationDetailComponent implements OnInit, OnDestroy {

    vehicleIdentification: VehicleIdentification;
    protected subscription: Subscription;
    protected eventSubscriber: Subscription;

    constructor(
        protected eventManager: JhiEventManager,
        protected vehicleIdentificationService: VehicleIdentificationService,
        protected route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInVehicleIdentifications();
    }

    load(id) {
        this.vehicleIdentificationService.find(id).subscribe((vehicleIdentification) => {
            this.vehicleIdentification = vehicleIdentification;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInVehicleIdentifications() {
        this.eventSubscriber = this.eventManager.subscribe(
            'vehicleIdentificationListModification',
            (response) => this.load(this.vehicleIdentification.idVehicleIdentification)
        );
    }
}
