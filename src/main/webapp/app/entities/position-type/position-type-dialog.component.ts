import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {PositionType} from './position-type.model';
import {PositionTypePopupService} from './position-type-popup.service';
import {PositionTypeService} from './position-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-position-type-dialog',
    templateUrl: './position-type-dialog.component.html'
})
export class PositionTypeDialogComponent implements OnInit {

    positionType: PositionType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected positionTypeService: PositionTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.positionType.idPositionType !== undefined) {
            this.subscribeToSaveResponse(
                this.positionTypeService.update(this.positionType));
        } else {
            this.subscribeToSaveResponse(
                this.positionTypeService.create(this.positionType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PositionType>) {
        result.subscribe((res: PositionType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: PositionType) {
        this.eventManager.broadcast({ name: 'positionTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'positionType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'positionType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-position-type-popup',
    template: ''
})
export class PositionTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected positionTypePopupService: PositionTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.positionTypePopupService
                    .open(PositionTypeDialogComponent as Component, params['id']);
            } else {
                this.positionTypePopupService
                    .open(PositionTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
