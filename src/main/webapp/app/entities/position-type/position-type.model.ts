import { BaseEntity } from './../../shared';

export class PositionType implements BaseEntity {
    constructor(
        public id?: number,
        public idPositionType?: number,
        public description?: string,
        public title?: string,
    ) {
    }
}
