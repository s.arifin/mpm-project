import { BaseEntity } from './../../shared';

export class PriceType implements BaseEntity {
    constructor(
        public id?: any,
        public idPriceType?: any,
        public idRuleType?: number,
        public description?: any
    ) {
    }
}
