import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {
    PriceTypeService
} from './';

@NgModule({
    providers: [
        PriceTypeService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPriceTypeModule {}
