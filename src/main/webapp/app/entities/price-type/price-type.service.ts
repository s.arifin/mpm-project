import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { JhiDateUtils } from 'ng-jhipster';

import { PriceType } from './price-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class PriceTypeService {
    protected itemValues: PriceType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/price-types';
    protected resourceSearchUrl = 'api/_search/price-types';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    getAllData(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl).map((res: Response) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    pushItems(data: PriceType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
