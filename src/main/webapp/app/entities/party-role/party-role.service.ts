import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { PartyRole } from './party-role.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PartyRoleService {
    protected itemValues: PartyRole[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl =  SERVER_API_URL + 'api/party-roles';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/party-roles';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(partyRole: PartyRole): Observable<PartyRole> {
        const copy = this.convert(partyRole);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(partyRole: PartyRole): Observable<PartyRole> {
        const copy = this.convert(partyRole);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<PartyRole> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(partyRole: PartyRole, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(partyRole);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, partyRole: PartyRole): Observable<PartyRole> {
        const copy = this.convert(partyRole);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, partyRoles: PartyRole[]): Observable<PartyRole[]> {
        const copy = this.convertList(partyRoles);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PartyRole.
     */
    protected convertItemFromServer(json: any): PartyRole {
        const entity: PartyRole = Object.assign(new PartyRole(), json);
        if (entity.dateRegister) {
            entity.dateRegister = new Date(entity.dateRegister);
        }
        if (entity.dateFrom) {
            entity.dateFrom = new Date(entity.dateFrom);
        }
        if (entity.dateThru) {
            entity.dateThru = new Date(entity.dateThru);
        }
        return entity;
    }

    /**
     * Convert a PartyRole to a JSON which can be sent to the server.
     */
    protected convert(partyRole: PartyRole): PartyRole {
        if (partyRole === null || partyRole === {}) {
            return {};
        }
        // const copy: PartyRole = Object.assign({}, partyRole);
        const copy: PartyRole = JSON.parse(JSON.stringify(partyRole));

        // copy.dateRegister = this.dateUtils.toDate(partyRole.dateRegister);

        // copy.dateFrom = this.dateUtils.toDate(partyRole.dateFrom);

        // copy.dateThru = this.dateUtils.toDate(partyRole.dateThru);
        return copy;
    }

    protected convertList(partyRoles: PartyRole[]): PartyRole[] {
        const copy: PartyRole[] = partyRoles;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: PartyRole[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
