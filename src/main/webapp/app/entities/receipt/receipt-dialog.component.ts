import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { Receipt } from './receipt.model';
import { ReceiptPopupService } from './receipt-popup.service';
import { ReceiptService } from './receipt.service';
import { ToasterService, CommonUtilService } from '../../shared';
import { PaymentType, PaymentTypeService } from '../payment-type';
import { PaymentMethod, PaymentMethodService } from '../payment-method';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import {ITEMS_PER_PAGE, Principal, ResponseWrapper} from '../../shared';
import {PaginationConfig} from '../../blocks/config/uib-pagination.config';

// nuse
import { AxPosting, AxPostingService } from '../axposting';
import { AxPostingLine } from '../axposting-line';
import { Requirement, RequirementService } from '../requirement';
import { DatePipe } from '@angular/common';
import { Customer, CustomerCService, CustomerAX } from '../customer';
import { AxCustomer, AxCustomerService } from '../axcustomer';
import { SURReceiptDialogComponent } from '../sales-unit-requirement/include/sur-receipt-dialog.component';

@Component({
    selector: 'jhi-receipt-dialog',
    templateUrl: './receipt-dialog.component.html'
})
export class ReceiptDialogComponent implements OnInit {

    receipt: Receipt;
    isSaving: boolean;
    idPaymentType: any;
    idMethod: any;
    idInternal: any;
    idPaidTo: any;
    idPaidFrom: any;
    idRequirement: string;
    selected: BillTo;
    validasiNIK: Boolean = false;

    public stepOne: boolean;
    public stepTwo: boolean;
    public selectedViaPayment: string;

    paymenttypes: PaymentType[];

    paymentmethods: PaymentMethod[];

    internals: Internal[];

    billtos: BillTo[];
// nuse
    axPosting: AxPosting;
    axPostingLines: AxPostingLine[];
    axPostingLine: AxPostingLine;
    internal: Internal;
    requirement: Requirement;
    customer: Customer;
    axCustomer: AxCustomer;
    customerAX: CustomerAX;

    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    routeData: any;
    previousPage: any;
    reverse: any;
    currentSearch: string;

    constructor(
        protected activatedRoute: ActivatedRoute,
        public activeModal: NgbActiveModal,
        protected principalService: Principal,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected receiptService: ReceiptService,
        protected alertService: JhiAlertService,
        protected paymentTypeService: PaymentTypeService,
        protected paymentMethodService: PaymentMethodService,
        protected internalService: InternalService,
        protected billToService: BillToService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected commonUtilService: CommonUtilService,
        // nuse
        protected axPostingService: AxPostingService,
        protected requirementService: RequirementService,
        protected customerCService: CustomerCService,
        protected axCustomerService: AxCustomerService,
        protected datePipe: DatePipe
    ) {
        // this.itemsPerPage = ITEMS_PER_PAGE;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        // this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        // this.reverse = 'asc';
        // this.queryCount = 0;

        if (this.idRequirement !== undefined && this.idRequirement !== null) {
            this.receipt.idRequirement = this.idRequirement;
        }

        if (this.idPaidFrom !== undefined && this.idPaidFrom !== null) {
            this.receipt.paidFromId = this.idPaidFrom;
        }

        this.selectedViaPayment = 'kasir';
        this.stepOne = true;
        this.stepTwo = false;
    }

    ngOnInit() {
        this.isSaving = false;
        this.paymentTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.paymenttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.paymentMethodService.query()
            .subscribe((res: ResponseWrapper) => { this.paymentmethods = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query({
            page: 0, size: 10000}).subscribe(
            (res: ResponseWrapper) => this.onSuccessBill(res.json, res.headers),
            (res: ResponseWrapper) => this.onErrorBill(res.json)
        );
        // this.billToService.query()
        //     .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    protected resetStep() {
        this.stepOne = false;
        this.stepTwo = false;
    }
    public goToStep(step: number) {
        this.resetStep();
        if (step === 1) {
            this.stepOne = true;
        } else if (step === 2) {
            this.stepTwo = true;
        }
    }

    protected onSuccessBill(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.billtos = data;
    }

    protected onErrorBill(error) {
        this.commonUtilService.showError(error);
    }
    createReceipt() {
        this.receipt.paidToId = this.principalService.getIdInternal();
        this.receipt.dateCreate = new Date();

        this.receiptService.create(this.receipt).subscribe(
            // nuse debug begin
            (res: Receipt) => {
                this.onSaveSuccess(res);
                console.log('cobain ax posting');
                const myaxPostingLine = [];
                console.log('receipt.internalid : ', this.receipt.paidToId);

                this.internalService.find(res.paidToId).subscribe(
                    (internal: Internal) => {
                        console.log('internal.id : ', internal.idMPM);
                        this.internal = internal;
                        console.log('this.receipt.idRequirement', res.idRequirement);
                        this.requirementService.find(this.receipt.idRequirement).subscribe(
                            (resReq: Requirement) => {

                                this.requirement = resReq;
                                this.customerCService.findByIdCustomer(this.receipt.paidFromId, this.receipt.methodId).subscribe(
                                    (resCustomer: CustomerAX) => {
                                        this.customerAX = resCustomer;

                                        this.axCustomer = new AxCustomer();
                                        this.axCustomer.AccountNum = this.customerAX.idMPM;
                                        this.axCustomer.DataArea = this.receipt.paidToId.substr(0, 3);
                                        this.axCustomer.CustGroup = '130-Retail';
                                        this.axCustomer.Name = this.customerAX.partyName;
                                        this.axCustomer.Dimensi1 = '1014';
                                        this.axCustomer.Dimensi2 = this.receipt.paidToId.substr(0, 3);
                                        this.axCustomer.Dimensi3 = '000';
                                        this.axCustomer.Dimensi4 = '000';
                                        this.axCustomer.Dimensi5 = '00';
                                        this.axCustomer.Dimensi6 = '000';

                                        console.log('axCustomer:', this.axCustomer);
                                        this.axCustomerService.send(this.axCustomer).subscribe(
                                            (resWrapper: ResponseWrapper) => {
                                                console.log('success : ', resWrapper.json.Message);

                                                console.log('requirementPayment.PaymentId : ', this.requirement.requirementNumber);

                                                const today = new Date();
                                                this.axPosting = new AxPosting();
                                                // dipake buat repush saja
                                                this.axPosting.AutoPosting = 'FALSE';
                                                this.axPosting.AutoSettlement = 'FALSE';
                                                this.axPosting.DataArea = this.internal.idMPM;
                                                this.axPosting.Guid = this.receipt.idRequirement;
                                                this.axPosting.JournalName = 'titipan-AR-Unit';
                                                this.axPosting.Name = 'Titipan an. ' + this.requirement.requirementNumber + ' ' + this.customerAX.partyName;
                                                this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                                this.axPosting.TransactionType = 'Unit';

                                                // credit
                                                this.axPostingLine = new AxPostingLine();
                                                this.axPostingLine.AccountNum = this.customerAX.idMPM;
                                                this.axPostingLine.AccountType = 'Cust';
                                                this.axPostingLine.AmountCurCredit = this.receipt.amount.toString();
                                                this.axPostingLine.AmountCurDebit = '0';
                                                this.axPostingLine.Company = this.internal.idMPM;
                                                this.axPostingLine.DMSNum = this.requirement.requirementNumber;
                                                this.axPostingLine.Description = 'Titipan an. ' + this.requirement.requirementNumber + ' ' + this.customerAX.partyName;
                                                this.axPostingLine.Dimension1 = '1014';
                                                this.axPostingLine.Dimension2 = this.internal.idMPM;
                                                this.axPostingLine.Dimension3 = '000';
                                                this.axPostingLine.Dimension4 = '000';
                                                this.axPostingLine.Dimension5 = '00';
                                                this.axPostingLine.Dimension6 = '000';
                                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                                this.axPostingLine.Invoice = '';
                                                this.axPostingLine.Payment = '';
                                                myaxPostingLine.push(this.axPostingLine);

                                                // debit
                                                this.axPostingLine = new AxPostingLine();
                                                this.axPostingLine.AccountNum = this.customerAX.bankAccount;
                                                this.axPostingLine.AccountType = 'Bank';
                                                this.axPostingLine.AmountCurCredit = '0';
                                                this.axPostingLine.AmountCurDebit = this.receipt.amount.toString();
                                                this.axPostingLine.Company = this.internal.idMPM;
                                                this.axPostingLine.DMSNum = null;  // ini untuk cash
                                                this.axPostingLine.Description = 'Titipan an. ' + this.requirement.requirementNumber + ' ' + this.customerAX.partyName;
                                                this.axPostingLine.Dimension1 = '1014';
                                                this.axPostingLine.Dimension2 = this.internal.idMPM;
                                                this.axPostingLine.Dimension3 = '000';
                                                this.axPostingLine.Dimension4 = '000';
                                                this.axPostingLine.Dimension5 = '00';
                                                this.axPostingLine.Dimension6 = '000';
                                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                                this.axPostingLine.Invoice = '';
                                                this.axPostingLine.Payment = '';

                                                myaxPostingLine.push(this.axPostingLine);

                                                this.axPosting.LedgerJournalLine = myaxPostingLine;

                                                this.axPostingService.send(this.axPosting).subscribe(
                                                    (resaxPosting: ResponseWrapper) => console.log('Success : ', resaxPosting.json.Message),
                                                    (resaxPosting: ResponseWrapper) => {
                                                        this.onError(resaxPosting.json);
                                                        console.log('error ax posting : ', resaxPosting.json);
                                                    }
                                                );
                                            },
                                            (resWrapper: ResponseWrapper) => this.onError(resWrapper.json)
                                        );
                                    }
                                );

                            }
                        );

                    });
                // nuse debug end
                return this.receipt;
            },
            (err) => {
                console.log('hahahahhaha wakwau');
                // this.onError(err);
                // this.commonUtilService.showError(err);
                // this.toaster.showToaster('warning', 'receipt Changed', err.json);
                // this.jhiAlertService.error(err.json, null, null);
                // this.validasiNIK = true;
                return this.receipt;
            }
        );
        // this.subscribeToSaveResponse(
        //    this.receiptService.create(this.receipt));
        return this.receipt;
    }

    save() {
        this.receipt.internalId = this.receipt.paidToId;
        this.isSaving = true;
        if (this.receipt.idPayment !== undefined) {
            this.subscribeToSaveResponse(
                this.receiptService.update(this.receipt));
        } else {
            this.subscribeToSaveResponse(
                this.receiptService.create(this.receipt))
        }
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idBillTo') {
            result.push('idBillTo');
        }
        return result;
    }

    protected subscribeToSaveResponse(result: Observable<Receipt>) {
        result.subscribe(
            (res: Receipt) => this.onSaveSuccess(res),
            (res) => {this.onError(res)}
            );
    }

    protected onSaveSuccess(result: Receipt) {
        this.eventManager.broadcast({ name: 'receiptListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'receipt saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'receipt Changed', error.detail);
        this.jhiAlertService.error(error.detail, null, null);
        this.commonUtilService.showError(error);
        this.isSaving = false;
    }

    trackPaymentTypeById(index: number, item: PaymentType) {
        return item.idPaymentType;
    }

    trackPaymentMethodById(index: number, item: PaymentMethod) {
        return item.idPaymentMethod;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }

    closeDialog() {
        this.isSaving = false;
        this.validasiNIK = false;
    }
}

@Component({
    selector: 'jhi-receipt-popup',
    template: ''
})
export class ReceiptPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected receiptPopupService: ReceiptPopupService,
        protected billToService: BillToService,
    ) {}

    ngOnInit() {
        this.receiptPopupService.idPaymentType = undefined;
        this.receiptPopupService.idMethod = undefined;
        this.receiptPopupService.idInternal = undefined;
        this.receiptPopupService.idPaidTo = undefined;
        this.receiptPopupService.idPaidFrom = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.receiptPopupService
                    .open(ReceiptDialogComponent as Component, params['id']);
            } else if (params['idPayment'] && params['idreq'] && params['idparty']) {
                this.receiptPopupService.idRequirement = params['idreq'];
                this.billToService.queryFilterBy({idParty: params['idparty']}).subscribe((res) => {
                    if (res.json.length) {
                        const obj: object = res.json;
                        this.receiptPopupService.idPaidFrom = obj[0].idBillTo;
                        this.receiptPopupService
                            .open(SURReceiptDialogComponent as Component);
                    }
                });
            } else if (params['idreq'] && params['idparty']) {
                this.receiptPopupService.idRequirement = params['idreq'];
                this.billToService.queryFilterBy({idParty: params['idparty']}).subscribe((res) => {
                    if (res.json.length) {
                        const obj: object = res.json;
                        this.receiptPopupService.idPaidFrom = obj[0].idBillTo;
                        this.receiptPopupService
                            .open(ReceiptDialogComponent as Component);
                    }
                });
            } else if ( params['parent'] ) {
                this.billToService.queryFilterBy({idParty: params['parent']}).subscribe((res) => {
                    if (res.json.length) {
                        const obj: object = res.json;
                        this.receiptPopupService.idPaidFrom = obj[0].idBillTo;
                        this.receiptPopupService
                            .open(ReceiptDialogComponent as Component);
                    }
                });
            } else {
                this.receiptPopupService
                    .open(ReceiptDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
