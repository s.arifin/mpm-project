import { BaseEntity } from './../../shared';
import { Payment } from '../payment/payment.model';

export class Receipt extends Payment {
    constructor(
        public idRequirement?: string
    ) {
        super();
    }
}
