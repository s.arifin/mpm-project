import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Receipt } from './receipt.model';
import { ReceiptService } from './receipt.service';
import { ToasterService} from '../../shared';
import { ReportUtilService} from '../../shared/report/report-util.service';
import { PaymentType, PaymentTypeService } from '../payment-type';
import { PaymentMethod, PaymentMethodService } from '../payment-method';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
   selector: 'jhi-receipt-edit',
   templateUrl: './receipt-edit.component.html'
})
export class ReceiptEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   receipt: Receipt;
   isSaving: boolean;
   idPayment: any;
   paramPage: number;
   routeId: number;

   paymenttypes: PaymentType[];

   paymentmethods: PaymentMethod[];

   internals: Internal[];

   billtos: BillTo[];

   constructor(
       protected alertService: JhiAlertService,
       protected receiptService: ReceiptService,
       protected paymentTypeService: PaymentTypeService,
       protected paymentMethodService: PaymentMethodService,
       protected internalService: InternalService,
       protected billToService: BillToService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected reportUtilService: ReportUtilService,
       protected toaster: ToasterService
   ) {
       this.receipt = new Receipt();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idPayment = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
       this.paymentTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.paymenttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.paymentMethodService.query()
            .subscribe((res: ResponseWrapper) => { this.paymentmethods = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.receiptService.find(this.idPayment).subscribe((receipt) => {
           this.receipt = receipt;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['receipt', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.receipt.idPayment !== undefined) {
           this.subscribeToSaveResponse(
               this.receiptService.update(this.receipt));
       } else {
           this.subscribeToSaveResponse(
               this.receiptService.create(this.receipt));
       }
   }

   protected subscribeToSaveResponse(result: Observable<Receipt>) {
       result.subscribe((res: Receipt) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: Receipt) {
       this.eventManager.broadcast({ name: 'receiptListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'receipt saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'receipt Changed', error.message);
       this.alertService.error(error.message, null, null);
   }

   trackPaymentTypeById(index: number, item: PaymentType) {
       return item.idPaymentType;
   }

   trackPaymentMethodById(index: number, item: PaymentMethod) {
       return item.idPaymentMethod;
   }

   trackInternalById(index: number, item: Internal) {
       return item.idInternal;
   }

   trackBillToById(index: number, item: BillTo) {
       return item.idBillTo;
   }
   print() {
       this.toaster.showToaster('info', 'Print Data', 'Printing.....');
       this.reportUtilService.viewFile('/api/report/sample/pdf');
   }

}
