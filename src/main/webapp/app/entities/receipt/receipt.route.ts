import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ReceiptComponent } from './receipt.component';
import { ReceiptEditComponent } from './receipt-edit.component';
import { ReceiptPopupComponent } from './receipt-dialog.component';
import { SURReceiptDialogComponent } from '../sales-unit-requirement/include/sur-receipt-dialog.component';

@Injectable()
export class ReceiptResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idPayment,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const receiptRoute: Routes = [
    {
        path: 'receipt',
        component: ReceiptComponent,
        resolve: {
            'pagingParams': ReceiptResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.receipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const receiptPopupRoute: Routes = [
    {
        path: 'receipt-popup-new-list/:idparty/:idreq',
        component: ReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.receipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'receipt-popup-new-list/:idparty/:idreq/:idPayment',
        component: ReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.receipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'receipt-popup-new-list/:parent',
        component: ReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.receipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'receipt-popup-new',
        component: ReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.receipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'receipt-new',
        component: ReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.receipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'receipt/:id/edit',
        component: ReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.receipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'receipt/:route/:page/:id/edit',
        component: ReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.receipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'receipt/:id/popup-edit',
        component: ReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.receipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
