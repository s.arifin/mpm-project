import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Receipt } from './receipt.model';
import { ReceiptService } from './receipt.service';

@Injectable()
export class ReceiptPopupService {
    protected ngbModalRef: NgbModalRef;
    idPaymentType: any;
    idMethod: any;
    idInternal: any;
    idPaidTo: any;
    idPaidFrom: any;
    idRequirement: string;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected receiptService: ReceiptService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.receiptService.find(id).subscribe((data) => {
                    // if (data.dateCreate) {
                    //    data.dateCreate = this.datePipe
                    //        .transform(data.dateCreate, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.receiptModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new Receipt();
                    if (this.idPaymentType) {
                        data.paymentTypeId = this.idPaymentType;
                    } else {
                        data.paymentTypeId = null;
                    }

                    if (this.idMethod) {
                        data.methodId = this.idMethod;
                    } else {
                        data.methodId = null;
                    }

                    data.internalId = this.idInternal;
                    data.paidToId = this.idPaidTo;
                    data.paidFromId = this.idPaidFrom;
                    data.idRequirement = this.idRequirement;
                    this.ngbModalRef = this.receiptModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    receiptModalRef(component: Component, receipt: Receipt): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.receipt = receipt;
        modalRef.componentInstance.idPaymentType = this.idPaymentType;
        modalRef.componentInstance.idMethod = this.idMethod;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idPaidTo = this.idPaidTo;
        modalRef.componentInstance.idPaidFrom = this.idPaidFrom;
        modalRef.componentInstance.idRequirement = this.idRequirement;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.receiptLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    receiptLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idPaymentType = this.idPaymentType;
        modalRef.componentInstance.idMethod = this.idMethod;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idPaidTo = this.idPaidTo;
        modalRef.componentInstance.idPaidFrom = this.idPaidFrom;
        modalRef.componentInstance.idRequirement = this.idRequirement;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
