import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Receipt } from './receipt.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ReceiptService {
   protected itemValues: Receipt[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/receipts';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/receipts';

   constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

   create(receipt: Receipt): Observable<Receipt> {
       const copy = this.convert(receipt);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(receipt: Receipt): Observable<Receipt> {
       const copy = this.convert(receipt);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<Receipt> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }
   findByPaidFromId(id: any): Observable<ResponseWrapper> {
       console.log('service receipt');
        return this.http.get(`${this.resourceUrl}/findByPaidFromId/${id}`)
            .map((res: Response) => this.convertResponse(res));
    }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   changeStatus(receipt: Receipt, id: Number): Observable<ResponseWrapper> {
       const copy = this.convert(receipt);
       return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: Receipt, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: Receipt[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       this.pushItems(result);
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to Receipt.
    */
   protected convertItemFromServer(json: any): Receipt {
       const entity: Receipt = Object.assign(new Receipt(), json);
       if (entity.dateCreate) {
           entity.dateCreate = new Date(entity.dateCreate);
       }
       return entity;
   }

   /**
    * Convert a Receipt to a JSON which can be sent to the server.
    */
   protected convert(receipt: Receipt): Receipt {
       if (receipt === null || receipt === {}) {
           return {};
       }
       // const copy: Receipt = Object.assign({}, receipt);
       const copy: Receipt = JSON.parse(JSON.stringify(receipt));

       // copy.dateCreate = this.dateUtils.toDate(receipt.dateCreate);
       return copy;
   }

   protected convertList(receipts: Receipt[]): Receipt[] {
       const copy: Receipt[] = receipts;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: Receipt[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
    /**
     * Count Total Receipt
     */
    public countTotalReceipt(receipts: Receipt[]): number {
        let tempTotalReceipt = 0;
        if (receipts.length > 0) {
            for (let i = 0; i < receipts.length; i++) {
                const a = receipts[i];
                tempTotalReceipt = tempTotalReceipt + a.amount;
            }
        }
        return tempTotalReceipt;
    }
}
