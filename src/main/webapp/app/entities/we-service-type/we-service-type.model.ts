import { BaseEntity } from './../../shared';

export class WeServiceType implements BaseEntity {
    constructor(
        public id?: number,
        public idWeType?: number,
        public flatRateTime?: number,
        public serviceId?: any,
    ) {
    }
}
