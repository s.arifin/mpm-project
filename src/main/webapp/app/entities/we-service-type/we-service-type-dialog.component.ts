import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {WeServiceType} from './we-service-type.model';
import {WeServiceTypePopupService} from './we-service-type-popup.service';
import {WeServiceTypeService} from './we-service-type.service';
import {ToasterService} from '../../shared';
import { Service, ServiceService } from '../service';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-we-service-type-dialog',
    templateUrl: './we-service-type-dialog.component.html'
})
export class WeServiceTypeDialogComponent implements OnInit {

    weServiceType: WeServiceType;
    isSaving: boolean;

    services: Service[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private weServiceTypeService: WeServiceTypeService,
        private serviceService: ServiceService,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.serviceService
            .query({filter: 'wetype-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.weServiceType.serviceId) {
                    this.services = res.json;
                } else {
                    this.serviceService
                        .find(this.weServiceType.serviceId)
                        .subscribe((subRes: Service) => {
                            this.services = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.weServiceType.idWeType !== undefined) {
            this.subscribeToSaveResponse(
                this.weServiceTypeService.update(this.weServiceType));
        } else {
            this.subscribeToSaveResponse(
                this.weServiceTypeService.create(this.weServiceType));
        }
    }

    private subscribeToSaveResponse(result: Observable<WeServiceType>) {
        result.subscribe((res: WeServiceType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: WeServiceType) {
        this.eventManager.broadcast({ name: 'weServiceTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'weServiceType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'weServiceType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackServiceById(index: number, item: Service) {
        return item.idProduct;
    }
}

@Component({
    selector: 'jhi-we-service-type-popup',
    template: ''
})
export class WeServiceTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private weServiceTypePopupService: WeServiceTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.weServiceTypePopupService
                    .open(WeServiceTypeDialogComponent as Component, params['id']);
            } else {
                this.weServiceTypePopupService
                    .open(WeServiceTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
