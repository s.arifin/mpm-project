import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { WeServiceType } from './we-service-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class WeServiceTypeService {
    private itemValues: WeServiceType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = 'api/we-service-types';
    private resourceSearchUrl = 'api/_search/we-service-types';

    constructor(private http: Http) { }

    create(weServiceType: WeServiceType): Observable<WeServiceType> {
        const copy = this.convert(weServiceType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(weServiceType: WeServiceType): Observable<WeServiceType> {
        const copy = this.convert(weServiceType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<WeServiceType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, weServiceType: WeServiceType): Observable<WeServiceType> {
        const copy = this.convert(weServiceType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, weServiceTypes: WeServiceType[]): Observable<WeServiceType[]> {
        const copy = this.convertList(weServiceTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(weServiceType: WeServiceType): WeServiceType {
        if (weServiceType === null || weServiceType === {}) {
            return {};
        }
        // const copy: WeServiceType = Object.assign({}, weServiceType);
        const copy: WeServiceType = JSON.parse(JSON.stringify(weServiceType));
        return copy;
    }

    private convertList(weServiceTypes: WeServiceType[]): WeServiceType[] {
        const copy: WeServiceType[] = weServiceTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: WeServiceType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
