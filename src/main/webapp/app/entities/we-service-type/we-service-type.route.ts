import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { WeServiceTypeComponent } from './we-service-type.component';
import { WeServiceTypeLovPopupComponent } from './we-service-type-as-lov.component';
import { WeServiceTypePopupComponent } from './we-service-type-dialog.component';

@Injectable()
export class WeServiceTypeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idWeType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const weServiceTypeRoute: Routes = [
    {
        path: 'we-service-type',
        component: WeServiceTypeComponent,
        resolve: {
            'pagingParams': WeServiceTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.weServiceType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const weServiceTypePopupRoute: Routes = [
    {
        path: 'we-service-type-lov',
        component: WeServiceTypeLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.weServiceType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'we-service-type-new',
        component: WeServiceTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.weServiceType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'we-service-type/:id/edit',
        component: WeServiceTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.weServiceType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
