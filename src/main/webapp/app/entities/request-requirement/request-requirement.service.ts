import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { RequestRequirement, CustomRequestRequirement } from './request-requirement.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class RequestRequirementService {
    protected itemValues: RequestRequirement[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/request-requirements';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/request-requirements';

    constructor(protected http: Http) { }

    create(requestRequirement: RequestRequirement): Observable<RequestRequirement> {
        const copy = this.convert(requestRequirement);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(requestRequirement: RequestRequirement): Observable<RequestRequirement> {
        const copy = this.convert(requestRequirement);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<RequestRequirement> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    public getByCustomRequestRequirement(): Observable<CustomRequestRequirement[]> {
        return this.http.get(this.resourceUrl + '/custom').map(
            (res: Response) => {
                return res.json();
            });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(requestRequirement: RequestRequirement, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(requestRequirement);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<RequestRequirement> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<RequestRequirement[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to RequestRequirement.
     */
    protected convertItemFromServer(json: any): RequestRequirement {
        const entity: RequestRequirement = Object.assign(new RequestRequirement(), json);
        return entity;
    }

    /**
     * Convert a RequestRequirement to a JSON which can be sent to the server.
     */
    protected convert(requestRequirement: RequestRequirement): RequestRequirement {
        if (requestRequirement === null || requestRequirement === {}) {
            return {};
        }
        // const copy: RequestRequirement = Object.assign({}, requestRequirement);
        const copy: RequestRequirement = JSON.parse(JSON.stringify(requestRequirement));
        return copy;
    }

    protected convertList(requestRequirements: RequestRequirement[]): RequestRequirement[] {
        const copy: RequestRequirement[] = requestRequirements;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: RequestRequirement[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
