import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RequestRequirement } from './request-requirement.model';
import { RequestRequirementService } from './request-requirement.service';
import { ToasterService} from '../../shared';
import { Requirement, RequirementService } from '../requirement';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-request-requirement-edit',
    templateUrl: './request-requirement-edit.component.html'
})
export class RequestRequirementEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    requestRequirement: RequestRequirement;
    isSaving: boolean;
    idRequest: any;
    paramPage: number;

    requirements: Requirement[];

    constructor(
        protected alertService: JhiAlertService,
        protected requestRequirementService: RequestRequirementService,
        protected requirementService: RequirementService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.requestRequirement = new RequestRequirement();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idRequest = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
        });
        this.isSaving = false;
        this.requirementService.query()
            .subscribe((res: ResponseWrapper) => { this.requirements = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.requestRequirementService.find(this.idRequest).subscribe((requestRequirement) => {
            this.requestRequirement = requestRequirement;
        });
    }

    previousState() {
        this.router.navigate(['request-requirement', { page: this.paramPage }]);
    }

    save() {
        this.isSaving = true;
        if (this.requestRequirement.idRequest !== undefined) {
            this.subscribeToSaveResponse(
                this.requestRequirementService.update(this.requestRequirement));
        } else {
            this.subscribeToSaveResponse(
                this.requestRequirementService.create(this.requestRequirement));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RequestRequirement>) {
        result.subscribe((res: RequestRequirement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: RequestRequirement) {
        this.eventManager.broadcast({ name: 'requestRequirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'requestRequirement saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'requestRequirement Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackRequirementById(index: number, item: Requirement) {
        return item.idRequirement;
    }
}
