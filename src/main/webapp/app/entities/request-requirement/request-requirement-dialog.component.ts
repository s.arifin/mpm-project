import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RequestRequirement } from './request-requirement.model';
import { RequestRequirementPopupService } from './request-requirement-popup.service';
import { RequestRequirementService } from './request-requirement.service';
import { ToasterService } from '../../shared';
import { Requirement, RequirementService } from '../requirement';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-request-requirement-dialog',
    templateUrl: './request-requirement-dialog.component.html'
})
export class RequestRequirementDialogComponent implements OnInit {

    requestRequirement: RequestRequirement;
    isSaving: boolean;
    idRequirement: any;

    requirements: Requirement[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected requestRequirementService: RequestRequirementService,
        protected requirementService: RequirementService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.requirementService.query()
            .subscribe((res: ResponseWrapper) => { this.requirements = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.requestRequirement.idRequest !== undefined) {
            this.subscribeToSaveResponse(
                this.requestRequirementService.update(this.requestRequirement));
        } else {
            this.subscribeToSaveResponse(
                this.requestRequirementService.create(this.requestRequirement));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RequestRequirement>) {
        result.subscribe((res: RequestRequirement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: RequestRequirement) {
        this.eventManager.broadcast({ name: 'requestRequirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'requestRequirement saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'requestRequirement Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRequirementById(index: number, item: Requirement) {
        return item.idRequirement;
    }
}

@Component({
    selector: 'jhi-request-requirement-popup',
    template: ''
})
export class RequestRequirementPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected requestRequirementPopupService: RequestRequirementPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.requestRequirementPopupService
                    .open(RequestRequirementDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.requestRequirementPopupService.parent = params['parent'];
                this.requestRequirementPopupService
                    .open(RequestRequirementDialogComponent as Component);
            } else {
                this.requestRequirementPopupService
                    .open(RequestRequirementDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
