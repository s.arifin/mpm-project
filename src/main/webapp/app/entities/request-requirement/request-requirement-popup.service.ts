import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RequestRequirement } from './request-requirement.model';
import { RequestRequirementService } from './request-requirement.service';

@Injectable()
export class RequestRequirementPopupService {
    protected ngbModalRef: NgbModalRef;
    idRequirement: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected requestRequirementService: RequestRequirementService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.requestRequirementService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.requestRequirementModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new RequestRequirement();
                    data.requirementId = this.idRequirement;
                    this.ngbModalRef = this.requestRequirementModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    requestRequirementModalRef(component: Component, requestRequirement: RequestRequirement): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.requestRequirement = requestRequirement;
        modalRef.componentInstance.idRequirement = this.idRequirement;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.requestRequirementLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    requestRequirementLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
