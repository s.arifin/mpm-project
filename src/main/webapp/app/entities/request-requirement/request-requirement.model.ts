import { BaseEntity } from './../../shared';

export class RequestRequirement implements BaseEntity {
    constructor(
        public id?: any,
        public idRequest?: any,
        public qty?: number,
        public requirementId?: any,
    ) {
    }
}

export class CustomRequestRequirement implements BaseEntity {
    constructor(
        public id?: any,
        public idRequest?: string,
        public requirementNumber?: string,
        public idRequirement?: string,
        public featureName?: string,
        public productName?: string,
        public dateCreated?: Date,
        public requestNumber?: string,
        public currentStatus?: number
    ) {

    }
}
