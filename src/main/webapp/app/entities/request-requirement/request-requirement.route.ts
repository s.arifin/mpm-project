import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { RequestRequirementComponent } from './request-requirement.component';
import { RequestRequirementEditComponent } from './request-requirement-edit.component';
import { RequestRequirementPopupComponent } from './request-requirement-dialog.component';

@Injectable()
export class RequestRequirementResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequest,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const requestRequirementRoute: Routes = [
    {
        path: 'request-requirement',
        component: RequestRequirementComponent,
        resolve: {
            'pagingParams': RequestRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const requestRequirementPopupRoute: Routes = [
    {
        path: 'request-requirement-popup-new-list/:parent',
        component: RequestRequirementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestRequirement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'request-requirement-popup-new',
        component: RequestRequirementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestRequirement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'request-requirement-new',
        component: RequestRequirementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request-requirement/:id/edit',
        component: RequestRequirementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request-requirement/:id/edit/:page',
        component: RequestRequirementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request-requirement/:id/popup-edit',
        component: RequestRequirementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestRequirement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
