import { BaseEntity } from '../shared-component';
import { Person } from '../person'
import { Suspect } from '../suspect/suspect.model';

export class SuspectPerson extends Suspect {
    constructor(
        public person?: any,
        public noMesin?: number,
        public salesDate?: any,
        public repurchaseProbability?: string,
        public priority?:  string,
        public statusSuspect?: number,
        public payment?: string,
        public lastAssignTo?: string,
        public lastAssignDate?: any,
        public statusSms?: string,
        public unitRevenue?: string,
        public lengthOfPurchase?: any,
        public freqService?: number,
        public serviceRevenue?: number,
        public freqSparePart?: number,
        public sparePartRevenue?: number,
        public totalRevenue?: number,
        public idParty?: any
    ) {
        super();
        this.person = new Person();
    }
}

export class SuspectPersonContainer {
    constructor(
        public nik?: number,
        public nama?: string,
        public noMesin?: string,
        public marketName?: string,
        public tanggalPenjualan?: string,
        public noHandphone?: number,
        public statusNoHp?: string,
        public noHandphone2?: number,
        public statusNoHp2?: string,
        public noTelpon?: number,
        public alamat1?: string,
        public alamat2?: string,
        public kelurahan?: string,
        public kecamatan?: string,
        public kota?: string,
        public namaSales?: string,
        public namaFincoy?: string,
        public tenor?: string,
        public lastAssignto?: string,
        public lastAssigndate?: string,
        public statusSms?: string,
        public salesDate?: any,
    ) {
    }
}
