import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { WorkType, WorkTypeService } from '../work-type';
import { SuspectPerson } from './suspect-person.model';
import { SuspectPersonService } from './suspect-person.service';
import { SuspectParameter } from '../suspect/suspect-parameter.model';
import { Motor, MotorService } from '../motor';
import { Salesman, SalesmanService } from '../salesman';
import { SuspectPersonContainer } from './suspect-person.model';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LoadingService } from '../../layouts/loading/loading.service';
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import * as BaseConstant from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-suspect-person-segmentation',
    templateUrl: './suspect-person-segmentation.component.html'
})
export class SuspectPersonSegmentationComponent implements OnInit, OnDestroy {

    currentAccount: any;
    suspectPeople: SuspectPerson[];
    selected: SuspectPerson[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    startDate: Number[];
    endDate: Number[];
    marketnames: Motor[];
    salesmen: Salesman[];
    display = false;
    selectedCoordinatorSales: any;
    containerData: SuspectPersonContainer[];
    SUSPECT_TYPE_SEGMENTATION = 12;
    selectedKorsal: string;
    selectedSalesman: string;
    selectedWorkType: string;
    selectedMarketName: string;
    selectedDistrict: string;
    suspectParameter: SuspectParameter;
    tgl1: Date;
    tgl2: Date;
    korsals: Salesman[];
    salesmans: Salesman[];
    worktypes: WorkType[];
    isfilter: Boolean;
    isCompleted: Boolean = false;
    smsGateway: Boolean = false;
    total: any;
    korsname: any;
    namekors: Salesman[];
    public listMotors = [{label: 'Please Select', value: null}];

    constructor(
        protected suspectPersonService: SuspectPersonService,
        protected salesmanService: SalesmanService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected marketNameService: MotorService,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected workTypeService: WorkTypeService,
        protected loadingService: LoadingService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.startDate = new Array<Number>();
        this.endDate = new Array<Number>();
        this.suspectParameter = new SuspectParameter();
    }

    loadAll() {
        if (this.currentSearch) {
            this.suspectPersonService.getAllSegmentation({
                idInternal: this.principal.getIdInternal(),
                page: this.page,
                query: this.currentSearch,
                size: this.itemsPerPage,
                // type: 'ALL-Segmentasi',
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        } else if (this.isfilter) {
            this.suspectPersonService.getByFilterSegmentation({
                idInternal: this.principal.getIdInternal(),
                page: this.page,
                // query: this.currentSearch,
                size: this.itemsPerPage,
                type: 'ALL-Segmentasi',
                marketname: this.selectedMarketName != null ? this.selectedMarketName : 'null',
                worktype: this.selectedWorkType != null ? this.selectedWorkType : 'null',
                korsal: this.selectedCoordinatorSales != null ? this.selectedCoordinatorSales : 'null',
                salesman: this.selectedSalesman != null ? this.selectedSalesman : 'null',
                district: this.selectedDistrict != null ? this.selectedDistrict : 'null',
                datefrom: this.tgl1 != null ? this.tgl1.toJSON() : 'null',
                datethru: this.tgl2 != null ? this.tgl2.toJSON() : 'null',
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        } else {
                this.suspectPersonService.getAllSegmentation({
                    idInternal: this.principal.getIdInternal(),
                        page: this.page,
                        size: this.itemsPerPage,
                        sort: this.sort()}).subscribe(
                        (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json)
                );
        }
        this.loadingService.loadingStart();
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    loadDate() {
        const date = new Date();
        for (let i = date.getFullYear() - 10; i <= date.getFullYear() + 10; i++) {
            this.startDate = [...this.startDate, i];
            this.endDate = [...this.endDate, i];
        }
    }

    transition() {
        this.router.navigate(['/suspect-person-segmentation'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/suspect-person-segmentation', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/suspect-person-segmentation', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.loadKorsal();
        this.workTypeService.getAll()
            .subscribe((res: ResponseWrapper) => { this.worktypes = res.json; });

        this.marketNameService.getAll().subscribe((res: ResponseWrapper) => {
            this.marketnames = res.json;
            this.marketnames.forEach((element) => {
                this.listMotors.push({
                    label: element.idProduct + ' - ' + element.description + ' - ' + element.name,
                    value: element.description });
            });
        });
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        // this.registerChangeInSuspectPeople();
        this.loadDate();
        this.containerData = new Array<SuspectPersonContainer>();
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSuspectPeople() {
        this.eventSubscriber = this.eventManager.subscribe('suspectPersonListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idparty') {
            result.push('idparty');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.suspectPeople = new Array<SuspectPerson>();
        this.queryCount = this.totalItems = data.length > 0 ? data[0].TotalData : 0;
        this.suspectPeople = data;
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.suspectPersonService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.suspectPersonService.update(event.data)
                .subscribe((res: SuspectPerson) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.suspectPersonService.create(event.data)
                .subscribe((res: SuspectPerson) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: SuspectPerson) {
        this.toasterService.showToaster('info', 'SuspectPerson Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    assign() {
        this.selectedCoordinatorSales = null;
        this.display = true;
    }

    assignTo() {
        this.display = false;
        this.suspectPersonService.creat (this.principal.getIdInternal(), this.selectedCoordinatorSales, this.principal.getUserLogin().replace('.', '$'), this.selected).subscribe((response) => {
            {       this.salesmanService.filterid(this.selectedCoordinatorSales).subscribe((res) => {
                    this.namekors = res.json;
                    console.log('nama == ', this.namekors[0].name);
                    this.korsname = this.namekors[0].name;
                    console.log('cuuux == ', this.korsname);
                    this.smsGateway = true;
                });
            }
            this.selected = new Array<SuspectPerson>();
        this.router.navigate(['/suspect-person-segmentation']);
        this.loadingService.loadingStop();
        this.smsGateway = true;
        this.total = response.length;
        });
    }

    smsDeliver() {
        this.suspectPersonService.smsDelivered(this.principal.getUserLogin().replace(/\./g, '$'), this.selected).subscribe((response) => {
            this.smsNo();
        });
    }

    smsNo() {
        this.smsGateway = false;
        this.loadingService.loadingStart();
        this.loadingService.loadingStop();
        this.isCompleted = true;
        // this.ngOnInit();
    }

    filter() {
        this.page = 1;
        this.currentSearch = '';
        this.isfilter = true;
        this.router.navigate(['/suspect-person-segmentation', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    private  loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.filter(obj).subscribe(
            (res: ResponseWrapper) => {
                // console.log('tarik data korsal', res.json);
                this.korsals = res.json;
            }
        )
    }

    public selectSalesByKorsal(): void {
        const obj: object = {
            idCoordinator : this.selectedKorsal
        }
        this.salesmanService.filter(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('selected Sales' , res.json);
                this.salesmans = res.json;
            }
        )
    }

    trackMarketNameById(index: number, item: Motor) {
        return item.idProduct;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idSalesman;
    }

    trackWorkTypeById(index: number, item: WorkType) {
        return item.idWorkType;
    }

    getDistrict(district: any) {
        this.selectedDistrict = district;
    }

}
