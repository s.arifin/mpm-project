import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { SuspectPerson } from './suspect-person.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { createSuspectParameterOption } from '../suspect/suspect-parameter.util';

@Injectable()
export class SuspectPersonService {
    protected itemValues: SuspectPerson[];
    protected message: String;

    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);
    messages: BehaviorSubject<any> = new BehaviorSubject<any>(this.message);

    protected resourceUrl = SERVER_API_URL + 'api/suspect-people';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/suspect-people';
    protected resourceUURL = SERVER_API_URL + '/api/suspects';
    protected resourceCUrl = process.env.API_C_URL + '/api/suspect';
    private resourceDOAUrl = process.env.API_C_URL + '/api/suspect/detailRepeatOrder';
    private resourceSAUrl = process.env.API_C_URL + '/api/suspect/detailSegmentation';

    constructor(protected http: Http) { }

    // SERVICE JAVA ///

    create(suspectPerson: SuspectPerson): Observable<SuspectPerson> {
        const copy = this.convert(suspectPerson);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(suspectPerson: SuspectPerson): Observable<SuspectPerson> {
        const copy = this.convert(suspectPerson);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<SuspectPerson> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idsuspecttype', req.idsuspecttype);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryall(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/all', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryalls(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/alls', options)
            .map((res: Response) => this.convertResponse(res));
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createSuspectParameterOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(suspectPerson: SuspectPerson, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(suspectPerson);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, suspectPerson: SuspectPerson): Observable<SuspectPerson> {
        const copy = this.convert(suspectPerson);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, suspectPersons: SuspectPerson[]): Observable<SuspectPerson[]> {
        const copy = this.convertList(suspectPersons);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            this.message = res.headers.toJSON()['x-mpmapp-alert'][0];
            this.messages.next(this.message);
            return res.json();
        });
    }

    // SERVICE .NET
    // SUSPECT - PERSON
    getByFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/filterOther', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAll(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl , options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAll3in1(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/3in1', options)
            .map((res: Response) => this.convertResponse(res));
    }

    // SUSPECT - SEGMENTASI

    getByFilterSegmentation(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/filterSegmentation', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllSegmentation(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/segmentation' , options)
            .map((res: Response) => this.convertResponse(res));
    }

    creat(idInternal: String , idkorsal: String, loginName: string, suspectPersons: SuspectPerson[]): Observable<SuspectPerson[]> {
        const copy = this.convertList(suspectPersons);
        return this.http.post(this.resourceCUrl + '/createSegmentation/' + idInternal + '/' + idkorsal + '/' + loginName, copy).map((res: Response) => {
            // this.message = res.headers.toJSON()['x-mpmapp-alert'][0];
            // this.messages.next(this.message);
            return res.json();
        });
    }

    // SUSPECT - REPEAT ORDER

    getByFilterRO(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/filterRepeatOrder', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getallRO(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/repeatOrder' , options)
            .map((res: Response) => this.convertResponse(res));
    }

    created(idInternal: String , idkorsal: String, loginName: string, suspectPersons: SuspectPerson[]): Observable<SuspectPerson[]> {
        const copy = this.convertList(suspectPersons);
        return this.http.post(this.resourceCUrl + '/createRepeatOrder/' + idInternal + '/' + idkorsal + '/' + loginName, copy).
        map((res: Response) => { return res.json();
        });
    }

    assign(idkorsal: String, loginName: string, suspectPersons: SuspectPerson[]): Observable<SuspectPerson[]> {
        const copy = this.convertList(suspectPersons);
        return this.http.post(this.resourceCUrl + '/assign/' + idkorsal + '/' + loginName, copy).map((res: Response) => {
            // this.message = res.headers.toJSON()['x-mpmapp-alert'][0];
            // this.messages.next(this.message);
            return res.json();
        });
    }

    assign3in1(idInternal: String, idkorsal: String, loginName: string, suspectPersons: SuspectPerson[]): Observable<SuspectPerson[]> {
        const copy = this.convertList(suspectPersons);
        return this.http.post(this.resourceCUrl + '/create3in1/' + idInternal + '/' + idkorsal + '/' + loginName, copy).map((res: Response) => {
            // this.message = res.headers.toJSON()['x-mpmapp-alert'][0];
            // this.messages.next(this.message);
            return res.json();
        });
    }

    smsDelivered(loginName: string, suspectPersons: SuspectPerson[]): Observable<SuspectPerson[]> {
        const copy = this.convertList(suspectPersons);
        return this.http.post(this.resourceCUrl + '/smsReminder/' + loginName, copy).map((res: Response) => {
            // this.message = res.headers.toJSON()['x-mpmapp-alert'][0];
            // this.messages.next(this.message);
            return res.json();
        });
    }

    uploadOther(suspectPersons: SuspectPerson[]): Observable<SuspectPerson[]> {
        const copy = this.convertList(suspectPersons);
        return this.http.post(this.resourceCUrl + '/other/upload', copy).map((res: Response) => {
            // this.message = res.headers.get('x-mpmapp-alert');
            // this.messages.next(this.message);
            return res.json();
        });
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(suspectPerson: SuspectPerson): SuspectPerson {
        if (suspectPerson === null || suspectPerson === {}) {
            return {};
        }
        // const copy: SuspectPerson = Object.assign({}, suspectPerson);
        const copy: SuspectPerson = JSON.parse(JSON.stringify(suspectPerson));
        return copy;
    }

    protected convertList(suspectPersons: SuspectPerson[]): SuspectPerson[] {
        const copy: SuspectPerson[] = suspectPersons;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: SuspectPerson[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    findSegmentasi(id: any): Observable<SuspectPerson> {
        return this.http.get(`${this.resourceSAUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findRepeatOrder(id: any): Observable<SuspectPerson> {
        return this.http.get(`${this.resourceDOAUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }
}
