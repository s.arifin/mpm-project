import { Observable, Subscription } from 'rxjs/Rx';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { BaseEntity } from '../shared-component';

import { SuspectPerson, SuspectPersonContainer, SuspectPersonService } from '../suspect-person';
import { SuspectParameter } from '../suspect/suspect-parameter.model'
import { WorkType, WorkTypeService } from '../work-type';
import { Salesman, SalesmanService } from '../salesman';
import { Motor, MotorService } from '../motor';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { District } from './../district/district.model';
import { DistrictService } from './../district/district.service';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';

import { LoadingService } from '../../layouts/loading/loading.service';
import * as XLSX from 'xlsx'
import { saveAs } from 'file-saver';
import * as BaseConstant from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-suspect-person',
    templateUrl: './suspect-person.component.html'
})
export class SuspectPersonComponent implements OnInit, OnDestroy {
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    korsname: any;
    namekors: Salesman[];
    predicate: any;
    previousPage: any;
    reverse: any;
    currentAccount: any;
    error: any;
    success: any;
    selectedCoordinatorSales: any;
    selectedCoordinatorSalesName: any;
    marketnames: Motor[];
    suspectPerson: SuspectPerson;
    suspectParameter: SuspectParameter;
    tgl1: Date;
    tgl2: Date;
    total: any;
    korsal: any;
    startDate: Number[];
    endDate: Number[];
    selected: SuspectPerson[];
    suspectPeople: SuspectPerson[];
    districts: District[];
    korsals: Salesman[];
    salesmans: Salesman[];
    selectedKorsal: string;
    selectedSalesman: string;
    selectedWorkType: string;
    selectedMarketName: string;
    selectedDistrict: string;
    worktypes: WorkType[];
    containerData: SuspectPersonContainer[];
    eventSubscriber: Subscription;
    display: Boolean = false;
    isCompleted: Boolean = false;
    smsGateway: Boolean = false;
    isfilter: Boolean;
    invalidFormat: Boolean;
    public listMotors = [{label: 'Please Select', value: null}];

    SUSPECT_TYPE_UPLOAD = 10;

    currentSearch: String;

    constructor(
        protected marketNameService: MotorService,
        protected suspectPersonService: SuspectPersonService,
        protected salesmanService: SalesmanService,
        protected workTypeService: WorkTypeService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected loadingService: LoadingService,
        private districtService: DistrictService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<SuspectPerson>();
        this.suspectParameter = new SuspectParameter();
        this.startDate = new Array<Number>();
        this.endDate = new Array<Number>();
        this.korsals = [];
        this.salesmans = [];
        this.districts = [];
    }

    loadAll() {
        if (this.currentSearch) {
            this.suspectPersonService.getAll3in1({
                idInternal: this.principal.getIdInternal(),
                page: this.page,
                query: this.currentSearch,
                size: this.itemsPerPage,
                type: 'ALL',
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        } else if (this.isfilter) {
            this.suspectPersonService.getAll3in1({
                idInternal: this.principal.getIdInternal(),
                page: this.page,
                // query: this.currentSearch,
                size: this.itemsPerPage,
                type: 'ALL',
                marketName: this.selectedMarketName,
                workTypeId: this.selectedWorkType,
                idKorsal: this.selectedCoordinatorSales,
                idSalesman: this.selectedSalesman,
                districtId: this.selectedDistrict,
                datefromsales: this.tgl1 != null ? this.tgl1.toJSON() : '',
                datethrusales: this.tgl2 != null ? this.tgl2.toJSON() : '',
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
        this.loadingService.loadingStart();
            return;
        }
    }

    loadDate() {
        const date = new Date();
        for (let i = date.getFullYear() - 10; i <= date.getFullYear() + 10; i++) {
            this.startDate = [...this.startDate, i];
            this.endDate = [...this.endDate, i];
        }
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/suspect-person'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.isfilter = false;
        this.currentSearch = '';
        this.reset();
        this.suspectPeople = new Array<SuspectPerson>();
    }

    getNames(name: string) {
        this.selectedCoordinatorSalesName = name;
        console.log(name, 'keluar dong')
    }

    reset() {
        this.selectedMarketName = null;
        this.selectedWorkType = null;
        this.tgl1 = null;
        this.tgl2 = null;
        this.selectedDistrict = null ;
        this.selectedKorsal = null;
        this.selectedSalesman = null;
        this.suspectPeople = new Array<SuspectPerson>();

    }

    filter() {
        this.page = 1;
        this.currentSearch = '';
        this.isfilter = true;
        this.router.navigate(['/suspect-person', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.isfilter = false;
        this.page = 1;
        this.currentSearch = query;
        this.router.navigate(['/suspect-person', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.loadKorsal();
        this.loadDistrict();
        this.workTypeService.getAll()
            .subscribe((res: ResponseWrapper) => { this.worktypes = res.json; });

        this.marketNameService.getAll().subscribe((res: ResponseWrapper) => {
            this.marketnames = res.json;
            this.marketnames.forEach((element) => {
                this.listMotors.push({
                    label: element.idProduct + ' - ' + element.description + ' - ' + element.name,
                    value: element.description });
            });
        });
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        // this.registerChangeInSuspectPeople();
        this.loadDate();
        this.containerData = new Array<SuspectPersonContainer>();
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSuspectPeople() {
        this.eventSubscriber = this.eventManager.subscribe('suspectPersonListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idparty') {
            result.push('idparty');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.suspectPeople = new Array<SuspectPerson>();
        this.queryCount = this.totalItems = data.length > 0 ? data[0].TotalData : 0;
        this.suspectPeople = data;
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    protected subscribeToSaveResponse(result: Observable<SuspectPerson[]>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe(
                    (res: SuspectPerson[]) => {
                        resolve();
                    }
                );
            }
        );
    }

    assign() {
        this.selectedCoordinatorSales = null;
        this.display = true;
    }

    assignTo() {
        this.display = false;
        this.loadingService.loadingStart();
        this.suspectPersonService.assign3in1(this.principal.getIdInternal(), this.selectedCoordinatorSales, this.principal.getUserLogin().replace(/\./g, '$'), this.selected).subscribe((response) => {
            {
                this.salesmanService.filterid(this.selectedCoordinatorSales).subscribe((res) => {
                this.namekors = res.json;
                this.korsname = this.namekors[0].name;
            });
        }
        this.selected = new Array<SuspectPerson>();
        this.router.navigate(['/suspect-person']);
        this.loadingService.loadingStop();
        this.smsGateway = true;
        this.total = response.length;
        });
    }

    smsDeliver() {
        this.suspectPersonService.smsDelivered(this.principal.getUserLogin().replace(/\./g, '$'), this.selected).subscribe((response) => {
            this.smsNo();
        });
    }

    smsNo() {
        this.smsGateway = false;
        this.loadingService.loadingStop();
        this.isCompleted = true;
        this.ngOnInit();
    }

    trackMarketNameById(index: number, item: Motor) {
        return item.idProduct;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idSalesman;
    }

    trackWorkTypeById(index: number, item: WorkType) {
        return item.idWorkType;
    }

    getDistrict(district: any) {
        this.selectedDistrict = district;
    }

    DataToExport() {
        this.suspectPeople.forEach( (element) => {
            const suspectPersonContainer = new SuspectPersonContainer();
            suspectPersonContainer.nik = element.person.personalIdNumber;
            suspectPersonContainer.nama = element.person.firstName + ' ' + element.person.firstName;
            this.containerData.push(suspectPersonContainer);
        });
    }
    /* bisa tapi masih berantakan formatnya */
    cobaExport() {
        this.DataToExport();
        const ws_data = this.containerData;
        /*create sheet data & add to workbook*/
        const ws_name = 'Data Prospect'
       // const ws = XLSX.utils.aoa_to_sheet[this.suspectPeople[0].person.name];
        const ws = XLSX.utils.json_to_sheet(ws_data);
        const wb: XLSX.WorkBook = { SheetNames: [], Sheets: {} };
        wb.Props = {
            Title: 'Daftar Prospect'
        };
        XLSX.utils.book_append_sheet(wb, ws, ws_name);
        const wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: false, type: 'binary'});
        saveAs(new Blob([s2ab(wbout)], { type: 'application/vnd.ms-excel' }), 'exported.xlsx');
        function s2ab(s) {
            const buf = new ArrayBuffer(s.length);
            const view = new Uint8Array(buf);
            for (let i = 0; i !== s.length; ++i) {
              // tslint:disable-next-line:no-bitwise
              view[i] = s.charCodeAt(i) & 0xFF;
            };
            return buf;
        }
    }

    private  loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.filter(obj).subscribe(
            (res: ResponseWrapper) => {
                // console.log('tarik data korsal', res.json);
                this.korsals = res.json;
            }
        )
    }

    private loadDistrict(): void {
        const obj: object = {
            idinternal: this.principal.getIdInternal()
        };
        this.districtService.GetRing(obj).subscribe(
            (res: ResponseWrapper) => {
                this.districts = res.json;
                console.log('tes', this.districts)
            }
        )
    }

    public selectSalesByKorsal(): void {
        const obj: object = {
            idCoordinator : this.selectedKorsal
        }
        this.salesmanService.filter(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('selected Sales' , res.json);
                this.salesmans = res.json;
            }
        )
    }
}
