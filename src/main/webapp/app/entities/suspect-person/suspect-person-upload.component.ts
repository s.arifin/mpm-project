import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { SuspectPerson } from './suspect-person.model';
import { SuspectPersonService } from './suspect-person.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService, FileUploadModule} from 'primeng/primeng';

import { LoadingService } from '../../layouts/loading/loading.service';

import * as XLSX from 'xlsx';
import { calendarFormat, localeData } from 'moment';

@Component({
    selector: 'jhi-suspect-person-upload',
    templateUrl: './suspect-person-upload.component.html'
})
export class SuspectPersonUploadComponent implements OnInit, OnDestroy {

    suspectPersonUpload: SuspectPerson[];
    suspectPerson: SuspectPerson;
    isSaving: boolean;
    invalidFormat: Boolean;
    isCompleted: Boolean;
    summaryMessage: String;
    total: any;
    korsal: any;
    currentAccount: any;
    error: any;
    success: any;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    containerData: any;

    constructor(
        protected alertService: JhiAlertService,
        protected suspectPersonService: SuspectPersonService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected loadingService: LoadingService
    ) {
        this.suspectPersonUpload = new Array<SuspectPerson>();
        this.itemsPerPage = ITEMS_PER_PAGE;
    }

    ngOnInit() {
        this.invalidFormat = false;
        this.isCompleted = false;
    }

    ngOnDestroy() {
    }

    previousState() {
        this.router.navigate(['suspect-person-other']);
    }

    backToPreviousState() {
        this.isCompleted = false;
        this.previousState();
    }

    save() {
        this.loadingService.loadingStart();
        this.isSaving = true;
        setTimeout(
            () => {
                this.subscribeToSaveResponse(
                    this.suspectPersonService.executeListProcess(101, this.principalService.getIdInternal(), this.suspectPersonUpload)
                ).then(
                    () => {
                        this.suspectPersonService.messages.subscribe(
                            (response) => {
                                if (response !== undefined) {
                                    this.summaryMessage = response;
                                    this.isCompleted = true;
                                    this.loadingService.loadingStop();
                                }
                            }
                        )
                    }
                );
            }, 1000);
    }

    protected convertXLXStoListSuspect(): void {
        if (this.containerData[0]['No. KTP'] &&
            this.containerData[0]['Nama'] &&
            this.containerData[0]['No. Handphone']) {

            this.suspectPersonUpload = new Array<SuspectPerson>();

            this.containerData.forEach( (element) => {
                this.suspectPerson = new SuspectPerson();

                if (element['Nama'].split(' ').length === 1) {
                    this.suspectPerson.person.firstName = element['Nama'];
                    this.suspectPerson.person.lastName = '';
                } else {
                    this.suspectPerson.person.firstName = element['Nama'].split(' ').slice(0, -1).join(' ');
                    this.suspectPerson.person.lastName = element['Nama'].split(' ').slice(-1).join(' ');
                }

                this.suspectPerson.person.personalIdNumber = element['No. KTP'];
                this.suspectPerson.person.cellPhone1 = element['No. Handphone'];
                this.suspectPerson.person.postalAddress.address1 = element['Alamat 1'];
                this.suspectPerson.person.postalAddress.address2 = element['Alamat 2'];
                this.suspectPerson.person.postalAddress.villageName = element['Kelurahan'];
                this.suspectPerson.person.postalAddress.districtName = element['Kecamatan'];
                this.suspectPerson.person.postalAddress.cityName = element['Kota / Kabupaten'];
                this.suspectPerson.marketName = element['Market Name'];
                this.suspectPerson.salesDate = element ['Tanggal Pembelian'] ;
                this.suspectPerson.person.workTypeDescription = element['Pekerjaan'];
                this.suspectPerson.dealerId = this.principalService.getIdInternal();
                this.suspectPersonUpload = [...this.suspectPersonUpload, this.suspectPerson];
            });
        } else {
            this.invalidFormat = true;
        }
    }

    protected subscribeToSaveResponse(result: Observable<SuspectPerson[]>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe(
                    (res: SuspectPerson[]) => {
                        this.onSaveSuccess(res)
                        resolve();
                    }
                );
            }
        );
    }

    protected onSaveSuccess(result: SuspectPerson[]) {
        // this.eventManager.broadcast({ name: 'suspectPersonListModification', content: 'OK'});
        // this.toaster.showToaster('info', 'Save', 'suspectPerson saved !');
        this.isSaving = false;
        // this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'suspectPerson Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.suspectPersonService.update(event.data)
                .subscribe((res: SuspectPerson) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.suspectPersonService.create(event.data)
                .subscribe((res: SuspectPerson) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: SuspectPerson) {
        this.toasterService.showToaster('info', 'SuspectPerson Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    onFileChange(evt: any) {
        /* wire up file reader */
        const target: DataTransfer = <DataTransfer>(evt.target);
        if (target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
          /* read workbooka */
          const bstr: string = e.target.result;
          const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});
          /* grab first sheet */
          const wsname: string = wb.SheetNames[0];
          const ws: XLSX.WorkSheet = wb.Sheets[wsname];
          /* save data */
          this.containerData = (XLSX.utils.sheet_to_json(ws));

          this.convertXLXStoListSuspect();
        };
        reader.readAsBinaryString(target.files[0]);
      }
}
