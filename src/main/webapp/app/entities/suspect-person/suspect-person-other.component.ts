import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { WorkType, WorkTypeService } from '../work-type';
import { SuspectPerson , SuspectPersonContainer } from './suspect-person.model';
import { SuspectPersonService } from './suspect-person.service';
import { SuspectParameter } from '../suspect/suspect-parameter.model';
import { Salesman, SalesmanService } from '../salesman';
import { Motor, MotorService } from '../motor';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { District } from './../district/district.model';
import { DistrictService } from './../district/district.service';
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';
import * as XLSX from 'xlsx'
import { saveAs } from 'file-saver';
import * as BaseConstant from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-suspect-person-other',
    templateUrl: './suspect-person-other.component.html'
})
export class SuspectPersonOtherComponent implements OnInit, OnDestroy {

    currentAccount: any;
    suspectPersonOther: SuspectPerson[];
    selected: SuspectPerson[];
    error: any;
    success: any;
    korsname: any;
    namekors: Salesman[];
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    startDate: Number[];
    endDate: Number[];
    marketnames: Motor[];
    salesmen: Salesman[];
    display = false;
    selectedCoordinatorSales: any;
    selectedKorsal: string;
    selectedSalesman: string;
    selectedWorkType: string;
    selectedMarketName: string;
    selectedDistrict: string;
    suspectParameter: SuspectParameter;
    tgl1: Date;
    tgl2: Date;
    total: any;
    korsal: any;
    isfilter: Boolean;
    SUSPECT_TYPE_OTHER = 99;
    containerData: SuspectPersonContainer[];
    korsals: Salesman[];
    salesmans: Salesman[];
    worktypes: WorkType[];
    districts: District[];
    isCompleted: Boolean = false;
    smsGateway: Boolean = false;
    public listMotors = [{label: 'Please Select', value: null}];

    constructor(
        protected marketNameService: MotorService,
        protected suspectPersonService: SuspectPersonService,
        protected salesmanService: SalesmanService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected workTypeService: WorkTypeService,
        protected toasterService: ToasterService,
        protected loadingService: LoadingService,
        private districtService: DistrictService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<SuspectPerson>();
        this.suspectParameter = new SuspectParameter();
        this.startDate = new Array<Number>();
        this.endDate = new Array<Number>();
        this.korsals = [];
        this.salesmans = [];
        this.districts = [];
    }

    loadAll() {
        if (this.currentSearch) {
            this.suspectPersonService.getByFilter({
                idInternal: this.principal.getIdInternal(),
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        } else if (this.isfilter) {
            this.suspectPersonService.getByFilter({
                idInternal: this.principal.getIdInternal(),
                page: this.page,
                size: this.itemsPerPage,
                marketname: this.selectedMarketName,
                worktype: this.selectedWorkType,
                korsal: this.selectedCoordinatorSales,
                salesman: this.selectedSalesman,
                district: this.selectedDistrict,
                datefrom: this.tgl1 != null ? this.tgl1.toJSON() : '',
                datethru: this.tgl2 != null ? this.tgl2.toJSON() : '',
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        } else {
            this.suspectPersonService.getAll({
                idInternal: this.principal.getIdInternal(),
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
        this.loadingService.loadingStart();
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    loadDate() {
        const date = new Date();
        for (let i = date.getFullYear() - 10; i <= date.getFullYear() + 10; i++) {
            this.startDate = [...this.startDate, i];
            this.endDate = [...this.endDate, i];
        }
    }

    transition() {
        this.router.navigate(['/suspect-person-other'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.isfilter = false;
        this.currentSearch = '';
        this.reset();
        this.router.navigate(['/suspect-person-other']);
        this.loadAll();
    }

    reset() {
        this.selectedMarketName = null;
        this.selectedWorkType = null;
        this.tgl1 = null;
        this.tgl2 = null;
        this.selectedDistrict = null ;
        this.selectedKorsal = null;
        this.selectedSalesman = null;

    }

    filter() {
        this.page = 1;
        this.currentSearch = '';
        this.isfilter = true;
        this.router.navigate(['/suspect-person-other', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.isfilter = false;
        this.page = 1;
        this.currentSearch = query;
        this.router.navigate(['/suspect-person-other', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.loadKorsal();
        this.loadDistrict();
        this.workTypeService.getAll()
            .subscribe((res: ResponseWrapper) => { this.worktypes = res.json; });
        this.marketNameService.getAll().subscribe((res: ResponseWrapper) => {
                this.marketnames = res.json;
                this.marketnames.forEach((element) => {
                    this.listMotors.push({
                        label: element.idProduct + ' - ' + element.description + ' - ' + element.name,
                        value: element.description });
            });
        });

        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.loadDate();
        this.containerData = new Array<SuspectPersonContainer>();
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SuspectPerson) {
        return item.idSuspect;
    }

    registerChangeInSuspectPeople() {
        this.eventSubscriber = this.eventManager.subscribe('suspectPersonListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idSuspect') {
            result.push('idSuspect');
        }
        return result;
    }

    private loadDistrict(): void {
        const obj: object = {
            idinternal: this.principal.getIdInternal()
        };
        this.districtService.GetRing(obj).subscribe(
            (res: ResponseWrapper) => {
                this.districts = res.json;
                console.log('tes', this.districts)
            }
        )
    }

    private loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.filter(obj).subscribe(
            (res: ResponseWrapper) => {
                // console.log('tarik data korsal', res.json);
                this.korsals = res.json;
            }
        )
    }

    protected onSuccess(data, headers) {
        this.suspectPersonOther = new Array<SuspectPerson>();
        this.totalItems = data[0].TotalData;
        this.queryCount = this.totalItems;
        this.suspectPersonOther = data;
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    executeProcess(data) {
        this.suspectPersonService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.suspectPersonService.update(event.data)
                .subscribe((res: SuspectPerson) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.suspectPersonService.create(event.data)
                .subscribe((res: SuspectPerson) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: SuspectPerson) {
        this.toasterService.showToaster('info', 'SuspectPerson Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    assign() {
        this.selectedCoordinatorSales = null;
        this.display = true;
    }

    assignTo() {
        this.display = false;
        this.loadingService.loadingStart();
        this.suspectPersonService.assign(this.selectedCoordinatorSales, this.principal.getUserLogin().replace(/\./g, '$'), this.selected).subscribe((response) => {
            {
                this.salesmanService.filterid(this.selectedCoordinatorSales).subscribe((res) => {
                this.namekors = res.json;
                this.korsname = this.namekors[0].name;
            });
        }
        this.selected = new Array<SuspectPerson>();
        this.router.navigate(['/suspect-person-other']);
        this.loadingService.loadingStop();
        this.smsGateway = true;
        this.total = response.length;
        });
    }

    smsDeliver() {
        this.suspectPersonService.smsDelivered(this.principal.getUserLogin().replace(/\./g, '$'), this.selected).subscribe((response) => {
            this.smsNo();
        });
    }

    smsNo() {
        this.smsGateway = false;
        this.loadingService.loadingStop();
        this.isCompleted = true;
        this.ngOnInit();
    }

    DataToExport() {
        this.suspectPersonOther.forEach( (element) => {
            const suspectPersonContainer = new SuspectPersonContainer();
            suspectPersonContainer.nik = element.person.personalIdNumber;
            suspectPersonContainer.nama = element.person.firstName + ' ' + element.person.lastName;
            this.containerData.push(suspectPersonContainer);
        });
    }
    /* bisa tapi masih berantakan formatnya */
    cobaExport() {
        this.DataToExport();
        const ws_data = this.containerData;
        /*create sheet data & add to workbook*/
        const ws_name = 'Data Prospect'
       // const ws = XLSX.utils.aoa_to_sheet[this.suspectPeople[0].person.name];
        const ws = XLSX.utils.json_to_sheet(ws_data);
        const wb: XLSX.WorkBook = { SheetNames: [], Sheets: {} };
        wb.Props = {
            Title: 'Daftar Prospect'
        };
        XLSX.utils.book_append_sheet(wb, ws, ws_name);
        const wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: false, type: 'binary'});
        saveAs(new Blob([s2ab(wbout)], { type: 'application/vnd.ms-excel' }), 'exported.xlsx');
        function s2ab(s) {
            const buf = new ArrayBuffer(s.length);
            const view = new Uint8Array(buf);
            for (let i = 0; i !== s.length; ++i) {
              // tslint:disable-next-line:no-bitwise
              view[i] = s.charCodeAt(i) & 0xFF;
            };
            return buf;
        }
    }

    trackMarketNameById(index: number, item: Motor) {
        return item.idProduct;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idSalesman;
    }

    trackWorkTypeById(index: number, item: WorkType) {
        return item.idWorkType;
    }

    getDistrict(district: any) {
        this.selectedDistrict = district;
    }

    public selectSalesByKorsal(): void {
        const obj: object = {
            idCoordinator : this.selectedKorsal
        }
        this.salesmanService.filter(obj).subscribe(
            (res: ResponseWrapper) => {
                // console.log('selected Sales' , res.json);
                this.salesmans = res.json;
            }
        )
    }
}
