import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    SuspectPersonService,
    SuspectPersonPopupService,
    SuspectPersonComponent,
    SuspectPersonSegmentationComponent,
    SuspectPersonROComponent,
    SuspectPersonOtherComponent,
    suspectPersonRoute,
    suspectPersonPopupRoute,
    SuspectPersonResolvePagingParams,
    SuspectPersonUploadComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         FileUploadModule,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule
        } from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...suspectPersonRoute,
    ...suspectPersonPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        FileUploadModule,
        TabViewModule,
        ScheduleModule,

        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule
    ],
    exports: [
        SuspectPersonComponent,
        SuspectPersonSegmentationComponent,
        SuspectPersonROComponent,
        SuspectPersonOtherComponent,
        SuspectPersonUploadComponent
    ],
    declarations: [
        SuspectPersonComponent,
        SuspectPersonSegmentationComponent,
        SuspectPersonROComponent,
        SuspectPersonOtherComponent,
        SuspectPersonUploadComponent
    ],
    entryComponents: [
        SuspectPersonComponent,
        SuspectPersonSegmentationComponent,
        SuspectPersonROComponent,
        SuspectPersonOtherComponent,
        SuspectPersonUploadComponent
    ],
    providers: [
        SuspectPersonService,
        SuspectPersonPopupService,
        SuspectPersonResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmSuspectPersonModule {}
