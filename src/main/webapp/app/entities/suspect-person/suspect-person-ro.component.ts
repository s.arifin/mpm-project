import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { LoadingService } from '../../layouts/loading/loading.service';
import { SuspectPerson } from './suspect-person.model';
import { SuspectPersonService } from './suspect-person.service';

import { Salesman, SalesmanService } from '../salesman';
import { SuspectParameter } from '../suspect/suspect-parameter.model'
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import * as BaseConstant from '../../shared/constants/base.constants';
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-suspect-person-ro',
    templateUrl: './suspect-person-ro.component.html'
})
export class SuspectPersonROComponent implements OnInit, OnDestroy {

    currentAccount: any;
    suspectPeopleRO: SuspectPerson[];
    selected: SuspectPerson[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    total: any;
    korsname: any;
    namekors: Salesman[];
    predicate: any;
    previousPage: any;
    reverse: any;
    korsals: Salesman[];
    salesmans: Salesman[];
    selectedKorsal: string;
    selectedSalesman: string;
    salesmen: Salesman[];
    display = false;
    selectedCoordinatorSales: any;
    selectedCoordinatorSaleses: any;
    idSuspect: any;
    statusMessage: any;
    statusSuspect: any;
    statusPayment: any;
    tgl1: Date;
    tgl2: Date;
    tgl3: Date;
    tgl4: Date;
    suspectParameter: SuspectParameter;
    SUSPECT_TYPE_RO = 11;
    isfilter: Boolean;
    isCompleted: Boolean = false;
    smsGateway: Boolean = false;

    constructor(
        protected suspectPersonService: SuspectPersonService,
        protected salesmanService: SalesmanService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected loadingService: LoadingService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<SuspectPerson>();
        this.suspectParameter = new SuspectParameter();
        this.idSuspect = new Array <SuspectPerson>();
        // this.namekors[0].name = name;
        // console.log('nama == ', this.namekors[0].name);
    }

    loadAll() {
        if (this.currentSearch) {
            this.suspectPersonService.getByFilterRO({
                idInternal: this.principal.getIdInternal(),
                page: this.page,
                size: this.itemsPerPage,
                query: this.currentSearch,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        } else
         if (this.isfilter) {
            this.suspectPersonService.getByFilterRO({
                idInternal: this.principal.getIdInternal(),
                page: this.page,
                size: this.itemsPerPage,
                korsal: this.selectedCoordinatorSaleses != null ? this.selectedCoordinatorSaleses : '',
                datefromassign: this.tgl3 != null ? this.tgl3.toJSON() : '',
                datethruassign: this.tgl4 != null ? this.tgl4.toJSON() : '',
                statusmessage: this.statusMessage,
                statussuspect: this.statusSuspect,
                statuspayment: this.statusPayment,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        } else {
            this.suspectPersonService.getallRO({
                idInternal: this.principal.getIdInternal(),
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
        this.loadingService.loadingStart();
        }
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/suspect-person-ro'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.isfilter = false;
        this.currentSearch = '';
        this.reset();
        this.router.navigate(['/suspect-person-ro']);
        this.loadAll();
    }

    reset() {
        this.selectedCoordinatorSaleses = null;
        this.tgl1 = null;
        this.tgl2 = null;
        this.tgl3 = null;
        this.tgl4 = null;
        this.statusMessage = null ;
        this.statusSuspect = null;
        this.statusPayment = null;

    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.isfilter = false;
        this.page = 1;
        this.currentSearch = query;
        this.router.navigate(['/suspect-person-ro', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll()
        this.loadKorsal();
        this.salesmanService.queryFilterBy({
                idInternal : this.principal.getIdInternal(),
                size: 1000
            })
            .subscribe((res: ResponseWrapper) => {
                this.salesmen = res.json.filter((d) => d.coordinator === true); },
            (res: ResponseWrapper) => this.onError(res.json));

        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSuspectPeople();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idSalesman;
    }

    registerChangeInSuspectPeople() {
        this.eventSubscriber = this.eventManager.subscribe('suspectPersonListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idparty') {
            result.push('idparty');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.suspectPeopleRO = new Array<SuspectPerson>();
        this.queryCount = this.totalItems = data.length > 0 ? data[0].TotalData : 0;
        this.suspectPeopleRO = data;
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.suspectPersonService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.suspectPersonService.update(event.data)
                .subscribe((res: SuspectPerson) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.suspectPersonService.create(event.data)
                .subscribe((res: SuspectPerson) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: SuspectPerson) {
        this.toasterService.showToaster('info', 'SuspectPerson Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    // getsales() {
    //     this.salesmanService.filterid(this.selectedCoordinatorSales).subscribe((res) => {
    //         this.namekors = res[0].name;
    //         console.log(res[0].name);
    //         this.smsGateway = true;
    //     });
    // }

    assign() {
        this.selectedCoordinatorSales = null;
        this.display = true;
    }

    assignTo() {
        this.display = false;
        this.loadingService.loadingStart();
        this.suspectPersonService.created (this.principal.getIdInternal(), this.selectedCoordinatorSales, this.principal.getUserLogin().replace(/\./g, '$'), this.selected).subscribe((response) => {
                {   this.salesmanService.filterid(this.selectedCoordinatorSales).subscribe((res) => {
                    this.namekors = res.json;
                    console.log('nama == ', this.namekors[0].name);
                    this.korsname = this.namekors[0].name;
                    console.log('cuuux == ', this.korsname);
                });
            }
            this.selected = new Array<SuspectPerson>();
            this.router.navigate(['/suspect-person-ro']);
            this.loadingService.loadingStop();
            this.smsGateway = true;
            this.total = response.length;
        });
    }

    smsDeliver() {
        this.suspectPersonService.smsDelivered(this.principal.getUserLogin().replace(/\./g, '$'), this.selected).subscribe((response) => {
            this.smsNo();
        });
    }

    smsNo() {
        this.smsGateway = false;
        this.loadingService.loadingStop();
        this.isCompleted = true;
        this.ngOnInit();
        // this.ngOnInit();
    }

    private  loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.filter(obj).subscribe(
            (res: ResponseWrapper) => {
                // console.log('tarik data korsal', res.json);
                this.korsals = res.json;
            }
        )
    }

    filter() {
        this.page = 1;
        this.currentSearch = '';
        this.isfilter = true;
        this.router.navigate(['/suspect-person-ro', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
}
