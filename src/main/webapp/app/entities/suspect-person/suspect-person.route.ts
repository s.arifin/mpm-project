import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SuspectPersonComponent } from './suspect-person.component';
import { SuspectPersonSegmentationComponent } from './suspect-person-segmentation.component';
import { SuspectPersonROComponent } from './suspect-person-ro.component';
import { SuspectPersonOtherComponent } from './suspect-person-other.component';
import { SuspectPersonUploadComponent } from './suspect-person-upload.component';

@Injectable()
export class SuspectPersonResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idSuspect,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const suspectPersonRoute: Routes = [
    {
        path: 'suspect-person',
        component: SuspectPersonComponent,
        resolve: {
            'pagingParams': SuspectPersonResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'suspect-person-segmentation',
        component: SuspectPersonSegmentationComponent,
        resolve: {
            'pagingParams': SuspectPersonResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'suspect-person-ro',
        component: SuspectPersonROComponent,
        resolve: {
            'pagingParams': SuspectPersonResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'suspect-person-other',
        component: SuspectPersonOtherComponent,
        resolve: {
            'pagingParams': SuspectPersonResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const suspectPersonPopupRoute: Routes = [
    {
        path: 'suspect-person-upload',
        component: SuspectPersonUploadComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
