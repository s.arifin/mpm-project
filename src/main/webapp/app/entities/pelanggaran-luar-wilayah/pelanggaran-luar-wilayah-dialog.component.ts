import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PelanggaranLuarWilayah } from './pelanggaran-luar-wilayah.model';
import { PelanggaranLuarWilayahPopupService } from './pelanggaran-luar-wilayah-popup.service';
import { PelanggaranLuarWilayahService } from './pelanggaran-luar-wilayah.service';

@Component({
    selector: 'jhi-pelanggaran-luar-wilayah-dialog',
    templateUrl: './pelanggaran-luar-wilayah-dialog.component.html'
})
export class PelanggaranLuarWilayahDialogComponent implements OnInit {

    pelanggaranLuarWilayah: PelanggaranLuarWilayah;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private pelanggaranLuarWilayahService: PelanggaranLuarWilayahService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.pelanggaranLuarWilayah.id !== undefined) {
            this.subscribeToSaveResponse(
                this.pelanggaranLuarWilayahService.update(this.pelanggaranLuarWilayah));
        } else {
            this.subscribeToSaveResponse(
                this.pelanggaranLuarWilayahService.create(this.pelanggaranLuarWilayah));
        }
    }

    private subscribeToSaveResponse(result: Observable<PelanggaranLuarWilayah>) {
        result.subscribe((res: PelanggaranLuarWilayah) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PelanggaranLuarWilayah) {
        this.eventManager.broadcast({ name: 'pelanggaranLuarWilayahListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-pelanggaran-luar-wilayah-popup',
    template: ''
})
export class PelanggaranLuarWilayahPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pelanggaranLuarWilayahPopupService: PelanggaranLuarWilayahPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.pelanggaranLuarWilayahPopupService
                    .open(PelanggaranLuarWilayahDialogComponent as Component, params['id']);
            } else {
                this.pelanggaranLuarWilayahPopupService
                    .open(PelanggaranLuarWilayahDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
