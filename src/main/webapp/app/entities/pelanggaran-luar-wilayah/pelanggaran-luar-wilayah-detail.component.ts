import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PelanggaranLuarWilayah } from './pelanggaran-luar-wilayah.model';
import { PelanggaranLuarWilayahService } from './pelanggaran-luar-wilayah.service';

@Component({
    selector: 'jhi-pelanggaran-luar-wilayah-detail',
    templateUrl: './pelanggaran-luar-wilayah-detail.component.html'
})
export class PelanggaranLuarWilayahDetailComponent implements OnInit, OnDestroy {

    pelanggaranLuarWilayah: PelanggaranLuarWilayah;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private pelanggaranLuarWilayahService: PelanggaranLuarWilayahService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPelanggaranLuarWilayahs();
    }

    load(id) {
        this.pelanggaranLuarWilayahService.find(id).subscribe((pelanggaranLuarWilayah) => {
            this.pelanggaranLuarWilayah = pelanggaranLuarWilayah;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPelanggaranLuarWilayahs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'pelanggaranLuarWilayahListModification',
            (response) => this.load(this.pelanggaranLuarWilayah.id)
        );
    }
}
