export * from './pelanggaran-luar-wilayah.model';
export * from './pelanggaran-luar-wilayah-popup.service';
export * from './pelanggaran-luar-wilayah.service';
export * from './pelanggaran-luar-wilayah-dialog.component';
export * from './pelanggaran-luar-wilayah-delete-dialog.component';
export * from './pelanggaran-luar-wilayah-detail.component';
export * from './pelanggaran-luar-wilayah.component';
export * from './pelanggaran-luar-wilayah.route';
