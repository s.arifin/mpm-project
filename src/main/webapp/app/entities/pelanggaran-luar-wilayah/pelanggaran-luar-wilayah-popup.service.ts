import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PelanggaranLuarWilayah } from './pelanggaran-luar-wilayah.model';
import { PelanggaranLuarWilayahService } from './pelanggaran-luar-wilayah.service';

@Injectable()
export class PelanggaranLuarWilayahPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private pelanggaranLuarWilayahService: PelanggaranLuarWilayahService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.pelanggaranLuarWilayahService.find(id).subscribe((pelanggaranLuarWilayah) => {
                    this.ngbModalRef = this.pelanggaranLuarWilayahModalRef(component, pelanggaranLuarWilayah);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.pelanggaranLuarWilayahModalRef(component, new PelanggaranLuarWilayah());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    pelanggaranLuarWilayahModalRef(component: Component, pelanggaranLuarWilayah: PelanggaranLuarWilayah): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.pelanggaranLuarWilayah = pelanggaranLuarWilayah;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
