import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PelanggaranLuarWilayahComponent } from './pelanggaran-luar-wilayah.component';
import { PelanggaranLuarWilayahDetailComponent } from './pelanggaran-luar-wilayah-detail.component';
import { PelanggaranLuarWilayahPopupComponent } from './pelanggaran-luar-wilayah-dialog.component';
import { PelanggaranLuarWilayahDeletePopupComponent } from './pelanggaran-luar-wilayah-delete-dialog.component';

@Injectable()
export class PelanggaranLuarWilayahResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const pelanggaranLuarWilayahRoute: Routes = [
    {
        path: 'pelanggaran-luar-wilayah',
        component: PelanggaranLuarWilayahComponent,
        resolve: {
            'pagingParams': PelanggaranLuarWilayahResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.pelanggaranLuarWilayah.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'pelanggaran-luar-wilayah/:id',
        component: PelanggaranLuarWilayahDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.pelanggaranLuarWilayah.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pelanggaranLuarWilayahPopupRoute: Routes = [
    {
        path: 'pelanggaran-luar-wilayah-new',
        component: PelanggaranLuarWilayahPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.pelanggaranLuarWilayah.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'pelanggaran-luar-wilayah/:id/edit',
        component: PelanggaranLuarWilayahPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.pelanggaranLuarWilayah.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'pelanggaran-luar-wilayah/:id/delete',
        component: PelanggaranLuarWilayahDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.pelanggaranLuarWilayah.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
