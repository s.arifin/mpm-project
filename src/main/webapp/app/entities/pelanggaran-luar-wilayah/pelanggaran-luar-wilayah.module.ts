import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    PelanggaranLuarWilayahService,
    PelanggaranLuarWilayahPopupService,
    PelanggaranLuarWilayahComponent,
    PelanggaranLuarWilayahDetailComponent,
    PelanggaranLuarWilayahDialogComponent,
    PelanggaranLuarWilayahPopupComponent,
    PelanggaranLuarWilayahDeletePopupComponent,
    PelanggaranLuarWilayahDeleteDialogComponent,
    pelanggaranLuarWilayahRoute,
    pelanggaranLuarWilayahPopupRoute,
    PelanggaranLuarWilayahResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...pelanggaranLuarWilayahRoute,
    ...pelanggaranLuarWilayahPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PelanggaranLuarWilayahComponent,
        PelanggaranLuarWilayahDetailComponent,
        PelanggaranLuarWilayahDialogComponent,
        PelanggaranLuarWilayahDeleteDialogComponent,
        PelanggaranLuarWilayahPopupComponent,
        PelanggaranLuarWilayahDeletePopupComponent,
    ],
    entryComponents: [
        PelanggaranLuarWilayahComponent,
        PelanggaranLuarWilayahDialogComponent,
        PelanggaranLuarWilayahPopupComponent,
        PelanggaranLuarWilayahDeleteDialogComponent,
        PelanggaranLuarWilayahDeletePopupComponent,
    ],
    providers: [
        PelanggaranLuarWilayahService,
        PelanggaranLuarWilayahPopupService,
        PelanggaranLuarWilayahResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPelanggaranLuarWilayahModule {}
