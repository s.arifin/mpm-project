import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PelanggaranLuarWilayah } from './pelanggaran-luar-wilayah.model';
import { PelanggaranLuarWilayahPopupService } from './pelanggaran-luar-wilayah-popup.service';
import { PelanggaranLuarWilayahService } from './pelanggaran-luar-wilayah.service';

@Component({
    selector: 'jhi-pelanggaran-luar-wilayah-delete-dialog',
    templateUrl: './pelanggaran-luar-wilayah-delete-dialog.component.html'
})
export class PelanggaranLuarWilayahDeleteDialogComponent {

    pelanggaranLuarWilayah: PelanggaranLuarWilayah;

    constructor(
        private pelanggaranLuarWilayahService: PelanggaranLuarWilayahService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.pelanggaranLuarWilayahService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'pelanggaranLuarWilayahListModification',
                content: 'Deleted an pelanggaranLuarWilayah'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-pelanggaran-luar-wilayah-delete-popup',
    template: ''
})
export class PelanggaranLuarWilayahDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pelanggaranLuarWilayahPopupService: PelanggaranLuarWilayahPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.pelanggaranLuarWilayahPopupService
                .open(PelanggaranLuarWilayahDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
