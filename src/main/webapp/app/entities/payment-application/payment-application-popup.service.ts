import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PaymentApplication } from './payment-application.model';
import { PaymentApplicationService } from './payment-application.service';

@Injectable()
export class PaymentApplicationPopupService {
    protected ngbModalRef: NgbModalRef;
    idPayment: any;
    idBilling: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected paymentApplicationService: PaymentApplicationService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.paymentApplicationService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.paymentApplicationModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new PaymentApplication();
                    data.paymentId = this.idPayment;
                    data.billingId = this.idBilling;
                    this.ngbModalRef = this.paymentApplicationModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    paymentApplicationModalRef(component: Component, paymentApplication: PaymentApplication): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.paymentApplication = paymentApplication;
        modalRef.componentInstance.idPayment = this.idPayment;
        modalRef.componentInstance.idBilling = this.idBilling;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.paymentApplicationLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    paymentApplicationLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idPayment = this.idPayment;
        modalRef.componentInstance.idBilling = this.idBilling;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
