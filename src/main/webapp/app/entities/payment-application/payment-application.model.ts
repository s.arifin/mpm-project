import { BaseEntity } from './../../shared';

export class PaymentApplication implements BaseEntity {
    constructor(
        public id?: any,
        public idPaymentApplication?: any,
        public amountApplied?: number,
        public paymentId?: any,
        public billingId?: any,
    ) {
    }
}
