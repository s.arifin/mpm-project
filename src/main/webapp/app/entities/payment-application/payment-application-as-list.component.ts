import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { PaymentApplication } from './payment-application.model';
import { PaymentApplicationService } from './payment-application.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-payment-application-as-list',
    templateUrl: './payment-application-as-list.component.html'
})
export class PaymentApplicationAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idPayment: any;
    @Input() idBilling: any;

    currentAccount: any;
    paymentApplications: PaymentApplication[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        protected paymentApplicationService: PaymentApplicationService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'idPaymentApplication';
        this.reverse = 'asc';
        this.first = 0;
    }

    loadAll() {
        this.paymentApplicationService.queryFilterBy({
            idPayment: this.idPayment,
            idBilling: this.idBilling,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/payment-application', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPaymentApplications();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idPayment']) {
            this.loadAll();
        }
        if (changes['idBilling']) {
            this.loadAll();
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PaymentApplication) {
        return item.idPaymentApplication;
    }

    registerChangeInPaymentApplications() {
        this.eventSubscriber = this.eventManager.subscribe('paymentApplicationListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idPaymentApplication') {
            result.push('idPaymentApplication');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.paymentApplications = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.paymentApplicationService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.subscribeToSaveResponse(
                this.paymentApplicationService.update(event.data));
        } else {
            this.subscribeToSaveResponse(
                this.paymentApplicationService.create(event.data));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PaymentApplication>) {
        result.subscribe((res: PaymentApplication) => this.onSaveSuccess(res));
    }

    protected onSaveSuccess(result: PaymentApplication) {
        this.eventManager.broadcast({ name: 'paymentApplicationListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'paymentApplication saved !');
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.paymentApplicationService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'paymentApplicationListModification',
                        content: 'Deleted an paymentApplication'
                    });
                });
            }
        });
    }
}
