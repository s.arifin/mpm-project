import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { PaymentApplication } from './payment-application.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PaymentApplicationService {
   protected itemValues: PaymentApplication[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/payment-applications';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/payment-applications';

   constructor(protected http: Http) { }

   create(paymentApplication: PaymentApplication): Observable<PaymentApplication> {
       const copy = this.convert(paymentApplication);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(paymentApplication: PaymentApplication): Observable<PaymentApplication> {
       const copy = this.convert(paymentApplication);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<PaymentApplication> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: PaymentApplication, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: PaymentApplication[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to PaymentApplication.
    */
   protected convertItemFromServer(json: any): PaymentApplication {
       const entity: PaymentApplication = Object.assign(new PaymentApplication(), json);
       return entity;
   }

   /**
    * Convert a PaymentApplication to a JSON which can be sent to the server.
    */
   protected convert(paymentApplication: PaymentApplication): PaymentApplication {
       if (paymentApplication === null || paymentApplication === {}) {
           return {};
       }
       // const copy: PaymentApplication = Object.assign({}, paymentApplication);
       const copy: PaymentApplication = JSON.parse(JSON.stringify(paymentApplication));
       return copy;
   }

   protected convertList(paymentApplications: PaymentApplication[]): PaymentApplication[] {
       const copy: PaymentApplication[] = paymentApplications;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: PaymentApplication[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
