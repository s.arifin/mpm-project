import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PaymentApplication } from './payment-application.model';
import { PaymentApplicationPopupService } from './payment-application-popup.service';
import { PaymentApplicationService } from './payment-application.service';
import { ToasterService } from '../../shared';
import { Payment, PaymentService } from '../payment';
import { Billing, BillingService } from '../billing';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-payment-application-dialog',
    templateUrl: './payment-application-dialog.component.html'
})
export class PaymentApplicationDialogComponent implements OnInit {

    paymentApplication: PaymentApplication;
    isSaving: boolean;
    idPayment: any;
    idBilling: any;

    payments: Payment[];

    billings: Billing[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected paymentApplicationService: PaymentApplicationService,
        protected paymentService: PaymentService,
        protected billingService: BillingService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.paymentService.query()
            .subscribe((res: ResponseWrapper) => { this.payments = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billingService.query()
            .subscribe((res: ResponseWrapper) => { this.billings = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.paymentApplication.idPaymentApplication !== undefined) {
            this.subscribeToSaveResponse(
                this.paymentApplicationService.update(this.paymentApplication));
        } else {
            this.subscribeToSaveResponse(
                this.paymentApplicationService.create(this.paymentApplication));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PaymentApplication>) {
        result.subscribe((res: PaymentApplication) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: PaymentApplication) {
        this.eventManager.broadcast({ name: 'paymentApplicationListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'paymentApplication saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'paymentApplication Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPaymentById(index: number, item: Payment) {
        return item.idPayment;
    }

    trackBillingById(index: number, item: Billing) {
        return item.idBilling;
    }
}

@Component({
    selector: 'jhi-payment-application-popup',
    template: ''
})
export class PaymentApplicationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected paymentApplicationPopupService: PaymentApplicationPopupService
    ) {}

    ngOnInit() {
        this.paymentApplicationPopupService.idPayment = undefined;
        this.paymentApplicationPopupService.idBilling = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.paymentApplicationPopupService
                    .open(PaymentApplicationDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.paymentApplicationPopupService.parent = params['parent'];
                this.paymentApplicationPopupService
                    .open(PaymentApplicationDialogComponent as Component);
            } else {
                this.paymentApplicationPopupService
                    .open(PaymentApplicationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
