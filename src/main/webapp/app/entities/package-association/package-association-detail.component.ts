import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PackageAssociation } from './package-association.model';
import { PackageAssociationService } from './package-association.service';

@Component({
    selector: 'jhi-package-association-detail',
    templateUrl: './package-association-detail.component.html'
})
export class PackageAssociationDetailComponent implements OnInit, OnDestroy {

    packageAssociation: PackageAssociation;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private packageAssociationService: PackageAssociationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPackageAssociations();
    }

    load(id) {
        this.packageAssociationService.find(id).subscribe((packageAssociation) => {
            this.packageAssociation = packageAssociation;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPackageAssociations() {
        this.eventSubscriber = this.eventManager.subscribe(
            'packageAssociationListModification',
            (response) => this.load(this.packageAssociation.id)
        );
    }
}
