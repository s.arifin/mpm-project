import { BaseEntity } from './../../shared';

export class PackageAssociation implements BaseEntity {
    constructor(
        public id?: number,
    ) {
    }
}
