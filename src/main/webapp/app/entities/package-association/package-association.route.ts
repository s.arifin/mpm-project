import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PackageAssociationComponent } from './package-association.component';
import { PackageAssociationDetailComponent } from './package-association-detail.component';
import { PackageAssociationPopupComponent } from './package-association-dialog.component';
import { PackageAssociationDeletePopupComponent } from './package-association-delete-dialog.component';

@Injectable()
export class PackageAssociationResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const packageAssociationRoute: Routes = [
    {
        path: 'package-association',
        component: PackageAssociationComponent,
        resolve: {
            'pagingParams': PackageAssociationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.packageAssociation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'package-association/:id',
        component: PackageAssociationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.packageAssociation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const packageAssociationPopupRoute: Routes = [
    {
        path: 'package-association-new',
        component: PackageAssociationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.packageAssociation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'package-association/:id/edit',
        component: PackageAssociationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.packageAssociation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'package-association/:id/delete',
        component: PackageAssociationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.packageAssociation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
