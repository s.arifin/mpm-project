import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PackageAssociation } from './package-association.model';
import { PackageAssociationPopupService } from './package-association-popup.service';
import { PackageAssociationService } from './package-association.service';

@Component({
    selector: 'jhi-package-association-delete-dialog',
    templateUrl: './package-association-delete-dialog.component.html'
})
export class PackageAssociationDeleteDialogComponent {

    packageAssociation: PackageAssociation;

    constructor(
        private packageAssociationService: PackageAssociationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.packageAssociationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'packageAssociationListModification',
                content: 'Deleted an packageAssociation'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-package-association-delete-popup',
    template: ''
})
export class PackageAssociationDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private packageAssociationPopupService: PackageAssociationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.packageAssociationPopupService
                .open(PackageAssociationDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
