import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PackageAssociation } from './package-association.model';
import { PackageAssociationPopupService } from './package-association-popup.service';
import { PackageAssociationService } from './package-association.service';

@Component({
    selector: 'jhi-package-association-dialog',
    templateUrl: './package-association-dialog.component.html'
})
export class PackageAssociationDialogComponent implements OnInit {

    packageAssociation: PackageAssociation;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private packageAssociationService: PackageAssociationService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.packageAssociation.id !== undefined) {
            this.subscribeToSaveResponse(
                this.packageAssociationService.update(this.packageAssociation));
        } else {
            this.subscribeToSaveResponse(
                this.packageAssociationService.create(this.packageAssociation));
        }
    }

    private subscribeToSaveResponse(result: Observable<PackageAssociation>) {
        result.subscribe((res: PackageAssociation) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PackageAssociation) {
        this.eventManager.broadcast({ name: 'packageAssociationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-package-association-popup',
    template: ''
})
export class PackageAssociationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private packageAssociationPopupService: PackageAssociationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.packageAssociationPopupService
                    .open(PackageAssociationDialogComponent as Component, params['id']);
            } else {
                this.packageAssociationPopupService
                    .open(PackageAssociationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
