import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    PackageAssociationService,
    PackageAssociationPopupService,
    PackageAssociationComponent,
    PackageAssociationDetailComponent,
    PackageAssociationDialogComponent,
    PackageAssociationPopupComponent,
    PackageAssociationDeletePopupComponent,
    PackageAssociationDeleteDialogComponent,
    packageAssociationRoute,
    packageAssociationPopupRoute,
    PackageAssociationResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...packageAssociationRoute,
    ...packageAssociationPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PackageAssociationComponent,
        PackageAssociationDetailComponent,
        PackageAssociationDialogComponent,
        PackageAssociationDeleteDialogComponent,
        PackageAssociationPopupComponent,
        PackageAssociationDeletePopupComponent,
    ],
    entryComponents: [
        PackageAssociationComponent,
        PackageAssociationDialogComponent,
        PackageAssociationPopupComponent,
        PackageAssociationDeleteDialogComponent,
        PackageAssociationDeletePopupComponent,
    ],
    providers: [
        PackageAssociationService,
        PackageAssociationPopupService,
        PackageAssociationResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPackageAssociationModule {}
