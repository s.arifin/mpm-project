import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { GoodContainer } from './good-container.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class GoodContainerService {
   protected itemValues: GoodContainer[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/good-containers';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/good-containers';

   constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

   create(goodContainer: GoodContainer): Observable<GoodContainer> {
       const copy = this.convert(goodContainer);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(goodContainer: GoodContainer): Observable<GoodContainer> {
       const copy = this.convert(goodContainer);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<GoodContainer> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: GoodContainer, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: GoodContainer[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to GoodContainer.
    */
   protected convertItemFromServer(json: any): GoodContainer {
       const entity: GoodContainer = Object.assign(new GoodContainer(), json);
       if (entity.dateFrom) {
           entity.dateFrom = new Date(entity.dateFrom);
       }
       if (entity.dateThru) {
           entity.dateThru = new Date(entity.dateThru);
       }
       return entity;
   }

   /**
    * Convert a GoodContainer to a JSON which can be sent to the server.
    */
   protected convert(goodContainer: GoodContainer): GoodContainer {
       if (goodContainer === null || goodContainer === {}) {
           return {};
       }
       // const copy: GoodContainer = Object.assign({}, goodContainer);
       const copy: GoodContainer = JSON.parse(JSON.stringify(goodContainer));

       // copy.dateFrom = this.dateUtils.toDate(goodContainer.dateFrom);

       // copy.dateThru = this.dateUtils.toDate(goodContainer.dateThru);
       return copy;
   }

   protected convertList(goodContainers: GoodContainer[]): GoodContainer[] {
       const copy: GoodContainer[] = goodContainers;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: GoodContainer[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
