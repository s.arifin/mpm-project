import { BaseEntity } from './../../shared';

export class GoodContainer implements BaseEntity {
    constructor(
        public id?: any,
        public idGoodContainer?: any,
        public dateFrom?: any,
        public dateThru?: any,
        public goodId?: any,
        public containerId?: any,
        public organizationId?: any,
    ) {
    }
}
