import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { GoodContainer } from './good-container.model';
import { GoodContainerService } from './good-container.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-good-container-as-list',
    templateUrl: './good-container-as-list.component.html'
})
export class GoodContainerAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idGood: any;
    @Input() idContainer: any;
    @Input() idOrganization: any;

    currentAccount: any;
    goodContainers: GoodContainer[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        protected goodContainerService: GoodContainerService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'idGoodContainer';
        this.reverse = 'asc';
        this.first = 0;
    }

    loadAll() {
        this.goodContainerService.queryFilterBy({
            idGood: this.idGood,
            idContainer: this.idContainer,
            idOrganization: this.idOrganization,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/good-container', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInGoodContainers();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idGood']) {
            this.loadAll();
        }
        if (changes['idContainer']) {
            this.loadAll();
        }
        if (changes['idOrganization']) {
            this.loadAll();
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: GoodContainer) {
        return item.idGoodContainer;
    }

    registerChangeInGoodContainers() {
        this.eventSubscriber = this.eventManager.subscribe('goodContainerListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idGoodContainer') {
            result.push('idGoodContainer');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.goodContainers = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.goodContainerService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.subscribeToSaveResponse(
                this.goodContainerService.update(event.data));
        } else {
            this.subscribeToSaveResponse(
                this.goodContainerService.create(event.data));
        }
    }

    protected subscribeToSaveResponse(result: Observable<GoodContainer>) {
        result.subscribe((res: GoodContainer) => this.onSaveSuccess(res));
    }

    protected onSaveSuccess(result: GoodContainer) {
        this.eventManager.broadcast({ name: 'goodContainerListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'goodContainer saved !');
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.goodContainerService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'goodContainerListModification',
                        content: 'Deleted an goodContainer'
                    });
                });
            }
        });
    }
}
