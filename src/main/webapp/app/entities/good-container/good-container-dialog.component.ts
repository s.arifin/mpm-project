import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { GoodContainer } from './good-container.model';
import { GoodContainerPopupService } from './good-container-popup.service';
import { GoodContainerService } from './good-container.service';
import { ToasterService } from '../../shared';
import { Good, GoodService } from '../good';
import { Container, ContainerService } from '../container';
import { Organization, OrganizationService } from '../organization';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-good-container-dialog',
    templateUrl: './good-container-dialog.component.html'
})
export class GoodContainerDialogComponent implements OnInit {

    goodContainer: GoodContainer;
    isSaving: boolean;
    idGood: any;
    idContainer: any;
    idOrganization: any;

    goods: Good[];

    containers: Container[];

    organizations: Organization[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected goodContainerService: GoodContainerService,
        protected goodService: GoodService,
        protected containerService: ContainerService,
        protected organizationService: OrganizationService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.goodService.query()
            .subscribe((res: ResponseWrapper) => { this.goods = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.containerService.query()
            .subscribe((res: ResponseWrapper) => { this.containers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.organizationService.query()
            .subscribe((res: ResponseWrapper) => { this.organizations = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.goodContainer.idGoodContainer !== undefined) {
            this.subscribeToSaveResponse(
                this.goodContainerService.update(this.goodContainer));
        } else {
            this.subscribeToSaveResponse(
                this.goodContainerService.create(this.goodContainer));
        }
    }

    protected subscribeToSaveResponse(result: Observable<GoodContainer>) {
        result.subscribe((res: GoodContainer) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: GoodContainer) {
        this.eventManager.broadcast({ name: 'goodContainerListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'goodContainer saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'goodContainer Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackGoodById(index: number, item: Good) {
        return item.idProduct;
    }

    trackContainerById(index: number, item: Container) {
        return item.idContainer;
    }

    trackOrganizationById(index: number, item: Organization) {
        return item.idParty;
    }
}

@Component({
    selector: 'jhi-good-container-popup',
    template: ''
})
export class GoodContainerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected goodContainerPopupService: GoodContainerPopupService
    ) {}

    ngOnInit() {
        this.goodContainerPopupService.idGood = undefined;
        this.goodContainerPopupService.idContainer = undefined;
        this.goodContainerPopupService.idOrganization = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.goodContainerPopupService
                    .open(GoodContainerDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.goodContainerPopupService.parent = params['parent'];
                this.goodContainerPopupService
                    .open(GoodContainerDialogComponent as Component);
            } else {
                this.goodContainerPopupService
                    .open(GoodContainerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
