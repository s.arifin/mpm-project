import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { GoodContainer } from './good-container.model';
import { GoodContainerService } from './good-container.service';

@Injectable()
export class GoodContainerPopupService {
    protected ngbModalRef: NgbModalRef;
    idGood: any;
    idContainer: any;
    idOrganization: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected goodContainerService: GoodContainerService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.goodContainerService.find(id).subscribe((data) => {
                    // if (data.dateFrom) {
                    //    data.dateFrom = this.datePipe
                    //        .transform(data.dateFrom, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    // if (data.dateThru) {
                    //    data.dateThru = this.datePipe
                    //        .transform(data.dateThru, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.goodContainerModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new GoodContainer();
                    data.goodId = this.idGood;
                    data.containerId = this.idContainer;
                    data.organizationId = this.idOrganization;
                    this.ngbModalRef = this.goodContainerModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    goodContainerModalRef(component: Component, goodContainer: GoodContainer): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.goodContainer = goodContainer;
        modalRef.componentInstance.idGood = this.idGood;
        modalRef.componentInstance.idContainer = this.idContainer;
        modalRef.componentInstance.idOrganization = this.idOrganization;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.goodContainerLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    goodContainerLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idGood = this.idGood;
        modalRef.componentInstance.idContainer = this.idContainer;
        modalRef.componentInstance.idOrganization = this.idOrganization;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
