import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ReturnPurchaseOrder } from './return-purchase-order.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ReturnPurchaseOrderService {
    protected itemValues: ReturnPurchaseOrder[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/return-purchase-orders';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/return-purchase-orders';

    constructor(protected http: Http) { }

    create(returnPurchaseOrder: ReturnPurchaseOrder): Observable<ReturnPurchaseOrder> {
        const copy = this.convert(returnPurchaseOrder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(returnPurchaseOrder: ReturnPurchaseOrder): Observable<ReturnPurchaseOrder> {
        const copy = this.convert(returnPurchaseOrder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<ReturnPurchaseOrder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(returnPurchaseOrder: ReturnPurchaseOrder, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(returnPurchaseOrder);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<ReturnPurchaseOrder> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<ReturnPurchaseOrder[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ReturnPurchaseOrder.
     */
    protected convertItemFromServer(json: any): ReturnPurchaseOrder {
        const entity: ReturnPurchaseOrder = Object.assign(new ReturnPurchaseOrder(), json);
        return entity;
    }

    /**
     * Convert a ReturnPurchaseOrder to a JSON which can be sent to the server.
     */
    protected convert(returnPurchaseOrder: ReturnPurchaseOrder): ReturnPurchaseOrder {
        if (returnPurchaseOrder === null || returnPurchaseOrder === {}) {
            return {};
        }
        // const copy: ReturnPurchaseOrder = Object.assign({}, returnPurchaseOrder);
        const copy: ReturnPurchaseOrder = JSON.parse(JSON.stringify(returnPurchaseOrder));
        return copy;
    }

    protected convertList(returnPurchaseOrders: ReturnPurchaseOrder[]): ReturnPurchaseOrder[] {
        const copy: ReturnPurchaseOrder[] = returnPurchaseOrders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ReturnPurchaseOrder[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
