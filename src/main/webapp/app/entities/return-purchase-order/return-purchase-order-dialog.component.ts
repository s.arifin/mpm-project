import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ReturnPurchaseOrder } from './return-purchase-order.model';
import { ReturnPurchaseOrderPopupService } from './return-purchase-order-popup.service';
import { ReturnPurchaseOrderService } from './return-purchase-order.service';
import { ToasterService } from '../../shared';
import { Vendor, VendorService } from '../vendor';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-return-purchase-order-dialog',
    templateUrl: './return-purchase-order-dialog.component.html'
})
export class ReturnPurchaseOrderDialogComponent implements OnInit {

    returnPurchaseOrder: ReturnPurchaseOrder;
    isSaving: boolean;
    idVendor: any;
    idInternal: any;
    idBillTo: any;

    vendors: Vendor[];

    internals: Internal[];

    billtos: BillTo[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected returnPurchaseOrderService: ReturnPurchaseOrderService,
        protected vendorService: VendorService,
        protected internalService: InternalService,
        protected billToService: BillToService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.vendorService.query()
            .subscribe((res: ResponseWrapper) => { this.vendors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.returnPurchaseOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.returnPurchaseOrderService.update(this.returnPurchaseOrder));
        } else {
            this.subscribeToSaveResponse(
                this.returnPurchaseOrderService.create(this.returnPurchaseOrder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ReturnPurchaseOrder>) {
        result.subscribe((res: ReturnPurchaseOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ReturnPurchaseOrder) {
        this.eventManager.broadcast({ name: 'returnPurchaseOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'returnPurchaseOrder saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'returnPurchaseOrder Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackVendorById(index: number, item: Vendor) {
        return item.idVendor;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}

@Component({
    selector: 'jhi-return-purchase-order-popup',
    template: ''
})
export class ReturnPurchaseOrderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected returnPurchaseOrderPopupService: ReturnPurchaseOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.returnPurchaseOrderPopupService
                    .open(ReturnPurchaseOrderDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.returnPurchaseOrderPopupService.parent = params['parent'];
                this.returnPurchaseOrderPopupService
                    .open(ReturnPurchaseOrderDialogComponent as Component);
            } else {
                this.returnPurchaseOrderPopupService
                    .open(ReturnPurchaseOrderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
