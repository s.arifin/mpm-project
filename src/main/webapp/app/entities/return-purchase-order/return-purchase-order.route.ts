import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ReturnPurchaseOrderComponent } from './return-purchase-order.component';
import { ReturnPurchaseOrderEditComponent } from './return-purchase-order-edit.component';
import { ReturnPurchaseOrderPopupComponent } from './return-purchase-order-dialog.component';

@Injectable()
export class ReturnPurchaseOrderResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idOrder,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const returnPurchaseOrderRoute: Routes = [
    {
        path: 'return-purchase-order',
        component: ReturnPurchaseOrderComponent,
        resolve: {
            'pagingParams': ReturnPurchaseOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returnPurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const returnPurchaseOrderPopupRoute: Routes = [
    {
        path: 'return-purchase-order-popup-new',
        component: ReturnPurchaseOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returnPurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'return-purchase-order-new',
        component: ReturnPurchaseOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returnPurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'return-purchase-order/:id/edit',
        component: ReturnPurchaseOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returnPurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'return-purchase-order/:route/:page/:id/edit',
        component: ReturnPurchaseOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returnPurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'return-purchase-order/:id/popup-edit',
        component: ReturnPurchaseOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returnPurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
