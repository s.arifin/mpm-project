import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { MasterMaterialPromo } from './master-material-promo.model';
import { MasterMaterialPromoService } from './master-material-promo.service';

@Component({
    selector: 'jhi-master-material-promo-detail',
    templateUrl: './master-material-promo-detail.component.html'
})
export class MasterMaterialPromoDetailComponent implements OnInit, OnDestroy {

    masterMaterialPromo: MasterMaterialPromo;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private masterMaterialPromoService: MasterMaterialPromoService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMasterMaterialPromos();
    }

    load(id) {
        this.masterMaterialPromoService.find(id).subscribe((masterMaterialPromo) => {
            this.masterMaterialPromo = masterMaterialPromo;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMasterMaterialPromos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'masterMaterialPromoListModification',
            (response) => this.load(this.masterMaterialPromo.id)
        );
    }
}
