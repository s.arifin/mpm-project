import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MasterMaterialPromoComponent } from './master-material-promo.component';
import { MasterMaterialPromoDetailComponent } from './master-material-promo-detail.component';
import { MasterMaterialPromoPopupComponent } from './master-material-promo-dialog.component';
import { MasterMaterialPromoDeletePopupComponent } from './master-material-promo-delete-dialog.component';

@Injectable()
export class MasterMaterialPromoResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const masterMaterialPromoRoute: Routes = [
    {
        path: 'master-material-promo',
        component: MasterMaterialPromoComponent,
        resolve: {
            'pagingParams': MasterMaterialPromoResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterMaterialPromo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'master-material-promo/:id',
        component: MasterMaterialPromoDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterMaterialPromo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const masterMaterialPromoPopupRoute: Routes = [
    {
        path: 'master-material-promo-new',
        component: MasterMaterialPromoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterMaterialPromo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'master-material-promo/:id/edit',
        component: MasterMaterialPromoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterMaterialPromo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'master-material-promo/:id/delete',
        component: MasterMaterialPromoDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterMaterialPromo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
