import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MasterMaterialPromo } from './master-material-promo.model';
import { MasterMaterialPromoPopupService } from './master-material-promo-popup.service';
import { MasterMaterialPromoService } from './master-material-promo.service';

@Component({
    selector: 'jhi-master-material-promo-delete-dialog',
    templateUrl: './master-material-promo-delete-dialog.component.html'
})
export class MasterMaterialPromoDeleteDialogComponent {

    masterMaterialPromo: MasterMaterialPromo;

    constructor(
        private masterMaterialPromoService: MasterMaterialPromoService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.masterMaterialPromoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'masterMaterialPromoListModification',
                content: 'Deleted an masterMaterialPromo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-master-material-promo-delete-popup',
    template: ''
})
export class MasterMaterialPromoDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private masterMaterialPromoPopupService: MasterMaterialPromoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.masterMaterialPromoPopupService
                .open(MasterMaterialPromoDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
