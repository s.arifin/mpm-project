import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    MasterMaterialPromoService,
    MasterMaterialPromoPopupService,
    MasterMaterialPromoComponent,
    MasterMaterialPromoDetailComponent,
    MasterMaterialPromoDialogComponent,
    MasterMaterialPromoPopupComponent,
    MasterMaterialPromoDeletePopupComponent,
    MasterMaterialPromoDeleteDialogComponent,
    masterMaterialPromoRoute,
    masterMaterialPromoPopupRoute,
    MasterMaterialPromoResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...masterMaterialPromoRoute,
    ...masterMaterialPromoPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        MasterMaterialPromoComponent,
        MasterMaterialPromoDetailComponent,
        MasterMaterialPromoDialogComponent,
        MasterMaterialPromoDeleteDialogComponent,
        MasterMaterialPromoPopupComponent,
        MasterMaterialPromoDeletePopupComponent,
    ],
    entryComponents: [
        MasterMaterialPromoComponent,
        MasterMaterialPromoDialogComponent,
        MasterMaterialPromoPopupComponent,
        MasterMaterialPromoDeleteDialogComponent,
        MasterMaterialPromoDeletePopupComponent,
    ],
    providers: [
        MasterMaterialPromoService,
        MasterMaterialPromoPopupService,
        MasterMaterialPromoResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMasterMaterialPromoModule {}
