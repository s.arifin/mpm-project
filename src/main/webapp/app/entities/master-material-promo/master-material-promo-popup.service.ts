import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MasterMaterialPromo } from './master-material-promo.model';
import { MasterMaterialPromoService } from './master-material-promo.service';

@Injectable()
export class MasterMaterialPromoPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private masterMaterialPromoService: MasterMaterialPromoService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.masterMaterialPromoService.find(id).subscribe((masterMaterialPromo) => {
                    this.ngbModalRef = this.masterMaterialPromoModalRef(component, masterMaterialPromo);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.masterMaterialPromoModalRef(component, new MasterMaterialPromo());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    masterMaterialPromoModalRef(component: Component, masterMaterialPromo: MasterMaterialPromo): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.masterMaterialPromo = masterMaterialPromo;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
