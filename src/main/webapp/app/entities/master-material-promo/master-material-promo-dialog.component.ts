import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { MasterMaterialPromo } from './master-material-promo.model';
import { MasterMaterialPromoPopupService } from './master-material-promo-popup.service';
import { MasterMaterialPromoService } from './master-material-promo.service';

@Component({
    selector: 'jhi-master-material-promo-dialog',
    templateUrl: './master-material-promo-dialog.component.html'
})
export class MasterMaterialPromoDialogComponent implements OnInit {

    masterMaterialPromo: MasterMaterialPromo;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private masterMaterialPromoService: MasterMaterialPromoService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.masterMaterialPromo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.masterMaterialPromoService.update(this.masterMaterialPromo));
        } else {
            this.subscribeToSaveResponse(
                this.masterMaterialPromoService.create(this.masterMaterialPromo));
        }
    }

    private subscribeToSaveResponse(result: Observable<MasterMaterialPromo>) {
        result.subscribe((res: MasterMaterialPromo) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: MasterMaterialPromo) {
        this.eventManager.broadcast({ name: 'masterMaterialPromoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-master-material-promo-popup',
    template: ''
})
export class MasterMaterialPromoPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private masterMaterialPromoPopupService: MasterMaterialPromoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.masterMaterialPromoPopupService
                    .open(MasterMaterialPromoDialogComponent as Component, params['id']);
            } else {
                this.masterMaterialPromoPopupService
                    .open(MasterMaterialPromoDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
