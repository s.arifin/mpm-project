export * from './master-material-promo.model';
export * from './master-material-promo-popup.service';
export * from './master-material-promo.service';
export * from './master-material-promo-dialog.component';
export * from './master-material-promo-delete-dialog.component';
export * from './master-material-promo-detail.component';
export * from './master-material-promo.component';
export * from './master-material-promo.route';
