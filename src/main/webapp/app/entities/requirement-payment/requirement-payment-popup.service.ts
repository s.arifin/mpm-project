import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RequirementPayment } from './requirement-payment.model';
import { RequirementPaymentService } from './requirement-payment.service';

@Injectable()
export class RequirementPaymentPopupService {
    protected ngbModalRef: NgbModalRef;
    idRequirement: any;
    idPayment: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected requirementPaymentService: RequirementPaymentService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.requirementPaymentService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.requirementPaymentModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new RequirementPayment();
                    data.requirementId = this.idRequirement;
                    data.paymentId = this.idPayment;
                    this.ngbModalRef = this.requirementPaymentModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    requirementPaymentModalRef(component: Component, requirementPayment: RequirementPayment): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.requirementPayment = requirementPayment;
        modalRef.componentInstance.idRequirement = this.idRequirement;
        modalRef.componentInstance.idPayment = this.idPayment;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.requirementPaymentLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    requirementPaymentLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idRequirement = this.idRequirement;
        modalRef.componentInstance.idPayment = this.idPayment;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
