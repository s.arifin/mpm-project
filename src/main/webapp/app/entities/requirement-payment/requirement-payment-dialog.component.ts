import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RequirementPayment } from './requirement-payment.model';
import { RequirementPaymentPopupService } from './requirement-payment-popup.service';
import { RequirementPaymentService } from './requirement-payment.service';
import { ToasterService } from '../../shared';
import { Requirement, RequirementService } from '../requirement';
import { Payment, PaymentService } from '../payment';
import { ResponseWrapper, Principal } from '../../shared';

@Component({
    selector: 'jhi-requirement-payment-dialog',
    templateUrl: './requirement-payment-dialog.component.html'
})
export class RequirementPaymentDialogComponent implements OnInit {

    requirementPayment: RequirementPayment;
    isSaving: boolean;
    idRequirement: any;
    idPayment: any;

    requirements: Requirement[];

    payments: Payment[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected requirementPaymentService: RequirementPaymentService,
        protected requirementService: RequirementService,
        protected paymentService: PaymentService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.requirementService.query()
            .subscribe((res: ResponseWrapper) => { this.requirements = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.paymentService.query()
            .subscribe((res: ResponseWrapper) => { this.payments = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.requirementPayment.idReqPayment !== undefined) {
            this.subscribeToSaveResponse(
                this.requirementPaymentService.update(this.requirementPayment));
        } else {
            this.subscribeToSaveResponse(
                this.requirementPaymentService.create(this.requirementPayment));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RequirementPayment>) {
        result.subscribe((res: RequirementPayment) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: RequirementPayment) {
        this.eventManager.broadcast({ name: 'requirementPaymentListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'requirementPayment saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'requirementPayment Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRequirementById(index: number, item: Requirement) {
        return item.idRequirement;
    }

    trackPaymentById(index: number, item: Payment) {
        return item.idPayment;
    }
}

@Component({
    selector: 'jhi-requirement-payment-popup',
    template: ''
})
export class RequirementPaymentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected requirementPaymentPopupService: RequirementPaymentPopupService
    ) {}

    ngOnInit() {
        this.requirementPaymentPopupService.idRequirement = undefined;
        this.requirementPaymentPopupService.idPayment = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.requirementPaymentPopupService
                    .open(RequirementPaymentDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.requirementPaymentPopupService.parent = params['parent'];
                this.requirementPaymentPopupService
                    .open(RequirementPaymentDialogComponent as Component);
            } else {
                this.requirementPaymentPopupService
                    .open(RequirementPaymentDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
