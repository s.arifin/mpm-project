import { BaseEntity } from './../../shared';

export class RequirementPayment implements BaseEntity {
    constructor(
        public id?: any,
        public idReqPayment?: any,
        public amount?: number,
        public requirementId?: any,
        public paymentId?: any,
    ) {
    }
}
