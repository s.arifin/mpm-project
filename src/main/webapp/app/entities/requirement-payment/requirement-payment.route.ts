import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { RequirementPaymentComponent } from './requirement-payment.component';
import { RequirementPaymentLovPopupComponent } from './requirement-payment-as-lov.component';
import { RequirementPaymentPopupComponent } from './requirement-payment-dialog.component';

@Injectable()
export class RequirementPaymentResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idReqPayment,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const requirementPaymentRoute: Routes = [
    {
        path: 'requirement-payment',
        component: RequirementPaymentComponent,
        resolve: {
            'pagingParams': RequirementPaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirementPayment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const requirementPaymentPopupRoute: Routes = [
    {
        path: 'requirement-payment-lov',
        component: RequirementPaymentLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirementPayment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'requirement-payment-popup-new-list/:parent',
        component: RequirementPaymentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirementPayment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'requirement-payment-new',
        component: RequirementPaymentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirementPayment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'requirement-payment/:id/edit',
        component: RequirementPaymentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirementPayment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
