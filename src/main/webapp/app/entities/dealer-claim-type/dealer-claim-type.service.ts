import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { DealerClaimType } from './dealer-claim-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class DealerClaimTypeService {
    protected itemValues: DealerClaimType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/dealer-claim-types';
    protected resourceSearchUrl = 'api/_search/dealer-claim-types';

    constructor(protected http: Http) { }

    create(dealerClaimType: DealerClaimType): Observable<DealerClaimType> {
        const copy = this.convert(dealerClaimType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(dealerClaimType: DealerClaimType): Observable<DealerClaimType> {
        const copy = this.convert(dealerClaimType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<DealerClaimType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, dealerClaimType: DealerClaimType): Observable<DealerClaimType> {
        const copy = this.convert(dealerClaimType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, dealerClaimTypes: DealerClaimType[]): Observable<DealerClaimType[]> {
        const copy = this.convertList(dealerClaimTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(dealerClaimType: DealerClaimType): DealerClaimType {
        if (dealerClaimType === null || dealerClaimType === {}) {
            return {};
        }
        // const copy: DealerClaimType = Object.assign({}, dealerClaimType);
        const copy: DealerClaimType = JSON.parse(JSON.stringify(dealerClaimType));
        return copy;
    }

    protected convertList(dealerClaimTypes: DealerClaimType[]): DealerClaimType[] {
        const copy: DealerClaimType[] = dealerClaimTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: DealerClaimType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
