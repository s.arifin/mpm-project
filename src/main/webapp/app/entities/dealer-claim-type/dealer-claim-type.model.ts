import { BaseEntity } from './../../shared';

export class DealerClaimType implements BaseEntity {
    constructor(
        public id?: number,
        public idClaimType?: number,
        public description?: string,
    ) {
    }
}
