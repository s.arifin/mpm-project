import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {DealerClaimType} from './dealer-claim-type.model';
import {DealerClaimTypePopupService} from './dealer-claim-type-popup.service';
import {DealerClaimTypeService} from './dealer-claim-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-dealer-claim-type-dialog',
    templateUrl: './dealer-claim-type-dialog.component.html'
})
export class DealerClaimTypeDialogComponent implements OnInit {

    dealerClaimType: DealerClaimType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected dealerClaimTypeService: DealerClaimTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dealerClaimType.idClaimType !== undefined) {
            this.subscribeToSaveResponse(
                this.dealerClaimTypeService.update(this.dealerClaimType));
        } else {
            this.subscribeToSaveResponse(
                this.dealerClaimTypeService.create(this.dealerClaimType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<DealerClaimType>) {
        result.subscribe((res: DealerClaimType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: DealerClaimType) {
        this.eventManager.broadcast({ name: 'dealerClaimTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'dealerClaimType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'dealerClaimType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-dealer-claim-type-popup',
    template: ''
})
export class DealerClaimTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected dealerClaimTypePopupService: DealerClaimTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dealerClaimTypePopupService
                    .open(DealerClaimTypeDialogComponent as Component, params['id']);
            } else {
                this.dealerClaimTypePopupService
                    .open(DealerClaimTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
