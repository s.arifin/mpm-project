import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DealerClaimTypeComponent } from './dealer-claim-type.component';
import { DealerClaimTypeLovPopupComponent } from './dealer-claim-type-as-lov.component';
import { DealerClaimTypePopupComponent } from './dealer-claim-type-dialog.component';

@Injectable()
export class DealerClaimTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idClaimType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const dealerClaimTypeRoute: Routes = [
    {
        path: 'dealer-claim-type',
        component: DealerClaimTypeComponent,
        resolve: {
            'pagingParams': DealerClaimTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dealerClaimType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dealerClaimTypePopupRoute: Routes = [
    {
        path: 'dealer-claim-type-lov',
        component: DealerClaimTypeLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dealerClaimType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'dealer-claim-type-new',
        component: DealerClaimTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dealerClaimType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'dealer-claim-type/:id/edit',
        component: DealerClaimTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dealerClaimType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
