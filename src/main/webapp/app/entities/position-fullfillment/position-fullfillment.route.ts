import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PositionFullfillmentComponent } from './position-fullfillment.component';
import { PositionFullfillmentLovPopupComponent } from './position-fullfillment-as-lov.component';
import { PositionFullfillmentPopupComponent } from './position-fullfillment-dialog.component';

@Injectable()
export class PositionFullfillmentResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idPositionFullfillment,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const positionFullfillmentRoute: Routes = [
    {
        path: 'position-fullfillment',
        component: PositionFullfillmentComponent,
        resolve: {
            'pagingParams': PositionFullfillmentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.positionFullfillment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const positionFullfillmentPopupRoute: Routes = [
    {
        path: 'position-fullfillment-lov',
        component: PositionFullfillmentLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.positionFullfillment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'position-fullfillment-new',
        component: PositionFullfillmentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.positionFullfillment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'position-fullfillment/:id/edit',
        component: PositionFullfillmentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.positionFullfillment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
