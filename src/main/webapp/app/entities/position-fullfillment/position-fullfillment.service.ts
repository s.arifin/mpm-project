import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { JhiDateUtils } from 'ng-jhipster';

import { PositionFullfillment } from './position-fullfillment.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class PositionFullfillmentService {
    protected itemValues: PositionFullfillment[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/position-fullfillments';
    protected resourceSearchUrl = 'api/_search/position-fullfillments';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(positionFullfillment: PositionFullfillment): Observable<PositionFullfillment> {
        const copy = this.convert(positionFullfillment);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(positionFullfillment: PositionFullfillment): Observable<PositionFullfillment> {
        const copy = this.convert(positionFullfillment);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<PositionFullfillment> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(positionFullfillment: PositionFullfillment, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(positionFullfillment);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, positionFullfillment: PositionFullfillment): Observable<PositionFullfillment> {
        const copy = this.convert(positionFullfillment);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, positionFullfillments: PositionFullfillment[]): Observable<PositionFullfillment[]> {
        const copy = this.convertList(positionFullfillments);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateFrom) {
            entity.dateFrom = new Date(entity.dateFrom);
        }
        if (entity.dateThru) {
            entity.dateThru = new Date(entity.dateThru);
        }
    }

    protected convert(positionFullfillment: PositionFullfillment): PositionFullfillment {
        if (positionFullfillment === null || positionFullfillment === {}) {
            return {};
        }
        // const copy: PositionFullfillment = Object.assign({}, positionFullfillment);
        const copy: PositionFullfillment = JSON.parse(JSON.stringify(positionFullfillment));

        // copy.dateFrom = this.dateUtils.toDate(positionFullfillment.dateFrom);

        // copy.dateThru = this.dateUtils.toDate(positionFullfillment.dateThru);
        return copy;
    }

    protected convertList(positionFullfillments: PositionFullfillment[]): PositionFullfillment[] {
        const copy: PositionFullfillment[] = positionFullfillments;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: PositionFullfillment[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
