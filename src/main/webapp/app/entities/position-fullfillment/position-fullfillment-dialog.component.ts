import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {PositionFullfillment} from './position-fullfillment.model';
import {PositionFullfillmentPopupService} from './position-fullfillment-popup.service';
import {PositionFullfillmentService} from './position-fullfillment.service';
import {ToasterService} from '../../shared';
import { Position, PositionService } from '../position';
import { Person, PersonService } from '../person';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-position-fullfillment-dialog',
    templateUrl: './position-fullfillment-dialog.component.html'
})
export class PositionFullfillmentDialogComponent implements OnInit {

    positionFullfillment: PositionFullfillment;
    isSaving: boolean;

    positions: Position[];

    people: Person[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected positionFullfillmentService: PositionFullfillmentService,
        protected positionService: PositionService,
        protected personService: PersonService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.positionService.query()
            .subscribe((res: ResponseWrapper) => { this.positions = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.personService.query()
            .subscribe((res: ResponseWrapper) => { this.people = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.positionFullfillment.idPositionFullfillment !== undefined) {
            this.subscribeToSaveResponse(
                this.positionFullfillmentService.update(this.positionFullfillment));
        } else {
            this.subscribeToSaveResponse(
                this.positionFullfillmentService.create(this.positionFullfillment));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PositionFullfillment>) {
        result.subscribe((res: PositionFullfillment) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: PositionFullfillment) {
        this.eventManager.broadcast({ name: 'positionFullfillmentListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'positionFullfillment saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'positionFullfillment Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackPositionById(index: number, item: Position) {
        return item.idPosition;
    }

    trackPersonById(index: number, item: Person) {
        return item.idParty;
    }
}

@Component({
    selector: 'jhi-position-fullfillment-popup',
    template: ''
})
export class PositionFullfillmentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected positionFullfillmentPopupService: PositionFullfillmentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.positionFullfillmentPopupService
                    .open(PositionFullfillmentDialogComponent as Component, params['id']);
            } else {
                this.positionFullfillmentPopupService
                    .open(PositionFullfillmentDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
