import { BaseEntity } from './../../shared';

export class PositionFullfillment implements BaseEntity {
    constructor(
        public id?: any,
        public idPositionFullfillment?: any,
        public dateFrom?: any,
        public dateThru?: any,
        public positionId?: any,
        public personId?: any,
    ) {
    }
}
