import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { Memos } from './memos.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class MemosService {
    protected itemValues: Memos[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/memos';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/memos';
    protected resourceAXUrl = process.env.API_C_URL + '/api/ax_generalledger';

    constructor(protected http: Http) { }

    create(memos: Memos): Observable<Memos> {
        const copy = this.convert(memos);
        console.log('Save memo - isi [create]==>', copy);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(memos: Memos): Observable<Memos> {
        const copy = this.convert(memos);
        console.log('Save memo - isi [update]==>', copy);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<Memos> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(memos: Memos, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(memos);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, memos: Memos): Observable<Memos> {
        const copy = this.convert(memos);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, memoss: Memos[]): Observable<Memos[]> {
        const copy = this.convertList(memoss);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(memos: Memos): Memos {
        if (memos === null || memos === {}) {
            return {};
        }
        // const copy: Memos = Object.assign({}, memos);
        const copy: Memos = JSON.parse(JSON.stringify(memos));
        return copy;
    }

    protected convertList(memoss: Memos[]): Memos[] {
        const copy: Memos[] = memoss;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: Memos[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    getDescriptionStatus(statusValue: Number): String {

        let hasil = null;

         switch (statusValue) {
            case 10 :
                hasil = 'DRAFT';
                return hasil;
            case 17 :
                hasil = 'COMPLETED';
                return hasil;
            case 61 :
                hasil = 'APPROVE';
                return hasil;
            case 75 :
                hasil = 'NOT APPROVED';
                return hasil;
            case 13 :
                hasil = 'CANCELED';
                return hasil;
            default :
                hasil = 'UNDIFINED STATUS ' + statusValue;
                return hasil;
        }
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post('${this.resourceUrl}/process', params, options).map((res: Response) => {
            return res.json();
        });
    }

    GLMemoKoreksiDiskon(memonumber: string): Observable<any> {
        return this.http.get(this.resourceAXUrl + '/GLMemoKoreksiDiskon?memonumber=' + memonumber).map((res: Response) => {
            return res.json();
        });
    }
}
