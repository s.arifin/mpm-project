import { Component, OnInit, OnDestroy, DoCheck, OnChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Memos } from './memos.model';
import { MemosService } from './memos.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { Billing, BillingService } from '../billing';
import { MemoType, MemoTypeService } from '../memo-type';
import { ResponseWrapper } from '../../shared';
import { FeatureService } from '../feature/index';
import { VehicleSalesBillingService } from '../vehicle-sales-billing/index';
import { VehicleSalesBilling } from '../vehicle-sales-billing/vehicle-sales-billing.model';
import { BillingItem, BillingItemService } from '../billing-item';
import { InventoryItem, InventoryItemService } from '../inventory-item';
import { LoadingService } from '../../layouts/index';
import { Feature } from '../feature/feature.model';
import * as _ from 'lodash';
import { Motor, MotorService } from '../motor';
import { OrderItemService, OrderItem } from '../order-item';
import { SalesUnitRequirementService, SalesUnitRequirement } from '../sales-unit-requirement';
import { VehicleSalesOrderService, VehicleSalesOrder } from '../vehicle-sales-order';

@Component({
    selector: 'jhi-memos-new',
    templateUrl: './memos-new.component.html'
})
export class MemosNewComponent implements OnInit, OnDestroy, DoCheck {

    protected subscription: Subscription;
    invItemSubscriber: Subscription;
    vsbSubscriber: Subscription;
    memos: Memos;
    viewMemo: Memos;
    memoKoreksi: Memos;
    memoses: Memos[];
    isSaving: boolean;
    internals: Internal[];
    billings: Billing[];
    memotypes: MemoType[];
    idMemoType: number;
    listMemo: boolean;
    colors: any[];
    hasilColor: any[];
    orders: any;
    tglMemo: Date;
    vehicleSalesBilling: VehicleSalesBilling;
    billingItems: BillingItem[];
    memoDetil: any;
    inventoryItem: InventoryItem;
    inventoryItem2: InventoryItem;
    internalId: any;
    jenisGanti: any;
    jenisGantiNoka: boolean;
    jenisCancelIVU: boolean;
    jenisGantiWarna: boolean;
    viewIdProd: any;
    viewIdColor: any;
    viewYear: any;
    cariNoInput: any;
    selectedColor: any;
    isInvalid = true;
    komposisiOTR = 0;
    komposisiBBN = 0;
    komposisiOffRoad = 0;
    komposisiSalesAmount = 0;
    komposisiDiscount = 0;
    komposisiPrice = 0;
    komposisiPPN = 0;
    komposisiAR: number;
    discExppn = 0;
    discEncppn = 0;
    features: Feature[];
    komposisiOnTheRoad: any;

    oldIdFrame: string;
    oldFeature: number;

    subsidiDealer = 0;
    subsidiFinancialComp = 0;
    subsidiMD = 0;
    subsidiAHM = 0;
    orderItem: OrderItem[];
    motors: Motor[];
    cogs: any;
    isDisable: boolean;
    ivtGC: InventoryItem[];
    ivtID: any;
    memoProductId: any;
    komposisiSUR: SalesUnitRequirement[];
    vso: VehicleSalesOrder;

    constructor(
        protected alertService: JhiAlertService,
        protected memosService: MemosService,
        protected internalService: InternalService,
        protected billingService: BillingService,
        protected memoTypeService: MemoTypeService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected featureService: FeatureService,
        protected vehicleSalesBillingService: VehicleSalesBillingService,
        protected billingItemService: BillingItemService,
        protected inventoryItemService: InventoryItemService,
        protected loadingService: LoadingService,
        protected motorService: MotorService,
        protected orderItemService: OrderItemService,
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected vehicleSalesOrderService: VehicleSalesOrderService,
    ) {
        this.motors = new Array<Motor>();
        this.memos = new Memos();
        this.memoKoreksi = new Memos();
        this.inventoryItem2 = new InventoryItem();
        this.vehicleSalesBilling = new VehicleSalesBilling();
        this.memoses = new Array<Memos>();
        this.komposisiSUR = new Array<SalesUnitRequirement>();
        this.subscription = new Subscription();
        this.invItemSubscriber = new Subscription();
        this.vsbSubscriber = new Subscription();
        this.inventoryItem = new InventoryItem();
        this.ivtGC = new Array<InventoryItem>();
        this.viewMemo = new Memos();
        this.oldFeature = null;
        this.oldIdFrame = null;

        this.komposisiOTR = 0;
        this.komposisiBBN = 0;
        this.komposisiOffRoad = 0;
        this.komposisiSalesAmount = 0;
        this.komposisiDiscount = 0;
        this.komposisiPrice = 0;
        this.komposisiPPN = 0;
        this.komposisiAR = 0;
        this.subsidiDealer = 0;
        this.subsidiFinancialComp = 0;
        this.subsidiMD = 0;
        this.subsidiAHM = 0;
        this.isDisable = false;
        this.discExppn = 0;
        this.discEncppn = 0;
        this.ivtID = null;
        this.memoProductId = null;
        this.vso = new VehicleSalesOrder();
    }

    ngOnInit() {
        this.registerFromvsbLovItem();
        this.registerFromIvtItemItem();

        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billingService.query()
            .subscribe((res: ResponseWrapper) => { this.billings = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.memoTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.memotypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.featureService
            .query()
            .subscribe(
                (res: ResponseWrapper) => { this.colors = res.json, console.log('feature => ', this.colors) },
                (res: ResponseWrapper) => { this.onError(res.json) }
            )
        console.log('Prepare param');
        this.listMemo = true;
        this.tglMemo = new Date();
        this.memoDetil = 1;
        this.jenisGanti = 1;
        this.memoses = new Array<Memos>();
        this.jenisGantiNoka = true;
        this.jenisGantiWarna = false;
        this.idMemoType = 2;
        this.viewIdProd = '';
        this.viewIdColor = '';
        this.viewYear = '';
        // 1 = kompisisi harga
        // 2 = noka nosin
        // 3 = warna
    }

    ngDoCheck() {
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.vsbSubscriber.unsubscribe();
        this.invItemSubscriber.unsubscribe();
    }

    memoGantiUnit(idProduct: string) {
        this.memoDetil = 2;
        this.motorService.find(idProduct).subscribe(
            (res) => {
                this.motors.push(res);
                this.getColorByMotor(idProduct);
                console.log('cari motor', res);
            }
        )
        // this.idMemoType = 2;
    }

    memoGantiKomposisi() {
        this.idMemoType = 1;
        this.memoDetil = 3;

        this.komposisiOTR = 0;
        this.komposisiBBN = 0;
        this.komposisiOffRoad = 0;
        this.komposisiSalesAmount = 0;
        this.komposisiDiscount = 0;
        this.komposisiPrice = 0;
        this.komposisiPPN = 0;
        this.komposisiAR = 0;

        let idBill: string;
        idBill = this.vehicleSalesBilling.id;
        console.log('search id billing = ', idBill);
        if (this.vehicleSalesBilling.typeId === 10 || this.vehicleSalesBilling.typeId === 110) {
            this.getBillingItemDetil(idBill);
        } else {
        }

    }
    getBillingItemDetil(idBill) {
        this.billingItemService
            .queryFilterBy({
                idBilling: idBill
            })
            .subscribe(
                (respon) => {
                    console.log('isi ==> billing item servie ', respon.json),
                        this.billingItems = respon.json
                    // for (const billingItem of this.billingItems) {
                    this.billingItems.forEach(
                        (billingItem) => {
                            console.log('billing item yg ga item', billingItem);

                            if (billingItem.idItemType === 501) {
                                this.subsidiDealer = billingItem.unitPrice;
                                // 501 = sur.subOwn
                            }

                            if (billingItem.idItemType === 502) {
                                this.subsidiFinancialComp = billingItem.unitPrice;
                                // 502 = sur.subsfinco
                            }

                            if (billingItem.idItemType === 503) {
                                this.subsidiMD = billingItem.unitPrice;
                                // 503 = sur.subsMd
                            }

                            if (billingItem.idItemType === 505) {
                                this.subsidiAHM = billingItem.unitPrice;
                                // 505 = sur.subsAhm
                            }

                            if (billingItem.idItemType === 511) {
                                this.komposisiBBN = billingItem.unitPrice;
                                // 511 == sur.bbnprice
                            }

                            if (billingItem.idItemType === 5000) {
                                // this.komposisiPPN = billingItem.unitPrice;
                            }

                            if (billingItem.sequence === 10) {
                                console.log('otr ', billingItem.unitPrice);
                                this.komposisiOTR = Math.round(billingItem.unitPrice * 1.1);
                                this.komposisiSalesAmount = (billingItem.unitPrice);
                                this.komposisiOffRoad = Math.round(billingItem.unitPrice * 1.1);
                            }
                            if (this.vso.subFincoyAsDp === true || this.vso.subFincoyAsDp === 1) {
                                this.komposisiDiscount = (this.subsidiDealer + this.subsidiMD + this.subsidiAHM) / 1.1;
                                console.log('ini diskon atas = ', this.komposisiDiscount);
                            } else {
                                this.komposisiDiscount = (this.subsidiDealer + this.subsidiFinancialComp + this.subsidiMD + this.subsidiAHM) / 1.1;
                                console.log('ini diskon bawah', this.komposisiDiscount);
                            }
                        }
                    )
                    /*
                        OTR = dimaster motor(Regular Price)
                        BBN = dimaster motor(BBN)
                        OFF THE ROAD = OTR - BBN
                        SALES AMOUNT =  OFF THE ROAD / 1,1
                        DISCOUNT = total discount (subsidi MD, subsidi AHM, Diskon Dealer)
                        PRICE = Sales Amount - discount
                        PPN = Price * 10%
                        AR = BBN + Price + PPN

                        disc keluarkan dari ppn
                        ppn ambil 10% dari price
                        aramaount = price+ppn+bbn
                        price = salesamount-disc

                    */
                    // this.komposisiOffRoad = this.komposisiOTR;
                    // this.komposisiSalesAmount = Math.round(this.komposisiOffRoad / 1.1);
                    this.komposisiPrice = Math.round(this.komposisiSalesAmount - this.komposisiDiscount);
                    this.komposisiPPN = Math.round((this.komposisiSalesAmount - this.komposisiDiscount) * 0.1);
                    this.komposisiOnTheRoad = Math.round(this.komposisiOTR + this.komposisiBBN);
                    this.komposisiAR = Math.round((this.komposisiBBN + this.komposisiPrice + this.komposisiPPN));
                }
            )
    }

    closeDetilGantiKomposisi(close: Boolean) {
        if (this.jenisGantiNoka === true && close === false) {
            this.idMemoType = 2;
        }
        if (this.jenisGantiWarna === true && close === false) {
            this.idMemoType = 3;
        } if (close === true) {
            this.memoDetil = 1;
        }
    }

    closeUpdateKomposisi() {
        this.memoDetil = 1;
    }

    search(event) {
        console.log('hasil ==>', event);
    }

    load(id) {
        this.loadingService.loadingStart();
        console.log('Get by id [', id, '] ')
        this.memosService.find(id)
            .subscribe((memos) => {
                this.memos = memos;
                this.jenisGantiNoka = false;
                this.jenisGantiWarna = false;
                this.idMemoType = this.memos.memoTypeId;
                switch (this.memos.memoTypeId) {
                    case 1:
                        break;
                    case 2:
                        this.jenisGantiNoka = true;
                        break;
                    case 3:
                        this.jenisGantiWarna = true;
                        break;
                }

                console.log('Get by id:find memo service =>[', memos, ']'),
                    this.vehicleSalesBillingService
                        .find(this.memos.billingId)
                        .subscribe(
                            (response) => {
                                console.log('Get by id:find vsb service =>[', response, ']'),
                                    this.vehicleSalesBilling = response;
                                if (this.vehicleSalesBilling === undefined) { return };
                                console.log('vsb terpilih dari get by Id adalah ==>', this.vehicleSalesBilling);
                                this.processMemosNextItemOldInv();
                                this.inventoryItemService.find(this.memos.inventoryItemId)
                                    .subscribe(
                                        (response2) => {
                                            console.log('response', response2);
                                            if (response !== undefined) {
                                                this.inventoryItem2 = response2;
                                                this.processMemosNextItemChoosenInv(this.inventoryItem2);
                                            }
                                            console.log('inv==>', this.inventoryItem2);
                                        });
                            });
            },
                (err) => { console.log('Error => ', err); },
                () => this.loadingService.loadingStop(),
            );
    }

    loadMemoComplete(id) {
        this.memosService.find(id)
            .subscribe(
                (res) => {
                    this.memoKoreksi = res;
                    this.oldIdFrame = this.memoKoreksi.oldInventoryItem.idFrame;
                    this.oldFeature = this.memoKoreksi.oldInventoryItem.idFeature;
                    console.log('memo load complete ', this.memoKoreksi);
                }
            )
    }

    clearPage() {
        this.cariNoInput = null;
        this.vehicleSalesBilling.billingNumber = null;
        this.memos.billingId = null;
        this.idMemoType = null;
        this.memos.dealerId = null;
        this.memos.memoNumber = null;
        this.tglMemo = null;
        this.memoses = new Array<Memos>();

    }

    previousState() {
        // this.clearPage();
        this.router.navigate(['memos']);
    }

    save() {
        this.internalId = this.internalId;
        this.memos.memoTypeId = this.idMemoType;
        this.memos.unitPrice = this.komposisiOffRoad;
        this.memos.saleSamount = this.komposisiSalesAmount;
        this.memos.memoProductId = this.memoProductId;
        this.isSaving = true;
        if (this.memos.memoTypeId === 1) {
            if (this.vehicleSalesBilling.typeId !== 10) {
                const a = this.memos.arAmount - this.komposisiAR;
                console.log('hasil ARAMOUNT = ', a);
                if (a !== 0) {
                    this.toaster.showToaster('EROR', 'EROR', 'Untuk Instansi AR Tidak Boleh Berubah');
                    this.isSaving = false;
                } else if (this.memos.arAmount === this.komposisiAR) {
                    if (this.memos.idMemo !== undefined) {
                        this.subscribeToSaveResponse(
                            this.memosService.update(this.memos));
                    } else {
                        this.subscribeToSaveResponse(
                            this.memosService.create(this.memos));
                    }
                }
            } else if (this.memos.idMemo !== undefined) {
                this.subscribeToSaveResponse(
                    this.memosService.update(this.memos));
            } else {
                this.subscribeToSaveResponse(
                    this.memosService.create(this.memos));
            }
        } else {
            if (this.memos.idMemo !== undefined) {
                this.subscribeToSaveResponse(
                    this.memosService.update(this.memos));
            } else {
                this.subscribeToSaveResponse(
                    this.memosService.create(this.memos));
            }
        }
    }

    reset() {
        this.motors = new Array<Motor>();
        this.memos = new Memos();
        this.memoKoreksi = new Memos();
        this.inventoryItem2 = new InventoryItem();
        this.vehicleSalesBilling = new VehicleSalesBilling();
        this.memoses = new Array<Memos>();
        this.komposisiSUR = new Array<SalesUnitRequirement>();
        this.subscription = new Subscription();
        this.invItemSubscriber = new Subscription();
        this.vsbSubscriber = new Subscription();
        this.ivtGC = new Array<InventoryItem>();
        this.inventoryItem = new InventoryItem();
    }

    protected subscribeToSaveResponse(result: Observable<Memos>) {
        result.subscribe((res: Memos) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Memos) {
        this.eventManager.broadcast({ name: 'memosListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'memos saved !');
        this.isSaving = false;
        this.previousState();
        this.ngOnDestroy();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'memos Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillingById(index: number, item: Billing) {
        return item.idBilling;
    }

    trackMemoTypeById(index: number, item: MemoType) {
        return item.idMemoType;
    }

    findInventoryByBillingForGC(idBill) {
        this.inventoryItemService.queryFilterBy({
            idBilling: idBill
        }).subscribe(
            (res) => {
                this.ivtGC = res.json;
                console.log('ini ivt gc', res);
            }
        )
    }
    selectedIVGC() {
        this.chooseIVTGC(this.ivtID);
        console.log('ivt yang dipilih == ', this.ivtID)
    }

    chooseIVTGC(idInvItem) {
        this.inventoryItemService
            .find(idInvItem)
            .subscribe(
                (res) => {
                    this.memoses = [];
                    this.inventoryItem = res,
                        console.log('isi item inventory ', res);
                    this.memos.oldInventoryItem = res,
                        this.memos.oldInventoryItemId = res.idInventoryItem,
                        this.viewIdProd = res.idProduct;
                    this.viewIdColor = res.idFeature;
                    console.log('memos wakwauuuu', this.memos);
                    // this.memoses = [];
                    this.memoses = [...this.memoses, this.memos];
                    this.viewMemo = this.memoses[0];
                    this.orderItemService.queryFilterBy({
                        idFrame: res.idFrame
                    })
                        .subscribe(
                            (resOi) => {
                                this.cogs = null;
                                this.orderItem = resOi.json;
                                console.log('cogs di memo = ', this.orderItem);
                                const cogsmemo = this.orderItem[0].cogs;
                                this.cogs = cogsmemo;
                                console.log('nilai cogs memo, ', this.cogs);
                            }
                        )
                },
                (err) => console.log('Item tidak ditemukan !!')
            );
    }

    registerGetChooseVsb() {
        // this.vsbSubscriber =
        this.vehicleSalesBillingService.values.subscribe(
            (response) => {
                // console.log('response', response);
                this.vehicleSalesBilling = response;
                if (this.vehicleSalesBilling === undefined) { return };
                this.memos.currbillnumb = this.vehicleSalesBilling.billingNumber;
                this.memos.billingType = this.vehicleSalesBilling.typeId;
                console.log('billing id di memo = ', this.memos.billingType);
                console.log('vsb terpilih dari lov adalah ==>', this.vehicleSalesBilling);
                this.processMemosNextItemOldInv();
            });
    }

    processMemosNextItemOldInv() {
        let idBill: string;
        let idInvItem: String;

        idBill = this.vehicleSalesBilling.idBilling;
        this.internalId = this.vehicleSalesBilling.internalId;
        this.memos.billingId = idBill;
        this.memos.dealerId = this.vehicleSalesBilling.internalId;

        console.log('cari billing item service==> ', idBill);
        // this.memos.billingId = idBill;
        this.billingItemService
            .queryFilterBy({
                idBilling: idBill
            })
            .subscribe(
                (respon: ResponseWrapper) => {
                    console.log('isi ==> billing item servie ', respon.json),
                        this.billingItems = respon.json;

                    for (const billingItem of this.billingItems) {
                        console.log('iterate bill item ', billingItem);
                        if (billingItem.idItemType === 10) {
                            idInvItem = billingItem.inventoryItemId;
                        }
                    }

                    if (this.vehicleSalesBilling.typeId === 10 || this.vehicleSalesBilling.typeId === 110) {
                        this.inventoryItemService
                            .find(idInvItem)
                            .subscribe(
                                (res) => {
                                    this.inventoryItem = res,
                                        console.log('isi item inventory ', res);
                                    this.memos.oldInventoryItem = res,
                                        this.memos.oldInventoryItemId = res.idInventoryItem,
                                        this.viewIdProd = res.idProduct;
                                    this.viewIdColor = res.idFeature;
                                    console.log('memos wakwauuuu', this.memos);
                                    this.memoses = [...this.memoses, this.memos];
                                    this.viewMemo = this.memoses[0];
                                },
                                (err) => console.log('Item tidak ditemukan !!')
                            );

                        this.orderItemService.queryFilterBy({
                            idBill: this.vehicleSalesBilling.idBilling
                        })
                            .subscribe(
                                (res) => {
                                    this.orderItem = res.json;
                                    console.log('cogs di memo = ', this.orderItem);
                                    const cogsmemo = this.orderItem[0].cogs;
                                    this.cogs = cogsmemo;
                                    console.log('nilai cogs memo, ', this.cogs);
                                    this.getVso(this.orderItem[0].ordersId);
                                }
                            )
                    } else {
                        this.memoProductId = this.memos.memoProductId;
                        this.inventoryItemService
                            .find(this.memos.oldInventoryItemId)
                            .subscribe(
                                (res) => {
                                    this.inventoryItem = res,
                                        console.log('isi item inventory ', res);
                                    this.memos.oldInventoryItem = res,
                                        this.memos.oldInventoryItemId = res.idInventoryItem,
                                        this.viewIdProd = res.idProduct;
                                    this.viewIdColor = res.idFeature;
                                    this.ivtID = this.memos.oldInventoryItemId;
                                    console.log('memos wakwauuuu', this.memos);
                                    this.memoses = [...this.memoses, this.memos];
                                    this.viewMemo = this.memoses[0];
                                },
                                (err) => console.log('Item tidak ditemukan !!')
                            );

                        this.orderItemService.queryFilterBy({
                            idBill: this.vehicleSalesBilling.idBilling
                        })
                            .subscribe(
                                (res) => {
                                    this.cogs = null;
                                    this.orderItem = res.json;
                                    console.log('cogs di memo = ', this.orderItem);
                                    const cogsmemo = this.orderItem[0].cogs;
                                    this.cogs = cogsmemo;
                                    console.log('nilai cogs memo, ', this.cogs);
                                }
                            )
                        this.findInventoryByBillingForGC(idBill);
                    }
                });

    }

    processMemosNextItemChoosenInv(inventoryItem) {
        this.memos.inventoryItem = inventoryItem;
        this.memos.inventoryItemId = inventoryItem.idInventoryItem;
        this.memoses = new Array<Memos>();
        this.memoses = [...this.memoses, this.memos];
    }

    registerFromvsbLovItem() {
        this.eventManager.subscribe('vehicleSalesBillingLovModification', (response) => (this.registerGetChooseVsb()))
    }

    registerFromIvtItemItem() {
        this.eventManager.subscribe('inventoryItemLovModification', (response) => this.registerGetChooseInventory())
    }

    registerGetChooseInventory() {
        // this.invItemSubscriber =
        this.inventoryItemService.values.subscribe(
            (response) => {
                console.log('response', response);
                if (response !== undefined) {
                    this.inventoryItem2 = response;
                    this.processMemosNextItemChoosenInv(this.inventoryItem2);
                }
                console.log('inv==>', this.inventoryItem2);
            });
    }

    public trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    protected getColorByMotor(idProduct: string): void {
        if (idProduct !== null) {
            this.features = new Array<Feature>();
            const selectedProduct: Motor = _.find(this.motors, function(e) {
                return e.idProduct.toLowerCase() === idProduct.toLowerCase();
            });
            this.features = selectedProduct.features;
        }
    }

    hitungHargaInputManual() {
        console.log('masuk ngga neehh');
        // this.discEncppn = Math.round(this.discExppn / (10 / 100));
        this.memos.discount = this.discExppn / 1.1;
        this.memos.dpp = this.komposisiSalesAmount - this.memos.discount;
        this.memos.ppn = Math.round(this.memos.dpp * (10 / 100))
        this.memos.arAmount = this.memos.dpp + this.memos.ppn + this.memos.bbn;

    }

    cancelIvu() {
        if (this.jenisCancelIVU === true) {
            this.idMemoType = 4;
            this.isDisable = true;
        } else {
            this.idMemoType = 0;
            this.isDisable = false;
        }
        console.log('id memo cancel ivu = ', this.idMemoType);
    }

    getVso(id) {
        this.vehicleSalesOrderService.find(id, null).subscribe(
            (res) => {
                this.vso = res
            }
        )
    }

}
