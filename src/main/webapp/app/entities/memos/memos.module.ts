import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    MemosService,
    MemosPopupService,
    MemosComponent,
    MemosDialogComponent,
    MemosPopupComponent,
    memosRoute,
    memosPopupRoute,
    MemosResolvePagingParams,
    MemosEditComponent,
    MemosNewComponent,
    IvtAsLovComponent,
    IvtLovPopupComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule,
         TooltipModule,
        } from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';
import { InventoryItemPopupService } from '../inventory-item';

const ENTITY_STATES = [
    ...memosRoute,
    ...memosPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,

        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        TooltipModule
    ],
    exports: [
        MemosComponent,
        MemosEditComponent,
        IvtAsLovComponent,
        IvtLovPopupComponent,
        MemosNewComponent,
    ],
    declarations: [
        MemosComponent,
        MemosDialogComponent,
        MemosPopupComponent,
        MemosEditComponent,
        IvtAsLovComponent,
        IvtLovPopupComponent,
        MemosNewComponent,
    ],
    entryComponents: [
        MemosComponent,
        MemosDialogComponent,
        MemosPopupComponent,
        MemosEditComponent,
        IvtAsLovComponent,
        IvtLovPopupComponent,
        MemosNewComponent,
    ],
    providers: [
        MemosService,
        MemosPopupService,
        MemosResolvePagingParams,
        InventoryItemPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMemosModule {}
