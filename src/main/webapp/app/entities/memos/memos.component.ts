import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { Memos } from './memos.model';
import { MemosService } from './memos.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';
import { APPROVAL_MEMO_KOREKSI_GANTI_MOTOR, APPROVAL_MEMO_KOREKSI_GANTI_HARGA, APPROVAL_MEMO_KOREKSI_CANCEL_IVU } from '../../shared/constants/vehicle-sales-order.constants';

// nuse
import { AxPosting, AxPostingService } from '../axposting';
import { AxPostingLine } from '../axposting-line';
import { BillingDetailC, BillingDetailCService } from '../billing-detail-c';
import { DatePipe } from '@angular/common';
import { log } from 'util';

@Component({
    selector: 'jhi-memos',
    templateUrl: './memos.component.html'
})
export class MemosComponent implements OnInit, OnDestroy {

    currentAccount: any;
    memos: Memos[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    // nuse
    billingDetail: BillingDetailC;
    axPosting: AxPosting;
    axPostingLine: AxPostingLine;
    VendorNum: string;

    constructor(
        protected memosService: MemosService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected commonUtilService: CommonUtilService,
        protected loadingService: LoadingService,
        // nuse
        protected billingDetailCService: BillingDetailCService,
        protected axPostingService: AxPostingService,
        protected datePipe: DatePipe,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.memosService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => this.loadingService.loadingStop(),
                );
            return;
        }
        this.memosService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json),
            () => this.loadingService.loadingStop(),
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/memos'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/memos', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/memos', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInMemos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Memos) {
        return item.idMemo;
    }

    registerChangeInMemos() {
        this.eventSubscriber = this.eventManager.subscribe('memosListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idMemo') {
            result.push('idMemo');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.memos = data;
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.memosService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.memosService.update(event.data)
                .subscribe((res: Memos) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.memosService.create(event.data)
                .subscribe((res: Memos) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: Memos) {
        this.toasterService.showToaster('info', 'Memos Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    approveMemo(memos: Memos) {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Approve Memo Ini [' + memos.currbillnumb + '] ?',
            header: 'Konfirmasi',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                if (memos.memoTypeId === 1) {
                    this.memosService
                    .executeProcess(APPROVAL_MEMO_KOREKSI_GANTI_HARGA, null, memos)
                    .subscribe(
                        (response) => {
                            this.memosService.GLMemoKoreksiDiskon(memos.memoNumber).subscribe(
                                (resGL) => { },
                                (err) => this.commonUtilService.showError(err)
                            );

                            this.loadingService.loadingStop();
                            this.eventManager.broadcast({
                                name: 'memosListModification',
                                content: 'Approved memo koreksi'
                            });
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    );
                }
                if (memos.memoTypeId === 2 || memos.memoTypeId === 3) {
                    this.memosService
                    .executeProcess(APPROVAL_MEMO_KOREKSI_GANTI_MOTOR, null, memos)
                    .subscribe(
                        (response) => {
                            this.loadingService.loadingStop();
                            this.eventManager.broadcast({
                                name: 'memosListModification',
                                content: 'Approved memo koreksi'
                            });
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    );
                }
                if (memos.memoTypeId === 4) {
                    this.memosService
                    .executeProcess(APPROVAL_MEMO_KOREKSI_CANCEL_IVU, null, memos)
                    .subscribe(
                        (response) => {
                            this.axPostJournal(memos);
                            this.loadingService.loadingStop();
                            this.eventManager.broadcast({
                                name: 'memosListModification',
                                content: 'Approved memo koreksi'
                            });
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    );
                }
            }
        });
    }

    getDescription(currentStatus: number) {

        return this.memosService.getDescriptionStatus(currentStatus);
    }

    buildReindex() {
        this.memosService.process({command: 'buildIndex'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed....');
        });
    }

    axPostJournal(memos) {
        this.loadingService.loadingStart();
        this.billingDetailCService.findbyBillingNumber(memos.currbillnumb).subscribe(
            (resBilDetail) => {
                this.billingDetail = resBilDetail;
                console.log('billing Detail : ', this.billingDetail);
                const today = new Date();
                const myaxPostingLine = [];

                this.axPosting = new AxPosting();

                this.axPosting.AutoPosting = 'FALSE';
                this.axPosting.AutoSettlement = 'FALSE';
                this.axPosting.DataArea = this.billingDetail.idMSO;
                this.axPosting.Guid = this.billingDetail.idOrdIte;
                this.axPosting.JournalName = 'VSO-CM';
                this.axPosting.Name = 'Cancel Sales Unit ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName + ' ' + this.billingDetail.noKa;
                this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                this.axPosting.TransactionType = 'Unit';

                // credit
                if ((this.billingDetail.finComRefCode !== '' && this.billingDetail.finComRefCode != null && this.billingDetail.creditDP > 0) ||
                    ((this.billingDetail.finComRefCode === '' || this.billingDetail.finComRefCode == null) && this.billingDetail.arAmount > 0)) {
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = this.billingDetail.idCustomer;
                    this.axPostingLine.AccountType = 'Cust';
                    if ( this.billingDetail.finComRefCode !== '' && this.billingDetail.finComRefCode != null ) {
                        this.axPostingLine.AmountCurCredit = this.billingDetail.creditDP.toString();
                    } else {
                        this.axPostingLine.AmountCurCredit = this.billingDetail.arAmount.toString();
                    }
                    this.axPostingLine.AmountCurDebit = '0';
                    this.axPostingLine.Company = this.billingDetail.idMSO;
                    this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                    this.axPostingLine.Description = 'AR Cust '  + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                    this.axPostingLine.Payment = '';

                    myaxPostingLine.push(this.axPostingLine);
                }

                if ( this.billingDetail.finComRefCode !== '' && this.billingDetail.finComRefCode != null ) {
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = this.billingDetail.finComParty;
                    this.axPostingLine.AccountType = 'Cust';
                    this.axPostingLine.AmountCurDebit = '0';
                    this.axPostingLine.AmountCurCredit = this.billingDetail.arAmount.toString();
                    this.axPostingLine.Company = this.billingDetail.idMSO;
                    this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                    this.axPostingLine.Description = 'AR Finco ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                    this.axPostingLine.Payment = '';

                    myaxPostingLine.push(this.axPostingLine);
                }

                if (this.billingDetail.discount > 0) {
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = '421001';
                    this.axPostingLine.AccountType = 'Ledger';
                    this.axPostingLine.AmountCurDebit = '0';
                    this.axPostingLine.AmountCurCredit = this.billingDetail.discount.toString();
                    this.axPostingLine.Company = this.billingDetail.idMSO;
                    this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                    this.axPostingLine.Description = 'DISC ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = this.billingDetail.CategoryType;
                    this.axPostingLine.Dimension5 = this.billingDetail.SegmentType;
                    this.axPostingLine.Dimension6 = this.billingDetail.TypeCode;
                    this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                    this.axPostingLine.Payment = '';

                    myaxPostingLine.push(this.axPostingLine);
                }

                // debit
                this.axPostingLine = new AxPostingLine();
                this.axPostingLine.AccountNum = '411001';
                this.axPostingLine.AccountType = 'Ledger';
                this.axPostingLine.AmountCurDebit = this.billingDetail.axUnitPrice.toString();
                this.axPostingLine.AmountCurCredit = '0';
                this.axPostingLine.Company = this.billingDetail.idMSO;
                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                this.axPostingLine.Description = 'SALES ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                this.axPostingLine.Dimension1 = '1014';
                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                this.axPostingLine.Dimension3 = '000';
                this.axPostingLine.Dimension4 = this.billingDetail.CategoryType;
                this.axPostingLine.Dimension5 = this.billingDetail.SegmentType;
                this.axPostingLine.Dimension6 = this.billingDetail.TypeCode;
                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                this.axPostingLine.Payment = '';

                myaxPostingLine.push(this.axPostingLine);

                this.axPostingLine = new AxPostingLine();
                this.axPostingLine.AccountNum = '218901';
                this.axPostingLine.AccountType = 'Ledger';
                this.axPostingLine.AmountCurDebit = this.billingDetail.axPPN.toString();
                this.axPostingLine.AmountCurCredit = '0';
                this.axPostingLine.Company = this.billingDetail.idMSO;
                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                this.axPostingLine.Description = 'PPN ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                this.axPostingLine.Dimension1 = '1014';
                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                this.axPostingLine.Dimension3 = '000';
                this.axPostingLine.Dimension4 = '000';
                this.axPostingLine.Dimension5 = '00';
                this.axPostingLine.Dimension6 = '000';
                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                this.axPostingLine.Payment = '';

                myaxPostingLine.push(this.axPostingLine);

                if (this.billingDetail.axBBN > 0) {
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = this.billingDetail.vendorCode;
                    this.axPostingLine.AccountType = 'Vend';
                    this.axPostingLine.AmountCurDebit = this.billingDetail.axBBN.toString();
                    this.axPostingLine.AmountCurCredit = '0';
                    this.axPostingLine.Company = this.billingDetail.idMSO;
                    this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                    this.axPostingLine.Description = 'BBN ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                    this.axPostingLine.Payment = '';

                    myaxPostingLine.push(this.axPostingLine);
                }
                this.axPosting.LedgerJournalLine = myaxPostingLine;
                console.log('axposting : ', this.axPosting);
                this.axPostingService.send(this.axPosting).subscribe(
                    (resaxPosting: ResponseWrapper) => {
                        console.log('Success : ', resaxPosting.json.Message);
                    },
                    (resaxPosting: ResponseWrapper) => {
                        this.onError(resaxPosting.json);
                        console.log('error ax posting : ', resaxPosting.json.Message);
                    }
                );

                const myaxPostingLineOther = [];
                this.axPosting = new AxPosting();

                this.axPosting.AutoPosting = 'FALSE';
                this.axPosting.AutoSettlement = 'FALSE';
                this.axPosting.DataArea = this.billingDetail.idMSO;
                this.axPosting.Guid = this.billingDetail.idOrdIte;
                this.axPosting.JournalName = 'VSO-InvReturn';
                this.axPosting.Name = 'Cancel Issue for ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa;
                this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                this.axPosting.TransactionType = 'Unit';

                // debit
                this.axPostingLine = new AxPostingLine();
                this.axPostingLine.AccountNum = '114101';
                this.axPostingLine.AccountType = 'Ledger';
                this.axPostingLine.AmountCurDebit = this.billingDetail.BasePrice.toString();
                this.axPostingLine.AmountCurCredit = '0';
                this.axPostingLine.Company = this.billingDetail.idMSO;
                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                this.axPostingLine.Description = 'Issue for ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa;
                this.axPostingLine.Dimension1 = '1014';
                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                this.axPostingLine.Dimension3 = '000';
                this.axPostingLine.Dimension4 = this.billingDetail.CategoryType;
                this.axPostingLine.Dimension5 = this.billingDetail.SegmentType;
                this.axPostingLine.Dimension6 = this.billingDetail.TypeCode;
                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                this.axPostingLine.Payment = '';

                myaxPostingLineOther.push(this.axPostingLine);

                // credit
                this.axPostingLine = new AxPostingLine();
                this.axPostingLine.AccountNum = '511001';
                this.axPostingLine.AccountType = 'Ledger';
                this.axPostingLine.AmountCurDebit = '0';
                this.axPostingLine.AmountCurCredit = this.billingDetail.BasePrice.toString();
                this.axPostingLine.Company = this.billingDetail.idMSO;
                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                this.axPostingLine.Description = 'Issue for ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa;
                this.axPostingLine.Dimension1 = '1014';
                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                this.axPostingLine.Dimension3 = '000';
                this.axPostingLine.Dimension4 = this.billingDetail.CategoryType;
                this.axPostingLine.Dimension5 = this.billingDetail.SegmentType;
                this.axPostingLine.Dimension6 = this.billingDetail.TypeCode;
                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                this.axPostingLine.Payment = '';

                myaxPostingLineOther.push(this.axPostingLine);

                this.axPosting.LedgerJournalLine = myaxPostingLineOther;
                console.log('axpostingOther : ', this.axPosting);
                this.axPostingService.send(this.axPosting).subscribe(
                    (resaxPostingNew: ResponseWrapper) => {
                        console.log('Success : ', resaxPostingNew.json.Message);
                    },
                    (resaxPostingNew: ResponseWrapper) => {
                        this.onError(resaxPostingNew.json);
                        console.log('error ax posting : ', resaxPostingNew.json.Message);
                    }
                );

                const myaxPostingLineOther2 = [];
                this.axPosting = new AxPosting();

                if (this.billingDetail.arTitipan > 0) {
                    this.axPosting.AutoPosting = 'FALSE';
                    this.axPosting.AutoSettlement = 'FALSE';
                    this.axPosting.DataArea = this.billingDetail.idMSO;
                    this.axPosting.Guid = this.billingDetail.idOrdIte;
                    this.axPosting.JournalName = 'AR-PayUnitRev';
                    this.axPosting.Name = 'Pelunasan ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName + ' ' + this.billingDetail.noKa;
                    this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPosting.TransactionType = 'Unit';

                    // credit
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = this.billingDetail.idCustomer;
                    this.axPostingLine.AccountType = 'Cust';
                    this.axPostingLine.AmountCurDebit = this.billingDetail.arTitipan.toString();
                    this.axPostingLine.AmountCurCredit = '0';
                    this.axPostingLine.Company = this.billingDetail.idMSO;
                    this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                    this.axPostingLine.Description = 'Pelunasan ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                    this.axPostingLine.Payment = '';

                    myaxPostingLineOther2.push(this.axPostingLine);

                    // debit
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = this.billingDetail.bankCashAccNum;
                    this.axPostingLine.AccountType = 'Bank';
                    this.axPostingLine.AmountCurCredit = this.billingDetail.arTitipan.toString();
                    this.axPostingLine.AmountCurDebit = '0';
                    this.axPostingLine.Company = this.billingDetail.idMSO;
                    this.axPostingLine.DMSNum = null;
                    this.axPostingLine.Description = 'Pelunasan ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                    this.axPostingLine.Payment = '';

                    myaxPostingLineOther2.push(this.axPostingLine);

                    this.axPosting.LedgerJournalLine = myaxPostingLineOther2;
                    console.log('axpostingOther : ', this.axPosting);
                    this.axPostingService.send(this.axPosting).subscribe(
                        (resaxPostingNew: ResponseWrapper) => {
                            console.log('Success : ', resaxPostingNew.json.Message);
                        },
                        (resaxPostingNew: ResponseWrapper) => {
                            this.onError(resaxPostingNew.json);
                            console.log('error ax posting : ', resaxPostingNew.json.Message);
                        }
                    );
                }

                if (this.billingDetail.totalTitipan > 0) {

                    const myaxPostingLineOther4 = [];
                    this.axPosting = new AxPosting();

                    this.axPosting = new AxPosting();
                    this.axPosting.AutoPosting = 'FALSE';
                    this.axPosting.AutoSettlement = 'FALSE';
                    this.axPosting.DataArea = this.billingDetail.idMSO;
                    this.axPosting.Guid = '';
                    this.axPosting.JournalName = 'titipan-AR-UnitRev';
                    this.axPosting.Name = 'Pengembalian Uang Titipan ' + this.billingDetail.idCustomer + ' ' + this.billingDetail.orderNumber;
                    this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPosting.TransactionType = 'Unit';

                    // credit
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = this.billingDetail.idCustomer;
                    this.axPostingLine.AccountType = 'Cust';
                    this.axPostingLine.AmountCurDebit = this.billingDetail.totalTitipan.toString();
                    this.axPostingLine.AmountCurCredit = '0';
                    this.axPostingLine.Company = this.billingDetail.idMSO;
                    this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                    this.axPostingLine.Description = 'Pengembalian Uang Titipan ' + this.billingDetail.idCustomer  + ' ' + this.billingDetail.orderNumber;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPostingLine.Invoice = '';
                    this.axPostingLine.Payment = '';

                    myaxPostingLineOther4.push(this.axPostingLine);

                    // debit
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = this.billingDetail.bankCashAccNum;
                    this.axPostingLine.AccountType = 'Bank';
                    this.axPostingLine.AmountCurDebit = '0';
                    this.axPostingLine.AmountCurCredit = this.billingDetail.totalTitipan.toString();
                    this.axPostingLine.Company = this.billingDetail.idMSO;
                    this.axPostingLine.DMSNum = null;
                    this.axPostingLine.Description = 'Pengembalian Uang Titipan ' + this.billingDetail.idCustomer  + ' ' + this.billingDetail.orderNumber;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPostingLine.Invoice = '';
                    this.axPostingLine.Payment = '';

                    myaxPostingLineOther4.push(this.axPostingLine);

                    this.axPosting.LedgerJournalLine = myaxPostingLineOther4;
                    console.log('axPosting data :', this.axPosting);
                    this.axPostingService.send(this.axPosting).subscribe(
                        (resaxPosting: ResponseWrapper) =>
                            console.log('Success : ', resaxPosting.json.Message),
                        (resaxPosting: ResponseWrapper) => {
                            this.onError(resaxPosting.json);
                            console.log('error ax posting : ', resaxPosting.json.Message);
                        }
                    );
                }

                if ( this.billingDetail.finComRefCode !== '' && this.billingDetail.finComRefCode != null ) {
                    const myaxPostingLineOther3 = [];
                    this.axPosting = new AxPosting();

                    this.axPosting.AutoPosting = 'FALSE';
                    this.axPosting.AutoSettlement = 'FALSE';
                    this.axPosting.DataArea = this.billingDetail.idMSO;
                    this.axPosting.Guid = this.billingDetail.idOrdIte;
                    this.axPosting.JournalName = 'AR-CM';
                    this.axPosting.Name = 'Cancel AR Refund ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName + ' ' + this.billingDetail.noKa;
                    this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPosting.TransactionType = 'Unit';

                    // credit
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = this.billingDetail.finComRefCode;
                    this.axPostingLine.AccountType = 'Cust';
                    this.axPostingLine.AmountCurDebit = '0';
                    this.axPostingLine.AmountCurCredit = this.billingDetail.totalFinCom.toString();
                    this.axPostingLine.Company = this.billingDetail.idMSO;
                    this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                    this.axPostingLine.Description = 'AR Refund ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                    this.axPostingLine.Payment = '';

                    myaxPostingLineOther3.push(this.axPostingLine);

                    // debit
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = '411004';
                    this.axPostingLine.AccountType = 'Ledger';
                    this.axPostingLine.AmountCurDebit = this.billingDetail.subsFinCom.toString();
                    this.axPostingLine.AmountCurCredit = '0';
                    this.axPostingLine.Company = this.billingDetail.idMSO;
                    this.axPostingLine.DMSNum = null;
                    this.axPostingLine.Description = 'Rev fr Finco ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                    this.axPostingLine.Payment = '';

                    myaxPostingLineOther3.push(this.axPostingLine);

                    // debit
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = '218905';
                    this.axPostingLine.AccountType = 'Ledger';
                    this.axPostingLine.AmountCurDebit = this.billingDetail.ppnFinCom.toString();
                    this.axPostingLine.AmountCurCredit = '0';
                    this.axPostingLine.Company = this.billingDetail.idMSO;
                    this.axPostingLine.DMSNum = null;
                    this.axPostingLine.Description = 'PPN Keluaran ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                    this.axPostingLine.Payment = '';

                    myaxPostingLineOther3.push(this.axPostingLine);

                    this.axPosting.LedgerJournalLine = myaxPostingLineOther3;
                    console.log('axpostingOther : ', this.axPosting);
                    this.axPostingService.send(this.axPosting).subscribe(
                        (resaxPostingNew: ResponseWrapper) => {
                            console.log('Success : ', resaxPostingNew.json.Message);
                        },
                        (resaxPostingNew: ResponseWrapper) => {
                            this.onError(resaxPostingNew.json);
                            console.log('error ax posting : ', resaxPostingNew.json.Message);
                        }
                    );
                }

                if (this.billingDetail.finComRefCode !== '' && this.billingDetail.finComRefCode !== null ) {
                    const myaxPostingLineOther5 = [];
                    this.axPosting = new AxPosting();

                    this.axPosting.AutoPosting = 'FALSE';
                    this.axPosting.AutoSettlement = 'FALSE';
                    this.axPosting.DataArea = this.billingDetail.idMSO;
                    this.axPosting.Guid = this.billingDetail.idOrdIte;
                    this.axPosting.JournalName = 'AR-PayUnitRev';
                    this.axPosting.Name = 'Pelunasan LS ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName + ' ' + this.billingDetail.noKa;
                    this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPosting.TransactionType = 'Unit';

                    // credit
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = this.billingDetail.finComParty;
                    this.axPostingLine.AccountType = 'Cust';
                    this.axPostingLine.AmountCurDebit = this.billingDetail.arAmount.toString();
                    this.axPostingLine.AmountCurCredit = '0';
                    this.axPostingLine.Company = this.billingDetail.idMSO;
                    this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                    this.axPostingLine.Description = 'Pelunasan LS ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                    this.axPostingLine.Payment = '';

                    myaxPostingLineOther5.push(this.axPostingLine);

                    // debit
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = this.billingDetail.bankCashAccNum;
                    this.axPostingLine.AccountType = 'Bank';
                    this.axPostingLine.AmountCurDebit = '0';
                    this.axPostingLine.AmountCurCredit = this.billingDetail.arAmount.toString();
                    this.axPostingLine.Company = this.billingDetail.idMSO;
                    this.axPostingLine.DMSNum = null;
                    this.axPostingLine.Description = 'Pelunasan LS ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                    this.axPostingLine.Payment = '';

                    myaxPostingLineOther5.push(this.axPostingLine);

                    this.axPosting.LedgerJournalLine = myaxPostingLineOther5;
                    console.log('axpostingOther : ', this.axPosting);
                    this.axPostingService.send(this.axPosting).subscribe(
                        (resaxPostingNew: ResponseWrapper) => {
                            console.log('Success : ', resaxPostingNew.json.Message);
                        },
                        (resaxPostingNew: ResponseWrapper) => {
                            this.onError(resaxPostingNew.json);
                            console.log('error ax posting : ', resaxPostingNew.json.Message);
                        }
                    );
                }
            }
        );
        this.loadingService.loadingStop();
    }

    cancelMemo(data: Memos) {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Untuk Cancel Memo Ini [' + data.currbillnumb + '] ?',
            header: 'Konfirmasi',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.memosService.executeProcess(400, null, data)
                .subscribe(
                    (res) => {
                        console.log('data memo berhasil di cancel');
                        this.loadAll();
                    },
                    (err) => {
                    this.onError(err);
            }
        )
            }
                }
        )
    }
}
