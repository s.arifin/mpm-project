import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {Memos} from './memos.model';
import {MemosPopupService} from './memos-popup.service';
import {MemosService} from './memos.service';
import {ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { Billing, BillingService } from '../billing';
import { MemoType, MemoTypeService } from '../memo-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-memos-dialog',
    templateUrl: './memos-dialog.component.html'
})
export class MemosDialogComponent implements OnInit {

    memos: Memos;
    isSaving: boolean;

    internals: Internal[];

    billings: Billing[];

    memotypes: MemoType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected memosService: MemosService,
        protected internalService: InternalService,
        protected billingService: BillingService,
        protected memoTypeService: MemoTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billingService.query()
            .subscribe((res: ResponseWrapper) => { this.billings = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.memoTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.memotypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.memos.idMemo !== undefined) {
            this.subscribeToSaveResponse(
                this.memosService.update(this.memos));
        } else {
            this.subscribeToSaveResponse(
                this.memosService.create(this.memos));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Memos>) {
        result.subscribe((res: Memos) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: Memos) {
        this.eventManager.broadcast({ name: 'memosListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'memos saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'memos Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillingById(index: number, item: Billing) {
        return item.idBilling;
    }

    trackMemoTypeById(index: number, item: MemoType) {
        return item.idMemoType;
    }
}

@Component({
    selector: 'jhi-memos-popup',
    template: ''
})
export class MemosPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected memosPopupService: MemosPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.memosPopupService
                    .open(MemosDialogComponent as Component, params['id']);
            } else {
                this.memosPopupService
                    .open(MemosDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
