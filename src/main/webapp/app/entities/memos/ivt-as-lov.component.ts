import {Component, OnInit, OnChanges, OnDestroy, Output} from '@angular/core';
import {Response} from '@angular/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Rx';
import {JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { InventoryItem, InventoryItemService, InventoryItemPopupService} from '../inventory-item';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper} from '../../shared';
import { PaginationConfig} from '../../blocks/config/uib-pagination.config';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { EventEmitter } from 'events';
import { FeatureService } from '../feature/feature.service';
import { Feature } from '../feature/index';
import { Motor, MotorService } from '../motor';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-ivt-as-lov',
    templateUrl: './ivt-as-lov.component.html'
})
export class IvtAsLovComponent implements OnInit, OnDestroy {

    // @Output()
      // @Output()
    // fnCallback = new EventEmitter();

    // this.fnCallback.emit(this.receipts);

    data: InventoryItem;
    currentAccount: any;
    isSaving: boolean;
    inventoryItems: InventoryItem[];
    selected: InventoryItem[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    routeSub: any;
    routeparam: any;
    filterRegNoKa: any;
    filterRegNoSin: any;
    regNoKa: any;
    regNoSin: any;
    regCOGS: any;
    featureList: Feature[];
    selectedColor: any;
    productList: Motor[];
    reqFeature: any;
    warnaPilih: string ;
    idFeature: any;
    regYear: any;

    constructor(
        public activeModal: NgbActiveModal,
        protected inventoryItemService: InventoryItemService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected route: ActivatedRoute,
        protected featureService: FeatureService,
        protected motorService: MotorService,
        protected loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idFrame';
        this.reverse = 'asc';
        this.isSaving = false;
        this.regYear = 0;
    }

    loadAll() {
        this.routeSub = this.route.params.subscribe((params) => {

            // console.log('Isi Params ==> ' , params);
            // if (params['idFeature'] ) {
            //     this.reqFeature = params['idFeature'];
            // }

            if (this.currentSearch) {
                this.inventoryItemService.filterForMemo({
                    regNoSin : this.currentSearch,
                    // this.data['regNoSin'],
                    idProduct: this.data['idProduct'],
                    idInternal : this.data.idPartyRole,
                    idFeature : this.data['idFeature'],
                    regNoKa : this.currentSearch,
                    regYear : this.regYear,
                    // this.data['regNoKa'],
                    cogs: this.regCOGS,
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    sort: this.sort()}).subscribe(
                        (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json)
                    );
                return;
            }
            this.inventoryItemService.filterForMemo({
                regNoSin : this.filterRegNoSin,
                // this.data['regNoSin'],
                idProduct: this.data['idProduct'],
                idInternal : this.data.idPartyRole,
                idFeature : this.data['idFeature'],
                regNoKa : this.filterRegNoKa,
                regYear : this.regYear,
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        });
    }

    getFeatureList(): void {
        this.featureService.query({
            page: 0,
            size: 1000,
            sort: ['idFeature,asc']} )
            .subscribe(
                (res: ResponseWrapper) => {
                    // console.log('feature', res.json);
                    this.featureList = res.json
                },
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    filterData() {
        console.log('filter di lov invItem==> idInternal==>', this.data.idPartyRole , '');

        this.warnaPilih = '99999';
        if (this.reqFeature === null || this.reqFeature === undefined) {
            this.warnaPilih = '99999';
        }
        if (this.reqFeature !== '99999' ) {
            this.warnaPilih = this.data['idFeature'].toString();
        } else {
            if ( this.selectedColor === null || this.selectedColor === undefined  ) {
                this.warnaPilih = '99999';
            } else {
                this.warnaPilih = this.selectedColor;
            }
        }
        console.log('warna ==> ', this.warnaPilih);
       //  this.loadingService.loadingStart();
        this.inventoryItemService.filterForMemo({
            regNoSin : this.filterRegNoSin,
            // this.data['regNoSin'],
            idProduct: this.data['idProduct'],
            idInternal : this.data.idPartyRole,
            idFeature : this.data['idFeature'],
            regNoKa : this.filterRegNoKa,
            regYear : this.regYear,
            // this.data['regNoKa'],
            cogs: this.regCOGS,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json),
                ( ) => {},
                // this.loadingService.loadingStop(),
        );
    }

    transition() {
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {

        this.reqFeature = this.data.idFeature;
        // this.getProductList();
        this.getFeatureList();

        // this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            console.log('isi acc =>', this.currentAccount);
        });
        console.log('isi dari data  => ', this.data);
        this.regNoKa = this.data.regNoKa;
        if (this.regNoKa !== 'null') {
            this.filterRegNoKa = this.regNoKa
        } else {
            this.filterRegNoKa = '';
        }
        this.regNoSin = this.data.regNoSin;
        if (this.regNoSin !== 'null') {
            this.filterRegNoSin = this.regNoSin;
        } else {
            this.filterRegNoSin = '';
        }
        this.regCOGS = this.data.regCOGS;
        this.regYear = this.data.regYear;
        console.log('cogeeesss == ', this.regCOGS);
        console.log('yerrssss == ', this.regYear);

    }

    getReqFeatureDesc() {
        if (this.reqFeature === '99999') {
            return 'ALL';
        }else {
            return this.reqFeature;
        }
    };

    ngOnDestroy() {
    }

    trackId(index: number, item: InventoryItem) {
        return item.idInventoryItem;
    }

    public trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idFrame') {
            result.push('idFrame');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.inventoryItems = data;

        this.inventoryItems.forEach((invItm: InventoryItem) => {
            invItm.featureDescription = this.getColor(invItm.idFeature);
            this.getProdukNama(invItm.idProduct, invItm );
        });

    };

    getColor(idCol): string {
        const warna = this.featureList.filter(
            function cariWarna(feature) {
                return (feature.idFeature === idCol);
            }
         );
        return warna[0].description;
    }

    getProdukNama(idProd: string, invItm: InventoryItem) {
        return this.motorService.findPromise(idProd)
            .then(
                (res) => {
                    console.log('product find by id ==>', res.name);
                    invItm.itemDescription = res.name;
                },
            );
    }

    getProductName(idProd): string {
        let nama =  'NOT FOUND cuy';
        if (this.productList === undefined ) {return} ;
        for (let i = 0; i <= this.productList.length - 1; i++ ) {
            if (this.productList[i].idProduct.toLowerCase() === idProd.toLowerCase()) {
                nama = this.productList[i].name;
            }
        }
        return nama;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    pushData() {
        this.eventManager.broadcast({ name: 'inventoryItemLovModification', content: 'OK'});
        this.inventoryItemService.pushItems(this.selected);
        this.activeModal.dismiss('close');
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

}

@Component({
    selector: 'jhi-inventory-item-lov-popup',
    template: ''
})
export class IvtLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected inventoryItemPopupService: InventoryItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['idProduct']) {
                // :idProduct/:idPartyRole/:idFeature/:regNoKa/:regNoSin
                const data: object = {
                    'idProduct' : params['idProduct'],
                    'idPartyRole': params['idPartyRole'],
                    'idFeature' : params['idFeature'],
                    'regNoKa': params['regNoKa'],
                    'regNoSin': params['regNoSin'],
                    'regCOGS': params['regcogs'],
                    'regYear': params['regYear'],
                }

                this.inventoryItemPopupService
                    .load(IvtAsLovComponent as Component, data);
            } else {
                this.inventoryItemPopupService
                    .load(IvtAsLovComponent as Component, null);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }

}
