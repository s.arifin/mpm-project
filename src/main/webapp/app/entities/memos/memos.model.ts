import { BaseEntity } from './../../shared';
import { InventoryItem } from '../inventory-item/index';

export class Memos implements BaseEntity {
    constructor(
        public id?: any,
        public idMemo?: any,
        public memoNumber?: string,
        public bbn?: number,
        public discount?: number,
        public unitPrice?: number,
        public dealerId?: any,
        public billingId?: any,
        public memoTypeId?: any,
        public inventoryItem?: InventoryItem,
        public oldInventoryItem?: InventoryItem,
        public inventoryItemId?: any,
        public oldInventoryItemId?: any,
        public currbillnumb?: any,
        public reqIdFeature?: any,
        public currentStatus?: any,
        public ppn?: any,
        public arAmount?: number,
        public dpp?: any,
        public saleSamount?: any,
        public dateCreated?: any,
        public billingType?: any,
        public memoProductId?: String
    ) {
        this.inventoryItem = new InventoryItem();
        this.oldInventoryItem = new InventoryItem()
    }
}
