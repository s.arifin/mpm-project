import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MemosComponent } from './memos.component';
import { MemosEditComponent } from './memos-edit.component';
import { MemosPopupComponent } from './memos-dialog.component';
import { IvtLovPopupComponent } from './ivt-as-lov.component';
import { MemosNewComponent } from './memos-new.component';

@Injectable()
export class MemosResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idMemo,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const memosRoute: Routes = [
    {
        path: 'memos',
        component: MemosComponent,
        resolve: {
            'pagingParams': MemosResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.memos.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const memosPopupRoute: Routes = [
    {
        path: 'memos-new/:new',
        component: MemosNewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.memos.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'memos/:id/edit',
        component: MemosEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.memos.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'memos-popup-new',
        component: MemosPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.memos.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'memos/:id/popup-edit',
        component: MemosPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.memos.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ivt-lov/:idProduct/:idPartyRole/:idFeature/:regNoKa/:regNoSin/:regcogs/:regYear',
        component: IvtLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.inventoryItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
