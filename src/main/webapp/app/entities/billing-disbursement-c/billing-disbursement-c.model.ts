import { Billing } from '../billing/billing.model';

export class BillingDisbursementC extends Billing {
    constructor(
        public id?: any,
        public idBilling?: string,
        public billingNumber?: string,
        public dateCreate?: any,
        public dateDue?: any,
        public vendorInvoice?: string,
        public details?: any,
        public billingTypeId?: any,
        public payments?: any,
        public internalId?: any,
        public internalName?: string,
        public billToId?: any,
        public billFromId?: any,
        public vendorId?: any,
        public vendorName?: any,
        public totalAmount?: number,
        public totalPPN?: number,
        public totalDiscount?: number,
        public totalUnitPrice?: number,
        public orderNumber?: string,
        public shippingFee?: number,
        public dateIssued?: string
    ) {
        super();
    }
}
