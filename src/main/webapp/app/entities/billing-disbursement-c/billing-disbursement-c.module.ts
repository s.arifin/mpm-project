import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { BillingDisbursementCService } from './';

@NgModule({
    providers: [
        BillingDisbursementCService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmBillingDisbursementCModule {}
