import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';
import { SERVER_API_C_URL } from '../../app.constants';

import { BillingDisbursementC } from './billing-disbursement-c.model';

import * as moment from 'moment';
import {BillingDisbursement} from '../billing-disbursement/billing-disbursement.model';

@Injectable()
export class BillingDisbursementCService {

    protected resourceUrl = process.env.API_C_URL + '/api/billing_disbursement/';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    findByInvoiceId(id: any): Observable<BillingDisbursementC> {
        return this.http.get(`${this.resourceUrl}GetBillingDisbursementByInvoiceId/?id=${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse;
        });
    }
}
