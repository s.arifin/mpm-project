import {Component, OnInit, OnChanges, OnDestroy, Output} from '@angular/core';
import {Response} from '@angular/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Rx';
import {JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleSalesBilling } from './vehicle-sales-billing.model';
import { VehicleSalesBillingService } from './vehicle-sales-billing.service';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper} from '../../shared';
import { PaginationConfig} from '../../blocks/config/uib-pagination.config';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { VehicleSalesBillingPopupService } from './vehicle-sales-billing-popup.service';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { EventEmitter } from 'events';

@Component({
    selector: 'jhi-vehicle-sales-billing-as-lov',
    templateUrl: './vehicle-sales-billing-as-lov.component.html'
})
export class VehicleSalesBillingAsLovComponent implements OnInit, OnDestroy {

    // @Output()
    // fnCallback = new EventEmitter();

    // this.fnCallback.emit(this.receipts);

    data: VehicleSalesBilling;
    currentAccount: any;
    isSaving: boolean;
    vehicleSalesBillings: VehicleSalesBilling[];
    selected: VehicleSalesBilling[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    routeSub: any;

    constructor(
        public activeModal: NgbActiveModal,
        private vehicleSalesBillingService: VehicleSalesBillingService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private route: ActivatedRoute,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idBilling';
        this.reverse = 'asc';
        this.isSaving = false;
        this.selected = new Array<VehicleSalesBilling>();
    }

    loadAll() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (this.currentSearch) {
                console.log('SEARCH');
                this.vehicleSalesBillingService.queryFilterBy({
                    queryFor: 'search',
                    dataParam: this.currentSearch,
                }).subscribe(
                        (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json),
                    );
                return;
            }
            this.vehicleSalesBillingService.query({
                page: this.page - 1,
                size: this.itemsPerPage,
                forMemo: 'true'
                // sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        });
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    filterData() {
        console.log('filter di lov invItem');
        this.vehicleSalesBillingService.query({
            // this.data['regNoSin'],
            idProduct: this.data['idProduct'],
            idPartyRole : this.data['idPartyRole'],
            idFeature : this.data['idFeature'],
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    transition() {
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        console.log('isi dari data  => ', this.data);
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: VehicleSalesBilling) {
        return item.idBilling;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateCreate') {
            result.push('dateCreate');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.vehicleSalesBillings = data;

    };

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    pushData() {
        this.eventManager.broadcast({ name: 'vehicleSalesBillingLovModification', content: 'OK'});
        this.vehicleSalesBillingService.pushItems(this.selected);
        this.activeModal.dismiss('close');
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

}

@Component({
    selector: 'jhi-vehicle-sales-billing-lov-popup',
    template: ''
})
export class VehicleSalesBillingLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vehicleSalesBillingPopupService: VehicleSalesBillingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['idProduct']) {
                // // :idProduct/:idPartyRole/:idFeature/:regNoKa/:regNoSin
                const data: object = {
                    'idProduct' : params['idProduct'],
                    'idPartyRole': params['idPartyRole'],
                    'idFeature' : params['idFeature'],
                    'regNoKa': params['regNoKa'],
                    'regNoSin': params['regNoSin']
                }

                this.vehicleSalesBillingPopupService
                    .load(VehicleSalesBillingAsLovComponent as Component, data);
            } else {
                this.vehicleSalesBillingPopupService
                    .load(VehicleSalesBillingAsLovComponent as Component, null);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }

}
