import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {VehicleSalesBilling} from './vehicle-sales-billing.model';
import {VehicleSalesBillingPopupService} from './vehicle-sales-billing-popup.service';
import {VehicleSalesBillingService} from './vehicle-sales-billing.service';
import {ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { Customer, CustomerService } from '../customer';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-vehicle-sales-billing-dialog',
    templateUrl: './vehicle-sales-billing-dialog.component.html'
})
export class VehicleSalesBillingDialogComponent implements OnInit {

    vehicleSalesBilling: VehicleSalesBilling;
    isSaving: boolean;

    internals: Internal[];

    billtos: BillTo[];

    customers: Customer[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private vehicleSalesBillingService: VehicleSalesBillingService,
        private internalService: InternalService,
        private billToService: BillToService,
        private customerService: CustomerService,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.vehicleSalesBilling.idBilling !== undefined) {
            this.subscribeToSaveResponse(
                this.vehicleSalesBillingService.update(this.vehicleSalesBilling));
        } else {
            this.subscribeToSaveResponse(
                this.vehicleSalesBillingService.create(this.vehicleSalesBilling));
        }
    }

    private subscribeToSaveResponse(result: Observable<VehicleSalesBilling>) {
        result.subscribe((res: VehicleSalesBilling) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: VehicleSalesBilling) {
        this.eventManager.broadcast({ name: 'vehicleSalesBillingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vehicleSalesBilling saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.toaster.showToaster('warning', 'vehicleSalesBilling Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }
}

@Component({
    selector: 'jhi-vehicle-sales-billing-popup',
    template: ''
})
export class VehicleSalesBillingPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vehicleSalesBillingPopupService: VehicleSalesBillingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vehicleSalesBillingPopupService
                    .open(VehicleSalesBillingDialogComponent as Component, params['id']);
            } else {
                this.vehicleSalesBillingPopupService
                    .open(VehicleSalesBillingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
