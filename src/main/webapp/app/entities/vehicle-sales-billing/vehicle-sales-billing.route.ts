import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { VehicleSalesBillingComponent } from './vehicle-sales-billing.component';
import { VehicleSalesBillingEditComponent } from './vehicle-sales-billing-edit.component';
import { VehicleSalesBillingPopupComponent } from './vehicle-sales-billing-dialog.component';
import { VehicleSalesBillingLovPopupComponent } from './vehicle-sales-billing-as-lov.component';

@Injectable()
export class VehicleSalesBillingResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        // const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'asc';
        return {
            page: this.paginationUtil.parsePage(page),
            // predicate: this.paginationUtil.parsePredicate(sort),
            // ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const vehicleSalesBillingRoute: Routes = [
    {
        path: 'vehicle-sales-billing',
        component: VehicleSalesBillingComponent,
        resolve: {
            'pagingParams': VehicleSalesBillingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesBilling.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const vehicleSalesBillingPopupRoute: Routes = [
    {
        path: 'vehicle-sales-billing-lov',
        component: VehicleSalesBillingLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesBilling.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'vehicle-sales-billing-new',
        component: VehicleSalesBillingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesBilling.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vehicle-sales-billing/:id/edit',
        component: VehicleSalesBillingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesBilling.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vehicle-sales-billing-popup-new',
        component: VehicleSalesBillingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesBilling.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'vehicle-sales-billing/:id/popup-edit',
        component: VehicleSalesBillingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesBilling.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
