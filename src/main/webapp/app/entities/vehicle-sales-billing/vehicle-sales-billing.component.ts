import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleSalesBilling } from './vehicle-sales-billing.model';
import { VehicleSalesBillingService } from './vehicle-sales-billing.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../shared';
import { OrdersParameter } from '../orders/orders-parameter.model'
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import { VehicleSalesOrder } from '../vehicle-sales-order/vehicle-sales-order.model';
import { VehicleSalesOrderService } from '../vehicle-sales-order/vehicle-sales-order.service';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-vehicle-sales-billing',
    templateUrl: './vehicle-sales-billing.component.html'
})
export class VehicleSalesBillingComponent implements OnInit  {

    refreshVSO: boolean;
    refreshVSB: boolean;
    vehicleSalesBilling: VehicleSalesBilling;
    vsbParameter: OrdersParameter;
    vsoParameter: OrdersParameter;
    currentSearch: string;
    itemsPerPage: any;
    routeData: any;
    queryCount: any;
    totalItems: any;
    page: any;
    links: any;
    previousPage: any;
    reverse: any;
    predicate: any;

    public yearForCalendar: string
    public vsbDateFrom: Date;
    public vsbDateThru: Date;
    public vsoDateFrom: Date;
    public vsoDateThru: Date;

    constructor(
        private vehicleSalesBillingService: VehicleSalesBillingService,
        private loadingService: LoadingService,
        private commonUtilService: CommonUtilService,
        private activatedRoute: ActivatedRoute,
        private alertService: JhiAlertService,
        private principal: Principal,
        private parseLinks: JhiParseLinks,
        private router: Router
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.yearForCalendar = commonUtilService.setYearRangeForCalendar(0, 70);
        this.vsbDateFrom = new Date();
        this.vsbDateThru = new Date();
        this.vsoDateFrom = new Date();
        this.vsoDateThru = new Date();

        this.vsbParameter = new OrdersParameter();
        this.vsoParameter = new OrdersParameter();
    }

    search(query) {}

    ngOnInit() {
        // this.loadingService.loadingStart();
        this.buildComponentChild(1);
        this.buildComponentChild(2);
        // this.loadingService.loadingStop();
    }

    private destroyComponentChild(param: Number): Promise<void> {
        return new Promise<void> (
            (resolve) => {
                if (param === 1) { this.refreshVSO = false; } else
                if (param === 2) { this.refreshVSB = false; }
                resolve();
            }
        )
    }

    trackId(index: number, item: VehicleSalesBilling) {
        return item.idBilling;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idBilling') {
            result.push('idBilling');
        }
        return result;
    }

    public filterVSB(): void {
        this.vsbParameter = new OrdersParameter();
        this.vsbParameter.internalId = this.principal.getIdInternal();
        this.vsbDateFrom.setHours(0, 0, 0, 0);
        this.vsbDateThru.setHours(23, 59, 59, 99);
        this.vsbParameter.orderDateFrom = this.vsbDateFrom.toISOString();
        this.vsbParameter.orderDateThru = this.vsbDateThru.toISOString();
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.vehicleSalesBilling = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
    private buildComponentChild(param: Number): void {
        if (param === 1) { this.refreshVSO = true; } else
        if (param === 2) { this.refreshVSB = true; }
    }

    public doRefresh(result, param: Number): void {
        // this.loadingService.loadingStart();
        this.destroyComponentChild(param).then(
            (res) => {
                setTimeout(
                    () => {
                        this.buildComponentChild(param);
                }, 1000);
            }
        );
        // this.loadingService.loadingStop();
    }

}

// implements OnInit, OnDestroy implements OnInit, OnDestroy
/*
    currentAccount: any;
    vehicleSalesBillings: VehicleSalesBilling[];
    vehicleSalesOrders: VehicleSalesOrder[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        private vehicleSalesBillingService: VehicleSalesBillingService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private vehicleSalesOrderService: VehicleSalesOrderService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    generateVsb(idOrder) {
        console.log('Generate VSB');
        this.vehicleSalesOrderService.find(idOrder)
            .subscribe((vehicleSalesOrder) => {

                this.vehicleSalesOrderService
                    .executeProcess(74, null, vehicleSalesOrder)
                    .subscribe((res) => {
                        console.log('approve');
                    })
        });
        // this.vehicleSalesOrderService.executeProcess()
    }

    loadAll() {
        if (this.currentSearch) {
            this.vehicleSalesBillingService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );

            return;
        }
        this.vehicleSalesBillingService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/vehicle-sales-billing'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/vehicle-sales-billing', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/vehicle-sales-billing', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInVehicleSalesBillings();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VehicleSalesBilling) {
        return item.idBilling;
    }

    registerChangeInVehicleSalesBillings() {
        this.eventSubscriber = this.eventManager.subscribe('vehicleSalesBillingListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idBilling') {
            result.push('idBilling');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.vehicleSalesBillings = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.vehicleSalesBillingService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        console.log('load lazy dunkk from vsb-->');
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleSalesBillingService.update(event.data)
                .subscribe((res: VehicleSalesBilling) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleSalesBillingService.create(event.data)
                .subscribe((res: VehicleSalesBilling) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: VehicleSalesBilling) {
        this.toasterService.showToaster('info', 'VehicleSalesBilling Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleSalesBillingService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'vehicleSalesBillingListModification',
                    content: 'Deleted an vehicleSalesBilling'
                    });
                });
            }
        });
    }
}
*/
