import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';

import {
    VehicleSalesBillingService,
    VehicleSalesBillingPopupService,
    VehicleSalesBillingComponent,
    VehicleSalesBillingDialogComponent,
    VehicleSalesBillingPopupComponent,
    vehicleSalesBillingRoute,
    vehicleSalesBillingPopupRoute,
    VehicleSalesBillingResolvePagingParams,
    VehicleSalesBillingEditComponent,
    VehicleSalesBillingListComponent,
    VehicleSalesBillingLovPopupComponent,
    VehicleSalesBillingAsLovComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule
        } from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...vehicleSalesBillingRoute,
    ...vehicleSalesBillingPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        MpmSharedEntityModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
    ],
    exports: [
        VehicleSalesBillingComponent,
        VehicleSalesBillingEditComponent,
        VehicleSalesBillingListComponent,
        VehicleSalesBillingLovPopupComponent,
        VehicleSalesBillingAsLovComponent,
    ],
    declarations: [
        VehicleSalesBillingComponent,
        VehicleSalesBillingDialogComponent,
        VehicleSalesBillingPopupComponent,
        VehicleSalesBillingEditComponent,
        VehicleSalesBillingListComponent,
        VehicleSalesBillingLovPopupComponent,
        VehicleSalesBillingAsLovComponent,
    ],
    entryComponents: [
        VehicleSalesBillingComponent,
        VehicleSalesBillingDialogComponent,
        VehicleSalesBillingPopupComponent,
        VehicleSalesBillingEditComponent,
        VehicleSalesBillingListComponent,
        VehicleSalesBillingLovPopupComponent,
        VehicleSalesBillingAsLovComponent,
    ],
    providers: [
        VehicleSalesBillingService,
        VehicleSalesBillingPopupService,
        VehicleSalesBillingResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmVehicleSalesBillingModule {}
