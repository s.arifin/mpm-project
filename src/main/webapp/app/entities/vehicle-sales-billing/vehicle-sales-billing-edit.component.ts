import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { VehicleSalesBilling } from './vehicle-sales-billing.model';
import { VehicleSalesBillingService } from './vehicle-sales-billing.service';
import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { Customer, CustomerService } from '../customer';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-vehicle-sales-billing-edit',
    templateUrl: './vehicle-sales-billing-edit.component.html'
})
export class VehicleSalesBillingEditComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    vehicleSalesBilling: VehicleSalesBilling;
    isSaving: boolean;

    internals: Internal[];

    billtos: BillTo[];

    customers: Customer[];

    constructor(
        private alertService: JhiAlertService,
        private vehicleSalesBillingService: VehicleSalesBillingService,
        private internalService: InternalService,
        private billToService: BillToService,
        private customerService: CustomerService,
        private route: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
        this.vehicleSalesBilling = new VehicleSalesBilling();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.vehicleSalesBillingService.find(id).subscribe((vehicleSalesBilling) => {
            this.vehicleSalesBilling = vehicleSalesBilling;
        });
    }

    previousState() {
        this.router.navigate(['vehicle-sales-billing']);
    }

    save() {
        this.isSaving = true;
        if (this.vehicleSalesBilling.idBilling !== undefined) {
            this.subscribeToSaveResponse(
                this.vehicleSalesBillingService.update(this.vehicleSalesBilling));
        } else {
            this.subscribeToSaveResponse(
                this.vehicleSalesBillingService.create(this.vehicleSalesBilling));
        }
    }

    private subscribeToSaveResponse(result: Observable<VehicleSalesBilling>) {
        result.subscribe((res: VehicleSalesBilling) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: VehicleSalesBilling) {
        this.eventManager.broadcast({ name: 'vehicleSalesBillingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vehicleSalesBilling saved !');
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'vehicleSalesBilling Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }
}
