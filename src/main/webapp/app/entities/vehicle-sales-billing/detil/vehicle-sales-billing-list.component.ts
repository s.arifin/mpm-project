import { Component, OnInit, OnDestroy, OnChanges, EventEmitter, Output, Input, SimpleChanges} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';

import { LazyLoadEvent, SelectItem } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import { VehicleSalesBilling } from '../vehicle-sales-billing.model';
import { VehicleSalesBillingService } from '../vehicle-sales-billing.service';
import { OrdersParameter } from '../../orders/orders-parameter.model'

import { VehicleSalesOrderService } from '../../vehicle-sales-order/vehicle-sales-order.service';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import { LoadingService } from '../../../layouts/index';
import { Billing } from '../../billing/billing.model'
import { OrderBillingItem, OrderBillingItemService } from '../../order-billing-item';
import { BillingItem, OrderItem, OrdersService, BillingItemService, OrderItemService, BillingService } from '../../shared-component';
import { VehicleSalesOrder } from '../../vehicle-sales-order';

// nuse
import { AxPosting, AxPostingService } from '../../axposting';
import { AxPostingLine } from '../../axposting-line';
import { BillingDetailC, BillingDetailCService } from '../../billing-detail-c';
import { DatePipe } from '@angular/common';
import { SalesmanService } from '../../salesman/salesman.service';
import { Salesman } from '../../salesman/salesman.model';
import * as BaseConstant from '../../../shared/constants/base.constants';
import { LeasingCompanyService } from '../../leasing-company/leasing-company.service';

@Component({
    selector: 'jhi-vehicle-sales-billing-list',
    templateUrl: './vehicle-sales-billing-list.component.html'
})
export class VehicleSalesBillingListComponent implements OnInit, OnDestroy, OnChanges {

    @Output()
    fnCallback = new EventEmitter();

    @Input()
    vsbParameter: OrdersParameter = new OrdersParameter();

    currentAccount: any;
    vehicleSalesBillings: VehicleSalesBilling[];
    billing: Billing;
    vsb: VehicleSalesBilling;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    isFiltered: boolean;
    noIvu: string;
    idBillingItem: any;
    orderBillingItem: OrderBillingItem[];
    billingItem: BillingItem[];
    orderItem: OrderItem[];
    idOrderItem: any;
    vso: VehicleSalesOrder[];
    noVso: any;
    ivu: any;
    selectedKorsal: string;
    salesmans: Salesman[];
    korsals: Salesman[];
    selectedSalesman: string;
    selectedLeasing: string;
    leasings: SelectItem[];
    // nuse
    billingDetail: BillingDetailC;
    axPosting: AxPosting;
    axPostingLine: AxPostingLine;
    VendorNum: string;

    public yearForCalendar: string
    public vsbDateFrom: Date;
    public vsbDateThru: Date;
    public vsoDateFrom: Date;
    public vsoDateThru: Date;

    constructor(
        private vehicleSalesBillingService: VehicleSalesBillingService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private reportUtilService: ReportUtilService,
        private loadingService: LoadingService,
        private orderBillingItemService: OrderBillingItemService,
        private orderService: OrdersService,
        private billingItemService: BillingItemService,
        private orderItemService: OrderItemService,
        private billingService: BillingService,
        // nuse
        private billingDetailCService: BillingDetailCService,
        private axPostingService: AxPostingService,
        private datePipe: DatePipe,
        private commonUtilService: CommonUtilService,
        private salesmanService: SalesmanService,
        private leasingCompanyService: LeasingCompanyService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.isFiltered = false;
        this.billingItem = new Array<BillingItem>();
        this.vsb = new VehicleSalesBilling();
        this.billing = new Billing();
        this.vsb.printCounter = 0;
        this.yearForCalendar = commonUtilService.setYearRangeForCalendar(0, 70);
        this.vsbDateFrom = new Date();
        this.vsbDateThru = new Date();
        this.vsoDateFrom = new Date();
        this.vsoDateThru = new Date();
        this.salesmans = new Array<Salesman>();
        this.korsals = new Array<Salesman>();
        this.leasings = [];
        this.vsbParameter = new OrdersParameter();
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            console.log('SEARCH');
            this.vehicleSalesBillingService.queryFilterBy({
                queryFor: 'search',
                dataParam: this.currentSearch,
            }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => this.loadingService.loadingStop(),
                );

            return;
        }
        if (this.isFiltered) {
            console.log('FILTER');
            this.vehicleSalesBillingService.queryFilterByPTO({
                ordersPTO: this.vsbParameter,
                page: this.page - 1,
                size: this.itemsPerPage,
                // sort: this.sort()
            })
                .toPromise()
                .then((res) => {
                    this.loadingService.loadingStop(),
                    this.onSuccess(res.json, res.headers)
                });

            return;
        }
        this.vehicleSalesBillingService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            // sort: this.sort()
            }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json),
            () => this.loadingService.loadingStop(),
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.loadAll();
    }

    loadLeasing() {
        this.leasingCompanyService.query({
            page: 0,
            size: 100}).subscribe(
            (res: ResponseWrapper) => this.leasings = res.json,
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['vsbParameter']) {
            console.log('PARAMETER VSB', this.vsbParameter)
            if (this.vsbParameter.orderDateFrom !== null &&
                this.vsbParameter.orderDateThru !== null) {
                    this.isFiltered = true;
                    this.currentSearch = null;
                    this.loadingService.loadingStart();
                    this.router.navigate(['/vehicle-sales-billing', { page: this.page }]);
                    this.loadAll();
            }
        }
    }

    cancelVsb(vsb) {
        console.log('Cancel VSB', vsb);
        this.confirmationService.confirm({
            message: 'Are you sure that you want to cancel Billing ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleSalesBillingService
                .executeProcess( 13, null, vsb)
                .subscribe((res) => {
                    this.billingDetailCService.findbyBillingNumber(vsb.billingNumber).subscribe(
                        (resBilDetail) => {
                            this.billingDetail = resBilDetail;
                            console.log('billing Detail : ', this.billingDetail);
                            const today = new Date();
                            const myaxPostingLine = [];

                            this.axPosting = new AxPosting();

                            this.axPosting.AutoPosting = 'FALSE';
                            this.axPosting.AutoSettlement = 'FALSE';
                            this.axPosting.DataArea = this.billingDetail.idMSO;
                            this.axPosting.Guid = this.billingDetail.idOrdIte;
                            this.axPosting.JournalName = 'VSO-CM';
                            this.axPosting.Name = 'Cancel Sales Unit ' + this.billingDetail.idCustomer + ' ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.noKa;
                            this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPosting.TransactionType = 'Unit';

                            // credit
                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = this.billingDetail.idCustomer;
                            this.axPostingLine.AccountType = 'Cust';
                            if ( this.billingDetail.finComRefCode !== '' && this.billingDetail.finComRefCode != null ) {
                                this.axPostingLine.AmountCurCredit = this.billingDetail.creditDP.toString();
                            } else {
                                this.axPostingLine.AmountCurCredit = this.billingDetail.arAmount.toString();
                            }
                            this.axPostingLine.AmountCurDebit = '0';
                            this.axPostingLine.Company = this.billingDetail.idMSO;
                            this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                            this.axPostingLine.Description = 'AR '  + this.billingDetail.orderNumber + ' ' + this.billingDetail.idCustomer + ' '  + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = '000';
                            this.axPostingLine.Dimension5 = '00';
                            this.axPostingLine.Dimension6 = '000';
                            this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                            this.axPostingLine.Payment = '';

                            myaxPostingLine.push(this.axPostingLine);

                            if ( this.billingDetail.finComRefCode !== '' && this.billingDetail.finComRefCode != null ) {
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = this.billingDetail.finComParty;
                                this.axPostingLine.AccountType = 'Cust';
                                this.axPostingLine.AmountCurDebit = '0';
                                this.axPostingLine.AmountCurCredit = this.billingDetail.arAmount.toString();
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                                this.axPostingLine.Description = 'AR' + this.billingDetail.orderNumber + ' ' + this.billingDetail.idCustomer + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLine.push(this.axPostingLine);
                            }

                            if (this.billingDetail.discount > 0) {
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = '421001';
                                this.axPostingLine.AccountType = 'Ledger';
                                this.axPostingLine.AmountCurDebit = '0';
                                this.axPostingLine.AmountCurCredit = this.billingDetail.discount.toString();
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                                this.axPostingLine.Description = 'DISC ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.idCustomer + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = this.billingDetail.CategoryType;
                                this.axPostingLine.Dimension5 = this.billingDetail.SegmentType;
                                this.axPostingLine.Dimension6 = this.billingDetail.TypeCode;
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLine.push(this.axPostingLine);
                            }

                            // debit
                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = '411001';
                            this.axPostingLine.AccountType = 'Ledger';
                            this.axPostingLine.AmountCurDebit = this.billingDetail.axUnitPrice.toString();
                            this.axPostingLine.AmountCurCredit = '0';
                            this.axPostingLine.Company = this.billingDetail.idMSO;
                            this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                            this.axPostingLine.Description = 'Sales Unit ' + this.billingDetail.idCustomer + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa + ' ' + this.billingDetail.orderNumber;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = this.billingDetail.CategoryType;
                            this.axPostingLine.Dimension5 = this.billingDetail.SegmentType;
                            this.axPostingLine.Dimension6 = this.billingDetail.TypeCode;
                            this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                            this.axPostingLine.Payment = '';

                            myaxPostingLine.push(this.axPostingLine);

                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = '218901';
                            this.axPostingLine.AccountType = 'Ledger';
                            this.axPostingLine.AmountCurDebit = this.billingDetail.axPPN.toString();
                            this.axPostingLine.AmountCurCredit = '0';
                            this.axPostingLine.Company = this.billingDetail.idMSO;
                            this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                            this.axPostingLine.Description = 'Vehicle Tax Out ' + this.billingDetail.idCustomer + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa + ' ' + this.billingDetail.orderNumber;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = '000';
                            this.axPostingLine.Dimension5 = '00';
                            this.axPostingLine.Dimension6 = '000';
                            this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                            this.axPostingLine.Payment = '';

                            myaxPostingLine.push(this.axPostingLine);

                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = this.billingDetail.vendorCode;
                            this.axPostingLine.AccountType = 'Vend';
                            this.axPostingLine.AmountCurDebit = this.billingDetail.axBBN.toString();
                            this.axPostingLine.AmountCurCredit = '0';
                            this.axPostingLine.Company = this.billingDetail.idMSO;
                            this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                            this.axPostingLine.Description = 'Cadangan BBN ' + this.billingDetail.idCustomer + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa + ' ' + this.billingDetail.orderNumber;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = '000';
                            this.axPostingLine.Dimension5 = '00';
                            this.axPostingLine.Dimension6 = '000';
                            this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                            this.axPostingLine.Payment = '';

                            myaxPostingLine.push(this.axPostingLine);

                            this.axPosting.LedgerJournalLine = myaxPostingLine;
                            console.log('axposting : ', this.axPosting);
                            this.axPostingService.send(this.axPosting).subscribe(
                                (resaxPosting: ResponseWrapper) => {
                                    console.log('Success : ', resaxPosting.json.Message);
                                },
                                (resaxPosting: ResponseWrapper) => {
                                    this.onError(resaxPosting.json);
                                    console.log('error ax posting : ', resaxPosting.json.Message);
                                }
                            );

                            const myaxPostingLineOther = [];
                            this.axPosting = new AxPosting();

                            this.axPosting.AutoPosting = 'FALSE';
                            this.axPosting.AutoSettlement = 'FALSE';
                            this.axPosting.DataArea = this.billingDetail.idMSO;
                            this.axPosting.Guid = this.billingDetail.idOrdIte;
                            this.axPosting.JournalName = 'VSO-InvReturn';
                            this.axPosting.Name = 'Cancel Issue for ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa;
                            this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPosting.TransactionType = 'Unit';

                            // debit
                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = '114101';
                            this.axPostingLine.AccountType = 'Ledger';
                            this.axPostingLine.AmountCurDebit = this.billingDetail.BasePrice.toString();
                            this.axPostingLine.AmountCurCredit = '0';
                            this.axPostingLine.Company = this.billingDetail.idMSO;
                            this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                            this.axPostingLine.Description = 'Issue for ' + this.billingDetail.idCustomer + ' '  + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa + ' ' + this.billingDetail.orderNumber;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = this.billingDetail.CategoryType;
                            this.axPostingLine.Dimension5 = this.billingDetail.SegmentType;
                            this.axPostingLine.Dimension6 = this.billingDetail.TypeCode;
                            this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                            this.axPostingLine.Payment = '';

                            myaxPostingLineOther.push(this.axPostingLine);

                            // credit
                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = '511001';
                            this.axPostingLine.AccountType = 'Ledger';
                            this.axPostingLine.AmountCurDebit = '0';
                            this.axPostingLine.AmountCurCredit = this.billingDetail.BasePrice.toString();
                            this.axPostingLine.Company = this.billingDetail.idMSO;
                            this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                            this.axPostingLine.Description = 'Issue for ' + this.billingDetail.idCustomer + ' '  + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa + ' ' + this.billingDetail.orderNumber;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = this.billingDetail.CategoryType;
                            this.axPostingLine.Dimension5 = this.billingDetail.SegmentType;
                            this.axPostingLine.Dimension6 = this.billingDetail.TypeCode;
                            this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                            this.axPostingLine.Payment = '';

                            myaxPostingLineOther.push(this.axPostingLine);

                            this.axPosting.LedgerJournalLine = myaxPostingLineOther;
                            console.log('axpostingOther : ', this.axPosting);
                            this.axPostingService.send(this.axPosting).subscribe(
                                (resaxPostingNew: ResponseWrapper) => {
                                    console.log('Success : ', resaxPostingNew.json.Message);
                                },
                                (resaxPostingNew: ResponseWrapper) => {
                                    this.onError(resaxPostingNew.json);
                                    console.log('error ax posting : ', resaxPostingNew.json.Message);
                                }
                            );

                            const myaxPostingLineOther2 = [];
                            this.axPosting = new AxPosting();

                            if (this.billingDetail.arTitipan > 0) {
                                this.axPosting.AutoPosting = 'FALSE';
                                this.axPosting.AutoSettlement = 'FALSE';
                                this.axPosting.DataArea = this.billingDetail.idMSO;
                                this.axPosting.Guid = this.billingDetail.idOrdIte;
                                this.axPosting.JournalName = 'AR-PayUnitRev';
                                this.axPosting.Name = 'Pelunasan AR Unit ' + this.billingDetail.idCustomer + ' ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.noKa;
                                this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPosting.TransactionType = 'Unit';

                                // credit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = this.billingDetail.idCustomer;
                                this.axPostingLine.AccountType = 'Cust';
                                this.axPostingLine.AmountCurDebit = this.billingDetail.arTitipan.toString();
                                this.axPostingLine.AmountCurCredit = '0';
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                                this.axPostingLine.Description = 'Pelunasan AR Unit ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.idCustomer;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther2.push(this.axPostingLine);

                                // debit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = this.billingDetail.bankCashAccNum;
                                this.axPostingLine.AccountType = 'Bank';
                                this.axPostingLine.AmountCurCredit = this.billingDetail.arTitipan.toString();
                                this.axPostingLine.AmountCurDebit = '0';
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = null;
                                this.axPostingLine.Description = 'Pelunasan AR Unit ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.idCustomer;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther2.push(this.axPostingLine);

                                this.axPosting.LedgerJournalLine = myaxPostingLineOther2;
                                console.log('axpostingOther : ', this.axPosting);
                                this.axPostingService.send(this.axPosting).subscribe(
                                    (resaxPostingNew: ResponseWrapper) => {
                                        console.log('Success : ', resaxPostingNew.json.Message);
                                    },
                                    (resaxPostingNew: ResponseWrapper) => {
                                        this.onError(resaxPostingNew.json);
                                        console.log('error ax posting : ', resaxPostingNew.json.Message);
                                    }
                                );
                            }

                            if (this.billingDetail.totalTitipan > 0) {

                                const myaxPostingLineOther4 = [];
                                this.axPosting = new AxPosting();

                                this.axPosting = new AxPosting();
                                this.axPosting.AutoPosting = 'FALSE';
                                this.axPosting.AutoSettlement = 'FALSE';
                                this.axPosting.DataArea = this.billingDetail.idMSO;
                                this.axPosting.Guid = '';
                                this.axPosting.JournalName = 'titipan-AR-UnitRev';
                                this.axPosting.Name = 'Pengembalian Uang Titipan ' + this.billingDetail.idCustomer + ' ' + this.billingDetail.orderNumber;
                                this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPosting.TransactionType = 'Unit';

                                // credit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = this.billingDetail.idCustomer;
                                this.axPostingLine.AccountType = 'Cust';
                                this.axPostingLine.AmountCurDebit = this.billingDetail.totalTitipan.toString();
                                this.axPostingLine.AmountCurCredit = '0';
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                                this.axPostingLine.Description = 'Pengembalian Uang Titipan ' + this.billingDetail.idCustomer  + ' ' + this.billingDetail.orderNumber;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = '';
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther4.push(this.axPostingLine);

                                // debit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = this.billingDetail.bankCashAccNum;
                                this.axPostingLine.AccountType = 'Bank';
                                this.axPostingLine.AmountCurDebit = '0';
                                this.axPostingLine.AmountCurCredit = this.billingDetail.totalTitipan.toString();
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = null;
                                this.axPostingLine.Description = 'Pengembalian Uang Titipan ' + this.billingDetail.idCustomer  + ' ' + this.billingDetail.orderNumber;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = '';
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther4.push(this.axPostingLine);

                                this.axPosting.LedgerJournalLine = myaxPostingLineOther4;
                                console.log('axPosting data :', this.axPosting);
                                this.axPostingService.send(this.axPosting).subscribe(
                                    (resaxPosting: ResponseWrapper) =>
                                        console.log('Success : ', resaxPosting.json.Message),
                                    (resaxPosting: ResponseWrapper) => {
                                        this.onError(resaxPosting.json);
                                        console.log('error ax posting : ', resaxPosting.json.Message);
                                    }
                                );
                            }

                            if ( this.billingDetail.finComRefCode !== '' && this.billingDetail.finComRefCode != null ) {
                                const myaxPostingLineOther3 = [];
                                this.axPosting = new AxPosting();

                                this.axPosting.AutoPosting = 'FALSE';
                                this.axPosting.AutoSettlement = 'FALSE';
                                this.axPosting.DataArea = this.billingDetail.idMSO;
                                this.axPosting.Guid = this.billingDetail.idOrdIte;
                                this.axPosting.JournalName = 'AR-CM';
                                this.axPosting.Name = 'Cancel Sales Unit ' + this.billingDetail.idCustomer + ' ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.noKa;
                                this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPosting.TransactionType = 'Unit';

                                // credit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = this.billingDetail.finComRefCode;
                                this.axPostingLine.AccountType = 'Cust';
                                this.axPostingLine.AmountCurDebit = '0';
                                this.axPostingLine.AmountCurCredit = this.billingDetail.totalFinCom.toString();
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                                this.axPostingLine.Description = 'Customer Refund Leasing ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.idCustomer + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther3.push(this.axPostingLine);

                                // debit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = '411004';
                                this.axPostingLine.AccountType = 'Ledger';
                                this.axPostingLine.AmountCurDebit = this.billingDetail.subsFinCom.toString();
                                this.axPostingLine.AmountCurCredit = '0';
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = null;
                                this.axPostingLine.Description = 'Revenue from Fincoy ' + this.billingDetail.idCustomer + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa + ' ' + this.billingDetail.orderNumber;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther3.push(this.axPostingLine);

                                // debit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = '218905';
                                this.axPostingLine.AccountType = 'Ledger';
                                this.axPostingLine.AmountCurDebit = this.billingDetail.ppnFinCom.toString();
                                this.axPostingLine.AmountCurCredit = '0';
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = null;
                                this.axPostingLine.Description = 'PPN Keluaran ' + this.billingDetail.idCustomer + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa + ' ' + this.billingDetail.orderNumber;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther3.push(this.axPostingLine);

                                this.axPosting.LedgerJournalLine = myaxPostingLineOther3;
                                console.log('axpostingOther : ', this.axPosting);
                                this.axPostingService.send(this.axPosting).subscribe(
                                    (resaxPostingNew: ResponseWrapper) => {
                                        console.log('Success : ', resaxPostingNew.json.Message);
                                    },
                                    (resaxPostingNew: ResponseWrapper) => {
                                        this.onError(resaxPostingNew.json);
                                        console.log('error ax posting : ', resaxPostingNew.json.Message);
                                    }
                                );
                            }

                            if (this.billingDetail.finComRefCode !== '' && this.billingDetail.finComRefCode !== null ) {
                                const myaxPostingLineOther5 = [];
                                this.axPosting = new AxPosting();

                                this.axPosting.AutoPosting = 'FALSE';
                                this.axPosting.AutoSettlement = 'FALSE';
                                this.axPosting.DataArea = this.billingDetail.idMSO;
                                this.axPosting.Guid = this.billingDetail.idOrdIte;
                                this.axPosting.JournalName = 'AR-PayUnitRev';
                                this.axPosting.Name = 'Pelunasan AR Unit ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.idCustomer + ' ' + this.billingDetail.noKa;
                                this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPosting.TransactionType = 'Unit';

                                // credit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = this.billingDetail.finComParty;
                                this.axPostingLine.AccountType = 'Cust';
                                this.axPostingLine.AmountCurDebit = this.billingDetail.arAmount.toString();
                                this.axPostingLine.AmountCurCredit = '0';
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                                this.axPostingLine.Description = 'Pelunasan AR Unit ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.idCustomer;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther5.push(this.axPostingLine);

                                // debit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = this.billingDetail.bankCashAccNum;
                                this.axPostingLine.AccountType = 'Bank';
                                this.axPostingLine.AmountCurDebit = '0';
                                this.axPostingLine.AmountCurCredit = this.billingDetail.arAmount.toString();
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = null;
                                this.axPostingLine.Description = 'Pelunasan AR Unit ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.idCustomer;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther5.push(this.axPostingLine);

                                this.axPosting.LedgerJournalLine = myaxPostingLineOther5;
                                console.log('axpostingOther : ', this.axPosting);
                                this.axPostingService.send(this.axPosting).subscribe(
                                    (resaxPostingNew: ResponseWrapper) => {
                                        console.log('Success : ', resaxPostingNew.json.Message);
                                    },
                                    (resaxPostingNew: ResponseWrapper) => {
                                        this.onError(resaxPostingNew.json);
                                        console.log('error ax posting : ', resaxPostingNew.json.Message);
                                    }
                                );
                            }
                        }
                    );
                    // this.toaster.showToaster('info', 'Data Proces', 'Cancel VSO');
                    console.log('finish');
                    this.transition();
                    // this.router.navigate(['../../vehicle-sales-order-sales']);
            });
            },
            reject: () => {

            }
        });
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/vehicle-sales-billing', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        this.isFiltered = false;
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        // this.router.navigate(['/vehicle-sales-billing', {
        //     search: this.currentSearch,
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInVehicleSalesBillings();
        this.loadKorsal();
        this.loadLeasing();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VehicleSalesBilling) {
        return item.idBilling;
    }

    registerChangeInVehicleSalesBillings() {
        this.eventSubscriber = this.eventManager.subscribe('vehicleSalesBillingListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'idBilling') {
            result.push('idBilling');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.vehicleSalesBillings = new Array<VehicleSalesBilling>();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.vehicleSalesBillings = data;
        // this.mappingVSONumber(data);
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.vehicleSalesBillingService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        console.log('load lazy dunkk from vsb-->');
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleSalesBillingService.update(event.data)
                .subscribe((res: VehicleSalesBilling) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleSalesBillingService.create(event.data)
                .subscribe((res: VehicleSalesBilling) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: VehicleSalesBilling) {
        this.toasterService.showToaster('info', 'VehicleSalesBilling Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    print(idBilling: string, data: VehicleSalesBilling) {
        this.vsb = data
        // const sum = this.vsb.printCounter + 1;
        //     this.vsb.printCounter = sum;
            this.vehicleSalesBillingService.executeProcess(15, null, data).subscribe(
            (res) => {
                if (data.typeId === 111 || data.typeId === 112 || data.typeId === 113 || data.typeId === 114) {
                    this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
                    this.reportUtilService.viewFile('/api/report/print_out_gc/custompdf', {noivu: idBilling, keyword: 'noivu'});
                } else {
                    this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
                    this.reportUtilService.viewFile('/api/report/print_out_ivu/custompdf', {noivu: idBilling, keyword: 'noivu'});
                }
                console.log('print counter ', data.printCounter);
            }
        );
    }

    printCounter(ivu: VehicleSalesBilling): Promise<any> {
        return new Promise (
            (resolve) => {
                const sum = ivu.printCounter + 1;
                ivu.printCounter = sum;
                this.vehicleSalesBillingService.executeProcess(15, null, ivu)
                .subscribe(
                    (res) => {
                        console.log('print counter ', ivu.printCounter);
                    }
                );
                resolve();
            }
        )

    }

    getDescription(currentStatus: number) {
        return this.vehicleSalesBillingService.getDescriptionStatus(currentStatus);
    }

    getDescTaxs(currentStatus: number) {
        return this.vehicleSalesBillingService.getDescTax(currentStatus);
    }

    requestTax(vehicleSalesBilling: VehicleSalesBilling) {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Akan Melakukan Request Faktur Pajak ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleSalesBillingService.executeProcess(16, null, vehicleSalesBilling)
                .subscribe(
                    (res) => {
                        console.log('request faktur', res);
                        this.loadAll();
                    }
                )
            }
            }
        )
    }

    public filterVSB(): void {
        this.page = 0;
        this.isFiltered = true;
        this.vsbParameter = new OrdersParameter();
        this.vehicleSalesBillings = new Array<VehicleSalesBilling>();
        this.vsbParameter.internalId = this.principal.getIdInternal();
        this.vsbDateFrom.setHours(0, 0, 0, 0);
        this.vsbDateThru.setHours(23, 59, 59, 99);
        this.vsbParameter.orderDateFrom = this.vsbDateFrom.toJSON();
        this.vsbParameter.orderDateThru = this.vsbDateThru.toJSON();
        if ( this.selectedLeasing != null && this.selectedLeasing !== undefined) {
            this.vsbParameter.leasingCompanyId = this.selectedLeasing;
            }

        if (this.selectedSalesman !== null && this.selectedSalesman !== undefined) {
            this.vsbParameter.salesmanId = this.selectedSalesman;
            }

        this.vehicleSalesBillingService.queryFilterByPTO({
            ordersPTO: this.vsbParameter,
            page: this.page,
            size: this.itemsPerPage,
            // sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => {this.onSuccess(res.json, res.headers)},
            (res: ResponseWrapper) => {this.onError(res.json)}
        )
    }

    private  loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES,
            size: 9999
        };
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('tarik data korsal', res.json);
                this.korsals = res.json;
            }
        )
    }

    public selectSalesByKorsal(): void {
        const obj: object = {
            idCoordinator : this.selectedKorsal
        }
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('selected Sales' , res.json);
                this.salesmans = res.json;
            }
        )
    }
}
