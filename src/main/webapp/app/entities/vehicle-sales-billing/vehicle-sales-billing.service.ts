import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { VehicleSalesBilling } from './vehicle-sales-billing.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';
import { createOrdersParameterOption } from '../orders/orders-parameter.util';

@Injectable()
export class VehicleSalesBillingService {
    private itemValues: VehicleSalesBilling[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = SERVER_API_URL + 'api/vehicle-sales-billings';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/vehicle-sales-billings';

    constructor(private http: Http) { }

    create(vehicleSalesBilling: VehicleSalesBilling): Observable<VehicleSalesBilling> {
        const copy = this.convert(vehicleSalesBilling);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(vehicleSalesBilling: VehicleSalesBilling): Observable<VehicleSalesBilling> {
        const copy = this.convert(vehicleSalesBilling);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<VehicleSalesBilling> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('noIvu', req.noIvu);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(vehicleSalesBilling: VehicleSalesBilling, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(vehicleSalesBilling);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterByPTO(req?: any): Observable<ResponseWrapper> {
        const options = createOrdersParameterOption(req);
        return this.http.get(this.resourceUrl + '/filterByPTO', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFakturFilterByPTO(req?: any): Observable<ResponseWrapper> {
        const options = createOrdersParameterOption(req);
        return this.http.get(this.resourceUrl + '/filterFakturByPTO', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, vehicleSalesBilling: VehicleSalesBilling): Observable<VehicleSalesBilling> {
        const copy = this.convert(vehicleSalesBilling);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, vehicleSalesBillings: VehicleSalesBilling[]): Observable<VehicleSalesBilling[]> {
        const copy = this.convertList(vehicleSalesBillings);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    queryTaxList(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('noIvu', req.noIvu);
        return this.http.get(this.resourceUrl + '/tax-list', options)
            .map((res: Response) => this.convertResponse(res));
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(vehicleSalesBilling: VehicleSalesBilling): VehicleSalesBilling {
        if (vehicleSalesBilling === null || vehicleSalesBilling === {}) {
            return {};
        }
        // const copy: VehicleSalesBilling = Object.assign({}, vehicleSalesBilling);
        const copy: VehicleSalesBilling = JSON.parse(JSON.stringify(vehicleSalesBilling));
        return copy;
    }

    private convertList(vehicleSalesBillings: VehicleSalesBilling[]): VehicleSalesBilling[] {
        const copy: VehicleSalesBilling[] = vehicleSalesBillings;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: VehicleSalesBilling[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    getDescriptionStatus(statusValue: Number): String {

        let hasil = null;

         switch (statusValue) {
            case 10 :
                hasil = 'COMPLETE';
                return hasil;
            case 11 :
                hasil = 'COMPLETE';
                return hasil;
            case 13 :
                hasil = 'CANCEL';
                return hasil;
            default :
                hasil = 'UNDIFINED STATUS ' + statusValue;
                return hasil;
            }
        }

    getDescTax(statusValue: Number): String {

        let hasil = null;

            switch (statusValue) {
            case 0 :
                hasil = 'OPEN';
                return hasil;
            case 1 :
                hasil = 'REQUEST FAKTUR';
                return hasil;
            case 2 :
                hasil = 'COMPLETE';
                return hasil;
            case 3 :
                hasil = 'REVISI';
                return hasil;
            default :
                hasil = 'UNDIFINED STATUS ' + statusValue;
                return hasil;
            }
        }

    findForMemo(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('noIvu', req.noIvu);
        return this.http.get(this.resourceUrl + '/findForMemo', options)
            .map((res: Response) => this.convertResponse(res));
    }
}
