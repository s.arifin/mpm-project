import { BaseEntity } from './../../shared';

export class VehicleSalesBilling implements BaseEntity {
    constructor(
        public id?: any,
        public idBilling?: any,
        public internalId?: any,
        public billToId?: any,
        public customerId?: any,
        public customerName?: any,
        public billingNumber?: any,
        public dateOrder?: any,
        public description?: any,
        public currentStatus?: any,
        public dateCreate?: any,
        public typeId?: any,
        public printCounter?: number,
    ) {
    }
}
