import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {CustomerType} from './customer-type.model';
import {CustomerTypePopupService} from './customer-type-popup.service';
import {CustomerTypeService} from './customer-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-customer-type-dialog',
    templateUrl: './customer-type-dialog.component.html'
})
export class CustomerTypeDialogComponent implements OnInit {

    customerType: CustomerType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected customerTypeService: CustomerTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.customerType.id !== undefined) {
            this.subscribeToSaveResponse(
                this.customerTypeService.update(this.customerType));
        } else {
            this.subscribeToSaveResponse(
                this.customerTypeService.create(this.customerType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<CustomerType>) {
        result.subscribe((res: CustomerType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: CustomerType) {
        this.eventManager.broadcast({ name: 'customerTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'customerType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'customerType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-customer-type-popup',
    template: ''
})
export class CustomerTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected customerTypePopupService: CustomerTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.customerTypePopupService
                    .open(CustomerTypeDialogComponent as Component, params['id']);
            } else {
                this.customerTypePopupService
                    .open(CustomerTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
