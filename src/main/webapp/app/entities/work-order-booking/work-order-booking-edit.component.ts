import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Response} from '@angular/http';

import { Subscription } from 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { WorkOrderBooking, WorkOrderBookingService } from './';
import { Motor, MotorService } from '../motor';
import { ProductCategory } from '../shared-component';
import { ProductService } from '../product';

import { ToasterService} from '../../shared';
import { PersonalCustomer, PersonalCustomerService } from '../shared-component';
import { VehicleIdentification, VehicleIdentificationService } from '../vehicle-identification';
import { BookingSlot, BookingSlotService } from '../booking-slot';
import { BookingType, BookingTypeService } from '../booking-type';
import { EventType, EventTypeService } from '../event-type';
import { ResponseWrapper } from '../../shared';
import { WeServiceTypeService } from '../we-service-type';
import { Feature, FeatureService } from '../feature';
import { Service, ServiceService } from '../service';

@Component({
    selector: 'jhi-work-order-booking-edit',
    templateUrl: './work-order-booking-edit.component.html'
})
export class WorkOrderBookingEditComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    private bookingSlotSubs: Subscription;
    private serviceSubs: Subscription;

    workOrderBooking: WorkOrderBooking;
    isSaving: boolean;

    motors: Motor[];

    personalcustomers: PersonalCustomer[];

    vehicleidentifications: VehicleIdentification[];

    bookingslots: BookingSlot[];

    bookingtypes: BookingType[];

    eventtypes: EventType[];

    colors: Feature[];

    motor: Motor;

    resumeBooking: boolean;

    constructor(
        private alertService: JhiAlertService,
        private workOrderBookingService: WorkOrderBookingService,
        private personalCustomerService: PersonalCustomerService,
        private vehicleIdentificationService: VehicleIdentificationService,
        private bookingSlotService: BookingSlotService,
        private bookingTypeService: BookingTypeService,
        private eventTypeService: EventTypeService,
        private route: ActivatedRoute,
        private eventManager: JhiEventManager,
        private toaster: ToasterService,
        private motorService: MotorService,
        private productService: ProductService,
        private weServiceTypeService: WeServiceTypeService,
        private colorService: FeatureService,
        private serviceService: ServiceService,
        private router: Router
    ) {
        this.resumeBooking = false;
        this.workOrderBooking = new WorkOrderBooking();
        this.motor = null;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });

        this.serviceSubs = this.serviceService.values.subscribe((value) => {
            this.appendServiceItem(value);
        });

        this.bookingSlotSubs = this.bookingSlotService.values.subscribe((value) => {
            if (value !== null || value !== undefined) {
                this.selectBookingSlot(value);
            }
        });

        this.isSaving = false;
        this.personalCustomerService.query()
            .subscribe((res: ResponseWrapper) => { this.personalcustomers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.vehicleIdentificationService.query()
            .subscribe((res: ResponseWrapper) => { this.vehicleidentifications = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.bookingSlotService.query()
            .subscribe((res: ResponseWrapper) => { this.bookingslots = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.bookingTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.bookingtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.eventTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.eventtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query()
            .subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.colorService.query()
            .subscribe((res: ResponseWrapper) => { this.colors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.serviceSubs.unsubscribe();
        this.bookingSlotSubs.unsubscribe();
    }

    load(id) {
        this.workOrderBookingService.find(id).subscribe((workOrderBooking) => {
            this.workOrderBooking = workOrderBooking;
        });
    }

    previousState() {
        this.router.navigate(['../work-order-booking']);
    }

    save() {
        this.isSaving = true;
        if (this.workOrderBooking.idBooking !== undefined) {
            this.subscribeToSaveResponse(
                this.workOrderBookingService.update(this.workOrderBooking));
        } else {
            this.subscribeToSaveResponse(
                this.workOrderBookingService.create(this.workOrderBooking));
        }
    }

    private subscribeToSaveResponse(result: Observable<WorkOrderBooking>) {
        result.subscribe((res: WorkOrderBooking) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: WorkOrderBooking) {
        this.eventManager.broadcast({ name: 'workOrderBookingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'workOrderBooking saved !');
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'workOrderBooking Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackPersonalCustomerById(index: number, item: PersonalCustomer) {
        return item.idCustomer;
    }

    trackVehicleIdentificationById(index: number, item: VehicleIdentification) {
        return item.idVehicleIdentification;
    }

    trackBookingSlotById(index: number, item: BookingSlot) {
        return item.idBookSlot;
    }

    trackBookingTypeById(index: number, item: BookingType) {
        return item.idBookingType;
    }

    trackEventTypeById(index: number, item: EventType) {
        return item.idEventType;
    }

    trackMotorTypeById(index: number, item: Motor) {
        if (item !== null) {
            return item.idProduct;
        }
        return null;
    }

    trackColorById(index: number, item: Feature) {
        return item.idFeature;
    }

    getMotorMarket(): string {
        return '';
    }

    appendServiceItem(item: any) {
        console.log('Append Item', item);
        // workOrderBooking
        // item.forEach(element => {
        //     const itemService = new WorkServiceRequirement();
        //     itemService.workEffortType = itenm.idWorkEffortType;
        //     this.workOrderBooking.services.push(itemService);
        // });
    }

    selectBookingSlot(item: BookingSlot[]) {
        if (item !== null || item !== undefined) {
            this.workOrderBooking.slotId = item[0].id;
        }
    }

}
