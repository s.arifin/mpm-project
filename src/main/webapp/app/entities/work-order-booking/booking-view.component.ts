import { Component, OnInit, OnDestroy, OnChanges, Input, Output, SimpleChanges, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

import { WorkOrderBooking } from './work-order-booking.model';
import { WorkOrderBookingService } from './work-order-booking.service';

@Component({
    selector: 'jhi-booking-view-item',
    templateUrl: 'booking-view.component.html'
})
export class WorkOrderBookingItemViewComponent  {

    @Input() item: WorkOrderBooking;
    @Output() process = new EventEmitter<WorkOrderBooking>();
    @Output() cancel = new EventEmitter<WorkOrderBooking>();
    @Output() close = new EventEmitter<WorkOrderBooking>();

    constructor() {
    }

    doProcess() {
        if (this.process != null) {
            this.process.emit(this.item);
        }
    }

    doCancel() {
        if (this.cancel != null) {
            this.cancel.emit(this.item);
        }
    }

    doClose() {
        if (this.close != null) {
            this.close.emit(this.item);
        }
    }

}
