import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { WorkOrderBooking } from './work-order-booking.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable()
export class WorkOrderBookingService {

    private resourceUrl = 'api/work-order-bookings';
    private resourceSearchUrl = 'api/_search/work-order-bookings';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(workOrderBooking: WorkOrderBooking): Observable<WorkOrderBooking> {
        const copy = this.convert(workOrderBooking);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(workOrderBooking: WorkOrderBooking): Observable<WorkOrderBooking> {
        const copy = this.convert(workOrderBooking);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<WorkOrderBooking> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(workOrderBooking: WorkOrderBooking, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(workOrderBooking);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, workOrderBooking: WorkOrderBooking): Observable<WorkOrderBooking> {
        const copy = this.convert(workOrderBooking);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, workOrderBookings: WorkOrderBooking[]): Observable<WorkOrderBooking[]> {
        const copy = this.convertList(workOrderBookings);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        if (entity.dateCreate) {
            entity.dateCreate = new Date(entity.dateCreate);
        }
    }

    private convert(workOrderBooking: WorkOrderBooking): WorkOrderBooking {
        if (workOrderBooking === null || workOrderBooking === {}) {
            return {};
        }
        const copy: WorkOrderBooking = JSON.parse(JSON.stringify(workOrderBooking));
        if (copy.vehicleNumber && copy.vehicleNumber !== null) {
            copy.vehicleNumber = copy.vehicleNumber.toUpperCase();
        }
        if (copy.customer.person.firstName) {
            copy.customer.person.firstName = copy.customer.person.firstName.toUpperCase();
        }
        if (copy.customer.person.lastName) {
            copy.customer.person.lastName = copy.customer.person.lastName.toUpperCase();
        }
        return copy;
    }

    private convertList(workOrderBookings: WorkOrderBooking[]): WorkOrderBooking[] {
        const copy = workOrderBookings;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

}
