import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { WorkOrderBookingComponent } from './work-order-booking.component';
import { WorkOrderBookingEditComponent } from './work-order-booking-edit.component';
import { WorkOrderBookingEmeregencyCallComponent } from './work-order-booking-emeregency-call.component';

@Injectable()
export class WorkOrderBookingResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'bookingNumber,desc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const workOrderBookingRoute: Routes = [
    {
        path: 'work-order-booking',
        component: WorkOrderBookingComponent,
        resolve: {
            'pagingParams': WorkOrderBookingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workOrderBooking.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const workOrderBookingPopupRoute: Routes = [
    {
        path: 'work-order-booking-new',
        component: WorkOrderBookingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workOrderBooking.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'work-order-booking/:id/edit',
        component: WorkOrderBookingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workOrderBooking.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'work-order-booking-emeregency-call',
        component: WorkOrderBookingEmeregencyCallComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workOrderBooking.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
