import { BaseEntity, PersonalCustomer } from './../shared-component';
import { Vehicle } from './../vehicle/vehicle.model';

export class WorkOrderBooking implements BaseEntity {
    constructor(
        public id?: any,
        public idBooking?: any,
        public bookingNumber?: string,
        public dateCreate?: Date,
        public slotId?: any,
        public bookingTypeId?: any,
        public currentStatus?: any,
        public eventTypeId?: any,
        public vehicleNumber?: String,
        public vehicleId?: any,
        public vehicle?: any,
        public customer?: any,
    ) {
        this.bookingTypeId = 10;
        this.eventTypeId = 10;
        this.dateCreate = new Date();
        this.customer = new PersonalCustomer();
        this.vehicle = new Vehicle()
    }
}
