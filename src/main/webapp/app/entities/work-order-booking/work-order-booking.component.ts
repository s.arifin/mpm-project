import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import {WorkOrderBooking} from './work-order-booking.model';
import {WorkOrderBookingService} from './work-order-booking.service';
import {ITEMS_PER_PAGE, Principal, ResponseWrapper} from '../../shared';
import {PaginationConfig} from '../../blocks/config/uib-pagination.config';

import {LazyLoadEvent} from 'primeng/primeng';
import {ToasterService} from '../../shared/alert/toaster.service';
import {ConfirmationService} from 'primeng/primeng';
import { VehicleIdentification, VehicleIdentificationService } from '../vehicle-identification';
import { Vehicle, VehicleService } from '../vehicle';
import { PersonalCustomer, PersonalCustomerService } from '../personal-customer';

import * as _ from 'lodash';

@Component({
    selector: 'jhi-work-order-booking',
    templateUrl: './work-order-booking.component.html'
})
export class WorkOrderBookingComponent implements OnInit, OnDestroy {

    currentAccount: any;
    workOrderBookings: WorkOrderBooking[];
    woBooking: WorkOrderBooking[];
    woFollowUp: WorkOrderBooking[];
    woEmergency: WorkOrderBooking[];
    woHome: WorkOrderBooking[];

    itemPerList: any;

    newBooking: Boolean = false;

    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    vehicleNumber: String = null;
    name: String = null;
    cellPhone: String = null;
    fullLookup: Boolean = false;

    constructor(
        private workOrderBookingService: WorkOrderBookingService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private vehicleIdService: VehicleIdentificationService,
        private vehicleService: VehicleService,
        private customerService: PersonalCustomerService
    ) {
        // karena 1 data list maksimum 10 item, ada 4 * data list
        // ToDo: Home dan Emergency di load berbeda dari regular, ada 3 x load dari database
        // atau di ambil semua, di pisah di client
        //      this.itemsPerPage = this.itemPerList * 10;
        this.itemPerList = 10;
        this.itemsPerPage = 20;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.workOrderBookingService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.workOrderBookingService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/work-order-booking'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/work-order-booking', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/work-order-booking', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInWorkOrderBookings();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: WorkOrderBooking) {
        return item.idBooking;
    }

    registerChangeInWorkOrderBookings() {
        this.eventSubscriber = this.eventManager.subscribe('workOrderBookingListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idBooking') {
            result.push('idBooking');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.workOrderBookings = data;
        this.woBooking = data.filter((o) => o.currentStatus !== 10 && o.bookingTypeId === 10);
        this.woFollowUp = data.filter((o) => o.currentStatus === 10 && o.bookingTypeId === 10);
        this.woHome = data.filter((o) => o.bookingTypeId === 11);
        this.woEmergency = data.filter((o) => o.bookingTypeId === 12);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.workOrderBookingService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.workOrderBookingService.update(event.data)
                .subscribe((res: WorkOrderBooking) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.workOrderBookingService.create(event.data)
                .subscribe((res: WorkOrderBooking) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: WorkOrderBooking) {
        this.toasterService.showToaster('info', 'WorkOrderBooking Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.workOrderBookingService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'workOrderBookingListModification',
                    content: 'Deleted an workOrderBooking'
                    });
                });
            }
        });
    }

    cancel(item: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to cancel?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.workOrderBookingService.changeStatus(item, 13).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'workOrderBookingListModification',
                    content: 'Deleted an workOrderBooking'
                    });
                });
            }
        });
    }

    approve(item: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to approved?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.workOrderBookingService.changeStatus(item, 11).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'workOrderBookingListModification',
                    content: 'Approved an workOrderBooking'
                    });
                });
            }
        });
    }

    clearOldData() {
        this.newBooking = false;
        this.fullLookup = false;
        this.vehicleNumber = null;
        this.name = null;
        this.cellPhone = null;
    }

    buildVehicleIdentification(o: WorkOrderBooking) {
        const v: VehicleIdentification  = new VehicleIdentification();
        v.vehicleId = o.vehicle.idVehicle;
        v.customerId = o.customer.idPartyRole;
        v.vehicleNumber = o.vehicleNumber.toUpperCase();
        this.vehicleIdService.create(v).subscribe(() => {

        });
    }

    doBuildBooking() {
        if (this.fullLookup === false) {
            // Jika no polisi di kenal
            this.vehicleIdService.executeProcess(101, this.vehicleNumber.toUpperCase(), {}).subscribe((vehicle) => {
                this.clearOldData();

                const booking = new WorkOrderBooking();
                booking.vehicleNumber = vehicle.vehicleNumber;

                // Build di server tidak client, mencegah call berulang ke server
                // 101 - Untuk membuat data Booking berdasarkan VehicleIdentification
                this.workOrderBookingService.executeProcess( 101, vehicle.vehicleNumber, null).subscribe((result) => {
                    this.router.navigate(['../work-order-booking/' + result.idBooking + '/edit']);
                });

                // Get Customer
                // this.customerService.find(vehicle.customerId).subscribe((customer) => {
                //     booking.customer = customer;

                //     //Get Vehicle Info
                //     this.vehicleService.find(vehicle.vehicleId).subscribe((vehicle) => {
                //         booking.vehicle = vehicle;

                //         // Buat booking dan buka halaman booking
                //         this.workOrderBookingService.create(booking).subscribe((result) => {
                //             this.router.navigate(['../work-order-booking/'+ result.idBooking + '/edit']);
                //         });

                //     });
                // });
            }, () => {
                // Jika data tidak di temukan, proses lengkap
                this.fullLookup = true;
            });
        } else {
            this.fullLookup = false;
            this.newBooking = false;

            const booking = new WorkOrderBooking();
            booking.vehicleNumber = this.vehicleNumber.toUpperCase();
            booking.customer.person.cellPhone1.number = this.cellPhone;

            if (this.name.split(' ').length === 1) {
                booking.customer.person.firstName = this.name.toUpperCase();
            } else {
                booking.customer.person.firstName = this.name.split(' ').slice(0, -1).join(' ').toUpperCase();
                booking.customer.person.lastName = this.name.split(' ').slice(-1).join(' ').toUpperCase();
            }

            this.workOrderBookingService.create(booking).subscribe((result) => {
                this.clearOldData();
                this.buildVehicleIdentification(result);
                this.router.navigate(['../work-order-booking/' + result.idBooking + '/edit']);
            });
        }
    }

    buildReindex() {
        this.workOrderBookingService.process({command: 'buildIndex'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }
}
