import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {
    BrokerTypeService,
} from './';

@NgModule({
    providers: [
        BrokerTypeService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmBrokerTypeModule {}
