import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { BrokerType } from './broker-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class BrokerTypeService {
    protected itemValues: BrokerType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/broker-types';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/broker-types';

    constructor(protected http: Http) { }

    create(brokerType: BrokerType): Observable<BrokerType> {
        const copy = this.convert(brokerType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(brokerType: BrokerType): Observable<BrokerType> {
        const copy = this.convert(brokerType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<BrokerType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, brokerType: BrokerType): Observable<BrokerType> {
        const copy = this.convert(brokerType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, brokerTypes: BrokerType[]): Observable<BrokerType[]> {
        const copy = this.convertList(brokerTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(brokerType: BrokerType): BrokerType {
        if (brokerType === null || brokerType === {}) {
            return {};
        }
        // const copy: BrokerType = Object.assign({}, brokerType);
        const copy: BrokerType = JSON.parse(JSON.stringify(brokerType));
        return copy;
    }

    protected convertList(brokerTypes: BrokerType[]): BrokerType[] {
        const copy: BrokerType[] = brokerTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: BrokerType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
