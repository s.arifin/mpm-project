import { BaseEntity } from './../../shared';

export class BrokerType implements BaseEntity {
    constructor(
        public id?: number,
        public idBrokerType?: number,
        public description?: string,
    ) {
    }
}
