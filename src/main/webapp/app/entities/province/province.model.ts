import { BaseEntity } from './../../shared';

export class Province implements BaseEntity {
    constructor(
        public id?: any,
        public idGeobou?: any,
        public description?: any,
        public geoCode?: any
    ) {
    }
}
