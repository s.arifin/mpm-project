import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { City } from './city.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class CityService {
    protected itemValues: City[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/cities';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/cities';
    private resourceCUrl = process.env.API_C_URL + '/api/cities';

    constructor(protected http: Http) { }

    create(city: City): Observable<City> {
        const copy = this.convert(city);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(city: City): Observable<City> {
        const copy = this.convert(city);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<City> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryByProvince(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-province', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getByProvince(id?: any): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrl + '/by-province/' + id)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, city: City): Observable<City> {
        const copy = this.convert(city);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, citys: City[]): Observable<City[]> {
        const copy = this.convertList(citys);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(city: City): City {
        if (city === null || city === {}) {
            return {};
        }
        // const copy: City = Object.assign({}, city);
        const copy: City = JSON.parse(JSON.stringify(city));
        return copy;
    }

    protected convertList(citys: City[]): City[] {
        const copy: City[] = citys;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: City[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
