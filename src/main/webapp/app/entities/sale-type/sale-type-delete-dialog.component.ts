import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SaleType } from './sale-type.model';
import { SaleTypePopupService } from './sale-type-popup.service';
import { SaleTypeService } from './sale-type.service';

@Component({
    selector: 'jhi-sale-type-delete-dialog',
    templateUrl: './sale-type-delete-dialog.component.html'
})
export class SaleTypeDeleteDialogComponent {

    saleType: SaleType;

    constructor(
        protected saleTypeService: SaleTypeService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.saleTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'saleTypeListModification',
                content: 'Deleted an saleType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sale-type-delete-popup',
    template: ''
})
export class SaleTypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected saleTypePopupService: SaleTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.saleTypePopupService
                .open(SaleTypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
