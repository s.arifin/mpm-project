import { BaseEntity } from './../../shared';

export class SaleType implements BaseEntity {
    constructor(
        public id?: number,
        public idSaleType?: number,
        public description?: string,
        public parentId?: any,
    ) {
    }
}
