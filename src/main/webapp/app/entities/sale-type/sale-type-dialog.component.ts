import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {SaleType} from './sale-type.model';
import {SaleTypePopupService} from './sale-type-popup.service';
import {SaleTypeService} from './sale-type.service';
import {ToasterService} from '../../shared';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-sale-type-dialog',
    templateUrl: './sale-type-dialog.component.html'
})
export class SaleTypeDialogComponent implements OnInit {

    saleType: SaleType;
    isSaving: boolean;

    saletypes: SaleType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected saleTypeService: SaleTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.saleTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.saletypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.saleType.idSaleType !== undefined) {
            this.subscribeToSaveResponse(
                this.saleTypeService.update(this.saleType));
        } else {
            this.subscribeToSaveResponse(
                this.saleTypeService.create(this.saleType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<SaleType>) {
        result.subscribe((res: SaleType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: SaleType) {
        this.eventManager.broadcast({ name: 'saleTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'saleType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'saleType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackSaleTypeById(index: number, item: SaleType) {
        return item.idSaleType;
    }
}

@Component({
    selector: 'jhi-sale-type-popup',
    template: ''
})
export class SaleTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected saleTypePopupService: SaleTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.saleTypePopupService
                    .open(SaleTypeDialogComponent as Component, params['id']);
            } else {
                this.saleTypePopupService
                    .open(SaleTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
