import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { SaleType } from './sale-type.model';
import { SaleTypeService } from './sale-type.service';

@Component({
    selector: 'jhi-sale-type-detail',
    templateUrl: './sale-type-detail.component.html'
})
export class SaleTypeDetailComponent implements OnInit, OnDestroy {

    saleType: SaleType;
    protected subscription: Subscription;
    protected eventSubscriber: Subscription;

    constructor(
        protected eventManager: JhiEventManager,
        protected saleTypeService: SaleTypeService,
        protected route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSaleTypes();
    }

    load(id) {
        this.saleTypeService.find(id).subscribe((saleType) => {
            this.saleType = saleType;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSaleTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'saleTypeListModification',
            (response) => this.load(this.saleType.id)
        );
    }
}
