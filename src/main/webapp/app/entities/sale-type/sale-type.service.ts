import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';
import { SaleType } from './sale-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as SaleTypeConstant from '../../shared/constants/sale-type.constants';
import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable()
export class SaleTypeService {
    protected itemValues: SaleType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/sale-types';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/sale-types';
    private resourceCUrl = process.env.API_C_URL + '/api/sale-type';
    private resourceCUrls = process.env.API_C_URL + '/api/sale-types';
    constructor(protected http: Http) { }

    public checkIfCash(constant: number): boolean {
        let a: boolean;
        a = false;

        if (constant === SaleTypeConstant.CASH_CBD || constant === SaleTypeConstant.CASH_COD || constant === SaleTypeConstant.CASH) {
            a = true;
        }

        return a;
    }

    public checkIfCredit(constant: number): boolean {
        let a: boolean;
        a = false;

        if (constant === SaleTypeConstant.CREDIT_CBD || constant === SaleTypeConstant.CREDIT_COD || constant  === SaleTypeConstant.CREDIT) {
            a = true;
        }

        return a;
    }

    public checkIfOffTheRoad(constant: number): boolean {
        let a: boolean;
        a = false;

        if (constant === SaleTypeConstant.CASH_OFF_THE_ROAD_CBD || constant === SaleTypeConstant.CASH_OFF_THE_ROAD_COD || constant === SaleTypeConstant.CASH_OFF_THE_ROAD) {
            a = true;
        }

        return a;
    }

    public filterSaleTypeByidParent(data: Array<SaleType>, idParent: number): Array<SaleType> {
        const a: Array<SaleType> = new Array<SaleType>();
        let sortData: any;

        sortData =  _.sortBy(data, function(e: SaleType) {
            return e.parentId;
        });

        this._filterSaleTypeByIdParent(sortData, idParent, a);

        console.log('sortData', sortData);

        return a;
        // return this._filterSaleTypeByIdParent(sortData, idParent, a);
    }

    protected _filterSaleTypeByIdParent(data: Array<SaleType>, idParent: number, result: Array<SaleType>): Array<SaleType> {
        if (data.length > 0) {
            let filter: Array<SaleType>;

            filter = _.filter(data, function(e: SaleType) {
                return e.parentId === idParent;
            });
            result.concat(filter);

        }

        return result;
    }

    create(saleType: SaleType): Observable<SaleType> {
        const copy = this.convert(saleType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(saleType: SaleType): Observable<SaleType> {
        const copy = this.convert(saleType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<SaleType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAll(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrl + '/getall')
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, saleType: SaleType): Observable<SaleType> {
        const copy = this.convert(saleType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, saleTypes: SaleType[]): Observable<SaleType[]> {
        const copy = this.convertList(saleTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(saleType: SaleType): SaleType {
        if (saleType === null || saleType === {}) {
            return {};
        }
        // const copy: SaleType = Object.assign({}, saleType);
        const copy: SaleType = JSON.parse(JSON.stringify(saleType));
        return copy;
    }

    protected convertList(saleTypes: SaleType[]): SaleType[] {
        const copy: SaleType[] = saleTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: SaleType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    queryC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrls, options)
            .map((res: Response) => this.convertResponse(res));
    }

    findByIdParentC(id?: any): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrls + '/by-idparent/' + id)
            .map((res: Response) => this.convertResponse(res));
    }
}
