import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { ApprovalService } from './';

@NgModule({
    providers: [
        ApprovalService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmApprovalModule {}
