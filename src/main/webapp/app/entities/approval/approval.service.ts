import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Approval } from './approval.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import { SalesUnitRequirement } from '../sales-unit-requirement';

import * as moment from 'moment';
import { SERVER_API_URL } from '../../app.constants';

@Injectable()
export class ApprovalService {
    protected itemValues: Approval[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/approvals';
    protected resourceSearchUrl = 'api/_search/approvals';

    constructor(
        protected http: Http
    ) { }

    create(approval: Approval): Observable<Approval> {
        const copy = this.convert(approval);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(approval: Approval): Observable<Approval> {
        const copy = this.convert(approval);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    getApprovalKacabForDiscount(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idinternal', req.idinternal);

        const http: string = this.resourceUrl + '/kacab/sales-unit-requirement/subsidi';
        return this.http.get(http, options).map((res: Response) => this.convertResponse(res));
    }

    getTerritorialViolation(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idinternal', req.idinternal);

        const http: string = this.resourceUrl + '/kacab/sales-unit-requirement/pelanggaran-wilayah';
        return this.http.get(http, options).map((res: Response) => this.convertResponse(res));
    }

    protected convert(approval: Approval): Approval {
        if (approval === null || approval === {}) {
            return {};
        }
        // const copy: SalesUnitRequirement = Object.assign({}, salesUnitRequirement);
        const copy: SalesUnitRequirement = JSON.parse(JSON.stringify(approval));
        return copy;
    }

    public getWaitingForApprovalSURForKorsal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idinternal', req.idinternal);

        const http: string = this.resourceUrl + '/korsal/sales-unit-requirement/subsidi';
        return this.http.get(http, options).map((res: Response) => this.convertResponse(res));
    }

    public getApprovalLeasing(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        const http: string = this.resourceUrl + '/leasing-approval';
        return this.http.get(http, options).map((res: Response) => this.convertResponse(res));
    }

    public getApprovalLeasingEmail(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        const http: string = SERVER_API_URL + 'api/approvals/leasing-approval/email';
        return this.http.get(http, options).map((res: Response) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertSUR(salesUnitRequirement: SalesUnitRequirement): SalesUnitRequirement {
        if (salesUnitRequirement === null || salesUnitRequirement === {}) {
            return {};
        }
        // const copy: SalesUnitRequirement = Object.assign({}, salesUnitRequirement);
        const copy: SalesUnitRequirement = JSON.parse(JSON.stringify(salesUnitRequirement));
        return copy;
    }
}
