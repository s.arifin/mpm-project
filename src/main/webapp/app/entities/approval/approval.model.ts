import { BaseEntity } from './../../shared';

export class Approval implements BaseEntity {
    constructor(
        public id?: number,
        public idApprovalType?: number,
        public idStatusType?: number,
        public idRequirement?: string,
        public idLeasingCompany?: string,
        public dateFrom?: Date,
        public dateThru?: Date,
        public reason?: string,
    ) {
    }
}
