import { BaseEntity } from './../../shared';

export class PartyCategory implements BaseEntity {
    constructor(
        public id?: number,
        public idCategory?: number,
        public refKey?: string,
        public description?: string,
        public categoryTypeId?: any,
    ) {
    }
}
