import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PartyCategoryComponent } from './party-category.component';
import { PartyCategoryEditComponent } from './party-category-edit.component';
import { PartyCategoryLovPopupComponent } from './party-category-as-lov.component';
import { PartyCategoryPopupComponent } from './party-category-dialog.component';

@Injectable()
export class PartyCategoryResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idCategory,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const partyCategoryRoute: Routes = [
    {
        path: 'party-category',
        component: PartyCategoryComponent,
        resolve: {
            'pagingParams': PartyCategoryResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const partyCategoryPopupRoute: Routes = [
    {
        path: 'party-category-lov',
        component: PartyCategoryLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'party-category-popup-new-list/:parent',
        component: PartyCategoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'party-category-popup-new',
        component: PartyCategoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'party-category-new',
        component: PartyCategoryEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategory.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'party-category/:id/edit',
        component: PartyCategoryEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategory.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'party-category/:route/:page/:id/edit',
        component: PartyCategoryEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategory.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'party-category/:id/popup-edit',
        component: PartyCategoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
