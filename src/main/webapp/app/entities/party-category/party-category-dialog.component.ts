import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PartyCategory } from './party-category.model';
import { PartyCategoryPopupService } from './party-category-popup.service';
import { PartyCategoryService } from './party-category.service';
import { ToasterService } from '../../shared';
import { PartyCategoryType, PartyCategoryTypeService } from '../party-category-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-party-category-dialog',
    templateUrl: './party-category-dialog.component.html'
})
export class PartyCategoryDialogComponent implements OnInit {

    partyCategory: PartyCategory;
    isSaving: boolean;
    idCategoryType: any;

    partycategorytypes: PartyCategoryType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected partyCategoryService: PartyCategoryService,
        protected partyCategoryTypeService: PartyCategoryTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.partyCategoryTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.partycategorytypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.partyCategory.idCategory !== undefined) {
            this.subscribeToSaveResponse(
                this.partyCategoryService.update(this.partyCategory));
        } else {
            this.subscribeToSaveResponse(
                this.partyCategoryService.create(this.partyCategory));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PartyCategory>) {
        result.subscribe((res: PartyCategory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: PartyCategory) {
        this.eventManager.broadcast({ name: 'partyCategoryListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'partyCategory saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'partyCategory Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPartyCategoryTypeById(index: number, item: PartyCategoryType) {
        return item.idCategoryType;
    }
}

@Component({
    selector: 'jhi-party-category-popup',
    template: ''
})
export class PartyCategoryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected partyCategoryPopupService: PartyCategoryPopupService
    ) {}

    ngOnInit() {
        this.partyCategoryPopupService.idCategoryType = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.partyCategoryPopupService
                    .open(PartyCategoryDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.partyCategoryPopupService.idCategoryType = params['parent'];
                this.partyCategoryPopupService
                    .open(PartyCategoryDialogComponent as Component);
            } else {
                this.partyCategoryPopupService
                    .open(PartyCategoryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
