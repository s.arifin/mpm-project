import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { PartyCategory } from './party-category.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PartyCategoryService {
   protected itemValues: PartyCategory[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/party-categories';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/party-categories';

   constructor(protected http: Http) { }

   create(partyCategory: PartyCategory): Observable<PartyCategory> {
       const copy = this.convert(partyCategory);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(partyCategory: PartyCategory): Observable<PartyCategory> {
       const copy = this.convert(partyCategory);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<PartyCategory> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: PartyCategory, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: PartyCategory[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to PartyCategory.
    */
   protected convertItemFromServer(json: any): PartyCategory {
       const entity: PartyCategory = Object.assign(new PartyCategory(), json);
       return entity;
   }

   /**
    * Convert a PartyCategory to a JSON which can be sent to the server.
    */
   protected convert(partyCategory: PartyCategory): PartyCategory {
       if (partyCategory === null || partyCategory === {}) {
           return {};
       }
       // const copy: PartyCategory = Object.assign({}, partyCategory);
       const copy: PartyCategory = JSON.parse(JSON.stringify(partyCategory));
       return copy;
   }

   protected convertList(partyCategorys: PartyCategory[]): PartyCategory[] {
       const copy: PartyCategory[] = partyCategorys;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: PartyCategory[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
