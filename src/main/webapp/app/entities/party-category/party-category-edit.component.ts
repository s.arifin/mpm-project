import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PartyCategory } from './party-category.model';
import { PartyCategoryService } from './party-category.service';
import { ToasterService} from '../../shared';
import { PartyCategoryType, PartyCategoryTypeService } from '../party-category-type';
import { ResponseWrapper } from '../../shared';

@Component({
   selector: 'jhi-party-category-edit',
   templateUrl: './party-category-edit.component.html'
})
export class PartyCategoryEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   partyCategory: PartyCategory;
   isSaving: boolean;
   idCategory: any;
   paramPage: number;
   routeId: number;

   partycategorytypes: PartyCategoryType[];

   constructor(
       protected alertService: JhiAlertService,
       protected partyCategoryService: PartyCategoryService,
       protected partyCategoryTypeService: PartyCategoryTypeService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected toaster: ToasterService
   ) {
       this.partyCategory = new PartyCategory();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idCategory = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
       this.partyCategoryTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.partycategorytypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.partyCategoryService.find(this.idCategory).subscribe((partyCategory) => {
           this.partyCategory = partyCategory;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['party-category', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.partyCategory.idCategory !== undefined) {
           this.subscribeToSaveResponse(
               this.partyCategoryService.update(this.partyCategory));
       } else {
           this.subscribeToSaveResponse(
               this.partyCategoryService.create(this.partyCategory));
       }
   }

   protected subscribeToSaveResponse(result: Observable<PartyCategory>) {
       result.subscribe((res: PartyCategory) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: PartyCategory) {
       this.eventManager.broadcast({ name: 'partyCategoryListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'partyCategory saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'partyCategory Changed', error.message);
       this.alertService.error(error.message, null, null);
   }

   trackPartyCategoryTypeById(index: number, item: PartyCategoryType) {
       return item.idCategoryType;
   }
}
