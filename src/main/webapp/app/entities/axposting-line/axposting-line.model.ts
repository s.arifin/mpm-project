import { BaseEntity } from '../../shared/index';

export class AxPostingLine {
    constructor(
        public AccountNum?: string,
        public AccountType?: string,
        public AmountCurCredit?: string,
        public AmountCurDebit?: string,
        public Company?: string,
        public DMSNum?: string,
        public Description?: string,
        public Dimension1?: string,
        public Dimension2?: string,
        public Dimension3?: string,
        public Dimension4?: string,
        public Dimension5?: string,
        public Dimension6?: string,
        public DueDate?: string,
        public Invoice?: string,
        public Payment?: string,
    ) {
    }
}
