import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { AxPostingLineService } from '../axposting-line/index';

@NgModule({
    providers: [
        AxPostingLineService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmAxPostingLineModule {}
