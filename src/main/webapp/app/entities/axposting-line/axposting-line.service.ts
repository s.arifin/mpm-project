import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { AxPostingLine } from './axposting-line.model';
import { ResponseWrapper } from '../../shared/index';

@Injectable()
export class AxPostingLineService {

    constructor(
        protected http: Http
    ) { }
}
