import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { RuleHotItem } from './rule-hot-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class RuleHotItemService {

    protected resourceUrl = SERVER_API_URL + 'api/rule-hot-items';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/rule-hot-items';

    constructor(protected http: Http) { }

    public checkByIdProductAndIdInternal(req?: any): Observable<Boolean> {
        const options = createRequestOption(req);
        options.params.set('idproduct', req.idproduct);
        options.params.set('idinternal', req.idinternal);
        return this.http.get(this.resourceUrl + '/check-by-product-and-internal', options)
            .map(
                (res: Response) => {
                    return res.json();
                }
            )
    }

    create(ruleHotItem: RuleHotItem): Observable<RuleHotItem> {
        const copy = this.convert(ruleHotItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(ruleHotItem: RuleHotItem): Observable<RuleHotItem> {
        const copy = this.convert(ruleHotItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<RuleHotItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findAllByidProductAndInternal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idProduct', req.idProduct);
        options.params.set('idinternal', req.idinternal);

        return this.http.get(this.resourceUrl + '/by-product-and-internal', options)
            .map(
                (res: Response) => this.convertResponse(res)
            )
    }

    findAllByIdInternal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idinternal', req.idinternal);

        const url = this.resourceUrl + '/by-internal';
        return this.http.get(url, options).map(
            (res: Response) => this.convertResponse(res)
        )
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(ruleHotItem: RuleHotItem): RuleHotItem {
        const copy: RuleHotItem = Object.assign({}, ruleHotItem);
        return copy;
    }
}
