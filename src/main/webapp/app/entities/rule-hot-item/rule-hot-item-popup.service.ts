import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RuleHotItem } from './rule-hot-item.model';
import { RuleHotItemService } from './rule-hot-item.service';
import * as RuleTypeConstants from '../../shared/constants/rule-type.constants';

@Injectable()
export class RuleHotItemPopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected ruleHotItemService: RuleHotItemService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, idproduct?: number | any, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.ruleHotItemService.find(id).subscribe((ruleHotItem) => {
                    this.ngbModalRef = this.ruleHotItemModalRef(component, ruleHotItem);
                    resolve(this.ngbModalRef);
                });
            } else if (idproduct) {
                setTimeout(() => {
                    const data = new RuleHotItem();
                    data.idProduct = idproduct;
                    data.dateFrom = new Date();
                    data.dateThru = new Date();
                    data.idRuleType = RuleTypeConstants.HOT_ITEM;
                    console.log('data', data);
                    this.ngbModalRef = this.ruleHotItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                const data = new RuleHotItem();
                data.dateFrom = new Date();
                data.dateThru = new Date();
                data.idRuleType = RuleTypeConstants.HOT_ITEM;
                setTimeout(() => {
                    this.ngbModalRef = this.ruleHotItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    ruleHotItemModalRef(component: Component, ruleHotItem: RuleHotItem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.ruleHotItem = ruleHotItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
