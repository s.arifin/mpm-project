import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { RuleHotItem } from './rule-hot-item.model';
import { RuleHotItemService } from './rule-hot-item.service';

@Component({
    selector: 'jhi-rule-hot-item-detail',
    templateUrl: './rule-hot-item-detail.component.html'
})
export class RuleHotItemDetailComponent implements OnInit, OnDestroy {

    ruleHotItem: RuleHotItem;
    protected subscription: Subscription;
    protected eventSubscriber: Subscription;

    constructor(
        protected eventManager: JhiEventManager,
        protected ruleHotItemService: RuleHotItemService,
        protected route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRuleHotItems();
    }

    load(id) {
        this.ruleHotItemService.find(id).subscribe((ruleHotItem) => {
            this.ruleHotItem = ruleHotItem;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRuleHotItems() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ruleHotItemListModification',
            (response) => this.load(this.ruleHotItem.id)
        );
    }
}
