import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    RuleHotItemAsListComponent,
    RuleHotItemService,
    RuleHotItemPopupService,
    RuleHotItemComponent,
    RuleHotItemDetailComponent,
    RuleHotItemDialogComponent,
    RuleHotItemPopupComponent,
    ruleHotItemRoute,
    ruleHotItemPopupRoute,
    RuleHotItemResolvePagingParams,
} from './';

import {
    CheckboxModule,
    InputTextModule,
    InputTextareaModule,
    CalendarModule,
    DropdownModule,
    EditorModule,
    ButtonModule,
    DataTableModule,
    DataListModule,
    DataGridModule,
    DataScrollerModule,
    CarouselModule,
    PickListModule,
    PaginatorModule,
    DialogModule,
    ConfirmDialogModule,
    ConfirmationService,
    GrowlModule,
    SharedModule,
    AccordionModule,
    TabViewModule,
    FieldsetModule,
    ScheduleModule,
    PanelModule,
    ListboxModule,
    ChartModule,
    DragDropModule,
    LightboxModule
   } from 'primeng/primeng';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...ruleHotItemRoute,
    ...ruleHotItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        // MpmSharedEntityModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule
    ],
    exports: [
        RuleHotItemComponent,
        RuleHotItemDetailComponent,
        RuleHotItemDialogComponent,
        RuleHotItemPopupComponent
    ],
    declarations: [
        RuleHotItemComponent,
        RuleHotItemDetailComponent,
        RuleHotItemDialogComponent,
        RuleHotItemPopupComponent
    ],
    entryComponents: [
        RuleHotItemComponent,
        RuleHotItemComponent,
        RuleHotItemDialogComponent,
        RuleHotItemPopupComponent
    ],
    providers: [
        RuleHotItemPopupService,
        RuleHotItemResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmRuleHotItemModule {}
