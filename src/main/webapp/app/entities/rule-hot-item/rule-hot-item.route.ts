import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { RuleHotItemComponent } from './rule-hot-item.component';
import { RuleHotItemDetailComponent } from './rule-hot-item-detail.component';
import { RuleHotItemPopupComponent } from './rule-hot-item-dialog.component';

@Injectable()
export class RuleHotItemResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRule,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const ruleHotItemRoute: Routes = [
    {
        path: 'rule-hot-item',
        component: RuleHotItemComponent,
        resolve: {
            'pagingParams': RuleHotItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.ruleHotItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'rule-hot-item/:id',
        component: RuleHotItemDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.ruleHotItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ruleHotItemPopupRoute: Routes = [
    {
        path: 'rule-hot-item-new/:idproduct',
        component: RuleHotItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.ruleHotItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rule-hot-item/:id/edit',
        component: RuleHotItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.ruleHotItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
