import { BaseEntity } from './../../shared';
import { Rules } from '../rules/rules.model';

export class RuleHotItem extends Rules {
    constructor(
        public idRule?: number,
        public idProduct?: string,
        public minDownPayment?: number,
    ) {
        super();
    }
}
