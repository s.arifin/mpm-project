import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { RuleHotItem } from './rule-hot-item.model';
import { RuleHotItemService } from './rule-hot-item.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import {LazyLoadEvent} from 'primeng/primeng';
import {ConfirmationService} from 'primeng/primeng';

@Component({
    selector: 'jhi-rule-hot-item-as-list',
    templateUrl: './rule-hot-item-as-list.component.html'
})
export class RuleHotItemAsListComponent implements OnChanges {
    @Input()
    idProduct: string;

    currentAccount: any;
    ruleHotItems: RuleHotItem[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    readonly: boolean;

    constructor(
        protected ruleHotItemService: RuleHotItemService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idProduct']) {
            if (this.idProduct !== undefined) {
                this.loadAll();
                this.registerChangeInRuleHotItems();
            }
        }
    }

    loadAll() {
        if (this.currentSearch) {
            this.ruleHotItemService.findAllByidProductAndInternal({
                idProduct: this.idProduct,
                idinternal : this.principalService.getIdInternal(),
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.ruleHotItemService.findAllByidProductAndInternal({
            idProduct: this.idProduct,
            idinternal: this.principalService.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/rule-hot-item'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });

        if (this.idProduct !== undefined) {
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/rule-hot-item', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        if (this.idProduct !== undefined) {
            this.loadAll();
        }
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/rule-hot-item', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        if (this.idProduct !== undefined) {
            this.loadAll();
        }
    }

    trackId(index: number, item: RuleHotItem) {
        return item.idRule;
    }
    registerChangeInRuleHotItems() {
        this.eventSubscriber = this.eventManager.subscribe('ruleHotItemListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRule') {
            result.push('idRule');
        }
        return result;
    }

    public loadDataLazy(event: LazyLoadEvent): void {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }

        if (this.idProduct !== undefined) {
            this.loadAll();
        }
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.ruleHotItems = data;
    }

    public delete(id: any, idx: number): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.ruleHotItemService.delete(id).subscribe(
                    (response) => {
                        this.eventManager.broadcast({
                            name: 'ruleHotItemListModification',
                            content: 'Deleted an Rule Hot Item'
                        });
                    },
                    (res) => {
                        this.onError(res);
                    }
                );
            }
        });
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
