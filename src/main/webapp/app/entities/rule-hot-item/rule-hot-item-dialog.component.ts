import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RuleHotItem } from './rule-hot-item.model';
import { RuleHotItemPopupService } from './rule-hot-item-popup.service';
import { RuleHotItemService } from './rule-hot-item.service';
import { Principal } from '../../shared';

import * as moment from 'moment';

import * as RuleTypeConstants from '../../shared/constants/rule-type.constants';

@Component({
    selector: 'jhi-rule-hot-item-dialog',
    templateUrl: './rule-hot-item-dialog.component.html'
})
export class RuleHotItemDialogComponent implements OnInit {

    ruleHotItem: RuleHotItem;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected ruleHotItemService: RuleHotItemService,
        protected eventManager: JhiEventManager,
        protected principalService: Principal
    ) {
    }

    ngOnInit() {
        if (this.ruleHotItem.idRule !== undefined) {
            this.ruleHotItem.dateFrom = new Date(this.ruleHotItem.dateFrom);
            this.ruleHotItem.dateThru = new Date(this.ruleHotItem.dateThru);
        }

        console.log('bbbb', this.ruleHotItem);

        this.ruleHotItem.idRuleType = RuleTypeConstants.HOT_ITEM;
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        const dtThru = new Date(this.ruleHotItem.dateThru);
        dtThru.setHours(23, 59, 59);
        this.ruleHotItem.dateThru = dtThru;

        if (this.ruleHotItem.idRule !== undefined) {
            this.subscribeToSaveResponse(
                this.ruleHotItemService.update(this.ruleHotItem));
        } else {
            this.ruleHotItem.idInternal = this.principalService.getIdInternal();
            this.subscribeToSaveResponse(
                this.ruleHotItemService.create(this.ruleHotItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RuleHotItem>) {
        result.subscribe((res: RuleHotItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: RuleHotItem) {
        this.eventManager.broadcast({ name: 'ruleHotItemListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-rule-hot-item-popup',
    template: ''
})
export class RuleHotItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected ruleHotItemPopupService: RuleHotItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.ruleHotItemPopupService
                    .open(RuleHotItemDialogComponent as Component, null, params['id']);
            } else if (params['idproduct']) {
                this.ruleHotItemPopupService
                    .open(RuleHotItemDialogComponent as Component, params['idproduct'], null);
            } else {
                this.ruleHotItemPopupService
                    .open(RuleHotItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
