import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    MappingAccessoriesComponent,
    MappingAccessoriesService,
    MappingAccessoriesRoute,
    MappingAccessoriesResolvePagingParams,
    MappingAccessoriesAsListComponent,
} from '.';

import { CommonModule } from '@angular/common';

import {CurrencyMaskModule} from 'ng2-currency-mask';

import {
        CheckboxModule,
        InputTextModule,
        CalendarModule,
        DropdownModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        DialogModule,
        ConfirmDialogModule,
        ConfirmationService,
        GrowlModule,
        DataGridModule,
        SharedModule,
        AccordionModule,
        TabViewModule,
        FieldsetModule,
        ScheduleModule,
        PanelModule,
        ListboxModule,
        SelectItem,
        MenuItem,
        Header,
        Footer} from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...MappingAccessoriesRoute,
];

@NgModule({
    imports: [
        MpmSharedEntityModule,
        InputTextareaModule,
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        DataGridModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        CurrencyMaskModule,
    ],
    exports: [
        MappingAccessoriesComponent,
        MappingAccessoriesAsListComponent
    ],
    declarations: [
        MappingAccessoriesComponent,
        MappingAccessoriesAsListComponent
    ],
    entryComponents: [
        MappingAccessoriesComponent,
        MappingAccessoriesAsListComponent

    ],
    providers: [
        MappingAccessoriesService,
        MappingAccessoriesResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMappingAccessoriesModule {}
