export * from './mapping-accessories.model';
export * from './mapping-accessories.service';
export * from './mapping-accessories.component';
export * from './mapping-accessories.route';
export * from './mapping-accessories-as-list.component';
