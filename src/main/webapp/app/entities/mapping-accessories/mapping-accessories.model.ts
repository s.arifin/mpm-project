import { BaseEntity } from './../shared-component';
// import { Good } from './../good';

export class MappingAccessories implements BaseEntity {
    constructor(
        public id?: any,
        public idProduct?: any,
        public idAccessories?: any,
        public description?: string,

    ) {
    }
}
