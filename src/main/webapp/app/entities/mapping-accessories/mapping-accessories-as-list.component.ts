import {Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges} from '@angular/core';
import {Response} from '@angular/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription, Observable} from 'rxjs/Rx';
import {JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { MappingAccessories } from './mapping-accessories.model';
import { MappingAccessoriesService } from './mapping-accessories.service';
import {ITEMS_PER_PAGE, Principal, ResponseWrapper} from '../../shared';
import {PaginationConfig} from '../../blocks/config/uib-pagination.config';

import {LazyLoadEvent} from 'primeng/primeng';
import {ToasterService} from '../../shared/alert/toaster.service';
import {ConfirmationService} from 'primeng/primeng';
import { MasterAksesorisService } from '../master-aksesoris/master-aksesoris.service';
import { MasterAksesoris } from '../master-aksesoris/master-aksesoris.model';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-mapping-accessories-as-list',
    templateUrl: './mapping-accessories-as-list.component.html'
})
export class MappingAccessoriesAsListComponent implements OnInit, OnDestroy, OnChanges {
    // @Input() filterBy: any;

    @Input()
    idProduct: string;

    readonly: boolean;
    currentAccount: any;
    mappingAccessories: MappingAccessories[];
    mappingAccessoriesSave: MappingAccessories;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    mappingAccessoriesModal: boolean;
    masterAksesoris: MasterAksesoris[];
    searchResultMasterAcc: MasterAksesoris;
    listMasterAksesoris = [{label: 'Please Select', value: null}];

    constructor(
        protected mappingAccessoriesService: MappingAccessoriesService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected dataUtils: JhiDataUtils,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected masterAksesorisService: MasterAksesorisService,
        protected activeModal: NgbActiveModal
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idProduct';
        this.reverse = 'asc';
    }

    loadAll() {
        console.log('idProduct as list', this.idProduct);
        this.mappingAccessoriesService.findByidProduct(
            this.idProduct).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/mapping-accessories'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        if (this.idProduct !== undefined) {
            this.loadAll();
        }
    }

    clear() {
        this.mappingAccessoriesModal = false;
        this.activeModal.dismiss('cancel');
    }

    ngOnInit() {
        this.mappingAccessoriesModal = false;
        this.mappingAccessoriesSave = new MappingAccessories();
        console.log('test debug');
        this.masterAksesorisService.query({
            page: '0',
            size: 1000,
            sort: ['idAccessories', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.masterAksesoris = res.json;
            this.masterAksesoris.forEach((element) => {
                this.listMasterAksesoris.push({
                    label: element.idAccessories,
                    value: element.idAccessories});
            })
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idproduct']) {
            if (this.idProduct !== undefined) {
                this.loadAll();
                this.registerChangeInMappingAccessories();
            }
        }
    }

    getDescription() {
        this.searchResultMasterAcc = this.masterAksesoris.find((x) => x.idAccessories === this.mappingAccessoriesSave.idAccessories);
        if (this.searchResultMasterAcc !== undefined) {
            this.mappingAccessoriesSave.description = this.searchResultMasterAcc.description;
            console.log('SearchResultMasterAcc', this.searchResultMasterAcc);
        } else {
            this.mappingAccessoriesSave.description = '';
        }
    }

    ngOnDestroy() {

    }

    trackId(index: number, item: MappingAccessories) {
        return item.idAccessories;
    }

    registerChangeInMappingAccessories() {
        this.eventSubscriber = this.eventManager.subscribe('mappingAccessoriesListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idAccessories') {
            result.push('idAccessories');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.mappingAccessories = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.mappingAccessoriesService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }

        if (this.idProduct !== undefined) {
            this.loadAll();
        }
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.mappingAccessoriesService.update(event.data)
                .subscribe((res: MappingAccessories) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.mappingAccessoriesService.create(event.data)
                .subscribe((res: MappingAccessories) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    saveFromDialog() {
        // this.isSaving = true;
        this.mappingAccessoriesModal = false;
            this.subscribeToSaveResponse(
                this.mappingAccessoriesService.create(this.mappingAccessoriesSave));
    }
    protected subscribeToSaveResponse(result: Observable<MappingAccessories>) {
        result.subscribe((res: MappingAccessories) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: MappingAccessories) {
        this.eventManager.broadcast({ name: 'MappingAccessoriesListModification', content: 'OK'});
        this.toasterService.showToaster('info', 'Save', 'Mapping Accessories saved !');
        // this.isSaving = false;
        this.mappingAccessoriesModal = false;

        this.loadAll();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        // this.isSaving = false;
        this.onError(error);
    }

    protected onRowDataSaveSuccess(result: MappingAccessories) {
        this.toasterService.showToaster('info', 'MappingAccessories Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    public delete(id: any, idx: number): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.mappingAccessoriesService.delete(id).subscribe(
                    (response) => {
                        this.eventManager.broadcast({
                            name: 'mappingAccessoriesListModification',
                            content: 'Deleted an Mapping Accessories'
                        });
                        this.loadAll();
                    },
                    (res) => {
                        this.onError(res);
                    }
                );
            }
        });
    }

    showMappingDetail() {
        this.mappingAccessoriesSave.idProduct = this.idProduct
        this.mappingAccessoriesModal = true;
    }
}
