import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { MappingAccessories } from './mapping-accessories.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class MappingAccessoriesService {

    protected resourceUrl = 'api/mapping-accessories';
    protected resourceSearchUrl = 'api/_search/mapping-accessories';
    protected resourceCUrl = process.env.API_C_URL + '/api/MappingAccessories/';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(mappingAccessories: MappingAccessories): Observable<MappingAccessories> {
        const copy = this.convert(mappingAccessories);
        return this.http.post(this.resourceCUrl + 'savedata/', copy).map((res: Response) => {
            return res.json();
        });
    }

    update(mappingAccessories: MappingAccessories): Observable<MappingAccessories> {
        const copy = this.convert(mappingAccessories);
        return this.http.put(this.resourceCUrl + 'savedata/', copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<MappingAccessories> {
        console.log('search id di mapping acc');
        return this.http.get(this.resourceUrl + 'findData/?id=' + id).map((res: Response) => {
            return res.json();
        });
    }

    findByidProduct(idProduct?: string): Observable<ResponseWrapper> {
        // const options = createRequestOption(req);
        // options.params.set('Product', req.Product);
        return this.http.get(this.resourceCUrl + '/GetDataByMotorId/?Id=' + idProduct)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(this.resourceCUrl + 'delData/?id=' + id).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(id: number, param: String, mappingAccessories: any): Observable<String> {
        const copy = this.convert(mappingAccessories);
        return this.http.post(`${this.resourceUrl}/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(mappingAccessories: MappingAccessories): MappingAccessories {
        if (mappingAccessories === null || mappingAccessories === {}) {
            return {};
        }
        const copy: MappingAccessories = Object.assign({}, mappingAccessories);
        return copy;
    }

    public convertmappingAccessoriesForSelectPrimeNg(mappingAccessories: Array<MappingAccessories>): Array<Object> {
        const arr: Array<Object> = new Array<Object>();

        if (mappingAccessories.length > 0) {
            mappingAccessories.forEach(
                (m) => {
                    const obj: object = {
                        label : m.idProduct + ' - ' +  m.description,
                        value : m.idProduct
                    };
                    arr.push(obj);
                }
            )
        }

        return arr;
    }
}
