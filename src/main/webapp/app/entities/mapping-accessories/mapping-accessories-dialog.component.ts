import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import { ToasterService } from '../../shared';
import { ProductCategory, ProductCategoryService } from '../product-category';
import { ResponseWrapper } from '../../shared';
import { MappingAccessories } from './mapping-accessories.model';
import { MappingAccessoriesPopupService } from './mapping-accessories-popup.service';
import { MappingAccessoriesService } from './mapping-accessories.service';
import { MasterAksesoris, MasterAksesorisService } from '../master-aksesoris';

@Component({
    selector: 'jhi-mapping-accessories-dialog',
    templateUrl: './mapping-accessories-dialog.component.html'
})
export class MappingAccessoriesDialogComponent implements OnInit {

    // masterAksesorisServices: MasterAksesorisService;
    // masterAksesoris: MasterAksesoris;
    mappingAccessories: MappingAccessories;
    isSaving: boolean;
    idProduct: any;

    public listMasterAksesoris = [{label: 'Please Select', value: null}];
    public masterAksesoris: MasterAksesoris[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected mappingAccessoriesService: MappingAccessoriesService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected masterAksesorisService: MasterAksesorisService,
        protected activatedRoute: ActivatedRoute,
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        console.log('test debug');
        this.masterAksesorisService.query({
            page: '0',
            size: 8,
            sort: ['idAccessories', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.masterAksesoris = res.json;
            this.masterAksesoris.forEach((element) => {
                this.listMasterAksesoris.push({
                    label: element.idAccessories,
                    value: element.idAccessories});
            })
        });

        this.activatedRoute.params.subscribe((params) => {
            console.log('params', params);
            if (params['idproduct']) {
                this.idProduct = params['idproduct'];
                console.log('idproduct')
            }
        });
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.mappingAccessories.idAccessories !== undefined) {
            this.subscribeToSaveResponse(
                this.mappingAccessoriesService.update(this.mappingAccessories));
        } else {
            this.subscribeToSaveResponse(
                this.mappingAccessoriesService.create(this.mappingAccessories));
        }
    }

    protected subscribeToSaveResponse(result: Observable<MappingAccessories>) {
        result.subscribe((res: MappingAccessories) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: MappingAccessories) {
        this.eventManager.broadcast({ name: 'MappingAccessoriesListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'Mapping Accessories saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        // this.toaster.showToaster('warning', 'MasterAksesoris Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackProductCategoryById(index: number, item: ProductCategory) {
        return item.idCategory;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-mapping-accessories-popup',
    template: ''
})
export class MappingAccessoriesPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected mappingAccessoriesPopupService: MappingAccessoriesPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.mappingAccessoriesPopupService
                    .open(MappingAccessoriesDialogComponent as Component, params['id']);
            } else {
                this.mappingAccessoriesPopupService
                    .open(MappingAccessoriesDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
