import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription, Observable} from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import {ITEMS_PER_PAGE, Principal, ResponseWrapper} from '../../shared';
import {PaginationConfig} from '../../blocks/config/uib-pagination.config';

import {LazyLoadEvent} from 'primeng/primeng';
import {ToasterService } from '../../shared/alert/toaster.service';
import {ConfirmationService} from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading';
import { MappingAccessories } from '../mapping-accessories';
import { MappingAccessoriesService } from './mapping-accessories.service';
import { MasterAksesoris, MasterAksesorisService } from '../master-aksesoris';

@Component({
    selector: 'jhi-mapping-accessories',
    templateUrl: './mapping-accessories.component.html'
})
export class MappingAccessoriesComponent implements OnInit, OnDestroy {

    currentAccount: any;
    // mappingAccessoriesService: MappingAccessoriesService;
    mappingAccessories: MappingAccessories[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    mappingAccessoriesModal: boolean;
    listMasterAksesoris = [{label: 'Please Select', value: null}];
    masterAksesoris: MasterAksesoris[];

    constructor(
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected loadingService: LoadingService,
        protected mappingAccessoriess: MappingAccessories,
        protected mappingAccessoriesService: MappingAccessoriesService,
        protected masterAksesorisService: MasterAksesorisService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.loadingService.loadingStart();
        console.log('masuk sini');
        if (this.currentSearch) {
            this.mappingAccessoriesService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => {
                        this.loadingService.loadingStop();
                    }
                );
            return;
        }
        this.masterAksesorisService.query({
            page: this.page - 1,
            size: '1000',
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json),
            () => {
            }
        );
        this.loadingService.loadingStop();
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/mapping-accessories'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/mapping-accessories', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/mapping-accessories', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.mappingAccessoriesModal = false;
        console.log('test debug');
        this.masterAksesorisService.query({
            page: '0',
            size: 8,
            sort: ['idAccessories', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.masterAksesoris = res.json;
            this.masterAksesoris.forEach((element) => {
                this.listMasterAksesoris.push({
                    label: element.idAccessories,
                    value: element.idAccessories});
            })
        });
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idProduct') {
            result.push('idProduct');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        // this.totalItems = headers.get('X-Total-Count');
        console.log('debug', data);
        if (data.count > 0) {
            this.totalItems = data.TotalData;
        } else {
            this.totalItems = 0;
        }
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.mappingAccessoriess = data;
        console.log('this.mappingAccessories', this.mappingAccessories);
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    // executeProcess(data) {
    //     this.mappingAccessoriesService.executeProcess(10, '', data).subscribe(
    //        (value) => console.log('this: ', value),
    //        (err) => console.log(err),
    //        () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
    //     );
    // }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.mappingAccessoriesService.update(event.data)
                .subscribe((res: MappingAccessories) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.mappingAccessoriesService.create(event.data)
                .subscribe((res: MappingAccessories) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: MappingAccessories) {
        this.toasterService.showToaster('info', 'mappingAccessories Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.mappingAccessoriesService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'mappingAccessoriesListModification',
                    content: 'Deleted an mappingAccessories'
                    });
                    this.loadAll();
                });
            }
        });
    }
}
