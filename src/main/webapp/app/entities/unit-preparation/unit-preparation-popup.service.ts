import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { UnitPreparation } from './unit-preparation.model';
import { UnitPreparationService } from './unit-preparation.service';

@Injectable()
export class UnitPreparationPopupService {
    protected ngbModalRef: NgbModalRef;
    idShipTo: any;
    idInternal: any;
    idFacility: any;
    idInventoryItem: any;
    idOrderItem: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected unitPreparationService: UnitPreparationService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.unitPreparationService.find(id).subscribe((data) => {
                    // if (data.dateCreate) {
                    //    data.dateCreate = this.datePipe
                    //        .transform(data.dateCreate, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.unitPreparationModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new UnitPreparation();
                    data.shipToId = this.idShipTo;
                    data.internalId = this.idInternal;
                    data.facilityId = this.idFacility;
                    data.inventoryItemId = this.idInventoryItem;
                    data.orderItemId = this.idOrderItem;
                    this.ngbModalRef = this.unitPreparationModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    unitPreparationModalRef(component: Component, unitPreparation: UnitPreparation): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.unitPreparation = unitPreparation;
        modalRef.componentInstance.idShipTo = this.idShipTo;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idFacility = this.idFacility;
        modalRef.componentInstance.idInventoryItem = this.idInventoryItem;
        modalRef.componentInstance.idOrderItem = this.idOrderItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.unitPreparationLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    unitPreparationLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idShipTo = this.idShipTo;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idFacility = this.idFacility;
        modalRef.componentInstance.idInventoryItem = this.idInventoryItem;
        modalRef.componentInstance.idOrderItem = this.idOrderItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
