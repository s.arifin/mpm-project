import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { UnitPreparation } from './unit-preparation.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UnitPreparationService {
   protected itemValues: UnitPreparation[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/unit-preparations';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/unit-preparations';

   constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

   create(unitPreparation: UnitPreparation): Observable<UnitPreparation> {
       const copy = this.convert(unitPreparation);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(unitPreparation: UnitPreparation): Observable<UnitPreparation> {
       const copy = this.convert(unitPreparation);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<UnitPreparation> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   changeStatus(unitPreparation: UnitPreparation, id: Number): Observable<UnitPreparation> {
       const copy = this.convert(unitPreparation);
       return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
           .map((res: Response) => this.convertItemFromServer(res.json));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: UnitPreparation, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: UnitPreparation[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to UnitPreparation.
    */
   protected convertItemFromServer(json: any): UnitPreparation {
       const entity: UnitPreparation = Object.assign(new UnitPreparation(), json);
       if (entity.dateCreate) {
           entity.dateCreate = new Date(entity.dateCreate);
       }
       return entity;
   }

   /**
    * Convert a UnitPreparation to a JSON which can be sent to the server.
    */
   protected convert(unitPreparation: UnitPreparation): UnitPreparation {
       if (unitPreparation === null || unitPreparation === {}) {
           return {};
       }
       // const copy: UnitPreparation = Object.assign({}, unitPreparation);
       const copy: UnitPreparation = JSON.parse(JSON.stringify(unitPreparation));

       // copy.dateCreate = this.dateUtils.toDate(unitPreparation.dateCreate);
       return copy;
   }

   protected convertList(unitPreparations: UnitPreparation[]): UnitPreparation[] {
       const copy: UnitPreparation[] = unitPreparations;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: UnitPreparation[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
