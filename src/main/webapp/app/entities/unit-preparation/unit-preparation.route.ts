import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { UnitPreparationComponent } from './unit-preparation.component';
import { UnitPreparationEditComponent } from './unit-preparation-edit.component';
import { UnitPreparationLovPopupComponent } from './unit-preparation-as-lov.component';
import { UnitPreparationPopupComponent } from './unit-preparation-dialog.component';

@Injectable()
export class UnitPreparationResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idSlip,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const unitPreparationRoute: Routes = [
    {
        path: 'unit-preparation',
        component: UnitPreparationComponent,
        resolve: {
            'pagingParams': UnitPreparationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitPreparation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const unitPreparationPopupRoute: Routes = [
    {
        path: 'unit-preparation-lov',
        component: UnitPreparationLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitPreparation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unit-preparation-popup-new-list/:parent',
        component: UnitPreparationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitPreparation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unit-preparation-popup-new',
        component: UnitPreparationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitPreparation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unit-preparation-new',
        component: UnitPreparationEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitPreparation.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'unit-preparation/:id/edit',
        component: UnitPreparationEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitPreparation.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'unit-preparation/:route/:page/:id/edit',
        component: UnitPreparationEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitPreparation.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'unit-preparation/:id/popup-edit',
        component: UnitPreparationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitPreparation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
