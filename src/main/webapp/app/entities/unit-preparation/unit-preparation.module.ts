import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    UnitPreparationService,
    UnitPreparationPopupService,
    UnitPreparationComponent,
    UnitPreparationDialogComponent,
    UnitPreparationPopupComponent,
    unitPreparationRoute,
    unitPreparationPopupRoute,
    UnitPreparationResolvePagingParams,
    UnitPreparationAsLovComponent,
    UnitPreparationLovPopupComponent,
    UnitPreparationEditComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         InputTextModule,
         InputTextareaModule,
         DataScrollerModule,
         PaginatorModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         FieldsetModule,
         ScheduleModule,
         ChartModule,
         DragDropModule,
         LightboxModule
} from 'primeng/primeng';

import { DataTableModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { StepsModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ButtonModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...unitPreparationRoute,
    ...unitPreparationPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        TabsModule.forRoot(),
        TooltipModule.forRoot(),
        InputTextareaModule,
        ButtonModule,
        DataListModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        DataTableModule,
        StepsModule,
        SliderModule,
        RadioButtonModule
    ],
    exports: [
        UnitPreparationComponent,
        UnitPreparationAsLovComponent,
        UnitPreparationLovPopupComponent,
        UnitPreparationEditComponent
    ],
    declarations: [
        UnitPreparationComponent,
        UnitPreparationDialogComponent,
        UnitPreparationPopupComponent,
        UnitPreparationAsLovComponent,
        UnitPreparationLovPopupComponent,
        UnitPreparationEditComponent
    ],
    entryComponents: [
        UnitPreparationComponent,
        UnitPreparationDialogComponent,
        UnitPreparationPopupComponent,
        UnitPreparationAsLovComponent,
        UnitPreparationLovPopupComponent,
        UnitPreparationEditComponent
    ],
    providers: [
        UnitPreparationService,
        UnitPreparationPopupService,
        UnitPreparationResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmUnitPreparationModule {}
