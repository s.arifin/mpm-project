import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { UnitPreparation } from './unit-preparation.model';
import { UnitPreparationService } from './unit-preparation.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-unit-preparation-as-list',
    templateUrl: './unit-preparation-as-list.component.html'
})
export class UnitPreparationAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idShipTo: any;
    @Input() idInternal: any;
    @Input() idFacility: any;
    @Input() idInventoryItem: any;
    @Input() idOrderItem: any;

    currentAccount: any;
    unitPreparations: UnitPreparation[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        protected unitPreparationService: UnitPreparationService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'idSlip';
        this.reverse = 'asc';
        this.first = 0;
    }

    loadAll() {
        this.unitPreparationService.queryFilterBy({
            idShipTo: this.idShipTo,
            idInternal: this.idInternal,
            idFacility: this.idFacility,
            idInventoryItem: this.idInventoryItem,
            idOrderItem: this.idOrderItem,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/unit-preparation', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInUnitPreparations();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idShipTo']) {
            this.loadAll();
        }
        if (changes['idInternal']) {
            this.loadAll();
        }
        if (changes['idFacility']) {
            this.loadAll();
        }
        if (changes['idInventoryItem']) {
            this.loadAll();
        }
        if (changes['idOrderItem']) {
            this.loadAll();
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: UnitPreparation) {
        return item.idSlip;
    }

    registerChangeInUnitPreparations() {
        this.eventSubscriber = this.eventManager.subscribe('unitPreparationListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idSlip') {
            result.push('idSlip');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.unitPreparations = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.unitPreparationService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.subscribeToSaveResponse(
                this.unitPreparationService.update(event.data));
        } else {
            this.subscribeToSaveResponse(
                this.unitPreparationService.create(event.data));
        }
    }

    protected subscribeToSaveResponse(result: Observable<UnitPreparation>) {
        result.subscribe((res: UnitPreparation) => this.onSaveSuccess(res));
    }

    protected onSaveSuccess(result: UnitPreparation) {
        this.eventManager.broadcast({ name: 'unitPreparationListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'unitPreparation saved !');
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.unitPreparationService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'unitPreparationListModification',
                        content: 'Deleted an unitPreparation'
                    });
                });
            }
        });
    }
}
