import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UnitPreparation } from './unit-preparation.model';
import { UnitPreparationPopupService } from './unit-preparation-popup.service';
import { UnitPreparationService } from './unit-preparation.service';
import { ToasterService } from '../../shared';
import { ShipTo, ShipToService } from '../ship-to';
import { Internal, InternalService } from '../internal';
import { Facility, FacilityService } from '../facility';
import { InventoryItem, InventoryItemService } from '../inventory-item';
import { OrderItem, OrderItemService } from '../order-item';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-unit-preparation-dialog',
    templateUrl: './unit-preparation-dialog.component.html'
})
export class UnitPreparationDialogComponent implements OnInit {

    unitPreparation: UnitPreparation;
    isSaving: boolean;
    idShipTo: any;
    idInternal: any;
    idFacility: any;
    idInventoryItem: any;
    idOrderItem: any;

    shiptos: ShipTo[];

    internals: Internal[];

    facilities: Facility[];

    inventoryitems: InventoryItem[];

    orderitems: OrderItem[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected unitPreparationService: UnitPreparationService,
        protected shipToService: ShipToService,
        protected internalService: InternalService,
        protected facilityService: FacilityService,
        protected inventoryItemService: InventoryItemService,
        protected orderItemService: OrderItemService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.shipToService.query()
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.inventoryItemService.query()
            .subscribe((res: ResponseWrapper) => { this.inventoryitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.orderItemService.query()
            .subscribe((res: ResponseWrapper) => { this.orderitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.unitPreparation.idSlip !== undefined) {
            this.subscribeToSaveResponse(
                this.unitPreparationService.update(this.unitPreparation));
        } else {
            this.subscribeToSaveResponse(
                this.unitPreparationService.create(this.unitPreparation));
        }
    }

    protected subscribeToSaveResponse(result: Observable<UnitPreparation>) {
        result.subscribe((res: UnitPreparation) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: UnitPreparation) {
        this.eventManager.broadcast({ name: 'unitPreparationListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'unitPreparation saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'unitPreparation Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackInventoryItemById(index: number, item: InventoryItem) {
        return item.idInventoryItem;
    }

    trackOrderItemById(index: number, item: OrderItem) {
        return item.idOrderItem;
    }
}

@Component({
    selector: 'jhi-unit-preparation-popup',
    template: ''
})
export class UnitPreparationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected unitPreparationPopupService: UnitPreparationPopupService
    ) {}

    ngOnInit() {
        this.unitPreparationPopupService.idShipTo = undefined;
        this.unitPreparationPopupService.idInternal = undefined;
        this.unitPreparationPopupService.idFacility = undefined;
        this.unitPreparationPopupService.idInventoryItem = undefined;
        this.unitPreparationPopupService.idOrderItem = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.unitPreparationPopupService
                    .open(UnitPreparationDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.unitPreparationPopupService.parent = params['parent'];
                this.unitPreparationPopupService
                    .open(UnitPreparationDialogComponent as Component);
            } else {
                this.unitPreparationPopupService
                    .open(UnitPreparationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
