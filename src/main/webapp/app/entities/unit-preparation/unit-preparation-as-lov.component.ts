import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { UnitPreparation } from './unit-preparation.model';
import { UnitPreparationService } from './unit-preparation.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { UnitPreparationPopupService } from './unit-preparation-popup.service';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';

@Component({
    selector: 'jhi-unit-preparation-as-lov',
    templateUrl: './unit-preparation-as-lov.component.html'
})
export class UnitPreparationAsLovComponent implements OnInit, OnDestroy {

    currentAccount: any;
    idShipTo: any;
    idInternal: any;
    idFacility: any;
    idInventoryItem: any;
    idOrderItem: any;
    unitPreparations: UnitPreparation[];
    selected: UnitPreparation[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        public activeModal: NgbActiveModal,
        protected unitPreparationService: UnitPreparationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'idSlip';
        this.reverse = 'asc';
        this.first = 0;
        this.selected = [];
    }

    loadAll() {
        if (this.currentSearch) {
            this.unitPreparationService.search({
                idShipTo: this.idShipTo,
                idInternal: this.idInternal,
                idFacility: this.idFacility,
                idInventoryItem: this.idInventoryItem,
                idOrderItem: this.idOrderItem,
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.unitPreparationService.queryFilterBy({
            idShipTo: this.idShipTo,
            idInternal: this.idInternal,
            idFacility: this.idFacility,
            idInventoryItem: this.idInventoryItem,
            idOrderItem: this.idOrderItem,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: UnitPreparation) {
        return item.idSlip;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idSlip') {
            result.push('idSlip');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.unitPreparations = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    pushData() {
        this.unitPreparationService.pushItems(this.selected);
        this.activeModal.dismiss('close');
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

}

@Component({
    selector: 'jhi-unit-preparation-lov-popup',
    template: ''
})
export class UnitPreparationLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected unitPreparationPopupService: UnitPreparationPopupService
    ) {}

    ngOnInit() {
        this.unitPreparationPopupService.idShipTo = undefined;
        this.unitPreparationPopupService.idInternal = undefined;
        this.unitPreparationPopupService.idFacility = undefined;
        this.unitPreparationPopupService.idInventoryItem = undefined;
        this.unitPreparationPopupService.idOrderItem = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            this.unitPreparationPopupService
                .load(UnitPreparationAsLovComponent as Component);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
