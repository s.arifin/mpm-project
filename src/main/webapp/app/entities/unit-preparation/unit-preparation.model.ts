import { BaseEntity } from './../../shared';

export class UnitPreparation implements BaseEntity {
    constructor(
        public id?: any,
        public idSlip?: any,
        public currentStatus?: number,
        public dateCreate?: any,
        public slipNumber?: string,
        public userMekanik?: string,
        public shipToId?: any,
        public internalId?: any,
        public facilityId?: any,
        public inventoryItemId?: any,
        public orderItemId?: any,
        public orderNumber?: any,
    ) {
    }
}
