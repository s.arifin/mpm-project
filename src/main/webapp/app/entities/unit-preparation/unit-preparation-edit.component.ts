import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UnitPreparation } from './unit-preparation.model';
import { UnitPreparationService } from './unit-preparation.service';
import { ToasterService} from '../../shared';
import { ShipTo, ShipToService } from '../ship-to';
import { Internal, InternalService } from '../internal';
import { Facility, FacilityService } from '../facility';
import { InventoryItem, InventoryItemService } from '../inventory-item';
import { OrderItem, OrderItemService } from '../order-item';
import { ResponseWrapper } from '../../shared';
import { ConfirmationService } from 'primeng/primeng';

@Component({
   selector: 'jhi-unit-preparation-edit',
   templateUrl: './unit-preparation-edit.component.html'
})
export class UnitPreparationEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   unitPreparation: UnitPreparation;
   isSaving: boolean;
   idSlip: any;
   paramPage: number;
   routeId: number;

   shiptos: ShipTo[];

   internals: Internal[];

   facilities: Facility[];

   inventoryitems: InventoryItem[];

   orderitems: OrderItem[];

   constructor(
       protected alertService: JhiAlertService,
       protected unitPreparationService: UnitPreparationService,
       protected shipToService: ShipToService,
       protected internalService: InternalService,
       protected facilityService: FacilityService,
       protected inventoryItemService: InventoryItemService,
       protected orderItemService: OrderItemService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected toaster: ToasterService,
       protected confirmationService: ConfirmationService
   ) {
       this.unitPreparation = new UnitPreparation();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idSlip = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
       this.shipToService.query()
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.inventoryItemService.query()
            .subscribe((res: ResponseWrapper) => { this.inventoryitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.orderItemService.query()
            .subscribe((res: ResponseWrapper) => { this.orderitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.unitPreparationService.find(this.idSlip).subscribe((unitPreparation) => {
           this.unitPreparation = unitPreparation;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['unit-preparation', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.unitPreparation.idSlip !== undefined) {
           this.subscribeToSaveResponse(
               this.unitPreparationService.update(this.unitPreparation));
       } else {
           this.subscribeToSaveResponse(
               this.unitPreparationService.create(this.unitPreparation));
       }
   }

   protected subscribeToSaveResponse(result: Observable<UnitPreparation>) {
       result.subscribe((res: UnitPreparation) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: UnitPreparation) {
       this.eventManager.broadcast({ name: 'unitPreparationListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'unitPreparation saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'unitPreparation Changed', error.message);
       this.alertService.error(error.message, null, null);
   }

   trackShipToById(index: number, item: ShipTo) {
       return item.idShipTo;
   }

   trackInternalById(index: number, item: Internal) {
       return item.idInternal;
   }

   trackFacilityById(index: number, item: Facility) {
       return item.idFacility;
   }

   trackInventoryItemById(index: number, item: InventoryItem) {
       return item.idInventoryItem;
   }

   trackOrderItemById(index: number, item: OrderItem) {
       return item.idOrderItem;
   }
   activated() {
     this.unitPreparationService.changeStatus(this.unitPreparation, 11).delay(1000).subscribe((r) => {
        this.load();
        this.toaster.showToaster('info', 'Data Activated', 'Activated.....');
      });
   }

   approved() {
     this.unitPreparationService.changeStatus(this.unitPreparation, 12).delay(1000).subscribe((r) => {
        this.load();
        this.toaster.showToaster('info', 'Data Approved', 'Approved.....');
      });
   }

   completed() {
       this.unitPreparationService.changeStatus(this.unitPreparation, 17).delay(1000).subscribe((r) => {
           this.eventManager.broadcast({
               name: 'unitPreparationListModification',
               content: 'Completed an unitPreparation'
            });
            this.toaster.showToaster('info', 'Data Completed', 'Completed.....');
            this.previousState();
        });
   }

   canceled() {
       this.confirmationService.confirm({
           message: 'Are you sure that you want to cancel?',
           header: 'Confirmation',
           icon: 'fa fa-question-circle',
           accept: () => {
               this.unitPreparationService.changeStatus(this.unitPreparation, 13).delay(1000).subscribe((r) => {
                   this.eventManager.broadcast({
                       name: 'unitPreparationListModification',
                       content: 'Cancel an unitPreparation'
                    });
                    this.toaster.showToaster('info', 'Data unitPreparation cancel', 'Cancel unitPreparation.....');
                    this.previousState();
                });
            }
        });
   }

}
