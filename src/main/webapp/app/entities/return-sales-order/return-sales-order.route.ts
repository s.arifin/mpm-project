import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ReturnSalesOrderComponent } from './return-sales-order.component';
import { ReturnSalesOrderEditComponent } from './return-sales-order-edit.component';
import { ReturnSalesOrderPopupComponent } from './return-sales-order-dialog.component';

@Injectable()
export class ReturnSalesOrderResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idOrder,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const returnSalesOrderRoute: Routes = [
    {
        path: 'return-sales-order',
        component: ReturnSalesOrderComponent,
        resolve: {
            'pagingParams': ReturnSalesOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returnSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const returnSalesOrderPopupRoute: Routes = [
    {
        path: 'return-sales-order-popup-new',
        component: ReturnSalesOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returnSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'return-sales-order-new',
        component: ReturnSalesOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returnSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'return-sales-order/:id/edit',
        component: ReturnSalesOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returnSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'return-sales-order/:route/:page/:id/edit',
        component: ReturnSalesOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returnSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'return-sales-order/:id/popup-edit',
        component: ReturnSalesOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.returnSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
