import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ReturnSalesOrder } from './return-sales-order.model';
import { ReturnSalesOrderService } from './return-sales-order.service';
import { ToasterService} from '../../shared';
import { ReportUtilService} from '../../shared/report/report-util.service';
import { Internal, InternalService } from '../internal';
import { Customer, CustomerService } from '../customer';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-return-sales-order-edit',
    templateUrl: './return-sales-order-edit.component.html'
})
export class ReturnSalesOrderEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    returnSalesOrder: ReturnSalesOrder;
    isSaving: boolean;
    idOrder: any;
    paramPage: number;
    routeId: number;

    internals: Internal[];

    customers: Customer[];

    billtos: BillTo[];

    constructor(
        protected alertService: JhiAlertService,
        protected returnSalesOrderService: ReturnSalesOrderService,
        protected internalService: InternalService,
        protected customerService: CustomerService,
        protected billToService: BillToService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected reportUtilService: ReportUtilService,
        protected toaster: ToasterService
    ) {
        this.returnSalesOrder = new ReturnSalesOrder();
        this.routeId = 0;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idOrder = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.returnSalesOrderService.find(this.idOrder).subscribe((returnSalesOrder) => {
            this.returnSalesOrder = returnSalesOrder;
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['return-sales-order', { page: this.paramPage }]);
        }
    }

    save() {
        this.isSaving = true;
        if (this.returnSalesOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.returnSalesOrderService.update(this.returnSalesOrder));
        } else {
            this.subscribeToSaveResponse(
                this.returnSalesOrderService.create(this.returnSalesOrder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ReturnSalesOrder>) {
        result.subscribe((res: ReturnSalesOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ReturnSalesOrder) {
        this.eventManager.broadcast({ name: 'returnSalesOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'returnSalesOrder saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'returnSalesOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
    print() {
        this.toaster.showToaster('info', 'Print Data', 'Printing.....');
        this.reportUtilService.viewFile('/api/report/sample/pdf');
    }

}
