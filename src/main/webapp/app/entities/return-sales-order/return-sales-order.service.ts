import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ReturnSalesOrder } from './return-sales-order.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ReturnSalesOrderService {
    protected itemValues: ReturnSalesOrder[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/return-sales-orders';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/return-sales-orders';

    constructor(protected http: Http) { }

    create(returnSalesOrder: ReturnSalesOrder): Observable<ReturnSalesOrder> {
        const copy = this.convert(returnSalesOrder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(returnSalesOrder: ReturnSalesOrder): Observable<ReturnSalesOrder> {
        const copy = this.convert(returnSalesOrder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<ReturnSalesOrder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(returnSalesOrder: ReturnSalesOrder, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(returnSalesOrder);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<ReturnSalesOrder> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<ReturnSalesOrder[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ReturnSalesOrder.
     */
    protected convertItemFromServer(json: any): ReturnSalesOrder {
        const entity: ReturnSalesOrder = Object.assign(new ReturnSalesOrder(), json);
        return entity;
    }

    /**
     * Convert a ReturnSalesOrder to a JSON which can be sent to the server.
     */
    protected convert(returnSalesOrder: ReturnSalesOrder): ReturnSalesOrder {
        if (returnSalesOrder === null || returnSalesOrder === {}) {
            return {};
        }
        // const copy: ReturnSalesOrder = Object.assign({}, returnSalesOrder);
        const copy: ReturnSalesOrder = JSON.parse(JSON.stringify(returnSalesOrder));
        return copy;
    }

    protected convertList(returnSalesOrders: ReturnSalesOrder[]): ReturnSalesOrder[] {
        const copy: ReturnSalesOrder[] = returnSalesOrders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ReturnSalesOrder[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
