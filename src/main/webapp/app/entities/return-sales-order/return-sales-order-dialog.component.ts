import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ReturnSalesOrder } from './return-sales-order.model';
import { ReturnSalesOrderPopupService } from './return-sales-order-popup.service';
import { ReturnSalesOrderService } from './return-sales-order.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { Customer, CustomerService } from '../customer';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-return-sales-order-dialog',
    templateUrl: './return-sales-order-dialog.component.html'
})
export class ReturnSalesOrderDialogComponent implements OnInit {

    returnSalesOrder: ReturnSalesOrder;
    isSaving: boolean;
    idInternal: any;
    idCustomer: any;
    idBillTo: any;

    internals: Internal[];

    customers: Customer[];

    billtos: BillTo[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected returnSalesOrderService: ReturnSalesOrderService,
        protected internalService: InternalService,
        protected customerService: CustomerService,
        protected billToService: BillToService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.returnSalesOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.returnSalesOrderService.update(this.returnSalesOrder));
        } else {
            this.subscribeToSaveResponse(
                this.returnSalesOrderService.create(this.returnSalesOrder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ReturnSalesOrder>) {
        result.subscribe((res: ReturnSalesOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ReturnSalesOrder) {
        this.eventManager.broadcast({ name: 'returnSalesOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'returnSalesOrder saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'returnSalesOrder Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}

@Component({
    selector: 'jhi-return-sales-order-popup',
    template: ''
})
export class ReturnSalesOrderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected returnSalesOrderPopupService: ReturnSalesOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.returnSalesOrderPopupService
                    .open(ReturnSalesOrderDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.returnSalesOrderPopupService.parent = params['parent'];
                this.returnSalesOrderPopupService
                    .open(ReturnSalesOrderDialogComponent as Component);
            } else {
                this.returnSalesOrderPopupService
                    .open(ReturnSalesOrderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
