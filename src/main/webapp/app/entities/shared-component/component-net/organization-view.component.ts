import { Component, Input, OnInit, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { PostalAddress } from '../../postal-address';

import { Organization, OrganizationService } from '../../organization';
import { CommonUtilService } from '../../../shared';

@Component({
    selector: 'jhi-organization-view-net',
    templateUrl: './organization-view.component.html'
})
export class OrganizationViewNetComponent implements OnInit, OnDestroy, OnChanges {

    @Input() organization: Organization = new Organization();
    @Input() readonly: boolean;
    public yearForCalendar: string

    constructor(
        private commonUtilService: CommonUtilService
    ) {
        this.yearForCalendar = this.commonUtilService.getYearRangeForCalendar();
    }

    ngOnInit() { }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['organization']) {
            console.log('organization view global', this.organization);
            if (this.organization.dateOrganizationEstablish !== null ) {
                this.organization.dateOrganizationEstablish = new Date(this.organization.dateOrganizationEstablish);
            }
        }
    }

    ngOnDestroy() {
    }

    getProvince(province: any) {
        this.organization.postalAddress.provinceId = province;
    }

    getDistrict(district: any) {
        this.organization.postalAddress.districtId = district;
    }

    getCity(city: any) {
        this.organization.postalAddress.cityId = city;
    }

    getVillage(village: any ) {
        this.organization.postalAddress.villageId = village;
    }
}
