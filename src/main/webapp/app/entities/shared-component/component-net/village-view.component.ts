import { Component, Input, Output, SimpleChanges, OnInit, OnChanges, EventEmitter } from '@angular/core';
import { PostalAddress } from '../../postal-address';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { JhiAlertService } from 'ng-jhipster';
import { Village } from '../../village/village.model';
import { VillageService } from '../../village/village.service';
import { ResponseWrapper } from '../../../shared';

@Component({
    selector: 'jhi-village-view-net',
    templateUrl: './village-view.component.html'
})

export class VillageViewNetComponent implements OnInit, OnChanges {

    @Input() village: any;
    @Input() district: any;
    @Input() readonly = false;
    @Output() select = new EventEmitter<any>();

    villages: Village[];

    constructor(
        private alertService: JhiAlertService,
        private villageService: VillageService
    ) {

    }

    ngOnInit() {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['district']) {
            if (this.district != null && this.district !== undefined) {
                this.resetData();
                this.loadAll();
            }
        }
    }

    loadAll() {
        this.villageService.getByDistrict(this.district).subscribe(
            (res: ResponseWrapper) => {this.onSuccess(res.json)},
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    doSelect(event) {
        if (this.select != null) {
            this.select.emit(this.village);
        }
    }

    trackById(index: number, item: Village) {
        return item.description;
    }

    private onSuccess(data) {
        this.villages = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    private resetData(): void {
        this.villages = new Array<Village>();
    }
}
