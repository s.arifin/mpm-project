import { Component, Input, Output, OnInit, SimpleChanges, OnChanges, EventEmitter } from '@angular/core';
import { PostalAddress } from '../../postal-address';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { JhiAlertService } from 'ng-jhipster';
import { Province } from '../../province/province.model';
import { ProvinceService } from '../../province/province.service';
import { ResponseWrapper } from '../../../shared';

@Component({
    selector: 'jhi-province-view-net',
    templateUrl: './province-view.component.html'
})

export class ProvinceViewNetComponent implements OnInit, OnChanges {

    @Input() province: string;
    @Input() readonly = false;
    @Output() select = new EventEmitter<any>();

    provinces: Province[];

    constructor(
        private alertService: JhiAlertService,
        private provinceService: ProvinceService
    ) {

    }

    ngOnInit() {
        this.loadAll();
    }

    loadAll() {
        this.provinceService.getAll().subscribe(
            (res: ResponseWrapper) => {this.onSuccess(res.json)},
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnChanges(changes: SimpleChanges): void {}

    doSelect(event) {
        if (this.select != null) {
            this.select.emit(this.province);
        }
    }

    trackById(index: number, item: Province) {
        return item.description;
    }

    private onSuccess(data) {
        this.provinces = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
