import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CurrencyPipe, DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiAlertService } from 'ng-jhipster';

import { Principal, ResponseWrapper } from '../../../shared';
import { CommunicationEventProspect } from '../../communication-event-prospect/communication-event-prospect.model';
import { CommunicationEventProspectService } from '../../communication-event-prospect/communication-event-prospect.service';

import { LazyLoadEvent } from 'primeng/primeng';
import { ConfirmationService } from 'primeng/primeng';

import { FollowUpHistory } from '../entity/follow-up-history.model';
import { ProspectPerson, ProspectPersonService } from '../../prospect-person';

import * as _ from 'lodash';
import { Prospect } from '../../prospect/index';

@Component({
    selector: 'jhi-communication-event-prospect-as-history-follow-up',
    templateUrl: './communication-event-prospect-as-history-follow-up.component.html'
})
export class CommunicationEventProspectAsHistoryFollowUpComponent implements OnChanges {

    @Input()
    idProspect: string;

    currentAccount: any;
    communicationEventProspects: CommunicationEventProspect[];
    prospectPerson: ProspectPerson;
    followUpHistory: FollowUpHistory;
    error: any;
    success: any;

    constructor(
        private communicationEventProspectService: CommunicationEventProspectService,
        private prospectPersonService: ProspectPersonService,
        private alertService: JhiAlertService,
        private datePipe: DatePipe,
    ) {
        this.followUpHistory = new FollowUpHistory();
        this.prospectPerson = new ProspectPerson();
        console.log('cek_mew', this.communicationEventProspects);
    }

    public loadAll(): void {
        this.communicationEventProspectService.query({
            idprospect: this.idProspect
        }).subscribe(
            (res: ResponseWrapper) => {
                this.communicationEventProspects = res.json;
                this.prospectPersonService.find(this.idProspect).subscribe((prospectPerson) => {
                    this.prospectPerson = prospectPerson;
                    this.summaryHistory();
                });
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idProspect']) {
            if (this.idProspect !== undefined && this.idProspect !== null) {
                this.loadAll();
            }
        }
    }

    private onSuccess(data, headers) {
        this.communicationEventProspects = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    private summaryHistory() {
        if (this.communicationEventProspects) {

            this.followUpHistory.lastFollowUp = this.datePipe.transform(this.prospectPerson.dateFollowUp.toString(), 'MMM d, y');
            this.followUpHistory.lastStatus = this.convertStatus(this.prospectPerson.currentStatus);
            console.log('cek_mew', this.communicationEventProspects);
            // this.countHistory();
            this.followUpHistory.phoneHistory = _.sumBy(this.communicationEventProspects, (i) => (i.eventTypeId === 151 ? 1 : 0));
            this.followUpHistory.visitHistory = _.sumBy(this.communicationEventProspects, (i) => (i.eventTypeId === 152 ? 1 : 0));
            this.followUpHistory.walkInHistory = _.sumBy(this.communicationEventProspects, (i) => (i.eventTypeId === 154 ? 1 : 0) + (i.eventTypeId === 155 ? 1 : 0) + (i.eventTypeId === 156 ? 1 : 0));
            console.log('cek_mew', this.communicationEventProspects);
        }
    }

    private countHistory() {

        this.followUpHistory.phoneHistory = 0;
        this.followUpHistory.visitHistory = 0;
        this.followUpHistory.walkInHistory = 0;

        this.communicationEventProspects.forEach(
            (i) => {
                if (i.eventTypeId === 151) {
                    this.followUpHistory.phoneHistory++;
                } else if (i.eventTypeId === 152) {
                    this.followUpHistory.visitHistory++;
                } else if (i.eventTypeId === 153) {
                    this.followUpHistory.walkInHistory++;
                }
            }
        );
    }

    private convertStatus(id: number): string {
        let result = '';

        switch (id) {
            case 51:
                result = 'LOW';
                break;
            case 52:
                result = 'MEDIUM';
                break;
            case 53:
                result = 'HOT';
                break;
            default:
                result = 'NONE'
          }
        return result;
    }
}
