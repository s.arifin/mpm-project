import { Component, Input, OnChanges, SimpleChanges  } from '@angular/core';
import { Person } from '../../person';
import { PersonalCustomerService } from '../';

@Component({
    selector: 'jhi-customer-view',
    templateUrl: './customer-view.component.html'
})

export class CustomerViewComponent implements OnChanges {
    @Input() idCustomer: string;
    @Input() readonly: boolean;

    person: Person;

    constructor(
        private personalCustomerService: PersonalCustomerService
    ) {
        this.person = new Person();
        this.readonly = false;
    }

    ngOnChanges(changes: SimpleChanges ) {
        if (this.idCustomer !== undefined) {
            this.personalCustomerService.find(this.idCustomer).subscribe((res) => {
                if (res.person.dob !== null) {
                    res.person.dob = new Date(res.person.dob);
                }

                this.person = res.person;
            });
        }
    }
}
