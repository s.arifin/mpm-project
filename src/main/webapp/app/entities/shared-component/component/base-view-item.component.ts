import { Component, OnInit, OnDestroy, OnChanges, Input, Output, SimpleChanges, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';
import { BaseEntity } from '../';

export class BaseViewItemComponent<T> {

    @Input() item: T;
    @Output() process = new EventEmitter<T>();
    @Output() cancel = new EventEmitter<T>();
    @Output() close = new EventEmitter<T>();

    constructor() {
    }

    doProcess() {
        if (this.process != null) {
            this.process.emit(this.item);
        }
    }

    doCancel() {
        if (this.cancel != null) {
            this.cancel.emit(this.item);
        }
    }

    doClose() {
        if (this.close != null) {
            this.close.emit(this.item);
        }
    }

}
