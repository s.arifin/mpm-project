import { Component, OnInit, Input, OnDestroy, OnChanges, SimpleChanges  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CommunicationEventDelivery } from '../../communication-event-delivery/communication-event-delivery.model';
import { CommunicationEventDeliveryPopupService } from '../../communication-event-delivery/communication-event-delivery-popup.service';
import { CommunicationEventDeliveryService } from '../../communication-event-delivery/communication-event-delivery.service';

@Component({
    selector: 'jhi-communication-event-delivery-as-list',
    templateUrl: './communication-event-delivery-as-list.component.html'
})
export class CommunicationEventDeliveryAsListComponent implements OnInit {
    @Input() idCostumer: string;
    communicationEventDelivery: CommunicationEventDelivery;
    isSaving: boolean;
    arraytmp: any[];

    constructor(
        // public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private communicationEventDeliveryService: CommunicationEventDeliveryService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.communicationEventDelivery = new CommunicationEventDelivery,
        this.communicationEventDelivery.idCustomer = this.idCostumer;
        this.isSaving = false;
        this.loadAll();
    }

    // ngOnChanges(changes: SimpleChanges) {
    //     if (changes['idCostumer']) {
    //         if (this.idCostumer !== undefined) {
    //             this.loadAll();
    //         }
    //     }
    // }
    public loadAll(): void {
        console.log('input shared id cos', this.idCostumer);
    }

    clear() {
        // this.activeModal.dismiss('cancel');
    }

    save() {
        // const _arr = Array();
        console.log('this.arraytmp', this.arraytmp);
        if (this.arraytmp.length > 0) {
        //     for (let i = 0; i < this.communicationEventDelivery.c3Option.length; i++) {
        //         const obj = this.communicationEventDelivery.c3Option[i];
                // if (obj.parentId === parentId) {
                    // this.communicationEventDelivery.c3Option.push(obj);
                // }
            // }
            this.communicationEventDelivery.c3Option = this.arraytmp.join(',');
        } else {
        }

        this.isSaving = true;
        if (this.communicationEventDelivery.id !== undefined) {
            this.subscribeToSaveResponse(
                this.communicationEventDeliveryService.update(this.communicationEventDelivery));
        } else {
            this.subscribeToSaveResponse(
                this.communicationEventDeliveryService.create(this.communicationEventDelivery));
        }
    }

    private subscribeToSaveResponse(result: Observable<CommunicationEventDelivery>) {
        result.subscribe((res: CommunicationEventDelivery) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CommunicationEventDelivery) {
        this.eventManager.broadcast({ name: 'communicationEventDeliveryListModification', content: 'OK'});
        this.isSaving = false;
        // this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

// @Component({
//     selector: 'jhi-communication-event-delivery-popup',
//     template: ''
// })
// export class CommunicationEventDeliveryPopupComponent implements OnInit, OnDestroy {

//     routeSub: any;

//     constructor(
//         private route: ActivatedRoute,
//         private communicationEventDeliveryPopupService: CommunicationEventDeliveryPopupService
//     ) {}

//     ngOnInit() {
//         this.routeSub = this.route.params.subscribe((params) => {
//             if ( params['id'] ) {
//                 this.communicationEventDeliveryPopupService
//                     .open(CommunicationEventDeliveryDialogComponent as Component, params['id']);
//             } else {
//                 this.communicationEventDeliveryPopupService
//                     .open(CommunicationEventDeliveryDialogComponent as Component);
//             }
//         });
//     }

//     ngOnDestroy() {
//         this.routeSub.unsubscribe();
//     }
// }
