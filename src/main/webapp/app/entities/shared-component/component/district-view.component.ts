import { Component, Input, Output, SimpleChanges, OnInit, OnChanges, EventEmitter } from '@angular/core';
import { PostalAddress } from '../../postal-address';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { JhiAlertService } from 'ng-jhipster';
import { District } from '../../district/district.model';
import { DistrictService } from '../../district/district.service';
import { ResponseWrapper } from '../../../shared';

@Component({
    selector: 'jhi-district-view',
    templateUrl: './district-view.component.html'
})

export class DistrictViewComponent implements OnInit, OnChanges {
    @Input() district: any;
    @Input() city: any;
    @Input() readonly: Boolean = false;
    @Input() size: Number;
    @Output() select = new EventEmitter<any>();

    districts: District[];

    constructor(
        private alertService: JhiAlertService,
        private districtService: DistrictService
    ) {

    }

    ngOnInit() {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['city']) {
            if (this.city != null && this.city !== undefined) {
                this.resetData();
                this.loadAll();
            }
        }
    }

    loadAll() {
        this.districtService.queryByCity({
                idcity: this.city,
                size: this.size,
                sort: ['description', 'asc']
        }).subscribe(
            (res: ResponseWrapper) => {this.onSuccess(res.json)},
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    doSelect(event) {
        if (this.select != null) {
            this.select.emit(this.district);
        }
    }

    trackById(index: number, item: District) {
        return item.description;
    }

    private onSuccess(data) {
        this.districts = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    private resetData(): void {
        this.districts = new Array<District>();
    }
}
