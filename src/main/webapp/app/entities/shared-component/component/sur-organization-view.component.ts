import { Component, Input, OnChanges, OnDestroy, SimpleChanges  } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Person } from '../../person';
import { PersonalCustomerService } from '../';
import { OrganizationCustomerService, OrganizationCustomer } from '../../organization-customer';
import { SalesUnitRequirement, SalesUnitRequirementService } from '../../sales-unit-requirement';

@Component({
    selector: 'jhi-sur-organization-view',
    templateUrl: './sur-organization-view.component.html'
})
export class SurOrganizationViewComponent implements OnChanges, OnDestroy {

    @Input() idRequirement: string;

    customer: OrganizationCustomer;
    sur: SalesUnitRequirement;

    constructor(
        // private service: SalesUnitRequirementService,
        // private serviceCustomer: OrganizationCustomerService
    ) {
    }

    load() {
        // this.service.find(this.idRequirement)
        // .flatMap((data) => {
        //     this.sur = data;
        //     return this.serviceCustomer.find(this.sur.customerId);
        // })
        // .subscribe(
        //     // Jika ada
        //     (value: OrganizationCustomer) => {this.customer = value; },
        //     // Jika error, atau tidak ada
        //     (error: any) => {this.customer = new OrganizationCustomer(); },
        //     // Jika sudah selesai
        //     () => {}
        // );
    }

    ngOnChanges(changes: SimpleChanges ) {
        if (changes['idRequirement']) {
            this.load();
        }
    }

    ngOnDestroy() {
        if (this.customer != null) {
            //
        }
    }
}
