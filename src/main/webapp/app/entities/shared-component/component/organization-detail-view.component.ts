import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Organization, OrganizationService } from '../../organization';
import { PostalAddress } from '../../postal-address';
import { Person } from '../../person';
import { ResponseWrapper, ToasterService } from '../../../shared';

import { ProspectOrganization } from '../../prospect-organization/prospect-organization.model';
import { ProspectOrganizationService } from '../../prospect-organization/prospect-organization.service';

@Component({
    selector: 'jhi-organization-detail-view',
    templateUrl: './organization-detail-view.component.html'
})
export class OrganizationDetailViewComponent implements OnChanges {

    // @Input() idProspect: string;
    @Input() organization: Organization = new Organization();
    @Input() pic: Person = new Person();
    @Input() addressTDP: PostalAddress = new PostalAddress();
    @Input() readonly: boolean;

    // @Input() idOrganization: string;

     constructor() {
     }

    // organizationDetail: OrganizationDetails;
    // organization: Organization;

    // prospectOrganization: ProspectOrganization;

    // constructor(
    //     private prospectOrganizationService: ProspectOrganizationService,
    //     private alertService: JhiAlertService,
    //     private organizationDetailsService: OrganizationDetailsService,
    //     private organizationService: OrganizationService,
    //     private toaster: ToasterService,
    // ) {
    //     this.prospectOrganization = new ProspectOrganization();
    //     this.organizationDetail = new OrganizationDetails();
    //     this.organization = new Organization();
    // }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['organizationDetail']) {
            console.log('organization', this.organization);
        }
    }

    getProvince(province: any) {
        this.organization.postalAddress.province = province;
    }

    getDistrict(district: any) {
        this.organization.postalAddress.district = district;
    }

    getCity(city: any) {
        this.organization.postalAddress.city = city;
    }

    getVillage(village: any ) {
        this.organization.postalAddress.village = village;
    }
    getProvinceTDP(province: any) {
        this.addressTDP.provinceId = province;
    }

    getDistrictTDP(district: any) {
        this.addressTDP.districtId = district;
    }

    getCityTDP(city: any) {
        this.addressTDP.cityId = city;
    }

    getVillageTDP(village: any) {
        this.addressTDP.villageId = village;
    }

    // private onError(error) {
    //     this.toaster.showToaster('warning', 'prospectOrganization Changed', error.message);
    //     this.alertService.error(error.message, null, null);
    // }
}
