import { Component, Input, Output, SimpleChanges, OnInit, OnChanges, EventEmitter } from '@angular/core';
import { PostalAddress } from '../../postal-address';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { JhiAlertService } from 'ng-jhipster';
import { City } from '../../city/city.model';
import { CityService } from '../../city/city.service';
import { ResponseWrapper } from '../../../shared';

@Component({
    selector: 'jhi-city-view',
    templateUrl: './city-view.component.html'
})

export class CityViewComponent implements OnInit, OnChanges {

    @Input() city: any;
    @Input() province: any;
    @Input() readonly: Boolean = false;
    @Output() select = new EventEmitter<any>();

    cities: City[];

    constructor(
        private alertService: JhiAlertService,
        private cityService: CityService
    ) {

    }

    ngOnInit() {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['province']) {
            if (this.province != null && this.province !== undefined) {
                this.resetData();
                this.loadAll();
            }
        }
    }

    loadAll() {
        this.cityService.queryByProvince({
            idprovince: this.province,
            sort: ['description', 'asc']
        }).subscribe(
            (res: ResponseWrapper) => {this.onSuccess(res.json)},
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    doSelect(event) {
        if (this.select != null) {
            this.select.emit(this.city);
        }
    }

    trackById(index: number, item: City) {
        return item.description;
    }

    private onSuccess(data) {
        this.cities = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    private resetData(): void {
        this.cities = new Array<City>();
    }
}
