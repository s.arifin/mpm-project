import { Component, Input, OnInit, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { Person, PersonService } from '../../person';
import { ResponseWrapper } from '../../../shared';

@Component({
    selector: 'jhi-person-base-view',
    templateUrl: './person-base-view.component.html'
})
export class PersonBaseViewComponent implements OnInit, OnDestroy, OnChanges {

    @Input() person: Person = new Person();
    @Input() readonly = false;

    genders: Array<object> = [
        {label : 'Pria', value : 'P'},
        {label : 'Wanita', value : 'W'}
    ];

    constructor(
    ) {}

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['person']) {
            console.log('this.person', this.person);
            if (this.person.dob !== null) {
                this.person.dob = new Date(this.person.dob);
            }
        }
    }

    ngOnDestroy() {
    }

}
