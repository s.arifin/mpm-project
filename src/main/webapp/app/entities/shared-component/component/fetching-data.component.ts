import { Component, Input, Output, SimpleChanges, OnInit } from '@angular/core';
import { PostalAddress } from '../../postal-address';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

@Component({
    selector: 'jhi-fetching-data',
    templateUrl: './fetching-data.component.html'
})

export class FetchingDataComponent implements OnInit {

    constructor(

    ) {

    }

    ngOnInit() {

    }
}
