import { BaseEntity } from '../';

export class FollowUpQuestion implements BaseEntity {
    constructor(
        public id?: number,
        public answer01?: string,
        public answer02?: string,
        public answer03?: string,
        public answer04?: string,
        public answer05?: string,
        public note?: string
    ) {
    }
}
