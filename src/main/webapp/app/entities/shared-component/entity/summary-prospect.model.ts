import { BaseEntity } from '../';

export class SummaryProspect implements BaseEntity {
    constructor(
        public id?: Number,
        public salesmanId?: String,
        public salesmanName?: String,
        public hotCount?: Number,
        public mediumCount?: Number,
        public lowCount?: Number
    ) {

    }
}
