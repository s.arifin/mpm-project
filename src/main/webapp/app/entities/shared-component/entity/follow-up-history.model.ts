import { BaseEntity } from '../';

export class FollowUpHistory implements BaseEntity {
    constructor(
        public id?: number,
        public phoneHistory?: number,
        public visitHistory?: number,
        public walkInHistory?: number,
        public lastFollowUp?: string,
        public lastStatus?: string,
        public note?: string
    ) {
    }
}
