import { BaseEntity } from '../';

export class ProductCategory implements BaseEntity {
    constructor(
        public id?: number,
        public idCategory?: number,
        public description?: string,
        public categoryTypeId?: any,
        public products?: any,
    ) {
    }
}
