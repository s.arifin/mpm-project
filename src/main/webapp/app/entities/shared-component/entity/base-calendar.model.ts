import { BaseEntity } from '../';

export class BaseCalendar implements BaseEntity {
    constructor(
        public id?: number,
        public idCalendar?: number,
        public dateKey?: any,
        public description?: string,
        public dateFrom?: any,
        public dateThru?: any,
        public calendarTypeId?: any,
        public parentId?: any,
    ) {
    }
}
