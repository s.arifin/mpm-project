import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleSalesOrder, VehicleSalesOrderService } from '../vehicle-sales-order';
import { OrdersParameter } from '../orders/orders-parameter.model'
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { SelectItem } from 'primeng/primeng';
import { Salesman, SalesmanService} from '../salesman';

import { LeasingCompanyService, LeasingCompany } from '../leasing-company';
import { LoadingService } from '../../layouts/loading/loading.service';

import { RuleHotItemService } from '../rule-hot-item/rule-hot-item.service';
import { Observable } from 'rxjs/Observable';
import * as BaseConstant from '../../shared/constants/base.constants';
import { SalesUnitRequirement, SalesUnitRequirementService } from '../sales-unit-requirement';

@Component({
    selector: 'jhi-approval-leasing-vso-draft',
    templateUrl: './approval-leasing-vso-draft.component.html'
})

export class ApprovalLeasingVSODraftComponent implements OnInit, OnDestroy {
    currentAccount: any;
    salesUnitRequirements: SalesUnitRequirement[]
    ordersParameter: OrdersParameter;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    currentSearching: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    tgl1: Date;
    tgl2: Date;
    // tglAwal:string;
    // tglAkhir:string;
    leasings: SelectItem[];
    selectedLeasing: string;
    // korsals: SelectItem[];
    korsals: Salesman[];
    salesmans: Salesman[];
    selectedKorsal: string;
    selectedSalesman: string;
    statusAL: SelectItem[];
    selectedStatusVso: Number;
    isLazyLoadingFirst: Boolean ;
    isMatchingMenu: Boolean ;
    isFiltered: Boolean;
    isSearch: Boolean = false;
    pc: any;
    ip: any;
    selectedStatusAL: any;

    constructor(
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private leasingCompanyService: LeasingCompanyService,
        private loadingService: LoadingService,
        private ruleHotItemService: RuleHotItemService,
        private toaster: ToasterService,
        private salesmanService: SalesmanService,
        private salesUnitRequirementService: SalesUnitRequirementService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        // this.currentSearching = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';

        this.leasings = [];

        this.statusAL = [];
        this.statusAL.push({label: 'OPEN', value: 70});
        this.statusAL.push({label: 'APPROVE', value: 61 });
        this.statusAL.push({label: 'NOT APPROVE', value: 62 });

        this.tgl1 = new Date();
        this.tgl2 = new Date();

        this.ordersParameter = new OrdersParameter();
        this.korsals = [];
        this.salesmans = [];
        this.isFiltered = false;
        this.isMatchingMenu = false;
        this.pc = new RTCPeerConnection({iceServers: []});
        this.ip = null;

    }

    loadLeasing() {
        this.leasingCompanyService.query({
            page: 0,
            size: 100}).subscribe(
            (res: ResponseWrapper) => this.leasings = res.json,
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadAll() {
        this.getIP();
        this.loadingService.loadingStart();
        if (this.isFiltered) {
            this.salesUnitRequirementService.queryApprovalLeasingFilter({
                ordersPTO: this.ordersParameter ,
                data: this.currentSearching,
                size: this.itemsPerPage,
                // sort: this.sort()
            }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => this.loadingService.loadingStop(),
                );

            return;
        }
        if (this.isSearch) {
            console.log('SEARCH');
            this.salesUnitRequirementService.queryFilterBy({
                page: this.page - 1,
                queryFor: 'approvalLeasing',
                data: this.currentSearching,
                size: this.itemsPerPage,
                // sort: this.sort()
            }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => this.loadingService.loadingStop(),
                );

            return;
        }
        this.salesUnitRequirementService.queryApprovalLeasing({
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
        })
        .subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers)
            }
        )
    }

    private  loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('tarik data korsal', res.json);
                this.korsals = res.json;
            }
        )
    }

    public selectSalesByKorsal(): void {
        const obj: object = {
            idCoordinator : this.selectedKorsal
        }
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('selected Sales' , res.json);
                this.salesmans = res.json;
            }
        )
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/approval-leasing'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
        console.log('is filter = ', this.isFiltered );
    }

    clear() {
        this.isSearch = false;
        this.currentSearching = null;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        // this.registerChangeInVehicleSalesOrders();
        // this.tglAwal = Date;
        // this.tglAkhir = Date;
        this.loadLeasing();
        this.loadKorsal()

    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
        // this.eventSubscriber.unsubscribe();
    }

    trackId(index: number, item: VehicleSalesOrder) {
        return item.idOrder;
    }

    trackleasingComById(index: number, item: LeasingCompany) {
        return item.idPartyRole;
    }

    trackkorsalById(index: number, item: Salesman) {
        return item.idPartyRole;
    }

    trackStatusVsoById(index: number, item: any) {
        return item.value;
    }

    registerChangeInVehicleSalesOrders() {
        this.eventSubscriber = this.eventManager.subscribe('approvalLeasingModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateEntry') {
            result.push('dateEntry');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.salesUnitRequirements = data;
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    executeProcess(data) {
        this.vehicleSalesOrderService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        if (this.isLazyLoadingFirst === false) {
            this.isLazyLoadingFirst = true;
            return ;
        }
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleSalesOrderService.update(event.data)
                .subscribe((res: VehicleSalesOrder) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleSalesOrderService.create(event.data)
                .subscribe((res: VehicleSalesOrder) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: VehicleSalesOrder) {
        this.toasterService.showToaster('info', 'VehicleSalesOrder Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleSalesOrderService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'vehicleSalesOrderListModification',
                    content: 'Deleted an vehicleSalesOrder'
                    });
                });
            }
        });
    }

    getDescription(currentStatus: number) {
        return this.vehicleSalesOrderService.getDescriptionStatus(currentStatus);
    }

    getStatusMotor(idProduct: string) {
        const hasil = 'Biasa';

        console.log('Id product : ', idProduct);
        // this.ruleHotItemService.findAllByidProduct(idProduct)
        //     .subscribe(
        //         (res) => console.log(res),
        //         (res) => console.log(res),
        // );
        // var isCekHotItm =  this.cekIfHotItem(idProduct);
        // if ( isCekHotItm !== ''){
        //     hasil = isCekHotItm;
        //     console.log('Result is hot item');
        // }
        return hasil ;
    }

    cekIfHotItem(id: string) {
        this.ruleHotItemService.query({
            idProduct: id,
            page: 0,
            size: 1})
        .toPromise()
        .then((data) => {
            return (data.json)
        });
        return '';
    };

    subscribeToSaveResponse(result: Observable<VehicleSalesOrder>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe((res: VehicleSalesOrder) => {
                    resolve();
                });
            });
        }

    batalVso(vso: VehicleSalesOrder ) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to cancel vso [' + vso.salesUnitRequirement.requirementNumber + '] ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.subscribeToSaveResponse(this.vehicleSalesOrderService.executeProcess(13, null, vso))
                .then(
                    () => {
                        this.eventManager.broadcast({
                        name: 'vehicleSalesOrderListModification',
                        content: 'cancel an vehicleSalesOrder'
                        });
                    this.toaster.showToaster('info', 'Data Proces', 'Cancel VSO');
                    this.loadAll();
                    }
                );
                // this.vehicleSalesOrderService.executeProcess(13, null, vso)
                //     .subscribe((response) => {
                //         this.eventManager.broadcast({
                //             name: 'vehicleSalesOrderListModification',
                //             content: 'cancel an vehicleSalesOrder'
                //             });
                //         this.toaster.showToaster('info', 'Data Proces', 'Cancel VSO');
                //     });
            }
        });
    }
    buildReindex() {
        this.vehicleSalesOrderService.process({command: 'buildIndexVSO'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }

        getStatus(statusValue: Number): String {

        let hasil = null;

         switch (statusValue) {
            case 70 :
                hasil = 'OPEN';
                return hasil;
            case 75 :
                hasil = 'OPEN';
                return hasil;
            case 61 :
                hasil = 'APPROVE';
                return hasil;
            case 62 :
                hasil = 'NOT APPROVE';
                return hasil;
            default :
                hasil = 'UNDIFINED STATUS ' + statusValue;
                return hasil;
        }
    }

    search(query) {
        this.isSearch = true;
        if (this.isSearch) {
            console.log('SEARCH');
            this.salesUnitRequirementService.queryFilterBy({
                page: this.page - 1,
                queryFor: 'approvalLeasing',
                data: query,
                size: this.itemsPerPage,
                // sort: this.sort()
            }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => this.loadingService.loadingStop(),
                );

            return;
        }
        this.loadAll();
    }

    filter() {
        this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered );
        this.ordersParameter = new OrdersParameter();
        this.ordersParameter.internalId = this.principal.getIdInternal();
        if ( this.selectedLeasing != null && this.selectedLeasing !== undefined) {
                this.ordersParameter.leasingCompanyId = this.selectedLeasing;
        }

        if (this.selectedSalesman !== null && this.selectedSalesman !== undefined) {
            this.ordersParameter.salesmanId = this.selectedSalesman;
        }

        if ( this.selectedStatusVso != null && this.selectedStatusVso !== undefined) {
                this.ordersParameter.status = [this.selectedStatusVso];
                this.ordersParameter.statusNotIn = [0];
            }
        if ( (this.ordersParameter.orderDateFrom != null || this.ordersParameter.orderDateFrom !== undefined) &&
              (this.ordersParameter.orderDateThru != null || this.ordersParameter.orderDateThru !== undefined)) {
                this.ordersParameter.orderDateFrom = this.tgl1.toJSON();
                this.ordersParameter.orderDateThru = this.tgl2.toJSON();
        }
        if ( this.selectedStatusAL !== null || this.selectedStatusAL !== '') {
            this.ordersParameter.statusAL = [this.selectedStatusAL];
        }
        this.salesUnitRequirementService.queryApprovalLeasingFilter({
            ordersPTO: this.ordersParameter ,
            data: this.currentSearching,
            size: this.itemsPerPage,
            // sort: this.sort()
        }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json),
                () => this.loadingService.loadingStop(),
            );

        console.log('tanggal 1', this.ordersParameter.orderDateFrom);
        console.log('tanggal 2', this.ordersParameter.orderDateThru);
    }

    getIP() {
            // // this.pc = new RTCPeerConnection({iceServers: []});
            // const noop = function(){};
            // this.pc.createDataChannel('');
            // this.pc.createOffer(this.pc.setLocalDescription.bind(this.pc), noop);
            // this.pc.onicecandidate = (ice) => {
            // if (!ice || !ice.candidate || !ice.candidate.candidate) {
            //     return;
            // }
            //         const myIP = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate)[1];
            //         this.ip = myIP.toString();
            //         console.log('my IP: ', myIP);
            //         console.log('IP: ', this.ip);
            // };
            // console.log('IP bawah ==:> ', this.ip);
    }
}
