import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ApprovalLeasingVSODraftComponent } from './approval-leasing-vso-draft.component';
import { ApprovalLeasingDetailComponent } from './approval-leasing-detail.component';
import { ApprovalEmailLeasingDetailComponent } from './approval-leasing-email/approval-email-leasing-detail.component';
import { KorsalRequestAttachmentComponent } from './korsal-request-attachment.component';

@Injectable()
export class ApprovalLeasingResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        // const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'orderNumber,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            // predicate: this.paginationUtil.parsePredicate(sort),
            // ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const approvalLeasingRoute: Routes = [
    {
        path: 'approval-leasing',
        component: ApprovalLeasingVSODraftComponent,
        resolve: {
            'pagingParams': ApprovalLeasingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval.leasing'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request-attachment',
        component: KorsalRequestAttachmentComponent,
        resolve: {
            'pagingParams': ApprovalLeasingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval.leasingkorsal'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-leasing-detail/:id/:isafc',
        component: ApprovalLeasingDetailComponent,
        resolve: {
            'pagingParams': ApprovalLeasingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval.leasing'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-email-leasing-detail/:key',
        component: ApprovalEmailLeasingDetailComponent,
        data: {
            authorities: [],
            pageTitle: 'mpmApp.approval.leasing'
        },
        canActivate: [UserRouteAccessService]
        }
];
