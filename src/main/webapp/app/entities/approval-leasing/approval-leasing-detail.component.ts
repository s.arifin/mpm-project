import { Component, OnInit, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { SalesUnitRequirementService, SalesUnitRequirement } from '../sales-unit-requirement';
import { PersonalCustomerService, PersonalCustomer } from '../shared-component';
import { Person } from '../person';
import { ConfirmationService } from 'primeng/primeng';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService, ToasterService } from '../../shared';
import { ApprovalService } from '../approval/approval.service';
import { Approval } from '../approval/approval.model';

@Component({
    selector: 'jhi-approval-leasing-detail',
    templateUrl: './approval-leasing-detail.component.html'
})
export class ApprovalLeasingDetailComponent implements OnInit, OnChanges {
    public subscription: Subscription;
    public idReq: any;
    public salesUnitRequirement: SalesUnitRequirement;
    public personalCustomer: PersonalCustomer;
    public isReadOnlyDocument: Boolean = false;
    public idParty: string;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    currentSearch: string;
    routeData: any;
    approval: Approval;
    isAfc: Boolean;
    userLogin: any;
    constructor(
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected route: ActivatedRoute,
        protected approvalService: ApprovalService,
        protected router: Router,
        protected personalCustomerService: PersonalCustomerService,
        private confirmationService: ConfirmationService,
        protected activatedRoute: ActivatedRoute,
        protected toaster: ToasterService,
        protected principal: Principal,
    ) {
        this.salesUnitRequirement = new SalesUnitRequirement();
        this.personalCustomer = new PersonalCustomer;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.approval = new Approval();
        this.isAfc = true;
        this.userLogin = null;
    }

    ngOnInit() {
        this.userLogin = this.principal.getUserLogin();
        console.log('ini user nama', this.userLogin);
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                    if (params['isafc'] === 'false') {
                        this.isAfc = false ;
                        console.log('param url = ', this.isAfc);
                    } else {
                        this.isReadOnlyDocument = true;
                    }
                this.salesUnitRequirementService.find(params['id']).subscribe(
                    (res) => {
                        this.salesUnitRequirement = res;
                        console.log('sur approval leasing = ', this.salesUnitRequirement);
                        this.getPersonalCustomer(this.salesUnitRequirement.customerId);
                        this.loadApproval(res.idRequirement)
                    }
                )
            }
        });

    }

    getPersonalCustomer(id: string): void {
        this.personalCustomerService.find(id).subscribe(
            (res) => {
                if (res.person.dob !== null) {
                    res.person.dob = new Date(res.person.dob);
                }
                console.log('aaaa', res);
                this.personalCustomer = res
                this.idParty = res.partyId;
            }
        )
    }

    loadApproval(idrequirement) {
        this.approvalService.getApprovalLeasing({
            idReq: idrequirement,
            queryFor: 'leasing',
            size: this.itemsPerPage,
            page: this.page - 1,
            sort: this.sort
        }).subscribe(
            (res) => {
                console.log('data approval dari sur = ', res);
                this.approval = res.json;
            }
        )
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    ngOnChanges(change: SimpleChanges) {
    }

    public save(): void {
        if (this.salesUnitRequirement.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.salesUnitRequirementService.update(this.salesUnitRequirement));
        } else {
            this.subscribeToSaveResponse(
                this.salesUnitRequirementService.create(this.salesUnitRequirement));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Approval>) {
        result.subscribe((res: Approval) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: SalesUnitRequirement) {
        // this.eventManager.broadcast({ name: 'salesUnitRequirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'salesUnitRequirement saved !');
        // this.isSaving = false;
        // this.activeModal.dismiss(result);
    }

    protected onSaveError() {
    }

    public approve() {
        this.salesUnitRequirement.ipAddress = this.userLogin;
        this.confirmationService.confirm({
            message: 'Apakah Yakin Untuk Approve',
            header: 'Konfirmasi',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.salesUnitRequirementService.executeProcess(112, null, this.salesUnitRequirement)
                .subscribe(
                    (res) => {
                        console.log('data berhasil di not approve = ', res);
                        this.previousState();
                    }
                )
                }
        })
    }

    public notApprove() {
        this.salesUnitRequirement.ipAddress = this.userLogin;
        this.confirmationService.confirm({
            message: 'Apakah Yakin Untuk Not Approve',
            header: 'Konfirmasi',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.salesUnitRequirementService.executeProcess(113, null, this.salesUnitRequirement)
                .subscribe(
                    (res) => {
                        console.log('data berhasil di not approve = ', res);
                        this.previousState();

                    }
                )
                }
        })
    }
    public previousState(): void {
        this.router.navigate(['approval-leasing']);
    }

    public request() {
        this.confirmationService.confirm({
            message: 'Apakah Yakin Untuk Request Lampiran',
            header: 'Konfirmasi',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.salesUnitRequirementService.executeProcess(114, null, this.salesUnitRequirement)
                .subscribe(
                    (res) => {

                    }
                )
                }
        })
    }

}
