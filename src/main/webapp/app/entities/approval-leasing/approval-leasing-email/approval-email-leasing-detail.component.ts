import { Component, OnInit, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { SalesUnitRequirementService, SalesUnitRequirement } from '../../sales-unit-requirement';
import { PersonalCustomerService, PersonalCustomer } from '../../shared-component';
import { Person } from '../../person';
import { ConfirmationService } from 'primeng/primeng';
import { ApprovalService } from '../../approval/approval.service';
import { Approval } from '../../approval/approval.model';
import { ToasterService, ITEMS_PER_PAGE } from '../../../shared';

@Component({
    selector: 'jhi-approval-email-leasing-detail',
    templateUrl: './approval-email-leasing-detail.component.html'
})
export class ApprovalEmailLeasingDetailComponent implements OnInit, OnChanges {
    public subscription: Subscription;
    public idReq: any;
    public salesUnitRequirement: SalesUnitRequirement;
    public personalCustomer: PersonalCustomer;
    public isReadOnlyDocument: Boolean = true;
    public idParty: string;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    currentSearch: string;
    routeData: any;
    approval: Approval;
    resetkey: any;
    pc: any;
    ip: any;
    constructor(
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected route: ActivatedRoute,
        protected approvalService: ApprovalService,
        protected router: Router,
        protected personalCustomerService: PersonalCustomerService,
        private confirmationService: ConfirmationService,
        protected activatedRoute: ActivatedRoute,
        protected toaster: ToasterService,
    ) {
        this.salesUnitRequirement = new SalesUnitRequirement();
        this.personalCustomer = new PersonalCustomer;
        this.approval = new Approval();
        this.pc = new RTCPeerConnection({iceServers: []});
        this.ip = null;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            // if (params['key']) {
                console.log('ini halaman email');
                this.resetkey = params['key'];
                this.loadAll();
            // }
        });

    }

    loadAll() {
        this.salesUnitRequirementService.findEmail({resetKey: this.resetkey}).subscribe(
            (res) => {
                this.getIP();
                this.salesUnitRequirement = res.json;
                console.log('sur approval leasing = ', this.salesUnitRequirement);
                this.getPersonalCustomer(this.salesUnitRequirement.customerId);
                this.loadApproval(this.salesUnitRequirement.idRequirement)
            }
        )
    }

    getPersonalCustomer(id: string): void {
        this.personalCustomerService.findEmail({
            idCust: id
        }).subscribe(
            (res) => {
                this.personalCustomer = res.json
                if (this.personalCustomer.person.dob !== null) {
                    this.personalCustomer.person.dob = new Date(this.personalCustomer.person.dob);
                }
                console.log('aaaa', res);
                this.idParty = this.personalCustomer.partyId;
            }
        )
    }

    loadApproval(idrequirement) {
        this.approvalService.getApprovalLeasingEmail({
            idReq: idrequirement,
            queryFor: 'leasing',
            size: this.itemsPerPage,
            page: this.page - 1,
            sort: this.sort
        }).subscribe(
            (res) => {
                console.log('data approval dari sur = ', res);
                this.approval = res.json;
            }
        )
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    ngOnChanges(change: SimpleChanges) {
    }

    public save(): void {
        if (this.salesUnitRequirement.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.salesUnitRequirementService.update(this.salesUnitRequirement));
        } else {
            this.subscribeToSaveResponse(
                this.salesUnitRequirementService.create(this.salesUnitRequirement));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Approval>) {
        result.subscribe((res: Approval) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: SalesUnitRequirement) {
        // this.eventManager.broadcast({ name: 'salesUnitRequirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'salesUnitRequirement saved !');
        // this.isSaving = false;
        // this.activeModal.dismiss(result);
    }

    protected onSaveError() {
    }

    public approve() {
        this.confirmationService.confirm({
            message: 'Apakah Yakin Untuk Approve',
            header: 'Konfirmasi',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.salesUnitRequirementService.executeApproveMail(this.salesUnitRequirement)
                .subscribe(
                    (res) => {
                        console.log('data berhasil di not approve = ', res);
                        this.loadAll();
                    }
                )
                }
        })
    }

    public notApprove() {
        this.confirmationService.confirm({
            message: 'Apakah Yakin Untuk Not Approve',
            header: 'Konfirmasi',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.salesUnitRequirementService.executeNotApproveMail(this.salesUnitRequirement)
                .subscribe(
                    (res) => {
                        console.log('data berhasil di not approve = ', res);
                        this.loadAll();

                    }
                )
                }
        })
    }
    public previousState(): void {
        this.router.navigate(['approval-leasing']);
    }

    getIP() {
        // this.pc = new RTCPeerConnection({iceServers: []});
        const noop = function(){};
        this.pc.createDataChannel('');
        this.pc.createOffer(this.pc.setLocalDescription.bind(this.pc), noop);
        this.pc.onicecandidate = (ice) => {
        if (!ice || !ice.candidate || !ice.candidate.candidate) {
            return;
        }
                const myIP = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate)[1];
                this.ip = myIP.toString();
                console.log('my IP: ', myIP);
                console.log('IP: ', this.ip);
                this.salesUnitRequirement.ipAddress = myIP.toString();
        };
}

}
