import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';

import {
    ApprovalLeasingVSODraftComponent,
    approvalLeasingRoute,
    ApprovalLeasingResolvePagingParams,
    ApprovalLeasingDetailComponent,
    ApprovalLeasingDocumentAsListComponent,
    ApprovalEmailLeasingDocumentAsListComponent,
    ApprovalEmailLeasingDetailComponent,
    KorsalRequestAttachmentComponent
 } from './';

 import {
    CheckboxModule,
    InputTextModule,
    InputTextareaModule,
    CalendarModule,
    DropdownModule,
    EditorModule,
    ButtonModule,
    DataTableModule,
    DataListModule,
    DataGridModule,
    DataScrollerModule,
    CarouselModule,
    PickListModule,
    PaginatorModule,
    DialogModule,
    ConfirmDialogModule,
    GrowlModule,
    SharedModule,
    AccordionModule,
    TabViewModule,
    FieldsetModule,
    ScheduleModule,
    PanelModule,
    ListboxModule,
    ChartModule,
    DragDropModule,
    LightboxModule,
    RadioButtonModule,
    TooltipModule,
   } from 'primeng/primeng';
import { MpmSharedEntityModule } from '../shared-entity.module';
import { MpmSalesUnitRequirementModule } from '../sales-unit-requirement';
import { CommonModule } from '@angular/common';
import { CurrencyMaskModule } from 'ng2-currency-mask';

const ENTITY_STATES = [
    ...approvalLeasingRoute
];

@NgModule({
    imports : [
        MpmSharedModule,
        MpmSharedEntityModule,
        MpmSalesUnitRequirementModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        RadioButtonModule ,
        TooltipModule,
    ],
    exports : [
        ApprovalLeasingVSODraftComponent,
        ApprovalLeasingDetailComponent,
        ApprovalLeasingDocumentAsListComponent,
        ApprovalEmailLeasingDocumentAsListComponent,
        ApprovalEmailLeasingDetailComponent,
        KorsalRequestAttachmentComponent
    ],
    declarations : [
        ApprovalLeasingVSODraftComponent,
        ApprovalLeasingDetailComponent,
        ApprovalLeasingDocumentAsListComponent,
        ApprovalEmailLeasingDocumentAsListComponent,
        ApprovalEmailLeasingDetailComponent,
        KorsalRequestAttachmentComponent
    ],
    entryComponents : [
        ApprovalLeasingVSODraftComponent,
        ApprovalLeasingDetailComponent,
        ApprovalLeasingDocumentAsListComponent,
        ApprovalEmailLeasingDocumentAsListComponent,
        ApprovalEmailLeasingDetailComponent,
        KorsalRequestAttachmentComponent
    ],
    providers : [
        ApprovalLeasingResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class MpmApprovalLeasingModule {}
