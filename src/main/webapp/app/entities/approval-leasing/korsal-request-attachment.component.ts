import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleSalesOrder, VehicleSalesOrderService } from '../vehicle-sales-order';
import { OrdersParameter } from '../orders/orders-parameter.model'
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { Salesman, SalesmanService} from '../salesman';

import { LeasingCompanyService, LeasingCompany } from '../leasing-company';
import { LoadingService } from '../../layouts/loading/loading.service';

import { RuleHotItemService } from '../rule-hot-item/rule-hot-item.service';
import { Observable } from 'rxjs/Observable';
import * as BaseConstant from '../../shared/constants/base.constants';
import { SalesUnitRequirement, SalesUnitRequirementService } from '../sales-unit-requirement';

@Component({
    selector: 'jhi-korsal-request-attachment',
    templateUrl: './korsal-request-attachment.component.html'
})

export class KorsalRequestAttachmentComponent implements OnInit, OnDestroy {
    currentAccount: any;
    salesUnitRequirements: SalesUnitRequirement[]
    ordersParameter: OrdersParameter;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    currentSearching: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    isLazyLoadingFirst: Boolean ;

    constructor(
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toasterService: ToasterService,
        private loadingService: LoadingService,
        private salesUnitRequirementService: SalesUnitRequirementService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        // this.currentSearching = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.loadingService.loadingStart();
        this.salesUnitRequirementService.queryApprovalLeasingRequestAttachment({
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
        })
        .subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers)
            }
        )
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/request-attachment'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {

    }

    search() {
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });

    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
        // this.eventSubscriber.unsubscribe();
    }

    trackId(index: number, item: VehicleSalesOrder) {
        return item.idOrder;
    }

    trackleasingComById(index: number, item: LeasingCompany) {
        return item.idPartyRole;
    }

    trackkorsalById(index: number, item: Salesman) {
        return item.idPartyRole;
    }

    trackStatusVsoById(index: number, item: any) {
        return item.value;
    }

    registerChangeInVehicleSalesOrders() {
        this.eventSubscriber = this.eventManager.subscribe('approvalLeasingModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateEntry') {
            result.push('dateEntry');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.salesUnitRequirements = data;
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    executeProcess(data) {
        this.vehicleSalesOrderService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        if (this.isLazyLoadingFirst === false) {
            this.isLazyLoadingFirst = true;
            return ;
        }
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleSalesOrderService.update(event.data)
                .subscribe((res: VehicleSalesOrder) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleSalesOrderService.create(event.data)
                .subscribe((res: VehicleSalesOrder) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: VehicleSalesOrder) {
        this.toasterService.showToaster('info', 'VehicleSalesOrder Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleSalesOrderService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'vehicleSalesOrderListModification',
                    content: 'Deleted an vehicleSalesOrder'
                    });
                });
            }
        });
    }

    getDescription(currentStatus: number) {
        return this.vehicleSalesOrderService.getDescriptionStatus(currentStatus);
    }

    subscribeToSaveResponse(result: Observable<VehicleSalesOrder>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe((res: VehicleSalesOrder) => {
                    resolve();
                });
            });
        }

        getStatus(statusValue: Number): String {

        let hasil = null;

         switch (statusValue) {
            case 70 :
                hasil = 'OPEN';
                return hasil;
            case 61 :
                hasil = 'APPROVE';
                return hasil;
            case 62 :
                hasil = 'NOT APPROVE';
                return hasil;
            default :
                hasil = 'UNDIFINED STATUS ' + statusValue;
                return hasil;
        }
    }
}
