import {Component, OnChanges, Input, SimpleChanges, OnInit} from '@angular/core';
import {Response} from '@angular/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Rx';
import {JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import {PartyDocument} from '../../party-document/party-document.model';
import {PartyDocumentService} from '../../party-document/party-document.service';
import {ITEMS_PER_PAGE, Principal, ResponseWrapper} from '../../../shared';
import {PaginationConfig} from '../../../blocks/config/uib-pagination.config';

import {LazyLoadEvent} from 'primeng/primeng';
import {ToasterService} from '../../../shared/alert/toaster.service';
import {ConfirmationService} from 'primeng/primeng';

import * as lodash from 'lodash';

@Component({
    selector: 'jhi-approval-leasing-document-as-list',
    templateUrl: './approval-leasing-document-as-list.component.html'
})
export class ApprovalLeasingDocumentAsListComponent implements OnInit, OnChanges {
    @Input() idparty: String;
    @Input() readonly: Boolean = false;
    @Input() isHaveOwnConfirmDialog: boolean;

    currentAccount: any;
    partyDocuments: PartyDocument[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        private partyDocumentService: PartyDocumentService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private dataUtils: JhiDataUtils,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idDocument';
        this.reverse = 'asc';
        this.isHaveOwnConfirmDialog = false;
    }

    // loadAll() {
    //     this.partyDocumentService.queryFilterByDocType({
    //         idParty: this.idparty,
    //         idDocType: 16,
    //         page: this.page - 1,
    //         size: this.itemsPerPage,
    //         sort: this.sort()
    //         }).subscribe(
    //         (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
    //         (res: ResponseWrapper) => this.onError(res.json)
    //     );
    // }

    loadAll() {
        this.partyDocumentService.queryFilterBy({
            query: this.idparty,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
            }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/party-document'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/party-document', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        console.log('ini idParty di aslist document ', this.idparty);
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idparty']) {
            if (this.idparty !== undefined) {
                this.loadAll();
                this.registerChangeInPartyDocuments();
            }
        }
    }

    // ngOnDestroy() {
    //     this.eventManager.destroy(this.eventSubscriber);
    // }

    trackId(index: number, item: PartyDocument) {
        return item.idDocument;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    registerChangeInPartyDocuments() {
        this.eventSubscriber = this.eventManager.subscribe('partyDocumentListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idDocument') {
            result.push('idDocument');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.partyDocuments = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.partyDocumentService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }

        if (this.idparty !== undefined) {
            this.loadAll();
        }
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.partyDocumentService.update(event.data)
                .subscribe((res: PartyDocument) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.partyDocumentService.create(event.data)
                .subscribe((res: PartyDocument) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: PartyDocument) {
        this.toasterService.showToaster('info', 'PartyDocument Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any, idx: number) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.partyDocumentService.delete(id).subscribe(
                    (response) => {
                        this.eventManager.broadcast({
                            name: 'partyDocumentListModification',
                            content: 'Deleted an partyDocument'
                        });
                        this.loadAll();
                    },
                    (res) => {
                        this.onError(res);
                    }
                );
            }
        });
    }
}
