import { BaseEntity } from './../../shared';

export class MemoType implements BaseEntity {
    constructor(
        public id?: number,
        public idMemoType?: number,
        public description?: string,
    ) {
    }
}
