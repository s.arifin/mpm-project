import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { MemoType } from './memo-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class MemoTypeService {
    protected itemValues: MemoType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/memo-types';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/memo-types';

    constructor(protected http: Http) { }

    create(memoType: MemoType): Observable<MemoType> {
        const copy = this.convert(memoType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(memoType: MemoType): Observable<MemoType> {
        const copy = this.convert(memoType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<MemoType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, memoType: MemoType): Observable<MemoType> {
        const copy = this.convert(memoType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, memoTypes: MemoType[]): Observable<MemoType[]> {
        const copy = this.convertList(memoTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to MemoType.
     */
    protected convertItemFromServer(json: any): MemoType {
        const entity: MemoType = Object.assign(new MemoType(), json);
        return entity;
    }

    /**
     * Convert a MemoType to a JSON which can be sent to the server.
     */
    protected convert(memoType: MemoType): MemoType {
        if (memoType === null || memoType === {}) {
            return {};
        }
        // const copy: MemoType = Object.assign({}, memoType);
        const copy: MemoType = JSON.parse(JSON.stringify(memoType));
        return copy;
    }

    protected convertList(memoTypes: MemoType[]): MemoType[] {
        const copy: MemoType[] = memoTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: MemoType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
