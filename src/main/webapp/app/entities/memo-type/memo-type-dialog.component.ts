import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MemoType } from './memo-type.model';
import { MemoTypePopupService } from './memo-type-popup.service';
import { MemoTypeService } from './memo-type.service';
import { ToasterService } from '../../shared';

@Component({
    selector: 'jhi-memo-type-dialog',
    templateUrl: './memo-type-dialog.component.html'
})
export class MemoTypeDialogComponent implements OnInit {

    memoType: MemoType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected memoTypeService: MemoTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;

        if ( this.memoType.id === null || this.memoType.id === undefined ) {
            this.memoType.id = 0;
        }

        if (this.memoType.idMemoType !== undefined) {
            this.subscribeToSaveResponse(
                this.memoTypeService.update(this.memoType));
        } else {
            this.subscribeToSaveResponse(
                this.memoTypeService.create(this.memoType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<MemoType>) {
        result.subscribe((res: MemoType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: MemoType) {
        this.eventManager.broadcast({ name: 'memoTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'memoType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'memoType Changed', error.message);
    }
}

@Component({
    selector: 'jhi-memo-type-popup',
    template: ''
})
export class MemoTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected memoTypePopupService: MemoTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.memoTypePopupService
                    .open(MemoTypeDialogComponent as Component, params['id']);
            } else {
                this.memoTypePopupService
                    .open(MemoTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
