import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { MemoTypeComponent } from './memo-type.component';
import { MemoTypePopupComponent } from './memo-type-dialog.component';

@Injectable()
export class MemoTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idMemoType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const memoTypeRoute: Routes = [
    {
        path: 'memo-type',
        component: MemoTypeComponent,
        resolve: {
            'pagingParams': MemoTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.memoType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const memoTypePopupRoute: Routes = [
    {
        path: 'memo-type-new',
        component: MemoTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.memoType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'memo-type/:id/edit',
        component: MemoTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.memoType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
