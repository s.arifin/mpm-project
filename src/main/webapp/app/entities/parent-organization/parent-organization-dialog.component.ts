import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {ParentOrganization} from './parent-organization.model';
import {ParentOrganizationPopupService} from './parent-organization-popup.service';
import {ParentOrganizationService} from './parent-organization.service';
import {ToasterService} from '../../shared';
import { RoleType, RoleTypeService } from '../role-type';
import { Party, PartyService } from '../party';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-parent-organization-dialog',
    templateUrl: './parent-organization-dialog.component.html'
})
export class ParentOrganizationDialogComponent implements OnInit {

    parentOrganization: ParentOrganization;
    isSaving: boolean;

    roletypes: RoleType[];

    parties: Party[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected parentOrganizationService: ParentOrganizationService,
        protected roleTypeService: RoleTypeService,
        protected partyService: PartyService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.roleTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.roletypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.partyService.query()
            .subscribe((res: ResponseWrapper) => { this.parties = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.parentOrganization.idInternal !== undefined) {
            this.subscribeToSaveResponse(
                this.parentOrganizationService.update(this.parentOrganization));
        } else {
            this.subscribeToSaveResponse(
                this.parentOrganizationService.create(this.parentOrganization));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ParentOrganization>) {
        result.subscribe((res: ParentOrganization) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ParentOrganization) {
        this.eventManager.broadcast({ name: 'parentOrganizationListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'parentOrganization saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'parentOrganization Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackRoleTypeById(index: number, item: RoleType) {
        return item.idRoleType;
    }

    trackPartyById(index: number, item: Party) {
        return item.idParty;
    }
}

@Component({
    selector: 'jhi-parent-organization-popup',
    template: ''
})
export class ParentOrganizationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected parentOrganizationPopupService: ParentOrganizationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.parentOrganizationPopupService
                    .open(ParentOrganizationDialogComponent as Component, params['id']);
            } else {
                this.parentOrganizationPopupService
                    .open(ParentOrganizationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
