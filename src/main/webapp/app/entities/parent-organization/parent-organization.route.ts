import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ParentOrganizationComponent } from './parent-organization.component';
import { ParentOrganizationPopupComponent } from './parent-organization-dialog.component';

@Injectable()
export class ParentOrganizationResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idInternal,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const parentOrganizationRoute: Routes = [
    {
        path: 'parent-organization',
        component: ParentOrganizationComponent,
        resolve: {
            'pagingParams': ParentOrganizationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.parentOrganization.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const parentOrganizationPopupRoute: Routes = [
    {
        path: 'parent-organization-new',
        component: ParentOrganizationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.parentOrganization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'parent-organization/:id/edit',
        component: ParentOrganizationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.parentOrganization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
