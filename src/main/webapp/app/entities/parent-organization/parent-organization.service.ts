import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { ParentOrganization } from './parent-organization.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ParentOrganizationService {

    protected resourceUrl = 'api/parent-organizations';
    protected resourceSearchUrl = 'api/_search/parent-organizations';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(parentOrganization: ParentOrganization): Observable<ParentOrganization> {
        const copy = this.convert(parentOrganization);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(parentOrganization: ParentOrganization): Observable<ParentOrganization> {
        const copy = this.convert(parentOrganization);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<ParentOrganization> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(parentOrganization: any): Observable<String> {
        const copy = this.convert(parentOrganization);
        return this.http.post(this.resourceUrl + '/execute', copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateRegister) {
            entity.dateRegister = new Date(entity.dateRegister);
        }
        if (entity.dateFrom) {
            entity.dateFrom = new Date(entity.dateFrom);
        }
        if (entity.dateThru) {
            entity.dateThru = new Date(entity.dateThru);
        }
    }

    protected convert(parentOrganization: ParentOrganization): ParentOrganization {
        const copy: ParentOrganization = Object.assign({}, parentOrganization);

        // copy.dateRegister = this.dateUtils.toDate(parentOrganization.dateRegister);

        // copy.dateFrom = this.dateUtils.toDate(parentOrganization.dateFrom);

        // copy.dateThru = this.dateUtils.toDate(parentOrganization.dateThru);
        return copy;
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

}
