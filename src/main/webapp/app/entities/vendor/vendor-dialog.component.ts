import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import { Vendor } from './vendor.model';
import { VendorPopupService } from './vendor-popup.service';
import { VendorService } from './vendor.service';
import { VendorTypeService, VendorType} from '../vendor-type';
import { ToasterService } from '../../shared';
import { RoleType, RoleTypeService } from '../role-type';
import { Party, PartyService } from '../party';
import { ResponseWrapper } from '../../shared';
import { ParentOrganization, ParentOrganizationService } from '../parent-organization';
import { VendorRelationship, VendorRelationshipService } from '../vendor-relationship';

@Component({
    selector: 'jhi-vendor-dialog',
    templateUrl: './vendor-dialog.component.html'
})
export class VendorDialogComponent implements OnInit {

    vendor: Vendor;
    isSaving: boolean;
    internals: ParentOrganization[];
    filteredInternal: ParentOrganization[];

    roletypes: RoleType[];
    vendortypes: VendorType[];

    parties: Party[];
    text: string;
    results: string[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private vendorService: VendorService,
        private vendorTypeService: VendorTypeService,
        private roleTypeService: RoleTypeService,
        private partyService: PartyService,
        private eventManager: JhiEventManager,
        private toaster: ToasterService,
        private internalService: ParentOrganizationService,
        private vendorRelationshipService: VendorRelationshipService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.roleTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.roletypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.partyService.query()
            .subscribe((res: ResponseWrapper) => { this.parties = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.vendorTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.vendortypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.queryFilterBy({idVendor: this.vendor.idVendor})
            .subscribe((res: ResponseWrapper) => { this.internals = res.json;
            console.log('res:', res) }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.vendor.idVendor !== undefined) {
            this.subscribeToSaveResponse(
                this.vendorService.update(this.vendor));
        } else {
            this.subscribeToSaveResponse(
                this.vendorService.create(this.vendor));
        }
    }

    searchFilterInternal(event) {
        // this.filteredInternal = [];
        this.internalService.search({query: event.query + '*'})
        .subscribe((res: ResponseWrapper) => {
            console.log('current: ', res);
            this.filteredInternal = res.json; },
            (res: ResponseWrapper) => this.onError(res.json));
    }

    selectInternal(value) {
        console.log('select', value);
        const currInternal = value.idInternal;
        this.vendorRelationshipService
        .queryFilterBy({idVendor: this.vendor.idVendor, idInternal: currInternal, size: 1})
        .subscribe((items) => {
            // Kalau data tidak ada
            if (items.json && items.json.length === 0) {
                const vr: VendorRelationship = new VendorRelationship();
                vr.internalId = currInternal;
                vr.vendorId = this.vendor.idVendor;
                this.vendorRelationshipService.create(vr).subscribe(() => {
                    console.log('Done 1');
                });
            }
        });
    }

    unSelectInternal(value) {
        console.log('unselect', value);
        const currInternal = value.idInternal;
        this.vendorRelationshipService
        .queryFilterBy({idVendor: this.vendor.idVendor, idInternal: currInternal, size: 1})
        .subscribe((items) => {
            // Kalau data tidak ada
            console.log('Done check', items);
            if (items.json && items.json.length > 0) {
                this.vendorRelationshipService.delete(items.json[0].idPartyRelationship).subscribe(() => {
                    console.log('Done 2');
                });
            }
        });
    }

    private subscribeToSaveResponse(result: Observable<Vendor>) {
        result.subscribe((res: Vendor) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Vendor) {
        this.eventManager.broadcast({ name: 'vendorListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vendor saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'vendor Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackRoleTypeById(index: number, item: RoleType) {
        return item.idRoleType;
    }

    trackPartyById(index: number, item: Party) {
        return item.idParty;
    }

    trackVendorTypeById(index: number, item: VendorType) {
        return item.idVendorType;
    }
}

@Component({
    selector: 'jhi-vendor-popup',
    template: ''
})
export class VendorPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vendorPopupService: VendorPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vendorPopupService
                    .open(VendorDialogComponent as Component, params['id']);
            } else {
                this.vendorPopupService
                    .open(VendorDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
