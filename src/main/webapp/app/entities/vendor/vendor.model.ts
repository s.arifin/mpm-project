import { BaseEntity } from './../shared-component';
import { Organization } from './../organization';

export class Vendor implements BaseEntity {
    constructor(
        public id?: any,
        public idVendor?: string,
        public vendorTypeId?: number,
        public organization?: any,
        public idRoleType?: any,
        public biroJasa?: boolean,
        public mainDealer?: boolean,
    ) {
        this.organization = new Organization();
    }
}
