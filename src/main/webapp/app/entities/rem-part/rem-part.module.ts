import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import { MpmSharedModule, JhiLanguageHelper } from '../../shared';
import { JhiLanguageService } from 'ng-jhipster';
import { customHttpProvider } from '../../blocks/interceptor/http.provider';
import { CommonModule } from '@angular/common';

import {
    RemPartService,
    RemPartPopupService,
    RemPartComponent,
    RemPartDialogComponent,
    RemPartPopupComponent,
    remPartRoute,
    remPartPopupRoute,
    RemPartResolvePagingParams,
} from './';

import {CurrencyMaskModule} from 'ng2-currency-mask';

import {
        CheckboxModule,
        InputTextModule,
        CalendarModule,
        DropdownModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        DialogModule,
        ConfirmDialogModule,
        ConfirmationService,
        GrowlModule,
        DataGridModule,
        SharedModule,
        AccordionModule,
        TabViewModule,
        FieldsetModule,
        ScheduleModule,
        PanelModule,
        ListboxModule,
        SelectItem,
        MenuItem,
        Header,
        Footer} from 'primeng/primeng';

const ENTITY_STATES = [
    ...remPartRoute,
    ...remPartPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        DataGridModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        CurrencyMaskModule
    ],
    exports: [
        RemPartComponent,
    ],
    declarations: [
        RemPartComponent,
        RemPartDialogComponent,
        RemPartPopupComponent,
    ],
    entryComponents: [
        RemPartComponent,
        RemPartDialogComponent,
        RemPartPopupComponent,
    ],
    providers: [
        RemPartPopupService,
        RemPartResolvePagingParams,
        customHttpProvider(),
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmRemPartModule {
    constructor(protected languageService: JhiLanguageService, protected languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => this.languageService.changeLanguage(languageKey));
    }
}
