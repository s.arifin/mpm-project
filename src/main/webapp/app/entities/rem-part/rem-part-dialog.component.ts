import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {RemPart} from './rem-part.model';
import {RemPartPopupService} from './rem-part-popup.service';
import {RemPartService} from './rem-part.service';
import {ToasterService} from '../../shared';
import { Uom, UomService } from '../uom';
import { Feature, FeatureService } from '../feature';
import { ProductCategory, ProductCategoryService } from '../product-category';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-rem-part-dialog',
    templateUrl: './rem-part-dialog.component.html'
})
export class RemPartDialogComponent implements OnInit {

    remPart: RemPart;
    isSaving: boolean;

    uoms: Uom[];

    features: Feature[];

    categories: ProductCategory[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected remPartService: RemPartService,
        protected uomService: UomService,
        protected featureService: FeatureService,
        protected categoryService: ProductCategoryService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.uomService.query()
            .subscribe((res: ResponseWrapper) => { this.uoms = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.featureService.query()
            .subscribe((res: ResponseWrapper) => { this.features = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.categoryService.query()
            .subscribe((res: ResponseWrapper) => { this.categories = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.remPart.idProduct !== undefined) {
            this.subscribeToSaveResponse(
                this.remPartService.update(this.remPart));
        } else {
            this.subscribeToSaveResponse(
                this.remPartService.create(this.remPart));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RemPart>) {
        result.subscribe((res: RemPart) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: RemPart) {
        this.eventManager.broadcast({ name: 'remPartListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'remPart saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'remPart Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackUomById(index: number, item: Uom) {
        return item.idUom;
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    trackCategoryById(index: number, item: ProductCategory) {
        return item.idCategory;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-rem-part-popup',
    template: ''
})
export class RemPartPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected remPartPopupService: RemPartPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.remPartPopupService
                    .open(RemPartDialogComponent as Component, params['id']);
            } else {
                this.remPartPopupService
                    .open(RemPartDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
