import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { StatusType } from './status-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class StatusTypeService {
    protected itemValues: StatusType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/status-types';
    protected resourceSearchUrl = 'api/_search/status-types';

    constructor(protected http: Http) { }

    create(statusType: StatusType): Observable<StatusType> {
        const copy = this.convert(statusType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(statusType: StatusType): Observable<StatusType> {
        const copy = this.convert(statusType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<StatusType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, statusType: StatusType): Observable<StatusType> {
        const copy = this.convert(statusType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, statusTypes: StatusType[]): Observable<StatusType[]> {
        const copy = this.convertList(statusTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(statusType: StatusType): StatusType {
        if (statusType === null || statusType === {}) {
            return {};
        }
        // const copy: StatusType = Object.assign({}, statusType);
        const copy: StatusType = JSON.parse(JSON.stringify(statusType));
        return copy;
    }

    protected convertList(statusTypes: StatusType[]): StatusType[] {
        const copy: StatusType[] = statusTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: StatusType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
