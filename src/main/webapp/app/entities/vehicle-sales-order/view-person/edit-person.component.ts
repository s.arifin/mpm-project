import { Component, Input, OnInit, OnDestroy, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService  } from 'ng-jhipster';

import { Person, PersonService } from '../../person';
import { ReligionType, ReligionTypeService } from '../../religion-type';
import { WorkType, WorkTypeService } from '../../work-type';
import { ResponseWrapper, CommonUtilService, ToasterService } from '../../../shared';
import { PersonalCustomerService, PersonalCustomer } from '../../shared-component';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { VehicleSalesOrder } from '../vehicle-sales-order.model';

@Component({
    selector: 'jhi-edit-person',
    templateUrl: './edit-person.component.html'
})
export class EditPersonComponent implements OnInit, OnDestroy, OnChanges {

    @Input() person: Person = new Person();
    @Input() readonly = false;
    @Input() index = 0;
    @Input() viewHiddenField: any = 0;
    @Input() customer: PersonalCustomer = new PersonalCustomer();
    @Input() vso: VehicleSalesOrder = new VehicleSalesOrder();
    @Output() fnCallBack = new EventEmitter();
    public isDetail: boolean;
    isSaving: boolean;
    religiontypes: ReligionType[];
    worktypes: WorkType[];

    public yearForCalendar: string

    genders: Array<object> = [
        {label : 'Pria', value : 'P'},
        {label : 'Wanita', value : 'W'}
    ];

    bloodTypes: Array<object> = [
        {label : 'O', value : 'O'},
        {label : 'A', value : 'A'},
        {label : 'B', value : 'B'},
        {label : 'AB', value : 'AB'}
    ];

    constructor(
        private workTypeService: WorkTypeService,
        private religionTypeService: ReligionTypeService,
        private commonUtilService: CommonUtilService,
        private personalCustomerService: PersonalCustomerService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager,
        private toaster: ToasterService,
        private alertService: JhiAlertService,
        private router: Router,
    ) {
        this.isDetail = true;
        this.yearForCalendar = this.commonUtilService.getYearRangeForCalendar();
    }

    ngOnInit() {
        this.religionTypeService.query({size: 100})
            .subscribe((res: ResponseWrapper) => { this.religiontypes = res.json; });
        this.workTypeService.query({size: 100})
            .subscribe((res: ResponseWrapper) => { this.worktypes = res.json; });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['person']) {
            console.log('this.person', this.person);
            if (this.person.dob !== null) {
                const tempDate = new Date(this.person.dob).setHours(23, 59, 59, 99);
                this.person.dob = new Date(tempDate);
            }
        }
    }

    ngOnDestroy() {
    }

    getProvince(province: any) {
        this.person.postalAddress.provinceId = province;
    }

    getDistrict(district: any) {
        this.person.postalAddress.districtId = district;
    }

    getCity(city: any) {
        this.person.postalAddress.cityId = city;
    }

    getVillage(village: any) {
        this.person.postalAddress.villageId = village;
    }

    trackWorkTypeById(index: number, item: WorkType) {
        return item.idWorkType;
    }

    trackReligionTypeById(index: number, item: ReligionType) {
        return item.idReligionType;
    }

    private onSaveSuccess(result: PersonalCustomer) {
        // this.personalCustomerService.newCustomerRegister(result);
        this.eventManager.broadcast({ name: 'personalCustomerListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'personalCustomer saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
        // this.load(result.idCustomer);
        // this.router.navigate(['../vehicle-sales-order/' + this.vso.idOrder + '/edit']);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'personalCustomer Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }
    private subscribeToSaveResponse(result: Observable<PersonalCustomer>) {
        result.subscribe((res: PersonalCustomer) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    save() {
        this.isSaving = true;
        if (this.customer.idCustomer !== undefined) {
            this.subscribeToSaveResponse(
                this.personalCustomerService.update(this.customer));
        } else {
            this.subscribeToSaveResponse(
                this.personalCustomerService.create(this.customer));
        }
    }

    load(id: string): void {
        this.personalCustomerService.find(id).subscribe(
            (res) => {
                // if (res.person.dob !== null) {
                //     res.person.dob = new Date(res.person.dob);
                // }
                console.log('customerrrr', res);
                // this.customer = res;
            }
        )
    }

    public goToEdit() {
        if (this.isDetail) {
            this.isDetail = false;
        } else {
            this.isDetail = true;
            this.load(this.person.idParty);
        }
    }
}
