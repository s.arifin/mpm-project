import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';

import { SalesUnitRequirement } from '../../sales-unit-requirement';
import { PersonalCustomerService, PersonalCustomer, OrganizationCustomer, OrganizationCustomerService } from '../../shared-component';

import * as reqType from '../../../shared/constants/requirement-type.constants';
import * as SalesUnitRequirementConstants from '../../../shared/constants/sales-unit-requirement.constants';
import { Organization } from '../../organization';
import { VehicleSalesOrder } from '../vehicle-sales-order.model';

@Component({
    selector: 'jhi-vso-customer-view',
    templateUrl: './vso-customer-view.component.html'
})

export class VsoCustomerViewComponent implements OnChanges {
    @Input()
    public salesUnitRequirement: SalesUnitRequirement =  new SalesUnitRequirement();
    @Input() organization: Organization;

    @Input()
    public viewDetailShipment: any = 0;
    @Input() vso: VehicleSalesOrder = new VehicleSalesOrder();
    @Input()
    readOnly: any;
    @Input()
    public status: any;
    public isComplete: Boolean = false;

    personalCustomer: PersonalCustomer;
    organizationCustomer: OrganizationCustomer;
    readonlyDocument: Boolean = true;

    isEdit: Boolean;
    isEditOrganization: Boolean;

    constructor(
        private personalCustomerService: PersonalCustomerService,
        private organizationCustomerService: OrganizationCustomerService
    ) {
        this.personalCustomer = new PersonalCustomer();
        this.organizationCustomer = new OrganizationCustomer();
        this.salesUnitRequirement = new  SalesUnitRequirement();
        this.organization = new Organization();
        this.isEdit = false;
        this.isEditOrganization = false;
        this.readOnly = false;
    }

    ngOnChanges(changes: SimpleChanges ) {
        if (changes['salesUnitRequirement']) {
            if (this.status === 17 || this.status === 13 ) {
                this.isComplete = true;
            }
            if (this.salesUnitRequirement.customerId !== undefined) {
                if (this.salesUnitRequirement.idReqTyp === 101 ||
                    this.salesUnitRequirement.idReqTyp === 103 ||
                    this.salesUnitRequirement.idReqTyp === 104 ||
                    this.salesUnitRequirement.idReqTyp === 105 ) {
                    this.getOrganizationCustomer(this.salesUnitRequirement.customerId);
                } else if (this.salesUnitRequirement.idReqTyp === 102 ||
                    this.salesUnitRequirement.idReqTyp === null ) {
                    this.getPersonalCustomer(this.salesUnitRequirement.customerId);
                }
            }
        }
        console.log('vso customer', this.salesUnitRequirement);
    }

    getPersonalCustomer(id: string): void {
        this.personalCustomerService.find(id).subscribe(
            (res) => {
                if (res.person.dob !== null) {
                    res.person.dob = new Date(res.person.dob);
                }
                console.log('aaaa', res);
                this.personalCustomer = res;
            }
        )
    }
    getOrganizationCustomer(id: string): void {
        this.organizationCustomerService.find(id).subscribe(
            (res) => {
                this.organizationCustomer = res;
                console.log('getOrganizationCustomer', res);
            }
        )
    }

    public showNotShipment(): boolean {
        let a: boolean;
        a = false;

        if (this.viewDetailShipment === 0) {
            a = true;
        }

        return a;
    }

    public showNameOrFullName(data: number): boolean {
        let a: boolean;
        a = false;

        if (data === 1 || data === 0) {
            a = true;
        }

        return a;
    }

    edit(event: Boolean) {
        this.isEdit = event;
    }

    editOrganization(event: Boolean) {
        this.isEditOrganization = event;
    }
};
