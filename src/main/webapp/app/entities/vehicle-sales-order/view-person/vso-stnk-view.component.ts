import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';

import { SalesUnitRequirement } from '../../sales-unit-requirement';
import * as reqType from '../../../shared/constants/requirement-type.constants';

@Component({
    selector: 'jhi-vso-stnk-view',
    templateUrl: './vso-stnk-view.component.html'
})

export class VsoSTNKViewComponent implements OnChanges {
    @Input() salesUnitRequirement: SalesUnitRequirement;
    @Input() viewDetailShipment: any = 0;
    readonlyDocument: boolean;
    @Input()
    public status: any;
    public isComplete: Boolean = false;

    constructor() {
        this.readonlyDocument = true;
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['salesUnitRequirement']) {
            if (this.status === 17 || this.status === 13 ) {
                this.isComplete = true;
            }
            // if (this.salesUnitRequirement.idReqTyp === reqType.REQUIREMENT_GC ) {
            // }
        }
    }
}
