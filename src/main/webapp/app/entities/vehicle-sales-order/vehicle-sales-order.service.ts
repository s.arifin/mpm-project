import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { VehicleSalesOrder } from './vehicle-sales-order.model';
import { SalesUnitRequirement } from '../sales-unit-requirement';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';
import { createOrdersParameterOption } from '../orders/orders-parameter.util';

@Injectable()
export class VehicleSalesOrderService {
    private itemValues: VehicleSalesOrder[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = 'api/vehicle-sales-orders';
    private resourceUrlLeasing = 'api/vehicle-sales-orders-leasing';
    private resourceUrlVSOSales = 'api/vehicle-sales-order-sales';
    private resourceSearchUrl = 'api/_search/vehicle-sales-orders';
    private resourceCUrl = process.env.API_C_URL + '/api/ax_generalledger/';
    private resourceCUrl2 = process.env.API_C_URL + '/api/vehicle-sales-order';
    private resourceCVSO = process.env.API_C_URL + '/api/vehicle-sales-orders';

    constructor(private http: Http) { }

    create(vehicleSalesOrder: VehicleSalesOrder): Observable<VehicleSalesOrder> {
        const copy = this.convert(vehicleSalesOrder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(vehicleSalesOrder: VehicleSalesOrder): Observable<VehicleSalesOrder> {
        const copy = this.convert(vehicleSalesOrder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    createGL(vehicleSalesOrder: VehicleSalesOrder): Observable<any> {
        const copy = this.convert(vehicleSalesOrder);
        return this.http.post(this.resourceCUrl + 'CreateJournal/?TranCode=103&IdInput=' + vehicleSalesOrder.idSalesUnitRequirement, copy)
            .map((res: Response) => {
                return res.json();
            });
    }

    updateAdminHandling(vehicleSalesOrder: VehicleSalesOrder): Observable<VehicleSalesOrder> {
        const copy = this.convert(vehicleSalesOrder);
        return this.http.put(this.resourceUrl + '/update', copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any, req: any): Observable<VehicleSalesOrder> {
        const options = createRequestOption(req);
        // options.params.set('idinternal', req.idinternal);
        return this.http.get(`${this.resourceUrl}/${id}`, options).map((res: Response) => {
            return res.json();
        });
    }

    findDoGudangC(id: any, req: any): Observable<VehicleSalesOrder> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceCUrl2}/do-gudang/${id}`, options).map((res: Response) => {
            return res.json();
        });
    }

    findSurByOrderId(id: any, req: any): Observable<SalesUnitRequirement> {
        const options = createRequestOption(req);
        // options.params.set('idinternal', req.idinternal);
        return this.http.get(`${this.resourceUrl}/findSurByOrderId/${id}`, options).map((res: Response) => {
            return res.json();
        });
    }

    findSurByOrderIdC(id: any, req: any): Observable<SalesUnitRequirement> {
        const options = createRequestOption(req);
        // options.params.set('idinternal', req.idinternal);
        return this.http.get(`${this.resourceCUrl2}/findSurByOrderId/check-payment/${id}`, options).map((res: Response) => {
            return res.json();
        });
    }

    findVsoForManualmatching(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        // options.params.set('idinternal', req.idinternal);
        return this.http.get(`${this.resourceUrl}/manualmatching/`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryLeasing(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        // options.params.set('idinternal', req.idinternal);
        return this.http.get(this.resourceUrlLeasing, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryByidOrderAndBillingNumber(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('billingNumber', req.billingNumber);
        return this.http.get(this.resourceUrl + '/findByidOrderAndBillingNumber/', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryLeasingSubmit(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        // options.params.set('idinternal', req.idinternal);
        return this.http.get(`${this.resourceUrlLeasing}/submit`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createOrdersParameterOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterByy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterByy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterAdminHandling(req?: any): Observable<ResponseWrapper> {
        const options = createOrdersParameterOption(req);
        return this.http.get(this.resourceUrl + '/filterAdminHandling', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        // options.params.set('status', '17')
        return this.http.get(this.resourceUrl + '/filter', options)
            .map((res: Response) => this.convertResponse(res));
    }

    querytoSearch(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        // options.params.set('status', '17')
        return this.http.get(this.resourceUrl + '/tosearch', options)
            .map((res: Response) => this.convertResponse(res));
    }

    querytoSearchAH(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        // options.params.set('status', '17')
        return this.http.get(this.resourceUrl + '/tosearchAH', options)
            .map((res: Response) => this.convertResponse(res));
    }

    querytoSearchAHSubmit(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        // options.params.set('status', '17')
        return this.http.get(this.resourceUrl + '/tosearchAHSubmit', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, vehicleSalesOrder: VehicleSalesOrder): Observable<VehicleSalesOrder> {
        const copy = this.convert(vehicleSalesOrder);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeProcessPromise(id: Number, param: String, vehicleSalesOrder: VehicleSalesOrder): Promise<VehicleSalesOrder> {
        const copy = this.convert(vehicleSalesOrder);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy)
        .delay(1000)
            .toPromise()
            .then((res: Response) => {
                // return res.json();
                return Promise.resolve(res.json());
            })
            .catch((res: any) => {
                return Promise.reject(res.message );
            });
    }

    executeListProcess(id: Number, param: String, vehicleSalesOrders: VehicleSalesOrder[]): Observable<VehicleSalesOrder[]> {
        const copy = this.convertList(vehicleSalesOrders);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        // options.params.set('idinternal', req.idinternal);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    searchUnit(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    searchLeasing(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceSearchUrl}/adminHandling`, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    processAdminHandling(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/processAdminHandling`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    queryVSOSales(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrlVSOSales, options)
            .map((res: Response) => this.convertResponse(res));
    }

    executeProcessAprrove(id: Number, param: String, vehicleSalesOrder: VehicleSalesOrder): Observable<ResponseWrapper> {
        const copy = this.convert(vehicleSalesOrder);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }
        findVsoForApproveKacab(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        // options.params.set('idinternal', req.idinternal);
        return this.http.get(`${this.resourceUrl}/approve-kacab/`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convert(vehicleSalesOrder: VehicleSalesOrder): VehicleSalesOrder {
        if (vehicleSalesOrder === null || vehicleSalesOrder === {}) {
            return {};
        }
        // const copy: VehicleSalesOrder = Object.assign({}, vehicleSalesOrder);
        const copy: VehicleSalesOrder = JSON.parse(JSON.stringify(vehicleSalesOrder));
        return copy;
    }

    private convertList(vehicleSalesOrders: VehicleSalesOrder[]): VehicleSalesOrder[] {
        const copy: VehicleSalesOrder[] = vehicleSalesOrders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    public convertListFinco(lists: Array<VehicleSalesOrder>): Array<Object> {
        const arr: Array<Object> = new Array<Object>();

        if (lists.length > 0) {
            lists.forEach(
                (m) => {
                    const obj: object = {
                        label : m.bastFinco,
                        value : m.bastFinco
                    };
                    arr.push(obj);
                }
            )
        }

        return arr;
    }

    pushItems(data: VehicleSalesOrder[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    getDescriptionStatus(statusValue: Number): String {

        let hasil = null;

         switch (statusValue) {
            case 10 :
                hasil = 'OPEN';
                return hasil;
            case 11 :
                hasil = 'OPEN';
                return hasil;
            case 17 :
                hasil = 'COMPLETED';
                return hasil;
            case 74 :
                hasil = 'MATCHING';
                return hasil;
            case 75 :
                hasil = 'NOT APPROVED';
                return hasil;
            case 13 :
                hasil = 'CANCELED';
                return hasil;
            case 61 :
                hasil = 'APPROVE';
                return hasil;
            case 88 :
                hasil = 'WAITING ADMNIN HANDLING';
                return hasil;
            default :
                hasil = 'UNDIFINED STATUS ' + statusValue;
                return hasil;
        }
    }
    // process(params?: any, req?: any): Observable<any> {
    //     const options = createRequestOption(req);
    //     return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
    //         return res.json();
    //     });
    // }
    queryC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('id internal == ', req)
        return this.http.get(this.resourceCVSO, options)
            .map((res: Response) => this.convertResponse(res));
    }

    findC(id: any): Observable<VehicleSalesOrder> {
        return this.http.get(this.resourceCVSO + '/' + id).map((res: Response) => {
            return res.json();
        });
    }

    executeProcessC(id: Number, param: String, vehicleSalesOrder: VehicleSalesOrder): Observable<VehicleSalesOrder> {
        const copy = this.convert(vehicleSalesOrder);
        return this.http.post(`${this.resourceCVSO}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    approveForMatching(): Observable<Response> {
        console.log('approveForMatching');
        return this.http.get(this.resourceUrl + '/approve-for-matching');
    }

}
