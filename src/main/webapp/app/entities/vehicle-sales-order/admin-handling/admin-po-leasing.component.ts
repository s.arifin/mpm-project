import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { Observable} from 'rxjs/Rx';
import { OrdersParameter } from '../../orders/orders-parameter.model'
import { AdminPOLeasingParameter} from './admin-po-leasing-parameter.model'
import { VehicleSalesOrder } from '../vehicle-sales-order.model';
import { VehicleSalesOrderService } from '../vehicle-sales-order.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { LazyLoadEvent } from 'primeng/primeng';
import { LeasingCompanyService, LeasingCompany } from '../../leasing-company';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { SalesUnitRequirement, SalesUnitRequirementService } from '../../sales-unit-requirement';
import { SelectItem} from 'primeng/primeng';
import { ReportUtilService } from '../../../shared/report/report-util.service';

@Component({
    selector: 'jhi-admin-po-leasing',
    templateUrl: './admin-po-leasing.component.html'
})
export class AdminPoLeasingComponent implements OnInit, OnDestroy {
    vehicleSalesOrders: VehicleSalesOrder[];
    TagihanLeasing: VehicleSalesOrder[];
    vehicleSalesOrderSubmit: VehicleSalesOrder[];
    leasings: SelectItem[];
    statusVsoes: SelectItem[];
    korsals: SelectItem[];
    selected: VehicleSalesOrder[];
    selectedLeasing: string;
    selectedKorsal: string;
    vehicleSalesOrder: VehicleSalesOrder;
    vehicleSalesOrderUpdate: VehicleSalesOrder;
    currentAccount: any;
    sur: SalesUnitRequirement;
    vso: VehicleSalesOrder;
    ordersParameter: OrdersParameter;
    adminHandlingParameter: AdminPOLeasingParameter;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    totalItemsLeasing: any;
    queryCount: any;
    itemsPerPage: any;
    itemsPerPageSubmit: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    tgl1: Date;
    tgl2: Date;
    tgl3: Date;
    tgl4: Date;
    printModal: boolean;
    leasingStatusPayment: any;
    newTagihan: Boolean = false;
    newPending: Boolean = false;
    isSaving: boolean;
    isFiltered: Boolean;
    selectedStatusVso: Number;
    isMatchingMenu: Boolean ;
    isMatchingMenuSubmit: Boolean ;
    currentSearching: string;
    currentSearchingSubmit: string;
    BAST: any;
    sts: any;
    reprintFinco: boolean;
    public listbastFinco: Array<Object>;
    SelectedbastFinco: any;
    public listbastForSelect: Array<Object>;
    public listbast: Array<Object>;

    constructor(
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private toasterService: ToasterService,
        private activatedRoute: ActivatedRoute,
        private salesUnitRequirementService: SalesUnitRequirementService,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private loadingService: LoadingService,
        private leasingCompanyService: LeasingCompanyService,
        private paginationConfig: PaginationConfig,
        private reportUtilService: ReportUtilService,
    ) {
        this.itemsPerPageSubmit = ITEMS_PER_PAGE;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearching = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.currentSearchingSubmit = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.printModal = false;
        this.isFiltered = false;
        this.leasings = [];
        this.korsals = [];
        this.statusVsoes = [];
        this.statusVsoes.push({label: 'OPEN', value: 10});
        this.statusVsoes.push({label: 'COMPLETED', value: 17 });
        this.selected = new Array<VehicleSalesOrder>();
        this.listbastFinco = new Array<Object>();
        this.vehicleSalesOrderUpdate = new VehicleSalesOrder();
        this.vehicleSalesOrder = new VehicleSalesOrder();
        this.listbastForSelect = new Array<Object>();
    }

    loadAll() {
        this.loadingService.loadingStart(); {
            console.log('is filter = ', this.isFiltered );
            console.log('masuk load all', this.isMatchingMenu)
            if (this.isMatchingMenu === true) {

                if (this.currentSearching) {
                    console.log('SEARCH');
                    this.vehicleSalesOrderService.querytoSearchAH({
                        idInternal: this.principal.getIdInternal(),
                        page: this.page - 1,
                        queryfor: 'search',
                        data: this.currentSearching,
                        size: this.itemsPerPage,
                        // sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                        );
                    return;
                }
            }else {
                if (this.currentSearching) {
                    console.log('SEARCH');
                    this.vehicleSalesOrderService.querytoSearchAH({
                        idInternal: this.principal.getIdInternal(),
                        page: this.page - 1,
                        queryfor: 'search',
                        data: this.currentSearching,
                        size: this.itemsPerPage,
                        // sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                        );
                    return;
                }

                this.vehicleSalesOrderService.queryLeasing({
                    idInternal: this.principal.getIdInternal(),
                    page: this.page - 1,
                    size: this.itemsPerPage,
                }).subscribe(
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                );
            }
        }

    }

    loadAllSubmit() {
        this.loadingService.loadingStart();
        try {
            console.log('is filter = ', this.isFiltered );
            console.log('apakah is matching menu ', this.isMatchingMenuSubmit)
            if (this.isMatchingMenuSubmit === true) {

                if (this.currentSearchingSubmit) {
                    console.log('SEARCH');
                    this.vehicleSalesOrderService.querytoSearchAH({
                        idInternal: this.principal.getIdInternal(),
                        page: this.page - 1,
                        queryfor: 'search',
                        data: this.currentSearchingSubmit,
                        size: this.itemsPerPageSubmit,
                        // sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccessT(res.json, res.headers)},
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                        );
                    return;
                }
            }else {

                if (this.currentSearchingSubmit) {
                    console.log('SEARCH');
                    this.vehicleSalesOrderService.querytoSearchAH({
                        idInternal: this.principal.getIdInternal(),
                        page: this.page - 1,
                        queryfor: 'search',
                        data: this.currentSearchingSubmit,
                        size: this.itemsPerPageSubmit,
                        // sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccessT(res.json, res.headers)},
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                        );
                    return;
                }

                this.vehicleSalesOrderService.queryLeasingSubmit({
                    idInternal: this.principal.getIdInternal(),
                    page: this.page - 1,
                    size: this.itemsPerPageSubmit,
                }).subscribe(
                    (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccessT(res.json, res.headers)},
                    (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                );
            }
        }catch (error) {
            this.loadingService.loadingStop()
        }
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/admin-po-leasing'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage && this.itemsPerPageSubmit,
                sort: this.predicate + ',' + (this.reverse ? 'desc' : 'asc')
            }
        });
        this.loadAll();
        this.loadAllSubmit();
    }

    loadLeasing() {
        this.leasingCompanyService.query({
            page: 0,
            size: 100}).subscribe(
            (res: ResponseWrapper) => this.leasings = res.json,
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    clearing() {
        this.page = 0;
        this.currentSearchingSubmit = null;
        this.router.navigate(['/admin-po-leasing', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.ngOnDestroy();
        this.ngOnInit();
    }

    clear() {
        this.page = 0;
        this.currentSearching = null;
        this.router.navigate(['/admin-po-leasing', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.ngOnDestroy();
        this.ngOnInit();
    }

    trackleasingComById(index: number, item: LeasingCompany) {
        return item.idPartyRole;
    }

    trackStatusVsoById(index: number, item: any) {
        return item.value;
    }

    printMultiSal(data: VehicleSalesOrder) {
        this.selected = new Array<VehicleSalesOrder>();
        console.log('masuk untuk print kurang dari atau sama dengan 2', data);
        this.toasterService.showToaster('Info', 'Print Data', 'Printing ...');
        this.reportUtilService.viewFile('/api/report/bast_tagihan_unit/pdf', {bastfinco: data.bastFinco, nama: data.name, nik: data.identityNumber, userlogin: this.currentAccount.firstName});
    }

    printMultipleSal(data: VehicleSalesOrder) {
        this.selected = new Array<VehicleSalesOrder>();
        console.log('masuk untuk printtt jika data lebih dari 2', data);
        this.toasterService.showToaster('Info', 'Print Data', 'Printing ...');
        this.reportUtilService.viewFile('/api/report/bast_tagihan_unit/pdf', {bastfinco: data.bastFinco, nama: data.name, nik: data.identityNumber, userlogin: this.currentAccount.firstName});
        console.log('tesdata', this.currentAccount.firstName);
    }

    findOneSal(): Promise<any> {
        return new Promise(
            (resolve) => {
                console.log('masuk fungsi find');
                console.log('masuk fungsi find CEK SELECTED', this.selected);
                console.log('masuk fungsi find CEK SELECTED', this.vehicleSalesOrderUpdate.billingNumber);
                const Billnums = this.selected[0].billingNumber;
                this.vehicleSalesOrderService.queryByidOrderAndBillingNumber({
                    billingNumber : Billnums
                }).subscribe(
                    (data) => {
                        console.log('hasil unit VSO', data.json);
                        this.vehicleSalesOrderUpdate = data.json;
                        if (this.selected.length <= 2) {
                            console.log('data terpilih kurang dari sama dengan 2 bast ke sales');
                            this.printMultiSal(this.vehicleSalesOrderUpdate)
                        } else {
                            console.log('data terpilih lebih dari 2 bast ke sales');
                            this.printMultipleSal(this.vehicleSalesOrderUpdate);
                        }
                    }
            )
            resolve()
            }
        )
    }

    listUpdateBAST(): Promise<any> {
        return new Promise(
            (resolve) => {
                console.log('masuk list update bast', this.selected); {
                this.vehicleSalesOrderService.executeListProcess(19, this.principal.getIdInternal(), this.selected )
                .subscribe(
                    (value) => {
                        this.findOneSal();
                        console.log('thissssss: ', value);
                        this.toasterService.showToaster('Info', 'Data Process', 'Done process in system..');
                        this.loadAll();
                    },
                    (err) => {
                        console.log(err);
                        this.toasterService.showToaster('Info', 'Data Process', 'Error Process');
                    },
                    () => {
                        this.vehicleSalesOrders = new Array<VehicleSalesOrder>();
                        this.vehicleSalesOrder = new VehicleSalesOrder();
                    }
                )
            }
            resolve();
            }
        )
    }

    reprint() {
        this.reprintFinco = true;
    }

    relistUpdateBAST(): Promise<any> {
        return new Promise(
            (resolve) => {
                console.log('masuk list update bast', this.selected); {
                this.vehicleSalesOrderService.executeListProcess(20, this.principal.getIdInternal(), this.selected )
                .subscribe(
                    (value) => {
                        this.findOneSal();
                        console.log('thissssss: ', value);
                        this.toasterService.showToaster('Info', 'Data Process', 'Done process in system..');
                        this.loadAll();
                    },
                    (err) => {
                        console.log(err);
                        this.toasterService.showToaster('Info', 'Data Process', 'Error Process');
                    },
                    () => {
                        this.vehicleSalesOrders = new Array<VehicleSalesOrder>();
                        this.vehicleSalesOrder = new VehicleSalesOrder();
                    }
                )
            }
            resolve();
            }
        )
    }

    submitBAST() {
        this.printModal = true;
    }

    saveTagihanSubmit(sts) {
        if ( this.selected.find((wik) => (wik.dateBASTUnit === null) === (wik.dateTakePicture === null) === (wik.dateSwipeMachine === null) === (wik.dateCoverNote === null) === (wik.dateCustomerReceipt === null))) {
                this.confirmationService.confirm({
                    message: 'Is Data Not Complete',
                    header: 'Confirmation',
                    icon: 'fa fa-question-circle',
                    acceptVisible: true,
                    rejectVisible: false
                });
        } else {
            this.loadingService.loadingStart();
            this.vehicleSalesOrderService.executeListProcess(18, null, this.selected)
            .subscribe(
                (value) => {
                    console.log('this: ', value);
                    this.selected = new Array<VehicleSalesOrder>();
                    this.loadAll();
                },
                (err) => console.log(err),
                () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
            )
        }
        };

    doPrintBAST() {
        this.printModal = false;
        if (this.selected.find((cuy) => (cuy.bastFinco === null) === (cuy.name === null))) {
            this.mappingDataPengambil().then(
                () => {
                    this.listUpdateBAST()
                    console.log('this', this.vehicleSalesOrder.bastFinco);
                }
            );
        } else {
        this.mappingDataPengambil().then(
            () => {
                this.relistUpdateBAST()
            }
        )
        }
    }

    mappingDataPengambil(): Promise<any> {
        return new Promise(
            (resolve) => {
                const data = this.selected;
                data.forEach (
                        (res) => {
                            res.identityNumber = this.vehicleSalesOrder.identityNumber;
                            res.name = this.vehicleSalesOrder.name;
                        }
                )
                resolve();
            }
        )
    }

    ngOnInit() {
        this.loadList();
        this.loadAllSubmit();
        this.loadAll();
        console.log(this.loadList, 'suck');
        console.log(this.vehicleSalesOrderSubmit);
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInVehicleSalesOrders();
        // this.tglAwal = Date;
        // this.tglAkhir = Date;
        this.loadLeasing()
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VehicleSalesOrder) {
        return item.idOrder;
    }

    registerChangeInVehicleSalesOrders() {
        this.eventSubscriber = this.eventManager.subscribe('vehicleSalesOrderListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idOrder') {
            result.push('idOrder');
        }
        return result;
    }

    sortleasing() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idOrderLeasing') {
            result.push('idOrderLeasing');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.loadingService.loadingStop();
        this.vehicleSalesOrders = data;
    }

    protected onSuccessT(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItemsLeasing = headers.get('X-Total-Count');
        this.queryCount = this.totalItemsLeasing;
        this.loadingService.loadingStop();
        this.TagihanLeasing = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.vehicleSalesOrderService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    loadDataLazySubmit(event: LazyLoadEvent) {
        this.itemsPerPageSubmit = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPageSubmit) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAllSubmit();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleSalesOrderService.update(event.data)
                .subscribe((res: VehicleSalesOrder) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleSalesOrderService.create(event.data)
                .subscribe((res: VehicleSalesOrder) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: VehicleSalesOrder) {
        this.toasterService.showToaster('info', 'VehicleSalesOrder Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleSalesOrderService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'vehicleSalesOrderListModification',
                    content: 'Deleted an vehicleSalesOrder'
                    });
                });
            }
        });
    }

    private subscribeToSaveResponse(result: Observable<VehicleSalesOrder>) {
        result.subscribe((res: VehicleSalesOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: VehicleSalesOrder) {
        this.eventManager.broadcast({ name: 'vehicleSalesOrderListModification', content: 'OK'});
        this.toasterService.showToaster('info', 'Save', 'Tagihan Submitted !');
        this.isSaving = false;
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    getDescription(currentStatus: number) {
        return this.vehicleSalesOrderService.getDescriptionStatus(currentStatus);
    }

    searching(query) {
        if (!query) {
            return this.clearing();
        }
        if (this.isMatchingMenu === true) {
            this.page = 0;
            this.currentSearching = query;
            this.router.navigate(['/admin-po-leasing', {
                search: this.currentSearching,
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }]);
            console.log('tes searcing');
            this.loadAll();
        }
            this.page = 0;
            this.currentSearching = query;
            this.router.navigate(['/admin-po-leasing', {
                search: this.currentSearching,
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }]);
            this.loadAll();

    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        if (this.isMatchingMenuSubmit === true) {
            this.page = 0;
            this.currentSearchingSubmit = query;
            this.router.navigate(['/admin-po-leasing', {
                search: this.currentSearchingSubmit,
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }]);
            console.log('tes searcing');
            this.loadAllSubmit();
        }
            this.page = 0;
            this.currentSearchingSubmit = query;
            this.router.navigate(['/admin-po-leasing', {
                search: this.currentSearchingSubmit,
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }]);
            this.loadAllSubmit();

    }

    buildReindex() {
        this.vehicleSalesOrderService.processAdminHandling({command: 'buildIndexVSO'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }

    filter() {
        this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered );
        this.adminHandlingParameter = new AdminPOLeasingParameter();
        this.adminHandlingParameter.internalId = this.principal.getIdInternal();
        if ( (this.adminHandlingParameter.vsoDateFrom != null || this.adminHandlingParameter.vsoDateThru !== undefined) &&
              (this.adminHandlingParameter.vsoDateFrom != null || this.adminHandlingParameter.vsoDateThru !== undefined)) {
                this.adminHandlingParameter.vsoDateFrom = this.tgl1.toJSON();
                this.adminHandlingParameter.vsoDateThru = this.tgl2.toJSON();
        }

        if (this.selectedLeasing !== null && this.selectedLeasing !== undefined) {
            this.adminHandlingParameter.finco = this.selectedLeasing;
        }

        if ( (this.adminHandlingParameter.ivuDateFrom != null || this.adminHandlingParameter.ivuDateThru !== undefined) &&
              (this.adminHandlingParameter.ivuDateFrom != null || this.adminHandlingParameter.ivuDateThru !== undefined)) {
                this.adminHandlingParameter.ivuDateFrom = this.tgl1.toJSON();
                this.adminHandlingParameter.ivuDateThru = this.tgl2.toJSON();
        }

        if ( this.selectedStatusVso != null && this.selectedStatusVso !== undefined) {
            this.adminHandlingParameter.status = [this.selectedStatusVso];
            this.adminHandlingParameter.statusNotIn = [0];
        }
        this.vehicleSalesOrderService.queryFilterAdminHandling({
            ordersPTO: this.adminHandlingParameter ,
            page: this.page - 1,
            size: this.itemsPerPage,
            // sort: this.sort()
        })
            .toPromise()
            .then((res) => {
                this.loadingService.loadingStop(),
                this.onSuccess(res.json, res.headers)
            }
        );

        // console.log('tanggal 1', this.adminHandlingParameter.orderDateFrom);
        // console.log('tanggal 2', this.adminHandlingParameter.orderDateThru);
    }

    protected convertListForSelect(data: Array<VehicleSalesOrder>) {
        this.listbastForSelect = this.vehicleSalesOrderService.convertListFinco(data);
    }

    private loadList(): Promise<VehicleSalesOrder[]> {
        return new Promise<VehicleSalesOrder[]>(
            (resolve) => {
                this.vehicleSalesOrderService.queryFilterByy({
                    idInternal: this.principal.getIdInternal(),
                    sort: ['bastFinco', 'asc'],
                    size: 9000,
                    queryFrom: 'bastFinco'
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.listbast = res.json;
                        this.convertListForSelect(res.json);
                        console.log('bast Finco', this.listbast)
                    },
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => {
                });
                resolve();
            }
        )
    }

}
