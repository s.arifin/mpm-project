import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { OrdersParameter } from '../../orders/orders-parameter.model'
import {Observable} from 'rxjs/Rx';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { VehicleSalesOrder } from '../vehicle-sales-order.model';
import { VehicleSalesOrderService } from '../vehicle-sales-order.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { LeasingCompanyService, LeasingCompany } from '../../leasing-company';
import { CalendarModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { SelectItem} from 'primeng/primeng';

import * as AdminHandlingConstant from '../../../shared/constants/vehicle-sales-order.constants';

@Component({
    selector: 'jhi-tagihan-leasing',
    templateUrl: './tagihan-leasing.component.html'
})
export class TagihanLeasingComponent implements OnInit, OnDestroy {

    currentAccount: any;
    vehicleSalesOrders: VehicleSalesOrder[];
    vso: VehicleSalesOrder;
    ordersParameter: OrdersParameter;
    vehicleSalesOrderSubmit: VehicleSalesOrder[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    tgl1: Date;
    tgl2: Date;
    tgl3: Date;
    tgl4: Date;
    leasingStatusPayment: any;
    newTagihan: Boolean = false;
    newPending: Boolean = false;
    isSaving: boolean;
    isFiltered: Boolean;
    selectedStatusVso: Number;
    isMatchingMenu: Boolean ;
    leasings: SelectItem[];
    selectedLeasing: string;
    statusVsoes: SelectItem[];
    korsals: SelectItem[];
    selectedKorsal: string;
    selected: VehicleSalesOrder[];
    currentSearching: string;

    constructor(
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private toasterService: ToasterService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private loadingService: LoadingService,
        private leasingCompanyService: LeasingCompanyService,
        private paginationConfig: PaginationConfig,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';

        this.leasings = [];
        this.korsals = [];
        this.statusVsoes = [];
        this.statusVsoes.push({label: 'OPEN', value: 10});
        this.statusVsoes.push({label: 'COMPLETED', value: 17 });
    }

    loadAll() {
        this.loadingService.loadingStart();
        try {
            console.log('is filter = ', this.isFiltered );
            console.log('masuk load all apakah is matching menu ', this.isMatchingMenu)
            if (this.isMatchingMenu === true) {

                if (this.isFiltered) {
                    this.vehicleSalesOrderService.findVsoForManualmatching({
                        idInternal: this.principal.getIdInternal(),
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                        // sort: this.sort()
                    }).subscribe(
                            (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                            (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                        );
                    return;
                }

                this.vehicleSalesOrderService.findVsoForManualmatching({
                    idInternal: this.principal.getIdInternal(),
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    // sort: this.sort()
                })
                    .toPromise()
                    .then(
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                );

                if (this.currentSearching) {
                    console.log('SEARCH');
                    this.vehicleSalesOrderService.searchUnit({
                        idInternal: this.principal.getIdInternal(),
                        page: this.page - 1,
                        query: this.currentSearching,
                        size: this.itemsPerPage,
                        // sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                        );
                    return;
                }
            }else {
                if (this.isFiltered === true) {
                    console.log('apakah masuk search ?? di vso utama??');
                    this.vehicleSalesOrderService.queryFilterBy({
                        ordersPTO: this.ordersParameter ,
                        idInternal: this.principal.getIdInternal(),
                        page: this.page - 1,
                        size: this.itemsPerPage,
                    }).subscribe(
                            (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                            (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                        );
                    return;
                }

                if (this.currentSearching) {
                    console.log('SEARCH');
                    this.vehicleSalesOrderService.searchUnit({
                        idInternal: this.principal.getIdInternal(),
                        page: this.page - 1,
                        query: this.currentSearching,
                        size: this.itemsPerPage,
                        // sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                        );
                    return;
                }

                this.vehicleSalesOrderService.queryLeasingSubmit({
                    idInternal: this.principal.getIdInternal(),
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    // sort: this.sort()
                })
                    .toPromise()
                    .then(
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                );
            }
        }catch (eror) {
            // this.loadingService.loadingStop()
        }

    }

    // loadAll() {
    //     this.vehicleSalesOrderService.queryLeasing({
    //         idInternal: this.principal.getIdInternal(),
    //         page: this.page - 1,
    //         size: this.itemsPerPage,
    //         }).subscribe(
    //             (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
    //             (res: ResponseWrapper) => this.onError(res.json)
    //         )
    // }

    loadAllSubmit() {
        this.vehicleSalesOrderService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            )
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/vehicle-sales-order'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'desc' : 'asc')
            }
        });
        this.loadAll();
        // this.loadAllSubmit();
    }

    private AR() {

    }

    loadLeasing() {
        this.leasingCompanyService.query({
            page: 0,
            size: 100}).subscribe(
            (res: ResponseWrapper) => this.leasings = res.json,
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    submitBAST(bast) {
        this.confirmationService.confirm({
            header : 'Confirmation',
            message : 'Anda akan melakukan cetak BAST Tagihan ke Finance Company?',
            accept: () => {
                this.vehicleSalesOrderService.executeProcess(AdminHandlingConstant.APPROVAL_BAST, null, bast).subscribe(
                    (res) => {
                        this.eventManager.broadcast({
                            name: 'vehicleSalesOrderListModification',
                            content: 'Cetak BAST Tagihan ke Finance Company Success'
                        });
                    },
                    (err) => {
                        this.onError(err);
                    }
                );
            }
        });
    }

    clear() {
        if (this.isMatchingMenu === true) {
            this.page = 0;
            this.currentSearching = '';
            this.router.navigate(['/vehicle-sales-order/true']);
            this.loadAll();
        }

        if (this.isMatchingMenu === false) {
            this.page = 0;
            this.currentSearching = '';
            this.router.navigate(['/vehicle-sales-order']);
            this.loadAll();
        }
    }

    searching(query) {
        if (!query) {
            return this.clear();
        }
        if (this.isMatchingMenu === true) {
            this.page = 0;
            this.currentSearching = query;
            this.router.navigate(['/vehicle-sales-order/true', {
                search: this.currentSearching,
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }]);
            this.loadAll();
        }
            this.page = 0;
            this.currentSearching = query;
            this.router.navigate(['/vehicle-sales-order', {
                search: this.currentSearching,
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }]);
            this.loadAll();

    }

    // search(query) {
    //     if (!query) {
    //         return this.clear();
    //     }
    //     this.page = 0;
    //     this.currentSearch = query;
    //     this.router.navigate(['/admin-po-leasing', {
    //         search: this.currentSearch,
    //         page: this.page,
    //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
    //     }]);
    //     this.loadAll();
    // }

    search() {
        this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered );
        this.ordersParameter = new OrdersParameter();
        this.ordersParameter.internalId = this.principal.getIdInternal();
        if ( this.selectedLeasing != null && this.selectedLeasing !== undefined) {
                this.ordersParameter.leasingCompanyId = this.selectedLeasing;
        }

        if ( this.selectedStatusVso != null && this.selectedStatusVso !== undefined) {
                this.ordersParameter.status = [this.selectedStatusVso];
                this.ordersParameter.statusNotIn = [0];
                // this.ordersParameter.orderDateFrom = this.tgl1.toJSON();
                // this.ordersParameter.orderDateThru = this.tgl2.toJSON();
            }
        if ( (this.ordersParameter.orderDateFrom != null || this.ordersParameter.orderDateFrom !== undefined) &&
              (this.ordersParameter.orderDateThru != null || this.ordersParameter.orderDateThru !== undefined)) {
                this.ordersParameter.orderDateFrom = this.tgl1.toJSON();
                this.ordersParameter.orderDateThru = this.tgl2.toJSON();
        }
        this.vehicleSalesOrderService.queryFilterBy({
            ordersPTO: this.ordersParameter ,
            page: this.page - 1,
            size: this.itemsPerPage,
            // sort: this.sort()
        })
            .toPromise()
            .then((res) => {
                this.loadingService.loadingStop(),
                this.onSuccess(res.json, res.headers)
            }
        );

        console.log('tanggal 1', this.ordersParameter.orderDateFrom);
        console.log('tanggal 2', this.ordersParameter.orderDateThru);
    }

    saveUploadFaktur() {
    };

    trackleasingComById(index: number, item: LeasingCompany) {
        return item.idPartyRole;
    }

    trackStatusVsoById(index: number, item: any) {
        return item.value;
    }

    ngOnInit() {
        // this.loadAllSubmit();
        this.loadAll();
        console.log(this.vehicleSalesOrders);
        console.log(this.vehicleSalesOrderSubmit);
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInVehicleSalesOrders();
        // this.tglAwal = Date;
        // this.tglAkhir = Date;
        this.loadLeasing()
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VehicleSalesOrder) {
        return item.idOrder;
    }

    registerChangeInVehicleSalesOrders() {
        this.eventSubscriber = this.eventManager.subscribe('vehicleSalesOrderListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idOrder') {
            result.push('idOrder');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.loadingService.loadingStop();
        // this.page = pagingParams.page;
        // console.log(data);

        // console.log(data.salesUnitRequirement.productId);

        data.forEach((vsoIterate) => {
            vsoIterate.statusMotor = 'Biasa';
            // var hasil = this.cekIfHotItem(data.salesUnitRequirement.productId);
            // if ( statusIsHot !== ''){
            //     vsoIterate.statusMotor = statusIsHot;
            // }
            // console.log('Hasil : ', hasil);
            // console.log('isi data.sur ====> ', data);
        });

        this.vehicleSalesOrders = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.vehicleSalesOrderService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    // approval(data: VehicleSalesOrder) {
    //     this.confirmationService.confirm({
    //         message: 'Do you want to Approve this PO?',
    //         header: 'Confirmation',
    //         icon: 'fa fa-question-circle',
    //         accept: () => {
    //             this.vehicleSalesOrderService.executeProcess(AdminHandlingConstant.VSO_APPROVAL_ADMIN_HANDLING, null, data).subscribe(
    //                 (res) => {
    //                     this.vehicleSalesOrders = new Array<VehicleSalesOrder>();
    //                     this.loadAll();
    //                 }
    //             )
    //         }
    //     });
    // }

    approval(data: VehicleSalesOrder) {
        this.router.navigate(['admin-po-leasing-detail/' + data.idOrder + '/detail']);
    }

    loadDataLazySubmit(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAllSubmit();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleSalesOrderService.update(event.data)
                .subscribe((res: VehicleSalesOrder) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleSalesOrderService.create(event.data)
                .subscribe((res: VehicleSalesOrder) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: VehicleSalesOrder) {
        this.toasterService.showToaster('info', 'VehicleSalesOrder Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleSalesOrderService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'vehicleSalesOrderListModification',
                    content: 'Deleted an vehicleSalesOrder'
                    });
                });
            }
        });
    }

    buildReindex() {
        this.vehicleSalesOrderService.process({command: 'buildIndex'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }

    updateLeasing(data?: any) {
        this.isSaving = true;
        this.leasingStatusPayment = 1;
        data.idLeasingStatusPayment = this.leasingStatusPayment;
        console.log(data.idLeasingStatusPayment);
        this.subscribeToSaveResponse(
            this.vehicleSalesOrderService.update(data));
    }

    private subscribeToSaveResponse(result: Observable<VehicleSalesOrder>) {
        result.subscribe((res: VehicleSalesOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: VehicleSalesOrder) {
        this.eventManager.broadcast({ name: 'vehicleSalesOrderListModification', content: 'OK'});
        this.toasterService.showToaster('info', 'Save', 'Tagihan Submitted !');
        this.isSaving = false;
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    getDescription(currentStatus: number) {
        return this.vehicleSalesOrderService.getDescriptionStatus(currentStatus);
    }

    getStatusMotor(idProduct: string) {
        const hasil = 'Biasa';

        console.log('Id product : ', idProduct);
        // this.ruleHotItemService.findAllByidProduct(idProduct)
        //     .subscribe(
        //         (res) => console.log(res),
        //         (res) => console.log(res),
        // );
        // var isCekHotItm =  this.cekIfHotItem(idProduct);
        // if ( isCekHotItm !== ''){
        //     hasil = isCekHotItm;
        //     console.log('Result is hot item');
        // }
        return hasil ;
    }
}
