import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Response} from '@angular/http';
import { SalesUnitRequirement, SalesUnitRequirementService } from '../../sales-unit-requirement';
import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';
import { ReportUtilService } from '../../../shared/report/report-util.service';
import {VehicleSalesOrder} from '../vehicle-sales-order.model';
import {VehicleSalesOrderPopupService} from '../vehicle-sales-order-popup.service';
import {VehicleSalesOrderService} from '../vehicle-sales-order.service';
import {ToasterService} from '../../../shared';
import { Internal, InternalService } from '../../internal';
import { Customer, CustomerService } from '../../customer';
import { BillTo, BillToService } from '../../bill-to';
import { ResponseWrapper } from '../../../shared';
import { CheckboxModule } from 'primeng/primeng';
import { adminState } from '../../../admin';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
    selector: 'jhi-admin-po-leasing-dialog',
    templateUrl: './admin-po-leasing-dialog.component.html'
})
export class AdminPoLeasingDialogComponent implements OnInit {
    sur: SalesUnitRequirement;
    vehicleSalesOrder: VehicleSalesOrder;
    isSaving: boolean;
    vehicleSalesOrders: VehicleSalesOrder[];
    dateBASTUnit: boolean;
    dateCustomerReceipt: boolean;
    nilaiIVU: number;
    dateSwipeMachine: boolean;
    dateTakePicture: boolean;
    checkedBASTUnit: boolean;
    checkedKuitansiTagihan: boolean;
    checkedGesekan: boolean;
    checkedLampiran: boolean;
    idOrder: any;
    billingNumber: any;
    dateBilling: any;
    billingAmount: any;
    requirementNumber: any;
    input_BAST_Unit: any;
    input_Kuitansi_Tagihan: any;
    input_Gesekan: any;
    input_Lampiran: any;
    bast: any;
    kuitansi: any;
    gesekan: any;
    lampiran: any;
    cover: any;
    internals: Internal[];
    customers: Customer[];
    billtos: BillTo[];
    nilaiPO: number;
    podate: Date;
    billnum: any;
    billdat: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private reportUtilService: ReportUtilService,
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private salesUnitRequirementService: SalesUnitRequirementService,
        private internalService: InternalService,
        private customerService: CustomerService,
        private billToService: BillToService,
        private eventManager: JhiEventManager,
        private router: Router,
        private toasterService: ToasterService,
        private toaster: ToasterService
    ) {
       this.dateBASTUnit = false;
       this.dateCustomerReceipt = false;
       this.dateSwipeMachine = false;
       this.dateTakePicture = false;
       this.sur = new SalesUnitRequirement();
       this.vehicleSalesOrder = new VehicleSalesOrder();
    }

    ngOnInit() {
        this.isSaving = false;
        this.vehicleSalesOrderService.query()
            .subscribe((res: ResponseWrapper) => {this.vehicleSalesOrders = res.json}, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        if (this.vehicleSalesOrder.dateBASTUnit !== null) {
            this.checkedBASTUnit = true;
        }
        if (this.vehicleSalesOrder.dateCustomerReceipt !== null) {
            this.checkedKuitansiTagihan = true;
        }
        if (this.vehicleSalesOrder.dateSwipeMachine !== null) {
            this.checkedGesekan = true;
        }
        if (this.vehicleSalesOrder.dateTakePicture !== null) {
            this.checkedLampiran = true;
        }
        if (this.vehicleSalesOrder.dateCoverNote !== null) {
            this.cover = this.vehicleSalesOrder.dateCoverNote;
        }
        if (this.vehicleSalesOrder.salesUnitRequirement.podate != null) {
            this.vehicleSalesOrder.salesUnitRequirement.podate = new Date(this.vehicleSalesOrder.salesUnitRequirement.podate)
        }
        this.podate = this.vehicleSalesOrder.salesUnitRequirement.podate;

        this.nilaiIVU = this.vehicleSalesOrder.salesUnitRequirement.hetprice + this.vehicleSalesOrder.salesUnitRequirement.bbnprice - (
            (this.vehicleSalesOrder.salesUnitRequirement.subsfincomp + this.vehicleSalesOrder.salesUnitRequirement.subsmd + this.vehicleSalesOrder.salesUnitRequirement.subsown + this.vehicleSalesOrder.salesUnitRequirement.subsahm)
        );

        this.nilaiPO =  (this.vehicleSalesOrder.salesUnitRequirement.hetprice + this.vehicleSalesOrder.salesUnitRequirement.bbnprice - (
            (this.vehicleSalesOrder.salesUnitRequirement.subsfincomp + this.vehicleSalesOrder.salesUnitRequirement.subsmd + this.vehicleSalesOrder.salesUnitRequirement.subsown +
            this.vehicleSalesOrder.salesUnitRequirement.subsahm))
            - (this.vehicleSalesOrder.salesUnitRequirement.creditDownPayment - (this.vehicleSalesOrder.salesUnitRequirement.subsfincomp + this.vehicleSalesOrder.salesUnitRequirement.subsmd +
            this.vehicleSalesOrder.salesUnitRequirement.subsown + this.vehicleSalesOrder.salesUnitRequirement.subsahm)) );
    }

    setBAST(): void {
        this.bast = new Date();
        this.vehicleSalesOrder.dateBASTUnit = this.bast;
    }

    setKuitansi(): void {
        this.kuitansi = new Date();
        this.vehicleSalesOrder.dateCustomerReceipt = this.kuitansi;
    }

    setGesekan(): void {
        this.gesekan = new Date();
        this.vehicleSalesOrder.dateSwipeMachine = this.gesekan;
    }

    setLampiran(): void {
        this.lampiran = new Date();
        this.vehicleSalesOrder.dateTakePicture = this.lampiran;
    }

    setCover(bnum: string ) {
        this.cover = new Date();
        this.vehicleSalesOrder.dateCoverNote = this.cover;
        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.viewFile('/api/report/cover_note/pdf', { noivu: bnum});
        console.log('kenapagajalan', bnum );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
            if (this.vehicleSalesOrder.idOrder !== undefined) {
                this.sur = this.vehicleSalesOrder.salesUnitRequirement;
                    this.subscribeToSaveResponse(
                        this.salesUnitRequirementService.update(this.vehicleSalesOrder.salesUnitRequirement));
                    this.subscribeToSaveResponse(
                        this.vehicleSalesOrderService.executeProcess(18, null, this.vehicleSalesOrder)
                    )
                    } else {
                        this.subscribeToSaveResponse(
                        this.vehicleSalesOrderService.create(this.vehicleSalesOrder));
            }
        }

    private subscribeToSaveResponse(result: Observable<VehicleSalesOrder>) {
        result.subscribe((res: VehicleSalesOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: VehicleSalesOrder) {
        this.eventManager.broadcast({ name: 'vehicleSalesOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vehicleSalesOrder saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'vehicleSalesOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}

@Component({
    selector: 'jhi-admin-po-leasing-popup',
    template: ''
})
export class AdminPoLeasingPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vehicleSalesOrderPopupService: VehicleSalesOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vehicleSalesOrderPopupService
                    .open(AdminPoLeasingDialogComponent as Component, params['id']);
            } else {
                this.vehicleSalesOrderPopupService
                    .open(AdminPoLeasingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
