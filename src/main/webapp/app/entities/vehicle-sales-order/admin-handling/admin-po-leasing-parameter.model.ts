export class AdminPOLeasingParameter {
    constructor(
        public vsoDateFrom?: any,
        public vsoDateThru?: any,
        public finco?: string,
        public ivuDateFrom?: any,
        public ivuDateThru?: any,
        public status?: Array<Number>,
        public internalId?: String,
        public statusNotIn?: Array<Number>,
    ) {
        this.vsoDateFrom = null;
        this.vsoDateThru = null;
        this.finco = null;
        this.ivuDateFrom = null;
        this.ivuDateThru = null;
        this.status = [10, 11, 18, 74, 61, 13, 17, 88];
        this.internalId = null;
        this.statusNotIn = [];

    }
}
