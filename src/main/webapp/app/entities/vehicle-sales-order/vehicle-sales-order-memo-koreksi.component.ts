import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { ResponseWrapper } from '../../shared';
import { Subscription, Observable } from 'rxjs/Rx';

import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ToasterService} from '../../shared';

import { FeatureService } from '../feature/feature.service';
import { Feature } from '../feature/feature.model';
import { SelectItem } from 'primeng/primeng';

import { VehicleSalesBillingService } from '../vehicle-sales-billing/vehicle-sales-billing.service';
import { VehicleSalesBilling } from '../vehicle-sales-billing/vehicle-sales-billing.model';

import { BillingItemService } from '../billing-item/billing-item.service';
import { BillingItem } from '../billing-item/billing-item.model';

@Component({
    selector: 'jhi-vehicle-sales-order-memo-koreksi',
    templateUrl: './vehicle-sales-order-memo-koreksi.component.html',
    styles: [`
       .kotak {
            border : 2px solid black;
            margin : 16px;
            padding : 16px;
       }
    `]
})
export class VehicleSalesOrderMemoKoreksiComponent implements OnInit, OnDestroy {

    private subscription: Subscription;

    listMemo: boolean;
    colors: any[];
    hasilColor: any[];
    orders: any;
    selectedColor: any
    tglMemo: Date;
    cariNoInput: string;
    vehicleSalesBilling: VehicleSalesBilling
    cariColor: any;
    billingItem: BillingItem;
    billingItems: BillingItem[];
    vsbSubscriber: Subscription;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private featureService: FeatureService,
        private toaster: ToasterService,
        private alertService: JhiAlertService,
        private vehicleSalesBillingService: VehicleSalesBillingService,
        private billingItemService: BillingItemService,
    ) {
    }

    ngOnInit() {
        // this.subscription = this.route.params.subscribe((params) => {
        //     if (params['id']) {
        //         this.vehicleSalesOrderId = params['id'];
        //         this.load(params['id']);
        //     }
        // });

        // this.subscription = this
        this.featureService
            .query()
            .subscribe(
                ( res: ResponseWrapper ) => { this.colors = res.json, console.log('feature => ', this.colors)  },
                ( res: ResponseWrapper ) => { this.onError(res.json) }
            )

        this.orders = [
            {field: 'xxx', header: 'xxx'},
            {field: 'yyy', header: 'yyy'},
        ];

        this.listMemo = true;
        this.tglMemo = new Date();
        this.registerGetChooseVsb();
    }

    ngOnDestroy() {
        this.vsbSubscriber.unsubscribe();
    }

    registerGetChooseVsb() {
        this.vsbSubscriber = this.vehicleSalesBillingService.values.subscribe(
            (response) => {
                // console.log('response', response);
                this.vehicleSalesBilling = response;
                if (this.vehicleSalesBilling === undefined  ) { return };
                console.log('vsb terpilih dari lov adalah ==>', this.vehicleSalesBilling);
                let idBill: string ;
                idBill = this.vehicleSalesBilling.idBilling;
                console.log('cari billing item service==> ', idBill);
                this.billingItemService
                    .query({
                        idBilling: idBill
                    })
                    .subscribe(
                        ( respon: ResponseWrapper ) => {
                            console.log('isi ==> billing item servie ', respon.json) ,
                            this.billingItems = respon.json
                        })
        });
    }

    cariIVU() {
        console.log('cari =IVU =>', this.cariNoInput );
        this.vehicleSalesBillingService
            .query({
                noIvu: this.cariNoInput,
            })
            .subscribe(
                (res) => {
                            console.log('isi vsb ==> ', res.json);
                            if (res.json.length <= 0) {
                                console.log('Cari IVU tidak dapet gan');
                                this.toaster.showToaster('warning', 'Billing', 'Pencarian tidak ditemukan');
                                return false;
                            }
                            this.vehicleSalesBilling = res.json[0] ;
                            let idBill: string ;
                            idBill = this.vehicleSalesBilling.idBilling;
                            console.log('cari billing item service==> ', idBill);
                            this.billingItemService
                                .query({
                                    idBilling: idBill
                                })
                                .subscribe(
                                    ( respon: ResponseWrapper ) => {
                                                console.log('isi ==> billing item servie ', respon.json) ,
                                                this.billingItems = respon.json
                                            })
                        });
    }

    load(id) {
        // this.vehicleSalesOrderService
        //     .find(id)
        //     .subscribe((res) => {
        //         this.vehicleSalesOrder =res ;
        //         console.log("isi vso", this.vehicleSalesOrder);
        //     });
    }

    previousState() {
        this.router.navigate(['vehicle-sales-order-sales']);
    }

    cancelVso(idVso) {

    }

    memoGantiUnit() {
        this.listMemo = false;
    }

    search(event) {
        return this.colors;
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'vehicleSalesOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    closeForm() {
        this.listMemo = true;
    }
}
