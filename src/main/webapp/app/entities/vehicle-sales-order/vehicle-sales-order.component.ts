import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleSalesOrder, VehicleSalesOrderService } from '../vehicle-sales-order';
import { OrdersParameter } from '../orders/orders-parameter.model'
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { SelectItem } from 'primeng/primeng';
import { Salesman, SalesmanService} from '../salesman';

import { LeasingCompanyService, LeasingCompany } from '../leasing-company';
import { LoadingService } from '../../layouts/loading/loading.service';

import { RuleHotItemService } from '../rule-hot-item/rule-hot-item.service';
import { Observable } from 'rxjs/Observable';
import * as BaseConstant from '../../shared/constants/base.constants';

// nuse
import { AxPosting, AxPostingService } from '../axposting';
import { AxPostingLine } from '../axposting-line';
import { DatePipe } from '@angular/common';
import { constants } from 'os';

@Component({
    selector: 'jhi-vehicle-sales-order',
    templateUrl: './vehicle-sales-order.component.html'
})
export class VehicleSalesOrderComponent implements OnInit, OnDestroy {

    currentAccount: any;
    vehicleSalesOrders: VehicleSalesOrder[];
    ordersParameter: OrdersParameter;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    currentSearching: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    tgl1: Date;
    tgl2: Date;
    leasings: SelectItem[];
    selectedLeasing: string;
    korsals: Salesman[];
    salesmans: Salesman[];
    selectedKorsal: string;
    selectedSalesman: string;
    statusVsoes: SelectItem[];
    selectedStatusVso: Number;
    isLazyLoadingFirst: Boolean ;
    isMatchingMenu: Boolean ;
    isFiltered: Boolean;
    // ismatching

    // nuse
    axPosting: AxPosting;
    axPostingLines: AxPostingLine[];
    axPostingLine: AxPostingLine;

    constructor(
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toasterService: ToasterService,
        private leasingCompanyService: LeasingCompanyService,
        private loadingService: LoadingService,
        private ruleHotItemService: RuleHotItemService,
        private toaster: ToasterService,
        private salesmanService: SalesmanService,
        // nuse
        private axPostingService: AxPostingService,
        private datePipe: DatePipe
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        // this.currentSearching = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';

        this.leasings = [];

        this.statusVsoes = [];
        this.statusVsoes.push({label: 'OPEN', value: 10});
        this.statusVsoes.push({label: 'COMPLETED', value: 17 });
        this.statusVsoes.push({label: 'APPROVE', value: 61 });
        this.statusVsoes.push({label: 'MATCHING', value: 74 });
        this.statusVsoes.push({label: 'CANCELED', value: 13 });
        this.statusVsoes.push({label: 'WAITING ADMIN HANDLING', value: 88 });
        this.tgl1 = new Date();
        this.tgl2 = new Date();
        this.ordersParameter = new OrdersParameter();
        this.korsals = [];
        this.salesmans = [];
        this.isFiltered = false;
        this.isMatchingMenu = false;

    }

    loadLeasing() {
        this.leasingCompanyService.query({
            page: 0,
            size: 100}).subscribe(
            (res: ResponseWrapper) => this.leasings = res.json,
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadAll() {
        this.loadingService.loadingStart();
        try {
            console.log('is filter = ', this.isFiltered );
            console.log('masuk load all apakah is matching menu ', this.isMatchingMenu)
            if (this.isMatchingMenu === true) {

                if (this.isFiltered) {
                    this.vehicleSalesOrderService.findVsoForManualmatching({
                        idInternal: this.principal.getIdInternal(),
                        page: this.page - 1,
                        query: this.currentSearch,
                        size: this.itemsPerPage,
                    }).subscribe(
                            (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                            (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                        );
                    return;
                }

                this.vehicleSalesOrderService.findVsoForManualmatching({
                    idInternal: this.principal.getIdInternal(),
                    page: this.page - 1,
                    size: this.itemsPerPage,
                })
                    .toPromise()
                    .then(
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                );

                if (this.currentSearching) {
                    console.log('SEARCH');
                    this.vehicleSalesOrderService.querytoSearch({
                        idInternal: this.principal.getIdInternal(),
                        page: this.page - 1,
                        queryfor: 'search',
                        data: this.currentSearching,
                        size: this.itemsPerPage,
                        // sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                        );
                    return;
                }
            }else {
                if (this.isFiltered === true) {
                    console.log('apakah masuk search ?? di vso utama??');
                    this.vehicleSalesOrderService.queryFilterBy({
                        ordersPTO: this.ordersParameter ,
                        idInternal: this.principal.getIdInternal(),
                        page: this.page - 1,
                        size: this.itemsPerPage,
                        // sort: this.sort()
                    }).subscribe(
                            (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                            (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                        );
                    return;
                }

                if (this.currentSearching) {
                    console.log('SEARCH');
                    this.vehicleSalesOrderService.querytoSearch({
                        idInternal: this.principal.getIdInternal(),
                        page: this.page - 1,
                        queryfor: 'search',
                        data: this.currentSearching,
                        size: this.itemsPerPage,
                        // sort: this.sort()
                    }).subscribe(
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                        );
                    return;
                }

                this.vehicleSalesOrderService.query({
                    idInternal: this.principal.getIdInternal(),
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    // sort: this.sort()
                })
                    .toPromise()
                    .then(
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onSuccess(res.json, res.headers)},
                        (res: ResponseWrapper) => {this.loadingService.loadingStop(), this.onError(res.json)}
                );
            }
        }catch (eror) {
            // this.loadingService.loadingStop()
        }

    }

    private  loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('tarik data korsal', res.json);
                this.korsals = res.json;
            }
        )
    }

    public selectSalesByKorsal(): void {
        const obj: object = {
            idCoordinator : this.selectedKorsal
        }
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('selected Sales' , res.json);
                this.salesmans = res.json;
            }
        )
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/vehicle-sales-order'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
        console.log('is filter = ', this.isFiltered );
    }

    clear() {
        if (this.isMatchingMenu === true) {
            this.page = 0;
            this.currentSearching = '';
            this.router.navigate(['/vehicle-sales-order/true']);
            this.loadAll();
        }

        if (this.isMatchingMenu === false) {
            this.page = 0;
            this.currentSearching = '';
            this.router.navigate(['/vehicle-sales-order']);
            this.loadAll();
        }

    }

    search() {
        this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered );
        this.ordersParameter = new OrdersParameter();
        this.ordersParameter.internalId = this.principal.getIdInternal();
        if ( this.selectedLeasing != null && this.selectedLeasing !== undefined) {
                this.ordersParameter.leasingCompanyId = this.selectedLeasing;
        }

        if (this.selectedSalesman !== null && this.selectedSalesman !== undefined) {
            this.ordersParameter.salesmanId = this.selectedSalesman;
        }

        if ( this.selectedStatusVso != null && this.selectedStatusVso !== undefined) {
                this.ordersParameter.status = [this.selectedStatusVso];
                this.ordersParameter.statusNotIn = [0];
            }
        if ( (this.ordersParameter.orderDateFrom != null || this.ordersParameter.orderDateFrom !== undefined) &&
              (this.ordersParameter.orderDateThru != null || this.ordersParameter.orderDateThru !== undefined)) {
                this.ordersParameter.orderDateFrom = this.tgl1.toJSON();
                this.ordersParameter.orderDateThru = this.tgl2.toJSON();
        }
        this.vehicleSalesOrderService.queryFilterBy({
            ordersPTO: this.ordersParameter ,
            page: this.page - 1,
            size: this.itemsPerPage,
            // sort: this.sort()
        })
            .toPromise()
            .then((res) => {
                this.loadingService.loadingStop(),
                this.onSuccess(res.json, res.headers)
            }
        );

        console.log('tanggal 1', this.ordersParameter.orderDateFrom);
        console.log('tanggal 2', this.ordersParameter.orderDateThru);
    }

    searching(query) {
        if (!query) {
            return this.clear();
        }
        if (this.isMatchingMenu === true) {
            this.page = 0;
            this.currentSearching = query;
            this.router.navigate(['/vehicle-sales-order/true', {
                search: this.currentSearching,
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }]);
            this.loadAll();
        }
        if (this.isMatchingMenu === false) {
            this.page = 0;
            this.currentSearching = query;
            this.router.navigate(['/vehicle-sales-order', {
                search: this.currentSearching,
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }]);
            this.loadAll();
        }

    }

    ngOnInit() {

        this.activatedRoute.params.subscribe((params) => {
            if (params['ismatching']) {
                this.isMatchingMenu = true;
                console.log('Masuk matching  menu')
            } else {
                this.isMatchingMenu = false;
            }
        });

        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInVehicleSalesOrders();
        // this.tglAwal = Date;
        // this.tglAkhir = Date;
        this.loadLeasing();
        this.loadKorsal()

    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
        this.eventSubscriber.unsubscribe();
    }

    trackId(index: number, item: VehicleSalesOrder) {
        return item.idOrder;
    }

    trackleasingComById(index: number, item: LeasingCompany) {
        return item.idPartyRole;
    }

    trackkorsalById(index: number, item: Salesman) {
        return item.idPartyRole;
    }

    trackStatusVsoById(index: number, item: any) {
        return item.value;
    }

    registerChangeInVehicleSalesOrders() {
        this.eventSubscriber = this.eventManager.subscribe('vehicleSalesOrderListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateEntry') {
            result.push('dateEntry');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.loadingService.loadingStop();

        data.forEach((vsoIterate) => {
            vsoIterate.statusMotor = 'Biasa';
        });

        this.vehicleSalesOrders = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    executeProcess(data) {
        this.vehicleSalesOrderService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        if (this.isLazyLoadingFirst === false) {
            this.isLazyLoadingFirst = true;
            return ;
        }
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleSalesOrderService.update(event.data)
                .subscribe((res: VehicleSalesOrder) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleSalesOrderService.create(event.data)
                .subscribe((res: VehicleSalesOrder) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: VehicleSalesOrder) {
        this.toasterService.showToaster('info', 'VehicleSalesOrder Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleSalesOrderService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'vehicleSalesOrderListModification',
                    content: 'Deleted an vehicleSalesOrder'
                    });
                });
            }
        });
    }

    getDescription(currentStatus: number) {
        return this.vehicleSalesOrderService.getDescriptionStatus(currentStatus);
    }

    getStatusMotor(idProduct: string) {
        const hasil = 'Biasa';

        console.log('Id product : ', idProduct);
        return hasil ;
    }

    cekIfHotItem(id: string) {
        this.ruleHotItemService.query({
            idProduct: id,
            page: 0,
            size: 1})
        .toPromise()
        .then((data) => {
            return (data.json)
        });
        return '';
    };

    subscribeToSaveResponse(result: Observable<VehicleSalesOrder>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe((res: VehicleSalesOrder) => {
                    resolve();
                });
            });
        }

    batalVso(vso: VehicleSalesOrder ) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to cancel vso [' + vso.salesUnitRequirement.requirementNumber + '] ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.subscribeToSaveResponse(this.vehicleSalesOrderService.executeProcess(13, null, vso))
                .then(
                    () => {
                        this.eventManager.broadcast({
                        name: 'vehicleSalesOrderListModification',
                        content: 'cancel an vehicleSalesOrder'
                        });
                    this.toaster.showToaster('info', 'Data Proces', 'Cancel VSO');
                    this.loadAll();
                    }
                );
            }
        });
    }
    buildReindex() {
        this.vehicleSalesOrderService.process({command: 'buildIndexVSO'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }
}
