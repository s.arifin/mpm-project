import {Pipe, PipeTransform } from '@angular/core';

@Pipe({
   name : 'VehicleSalesOrderStatusPipe'
})

export class VehicleSalesOrderStatusPipe implements PipeTransform {

    transform(value: number): string {
       return 'approved';
    }
}
