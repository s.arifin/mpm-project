import {Component, OnChanges, Input, SimpleChanges, AfterViewInit, Output, EventEmitter, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from '../../layouts/loading';

import { setTimeout } from 'timers';
import * as BaseConstants from '../../shared/constants/base.constants';
import { SalesUnitRequirement } from '../sales-unit-requirement/sales-unit-requirement.model';
import { SalesUnitRequirementForProspectByApprovalComponent } from '../sales-unit-requirement/prospect/sales-unit-requirement-for-prospect-by-approval.component'

@Component({
    selector: 'jhi-vso-draft-korsal',
    templateUrl: './vso-draft-korsal.component.html'
})
export class VsoDraftKorsalComponent implements OnChanges, AfterViewInit, OnInit {
    @Input()
    public idProspect: string

    @Output()
    public fnAfterSave = new EventEmitter();

    public salesUnitRequirement: SalesUnitRequirement;
    public editPage: boolean;
    public baseConstants: any;
    public statusOne: boolean;
    public statusTwo: boolean;
    public statusThree: boolean;
    public statusFour: boolean;
    public korsal: Boolean = true;

    constructor(
        private loadingService: LoadingService,
        private router: Router,
    ) {
        this.editPage = false;
        this.baseConstants = BaseConstants;
        this.korsal = true
    }

    ngOnChanges(changes: SimpleChanges) {
        // if (changes['idProspect']) {
            this.loadingService.loadingStart();
            this.buildComponentChild();
        // }
    }

    ngOnInit() {
         // if (changes['idProspect']) {
            this.loadingService.loadingStart();
            this.buildComponentChild();
        // }
    }

    ngAfterViewInit() {
        this.loadingService.loadingStop();
    }

    public edit(sur: SalesUnitRequirement): void {
        this.editPage = true;
        this.salesUnitRequirement = sur;
    }

    public previousState(): void {
        this.editPage = false;
        this.salesUnitRequirement = null;
    }

    private destroyComponentChild(): Promise<void> {
        return new Promise<void> (
            (resolve) => {
                this.statusOne = false;
                this.statusTwo = false;
                this.statusThree = false;
                this.statusFour = false;
                resolve();
            }
        )
    }

    private buildComponentChild(): void {
        this.statusOne = true;
        this.statusTwo = true;
        this.statusThree = true;
        this.statusFour = true;
    }

    public doRefresh(result): void {
        this.destroyComponentChild().then(
            (res) => {
                setTimeout(
                    () => {
                        this.buildComponentChild();
                }, 1000);
            }
        );
    }

    public backToProspect(): void {
        this.fnAfterSave.emit();
    }

}
