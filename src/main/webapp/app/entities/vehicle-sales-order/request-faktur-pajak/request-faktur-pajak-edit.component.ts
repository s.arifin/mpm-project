import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { VehicleSalesOrder } from '../vehicle-sales-order.model';
import { VehicleSalesOrderService } from '../vehicle-sales-order.service';
import { ToasterService} from '../../../shared';
import { Internal, InternalService } from '../../internal';
import { Customer, CustomerService } from '../../customer';
import { BillTo, BillToService } from '../../bill-to';
import { ResponseWrapper, Principal } from '../../../shared';
import {StepsModule, MenuItem} from 'primeng/primeng';
import { LoadingService } from '../../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-request-faktur-pajak-edit',
    templateUrl: './request-faktur-pajak-edit.component.html'
})

export class RequestFakturPajakEditComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    vehicleSalesOrder: VehicleSalesOrder;
    isSaving: boolean;
    isReadOnlyReceipt = true;
    internals: Internal[];
    customers: Customer[];
    billtos: BillTo[];

    // msgs: Message[] = [];
    items: MenuItem[];
    activeIndex = 1;

    stepUi = 1;
    stepLabel: String[];

    displayDialog = false;
    notApprvoceReason: String = '';
    openDialogBy: String;

    constructor(
        private alertService: JhiAlertService,
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private internalService: InternalService,
        private customerService: CustomerService,
        private billToService: BillToService,
        private route: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toaster: ToasterService,
        private principal: Principal,
        private loadingService: LoadingService,
    ) {
        this.vehicleSalesOrder = new VehicleSalesOrder();
        console.log('this.vehicleSalesOrder First', this.vehicleSalesOrder);
        this.stepUi = 1;

        this.stepLabel = ['Data Customer', 'Data STNK', 'Detil Kendaraan',
                        'Harga dan Pembayaran'];
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
    }

    load(id) {
        if (id === undefined) {
            return;
        }
        // this.loadingService.loadingStart();
        this.vehicleSalesOrderService.find(id, this.principal.getIdInternal())
            .subscribe(
                (vehicleSalesOrder) => {
                    if (vehicleSalesOrder.salesUnitRequirement.personOwner.dob !== null) {
                        vehicleSalesOrder.salesUnitRequirement.personOwner.dob = new Date(vehicleSalesOrder.salesUnitRequirement.personOwner.dob);
                    }

                    if (vehicleSalesOrder.vrtglpembayaran !== null) {
                        vehicleSalesOrder.vrtglpembayaran = new Date(vehicleSalesOrder.vrtglpembayaran);
                    }

                    if (vehicleSalesOrder.vrtglpengiriman !== null) {
                        vehicleSalesOrder.vrtglpengiriman = new Date(vehicleSalesOrder.vrtglpengiriman);
                    }

                    this.vehicleSalesOrder = vehicleSalesOrder;
                    // console.log('data dari edit html => ' , this.vehicleSalesOrder )
                },
                (err) => {},
                () => {},
                // this.loadingService.loadingStop(),
            );
    }

    closeForm() {
        this.router.navigate(['request-faktur-pajak']);
    }

    prev() {
        if (this.stepUi ===  1) {

        }else {
            this.stepUi--;
        }
    }

    next() {
        if (this.stepUi === 8) {
        }else {
            this.stepUi++;
        }
    }

    save() {
        this.isSaving = true;
        if (this.vehicleSalesOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.vehicleSalesOrderService.update(this.vehicleSalesOrder));
        } else {
            this.subscribeToSaveResponse(
                this.vehicleSalesOrderService.create(this.vehicleSalesOrder));
        }
    }

    private subscribeToSaveResponse(result: Observable<VehicleSalesOrder>) {
        result.subscribe((res: VehicleSalesOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: VehicleSalesOrder) {
        this.eventManager.broadcast({ name: 'vehicleSalesOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vehicleSalesOrder saved !');
        this.isSaving = false;
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'vehicleSalesOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    // trackCustomerById(index: number, item: Customer) {
    //     return item.idCustomer;
    // }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }

    openDialogNotif(openedBy: String): void {
        // afc  || not afc
        this.notApprvoceReason = '';
        this.openDialogBy = openedBy;
        this.displayDialog = true;
    }

    closeDialogNotif(): void {
        console.log('close dialog notif ==>', this.openDialogBy);
        if (this.openDialogBy === 'afc') {
            console.log('close dialog : afc ===', this.openDialogBy);
            this.vehicleSalesOrder.afcNotApprvNote = this.notApprvoceReason;
            this.approveAfc(2);
        }else {
            console.log('close dialog : admin sales ===', this.openDialogBy);
            this.vehicleSalesOrder.adminSalesNotApprvNote = this.notApprvoceReason;
            console.log('close dialog : admin sales ===', this.vehicleSalesOrder.adminSalesNotApprvNote);
            this.approveAdminSales(2);
        }
        this.displayDialog = false;
    }

    approveAdminSales(nilai: number): void {
        console.log('void approve admin sales =', nilai);
        this.vehicleSalesOrder.adminSalesVerifyValue = nilai;
        this.verifikasiVSO(77);
    }

    approveAfc(nilai: number): void {
        console.log('void approve afc =', nilai);
        this.vehicleSalesOrder.afcVerifyValue = nilai;
        this.verifikasiVSO(76);
    }

    verifikasiVSO(idExecProses: Number) {
        console.log('void Verifikasi VSO=', idExecProses);
        this.vehicleSalesOrderService
            .executeProcess(idExecProses, null, this.vehicleSalesOrder)
            .subscribe((res) => {
                console.log('verifikasi');
                this.toaster.showToaster('info', 'Save', 'vehicleSalesOrder approved !');
                switch (idExecProses) {
                    case 77 :
                        this.vehicleSalesOrder.adminSalesVeriDate = new Date();
                        return;
                    case 76 :
                        this.vehicleSalesOrder.afcVeriDate = new Date();
                        return ;
                }
            })
    }

    getDescription(statusValue: Number): String {
        let hasil: String;

        switch (statusValue) {
            case 0: hasil = '';
                break;
            case 1: hasil = 'Approve';
                break;
            case 2: hasil = 'Not Approve';
                break;
        }

        return hasil;
    }

}
