import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { VehicleSalesOrder } from './vehicle-sales-order.model';
import { VehicleSalesOrderService } from './vehicle-sales-order.service';
import { ToasterService, Principal, CommonUtilService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { Customer, CustomerService } from '../customer';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';
import { StepsModule, MenuItem} from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';
import 'rxjs/add/operator/toPromise';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-vehicle-sales-order-edit',
    templateUrl: './vehicle-sales-order-edit.component.html'
})
export class VehicleSalesOrderEditComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    vehicleSalesOrder: VehicleSalesOrder;
    isSaving: boolean;
    isReadOnlyReceipt = true;
    internals: Internal[];
    customers: Customer[];
    billtos: BillTo[];
    currentAccount: any;

    // msgs: Message[] = [];
    items: MenuItem[];
    activeIndex = 1;

    stepUi = 1;
    stepLabel: String[];

    displayDialogAfc = false;
    displayDialogAdmSls = false;
    notApprvoceReason: String = '';
    openDialogBy: String;
    isMatchingMenu: boolean ;
    idApproved: number;
    isDisable: Boolean = true;

    kodeApproveKacab = 61;

    constructor(
        private alertService: JhiAlertService,
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private internalService: InternalService,
        private customerService: CustomerService,
        private billToService: BillToService,
        private route: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toaster: ToasterService,
        private loadingService: LoadingService,
        private principal: Principal,
        private confirmationService: ConfirmationService,
        protected commonUtilService: CommonUtilService,
    ) {
        this.idApproved = 61;
        this.vehicleSalesOrder = new VehicleSalesOrder();
        console.log('this.vehicleSalesOrder First', this.vehicleSalesOrder);
        this.stepUi = 1;

        this.stepLabel = ['Verifikasi', 'Data Customer', 'Data STNK', 'Detail Kendaraan',
                        'File Dokumen', 'Harga dan Pembayaran', 'Note', 'Detail CDB'];
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
            if (params['ismatching']) {
                this.isMatchingMenu = true;
                console.log('Masuk matching menu')
            }else {
                this.isMatchingMenu = false;
            }
        });
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));

    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        if (id === undefined) {
            return;
        }
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.loadingService.loadingStart();
        this.vehicleSalesOrderService.find(id, this.principal.getIdInternal())
            .subscribe(
                (vehicleSalesOrder) => {
                    if (vehicleSalesOrder.salesUnitRequirement.personOwner !== null) {
                        if (vehicleSalesOrder.salesUnitRequirement.personOwner.dob !== null) {
                            vehicleSalesOrder.salesUnitRequirement.personOwner.dob = new Date(vehicleSalesOrder.salesUnitRequirement.personOwner.dob);
                        }

                        if (vehicleSalesOrder.vrtglpembayaran !== null) {
                            vehicleSalesOrder.vrtglpembayaran = new Date(vehicleSalesOrder.vrtglpembayaran);
                        }

                        if (vehicleSalesOrder.vrtglpengiriman !== null) {
                            vehicleSalesOrder.vrtglpengiriman = new Date(vehicleSalesOrder.vrtglpengiriman);
                        }
                    }
                    this.vehicleSalesOrder = vehicleSalesOrder;
                    console.log('data dari edit html => ' , this.vehicleSalesOrder )
                },
                (err) => {},
                () => this.loadingService.loadingStop(),
            );
    }

    closeForm() {
        if (this.isMatchingMenu === true) {
            this.router.navigate(['vehicle-sales-order/' + this.isMatchingMenu ]);
        }else {
            this.router.navigate(['vehicle-sales-order']);
        }
    }

    prev() {
        if (this.stepUi ===  1) {

        } else {
            this.stepUi--;
        }
    }

    next() {
        if (this.stepUi === 8) {
        } else {
            this.stepUi++;
        }
    }

    save() {
        this.isSaving = true;
        if (this.vehicleSalesOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.vehicleSalesOrderService.update(this.vehicleSalesOrder));
        } else {
            this.subscribeToSaveResponse(
                this.vehicleSalesOrderService.create(this.vehicleSalesOrder));
        }
    }

    private subscribeToSaveResponse(result: Observable<VehicleSalesOrder>) {
        result.subscribe((res: VehicleSalesOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: VehicleSalesOrder) {
        this.eventManager.broadcast({ name: 'vehicleSalesOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vehicleSalesOrder saved !');
        this.isSaving = false;
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'vehicleSalesOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    // trackCustomerById(index: number, item: Customer) {
    //     return item.idCustomer;
    // }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }

    openDialogNotif(openedBy: String): void {
        if (openedBy === 'afc') {
            this.openDialogBy = openedBy;
            this.displayDialogAfc = true;
        }
        if (openedBy === 'admsls') {
            this.openDialogBy = openedBy;
            this.displayDialogAdmSls = true;
        }

    }

    closeDialogNotif(): void {
        console.log('close dialog notif ==>', this.openDialogBy);
        if (this.openDialogBy === 'afc') {
            console.log('close dialog : afc ===', this.openDialogBy);
            console.log('close dialog : afcnote ===', this.vehicleSalesOrder.afcNotApprvNote);
            // this.vehicleSalesOrder.afcNotApprvNote = this.notApprvoceReason;
            this.approveAfc(2);
        } else {
            console.log('close dialog : admin sales ===', this.openDialogBy);
            // this.vehicleSalesOrder.adminSalesNotApprvNote = this.notApprvoceReason;
            console.log('close dialog : admin sales ===', this.vehicleSalesOrder.adminSalesNotApprvNote);
            this.approveAdminSales(2);
        }
        this.displayDialogAfc = false;
        this.displayDialogAdmSls = false;
    }

    approveAdminSales(nilai: number): void {
        console.log('void approve admin sales =', nilai);
        this.vehicleSalesOrder.adminSalesVerifyValue = nilai;
        this.verifikasiVSO(77);
    }

    approveAfc(nilai: number): void {
        if (nilai === 2) {
            console.log('void approve afc =', nilai);
            this.vehicleSalesOrder.afcVerifyValue = nilai;
            this.vehicleSalesOrder.afcVeriDate = new Date();
            this.verifikasiVSO(78);
        }
        if (nilai === 1) {
            if (this.vehicleSalesOrder.salesUnitRequirement.idReqTyp === null) {
                if (this.vehicleSalesOrder.salesUnitRequirement.prospectSourceId === 11) {
                    console.log('void approve afc =', nilai);
                    this.vehicleSalesOrder.afcVerifyValue = nilai;
                    this.verifikasiVSO(78);
                }
                if (this.vehicleSalesOrder.salesUnitRequirement.prospectSourceId !== 11) {
                    console.log('void approve afc =', nilai);
                    this.vehicleSalesOrder.afcVerifyValue = nilai;
                    this.verifikasiVSO(76);
                }
            }
        }
        if (this.vehicleSalesOrder.salesUnitRequirement.idReqTyp === 101 ||
            this.vehicleSalesOrder.salesUnitRequirement.idReqTyp === 102 ||
            this.vehicleSalesOrder.salesUnitRequirement.idReqTyp === 103 ||
            this.vehicleSalesOrder.salesUnitRequirement.idReqTyp === 104 ||
            this.vehicleSalesOrder.salesUnitRequirement.idReqTyp === 105) {
                    console.log('void approve afc =', nilai);
                    this.vehicleSalesOrder.afcVerifyValue = nilai;
                    this.verifikasiVSO(78);
        }
    }

    verifikasiVSOold(idExecProses: Number) {
        console.log('void Verifikasi VSO=', idExecProses);
        this.vehicleSalesOrderService
            .executeProcess(idExecProses, null, this.vehicleSalesOrder)
            .subscribe((res) => {
                console.log('verifikasi');
                this.toaster.showToaster('info', 'Save', 'vehicleSalesOrder approved !');
                switch (idExecProses) {
                    case 77 :
                        this.vehicleSalesOrder.adminSalesVeriDate = new Date();
                        return;
                    case 76 :
                        // jika afc approve, langsung approve kacab dan matching
                        // REQ USER per 10 Maret 2017

                        // Auto approve kacab
                        const kodeApproveKacab = 61;
                        this.vehicleSalesOrderService
                            .executeProcess(kodeApproveKacab, null, this.vehicleSalesOrder)
                            .first()
                            .subscribe((resApporoveKacab) => {
                                this.toaster.showToaster('info', 'Kacab', 'Kacab Approve  success !');

                                // Proses Automatching
                                const kodeAutoMatching = 74;
                                this.vehicleSalesOrderService
                                    .executeProcess(kodeAutoMatching, null, this.vehicleSalesOrder)
                                    .first()
                                    .subscribe((result) => {
                                        console.log('Automatching');
                                        this.toaster.showToaster('info', 'matching', 'Auto Matching success !');
                                        // this.toaster.showToaster('info', 'Approve', 'Kacab Approve !');
                                    },
                                    ( err ) => { this.toaster.showToaster('info', 'matching', 'Error ' && err); }
                                )
                                // END Proses Automatching

                            },
                            ( err ) =>  {  this.toaster.showToaster('info', 'kacab', 'Error ' && err )}
                        )
                        // END Auto approve kacab
                        this.vehicleSalesOrder.afcVeriDate = new Date();
                        return ;
                }
            })
    }

    verifikasiVSO_ok(idExecProses: Number) {
        console.log('PROMISE  Verifikasi VSO=', idExecProses);
        this.vehicleSalesOrderService
            .executeProcess(idExecProses, null, this.vehicleSalesOrder)
            .toPromise()
            .then((res) => {
                console.log('verifikasi');
                this.toaster.showToaster('info', 'Save', 'vehicleSalesOrder approved !');
                switch (idExecProses) {
                    case 77 :
                        this.vehicleSalesOrder.adminSalesVeriDate = new Date();
                        return;
                    case 76 :
                         console.log('approve kacab');
                        const kodeApproveKacab = 61;
                        this.vehicleSalesOrderService
                            .executeProcess(kodeApproveKacab, null, this.vehicleSalesOrder)
                            .toPromise()
                            .then((resApporoveKacab) => {
                                this.toaster.showToaster('info', 'Kacab', 'Kacab Approve  success !-');
                                console.log('isi Res Approve kacab =>', resApporoveKacab, '<==');
                                this.vehicleSalesOrder = resApporoveKacab;
                                if (resApporoveKacab.currentStatus === 61 ) {
                                    console.log('proses matching !!!')
                                }
                                // this.confirmationService.confirm({
                                //     message: 'Are you sure to process automatching  ?',
                                //     header: 'Confirmation',
                                //     icon: 'fa fa-question-circle',
                                //     accept: () => {
                                //     }
                                // });

                            })
                            .catch( ( err ) =>  {  this.toaster.showToaster('info', 'kacab', 'Error ' && err )} )
                        // END Auto approve kacab
                        this.vehicleSalesOrder.afcVeriDate = new Date();
                        return ;
                }
            })
            .catch( (er) => console.log('error'));
    }

    verifikasiVSO(idExecProses: Number) {
        this.loadingService.loadingStart();
        console.log('PROMISE NEW Verifikasi VSO=', idExecProses);
        this.vehicleSalesOrderService
            .executeProcessAprrove(idExecProses, null, this.vehicleSalesOrder)
            .subscribe(
                (res: ResponseWrapper) => {
                console.log('verifikasi');
                this.toaster.showToaster('info', 'Save', 'vehicleSalesOrder approved !');
                switch (idExecProses) {
                    case 77 :
                        this.vehicleSalesOrder.adminSalesVeriDate = new Date();
                        this.loadingService.loadingStop();
                        return;
                    case 78 :
                        this.vehicleSalesOrder.afcVeriDate = new Date();
                        this.loadingService.loadingStop();
                        return;
                    case 76 :
                         console.log('approve kacab');
                         this.approveKacab();
                        // END Auto approve kacab
                        this.vehicleSalesOrder.afcVeriDate = new Date();
                        this.loadingService.loadingStop();
                        return ;
                }
                this.loadingService.loadingStop();
            },
           (res: ResponseWrapper) => {
            this.commonUtilService.showError(res);
            this.loadingService.loadingStop();
           })
            // .catch((er) => {
            //     console.log('error');
            //     // this.commonUtilService.showError(er);
            //     console.log('eroooorrrr == ', er)
            //     this.toaster.showToaster('EROR', 'Information', er);
            //     this.loadingService.loadingStop();
            // });
    }

    approveKacab(): void {
        this.loadingService.loadingStart();
        this.vehicleSalesOrderService
            .executeProcessPromise(this.kodeApproveKacab, null, this.vehicleSalesOrder)
            .then((resApporoveKacab) => {
                this.toaster.showToaster('info', 'Kacab', 'Kacab Approve  success !-');
                console.log('isi Res Approve kacab =>', resApporoveKacab, '<==');
                this.vehicleSalesOrder = resApporoveKacab;
                console.log('proses matching !!!');
                this.autoMatch();
                this.loadingService.loadingStop();
            })
    };

    autoMatch(): void {
        // Proses Automatching
        this.loadingService.loadingStart();
        const kodeAutoMatching = 74;
        this.vehicleSalesOrderService
            .executeProcessPromise(kodeAutoMatching, null, this.vehicleSalesOrder)
            .then((result) => {
                console.log('Automatching isi result -->', result, '<--');
                this.toaster.showToaster('info', 'matching', 'Auto Matching success !');
                this.loadingService.loadingStop();
                // this.toaster.showToaster('info', 'Approve', 'Kacab Approve !');
            })
            .catch (( err ) => { this.toaster.showToaster('info', 'matching', 'Error ' && err) })
        // END Proses Automatching
    }

    getDescription(statusValue: Number): String {
        let hasil: String;

        switch (statusValue) {
            case 0: hasil = '';
                break;
            case 1: hasil = 'Approve';
                break;
            case 2: hasil = 'Not Approve';
                break;
        }

        return hasil;
    }

    enablePassDP() {
        this.vehicleSalesOrder.canCloseDP = false;
        this.vehicleSalesOrderService.update(this.vehicleSalesOrder).subscribe(() => {
            this.toaster.showToaster('info', 'Update Pass DP', 'Pass DP Done !');
        });
    }

    cekDisable(a: Boolean) {
        this.isDisable = a;
    }

}
