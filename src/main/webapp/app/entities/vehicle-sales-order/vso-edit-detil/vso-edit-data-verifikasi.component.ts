import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleSalesOrder } from '../vehicle-sales-order.model';
import { VehicleSalesOrderService } from '../vehicle-sales-order.service';
import { ReceiptService, Receipt } from '../../receipt';
import { LeasingTenorProvideService, LeasingTenorProvide } from '../../leasing-tenor-provide';
import { SalesUnitRequirementService, SalesUnitRequirement } from '../../sales-unit-requirement';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { ShipmentService } from '../../shipment';
import { GoodService, Good } from '../../good';

import * as _ from 'lodash';
import * as moment from 'moment';
import { forEach } from '@angular/router/src/utils/collection';
import { SaleType, SaleTypeService } from '../../sale-type';
import * as SaleTypeConstants from '../../../shared/constants/sale-type.constants';
import { OrderItem, OrderItemService } from '../../order-item';
import { resolve } from 'path';
import { promisify } from 'util';

import * as reqType from '../../../shared/constants/requirement-type.constants';
import { PersonalCustomerService, PersonalCustomer, OrganizationCustomer, OrganizationCustomerService } from '../../shared-component';

@Component({
    selector: 'jhi-vso-edit-data-verifikasi',
    templateUrl: './vso-edit-data-verifikasi.component.html'
})

export class VsoEditDataVerifikasiComponent implements OnInit, OnDestroy, OnChanges {
    @Input() data: VehicleSalesOrder = new VehicleSalesOrder();
    @Input()
    public status: any;

    VSO_VERIFIKASI= 79;
    selectedGroupTipeBayar: String  = 'COD';
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    receipts: Receipt[];
    datas: any;
    dateSchedulle: any;
    LeasingTenorProvide: LeasingTenorProvide[];
    tenors: any;
    goods: Good;
    saleTypes: SaleType[];
    saleTypeChild= [];
    selectedIsCod: any;
    totalItems: any;
    totalTandaJadi: number;
    sisaDibayar: number;
    orderItem: OrderItem;
    totalDisc: number;
    public totalPayment: number;
    personalCustomer: PersonalCustomer;
    organizationCustomer: OrganizationCustomer;
    datePayment: Date;
    isComplete: Boolean = false;

    constructor(
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private goodService: GoodService,
        private leasingTenorProvideService: LeasingTenorProvideService,
        private salesUnitRequirementService: SalesUnitRequirementService,
        private toaster: ToasterService,
        private receiptService: ReceiptService,
        private shipmentService: ShipmentService,
        private commonService: CommonUtilService,
        private saleTypeService: SaleTypeService,
        private orderItemService: OrderItemService,
        private personalCustomerService: PersonalCustomerService,
        private organizationCustomerService: OrganizationCustomerService

    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idPayment';
        this.datas = {
            dpmurni: 0,
            tandajadi: 0,
            tenor: null,
            angsuran: null,
            tanggalPembayaran: null,
            sisa: null
        }
        this.totalTandaJadi = 0;
        this.totalPayment = 0;
        this.sisaDibayar = 0;
        this.personalCustomer = new  PersonalCustomer();
        this.organizationCustomer = new OrganizationCustomer();
        this.datePayment = new Date();
    }

    ngOnInit() {
    }

    private checkIfSaleTypeIsParent(saleTypeId: number) {
        if (saleTypeId === SaleTypeConstants.CASH || saleTypeId === SaleTypeConstants.CREDIT) {
            return true;
        }
        return false;
    }

    public getDownPaymentMurni(): void {
        let dpMurni: number;
        dpMurni = this.data.vrdpmurni;

        const salesUnitRequirement: SalesUnitRequirement = this.data.salesUnitRequirement;
        if (this.saleTypeService.checkIfCredit(salesUnitRequirement.saleTypeId)) {
            const totalDiskon = this.salesUnitRequirementService.fnCountTotalSubsidi(salesUnitRequirement);
            dpMurni = salesUnitRequirement.creditDownPayment - totalDiskon;
            this.data.vrdpmurni = dpMurni;
        }
    }

    public getTotalDiskonSur(salesUnitRequirement: SalesUnitRequirement): number {
        return this.salesUnitRequirementService.fnCountTotalSubsidi(salesUnitRequirement);
    }

    private getSaleTypeDescription(data) {
        if (this.data.salesUnitRequirement.saleTypeId === SaleTypeConstants.CASH ||
            this.data.salesUnitRequirement.saleTypeId === SaleTypeConstants.CREDIT ||
            this.data.salesUnitRequirement.saleTypeId === SaleTypeConstants.CASH_OFF_THE_ROAD) {
            this.saleTypeChild.push(... this.commonService.getDataByParent(data, this.data.salesUnitRequirement.saleTypeId));
        }
        // else {
        //     this.saleTypeChild.push(_.find(this.saleTypes,
        //         function(e: SaleType) {
        //         return e.idSaleType = this.data.salesUnitRequirement.saleTypeId;
        //     }));
        // }
        console.log('lalala = ', this.saleTypeChild);
        console.log('saletype = ', this.data.salesUnitRequirement.saleTypeId);
        console.log('data terima = ', this.data.salesUnitRequirement);
    }

    ngOnDestroy() {

    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['data']) {
            if (this.status === 17 || this.status === 13 || this.status === 88 ) {
                this.isComplete = true;
            }

            console.log('on simple change')
            if (this.data.vrtglpengiriman !== null) {
                console.log('aaaa', this.data.vrtglpengiriman );
                const tanggal: Date = new Date(this.data.vrtglpengiriman);
                console.log('tanggal', tanggal);
                this.data.vrtglpengiriman = new Date(this.data.vrtglpengiriman);
            }

            if (this.data.vrtglpembayaran !== null) {
                this.data.vrtglpembayaran = new Date(this.data.vrtglpembayaran);
            }
            this.orderItemService.queryFilterBy({
                idOrders: this.data.idOrder,
                size: 100,
                page: 0,
                sort: ['idOrderItem', 'asc']
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json),
                ( ) => { }
            );
            this.getDownPaymentMurni();
            this.loadSur();
            this.cekTanggalPengiriman();
            // // this.getTotalDisc();
            // this.getSisaDibayarkan();

            this.saleTypeService.query().subscribe(
                (res) => {
                    // this.saleTypes = res.json;
                    this.getSaleTypeDescription(res.json);
                });
            if (this.data.vriscod != null || this.data.vriscod !== 0) {
                this.selectedIsCod = this.data.vriscod;
            }
            console.log('Total Diskon', this.totalDisc);

            if (this.data.salesUnitRequirement.customerId !== undefined) {
                if (this.data.salesUnitRequirement.idReqTyp === 101 ||
                    this.data.salesUnitRequirement.idReqTyp === 103  ||
                    this.data.salesUnitRequirement.idReqTyp === 104  ||
                    this.data.salesUnitRequirement.idReqTyp === 105 ) {
                    this.getOrganizationCustomer(this.data.salesUnitRequirement.customerId);
                } else if (this.data.salesUnitRequirement.idReqTyp === 102 ||
                    this.data.salesUnitRequirement.idReqTyp === null ) {
                    this.getPersonalCustomer(this.data.salesUnitRequirement.customerId);
                }
            }
        }
    }

    private onSuccess(data, headers) {
        // keluar banyak order item nya, sementara dibuat filter di FE
        const ordItem: OrderItem = _.find(data, ['ordersId', this.data.idOrder]);
        this.orderItem = ordItem;
        /*
            OTR = dimaster motor(Regular Price)
            BBN = dimaster motor(BBN)
            OFF THE ROAD = OTR - BBN
            SALES AMOUNT =  OFF THE ROAD / 1,1
            DISCOUNT = total discount (subsidi MD, subsidi AHM, Diskon Dealer)
            PRICE = Sales Amount - discount
            PPN = Price * 10%
            AR = BBN + Price + PPN

        */
        console.log('hasil ==> idorder=' , this.orderItem);

        // this.offTheRoadRp = ((this.otrRp - this.bbn) * 10) / 11;
        // this.offTheRoadRp = this.offTheRoadRp - this.getTotalDisc();
        // this.offTheRoadRp = this.offTheRoadRp / 10;
        this.hitung_komponent_harga();
        const totalDisc = this.getTotalDisc();
        this.totalDisc = totalDisc;
        // this.getSisaDibayarkan();
    }

    loadSur() {

        this.receiptService.queryFilterBy({
            idRequirement: this.data.salesUnitRequirement.idRequirement,
            isSales: 'true',
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => {
                console.log('load sur', res)
                this.receipts = res.json.Receipt;
                this.hitung();
                this.getTenor();
            },
            (res: ResponseWrapper) => {
            }
        );
    }

    loadSur1() {
        this.receiptService.queryFilterBy({
            query: this.data.salesUnitRequirement.idRequirement,
            isSales: 'true',
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => {
                this.receipts = res.json.Receipt;
                this.hitung();
            },
            (res: ResponseWrapper) => {
            }
        );
    }

    getTenor() {
        if ( this.data.salesUnitRequirement === null ) {
            return null;
        }
        console.log('Sur get tenor ==> ', this.data.salesUnitRequirement)
        if (this.data.salesUnitRequirement.leasingTenorProvideId !== undefined ) {
            this.leasingTenorProvideService.find(this.data.salesUnitRequirement.leasingTenorProvideId)
                .subscribe(
                    (  res: LeasingTenorProvide ) => {
                        // console.log(LeasingTenorProvide);
                        console.log('get tenor', res)
                        this.datas.tenor = res.tenor;
                        this.datas.angsuran = res.installment;
                        this.datas.dpmurni = res.downPayment ;
                    }
            );
        }
    }

    cekTanggalPengiriman(): void {
        console.log('cek tgl pengiriman')
        this.shipmentService.query({
            query: this.data.idOrder,
            page: 0,
            size: 1}).subscribe(
            (res: ResponseWrapper) => {
                console.log('Shipment = ', res.json);
                if ( res.json.length > 0  ) {
                   this.dateSchedulle = res.json[0].dateSchedulle;
                }
            },
            (res: ResponseWrapper) => {
            }
        );
    }

    hitung() {

        const subsidi = this.salesUnitRequirementService.fnCountTotalSubsidi(this.data.salesUnitRequirement);
        if (this.receipts !== null || this.receipts !== undefined  ) {
            // for (let id in this.receipts) {
            //     this.datas.dpmurni += this.receipts[id].amount;
            // }
            console.log('Receipts[] ==>', this.receipts);
            for (const rec in this.receipts) {
                // this.datas.dpmurni += this.receipts[id].amount;
                if ( this.receipts[rec] === rec ) {
                    this.datas.dpmurni += this.receipts[rec].amount;
                    console.log(rec);
                }
            }

            // this.receipts.forEach( rec: Receipt => {
            //     console.log(rec);
            // });

            this.datas.sisa = this.data.salesUnitRequirement.unitPrice - this.datas.dpmurni - subsidi;
            if ( this.receipts !== undefined) {
                console.log('Receipts len', this.receipts.length);
                this.datas.tanggalPembayaran = this.receipts[0].dateCreate;
                this.datas.tandajadi = this.receipts[0].amount;
            }

        } else {
            this.datas.sisa = this.data.salesUnitRequirement.unitPrice;
            this.datas.tanggalPembayaran = this.data.salesUnitRequirement.dateCreate;
        };
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        return result;
    }

    updateVerifikasi(): void {
        console.log('VSO -> update verifikasi  ==>', this.data );
        // this.data.vriscod = this.selectedIsCod;
        this.vehicleSalesOrderService.executeProcess(this.VSO_VERIFIKASI , null , this.data).subscribe(
            (res) => {
                this.data = this.convertDateStringToDate(res);
                this.toaster.showToaster('info', 'Save', 'Verifikasi saved !');
            }
        )
    }

    private convertDateStringToDate(data: VehicleSalesOrder): VehicleSalesOrder {
        if (data.vrtglpembayaran !== null) {
            data.vrtglpembayaran = new Date(data.vrtglpembayaran);
        }

        if (data.vrtglpengiriman !== null) {
            data.vrtglpengiriman = new Date(data.vrtglpengiriman);
        }

        return data;
    }

    trackGoodById(index: number, item: Good) {
        return item.idProduct;
    }

    cekCodPayment(isSelect?: Boolean) {
        if (isSelect) {
            this.data.vriscod = this.data.vriscod;
        }
    }

    private hitung_komponent_harga() {
        if (this.data.salesUnitRequirement.idRequirement) {
            this.receiptService.queryFilterBy({
                idRequirement: this.data.salesUnitRequirement.idRequirement,
                isSales: 'true',
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => {
                        this.receipts = res.json;
                        this.totalItems = res.headers.get('X-Total-Count');
                        console.log('receipt ', res.json)
                        this.receipts.forEach(
                            (receipt) => {
                                this.datePayment = receipt.dateCreate;
                                this.totalPayment += receipt.amount;
                            console.log('total pembayaran ', this.totalPayment);
                            if (receipt.paymentTypeId === 50 ) {
                                // hitung total tanda jadi
                                this.totalTandaJadi += receipt.amount;
                            }
                            // else {
                            //     // hitung total DP
                            //     if (receipt.paymentTypeId === 51 ) {
                            //         this.totalDp += receipt.amount;
                            //     }
                            // };
                            // if (! (receipt.refferenceNumber === null || receipt.refferenceNumber === '') ) {
                            //     // hitung total tanda jadi
                            //     this.totalKasirRp += receipt.amount;
                            // } else {
                            //     // hitung total kasir
                            //     this.totalSalesRp += receipt.amount;
                            // }
                        });
                    } ,
                    (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    private onError(error) {
        // this.alertService.error(error.message, null, null);
    }

    public getSisaDibayarkan() {
        // const totalDisc = this.getTotalDisc();
        this.sisaDibayar = this.data.salesUnitRequirement.minPayment - (this.totalPayment + this.totalDisc);
        console.log('min payment', this.data.salesUnitRequirement.minPayment);
        console.log('tottal payment', this.totalPayment);
        console.log('total discount', this.totalDisc);
    }

     public getTotalDisc() {
        if ( !(this.orderItem === null || this.orderItem === undefined )) {
            const discMd: number = isNaN(this.orderItem.discountMD) ? 0 : Number(this.orderItem.discountMD);
            const discATPM: number = isNaN(this.orderItem.discountatpm) ? 0 : Number(this.orderItem.discountatpm);
            const discFinco: number = isNaN(this.orderItem.discountFinCoy) ? 0 : Number(this.orderItem.discountFinCoy);
            const discDealer: number = isNaN(this.orderItem.discount) ? 0 : Number(this.orderItem.discount);
            return ( discMd + discATPM + discFinco +  discDealer );
        } else {
            return 0;
        }
    }

    getPersonalCustomer(id: string): void {
        this.personalCustomerService.find(id).subscribe(
            (res) => {
                if (res.person.dob !== null) {
                    res.person.dob = new Date(res.person.dob);
                }
                console.log('aaaa', res);
                this.personalCustomer = res;
            }
        )
    }
    getOrganizationCustomer(id: string): void {
        this.organizationCustomerService.find(id).subscribe(
            (res) => {
                this.organizationCustomer = res;
                console.log('getOrganizationCustomer', res);
            }
        )
    }
}
