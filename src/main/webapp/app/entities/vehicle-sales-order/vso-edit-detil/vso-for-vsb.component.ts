import { Component, OnInit, OnDestroy, OnChanges, EventEmitter, Output, Input, SimpleChanges} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import { VehicleSalesOrder } from '../../vehicle-sales-order/vehicle-sales-order.model';
import { VehicleSalesOrderService } from '../../vehicle-sales-order/vehicle-sales-order.service';
import { OrdersParameter } from '../../orders/orders-parameter.model'
import { LoadingService } from '../../../layouts/index';

@Component({
    selector: 'jhi-vso-for-vsb',
    templateUrl: './vso-for-vsb.component.html'
})

export class VsoForVsbComponent implements OnInit, OnDestroy, OnChanges {

    @Output()
    fnCallback = new EventEmitter();
    @Input()
    public status: any;

    @Input()
    vsoParameter: OrdersParameter = new OrdersParameter();

    proccessIdGenerateVSB = 17;

    currentAccount: any;
    vehicleSalesOrders: VehicleSalesOrder[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any= 1;
    predicate: any= 'idOrder';
    previousPage: any;
    reverse: any= false;
    tgl1: Date;
    tgl2: Date;
    selectedStatusVso: string;

    constructor(
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            // console.log('disuatu hari', data);
            // this.page = data['pagingParams'].page;
            // this.previousPage = data['pagingParams'].page;
            // this.reverse = data['pagingParams'].ascending;
            // this.predicate = data['pagingParams'].predicate;

        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.vehicleSalesOrderService.search({
                idInternal: this.principal.getIdInternal(),
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => this.loadingService.loadingStop(),
                );
            return;
        }
        this.vehicleSalesOrderService.query({
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json),
                () => this.loadingService.loadingStop(),
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/vehicle-sales-order'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['vsoParameter']) {
            console.log('VSO PARAMETER', this.vsoParameter);
        }
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/vehicle-sales-order', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/vehicle-sales-order', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInVehicleSalesOrders();

    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VehicleSalesOrder) {
        return item.idOrder;
    }

    trackStatusVsoById(index: number, item: any) {
        return item.value;
    }

    registerChangeInVehicleSalesOrders() {
        this.eventSubscriber = this.eventManager.subscribe('vehicleSalesOrderListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idOrder') {
            result.push('orderNumber');
            result.push('idOrder');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.vehicleSalesOrders = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.vehicleSalesOrderService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleSalesOrderService.update(event.data)
                .subscribe((res: VehicleSalesOrder) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleSalesOrderService.create(event.data)
                .subscribe((res: VehicleSalesOrder) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: VehicleSalesOrder) {
        this.toasterService.showToaster('info', 'VehicleSalesOrder Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    cancelApprovekacab(vso) {

        this.confirmationService.confirm({
            message: 'Are you sure that you want to cancel kacab approval ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleSalesOrderService
                .executeProcess(85, null, vso)
                .subscribe((res) => {
                    console.log('VSO Complete, vsb generated !');
                    this.loadAll();
                    this.toasterService.showToaster('info', 'approved kacab canceled', 'Success..');
                },
                (err ) => {
                    this.toasterService.showToaster('error', 'Error ', err);
                })
            },
            reject: () => {
                this.toasterService.showToaster('info', 'Info ', 'Dialog closed');
            }
        });

        console.log('VSO Cancel Approve kacab.. ', vso);

    }

    generateVsb(idOrder) {
        // this.fnCallback.emit();

        // return ;
        this.loadingService.loadingStart();
        console.log('Generate VSB');
        this.vehicleSalesOrderService.find(idOrder, this.principal.getIdInternal())
            .subscribe((vehicleSalesOrder) => {
                console.log('Execute proces = ', this.proccessIdGenerateVSB );
                this.vehicleSalesOrderService
                    .executeProcess(this.proccessIdGenerateVSB, null, vehicleSalesOrder)
                    .delay(1000)
                    .subscribe((res) => {
                        console.log('VSO Complete, vsb generated !');
                        this.loadAll();
                        this.toasterService.showToaster('info', 'billing created', 'Success..');
                    },
                    (err ) => {
                        this.loadingService.loadingStop();
                        this.toasterService.showToaster('error', 'Error ', err);
                    },
                    ( ) => this.loadingService.loadingStop(),
                )
        });

    }

    getDescription(currentStatus: number) {
        return this.vehicleSalesOrderService.getDescriptionStatus(currentStatus);
    }

}
