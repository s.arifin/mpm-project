import { Component, OnInit, Input, OnDestroy , OnChanges, SimpleChanges, Output, EventEmitter} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleSalesOrder } from '../vehicle-sales-order.model';
import { VehicleSalesOrderService } from '../vehicle-sales-order.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { SelectItem} from 'primeng/primeng';

import { InventoryItemService } from '../../inventory-item/inventory-item.service';
import { InventoryItem } from '../../inventory-item/inventory-item.model';
import { SalesUnitRequirement, SalesUnitRequirementService } from '../../sales-unit-requirement';
import { OrderItem, OrderItemService } from '../../order-item';
import { ReceiptService, Receipt } from '../../receipt';
import { Vendor, VendorService } from '../../vendor';

import { SaleType, SaleTypeService, SaleTypeComponent } from '../../sale-type';

import * as _ from 'lodash';
import { PriceComponentService, PriceComponent } from '../../price-component';
import { LoadingService } from '../../../layouts';
import * as SUR_CONSTAN from '../../../shared/constants/sales-unit-requirement.constants';

@Component({
  selector: 'jhi-vso-edit-data-harga-pembayaran',
  templateUrl: './vso-edit-data-harga-pembayaran.component.html',
})
export class VsoEditDataHargaPembayaranComponent implements OnInit, OnDestroy, OnChanges {
    @Input() data: SalesUnitRequirement = new SalesUnitRequirement();
    @Input() statusVso: number;
    @Input() vso: VehicleSalesOrder = new VehicleSalesOrder();
    @Input() readOnly: Boolean = false
    @Output() fnDisable = new EventEmitter();
    @Input()
    public status: any;

    receipts: Receipt[];
    datas: any;
    detail: any;
    tableDetail: any;
    invItemSubscriber: Subscription;
    eventSubscriber: Subscription;
    inventoryItem: InventoryItem;
    salesUnitRequirement: SalesUnitRequirement;
    vehicleSalesOrders: VehicleSalesOrder[];
    orderItem: OrderItem;
    alreadyLoaded: Boolean = false;
    totalItems: any;
    itemsPerPage: number;
    previousPage: any;
    links: any;
    reverse: any;
    first: number;
    predicate: any;
    page: any;
    totalPembayran: any;
    diffWith: Boolean = false;
    isComplete: Boolean = false

    bbnkb: any;
    pkb: any;
    swdkllj: any;
    stnk: any;
    tnkb: any;
    parkir: any;
    biaya: any;
    birojasa: any;
    biayaSTNKB: any;

    komisiSales: number;
    totalDp: number;
    totalTandaJadi: number;
    totalKasirRp: number;
    totalSalesRp: number;
    // salesAmount: number;

    otrRp: number;
    totalDiscRp: number;
    offTheRoadRp: number;
    bbn: number;
    refundFinco: number;
    COGS: number;
    sumDiscount: number;
    sumDiscount2: number;

    vendors: Vendor[] = [];
    biroJasaSelected: any;
    tglPoFinco: any;
    poNumber: string;
    dpFinco: any;
    priceComp: PriceComponent[];

    saletypes: SaleType[];
    totalMargin: any;
    countDiscDialog: any;

    // public totalPriceAfterDiscount: number;
    public tax: number;
    public totalOffTheRoadAndTax: number;
    public totalDisc: number;
    public totalDiscExPPN: number;
    public totalPayment: number;
    public isCheck: Boolean = false;

    constructor(
        private inventoryItemService: InventoryItemService,
        private salesUnitRequirementService: SalesUnitRequirementService,
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private orderItemService: OrderItemService,
        private receiptService: ReceiptService,
        private router: Router,
        private vendorService: VendorService,
        private toaster: ToasterService,
        private saleTypeService: SaleTypeService,
        private commontUtilService: CommonUtilService,
        private principal: Principal,
        private priceComponentService: PriceComponentService,
        private loadingService: LoadingService,
    ) {
        this.datas = {
            otr: 0,
        }
        this.totalPayment = 0;
        this.itemsPerPage = 1000;
        this.page = 1;
        this.predicate = 'idPayment';
        this.reverse = 'asc';
        this.first = 0;

        this.totalDp = 0;
        this.totalTandaJadi = 0;
        this.totalKasirRp = 0;
        this.totalSalesRp = 0;
        this.otrRp = 0;
        this.totalDiscRp = 0;
        this.offTheRoadRp = 0;
        this.bbn = 0;
        this.refundFinco = 0;
        this.komisiSales = 0;
        this.sumDiscount = 0;
        // this.salesAmount = 0;
        // this.totalPriceAfterDiscount = 0;
        this.tax = 0;
        this.totalOffTheRoadAndTax = 0;
        this.COGS = 0;
        this.totalPembayran = 0;
        this.orderItem = new OrderItem();

        this.bbnkb = 0;
        this.pkb = 0;
        this.swdkllj = 0;
        this.stnk = 0;
        this.tnkb = 0;
        this.parkir = 0;
        this.biaya = 0;
        this.birojasa = 0;
        this.biayaSTNKB = 0;
        this.totalMargin = 0;
        this.sumDiscount = 0;
        this.sumDiscount2 = 0;
        this.countDiscDialog = null;
        this.totalDiscExPPN = 0;
    }

    goDetail() {
        this.detail = 1;
    }

    goTableDetail() {
        this.detail = 2
    }

    ngOnInit() {
        this.sumMargin();
        this.saleTypeService.query()
        .subscribe((res: ResponseWrapper) => {
            this.saletypes = this.commontUtilService.getDataByParent(res.json, 1);
            },
            (res: ResponseWrapper) => this.onError(res.json));

    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['vso']) {
            if (this.status === 17 || this.status === 13 ) {
                this.isComplete = true;
            }

            if ((this.data.saleTypeId === 20 || this.data.saleTypeId === 50 || this.data.saleTypeId === 51)) {
                if (this.data.komisiSales != null && isNaN(this.data.komisiSales) !== true) {
                    this.fnDisable.emit(false);
                    // this.readOnly = true;
                } else {
                    // this.readOnly = false;
                    this.fnDisable.emit(true);
                }
        } else if ((this.data.saleTypeId === 21 || this.data.saleTypeId === 52 || this.data.saleTypeId === 53)) {
            if ((this.vso.refundFinco != null && this.data.komisiSales != null) && (isNaN(this.vso.refundFinco) !== true && isNaN(this.data.komisiSales) !== true)) {
                this.fnDisable.emit(false);
                // this.readOnly = true;
                console.log('refund = ', isNaN(this.vso.refundFinco));
                console.log('komisi sales = ', isNaN(this.data.komisiSales));
            } else {
                // this.readOnly = false;
                this.fnDisable.emit(true);
            }
        }

        if ( this.vso.idOrder === undefined ) {
            return ;
        }
        console.log('Masuk edit data harga pembayaran, cari order item ==> idorder=' , this.vso.idOrder);
        this.orderItemService.queryFilterBy({
                idOrders: this.vso.idOrder,
                size: 100,
                page: 0,
                sort: ['idOrderItem', 'asc']
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json),
                ( ) => { }
            );
            // this.loadAllBiroJasa();
            this.loadVendor();
            // this.loadSTNKB();
            // this.sumMargin();
        }

        this.tglPoFinco = new Date(this.data.podate);
        this.poNumber = this.data.poNumber ;

    }

    private onSuccess(data, headers) {
        // keluar banyak order item nya, sementara dibuat filter di FE
        const ordItem: OrderItem = _.find(data, ['ordersId', this.vso.idOrder]);
        this.orderItem = ordItem;
        console.log('hasil ==> idorder=' , this.orderItem);
        this.formulaCountPrice(this.orderItem);
        this.hitung_komponent_harga();
        this.loadSTNKB();
        this.alreadyLoaded = true;
    }

    // rumus nya di sini
    private formulaCountPrice(orderItem: OrderItem): void {
        if (orderItem.taxAmount === 0) {
            this.otrRp = Math.round((orderItem.unitPrice) + orderItem.bbn);
        } else {
            this.otrRp = Math.round((orderItem.unitPrice * 1.1) + orderItem.bbn);
        }
        this.bbn = orderItem.bbn;
        this.offTheRoadRp = orderItem.unitPrice;

        const totalDisc = this.getTotalDisc();
        this.totalDisc = totalDisc;
        if (orderItem.taxAmount === 0 ) {
            this.totalDiscExPPN = totalDisc;
        } else {
            this.totalDiscExPPN = Math.round(totalDisc / 1.1);
        }
        this.tax = orderItem.taxAmount;
        this.totalOffTheRoadAndTax = Math.round(this.offTheRoadRp + this.tax);
        this.COGS = orderItem.cogs;

        if (this.vso.subFincoyAsDp === true) {
            console.log('hayooooo == ', this.vso.subFincoyAsDp);
            this.vso.subFincoyAsDp = 1;
            console.log('0/1 == ', this.vso.subFincoyAsDp);
            // this.data.creditDownPayment = this.data.creditDownPayment + this.orderItem.discountFinCoy;
            this.totalDisc = this.totalDisc - this.orderItem.discountFinCoy;
            }
    }

    public getTotalDisc() {
        if ( !(this.orderItem === null || this.orderItem === undefined )) {
            const discMd: number = isNaN(this.orderItem.discountMD) ? 0 : Number(this.orderItem.discountMD);
            const discATPM: number = isNaN(this.orderItem.discountatpm) ? 0 : Number(this.orderItem.discountatpm);
            const discFinco: number = isNaN(this.orderItem.discountFinCoy) ? 0 : Number(this.orderItem.discountFinCoy);
            const discDealer: number = isNaN(this.orderItem.discount) ? 0 : Number(this.orderItem.discount);
            return  ( discMd + discATPM + discFinco +  discDealer );
        } else {
            return 0;
        }
    }

    // uang di pegang sales => di payment ada reffnum
    // uand di kasir tidak ada reff num
    // payment type
    //   50 - tanda jadi
    //   51 - DP
    private hitung_komponent_harga() {
        if (this.data.idRequirement) {
            this.receiptService.queryFilterBy({
                idRequirement: this.data.idRequirement,
                isSales: 'true',
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => {
                        this.receipts = res.json;
                        this.totalItems = res.headers.get('X-Total-Count');
                        console.log('receipt ', res.json)

                        this.receipts.forEach(
                            (receipt) => {
                                this.totalPayment += receipt.amount;
                            // console.log('isi ', receipt);
                            if (receipt.paymentTypeId === 50 ) {
                                // hitung total tanda jadi
                                this.totalTandaJadi += receipt.amount;
                            } else {
                                // hitung total DP
                                if (receipt.paymentTypeId === 51 ) {
                                    this.totalDp += receipt.amount;
                                }
                            };
                            if (! (receipt.refferenceNumber === null || receipt.refferenceNumber === '') ) {
                                // hitung total tanda jadi
                                this.totalSalesRp += receipt.amount;
                            } else {
                                // hitung total kasir
                                this.totalKasirRp += receipt.amount;
                            }
                        });
                        this.totalPembayran = this.totalTandaJadi + this.data.brokerFee + this.totalDp;
                    } ,
                    (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.hitung_komponent_harga();
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.hitung_komponent_harga();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/receipt', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.hitung_komponent_harga();
    }

    trackId(index: number, item: Receipt) {
        return item.idPayment;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idPayment') {
            result.push('idPayment');
        }
        return result;
    }

    private onError(error) {
        // this.alertService.error(error.message, null, null);
    }
    ngOnDestroy() {
        // this.invItemSubscriber.unsubscribe();
    }

    registerChangeInCategories() {
        // this.eventSubscriber = this.eventManager.subscribe('categoryListModification', (response) => this.loadAll());
    }

    fnCallback(event): void {
        console.log('evet ', event);
    }

    private ifNumberNull(value?: number): number {
        if (value) {
            return value;
        }else {
            return 0;
        }
    }

    updateComponentHarga(): void {
        this.sumDiscount = (this.orderItem.discountFinCoy + this.orderItem.discountMD + this.orderItem.discountatpm + this.orderItem.discount)
        this.sumDiscount2 = ( this.orderItem.discountMD + this.orderItem.discountatpm + this.orderItem.discount);
        if (this.orderItem.discountFinCoy == null) {
            this.orderItem.discountFinCoy = 0;
        }
        if (this.orderItem.discount == null) {
            this.orderItem.discount = 0;
        }
        if ( (this.vso.subFincoyAsDp === 0 || this.vso.subFincoyAsDp === false || this.vso.subFincoyAsDp == null) && ( this.sumDiscount !== this.totalDisc) ) {
                this.countDiscDialog = this.sumDiscount;
                this.diffWith = true;
        } else
        if ((this.vso.subFincoyAsDp === 1 || this.vso.subFincoyAsDp === true) && ( this.sumDiscount2 !== this.totalDisc) ) {
                this.countDiscDialog = this.sumDiscount2;
                this.diffWith = true;
        } else {
        let saveSur: Boolean  = false;
        if ((this.data.saleTypeId === 20 || this.data.saleTypeId === 22 || this.data.saleTypeId === 50 || this.data.saleTypeId === 51 || this.data.saleTypeId === 54 || this.data.saleTypeId === 55)) {
            if (this.data.komisiSales != null && isNaN(this.data.komisiSales) !== true) {
                this.fnDisable.emit(false);
                // this.readOnly = true;
            } else {
                // this.readOnly = false;
                this.fnDisable.emit(true);
            }
        } else if ((this.data.saleTypeId === 21 || this.data.saleTypeId === 52 || this.data.saleTypeId === 53)) {
        if ((this.vso.refundFinco != null && this.data.komisiSales != null) && (isNaN(this.vso.refundFinco) !== true && isNaN(this.data.komisiSales) !== true)) {
            this.fnDisable.emit(false);
            // this.readOnly = true;
            console.log('refund = ', isNaN(this.vso.refundFinco));
            console.log('komisi sales = ', isNaN(this.data.komisiSales));
        } else {
            // this.readOnly = false;
            this.fnDisable.emit(true);
        }
        }

        if (this.tglPoFinco !== null ) {
            saveSur = true;
            this.data.podate = new Date(this.tglPoFinco);
            // .toISOString().slice(0, 10);
        }
        if (this.poNumber !== null ) {
            saveSur = true;
            this.data.poNumber = this.poNumber;
        }
        if (saveSur === true ) {
            // this.loadingService.loadingStart();
            // this.data.subsown = this.orderItem.discount;
            // this.data.subsfincomp = this.orderItem.discountFinCoy;
            this.salesUnitRequirementService
                .executeProcess(SUR_CONSTAN.PROSES_UPDATE_HARGA_PEMBAYARAN, null,  this.data)
                .subscribe(
                    (res) => {
                        this.loadingService.loadingStart();

                        if (this.vso.subFincoyAsDp === false) {
                            this.vso.subFincoyAsDp = 0;
                            console.log('0/1 == ', this.vso.subFincoyAsDp);
                        }
                        if (this.vso.subFincoyAsDp === true) {
                            this.vso.subFincoyAsDp = 1;
                            console.log('0/1 == ', this.vso.subFincoyAsDp);
                        }

                        this.vehicleSalesOrderService.executeProcess(104, null, this.vso)
                        .subscribe(
                            (resVSO) => {
                                        this.loadingService.loadingStart();
                                        console.log('hahaha === ', resVSO);
                                        this.toaster.showToaster('info', 'Save', 'data save' );
                                        },
                                        () => {this.loadingService.loadingStop()},
                                        () => {this.loadingService.loadingStop()}
                                )
                        this.orderItemService.executeProcessData(1000, null, this.orderItem)
                            .subscribe(
                                (resOI) => {
                                    this.loadingService.loadingStart();
                                    this.toaster.showToaster('info', 'Save', 'data save' );
                                },
                                () => {this.loadingService.loadingStop()},
                                () => {this.loadingService.loadingStop()}
                            )
                            this.loadingService.loadingStop();
                            },
                    (err) => {
                        this.toaster.showToaster('info', 'Save', err );
                    },
                );
                this.loadingService.loadingStop();
        }
        console.log('Save sur ', this.data);
    }
    }

    public trackSaleTypeById(index: number, item: SaleType) {
        return item.idSaleType;
    }

    onChangeSubsidi() {
        if (this.vso.subFincoyAsDp === true) {
        console.log('hayooooo == ', this.vso.subFincoyAsDp);
        this.vso.subFincoyAsDp = 1;
        console.log('0/1 == ', this.vso.subFincoyAsDp);
        // this.data.creditDownPayment = this.data.creditDownPayment + this.orderItem.discountFinCoy;
        this.totalDisc = this.totalDisc - this.orderItem.discountFinCoy;
        }
        if (this.vso.subFincoyAsDp === false) {
        console.log('hayooooo == ', this.vso.subFincoyAsDp);
        this.vso.subFincoyAsDp = 0;
        console.log('0/1 == ', this.vso.subFincoyAsDp);
        // this.data.creditDownPayment = this.data.creditDownPayment - this.orderItem.discountFinCoy;
        this.totalDisc = this.totalDisc + this.orderItem.discountFinCoy;
        }
    }

    private loadVendor(): Promise<Vendor[]> {
        return new Promise<Vendor[]>(
            (resolve) => {
                this.vendorService.queryFilterByBiroJasa({
                    idInternal: this.principal.getIdInternal(),
                    sort: ['idVendor', 'asc'],
                    size : 3000}).subscribe(
                    (res: ResponseWrapper) => {
                        // this.convertVendorForSelect(res.json);
                        this.vendors = res.json;
                        console.log('vendor nih', this.vendors)
                    },
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => {
                });
                resolve();
            }
        );
    }

    public loadSTNKB() {
        console.log('idProduct = ', this.orderItem.idProduct);
        console.log('idVendor = ', this.vso.idBiroJasa);

        this.priceComponentService.getAllStnkb(
            {
                idproduct: this.orderItem.idProduct,
                idvendor: this.vso.idBiroJasa,
                size: 200
            }
        )
        .subscribe(
            (res) => {
                this.priceComp = res.json;
                console.log('coba STNKB == ', res.json);
                // const stnkb = this.priceComp[0];

                this.priceComp.forEach(
                    (f) => {
                    if (f.idPriceType === 210) {
                        this.bbnkb = f.price;
                        console.log('BBNKB', this.bbnkb)
                    }
                    if (f.idPriceType === 211) {
                        this.pkb = f.price;
                        console.log('PKB', this.pkb)
                    }
                    if (f.idPriceType === 212) {
                        this.swdkllj = f.price;
                        console.log('SWDKLLJ', this.swdkllj)
                    }
                    if (f.idPriceType === 213) {
                        this.stnk = f.price;
                        console.log('STNK', this.stnk)
                    }
                    if (f.idPriceType === 214) {
                        this.tnkb = f.price;
                        console.log('TNKB', this.tnkb)
                    }
                    if (f.idPriceType === 215) {
                        this.parkir = f.price;
                        console.log('PARKIR', this.parkir)
                    }
                    if (f.idPriceType === 216) {
                        this.biaya = f.price;
                        console.log('stnkb', this.biaya)
                    }
                    if (f.idPriceType === 217) {
                        this.birojasa = f.price;
                        console.log('BIROJASA', this.birojasa)
                    }
                    this.biayaSTNKB = this.bbnkb + this.pkb + this.swdkllj +
                                        this.stnk + this.tnkb + this.parkir +
                                        this.biaya + this.birojasa;
                    }
                )

                if (this.priceComp.length === 0) {
                    this.biayaSTNKB = 0;
                }
                this.sumMargin();
            }
        )

    }
    sumMargin() {
        if ((this.data.saleTypeId === 20 || this.data.saleTypeId === 50 || this.data.saleTypeId === 51)) {
                if (this.data.komisiSales != null && isNaN(this.data.komisiSales) !== true) {
                    // this.fnDisable.emit(false);
                    this.isCheck = false;
                } else {
                    this.isCheck = true;
                    // this.fnDisable.emit(true);
                }
        } else if ((this.data.saleTypeId === 21 || this.data.saleTypeId === 52 || this.data.saleTypeId === 53)) {
            if ((this.vso.refundFinco != null && this.data.komisiSales != null) && (isNaN(this.vso.refundFinco) !== true && isNaN(this.data.komisiSales) !== true)) {
                // this.fnDisable.emit(false);
                this.isCheck = false;
                console.log('refund = ', isNaN(this.vso.refundFinco));
                console.log('komisi sales = ', isNaN(this.data.komisiSales));
            } else {
                this.isCheck = true;
                // this.fnDisable.emit(true);
            }
        }

        const price = ((this.offTheRoadRp) - (this.totalDisc / 1.1));
        console.log('offtheroad = ', this.offTheRoadRp);
        console.log('price = ', price);
        console.log('total discount', (this.totalDisc / 1.1));
        const marginBbn = (this.orderItem.bbn - this.biayaSTNKB) / 1.1;
        console.log('margin bbn = ', marginBbn);
        this.refundFinco = this.vso.refundFinco / 1.1
        console.log('total refund finco = ', this.refundFinco);
        console.log('total Komisi Sales = ', this.data.komisiSales);
        this.totalMargin = Math.round((price - this.COGS + marginBbn + this.refundFinco - this.data.komisiSales - this.data.brokerFee));

    }

    closeDialog() {
        this.diffWith = false;
    }
}
