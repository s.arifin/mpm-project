import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleSalesOrder } from '../vehicle-sales-order.model';
import { VehicleSalesOrderService } from '../vehicle-sales-order.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-vso-edit-data-note',
    templateUrl: './vso-edit-data-note.component.html'
})
export class VsoEditDataNoteComponent  {

    @Input() data: any ;
    @Input()
    public status: any;
    constructor(
    ) {
    }

}
