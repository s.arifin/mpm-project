import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleSalesOrder } from '../vehicle-sales-order.model';
import { VehicleSalesOrderService } from '../vehicle-sales-order.service';
import { InventoryItemService } from '../../inventory-item/inventory-item.service';
import { InventoryItem } from '../../inventory-item/inventory-item.model';
import { SalesUnitRequirement, SalesUnitRequirementService } from '../../sales-unit-requirement';
import { Feature } from '../../feature';
import { Motor, MotorService } from '../../motor';
import * as _ from 'lodash';
import { Product } from '../../product';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../../layouts';
import { FacilityService } from '../../facility';
import { Facility } from '../../shared-component/entity/facility.model';
import { Principal, ToasterService } from '../../../shared';

@Component({
    selector: 'jhi-vso-edit-dataa-kendaraan',
    templateUrl: './vso-edit-data-kendaraan.component.html'
})

export class VsoEditDataKendaraanComponent implements OnChanges, OnInit, OnDestroy {
    @Input() data: SalesUnitRequirement = new SalesUnitRequirement();
    @Input() statusVso: number;
    @Input() vso: VehicleSalesOrder = new VehicleSalesOrder();
    @Input() isMatchingMenu: boolean;
    @Input() isEdit: Boolean = false;
    @Input()
    public status: any;

    private subscription: Subscription;
    invItemSubscriber: Subscription;
    eventSubscriber: Subscription;
    inventoryItem: InventoryItem;
    salesUnitRequirement: SalesUnitRequirement;
    features: Feature[];
    motors: Motor[]
    product: Product;
    selectedProduct: Motor;
    idVSO: any;
    isComplete: Boolean = false
    facilities: Facility[] = new Array<Facility>();
    idGudang: any = null;
    constructor(
        private inventoryItemService: InventoryItemService,
        private salesUnitRequirementService: SalesUnitRequirementService,
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private route: ActivatedRoute,
        private motorService: MotorService,
        private confirmationService: ConfirmationService,
        private router: Router,
        private loadingService: LoadingService,
        private facilityservice: FacilityService,
        private principal: Principal,
        private toaster: ToasterService,
    ) {
        this.features = new Array<Feature>();
        this.motors = new Array<Motor>();
        this.product = new Product();
        this.selectedProduct = new Motor();

    }

    refreshData() {
        this.vehicleSalesOrderService.find(this.vso.idOrder, null).subscribe(
            (res: VehicleSalesOrder) => {
                if (res.currentStatus === 74) {
                    this.salesUnitRequirementService
                    .find(this.data.idRequirement).subscribe((sur) => {
                        console.log('sur refresh lagi     ==>', sur);
                        this.data.requestIdFrame = sur.requestIdFrame;
                        this.data.requestIdMachine = sur.requestIdMachine;
                });
                } else if (res.currentStatus === 61) {
                    this.autoMatch(res);
                }
            }
        )

    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['data']) {
            if (this.status === 17 || this.status === 13 || this.status === 88 ) {
                this.isComplete = true;
            }
            if (this.data.idRequirement !== undefined) {
                console.log('isi data kiriman ke edit kendaraan ==>', this.data);
                this.getFacility();
            }
            // this.registerGetChooseInventory();
        }
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['idreq']) {
                if (params['isedit'] === 'true') {
                    this.isEdit = true;
                    console.log('is edit di kendaraan', this.isEdit);
                }
                this.idVSO = params['idvso'];
                this.salesUnitRequirementService.find(params['idreq'])
                .subscribe(
                    (ressur) => {
                        this.data = ressur;
                        console.log('sur edit vso = ', ressur.productId);
                        console.log('sur edit vso isedit= ', this.isEdit);
                        this.getColorByMotor(ressur.productId)
                    }
                )
            }
        });
        this.registerGetChooseInventory();
       //  this.inventoryItemService.values();
    }

    ngOnDestroy() {
        this.invItemSubscriber.unsubscribe();
    }

    registerChangeInCategories() {
        // this.eventSubscriber = this.eventManager.subscribe('categoryListModification', (response) => this.loadAll());
    }

    registerGetChooseInventory() {
        this.invItemSubscriber = this.inventoryItemService.values.subscribe(
            (response) => {
                console.log('response', response);
                this.inventoryItem = response;
                console.log('inv==>', this.inventoryItem);
                if (this.inventoryItem !== undefined) {
                // SalesUnitRequirement sur = this.salesUnitRequirementService.find
                    this.salesUnitRequirementService.find(this.data.idRequirement).subscribe((sur) => {
                        this.salesUnitRequirement = sur;
                        console.log('sur hasil dulu sur nya sblm update  ==>', sur);
                        sur.requestIdFrame = this.inventoryItem.idFrame;
                        sur.requestIdMachine = this.inventoryItem.idMachine;

                        this.salesUnitRequirementService.update(sur).subscribe(
                            ( sur1 ) => {
                                this.data = sur1,
                                console.log('success save isi kembali = ', sur1);

                                const kodeAutoMatching = 740;
                                console.log('kode manual matching : ', kodeAutoMatching)
                                this.vehicleSalesOrderService
                                    .executeProcess(kodeAutoMatching, null, this.vso)
                                    .subscribe((res) => {
                                        console.log('Automatching');
                                        this.data.requestIdFrame = sur.requestIdFrame;
                                        this.data.requestIdMachine = sur.requestIdMachine;
                                    })
                            }
                        )
                    });
                }

        });
    }

    fnCallback(event): void {
        console.log('evet ', event);
    }

    public trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    private getColorByMotor(idProduct: string): void {
        if (idProduct !== null) {
            this.selectedProduct = new Motor();
            this.features = new Array<Feature>();
            // this.selectedProduct = _.find(this.motors,
            //     function(e) { return e.idProduct.toLowerCase() === idProduct.toLowerCase();
            // });
            this.motorService.find(idProduct)
            .subscribe(
                (resMotor) => {
                    this.features = resMotor.features
                }
            )
            // this.features = this.selectedProduct.features;
        }
    }

    previousPage() {
        this.router.navigate(['vehicle-sales-order-sales/' + this.idVSO])
    }
    saveColor() {
        console.log('Ganti VSo');
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Akan Ganti Warna ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.salesUnitRequirementService.executeProcess(115, null, this.data)
                .subscribe(
                    (res) => {
                        this.loadingService.loadingStop();
                        console.log('gantiiii warna ahh  ', res);
                        this.previousPage();
                    }
                )
                },
            reject: () => {

            }
        });
    }

    // this.inventoryItemService.pushItems(this.selected);

    getFacility() {
        this.facilityservice.queryFilterBy({
            idInternal: this.principal.getIdInternal(),
        }).subscribe(
            (res) => {
                this.facilities = res.json;
                console.log(' this facilities internal == ', this.facilities);
            }
        )
    }

    chooseFacility() {
        this.data.idGudang = this.idGudang;
        this.salesUnitRequirementService.executeProcess(121, null, this.data).subscribe((res) => console.log('update facility == ', this.data));
    }

    autoMatch(vso: VehicleSalesOrder): void {
        // Proses  Automatching
        this.loadingService.loadingStart();
        const kodeAutoMatching = 74;
        this.vehicleSalesOrderService
            .executeProcessPromise(kodeAutoMatching, null, vso)
            .then((result) => {
                console.log('Automatching isi result -->', result, '<--');
                this.toaster.showToaster('info', 'matching', 'Auto Matching success !');
                this.loadingService.loadingStop();
                // this.toaster.showToaster('info', 'Approve', 'Kacab Approve !');
            },
            (errResult) => {
                this.loadingService.loadingStop();
            })
            .catch (( err ) => { this.toaster.showToaster('info', 'matching', 'Error ' && err) })
        // END Proses Automatching
    }

}
