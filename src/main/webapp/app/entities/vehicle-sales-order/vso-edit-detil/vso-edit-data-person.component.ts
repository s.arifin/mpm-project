import { Component, OnDestroy, OnChanges, SimpleChanges, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { CommonUtilService } from '../../../shared';
import {PersonService, Person} from '../../person';
import {PersonalCustomerService} from '../../../entities/personal-customer/personal-customer.service';
import {SalesUnitRequirement, SalesUnitRequirementService} from '../../sales-unit-requirement';
import {LoadingService} from '../../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-vso-edit-data-person',
    templateUrl: './vso-edit-data-person.component.html'
})
export class VsoEditDataPersonComponent implements OnChanges {
    @Input()
    public salesUnitRequirement: SalesUnitRequirement;
    @Input()
    public status: any;

    @Input()
    public display: string;

    public isViewMode: boolean;
    public customer: Person;

    constructor(
        private commonUtilService: CommonUtilService,
        private personService: PersonService,
        private loadingService: LoadingService,
        private personalCustomerService: PersonalCustomerService
    ) {
        this.isViewMode = true;
    }

    ngOnChanges(change: SimpleChanges) {
        if (change['display'] && change['salesUnitRequirement']) {
            if (this.display !== undefined) {
                if (this.display === 'customer') {
                    if (this.salesUnitRequirement.customerId !== undefined) {
                        this.loadCustomer(this.salesUnitRequirement.customerId);
                    }
                }
            }
        }
    }

    public viewEdit(): void {
        if (this.isViewMode) {
            this.isViewMode = false;
        } else {
            this.isViewMode = true;
        }
    }

    private loadCustomer(customerId: string): void {
        this.personalCustomerService.find(customerId).subscribe(
            (res) => {
                this.customer = res.person;
            },
            (err) => {
                this.commonUtilService.showError(err);
            }
        )
    }

    public vsoEditUpdatePerson(person: Person): void {
        this.loadingService.loadingStart();
        this.personService.update(person).subscribe(
            (res: Person) => {
                this.viewEdit();
                this.loadingService.loadingStop();
            },
            (err) => {
                this.loadingService.loadingStop();
                this.commonUtilService.showError(err);
            }
        )
    }
}
