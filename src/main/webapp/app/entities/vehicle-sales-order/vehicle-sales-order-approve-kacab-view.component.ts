import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { VehicleSalesOrder } from './vehicle-sales-order.model';
import { VehicleSalesOrderService } from './vehicle-sales-order.service';
import { ToasterService, Principal } from '../../shared';

@Component({
    selector: 'jhi-vehicle-sales-order-approve-kacab-view',
    templateUrl: './vehicle-sales-order-approve-kacab-view.component.html'
})
export class VehicleSalesOrderApproveKacabViewComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    vehicleSalesOrderId: number;

    vehicleSalesOrder: VehicleSalesOrder;
    isApprove: String = 'Y';
    idRequestType: any;

    constructor(
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private route: ActivatedRoute,
        private router: Router,
        private toaster: ToasterService,
        private principal: Principal,
    ) {
        this.vehicleSalesOrder = new VehicleSalesOrder();
        this.idRequestType = null;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.vehicleSalesOrderId = params['id'];
                this.load(params['id']);
            }
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.vehicleSalesOrderService
            .find(id,  this.principal.getIdInternal())
            .subscribe((res) => {
                this.vehicleSalesOrder = res;
                this.idRequestType = this.vehicleSalesOrder.salesUnitRequirement.idRequest;
                console.log('isi vso', this.vehicleSalesOrder);
            });
    }

    previousState(): void {
        this.router.navigate(['vehicle-sales-order-approve-kacab']);
    }

    approveKacab() {
        // approved
        let kodeApprove = 61;

        console.log('kode approve : ', kodeApprove)

        if (this.isApprove === 'T') {
            // not approved
            kodeApprove = 62;
            console.log('kode approve (not approve): ', kodeApprove)
            this.vehicleSalesOrderService
            .executeProcess(62, null, this.vehicleSalesOrder)
            .subscribe((res) => {
                console.log('approve');
                this.vehicleSalesOrderService.createGL(this.vehicleSalesOrder).subscribe(
                    (res1) => console.log('jurnal terkirim'),
                    (res1) => console.log('jurnal gagal kirim')
                );
            },
            ( err ) =>  { },
            () => {
                this.previousState();
                this.toaster.showToaster('info', 'Approve', 'Kacab Approve !');
            }
        )
        }

        if (this.isApprove === 'Y') {
        this.vehicleSalesOrderService
            .executeProcess(61, null, this.vehicleSalesOrder)
            .subscribe((res) => {
                console.log('approve');

                const kodeAutoMatching = 74;
                console.log('kode auto matching : ', kodeAutoMatching)
                this.vehicleSalesOrderService
                    .executeProcess(kodeAutoMatching, null, this.vehicleSalesOrder)
                    .subscribe((result) => {
                        console.log('Automatching');
                        // this.previousState();
                        // this.toaster.showToaster('info', 'Approve', 'Kacab Approve !');
                    })

            },
            ( err ) =>  { },
            () => {
                this.previousState();
                this.toaster.showToaster('info', 'Approve', 'Kacab Approve !');
            }
        )
        }
    }

    getDescription(statusValue: Number): String {
        let hasil: String;

        switch (statusValue) {
            case 0: hasil = '';
                break;
            case 1: hasil = 'Approve';
                break;
            case 2: hasil = 'Not Approve';
                break;
        }

        return hasil;
    }

}
