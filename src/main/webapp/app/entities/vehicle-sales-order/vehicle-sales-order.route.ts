import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { VehicleSalesOrderComponent } from './vehicle-sales-order.component';
import { VehicleSalesOrderEditComponent } from './vehicle-sales-order-edit.component';
import { VehicleSalesOrderPopupComponent } from './vehicle-sales-order-dialog.component';
import { VehicleSalesOrderManualMatchingComponent } from './vehicle-sales-order-manual-matching.component';
import { AdminPoLeasingComponent } from './admin-handling/admin-po-leasing.component';
import { AdminPoLeasingDialogComponent } from './admin-handling/admin-po-leasing-dialog.component';
import { AdminPoLeasingPopupComponent } from './admin-handling/admin-po-leasing-dialog.component';
import { VehicleSalesOrderSalesComponent} from './vehicle-sales-order-sales.component';
import { VehicleSalesOrderSalesViewComponent } from './vehicle-sales-order-sales-view.component';
import { VehicleSalesOrderApproveKacabComponent } from './vehicle-sales-order-approve-kacab.component';
import { VehicleSalesOrderApproveKacabViewComponent } from './vehicle-sales-order-approve-kacab-view.component';
import { VehicleSalesOrderMemoKoreksiComponent } from './vehicle-sales-order-memo-koreksi.component';
import { RequestFakturPajakComponent } from './request-faktur-pajak/request-faktur-pajak.component';
import { RequestFakturPajakEditComponent } from './request-faktur-pajak/request-faktur-pajak-edit.component';
import { AdminPoLeasingDetailComponent } from './admin-handling/admin-po-leasing-detail.component';
import { VsoIvuEditComponent } from './vso-edit-detil/vso-ivu-edit.component';
import { SurVsoSalesEditComponent } from './vso-edit-detil/sur-vso-sales-edit.component';
import { VsoEditDataKendaraanComponent } from './vso-edit-detil/vso-edit-data-kendaraan.component';
import { VsoDraftKorsalComponent } from './vso-draft-korsal.component';
import { VsoDraftKorsalDetailComponent } from './vso-draft-korsal-detail.component';

@Injectable()
export class VehicleSalesOrderResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        // const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'orderNumber,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            // predicate: this.paginationUtil.parsePredicate(sort),
            // ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const vehicleSalesOrderRoute: Routes = [
    {
        path: 'vehicle-sales-order',
        component: VehicleSalesOrderComponent,
        resolve: {
            'pagingParams': VehicleSalesOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'admin-po-leasing',
        component: AdminPoLeasingComponent,
        resolve: {
            'pagingParams': VehicleSalesOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'admin-po-leasing-detail/:idOrder/detail',
        component: AdminPoLeasingDetailComponent,
        resolve: {
            'pagingParams': VehicleSalesOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request-faktur-pajak',
        component: RequestFakturPajakComponent,
        resolve: {
            'pagingParams': VehicleSalesOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestFakturPajak.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vehicle-sales-order/:ismatching',
        component: VehicleSalesOrderComponent,
        resolve: {
            'pagingParams': VehicleSalesOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vso-draft-korsal',
        component: VsoDraftKorsalComponent,
        resolve: {
            'pagingParams': VehicleSalesOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vso-draft-korsal/:id/detail',
        component: VsoDraftKorsalDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }

];

export const vehicleSalesOrderPopupRoute: Routes = [
    {
        path: 'vehicle-sales-order-new',
        component: VehicleSalesOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vehicle-sales-order/:id/edit',
        component: VehicleSalesOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vso-invoice/:id/edit',
        component: VsoIvuEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vehicle-sales-order/:id/matching/:ismatching',
        component: VehicleSalesOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vehicle-sales-order-popup-new',
        component: VehicleSalesOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'vehicle-sales-order/:id/popup-edit',
        component: VehicleSalesOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'admin-po-leasing/:id/popup-edit',
        component: AdminPoLeasingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }, {
        path: 'vehicle-sales-order-manual-matching',
        component: VehicleSalesOrderManualMatchingComponent,
        resolve: {
            'pagingParams': VehicleSalesOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'vehicle-sales-order-sales',
        component: VehicleSalesOrderSalesComponent,
        resolve: {
            'pagingParams': VehicleSalesOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'vehicle-sales-order-sales/:id',
        component: VehicleSalesOrderSalesViewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'vehicle-sales-order-approve-kacab',
        component: VehicleSalesOrderApproveKacabComponent,
        resolve: {
            'pagingParams': VehicleSalesOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'vehicle-sales-order-approve-kacab/:id',
        component: VehicleSalesOrderApproveKacabViewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vehicle-sales-order-memo-koreksi',
        component: VehicleSalesOrderMemoKoreksiComponent,
        resolve: {
            'pagingParams': VehicleSalesOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request-faktur-pajak/:id/edit',
        component: RequestFakturPajakEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sur-vso-sales-edit/:idReq',
        component: SurVsoSalesEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vso-sales-edit/:idreq/:idvso/:isedit',
        component: VsoEditDataKendaraanComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },

];
