import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import {LoadingService} from '../../layouts/loading';

import { Person } from '../person';
import { SalesUnitRequirement } from '../sales-unit-requirement/sales-unit-requirement.model';
import { SalesUnitRequirementService } from '../sales-unit-requirement/sales-unit-requirement.service';

@Component({
    selector: 'jhi-vso-draft-korsal-detail',
    templateUrl: './vso-draft-korsal-detail.component.html'
})
export class VsoDraftKorsalDetailComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;

    salesUnitRequirementId: number;

    salesUnitRequirement: SalesUnitRequirement;

    constructor(
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected loadingService: LoadingService
    ) {
        this.salesUnitRequirement = new SalesUnitRequirement();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.salesUnitRequirementId = params['id'];
                this.load(params['id']);
            }
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.loadingService.loadingStart();
        try {
            this.salesUnitRequirementService.find(id).subscribe((salesUnitRequirement) => {
                if (salesUnitRequirement.personOwner === null) {
                    salesUnitRequirement.personOwner = new Person();
                    salesUnitRequirement.personOwner.dob = null;
                }
                this.salesUnitRequirement = salesUnitRequirement;
            });
        } finally {
            this.loadingService.loadingStop();
        }
    }

    previousState() {
        this.router.navigate(['vso-draft-korsal']);
    }
}
