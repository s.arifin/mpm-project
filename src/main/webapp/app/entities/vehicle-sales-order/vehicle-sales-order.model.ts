import { BaseEntity } from './../../shared';
import { Customer } from './../customer';
import { Internal } from './../internal';
import { BillTo } from './../bill-to';
import { Orders } from './../orders';
import { SalesUnitRequirement } from './../sales-unit-requirement';

export class VehicleSalesOrder implements BaseEntity {
    constructor(
        public id?: any,
        public idOrder?: any,
        public internalId?: any,
        public customerId?: any,
        public billToId?: any,
        public idSalesUnitRequirement?: any,
        public leasingCompanyId?: any,
        public dateBASTUnit?: any,
        public dateCustomerReceipt?: any,
        public dateSwipeMachine?: any,
        public dateCoverNote?: any,
        public dateTakePicture?: any,
        public idLeasingStatusPayment?: any,
        public salesUnitRequirement?: SalesUnitRequirement,
        public afcVeriDate?: any,
        public adminSalesVeriDate?: any,
        public dateOrder?: any,
        public dateOrderStatusApprove?: any,
        public billingAmount?: any,
        public dateBilling?: any,
        public billingNumber?: any,
        public afcVerifyValue?: any,
        public adminSalesVerifyValue?: any,
        public adminSalesNotApprvNote?: any,
        public afcNotApprvNote?: any,
        public vrnama1?: any,
        public vrnama2?: any,
        public vrnamamarket?: any,
        public vrwarna?: any,
        public vrtahunproduksi?: any,
        public vrangsuran?: any,
        public vrtenor?: any,
        public vrleasing?: any,
        public vrtglpengiriman?: any,
        public vrjampengiriman?: any,
        public vrtipepenjualan?: any,
        public vrdpmurni?: any,
        public vrtandajadi?: any,
        public vrtglpembayaran?: any,
        public vrsisa?: any,
        public vriscod?: any,
        public vrcatatantambahan?: any,
        public vrisverified?: any,
        public vrcatatan?: any,
        public deliveryDate?: any,
        public leasingName?: any,
        public currentStatus?: any,
        public kacabnote?: any,
        public statusMotor?: any,
        public customer?: any,
        public doneMatching?: any,
        public idBiroJasa?: any,
        public orderNumber?: any,
        public idSalesman?: any,
        public requestNumber?: any,
        public subFincoyAsDp?: any,
        public refundFinco?: any,
        public canCloseDP?: boolean,
        public salesNumber?: string,
        public korsalName?: string,
        public orderTypeId?: any,
        public unitprice?: any,
        public downpayment?: any,
        public AR?: any,
        public bastFinco?: string,
        public name?: string,
        public identityNumber?: any,
    ) {
        this.salesUnitRequirement = new SalesUnitRequirement();
        this.canCloseDP = false;
        // this.vrtglpembayaran = new Date();
        // this.vrtglpengiriman = new Date();
        this.AR = unitprice - downpayment;
    }
}
export class TambahReference {
    constructor(
        public bastFinco?: string,
    ) {

    }
}
