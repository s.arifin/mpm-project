import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {VehicleSalesOrder} from './vehicle-sales-order.model';
import {VehicleSalesOrderPopupService} from './vehicle-sales-order-popup.service';
import {VehicleSalesOrderService} from './vehicle-sales-order.service';
import {ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { Customer, CustomerService } from '../customer';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-vehicle-sales-order-dialog',
    templateUrl: './vehicle-sales-order-dialog.component.html'
})
export class VehicleSalesOrderDialogComponent implements OnInit {

    vehicleSalesOrder: VehicleSalesOrder;
    isSaving: boolean;

    internals: Internal[];

    customers: Customer[];

    billtos: BillTo[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private internalService: InternalService,
        private customerService: CustomerService,
        private billToService: BillToService,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.vehicleSalesOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.vehicleSalesOrderService.update(this.vehicleSalesOrder));
        } else {
            this.subscribeToSaveResponse(
                this.vehicleSalesOrderService.create(this.vehicleSalesOrder));
        }
    }

    private subscribeToSaveResponse(result: Observable<VehicleSalesOrder>) {
        result.subscribe((res: VehicleSalesOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: VehicleSalesOrder) {
        this.eventManager.broadcast({ name: 'vehicleSalesOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vehicleSalesOrder saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'vehicleSalesOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}

@Component({
    selector: 'jhi-vehicle-sales-order-popup',
    template: ''
})
export class VehicleSalesOrderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vehicleSalesOrderPopupService: VehicleSalesOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vehicleSalesOrderPopupService
                    .open(VehicleSalesOrderDialogComponent as Component, params['id']);
            } else {
                this.vehicleSalesOrderPopupService
                    .open(VehicleSalesOrderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
