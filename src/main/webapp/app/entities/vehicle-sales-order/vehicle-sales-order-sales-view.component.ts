import { Component, OnInit, OnDestroy, SimpleChanges, OnChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService, JhiParseLinks, JhiPaginationUtil, JhiLanguageService } from 'ng-jhipster';

import { VehicleSalesOrder } from './vehicle-sales-order.model';
import { VehicleSalesOrderService } from './vehicle-sales-order.service';
import { LoadingService } from '../../layouts/loading/loading.service';
import { ConfirmationService } from 'primeng/primeng';
import * as reqType from '../../shared/constants/requirement-type.constants';

import { ToasterService, Principal, CommonUtilService } from '../../shared';
import { SalesUnitRequirement, SalesUnitRequirementService } from '../sales-unit-requirement';
import { ReceiptService } from '../receipt/receipt.service';
import { CurrencyPipe } from '@angular/common';
import { Requirement, RequirementService } from '../requirement';
import { PersonalCustomerService } from '../personal-customer/personal-customer.service';
import { OrganizationCustomerService } from '../organization-customer/organization-customer.service';
import { OrganizationCustomer } from '../organization-customer/organization-customer.model';
import { LeasingTenorProvideService, LeasingTenorProvide } from '../leasing-tenor-provide';
import { LeasingCompanyService, LeasingCompany } from '../leasing-company';
import { SaleTypeService } from '../sale-type';
import { CustomerService } from '../customer';
import { Feature } from '../feature';

@Component({
    selector: 'jhi-vehicle-sales-order-sales-view',
    templateUrl: './vehicle-sales-order-sales-view.component.html'
})
export class VehicleSalesOrderSalesViewComponent implements OnInit, OnDestroy, OnChanges {

    private subscription: Subscription;
    public vehicleSalesOrderId: number;
    public vehicleSalesOrder: VehicleSalesOrder;
    public dataApproves: any[];
    public isEdit: Boolean
    public isGanti: Boolean;
    public requirementId: any;
    public salesUnitRequirement: SalesUnitRequirement;
    public isCountReceipt: Boolean = true;
    private subscriptionReceipt: Subscription;
    public totalReceipt: number;
    public remainingPayment: any;
    public totalSubsidi: number;
    public organizationCustomer: OrganizationCustomer;
    public isLoad: Boolean = false;
    public sur: SalesUnitRequirement;
    public partyId: any;
    public remainingDownPayment: any;
    public paymentReadonly: Boolean = true;
    public isCredit: boolean;
    public installment: number;
    public leasingCompany: LeasingCompany;
    public leasingTenorProvide: LeasingTenorProvide;
    public isLoadPage: Boolean = false;

    constructor(
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private route: ActivatedRoute,
        private router: Router,
        private loadingService: LoadingService,
        private confirmationService: ConfirmationService,
        private toaster: ToasterService,
        private principal: Principal,
        private salesUnitRequirementService: SalesUnitRequirementService,
        private receiptService: ReceiptService,
        private currencyPipe: CurrencyPipe,
        private requirementService: RequirementService,
        private commontUtilService: CommonUtilService,
        private personalCustomerService: PersonalCustomerService,
        private organizationCustomerService: OrganizationCustomerService,
        private leasingTenorProvideService: LeasingTenorProvideService,
        private leasingCompanyService: LeasingCompanyService,
        private saleTypeService: SaleTypeService,
        private customerService: CustomerService,
    ) {
        this.vehicleSalesOrder = new VehicleSalesOrder();
        this.isGanti = false;
        this.salesUnitRequirement = new SalesUnitRequirement();
        this.totalReceipt = 0;
        this.remainingPayment = '';
        this.totalSubsidi = 0;
        this.sur = new  SalesUnitRequirement();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.dataApproves = [
            { nama: 'Admin Sales', isApprove: false },
            { nama: 'AFC', isApprove: false },
            { nama: 'KACAB', isApprove: false }
        ];
    }

    // ngOnDestroy() {
    //     this.subscription.unsubscribe();
    // }

    load(id) {
        this.loadingService.loadingStart();

        this.vehicleSalesOrderService.find(id, this.principal.getIdInternal()).subscribe(
            (res) => {
                this.vehicleSalesOrder = res;
                this.registerReceipt();
                console.log('isi vso', this.vehicleSalesOrder);

                if (this.vehicleSalesOrder.salesUnitRequirement.idReqTyp === reqType.ORGANIZATION_CUSTOMER) {
                    this.getDataByOrganization(this.vehicleSalesOrder.salesUnitRequirement);
                } else if (this.vehicleSalesOrder.salesUnitRequirement.idReqTyp === null) {
                    this.getDataByPersonal(this.vehicleSalesOrder.salesUnitRequirement);
                }
            },
            (err) => { },
            () => this.loadingService.loadingStop(),
        );
    }

    previousState() {
        this.router.navigate(['vehicle-sales-order-sales']);
    }

    cancelVso() {
        console.log('Cancel VSO');
        this.confirmationService.confirm({
            message: 'Are you sure that you want to cancel vso ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleSalesOrderService
                    .executeProcess(13, null, this.vehicleSalesOrder)
                    .subscribe((res) => {
                        this.toaster.showToaster('info', 'Data Proces', 'Cancel VSO');
                        console.log('approve');
                        this.router.navigate(['../../vehicle-sales-order-sales']);
                    });
            },
            reject: () => {

            }
        });
    }
    editVso() {
        console.log('Edit VSO');
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Akan Edit VSO ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                // this.router.navigate(['../../sur-vso-sales-edit//edit']);
                this.router.navigate(['../vso-sales-edit/' + this.vehicleSalesOrder.salesUnitRequirement.idRequirement + '/' + this.vehicleSalesOrder.idOrder + '/true'])
            },
            reject: () => {

            }
        });
    }

    gantiVso(data: SalesUnitRequirement, vso: VehicleSalesOrder) {
        console.log('Ganti VSo');
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin Akan Ganti VSO ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleSalesOrderService.executeProcess(105, null, vso)
                    .subscribe(
                        (res) => {
                            this.isGanti = true;
                            console.log('ganti vsoo ', this.isGanti);
                            this.requirementId = data.idRequirement;
                            console.log('ganti vsoo idrequest', this.requirementId);
                            console.log('gantiiii piesoh ahh  ', res);
                        },
                        (err) => {
                            this.commontUtilService.showError(err);
                        }
                    )
            },
            reject: () => {

            }
        });
    }

    // private registerReceipt(): void {
    //     this.subscriptionReceipt = this.receiptService.values.subscribe(
    //         (res) => {
    //             this.countReceipt(res);
    //         }
    //     );
    // }

    public countReceipt(res): void {
        this.totalReceipt = this.receiptService.countTotalReceipt(res);
        this.countRemainingPayment();
        //    this.setRequirementNumber();
    }

    public countRemainingPayment(): void {
        const productPrice = this.salesUnitRequirement.unitPrice;
        if (productPrice !== null) {
            this.countTotalSubsidi();
            this.remainingPayment = this.currencyPipe.transform(productPrice - this.totalSubsidi - this.totalReceipt, 'IDR', true);

            this.salesUnitRequirement.downPayment = this.totalSubsidi + this.totalReceipt;
        } else {
            this.remainingPayment = 'Please select motor';
        }
    }

    private setRequirementNumber(): void {
        if (this.salesUnitRequirement.requirementNumber === null || this.salesUnitRequirement.requirementNumber === undefined) {
            this.requirementService.find(this.vehicleSalesOrder.idSalesUnitRequirement).subscribe(
                (res: Requirement) => {
                    this.salesUnitRequirement.requirementNumber = res.requirementNumber;
                },
                (err) => {
                    this.commontUtilService.showError(err);
                })
        }
    }

    // public countTotalSubsidi(): void {
    //     this.totalSubsidi = this.salesUnitRequirementService.fnCountTotalSubsidi(this.salesUnitRequirement);
    // }

    private getDataByOrganization(salesUnitRequirement: SalesUnitRequirement): void {
        this.organizationCustomerService.find(salesUnitRequirement.customerId).subscribe(
            (res) => {
                this.organizationCustomer = res;
                salesUnitRequirement.partyId = res.partyId;
                this.salesUnitRequirement = salesUnitRequirement;
            },
            (err) => {
                this.commontUtilService.showError(err);
                this.loadingService.loadingStop();
            }
        )
    }

    private getDataByPersonal(salesUnitRequirement: SalesUnitRequirement): void {
        this.personalCustomerService.find(salesUnitRequirement.customerId).subscribe(
            (res) => {
                salesUnitRequirement.partyId = res.partyId;
                this.salesUnitRequirement = salesUnitRequirement;
            },
            (err) => {
                this.commontUtilService.showError(err);
                this.loadingService.loadingStop();
            }
        )
    }

    loadsur(id) {
        this.salesUnitRequirementService.find(id).subscribe(
            (res) => {
                this.isLoadPage = true;
                this.sur = res;
                this.customerService.find(res.customerId)
                .subscribe(
                    (rescustomer) => {
                        this.partyId = rescustomer.partyId;
                    }
                )
                console.log('surrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr', res)
            }
        )
    }

    onTabOpen(event) {
        console.log('masuk tab open ah', event);
        this.isLoad = true;
        this.loadsur(this.vehicleSalesOrder.idSalesUnitRequirement);
    }

    onTabClose(event) {
        this.isLoad = false;
        console.log('masuk tab close ah', event);
        this.loadsur(this.vehicleSalesOrder.idSalesUnitRequirement);
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['salesUnitRequirement']) {
            this.salesUnitRequirementService.find(this.salesUnitRequirement.idRequirement)
            .subscribe((res) => {
                this.sur = res;
                this.customerService.find(res.customerId)
                .subscribe(
                    (rescustomer) => {
                        this.partyId = rescustomer.partyId;
                    }
                )
            });
            console.log('SUR di payment', this.salesUnitRequirement);
            this.registerReceipt().then(
                (resolve) => {
                    this.loadData();
                    this.countTotalSubsidi();
                }
            )
        }
        if (changes['isLoadPage']) {
            console.log('SUR di payment', this.salesUnitRequirement);
            this.salesUnitRequirementService.find(this.salesUnitRequirement.idRequirement)
            .subscribe((res) => {
                this.sur = res;
                this.customerService.find(res.customerId)
                .subscribe(
                    (rescustomer) => {
                        this.partyId = rescustomer.partyId;
                    }
                )
            });
            this.registerReceipt().then(
                (resolve) => {
                    this.loadData();
                    this.countTotalSubsidi();
                }
            )
        }
    }

    ngOnDestroy() {
        this.subscriptionReceipt.unsubscribe();
    }

    private registerReceipt(): Promise<void> {
        return new Promise<void>(
            (resolve, reject) => {
                this.subscriptionReceipt = this.receiptService.values.subscribe(
                    (res) => {
                        this.countReceipt(res);
                        this.loadData();
                        resolve();
                    }
                );
            }
        );
    }

    private loadData(): void {
        if (this.saleTypeService.checkIfCredit(this.salesUnitRequirement.saleTypeId)) {
            this.isCredit = true;
            this.countRemainingDownPayment();
        }
    }

    // public countReceipt(res): void {
    //     this.totalReceipt = this.receiptService.countTotalReceipt(res);
    //  }

    // public countRemainingPayment(): void {
    //     const productPrice = this.salesUnitRequirement.unitPrice;
    //     if (productPrice !== null) {
    //         // this.countTotalSubsidi();
    //         this.remainingPayment = this.currencyPipe.transform(productPrice - this.totalSubsidi - this.totalReceipt, 'IDR', true);

    //         this.salesUnitRequirement.downPayment = this.totalSubsidi + this.totalReceipt;
    //     } else {
    //         this.remainingPayment = 'Please choose product!';
    //     }
    // }

    public countRemainingDownPayment(): void {
        console.log('this.totalReceipt', this.totalReceipt);
        this.remainingDownPayment = this.salesUnitRequirementService.countRemainingDownPaymentForCredit(this.salesUnitRequirement, this.totalReceipt);
    }

    countTotalSubsidi(): void {
        this.totalSubsidi = this.salesUnitRequirementService.fnCountTotalSubsidi(this.salesUnitRequirement);
        this.countRemainingPayment();
    }

    public trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }
}
