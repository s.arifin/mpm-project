import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { ShipmentPackageType } from './shipment-package-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ShipmentPackageTypeService {
    protected itemValues: ShipmentPackageType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/shipment-package-types';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/shipment-package-types';

    constructor(protected http: Http) { }

    create(shipmentPackageType: ShipmentPackageType): Observable<ShipmentPackageType> {
        const copy = this.convert(shipmentPackageType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(shipmentPackageType: ShipmentPackageType): Observable<ShipmentPackageType> {
        const copy = this.convert(shipmentPackageType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<ShipmentPackageType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, shipmentPackageType: ShipmentPackageType): Observable<ShipmentPackageType> {
        const copy = this.convert(shipmentPackageType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, shipmentPackageTypes: ShipmentPackageType[]): Observable<ShipmentPackageType[]> {
        const copy = this.convertList(shipmentPackageTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(shipmentPackageType: ShipmentPackageType): ShipmentPackageType {
        if (shipmentPackageType === null || shipmentPackageType === {}) {
            return {};
        }
        // const copy: ShipmentPackageType = Object.assign({}, shipmentPackageType);
        const copy: ShipmentPackageType = JSON.parse(JSON.stringify(shipmentPackageType));
        return copy;
    }

    protected convertList(shipmentPackageTypes: ShipmentPackageType[]): ShipmentPackageType[] {
        const copy: ShipmentPackageType[] = shipmentPackageTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ShipmentPackageType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
