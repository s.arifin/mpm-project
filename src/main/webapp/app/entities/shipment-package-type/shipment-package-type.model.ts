import { BaseEntity } from './../../shared';

export class ShipmentPackageType implements BaseEntity {
    constructor(
        public id?: number,
        public idPackageType?: number,
        public description?: string,
    ) {
    }
}
