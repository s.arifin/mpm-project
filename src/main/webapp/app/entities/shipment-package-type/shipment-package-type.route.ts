import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ShipmentPackageTypeComponent } from './shipment-package-type.component';
import { ShipmentPackageTypePopupComponent } from './shipment-package-type-dialog.component';

@Injectable()
export class ShipmentPackageTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idPackageType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const shipmentPackageTypeRoute: Routes = [
    {
        path: 'shipment-package-type',
        component: ShipmentPackageTypeComponent,
        resolve: {
            'pagingParams': ShipmentPackageTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentPackageType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const shipmentPackageTypePopupRoute: Routes = [
    {
        path: 'shipment-package-type-new',
        component: ShipmentPackageTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentPackageType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shipment-package-type/:id/edit',
        component: ShipmentPackageTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentPackageType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
