import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {ShipmentPackageType} from './shipment-package-type.model';
import {ShipmentPackageTypePopupService} from './shipment-package-type-popup.service';
import {ShipmentPackageTypeService} from './shipment-package-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-shipment-package-type-dialog',
    templateUrl: './shipment-package-type-dialog.component.html'
})
export class ShipmentPackageTypeDialogComponent implements OnInit {

    shipmentPackageType: ShipmentPackageType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected shipmentPackageTypeService: ShipmentPackageTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.shipmentPackageType.idPackageType !== undefined) {
            this.subscribeToSaveResponse(
                this.shipmentPackageTypeService.update(this.shipmentPackageType));
        } else {
            this.subscribeToSaveResponse(
                this.shipmentPackageTypeService.create(this.shipmentPackageType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ShipmentPackageType>) {
        result.subscribe((res: ShipmentPackageType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ShipmentPackageType) {
        this.eventManager.broadcast({ name: 'shipmentPackageTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'shipmentPackageType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'shipmentPackageType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-shipment-package-type-popup',
    template: ''
})
export class ShipmentPackageTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected shipmentPackageTypePopupService: ShipmentPackageTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.shipmentPackageTypePopupService
                    .open(ShipmentPackageTypeDialogComponent as Component, params['id']);
            } else {
                this.shipmentPackageTypePopupService
                    .open(ShipmentPackageTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
