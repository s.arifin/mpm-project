import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { BillingType } from './billing-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class BillingTypeService {
    protected itemValues: BillingType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/billing-types';
    protected resourceSearchUrl = 'api/_search/billing-types';

    constructor(protected http: Http) { }

    create(billingType: BillingType): Observable<BillingType> {
        const copy = this.convert(billingType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(billingType: BillingType): Observable<BillingType> {
        const copy = this.convert(billingType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<BillingType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, billingType: BillingType): Observable<BillingType> {
        const copy = this.convert(billingType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, billingTypes: BillingType[]): Observable<BillingType[]> {
        const copy = this.convertList(billingTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(billingType: BillingType): BillingType {
        if (billingType === null || billingType === {}) {
            return {};
        }
        // const copy: BillingType = Object.assign({}, billingType);
        const copy: BillingType = JSON.parse(JSON.stringify(billingType));
        return copy;
    }

    protected convertList(billingTypes: BillingType[]): BillingType[] {
        const copy: BillingType[] = billingTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: BillingType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
