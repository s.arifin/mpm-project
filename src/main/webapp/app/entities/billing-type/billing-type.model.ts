import { BaseEntity } from './../../shared';

export class BillingType implements BaseEntity {
    constructor(
        public id?: number,
        public idBillingType?: number,
        public description?: string,
    ) {
    }
}
