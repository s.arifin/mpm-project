import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {BillingType} from './billing-type.model';
import {BillingTypePopupService} from './billing-type-popup.service';
import {BillingTypeService} from './billing-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-billing-type-dialog',
    templateUrl: './billing-type-dialog.component.html'
})
export class BillingTypeDialogComponent implements OnInit {

    billingType: BillingType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected billingTypeService: BillingTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.billingType.idBillingType !== undefined) {
            this.subscribeToSaveResponse(
                this.billingTypeService.update(this.billingType));
        } else {
            this.subscribeToSaveResponse(
                this.billingTypeService.create(this.billingType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<BillingType>) {
        result.subscribe((res: BillingType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: BillingType) {
        this.eventManager.broadcast({ name: 'billingTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'billingType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'billingType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-billing-type-popup',
    template: ''
})
export class BillingTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected billingTypePopupService: BillingTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.billingTypePopupService
                    .open(BillingTypeDialogComponent as Component, params['id']);
            } else {
                this.billingTypePopupService
                    .open(BillingTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
