import { BaseEntity } from './../../shared';

export class BillTo implements BaseEntity {
    constructor(
        public id?: number,
        public idBillTo?: number,
        public idRoleType?: number,
        public partyId?: any,
    ) {
    }
}
