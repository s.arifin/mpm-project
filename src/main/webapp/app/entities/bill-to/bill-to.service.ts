import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { BillTo } from './bill-to.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class BillToService {
   protected itemValues: BillTo[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/bill-tos';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/bill-tos';

   constructor(protected http: Http) { }

   create(billTo: BillTo): Observable<BillTo> {
       const copy = this.convert(billTo);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(billTo: BillTo): Observable<BillTo> {
       const copy = this.convert(billTo);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<BillTo> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: BillTo, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: BillTo[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to BillTo.
    */
   protected convertItemFromServer(json: any): BillTo {
       const entity: BillTo = Object.assign(new BillTo(), json);
       return entity;
   }

   /**
    * Convert a BillTo to a JSON which can be sent to the server.
    */
   protected convert(billTo: BillTo): BillTo {
       if (billTo === null || billTo === {}) {
           return {};
       }
       // const copy: BillTo = Object.assign({}, billTo);
       const copy: BillTo = JSON.parse(JSON.stringify(billTo));
       return copy;
   }

   protected convertList(billTos: BillTo[]): BillTo[] {
       const copy: BillTo[] = billTos;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: BillTo[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
