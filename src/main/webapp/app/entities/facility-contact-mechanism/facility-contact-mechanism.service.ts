import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { FacilityContactMechanism } from './facility-contact-mechanism.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class FacilityContactMechanismService {
   protected itemValues: FacilityContactMechanism[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/facility-contact-mechanisms';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/facility-contact-mechanisms';

   constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

   create(facilityContactMechanism: FacilityContactMechanism): Observable<FacilityContactMechanism> {
       const copy = this.convert(facilityContactMechanism);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(facilityContactMechanism: FacilityContactMechanism): Observable<FacilityContactMechanism> {
       const copy = this.convert(facilityContactMechanism);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<FacilityContactMechanism> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: FacilityContactMechanism, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: FacilityContactMechanism[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to FacilityContactMechanism.
    */
   protected convertItemFromServer(json: any): FacilityContactMechanism {
       const entity: FacilityContactMechanism = Object.assign(new FacilityContactMechanism(), json);
       if (entity.dateFrom) {
           entity.dateFrom = new Date(entity.dateFrom);
       }
       if (entity.dateThru) {
           entity.dateThru = new Date(entity.dateThru);
       }
       return entity;
   }

   /**
    * Convert a FacilityContactMechanism to a JSON which can be sent to the server.
    */
   protected convert(facilityContactMechanism: FacilityContactMechanism): FacilityContactMechanism {
       if (facilityContactMechanism === null || facilityContactMechanism === {}) {
           return {};
       }
       // const copy: FacilityContactMechanism = Object.assign({}, facilityContactMechanism);
       const copy: FacilityContactMechanism = JSON.parse(JSON.stringify(facilityContactMechanism));

       // copy.dateFrom = this.dateUtils.toDate(facilityContactMechanism.dateFrom);

       // copy.dateThru = this.dateUtils.toDate(facilityContactMechanism.dateThru);
       return copy;
   }

   protected convertList(facilityContactMechanisms: FacilityContactMechanism[]): FacilityContactMechanism[] {
       const copy: FacilityContactMechanism[] = facilityContactMechanisms;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: FacilityContactMechanism[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
