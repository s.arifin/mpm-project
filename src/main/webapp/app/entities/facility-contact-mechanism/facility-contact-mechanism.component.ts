import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { FacilityContactMechanism } from './facility-contact-mechanism.model';
import { FacilityContactMechanismService } from './facility-contact-mechanism.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
   selector: 'jhi-facility-contact-mechanism',
   templateUrl: './facility-contact-mechanism.component.html'
})
export class FacilityContactMechanismComponent implements OnInit, OnDestroy {

    currentAccount: any;
    facilityContactMechanisms: FacilityContactMechanism[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        protected facilityContactMechanismService: FacilityContactMechanismService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
                    this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
    }

    loadAll() {
        if (this.currentSearch) {
            this.facilityContactMechanismService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.facilityContactMechanismService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/facility-contact-mechanism'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/facility-contact-mechanism', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/facility-contact-mechanism', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

   ngOnInit() {
       this.loadAll();
       this.principal.identity(true).then((account) => {
           this.currentAccount = account;
       });
       this.registerChangeInFacilityContactMechanisms();
   }

   ngOnDestroy() {
       this.eventManager.destroy(this.eventSubscriber);
   }

   trackId(index: number, item: FacilityContactMechanism) {
       return item.idContactMechPurpose;
   }

   registerChangeInFacilityContactMechanisms() {
       this.eventSubscriber = this.eventManager.subscribe('facilityContactMechanismListModification', (response) => this.loadAll());
   }

   sort() {
       const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
       if (this.predicate !== 'idContactMechPurpose') {
           result.push('idContactMechPurpose');
       }
       return result;
   }

   protected onSuccess(data, headers) {
       this.links = this.parseLinks.parse(headers.get('link'));
       this.totalItems = headers.get('X-Total-Count');
       this.queryCount = this.totalItems;
       // this.page = pagingParams.page;
       this.facilityContactMechanisms = data;
   }

   protected onError(error) {
       this.jhiAlertService.error(error.message, null, null);
   }

   executeProcess(item) {
       this.facilityContactMechanismService.executeProcess(item).subscribe(
          (value) => console.log('this: ', value),
          (err) => console.log(err),
          () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
       );
   }

   loadDataLazy(event: LazyLoadEvent) {
       this.itemsPerPage = event.rows;
       this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
       this.previousPage = this.page;

       if (event.sortField !== undefined) {
           this.predicate = event.sortField;
           this.reverse = event.sortOrder;
       }
       this.loadAll();
   }

   updateRowData(event) {
       if (event.data.id !== undefined) {
           this.facilityContactMechanismService.update(event.data)
               .subscribe((res: FacilityContactMechanism) =>
                   this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
       } else {
           this.facilityContactMechanismService.create(event.data)
               .subscribe((res: FacilityContactMechanism) =>
                   this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
       }
   }

   protected onRowDataSaveSuccess(result: FacilityContactMechanism) {
       this.toaster.showToaster('info', 'FacilityContactMechanism Saved', 'Data saved..');
   }

   protected onRowDataSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.onError(error);
   }

   delete(id: any) {
       this.confirmationService.confirm({
           message: 'Are you sure that you want to delete?',
           header: 'Confirmation',
           icon: 'fa fa-question-circle',
           accept: () => {
               this.facilityContactMechanismService.delete(id).subscribe((response) => {
                   this.eventManager.broadcast({
                       name: 'facilityContactMechanismListModification',
                       content: 'Deleted an facilityContactMechanism'
                   });
               });
           }
       });
   }

   process() {
       this.facilityContactMechanismService.process({}).subscribe((r) => {
           console.log('result', r);
           this.toaster.showToaster('info', 'Data Processed', 'processed.....');
       });
   }

   buildReindex() {
    this.facilityContactMechanismService.process({command: 'buildIndex'}).subscribe((r) => {
        this.toaster.showToaster('info', 'Build Index', 'Build Index processed.....');
    });
}

}
