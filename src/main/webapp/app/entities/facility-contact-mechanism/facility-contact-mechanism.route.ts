import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { FacilityContactMechanismComponent } from './facility-contact-mechanism.component';
import { FacilityContactMechanismPopupComponent } from './facility-contact-mechanism-dialog.component';

@Injectable()
export class FacilityContactMechanismResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idContactMechPurpose,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const facilityContactMechanismRoute: Routes = [
    {
        path: 'facility-contact-mechanism',
        component: FacilityContactMechanismComponent,
        resolve: {
            'pagingParams': FacilityContactMechanismResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facilityContactMechanism.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const facilityContactMechanismPopupRoute: Routes = [
    {
        path: 'facility-contact-mechanism-popup-new-list/:parent',
        component: FacilityContactMechanismPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facilityContactMechanism.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'facility-contact-mechanism-new',
        component: FacilityContactMechanismPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facilityContactMechanism.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'facility-contact-mechanism/:id/edit',
        component: FacilityContactMechanismPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facilityContactMechanism.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
