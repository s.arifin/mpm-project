import { BaseEntity } from './../../shared';

export class FacilityContactMechanism implements BaseEntity {
    constructor(
        public id?: any,
        public idContactMechPurpose?: any,
        public idPurposeType?: number,
        public dateFrom?: any,
        public dateThru?: any,
        public facilityId?: any,
        public contactId?: any,
    ) {
    }
}
