import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { FacilityContactMechanism } from './facility-contact-mechanism.model';
import { FacilityContactMechanismService } from './facility-contact-mechanism.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-facility-contact-mechanism-as-list',
    templateUrl: './facility-contact-mechanism-as-list.component.html'
})
export class FacilityContactMechanismAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idFacility: any;
    @Input() idContact: any;

    currentAccount: any;
    facilityContactMechanisms: FacilityContactMechanism[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        protected facilityContactMechanismService: FacilityContactMechanismService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'idContactMechPurpose';
        this.reverse = 'asc';
        this.first = 0;
    }

    loadAll() {
        this.facilityContactMechanismService.queryFilterBy({
            idFacility: this.idFacility,
            idContact: this.idContact,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/facility-contact-mechanism', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInFacilityContactMechanisms();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idFacility']) {
            this.loadAll();
        }
        if (changes['idContact']) {
            this.loadAll();
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: FacilityContactMechanism) {
        return item.idContactMechPurpose;
    }

    registerChangeInFacilityContactMechanisms() {
        this.eventSubscriber = this.eventManager.subscribe('facilityContactMechanismListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idContactMechPurpose') {
            result.push('idContactMechPurpose');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.facilityContactMechanisms = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.facilityContactMechanismService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.subscribeToSaveResponse(
                this.facilityContactMechanismService.update(event.data));
        } else {
            this.subscribeToSaveResponse(
                this.facilityContactMechanismService.create(event.data));
        }
    }

    protected subscribeToSaveResponse(result: Observable<FacilityContactMechanism>) {
        result.subscribe((res: FacilityContactMechanism) => this.onSaveSuccess(res));
    }

    protected onSaveSuccess(result: FacilityContactMechanism) {
        this.eventManager.broadcast({ name: 'facilityContactMechanismListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'facilityContactMechanism saved !');
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.facilityContactMechanismService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'facilityContactMechanismListModification',
                        content: 'Deleted an facilityContactMechanism'
                    });
                });
            }
        });
    }
}
