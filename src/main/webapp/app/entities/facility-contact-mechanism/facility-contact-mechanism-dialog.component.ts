import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { FacilityContactMechanism } from './facility-contact-mechanism.model';
import { FacilityContactMechanismPopupService } from './facility-contact-mechanism-popup.service';
import { FacilityContactMechanismService } from './facility-contact-mechanism.service';
import { ToasterService } from '../../shared';
import { Facility, FacilityService } from '../facility';
import { ContactMechanism, ContactMechanismService } from '../contact-mechanism';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-facility-contact-mechanism-dialog',
    templateUrl: './facility-contact-mechanism-dialog.component.html'
})
export class FacilityContactMechanismDialogComponent implements OnInit {

    facilityContactMechanism: FacilityContactMechanism;
    isSaving: boolean;
    idFacility: any;
    idContact: any;

    facilities: Facility[];

    contactmechanisms: ContactMechanism[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected facilityContactMechanismService: FacilityContactMechanismService,
        protected facilityService: FacilityService,
        protected contactMechanismService: ContactMechanismService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.contactMechanismService.query()
            .subscribe((res: ResponseWrapper) => { this.contactmechanisms = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.facilityContactMechanism.idContactMechPurpose !== undefined) {
            this.subscribeToSaveResponse(
                this.facilityContactMechanismService.update(this.facilityContactMechanism));
        } else {
            this.subscribeToSaveResponse(
                this.facilityContactMechanismService.create(this.facilityContactMechanism));
        }
    }

    protected subscribeToSaveResponse(result: Observable<FacilityContactMechanism>) {
        result.subscribe((res: FacilityContactMechanism) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: FacilityContactMechanism) {
        this.eventManager.broadcast({ name: 'facilityContactMechanismListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'facilityContactMechanism saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'facilityContactMechanism Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackContactMechanismById(index: number, item: ContactMechanism) {
        return item.idContact;
    }
}

@Component({
    selector: 'jhi-facility-contact-mechanism-popup',
    template: ''
})
export class FacilityContactMechanismPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected facilityContactMechanismPopupService: FacilityContactMechanismPopupService
    ) {}

    ngOnInit() {
        this.facilityContactMechanismPopupService.idFacility = undefined;
        this.facilityContactMechanismPopupService.idContact = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.facilityContactMechanismPopupService
                    .open(FacilityContactMechanismDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.facilityContactMechanismPopupService.parent = params['parent'];
                this.facilityContactMechanismPopupService
                    .open(FacilityContactMechanismDialogComponent as Component);
            } else {
                this.facilityContactMechanismPopupService
                    .open(FacilityContactMechanismDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
