import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PartSalesOrder } from './part-sales-order.model';
import { PartSalesOrderService } from './part-sales-order.service';

@Injectable()
export class PartSalesOrderPopupService {
    protected ngbModalRef: NgbModalRef;
    idInternal: any;
    idCustomer: any;
    idBillTo: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected partSalesOrderService: PartSalesOrderService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.partSalesOrderService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.partSalesOrderModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new PartSalesOrder();
                    data.internalId = this.idInternal;
                    data.customerId = this.idCustomer;
                    data.billToId = this.idBillTo;
                    this.ngbModalRef = this.partSalesOrderModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    partSalesOrderModalRef(component: Component, partSalesOrder: PartSalesOrder): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.partSalesOrder = partSalesOrder;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idCustomer = this.idCustomer;
        modalRef.componentInstance.idBillTo = this.idBillTo;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.partSalesOrderLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    partSalesOrderLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
