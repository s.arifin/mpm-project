import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PartSalesOrderComponent } from './part-sales-order.component';
import { PartSalesOrderEditComponent } from './part-sales-order-edit.component';
import { PartSalesOrderPopupComponent } from './part-sales-order-dialog.component';

@Injectable()
export class PartSalesOrderResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idOrder,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const partSalesOrderRoute: Routes = [
    {
        path: 'part-sales-order',
        component: PartSalesOrderComponent,
        resolve: {
            'pagingParams': PartSalesOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const partSalesOrderPopupRoute: Routes = [
    {
        path: 'part-sales-order-popup-new',
        component: PartSalesOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'part-sales-order-new',
        component: PartSalesOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'part-sales-order/:id/edit',
        component: PartSalesOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'part-sales-order/:route/:page/:id/edit',
        component: PartSalesOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'part-sales-order/:id/popup-edit',
        component: PartSalesOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
