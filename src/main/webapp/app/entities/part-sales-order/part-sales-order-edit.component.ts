import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PartSalesOrder } from './part-sales-order.model';
import { PartSalesOrderService } from './part-sales-order.service';
import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { Customer, CustomerService } from '../customer';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-part-sales-order-edit',
    templateUrl: './part-sales-order-edit.component.html'
})
export class PartSalesOrderEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    partSalesOrder: PartSalesOrder;
    isSaving: boolean;
    idOrder: any;
    paramPage: number;
    routeId: number;

    internals: Internal[];

    customers: Customer[];

    billtos: BillTo[];

    constructor(
        protected alertService: JhiAlertService,
        protected partSalesOrderService: PartSalesOrderService,
        protected internalService: InternalService,
        protected customerService: CustomerService,
        protected billToService: BillToService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.partSalesOrder = new PartSalesOrder();
        this.routeId = 0;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idOrder = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.partSalesOrderService.find(this.idOrder).subscribe((partSalesOrder) => {
            this.partSalesOrder = partSalesOrder;
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['part-sales-order', { page: this.paramPage }]);
        }
    }

    save() {
        this.isSaving = true;
        if (this.partSalesOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.partSalesOrderService.update(this.partSalesOrder));
        } else {
            this.subscribeToSaveResponse(
                this.partSalesOrderService.create(this.partSalesOrder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PartSalesOrder>) {
        result.subscribe((res: PartSalesOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: PartSalesOrder) {
        this.eventManager.broadcast({ name: 'partSalesOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'partSalesOrder saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'partSalesOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}
