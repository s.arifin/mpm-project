import { BaseEntity } from './../../shared';

export class PartSalesOrder implements BaseEntity {
    constructor(
        public id?: any,
        public idOrder?: any,
        public internalId?: any,
        public customerId?: any,
        public billToId?: any,
    ) {
    }
}
