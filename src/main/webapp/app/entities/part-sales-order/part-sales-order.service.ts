import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { PartSalesOrder } from './part-sales-order.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PartSalesOrderService {
    protected itemValues: PartSalesOrder[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/part-sales-orders';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/part-sales-orders';

    constructor(protected http: Http) { }

    create(partSalesOrder: PartSalesOrder): Observable<PartSalesOrder> {
        const copy = this.convert(partSalesOrder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(partSalesOrder: PartSalesOrder): Observable<PartSalesOrder> {
        const copy = this.convert(partSalesOrder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<PartSalesOrder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(partSalesOrder: PartSalesOrder, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(partSalesOrder);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<PartSalesOrder> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<PartSalesOrder[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PartSalesOrder.
     */
    protected convertItemFromServer(json: any): PartSalesOrder {
        const entity: PartSalesOrder = Object.assign(new PartSalesOrder(), json);
        return entity;
    }

    /**
     * Convert a PartSalesOrder to a JSON which can be sent to the server.
     */
    protected convert(partSalesOrder: PartSalesOrder): PartSalesOrder {
        if (partSalesOrder === null || partSalesOrder === {}) {
            return {};
        }
        // const copy: PartSalesOrder = Object.assign({}, partSalesOrder);
        const copy: PartSalesOrder = JSON.parse(JSON.stringify(partSalesOrder));
        return copy;
    }

    protected convertList(partSalesOrders: PartSalesOrder[]): PartSalesOrder[] {
        const copy: PartSalesOrder[] = partSalesOrders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: PartSalesOrder[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
