import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PartSalesOrder } from './part-sales-order.model';
import { PartSalesOrderPopupService } from './part-sales-order-popup.service';
import { PartSalesOrderService } from './part-sales-order.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { Customer, CustomerService } from '../customer';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-part-sales-order-dialog',
    templateUrl: './part-sales-order-dialog.component.html'
})
export class PartSalesOrderDialogComponent implements OnInit {

    partSalesOrder: PartSalesOrder;
    isSaving: boolean;
    idInternal: any;
    idCustomer: any;
    idBillTo: any;

    internals: Internal[];

    customers: Customer[];

    billtos: BillTo[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected partSalesOrderService: PartSalesOrderService,
        protected internalService: InternalService,
        protected customerService: CustomerService,
        protected billToService: BillToService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.partSalesOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.partSalesOrderService.update(this.partSalesOrder));
        } else {
            this.subscribeToSaveResponse(
                this.partSalesOrderService.create(this.partSalesOrder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PartSalesOrder>) {
        result.subscribe((res: PartSalesOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: PartSalesOrder) {
        this.eventManager.broadcast({ name: 'partSalesOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'partSalesOrder saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'partSalesOrder Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}

@Component({
    selector: 'jhi-part-sales-order-popup',
    template: ''
})
export class PartSalesOrderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected partSalesOrderPopupService: PartSalesOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.partSalesOrderPopupService
                    .open(PartSalesOrderDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.partSalesOrderPopupService.parent = params['parent'];
                this.partSalesOrderPopupService
                    .open(PartSalesOrderDialogComponent as Component);
            } else {
                this.partSalesOrderPopupService
                    .open(PartSalesOrderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
