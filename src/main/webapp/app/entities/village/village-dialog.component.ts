import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {Village} from './village.model';
import {VillagePopupService} from './village-popup.service';
import {VillageService} from './village.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-village-dialog',
    templateUrl: './village-dialog.component.html'
})
export class VillageDialogComponent implements OnInit {

    village: Village;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private villageService: VillageService,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.village.idGeobou !== undefined) {
            this.subscribeToSaveResponse(
                this.villageService.update(this.village));
        } else {
            this.subscribeToSaveResponse(
                this.villageService.create(this.village));
        }
    }

    private subscribeToSaveResponse(result: Observable<Village>) {
        result.subscribe((res: Village) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Village) {
        this.eventManager.broadcast({ name: 'villageListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'village saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.toaster.showToaster('warning', 'village Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-village-popup',
    template: ''
})
export class VillagePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private villagePopupService: VillagePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.villagePopupService
                    .open(VillageDialogComponent as Component, params['id']);
            } else {
                this.villagePopupService
                    .open(VillageDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
