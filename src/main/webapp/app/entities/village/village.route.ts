import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { VillageComponent } from './village.component';
import { VillagePopupComponent } from './village-dialog.component';

@Injectable()
export class VillageResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idGeobou,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const villageRoute: Routes = [
    {
        path: 'village',
        component: VillageComponent,
        resolve: {
            'pagingParams': VillageResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.village.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const villagePopupRoute: Routes = [
    {
        path: 'village-new',
        component: VillagePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.village.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'village/:id/edit',
        component: VillagePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.village.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
