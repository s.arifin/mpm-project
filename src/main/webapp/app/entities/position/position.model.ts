import { BaseEntity } from './../../shared';

export class Position implements BaseEntity {
    constructor(
        public id?: any,
        public idPosition?: any,
        public sequenceNumber?: number,
        public description?: string,
        public userName?: string,
        public positionTypeId?: any,
        public organizationId?: any,
        public internalId?: any,
        public ownerId?: any,
        public internalName?: any,
        public ownerName?: any,
    ) {
    }
}
