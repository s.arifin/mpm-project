import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { Position } from './position.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PositionService {
    protected itemValues: Position[];
    protected selected: Position[];

    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);
    original: BehaviorSubject<any> = new BehaviorSubject<any>(this.selected);

    protected resourceUrl = SERVER_API_URL + 'api/positions';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/positions';

    constructor(protected http: Http) {
        this.itemValues = new Array<Position>();
        this.selected = new Array<Position>();
    }

    create(position: Position): Observable<Position> {
        const copy = this.convert(position);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(position: Position): Observable<Position> {
        const copy = this.convert(position);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<Position> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(position: Position, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(position);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, position: Position): Observable<Position> {
        const copy = this.convert(position);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, positions: Position[]): Observable<Position[]> {
        const copy = this.convertList(positions);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Position.
     */
    protected convertItemFromServer(json: any): Position {
        const entity: Position = Object.assign(new Position(), json);
        return entity;
    }

    /**
     * Convert a Position to a JSON which can be sent to the server.
     */
    protected convert(position: Position): Position {
        if (position === null || position === {}) {
            return {};
        }
        // const copy: Position = Object.assign({}, position);
        const copy: Position = JSON.parse(JSON.stringify(position));
        return copy;
    }

    protected convertList(positions: Position[]): Position[] {
        const copy: Position[] = positions;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: Position[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    pushSelected(data: Position[]) {
        this.selected = data;
        this.original.next(this.selected);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
