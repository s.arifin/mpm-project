import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Position } from './position.model';
import { PositionPopupService } from './position-popup.service';
import { PositionService } from './position.service';
import { ToasterService } from '../../shared';
import { PositionType, PositionTypeService } from '../position-type';
import { Organization, OrganizationService } from '../organization';
import { Internal, InternalService } from '../internal';
import { UserMediator, UserMediatorService } from '../user-mediator';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-position-dialog',
    templateUrl: './position-dialog.component.html'
})
export class PositionDialogComponent implements OnInit {

    position: Position;
    isSaving: boolean;

    positiontypes: PositionType[];

    organizations: Organization[];

    internals: Internal[];

    usermediators: UserMediator[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected positionService: PositionService,
        protected positionTypeService: PositionTypeService,
        protected organizationService: OrganizationService,
        protected internalService: InternalService,
        protected userMediatorService: UserMediatorService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.positionTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.positiontypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.organizationService.query()
            .subscribe((res: ResponseWrapper) => { this.organizations = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.userMediatorService.query()
            .subscribe((res: ResponseWrapper) => { this.usermediators = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.position.idPosition !== undefined) {
            this.subscribeToSaveResponse(
                this.positionService.update(this.position));
        } else {
            this.subscribeToSaveResponse(
                this.positionService.create(this.position));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Position>) {
        result.subscribe((res: Position) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: Position) {
        this.eventManager.broadcast({ name: 'positionListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'position saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'position Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPositionTypeById(index: number, item: PositionType) {
        return item.idPositionType;
    }

    trackOrganizationById(index: number, item: Organization) {
        return item.idParty;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackUserMediatorById(index: number, item: UserMediator) {
        return item.idUserMediator;
    }

    onChangeInternal(event) {
        console.log('xx: ', event);
        this.userMediatorService.query()
        .subscribe((res: ResponseWrapper) => { this.usermediators = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }
}

@Component({
    selector: 'jhi-position-popup',
    template: ''
})
export class PositionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected positionPopupService: PositionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.positionPopupService
                    .open(PositionDialogComponent as Component, params['id']);
            } else {
                this.positionPopupService
                    .open(PositionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
