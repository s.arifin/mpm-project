import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { WorkProductRequirementComponent } from './work-product-requirement.component';
import { WorkProductRequirementOplComponent } from './work-product-requirement-opl.component';
import { WorkProductRequirementDoOplComponent } from './work-product-requirement-do-opl.component';
import { WorkProductRequirementOplApprovalComponent } from './work-product-requirement-opl-approval.component';
import { WorkProductRequirementOoplComponent } from './work-product-requirement-oopl.component';
import { WorkProductRequirementOoplApprovalComponent } from './work-product-requirement-oopl-approval.component';

@Injectable()
export class WorkProductRequirementResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequirement,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const workProductRequirementRoute: Routes = [
    {
        path: 'work-product-requirement',
        component: WorkProductRequirementComponent,
        resolve: {
            'pagingParams': WorkProductRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workProductRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'work-product-requirement-opl',
        component: WorkProductRequirementOplComponent,
        resolve: {
            'pagingParams': WorkProductRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workProductRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'work-product-requirement-do-opl',
        component: WorkProductRequirementDoOplComponent,
        resolve: {
            'pagingParams': WorkProductRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workProductRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'work-product-requirement-opl-approval',
        component: WorkProductRequirementOplApprovalComponent,
        resolve: {
            'pagingParams': WorkProductRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workProductRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'work-product-requirement-oopl',
        component: WorkProductRequirementOoplComponent,
        resolve: {
            'pagingParams': WorkProductRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workProductRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'work-product-requirement-oopl-approval',
        component: WorkProductRequirementOoplApprovalComponent,
        resolve: {
            'pagingParams': WorkProductRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workProductRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const workProductRequirementPopupRoute: Routes = [
];
