import { BaseEntity } from './../../shared';

export class WorkProductRequirement implements BaseEntity {
    constructor(
        public id?: any,
        public idRequirement?: any,
    ) {
    }
}
