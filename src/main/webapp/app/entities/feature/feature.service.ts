import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Feature } from './feature.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class FeatureService {
   protected itemValues: Feature[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/features';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/features';

   constructor(protected http: Http) { }

   create(feature: Feature): Observable<Feature> {
       const copy = this.convert(feature);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(feature: Feature): Observable<Feature> {
       const copy = this.convert(feature);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<Feature> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: Feature, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: Feature[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to Feature.
    */
   protected convertItemFromServer(json: any): Feature {
       const entity: Feature = Object.assign(new Feature(), json);
       return entity;
   }

   /**
    * Convert a Feature to a JSON which can be sent to the server.
    */
   protected convert(feature: Feature): Feature {
       if (feature === null || feature === {}) {
           return {};
       }
       // const copy: Feature = Object.assign({}, feature);
       const copy: Feature = JSON.parse(JSON.stringify(feature));
       return copy;
   }

   protected convertList(features: Feature[]): Feature[] {
       const copy: Feature[] = features;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: Feature[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }

    filterFeatureByTypeId(features: Feature[], typeId: number): Feature[] {
        let rs: Feature[];
        rs = new Array<Feature>();

        if (features.length > 0) {
            for (let i = 0; i < features.length; i++) {
                const item = features[i];
                if (item.featureTypeId === typeId) {
                    rs.push(item);
                }
            }
        }

        return rs;
    }

}
