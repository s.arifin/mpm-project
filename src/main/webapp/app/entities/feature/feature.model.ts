import { BaseEntity } from './../../shared';

export class Feature implements BaseEntity {
    constructor(
        public id?: number,
        public idFeature?: number,
        public description?: string,
        public refKey?: string,
        public featureTypeId?: any,
    ) {
    }
}
