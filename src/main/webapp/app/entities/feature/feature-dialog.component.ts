import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Feature } from './feature.model';
import { FeaturePopupService } from './feature-popup.service';
import { FeatureService } from './feature.service';
import { ToasterService } from '../../shared';
import { FeatureType, FeatureTypeService } from '../feature-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-feature-dialog',
    templateUrl: './feature-dialog.component.html'
})
export class FeatureDialogComponent implements OnInit {

    feature: Feature;
    isSaving: boolean;
    idFeatureType: any;

    featuretypes: FeatureType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected featureService: FeatureService,
        protected featureTypeService: FeatureTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.featureTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.featuretypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.feature.idFeature !== undefined) {
            this.subscribeToSaveResponse(
                this.featureService.update(this.feature));
        } else {
            this.subscribeToSaveResponse(
                this.featureService.create(this.feature));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Feature>) {
        result.subscribe((res: Feature) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: Feature) {
        this.eventManager.broadcast({ name: 'featureListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'feature saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'feature Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackFeatureTypeById(index: number, item: FeatureType) {
        return item.idFeatureType;
    }
}

@Component({
    selector: 'jhi-feature-popup',
    template: ''
})
export class FeaturePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected featurePopupService: FeaturePopupService
    ) {}

    ngOnInit() {
        this.featurePopupService.idFeatureType = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.featurePopupService
                    .open(FeatureDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.featurePopupService.idFeatureType = params['parent'];
                this.featurePopupService
                    .open(FeatureDialogComponent as Component);
            } else {
                this.featurePopupService
                    .open(FeatureDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
