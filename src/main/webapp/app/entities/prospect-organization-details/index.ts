export * from './prospect-organization-details.model';
export * from './prospect-organization-details-popup.service';
export * from './prospect-organization-details.service';
export * from './prospect-organization-details-dialog.component';
export * from './prospect-organization-details.component';
export * from './prospect-organization-details.route';
export * from './prospect-organization-details-edit.component';
