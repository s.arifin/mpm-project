import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { ProspectOrganizationDetails } from './prospect-organization-details.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ProspectOrganizationDetailsService {
    protected itemValues: ProspectOrganizationDetails[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/prospect-organization-details';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/prospect-organization-details';

    constructor(protected http: Http) { }

    create(prospectOrganizationDetails: ProspectOrganizationDetails): Observable<ProspectOrganizationDetails> {
        const copy = this.convert(prospectOrganizationDetails);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(prospectOrganizationDetails: ProspectOrganizationDetails): Observable<ProspectOrganizationDetails> {
        const copy = this.convert(prospectOrganizationDetails);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<ProspectOrganizationDetails> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idProspect', req.idProspect);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, prospectOrganizationDetails: ProspectOrganizationDetails): Observable<ProspectOrganizationDetails> {
        const copy = this.convert(prospectOrganizationDetails);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, prospectOrganizationDetailss: ProspectOrganizationDetails[]): Observable<ProspectOrganizationDetails[]> {
        const copy = this.convertList(prospectOrganizationDetailss);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(prospectOrganizationDetails: ProspectOrganizationDetails): ProspectOrganizationDetails {
        if (prospectOrganizationDetails === null || prospectOrganizationDetails === {}) {
            return {};
        }
        // const copy: ProspectOrganizationDetails = Object.assign({}, prospectOrganizationDetails);
        const copy: ProspectOrganizationDetails = JSON.parse(JSON.stringify(prospectOrganizationDetails));
        return copy;
    }

    protected convertList(prospectOrganizationDetailss: ProspectOrganizationDetails[]): ProspectOrganizationDetails[] {
        const copy: ProspectOrganizationDetails[] = prospectOrganizationDetailss;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ProspectOrganizationDetails[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
