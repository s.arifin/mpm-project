import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {ProspectOrganizationDetails} from './prospect-organization-details.model';
import {ProspectOrganizationDetailsPopupService} from './prospect-organization-details-popup.service';
import {ProspectOrganizationDetailsService} from './prospect-organization-details.service';
import {ToasterService} from '../../shared';
import { Person, PersonService } from '../person';
import { Motor, MotorService } from '../motor';
import { Feature, FeatureService } from '../feature';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-prospect-organization-details-dialog',
    templateUrl: './prospect-organization-details-dialog.component.html'
})
export class ProspectOrganizationDetailsDialogComponent implements OnInit {

    prospectOrganizationDetails: ProspectOrganizationDetails;
    isSaving: boolean;

    people: Person[];

    motors: Motor[];

    features: Feature[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected prospectOrganizationDetailsService: ProspectOrganizationDetailsService,
        protected personService: PersonService,
        protected motorService: MotorService,
        protected featureService: FeatureService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.personService.query()
            .subscribe((res: ResponseWrapper) => { this.people = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query()
            .subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.featureService.query()
            .subscribe((res: ResponseWrapper) => { this.features = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.prospectOrganizationDetails.idProspectOrganizationDetail !== undefined) {
            this.subscribeToSaveResponse(
                this.prospectOrganizationDetailsService.update(this.prospectOrganizationDetails));
        } else {
            this.subscribeToSaveResponse(
                this.prospectOrganizationDetailsService.create(this.prospectOrganizationDetails));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProspectOrganizationDetails>) {
        result.subscribe((res: ProspectOrganizationDetails) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ProspectOrganizationDetails) {
        this.eventManager.broadcast({ name: 'prospectOrganizationDetailsListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'prospectOrganizationDetails saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'prospectOrganizationDetails Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackPersonById(index: number, item: Person) {
        return item.idParty;
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }
}

@Component({
    selector: 'jhi-prospect-organization-details-popup',
    template: ''
})
export class ProspectOrganizationDetailsPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected prospectOrganizationDetailsPopupService: ProspectOrganizationDetailsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.prospectOrganizationDetailsPopupService
                    .open(ProspectOrganizationDetailsDialogComponent as Component, params['id']);
            } else {
                this.prospectOrganizationDetailsPopupService
                    .open(ProspectOrganizationDetailsDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
