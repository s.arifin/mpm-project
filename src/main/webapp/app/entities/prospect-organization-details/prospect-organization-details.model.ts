import { BaseEntity } from './../../shared';
import { Person } from '../person';

export class ProspectOrganizationDetails implements BaseEntity {
    constructor(
        public id?: any,
        public idProspectOrganizationDetail?: any,
        public idProspect?: string,
        public unitPrice?: number,
        public discount?: number,
        public person?: any,
        public productId?: any,
        public colorId?: any,
    ) {
        this.person = new Person();
    }
}
