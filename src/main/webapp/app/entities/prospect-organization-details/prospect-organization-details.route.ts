import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ProspectOrganizationDetailsComponent } from './prospect-organization-details.component';
import { ProspectOrganizationDetailsEditComponent } from './prospect-organization-details-edit.component';
import { ProspectOrganizationDetailsPopupComponent } from './prospect-organization-details-dialog.component';

@Injectable()
export class ProspectOrganizationDetailsResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idProspectOrganizationDetail,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const prospectOrganizationDetailsRoute: Routes = [
    {
        path: 'prospect-organization-details',
        component: ProspectOrganizationDetailsComponent,
        resolve: {
            'pagingParams': ProspectOrganizationDetailsResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganizationDetails.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const prospectOrganizationDetailsPopupRoute: Routes = [
    {
        path: 'prospect-organization-details-new',
        component: ProspectOrganizationDetailsEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganizationDetails.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-organization-details/:id',
        component: ProspectOrganizationDetailsComponent,
        resolve: {
            'pagingParams': ProspectOrganizationDetailsResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganizationDetails.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-organization-details/:id/edit',
        component: ProspectOrganizationDetailsEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganizationDetails.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-organization-details/:id/new',
        component: ProspectOrganizationDetailsEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganizationDetails.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-organization-details-popup-new',
        component: ProspectOrganizationDetailsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganizationDetails.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'prospect-organization-details/:id/popup-edit',
        component: ProspectOrganizationDetailsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganizationDetails.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
