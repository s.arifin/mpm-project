import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProspectOrganizationDetails } from './prospect-organization-details.model';
import { ProspectOrganizationDetailsService } from './prospect-organization-details.service';
import { ToasterService} from '../../shared';
import { Person, PersonService } from '../person';
import { Motor, MotorService } from '../motor';
import { Feature, FeatureService } from '../feature';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-prospect-organization-details-edit',
    templateUrl: './prospect-organization-details-edit.component.html'
})
export class ProspectOrganizationDetailsEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    prospectOrganizationDetails: ProspectOrganizationDetails;
    isSaving: boolean;
    index: number;
    indexAccordion: number;
    people: Person[];
    motors: Motor[];
    features: Feature[];

    constructor(
        protected alertService: JhiAlertService,
        protected prospectOrganizationDetailsService: ProspectOrganizationDetailsService,
        protected personService: PersonService,
        protected motorService: MotorService,
        protected featureService: FeatureService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.prospectOrganizationDetails = new ProspectOrganizationDetails();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.prospectOrganizationDetails.idProspect = params['id'];
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.personService.query()
            .subscribe((res: ResponseWrapper) => { this.people = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query()
            .subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.featureService.query()
            .subscribe((res: ResponseWrapper) => { this.features = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.prospectOrganizationDetailsService.find(id).subscribe((prospectOrganizationDetails) => {
            this.prospectOrganizationDetails = prospectOrganizationDetails;
        });
    }

    previousState() {
        this.router.navigate(['prospect-organization-details/' + this.prospectOrganizationDetails.idProspect ]);
    }

    save() {
        this.isSaving = true;
        if (this.prospectOrganizationDetails.idProspectOrganizationDetail !== undefined) {
            this.subscribeToSaveResponse(
                this.prospectOrganizationDetailsService.update(this.prospectOrganizationDetails));
        } else {
            this.subscribeToSaveResponse(
                this.prospectOrganizationDetailsService.create(this.prospectOrganizationDetails));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProspectOrganizationDetails>) {
        result.subscribe((res: ProspectOrganizationDetails) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ProspectOrganizationDetails) {
        this.eventManager.broadcast({ name: 'prospectOrganizationDetailsListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'prospectOrganizationDetails saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'prospectOrganizationDetails Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackPersonById(index: number, item: Person) {
        return item.idParty;
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }
}
