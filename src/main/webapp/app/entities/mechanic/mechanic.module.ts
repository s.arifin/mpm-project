import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    MechanicService,
    MechanicPopupService,
    MechanicComponent,
    MechanicDetailComponent,
    MechanicDialogComponent,
    MechanicPopupComponent,
    MechanicDeletePopupComponent,
    MechanicDeleteDialogComponent,
    mechanicRoute,
    mechanicPopupRoute,
    MechanicResolvePagingParams,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { CheckboxModule,
         InputTextModule,
         CalendarModule,
         DropdownModule,
         ButtonModule,
         DataTableModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         InputSwitchModule,
         GrowlModule,
         DataGridModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         SelectItem,
         MenuItem,
         Header,
         Footer} from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...mechanicRoute,
    ...mechanicPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        ConfirmDialogModule,
        InputSwitchModule,
        TabViewModule,
        DataGridModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        CurrencyMaskModule
    ],
    exports: [
        MechanicComponent,
        MechanicDetailComponent,
    ],
    declarations: [
        MechanicComponent,
        MechanicDetailComponent,
        MechanicDialogComponent,
        MechanicDeleteDialogComponent,
        MechanicPopupComponent,
        MechanicDeletePopupComponent,
    ],
    entryComponents: [
        MechanicComponent,
        MechanicDialogComponent,
        MechanicPopupComponent,
        MechanicDeleteDialogComponent,
        MechanicDeletePopupComponent,
    ],
    providers: [
        MechanicService,
        MechanicPopupService,
        MechanicResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMechanicModule {}
