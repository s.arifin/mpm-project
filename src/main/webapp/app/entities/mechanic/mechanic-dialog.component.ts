import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiParseLinks, JhiAlertService} from 'ng-jhipster';

import {Mechanic} from './mechanic.model';
import { Vendor, VendorService } from '../vendor';
import {MechanicPopupService} from './mechanic-popup.service';
import {MechanicService} from './mechanic.service';
import {ToasterService, ResponseWrapper, ITEMS_PER_PAGE} from '../../shared';

@Component({
    selector: 'jhi-mechanic-dialog',
    templateUrl: './mechanic-dialog.component.html'
})
export class MechanicDialogComponent implements OnInit {

    mechanic: Mechanic;
    isSaving: boolean;
    vendors: Vendor[];
    selectVendor: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected mechanicService: MechanicService,
        protected parseLinks: JhiParseLinks,
        protected vendorService: VendorService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idVendor';
    }

    ngOnInit() {
        this.isSaving = false;
        this.vendorService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
       this.isSaving = true;
       console.log('this.mechanic', this.mechanic);

       if (this.mechanic.idPartyRole !== undefined) {
            if (this.mechanic.external === true && this.selectVendor != null) {
                this.mechanic.idVendor = this.selectVendor.idVendor;
            }
            this.subscribeToSaveResponse(
                this.mechanicService.update(this.mechanic));
        } else {
            if (this.mechanic.external === true && this.selectVendor != null) {
                this.mechanic.idVendor = this.selectVendor.idVendor;
            }
            this.subscribeToSaveResponse(
                this.mechanicService.create(this.mechanic));
        }
    }

    status() {
        if (this.mechanic.external === false) {
            this.mechanic.idVendor = '';
            this.selectVendor = null;
        }
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idPartyRole') {
            result.push('idPartyRole');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.vendors = data;
    }

    protected subscribeToSaveResponse(result: Observable<Mechanic>) {
        result.subscribe((res: Mechanic) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Mechanic) {
        this.eventManager.broadcast({ name: 'mechanicListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'mechanic saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'mechanic Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

}

@Component({
    selector: 'jhi-mechanic-popup',
    template: ''
})
export class MechanicPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected mechanicPopupService: MechanicPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.mechanicPopupService
                    .open(MechanicDialogComponent as Component, params['id']);
            } else {
                this.mechanicPopupService
                    .open(MechanicDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
