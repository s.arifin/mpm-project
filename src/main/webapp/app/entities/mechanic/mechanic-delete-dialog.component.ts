import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {Mechanic} from './mechanic.model';
import {MechanicPopupService} from './mechanic-popup.service';
import {MechanicService} from './mechanic.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-mechanic-delete-dialog',
    templateUrl: './mechanic-delete-dialog.component.html'
})
export class MechanicDeleteDialogComponent {

    mechanic: Mechanic;

    constructor(
        protected mechanicService: MechanicService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id?: any) {
        this.mechanicService.delete(id).subscribe((response) => {
            this.toaster.showToaster('info', 'Data Deleted', 'Data Mechanic di hapus !');
            this.eventManager.broadcast({
                name: 'mechanicListModification',
                content: 'Deleted an mechanic'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-mechanic-delete-popup',
    template: ''
})
export class MechanicDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected mechanicPopupService: MechanicPopupService,
        protected toaster: ToasterService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.mechanicPopupService
                .open(MechanicDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
