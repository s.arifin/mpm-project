import { BaseEntity } from '../shared-component';
import { Person } from '../person'

export class Mechanic implements BaseEntity {
    constructor(
        public id?: any,
        public idPartyRole?: any,
        public idMechanic?: string,
        public person?: any,
        public external?: boolean,
        public idVendor?: string
    ) {
        this.person = new Person();
    }
}
