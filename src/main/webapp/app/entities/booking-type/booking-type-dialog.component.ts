import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {BookingType} from './booking-type.model';
import {BookingTypePopupService} from './booking-type-popup.service';
import {BookingTypeService} from './booking-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-booking-type-dialog',
    templateUrl: './booking-type-dialog.component.html'
})
export class BookingTypeDialogComponent implements OnInit {

    bookingType: BookingType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected bookingTypeService: BookingTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.bookingType.idBookingType !== undefined) {
            this.subscribeToSaveResponse(
                this.bookingTypeService.update(this.bookingType));
        } else {
            this.subscribeToSaveResponse(
                this.bookingTypeService.create(this.bookingType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<BookingType>) {
        result.subscribe((res: BookingType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: BookingType) {
        this.eventManager.broadcast({ name: 'bookingTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'bookingType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'bookingType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-booking-type-popup',
    template: ''
})
export class BookingTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected bookingTypePopupService: BookingTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.bookingTypePopupService
                    .open(BookingTypeDialogComponent as Component, params['id']);
            } else {
                this.bookingTypePopupService
                    .open(BookingTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
