import { BaseEntity } from './../../shared';

export class BookingType implements BaseEntity {
    constructor(
        public id?: number,
        public idBookingType?: number,
        public description?: string,
    ) {
    }
}
