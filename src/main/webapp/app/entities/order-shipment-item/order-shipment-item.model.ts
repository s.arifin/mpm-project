import { BaseEntity } from './../../shared';

export class OrderShipmentItem implements BaseEntity {
    constructor(
        public id?: any,
        public idOrderShipmentItem?: any,
        public qty?: number,
        public orderItemId?: any,
        public shipmentItemId?: any,
    ) {
    }
}
