import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { OrderShipmentItem } from './order-shipment-item.model';
import { OrderShipmentItemService } from './order-shipment-item.service';

@Injectable()
export class OrderShipmentItemPopupService {
    protected ngbModalRef: NgbModalRef;
    idOrderItem: any;
    idShipmentItem: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected orderShipmentItemService: OrderShipmentItemService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.orderShipmentItemService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.orderShipmentItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new OrderShipmentItem();
                    data.orderItemId = this.idOrderItem;
                    data.shipmentItemId = this.idShipmentItem;
                    this.ngbModalRef = this.orderShipmentItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    orderShipmentItemModalRef(component: Component, orderShipmentItem: OrderShipmentItem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.orderShipmentItem = orderShipmentItem;
        modalRef.componentInstance.idOrderItem = this.idOrderItem;
        modalRef.componentInstance.idShipmentItem = this.idShipmentItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.orderShipmentItemLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    orderShipmentItemLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
