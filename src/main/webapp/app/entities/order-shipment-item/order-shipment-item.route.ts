import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { OrderShipmentItemComponent } from './order-shipment-item.component';
import { OrderShipmentItemLovPopupComponent } from './order-shipment-item-as-lov.component';
import { OrderShipmentItemPopupComponent } from './order-shipment-item-dialog.component';

@Injectable()
export class OrderShipmentItemResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idOrderShipmentItem,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const orderShipmentItemRoute: Routes = [
    {
        path: 'order-shipment-item',
        component: OrderShipmentItemComponent,
        resolve: {
            'pagingParams': OrderShipmentItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderShipmentItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const orderShipmentItemPopupRoute: Routes = [
    {
        path: 'order-shipment-item-lov',
        component: OrderShipmentItemLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderShipmentItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'order-shipment-item-popup-new-list/:parent',
        component: OrderShipmentItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderShipmentItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'order-shipment-item-new',
        component: OrderShipmentItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderShipmentItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'order-shipment-item/:id/edit',
        component: OrderShipmentItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderShipmentItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
