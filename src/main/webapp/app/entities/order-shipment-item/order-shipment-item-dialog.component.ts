import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { OrderShipmentItem } from './order-shipment-item.model';
import { OrderShipmentItemPopupService } from './order-shipment-item-popup.service';
import { OrderShipmentItemService } from './order-shipment-item.service';
import { ToasterService } from '../../shared';
import { OrderItem, OrderItemService } from '../order-item';
import { ShipmentItem, ShipmentItemService } from '../shipment-item';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-order-shipment-item-dialog',
    templateUrl: './order-shipment-item-dialog.component.html'
})
export class OrderShipmentItemDialogComponent implements OnInit {

    orderShipmentItem: OrderShipmentItem;
    isSaving: boolean;
    idOrderItem: any;
    idShipmentItem: any;

    orderitems: OrderItem[];

    shipmentitems: ShipmentItem[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected orderShipmentItemService: OrderShipmentItemService,
        protected orderItemService: OrderItemService,
        protected shipmentItemService: ShipmentItemService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.orderItemService.query()
            .subscribe((res: ResponseWrapper) => { this.orderitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipmentItemService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmentitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.orderShipmentItem.idOrderShipmentItem !== undefined) {
            this.subscribeToSaveResponse(
                this.orderShipmentItemService.update(this.orderShipmentItem));
        } else {
            this.subscribeToSaveResponse(
                this.orderShipmentItemService.create(this.orderShipmentItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<OrderShipmentItem>) {
        result.subscribe((res: OrderShipmentItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: OrderShipmentItem) {
        this.eventManager.broadcast({ name: 'orderShipmentItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'orderShipmentItem saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'orderShipmentItem Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackOrderItemById(index: number, item: OrderItem) {
        return item.idOrderItem;
    }

    trackShipmentItemById(index: number, item: ShipmentItem) {
        return item.idShipmentItem;
    }
}

@Component({
    selector: 'jhi-order-shipment-item-popup',
    template: ''
})
export class OrderShipmentItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected orderShipmentItemPopupService: OrderShipmentItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.orderShipmentItemPopupService
                    .open(OrderShipmentItemDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.orderShipmentItemPopupService.parent = params['parent'];
                this.orderShipmentItemPopupService
                    .open(OrderShipmentItemDialogComponent as Component);
            } else {
                this.orderShipmentItemPopupService
                    .open(OrderShipmentItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
