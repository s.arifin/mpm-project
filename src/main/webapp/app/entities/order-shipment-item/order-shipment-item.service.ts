import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { OrderShipmentItem } from './order-shipment-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class OrderShipmentItemService {
    protected itemValues: OrderShipmentItem[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/order-shipment-items';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/order-shipment-items';

    constructor(protected http: Http) { }

    create(orderShipmentItem: OrderShipmentItem): Observable<OrderShipmentItem> {
        const copy = this.convert(orderShipmentItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(orderShipmentItem: OrderShipmentItem): Observable<OrderShipmentItem> {
        const copy = this.convert(orderShipmentItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<OrderShipmentItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<OrderShipmentItem> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<OrderShipmentItem[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to OrderShipmentItem.
     */
    protected convertItemFromServer(json: any): OrderShipmentItem {
        const entity: OrderShipmentItem = Object.assign(new OrderShipmentItem(), json);
        return entity;
    }

    /**
     * Convert a OrderShipmentItem to a JSON which can be sent to the server.
     */
    protected convert(orderShipmentItem: OrderShipmentItem): OrderShipmentItem {
        if (orderShipmentItem === null || orderShipmentItem === {}) {
            return {};
        }
        // const copy: OrderShipmentItem = Object.assign({}, orderShipmentItem);
        const copy: OrderShipmentItem = JSON.parse(JSON.stringify(orderShipmentItem));
        return copy;
    }

    protected convertList(orderShipmentItems: OrderShipmentItem[]): OrderShipmentItem[] {
        const copy: OrderShipmentItem[] = orderShipmentItems;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: OrderShipmentItem[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
