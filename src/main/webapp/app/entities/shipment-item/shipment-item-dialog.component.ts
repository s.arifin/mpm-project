import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ShipmentItem } from './shipment-item.model';
import { ShipmentItemPopupService } from './shipment-item-popup.service';
import { ShipmentItemService } from './shipment-item.service';
import { ToasterService } from '../../shared';
import { Shipment, ShipmentService } from '../shipment';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-shipment-item-dialog',
    templateUrl: './shipment-item-dialog.component.html'
})
export class ShipmentItemDialogComponent implements OnInit {

    shipmentItem: ShipmentItem;
    isSaving: boolean;
    idShipment: any;

    shipments: Shipment[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected shipmentItemService: ShipmentItemService,
        protected shipmentService: ShipmentService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.shipmentService.query()
            .subscribe((res: ResponseWrapper) => { this.shipments = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.shipmentItem.idShipmentItem !== undefined) {
            this.subscribeToSaveResponse(
                this.shipmentItemService.update(this.shipmentItem));
        } else {
            this.subscribeToSaveResponse(
                this.shipmentItemService.create(this.shipmentItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ShipmentItem>) {
        result.subscribe((res: ShipmentItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ShipmentItem) {
        this.eventManager.broadcast({ name: 'shipmentItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'shipmentItem saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'shipmentItem Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackShipmentById(index: number, item: Shipment) {
        return item.idShipment;
    }
}

@Component({
    selector: 'jhi-shipment-item-popup',
    template: ''
})
export class ShipmentItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected shipmentItemPopupService: ShipmentItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.shipmentItemPopupService
                    .open(ShipmentItemDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.shipmentItemPopupService.idShipment = params['parent'];
                this.shipmentItemPopupService
                    .open(ShipmentItemDialogComponent as Component);
            } else {
                this.shipmentItemPopupService
                    .open(ShipmentItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
