import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ShipmentItem } from './shipment-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ShipmentItemService {
    protected itemValues: ShipmentItem[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/shipment-items';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/shipment-items';

    constructor(protected http: Http) { }

    create(shipmentItem: ShipmentItem): Observable<ShipmentItem> {
        const copy = this.convert(shipmentItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(shipmentItem: ShipmentItem): Observable<ShipmentItem> {
        const copy = this.convert(shipmentItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<ShipmentItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryCustomFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '-spg' + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<ShipmentItem> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<ShipmentItem[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ShipmentItem.
     */
    protected convertItemFromServer(json: any): ShipmentItem {
        const entity: ShipmentItem = Object.assign(new ShipmentItem(), json);
        return entity;
    }

    /**
     * Convert a ShipmentItem to a JSON which can be sent to the server.
     */
    protected convert(shipmentItem: ShipmentItem): ShipmentItem {
        if (shipmentItem === null || shipmentItem === {}) {
            return {};
        }
        // const copy: ShipmentItem = Object.assign({}, shipmentItem);
        const copy: ShipmentItem = JSON.parse(JSON.stringify(shipmentItem));
        return copy;
    }

    protected convertList(shipmentItems: ShipmentItem[]): ShipmentItem[] {
        const copy: ShipmentItem[] = shipmentItems;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ShipmentItem[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
