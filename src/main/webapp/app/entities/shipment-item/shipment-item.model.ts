import { BaseEntity } from './../../shared';

export class ShipmentItem implements BaseEntity {
    constructor(
        public id?: any,
        public idShipmentItem?: any,
        public idProduct?: string,
        public idFrame?: string,
        public idMachine?: string,
        public idFeature?: number,
        public featureRefKey?: number,
        public featureDescriptionription?: number,
        public itemDescription?: string,
        public qty?: number,
        public contentDescription?: string,
        public shipmentId?: any,
        public billingItems?: BaseEntity[],
    ) {
    }
}

export class CustomShipmentItem {
    constructor(
        public idShipmentItem?: string,
        public idProduct?: string,
        public descriptionProduct?: string,
        public idFeature?: number,
        public featureRefKey?: number,
        public featureDescriptionription?: number,
        public descriptionFeature?: string,
        public qty?: number,
        public idShipment?: string,
        public idReceipt?: string,
        public idFrame?: string,
        public idMachine?: string,
        public yearAssembly?: number,
        public idShipmentPackage?: string
    ) {
    }
}
