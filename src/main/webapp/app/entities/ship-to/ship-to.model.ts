import { BaseEntity } from './../../shared';

export class ShipTo implements BaseEntity {
    constructor(
        public id?: number,
        public idShipTo?: number,
        public idRoleType?: number,
        public partyId?: any,
    ) {
    }
}
