import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ShipTo } from './ship-to.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ShipToService {
   protected itemValues: ShipTo[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/ship-tos';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/ship-tos';

   constructor(protected http: Http) { }

   create(shipTo: ShipTo): Observable<ShipTo> {
       const copy = this.convert(shipTo);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(shipTo: ShipTo): Observable<ShipTo> {
       const copy = this.convert(shipTo);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<ShipTo> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: ShipTo, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: ShipTo[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to ShipTo.
    */
   protected convertItemFromServer(json: any): ShipTo {
       const entity: ShipTo = Object.assign(new ShipTo(), json);
       return entity;
   }

   /**
    * Convert a ShipTo to a JSON which can be sent to the server.
    */
   protected convert(shipTo: ShipTo): ShipTo {
       if (shipTo === null || shipTo === {}) {
           return {};
       }
       // const copy: ShipTo = Object.assign({}, shipTo);
       const copy: ShipTo = JSON.parse(JSON.stringify(shipTo));
       return copy;
   }

   protected convertList(shipTos: ShipTo[]): ShipTo[] {
       const copy: ShipTo[] = shipTos;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: ShipTo[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
