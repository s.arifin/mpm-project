import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {MovingSlip} from './moving-slip.model';
import {MovingSlipPopupService} from './moving-slip-popup.service';
import {MovingSlipService} from './moving-slip.service';
import {ToasterService} from '../../shared';
import { InventoryItem, InventoryItemService } from '../inventory-item';
import { Container, ContainerService } from '../container';
import { Facility, FacilityService } from '../facility';
import { Product, ProductService } from '../product';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-moving-slip-dialog',
    templateUrl: './moving-slip-dialog.component.html'
})
export class MovingSlipDialogComponent implements OnInit {

    movingSlip: MovingSlip;
    isSaving: boolean;

    inventoryitems: InventoryItem[];

    containers: Container[];

    facilities: Facility[];

    products: Product[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected movingSlipService: MovingSlipService,
        protected inventoryItemService: InventoryItemService,
        protected containerService: ContainerService,
        protected facilityService: FacilityService,
        protected productService: ProductService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.inventoryItemService.query()
            .subscribe((res: ResponseWrapper) => { this.inventoryitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.containerService.query()
            .subscribe((res: ResponseWrapper) => { this.containers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.movingSlip.idSlip !== undefined) {
            this.subscribeToSaveResponse(
                this.movingSlipService.update(this.movingSlip));
        } else {
            this.subscribeToSaveResponse(
                this.movingSlipService.create(this.movingSlip));
        }
    }

    protected subscribeToSaveResponse(result: Observable<MovingSlip>) {
        result.subscribe((res: MovingSlip) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: MovingSlip) {
        this.eventManager.broadcast({ name: 'movingSlipListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'movingSlip saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'movingSlip Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInventoryItemById(index: number, item: InventoryItem) {
        return item.idInventoryItem;
    }

    trackContainerById(index: number, item: Container) {
        return item.idContainer;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackProductById(index: number, item: Product) {
        return item.idProduct;
    }
}

@Component({
    selector: 'jhi-moving-slip-popup',
    template: ''
})
export class MovingSlipPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected movingSlipPopupService: MovingSlipPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.movingSlipPopupService
                    .open(MovingSlipDialogComponent as Component, params['id']);
            } else {
                this.movingSlipPopupService
                    .open(MovingSlipDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
