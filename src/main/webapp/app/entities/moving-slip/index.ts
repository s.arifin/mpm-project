export * from './moving-slip.model';
export * from './moving-slip-popup.service';
export * from './moving-slip.service';
export * from './moving-slip-dialog.component';
export * from './moving-slip.component';
export * from './moving-slip.route';
export * from './moving-slip-add.component';
