import { BaseEntity } from './../../shared';
import { InventoryMovement } from '../inventory-movement/inventory-movement.model';

export class MovingSlip extends InventoryMovement {
    constructor(
        public id?: any,
        public idSlip?: any,
        public qty?: number,
        public inventoryItemToId?: any,
        public containerFromId?: any,
        public containerToId?: any,
        public facilityFromId?: any,
        public facilityToId?: any,
        public productId?: any,
        public inventoryItemId?: any,
        public qtyAwal?: number,
        public idFrame?: string,
        public idMachine?: string,
        public requestId?: string
    ) {
        super();
    }
}
