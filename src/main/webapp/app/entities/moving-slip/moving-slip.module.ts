import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    MovingSlipService,
    MovingSlipPopupService,
    MovingSlipComponent,
    MovingSlipDialogComponent,
    MovingSlipPopupComponent,
    movingSlipRoute,
    movingSlipPopupRoute,
    MovingSlipResolvePagingParams,
    MovingSlipAddComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule
        } from 'primeng/primeng';

import { FieldsetModule } from 'primeng/primeng';
import { DataTableModule } from 'primeng/primeng';
import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...movingSlipRoute,
    ...movingSlipPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        MpmSharedEntityModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule
    ],
    exports: [
        MovingSlipComponent,
        MovingSlipAddComponent
    ],
    declarations: [
        MovingSlipComponent,
        MovingSlipDialogComponent,
        MovingSlipPopupComponent,
        MovingSlipAddComponent
    ],
    entryComponents: [
        MovingSlipComponent,
        MovingSlipDialogComponent,
        MovingSlipPopupComponent,
        MovingSlipAddComponent
    ],
    providers: [
        MovingSlipService,
        MovingSlipPopupService,
        MovingSlipResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMovingSlipModule {}
