import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MovingSlipComponent } from './moving-slip.component';
import { MovingSlipAddComponent } from './moving-slip-add.component';
import { MovingSlipPopupComponent } from './moving-slip-dialog.component';

@Injectable()
export class MovingSlipResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idSlip,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const movingSlipRoute: Routes = [
    {
        path: 'moving-slip',
        component: MovingSlipComponent,
        resolve: {
            'pagingParams': MovingSlipResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.movingSlip.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const movingSlipPopupRoute: Routes = [
    {
        path: 'moving-slip-new',
        component: MovingSlipAddComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.movingSlip.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    // {
    //     path: 'moving-slip/:id/edit',
    //     component: MovingSlipEditComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.movingSlip.home.title'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
    {
        path: 'moving-slip-popup-new',
        component: MovingSlipPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.movingSlip.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'moving-slip/:id/popup-edit',
        component: MovingSlipPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.movingSlip.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
