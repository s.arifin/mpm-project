import { Component, OnInit, OnDestroy, DoCheck } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { MovingSlip, MovingSlipService } from './';
import { ToasterService} from '../../shared';
import { Container, ContainerService } from '../container';
import { Facility, FacilityService } from '../facility';
import { Product, ProductService } from '../product';
import { ResponseWrapper, ITEMS_PER_PAGE } from '../../shared';
import { InventoryItem, InventoryItemService } from '../inventory-item';
import { RequestProduct, RequestProductService } from '../request-product';
import { LazyLoadEvent, DataTable } from 'primeng/primeng';
import { BULK_SAVE } from '../../shared/constants/moving-slip.constants';
import { CommonUtilService, ErrorBody } from '../../shared/utils/common.service';
import * as _ from 'lodash';
import { LoadingService } from '../../layouts/index';
import { FacilityType, FacilityTypeService } from '../facility-type/index';

@Component({
    selector: 'jhi-moving-slip-add',
    templateUrl: './moving-slip-add.component.html'
})
export class MovingSlipAddComponent implements OnInit, OnDestroy {
    protected subscription: Subscription;
    protected subscriptionInventoryItem: Subscription;
    protected subscriptionRequest: Subscription;

    public selectRequest: string;
    public isShowDetailRequest: boolean;
    public selectFacilityTypeTo: number;
    public selectFacilityTypeFrom: number;
    public selectContainerTo: string;
    public selectFacilityTo: string;
    public selectProduct: string;
    public selectFacilityFrom: string;
    public selectContainerFrom: string;
    public inventoryItems: Array<InventoryItem>;
    public totalRecords: number;
    public dataMovingSlipCustomSources: Array<MovingSlip> = new Array<MovingSlip>();
    public itemsPerPage: number;
    public movingSlips: Array<MovingSlip>;
    public containersTo: Array<Container>;
    public containersFrom: Array<Container>;
    public facilityTypesTo: Array<FacilityType>;
    public facilityTypesFrom: Array<FacilityType>;
    public facilitiesTo: Array<Facility>;
    public facilitiesFrom: Array<Facility>;
    public products: Array<Product>;

    constructor(
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected alertService: JhiAlertService,
        protected facilityTypeService: FacilityTypeService,
        protected movingSlipService: MovingSlipService,
        protected containerService: ContainerService,
        protected facilityService: FacilityService,
        protected productService: ProductService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected inventoryItemService: InventoryItemService,
        protected requestProductService: RequestProductService
    ) {
        this.totalRecords = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
    }

    ngOnInit() {
        this.clearData();
        this.getChosenRequest();
        this.registerSelectInventoryItem();
        this.facilityTypeService.query()
            .subscribe((res: ResponseWrapper) => {
                this.facilityTypesTo = res.json;
                this.facilityTypesFrom = res.json;
            }, (err: ResponseWrapper) => this.onError(err.json));
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscriptionInventoryItem.unsubscribe();
        this.subscriptionRequest.unsubscribe();
    }

    loadFacilityContainer(): void {
        if (this.selectFacilityTypeTo !== null) {
            this.getFacilityByType('to');
            if (this.selectFacilityTo !== null) {
                this.getContainerByFacilityId('to');
            }
        }

        if (this.selectFacilityTypeFrom !== null) {
            this.getFacilityByType('from');
            if (this.selectFacilityFrom !== null) {
                this.getContainerByFacilityId('from');
            }
        }
    }

    public getFacilityByType(target: string): void {
        if (target === 'to') {
            if (this.selectFacilityTypeTo !== null) {
                this.facilityService.queryFilterBy({ idFacilityType: this.selectFacilityTypeTo }).subscribe(
                    (res) => {
                        this.facilitiesTo = res.json;
                    },
                    (err) => {
                        this.commonUtilService.showError(err);
                    }
                )
            }
        } else if (target === 'from') {
            if (this.selectFacilityTypeFrom !== null) {
                this.facilityService.queryFilterBy({ idFacilityType: this.selectFacilityTypeFrom }).subscribe(
                    (res) => {
                        this.facilitiesFrom = res.json;
                    },
                    (err) => {
                        this.commonUtilService.showError(err);
                    }
                )
            }
        }
    }

    public getContainerByFacilityId(target: string): void {
        if (target === 'from') {
            this.containerService.getContainerByFacilityId(this.selectFacilityFrom).subscribe(
                (res) => {
                    this.selectContainerFrom = null;
                    this.containersFrom = res.json;
                }
            )
        } else if (target === 'to') {
            this.containerService.getContainerByFacilityId(this.selectFacilityTo).subscribe(
                (res) => {
                    this.mappingContainerAndFacility('facilityTo');
                    this.selectContainerTo = null;
                    this.containersTo = res.json;
                }
            )
        }
    }

    public selectedContainerTo(): void {
        this.mappingContainerAndFacility('containerTo');
    }

    protected mappingContainerAndFacility(target: string): void {
        if (this.dataMovingSlipCustomSources.length > 0) {
            for (let i = 0; i < this.dataMovingSlipCustomSources.length; i++) {
                const item: MovingSlip = this.dataMovingSlipCustomSources[i];
                if (target === 'facilityTo') {
                    item.facilityToId = this.selectFacilityTo;
                } else if (target === 'containerTo') {
                    item.containerToId = this.selectContainerTo;
                }
            }
        }
    }

    protected convertInventoryItemToMovingSlip(data: Array<InventoryItem>): Array<MovingSlip> {
        this.dataMovingSlipCustomSources = new Array<MovingSlip>();
        const containerTo: string = this.selectContainerTo;
        const facilityTo: string = this.selectFacilityTo;
        if (data.length > 0) {
            this.dataMovingSlipCustomSources = _.map(data, function(e: InventoryItem) {
                let a: MovingSlip;
                a = new MovingSlip;

                a.idFrame = e.idFrame;
                a.idMachine = e.idMachine;
                a.containerFromId = e.idContainer;
                a.containerToId = containerTo;
                a.facilityFromId = e.idFacility;
                a.facilityToId = facilityTo;
                a.qty = 0; // digunakan untuk split barang yang akan di moving
                a.qtyAwal = e.qty - e.qtyBooking; // digunakan untuk informasi qty yang exist saat ini di container
                a.productId = e.idProduct;
                a.inventoryItemId = e.idInventoryItem;

                return a;
            });
        }
        return this.dataMovingSlipCustomSources;
    }

    protected registerSelectInventoryItem(): void {
        this.subscriptionInventoryItem = this.inventoryItemService.values.subscribe(
            (res) => {
                if (res !== undefined) {
                    this.dataMovingSlipCustomSources = this.convertInventoryItemToMovingSlip(res);
                    this.loadSelectedInventoryItems();
                }
            }
        )
    }

    protected getChosenRequest(): void {
        this.subscriptionRequest = this.requestProductService.values.subscribe(
            (res) => {
                if (res !== undefined) {
                    this.selectRequest = res.idRequest;
                    this.selectFacilityTypeTo = res.facilityTypeToId;
                    this.selectFacilityTo = res.facilityToId;
                    this.selectFacilityTypeFrom = res.facilityTypeFromId;
                    this.selectFacilityFrom = res.facilityFromId;
                    this.isShowDetailRequest = true;
                    this.loadFacilityContainer();
                    // this.dataMovingSlipCustomSources = this.convertInventoryItemToMovingSlip(res);
                    // this.loadSelectedInventoryItems();
                }
            }
        )
    }

    protected loadSelectedInventoryItems(): void {
        this.totalRecords = this.dataMovingSlipCustomSources.length;
        this.movingSlips = this.dataMovingSlipCustomSources.slice(0, this.itemsPerPage);
    }

    public loadMovingSlipsCustom(event: LazyLoadEvent): void {
        setTimeout(() => {
            if (this.dataMovingSlipCustomSources) {
                this.movingSlips = new Array<MovingSlip>();
                this.movingSlips = this.dataMovingSlipCustomSources.slice(event.first, (event.first + event.rows));
            }
        }, 250);
    }

    public previousState(): void {
        this.router.navigate(['moving-slip']);
    }

    public save(): void {
        this.loadingService.loadingStart();
        this.movingSlipService.executeListProcess(BULK_SAVE, null, this.dataMovingSlipCustomSources).subscribe(
            (res) => {
                this.previousState();
            },
            (err) => {
                this.loadingService.loadingStop();
                this.commonUtilService.showError(err);
            }
        )
    }

    protected clearData(): void {
        this.dataMovingSlipCustomSources = new Array<MovingSlip>();
        this.movingSlips = new Array<MovingSlip>();
        this.selectRequest = '';
        this.isShowDetailRequest = false;
        this.selectFacilityTypeTo = null;
        this.selectFacilityTypeFrom = null;
        this.selectFacilityFrom = null;
        this.selectContainerFrom = null;
        this.selectProduct = null;
        this.selectContainerTo = null;
        this.selectFacilityTo = null;
    }

    protected subscribeToSaveResponse(result: Observable<MovingSlip>) {
        result.subscribe((res: MovingSlip) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: MovingSlip) {
        this.eventManager.broadcast({ name: 'movingSlipListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'movingSlip saved !');
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'movingSlip Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    public selectRequirementForSelectInventoryToMove(): boolean {
        let rs: boolean;
        rs = true;

        if (this.selectContainerFrom !== null && this.selectFacilityFrom !== null) {
            rs = false;
        }

        return rs;
    }

    trackContainerById(index: number, item: Container) {
        return item.idContainer;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackProductById(index: number, item: Product) {
        return item.idProduct;
    }

    public trackFacilityTypeById(index: number, item: FacilityType) {
        return item.idFacilityType;
    }
}
