import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { MovingSlip } from './moving-slip.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class MovingSlipService {
    protected itemValues: MovingSlip[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/moving-slips';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/moving-slips';

    constructor(protected http: Http) { }

    create(movingSlip: MovingSlip): Observable<MovingSlip> {
        const copy = this.convert(movingSlip);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(movingSlip: MovingSlip): Observable<MovingSlip> {
        const copy = this.convert(movingSlip);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<MovingSlip> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(movingSlip: MovingSlip, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(movingSlip);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, movingSlip: MovingSlip): Observable<MovingSlip> {
        const copy = this.convert(movingSlip);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, movingSlips: MovingSlip[]): Observable<MovingSlip[]> {
        const copy = this.convertList(movingSlips);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(movingSlip: MovingSlip): MovingSlip {
        if (movingSlip === null || movingSlip === {}) {
            return {};
        }
        // const copy: MovingSlip = Object.assign({}, movingSlip);
        const copy: MovingSlip = JSON.parse(JSON.stringify(movingSlip));
        return copy;
    }

    protected convertList(movingSlips: MovingSlip[]): MovingSlip[] {
        const copy: MovingSlip[] = movingSlips;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: MovingSlip[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    public findByIdFacilityType(id: number): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/find-by-facility-type/' + id).map(
            (res: Response) => this.convertResponse(res)
        )
    }
}
