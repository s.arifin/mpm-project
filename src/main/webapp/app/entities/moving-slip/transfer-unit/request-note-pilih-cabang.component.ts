import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, Data } from '@angular/router';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { LazyLoadEvent } from 'primeng/primeng';
import { ConfirmationService } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { SelectItem } from 'primeng/primeng';

// nuse
import { DatePipe } from '@angular/common';
import { constants } from 'os';
import { VehicleSalesOrder, VehicleSalesOrderService } from '../../vehicle-sales-order';
import { OrdersParameter } from '../../orders';
import { Salesman, SalesmanService } from '../../salesman';
import { AxPosting, AxPostingService } from '../../axposting';
import { AxPostingLine } from '../../axposting-line';
import { Principal, ToasterService, ITEMS_PER_PAGE, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { LeasingCompanyService, LeasingCompany } from '../../leasing-company';
import { LoadingService } from '../../../layouts';
import { RuleHotItemService } from '../../rule-hot-item';
import * as BaseConstant from '../../../shared/constants/base.constants';
import { UnitDocumentMessage, UnitDocumentMessageService, CustomUnitModel, CustomTemp } from '../../unit-document-message';
import { Internal } from '../../internal/internal.model';
import { InternalService } from '../../internal/internal.service';
import { TransferUnitRequestComponent } from './transfer-unit-request-note.component';
import { forEach } from '@angular/router/src/utils/collection';
import { ReportUtilService } from '../../../shared/report/report-util.service';

@Component({
    selector: 'jhi-request-note-pilih-cabang',
    templateUrl: './request-note-pilih-cabang.component.html'
})
export class RequestNoteComponent implements OnInit {

    customTemp: CustomTemp;
    customUnitModel: CustomUnitModel[];
    eachCustomUnitModel: CustomUnitModel;
    unitDocumentMessages: UnitDocumentMessage[];
    currentAccount: any;
    vehicleSalesOrders: VehicleSalesOrder[];
    ordersParameter: OrdersParameter;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    currentSearching: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    tgl1: Date;
    tgl2: Date;
    internals: Internal[];
    internal1: Internal;
    internal2: Internal;
    internal3: Internal;
    // tglAwal:string;
    // tglAkhir:string;
    leasings: SelectItem[];
    selectedLeasing: string;
    // korsals: SelectItem[];
    korsals: Salesman[];
    salesmans: Salesman[];
    selectedKorsal: string;
    selectedSalesman: string;
    statusVsoes: SelectItem[];
    selectedStatusVso: Number;
    isLazyLoadingFirst: Boolean ;
    isMatchingMenu: Boolean ;
    isFiltered: Boolean;
    // ismatching

    // nuse
    axPosting: AxPosting;
    axPostingLines: AxPostingLine[];
    axPostingLine: AxPostingLine;

    constructor(
        protected reportUtilService: ReportUtilService,
        protected internalService: InternalService,
        private unitDocumentMesageService: UnitDocumentMessageService,
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private leasingCompanyService: LeasingCompanyService,
        private loadingService: LoadingService,
        private ruleHotItemService: RuleHotItemService,
        private toaster: ToasterService,
        private salesmanService: SalesmanService,
        // nuse
        private axPostingService: AxPostingService,
        private datePipe: DatePipe
    ) {
        this.customUnitModel = new Array<CustomUnitModel>();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        // this.currentSearching = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';

        // this.leasings = [];

        // this.statusVsoes = [];
        // this.statusVsoes.push({label: 'OPEN', value: 10});
        // this.statusVsoes.push({label: 'COMPLETED', value: 17 });
        // this.statusVsoes.push({label: 'APPROVE', value: 61 });
        // this.statusVsoes.push({label: 'MATCHING', value: 74 });
        // this.statusVsoes.push({label: 'CANCELED', value: 13 });
        // this.statusVsoes.push({label: 'WAITING ADMIN HANDLING', value: 88 });

        // this.tgl1 = new Date();
        // this.tgl2 = new Date();

        // this.ordersParameter = new OrdersParameter();
        // this.korsals = [];
        // this.salesmans = [];
        // this.isFiltered = false;
        // this.isMatchingMenu = false;
        // this.customUnitModel = new Array <CustomUnitModel>();
    }

    // loadLeasing() {
    //     this.leasingCompanyService.query({
    //         page: 0,
    //         size: 100}).subscribe(
    //         (res: ResponseWrapper) => this.leasings = res.json,
    //         (res: ResponseWrapper) => this.onError(res.json)
    //     );
    // }

    loadAll() {

    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/unit-document-message'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
        console.log('is filter = ', this.isFiltered );
    }

    printSave() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            data: this.unitDocumentMessages
        }
        this.principal.getUserLogin();
        console.log('ini createrequest', this.unitDocumentMessages);
        console.log('ini createrequest', this.principal.getUserLogin);
        console.log('ini internal', this.principal.getIdInternal());
        this.unitDocumentMesageService.printSave(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessPilihsave(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        )

    }
    private onSuccessPilihsave(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        // this.totalItems = headers.get('X-Total-Count');
        // this.queryCount = this.totalItems;
        console.log(data);
        this.customTemp = data;
        this.print(this.customTemp);
        this.loadingService.loadingStop();
    }

    print(customTemp: Data) {
        // let httpParams = new httpParams();
        // Object.keys(unitDocumentMessages).forEach(function (key) {
        //  httpParams = httpParams.append(key, unitDocumentMessages[key]);
        // });
        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.viewFile('/api/report/print_list/pdf', { temp: customTemp.idtemp});
    }

    createRequestNote() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            data: this.unitDocumentMessages
        }
        this.principal.getUserLogin();
        console.log('ini createrequest', this.unitDocumentMessages);
        console.log('ini createrequest', this.principal.getUserLogin);
        console.log('ini internal', this.principal.getIdInternal());
        this.unitDocumentMesageService.createRequestnote(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessprintsave(res, res.headers),
            (res: ResponseWrapper) => this.onErrorprint(res.json)
        )

    }
    private onSuccessprintsave(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        // this.totalItems = headers.get('X-Total-Count');
        // this.queryCount = this.totalItems;
        console.log(data);
        this.unitDocumentMessages = data;
        this.loadingService.loadingStop();
    }
    private onErrorprint(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    // clear() {
    //     if (this.isMatchingMenu === true) {
    //         this.page = 0;
    //         this.currentSearching = '';
    //         this.router.navigate(['/vehicle-sales-order/true']);
    //         this.loadAll();
    //     }

    //     if (this.isMatchingMenu === false) {
    //         this.page = 0;
    //         this.currentSearching = '';
    //         this.router.navigate(['/vehicle-sales-order']);
    //         this.loadAll();
    //     }

    // }

    search() {
        this.customUnitModel = new Array <CustomUnitModel>();
        this.unitDocumentMessages.forEach(
            (item) => {
                this.eachCustomUnitModel = new CustomUnitModel();
                this.eachCustomUnitModel.idMessage = item.idMessage;
                this.eachCustomUnitModel.idRequirement = item.idRequirement;
                this.eachCustomUnitModel.idProduct = item.IdProduct;
                this.eachCustomUnitModel.idFeature = item.idFeature;
                this.eachCustomUnitModel.Color = item.Color;
                this.eachCustomUnitModel.cabang1 = this.internal1;
                this.eachCustomUnitModel.cabang2 = this.internal2;
                this.eachCustomUnitModel.cabang3 = this.internal3;

                this.customUnitModel.push(this.eachCustomUnitModel);
            }
        )
        console.log('internal1', this.internal1);
        console.log('internal2', this.internal2);
        console.log('internal3', this.internal3);
        console.log('customunit', this.customUnitModel);
        this.unitDocumentMesageService.pillihCabangC(this.customUnitModel).subscribe(
            (res: ResponseWrapper) => this.onSuccesscari(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        )
    }

    private onSuccesscari(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        // this.totalItems = headers.get('X-Total-Count');
        // this.queryCount = this.totalItems;
        console.log(data);
        this.unitDocumentMessages = data;
        this.loadingService.loadingStop();
    }

    searching(query) {
        if (!query) {
            // return this.clear();
        }
        // if (this.isMatchingMenu === true) {
        //     this.page = 0;
        //     this.currentSearching = query;
        //     this.router.navigate(['/vehicle-sales-order/true', {
        //         search: this.currentSearching,
        //         page: this.page,
        //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        //     }]);
        //     this.loadAll();
        // }
        // if (this.isMatchingMenu === false) {
        //     this.page = 0;
        //     this.currentSearching = query;
        //     this.router.navigate(['/vehicle-sales-order', {
        //         search: this.currentSearching,
        //         page: this.page,
        //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        //     }]);
        //     this.loadAll();
        // }

    }

    ngOnInit() {
        this.internalService.query({size: 999999})
        .subscribe((res: ResponseWrapper) => {
            this.internals = res.json;
            console.log('internal :', this.internals)
        });

        // this.activatedRoute.params.subscribe((params) => {
        //     if (params['ismatching']) {
        //         this.isMatchingMenu = true;
        //         console.log('Masuk matching  menu')
        //     } else {
        //         this.isMatchingMenu = false;
        //     }
        // });
        this.unitDocumentMesageService.passingData.subscribe( (unitDocumentMessage) => {
            this.unitDocumentMessages = unitDocumentMessage;
            console.log('ini nerima subscribe', this.unitDocumentMessages);
            }
        );
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        // this.registerChangeInVehicleSalesOrders();
        // this.tglAwal = Date;
        // this.tglAkhir = Date;
        // this.loadLeasing();
        // this.loadKorsal()

    }

    // ngOnDestroy() {
    //     this.eventManager.destroy(this.eventSubscriber);
    //     this.eventSubscriber.unsubscribe();
    // }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }
    trackId(index: number, item: VehicleSalesOrder) {
        return item.idOrder;
    }

    // trackleasingComById(index: number, item: LeasingCompany) {
    //     return item.idPartyRole;
    // }

    // trackkorsalById(index: number, item: Salesman) {
    //     return item.idPartyRole;
    // }

    // trackStatusVsoById(index: number, item: any) {
    //     return item.value;
    // }

    // registerChangeInVehicleSalesOrders() {
    //     this.eventSubscriber = this.eventManager.subscribe('vehicleSalesOrderListModification', (response) => this.loadAll());
    // }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateEntry') {
            result.push('dateEntry');
        }
        return result;
    }

    private onSuccessPilihCabang(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        // this.totalItems = headers.get('X-Total-Count');
        // this.queryCount = this.totalItems;
        console.log(data);
        this.unitDocumentMessages = data;
        this.loadingService.loadingStop();
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.loadingService.loadingStop();
        // this.page = pagingParams.page;
        // console.log(data);

        // console.log(data.salesUnitRequirement.productId);

        // data.forEach((vsoIterate) => {
        //     vsoIterate.statusMotor = 'Biasa';
        //     // var hasil = this.cekIfHotItem(data.salesUnitRequirement.productId);
        //     // if ( statusIsHot !== ''){
        //     //     vsoIterate.statusMotor = statusIsHot;
        //     // }
        //     // console.log('Hasil : ', hasil);
        //     // console.log('isi data.sur ====> ', data);
        // });

        // this.vehicleSalesOrders = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    // executeProcess(data) {
    //     this.vehicleSalesOrderService.executeProcess(0, null, data).subscribe(
    //        (value) => console.log('this: ', value),
    //        (err) => console.log(err),
    //        () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
    //     );
    // }

    loadDataLazy(event: LazyLoadEvent) {
        if (this.isLazyLoadingFirst === false) {
            this.isLazyLoadingFirst = true;
            return ;
        }
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    // updateRowData(event) {
    //     if (event.data.id !== undefined) {
    //         this.vehicleSalesOrderService.update(event.data)
    //             .subscribe((res: VehicleSalesOrder) =>
    //                 this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
    //     } else {
    //         this.vehicleSalesOrderService.create(event.data)
    //             .subscribe((res: VehicleSalesOrder) =>
    //                 this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
    //     }
    // }

    // private onRowDataSaveSuccess(result: VehicleSalesOrder) {
    //     this.toasterService.showToaster('info', 'VehicleSalesOrder Saved', 'Data saved..');
    // }

    // private onRowDataSaveError(error) {
    //     try {
    //         error.json();
    //     } catch (exception) {
    //         error.message = error.text();
    //     }
    //     this.onError(error);
    // }

    // delete(id: any) {
    //     this.confirmationService.confirm({
    //         message: 'Are you sure that you want to delete?',
    //         header: 'Confirmation',
    //         icon: 'fa fa-question-circle',
    //         accept: () => {
    //             this.vehicleSalesOrderService.delete(id).subscribe((response) => {
    //             this.eventManager.broadcast({
    //                 name: 'vehicleSalesOrderListModification',
    //                 content: 'Deleted an vehicleSalesOrder'
    //                 });
    //             });
    //         }
    //     });
    // }

    // getDescription(currentStatus: number) {
    //     return this.vehicleSalesOrderService.getDescriptionStatus(currentStatus);
    // }

    // getStatusMotor(idProduct: string) {
    //     const hasil = 'Biasa';

    //     console.log('Id product : ', idProduct);
    //     // this.ruleHotItemService.findAllByidProduct(idProduct)
    //     //     .subscribe(
    //     //         (res) => console.log(res),
    //     //         (res) => console.log(res),
    //     // );
    //     // var isCekHotItm =  this.cekIfHotItem(idProduct);
    //     // if ( isCekHotItm !== ''){
    //     //     hasil = isCekHotItm;
    //     //     console.log('Result is hot item');
    //     // }
    //     return hasil ;
    // }

    // cekIfHotItem(id: string) {
    //     this.ruleHotItemService.query({
    //         idProduct: id,
    //         page: 0,
    //         size: 1})
    //     .toPromise()
    //     .then((data) => {
    //         return (data.json)
    //     });
    //     return '';
    // };

    // subscribeToSaveResponse(result: Observable<VehicleSalesOrder>): Promise<any> {
    //     return new Promise<any>(
    //         (resolve) => {
    //             result.subscribe((res: VehicleSalesOrder) => {
    //                 resolve();
    //             });
    //         });
    //     }

    // batalVso(vso: VehicleSalesOrder ) {
    //     this.confirmationService.confirm({
    //         message: 'Are you sure that you want to cancel vso [' + vso.salesUnitRequirement.requirementNumber + '] ?',
    //         header: 'Confirmation',
    //         icon: 'fa fa-question-circle',
    //         accept: () => {
    //             this.subscribeToSaveResponse(this.vehicleSalesOrderService.executeProcess(13, null, vso))
    //             .then(
    //                 () => {
    //                     this.eventManager.broadcast({
    //                     name: 'vehicleSalesOrderListModification',
    //                     content: 'cancel an vehicleSalesOrder'
    //                     });
    //                 this.toaster.showToaster('info', 'Data Proces', 'Cancel VSO');
    //                 this.loadAll();
    //                 }
    //             );
    //             // this.vehicleSalesOrderService.executeProcess(13, null, vso)
    //             //     .subscribe((response) => {
    //             //         this.eventManager.broadcast({
    //             //             name: 'vehicleSalesOrderListModification',
    //             //             content: 'cancel an vehicleSalesOrder'
    //             //             });
    //             //         this.toaster.showToaster('info', 'Data Proces', 'Cancel VSO');
    //             //     });
    //         }
    //     });
    // }
    // buildReindex() {
    //     this.vehicleSalesOrderService.process({command: 'buildIndexVSO'}).subscribe((r) => {
    //         this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
    //     });
    // }
}
