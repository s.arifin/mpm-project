import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import {Observable} from 'rxjs/Rx';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { SelectItem} from 'primeng/primeng';

import * as AdminHandlingConstant from '../../../shared/constants/vehicle-sales-order.constants';
import { VehicleSalesOrder, VehicleSalesOrderService } from '../../vehicle-sales-order';
import { Request, RequestService, CustomRequestNote } from '../../request';
import { Internal } from '../../internal';

@Component({
    selector: 'jhi-transfer-unit-request-note',
    templateUrl: './transfer-unit-request-note.component.html'
})
export class TransferUnitRequestComponent implements OnInit {

    requestNotes: CustomRequestNote[];
    currentAccount: any;
    vehicleSalesOrders: VehicleSalesOrder[];
    vehicleSalesOrderSubmit: VehicleSalesOrder[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    leasingStatusPayment: any;
    newTagihan: Boolean = false;
    newPending: Boolean = false;
    isSaving: boolean;
    // tglAwal:string;
    // tglAkhir:string;

    leasings: SelectItem[];
    selectedLeasing: string;

    korsals: SelectItem[];
    selectedKorsal: string;
    selected: VehicleSalesOrder[];

    constructor(
        private requestService: RequestService,
        private vehicleSalesOrderService: VehicleSalesOrderService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';

        this.leasings = [];
        this.korsals = [];
    }

    loadAll(idInternal?: string) {
        this.requestService.queryFilterByinternalC({
            query: 'idInternal:' + idInternal,
            page: this.page,
            size: this.itemsPerPage,
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            )
        console.log('ini internal load all', this.principal.getIdInternal());
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    saveUploadFaktur() {};

    transition() {
        this.router.navigate(['/transfer-unit-request-note'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    // submitBAST(bast) {
    //     this.confirmationService.confirm({
    //         header : 'Confirmation',
    //         message : 'Anda akan melakukan cetak BAST Tagihan ke Finance Company?',
    //         accept: () => {
    //             this.vehicleSalesOrderService.executeProcess(AdminHandlingConstant.APPROVAL_BAST, null, bast).subscribe(
    //                 (res) => {
    //                     this.eventManager.broadcast({
    //                         name: 'vehicleSalesOrderListModification',
    //                         content: 'Cetak BAST Tagihan ke Finance Company Success'
    //                     });
    //                 },
    //                 (err) => {
    //                     this.onError(err);
    //                 }
    //             );
    //         }
    //     });
    // }

    clear() {
        // this.page = 0;
        // this.currentSearch = '';
        // this.router.navigate(['/vehicle-sales-order', {
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        // this.loadAll();
    }

    search(query) {
        // if (!query) {
        //     return this.clear();
        // }
        // this.page = 0;
        // this.currentSearch = query;
        // this.router.navigate(['/admin-po-leasing', {
        //     search: this.currentSearch,
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        // this.loadAll();
    }

    ngOnInit() {
        // this.loadAllSubmit();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll(this.principal.getIdInternal());
        });
        // this.loadAll();
        // this.registerChangeInVehicleSalesOrders();
        // this.tglAwal = Date;
        // this.tglAkhir = Date;
    }

    // ngOnDestroy() {
    //     this.eventManager.destroy(this.eventSubscriber);
    // }

    // trackId(index: number, item: VehicleSalesOrder) {
    //     return item.idOrder;
    // }

    // registerChangeInVehicleSalesOrders() {
    //     // this.eventSubscriber = this.eventManager.subscribe('vehicleSalesOrderListModification', (response) => this.loadAll());
    // }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idreqnot') {
            result.push('idreqnot');
        }
        return result;
    }

    private onSuccess(data, headers) {
        if (data !== null) {
            this.totalItems = data[0].Totaldata;
            this.queryCount = this.totalItems;
            this.requestNotes = data;
        }
        console.log('ini data internal', data);
        // this.links = this.parseLinks.parse(headers.get('link'));
        // this.totalItems = headers.get('X-Total-Count');
        // this.queryCount = this.totalItems;
        // // this.page = pagingParams.page;
        // // filter((o) => o.idCategory === 11 || o.idCategory === 12)
        // // .filter((p) => p.currenntStatus === 17)
        // this.vehicleSalesOrders = data;
        // this.vehicleSalesOrderSubmit = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    // executeProcess(data) {
    //     this.vehicleSalesOrderService.executeProcess(0, null, data).subscribe(
    //        (value) => console.log('this: ', value),
    //        (err) => console.log(err),
    //        () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
    //     );
    // }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll(this.principal.getIdInternal());
    }

    // approval(data: VehicleSalesOrder) {
    //     this.confirmationService.confirm({
    //         message: 'Do you want to Approve this PO?',
    //         header: 'Confirmation',
    //         icon: 'fa fa-question-circle',
    //         accept: () => {
    //             this.vehicleSalesOrderService.executeProcess(AdminHandlingConstant.VSO_APPROVAL_ADMIN_HANDLING, null, data).subscribe(
    //                 (res) => {
    //                     this.vehicleSalesOrders = new Array<VehicleSalesOrder>();
    //                     this.loadAll();
    //                 }
    //             )
    //         }
    //     });
    // }

    loadDataLazySubmit(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAllSubmit();
    }

    updateRowData(event) {
        // if (event.data.id !== undefined) {
        //     this.vehicleSalesOrderService.update(event.data)
        //         .subscribe((res: VehicleSalesOrder) =>
        //             this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        // } else {
        //     this.vehicleSalesOrderService.create(event.data)
        //         .subscribe((res: VehicleSalesOrder) =>
        //             this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        // }
    }

    // private onRowDataSaveSuccess(result: VehicleSalesOrder) {
    //     this.toasterService.showToaster('info', 'VehicleSalesOrder Saved', 'Data saved..');
    // }

    // private onRowDataSaveError(error) {
    //     try {
    //         error.json();
    //     } catch (exception) {
    //         error.message = error.text();
    //     }
    //     this.onError(error);
    // }

    delete(id: any) {
        // this.confirmationService.confirm({
        //     message: 'Are you sure that you want to delete?',
        //     header: 'Confirmation',
        //     icon: 'fa fa-question-circle',
        //     accept: () => {
        //         this.vehicleSalesOrderService.delete(id).subscribe((response) => {
        //         this.eventManager.broadcast({
        //             name: 'vehicleSalesOrderListModification',
        //             content: 'Deleted an vehicleSalesOrder'
        //             });
        //         });
        //     }
        // });
    }

    buildReindex() {
        this.vehicleSalesOrderService.process({command: 'buildIndex'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }

    // updateLeasing(data?: any) {
    //     this.isSaving = true;
    //     this.leasingStatusPayment = 1;
    //     data.idLeasingStatusPayment = this.leasingStatusPayment;
    //     console.log(data.idLeasingStatusPayment);
    //     this.subscribeToSaveResponse(
    //         this.vehicleSalesOrderService.update(data));
    // }

    // private subscribeToSaveResponse(result: Observable<VehicleSalesOrder>) {
    //     result.subscribe((res: VehicleSalesOrder) =>
    //         this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    // }

    // private onSaveSuccess(result: VehicleSalesOrder) {
    //     this.eventManager.broadcast({ name: 'vehicleSalesOrderListModification', content: 'OK'});
    //     this.toasterService.showToaster('info', 'Save', 'Tagihan Submitted !');
    //     this.isSaving = false;
    // }

    // private onSaveError(error) {
    //     try {
    //         error.json();
    //     } catch (exception) {
    //         error.message = error.text();
    //     }
    //     this.isSaving = false;
    //     this.onError(error);
    // }
}
