import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import {Observable} from 'rxjs/Rx';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { SelectItem} from 'primeng/primeng';

import * as AdminHandlingConstant from '../../../shared/constants/vehicle-sales-order.constants';
import { VehicleSalesOrder, VehicleSalesOrderService } from '../../vehicle-sales-order';
import { UnitDocumentMessage, UnitDocumentMessageService } from '../../unit-document-message';

@Component({
    selector: 'jhi-transfer-unit-sales-note',
    templateUrl: './transfer-unit-sales-note.component.html'
})
export class TransferUnitComponent implements OnInit {

    unitDocumentMessage: UnitDocumentMessage[];
    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    isSaving: boolean;
    // tglAwal:string;
    // tglAkhir:string;
    korsals: SelectItem[];
    selectedKorsal: string;
    selected: any = [];

    constructor(
        private unitDocumentMessageService: UnitDocumentMessageService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.unitDocumentMessageService.queryFilterByinternalC({
            // idInternal: this.principal.getIdInternal(),
            query: 'idInternal:' + this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                // (res: ResponseWrapper) => this.onError(res.json)
            )
    }

    createRequest() {
        this.unitDocumentMessageService.passingCustomData(this.selected);
        console.log('isi selected', this.selected);
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/unit-document-message'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
        // this.loadAllSubmit();
    }

    clear() {
        // this.page = 0;
        // this.currentSearch = '';
        // this.router.navigate(['/vehicle-sales-order', {
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        // this.loadAll();
    }

    search(query) {
        // if (!query) {
        //     return this.clear();
        // }
        // this.page = 0;
        // this.currentSearch = query;
        // this.router.navigate(['/admin-po-leasing', {
        //     search: this.currentSearch,
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        // this.loadAll();
    }

    saveUploadFaktur() {
    };

    ngOnInit() {
        // this.loadAllSubmit();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    trackId(index: number, item: VehicleSalesOrder) {
        return item.idOrder;
    }

    sort() {
        // const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        // if (this.predicate !== 'idOrder') {
        //     result.push('idOrder');
        // }
        // return result;
    }

    private onSuccess(data, headers) {
        if (data !== null) {
            this.totalItems = data[0].Totaldata;
            this.queryCount = this.totalItems;
            this.unitDocumentMessage = data;
        }
        console.log('ini data internal', data);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        console.log('ini data onerror', error.message);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    loadDataLazySubmit(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAllSubmit();
    }

    delete(id: any) {
        // this.confirmationService.confirm({
        //     message: 'Are you sure that you want to delete?',
        //     header: 'Confirmation',
        //     icon: 'fa fa-question-circle',
        //     accept: () => {
        //         this.vehicleSalesOrderService.delete(id).subscribe((response) => {
        //         this.eventManager.broadcast({
        //             name: 'vehicleSalesOrderListModification',
        //             content: 'Deleted an vehicleSalesOrder'
        //             });
        //         });
        //     }
        // });
    }

}
