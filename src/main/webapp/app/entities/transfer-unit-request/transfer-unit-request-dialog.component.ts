import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TransferUnitRequest } from './transfer-unit-request.model';
import { TransferUnitRequestPopupService } from './transfer-unit-request-popup.service';
import { TransferUnitRequestService } from './transfer-unit-request.service';

@Component({
    selector: 'jhi-transfer-unit-request-dialog',
    templateUrl: './transfer-unit-request-dialog.component.html'
})
export class TransferUnitRequestDialogComponent implements OnInit {

    transferUnitRequest: TransferUnitRequest;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private transferUnitRequestService: TransferUnitRequestService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.transferUnitRequest.id !== undefined) {
            this.subscribeToSaveResponse(
                this.transferUnitRequestService.update(this.transferUnitRequest));
        } else {
            this.subscribeToSaveResponse(
                this.transferUnitRequestService.create(this.transferUnitRequest));
        }
    }

    private subscribeToSaveResponse(result: Observable<TransferUnitRequest>) {
        result.subscribe((res: TransferUnitRequest) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TransferUnitRequest) {
        this.eventManager.broadcast({ name: 'transferUnitRequestListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-transfer-unit-request-popup',
    template: ''
})
export class TransferUnitRequestPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private transferUnitRequestPopupService: TransferUnitRequestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.transferUnitRequestPopupService
                    .open(TransferUnitRequestDialogComponent as Component, params['id']);
            } else {
                this.transferUnitRequestPopupService
                    .open(TransferUnitRequestDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
