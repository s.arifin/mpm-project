import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

import { LazyLoadEvent, AutoComplete} from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import { TransferUnitRequest } from './transfer-unit-request.model';
import { LoadingService } from '../../layouts/loading/loading.service';
import { CommunicationEventService } from '../communication-event/communication-event.service';
import { CommunicationEventInternal } from '../communication-event/communication-event.model';
import { TransferUnitRequestService } from './transfer-unit-request.service';
import { EventTypeService } from '../event-type/event-type.service';
import { EventType } from '../event-type/event-type.model';
import { GoodService } from '../good/good.service'
import { Good } from '../good/good.model'
import { Feature } from '../feature/feature.model'
import { Facility } from '../facility/facility.model'

@Component({
    selector: 'jhi-transfer-unit-request-event-add',
    templateUrl: './transfer-unit-request-event-add.component.html'
})
export class TransferUnitRequestEventAddComponent implements OnInit, OnDestroy {

    currentAccount: any;
    eventType: EventType[];
    transferRes: TransferUnitRequest;
    transferUnitRequestEventAdds: TransferUnitRequest[];
    transferUnitRequests: TransferUnitRequest[];
    communicationEventInternal: CommunicationEventInternal[];
    selectedTglMulai: any;
    selectedTglSelesai: any;
    selectedTglKirim: any;
    selectedTglAmbil: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    goods: Good[];
    years: number[];
    features: Feature[];
    nameEvent: string[];
    selectedGood: any;
    marketname: any;
    facilityDesc: any;
    selectedYear: any;
    selectedFeature: any;
    selectedDate: any;
    selectedEventType: any;
    warna: any;
    qtyRequest: any;
    active: boolean;
    text: string;
    disableTglEvent: boolean;
    disableNamaEvent: boolean;

    constructor(
        private transferUnitRequestService: TransferUnitRequestService,
        private goodService: GoodService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        private principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected eventTypeService: EventTypeService,
        protected toasterService: ToasterService,
        private loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
            this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.transferUnitRequests = new Array<TransferUnitRequest>();
        this.communicationEventInternal = new Array<CommunicationEventInternal>();
        this.nameEvent = new Array<string>();
        this.transferRes = new TransferUnitRequest();
        this.disableTglEvent = false;
        this.disableNamaEvent = true;
        this.active = false;
        // this.selectedTglAmbil = new Date();
        // this.selectedTglKirim = new Date();
        // this.selectedTglMulai = new Date();
        // this.selectedTglSelesai = new Date();
    }

    loadAll() {
    }

    loadGood() {
        this.selectedGood = null;
        this.selectedYear = null;
        this.qtyRequest = 1;
        this.goodService.queryByInternalC({
            pidinternal: 'idinternal:' + this.principal.getIdInternal()
        }).subscribe(
            (res: ResponseWrapper) => {
                this.goods = res.json;
            }
        );
    }

    EventName(event) {
        this.disableTglEvent = false;
        const query = event.query;
        if (query.length > 3) {
            this.eventTypeService.findQueryEventName({
                query: 'query:' + query,
                eventType: 'idEventType:' + this.selectedEventType,
                internals: 'internal:' + this.principal.getIdInternal()
            }).subscribe(
                (res) => {
                    this.nameEvent = new Array<string>();
                    this.communicationEventInternal = res.json;
                    for (const tmp in this.communicationEventInternal) {
                        if (this.communicationEventInternal[tmp] !== null) {
                            this.nameEvent.push(this.communicationEventInternal[tmp].EventName);
                        }
                    }
                    this.nameEvent = [...this.nameEvent];
                }
            )
        }
    }

    onSelectEventName(teks: string) {
        for (const tmp in this.communicationEventInternal) {
            if (this.communicationEventInternal[tmp].EventName === this.text) {
                this.selectedTglMulai = new Date(this.communicationEventInternal[tmp].DateFrom);
                this.selectedTglSelesai = new Date(this.communicationEventInternal[tmp].DateThru);
                this.disableTglEvent = true;
            }
        }
    }

    onChanegeEventType() {
        this.disableNamaEvent = false;
    }

    loadYear(idproduct: any) {
        this.selectedFeature = null;
        this.selectedYear = null;
        this.features = null;
        this.years = null;
        this.qtyRequest = 1;
        this.goodService.queryYearsByProductC({
            pidinternal: 'idinternal:' + this.principal.getIdInternal(),
            pidproduct: 'idproduct:' + idproduct
        }).subscribe(
            (res: ResponseWrapper) => {
                this.years = res.json;
            }
        );
    }

    loadFeature(idproduct: any, year: number) {
        this.selectedFeature = null;
        this.features = null;
        this.qtyRequest = 1;
        this.goodService.queryGetFeatureByYearProductC({
            pidinternal: 'idinternal:' + this.principal.getIdInternal(),
            pidproduct: 'idproduct:' + idproduct,
            pyear: 'year:' + year
        }).subscribe(
            (res: ResponseWrapper) => {
                this.features = res.json
            }
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/transfer-unit-request-event'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    search: this.currentSearch,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/transfer-unit-request-event', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    // search(query) {
    //     if (!query) {
    //         return this.clear();
    //     }
    //     this.page = 0;
    //     this.currentSearch = query;
    //     this.router.navigate(['/stock-opname', {
    //         search: this.currentSearch,
    //         page: this.page,
    //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
    //     }]);
    //     this.loadAll();
    // }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.eventTypeService.findByParentEventType({
                parent: 'idParentEvent:' + 1
            }).subscribe(
                (data) => {
                    this.eventType = data.json;
                }
            )
            this.loadGood();
        });
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    // trackId(index: number, item: TransferUnitRequest) {
    //     return item.idStockOpname;
    // }
    registerChangeInTransferUnitRequests() {
        this.eventSubscriber = this.eventManager.subscribe('transferUnitRequestListModification', (response) => this.loadAll());
    }

    // registerChangeInStockOpnames() {
    //     this.eventSubscriber = this.eventManager.subscribe('stockOpnameListModification', (response) => this.loadAll());
    // }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.transferUnitRequestEventAdds = data;
    }

    protected onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    // executeProcess(item) {
    //     this.transferUnitRequestService.executeProcess(item).subscribe(
    //         (value) => console.log('this: ', value),
    //         (err) => console.log(err),
    //         () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
    //     );
    // }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.transferUnitRequestService.update(event.data)
                .subscribe((res: TransferUnitRequest) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.transferUnitRequestService.create(event.data)
                .subscribe((res: TransferUnitRequest) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    DeleteOne(index: any) {
        this.transferUnitRequests.splice(index , 1);
        this.transferUnitRequests = [...this.transferUnitRequests];
    }

    protected onRowDataSaveSuccess(result: TransferUnitRequest) {
        this.toasterService.showToaster('info', 'Transfer Unit Request Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.transferUnitRequestService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'transferUnitRequestListModification',
                        content: 'Deleted an stockOpname'
                    });
                });
            }
        });
    }

    tambahDetail(idproduct: any, year: number, feature: any) {
        console.log('data sekarang :', idproduct, year, feature);
        const tempTransferUnit1 = new TransferUnitRequest;
        for (const tmp in this.goods) {
            if (this.goods[tmp].idProduct === idproduct) {
                tempTransferUnit1.idProduct = this.goods[tmp].idProduct;
                tempTransferUnit1.MarketName = this.goods[tmp].name;
            }
        }
        for (const tmp in this.features) {
            if (this.features[tmp].idFeature === feature) {
                tempTransferUnit1.IdFeature = this.features[tmp].idFeature;
                tempTransferUnit1.Color = this.features[tmp].description;
            }
        }
        tempTransferUnit1.Year = year;
        tempTransferUnit1.Qty = this.qtyRequest;
        this.transferUnitRequests.push(tempTransferUnit1);
        this.transferUnitRequests = [...this.transferUnitRequests];
        console.log('data sekarang :', this.transferUnitRequests);
        this.clearData();
    }

    createRequestNote() {
        if (this.selectedTglMulai == null || this.selectedTglKirim == null || this.selectedEventType == null || this.selectedEventType == null || this.selectedTglSelesai == null || this.selectedTglAmbil == null) {
            this.toasterService.showToaster('info', 'Gagal!', 'Lokasi Tujuan , Tanggal Pengiriman dan event Tidak Boleh Kosong');
        } else {
            if (this.transferUnitRequests.length !== 0) {
                this.loadingService.loadingStart();
                const obj = {
                    IdInternal: this.principal.getIdInternal(),
                    DeliveryDate: this.selectedTglKirim,
                    ReturnDate: this.selectedTglAmbil,
                    DateFrom: this.selectedTglMulai,
                    DateThru: this.selectedTglSelesai,
                    NamaEvent: this.text,
                    EventType: this.selectedEventType,
                    PIC: this.principal.getUserLogin(),
                    data: this.transferUnitRequests
                }
                this.transferUnitRequestService.createRequestnoteEvent(obj).subscribe(
                    (res: ResponseWrapper) => this.onSuccessPos(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                )
            } else {
                this.toasterService.showToaster('info', 'Gagal!', 'Request motor minimal 1');
            }
        }
    }

    private onSuccessPos(data, headers) {
        this.active = true;
        this.disableNamaEvent = true;
        this.transferRes = data;
        this.loadingService.loadingStop();
    }

    clearData() {
        this.years = null;
        this.features = null;
        this.selectedGood = null;
        this.selectedYear = null;
        this.selectedFeature = null;
        this.qtyRequest = null;
    }

    trackGoodById(index: number, item ) {
        return item.idProduct;
    }
    trackFeatureById(index: number, item ) {
        return item.idFeature;
    }
    trackYear(index: number, item ) {
        return item;
    }
    trackEventType(index: number, item ) {
        return item;
    }
}
