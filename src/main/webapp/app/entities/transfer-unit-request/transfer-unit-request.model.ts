import { BaseEntity } from './../../shared';

export class TransferUnitRequest implements BaseEntity {
    constructor(
        public id?: number,
        public IdReqnot?: String,
        public ReqnotNumber?: String,
        public RequestDate?: any,
        public DeliveryDate?: any,
        public LocationTo?: String,
        public IdFacilityTo?: any,
        public IdInternal?: any,
        public PIC?: any,
        public status?: any,
        public IdReqnotDest?: any,
        public LocationFrom?: String,
        public IdFacilityFrom?: any,
        public idProduct?: any,
        public IdProduct?: any,
        public IdFeature?: any,
        public Color?: any,
        public Year?: any,
        public Qty?: any,
        public QtyTransfer?: any,
        public MarketName?: any,
        public IdMachine?: any,
        public IdFrame?: any,
        public Totaldata?: any,
    ) {
    }
}

export class TransferUnitRequestMain implements BaseEntity {
    constructor(
        public id?: number,
        public IdReqNot?: any,
        public RequestNumber?: any,
        public dtRequest?: any,
        public FacilitySrc?: any,
        public IdFacilitySrc?: any,
        public FacilityDest?: any,
        public IdFacilityDest?: any,
        public dtSent?: Date,
        public eventName?: any,
        public dtReturn?: any,
        public dtFrom?: Date,
        public dtThru?: Date,
        public StatusReq?: any,
        public Status?: any,
        public idComEvent?: any,
        public Qty?: any,
        public TotalData?: any,
    ) {
    }
}

export class TransferUnitReceive implements BaseEntity {
    constructor(
        public id?: number,
        public kodeTipe?: String,
        public color?: String,
        public idMachine?: Date,
        public accessories1?: String,
        public accessories2?: String,
        public accessories3?: String,
        public accessories4?: String,
        public accessories5?: String,
        public accessories6?: String,
        public accessories7?: String,
        public accessories8?: String,
        public accessoriesAdd1?: String,
        public accessoriesAdd2?: Date,
        public location?: String,
        public status?: String,
    ) {
    }
}
