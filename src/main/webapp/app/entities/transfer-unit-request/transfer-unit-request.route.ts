import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TransferUnitRequestComponent } from './transfer-unit-request.component';
import { TransferUnitRequestDetailComponent } from './transfer-unit-request-detail.component';
import { TransferUnitRequestPopupComponent } from './transfer-unit-request-dialog.component';
import { TransferUnitRequestDeletePopupComponent } from './transfer-unit-request-delete-dialog.component';
import { TransferUnitRequestEventComponent } from './transfer-unit-request-event.component';
import { TransferUnitRequestEventAddComponent } from './transfer-unit-request-event-add.component';
import { TransferUnitRequestEventDetailComponent } from './transfer-unit-request-event-detail.component';
import { TransferUnitRequestPosComponent } from './transfer-unit-request-pos.component';
import { TransferUnitRequestPosDetailComponent } from './transfer-unit-request-pos-detail.component';
import { TransferUnitRequestPosTambahComponent } from './transfer-unit-request-pos-tambah.component';
import { TransferUnitReceivePosComponent } from './transfer-unit-receive/transfer-unit-receive-pos.component';
import { TransferUnitReceivePosDetailComponent } from './transfer-unit-receive/transfer-unit-receive-pos-detail.component';
import { TransferUnitReceiveEventDetailComponent } from './transfer-unit-receive/transfer-unit-receive-event-detail.component';
import { TransferUnitReceiveEventComponent } from './transfer-unit-receive/transfer-unit-receive-event.component';
import { TransferUnitReceiveGudangComponent } from './transfer-unit-receive/transfer-unit-receive-gudang.component';
import { TransferUnitReceiveGudangDetailComponent } from './transfer-unit-receive/transfer-unit-receive-gudang-detail.component';
import { TransferUnitReceiveGudangCheckComponent } from './transfer-unit-receive/transfer-unit-receive-gudang-check.component';
import { AntarCabangComponent } from './transfer-unit-receive/antar-cabang.component';
import { AntarCabangDetailComponent } from './transfer-unit-receive/antar-cabang-detail.component';
// import { TransferUnitReceiveEventDetailComponent } from './transfer-unit-receive-event-detail.component';
// import { TransferUnitReceiveEventComponent } from './transfer-unit-receive-event.component';

@Injectable()
export class TransferUnitRequestResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const transferUnitRequestRoute: Routes = [
    {
        path: 'transfer-unit-request',
        component: TransferUnitRequestComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    },  {
        path: 'transfer-unit-request-event',
        component: TransferUnitRequestEventComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    },  {
        path: 'transfer-unit-request/:id',
        component: TransferUnitRequestDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'transfer-unit-request-pos',
        component: TransferUnitRequestPosComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'transfer-unit-request-pos-detail',
        component: TransferUnitRequestPosDetailComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    },  {
        path: 'transfer-unit-request-pos-tambah',
        component: TransferUnitRequestPosTambahComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'transfer-unit-request-event-add',
        component: TransferUnitRequestEventAddComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'transfer-unit-request-event-detail',
        component: TransferUnitRequestEventDetailComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'transfer-unit-receive-pos',
        component: TransferUnitReceivePosComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'transfer-unit-receive-pos-detail',
        component: TransferUnitReceivePosDetailComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'transfer-unit-receive-event-detail',
        component: TransferUnitReceiveEventDetailComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'transfer-unit-receive-event',
        component: TransferUnitReceiveEventComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'transfer-unit-receive-gudang',
        component: TransferUnitReceiveGudangComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'transfer-unit-receive-gudang/:IdReqNot/transfer-unit-receive-gudang-detail',
        component: TransferUnitReceiveGudangDetailComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'transfer-unit-receive-gudang-check',
        component: TransferUnitReceiveGudangCheckComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'receive-antar-cabang',
        component: AntarCabangComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'receive-antar-cabang-detail',
        component: AntarCabangDetailComponent,
        resolve: {
            'pagingParams': TransferUnitRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];

export const transferUnitRequestPopupRoute: Routes = [
    {
        path: 'transfer-unit-request-new',
        component: TransferUnitRequestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'transfer-unit-request/:id/edit',
        component: TransferUnitRequestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'transfer-unit-request/:id/delete',
        component: TransferUnitRequestDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnitRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
