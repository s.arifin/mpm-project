import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { TransferUnitRequest, TransferUnitRequestMain } from './transfer-unit-request.model';
import { TransferUnitRequestService } from './transfer-unit-request.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { FacilityService } from '../facility/facility.service';
import { Facility } from '../facility/facility.model';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LazyLoadEvent } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-transfer-unit-request-pos',
    templateUrl: './transfer-unit-request-pos.component.html'
})
export class TransferUnitRequestPosComponent implements OnInit, OnDestroy {

    currentAccount: any;
    transferUnitRequests: TransferUnitRequestMain[];
    facilities: Facility[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    searching: string;
    filtering: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    idStatusType: number;
    filterStatus: any;
    filterDateFrom: Date;
    filterDateThru: Date;
    filterDeliveryDateFrom: Date;
    filterDeliveryDateThru: Date;
    selectedFacility: any;
    selectedFacilityFrom: any;
    convertedDateFrom: any;
    convertedDateThru: any;
    convertedDeliveryDateFrom: any;
    convertedDeliveryDateThru: any;

    constructor(
        private transferUnitRequestService: TransferUnitRequestService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private loadingService: LoadingService,
        private facilityService: FacilityService,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.idStatusType = 12;
        this.filterStatus = [
            { statusId: 12, statusDeskripsi: 'Open' },
            { statusId: 14, statusDeskripsi: 'Approve' },
            { statusId: 13, statusDeskripsi: 'Cancel' },
            { statusId: 15, statusDeskripsi: 'Not Approve' },
            { statusId: 16, statusDeskripsi: 'Inventory Transfer' },
            { statusId: 17, statusDeskripsi: 'Complete' },
        ];
        this.filterDateFrom = null;
        this.filterDateThru = null;
        this.filterDeliveryDateFrom = null;
        this.filterDeliveryDateThru = null;
        this.selectedFacility = null;
        this.selectedFacilityFrom = null;
        this.convertedDateFrom = null;
        this.convertedDateThru = null;
        this.convertedDeliveryDateFrom = null;
        this.convertedDeliveryDateThru = null;
        this.searching = 'false';
        this.filtering = 'false';
    }

    loadAll() {
        console.log('Page :', this.page, ' previousPage:', this.previousPage, ' rerse :', this.reverse, ' predicate:', this.predicate);
        this.loadingService.loadingStart();
        if (this.filterDateFrom !== null) {
            this.convertedDateFrom = this.filterDateFrom.toJSON();
        }
        if (this.filterDateThru !== null) {
            this.convertedDateThru = this.filterDateThru.toJSON();
        }
        if (this.filterDeliveryDateFrom !== null) {
            this.convertedDeliveryDateFrom = this.filterDeliveryDateFrom.toJSON();
        }
        if (this.filterDeliveryDateThru !== null) {
            this.convertedDeliveryDateThru = this.filterDeliveryDateThru.toJSON();
        }
        this.transferUnitRequestService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            status: this.idStatusType,
            filterLocationFrom: this.selectedFacilityFrom,
            filterLocation: this.selectedFacility,
            filterDtFrom: this.convertedDateFrom,
            filterDtThru: this.convertedDateThru,
            filterDelDtFrom: this.convertedDeliveryDateFrom,
            filterDelDtThru: this.convertedDeliveryDateThru,
            checkSearching: this.searching,
            checkFilter: this.filtering,
            querySearch: this.currentSearch,
            query: 'idInternal:' + this.principal.getIdInternal(),
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/transfer-unit-request-pos'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 1;
        this.previousPage = 1;
        this.currentSearch = '';
        this.router.navigate(['/transfer-unit-request-pos', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.searching = 'false';
        this.filtering = 'false';
        this.loadAll();
    }
    search(query) {
        console.log('Page :', this.page, ' previousPage:', this.previousPage, ' rerse :', this.reverse, ' predicate:', this.predicate);
        this.searching = 'true';
        this.filtering = 'false';
        this.page = 1;
        this.previousPage = 1;
        this.loadAll();
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.loadFacility();
        });
        this.registerChangeInTransferUnitRequests();
    }

    loadFacility() {
        this.facilityService.queryFilterByinternalC({
            query: 'idinternal:' + this.principal.getIdInternal()
        }).subscribe(
            (res: ResponseWrapper) => {
                this.facilities = res.json;
            }
        );
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TransferUnitRequest) {
        return item.id;
    }
    registerChangeInTransferUnitRequests() {
        this.eventSubscriber = this.eventManager.subscribe('transferUnitRequestListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    gotoDetail(data: TransferUnitRequestMain) {
        this.transferUnitRequestService.changeShareDataTmp(data);
        this.router.navigate(['../transfer-unit-request-pos-detail']);
    }

    private onSuccess(data, headers) {
        this.transferUnitRequests = null;
        this.loadingService.loadingStop();
        this.queryCount = 0;
        this.totalItems = 0;
        this.queryCount = data[0].TotalData;
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = data[0].TotalData;
        // this.page = pagingParams.page;
        this.transferUnitRequests = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
    trackStatusType(index: number, item) {
        return item.statusId;
    }

    trackFacilityById(index: number, item) {
        return item.statusId;
    }
    trackFacilityFromById(index: number, item) {
        return item.statusId;
    }
    executeFilter() {
        this.page = 0;
        this.previousPage = 0;
        this.transferUnitRequests = null;
        this.filtering = 'true';
        this.loadAll();
    }
}
