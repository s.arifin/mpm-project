import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { TransferUnitRequest } from './transfer-unit-request.model';
import { TransferUnitRequestService } from './transfer-unit-request.service';
import { FacilityService } from '../facility/facility.service'
import { LoadingService } from '../../layouts/loading/loading.service';
import { GoodService } from '../good/good.service'
import { Good } from '../good/good.model'
import { Feature } from '../feature/feature.model'
import { Facility } from '../facility/facility.model'
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-transfer-unit-request-pos-tambah',
    templateUrl: './transfer-unit-request-pos-tambah.component.html'
})
export class TransferUnitRequestPosTambahComponent implements OnInit, OnDestroy {

currentAccount: any;
    transferUnitRequests: TransferUnitRequest[];
    tempTransferUnit: TransferUnitRequest;
    facilities: Facility[];
    facilitiesTo: Facility[];
    transferRes: TransferUnitRequest;
    goods: Good[];
    years: number[];
    features: Feature[];
    selectedGood: any;
    marketname: any;
    selectedFacility: any;
    selectedFacilityTo: any;
    facilityDesc: any;
    selectedYear: any;
    selectedFeature: any;
    selectedDate: any;
    warna: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    qtyRequest: any;
    btnTambah: boolean;
    active: boolean;

    constructor(
        private transferUnitRequestService: TransferUnitRequestService,
        private facilityService: FacilityService,
        private goodService: GoodService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private confirmationService: ConfirmationService,
        private loadingService: LoadingService,
    ) {
        this.facilities = null;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.transferUnitRequests = new Array<TransferUnitRequest>();
        this.tempTransferUnit = new TransferUnitRequest();
        this.transferRes = new TransferUnitRequest();
        this.qtyRequest = 1;
        this.active = false;
        this.btnTambah = false;
    }

    loadAll() {
        // if (this.currentSearch) {
        //     this.transferUnitRequestService.search({
        //         page: this.page - 1,
        //         query: this.currentSearch,
        //         size: this.itemsPerPage,
        //         sort: this.sort()}).subscribe(
        //             (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
        //             (res: ResponseWrapper) => this.onError(res.json)
        //         );
        //     return;
        // }
        // this.transferUnitRequestService.query({
        //     page: this.page - 1,
        //     size: this.itemsPerPage,
        //     sort: this.sort()}).subscribe(
        //     (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
        //     (res: ResponseWrapper) => this.onError(res.json)
        // );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    loadFacility() {
        this.facilityService.queryFilterByinternalC({
            query: 'idinternal:' + this.principal.getIdInternal()
        }).subscribe(
            (res: ResponseWrapper) => {
                this.facilities = res.json;
                this.facilitiesTo = res.json;
            }
        );
    }

    loadGood(idfacility: any) {
        console.log('facility : ', idfacility);
        this.selectedGood = null;
        this.selectedFeature = null;
        this.selectedYear = null;
        this.qtyRequest = 1;
        this.goods = null;
        this.years = null;
        this.features = null;
        this.goodService.queryFilterByFeatureC({
            pidinternal: 'idinternal:' + this.principal.getIdInternal(),
            pidfacility: 'idfacility:' + idfacility
        }).subscribe(
            (res: ResponseWrapper) => {
                this.goods = res.json;
            }
        );
    }
    loadYear(idfacility: any, idproduct: any) {
        this.selectedFeature = null;
        this.selectedYear = null;
        this.qtyRequest = 1;
        this.years = null;
        this.features = null;
        this.goodService.queryGetyearsC({
            pidinternal: 'idinternal:' + this.principal.getIdInternal(),
            pidfacility: 'idfacility:' + idfacility,
            pidproduct: 'idproduct:' + idproduct
        }).subscribe(
            (res: ResponseWrapper) => {
                this.years = res.json;
            }
        );
    }
    loadFeature(idfacility: any, idproduct: any, year: number) {
        this.selectedFeature = null;
        this.qtyRequest = 1;
        this.features = null;
        this.goodService.queryGetFeatureC({
            pidinternal: 'idinternal:' + this.principal.getIdInternal(),
            pidfacility: 'idfacility:' + idfacility,
            pidproduct: 'idproduct:' + idproduct,
            pyear: 'year:' + year
        }).subscribe(
            (res: ResponseWrapper) => {
                this.features = res.json
            }
        );
    }

    tambahDetail(idfacility: any, idproduct: any, year: number, feature: any) {
        console.log('data sekarang :', idfacility, idproduct, year, feature);
        const tempTransferUnit1 = new TransferUnitRequest;
        for (const tmp in this.facilities) {
            if (this.facilities[tmp].idFacility === idfacility) {
                tempTransferUnit1.IdFacilityFrom = idfacility;
                tempTransferUnit1.LocationFrom = this.facilities[tmp].description;
            }
        }
        for (const tmp in this.goods) {
            if (this.goods[tmp].idProduct === idproduct) {
                tempTransferUnit1.idProduct = this.goods[tmp].idProduct;
                tempTransferUnit1.MarketName = this.goods[tmp].name;
            }
        }
        for (const tmp in this.features) {
            if (this.features[tmp].idFeature === feature) {
                tempTransferUnit1.IdFeature = this.features[tmp].idFeature;
                tempTransferUnit1.Color = this.features[tmp].description;
            }
        }
        tempTransferUnit1.Year = year;
        tempTransferUnit1.Qty = this.qtyRequest;
        this.transferUnitRequests.push(tempTransferUnit1);
        this.transferUnitRequests = [...this.transferUnitRequests];
        console.log('data sekarang :', this.transferUnitRequests);
        this.clearData();
    }

    createRequestNote() {
        if (this.selectedDate == null || this.selectedFacilityTo == null) {
            this.toasterService.showToaster('info', 'Gagal!', 'Lokasi Tujuan dan Tanggal Pengiriman Tidak Boleh Kosong');
        } else {
            if (this.transferUnitRequests.length !== 0) {
                this.loadingService.loadingStart();
                const obj = {
                    IdFacilityTo: this.selectedFacilityTo,
                    IdInternal: this.principal.getIdInternal(),
                    PIC: this.principal.getUserLogin(),
                    DeliveryDate: this.selectedDate,
                    data: this.transferUnitRequests
                }
                this.transferUnitRequestService.createRequestnotePOS(obj).subscribe(
                    (res: ResponseWrapper) => this.onSuccessPos(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                )
            } else {
                this.toasterService.showToaster('info', 'Gagal!', 'Request motor minimal 1');
            }
        }
    }

    private onSuccessPos(data, headers) {
        this.active = true;
        this.transferRes = data;
        this.loadingService.loadingStop();
    }

    DeleteOne(index: any) {
        this.transferUnitRequests.splice(index , 1);
        this.transferUnitRequests = [...this.transferUnitRequests];
    }

    clearData() {
        this.goods = null;
        this.years = null;
        this.features = null;
        this.selectedGood = null;
        this.selectedYear = null;
        this.selectedFeature = null;
        this.selectedFacility = null;
        this.qtyRequest = null;
    }

    transition() {
        this.router.navigate(['/transfer-unit-request'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/transfer-unit-request', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/transfer-unit-request', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.transferUnitRequestService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                         name: 'shipmentOutgoingListModification',
                         content: 'Deleted an shipmentOutgoing'
                    });
                });
            }
        });
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.transferUnitRequestService.update(event.data)
                .subscribe((res: TransferUnitRequest) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.transferUnitRequestService.create(event.data)
                .subscribe((res: TransferUnitRequest) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: TransferUnitRequest) {
        this.toasterService.showToaster('info', 'TransferUnitRequest Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.loadFacility();
        });
        this.registerChangeInTransferUnitRequests();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TransferUnitRequest) {
        return item.id;
    }
    registerChangeInTransferUnitRequests() {
        this.eventSubscriber = this.eventManager.subscribe('transferUnitRequestListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }
    trackFacilityById(index: number, item ) {
        // console.log('index', index);
        // console.log('item', item);
        return item.IdFacility;
    }
    trackGoodById(index: number, item ) {
        // console.log('index', index);
        // console.log('item', item);
        return item.idProduct;
    }
    trackFeatureById(index: number, item ) {
        // console.log('index', index);
        // console.log('item', item);
        return item.idFeature;
    }
    trackYear(index: number, item ) {
        // console.log('index', index);
        // console.log('item', item);
        return item;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.transferUnitRequests = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
