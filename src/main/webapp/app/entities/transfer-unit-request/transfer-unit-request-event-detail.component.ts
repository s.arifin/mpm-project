import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { TransferUnitRequest, TransferUnitRequestMain } from './transfer-unit-request.model';
import { TransferUnitRequestService } from './transfer-unit-request.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LazyLoadEvent } from 'primeng/primeng';
import { CommunicationEvent } from '../communication-event/communication-event.model';

@Component({
    selector: 'jhi-request-event-detail',
    templateUrl: './transfer-unit-request-event-detail.component.html'
})
export class TransferUnitRequestEventDetailComponent implements OnInit, OnDestroy {

    transferUnitRequests: TransferUnitRequest[];
    transferUnitMain: TransferUnitRequestMain;
    transferUnitMainDetail: TransferUnitRequestMain;
    selectedUpdateDtFrom: Date;
    selectedUpdateDtThru: Date;
    minimalDate: Date;
    communicationEvent: CommunicationEvent;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    printBastKeHoModal: boolean;
    perpanjangEvent: boolean;
    isOkay: Boolean = false;

    constructor(
        private transferUnitRequestService: TransferUnitRequestService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.transferUnitMainDetail = new TransferUnitRequestMain();
        this.communicationEvent = new CommunicationEvent();
    }

    loadAll() {
        this.transferUnitRequestService.getDetailEvent({
            query: 'IdReqNot:' + this.transferUnitMain.IdReqNot,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/transfer-unit-request'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/transfer-unit-request', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/transfer-unit-request', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    // previousState() {
    //     this.router.navigate(['./transfer-unit-request-pos-tambah']);
    // }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.transferUnitRequestService.update(event.data)
                .subscribe((res: TransferUnitRequest) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.transferUnitRequestService.create(event.data)
                .subscribe((res: TransferUnitRequest) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: TransferUnitRequest) {
        this.toasterService.showToaster('info', 'TransferUnitRequest Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    ngOnInit() {
        this.transferUnitRequestService.currentData.subscribe(
            (res) => {
                this.transferUnitMain = res;
                this.transferUnitRequestService.queryGetMainDetailDataEvent({
                    query: 'idReqnot:' + this.transferUnitMain.IdReqNot,
                }).subscribe(
                    (data) => {
                        this.transferUnitMainDetail = data.json;
                        this.minimalDate = new Date(this.transferUnitMainDetail.dtRequest)
                        if (this.transferUnitMainDetail.dtFrom < this.transferUnitMainDetail.dtRequest) {
                            this.selectedUpdateDtFrom = new Date(this.transferUnitMainDetail.dtRequest);
                        } else {
                            this.selectedUpdateDtFrom = new Date(this.transferUnitMainDetail.dtFrom);
                        }
                        if (this.transferUnitMainDetail.dtThru < this.selectedUpdateDtFrom) {
                            this.selectedUpdateDtThru = this.selectedUpdateDtFrom;
                        } else {
                            this.selectedUpdateDtThru = new Date(this.transferUnitMainDetail.dtThru);
                        }
                    }
                );
            });
        this.loadAll();
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    validate() {
        if (this.selectedUpdateDtThru >= this.selectedUpdateDtFrom) {
            this.isOkay = true;
        } else {
            this.isOkay = false;
        }
    }

    receiveBAST() {
        this.transferUnitRequestService.receiveBast({
            reqnot: 'IdReqnot:' + this.transferUnitMain.IdReqNot,
           account: 'username:' + this.principal.getUserLogin()
        }).subscribe(
            (res) => {
                this.toasterService.showToaster('info', 'Berhasil', 'BAST diterima');
                this.previousState();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        )
    }

    returnEvent() {
        this.transferUnitRequestService.returnEvent({
            reqnot: 'IdReqnot:' + this.transferUnitMain.IdReqNot,
           account: 'username:' + this.principal.getUserLogin()
        }).subscribe(
            (res) => {
                this.toasterService.showToaster('info', 'Berhasil', 'Return Sukses!');
                this.previousState();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        )
    }

    trackId(index: number, item: TransferUnitRequest) {
        return item.id;
    }
    registerChangeInTransferUnitRequests() {
        this.eventSubscriber = this.eventManager.subscribe('transferUnitRequestListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.transferUnitRequests = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    showKonfirmasi() {
        this.printBastKeHoModal = true;
    }

    showPerpanjangEvent() {
        this.perpanjangEvent = true;
    }

    previousState() {
        this.router.navigate(['./transfer-unit-request-pos']);
    }

    cancelRquest() {
        this.transferUnitRequestService.cancelEvent({
            reqnot: 'IdReqnot:' + this.transferUnitMain.IdReqNot,
           account: 'username:' + this.principal.getUserLogin()
        }).subscribe(
            (res) => {
                this.toasterService.showToaster('info', 'Berhasil', 'Request Pos dicancel');
                this.previousState();
            },
            (res: ResponseWrapper) => this.onError(res.json)
        )
    }

    ExtendEvent() {
        this.transferUnitMainDetail.dtFrom = this.selectedUpdateDtFrom;
        this.transferUnitMainDetail.dtThru = this.selectedUpdateDtThru;
        this.transferUnitRequestService.extendEvent(this.transferUnitMainDetail).subscribe(
            (res) => {
                this.transferUnitMainDetail = res.json;
                this.perpanjangEvent = false;
                this.toasterService.showToaster('info', 'Berhasil', 'Jadwal telah dirubah');
            }
        )
    }
}
