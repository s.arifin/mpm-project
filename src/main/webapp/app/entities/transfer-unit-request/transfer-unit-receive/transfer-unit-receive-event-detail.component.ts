import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { LazyLoadEvent } from 'primeng/primeng';
import { TransferUnitRequest, TransferUnitReceive, TransferUnitRequestMain } from '../transfer-unit-request.model';
import { TransferUnitRequestService } from '../transfer-unit-request.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { ShipmentOutgoingService } from '../../shipment-outgoing/shipment-outgoing.service';
import { ShipmentOutgoingMatching } from '../../shipment-outgoing/shipment-outgoing-matching.model';
import { DriverService } from '../../driver/driver.service'
import { Driver } from '../../driver/driver.model'
import { PickingSlip } from '../../picking-slip/picking-slip.model';
import { ReportUtilService } from '../../../shared/report/report-util.service';

@Component({
    selector: 'jhi-transfer-unit-receive-event-detail',
    templateUrl: './transfer-unit-receive-event-detail.component.html'
})
export class TransferUnitReceiveEventDetailComponent implements OnInit, OnDestroy {

    currentAccount: any;
    shipmentMatching: ShipmentOutgoingMatching;
    selectedDriver: any;
    drivers: Driver[];
    driverInternal: Driver[];
    driverExternal: Driver[];
    otherName: string;
    transferUnitRequests: TransferUnitRequest[];
    transferUnitReceive: TransferUnitReceive[];
    transferUnitReceiveUpdate: TransferUnitReceive;
    pickingSlip: PickingSlip[];
    updatePickingSlip: PickingSlip;
    manualMatchingPS: PickingSlip;
    tempTransUnit: TransferUnitRequestMain;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    printBast: boolean;
    printAcc: boolean;
    manualMatchingAcc: boolean;
    stat: any;
    idDriver: any;
    cetakanBast: boolean;
    cetakanAksesoris: boolean;

    constructor(
        private driverService: DriverService,
        private transferUnitRequestService: TransferUnitRequestService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private reportUtilService: ReportUtilService,
        private shipmentOutgoingService: ShipmentOutgoingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.updatePickingSlip = new PickingSlip();
        this.manualMatchingPS = new PickingSlip();
        this.drivers = new Array<Driver>();
        this.driverExternal = new Array<Driver>();
        this.driverInternal = new Array<Driver>();
        this.otherName = '';
        this.stat = '1';
        this.selectedDriver = null;
        this.cetakanBast = false;
        this.cetakanAksesoris = false;
    }

    loadAll() {
        this.transferUnitRequestService.queryGetDataPicking({
            reqnot: 'IdReqnot:' + this.tempTransUnit.IdReqNot
            }).subscribe(
            (res) => this.pickingSlip = res.json
        );
    }

    status() {
        if (this.stat === '2') {
            this.idDriver = null;
            this.selectedDriver = null;
        } else {
            this.otherName = '';
        }
    }

    loadDriver() {
        this.driverService.getDriverbyInternal({
            query: 'idinternal:' + this.principal.getIdInternal()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccessDriver(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private onSuccessDriver(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        // this.totalItems = headers.get('X-Total-Count');
        // this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.drivers = data;
        // console.log("drivernya=>>>>>>>>>>>>>>",this.drivers);

        for ( let i = 0 ; i < this.drivers.length; i++) {

            if (this.drivers[i].externalDriver === true ) {
                this.driverExternal.push(this.drivers[i]);

            } else if (this.drivers[i].externalDriver === false || this.drivers[i].externalDriver == null) {
                this.driverInternal.push(this.drivers[i]);
            }
        }
         console.log('drivernya', this.driverExternal);
         console.log('drivernyaint', this.driverInternal);
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/transfer-unit-request'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/transfer-unit-request', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        // if (!query) {
        //     return this.clear();
        // }
        // this.page = 0;
        // this.currentSearch = query;
        // this.router.navigate(['/transfer-unit-request', {
        //     search: this.currentSearch,
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        // this.loadAll();
    }

    showPrintBast() {
        this.transferUnitReceiveUpdate = new TransferUnitReceive();
        this.printBast = true;
    }

    showPrintAcc(data: PickingSlip) {
        this.updatePickingSlip = data;
        this.printAcc = true;
    }

    showPrintManual(data: PickingSlip) {
        this.manualMatchingPS = data;
        console.log('data manual Matching :', this.manualMatchingPS);
        this.manualMatchingAcc = true;
        this.shipmentOutgoingService.queryFindMatchingEvent({
            codetype: 'codeType:' + this.manualMatchingPS.idproduct,
            feature: 'idFeature:' + this.manualMatchingPS.idfeature,
            internals: 'idInternal:' + this.principal.getIdInternal(),
            Year: 'yearAssembly:' + this.manualMatchingPS.yeaAssembly
        }).subscribe(
            (res) => {
                this.shipmentMatching = res.json;
                // this.loadingService.loadingStop();
            }
        );
    }

    pilih(data: ShipmentOutgoingMatching) {
        data.idslip = this.manualMatchingPS.idSlip;
        data.oldidframe = this.manualMatchingPS.idframe;
        data.reqnotNumber = this.tempTransUnit.RequestNumber;
        // const obj = {...data, ...{idslip: this.customShipment.idSlip}}
        this.transferUnitRequestService.executeProcessMatching(data).subscribe(
            (res) => {
                this.toasterService.showToaster('success', 'Sucess', 'Manual matching berhasil!');
                this.manualMatchingAcc = false;
                this.loadAll();
            },
            (err) => {
                this.toasterService.showToaster('error', 'Error', err.message);
            }
        )
    }

    doPrint() {
        console.log('Driver int : ', this.selectedDriver);
        console.log('Stat : ', this.stat);
        console.log('OtherName : ', this.otherName);
        if ((this.selectedDriver != null && this.stat === '1') || (this.otherName !== '' && this.stat === '2')) {
            console.log('Masuk pak eko : ');
            if (this.stat === '2' && this.otherName.length < 3) {
                this.toasterService.showToaster('info', 'Error', 'Driver belum dipilih');
            } else {
                if (this.tempTransUnit.Status === 8) {
                    this.transferUnitRequestService.cetakBAST({
                        reqnot: 'IdReqnot:' + this.tempTransUnit.IdReqNot,
                        account: 'username:' + this.principal.getUserLogin(),
                        idDriver: 'IdDriver:' + this.selectedDriver,
                        otherName: 'OtherName:' + this.otherName,
                        stat: 'stat:' + this.stat
                    }).subscribe(
                        (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                        (res: ResponseWrapper) => this.onError(res.json)
                    )
                } else {
                    this.onSuccess(null, null)
                }
            }
        } else {
            if (this.tempTransUnit.Status === 8) {
                this.toasterService.showToaster('info', 'Error', 'Driver belum dipilih');
            } else {
                this.onSuccess(null, null)
            }
        }
    }

    previousState() {
        this.router.navigate(['./transfer-unit-request-pos']);
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.transferUnitRequestService.update(event.data)
                .subscribe((res: TransferUnitRequest) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.transferUnitRequestService.create(event.data)
                .subscribe((res: TransferUnitRequest) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: TransferUnitRequest) {
        this.toasterService.showToaster('info', 'TransferUnitRequest Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    loadDataLazy(event: LazyLoadEvent) {
        // this.itemsPerPage = event.rows;
        // this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        // this.previousPage = this.page;

        // if (event.sortField !== undefined) {
        //     this.predicate = event.sortField;
        //     this.reverse = event.sortOrder;
        // }
        // this.loadAll();
    }
    ngOnInit() {
        this.transferUnitRequestService.passingData.subscribe(
            (data) => this.tempTransUnit = data
        );
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTransferUnitRequests();
        this.loadDriver();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackDriverById(index: number, item: Driver) {
        // console.log('index', index);
        // console.log('item', item);
        return item.idPartyRole;
    }

    trackId(index: number, item: TransferUnitRequest) {
        return item.id;
    }
    registerChangeInTransferUnitRequests() {
        this.eventSubscriber = this.eventManager.subscribe('transferUnitRequestListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    updatePicking() {
        this.printAcc = false;
        this.transferUnitRequestService.executeUpdatePickingSlip(this.updatePickingSlip).subscribe(
            (res) => {
                this.eventManager.broadcast({ name: 'PickingSlipListModification', content: 'OK'}),
                this.toasterService.showToaster('info', 'Save', 'Aksesoris telah diupdate!')
                this.loadAll();
            },
            (res) => {
                this.toasterService.showToaster('warning', 'Update Gagal!', res.message);
                this.alertService.error(res.message, null, null);
            }
        );
    }

    private onSuccess(data, headers) {
        this.toasterService.showToaster('info', 'Berhasil', 'Cetak BAST');
        if (this.cetakanBast) {
            this.reportUtilService.viewFile('/api/report/bast_unit_ke_event/pdf', { no_reqnot: this.tempTransUnit.RequestNumber });
        }
        if (this.cetakanAksesoris) {
            this.reportUtilService.viewFile('/api/report/bast_accesories_pos_event/pdf', { no_reqnot: this.tempTransUnit.RequestNumber });
        }
        this.router.navigate(['../transfer-unit-receive-pos']);
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
