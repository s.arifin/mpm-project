import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ConfirmationService } from 'primeng/primeng';
import { CustomShipment, ShipmentOutgoing, ShipmentOutgoingService } from '../../shipment-outgoing';
import { VehicleSalesOrder, VehicleSalesOrderService } from '../../vehicle-sales-order';
import { OrderItem } from '../../shared-component/entity/order-item.model';
import * as ShipmentOutgoingConstrants from '../../../shared/constants/shipmentOutgoing.constrants'
import { PickingSlip, PickingSlipService } from '../../picking-slip';
import { OrderItemService } from '../../order-item';
import { RequirementService } from '../../requirement';
import { DeliveryOrderService } from '../../delivery-order';
import { Principal, ToasterService, ITEMS_PER_PAGE, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { TransferUnitRequestService } from '../transfer-unit-request.service';
import { TransferUnitRequestMain, TransferUnitRequest } from '../transfer-unit-request.model';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
    selector: 'jhi-transfer-unit-receive-gudang-detail',
    templateUrl: './transfer-unit-receive-gudang-detail.component.html'
})

export class TransferUnitReceiveGudangDetailComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    deliver: any[];
    currentSearch: string;
    shipmentTMP: CustomShipment;
    shipmentOutgoing: ShipmentOutgoing;
    vehicleSalesOrder: VehicleSalesOrder;
    IdReqNot: any;
    idShipment: string = null;
    idSlip: string = null;
    orderItem: OrderItem[];
    pickingSlip: PickingSlip;
    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    transferUnitRequestMain: TransferUnitRequestMain;
    transferUnitRequest: TransferUnitRequest[];
    psTemp: PickingSlip;
    psCheck: boolean;

    constructor(
        private transferUnitRequestService: TransferUnitRequestService,
        protected orderItemService: OrderItemService,
        protected vehicleSalesOrderService: VehicleSalesOrderService,
        protected requirementService: RequirementService,
        protected shipmentOutgoingService: ShipmentOutgoingService,
        protected deliveryOrderService: DeliveryOrderService,
        protected pickingSlipService: PickingSlipService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.orderItem = new Array<OrderItem>();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            // console.log('data page', data['pagingParams']);
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.deliver = [{}];
        this.transferUnitRequestMain = new TransferUnitRequestMain();
        this.psCheck = true;
    }

    ngOnInit() {
        // this.transferUnitRequestService.passingData.subscribe((data) => this.transferUnitRequestMain = data);
        this.subscription = this.activatedRoute.params.subscribe(
            (params) => {
                if (params['IdReqNot']) {
                    this.IdReqNot = params['IdReqNot'];
                }
                console.log('SINI COYYYYYY : ', + this.IdReqNot)
                this.transferUnitRequestService.getMainPosGudang({
                    query: 'idreqnot:' + this.IdReqNot
                }).subscribe((data) => {
                    this.transferUnitRequestMain = data.json;
                    this.loadAll();
                })
            }
        )
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    loadAll() {
        this.transferUnitRequestService.queryDetailGudang({
            page: this.page - 1,
            query: 'idReqNot:' + this.transferUnitRequestMain.IdReqNot,
            internals: 'idInternal:' + this.principal.getIdInternal(),
            size: this.itemsPerPage}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    private onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.transferUnitRequest = data;
    }

    ngOnDestroy() {
    }

    protected onSuccessOrderItem(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        console.log('orderItem', data);
        this.orderItem = data;
    }

    gotoDetail(rowData): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                this.transferUnitRequestService.queryPickingSlip({
                    query: 'IdReqnotDest:' + rowData.IdReqnotDest
                }).subscribe(
                    (data) => {
                        this.pickingSlip = data.json;
                        console.log('kirim detail',  this.pickingSlip);
                        this.transferUnitRequestService.changeShareDataTmp(this.pickingSlip);
                        this.transferUnitRequestService.passingCustomData(rowData);
                        this.router.navigate(['../transfer-unit-receive-gudang-check']);
                    }
                )
            }
        )
    }
    selesaiGudang() {
        this.transferUnitRequestService.queryCheckPickingSlip({
            query: 'IdReqnot:' + this.IdReqNot
        }).subscribe(
            (data) => {
                if (data.json) {
                    this.transferUnitRequestService.changeShipmentStatus({
                        query: 'IdReqnot:' + this.transferUnitRequestMain.IdReqNot,
                        account: 'username:' + this.principal.getUserLogin()
                    }).subscribe(
                        (res) => {
                            this.toasterService.showToaster('info', 'Save', 'Berhasil !');
                            this.router.navigate(['../transfer-unit-receive-gudang']);
                        },
                        (res) => {
                            this.toasterService.showToaster('error', 'Error!!', res.message);
                            this.alertService.error(res.message, null, null);
                        }
                    );
                } else {
                    this.toasterService.showToaster('error', 'Gagal!!', 'Mekanik belum dipilih');
                }
            }
        )
    }

    // selesaiGudang(): Promise<any> {
    //     return new Promise<any>(
    //         (resolve) => {
    //             this.pickingSlipService.find(this.shipmentTMP.idSlip).subscribe(
    //                 (datapicking) => {
    //                     if (datapicking.mechanicexternal !== null || datapicking.mechanicinternal !== null ) {
    //                         this.shipmentOutgoingService.find(this.idShipment).subscribe(
    //                             (data) => {
    //                                 this.shipmentOutgoing = data;
    //                                 this.dokirimGudang(this.shipmentOutgoing).then(
    //                                     () => {
    //                                         this.router.navigate(['delivery-order']);
    //                                     }
    //                                 )
    //                             }
    //                         )
    //                     }else {
    //                         this.toasterService.showToaster('warning', 'Data Tidak Lengkap', 'Mekanik Belum Dipilih');
    //                     }
    //                 }
    //             )
    //         }
    //     )
    // }

    dokirimGudang(datashipment): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                this.shipmentOutgoingService.executeProcess(ShipmentOutgoingConstrants.COMPLETE_GUDANG, null, datashipment ).subscribe(
                    (value) => console.log('this: ', value),
                    (err) => console.log(err),
                    () => {
                        this.eventManager.broadcast({
                            name: 'PickingSlipListModification',
                            content: 'Sent From Werehouse'
                       });
                        // console.log('harus selesai kirim gudang dulu');
                        // this.loadAll();
                        resolve()
                    }
                );
            }
        )
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    public checkIdMachine(): boolean {
        let a: boolean;
        a = false;
        if (this.vehicleSalesOrder.salesUnitRequirement.idmachine === null || this.vehicleSalesOrder.salesUnitRequirement.idmachine === undefined) {
            a = true;
        }

        return a;
    }

    // previousState() {
    //     this.shipmentTMP = {};
    //     this.shipmentOutgoingService.changeShareDataTmp(this.shipmentTMP);
    //     this.router.navigate(['delivery-order']);
    // }
}
