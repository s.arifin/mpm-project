import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import {
    LazyLoadEvent,
    ConfirmationService
} from 'primeng/primeng';
import { PickingSlip, PickingSlipService } from '../../picking-slip';
import { Mechanic, MechanicService } from '../../mechanic';
import { VehicleSalesOrder, VehicleSalesOrderService } from '../../vehicle-sales-order';
import { CustomShipment, ShipmentOutgoingService } from '../../shipment-outgoing';
import { OrderItemService } from '../../order-item';
import { DeliveryOrderService } from '../../delivery-order';
import { Principal, ToasterService, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { TransferUnitRequestService } from '../transfer-unit-request.service';
import { TransferUnitRequestMain, TransferUnitRequest } from '../transfer-unit-request.model';

@Component({
    selector: 'jhi-transfer-unit-receive-gudang-check',
    templateUrl: './transfer-unit-receive-gudang-check.component.html'
})

export class TransferUnitReceiveGudangCheckComponent implements OnInit {

    protected subscription: Subscription;

    transferUnitRequest: TransferUnitRequest;
    pickingSlip: PickingSlip;
    // shipmentTMP: PickingSlip;
    mechanicInternal: Mechanic[];
    mechanicExternal: Mechanic[];
    vehicleSalesOrder: VehicleSalesOrder;
    customShipment: CustomShipment;
    idOrder: string;
    idSlip: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    stat: number;

    constructor(
        protected transferUnitRequestService: TransferUnitRequestService,
        protected orderItemService: OrderItemService,
        protected shipmentOutgoingService: ShipmentOutgoingService,
        protected deliveryOrderService: DeliveryOrderService,
        protected mechanicService: MechanicService,
        protected pickingSlipService: PickingSlipService,
        protected vehicleSalesOrderService: VehicleSalesOrderService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.mechanicExternal = new Array <Mechanic>();
        this.mechanicInternal = new Array <Mechanic>();
        this.stat = 1;
        this.idOrder = null;
    }

    ngOnInit() {
        this.transferUnitRequestService.currentData.subscribe(
            (shadedata) => {
                this.pickingSlip = shadedata;
            });
        this.transferUnitRequestService.passingData.subscribe(
            (transfer) => {
                this.transferUnitRequest = transfer;
            });
        this.mechanicService.getMechanicbyInternal({
            query : 'idinternal:' + this.principal.getIdInternal()
        })
        .subscribe(
            (res: ResponseWrapper) => this.mechanicType(res.json)
            , (res: ResponseWrapper) => this.onError(res.json));
        console.log('shipmentTMP', this.pickingSlip);
        this.loadAll();
    }
    loadAll() {
    }
    public update() {
        this.transferUnitRequestService.executeUpdatePickingSlip(this.pickingSlip).subscribe(
            (res) => {
                this.eventManager.broadcast({ name: 'PickingSlipListModification', content: 'OK'}),
                this.toasterService.showToaster('info', 'Save', 'ShipmentOutgoing saved !')
            },
            (res) => {
                this.toasterService.showToaster('warning', 'data changed', res.message);
                this.alertService.error(res.message, null, null);
            }
        )
        this.router.navigate([`../transfer-unit-receive-gudang/` + this.transferUnitRequest.IdReqnot + `/transfer-unit-receive-gudang-detail`]);
     }

     protected subscribeToSaveResponse(result: Observable<PickingSlip>): void {
         result.subscribe(
            (res: PickingSlip) => this.onSaveSuccess(),
            (res: Response) => this.onSaveError(res)
        );
     }

     protected onSaveSuccess(): void {
         this.eventManager.broadcast({ name: 'ShipmentOutgoingListModification', content: 'OK'});
         this.toasterService.showToaster('info', 'Save', 'ShipmentOutgoing saved !');
        // this.kirimGudang(this.customShipment);
     }
     protected onSaveError(error): void {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    protected onSuccess(data): void {
        console.log('datanyadelivery', data);
        // data.dateSchedulle = new Date(data.dateSchedulle)
        this.pickingSlip = data;
    }

    protected onError(error): void {
        this.toasterService.showToaster('warning', 'data changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    public previousState(): void {
        this.router.navigate([`../transfer-unit-receive-gudang/` + this.transferUnitRequest.IdReqnot + `/transfer-unit-receive-gudang-detail`]);
    }

    protected mechanicType(data): void {
        console.log('mekanik', data);
        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                if ( data[i].external === 'true') {
                    this.mechanicExternal.push(data[i]);
                } else {
                    this.mechanicInternal.push(data[i]);
                }
            }
        }
    }

    public status(): void {
        if (this.stat === 1) {
            this.pickingSlip.mechanicinternal = '';
        } else {
            this.pickingSlip.mechanicexternal = '';
        }
    }

    public trackMechanicById(index: number, item: Mechanic): string {
        return item.idMechanic;
    }
}
