import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { LazyLoadEvent } from 'primeng/primeng';
import { FacilityService } from '../../facility/facility.service';
import { Facility } from '../../facility/facility.model';
import { TransferUnitRequest, TransferUnitReceive, TransferUnitRequestMain } from '../transfer-unit-request.model';
import { TransferUnitRequestService } from '../transfer-unit-request.service';
import { Principal, ITEMS_PER_PAGE, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { LoadingService } from '../../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-transfer-unit-receive-pos',
    templateUrl: './transfer-unit-receive-pos.component.html'
})
export class TransferUnitReceivePosComponent implements OnInit, OnDestroy {

currentAccount: any;
    transferUnitRequests: TransferUnitRequestMain[];
    transferUnitReceive: TransferUnitReceive[];
    facilities: Facility[];
    tempTransUnit: TransferUnitRequestMain;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    searching: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    idStatusType: number;
    filterStatus: any;
    filterDateFrom: Date;
    filterDateThru: Date;
    selectedFacility: any;
    convertedDateFrom: any;
    convertedDateThru: any;

    constructor(
        private transferUnitRequestService: TransferUnitRequestService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private loadingService: LoadingService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private facilityService: FacilityService,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.idStatusType = 8;
        this.filterStatus = [
            {statusId: 8, statusDeskripsi: 'Approve'},
            {statusId: 16, statusDeskripsi: 'Inventory Transfer'},
            {statusId: 17, statusDeskripsi: 'Complete'},
        ];
        this.filterDateFrom = null;
        this.filterDateThru = null;
        this.selectedFacility = null;
        this.convertedDateFrom = null;
        this.convertedDateThru = null;
        this.searching = 'false';
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.filterDateFrom !== null) {
            this.convertedDateFrom = this.filterDateFrom.toJSON();
        }
        if (this.filterDateThru !== null) {
            this.convertedDateThru = this.filterDateThru.toJSON();
        }
        this.transferUnitRequestService.queryGetDataBast({
            page: this.page,
            size: this.itemsPerPage,
            status: this.idStatusType,
            filterLocation: this.selectedFacility,
            filterDtFrom: this.convertedDateFrom,
            filterDtThru: this.convertedDateThru,
            checkSearching: this.searching,
            querySearch: this.currentSearch,
            query: 'idinternal:' + this.principal.getIdInternal(),
            account: 'username:' + this.principal.getUserLogin(),
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/transfer-unit-receive-pos'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.previousPage = 0;
        this.currentSearch = '';
        this.router.navigate(['/transfer-unit-receive-pos', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.searching = 'false';
        this.loadAll();
    }
    search(query) {
        this.page = 0;
        this.previousPage = 0;
        this.searching = 'true';
        this.loadAll();
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.loadFacility();
        });
        this.registerChangeInTransferUnitRequests();
    }

    loadFacility() {
        this.facilityService.queryFilterByinternalC({
            query: 'idinternal:' + this.principal.getIdInternal()
        }).subscribe(
            (res: ResponseWrapper) => {
                this.facilities = res.json;
            }
        );
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TransferUnitRequest) {
        return item.id;
    }
    registerChangeInTransferUnitRequests() {
        // this.eventSubscriber = this.eventManager.subscribe('transferUnitRequestListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    gotoDetail(rowdata) {
        this.transferUnitRequestService.passingCustomData(rowdata);
    }

    private onSuccess(data, headers) {
        this.transferUnitRequests = null;
        this.loadingService.loadingStop();
        this.queryCount = 0;
        this.totalItems = 0;
        this.queryCount = data[0].TotalData;
        this.totalItems = data[0].TotalData;
        this.transferUnitRequests = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackStatusType(index: number, item ) {
        return item.statusId;
    }

    trackFacilityById(index: number, item ) {
        return item.statusId;
    }
    executeFilter() {
        this.page = 0;
        this.previousPage = 0;
        this.transferUnitRequests = null;
        this.loadAll();
    }
}
