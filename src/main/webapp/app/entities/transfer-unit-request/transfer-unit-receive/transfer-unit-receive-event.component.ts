import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { TransferUnitRequest, TransferUnitRequestMain, TransferUnitReceive } from '../transfer-unit-request.model';
import { TransferUnitRequestService } from '../transfer-unit-request.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { LazyLoadEvent } from 'primeng/primeng';
import { LoadingService } from '../../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-transfer-unit-receive-event',
    templateUrl: './transfer-unit-receive-event.component.html'
})
export class TransferUnitReceiveEventComponent implements OnInit, OnDestroy {

    currentAccount: any;
    transferUnitRequests: TransferUnitRequestMain[];
    transferUnitReceive: TransferUnitReceive[];
    tempTransUnit: TransferUnitRequestMain;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    searching: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    idStatusType: number;
    filterStatus: any;
    filterDateFrom: Date;
    filterDateThru: Date;
    filterFromMulai: Date;
    filterThruMulai: Date;
    filterFromSelesai: Date;
    filterThruSelesai: Date;
    selectedFacility: any;
    convertedDateFrom: any;
    convertedDateThru: any;
    convertedFromMulai: any;
    convertedThruMulai: any;
    convertedFromSelesai: any;
    convertedThruSelesai: any;

    constructor(
        private transferUnitRequestService: TransferUnitRequestService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private loadingService: LoadingService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.idStatusType = 8;
        this.filterStatus = [
            {statusId: 8, statusDeskripsi: 'Approve'},
            {statusId: 16, statusDeskripsi: 'Inventory Transfer'},
            {statusId: 18, statusDeskripsi: 'Complete'},
            {statusId: 17, statusDeskripsi: 'Complete Returned'}
        ];
        this.filterDateFrom = null;
        this.filterDateThru = null;
        this.filterFromMulai = null;
        this.filterThruMulai = null;
        this.filterFromSelesai = null;
        this.filterThruSelesai = null;
        this.selectedFacility = null;
        this.convertedDateFrom = null;
        this.convertedDateThru = null;
        this.convertedFromMulai = null;
        this.convertedThruMulai = null;
        this.convertedFromSelesai = null;
        this.convertedThruSelesai = null;
        this.searching = 'false';
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.filterDateFrom !== null) {
            this.convertedDateFrom = this.filterDateFrom.toJSON();
        }
        if (this.filterDateThru !== null) {
            this.convertedDateThru = this.filterDateThru.toJSON();
        }
        if (this.filterFromMulai !== null) {
            this.convertedFromMulai = this.filterFromMulai.toJSON();
        }
        if (this.filterThruMulai !== null) {
            this.convertedThruMulai = this.filterThruMulai.toJSON();
        }
        if (this.filterFromSelesai !== null) {
            this.convertedFromSelesai = this.filterFromSelesai.toJSON();
        }
        if (this.filterThruSelesai !== null) {
            this.convertedThruSelesai = this.filterThruSelesai.toJSON();
        }
        this.transferUnitRequestService.queryGetDataBastEvent({
            page: this.page,
            size: this.itemsPerPage,
            status: this.idStatusType,
            query: 'idinternal:' + this.principal.getIdInternal(),
            account: 'username:' + this.principal.getUserLogin(),
            filterDtFrom: this.convertedDateFrom,
            filterDtThru: this.convertedDateThru,
            filterFromMulai: this.convertedFromMulai,
            filterThruMulai: this.convertedThruMulai,
            filterFromSelesai: this.convertedFromSelesai,
            filterThruSelesai: this.convertedThruSelesai,
            checkSearching: this.searching,
            querySearch: this.currentSearch,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    clear() {
        this.page = 0;
        this.previousPage = 0;
        this.currentSearch = '';
        this.router.navigate(['/transfer-unit-receive-pos', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.searching = 'false';
        this.loadAll();
    }
    search(query) {
        this.page = 0;
        this.previousPage = 0;
        this.searching = 'true';
        this.loadAll();
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/transfer-unit-request'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.transferUnitRequestService.update(event.data)
                .subscribe((res: TransferUnitRequest) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.transferUnitRequestService.create(event.data)
                .subscribe((res: TransferUnitRequest) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: TransferUnitRequest) {
        this.toasterService.showToaster('info', 'TransferUnitRequest Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTransferUnitRequests();
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TransferUnitRequest) {
        return item.id;
    }
    registerChangeInTransferUnitRequests() {
        this.eventSubscriber = this.eventManager.subscribe('transferUnitRequestListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.transferUnitRequests = null;
        this.loadingService.loadingStop();
        this.queryCount = 0;
        this.totalItems = 0;
        this.queryCount = data[0].TotalData;
        this.totalItems = data[0].TotalData;
        this.transferUnitRequests = data;
    }

    gotoDetail(rowdata) {
        this.transferUnitRequestService.passingCustomData(rowdata);
    }

    private onError(error) {
        // this.alertService.error(error.message, null, null);
    }

    executeFilter() {
        this.page = 0;
        this.previousPage = 0;
        this.transferUnitRequests = null;
        this.loadAll();
    }

    trackStatusType(index: number, item ) {
        return item.statusId;
    }
}
