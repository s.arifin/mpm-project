import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ShipmentOutgoingMatching } from '../shipment-outgoing/shipment-outgoing-matching.model';

import { TransferUnitRequest, TransferUnitRequestMain } from './transfer-unit-request.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TransferUnitRequestService {
    valueTmp = {};
    protected dataSource = new BehaviorSubject<any>(this.valueTmp);
    currentData = this.dataSource.asObservable();

    private resourceUrl = SERVER_API_URL + 'api/transfer-unit-requests';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/transfer-unit-requests';
    protected resourceCUrl = process.env.API_C_URL + '/api/transfer-unit-requests';
    protected resourceCUrl2 = process.env.API_C_URL + '/api/transfer-unit-receive-gudang';
    protected dummy = 'http://localhost:52374/api/transfer-unit-requests';
    protected dummy2 = 'http://localhost:52374/api/transfer-unit-receive-gudang';

    valueUnitDocumentMessage = {};
    protected enheritancedata = new BehaviorSubject<any>(this.valueUnitDocumentMessage);
    passingData = this.enheritancedata.asObservable();
    constructor(private http: Http) { }

    executeProcessMatching(query?: ShipmentOutgoingMatching): Observable<ResponseWrapper> {
        const copy = JSON.stringify(query);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceCUrl2}/executeMatching`, copy, options).map((res: Response) => this.convertResponse(res));
    }

    create(transferUnitRequest: TransferUnitRequest): Observable<TransferUnitRequest> {
        const copy = this.convert(transferUnitRequest);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(transferUnitRequest: TransferUnitRequest): Observable<TransferUnitRequest> {
        const copy = this.convert(transferUnitRequest);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<TransferUnitRequest> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceCUrl + '/GetMainDataPOS'}`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryEvent(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceCUrl + '/GetMainDataEvent'}`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    getDetailPos(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceCUrl + '/GetMainDataDetailPOS'}`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    getDetailEvent(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceCUrl + '/GetMainDataDetailEvent'}`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(transferUnitRequest: TransferUnitRequest): TransferUnitRequest {
        const copy: TransferUnitRequest = Object.assign({}, transferUnitRequest);
        return copy;
    }

    private convertUnitMain(transferUnitRequest: TransferUnitRequestMain): TransferUnitRequestMain {
        const copy: TransferUnitRequestMain = Object.assign({}, transferUnitRequest);
        return copy;
    }

    passingCustomData(custom) {
        this.enheritancedata.next(custom);
    }

    createRequestnotePOS(requetNotePos: any): Observable<ResponseWrapper> {
        const copy = JSON.stringify(requetNotePos);
        const options: BaseRequestOptions = new BaseRequestOptions();
        // const params: URLSearchParams = new URLSearchParams();
        // params.set('query', copy);
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceCUrl + '/createPOS'}`, copy, options).map((res: Response) => this.convertResponse(res));
    }

    changeShareDataTmp(datatmp) {
        console.log('surnya service', datatmp)
        this.dataSource.next(datatmp);
    }

    executeListProcessAcceptPOS(unitDocumentMessage: any): Observable<ResponseWrapper> {
        const copy = JSON.stringify(unitDocumentMessage);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl}/acceptRequestPOS`, copy, options)
        .map((res: Response) => this.convertResponse(res));
    }

    executeListProcessAcceptEvent(unitDocumentMessage: any): Observable<ResponseWrapper> {
        const copy = JSON.stringify(unitDocumentMessage);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl}/acceptRequestEvent`, copy, options)
        .map((res: Response) => this.convertResponse(res));
    }

    queryGetDataGudang(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl2}/Recive`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryDetailGudang(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl2}/detail`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryPickingSlip(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        return this.http.get(`${this.resourceCUrl2}/PickingSlip`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    executeUpdatePickingSlip(pickingSlip: any): Observable<ResponseWrapper> {
        const copy = JSON.stringify(pickingSlip);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceCUrl2}/updatePickingSlip`, copy, options)
        .map((res: Response) => this.convertResponse(res));
    }

    changeShipmentStatus(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        return this.http.get(`${this.resourceCUrl2}/changeShipmentStatus`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryGetDataBast(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl2}/GetMainDataPOS`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryGetDataBastEvent(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl2}/GetMainDataEvent`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryGetMainDetailDataEvent(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/GetMaindDetailDataEvent`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryGetDataPicking(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl2}/GetMainDataPicking`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    cetakBAST(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        return this.http.get(`${this.resourceCUrl2}/cetakBAST`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    getMainPosGudang(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        return this.http.get(`${this.resourceCUrl2}/GetMainPos`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    receiveBast(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        return this.http.get(`${this.resourceCUrl}/receiveBAST`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    returnEvent(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        return this.http.get(`${this.resourceCUrl}/returnEvent`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    cancelPos(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        return this.http.get(`${this.resourceCUrl}/cancelPos`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    cancelEvent(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        return this.http.get(`${this.resourceCUrl}/cancelEvent`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    extendEvent(requetNotePos: any): Observable<ResponseWrapper> {
        const copy = this.convertUnitMain(requetNotePos);
        return this.http.post(`${this.resourceCUrl}/UpdateExtendEvent`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryCheckPickingSlip(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        return this.http.get(`${this.resourceCUrl2}/CheckPickingSlip`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    createRequestnoteEvent(requetNotePos: any): Observable<ResponseWrapper> {
        const copy = JSON.stringify(requetNotePos);
        const options: BaseRequestOptions = new BaseRequestOptions();
        // const params: URLSearchParams = new URLSearchParams();
        // params.set('query', copy);
        options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceCUrl + '/createEvent'}`, copy, options).map((res: Response) => this.convertResponse(res));
    }
}
