import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TransferUnitRequest } from './transfer-unit-request.model';
import { TransferUnitRequestPopupService } from './transfer-unit-request-popup.service';
import { TransferUnitRequestService } from './transfer-unit-request.service';

@Component({
    selector: 'jhi-transfer-unit-request-delete-dialog',
    templateUrl: './transfer-unit-request-delete-dialog.component.html'
})
export class TransferUnitRequestDeleteDialogComponent {

    transferUnitRequest: TransferUnitRequest;

    constructor(
        private transferUnitRequestService: TransferUnitRequestService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.transferUnitRequestService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'transferUnitRequestListModification',
                content: 'Deleted an transferUnitRequest'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-transfer-unit-request-delete-popup',
    template: ''
})
export class TransferUnitRequestDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private transferUnitRequestPopupService: TransferUnitRequestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.transferUnitRequestPopupService
                .open(TransferUnitRequestDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
