import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TransferUnitRequest } from './transfer-unit-request.model';
import { TransferUnitRequestService } from './transfer-unit-request.service';

@Component({
    selector: 'jhi-transfer-unit-request-detail',
    templateUrl: './transfer-unit-request-detail.component.html'
})
export class TransferUnitRequestDetailComponent implements OnInit, OnDestroy {

    transferUnitRequest: TransferUnitRequest;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private transferUnitRequestService: TransferUnitRequestService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTransferUnitRequests();
    }

    load(id) {
        this.transferUnitRequestService.find(id).subscribe((transferUnitRequest) => {
            this.transferUnitRequest = transferUnitRequest;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTransferUnitRequests() {
        this.eventSubscriber = this.eventManager.subscribe(
            'transferUnitRequestListModification',
            (response) => this.load(this.transferUnitRequest.id)
        );
    }
}
