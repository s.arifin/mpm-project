import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {ContactMechanism} from './contact-mechanism.model';
import {ContactMechanismPopupService} from './contact-mechanism-popup.service';
import {ContactMechanismService} from './contact-mechanism.service';
import {ToasterService} from '../../shared';
import { Facility, FacilityService } from '../facility';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-contact-mechanism-dialog',
    templateUrl: './contact-mechanism-dialog.component.html'
})
export class ContactMechanismDialogComponent implements OnInit {

    contactMechanism: ContactMechanism;
    isSaving: boolean;

    facilities: Facility[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected contactMechanismService: ContactMechanismService,
        protected facilityService: FacilityService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.contactMechanism.idContact !== undefined) {
            this.subscribeToSaveResponse(
                this.contactMechanismService.update(this.contactMechanism));
        } else {
            this.subscribeToSaveResponse(
                this.contactMechanismService.create(this.contactMechanism));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ContactMechanism>) {
        result.subscribe((res: ContactMechanism) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ContactMechanism) {
        this.eventManager.broadcast({ name: 'contactMechanismListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'contactMechanism saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'contactMechanism Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-contact-mechanism-popup',
    template: ''
})
export class ContactMechanismPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected contactMechanismPopupService: ContactMechanismPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.contactMechanismPopupService
                    .open(ContactMechanismDialogComponent as Component, params['id']);
            } else {
                this.contactMechanismPopupService
                    .open(ContactMechanismDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
