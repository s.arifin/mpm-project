import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ContactMechanism } from './contact-mechanism.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ContactMechanismService {
    protected itemValues: ContactMechanism[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/contact-mechanisms';
    protected resourceSearchUrl = 'api/_search/contact-mechanisms';

    constructor(protected http: Http) { }

    create(contactMechanism: ContactMechanism): Observable<ContactMechanism> {
        const copy = this.convert(contactMechanism);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(contactMechanism: ContactMechanism): Observable<ContactMechanism> {
        const copy = this.convert(contactMechanism);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<ContactMechanism> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, contactMechanism: ContactMechanism): Observable<ContactMechanism> {
        const copy = this.convert(contactMechanism);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, contactMechanisms: ContactMechanism[]): Observable<ContactMechanism[]> {
        const copy = this.convertList(contactMechanisms);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(contactMechanism: ContactMechanism): ContactMechanism {
        if (contactMechanism === null || contactMechanism === {}) {
            return {};
        }
        // const copy: ContactMechanism = Object.assign({}, contactMechanism);
        const copy: ContactMechanism = JSON.parse(JSON.stringify(contactMechanism));
        return copy;
    }

    protected convertList(contactMechanisms: ContactMechanism[]): ContactMechanism[] {
        const copy: ContactMechanism[] = contactMechanisms;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ContactMechanism[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
