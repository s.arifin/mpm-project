import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ContactMechanismComponent } from './contact-mechanism.component';
import { ContactMechanismPopupComponent } from './contact-mechanism-dialog.component';

@Injectable()
export class ContactMechanismResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idContact,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const contactMechanismRoute: Routes = [
    {
        path: 'contact-mechanism',
        component: ContactMechanismComponent,
        resolve: {
            'pagingParams': ContactMechanismResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.contactMechanism.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const contactMechanismPopupRoute: Routes = [
    {
        path: 'contact-mechanism-new',
        component: ContactMechanismPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.contactMechanism.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'contact-mechanism/:id/edit',
        component: ContactMechanismPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.contactMechanism.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
