import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SalesTargetComponent } from './sales-target.component';
import { SalesTargetDetailComponent } from './sales-target-detail.component';
import { SalesTargetPopupComponent } from './sales-target-dialog.component';
import { SalesTargetDeletePopupComponent } from './sales-target-delete-dialog.component';

@Injectable()
export class SalesTargetResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const salesTargetRoute: Routes = [
    {
        path: 'sales-target',
        component: SalesTargetComponent,
        resolve: {
            'pagingParams': SalesTargetResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesTarget.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'sales-target/:id',
        component: SalesTargetDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesTarget.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const salesTargetPopupRoute: Routes = [
    {
        path: 'sales-target-new',
        component: SalesTargetPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesTarget.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sales-target/:id/edit',
        component: SalesTargetPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesTarget.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sales-target/:id/delete',
        component: SalesTargetDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesTarget.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
