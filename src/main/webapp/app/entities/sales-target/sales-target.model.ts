import { BaseEntity } from './../../shared';

export class SalesTarget implements BaseEntity {
    constructor(
        public id?: number,
        public idslstrgt?: any,
        public idsalesman?: any,
        public internal?: string,
        public targetsales?: number,
        public dtfrom?: Date,
        public dtthru?: Date,
        public selectedKorsal?: string,
        public selectedSalesman?: string,
        public namaSales?: string
    ) {
    }
}
