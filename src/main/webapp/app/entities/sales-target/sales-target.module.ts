import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    SalesTargetService,
    SalesTargetPopupService,
    SalesTargetComponent,
    SalesTargetDetailComponent,
    SalesTargetDialogComponent,
    SalesTargetPopupComponent,
    SalesTargetDeletePopupComponent,
    SalesTargetDeleteDialogComponent,
    salesTargetRoute,
    salesTargetPopupRoute,
    SalesTargetResolvePagingParams,
} from './';

import {
    CheckboxModule,
    InputTextModule,
    InputTextareaModule,
    CalendarModule,
    DropdownModule,
    EditorModule,
    ButtonModule,
    DataTableModule,
    DataListModule,
    DataGridModule,
    DataScrollerModule,
    CarouselModule,
    PickListModule,
    PaginatorModule,
    DialogModule,
    ConfirmDialogModule,
    ConfirmationService,
    FileUploadModule,
    GrowlModule,
    SharedModule,
    AccordionModule,
    TabViewModule,
    FieldsetModule,
    ScheduleModule,
    PanelModule,
    ListboxModule,
    ChartModule,
    DragDropModule,
    LightboxModule
   } from 'primeng/primeng';

const ENTITY_STATES = [
    ...salesTargetRoute,
    ...salesTargetPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        CalendarModule,
    ],
    declarations: [
        SalesTargetComponent,
        SalesTargetDetailComponent,
        SalesTargetDialogComponent,
        SalesTargetDeleteDialogComponent,
        SalesTargetPopupComponent,
        SalesTargetDeletePopupComponent,
    ],
    entryComponents: [
        SalesTargetComponent,
        SalesTargetDialogComponent,
        SalesTargetPopupComponent,
        SalesTargetDeleteDialogComponent,
        SalesTargetDeletePopupComponent,
    ],
    providers: [
        SalesTargetService,
        SalesTargetPopupService,
        SalesTargetResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmSalesTargetModule {}
