import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { SalesTarget } from './sales-target.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class SalesTargetService {

    private resourceUrl = SERVER_API_URL + 'api/sales-targets';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/sales-targets';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(salesTarget: SalesTarget): Observable<SalesTarget> {
        const copy = this.convert(salesTarget);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(salesTarget: SalesTarget): Observable<SalesTarget> {
        const copy = this.convert(salesTarget);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<SalesTarget> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dtfrom = this.dateUtils
            .convertDateTimeFromServer(entity.dtfrom);
        entity.dtthru = this.dateUtils
            .convertDateTimeFromServer(entity.dtthru);
    }

    private convert(salesTarget: SalesTarget): SalesTarget {
        const copy: SalesTarget = Object.assign({}, salesTarget);

        // copy.dtfrom = this.dateUtils.toDate(salesTarget.dtfrom);

        // copy.dtthru = this.dateUtils.toDate(salesTarget.dtthru);
        return copy;
    }
}
