import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { SalesTarget } from './sales-target.model';
import { SalesTargetService } from './sales-target.service';

@Component({
    selector: 'jhi-sales-target-detail',
    templateUrl: './sales-target-detail.component.html'
})
export class SalesTargetDetailComponent implements OnInit, OnDestroy {

    salesTarget: SalesTarget;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private salesTargetService: SalesTargetService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSalesTargets();
    }

    load(id) {
        this.salesTargetService.find(id).subscribe((salesTarget) => {
            this.salesTarget = salesTarget;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSalesTargets() {
        this.eventSubscriber = this.eventManager.subscribe(
            'salesTargetListModification',
            (response) => this.load(this.salesTarget.id)
        );
    }
}
