import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SalesTarget } from './sales-target.model';
import { SalesTargetPopupService } from './sales-target-popup.service';
import { SalesTargetService } from './sales-target.service';

@Component({
    selector: 'jhi-sales-target-delete-dialog',
    templateUrl: './sales-target-delete-dialog.component.html'
})
export class SalesTargetDeleteDialogComponent {

    salesTarget: SalesTarget;

    constructor(
        private salesTargetService: SalesTargetService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.salesTargetService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'salesTargetListModification',
                content: 'Deleted an salesTarget'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sales-target-delete-popup',
    template: ''
})
export class SalesTargetDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private salesTargetPopupService: SalesTargetPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.salesTargetPopupService
                .open(SalesTargetDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
