import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { SalesTarget } from './sales-target.model';
import { SalesTargetService } from './sales-target.service';

@Injectable()
export class SalesTargetPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private salesTargetService: SalesTargetService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.salesTargetService.find(id).subscribe((salesTarget) => {
                    // salesTarget.dtfrom = this.datePipe
                    //     .transform(salesTarget.dtfrom, 'yyyy-MM-ddTHH:mm:ss');
                    // salesTarget.dtthru = this.datePipe
                    //     .transform(salesTarget.dtthru, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.salesTargetModalRef(component, salesTarget);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.salesTargetModalRef(component, new SalesTarget());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    salesTargetModalRef(component: Component, salesTarget: SalesTarget): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.salesTarget = salesTarget;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
