import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ResponseWrapper, Principal } from '../../shared';
import { SalesTarget } from './sales-target.model';
import { SalesTargetPopupService } from './sales-target-popup.service';
import { SalesTargetService } from './sales-target.service';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { Salesman, SalesmanService} from '../salesman';
import * as BaseConstant from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-sales-target-dialog',
    templateUrl: './sales-target-dialog.component.html'
})
export class SalesTargetDialogComponent implements OnInit {

    salesTarget: SalesTarget;
    isSaving: boolean;
    tgl1: Date;
    tgl2: Date;
    salesmans: Salesman[];
    korsals: Salesman[];
    public showSalesman: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private salesTargetService: SalesTargetService,
        private eventManager: JhiEventManager,
        protected salesmanService: SalesmanService,
        protected principal: Principal,
    ) {
        this.tgl1 = null;
        this.tgl2 = null;
        this.salesmans = [];
        this.korsals = [];
        this.showSalesman = false;
    }

    ngOnInit() {
        this.isSaving = false;
        // this.loadKorsal();

        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            userName: this.principal.getUserLogin()
        };
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper,
            ) => {
                console.log('selected Sales' , res.json);
                this.salesmans = res.json;
                this.showSalesman = true;
            }
        )
        this.salesTarget.internal = this.principal.getIdInternal();
    }

    // protected  loadKorsal(): void {
    //     const obj: object = {
    //         idInternal: this.principal.getIdInternal(),
    //         idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
    //     };
    //     this.salesmanService.queryFilterBy(obj).subscribe(
    //         (res: ResponseWrapper) => {
    //             console.log('tarik data korsal', res.json);
    //             this.korsals = res.json;
    //         }
    //     )
    // }

    // public selectSalesByKorsal(): void {
    //     const obj: object = {
    //         idCoordinator : this.salesTarget.selectedKorsal,
    //         size : 9999
    //     }
    //     this.salesmanService.queryFilterBy(obj).subscribe(
    //         (res: ResponseWrapper,
    //         ) => {
    //             console.log('selected Sales' , res.json);
    //             this.salesmans = res.json;
    //             this.showSalesman = true;
    //         }
    //     )
    // }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.salesTarget.idslstrgt !== undefined) {
            this.subscribeToSaveResponse(
                this.salesTargetService.update(this.salesTarget));
        } else {
            this.subscribeToSaveResponse(
                this.salesTargetService.create(this.salesTarget));
        }
    }

    private subscribeToSaveResponse(result: Observable<SalesTarget>) {
        result.subscribe((res: SalesTarget) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: SalesTarget) {
        this.eventManager.broadcast({ name: 'salesTargetListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-sales-target-popup',
    template: ''
})
export class SalesTargetPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private salesTargetPopupService: SalesTargetPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.salesTargetPopupService
                    .open(SalesTargetDialogComponent as Component, params['id']);
            } else {
                this.salesTargetPopupService
                    .open(SalesTargetDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
