import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { RequirementTypeComponent } from './requirement-type.component';
import { RequirementTypePopupComponent } from './requirement-type-dialog.component';

@Injectable()
export class RequirementTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequirementType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const requirementTypeRoute: Routes = [
    {
        path: 'requirement-type',
        component: RequirementTypeComponent,
        resolve: {
            'pagingParams': RequirementTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirementType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const requirementTypePopupRoute: Routes = [
    {
        path: 'requirement-type-new',
        component: RequirementTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirementType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'requirement-type/:id/edit',
        component: RequirementTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirementType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
