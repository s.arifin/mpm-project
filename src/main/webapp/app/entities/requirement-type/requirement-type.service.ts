import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { RequirementType } from './requirement-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as reqTypeConstant from '../../shared/constants/requirement-type.constants';
import * as moment from 'moment';

@Injectable()
export class RequirementTypeService {
    protected itemValues: RequirementType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/requirement-types';
    protected resourceSearchUrl = 'api/_search/requirement-types';

    constructor(protected http: Http) { }

    create(requirementType: RequirementType): Observable<RequirementType> {
        const copy = this.convert(requirementType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(requirementType: RequirementType): Observable<RequirementType> {
        const copy = this.convert(requirementType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<RequirementType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, requirementType: RequirementType): Observable<RequirementType> {
        const copy = this.convert(requirementType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, requirementTypes: RequirementType[]): Observable<RequirementType[]> {
        const copy = this.convertList(requirementTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(requirementType: RequirementType): RequirementType {
        if (requirementType === null || requirementType === {}) {
            return {};
        }
        // const copy: RequirementType = Object.assign({}, requirementType);
        const copy: RequirementType = JSON.parse(JSON.stringify(requirementType));
        return copy;
    }

    protected convertList(requirementTypes: RequirementType[]): RequirementType[] {
        const copy: RequirementType[] = requirementTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: RequirementType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    public getRequirementType(): Array<RequirementType> {
        let arr: Array<RequirementType>;
        arr = new Array<RequirementType>();

        let reqType: RequirementType;

        reqType = new RequirementType;
        reqType.idRequirementType = reqTypeConstant.ORGANIZATION_CUSTOMER;
        reqType.description = 'Customer Organization';

        arr.push(reqType);

        reqType = new RequirementType;
        reqType.idRequirementType = reqTypeConstant.REQUIREMENT_PERSON;
        reqType.description = 'Customer Person';

        arr.push(reqType);

        return arr;
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
