import { BaseEntity } from './../../shared';

export class RequirementType implements BaseEntity {
    constructor(
        public id?: number,
        public idRequirementType?: number,
        public description?: string,
    ) {
    }
}
