import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {RequirementType} from './requirement-type.model';
import {RequirementTypePopupService} from './requirement-type-popup.service';
import {RequirementTypeService} from './requirement-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-requirement-type-dialog',
    templateUrl: './requirement-type-dialog.component.html'
})
export class RequirementTypeDialogComponent implements OnInit {

    requirementType: RequirementType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected requirementTypeService: RequirementTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.requirementType.idRequirementType !== undefined) {
            this.subscribeToSaveResponse(
                this.requirementTypeService.update(this.requirementType));
        } else {
            this.subscribeToSaveResponse(
                this.requirementTypeService.create(this.requirementType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RequirementType>) {
        result.subscribe((res: RequirementType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: RequirementType) {
        this.eventManager.broadcast({ name: 'requirementTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'requirementType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'requirementType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-requirement-type-popup',
    template: ''
})
export class RequirementTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected requirementTypePopupService: RequirementTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.requirementTypePopupService
                    .open(RequirementTypeDialogComponent as Component, params['id']);
            } else {
                this.requirementTypePopupService
                    .open(RequirementTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
