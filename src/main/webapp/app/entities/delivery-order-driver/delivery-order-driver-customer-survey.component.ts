import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DeliveryOrderDriver } from './delivery-order-driver.model';
import { DeliveryOrderDriverService } from './delivery-order-driver.service';

@Component({
    selector: 'jhi-delivery-order-driver-customer-survey',
    templateUrl: './delivery-order-driver-customer-survey.component.html'
})

export class DeliveryOrderDriverCustomerSurveyComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;

    val1: string;
    B1: string;
    B2: string;
    B3: string;
    B4: string;
    C1: string;
    C2: string;
    C3: string;
    C4: string;
    C5: string;
    C6: string;
    C7: string;
    C8: string;
    C9: string;
    C10: string;
    CC2: string;
    CC3: string;
    jk: string;
    kdBisnis: string;

    constructor(
        protected deliveryOrderDriverService: DeliveryOrderDriverService,
        protected route: ActivatedRoute,
        protected router: Router
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    load(id) {
    }

    previousState() {
        // this.router.navigate(['../delivery-order-driver-detail']);
    }
}
