import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DeliveryOrderDriverComponent } from './delivery-order-driver.component';
import { DeliveryOrderDriverDetailComponent } from './delivery-order-driver-detail.component';
import { DeliveryOrderDriverCustomerSurveyComponent } from './delivery-order-driver-customer-survey.component';

@Injectable()
export class DeliveryOrderDriverResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idShipment,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const deliveryOrderDriverRoute: Routes = [
    {
        path: 'delivery-order-driver',
        component: DeliveryOrderDriverComponent,
        resolve: {
            'pagingParams': DeliveryOrderDriverResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.deliveryOrderDriver.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const deliveryOrderDriverPopupRoute: Routes = [
    {
        path: 'delivery-order-driver/:id/delivery-order-driver-detail',
        component: DeliveryOrderDriverDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.deliveryOrderDriver.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'delivery-order-driver/delivery-order-driver-customer-survey',
        component: DeliveryOrderDriverCustomerSurveyComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.deliveryOrderDriver.home.title'
        },
        canActivate: [UserRouteAccessService]
    }

];
