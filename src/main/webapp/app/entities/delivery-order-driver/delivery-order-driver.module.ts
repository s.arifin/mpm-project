import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    DeliveryOrderDriverService,
    DeliveryOrderDriverPopupService,
    DeliveryOrderDriverComponent,
    DeliveryOrderDriverTodayComponent,
    DeliveryOrderDriverResolvePagingParams,
    deliveryOrderDriverRoute,
    deliveryOrderDriverPopupRoute,
    DeliveryOrderDriverDetailComponent,
    DeliveryOrderDriverCustomerSurveyComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
         MessagesModule,
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule,
         RadioButtonModule
        } from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';
import { MessagePipe } from '../../shared/pipe/message.pipe';

const ENTITY_STATES = [
    ...deliveryOrderDriverRoute,
    ...deliveryOrderDriverPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        MessagesModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        RadioButtonModule
    ],
    exports: [
        DeliveryOrderDriverComponent,
        DeliveryOrderDriverDetailComponent,
        DeliveryOrderDriverCustomerSurveyComponent,
        DeliveryOrderDriverTodayComponent,
    ],
    declarations: [
        DeliveryOrderDriverComponent,
        DeliveryOrderDriverDetailComponent,
        DeliveryOrderDriverCustomerSurveyComponent,
        DeliveryOrderDriverTodayComponent,
    ],
    entryComponents: [
        DeliveryOrderDriverComponent,
        DeliveryOrderDriverDetailComponent,
        DeliveryOrderDriverCustomerSurveyComponent,
        DeliveryOrderDriverTodayComponent,
    ],
    providers: [
        DeliveryOrderDriverService,
        DeliveryOrderDriverPopupService,
        DeliveryOrderDriverResolvePagingParams,
        DeliveryOrderDriverTodayComponent,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmDeliveryOrderDriverModule {}
