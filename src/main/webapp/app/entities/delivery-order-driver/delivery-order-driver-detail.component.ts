import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { LoadingService } from '../../layouts/loading/loading.service';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import * as ShipmentOutgoingConstrants from '../../shared/constants/shipmentOutgoing.constrants'
import { CustomShipment, ShipmentOutgoing, ShipmentOutgoingService } from '../shipment-outgoing';
import {SalesUnitRequirement, SalesUnitRequirementService} from '../sales-unit-requirement';
import {VehicleSalesOrder, VehicleSalesOrderService} from '../vehicle-sales-order';
import {Receipt, ReceiptService} from '../receipt';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
// import { ShipmentOutgoingService } from '../shipment-outgoing.service';
import { ToasterService} from '../../shared';

@Component({
    selector: 'jhi-delivery-order-driver-detail',
    templateUrl: './delivery-order-driver-detail.component.html'
})

export class DeliveryOrderDriverDetailComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    shipmentOutgoing: ShipmentOutgoing;
    isSaving: boolean;
    batalKirimDialog: boolean;
    salesUnitRequirement: SalesUnitRequirement;
    statusPengirimanDialog: boolean;
    shipmentTMP: VehicleSalesOrder;
    customOutgoings: CustomShipment;
    pengirimanAksesoris: boolean;
    receipt: Receipt;
    receipts: Receipt[];
    idInternalHere: any;
    datas: any;
    statusPayment: boolean;
    messages: Array<Object> = [
        {
            severity: 'info',
            summary: 'Info',
            detail: 'Periksa Pembayaran'
        }
    ];

    constructor(
        protected alertService: JhiAlertService,
        protected shipmentOutgoingService: ShipmentOutgoingService,
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected vehicleSalesOrderService: VehicleSalesOrderService,
        protected receiptService: ReceiptService,
        protected route: ActivatedRoute,
        protected loadingService: LoadingService,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected principal: Principal,
    ) {
        this.shipmentOutgoing = new ShipmentOutgoing();
        this.datas = {
            dpmurni: 0,
            tandajadi: 0,
            tenor: null,
            angsuran: null,
            tanggalPembayaran: null,
            sisa: null,
            totalPembayaran: 0,
            totalSurReceipt: 0
        }
        this.batalKirimDialog = false;
        this.statusPengirimanDialog = false;
        this.statusPayment = false;
    }

    ngOnInit() {
        this.loadingService.loadingStart();
        this.idInternalHere = this.principal.getIdInternal();
        // this.shipmentOutgoingService.currentData.subscribe( (shipmentTMP) => this.shipmentTMP = shipmentTMP);
        // console.log('shipmentTMP', this.shipmentTMP);
        this.shipmentOutgoingService.passingData.subscribe( (shipmentOutgoing) => {
            this.customOutgoings = shipmentOutgoing;
            this.vehicleSalesOrderService.find(
                this.customOutgoings.idOrder, this.principal.getIdInternal).subscribe(
                    (data) => {
                        this.loadingService.loadingStop();
                        this.shipmentTMP = data;
                        console.log('shipmentTMP', this.shipmentTMP);
                    }
                );
        }
    );
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });

        this.isSaving = false;
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
    }

    load(id) {
        this.shipmentOutgoingService.find(id).subscribe((shipmentOutgoing) => {
            this.shipmentOutgoing = shipmentOutgoing;
            if (this.idInternalHere !== this.customOutgoings.idInternalsrc) {
                this.statusPayment = true;
            };
        });
    }

    previousState() {
        this.router.navigate(['delivery-order-driver']);
    }

    showBatalKirimDialog() {
        this.batalKirimDialog = true;
    }

    cancelShipment(): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                // console.log('cancel', rowdata);
                // console.log('KIRIM_GUDANG', ShipmentOutgoingConstrants.KIRIM_GUDANG);
                this.shipmentOutgoingService.executeProcess(ShipmentOutgoingConstrants.CANCEL, null, this.shipmentOutgoing ).subscribe(
                    (value) => console.log('this: ', value),
                    (err) => console.log(err),
                    () => {
                        this.previousState();
                        resolve()
                    }
                );
            }
        )
    }
    completeShipment(): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                // console.log('cancel', rowdata);
                // console.log('KIRIM_GUDANG', ShipmentOutgoingConstrants.KIRIM_GUDANG);
                this.shipmentOutgoingService.executeProcess(ShipmentOutgoingConstrants.COMPLETE, null, this.shipmentOutgoing ).subscribe(
                    (value) => console.log('this: ', value),
                    (err) => console.log(err),
                    () => {
                        this.previousState();
                        resolve()
                    }
                );
            }
        )
    }

    showStatusPengirimanDialog() {
        this.statusPengirimanDialog = true;
    }
    resetvalue() {
        this.datas = {
            dpmurni: 0,
            tandajadi: 0,
            tenor: null,
            angsuran: null,
            tanggalPembayaran: null,
            sisa: null,
            totalPembayaran: 0
        }
    }

    checkPayment() {
        this.resetvalue();
        this.vehicleSalesOrderService.findSurByOrderId(this.shipmentTMP.idOrder, this.principal.getIdInternal()).subscribe((sur) => {
            this.salesUnitRequirement = sur;
            console.log('this.salesUnitRequirement', this.salesUnitRequirement);
            this.receiptService.findByPaidFromId(this.salesUnitRequirement.customerId).subscribe(
                (res: ResponseWrapper) => {
                    console.log('res', res);
                    this.receipts = res.json;
                // this.receipts = receipt;
                for (const rec in this.receipts) {
                    // this.datas.dpmurni += this.receipts[id].amount;
                    if ( this.receipts[rec].amount !== 0 ) {
                        // this.datas.dpmurni += this.receipts[rec].amount;
                        this.datas.totalPembayaran += this.receipts[rec].amount;
                        console.log(rec);
                    }
                    // this.datas.
                }
                this.datas.totalSurReceipt = this.datas.totalPembayaran + this.salesUnitRequirement.downPayment;
                console.log('this.datas.totalPembayaran', this.datas.totalPembayaran);
                console.log('this.receipt', this.receipts);
                if (this.datas.totalSurReceipt >= this.salesUnitRequirement.minPayment) {
                    // console.log('lunas');
                    this.statusPayment = true;
                    this.toaster.showToaster('info', 'Payment', 'Pembayaran Lunas');
                } else {
                    // console.log('belum lunas');
                    this.statusPayment = true; // sementara diloloskan sebelum ar dapat digunakan untuk cek pelunasan (permintaan pak yosep dan pak adith)
                    // this.statusPayment = false;
                    this.toaster.showToaster('info', 'Payment', 'Pembayaran Belum Lunas');
                }
            });
        });
    }

    save() {
        this.isSaving = true;
        if (this.shipmentOutgoing.idShipment !== undefined) {
            this.subscribeToSaveResponse(
                this.shipmentOutgoingService.update(this.shipmentOutgoing));
        } else {
            this.subscribeToSaveResponse(
                this.shipmentOutgoingService.create(this.shipmentOutgoing));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ShipmentOutgoing>) {
        result.subscribe((res: ShipmentOutgoing) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ShipmentOutgoing) {
        this.eventManager.broadcast({ name: 'shipmentOutgoingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'shipmentOutgoing saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'shipmentOutgoing Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}
