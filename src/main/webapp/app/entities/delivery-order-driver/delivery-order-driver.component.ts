import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { DeliveryOrderDriver } from './delivery-order-driver.model';
import { DeliveryOrderDriverService } from './delivery-order-driver.service';

import {CustomShipment, ShipmentOutgoing, ShipmentOutgoingService} from '../shipment-outgoing';
import {VehicleSalesOrder, VehicleSalesOrderService } from './../vehicle-sales-order';
import { Account, ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import * as ShipmentOutgoingConstrants from '../../shared/constants/shipmentOutgoing.constrants'

import { LoadingService } from '../../layouts/loading/loading.service';
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import * as moment from 'moment';

@Component({
    selector: 'jhi-delivery-order-driver',
    templateUrl: './delivery-order-driver.component.html'
})
export class DeliveryOrderDriverComponent implements OnInit, OnDestroy {

    currentSearch: string;
    shipmentOutgoing: CustomShipment[];
    shipmentOutgoingToday: CustomShipment[];
    vso: VehicleSalesOrder[];
    vsoh: VehicleSalesOrder;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    isLazyLoadingFirst: Boolean = false;
    account: Account;
    username: any;
    idInternalHere: any;

    dateFrom: Date;
    dateThru: Date;

    constructor(
        protected shipmentOutgoingService: ShipmentOutgoingService,
        protected vehicleSalesOrderService: VehicleSalesOrderService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected loadingService: LoadingService,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.vso = new Array<VehicleSalesOrder>();
        this.vsoh = new VehicleSalesOrder();
        this.shipmentOutgoing = new Array<CustomShipment>();
        this.shipmentOutgoingToday = new Array<CustomShipment>();
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.username = null;
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.shipmentOutgoingService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                // sort: this.sort()
            }).subscribe(
                    (res: ResponseWrapper) => this.onSuccessAll(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.shipmentOutgoingService.queryFilterBy({
            idstatustype: [22],
            idInternal: this.principal.getIdInternal(),
            username: this.username,
            queryFor: 'driver',
            page: this.page - 1,
            size: this.itemsPerPage,
            // sort: this.sort()
            }).subscribe(
            (res: ResponseWrapper) => this.onSuccessAll(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
           this.transition();
       }
    }

    transition() {
        this.router.navigate(['/delivery-order-driver'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/shipment-outgoing', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/shipment-outgoing', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

     ngOnInit() {

        this.principal.identity().then((account) => {
            this.account = account;
            this.username = this.account.login;
            this.loadAll();
            this.idInternalHere = this.principal.getIdInternal();
        });
        // this.principal.identity(true).then((account) => {
        //     this.currentAccount = account;
        // });
        // this.registerChangeInShipmentOutgoings();
     }

     ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
     }

    // trackId(index: number, item: ShipmentOutgoing) {
    //     return item.idShipment;
    // }

    // registerChangeInShipmentOutgoings() {
    //     this.eventSubscriber = this.eventManager.subscribe('shipmentOutgoingListModification', (response) => this.loadAll());
    // }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idShipment') {
            result.push('idShipment');
        }
        return result;
    }

    protected onSuccessAll(dataparam: CustomShipment[], headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.shipmentOutgoing = dataparam;
        console.log('dataparam', dataparam);
        // this.vso = new Array<VehicleSalesOrder>();
        // if (dataparam.length > 0) {
        //     for (let i = 0; i < dataparam.length; i++) {
        //         this.vehicleSalesOrderService.find(
        //             dataparam[i].idOrder, this.principal.getIdInternal).subscribe(
        //                 (data) => {
        //                     this.vso = [...this.vso, data];
        //                 }
        //             );
        //     }
        // }
        // const datas = new Array<CustomShipment>();
        // const dataall = new Array<CustomShipment>();
        // this.vso = new Array<VehicleSalesOrder>();
        // if (dataparam.length > 0) {
        //     for (let i = 0; i < dataparam.length; i++) {
        //         this.vehicleSalesOrderService.find(
        //             dataparam[i].idOrder, this.principal.getIdInternal()).subscribe(
        //             (data) => {
        //                 this.vso = [...this.vso, data];
        //                 dataparam[i].nameCustomer = data.salesUnitRequirement.personOwner.name;
        //                 dataparam[i].noHp = data.salesUnitRequirement.personOwner.cellPhone1;
        //                 dataparam[i].address = data.salesUnitRequirement.personOwner.postalAddress.address1;
        //                 dataparam[i].city = data.salesUnitRequirement.personOwner.postalAddress.cityName;
        //                 // dataall.push(dataparam[i]);
        //                 this.shipmentOutgoing = [...this.shipmentOutgoing, dataparam[i]];
        //                 // if (moment(dataparam[i].dateSchedulle).format('DD/MM/YYYY') === moment(new Date()).format('DD/MM/YYYY')) {
        //                     // datas.push(dataparam[i]);
        //                     // this.shipmentOutgoingToday = [...this.shipmentOutgoingToday, dataparam[i]];
        //                 // }
        //             }
        //         );
        //     }
        // }
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.shipmentOutgoingService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        if (this.isLazyLoadingFirst === false) {
            this.isLazyLoadingFirst = true;
            return ;
        }
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.shipmentOutgoingService.update(event.data)
                .subscribe((res: ShipmentOutgoing) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.shipmentOutgoingService.create(event.data)
                .subscribe((res: ShipmentOutgoing) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: ShipmentOutgoing) {
        this.toasterService.showToaster('info', 'ShipmentOutgoing Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    gotoDetail(rowData)  {
        console.log('datadriverdetail', rowData);
        console.log('datadriverdetailthis.vso', this.vso);
        // console.log('datadriverdetailindex', rowIndex);
        this.shipmentOutgoingService.passingCustomData(rowData);
        // this.vehicleSalesOrderService.find(
        //     rowData.idOrder, this.principal.getIdInternal).subscribe(
        //         (data) => {
        //             this.vsoh = this.vsoh,data;
        //         }
        //     );
        // this.shipmentOutgoingService.changeShareDataTmp(this.vsoh);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.shipmentOutgoingService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'shipmentOutgoingListModification',
                    content: 'Deleted an shipmentOutgoing'
                    });
                });
            }
        });
    }

}
