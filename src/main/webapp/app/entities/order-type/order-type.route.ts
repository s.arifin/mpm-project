import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { OrderTypeComponent } from './order-type.component';
import { OrderTypePopupComponent } from './order-type-dialog.component';

@Injectable()
export class OrderTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idOrderType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const orderTypeRoute: Routes = [
    {
        path: 'order-type',
        component: OrderTypeComponent,
        resolve: {
            'pagingParams': OrderTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const orderTypePopupRoute: Routes = [
    {
        path: 'order-type-new',
        component: OrderTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'order-type/:id/edit',
        component: OrderTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
