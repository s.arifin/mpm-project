import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {OrderType} from './order-type.model';
import {OrderTypePopupService} from './order-type-popup.service';
import {OrderTypeService} from './order-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-order-type-dialog',
    templateUrl: './order-type-dialog.component.html'
})
export class OrderTypeDialogComponent implements OnInit {

    orderType: OrderType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected orderTypeService: OrderTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.orderType.idOrderType !== undefined) {
            this.subscribeToSaveResponse(
                this.orderTypeService.update(this.orderType));
        } else {
            this.subscribeToSaveResponse(
                this.orderTypeService.create(this.orderType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<OrderType>) {
        result.subscribe((res: OrderType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: OrderType) {
        this.eventManager.broadcast({ name: 'orderTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'orderType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'orderType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-order-type-popup',
    template: ''
})
export class OrderTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected orderTypePopupService: OrderTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.orderTypePopupService
                    .open(OrderTypeDialogComponent as Component, params['id']);
            } else {
                this.orderTypePopupService
                    .open(OrderTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
