import { BaseEntity } from './../../shared';

export class OrderType implements BaseEntity {
    constructor(
        public id?: number,
        public idOrderType?: number,
        public description?: string,
    ) {
    }
}
