import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { VendorTypeComponent } from './vendor-type.component';
import { VendorTypePopupComponent } from './vendor-type-dialog.component';

@Injectable()
export class VendorTypeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idVendorType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const vendorTypeRoute: Routes = [
    {
        path: '',
        component: VendorTypeComponent,
        resolve: {
            'pagingParams': VendorTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vendorType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const vendorTypePopupRoute: Routes = [
    {
        path: 'new',
        component: VendorTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vendorType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: ':id/edit',
        component: VendorTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vendorType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
