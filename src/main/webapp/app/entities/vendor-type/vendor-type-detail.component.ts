import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { VendorType } from './vendor-type.model';
import { VendorTypeService } from './vendor-type.service';

@Component({
    selector: 'jhi-vendor-type-detail',
    templateUrl: './vendor-type-detail.component.html'
})
export class VendorTypeDetailComponent implements OnInit, OnDestroy {

    vendorType: VendorType;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private vendorTypeService: VendorTypeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInVendorTypes();
    }

    load(id) {
        this.vendorTypeService.find(id).subscribe((vendorType) => {
            this.vendorType = vendorType;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInVendorTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'vendorTypeListModification',
            (response) => this.load(this.vendorType.id)
        );
    }
}
