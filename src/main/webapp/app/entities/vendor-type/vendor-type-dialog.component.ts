import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {VendorType} from './vendor-type.model';
import {VendorTypePopupService} from './vendor-type-popup.service';
import {VendorTypeService} from './vendor-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-vendor-type-dialog',
    templateUrl: './vendor-type-dialog.component.html'
})
export class VendorTypeDialogComponent implements OnInit {

    vendorType: VendorType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private vendorTypeService: VendorTypeService,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.vendorType.idVendorType !== undefined) {
            this.subscribeToSaveResponse(
                this.vendorTypeService.update(this.vendorType));
        } else {
            this.subscribeToSaveResponse(
                this.vendorTypeService.create(this.vendorType));
        }
    }

    private subscribeToSaveResponse(result: Observable<VendorType>) {
        result.subscribe((res: VendorType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: VendorType) {
        this.eventManager.broadcast({ name: 'vendorTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vendorType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.toaster.showToaster('warning', 'vendorType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-vendor-type-popup',
    template: ''
})
export class VendorTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vendorTypePopupService: VendorTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vendorTypePopupService
                    .open(VendorTypeDialogComponent as Component, params['id']);
            } else {
                this.vendorTypePopupService
                    .open(VendorTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
