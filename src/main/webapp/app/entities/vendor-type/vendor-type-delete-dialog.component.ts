import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { VendorType } from './vendor-type.model';
import { VendorTypePopupService } from './vendor-type-popup.service';
import { VendorTypeService } from './vendor-type.service';

@Component({
    selector: 'jhi-vendor-type-delete-dialog',
    templateUrl: './vendor-type-delete-dialog.component.html'
})
export class VendorTypeDeleteDialogComponent {

    vendorType: VendorType;

    constructor(
        private vendorTypeService: VendorTypeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.vendorTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'vendorTypeListModification',
                content: 'Deleted an vendorType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-vendor-type-delete-popup',
    template: ''
})
export class VendorTypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vendorTypePopupService: VendorTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.vendorTypePopupService
                .open(VendorTypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
