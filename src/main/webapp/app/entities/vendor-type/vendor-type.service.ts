import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { VendorType } from './vendor-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class VendorTypeService {
    private itemValues: VendorType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = SERVER_API_URL + 'api/vendor-types';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/vendor-types';

    constructor(private http: Http) { }

    create(vendorType: VendorType): Observable<VendorType> {
        const copy = this.convert(vendorType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(vendorType: VendorType): Observable<VendorType> {
        const copy = this.convert(vendorType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<VendorType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, vendorType: VendorType): Observable<VendorType> {
        const copy = this.convert(vendorType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, vendorTypes: VendorType[]): Observable<VendorType[]> {
        const copy = this.convertList(vendorTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(vendorType: VendorType): VendorType {
        if (vendorType === null || vendorType === {}) {
            return {};
        }
        // const copy: VendorType = Object.assign({}, vendorType);
        const copy: VendorType = JSON.parse(JSON.stringify(vendorType));
        return copy;
    }

    private convertList(vendorTypes: VendorType[]): VendorType[] {
        const copy: VendorType[] = vendorTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: VendorType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
