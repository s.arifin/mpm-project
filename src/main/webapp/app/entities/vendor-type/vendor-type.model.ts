import { BaseEntity } from './../../shared';

export class VendorType implements BaseEntity {
    constructor(
        public id?: number,
        public idVendorType?: number,
        public description?: string,
    ) {
    }
}
