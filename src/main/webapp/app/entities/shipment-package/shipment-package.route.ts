import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ShipmentPackageComponent } from './shipment-package.component';
import { ShipmentPackagePopupComponent } from './shipment-package-dialog.component';

@Injectable()
export class ShipmentPackageResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idPackage,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const shipmentPackageRoute: Routes = [
    {
        path: 'shipment-package',
        component: ShipmentPackageComponent,
        resolve: {
            'pagingParams': ShipmentPackageResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentPackage.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const shipmentPackagePopupRoute: Routes = [
    {
        path: 'shipment-package-new',
        component: ShipmentPackagePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentPackage.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shipment-package/:id/edit',
        component: ShipmentPackagePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentPackage.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
