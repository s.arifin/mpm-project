import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';

import { ShipmentPackage } from './shipment-package.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ShipmentPackageService {
    protected itemValues: ShipmentPackage[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/shipment-packages';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/shipment-packages';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(shipmentPackage: ShipmentPackage): Observable<ShipmentPackage> {
        const copy = this.convert(shipmentPackage);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(shipmentPackage: ShipmentPackage): Observable<ShipmentPackage> {
        const copy = this.convert(shipmentPackage);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<ShipmentPackage> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(shipmentPackage: ShipmentPackage, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(shipmentPackage);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, shipmentPackage: ShipmentPackage): Observable<ShipmentPackage> {
        const copy = this.convert(shipmentPackage);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, shipmentPackages: ShipmentPackage[]): Observable<ShipmentPackage[]> {
        const copy = this.convertList(shipmentPackages);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateCreated) {
            entity.dateCreated = new Date(entity.dateCreated);
        }
    }

    protected convert(shipmentPackage: ShipmentPackage): ShipmentPackage {
        if (shipmentPackage === null || shipmentPackage === {}) {
            return {};
        }
        // const copy: ShipmentPackage = Object.assign({}, shipmentPackage);
        const copy: ShipmentPackage = JSON.parse(JSON.stringify(shipmentPackage));

        // copy.dateCreated = this.dateUtils.toDate(shipmentPackage.dateCreated);
        return copy;
    }

    protected convertList(shipmentPackages: ShipmentPackage[]): ShipmentPackage[] {
        const copy: ShipmentPackage[] = shipmentPackages;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ShipmentPackage[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
