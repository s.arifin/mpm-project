import { BaseEntity } from './../../shared';

export class ShipmentPackage implements BaseEntity {
    constructor(
        public id?: any,
        public idPackage?: any,
        public idPackageType?: number,
        public dateCreated?: any,
        public internalId?: any,
    ) {
    }
}
