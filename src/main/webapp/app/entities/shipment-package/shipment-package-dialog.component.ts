import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {ShipmentPackage} from './shipment-package.model';
import {ShipmentPackagePopupService} from './shipment-package-popup.service';
import {ShipmentPackageService} from './shipment-package.service';
import {ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-shipment-package-dialog',
    templateUrl: './shipment-package-dialog.component.html'
})
export class ShipmentPackageDialogComponent implements OnInit {

    shipmentPackage: ShipmentPackage;
    isSaving: boolean;

    internals: Internal[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected shipmentPackageService: ShipmentPackageService,
        protected internalService: InternalService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.shipmentPackage.idPackage !== undefined) {
            this.subscribeToSaveResponse(
                this.shipmentPackageService.update(this.shipmentPackage));
        } else {
            this.subscribeToSaveResponse(
                this.shipmentPackageService.create(this.shipmentPackage));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ShipmentPackage>) {
        result.subscribe((res: ShipmentPackage) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ShipmentPackage) {
        this.eventManager.broadcast({ name: 'shipmentPackageListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'shipmentPackage saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'shipmentPackage Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }
}

@Component({
    selector: 'jhi-shipment-package-popup',
    template: ''
})
export class ShipmentPackagePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected shipmentPackagePopupService: ShipmentPackagePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.shipmentPackagePopupService
                    .open(ShipmentPackageDialogComponent as Component, params['id']);
            } else {
                this.shipmentPackagePopupService
                    .open(ShipmentPackageDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
