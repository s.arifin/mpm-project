import { BaseEntity } from './../../shared';

export class InventoryItem implements BaseEntity {
    constructor(
        public id?: any,
        public idInventoryItem?: any,
        public idOwner?: string,
        public idFacility?: string,
        public facilityCode?: string,
        public idContainer?: string,
        public containerCode?: string,
        public idProduct?: string,
        public idFeature?: number,
        public dateCreated?: any,
        public qty?: number,
        public qtyBooking?: number,
        public idFrame?: string,
        public idMachine?: string,
        public shipmentReceiptId?: any,
        public regNoKa?: string,
        public regNoSin?: string,
        public idPartyRole?: any,
        public featureDescription?: string,
        public itemDescription?: string,
        public regCOGS?: any,
        public regYear?: any,
        public yearAssembly?: number,
        public regGudang?: any,
    ) {
    }
}
