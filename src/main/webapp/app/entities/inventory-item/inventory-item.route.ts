import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { InventoryItemComponent } from './inventory-item.component';
import { InventoryItemLovPopupComponent } from './inventory-item-as-lov.component';
import { InventoryItemPopupComponent } from './inventory-item-dialog.component';
import { InventoryItemAvailableForMovingLovPopupComponent } from './inventory-item-in-facility-container-as-lov.component';

@Injectable()
export class InventoryItemResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idInventoryItem,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const inventoryItemRoute: Routes = [
    {
        path: 'inventory-item',
        component: InventoryItemComponent,
        resolve: {
            'pagingParams': InventoryItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.inventoryItem.home.title'
        },
        canActivate: [UserRouteAccessService],
         // redirectTo :  '/'
    }
];

export const inventoryItemPopupRoute: Routes = [
    {
        path: 'inventory-item-in-facility-container-as-lov/:idproduct/:idfacility/:idcontainer',
        component: InventoryItemAvailableForMovingLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.inventoryItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'inventory-item-lov',
        component: InventoryItemLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.inventoryItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'inventory-item-new',
        component: InventoryItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.inventoryItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'inventory-item/:id/edit',
        component: InventoryItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.inventoryItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    // todo remark karena ga pakai gidang dulu
    // {
    //     path: 'inventory-item-lov/:idProduct/:idPartyRole/:idFeature/:regNoKa/:regNoSin/:regYear/:regGudang',
    //     component: InventoryItemLovPopupComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.inventoryItem.home.title'
    //     },
    //     canActivate: [UserRouteAccessService],
    //     outlet: 'popup'
    // }
    {
        path: 'inventory-item-lov/:idProduct/:idPartyRole/:idFeature/:regNoKa/:regNoSin/:regYear',
        component: InventoryItemLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.inventoryItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
