import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { InventoryItemService, InventoryItem, InventoryItemPopupComponent, InventoryItemPopupService } from './';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ResponseWrapper, ITEMS_PER_PAGE } from '../../shared';
import { LazyLoadEvent } from 'primeng/primeng';
import * as _ from 'lodash';

@Component({
    selector: 'jhi-inventory-item-in-facility-container-as-lov',
    templateUrl: './inventory-item-in-facility-container-as-lov.component.html'
})
export class InventoryItemInFacilityContainerAsLovComponent implements OnInit {
    protected data: object;
    protected pushInventoryItems: Array<InventoryItem> = new Array<InventoryItem>();

    public dataSources: Array<InventoryItem> = new Array<InventoryItem>();
    public itemsPerPage: number;
    public inventoryItems: Array<InventoryItem>;
    public selectedInventoryItem: Array<string> = new Array<string>();
    public totalRecords: any;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,

        protected inventoryItemService: InventoryItemService,
        protected route: Router,
        protected activatedRoute: ActivatedRoute,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
    }

    ngOnInit() {
        this.loadAvailableItem();
    }

    protected loadAvailableItem(): void {
        const obj: any = {
            idproduct : this.data['idproduct'],
            idfacility: this.data['idfacility'],
            idcontainer: this.data['idcontainer'],
            page: 0,
            size: 10000,
            sort: ['idInventoryItem,asc']
        };
        this.inventoryItemService.availableStockByProduct(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res)
        );
    }

    protected onSuccess(res: ResponseWrapper): void {
        this.totalRecords = res.headers.get('X-Total-Count');
        this.dataSources = res.json;
        this.inventoryItems = this.dataSources.slice(0, this.itemsPerPage);
    }

    public clearPage(): void {
        this.activeModal.dismiss('cancel');
    }

    public loadInventoryItemLazy(event: LazyLoadEvent): void {
        setTimeout(() => {
            if (this.dataSources) {
                this.inventoryItems = new Array<InventoryItem>();
                this.inventoryItems = this.dataSources.slice(event.first, (event.first + event.rows));
            }
        }, 250);
    }

    public pushInventoryItem(): void {
        this.pushInventoryItems = new Array<InventoryItem>();
        this.pushInventoryItems = _.map(this.selectedInventoryItem, function(e) {
            return <InventoryItem> JSON.parse(e);
        });
        this.inventoryItemService.pushItems(this.pushInventoryItems);
        this.pushInventoryItems = new Array<InventoryItem>();
        this.clearPage();
    }

    public countQtyRemaining(invItem: InventoryItem): number {
        return this.inventoryItemService.countRemainingQtyInventoryItem(invItem);
    }
}

@Component({
    selector: 'jhi-inventory-item-available-for-moving-dialog-lov-popup',
    template: ''
})
export class InventoryItemAvailableForMovingLovPopupComponent implements OnInit, OnDestroy {
    protected routeSub: any;

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected inventoryItemPopupService: InventoryItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.activatedRoute.params.subscribe(
            (params) => {
                if (params['idproduct'] && params['idfacility'] && params['idcontainer']) {
                    let obj: object;
                    if (params['idproduct'] === 'null') {
                        obj = {
                            'idfacility': params['idfacility'],
                            'idcontainer': params['idcontainer']
                        };
                    } else {
                        obj = {
                            'idproduct': params['idproduct'],
                            'idfacility': params['idfacility'],
                            'idcontainer': params['idcontainer']
                        };
                    }

                    this.inventoryItemPopupService.load(InventoryItemInFacilityContainerAsLovComponent as Component, obj);
                }
            }
        )
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
