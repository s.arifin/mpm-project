import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';

import { InventoryItem } from './inventory-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class InventoryItemService {
    protected itemValues: InventoryItem[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/inventory-items';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/inventory-items';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(inventoryItem: InventoryItem): Observable<InventoryItem> {
        const copy = this.convert(inventoryItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(inventoryItem: InventoryItem): Observable<InventoryItem> {
        const copy = this.convert(inventoryItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<InventoryItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    changeStatus(inventoryItem: InventoryItem, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(inventoryItem);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, inventoryItem: InventoryItem): Observable<InventoryItem> {
        const copy = this.convert(inventoryItem);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, inventoryItems: InventoryItem[]): Observable<InventoryItem[]> {
        const copy = this.convertList(inventoryItems);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    availableStockByProduct(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req)
        return this.http.get(this.resourceUrl + '/available-stock-by-product', options)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);

        // options.params.set('regNoSin', req.regNoSin);
        // options.params.set('idProduct', req.idProduct);
        // options.params.set('idInternal', req.idInternal);
        // options.params.set('idFeature', req.idFeature);
        // options.params.set('regNoKa', req.regNoKa);

        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    filterForMemo(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);

        // options.params.set('regNoSin', req.regNoSin);
        // options.params.set('idProduct', req.idProduct);
        // options.params.set('idInternal', req.idInternal);
        // options.params.set('idFeature', req.idFeature);
        // options.params.set('regNoKa', req.regNoKa);

        return this.http.get(this.resourceUrl + '/memo', options)
            .map((res: Response) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateCreated) {
            entity.dateCreated = new Date(entity.dateCreated);
        }
    }

    protected convert(inventoryItem: InventoryItem): InventoryItem {
        if (inventoryItem === null || inventoryItem === {}) {
            return {};
        }
        // const copy: InventoryItem = Object.assign({}, inventoryItem);
        const copy: InventoryItem = JSON.parse(JSON.stringify(inventoryItem));

        // copy.dateCreated = this.dateUtils.toDate(inventoryItem.dateCreated);
        return copy;
    }

    protected convertList(inventoryItems: InventoryItem[]): InventoryItem[] {
        const copy: InventoryItem[] = inventoryItems;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: InventoryItem[]) {
        this.itemValues = new Array<InventoryItem>();
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    public countRemainingQtyInventoryItem(item: InventoryItem): number {
        return item.qty - item.qtyBooking;
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

}
