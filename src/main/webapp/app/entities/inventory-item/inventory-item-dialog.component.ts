import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {InventoryItem} from './inventory-item.model';
import {InventoryItemPopupService} from './inventory-item-popup.service';
import {InventoryItemService} from './inventory-item.service';
import {ToasterService} from '../../shared';
import { ShipmentReceipt, ShipmentReceiptService } from '../shipment-receipt';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-inventory-item-dialog',
    templateUrl: './inventory-item-dialog.component.html'
})
export class InventoryItemDialogComponent implements OnInit {

    inventoryItem: InventoryItem;
    isSaving: boolean;

    shipmentreceipts: ShipmentReceipt[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected inventoryItemService: InventoryItemService,
        protected shipmentReceiptService: ShipmentReceiptService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.shipmentReceiptService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmentreceipts = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.inventoryItem.idInventoryItem !== undefined) {
            this.subscribeToSaveResponse(
                this.inventoryItemService.update(this.inventoryItem));
        } else {
            this.subscribeToSaveResponse(
                this.inventoryItemService.create(this.inventoryItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<InventoryItem>) {
        result.subscribe((res: InventoryItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: InventoryItem) {
        this.eventManager.broadcast({ name: 'inventoryItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'inventoryItem saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'inventoryItem Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackShipmentReceiptById(index: number, item: ShipmentReceipt) {
        return item.idReceipt;
    }
}

@Component({
    selector: 'jhi-inventory-item-popup',
    template: ''
})
export class InventoryItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected inventoryItemPopupService: InventoryItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.inventoryItemPopupService
                    .open(InventoryItemDialogComponent as Component, params['id']);
            } else {
                this.inventoryItemPopupService
                    .open(InventoryItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
