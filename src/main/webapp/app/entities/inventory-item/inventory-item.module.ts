import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    InventoryItemService,
    InventoryItemPopupService,
    InventoryItemComponent,
    InventoryItemDialogComponent,
    InventoryItemPopupComponent,
    inventoryItemRoute,
    inventoryItemPopupRoute,
    InventoryItemResolvePagingParams,
    InventoryItemAsListComponent,
    InventoryItemAsLovComponent,
    InventoryItemLovPopupComponent,
    InventoryItemInFacilityContainerAsLovComponent,
    InventoryItemAvailableForMovingLovPopupComponent
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule
        } from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...inventoryItemRoute,
    ...inventoryItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule
    ],
    exports: [
        InventoryItemAvailableForMovingLovPopupComponent,
        InventoryItemInFacilityContainerAsLovComponent,
        InventoryItemComponent,
        InventoryItemAsListComponent,
        InventoryItemAsLovComponent,
        InventoryItemLovPopupComponent,
    ],
    declarations: [
        InventoryItemAvailableForMovingLovPopupComponent,
        InventoryItemInFacilityContainerAsLovComponent,
        InventoryItemComponent,
        InventoryItemDialogComponent,
        InventoryItemPopupComponent,
        InventoryItemAsListComponent,
        InventoryItemAsLovComponent,
        InventoryItemLovPopupComponent,
    ],
    entryComponents: [
        InventoryItemInFacilityContainerAsLovComponent,
        InventoryItemComponent,
        InventoryItemDialogComponent,
        InventoryItemPopupComponent,
        InventoryItemAsListComponent,
        InventoryItemAsLovComponent,
        InventoryItemLovPopupComponent,
    ],
    providers: [
        InventoryItemService,
        InventoryItemPopupService,
        InventoryItemResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmInventoryItemModule {}
