import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProductCategoryType } from './product-category-type.model';
import { ProductCategoryTypeService } from './product-category-type.service';
import { ToasterService} from '../../shared';

@Component({
   selector: 'jhi-product-category-type-edit',
   templateUrl: './product-category-type-edit.component.html'
})
export class ProductCategoryTypeEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   productCategoryType: ProductCategoryType;
   isSaving: boolean;
   idCategoryType: any;
   paramPage: number;
   routeId: number;

   constructor(
       protected alertService: JhiAlertService,
       protected productCategoryTypeService: ProductCategoryTypeService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected toaster: ToasterService
   ) {
       this.productCategoryType = new ProductCategoryType();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idCategoryType = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.productCategoryTypeService.find(this.idCategoryType).subscribe((productCategoryType) => {
           this.productCategoryType = productCategoryType;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['product-category-type', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.productCategoryType.idCategoryType !== undefined) {
           this.subscribeToSaveResponse(
               this.productCategoryTypeService.update(this.productCategoryType));
       } else {
           this.subscribeToSaveResponse(
               this.productCategoryTypeService.create(this.productCategoryType));
       }
   }

   protected subscribeToSaveResponse(result: Observable<ProductCategoryType>) {
       result.subscribe((res: ProductCategoryType) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: ProductCategoryType) {
       this.eventManager.broadcast({ name: 'productCategoryTypeListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'productCategoryType saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'productCategoryType Changed', error.message);
       this.alertService.error(error.message, null, null);
   }
}
