import { BaseEntity } from './../../shared';

export class ProductCategoryType implements BaseEntity {
    constructor(
        public id?: number,
        public idCategoryType?: number,
        public description?: string,
        public refKey?: string,
    ) {
    }
}
