import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ProductCategoryType } from './product-category-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ProductCategoryTypeService {
   protected itemValues: ProductCategoryType[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/product-category-types';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/product-category-types';

   constructor(protected http: Http) { }

   create(productCategoryType: ProductCategoryType): Observable<ProductCategoryType> {
       const copy = this.convert(productCategoryType);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(productCategoryType: ProductCategoryType): Observable<ProductCategoryType> {
       const copy = this.convert(productCategoryType);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<ProductCategoryType> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: ProductCategoryType, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: ProductCategoryType[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to ProductCategoryType.
    */
   protected convertItemFromServer(json: any): ProductCategoryType {
       const entity: ProductCategoryType = Object.assign(new ProductCategoryType(), json);
       return entity;
   }

   /**
    * Convert a ProductCategoryType to a JSON which can be sent to the server.
    */
   protected convert(productCategoryType: ProductCategoryType): ProductCategoryType {
       if (productCategoryType === null || productCategoryType === {}) {
           return {};
       }
       // const copy: ProductCategoryType = Object.assign({}, productCategoryType);
       const copy: ProductCategoryType = JSON.parse(JSON.stringify(productCategoryType));
       return copy;
   }

   protected convertList(productCategoryTypes: ProductCategoryType[]): ProductCategoryType[] {
       const copy: ProductCategoryType[] = productCategoryTypes;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: ProductCategoryType[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
