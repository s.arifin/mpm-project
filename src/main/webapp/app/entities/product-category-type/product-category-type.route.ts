import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ProductCategoryTypeComponent } from './product-category-type.component';
import { ProductCategoryTypeEditComponent } from './product-category-type-edit.component';
import { ProductCategoryTypePopupComponent } from './product-category-type-dialog.component';

@Injectable()
export class ProductCategoryTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idCategoryType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const productCategoryTypeRoute: Routes = [
    {
        path: 'product-category-type',
        component: ProductCategoryTypeComponent,
        resolve: {
            'pagingParams': ProductCategoryTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productCategoryType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const productCategoryTypePopupRoute: Routes = [
    {
        path: 'product-category-type-popup-new',
        component: ProductCategoryTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productCategoryType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-category-type-new',
        component: ProductCategoryTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productCategoryType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-category-type/:id/edit',
        component: ProductCategoryTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productCategoryType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-category-type/:route/:page/:id/edit',
        component: ProductCategoryTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productCategoryType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-category-type/:id/popup-edit',
        component: ProductCategoryTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productCategoryType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
