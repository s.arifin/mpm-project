import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ProductCategoryType } from './product-category-type.model';
import { ProductCategoryTypePopupService } from './product-category-type-popup.service';
import { ProductCategoryTypeService } from './product-category-type.service';
import { ToasterService } from '../../shared';

@Component({
    selector: 'jhi-product-category-type-dialog',
    templateUrl: './product-category-type-dialog.component.html'
})
export class ProductCategoryTypeDialogComponent implements OnInit {

    productCategoryType: ProductCategoryType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected productCategoryTypeService: ProductCategoryTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.productCategoryType.idCategoryType !== undefined) {
            this.subscribeToSaveResponse(
                this.productCategoryTypeService.update(this.productCategoryType));
        } else {
            this.subscribeToSaveResponse(
                this.productCategoryTypeService.create(this.productCategoryType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProductCategoryType>) {
        result.subscribe((res: ProductCategoryType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ProductCategoryType) {
        this.eventManager.broadcast({ name: 'productCategoryTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'productCategoryType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-product-category-type-popup',
    template: ''
})
export class ProductCategoryTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected productCategoryTypePopupService: ProductCategoryTypePopupService
    ) {}

    ngOnInit() {

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.productCategoryTypePopupService
                    .open(ProductCategoryTypeDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.productCategoryTypePopupService.parent = params['parent'];
                this.productCategoryTypePopupService
                    .open(ProductCategoryTypeDialogComponent as Component);
            } else {
                this.productCategoryTypePopupService
                    .open(ProductCategoryTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
