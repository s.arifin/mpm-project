import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { PostalAddress } from './postal-address.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PostalAddressService {
    protected itemValues: PostalAddress[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/postal-addresses';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/postal-addresses';

    constructor(protected http: Http) { }

    create(postalAddress: PostalAddress): Observable<PostalAddress> {
        const copy = this.convert(postalAddress);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(postalAddress: PostalAddress): Observable<PostalAddress> {
        const copy = this.convert(postalAddress);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<PostalAddress> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<PostalAddress> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<PostalAddress[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PostalAddress.
     */
    protected convertItemFromServer(json: any): PostalAddress {
        const entity: PostalAddress = Object.assign(new PostalAddress(), json);
        return entity;
    }

    /**
     * Convert a PostalAddress to a JSON which can be sent to the server.
     */
    protected convert(postalAddress: PostalAddress): PostalAddress {
        if (postalAddress === null || postalAddress === {}) {
            return {};
        }
        // const copy: PostalAddress = Object.assign({}, postalAddress);
        const copy: PostalAddress = JSON.parse(JSON.stringify(postalAddress));
        return copy;
    }

    protected convertList(postalAddresss: PostalAddress[]): PostalAddress[] {
        const copy: PostalAddress[] = postalAddresss;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: PostalAddress[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
