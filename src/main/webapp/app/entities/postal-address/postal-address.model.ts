import { BaseEntity } from './../../shared';

export class PostalAddress implements BaseEntity {
    constructor(
        public id?: any,
        public idContact?: any,
        public address1?: string,
        public address2?: string,
        public district?: string,
        public districtCode?: string,
        public districtId?: string,
        public districtName?: string,
        public villageId?: string,
        public villageCode?: string,
        public villageName?: string,
        public cityId?: string,
        public cityCode?: string,
        public cityName?: string,
        public provinceId?: string,
        public provinceCode?: string,
        public provinceName?: string,
    ) {
    }
}
