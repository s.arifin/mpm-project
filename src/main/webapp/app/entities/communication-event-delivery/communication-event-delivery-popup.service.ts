import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CommunicationEventDelivery } from './communication-event-delivery.model';
import { CommunicationEventDeliveryService } from './communication-event-delivery.service';

@Injectable()
export class CommunicationEventDeliveryPopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected communicationEventDeliveryService: CommunicationEventDeliveryService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.communicationEventDeliveryService.find(id).subscribe((communicationEventDelivery) => {
                    this.ngbModalRef = this.communicationEventDeliveryModalRef(component, communicationEventDelivery);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.communicationEventDeliveryModalRef(component, new CommunicationEventDelivery());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    communicationEventDeliveryModalRef(component: Component, communicationEventDelivery: CommunicationEventDelivery): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.communicationEventDelivery = communicationEventDelivery;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
