import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { CommunicationEventDelivery } from './communication-event-delivery.model';
import { CommunicationEventDeliveryService } from './communication-event-delivery.service';

@Component({
    selector: 'jhi-communication-event-delivery-detail',
    templateUrl: './communication-event-delivery-detail.component.html'
})
export class CommunicationEventDeliveryDetailComponent implements OnInit, OnDestroy {

    communicationEventDelivery: CommunicationEventDelivery;
    protected subscription: Subscription;
    protected eventSubscriber: Subscription;

    constructor(
        protected eventManager: JhiEventManager,
        protected communicationEventDeliveryService: CommunicationEventDeliveryService,
        protected route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCommunicationEventDeliveries();
    }

    load(id) {
        this.communicationEventDeliveryService.find(id).subscribe((communicationEventDelivery) => {
            this.communicationEventDelivery = communicationEventDelivery;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCommunicationEventDeliveries() {
        this.eventSubscriber = this.eventManager.subscribe(
            'communicationEventDeliveryListModification',
            (response) => this.load(this.communicationEventDelivery.id)
        );
    }
}
