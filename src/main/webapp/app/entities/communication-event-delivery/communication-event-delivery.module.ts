import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';

import {
    RadioButtonModule,
    CheckboxModule,
    AccordionModule,
} from 'primeng/primeng';

import {
    CommunicationEventDeliveryService,
    CommunicationEventDeliveryPopupService,
    CommunicationEventDeliveryComponent,
    CommunicationEventDeliveryDetailComponent,
    CommunicationEventDeliveryDialogComponent,
    CommunicationEventDeliveryPopupComponent,
    CommunicationEventDeliveryDeletePopupComponent,
    CommunicationEventDeliveryDeleteDialogComponent,
    communicationEventDeliveryRoute,
    communicationEventDeliveryPopupRoute,
    CommunicationEventDeliveryResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...communicationEventDeliveryRoute,
    ...communicationEventDeliveryPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        RadioButtonModule,
        CheckboxModule,
        AccordionModule,
    ],
    declarations: [
        CommunicationEventDeliveryComponent,
        CommunicationEventDeliveryDetailComponent,
        CommunicationEventDeliveryDialogComponent,
        CommunicationEventDeliveryDeleteDialogComponent,
        CommunicationEventDeliveryPopupComponent,
        CommunicationEventDeliveryDeletePopupComponent,
    ],
    exports: [
        CommunicationEventDeliveryDialogComponent,
        CommunicationEventDeliveryComponent,
        CommunicationEventDeliveryPopupComponent,
    ],
    entryComponents: [
        CommunicationEventDeliveryComponent,
        CommunicationEventDeliveryDialogComponent,
        CommunicationEventDeliveryPopupComponent,
        CommunicationEventDeliveryDeleteDialogComponent,
        CommunicationEventDeliveryDeletePopupComponent,
    ],
    providers: [
        CommunicationEventDeliveryService,
        CommunicationEventDeliveryPopupService,
        CommunicationEventDeliveryResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmCommunicationEventDeliveryModule {}
