import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CommunicationEventDelivery } from './communication-event-delivery.model';
import { CommunicationEventDeliveryPopupService } from './communication-event-delivery-popup.service';
import { CommunicationEventDeliveryService } from './communication-event-delivery.service';

@Component({
    selector: 'jhi-communication-event-delivery-dialog',
    templateUrl: './communication-event-delivery-dialog.component.html'
})
export class CommunicationEventDeliveryDialogComponent implements OnInit {
    @Input() idCostumer: String;
    communicationEventDelivery: CommunicationEventDelivery;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected communicationEventDeliveryService: CommunicationEventDeliveryService,
        protected eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.communicationEventDelivery = new CommunicationEventDelivery();
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.communicationEventDelivery.id !== undefined) {
            this.subscribeToSaveResponse(
                this.communicationEventDeliveryService.update(this.communicationEventDelivery));
        } else {
            this.subscribeToSaveResponse(
                this.communicationEventDeliveryService.create(this.communicationEventDelivery));
        }
    }

    protected subscribeToSaveResponse(result: Observable<CommunicationEventDelivery>) {
        result.subscribe((res: CommunicationEventDelivery) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: CommunicationEventDelivery) {
        this.eventManager.broadcast({ name: 'communicationEventDeliveryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-communication-event-delivery-popup',
    template: ''
})
export class CommunicationEventDeliveryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected communicationEventDeliveryPopupService: CommunicationEventDeliveryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.communicationEventDeliveryPopupService
                    .open(CommunicationEventDeliveryDialogComponent as Component, params['id']);
            } else {
                this.communicationEventDeliveryPopupService
                    .open(CommunicationEventDeliveryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
