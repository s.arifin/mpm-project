import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CommunicationEventDelivery } from './communication-event-delivery.model';
import { CommunicationEventDeliveryPopupService } from './communication-event-delivery-popup.service';
import { CommunicationEventDeliveryService } from './communication-event-delivery.service';

@Component({
    selector: 'jhi-communication-event-delivery-delete-dialog',
    templateUrl: './communication-event-delivery-delete-dialog.component.html'
})
export class CommunicationEventDeliveryDeleteDialogComponent {

    communicationEventDelivery: CommunicationEventDelivery;

    constructor(
        protected communicationEventDeliveryService: CommunicationEventDeliveryService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.communicationEventDeliveryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'communicationEventDeliveryListModification',
                content: 'Deleted an communicationEventDelivery'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-communication-event-delivery-delete-popup',
    template: ''
})
export class CommunicationEventDeliveryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected communicationEventDeliveryPopupService: CommunicationEventDeliveryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.communicationEventDeliveryPopupService
                .open(CommunicationEventDeliveryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
