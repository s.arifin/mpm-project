import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { CommunicationEventDelivery } from './communication-event-delivery.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CommunicationEventDeliveryService {

    protected resourceUrl = SERVER_API_URL + 'api/communication-event-deliveries';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/communication-event-deliveries';

    constructor(protected http: Http) { }

    create(communicationEventDelivery: CommunicationEventDelivery): Observable<CommunicationEventDelivery> {
        const copy = this.convert(communicationEventDelivery);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(communicationEventDelivery: CommunicationEventDelivery): Observable<CommunicationEventDelivery> {
        const copy = this.convert(communicationEventDelivery);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<CommunicationEventDelivery> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(communicationEventDelivery: CommunicationEventDelivery): CommunicationEventDelivery {
        const copy: CommunicationEventDelivery = Object.assign({}, communicationEventDelivery);
        return copy;
    }
}
