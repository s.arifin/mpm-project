import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CommunicationEventDeliveryComponent } from './communication-event-delivery.component';
import { CommunicationEventDeliveryDetailComponent } from './communication-event-delivery-detail.component';
import { CommunicationEventDeliveryPopupComponent } from './communication-event-delivery-dialog.component';
import { CommunicationEventDeliveryDeletePopupComponent } from './communication-event-delivery-delete-dialog.component';

@Injectable()
export class CommunicationEventDeliveryResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const communicationEventDeliveryRoute: Routes = [
    {
        path: 'communication-event-delivery',
        component: CommunicationEventDeliveryComponent,
        resolve: {
            'pagingParams': CommunicationEventDeliveryResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventDelivery.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'communication-event-delivery/:id',
        component: CommunicationEventDeliveryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventDelivery.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const communicationEventDeliveryPopupRoute: Routes = [
    {
        path: 'communication-event-delivery-new',
        component: CommunicationEventDeliveryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventDelivery.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'communication-event-delivery/:id/edit',
        component: CommunicationEventDeliveryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventDelivery.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'communication-event-delivery/:id/delete',
        component: CommunicationEventDeliveryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventDelivery.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
