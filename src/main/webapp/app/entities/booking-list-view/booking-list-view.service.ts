import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { BookingListView } from './booking-list-view.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class BookingListViewService {

    protected resourceUrl = 'api/work-order-bookings';
    protected resourceSearchUrl = 'api/_search/work-order-bookings';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(bookingListView: BookingListView): Observable<BookingListView> {
        const copy = this.convert(bookingListView);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(bookingListView: BookingListView): Observable<BookingListView> {
        const copy = this.convert(bookingListView);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<BookingListView> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(bookingListView: BookingListView, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(bookingListView);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, bookingListView: BookingListView): Observable<BookingListView> {
        const copy = this.convert(bookingListView);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, bookingListViews: BookingListView[]): Observable<BookingListView[]> {
        const copy = this.convertList(bookingListViews);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateCreate) {
            entity.dateCreate = new Date(entity.dateCreate);
        }
    }

    protected convert(bookingListView: BookingListView): BookingListView {
        if (bookingListView === null || bookingListView === {}) {
            return {};
        }
        // const copy: BookingListView = Object.assign({}, bookingListView);
        const copy: BookingListView = JSON.parse(JSON.stringify(bookingListView));

        // copy.dateCreate = this.dateUtils.toDate(bookingListView.dateCreate);
        return copy;
    }

    protected convertList(bookingListViews: BookingListView[]): BookingListView[] {
        const copy: BookingListView[] = bookingListViews;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

}
