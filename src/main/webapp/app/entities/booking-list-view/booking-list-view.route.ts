import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { BookingListViewComponent } from './booking-list-view.component';
import { BookingListViewCreateComponent } from './booking-list-view-create.component';
import { BookingListViewEditComponent } from './booking-list-view-edit.component';

@Injectable()
export class BookingListViewResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idBooking,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const bookingListViewRoute: Routes = [
    {
        path: 'booking-list-view',
        component: BookingListViewComponent,
        resolve: {
            'pagingParams': BookingListViewResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.bookingListView.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const bookingListViewPopupRoute: Routes = [
    {
        path: 'booking-list-view-new',
        component: BookingListViewEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.bookingListView.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'booking-list-view-create',
        component: BookingListViewCreateComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.bookingListView.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'booking-list-view/:id/edit',
        component: BookingListViewEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.bookingListView.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
