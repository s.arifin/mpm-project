import { BaseEntity } from './../../shared';

export class BookingListView implements BaseEntity {
    constructor(
        public id?: any,
        public idBooking?: any,
        public bookingNumber?: string,
        public vehicleNumber?: string,
        public dateCreate?: any,
        public customerId?: any,
        public vehicleId?: any,
        public slotId?: any,
        public bookingTypeId?: any,
        public eventTypeId?: any,
        public items?: any,
        public internalId?: any,
    ) {
        this.bookingTypeId = 10;
    }
}
