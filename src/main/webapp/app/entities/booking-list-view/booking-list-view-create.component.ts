import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Subscription} from 'rxjs/Rx';
import {Observable} from 'rxjs/Rx';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {BookingListView} from './booking-list-view.model';
import {BookingListViewService} from './booking-list-view.service';
import {ToasterService} from '../../shared';
import { PersonalCustomer, PersonalCustomerService } from '../personal-customer';
import { WorkOrderBooking, WorkOrderBookingService } from '../work-order-booking';
import { Vehicle, VehicleService } from '../vehicle';
import { BookingSlot, BookingSlotService } from '../booking-slot';
import { BookingType, BookingTypeService } from '../booking-type';
import { VehicleIdentification, VehicleIdentificationService } from '../vehicle-identification';
import { EventType, EventTypeService } from '../event-type';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-booking-list-view-create',
    templateUrl: './booking-list-view-create.component.html'
})
export class BookingListViewCreateComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    wo: WorkOrderBooking;
    bookingListView: BookingListView;
    isSaving: boolean;

    personalcustomers: PersonalCustomer[];

    vehicles: Vehicle[];

    bookingslots: BookingSlot[];

    bookingtypes: BookingType[];

    eventtypes: EventType[];

    internals: Internal[];

    showGreeting = false;

    bookingListViews: BookingListView[];
    itemsPerPage: any;
    totalItems: any;

    vPoliceNum: String;
    vName: String;

    constructor(
        protected alertService: JhiAlertService,
        protected bookingListViewService: BookingListViewService,
        protected personalCustomerService: PersonalCustomerService,
        protected vehicleService: VehicleService,
        protected bookingSlotService: BookingSlotService,
        protected bookingTypeService: BookingTypeService,
        protected eventTypeService: EventTypeService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected vehIdService: VehicleIdentificationService,
        protected woService: WorkOrderBookingService
    ) {
        this.bookingListView = new BookingListView();
        this.wo = new WorkOrderBooking();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.personalCustomerService.query()
            .subscribe((res: ResponseWrapper) => { this.personalcustomers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.vehicleService.query()
            .subscribe((res: ResponseWrapper) => { this.vehicles = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.bookingSlotService.query()
            .subscribe((res: ResponseWrapper) => { this.bookingslots = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.bookingTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.bookingtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.eventTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.eventtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.bookingListViewService.find(id).subscribe((bookingListView) => {
            this.bookingListView = bookingListView;
        });
    }

    previousState() {
        window.history.back();
    }

    showAntrian() {
        this.showGreeting = true;
    }

    printAntrian() {
        alert('PRINT TIKET');
    }

    loadDataLazy(event) {};

    updateRowData(event) {};

    save() {
        console.log('x2', this.bookingListView);
        this.isSaving = true;
        if (this.bookingListView.idBooking !== undefined) {
            this.subscribeToSaveResponse(
                this.bookingListViewService.update(this.bookingListView));
        } else {
            this.subscribeToSaveResponse(
                this.bookingListViewService.create(this.bookingListView));
                this.showGreeting = true;
        }
    }

    protected subscribeToSaveResponse(result: Observable<BookingListView>) {
        result.subscribe((res: BookingListView) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: BookingListView) {
        console.log('x3', result);
        this.bookingListView = result;
        this.eventManager.broadcast({ name: 'bookingListViewListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'bookingListView saved !');
        this.isSaving = false;
        // this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'bookingListView Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackPersonalCustomerById(index: number, item: PersonalCustomer) {
        return item.idCustomer;
    }

    trackVehicleById(index: number, item: Vehicle) {
        return item.idVehicle;
    }

    trackBookingSlotById(index: number, item: BookingSlot) {
        return item.idBookSlot;
    }

    trackBookingTypeById(index: number, item: BookingType) {
        return item.idBookingType;
    }

    trackEventTypeById(index: number, item: EventType) {
        return item.idEventType;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    checkVPolice(id: String) {
        this.vName = '';
        this.vehIdService.executeProcess(101, id.toUpperCase(), {}).subscribe((vehicle) => {

            console.log('x1', vehicle);
            this.vName = vehicle.customerName;

            this.bookingListView.customerId = vehicle.customerId;
            this.bookingListView.vehicleId = vehicle.vehicleId;
            this.bookingListView.vehicleNumber = vehicle.vehicleNumber;

            // Build di server tidak client, mencegah call berulang ke server
            // 101 - Untuk membuat data Booking berdasarkan VehicleIdentification
           // this.woService.executeProcess( 101, vehicle.vehicleNumber, null).subscribe((result) => {
           // });

        });
    }
}
