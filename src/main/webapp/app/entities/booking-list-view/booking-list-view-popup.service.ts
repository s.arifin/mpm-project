import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { BookingListView } from './booking-list-view.model';
import { BookingListViewService } from './booking-list-view.service';

@Injectable()
export class BookingListViewPopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected bookingListViewService: BookingListViewService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.bookingListViewService.find(id).subscribe((data) => {
                    if (data.dateCreate) {
                        data.dateCreate = this.datePipe
                            .transform(data.dateCreate, 'yyyy-MM-ddTHH:mm:ss');
                    }
                    this.ngbModalRef = this.bookingListViewModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new BookingListView();
                    this.ngbModalRef = this.bookingListViewModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    bookingListViewModalRef(component: Component, bookingListView: BookingListView): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.bookingListView = bookingListView;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
