import { BaseEntity } from './../../shared';

export class EventType implements BaseEntity {
    constructor(
        public id?: number,
        public idEventType?: number,
        public description?: string,
    ) {
    }
}
