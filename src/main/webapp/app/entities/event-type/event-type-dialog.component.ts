import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {EventType} from './event-type.model';
import {EventTypePopupService} from './event-type-popup.service';
import {EventTypeService} from './event-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-event-type-dialog',
    templateUrl: './event-type-dialog.component.html'
})
export class EventTypeDialogComponent implements OnInit {

    eventType: EventType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected eventTypeService: EventTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.eventType.idEventType !== undefined) {
            this.subscribeToSaveResponse(
                this.eventTypeService.update(this.eventType));
        } else {
            this.subscribeToSaveResponse(
                this.eventTypeService.create(this.eventType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<EventType>) {
        result.subscribe((res: EventType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: EventType) {
        this.eventManager.broadcast({ name: 'eventTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'eventType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'eventType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-event-type-popup',
    template: ''
})
export class EventTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected eventTypePopupService: EventTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.eventTypePopupService
                    .open(EventTypeDialogComponent as Component, params['id']);
            } else {
                this.eventTypePopupService
                    .open(EventTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
