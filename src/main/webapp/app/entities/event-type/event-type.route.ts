import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { EventTypeComponent } from './event-type.component';
import { EventTypePopupComponent } from './event-type-dialog.component';

@Injectable()
export class EventTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idEventType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const eventTypeRoute: Routes = [
    {
        path: 'event-type',
        component: EventTypeComponent,
        resolve: {
            'pagingParams': EventTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.eventType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const eventTypePopupRoute: Routes = [
    {
        path: 'event-type-new',
        component: EventTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.eventType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'event-type/:id/edit',
        component: EventTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.eventType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
