import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { EventType } from './event-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class EventTypeService {

    protected resourceUrl = 'api/event-types';
    protected resourceSearchUrl = 'api/_search/event-types';
    protected resourceCUrl = process.env.API_C_URL + '/api/event-types';
    protected dummy = 'http://localhost:52374/api/EventType';
    constructor(protected http: Http) { }

    create(eventType: EventType): Observable<EventType> {
        const copy = this.convert(eventType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    createC(item: EventType): Observable<EventType> {
        const copy = this.convert(item);
        return this.http.post(this.resourceCUrl + '/Create', copy).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse;
        });
    }

    updateC(item: EventType): Observable<EventType> {
        const copy = this.convert(item);
        return this.http.put(this.resourceCUrl + '/Update', copy).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse;
        });
    }

    update(eventType: EventType): Observable<EventType> {
        const copy = this.convert(eventType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<EventType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    queryC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('Masuk Event Type');
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/GetData', options)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        let result = null;
        const options = createRequestOption(req);

        if (req) {
            options.params.set('idparent', req.idparent);
            result = this.http.get(this.resourceUrl + '/by-parent', options).map((res: Response) => this.convertResponse(res));
        } else {
            result = this.http.get(this.resourceUrl, options).map((res: Response) => this.convertResponse(res));
        }

        return result;
    }

    getByParent(parrent: any): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrl + '/by-parent/' + parrent).map((res: Response) => this.convertResponse(res));
    }

    delete(idevetyp?: any): Observable<Response> {
        return this.http.delete(this.resourceCUrl + '/DelData?idevetyp=' + idevetyp);
    }

    executeProcess(eventType: any, id: Number): Observable<String> {
        const copy = this.convert(eventType);
        return this.http.post(`${this.resourceUrl}/execute/${id}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('Cari Event Type');
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/FindData', options)
            .map((res: Response) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(eventType: EventType): EventType {
        const copy: EventType = Object.assign({}, eventType);
        return copy;
    }
    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    findByParentEventType(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceCUrl + '/GetEventTypeByParent'}`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    findQueryEventName(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceCUrl + '/GetComEventInternalSearch'}`, options)
            .map((res: Response) => this.convertResponse(res));
    }
}
