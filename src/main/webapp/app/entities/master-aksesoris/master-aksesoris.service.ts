import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable, BehaviorSubject, Subject } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { MasterAksesoris } from './master-aksesoris.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class MasterAksesorisService {

    protected itemValues: MasterAksesoris[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues)

    detectChange: Subject<boolean> = new Subject<boolean>();
    public getChangeRequest = this.detectChange.asObservable();

    protected resourceUrl = 'api/master-aksesoris';
    protected resourceSearchUrl = 'api/_search/master-aksesoris';
    protected resourceCUrl = process.env.API_C_URL + '/api/MasterAccessories/';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(masterAksesoris: MasterAksesoris): Observable<MasterAksesoris> {
        const copy = this.convert(masterAksesoris);
        return this.http.post(this.resourceCUrl  + 'savedata/', copy).map((res: Response) => {
            return res.json();
        });
    }

    update(masterAksesoris: MasterAksesoris): Observable<MasterAksesoris> {
        const copy = this.convert(masterAksesoris);
        return this.http.put(this.resourceCUrl + 'savedata/', copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<MasterAksesoris> {
        return this.http.get(this.resourceCUrl + 'findData/?id=' + id).map((res: Response) => {
            return res.json();
        });
    }

    findByIdAccessories(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idAccessories', req.idAccessories);
        return this.http.get(this.resourceUrl + '/by-product', options)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('masuk query');
        return this.http.get(this.resourceCUrl + 'GetData/', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(this.resourceCUrl + 'DelData/?id=' + id);
    }

    // executeProcess(id: Number, param: String, masterAksesoris: MasterAksesoris): Observable<MasterAksesoris> {
    //     const copy = this.convert(masterAksesoris);
    //     return this.http.post(`${this.resourceCUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
    //         return res.json();
    //     });
    // }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(masterAksesoris: MasterAksesoris): MasterAksesoris {
        if (masterAksesoris === null || masterAksesoris === {}) {
            return {};
        }
        const copy: MasterAksesoris = Object.assign({}, masterAksesoris);
        return copy;
    }

    pushItems(data: MasterAksesoris[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

}
