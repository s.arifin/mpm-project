import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MasterAksesorisComponent } from './master-aksesoris.component';
import { MasterAksesorisPopupComponent } from './master-aksesoris-dialog.component';

@Injectable()
export class MasterAksesorisResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idProduct,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const masteraksesorisRoute: Routes = [
    {
        path: 'master-aksesoris',
        component: MasterAksesorisComponent,
        resolve: {
            'pagingParams': MasterAksesorisResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterAksesoris.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];

export const masteraksesorisPopupRoute: Routes = [
    {
        path: 'master-aksesoris-new',
        component: MasterAksesorisPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterAksesoris.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'master-aksesoris/:id/edit',
        component: MasterAksesorisPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.masterAksesoris.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
];
