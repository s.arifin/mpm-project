import { BaseEntity } from './../shared-component';
// import { Good } from './../good';

export class MasterAksesoris implements BaseEntity {
    constructor(
        public id?: any,
        public idAccessories?: any,
        public description?: string,
    ) {
    }
}
