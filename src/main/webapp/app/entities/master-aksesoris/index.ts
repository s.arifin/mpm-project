export * from './master-aksesoris.model';
export * from './master-aksesoris.service';
export * from './master-aksesoris.component';
export * from './master-aksesoris.route';
export * from './master-aksesoris-dialog.component';
export * from './master-aksesoris-popup.service';
