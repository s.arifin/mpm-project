import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import { MasterAksesoris } from './master-aksesoris.model'
import { MasterAksesorisPopupService } from './master-aksesoris-popup.service';
import { MasterAksesorisService } from './master-aksesoris.service';
import { ToasterService } from '../../shared';
import { ResponseWrapper } from '../../shared';
import { Motor, MotorService } from '../motor';

@Component({
    selector: 'jhi-masteraksesoris-dialog',
    templateUrl: './master-aksesoris-dialog.component.html'
})
export class MasterAksesorisDialogComponent implements OnInit {

    masterAksesoris: MasterAksesoris;
    isSaving: boolean;

    // motors: Motor[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected masterAksesorisService: MasterAksesorisService,
        // protected motorService: MotorService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        // this.masterAksesorisService.query()
        //     .subscribe((res: ResponseWrapper) => { this.masterAksesoris = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
            this.subscribeToSaveResponse(
                this.masterAksesorisService.create(this.masterAksesoris));
    }

    protected subscribeToSaveResponse(result: Observable<MasterAksesoris>) {
        result.subscribe((res: MasterAksesoris) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result) {
        this.eventManager.broadcast({ name: 'MasterAksesorisListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'masterAksesoris saved !');
        this.isSaving = false;
        this.activeModal.close(this.masterAksesorisService.detectChange.next(true));
        console.log('data dihapus');
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            // error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
        this.activeModal.dismiss(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'MasterAksesoris Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-master-aksesoris-popup',
    template: ''
})
export class MasterAksesorisPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected masterAksesorisPopupService: MasterAksesorisPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.masterAksesorisPopupService
                    .open(MasterAksesorisDialogComponent as Component, params['id']);
            } else {
                this.masterAksesorisPopupService
                    .open(MasterAksesorisDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
