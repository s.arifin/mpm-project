import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { MasterAksesoris } from './master-aksesoris.model';
import { MasterAksesorisService } from './master-aksesoris.service';

@Injectable()
export class MasterAksesorisPopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        // protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected masterAksesorisService: MasterAksesorisService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.masterAksesorisService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.masterAksesorisModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new MasterAksesoris();
                    this.ngbModalRef = this.masterAksesorisModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    masterAksesorisModalRef(component: Component, masterAksesoris: MasterAksesoris): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.masterAksesoris = masterAksesoris;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            setTimeout(() => {
                this.ngbModalRef = this.masterAksesorisLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    masterAksesorisLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: {popup: null}}], {replaceUrl: true});
            this.ngbModalRef = null;
        }, ( reason) => {
            this.router.navigate([{ outlets: {popup: null}}], {replaceUrl: true});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
