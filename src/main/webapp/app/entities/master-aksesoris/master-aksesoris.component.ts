import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import {ITEMS_PER_PAGE, Principal, ResponseWrapper} from '../../shared';
import {PaginationConfig} from '../../blocks/config/uib-pagination.config';

import {LazyLoadEvent} from 'primeng/primeng';
import {ToasterService} from '../../shared/alert/toaster.service';
import {ConfirmationService} from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading';
import { MasterAksesoris } from './master-aksesoris.model';
import { MasterAksesorisService } from './master-aksesoris.service';

@Component({
    selector: 'jhi-master-aksesoris',
    templateUrl: './master-aksesoris.component.html'
})
export class MasterAksesorisComponent implements OnInit, OnDestroy {

    currentAccount: any;
    masterAksesoris: MasterAksesoris[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected loadingService: LoadingService,
        protected masterAksesorisService: MasterAksesorisService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.masterAksesorisService.getChangeRequest.subscribe((res) => {
            this.loadAll();
        });
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.masterAksesorisService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.masterAksesorisService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        this.loadingService.loadingStop();
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/master-aksesoris'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/master-aksesoris', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/master-aksesoris', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInMasterAksesoris();
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMasterAksesoris() {
        this.eventSubscriber = this.eventManager.subscribe('MasterAksesorisListModification', (Response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idAccessories') {
            result.push('idAccessories');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        // this.totalItems = headers.get('X-Total-Count');
        console.log('debug', data);
        if (data.count > 0) {
            this.totalItems = data.TotalData;
        } else {
            this.totalItems = 0;
        }

        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.masterAksesoris = data;
        console.log('this.masterAksesoris', this.masterAksesoris);
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    // executeProcess(data) {
    //     this.masterAksesorisService.executeProcess(10, '', data).subscribe(
    //        (value) => console.log('this: ', value),
    //        (err) => console.log(err),
    //        () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
    //     );
    // }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.masterAksesorisService.update(event.data)
                .subscribe((res: MasterAksesoris) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.masterAksesorisService.create(event.data)
                .subscribe((res: MasterAksesoris) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: MasterAksesoris) {
        this.toasterService.showToaster('info', 'masterAksesoris Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            // error.message = error.text();
        }
        this.onError(error);
        // this.loadAll();
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.masterAksesorisService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'MasterAksesorisListModification',
                    content: 'Deleted an masterAksesoris'
                    });
                    this.loadAll();
                });
            }
        });
    }
}
