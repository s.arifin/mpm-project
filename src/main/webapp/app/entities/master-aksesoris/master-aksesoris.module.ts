import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    MasterAksesorisComponent,
    masteraksesorisRoute,
    masteraksesorisPopupRoute,
    MasterAksesorisResolvePagingParams,
    MasterAksesorisService,
    MasterAksesorisDialogComponent,
    MasterAksesorisPopupComponent,
} from './';

import { CommonModule } from '@angular/common';

import {CurrencyMaskModule} from 'ng2-currency-mask';

import {
        CheckboxModule,
        InputTextModule,
        CalendarModule,
        DropdownModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        DialogModule,
        ConfirmDialogModule,
        ConfirmationService,
        GrowlModule,
        DataGridModule,
        SharedModule,
        AccordionModule,
        TabViewModule,
        FieldsetModule,
        ScheduleModule,
        PanelModule,
        ListboxModule,
        SelectItem,
        MenuItem,
        Header,
        Footer} from 'primeng/primeng';

// import { MpmPriceComponentModule } from '../price-component/price-component.module';
import { MpmRuleHotItemModule } from '../rule-hot-item/rule-hot-item.module';
import { MpmSharedEntityModule } from '../shared-entity.module';
import { MasterAksesorisPopupService } from './master-aksesoris-popup.service';

const ENTITY_STATES = [
    ...masteraksesorisRoute,
    ...masteraksesorisPopupRoute
];

@NgModule({
    imports: [
        MpmSharedEntityModule,
        InputTextareaModule,
        // MpmRuleHotItemModule,
        MpmSharedModule,
        // MpmPriceComponentModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        DataGridModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        CurrencyMaskModule,
    ],
    exports: [
        MasterAksesorisComponent,
    ],
    declarations: [
        MasterAksesorisComponent,
        MasterAksesorisDialogComponent,
        MasterAksesorisPopupComponent,

    ],
    entryComponents: [
        MasterAksesorisComponent,
        MasterAksesorisDialogComponent,
        MasterAksesorisPopupComponent,

    ],
    providers: [
        MasterAksesorisService,
        MasterAksesorisPopupService,
        MasterAksesorisResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMasterAksesorisModule {}
