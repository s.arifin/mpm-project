import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { BillingReceipt } from './billing-receipt.model';
import { BillingReceiptService } from './billing-receipt.service';
import { ToasterService} from '../../shared';
import { BillingType, BillingTypeService } from '../billing-type';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-billing-receipt-edit',
    templateUrl: './billing-receipt-edit.component.html'
})
export class BillingReceiptEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    billingReceipt: BillingReceipt;
    isSaving: boolean;
    idBilling: any;
    paramPage: number;
    routeId: number;

    billingtypes: BillingType[];

    internals: Internal[];

    billtos: BillTo[];

    constructor(
        protected alertService: JhiAlertService,
        protected billingReceiptService: BillingReceiptService,
        protected billingTypeService: BillingTypeService,
        protected internalService: InternalService,
        protected billToService: BillToService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.billingReceipt = new BillingReceipt();
        this.routeId = 0;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idBilling = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        this.billingTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.billingtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.billingReceiptService.find(this.idBilling).subscribe((billingReceipt) => {
            this.billingReceipt = billingReceipt;
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['billing-receipt', { page: this.paramPage }]);
        }
    }

    save() {
        this.isSaving = true;
        if (this.billingReceipt.idBilling !== undefined) {
            this.subscribeToSaveResponse(
                this.billingReceiptService.update(this.billingReceipt));
        } else {
            this.subscribeToSaveResponse(
                this.billingReceiptService.create(this.billingReceipt));
        }
    }

    protected subscribeToSaveResponse(result: Observable<BillingReceipt>) {
        result.subscribe((res: BillingReceipt) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: BillingReceipt) {
        this.eventManager.broadcast({ name: 'billingReceiptListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'billingReceipt saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'billingReceipt Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackBillingTypeById(index: number, item: BillingType) {
        return item.idBillingType;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}
