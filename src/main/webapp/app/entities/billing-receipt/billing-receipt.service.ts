import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { BillingReceipt } from './billing-receipt.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class BillingReceiptService {
    protected itemValues: BillingReceipt[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/billing-receipts';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/billing-receipts';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(billingReceipt: BillingReceipt): Observable<BillingReceipt> {
        const copy = this.convert(billingReceipt);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(billingReceipt: BillingReceipt): Observable<BillingReceipt> {
        const copy = this.convert(billingReceipt);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<BillingReceipt> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(billingReceipt: BillingReceipt, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(billingReceipt);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<BillingReceipt> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<BillingReceipt[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to BillingReceipt.
     */
    protected convertItemFromServer(json: any): BillingReceipt {
        const entity: BillingReceipt = Object.assign(new BillingReceipt(), json);
        if (entity.dateCreate) {
            entity.dateCreate = new Date(entity.dateCreate);
        }
        if (entity.dateDue) {
            entity.dateDue = new Date(entity.dateDue);
        }
        return entity;
    }

    /**
     * Convert a BillingReceipt to a JSON which can be sent to the server.
     */
    protected convert(billingReceipt: BillingReceipt): BillingReceipt {
        if (billingReceipt === null || billingReceipt === {}) {
            return {};
        }
        // const copy: BillingReceipt = Object.assign({}, billingReceipt);
        const copy: BillingReceipt = JSON.parse(JSON.stringify(billingReceipt));

        // copy.dateCreate = this.dateUtils.toDate(billingReceipt.dateCreate);

        // copy.dateDue = this.dateUtils.toDate(billingReceipt.dateDue);
        return copy;
    }

    protected convertList(billingReceipts: BillingReceipt[]): BillingReceipt[] {
        const copy: BillingReceipt[] = billingReceipts;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: BillingReceipt[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
