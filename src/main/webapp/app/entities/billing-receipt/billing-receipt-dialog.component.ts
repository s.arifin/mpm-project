import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { BillingReceipt } from './billing-receipt.model';
import { BillingReceiptPopupService } from './billing-receipt-popup.service';
import { BillingReceiptService } from './billing-receipt.service';
import { ToasterService } from '../../shared';
import { BillingType, BillingTypeService } from '../billing-type';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-billing-receipt-dialog',
    templateUrl: './billing-receipt-dialog.component.html'
})
export class BillingReceiptDialogComponent implements OnInit {

    billingReceipt: BillingReceipt;
    isSaving: boolean;
    idBillingType: any;
    idInternal: any;
    idBillTo: any;
    idBillFrom: any;

    billingtypes: BillingType[];

    internals: Internal[];

    billtos: BillTo[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected billingReceiptService: BillingReceiptService,
        protected billingTypeService: BillingTypeService,
        protected internalService: InternalService,
        protected billToService: BillToService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.billingTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.billingtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.billingReceipt.idBilling !== undefined) {
            this.subscribeToSaveResponse(
                this.billingReceiptService.update(this.billingReceipt));
        } else {
            this.subscribeToSaveResponse(
                this.billingReceiptService.create(this.billingReceipt));
        }
    }

    protected subscribeToSaveResponse(result: Observable<BillingReceipt>) {
        result.subscribe((res: BillingReceipt) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: BillingReceipt) {
        this.eventManager.broadcast({ name: 'billingReceiptListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'billingReceipt saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'billingReceipt Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackBillingTypeById(index: number, item: BillingType) {
        return item.idBillingType;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}

@Component({
    selector: 'jhi-billing-receipt-popup',
    template: ''
})
export class BillingReceiptPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected billingReceiptPopupService: BillingReceiptPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.billingReceiptPopupService
                    .open(BillingReceiptDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.billingReceiptPopupService.parent = params['parent'];
                this.billingReceiptPopupService
                    .open(BillingReceiptDialogComponent as Component);
            } else {
                this.billingReceiptPopupService
                    .open(BillingReceiptDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
