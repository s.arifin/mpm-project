import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { BillingReceiptComponent } from './billing-receipt.component';
import { BillingReceiptEditComponent } from './billing-receipt-edit.component';
import { BillingReceiptPopupComponent } from './billing-receipt-dialog.component';

@Injectable()
export class BillingReceiptResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'billingNumber,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const billingReceiptRoute: Routes = [
    {
        path: 'billing-receipt',
        component: BillingReceiptComponent,
        resolve: {
            'pagingParams': BillingReceiptResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const billingReceiptPopupRoute: Routes = [
    {
        path: 'billing-receipt-popup-new',
        component: BillingReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingReceipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'billing-receipt-new',
        component: BillingReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'billing-receipt/:id/edit',
        component: BillingReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'billing-receipt/:route/:page/:id/edit',
        component: BillingReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'billing-receipt/:id/popup-edit',
        component: BillingReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingReceipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
