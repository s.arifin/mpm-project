import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { BillingReceipt } from './billing-receipt.model';
import { BillingReceiptService } from './billing-receipt.service';

@Injectable()
export class BillingReceiptPopupService {
    protected ngbModalRef: NgbModalRef;
    idBillingType: any;
    idInternal: any;
    idBillTo: any;
    idBillFrom: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected billingReceiptService: BillingReceiptService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.billingReceiptService.find(id).subscribe((data) => {
                    // if (data.dateCreate) {
                    //    data.dateCreate = this.datePipe
                    //        .transform(data.dateCreate, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    // if (data.dateDue) {
                    //    data.dateDue = this.datePipe
                    //        .transform(data.dateDue, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.billingReceiptModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new BillingReceipt();
                    data.billingTypeId = this.idBillingType;
                    data.internalId = this.idInternal;
                    data.billToId = this.idBillTo;
                    data.billFromId = this.idBillFrom;
                    this.ngbModalRef = this.billingReceiptModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    billingReceiptModalRef(component: Component, billingReceipt: BillingReceipt): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.billingReceipt = billingReceipt;
        modalRef.componentInstance.idBillingType = this.idBillingType;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idBillTo = this.idBillTo;
        modalRef.componentInstance.idBillFrom = this.idBillFrom;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.billingReceiptLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    billingReceiptLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
