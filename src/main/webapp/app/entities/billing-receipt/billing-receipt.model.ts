import { BaseEntity } from './../../shared';

export class BillingReceipt implements BaseEntity {
    constructor(
        public id?: any,
        public idBilling?: any,
        public billingNumber?: string,
        public dateCreate?: any,
        public dateDue?: any,
        public details?: any,
        public billingTypeId?: any,
        public payments?: any,
        public internalId?: any,
        public billToId?: any,
        public billFromId?: any,
    ) {
    }
}
