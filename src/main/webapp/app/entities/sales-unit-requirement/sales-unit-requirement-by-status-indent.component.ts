import { Component, OnInit } from '@angular/core';

import * as BaseConstant from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-sales-unit-requirement-by-status-indent',
    templateUrl: './sales-unit-requirement-by-status-indent.component.html'
})

export class SalesUnitRequirementByStatusIndentComponent implements OnInit {
    public statusApproval: number;
    public status: number;
    public btnEdit: boolean;
    public btnSubmit: boolean;

    constructor() {

    }

    ngOnInit() {
        this.statusApproval = BaseConstant.Status.STATUS_APPROVED;
        this.status = BaseConstant.Status.STATUS_INDENT;
        this.btnEdit = true;
        this.btnSubmit = true;
    }
}
