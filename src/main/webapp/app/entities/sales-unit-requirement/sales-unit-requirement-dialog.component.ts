import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';
import { CurrencyPipe, DatePipe } from '@angular/common';

import {SalesUnitRequirement, Subsidi} from './sales-unit-requirement.model';
import {SalesUnitRequirementPopupService} from './sales-unit-requirement-popup.service';
import {SalesUnitRequirementService} from './sales-unit-requirement.service';
import {ToasterService, CommonUtilService, Principal} from '../../shared';
import { Customer, CustomerService } from '../customer';
import { Salesman, SalesmanService } from '../salesman';
import { Internal, InternalService } from '../internal';
import { SaleType, SaleTypeService } from '../sale-type';
import { BillTo, BillToService } from '../bill-to';
import { ShipTo, ShipToService } from '../ship-to';
import { Person } from '../person';
import { SalesBroker, SalesBrokerService } from '../sales-broker';
import { Feature, FeatureService } from '../feature';
import { Motor, MotorService } from '../motor';
import { ResponseWrapper } from '../../shared';
import { PriceComponentService, PriceComponent } from '../price-component';
import { LoadingService } from '../../layouts/loading/loading.service';
import { OrganizationCustomerService, OrganizationCustomer } from '../shared-component';
import { PersonalCustomerService } from '../personal-customer/personal-customer.service';
import { RequirementType, RequirementTypeService } from '../requirement-type';

import * as SurConstant from '../../shared/constants/sales-unit-requirement.constants';
import * as reqTypeConstants from '../../shared/constants/requirement-type.constants';
import * as PriceTypeConstants from '../../shared/constants/price-type.constants';
import * as _ from 'lodash';
import * as BaseConstants from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-sales-unit-requirement-dialog',
    templateUrl: './sales-unit-requirement-dialog.component.html'
})
export class SalesUnitRequirementDialogComponent implements OnInit {
    public idRequest: string;

    salesUnitRequirement: SalesUnitRequirement;
    isSaving: boolean;

    customers: Customer[];

    salesmen: Salesman[];

    internals: Internal[];

    saletypes: SaleType[];

    billtos: BillTo[];

    shiptos: ShipTo[];

    salesbrokers: SalesBroker[];

    features: Feature[];

    motors: Motor[];

    protected totalSubsidi: number;

    public isOrganization: boolean;
    public isPerson: boolean;
    public totalReceipt: number;
    public isCredit: boolean;
    public organizationCustomer: OrganizationCustomer;
    public remainingDownPayment: any;
    public years: Array<number>;
    public fetchMotor: boolean;
    public motorsForSelect: Array<Object>;
    public selectedMotor: any;
    public subsidi: Subsidi;
    protected BBNPrice: number;
    protected HETPrice: number;
    public firstPrice: number;
    public tenors: Array<number>;
    public installment: number;
    public remainingPayment: any;
    public requirementTypes: Array<RequirementType>;

    constructor(
        public activeModal: NgbActiveModal,

        protected priceComponentService: PriceComponentService,
        protected alertService: JhiAlertService,
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected customerService: CustomerService,
        protected salesmanService: SalesmanService,
        protected internalService: InternalService,
        protected saleTypeService: SaleTypeService,
        protected billToService: BillToService,
        protected shipToService: ShipToService,
        protected salesBrokerService: SalesBrokerService,
        protected featureService: FeatureService,
        protected motorService: MotorService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected commontUtilService: CommonUtilService,
        protected loadingService: LoadingService,
        protected principalService: Principal,
        protected organizationCustomerService: OrganizationCustomerService,
        protected personalCustomerService: PersonalCustomerService,
        protected currencyPipe: CurrencyPipe,
        protected requirementTypeService: RequirementTypeService
    ) {
        this.isOrganization = false;
        this.isPerson = false;
        this.motorsForSelect = new Array<Object>();
        this.fetchMotor = true;
        this.isCredit = false;
        this.requirementTypes = this.requirementTypeService.getRequirementType();
    }

    ngOnInit() {
        this.getYear();
        this.isSaving = false;
        this.saleTypeService.query().subscribe((res: ResponseWrapper) => {
                this.saletypes = this.commontUtilService.getDataByParent(res.json, 1);
            }, (res: ResponseWrapper) => this.onError(res.json));
        if (this.salesUnitRequirement.idRequirement !== null && this.salesUnitRequirement.idRequirement !== undefined) {
            this.load(this.salesUnitRequirement.idRequirement);
        } else {
            this.loadMotor();
        }
    }

    public load(id: string): void {
        this.loadingService.loadingStart();
        try {
            this.salesUnitRequirementService.find(id).subscribe((salesUnitRequirement: SalesUnitRequirement) => {
                this.selectedMotor = salesUnitRequirement.productId;

                if (salesUnitRequirement.personOwner === null) {
                    salesUnitRequirement.personOwner = new Person();
                    salesUnitRequirement.personOwner.dob = null;
                }

                if (salesUnitRequirement.personCustomer === null) {
                    salesUnitRequirement.personCustomer = new Person();
                    salesUnitRequirement.personCustomer.dob = null;
                }

                if (salesUnitRequirement.productId !== null) {
                    salesUnitRequirement.productId = salesUnitRequirement.productId.toUpperCase();
                    this.getMotorPriceByIdProduct(salesUnitRequirement.productId);
                }

                if (salesUnitRequirement.idReqTyp === reqTypeConstants.ORGANIZATION_CUSTOMER) {
                    this.getDataByOrganization(salesUnitRequirement);
                } else if (salesUnitRequirement.idReqTyp === null) {
                    if (salesUnitRequirement.customerId !== null) {
                        this.getDataByPersonal(salesUnitRequirement);
                    } else {
                        this.loadMotor().then(
                            () => {
                                this.selectMotor();
                                this.selectSaleType();
                            }
                        )
                    }
                }
            });
        } finally {
            this.loadingService.loadingStop();
        }
    }

    protected countMotorPriceBySaleType(saleTypeId: number): void {
        if (this.saleTypeService.checkIfOffTheRoad(saleTypeId)) {
            this.salesUnitRequirement.unitPrice = this.firstPrice - this.BBNPrice;
        } else {
           if ( this.salesUnitRequirement.unitPrice !== (this.BBNPrice + this.HETPrice)) {
               this.salesUnitRequirement.unitPrice = this.firstPrice;
           }
        }
        this.setMinPayment();
    }

    public selectSaleType(): void {
        const saleTypeId = this.salesUnitRequirement.saleTypeId;
        this.countMotorPriceBySaleType(saleTypeId);
        if (this.saleTypeService.checkIfCash(saleTypeId)) { // Cash
            this.salesUnitRequirement.subsfincomp = 0;
            this.isCredit = false;
            this.salesUnitRequirement.leasingCompanyId = null;
            this.salesUnitRequirement.leasingTenorProvideId = null;
            this.tenors = Array();
            this.installment = 0;
            this.countRemainingPayment();

            this.salesUnitRequirement.bbnprice = this.BBNPrice;
        } else if (this.saleTypeService.checkIfCredit(saleTypeId)) { // Credit Sales
            this.isCredit = true;
            this.countRemainingDownPayment();
            this.salesUnitRequirement.bbnprice = this.BBNPrice;
        } else if (this.saleTypeService.checkIfOffTheRoad(saleTypeId)) { // Off The Road
            this.salesUnitRequirement.subsfincomp = 0;
            this.isCredit = false;
            this.salesUnitRequirement.leasingCompanyId = null;
            this.salesUnitRequirement.leasingTenorProvideId = null;
            this.tenors = Array();
            this.installment = 0;
            this.countRemainingPayment();
            this.salesUnitRequirement.bbnprice = 0;
        } else {
            this.isCredit = false;
            this.salesUnitRequirement.subsfincomp = 0;
        }
        this.setMinPayment();
    }

    public countRemainingDownPayment(): void {
        const productPrice = this.salesUnitRequirement.unitPrice;
        this.remainingDownPayment = this.salesUnitRequirementService.countRemainingDownPaymentForCredit(this.salesUnitRequirement, this.totalReceipt);
        if (productPrice !== null) {
            this.countTotalSubsidi();
            this.salesUnitRequirement.downPayment = this.totalSubsidi + this.totalReceipt;
        }
        this.setMinPayment();
    }

    public countTotalSubsidi(): void {
        this.totalSubsidi = this.salesUnitRequirementService.fnCountTotalSubsidi(this.salesUnitRequirement);
    }

    public countRemainingPayment(): void {
        const productPrice = this.salesUnitRequirement.unitPrice;
        if (productPrice !== null) {
            this.countTotalSubsidi();
            this.remainingPayment = this.currencyPipe.transform(productPrice - this.totalSubsidi - this.totalReceipt, 'IDR', true);

            this.salesUnitRequirement.downPayment = this.totalSubsidi + this.totalReceipt;
        } else {
            this.remainingPayment = 'Please select motor';
        }
    }

    protected getDataByPersonal(salesUnitRequirement: SalesUnitRequirement): void {
        this.personalCustomerService.find(salesUnitRequirement.customerId).subscribe(
            (res) => {
                salesUnitRequirement.partyId = res.partyId;
                this.salesUnitRequirement = salesUnitRequirement;
                this.loadMotor().then(
                    () => {
                        this.selectMotor();
                        this.selectSaleType();
                    }
                )
                this.loadingService.loadingStop();
            },
            (err) => {
                this.commontUtilService.showError(err);
                this.loadingService.loadingStop();
            }
        )
    }

    public selectRequirementType(): void {
        if (this.salesUnitRequirement.idReqTyp === reqTypeConstants.ORGANIZATION_CUSTOMER) {
            this.salesUnitRequirement.organizationCustomer = new OrganizationCustomer();
            this.showPersonOrOrganization('organization');
        } else  {
            this.salesUnitRequirement.personCustomer = new Person();
            this.showPersonOrOrganization('person');
        }
    }

    protected getDataByOrganization(salesUnitRequirement: SalesUnitRequirement): void {
        salesUnitRequirement.partyId = salesUnitRequirement.organizationCustomer.partyId;
        this.salesUnitRequirement = salesUnitRequirement;
        this.loadMotor().then(
            () => {
                this.selectMotor();
                this.selectSaleType();
            }
        )
    }

    protected getMotorPriceByIdProduct(_idproduct: string): void {
        const obj = {
            idproduct : _idproduct,
            idInternal : this.salesUnitRequirement.internalId
        };
        this.motorService.getPrice(obj).subscribe(
            (res) => {
                const priceComponents: PriceComponent[] = res.json;

                // binding for BBN price
                const priceComponentBBN: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.BBN]);
                if (priceComponentBBN !== undefined) {
                    this.BBNPrice = priceComponentBBN.price;
                }

                // binding for HET price
                const priceComponentHET: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.HET]);
                if (priceComponentHET !== undefined) {
                    this.HETPrice = priceComponentHET.price;
                }

                this.firstPrice = this.priceComponentService.countMotorPrice(priceComponents);
            }
        )
    }

    protected getColorByMotor(idProduct: string): void {
        if (idProduct !== null) {
            this.features = new Array<Feature>();
            const selectedProduct: Motor = _.find(this.motors, function(e) {
                return e.idProduct.toLowerCase() === idProduct.toLowerCase();
            });
            this.features = selectedProduct.features;
        }
    }

    protected resetLeasing(): void {
        this.salesUnitRequirement.leasingCompanyId = null;
        // this.selectLeasing();
        this.remainingDownPayment = this.salesUnitRequirementService.notSelectLeasingTenorProvideMessage;
    }

    public selectMotor(isSelect?: boolean): void {
        this.salesUnitRequirement.productId = this.selectedMotor;
        this.getColorByMotor(this.salesUnitRequirement.productId);
        // this.setIsHotItemAndIsIndent(this.salesUnitRequirement.productId);
        if (isSelect) {// dijalankan ketika melakukan pemilihan motor
            this.resetLeasing();
            const obj = {
                idproduct : this.salesUnitRequirement.productId,
                idinternal : this.salesUnitRequirement.internalId
            };
            this.motorService.getPrice(obj).subscribe(
                (res) => {
                    const priceComponents: PriceComponent[] = res.json;

                    // binding for subsidi AHM
                    const priceComponentSubsidiAHM: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.SUBSIDI_AHM]);
                    this.salesUnitRequirement.subsahm = 0;
                    if (priceComponentSubsidiAHM !== undefined) {
                    //     this.salesUnitRequirement.subsahm = priceComponentSubsidiAHM.price;
                        this.subsidi.subsidiAHM = priceComponentSubsidiAHM.price;
                    }

                    // binding for subsidi main dealer
                    const priceComponentSubsidiMD: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.SUBSIDI_MD]);
                    this.salesUnitRequirement.subsmd = 0;
                    if (priceComponentSubsidiMD !== undefined) {
                    //     this.salesUnitRequirement.subsmd = priceComponentSubsidiMD.price;
                        this.subsidi.subsidiMD = priceComponentSubsidiMD.price;
                    }

                    // binding for BBN price
                    const priceComponentBBN: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.BBN]);

                    this.salesUnitRequirement.bbnprice = 0;
                    if (priceComponentBBN !== undefined) {
                        this.BBNPrice = priceComponentBBN.price;
                        this.salesUnitRequirement.bbnprice = priceComponentBBN.price;
                    }

                    // binding for HET price
                    const priceComponentHET: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.HET]);
                    this.salesUnitRequirement.hetprice = 0;

                    if (priceComponentHET !== undefined) {
                        this.HETPrice = priceComponentHET.price;
                        this.salesUnitRequirement.hetprice = priceComponentHET.price;
                    }
                    // binding for unit price
                    this.salesUnitRequirement.unitPrice = this.priceComponentService.countMotorPrice(priceComponents);

                    this.firstPrice = this.priceComponentService.countMotorPrice(priceComponents);

                    this.setMinPayment();
                }, (err) => {
                   this.commontUtilService.showError(err);
                }
            );
        }
    }

    public setMinPayment() {
        const saleTypeId: number = this.salesUnitRequirement.saleTypeId;
        if (this.saleTypeService.checkIfCash(saleTypeId)) {
            this.salesUnitRequirement.minPayment = this.salesUnitRequirement.unitPrice;
        } else if (this.saleTypeService.checkIfCredit(saleTypeId)) {
            this.salesUnitRequirement.minPayment = this.salesUnitRequirement.creditDownPayment;
        } else if (this.saleTypeService.checkIfOffTheRoad(saleTypeId)) {
            this.salesUnitRequirement.minPayment = this.salesUnitRequirement.hetprice;
        }
    }

    protected resetShowPersonOrOrganization(): void {
        this.isPerson = false;
        this.isOrganization = false;
    }

    public showPersonOrOrganization(data: string): void {
        this.resetShowPersonOrOrganization();
        if (data === 'person') {
            this.isPerson = true;
        } else if (data === 'organization') {
            this.isOrganization = true;
        }
    }

    loadMotor(): Promise<Motor[]> {
        return new Promise<Motor[]>(
            (resolve) => {
                this.motorService.query(
                    {
                        page: 0,
                        size: 10000,
                        sort: ['idProduct', 'asc']
                    }
                ).subscribe(
                    (res: ResponseWrapper) => {
                        this.fetchMotor = false;
                        this.convertMotorForSelect(res.json);
                        this.motors = res.json;
                    },
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => {
                        resolve();
                });
            }
        );
    }

    protected convertMotorForSelect(data: Array<Motor>) {
        if (data.length > 0) {
            data.forEach(
                (m) => {
                    const obj: object = {
                        label : m.idProduct + ' - ' + m.description,
                        value : m.idProduct
                    };
                    this.motorsForSelect.push(obj);
                }
            )
        }
    }

    public getYear(): void {
        this.years = new Array();
        this.years = this.salesUnitRequirementService.optionYearAssembly();
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    public save(): void {
        this.isSaving = true;
        if (this.salesUnitRequirement.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.salesUnitRequirementService.update(this.salesUnitRequirement));
        } else {
            this.subscribeToSaveResponse(
                this.salesUnitRequirementService.create(this.salesUnitRequirement));
        }
    }

    protected subscribeToSaveResponse(result: Observable<SalesUnitRequirement>) {
        result.subscribe((res: SalesUnitRequirement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: SalesUnitRequirement) {
        this.eventManager.broadcast({ name: 'salesUnitRequirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'salesUnitRequirement saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'salesUnitRequirement Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idPartyRole;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackSaleTypeById(index: number, item: SaleType) {
        return item.idSaleType;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }

    trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }

    trackSalesBrokerById(index: number, item: SalesBroker) {
        return item.idPartyRole;
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }
}

@Component({
    selector: 'jhi-sales-unit-requirement-popup',
    template: ''
})
export class SalesUnitRequirementPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected salesUnitRequirementPopupService: SalesUnitRequirementPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.salesUnitRequirementPopupService
                    .open(SalesUnitRequirementDialogComponent as Component, params['id'], null);
            } else if (params['idRequest']) {
                this.salesUnitRequirementPopupService.idRequest = params['idRequest'];
                this.salesUnitRequirementPopupService
                    .open(SalesUnitRequirementDialogComponent as Component, null, params['idRequest']);
            } else {
                this.salesUnitRequirementPopupService
                    .open(SalesUnitRequirementDialogComponent as Component, null, null);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
