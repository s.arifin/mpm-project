import { Component, OnInit, OnDestroy } from '@angular/core';
import { Route, Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UnitDocumentMessage, UnitDocumentMessageService } from '../unit-document-message';

@Component({
    selector: 'jhi-sales-unit-requirement-status-unit-document-message',
    templateUrl: './sales-unit-requirement-status-unit-document-message.component.html'
})

export class SalesUnitRequirementStatusUnitDocumentMessageComponent implements OnInit {
    protected subscription: Subscription;

    public idSalesUnitRequirement: string;
    constructor(
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected route: ActivatedRoute
    ) {

    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idSalesUnitRequirement = params['id'];
            }
        });
    }
}
