import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { SalesUnitRequirement } from './sales-unit-requirement.model';
import { SalesUnitRequirementService } from './sales-unit-requirement.service';
import { Principal } from '../../shared';

@Injectable()
export class SalesUnitRequirementPopupService {
    protected ngbModalRef: NgbModalRef;

    public idRequest: string;

    constructor(
        protected principalService: Principal,
        protected modalService: NgbModal,
        protected router: Router,
        protected salesUnitRequirementService: SalesUnitRequirementService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any, idRequest?: string | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.salesUnitRequirementService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.salesUnitRequirementModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new SalesUnitRequirement();
                    data.internalId = this.principalService.getIdInternal();
                    if (idRequest !== null) {
                        data.idRequest = idRequest;
                    }
                    this.ngbModalRef = this.salesUnitRequirementModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    salesUnitRequirementModalRef(component: Component, salesUnitRequirement: SalesUnitRequirement): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.salesUnitRequirement = salesUnitRequirement;
        modalRef.componentInstance.idRequest = this.idRequest;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.salesUnitRequirementLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    salesUnitRequirementLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
