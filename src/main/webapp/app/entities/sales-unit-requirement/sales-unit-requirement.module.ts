import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';
import { CurrencyPipe } from '@angular/common';

import {MpmSharedModule} from '../../shared';

import {
    SalesUnitRequirementByStatusIndentComponent,
    SalesUnitRequirementByStatusAndApprovalComponent,
    SalesUnitRequirementByStatusApprovalAndSalesComponent,
    SalesUnitRequirementService,
    SalesUnitRequirementPopupService,
    SalesUnitRequirementComponent,
    SalesUnitRequirementDialogComponent,
    SalesUnitRequirementPopupComponent,
    salesUnitRequirementRoute,
    salesUnitRequirementPopupRoute,
    SalesUnitRequirementResolvePagingParams,
    SalesUnitRequirementAsListComponent,
    SalesUnitRequirementAsLovComponent,
    SalesUnitRequirementLovPopupComponent,
    SalesUnitRequirementEditComponent,
    SalesUnitRequirementDealDetailViewComponent,
    SalesUnitRequirementPaymentViewComponent,
    SalesUnitRequirementCustomerViewComponent,
    SalesUnitRequirementInformationViewComponent,
    SalesUnitRequirementSTNKViewComponent,
    SalesUnitRequirementMessageViewComponent,
    SalesUnitRequirementDetailComponent,
    SalesUnitRequirementFollowUpViewComponent,
    SalesUnitRequirementDocumentViewComponent,
    SalesUnitRequirementCheckAvailabilityComponent,
    SalesUnitRequirementStatusUnitDocumentMessageComponent,
    SalesUnitRequirementDetailContainerComponent,
    SalesUnitRequirementForProspectByApprovalComponent,
    SalesUnitRequirementForProspectComponent,
    SalesUnitRequirementForProspectEditComponent,
    SalesUnitRequirementPersonComponent
} from './';

import { MpmReceiptModule } from '../receipt/receipt.module';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { MpmProspectModule } from '../prospect/prospect.module';

import { MpmUnitDocumentMessageModule } from '../unit-document-message/unit-document-message.module';

import {
    AutoCompleteModule,
    InputSwitchModule,
    CheckboxModule,
    InputTextModule,
    InputTextareaModule,
    CalendarModule,
    DropdownModule,
    EditorModule,
    ButtonModule,
    DataTableModule,
    DataListModule,
    DataGridModule,
    DataScrollerModule,
    CarouselModule,
    PickListModule,
    PaginatorModule,
    DialogModule,
    ConfirmDialogModule,
    ConfirmationService,
    GrowlModule,
    SharedModule,
    AccordionModule,
    TabViewModule,
    FieldsetModule,
    ScheduleModule,
    PanelModule,
    ListboxModule,
    ChartModule,
    DragDropModule,
    LightboxModule,
    MessagesModule,
    RadioButtonModule
} from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';
import { MessagePipe } from '../../shared/pipe/message.pipe';
import { MpmPartyDocumentModule } from '../party-document';
import { SURReceiptDialogComponent, SURReceiptPopupComponent } from './include/sur-receipt-dialog.component';
import { SURReceiptAsListComponent } from './include/sur-receipt-as-list.component';
import { ReceiptPopupService } from '../receipt/receipt-popup.service';

const ENTITY_STATES = [
    ...salesUnitRequirementRoute,
    ...salesUnitRequirementPopupRoute,
];

@NgModule({
    imports: [
        // MpmUnitDocumentMessageModule,
        // MpmProspectModule,
        MessagesModule,
        // MpmPartyDocumentModule,
        MpmSharedEntityModule,
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        InputSwitchModule,
        CommonModule,
        EditorModule,
        AutoCompleteModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        RadioButtonModule
    ],
    exports: [
        SalesUnitRequirementAsListComponent,
        SalesUnitRequirementForProspectComponent,
        SalesUnitRequirementDetailContainerComponent,
        SalesUnitRequirementDealDetailViewComponent,
        SalesUnitRequirementPaymentViewComponent,
        SalesUnitRequirementCustomerViewComponent,
        SalesUnitRequirementInformationViewComponent,
        SalesUnitRequirementSTNKViewComponent,
        SalesUnitRequirementMessageViewComponent,
        SalesUnitRequirementDetailComponent,
        SalesUnitRequirementFollowUpViewComponent,
        SalesUnitRequirementDocumentViewComponent,
        SURReceiptDialogComponent,
        SURReceiptAsListComponent,
        SURReceiptPopupComponent,
        SalesUnitRequirementForProspectByApprovalComponent,
        SalesUnitRequirementForProspectEditComponent,
        SalesUnitRequirementByStatusApprovalAndSalesComponent
    ],
    declarations: [
        SalesUnitRequirementPersonComponent,
        SalesUnitRequirementForProspectEditComponent,
        SalesUnitRequirementForProspectByApprovalComponent,
        SalesUnitRequirementForProspectComponent,
        SalesUnitRequirementDetailContainerComponent,
        SalesUnitRequirementStatusUnitDocumentMessageComponent,
        SalesUnitRequirementByStatusIndentComponent,
        SalesUnitRequirementByStatusAndApprovalComponent,
        SalesUnitRequirementStatusUnitDocumentMessageComponent,
        SalesUnitRequirementCheckAvailabilityComponent,
        SalesUnitRequirementByStatusApprovalAndSalesComponent,
        SalesUnitRequirementComponent,
        SalesUnitRequirementDialogComponent,
        SalesUnitRequirementPopupComponent,
        SalesUnitRequirementAsListComponent,
        SalesUnitRequirementAsLovComponent,
        SalesUnitRequirementLovPopupComponent,
        SalesUnitRequirementEditComponent,
        SalesUnitRequirementDealDetailViewComponent,
        SalesUnitRequirementPaymentViewComponent,
        SalesUnitRequirementCustomerViewComponent,
        SalesUnitRequirementInformationViewComponent,
        SalesUnitRequirementSTNKViewComponent,
        SalesUnitRequirementMessageViewComponent,
        SalesUnitRequirementDetailComponent,
        SalesUnitRequirementFollowUpViewComponent,
        SalesUnitRequirementDocumentViewComponent,
        SURReceiptDialogComponent,
        SURReceiptAsListComponent,
        SURReceiptPopupComponent
    ],
    entryComponents: [
        SalesUnitRequirementPersonComponent,
        SalesUnitRequirementForProspectEditComponent,
        SalesUnitRequirementForProspectByApprovalComponent,
        SalesUnitRequirementForProspectComponent,
        SalesUnitRequirementDetailContainerComponent,
        SalesUnitRequirementStatusUnitDocumentMessageComponent,
        SalesUnitRequirementByStatusIndentComponent,
        SalesUnitRequirementByStatusAndApprovalComponent,
        SalesUnitRequirementStatusUnitDocumentMessageComponent,
        SalesUnitRequirementCheckAvailabilityComponent,
        SalesUnitRequirementByStatusApprovalAndSalesComponent,
        SalesUnitRequirementComponent,
        SalesUnitRequirementDialogComponent,
        SalesUnitRequirementPopupComponent,
        SalesUnitRequirementAsListComponent,
        SalesUnitRequirementAsLovComponent,
        SalesUnitRequirementLovPopupComponent,
        SalesUnitRequirementEditComponent,
        SalesUnitRequirementDealDetailViewComponent,
        SalesUnitRequirementPaymentViewComponent,
        SalesUnitRequirementCustomerViewComponent,
        SalesUnitRequirementInformationViewComponent,
        SalesUnitRequirementSTNKViewComponent,
        SalesUnitRequirementMessageViewComponent,
        SalesUnitRequirementDetailComponent,
        SalesUnitRequirementFollowUpViewComponent,
        SalesUnitRequirementDocumentViewComponent,
        SURReceiptDialogComponent,
        SURReceiptAsListComponent
    ],
    providers: [
        CurrencyPipe,
        SalesUnitRequirementService,
        SalesUnitRequirementPopupService,
        SalesUnitRequirementResolvePagingParams,
        ReceiptPopupService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmSalesUnitRequirementModule {}
