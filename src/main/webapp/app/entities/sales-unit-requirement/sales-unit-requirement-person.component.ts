import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { Person, PersonService } from '../person';
import { PersonalCustomerService, PersonalCustomer } from '../personal-customer';
import { SalesUnitRequirement } from '.';
import { LoadingService } from '../../layouts';
import { CommonUtilService, ToasterService } from '../../shared';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-sales-unit-requirement-person',
    templateUrl: './sales-unit-requirement-person.component.html'
})

export class SalesUnitRequirementPersonComponent implements OnChanges {
    @Input()
    public salesUnitRequirement: SalesUnitRequirement;

    public ktpNumber: string;
    public isSameWithCustomer: boolean;
    public disable: boolean;

    constructor(
        protected toasterService: ToasterService,
        protected commonUtilService: CommonUtilService,
        protected loadingService: LoadingService,
        protected personService: PersonService,
        protected personalCustomerService: PersonalCustomerService,
        protected confirmationService: ConfirmationService
    ) {
        this.isSameWithCustomer = false;
        this.disable =  false;
    }

    ngOnChanges(change: SimpleChanges) {
        if (change['salesUnitRequirement']) {
            this.load();
        }
    }

    protected load(): void {
        // if (this.idReTypeReady && this.surReady) {
            const custId: string = this.salesUnitRequirement.customerId;
            const personOwner: string = this.salesUnitRequirement.personOwner.idParty;
            if (custId !== null && custId !== undefined) {
                    this.personalCustomerService.find(custId).subscribe(
                        (res: PersonalCustomer) => {
                            if (res.partyId === personOwner) {
                                this.disable = true;
                                this.isSameWithCustomer = true;
                                console.log('disable :', this.disable)
                            }
                        },
                        (err) => {
                            this.commonUtilService.showError(err);
                        }
                    )
                } else if (custId !== null && custId !== undefined) {
                    this.personalCustomerService.find(custId).subscribe(
                        (res: PersonalCustomer) => {
                            if (res.partyId !== personOwner) {
                                this.disable = false;
                                this.isSameWithCustomer = false;
                                console.log('disable :', this.disable)
                            }
                        },
                        (err) => {
                            this.commonUtilService.showError(err);
                        }
                    )
            }
        // }
    }

    public onChangeSameWithCustomer(e): void {
        const checked: boolean = e.checked;
        if (checked) {
            this.disable = true;
             this.personalCustomerService.find(this.salesUnitRequirement.customerId).subscribe(
                 (res) => {
                     let newPerson: Person;
                     newPerson = res.person;

                     if (newPerson.dob != null) {
                         newPerson.dob = new Date(newPerson.dob);
                     }

                     this.salesUnitRequirement.personOwner = newPerson;
                 }
             )
        } else {
             this.salesUnitRequirement.personOwner = new Person();
             this.disable = false;
        }
     }

     public disableSearchButton(): boolean {
         if (this.ktpNumber === '' || this.ktpNumber === undefined) {
             return true;
         } else {
             return false;
         }
     }

     public createNewPerson(): void {
        this.confirmationService.confirm({
            message: 'Are you sure do you want to change STNK data ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                try {
                    this.salesUnitRequirement.personOwner = new Person();
                } finally {
                    this.loadingService.loadingStop();
                }
            }
        })
     }

     public findPersonByKTP(): void {
        let msg: string = null;
        this.loadingService.loadingStart();
        try {
            const obj: object = {
                personalIdNumber : this.ktpNumber
            }
            this.personService.findByPersonalIdNumber(obj).subscribe(
                (person: Person) => {
                    if (person.idParty === undefined) {
                        msg = 'Data yang anda cari tidak ditemukan';
                    } else {
                        this.salesUnitRequirement.personOwner = person;
                    }
                },
                (err) => {
                    this.commonUtilService.showError(err);
                }
            )
        } finally {
            this.loadingService.loadingStop();
            if (msg) {
                this.toasterService.showToaster('warn', 'Info', msg);
            }
        }
     }
}
