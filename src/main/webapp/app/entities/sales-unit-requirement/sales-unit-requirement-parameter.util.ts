import { URLSearchParams, BaseRequestOptions } from '@angular/http';

export const createSalesUnitRequirementParameterOption = (req?: any): BaseRequestOptions => {
    const options: BaseRequestOptions = new BaseRequestOptions();
    if (req) {
        const params: URLSearchParams = new URLSearchParams();
        params.set('page', req.page);
        params.set('size', req.size);
        if (req.sort) {
            params.paramsMap.set('sort', req.sort);
        }
        params.set('query', req.query);

        params.set('nama', req.salesUnitRequirementPTO.nama);
        params.set('internalId', req.salesUnitRequirementPTO.internalId);
        options.params = params;
    }
    return options;
}
