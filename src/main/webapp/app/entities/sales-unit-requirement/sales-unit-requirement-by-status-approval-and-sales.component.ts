import { Component, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { LazyLoadEvent } from 'primeng/primeng';
import { Subscription, Observable } from 'rxjs/Rx';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../shared';

import { SalesUnitRequirement } from './sales-unit-requirement.model';
import { SalesUnitRequirementsParameters } from './sales-unit-requirement-parameter.model';
import { SalesUnitRequirementService } from './sales-unit-requirement.service';

import { ConfirmationService } from 'primeng/primeng';
import { ValidationUtilsService } from '../../shared/utils/validation.service';

import {LoadingService} from '../../layouts/loading/loading.service';
import { SaleTypeService } from '../sale-type';
import {ToasterService} from '../../shared/alert/toaster.service';

import { Person } from '../person';
import { PersonalCustomerService } from '../personal-customer/personal-customer.service';

import { Mokita } from '../mokita/mokita.model';
import { MokitaService } from '../mokita/mokita.service';

import * as SalesUnitRequirementConstant from '../../shared/constants/sales-unit-requirement.constants';
import * as SaleTypeConstant from '../../shared/constants/sale-type.constants';
import * as BaseConstants from '../../shared/constants/base.constants';
import { PersonalCustomer } from '../shared-component';

@Component({
    selector: 'jhi-sales-unit-requirement-by-status-approval-and-sales',
    templateUrl: './sales-unit-requirement-by-status-approval-and-sales.component.html'
})

export class SalesUnitRequirementByStatusApprovalAndSalesComponent implements OnChanges {
    @Input()
    status: number;

    @Output()
    fnCallback = new EventEmitter();

    @Input()
    isKorsal: Boolean = false;

    @Output()
    fnEdit = new EventEmitter();

    salesUnitRequirements: SalesUnitRequirement[];
    salesUnitRequirementsParameters: SalesUnitRequirementsParameters;
    totalItems: any;
    queryCount: number;
    links: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    currentSearch: string;
    routeData: any;
    baseConstants: any;
    mokita: Mokita;
    public loading: boolean;
    public infoListNotComplete: string;
    public displayCompleteData: boolean;

    filtered: any;

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected alertService: JhiAlertService,
        protected router: Router,
        protected parseLinks: JhiParseLinks,
        protected eventManager: JhiEventManager,
        protected confirmationService: ConfirmationService,
        protected validationUtilsService: ValidationUtilsService,
        protected loadingService: LoadingService,
        protected saleTypeService: SaleTypeService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        protected toasterService: ToasterService,
        protected personalCustomerService: PersonalCustomerService,
        protected mokitaService: MokitaService,
        protected principal: Principal,
    ) {
        this.mokita = new Mokita();
        this.loading = true;
        this.displayCompleteData = false;
        this.baseConstants = BaseConstants;
        this.queryCount = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['status']) {
            if (changes['status'] && changes['isKorsal']) {
                this.loadAllByKorsal();
                console.log('ini adalah menu korsal bawah');
            } else {
                this.loadAll();
                console.log('ini adalah menu korsal atas');
            }
        }
    }

    public showNotCompleteData(str: string): void {
        this.infoListNotComplete = str;
        this.displayCompleteData = true;
    }

    public notCompleteData(sur: SalesUnitRequirement): string {
        return this.salesUnitRequirementService.notCompleteData(sur);
    }

    public refine(): void {
        this.loadingService.loadingStart();
        this.salesUnitRequirements = new Array();
        if (this.isKorsal === false) {
            this.loadAll().then(
                (resolve) => {
                    this.loadingService.loadingStop();
                }
            );
        } else {
            this.loadAllByKorsal().then(
                (resolve) => {
                    this.loadingService.loadingStop();
                }
            );
        }
    }

    public loadAll(): Promise<void> {
        return new Promise<void>(
            (resolve) => {
                this.loading = true;
                    this.salesUnitRequirementService.findByApprovalDiskonPelanggaranWilayahBySales({
                    idstatustype : this.status,
                    idinternal: this.principalService.getIdInternal(),
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => {
                        const headers = res.headers;
                        const data = res.json;

                        this.links = this.parseLinks.parse(headers.get('link'));
                        this.totalItems = headers.get('X-Total-Count');
                        this.queryCount = this.totalItems;
                        // this.page = pagingParams.page;

                        this.mappingSurWithCustomer(data).then(
                            (rs) => {
                                this.salesUnitRequirements = rs;
                                this.loading = false;
                                resolve();
                            }
                        )
                    },
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        )
    }

    public loadAllByKorsal(): Promise<void> {
        return new Promise<void>(
            (resolve) => {
                this.salesUnitRequirementService.findByApprovalDiskonPelanggaranWilayahByKorsal({
                    idstatustype : this.status,
                    idinternal: this.principalService.getIdInternal(),
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    // sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {
                        const headers = res.headers;
                        const data = res.json;

                        this.links = this.parseLinks.parse(headers.get('link'));
                        this.totalItems = headers.get('X-Total-Count');
                        this.queryCount = this.totalItems;

                        this.mappingSurWithCustomer(data).then(
                            (rs) => {
                                this.salesUnitRequirements = rs;
                                resolve();
                            }
                        );
                    },
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        )
    }

    filter() {
        this.loadingService.loadingStart();

        this.salesUnitRequirementsParameters = new SalesUnitRequirementsParameters();
        this.salesUnitRequirementsParameters.internalId = this.principal.getIdInternal();
        this.salesUnitRequirementsParameters.nama = this.filtered;

        this.salesUnitRequirementService.queryByName({
            salesUnitRequirementPTO: this.salesUnitRequirementsParameters,
            sort: this.sort()
        })
            .toPromise()
            .then((res) => {
                console.log('filter data isi nama == ', res)
                this.loadingService.loadingStop(),
                this.onSuccessFilter(res.json, res.headers)
            }
        );

        console.log('nama ', this.salesUnitRequirementsParameters.nama);
        console.log('internal ', this.salesUnitRequirementsParameters.internalId);
    }

    protected onSuccessFilter(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.salesUnitRequirements = data;
        // if (data.length === 0) {
        //     this.unitDeliverables = [...this.unitDeliverables, this.unitDeliverable];
        // }
        this.mappingSurWithCustomer(data).then(
            (rs) => {
                this.salesUnitRequirements = rs;
            }
        );
    }

    protected mappingEachSurWithCustomer(sur: SalesUnitRequirement): Promise<SalesUnitRequirement> {
        return new Promise<SalesUnitRequirement>(
            (resolve, reject) => {
                if (sur.idReqTyp === null || sur.idReqTyp === BaseConstants.REQUIREMENT_TYPE.REQUIREMENT_TYPE_SUR_RETAIL) {
                    this.personalCustomerService.find(sur.customerId).subscribe(
                        (res: PersonalCustomer) => {
                            if (res !== null) {
                                sur.personCustomer = res.person;
                            }

                            resolve(sur);
                        },
                        (err) => {
                            reject(err);
                        }
                    )
                }
            }
        );
    }

    protected mappingSurWithCustomer(data: Array<SalesUnitRequirement>): any {
        let promises: Array<Promise<SalesUnitRequirement>>;
        promises = new Array<Promise<SalesUnitRequirement>>();

        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const each: SalesUnitRequirement = data[i];
                promises.push(this.mappingEachSurWithCustomer(each));
            }
        }

        return Promise.all(promises);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }

        if (this.status !== undefined) {
            if (this.isKorsal === false) {
                this.loadAll();
            } else {
                this.loadAllByKorsal();
            }
        }
    }

    transition() {
        this.router.navigate(['/sales-unit-requirement'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        if (this.isKorsal === false) {
            this.loadAll();
        } else {
            this.loadAllByKorsal();
        }
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/sales-unit-requirement', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        if (this.isKorsal === false) {
            this.loadAll();
        } else {
            this.loadAllByKorsal();
        }
        // this.loadAll();
    }

    search() {
        console.log('coba klik');
    }

    // search(query) {
    //     // if (!query) {
    //     //     return this.clear();
    //     // }
    //     // this.page = 0;
    //     // this.currentSearch = query;
    //     // this.router.navigate(['/sales-unit-requirement', {
    //     //     search: this.currentSearch,
    //     //     page: this.page,
    //     //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
    //     // }]);
    //     // this.loadAll();
    // }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('requirementNumber');
            result.push('idRequirement');
        }
        return result;
    }

    public titleCustomer(person?: Person): string {
        if (person !== null && person !== undefined) {
            if (person.gender === 'P') {
                return 'Bpk ';
            } else if (person.gender === 'W') {
                return 'Ibu ';
            } else {
                return '';
            }
        } else {
            return 'Bpk / Ibu';
        }
    }

    public doCancel(salesUnitRequirement: SalesUnitRequirement): void {
        this.mokita.idreq = salesUnitRequirement.idRequirement;
        this.mokita.idorderstatus = 13;
        this.confirmationService.confirm({
            message: 'Do you want to cancel this VSO Draft?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                // this.salesUnitRequirementService.changeStatus(salesUnitRequirement, BaseConstants.Status.STATUS_CANCEL).subscribe(
                //     (res) => {
                //         this.refine();
                //     }
                // )
                this.salesUnitRequirementService.executeProcess(109, null, salesUnitRequirement)
                .subscribe(
                    (res) => {
                        this.mokitaService.spk(this.mokita).subscribe((a) => {});
                        this.refine();
                    }
                )
            }
        });
    }

    public submitVSO(sur: SalesUnitRequirement): void {
        this.confirmationService.confirm({
            header : 'Confirmation',
            message : 'Do you want to submit this VSO Draft?',
            accept: () => {
                this.loadingService.loadingStart();
                this.checkRequirementNumber(sur).then(
                    (resolve) => {
                        this.loadingService.loadingStop();
                        console.log('sur', sur);
                        if (this.goToCheckAvailability(sur)) {
                            console.log('masuk check');
                            this.router.navigate(['sales-unit-requirement/' + sur.idRequirement + '/check-availability']);
                        } else {
                            this.salesUnitRequirementService.executeProcess(SalesUnitRequirementConstant.SUBMIT_VSO, null, sur).subscribe(
                                (res) => {
                                    this.eventManager.broadcast({
                                        name: 'Submit VSO',
                                        content: 'VSO Updated'
                                    });

                                    this.fnCallback.emit();
                                },
                                (err) => {
                                    this.onError(err);
                                }
                            );
                        }
                    },
                    (reject) => {
                        this.loadingService.loadingStop();
                        this.commonUtilService.showError(reject);
                    }
                )
            }
        })
    }

    protected checkRequirementNumber(salesUnitRequirement: SalesUnitRequirement): Promise<void> {
        return new Promise<void> (
            (resolve, reject) => {
                if (salesUnitRequirement.approvalDiscount === BaseConstants.Status.STATUS_APPROVED) {
                    this.salesUnitRequirementService.executeProcess( SalesUnitRequirementConstant.FIND_OR_CREATE_VSO_NUMBER, null, salesUnitRequirement).subscribe(
                        (res) => {
                            resolve();
                        },
                        (err) => {
                            reject(err);
                        }
                    )
                } else {
                    resolve();
                }
            }
        )
    }

    public labelApproval(sur: SalesUnitRequirement): string {
        return this.salesUnitRequirementService.generateLabelApproval(sur);
    }

    public labelApprovalLeasing(sur: SalesUnitRequirement): string {
        return this.salesUnitRequirementService.generateLabelApprovalLeasing(sur);
    }

    protected checkApprovalSubsidiStatus(sur: SalesUnitRequirement): boolean {
        if (sur.approvalDiscount === BaseConstants.Status.STATUS_WAITING_FOR_APPROVAL) {
            return true;
        } else if (sur.approvalDiscount === BaseConstants.Status.STATUS_REJECTED) {
            return true;
        } else if (sur.approvalDiscount === BaseConstants.Status.STATUS_APPROVED) {
            return true;
        } else {
            return false;
        }
    }

    protected isNeedApproval(sur: SalesUnitRequirement): boolean {
        let isDisable: boolean;
        isDisable = false;

        if (sur.approvalDiscount === BaseConstants.Status.STATUS_WAITING_FOR_APPROVAL || sur.approvalTerritorial === BaseConstants.Status.STATUS_WAITING_FOR_APPROVAL) {
            isDisable = true;
        }

        return isDisable;
    }

    public isDisableDealDetailBtn(sur: SalesUnitRequirement, notCompleteDataStr: string): boolean {
        let isDisable: boolean;
        isDisable = false;

        const needApproval = this.isNeedApproval(sur);
        if (notCompleteDataStr !== '' || needApproval) {
            isDisable = true;
        }

        return isDisable;
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        // this.alertService.error(error.message, null, null);
        this.commonUtilService.showError(error);
    }

    protected goToCheckAvailability(sur: SalesUnitRequirement): boolean {
        let a: boolean;
        a = false;

        if (sur.approvalDiscount === BaseConstants.Status.STATUS_APPROVED && sur.approvalTerritorial === BaseConstants.Status.STATUS_APPROVED  && sur.approvalLeasing === BaseConstants.Status.STATUS_NOT_REQUIRED) {
            if (this.saleTypeService.checkIfCash(sur.saleTypeId) || this.saleTypeService.checkIfCredit(sur.saleTypeId) || this.saleTypeService.checkIfOffTheRoad(sur.saleTypeId)) {
                a = true;
            }
        }

        return a;
    }

    trackId(index: number, item: SalesUnitRequirement) {
        return item.idRequirement;
    }

    // buildReindex() {
    //     this.salesUnitRequirementService.process({command: 'buildIndex'}).subscribe((r) => {
    //         this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
    //     });
    // }

    public edit(sur: SalesUnitRequirement): void {
        this.fnEdit.emit(sur);
    }
}
