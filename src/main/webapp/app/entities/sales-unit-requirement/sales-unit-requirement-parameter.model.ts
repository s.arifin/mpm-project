export class SalesUnitRequirementsParameters {
    constructor(
        public nama?: String,
        public internalId?: String
    ) {
        this.nama = null;
        this.internalId = null;
    }
}
