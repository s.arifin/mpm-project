import { Component, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { LazyLoadEvent } from 'primeng/primeng';
import { Subscription, Observable } from 'rxjs/Rx';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../shared';

import { SalesUnitRequirement } from './sales-unit-requirement.model';
import { SalesUnitRequirementService } from './sales-unit-requirement.service';

import { ConfirmationService } from 'primeng/primeng';
import { ValidationUtilsService } from '../../shared/utils/validation.service';

import * as SalesUnitRequirementConstant from '../../shared/constants/sales-unit-requirement.constants';
import * as SaleTypeConstant from '../../shared/constants/sale-type.constants';
import * as BaseConstants from '../../shared/constants/base.constants';

import {LoadingService} from '../../layouts/loading/loading.service';
import { SaleTypeService } from '../sale-type';

@Component({
    selector: 'jhi-sales-unit-requirement-by-status-and-approval',
    templateUrl: './sales-unit-requirement-by-status-and-approval.component.html'
})

export class SalesUnitRequirementByStatusAndApprovalComponent implements OnChanges {
    @Input()
    public status: number;

    @Input()
    public statusApproval: number;

    @Input()
    public btnEdit: boolean;

    @Input()
    public btnSubmit: boolean;

    @Input()
    public keterangan: string;

    public salesUnitRequirements: Array<SalesUnitRequirement>;
    totalItems: any;
    queryCount: number;
    links: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    currentSearch: string;
    routeData: any;
    baseConstants: any;
    infoListNotComplete: string;
    displayCompleteData: boolean;

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected alertService: JhiAlertService,
        protected router: Router,
        protected parseLinks: JhiParseLinks,
        protected eventManager: JhiEventManager,
        protected confirmationService: ConfirmationService,
        protected validationUtilsService: ValidationUtilsService,
        protected loadingService: LoadingService,
        protected saleTypeService: SaleTypeService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal
    ) {
        this.displayCompleteData = false;
        this.baseConstants = BaseConstants;
        this.queryCount = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['status'] && changes['statusApproval']) {
            if (status !== undefined && this.statusApproval !== undefined) {
                this.loadAll();
            }
        }
    }

    public loadAll(): Promise<void> {
        return new Promise<void>(
            (resolve, reject) => {
                this.loadingService.loadingStart();

                const obj: Object = {
                    idinternal: this.principalService.getIdInternal(),
                    idstatusapproval: this.statusApproval,
                    idstatustype: this.status,
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }

                this.salesUnitRequirementService.getDataByStatusAndStatusApproval(obj).subscribe(
                    (res: ResponseWrapper) => {
                        this.salesUnitRequirements = res.json;
                        const headers = res.headers;
                        const data = res.json;

                        this.links = this.parseLinks.parse(headers.get('link'));
                        this.totalItems = headers.get('X-Total-Count');

                        this.queryCount = this.totalItems;
                        this.loadingService.loadingStop();
                        resolve();
                    },
                    (err) => {
                        reject(err);
                    }
                )
            }
        );
    }

    public refine(): void {
        this.loadingService.loadingStart();
        this.salesUnitRequirements = new Array();
        this.loadAll().then(
            (resolve) => {
                this.loadingService.loadingStop();
            },
            (err) => {
                this.loadingService.loadingStop();
                this.commonUtilService.showError(err);
            }
        );
    }

    public loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }

        if (this.status !== undefined && this.statusApproval !== undefined) {
            this.loadAll();
        }
    }

    public sort(): Array<string> {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    public trackId(index: number, item: SalesUnitRequirement): string {
        return item.idRequirement;
    }

    public isDisableBtnSubmit(): boolean {
        return this.btnSubmit;
    }

    public isDisableBtnEdit(): boolean {
        return this.btnEdit;
    }

    public backOrder(sur: SalesUnitRequirement): void {
        this.confirmationService.confirm({
            message: 'Are you sure do you want to back order?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.salesUnitRequirementService.executeProcess(SalesUnitRequirementConstant.PROSES_BACK_ORDER_FROM_INDENT, null, sur).subscribe(
                    (res2) => {
                        this.loadingService.loadingStop();
                        this.loadAll();
                    },
                    (err2) => {
                        this.loadingService.loadingStop();
                        this.commonUtilService.showError(err2);
                    }
                )
            },
        });
    }
};
