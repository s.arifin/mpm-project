import { Requirement } from './../requirement';

import { Person } from '../person';
import { OrganizationCustomer } from '../shared-component';
import { Customer } from '../customer';
import { Organization } from '../organization';

export class SalesUnitRequirement extends Requirement {
    constructor(
        public idRequirement?: any,
        public tenor?: number,
        public downPayment?: number,
        public customerId?: any,
        public customerName?: string,
        public organizationCustomer?: OrganizationCustomer,
        public personCustomer?: Person,
        public partyId?: any,
        public partyName?: any,
        public salesmanId?: any,
        public salesmanName?: string,
        public internalId?: any,
        public internalName?: string,
        public saleTypeId?: any,
        public saleTypeDescription?: string,
        public billToId?: any,
        public billToName?: string,
        public shipToId?: any,
        public brokerId?: any,
        public brokerName?: string,
        public colorId?: any,
        public colorDescription?: string,
        public productId?: any,
        public productName?: string,
        public personOwner?: Person,
        public note?: string,
        public idmachine?: string,
        public idframe?: string,
        public requestIdMachine?: string,
        public requestIdFrame?: string,
        public subsfincomp?: number,
        public subsmd?: number,
        public subsown?: number,
        public subsahm?: number,
        public brokerFee?: number,
        public unitPrice?: number,
        public notePartner?: string,
        public noteShipment?: string,
        public leasingCompanyId?: string,
        public leasingTenorProvideId?: string,
        public hetprice?: number,
        public bbnprice?: number,
        public idProspect?: string,
        public requestpoliceid?: string,
        public approvalDiscount?: number,
        public approvalTerritorial?: number,
        public approvalLeasing?: number,
        public prospectSourceId?: number,
        public productYear?: number,
        public indent?: boolean,
        public requestIdMachineAndFrame?: string,
        public minPayment?: number,
        public idFeature?: number,
        public unitHotItem?: boolean,
        public unitIndent?: boolean,
        public waitStnk?: boolean,
        public creditDownPayment?: number,
        public creditTenor?: number,
        public creditInstallment?: number,
        public podate?: any,
        public poNumber?: string,
        public idRequest?: string,
        public organizationOwner?: Organization,
        public komisiSales?: any,
        public dateCreate?: any,
        public leasingName?: string,
        public korsalName?: string,
        public leasingnote?: string,
        public lastModifiedBy?: string,
        public resetKey?: string,
        public ipAddress?: string,
        public idGudang?: any,
    ) {
        super();
        this.organizationCustomer = new Person();
        this.organizationOwner = new Organization();
        this.personCustomer = new Person();
        this.personOwner = new Person();
    }
}

export class Subsidi {
    constructor(
        public subsidiAHM?: number,
        public subsidiMD?: number
    ) {

    }
}
