import { Component, Input, OnChanges, SimpleChanges, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { CurrencyPipe, DatePipe } from '@angular/common';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SalesUnitRequirement, Subsidi } from '../sales-unit-requirement.model';
import { SalesUnitRequirementService } from '../sales-unit-requirement.service';
import { ToasterService, CommonUtilService, Principal} from '../../../shared';
import { Customer, CustomerService } from '../../customer';
import { Salesman, SalesmanService } from '../../salesman';
import { Internal, InternalService } from '../../internal';
import { SaleType, SaleTypeService, SaleTypeComponent } from '../../sale-type';
import { BillTo, BillToService } from '../../bill-to';
import { ShipTo, ShipToService } from '../../ship-to';
import { SalesBroker, SalesBrokerService } from '../../sales-broker';
import { Feature } from '../../feature';
import { Motor, MotorService } from '../../motor';
import { ResponseWrapper } from '../../../shared';
import { ConfirmationService } from 'primeng/primeng';
import { Person } from '../../person';
import { PriceComponentService, PriceComponent } from '../../price-component';
import { PersonalCustomerService } from '../../../entities/personal-customer/personal-customer.service';
import { LeasingTenorProvideService, LeasingTenorProvide } from '../../leasing-tenor-provide';
import { LeasingCompany, LeasingCompanyService } from '../../leasing-company';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { Receipt, ReceiptService } from '../../receipt';
import { OrganizationCustomer, OrganizationCustomerService } from '../../shared-component';
import { RequirementService, Requirement } from '../../requirement';
import { RuleIndentService } from '../../rule-indent';
import { RuleHotItemService } from '../../rule-hot-item'
import { SelectItem } from 'primeng/primeng';

import * as SalesUnitRequirementConstants from '../../../shared/constants/sales-unit-requirement.constants';
import * as reqType from '../../../shared/constants/requirement-type.constants';
import * as PriceTypeConstants from '../../../shared/constants/price-type.constants';
import * as BaseConstants from '../../../shared/constants/base.constants';
import * as ProspectSourceConstants from '../../../shared/constants/prospect-source.constants';
import * as _ from 'lodash';

@Component({
    selector: 'jhi-sales-unit-requirement-for-prospect-edit',
    templateUrl: './sales-unit-requirement-for-prospect-edit.component.html'
})

export class SalesUnitRequirementForProspectEditComponent implements OnInit, OnDestroy, OnChanges {
    @Input()
    idReq: string;

    @Output()
    fnPreviousState = new EventEmitter();

    private leasingData: Observable<any>;
    private totalSubsidi: number;
    private BBNPrice: number;
    private HETPrice: number;
    private subscriptionReceipt: Subscription;

    public selectedMotor: any;
    public organizationCustomer: OrganizationCustomer;
    public isCredit: Boolean = false;
    public isCountReceipt: Boolean = true;
    public totalReceipt: number;
    public installment: number;
    public remainingPayment: any;
    public remainingDownPayment: any;
    public isReadOnlyDocument: Boolean = false;
    public salesUnitRequirementId: string;
    public salesUnitRequirement: SalesUnitRequirement;
    public firstPrice: number;
    public fetchMotor: boolean;
    public subsidi: Subsidi;
    public isDisabledSubsidiFinCo: boolean;
    public motors: Array<Motor>;
    public motorsForSelect: Array<Object>;

    isSaving: boolean;

    leasingCompany: LeasingCompany;

    customers: Customer[];

    salesmen: Salesman[];

    internals: Internal[];

    saletypes: SaleType[];

    billtos: BillTo[];

    shiptos: ShipTo[];

    salesbrokers: SalesBroker[];

    features: Feature[];

    leasingCompanies: LeasingCompany[];

    tempLeasingTenorProvides: LeasingTenorProvide[];
    leasingTenorProvides: LeasingTenorProvide[];
    selectedLeasingTenorProvide?: LeasingTenorProvide = null;
    tenors: Array<number>;
    selectedTenor?: number = null;
    years: Array<number>;

    constructor(
        private commontUtilService: CommonUtilService,
        private alertService: JhiAlertService,
        private organizationCustomerService: OrganizationCustomerService,
        private salesUnitRequirementService: SalesUnitRequirementService,
        private customerService: CustomerService,
        private salesmanService: SalesmanService,
        private internalService: InternalService,
        private saleTypeService: SaleTypeService,
        private billToService: BillToService,
        private shipToService: ShipToService,
        private salesBrokerService: SalesBrokerService,
        private motorService: MotorService,
        private route: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toaster: ToasterService,
        private confirmationService: ConfirmationService,
        private priceComponentService: PriceComponentService,
        private personalCustomerService: PersonalCustomerService,
        private currencyPipe: CurrencyPipe,
        private datePipe: DatePipe,
        private leasingTenorProvideService: LeasingTenorProvideService,
        private leasingCompanyService: LeasingCompanyService,
        private receiptService: ReceiptService,
        private loadingService: LoadingService,
        private principalService: Principal,
        private requirementService: RequirementService,
        private ruleIndentService: RuleIndentService,
        private ruleHotItemService: RuleHotItemService
    ) {
        this.fetchMotor = true;
        this.remainingPayment = '';
        this.remainingDownPayment = '';
        this.salesUnitRequirement = new SalesUnitRequirement();
        this.totalSubsidi = 0;
        this.totalReceipt = 0;
        this.BBNPrice = 0;
        this.HETPrice = 0;
        this.installment = 0;
        this.tempLeasingTenorProvides = new Array<LeasingTenorProvide>();

        this.motorsForSelect = new Array<Object>();
        this.subsidi = new Subsidi;
        this.subsidi.subsidiAHM = 0;
        this.subsidi.subsidiMD = 0;
    }

    ngOnChanges(change: SimpleChanges) {
        if (change['idReq']) {
            this.salesUnitRequirementId = this.idReq;
            this.load(this.salesUnitRequirementId);
        }
    }

    ngOnInit() {
        this.registerReceipt();
        this.isSaving = false;
        this.leasingCompanyService.query()
            .subscribe((res: ResponseWrapper) => {this.leasingCompanies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesmanService.query()
            .subscribe((res: ResponseWrapper) => { this.salesmen = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.saleTypeService.query().subscribe((res: ResponseWrapper) => {
                this.saletypes = this.commontUtilService.getDataByParent(res.json, 1);
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipToService.query()
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.getYear();
    }

    ngOnDestroy() {
        this.subscriptionReceipt.unsubscribe();
    }

    private checkIfDataDiffInternal(sur: SalesUnitRequirement): void {
        const internalId: String = this.principalService.getIdInternal();
        if (sur.internalId !== internalId) {
            this.previousState();
        }
    }

    private registerReceipt(): void {
        this.subscriptionReceipt = this.receiptService.values.subscribe(
            (res) => {
                this.countReceipt(res);
            }
        );
    }

    public getYear(): void {
        this.years = new Array();
        this.years = this.salesUnitRequirementService.optionYearAssembly();
    }

    public countReceipt(res): void {
       this.totalReceipt = this.receiptService.countTotalReceipt(res);
       this.countRemainingPayment();
       this.setRequirementNumber();
    }

    private setRequirementNumber(): void {
        if (this.salesUnitRequirement.requirementNumber === null || this.salesUnitRequirement.requirementNumber === undefined) {
            this.requirementService.find(this.salesUnitRequirementId).subscribe(
                (res: Requirement) => {
                    this.salesUnitRequirement.requirementNumber = res.requirementNumber;
                },
                (err) => {
                    this.commontUtilService.showError(err);
                })
        }
    }

    public countTotalSubsidi(): void {
        this.totalSubsidi = this.salesUnitRequirementService.fnCountTotalSubsidi(this.salesUnitRequirement);
    }

    private convertMotorForSelect(data: Array<Motor>) {
        this.motorsForSelect = this.motorService.convertMotorForSelectPrimeNg(data);
    }

    loadMotor(): Promise<Motor[]> {
        return new Promise<Motor[]>(
            (resolve) => {
                this.motorService.query(
                    {
                        page: 0,
                        size: 10000,
                        sort: ['idProduct', 'asc']
                    }
                ).subscribe(
                    (res: ResponseWrapper) => {
                        this.fetchMotor = false;
                        this.convertMotorForSelect(res.json);
                        this.motors = res.json;
                    },
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => {
                        resolve();
                });
            }
        );
    }

    load(id): void {
        this.loadingService.loadingStart();
        this.salesUnitRequirementService.find(id).subscribe((salesUnitRequirement: SalesUnitRequirement) => {
            this.selectedMotor = salesUnitRequirement.productId;
            // this.checkIfDataDiffInternal(salesUnitRequirement);

            if (salesUnitRequirement.personOwner === null) {
                salesUnitRequirement.personOwner = new Person();
                salesUnitRequirement.personOwner.dob = null;
            }

            if (salesUnitRequirement.productId !== null) {
                salesUnitRequirement.productId = salesUnitRequirement.productId.toUpperCase();

                this.HETPrice = salesUnitRequirement.hetprice;
                this.BBNPrice = salesUnitRequirement.bbnprice;
                console.log('cari harga unit price', this.salesUnitRequirement.unitPrice);
                salesUnitRequirement.hetprice = this.HETPrice;
                salesUnitRequirement.bbnprice = this.BBNPrice;
                salesUnitRequirement.unitPrice = (this.HETPrice + this.BBNPrice);

                // this.getMotorPriceByIdProduct(salesUnitRequirement.productId);
            }

            if (salesUnitRequirement.idReqTyp === reqType.ORGANIZATION_CUSTOMER) {
                this.getDataByOrganization(salesUnitRequirement);
            } else if (salesUnitRequirement.idReqTyp === null) {
                this.getDataByPersonal(salesUnitRequirement);
            }
        });
    }

    private getDataByOrganization(salesUnitRequirement: SalesUnitRequirement): void {
        this.organizationCustomerService.find(salesUnitRequirement.customerId).subscribe(
            (res) => {
                this.organizationCustomer = res;
                salesUnitRequirement.partyId = res.partyId;
                this.salesUnitRequirement = salesUnitRequirement;
                this.loadMotor().then(
                    () => {
                        this.selectMotor();
                        this.selectSaleType();
                    }
                )
                this.loadingService.loadingStop();
                // const saleTypeId: number = this.salesUnitRequirement.saleTypeId;
                // const selLeasingTenorProvide: string = this.salesUnitRequirement.leasingTenorProvideId;
                // if (this.saleTypeService.checkIfCredit(saleTypeId) && selLeasingTenorProvide) {
                    // get detail selected leasing tenor provide
                    // this.leasingTenorProvideService.find(this.salesUnitRequirement.leasingTenorProvideId).subscribe(
                    //     (resSelectedLeasingProvide) => {
                    //         this.selectedLeasingTenorProvide = resSelectedLeasingProvide;
                            // get all leasing
                            // this.selectLeasing().then(
                            //     () => {
                            //         // set selected tenor
                            //         this.selectedTenor = this.selectedLeasingTenorProvide.tenor;
                            //         this.selectTenor(this.selectedTenor).then(
                            //             () => {
                            //                 this.selectDownPayment(this.salesUnitRequirement.leasingTenorProvideId);
                            //                 this.loadingService.loadingStop();
                            //             }
                            //         );
                            //     }
                            // );
                        // }
                    // )
                // } else {

                // }
            },
            (err) => {
                this.commontUtilService.showError(err);
                this.loadingService.loadingStop();
            }
        )
    }

    private getDataByPersonal(salesUnitRequirement: SalesUnitRequirement): void {
        this.personalCustomerService.find(salesUnitRequirement.customerId).subscribe(
            (res) => {
                salesUnitRequirement.partyId = res.partyId;
                this.salesUnitRequirement = salesUnitRequirement;
                this.loadMotor().then(
                    () => {
                        this.selectMotor();
                        this.selectSaleType();
                    }
                )
                this.loadingService.loadingStop();
                // const saleTypeId: number = this.salesUnitRequirement.saleTypeId;
                // const selLeasingTenorProvide: string = this.salesUnitRequirement.leasingTenorProvideId;
                // if (this.saleTypeService.checkIfCredit(saleTypeId) && selLeasingTenorProvide) {
                //     // get detail selected leasing tenor provide
                //     this.leasingTenorProvideService.find(this.salesUnitRequirement.leasingTenorProvideId).subscribe(
                //         (resSelectedLeasingProvide) => {
                //             this.selectedLeasingTenorProvide = resSelectedLeasingProvide;
                            // get all leasing
                            // this.selectLeasing().then(
                            //     () => {
                            //         // set selected tenor
                            //         this.selectedTenor = this.selectedLeasingTenorProvide.tenor;
                            //         this.selectTenor(this.selectedTenor).then(
                            //             () => {
                            //                 this.selectDownPayment(this.salesUnitRequirement.leasingTenorProvideId);
                            //                 this.loadingService.loadingStop();
                            //             }
                            //         );
                            //     }
                            // );
                //         }
                //     )
                // } else {

                // }
            },
            (err) => {
                this.commontUtilService.showError(err);
                this.loadingService.loadingStop();
            }
        )
    }

    filterSaleType(data: SaleType[], parentId?: number): Array<SaleType> {
        const _arr = Array();
        if (data.length > 0) {
            data.forEach(
                (e: SaleType) => {
                    if (e.parentId === parentId) {
                        _arr.push(e);
                    }
                }
            )
        }
        return _arr;
    }

    private getMotorPriceByIdProduct(_idproduct: string): void {
        const obj = {
            idproduct : _idproduct,
            idInternal : this.salesUnitRequirement.internalId
        };
        this.motorService.getPrice(obj).subscribe(
            (res) => {
                const priceComponents: PriceComponent[] = res.json;

                // binding for BBN price
                const priceComponentBBN: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.BBN]);
                if (priceComponentBBN !== undefined) {
                    this.BBNPrice = priceComponentBBN.price;
                }

                // binding for HET price
                const priceComponentHET: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.HET]);
                if (priceComponentHET !== undefined) {
                    this.HETPrice = priceComponentHET.price;
                }

              //  this.salesUnitRequirement.unitPrice = (this.salesUnitRequirement.bbnprice + this.salesUnitRequirement.hetprice)
                this.salesUnitRequirement.unitPrice = this.HETPrice;
                this.salesUnitRequirement.bbnprice = this.BBNPrice;
                this.salesUnitRequirement.unitPrice = (this.BBNPrice + this.HETPrice);
                console.log('cari harga unit price', this.salesUnitRequirement.unitPrice);
                console.log('cari harga HET', this.salesUnitRequirement.hetprice);
                console.log('cari harga BBN', this.salesUnitRequirement.bbnprice);
                console.log('cari harga Unit', this.salesUnitRequirement.unitPrice);
                // this.firstPrice = this.priceComponentService.countMotorPrice(priceComponents);
                this.firstPrice = this.salesUnitRequirement.unitPrice;
            }
        )
    }

    private setIsHotItemAndIsIndent(productId?: String): void {
        if (productId !== null) {
            const selectedProductId = this.salesUnitRequirement.productId;
            const motor: Motor = _.find(this.motors, function(e: Motor) {
                return e.idProduct === selectedProductId;
            });

            this.salesUnitRequirement.unitIndent = false;
            this.salesUnitRequirement.unitHotItem = false;

            // check indent
            this.ruleIndentService.checkByIdInternalAndIdProduct(
                {
                    idProduct: productId,
                    idInternal: this.principalService.getIdInternal()
                }
            ).subscribe(
                (res: Boolean) => {
                    if (res) {
                        this.salesUnitRequirement.unitIndent = true;
                    }
                }
            )

            // check hot item
            this.ruleHotItemService.checkByIdProductAndIdInternal(
                {
                    idproduct: productId,
                    idinternal: this.principalService.getIdInternal()
                }
            ).subscribe(
                (res: Boolean) => {
                    if (res) {
                        this.salesUnitRequirement.unitHotItem = true;
                    }
                }
            )
        }
    }

    public selectMotor(isSelect?: boolean): void {
        this.salesUnitRequirement.productId = this.selectedMotor;
        this.BBNPrice = this.salesUnitRequirement.bbnprice;
        this.HETPrice = this.salesUnitRequirement.hetprice;
        console.log('ini bbn ', this.BBNPrice);
        console.log('ini het ', this.HETPrice);
        console.log('select motor', isSelect);
        this.getColorByMotor(this.salesUnitRequirement.productId);
        this.setIsHotItemAndIsIndent(this.salesUnitRequirement.productId);
        if (isSelect) {// dijalankan ketika melakukan pemilihan motor
            this.resetLeasing();
            this.resetSaleType();
            const obj = {
                idproduct : this.salesUnitRequirement.productId,
                idinternal : this.salesUnitRequirement.internalId
            };
            this.motorService.getPrice(obj).subscribe(
                (res) => {
                    const priceComponents: PriceComponent[] = res.json;

                    // binding for subsidi AHM
                    const priceComponentSubsidiAHM: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.SUBSIDI_AHM]);
                    this.salesUnitRequirement.subsahm = 0;
                    if (priceComponentSubsidiAHM !== undefined) {
                    //     this.salesUnitRequirement.subsahm = priceComponentSubsidiAHM.price;
                        this.subsidi.subsidiAHM = priceComponentSubsidiAHM.price;
                    }

                    // binding for subsidi main dealer
                    const priceComponentSubsidiMD: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.SUBSIDI_MD]);
                    this.salesUnitRequirement.subsmd = 0;
                    if (priceComponentSubsidiMD !== undefined) {
                    //     this.salesUnitRequirement.subsmd = priceComponentSubsidiMD.price;
                        this.subsidi.subsidiMD = priceComponentSubsidiMD.price;
                    }

                    // binding for BBN price
                    const priceComponentBBN: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.BBN]);

                    this.salesUnitRequirement.bbnprice = 0;
                    if (priceComponentBBN !== undefined) {
                        this.BBNPrice = priceComponentBBN.price;
                        this.salesUnitRequirement.bbnprice = priceComponentBBN.price;
                    }

                    // binding for HET price
                    const priceComponentHET: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.HET]);
                    this.salesUnitRequirement.hetprice = 0;

                    if (priceComponentHET !== undefined) {
                        this.HETPrice = priceComponentHET.price;
                        this.salesUnitRequirement.hetprice = priceComponentHET.price;
                    }
                    // binding for unit price
                    this.salesUnitRequirement.unitPrice = this.priceComponentService.countMotorPrice(priceComponents);

                    this.firstPrice = this.priceComponentService.countMotorPrice(priceComponents);

                    this.setMinPayment();
                }, (err) => {
                   this.commontUtilService.showError(err);
                }
            );
        }
    }

    // public selectLeasing(): Promise<any> {
    //     this.tempLeasingTenorProvides = new Array<LeasingTenorProvide>();
    //     this.remainingDownPayment = this.salesUnitRequirementService.notSelectLeasingTenorProvideMessage;
    //     return new Promise<any>(
    //         (resolve) => {
    //             if (this.salesUnitRequirement.leasingCompanyId != null) {
    //                 const req = {
    //                     idproduct : this.salesUnitRequirement.productId,
    //                     idleasing : this.salesUnitRequirement.leasingCompanyId
    //                 };
    //                 this.leasingTenorProvideService.getActiveTenorByIdProductAndIdLeasing(req).subscribe(
    //                     (res: ResponseWrapper) => {
    //                         this.tempLeasingTenorProvides = res.json;
    //                         this.checkPastTenorProvide().then(
    //                             () => {
    //                                 this.getTenor(this.tempLeasingTenorProvides);
    //                                 resolve();
    //                             }
    //                         )
    //                     }
    //                 )
    //             } else {
    //                 this.salesUnitRequirement.leasingTenorProvideId = null;
    //                 this.tenors = Array();
    //                 this.installment = 0;
    //                 this.selectedTenor = null;

    //                 resolve();
    //             }
    //         }
    //     );
    // }

    public selectTenor(tenor: number): Promise<void> {
        return new Promise<void>(
            (resolve) => {
                this.selectedTenor = tenor;
                this.leasingTenorProvides = _.filter(this.tempLeasingTenorProvides, function(e) {
                    return e.tenor === tenor;
                });
                this.installment = 0;
                this.remainingDownPayment = this.salesUnitRequirementService.notSelectLeasingTenorProvideMessage;
                resolve();
            }
        )
    }

    public selectDownPayment(data: string): void {
        this.installment = 0;
        const objLeasingTenor: LeasingTenorProvide =  this.salesUnitRequirementService.fnGetSelectedLeasingTenorProvide(this.tempLeasingTenorProvides, data);

        this.installment = objLeasingTenor.installment;
        this.salesUnitRequirement.minPayment = objLeasingTenor.downPayment;
        this.countRemainingDownPayment();
    }

    public setMinPayment() {
        const saleTypeId: number = this.salesUnitRequirement.saleTypeId;
        if (this.saleTypeService.checkIfCash(saleTypeId)) {
            this.salesUnitRequirement.minPayment = this.salesUnitRequirement.unitPrice;
        } else if (this.saleTypeService.checkIfCredit(saleTypeId)) {
            this.salesUnitRequirement.minPayment = this.salesUnitRequirement.creditDownPayment;
        } else if (this.saleTypeService.checkIfOffTheRoad(saleTypeId)) {
            this.salesUnitRequirement.minPayment = this.salesUnitRequirement.hetprice;
        }
    }

    public selectSaleType(): void {
        const saleTypeId = this.salesUnitRequirement.saleTypeId;
        this.countMotorPriceBySaleType(saleTypeId);
        if (this.saleTypeService.checkIfCash(saleTypeId)) { // Cash
            this.salesUnitRequirement.subsfincomp = 0;
            this.isCredit = false;
            this.salesUnitRequirement.leasingCompanyId = null;
            this.salesUnitRequirement.leasingTenorProvideId = null;
            this.salesUnitRequirement.creditTenor = 0;
            this.salesUnitRequirement.creditInstallment = 0;
            this.salesUnitRequirement.creditDownPayment = 0;
            this.salesUnitRequirement.unitPrice = this.HETPrice + this.BBNPrice;
            this.countRemainingPayment();

            this.salesUnitRequirement.bbnprice = this.BBNPrice;
            console.log('ini pilih cash', this.salesUnitRequirement.bbnprice)
        } else if (this.saleTypeService.checkIfCredit(saleTypeId)) { // Credit Sales
            this.isCredit = true;
            this.countRemainingDownPayment();
            this.salesUnitRequirement.bbnprice = this.BBNPrice;
            this.salesUnitRequirement.unitPrice = this.HETPrice + this.BBNPrice;
            console.log('ini pilih credit', this.salesUnitRequirement.bbnprice)
        } else if (this.saleTypeService.checkIfOffTheRoad(saleTypeId)) { // Off The Road
            this.salesUnitRequirement.subsfincomp = 0;
            this.isCredit = false;
            this.salesUnitRequirement.leasingCompanyId = null;
            this.salesUnitRequirement.leasingTenorProvideId = null;
            this.salesUnitRequirement.creditTenor = 0;
            this.salesUnitRequirement.creditInstallment = 0;
            this.salesUnitRequirement.creditDownPayment = 0;
            this.salesUnitRequirement.bbnprice = 0;
            this.salesUnitRequirement.unitPrice = this.HETPrice;
            console.log('ini pilih offtheroad', this.salesUnitRequirement.bbnprice);
            this.countRemainingPayment();
        } else {
            this.isCredit = false;
            this.salesUnitRequirement.subsfincomp = 0;
        }
        this.setMinPayment();
    }

    private countMotorPriceBySaleType(saleTypeId: number): void {
        if (this.saleTypeService.checkIfOffTheRoad(saleTypeId)) {
            this.salesUnitRequirement.unitPrice = this.firstPrice;
            console.log('ini firstprice', this.firstPrice)
            console.log('ini bbn', this.BBNPrice)
            console.log('hitung pilih offtheroad', this.salesUnitRequirement.unitPrice)
        } else {
           if ( this.salesUnitRequirement.unitPrice !== (this.BBNPrice + this.HETPrice)) {
               this.salesUnitRequirement.unitPrice = this.firstPrice;
           }
        }
        this.setMinPayment();
    }

    public countSubsidiDealer(): void {
        const saleTypeId = this.salesUnitRequirement.saleTypeId;
        if (this.saleTypeService.checkIfCash(saleTypeId) || saleTypeId === this.saleTypeService.checkIfOffTheRoad(saleTypeId)) { // Cash
            this.countRemainingPayment();
        } else if (this.saleTypeService.checkIfCredit(saleTypeId)) { // Credit Sales
            this.countRemainingDownPayment();
        }
    }

    private resetLeasing(): void {
        this.salesUnitRequirement.leasingCompanyId = null;
        this.salesUnitRequirement.creditTenor = 0;
        this.salesUnitRequirement.creditDownPayment = 0;
        this.salesUnitRequirement.creditInstallment = 0;
        this.salesUnitRequirement.subsfincomp = 0;
        console.log('reset ');
        // this.selectLeasing();
        this.remainingDownPayment = this.salesUnitRequirementService.notSelectLeasingTenorProvideMessage;
    }

    private resetSaleType(): void {
        this.salesUnitRequirement.saleTypeId = null;
        this.salesUnitRequirement.brokerFee = 0;
        this.salesUnitRequirement.subsahm = 0;
        this.salesUnitRequirement.subsmd = 0;
        this.salesUnitRequirement.subsown = 0;
        console.log('reset ');
        // this.selectLeasing();
        this.remainingDownPayment = this.salesUnitRequirementService.notSelectLeasingTenorProvideMessage;
    }

    public countRemainingDownPayment(): void {
        const productPrice = this.salesUnitRequirement.unitPrice;
        this.remainingDownPayment = this.salesUnitRequirementService.countRemainingDownPaymentForCredit(this.salesUnitRequirement, this.totalReceipt);
        if (productPrice !== null) {
            this.countTotalSubsidi();
            this.salesUnitRequirement.downPayment = this.totalSubsidi + this.totalReceipt;
        }
        this.setMinPayment();
        this.countRemainingPayment();
    }

    public countRemainingPayment(): void {
        const productPrice = this.salesUnitRequirement.unitPrice;
        if (productPrice !== null) {
            this.countTotalSubsidi();
            this.remainingPayment = this.currencyPipe.transform(productPrice - this.totalSubsidi - this.totalReceipt, 'IDR', true);

            this.salesUnitRequirement.downPayment = this.totalSubsidi + this.totalReceipt;
        } else {
            this.remainingPayment = 'Please select motor';
        }
    }

    public previousState(): void {
        this.fnPreviousState.emit();
    }

    public save(): void {
        this.isSaving = true;
        if (this.salesUnitRequirement.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.salesUnitRequirementService.update(this.salesUnitRequirement));
        } else {
            this.subscribeToSaveResponse(
                this.salesUnitRequirementService.create(this.salesUnitRequirement));
        }
    }

    private getColorByMotor(idProduct: string): void {
        if (idProduct !== null) {
            this.features = new Array<Feature>();
            const selectedProduct: Motor = _.find(this.motors, function(e) {
                return e.idProduct.toLowerCase() === idProduct.toLowerCase();
            });
            this.features = selectedProduct.features;
        }
    }

    private checkPastTenorProvide(): Promise<void> {
        return new Promise<void>(
            (resolve) => {
                const tenorProvId = this.salesUnitRequirement.leasingTenorProvideId;

                let selectedLeasingTenorProvide: LeasingTenorProvide;
                selectedLeasingTenorProvide = this.salesUnitRequirementService.fnGetSelectedLeasingTenorProvide(this.tempLeasingTenorProvides, tenorProvId);

                if (selectedLeasingTenorProvide === undefined && this.selectedLeasingTenorProvide !== null) {
                    this.tempLeasingTenorProvides.push(this.selectedLeasingTenorProvide);
                }
                resolve();
            }
        )
    }

    private getTenor(data: any): void {
        if (data.length > 0) {
            const leasingTenorOrderByTenor: LeasingTenorProvide[] = _.orderBy(data, ['tenor'], ['asc']);
            this.tenors = _.map(_.uniqBy(leasingTenorOrderByTenor, 'tenor'), function(e) {
                return e.tenor;
            });
        }
    }

    private subscribeToSaveResponse(result: Observable<SalesUnitRequirement>) {
        result.subscribe((res: SalesUnitRequirement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: SalesUnitRequirement) {
        this.eventManager.broadcast({ name: 'salesUnitRequirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'salesUnitRequirement saved !');
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError(error): void {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error): void {
        this.toaster.showToaster('warning', 'salesUnitRequirement Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    public isDisabledSubsidiDealer(sur: SalesUnitRequirement): boolean {
        if (sur.approvalDiscount === BaseConstants.Status.STATUS_WAITING_FOR_APPROVAL || sur.approvalDiscount === BaseConstants.Status.STATUS_APPROVED) {
            return true;
        } else {
            return false;
        }
    }

    public isDisableSelectMotor(sur: SalesUnitRequirement): boolean {
        if (sur.approvalDiscount !== BaseConstants.Status.STATUS_NOT_REQUIRED) {
            return true;
        } else {
            return false;
        }
    }

    public disableMakelarFee(): boolean {
        let isDisable: boolean;
        isDisable = true;

        if (this.salesUnitRequirement.prospectSourceId === ProspectSourceConstants.MAKELAR) {
            isDisable = false;
        }

        return isDisable;
    }

    public trackLeasingCompanyById(index: number, item: LeasingCompany): String {
        return item.idLeasingCompany;
    }

    public trackSalesmanById(index: number, item: Salesman) {
        return item.idPartyRole;
    }

    public trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    public trackSaleTypeById(index: number, item: SaleType) {
        return item.idSaleType;
    }

    public trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }

    public trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }

    public trackSalesBrokerById(index: number, item: SalesBroker) {
        return item.idPartyRole;
    }

    public trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    public trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }

    public trackLeasingTenorProvideById(index: number, item: LeasingTenorProvide) {
        return item.idLeasingProvide;
    }
}
