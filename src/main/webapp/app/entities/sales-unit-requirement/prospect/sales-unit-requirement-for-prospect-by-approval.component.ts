import {Component, OnChanges, Input, SimpleChanges, Output, EventEmitter} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../../shared';
import { LazyLoadEvent } from 'primeng/primeng';
import { Person } from '../../person';

import * as SalesUnitRequirementConstant from '../../../shared/constants/sales-unit-requirement.constants';
import * as SaleTypeConstant from '../../../shared/constants/sale-type.constants';
import * as BaseConstants from '../../../shared/constants/base.constants';

import {LoadingService} from '../../../layouts/loading/loading.service';
import {SalesUnitRequirementService, SalesUnitRequirement} from '../';
import { PersonalCustomerService, PersonalCustomer } from '../../personal-customer';

@Component({
    selector: 'jhi-sales-unit-requirement-for-prospect-by-approval',
    templateUrl: './sales-unit-requirement-for-prospect-by-approval.component.html'
})
export class SalesUnitRequirementForProspectByApprovalComponent implements OnChanges {
    @Input()
    status: number;

    @Input()
    idProspect: string;

    @Input()
    isKorsal: Boolean = false;

    @Output()
    fnEdit = new EventEmitter();

    public editPage: boolean;
    public totalItems: any;
    public queryCount: number;
    public links: any;
    public itemsPerPage: any;
    public page: any;
    public predicate: any;
    public previousPage: any;
    public reverse: any;
    public routeData: any;
    public salesUnitRequirements: Array<SalesUnitRequirement>;
    public displayCompleteData: boolean;
    public infoListNotComplete: string;

    constructor(
        private commonUtilService: CommonUtilService,
        private activatedRoute: ActivatedRoute,
        private parseLinks: JhiParseLinks,
        private principalService: Principal,
        private loadingService: LoadingService,
        private salesUnitRequirementService: SalesUnitRequirementService,
        private personalCustomerService: PersonalCustomerService
    ) {
        this.editPage = false;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idRequirement';
        this.reverse = 'asc';
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log('ini adalah menu korsal utama = ', changes);
        if (changes['status'] && changes['idProspect']) {
            this.loadAll();
            console.log('ini adalah menu korsal atas');
        } else
        if (changes['status'] && changes['isKorsal']) {
            this.loadAllByKorsal();
            console.log('ini adalah menu korsal bawah');
        }
    }

    public loadAll(): Promise<void> {
        return new Promise<void>(
            (resolve) => {
                this.salesUnitRequirementService.findByApprovalDiskonPelanggaranWilayahByProspect({
                    idstatustype : this.status,
                    idinternal: this.principalService.getIdInternal(),
                    idprospect: this.idProspect,
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => {
                        const headers = res.headers;
                        const data = res.json;

                        this.links = this.parseLinks.parse(headers.get('link'));
                        this.totalItems = headers.get('X-Total-Count');
                        this.queryCount = this.totalItems;

                        this.mappingSurWithCustomer(data).then(
                            (rs) => {
                                this.salesUnitRequirements = rs;
                                resolve();
                            }
                        );
                    },
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        )
    }

    public loadAllByKorsal(): Promise<void> {
        return new Promise<void>(
            (resolve) => {
                this.salesUnitRequirementService.findByApprovalDiskonPelanggaranWilayahByKorsal({
                    idstatustype : this.status,
                    idinternal: this.principalService.getIdInternal(),
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => {
                        const headers = res.headers;
                        const data = res.json;

                        this.links = this.parseLinks.parse(headers.get('link'));
                        this.totalItems = headers.get('X-Total-Count');
                        this.queryCount = this.totalItems;

                        this.mappingSurWithCustomer(data).then(
                            (rs) => {
                                this.salesUnitRequirements = rs;
                                resolve();
                            }
                        );
                    },
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        )
    }

    private mappingEachSurWithCustomer(sur: SalesUnitRequirement): Promise<SalesUnitRequirement> {
        return new Promise<SalesUnitRequirement>(
            (resolve, reject) => {
                if (sur.idReqTyp === null || sur.idReqTyp === BaseConstants.REQUIREMENT_TYPE.REQUIREMENT_TYPE_SUR_RETAIL) {
                    this.personalCustomerService.find(sur.customerId).subscribe(
                        (res: PersonalCustomer) => {
                            if (res !== null) {
                                sur.personCustomer = res.person;
                            }

                            resolve(sur);
                        },
                        (err) => {
                            reject(err);
                        }
                    )
                }
            }
        );
    }

    private mappingSurWithCustomer(data: Array<SalesUnitRequirement>): any {
        let promises: Array<Promise<SalesUnitRequirement>>;
        promises = new Array<Promise<SalesUnitRequirement>>();

        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const each: SalesUnitRequirement = data[i];
                promises.push(this.mappingEachSurWithCustomer(each));
            }
        }

        return Promise.all(promises);
    }

    public titleCustomer(person: Person): string {
        if (person.gender === 'P') {
            return 'Bpk ';
        } else if (person.gender === 'W') {
            return 'Ibu ';
        } else {
            return '';
        }
    }

    public showNotCompleteData(str: string): void {
        this.infoListNotComplete = str;
        this.displayCompleteData = true;
    }

    private sort(): Array<string> {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    public notCompleteData(sur: SalesUnitRequirement): string {
        return this.salesUnitRequirementService.notCompleteData(sur);
    }

    private onError(error): void {
        this.loadingService.loadingStop();
        this.commonUtilService.showError(error);
    }

    public loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }

        if (this.status !== undefined) {
            if (this.isKorsal === false) {
                this.loadAll();
            } else {
                this.loadAllByKorsal();
            }
        }
    }

    public labelApproval(sur: SalesUnitRequirement): string {
        return this.salesUnitRequirementService.generateLabelApproval(sur);
    }

    private checkApprovalTerritory(sur: SalesUnitRequirement): boolean {
        if (sur.approvalTerritorial === BaseConstants.Status.STATUS_WAITING_FOR_APPROVAL) {
            return true;
        } else if (sur.approvalTerritorial === BaseConstants.Status.STATUS_REJECTED) {
            return true;
        } else if (sur.approvalTerritorial === BaseConstants.Status.STATUS_APPROVED) {
            return true;
        } else {
            return false;
        }
    }

    private checkApprovalSubsidiStatus(sur: SalesUnitRequirement): boolean {
        if (sur.approvalDiscount === BaseConstants.Status.STATUS_WAITING_FOR_APPROVAL) {
            return true;
        } else if (sur.approvalDiscount === BaseConstants.Status.STATUS_REJECTED) {
            return true;
        } else if (sur.approvalDiscount === BaseConstants.Status.STATUS_APPROVED) {
            return true;
        } else {
            return false;
        }
    }

    public refine(): void {
        this.loadingService.loadingStart();
        this.salesUnitRequirements = new Array();
        if (this.isKorsal === false) {
            this.loadAll().then(
                (resolve) => {
                    this.loadingService.loadingStop();
                }
            );
        } else {
            this.loadAllByKorsal().then(
                (resolve) => {
                    this.loadingService.loadingStop();
                }
            );
        }
    }

    public trackId(index: number, item: SalesUnitRequirement): string {
        return item.idRequirement;
    }

    public edit(sur: SalesUnitRequirement): void {
        this.fnEdit.emit(sur);
    }
}
