// last update 22/03/2018

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { CurrencyPipe, DatePipe } from '@angular/common';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SalesUnitRequirement } from './sales-unit-requirement.model';
import { SalesUnitRequirementService } from './sales-unit-requirement.service';
import { ToasterService, CommonUtilService, Principal} from '../../shared';
import { Customer, CustomerService } from '../customer';
import { Salesman, SalesmanService } from '../salesman';
import { Internal, InternalService } from '../internal';
import { SaleType, SaleTypeService, SaleTypeComponent } from '../sale-type';
import { BillTo, BillToService } from '../bill-to';
import { ShipTo, ShipToService } from '../ship-to';
import { SalesBroker, SalesBrokerService } from '../sales-broker';
import { Feature } from '../feature';
import { Motor, MotorService } from '../motor';
import { ResponseWrapper } from '../../shared';
import { ConfirmationService } from 'primeng/primeng';
import { Person } from '../person';
import { PriceComponentService, PriceComponent } from '../price-component';
import { PersonalCustomerService } from '../../entities/personal-customer/personal-customer.service';
import { LeasingTenorProvideService, LeasingTenorProvide } from '../leasing-tenor-provide';
import { LeasingCompany, LeasingCompanyService } from '../leasing-company';
import { LoadingService } from '../../layouts/loading/loading.service';
import { Receipt, ReceiptService } from '../receipt';
import { OrganizationCustomer, OrganizationCustomerService } from '../shared-component';
import { RuleIndentService } from '../rule-indent';
import { RuleHotItemService } from '../rule-hot-item'

import * as reqType from '../../shared/constants/requirement-type.constants';
import * as SalesUnitRequirementConstants from '../../shared/constants/sales-unit-requirement.constants';
import * as PriceTypeConstants from '../../shared/constants/price-type.constants';
import * as BaseConstants from '../../shared/constants/base.constants';
import * as ProspectSourceConstants from '../../shared/constants/prospect-source.constants';
import * as _ from 'lodash';

@Component({
    selector: 'jhi-sales-unit-requirement-edit',
    templateUrl: './sales-unit-requirement-edit.component.html'
})

export class SalesUnitRequirementEditComponent implements OnInit, OnDestroy {
    protected subscription: Subscription;
    protected leasingData: Observable<any>;
    protected totalSubsidi: number;
    protected BBNPrice: number;
    protected HETPrice: number;
    protected subscriptionReceipt: Subscription;

    public organizationCustomer: OrganizationCustomer;
    public isCredit: Boolean = false;
    public isCountReceipt: Boolean = true;
    public totalReceipt: number;
    public installment: number;
    public remainingPayment: any;
    public remainingDownPayment: any;
    public isSameWithCustomer: Boolean = false;
    public isReadOnlyDocument: Boolean = false;
    public salesUnitRequirementId: number;
    public salesUnitRequirement: SalesUnitRequirement;
    public firstPrice: number;
    public disable: boolean;

    isDisabledSubsidiFinCo: boolean;

    isSaving: boolean;

    leasingCompany: LeasingCompany;

    customers: Customer[];

    salesmen: Salesman[];

    internals: Internal[];

    saletypes: SaleType[];

    billtos: BillTo[];

    shiptos: ShipTo[];

    salesbrokers: SalesBroker[];

    features: Feature[];

    motors: Motor[];

    leasingCompanies: LeasingCompany[];

    tempLeasingTenorProvides: LeasingTenorProvide[];
    leasingTenorProvides: LeasingTenorProvide[];
    selectedLeasingTenorProvide?: LeasingTenorProvide = null;
    tenors: Array<number>;
    selectedTenor?: number = null;
    years: Array<number>;

    constructor(
        protected commontUtilService: CommonUtilService,
        protected alertService: JhiAlertService,
        protected organizationCustomerService: OrganizationCustomerService,
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected customerService: CustomerService,
        protected salesmanService: SalesmanService,
        protected internalService: InternalService,
        protected saleTypeService: SaleTypeService,
        protected billToService: BillToService,
        protected shipToService: ShipToService,
        protected salesBrokerService: SalesBrokerService,
        protected motorService: MotorService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected confirmationService: ConfirmationService,
        protected priceComponentService: PriceComponentService,
        protected personalCustomerService: PersonalCustomerService,
        protected currencyPipe: CurrencyPipe,
        protected datePipe: DatePipe,
        protected leasingTenorProvideService: LeasingTenorProvideService,
        protected leasingCompanyService: LeasingCompanyService,
        protected receiptService: ReceiptService,
        protected loadingService: LoadingService,
        protected principalService: Principal,
        protected ruleIndentService: RuleIndentService,
        protected ruleHotItemService: RuleHotItemService
    ) {
        this.remainingPayment = '';
        this.remainingDownPayment = '';
        this.salesUnitRequirement = new SalesUnitRequirement();
        this.totalSubsidi = 0;
        this.totalReceipt = 0;
        this.BBNPrice = 0;
        this.HETPrice = 0;
        this.installment = 0;
        this.tempLeasingTenorProvides = new Array<LeasingTenorProvide>();
        this.disable = false;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.salesUnitRequirementId = params['id'];
                this.load(params['id']);
            } else {
                this.loadMotor();
            }
        });

        this.registerReceipt();
        this.isSaving = false;
        this.leasingCompanyService.query()
            .subscribe((res: ResponseWrapper) => {this.leasingCompanies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesmanService.query()
            .subscribe((res: ResponseWrapper) => { this.salesmen = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.saleTypeService.query().subscribe((res: ResponseWrapper) => {
                this.saletypes = this.commontUtilService.getDataByParent(res.json, 1);
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipToService.query()
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.getYear();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.subscriptionReceipt.unsubscribe();
    }

    protected checkIfDataDiffInternal(sur: SalesUnitRequirement): void {
        const internalId: String = this.principalService.getIdInternal();
        if (sur.internalId !== internalId) {
            this.previousState();
        }
    }

    protected registerReceipt(): void {
        this.subscriptionReceipt = this.receiptService.values.subscribe(
            (res) => {
                this.countReceipt(res);
            }
        );
    }

    public getYear(): void {
        this.years = new Array();
        this.years = this.salesUnitRequirementService.optionYearAssembly();
    }

    public countReceipt(res): void {
       this.totalReceipt = this.receiptService.countTotalReceipt(res);
       this.countRemainingPayment();
       this.setRequirementNumber();
    }

    protected setRequirementNumber(): void {
        if (this.salesUnitRequirement.requirementNumber === null || this.salesUnitRequirement.requirementNumber === undefined) {
            this.salesUnitRequirementService.find(this.salesUnitRequirementId).subscribe(
                (res: SalesUnitRequirement) => {
                    this.salesUnitRequirement.requirementNumber = res.requirementNumber;
                },
                (err) => {
                    this.commontUtilService.showError(err);
                })
        }
    }

    public countTotalSubsidi(): void {
        this.totalSubsidi = this.salesUnitRequirementService.fnCountTotalSubsidi(this.salesUnitRequirement);
    }

    loadMotor(): Promise<Motor[]> {
        return new Promise<Motor[]>(
            (resolve) => {
                this.motorService.query().subscribe(
                    (res: ResponseWrapper) => {
                        console.log('motor', res.json);
                        this.motors = res.json;
                    },
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => {
                        resolve();
                });
            }
        );
    }

    load(id): void {
        this.loadingService.loadingStart();
        try {
            this.salesUnitRequirementService.find(id).subscribe((salesUnitRequirement: SalesUnitRequirement) => {
                this.checkIfDataDiffInternal(salesUnitRequirement);

                if (salesUnitRequirement.personOwner === null) {
                    salesUnitRequirement.personOwner = new Person();
                    salesUnitRequirement.personOwner.dob = null;
                }

                if (salesUnitRequirement.productId !== null) {
                    salesUnitRequirement.productId = salesUnitRequirement.productId.toUpperCase();
                    this.getMotorPriceByIdProduct(salesUnitRequirement.productId);
                }

                if (salesUnitRequirement.idReqTyp === reqType.ORGANIZATION_CUSTOMER) {
                    this.getDataByOrganization(salesUnitRequirement);
                } else if (salesUnitRequirement.idReqTyp === null) {
                    this.getDataByPersonal(salesUnitRequirement);
                }
            });
        } finally {
            this.loadingService.loadingStop();
        }
    }

    protected getDataByOrganization(salesUnitRequirement: SalesUnitRequirement): void {
        this.organizationCustomerService.find(salesUnitRequirement.customerId).subscribe(
            (res) => {
                this.organizationCustomer = res;
                salesUnitRequirement.partyId = res.partyId;
                this.salesUnitRequirement = salesUnitRequirement;
                this.loadMotor().then(
                    () => {
                        this.selectMotor();
                        this.selectSaleType();
                    }
                )
                this.loadingService.loadingStop();
                // const saleTypeId: number = this.salesUnitRequirement.saleTypeId;
                // const selLeasingTenorProvide: string = this.salesUnitRequirement.leasingTenorProvideId;
                // if (this.saleTypeService.checkIfCredit(saleTypeId) && selLeasingTenorProvide) {
                    // get detail selected leasing tenor provide
                    // this.leasingTenorProvideService.find(this.salesUnitRequirement.leasingTenorProvideId).subscribe(
                    //     (resSelectedLeasingProvide) => {
                    //         this.selectedLeasingTenorProvide = resSelectedLeasingProvide;
                            // get all leasing
                            // this.selectLeasing().then(
                            //     () => {
                            //         // set selected tenor
                            //         this.selectedTenor = this.selectedLeasingTenorProvide.tenor;
                            //         this.selectTenor(this.selectedTenor).then(
                            //             () => {
                            //                 this.selectDownPayment(this.salesUnitRequirement.leasingTenorProvideId);
                            //                 this.loadingService.loadingStop();
                            //             }
                            //         );
                            //     }
                            // );
                        // }
                    // )
                // } else {

                // }
            },
            (err) => {
                this.commontUtilService.showError(err);
                this.loadingService.loadingStop();
            }
        )
    }

    protected getDataByPersonal(salesUnitRequirement: SalesUnitRequirement): void {
        this.personalCustomerService.find(salesUnitRequirement.customerId).subscribe(
            (res) => {
                salesUnitRequirement.partyId = res.partyId;
                this.salesUnitRequirement = salesUnitRequirement;
                this.loadMotor().then(
                    () => {
                        this.selectMotor();
                        this.selectSaleType();
                    }
                )
                this.loadingService.loadingStop();
                // const saleTypeId: number = this.salesUnitRequirement.saleTypeId;
                // const selLeasingTenorProvide: string = this.salesUnitRequirement.leasingTenorProvideId;
                // if (this.saleTypeService.checkIfCredit(saleTypeId) && selLeasingTenorProvide) {
                //     // get detail selected leasing tenor provide
                //     this.leasingTenorProvideService.find(this.salesUnitRequirement.leasingTenorProvideId).subscribe(
                //         (resSelectedLeasingProvide) => {
                //             this.selectedLeasingTenorProvide = resSelectedLeasingProvide;
                            // get all leasing
                            // this.selectLeasing().then(
                            //     () => {
                            //         // set selected tenor
                            //         this.selectedTenor = this.selectedLeasingTenorProvide.tenor;
                            //         this.selectTenor(this.selectedTenor).then(
                            //             () => {
                            //                 this.selectDownPayment(this.salesUnitRequirement.leasingTenorProvideId);
                            //                 this.loadingService.loadingStop();
                            //             }
                            //         );
                            //     }
                            // );
                //         }
                //     )
                // } else {

                // }
            },
            (err) => {
                this.commontUtilService.showError(err);
                this.loadingService.loadingStop();
            }
        )
    }

    filterSaleType(data: SaleType[], parentId?: number): Array<SaleType> {
        const _arr = Array();
        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const obj = data[i];
                if (obj.parentId === parentId) {
                    _arr.push(obj);
                }
            }
        }
        return _arr;
    }

    protected getMotorPriceByIdProduct(_idproduct: string): void {
        const obj = {
            idproduct : _idproduct,
            idInternal : this.salesUnitRequirement.internalId
        };
        this.motorService.getPrice(obj).subscribe(
            (res) => {
                const priceComponents: PriceComponent[] = res.json;

                // binding for BBN price
                const priceComponentBBN: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.BBN]);
                if (priceComponentBBN !== undefined) {
                    this.BBNPrice = priceComponentBBN.price;
                }

                // binding for HET price
                const priceComponentHET: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.HET]);
                if (priceComponentHET !== undefined) {
                    this.HETPrice = priceComponentHET.price;
                }

                this.firstPrice = this.priceComponentService.countMotorPrice(priceComponents);
            }
        )
    }

    protected setIsHotItemAndIsIndent(productId?: String): void {
        if (productId !== null) {
            const selectedProductId = this.salesUnitRequirement.productId;
            console.log('aa', selectedProductId);
            const motor: Motor = _.find(this.motors, function(e: Motor) {
                return e.idProduct === selectedProductId;
            });

            this.salesUnitRequirement.unitIndent = false;
            this.salesUnitRequirement.unitHotItem = false;

            // check indent
            this.ruleIndentService.checkByIdInternalAndIdProduct(
                {
                    idProduct: productId,
                    idInternal: this.principalService.getIdInternal()
                }
            ).subscribe(
                (res: Boolean) => {
                    if (res) {
                        this.salesUnitRequirement.unitIndent = true;
                    }
                }
            )

            // check hot item
            this.ruleHotItemService.checkByIdProductAndIdInternal(
                {
                    idproduct: productId,
                    idinternal: this.principalService.getIdInternal()
                }
            ).subscribe(
                (res: Boolean) => {
                    if (res) {
                        this.salesUnitRequirement.unitHotItem = true;
                    }
                }
            )
        }
    }

    public selectMotor(isSelect?: boolean): void {
        this.getColorByMotor(this.salesUnitRequirement.productId);
        this.setIsHotItemAndIsIndent(this.salesUnitRequirement.productId);
        if (isSelect) {// dijalankan ketika melakukan pemilihan motor
            this.resetLeasing();
            const obj = {
                idproduct : this.salesUnitRequirement.productId,
                idinternal : this.salesUnitRequirement.internalId
            };
            this.motorService.getPrice(obj).subscribe(
                (res) => {
                    const priceComponents: PriceComponent[] = res.json;

                    // binding for subsidi AHM
                    const priceComponentSubsidiAHM: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.SUBSIDI_AHM]);

                    this.salesUnitRequirement.subsahm = 0;
                    if (priceComponentSubsidiAHM !== undefined) {
                        this.salesUnitRequirement.subsahm = priceComponentSubsidiAHM.price;
                    }
                    // binding for subsidi main dealer
                    const priceComponentSubsidiMD: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.SUBSIDI_MD]);
                    this.salesUnitRequirement.subsmd = 0;
                    if (priceComponentSubsidiMD !== undefined) {
                        this.salesUnitRequirement.subsmd = priceComponentSubsidiMD.price;
                    }

                    // binding for BBN price
                    const priceComponentBBN: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.BBN]);
                    this.salesUnitRequirement.bbnprice = 0;
                    if (priceComponentBBN !== undefined) {
                        this.BBNPrice = priceComponentBBN.price;
                        this.salesUnitRequirement.bbnprice = priceComponentBBN.price;
                    }

                    // binding for HET price
                    const priceComponentHET: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.HET]);
                    this.salesUnitRequirement.hetprice = 0;
                    if (priceComponentHET !== undefined) {
                        this.HETPrice = priceComponentHET.price;
                        this.salesUnitRequirement.hetprice = priceComponentHET.price;
                    }
                    // binding for unit price
                    this.salesUnitRequirement.unitPrice = this.priceComponentService.countMotorPrice(priceComponents);

                    this.firstPrice = this.priceComponentService.countMotorPrice(priceComponents);

                    this.salesUnitRequirement.minPayment = this.firstPrice;
                }
            );
        }
    }

    // public selectLeasing(): Promise<any> {
    //     this.tempLeasingTenorProvides = new Array<LeasingTenorProvide>();
    //     this.remainingDownPayment = this.salesUnitRequirementService.notSelectLeasingTenorProvideMessage;
    //     return new Promise<any>(
    //         (resolve) => {
    //             if (this.salesUnitRequirement.leasingCompanyId != null) {
    //                 const req = {
    //                     idproduct : this.salesUnitRequirement.productId,
    //                     idleasing : this.salesUnitRequirement.leasingCompanyId
    //                 };
    //                 this.leasingTenorProvideService.getActiveTenorByIdProductAndIdLeasing(req).subscribe(
    //                     (res: ResponseWrapper) => {
    //                         this.tempLeasingTenorProvides = res.json;
    //                         this.checkPastTenorProvide().then(
    //                             () => {
    //                                 this.getTenor(this.tempLeasingTenorProvides);
    //                                 resolve();
    //                             }
    //                         )
    //                     }
    //                 )
    //             } else {
    //                 this.salesUnitRequirement.leasingTenorProvideId = null;
    //                 this.tenors = Array();
    //                 this.installment = 0;
    //                 this.selectedTenor = null;

    //                 resolve();
    //             }
    //         }
    //     );
    // }

    public selectTenor(tenor: number): Promise<void> {
        return new Promise<void>(
            (resolve) => {
                this.selectedTenor = tenor;
                this.leasingTenorProvides = _.filter(this.tempLeasingTenorProvides, function(e) {
                    return e.tenor === tenor;
                });
                this.installment = 0;
                this.remainingDownPayment = this.salesUnitRequirementService.notSelectLeasingTenorProvideMessage;
                resolve();
            }
        )
    }

    public selectDownPayment(data: string): void {
        this.installment = 0;
        const objLeasingTenor: LeasingTenorProvide =  this.salesUnitRequirementService.fnGetSelectedLeasingTenorProvide(this.tempLeasingTenorProvides, data);

        this.installment = objLeasingTenor.installment;
        this.salesUnitRequirement.minPayment = objLeasingTenor.downPayment;
        this.countRemainingDownPayment();
    }

    public selectSaleType(): void {
        const saleTypeId = this.salesUnitRequirement.saleTypeId;
        this.countMotorPriceBySaleType(saleTypeId);
        if (this.saleTypeService.checkIfCash(saleTypeId)) { // Cash
            this.salesUnitRequirement.subsfincomp = 0;
            this.isCredit = false;
            this.salesUnitRequirement.leasingCompanyId = null;
            this.salesUnitRequirement.leasingTenorProvideId = null;
            this.tenors = Array();
            this.installment = 0;
            this.countRemainingPayment();

            this.salesUnitRequirement.bbnprice = this.BBNPrice;
        } else if (this.saleTypeService.checkIfCredit(saleTypeId)) { // Credit Sales
            this.salesUnitRequirement.minPayment = 0;
            this.isCredit = true;

            this.salesUnitRequirement.bbnprice = this.BBNPrice;
        } else if (this.saleTypeService.checkIfOffTheRoad(saleTypeId)) { // Off The Road
            this.salesUnitRequirement.subsfincomp = 0;
            this.isCredit = false;
            this.salesUnitRequirement.leasingCompanyId = null;
            this.salesUnitRequirement.leasingTenorProvideId = null;
            this.tenors = Array();
            this.installment = 0;
            this.countRemainingPayment();
            this.salesUnitRequirement.bbnprice = 0;
        } else {
            this.isCredit = false;
            this.salesUnitRequirement.subsfincomp = 0;
        }
    }

    protected countMotorPriceBySaleType(saleTypeId: number): void {
        if (this.saleTypeService.checkIfOffTheRoad(saleTypeId)) {
            this.salesUnitRequirement.unitPrice = this.firstPrice - this.BBNPrice;
            this.salesUnitRequirement.minPayment = this.firstPrice - this.BBNPrice;
        } else {
           if ( this.salesUnitRequirement.unitPrice !== (this.BBNPrice + this.HETPrice)) {
               this.salesUnitRequirement.unitPrice = this.firstPrice;
               this.salesUnitRequirement.minPayment = this.firstPrice;
           } else {
               this.salesUnitRequirement.minPayment = this.firstPrice;
           }
        }
    }

    public countSubsidiDealer(): void {
        const saleTypeId = this.salesUnitRequirement.saleTypeId;
        if (this.saleTypeService.checkIfCash(saleTypeId) || saleTypeId === this.saleTypeService.checkIfOffTheRoad(saleTypeId)) { // Cash
            this.countRemainingPayment();
        } else if (this.saleTypeService.checkIfCredit(saleTypeId)) { // Credit Sales
            this.countRemainingDownPayment();
        }
    }

    protected resetLeasing(): void {
        this.salesUnitRequirement.leasingCompanyId = null;
        // this.selectLeasing();
        this.remainingDownPayment = this.salesUnitRequirementService.notSelectLeasingTenorProvideMessage;
    }

    public countRemainingDownPayment(): void {
        const productPrice = this.salesUnitRequirement.unitPrice;
        if (productPrice !== null) {
            this.countTotalSubsidi();
            // const objLeasingTenor: LeasingTenorProvide = this.salesUnitRequirementService.fnGetSelectedLeasingTenorProvide(this.tempLeasingTenorProvides, data);
            let creditDownPayment: number = this.salesUnitRequirement.creditDownPayment;
            if (creditDownPayment === null || creditDownPayment === undefined) {
                creditDownPayment = 0;
            }

            let subsFinComp: number = this.salesUnitRequirement.subsfincomp;
            if (subsFinComp === null || subsFinComp === undefined) {
                subsFinComp = 0;
            }

            const totalPaymented = this.salesUnitRequirement.creditDownPayment - (this.totalReceipt + subsFinComp);
            // this.remainingDownPayment = this.currencyPipe.transform(objLeasingTenor.downPayment - totalPaymented, 'IDR', true);
            this.remainingDownPayment = this.currencyPipe.transform(totalPaymented , 'IDR', true);
            this.salesUnitRequirement.downPayment = this.totalSubsidi + this.totalReceipt;
        } else {
            this.remainingDownPayment = 'Please select motor';
        }
    }

    public countRemainingPayment(): void {
        const productPrice = this.salesUnitRequirement.unitPrice;
        if (productPrice !== null) {
            this.countTotalSubsidi();
            this.remainingPayment = this.currencyPipe.transform(productPrice - this.totalSubsidi - this.totalReceipt, 'IDR', true);

            this.salesUnitRequirement.downPayment = this.totalSubsidi + this.totalReceipt;
        } else {
            this.remainingPayment = 'Please select motor';
        }
    }

    public previousState(): void {
        this.router.navigate(['sales-unit-requirement']);
    }

    public save(): void {
        this.isSaving = true;
        if (this.salesUnitRequirement.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.salesUnitRequirementService.update(this.salesUnitRequirement));
        } else {
            this.subscribeToSaveResponse(
                this.salesUnitRequirementService.create(this.salesUnitRequirement));
        }
    }

    public onChangeSameWithCustomer(e): void {
       const checked: boolean = e.checked;
       if (checked) {
           this.disable = true;
            this.personalCustomerService.find(this.salesUnitRequirement.customerId).subscribe(
                (res) => {
                    let newPerson: Person;
                    newPerson = res.person;

                    if (newPerson.dob != null) {
                        newPerson.dob = new Date(newPerson.dob);
                    }

                    this.salesUnitRequirement.personOwner = newPerson;
                }
            )
       } else {
            this.salesUnitRequirement.personOwner = new Person();
            this.disable = false;
       }
    }

    protected getColorByMotor(idProduct: string): void {
        if (idProduct !== null) {
            this.features = new Array<Feature>();
            const selectedProduct: Motor = _.find(this.motors, function(e) {
                return e.idProduct.toLowerCase() === idProduct.toLowerCase();
            });
            this.features = selectedProduct.features;
        }
    }

    protected checkPastTenorProvide(): Promise<void> {
        return new Promise<void>(
            (resolve) => {
                const tenorProvId = this.salesUnitRequirement.leasingTenorProvideId;

                let selectedLeasingTenorProvide: LeasingTenorProvide;
                selectedLeasingTenorProvide = this.salesUnitRequirementService.fnGetSelectedLeasingTenorProvide(this.tempLeasingTenorProvides, tenorProvId);

                if (selectedLeasingTenorProvide === undefined && this.selectedLeasingTenorProvide !== null) {
                    this.tempLeasingTenorProvides.push(this.selectedLeasingTenorProvide);
                }
                resolve();
            }
        )
    }

    protected getTenor(data: any): void {
        if (data.length > 0) {
            const leasingTenorOrderByTenor: LeasingTenorProvide[] = _.orderBy(data, ['tenor'], ['asc']);
            this.tenors = _.map(_.uniqBy(leasingTenorOrderByTenor, 'tenor'), function(e) {
                return e.tenor;
            });
        }
    }

    protected subscribeToSaveResponse(result: Observable<SalesUnitRequirement>) {
        result.subscribe((res: SalesUnitRequirement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: SalesUnitRequirement) {
        this.eventManager.broadcast({ name: 'salesUnitRequirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'salesUnitRequirement saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error): void {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error): void {
        this.toaster.showToaster('warning', 'salesUnitRequirement Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    public isDisabledSubsidiDealer(sur: SalesUnitRequirement): boolean {
        if (sur.approvalDiscount === BaseConstants.Status.STATUS_WAITING_FOR_APPROVAL || sur.approvalDiscount === BaseConstants.Status.STATUS_APPROVED) {
            return true;
        } else {
            return false;
        }
    }

    public isDisableSelectMotor(sur: SalesUnitRequirement): boolean {
        if (sur.approvalDiscount !== BaseConstants.Status.STATUS_NOT_REQUIRED) {
            return true;
        } else {
            return false;
        }
    }

    public disableMakelarFee(): boolean {
        let isDisable: boolean;
        isDisable = true;

        if (this.salesUnitRequirement.prospectSourceId === ProspectSourceConstants.MAKELAR) {
            isDisable = false;
        }

        return isDisable;
    }

    public trackLeasingCompanyById(index: number, item: LeasingCompany): String {
        return item.idLeasingCompany;
    }

    public trackSalesmanById(index: number, item: Salesman) {
        return item.idPartyRole;
    }

    public trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    public trackSaleTypeById(index: number, item: SaleType) {
        return item.idSaleType;
    }

    public trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }

    public trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }

    public trackSalesBrokerById(index: number, item: SalesBroker) {
        return item.idPartyRole;
    }

    public trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    public trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }

    public trackLeasingTenorProvideById(index: number, item: LeasingTenorProvide) {
        return item.idLeasingProvide;
    }
}
