import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from '../../layouts/loading';
import * as BaseConstants from '../../shared/constants/base.constants';
import { setTimeout } from 'timers';

@Component({
    selector: 'jhi-sales-unit-requirement',
    templateUrl: './sales-unit-requirement.component.html'
})
export class SalesUnitRequirementComponent implements OnInit, AfterViewInit {
    baseConstants: any;

    statusOne: boolean;
    statusTwo: boolean;
    statusThree: boolean;
    statusFour: boolean;

    constructor(
        protected router: Router,
        protected loadingService: LoadingService
    ) {
        this.statusOne = false;
        this.statusTwo = false;
        this.statusThree = false;
        this.statusFour = false;
        this.baseConstants = BaseConstants;
    }

    ngOnInit() {
        this.loadingService.loadingStart();
        try {
            this.doRefresh();
        } finally {
            this.loadingService.loadingStop();
        }
    }

    ngAfterViewInit() {
        this.loadingService.loadingStop();
    }

    protected destroyComponentChild(): Promise<void> {
        return new Promise<void> (
            (resolve) => {
                this.statusOne = false;
                this.statusTwo = false;
                this.statusThree = false;
                this.statusFour = false;
                resolve();
            }
        )
    }

    protected buildComponentChild(): void {
        this.statusOne = true;
        this.statusTwo = true;
        this.statusThree = true;
        this.statusFour = true;
    }

    public doRefresh(): void {
        this.destroyComponentChild().then(
            (res) => {
                setTimeout(
                    () => {
                        this.buildComponentChild();
                }, 1000);
            }
        );
    }
}
