import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { Receipt } from '../../receipt/receipt.model';
import { ReceiptService } from '../../receipt/receipt.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
// nuse
import { AxPostingLine } from '../../axposting-line';
import { Internal, InternalService } from '../../internal';
import { Requirement, RequirementService } from '../../requirement';
import { AxPosting, AxPostingService } from '../../axposting';
import { DatePipe } from '@angular/common';
import { Customer, CustomerCService, CustomerAX } from '../../customer';

@Component({
    selector: 'jhi-sur-receipt-as-list',
    templateUrl: './sur-receipt-as-list.component.html'
})
export class SURReceiptAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idPaymentType: any;
    @Input() idMethod: any;
    @Input() idInternal: any;
    @Input() idPaidTo: any;
    @Input() idPaidFrom: any;
    @Input() idParty: any;
    @Input() idRequirement: any;
    @Input() readonly: boolean;
    @Input() isLoadPage: Boolean = false;

    // nuse
    receipt: Receipt;
    axPosting: AxPosting;
    internal: Internal;
    requirement: Requirement;
    axPostingLine: AxPostingLine;
    customer: Customer;
    customerAX: CustomerAX;

    currentAccount: any;
    receipts: Receipt[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        protected receiptService: ReceiptService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService,
        // nuse
        protected axPostingService: AxPostingService,
        protected requirementService: RequirementService,
        protected internalService: InternalService,
        protected datePipe: DatePipe,
        protected customerCService: CustomerCService
    ) {
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'idPayment';
        this.reverse = 'asc';
        this.first = 0;
        this.idParty = null;
        this.idRequirement = null;
    }

    loadAll() {
        this.receiptService.queryFilterBy({
            idPaymentType: this.idPaymentType,
            idMethod: this.idMethod,
            idInternal: this.principal.getIdInternal(),
            idPaidTo: this.idPaidTo,
            idPaidFrom: this.idPaidFrom,
            idRequirement: this.idRequirement,
            isSales: 'false',
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccessC(res.json),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/receipt', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInReceipts();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idRequirement']) {
            if (this.idRequirement !== undefined && this.idRequirement !== null) {
                this.loadAll();
            }
        }

        if (changes['idPaymentType']) {
            if (this.idPaymentType !== undefined && this.idPaymentType !== null) {
                this.loadAll();
            }
        }
        if (changes['idMethod']) {
            if (this.idMethod !== undefined && this.idMethod !== null) {
                this.loadAll();
            }
        }
        if (changes['idInternal']) {
            if (this.idInternal !== undefined && this.idInternal !== null) {
                this.loadAll();
            }
        }
        if (changes['idPaidTo']) {
            if (this.idPaidTo !== undefined && this.idPaidTo !== null) {
                this.loadAll();
            }
        }
        if (changes['idPaidFrom']) {
            if (this.idPaidFrom !== undefined && this.idPaidFrom !== null) {
                this.loadAll();
            }
        }
        if (changes['idRequirement'] && changes['isLoadPage']) {
            if (this.idRequirement !== undefined || this.idRequirement !== null
                || this.isLoadPage === true || this.isLoadPage === false) {
                this.loadAll();
            }
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Receipt) {
        return item.idPayment;
    }

    registerChangeInReceipts() {
        this.eventSubscriber = this.eventManager.subscribe('receiptListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idPayment') {
            result.push('idPayment');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        console.log('data', data);
        this.receipts = data;
    }

    protected onSuccessC(data) {
        this.queryCount = this.totalItems = data.length > 0 ? data[0].TotalData : 0;
        this.receipts = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.receiptService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }

        if (this.checkIfInputExist()) {
            this.loadAll();
        }
    }

    protected checkIfInputExist(): boolean {
        if (this.idRequirement !== undefined && this.idRequirement !== null) {
            return true;
        }

        if (this.idPaymentType !== undefined && this.idPaymentType !== null) {
            return true;
        }

        if (this.idMethod !== undefined && this.idMethod !== null) {
            return true;
        }

        if (this.idInternal !== undefined && this.idInternal !== null) {
            return true;
        }

        if (this.idPaidTo !== undefined && this.idPaidTo !== null) {
            return true;
        }

        if (this.idPaidFrom !== undefined && this.idPaidFrom !== null) {
            return true;
        }
        if (this.idRequirement !== undefined || this.idRequirement !== null
            || this.isLoadPage === true || this.isLoadPage === false) {
                return true;
        }

        return false;
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.subscribeToSaveResponse(
                this.receiptService.update(event.data));
        }
        // else {
        //     this.subscribeToSaveResponse(
        //         this.receiptService.create(event.data));
        // }
    }

    protected subscribeToSaveResponse(result: Observable<Receipt>) {
        result.subscribe((res: Receipt) => this.onSaveSuccess(res));
    }

    protected onSaveSuccess(result: Receipt) {
        this.eventManager.broadcast({ name: 'receiptListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'receipt saved !');
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                console.log('ini hapus nominal');
                this.receiptService.find(id).subscribe(
                    ( resReceipt: Receipt ) => {
                        this.receipt = resReceipt;

                        this.receiptService.delete(id).subscribe((response) => {
                            this.eventManager.broadcast({
                                name: 'receiptListModification',
                                content: 'Deleted an receipt'
                            });
                        });

                        this.internalService.find(resReceipt.paidToId).subscribe(
                            (internal: Internal) => {
                                console.log('internal.id : ', internal.idMPM);
                                this.internal = internal;
                                console.log('this.receipt.idRequirement', this.receipt.idRequirement);
                                this.requirementService.find(this.receipt.idRequirement).subscribe(
                                    (resReq: Requirement) => {
                                        this.requirement = resReq;
                                        console.log('requirementPayment.PaymentId : ', this.requirement.requirementNumber);

                                        this.customerCService.findByIdCustomer(this.receipt.paidFromId, this.receipt.methodId).subscribe(
                                            (resCustomer: CustomerAX) => {
                                                this.customerAX = resCustomer;

                                                const today = new Date();
                                                const myaxPostingLine = [];

                                                this.axPosting = new AxPosting();
                                                this.axPosting.AutoPosting = 'FALSE';
                                                this.axPosting.AutoSettlement = 'FALSE';
                                                this.axPosting.DataArea = this.internal.idMPM;
                                                this.axPosting.Guid = this.receipt.idRequirement;
                                                this.axPosting.JournalName = 'titipan-AR-UnitRev';
                                                this.axPosting.Name = 'Pengembalian Uang Titipan ' + this.customerAX.idMPM + ' ' + this.requirement.requirementNumber;
                                                this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                                this.axPosting.TransactionType = 'Unit';

                                                // credit
                                                this.axPostingLine = new AxPostingLine();
                                                this.axPostingLine.AccountNum = this.customer.idMPM;
                                                this.axPostingLine.AccountType = 'Cust';
                                                this.axPostingLine.AmountCurDebit = this.receipt.amount.toString();
                                                this.axPostingLine.AmountCurCredit = '0';
                                                this.axPostingLine.Company = this.internal.idMPM;
                                                this.axPostingLine.DMSNum = this.requirement.requirementNumber;
                                                this.axPostingLine.Description = 'Pengembalian Uang Titipan ' + this.customerAX.idMPM + ' ' + this.requirement.requirementNumber;
                                                this.axPostingLine.Dimension1 = '1014';
                                                this.axPostingLine.Dimension2 = this.internal.idMPM;
                                                this.axPostingLine.Dimension3 = '000';
                                                this.axPostingLine.Dimension4 = '000';
                                                this.axPostingLine.Dimension5 = '00';
                                                this.axPostingLine.Dimension6 = '000';
                                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                                this.axPostingLine.Invoice = '';
                                                this.axPostingLine.Payment = '';

                                                myaxPostingLine.push(this.axPostingLine);

                                                // debit
                                                this.axPostingLine = new AxPostingLine();
                                                this.axPostingLine.AccountNum = this.customerAX.bankAccount;
                                                this.axPostingLine.AccountType = 'Bank';
                                                this.axPostingLine.AmountCurDebit = '0';
                                                this.axPostingLine.AmountCurCredit = this.receipt.amount.toString();
                                                this.axPostingLine.Company = this.internal.idMPM;
                                                this.axPostingLine.DMSNum = null;
                                                this.axPostingLine.Description = 'Pengembalian Uang Titipan ' + this.customerAX.idMPM + ' ' + this.requirement.requirementNumber;
                                                this.axPostingLine.Dimension1 = '1014';
                                                this.axPostingLine.Dimension2 = this.internal.idMPM;
                                                this.axPostingLine.Dimension3 = '000';
                                                this.axPostingLine.Dimension4 = '000';
                                                this.axPostingLine.Dimension5 = '00';
                                                this.axPostingLine.Dimension6 = '000';
                                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                                this.axPostingLine.Invoice = '';
                                                this.axPostingLine.Payment = '';

                                                myaxPostingLine.push(this.axPostingLine);

                                                this.axPosting.LedgerJournalLine = myaxPostingLine;
                                                console.log('axPosting data :', this.axPosting);
                                                this.axPostingService.send(this.axPosting).subscribe(
                                                    (resaxPosting: ResponseWrapper) =>
                                                        console.log('Success : ', resaxPosting.json.Message),
                                                    (resaxPosting: ResponseWrapper) => {
                                                        this.onError(resaxPosting.json);
                                                        console.log('error ax posting : ', resaxPosting.json.Message);
                                                    }
                                                );
                                            }
                                        );
                                    }
                                );
                            }
                        );
                    }
                );
            }
        });
    }
}
