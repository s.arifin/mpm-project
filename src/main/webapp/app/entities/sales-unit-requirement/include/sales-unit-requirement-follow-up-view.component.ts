import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';

import { SalesUnitRequirement } from '../sales-unit-requirement.model';
import { SalesUnitRequirementService } from '../sales-unit-requirement.service';

@Component({
    selector: 'jhi-sales-unit-requirement-follow-up-view',
    templateUrl: './sales-unit-requirement-follow-up-view.component.html'
})

export class SalesUnitRequirementFollowUpViewComponent {
    @Input()
    salesUnitRequirement: SalesUnitRequirement;

    constructor() {

    }
};
