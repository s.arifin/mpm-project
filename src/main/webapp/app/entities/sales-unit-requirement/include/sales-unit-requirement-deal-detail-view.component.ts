import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';

import { SalesUnitRequirement } from '../sales-unit-requirement.model';

@Component({
    selector: 'jhi-sales-unit-requirement-deal-detail-view',
    templateUrl: './sales-unit-requirement-deal-detail-view.component.html'
})

export class SalesUnitRequirementDealDetailViewComponent implements OnChanges {
    @Input()
    salesUnitRequirement: SalesUnitRequirement;
    @Input() viewDetailShipment: any = 0;

    constructor() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['salesUnitRequirement']) {
        }
    }
}
