import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';

import { SalesUnitRequirement } from '../../sales-unit-requirement';

@Component({
    selector: 'jhi-sales-unit-requirement-detail-container',
    templateUrl: './sales-unit-requirement-detail-container.component.html'
})

export class SalesUnitRequirementDetailContainerComponent {
    @Input()
    public salesUnitRequirement: SalesUnitRequirement;

    public isCountReceipt: boolean;

    constructor() {
        this.isCountReceipt = true;
    }
}
