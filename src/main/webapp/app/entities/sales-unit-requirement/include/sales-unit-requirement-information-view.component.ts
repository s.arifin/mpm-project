import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';

import { SalesUnitRequirement } from '../sales-unit-requirement.model';
import { SalesUnitRequirementService } from '../sales-unit-requirement.service';

@Component({
    selector: 'jhi-sales-unit-requirement-information-view',
    templateUrl: './sales-unit-requirement-information-view.component.html'
})

export class SalesUnitRequirementInformationViewComponent implements OnChanges {
    @Input()
    public salesUnitRequirement: SalesUnitRequirement;

    constructor() {

    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['salesUnitRequirement']) {

        }
    }
};
