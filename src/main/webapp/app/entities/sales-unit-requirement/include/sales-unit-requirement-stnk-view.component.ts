import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';

import { SalesUnitRequirement } from '../sales-unit-requirement.model';

@Component({
    selector: 'jhi-sales-unit-requirement-stnk-view',
    templateUrl: './sales-unit-requirement-stnk-view.component.html'
})

export class SalesUnitRequirementSTNKViewComponent implements OnChanges {
    @Input() salesUnitRequirement: SalesUnitRequirement;
    @Input() viewDetailShipment: any = 0;
    readonlyDocument: boolean;

    constructor() {
        this.readonlyDocument = true;
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['salesUnitRequirement']) {

        }
    }
}
