import { Component, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrencyPipe } from '@angular/common';
import { ResponseWrapper } from '../../../shared';

import { SalesUnitRequirement, SalesUnitRequirementService } from '../';
import { ReceiptService, Receipt } from '../../receipt';
import { LeasingTenorProvide, LeasingTenorProvideService } from '../../leasing-tenor-provide';
import { LeasingCompany, LeasingCompanyService } from '../../leasing-company';

import * as SaleTypeConstants from '../../../shared/constants/sale-type.constants';
import * as _ from 'lodash';
import { SaleTypeService } from '../../sale-type';
import { Subscription } from 'rxjs';

@Component({
    selector: 'jhi-sales-unit-requirement-payment-view',
    templateUrl: './sales-unit-requirement-payment-view.component.html'
})

export class SalesUnitRequirementPaymentViewComponent implements OnChanges, OnDestroy {
    @Input()
    salesUnitRequirement: SalesUnitRequirement;

    private subscriptionReceipt: Subscription;

    public remainingDownPayment: any;
    public paymentReadonly: Boolean = true;
    public isCredit: boolean;
    public installment: number;
    public leasingCompany: LeasingCompany;
    public leasingTenorProvide: LeasingTenorProvide;

    private tenors: Array<number>;

    totalReceipt: number;

    totalSubsidi: number;

    remainingPayment: any;

    isCountReceipt: Boolean = true;

    constructor(
        private receiptService: ReceiptService,
        private salesUnitRequirementService: SalesUnitRequirementService,
        private currencyPipe: CurrencyPipe,
        private leasingTenorProvideService: LeasingTenorProvideService,
        private leasingCompanyService: LeasingCompanyService,
        private saleTypeService: SaleTypeService
    ) {
        this.leasingCompany = new LeasingCompany();
        this.leasingTenorProvide = new LeasingTenorProvide();
        this.totalReceipt = 0;
        this.totalSubsidi = 0;
        this.isCredit = false;
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['salesUnitRequirement']) {
            console.log('SUR di payment', this.salesUnitRequirement);
            this.registerReceipt().then(
                (resolve) => {
                    this.loadData();
                    this.countTotalSubsidi();
                }
            )
        }
    }

    ngOnDestroy() {
        this.subscriptionReceipt.unsubscribe();
    }

    private registerReceipt(): Promise<void> {
        return new Promise<void>(
            (resolve, reject) => {
                this.subscriptionReceipt = this.receiptService.values.subscribe(
                    (res) => {
                        this.countReceipt(res);
                        resolve();
                    }
                );
            }
        );
    }

    private loadData(): void {
        if (this.saleTypeService.checkIfCredit(this.salesUnitRequirement.saleTypeId)) {
            this.isCredit = true;
            this.countRemainingDownPayment();
        }
    }

    public countReceipt(res): void {
        this.totalReceipt = this.receiptService.countTotalReceipt(res);
     }

    public countRemainingPayment(): void {
        const productPrice = this.salesUnitRequirement.unitPrice;
        if (productPrice !== null) {
            // this.countTotalSubsidi();
            this.remainingPayment = this.currencyPipe.transform(productPrice - this.totalSubsidi - this.totalReceipt, 'IDR', true);

            this.salesUnitRequirement.downPayment = this.totalSubsidi + this.totalReceipt;
        } else {
            this.remainingPayment = 'Please choose product!';
        }
    }

    public countRemainingDownPayment(): void {
        console.log('this.totalReceipt', this.totalReceipt);
        this.remainingDownPayment = this.salesUnitRequirementService.countRemainingDownPaymentForCredit(this.salesUnitRequirement, this.totalReceipt);
    }

    countTotalSubsidi(): void {
        this.totalSubsidi = this.salesUnitRequirementService.fnCountTotalSubsidi(this.salesUnitRequirement);
        this.countRemainingPayment();
    }
}
