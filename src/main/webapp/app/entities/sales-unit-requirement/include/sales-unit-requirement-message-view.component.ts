import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';

import { SalesUnitRequirement } from '../sales-unit-requirement.model';
import { SalesUnitRequirementService } from '../sales-unit-requirement.service';

@Component({
    selector: 'jhi-sales-unit-requirement-message-view',
    templateUrl: './sales-unit-requirement-message-view.component.html'
})

export class SalesUnitRequirementMessageViewComponent implements OnInit, OnDestroy {
    @Input()
    id: number;

    messages: Array<Object> = [
        {
            severity: 'info',
            summary: 'Info',
            detail: 'Coming Soon'
        }
    ];

    constructor() {

    }

    ngOnInit() {

    }

    ngOnDestroy() {}
}
