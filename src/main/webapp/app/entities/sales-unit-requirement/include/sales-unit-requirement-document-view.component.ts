import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';

import { SalesUnitRequirement } from '../sales-unit-requirement.model';
import { PersonalCustomerService } from '../../personal-customer';

@Component({
    selector: 'jhi-sales-unit-requirement-document-view',
    templateUrl: './sales-unit-requirement-document-view.component.html'
})

export class SalesUnitRequirementDocumentViewComponent implements OnChanges {
    @Input()
    salesUnitRequirement: SalesUnitRequirement;

    partyId: String;

    readonlyDocument: boolean;

    constructor(
        private personalCustomerService: PersonalCustomerService
    ) {
        this.readonlyDocument = true;
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.salesUnitRequirement.customerId !== undefined) {
            this.personalCustomerService.find(this.salesUnitRequirement.customerId).subscribe(
                (res) => {
                    this.partyId = res.partyId;
                }
            )
        }
    }
};
