import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';

import { SalesUnitRequirement} from '../';
import { PersonalCustomerService, PersonalCustomer, OrganizationCustomer, OrganizationCustomerService } from '../../shared-component';

import * as reqType from '../../../shared/constants/requirement-type.constants';
import * as SalesUnitRequirementConstants from '../../../shared/constants/sales-unit-requirement.constants';

@Component({
    selector: 'jhi-sales-unit-requirement-customer-view',
    templateUrl: './sales-unit-requirement-customer-view.component.html'
})

export class SalesUnitRequirementCustomerViewComponent implements OnChanges {
    @Input()
    public salesUnitRequirement: SalesUnitRequirement;

    @Input()
    public viewDetailShipment: any = 0;

    personalCustomer: PersonalCustomer;
    organizationCustomer: OrganizationCustomer;
    readonlyDocument: Boolean = true;

    constructor(
        private personalCustomerService: PersonalCustomerService,
        private organizationCustomerService: OrganizationCustomerService
    ) {
        this.personalCustomer = new PersonalCustomer();
    }

    ngOnChanges(changes: SimpleChanges ) {
        if (changes['salesUnitRequirement']) {
            if (this.salesUnitRequirement.customerId !== undefined) {
                if (this.salesUnitRequirement.idReqTyp === reqType.ORGANIZATION_CUSTOMER ) {
                    this.getOrganizationCustomer(this.salesUnitRequirement.customerId);
                } else {
                    this.getPersonalCustomer(this.salesUnitRequirement.customerId);
                }
            }
        }
    }

    getPersonalCustomer(id: string): void {
        this.personalCustomerService.find(id).subscribe(
            (res) => {
                if (res.person.dob !== null) {
                    res.person.dob = new Date(res.person.dob);
                }
                console.log('aaaa', res);
                this.personalCustomer = res;
            }
        )
    }
    getOrganizationCustomer(id: string): void {
        this.organizationCustomerService.find(id).subscribe(
            (res) => {
                this.organizationCustomer = res;
            }
        )
    }

    public showNotShipment(): boolean {
        let a: boolean;
        a = false;

        if (this.viewDetailShipment === 0) {
            a = true;
        }

        return a;
    }

    public showNameOrFullName(data: number): boolean {
        let a: boolean;
        a = false;

        if (data === 1 || data === 0) {
            a = true;
        }

        return a;
    }
};
