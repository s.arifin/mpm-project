import {Component, OnInit, OnChanges, OnDestroy, Input, Output, SimpleChanges, EventEmitter} from '@angular/core';
import {Response} from '@angular/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Rx';
import {JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import {SalesUnitRequirement} from './sales-unit-requirement.model';
import {SalesUnitRequirementService} from './sales-unit-requirement.service';
import {ITEMS_PER_PAGE, Principal, ResponseWrapper} from '../../shared';
import {PaginationConfig} from '../../blocks/config/uib-pagination.config';

import {LazyLoadEvent} from 'primeng/primeng';
import {ToasterService} from '../../shared/alert/toaster.service';
import {ConfirmationService} from 'primeng/primeng';
import { PersonalCustomerService } from '../personal-customer';
import { OrganizationCustomerService } from '../organization-customer';

import * as BaseConstants from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-sales-unit-requirement-as-list',
    templateUrl: './sales-unit-requirement-as-list.component.html'
})
export class SalesUnitRequirementAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() filterBy: any;
    @Input() idRequest: string;
    @Input() requestType: number;

    @Output() current = new EventEmitter<SalesUnitRequirement>();

    currentAccount: any;
    salesUnitRequirements: SalesUnitRequirement[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    newPersonal: Subscription;
    newOrganization: Subscription;

    constructor(
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected personalCustomerService: PersonalCustomerService,
        protected organizationCustomerService: OrganizationCustomerService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idRequirement';
        this.reverse = 'asc';
    }

    loadAll() {
        this.salesUnitRequirementService.queryFilterBy({
            filterBy: this.filterBy,
            idRequest: this.idRequest,
            idInternal: this.principalService.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    addNewItem(customer) {
        // const n: CustomerRequestItem = new CustomerRequestItem();
        // n.customerId = customer.idCustomer;
        // n.requestId = this.idRequest;
        // this.customerRequestItemService.create(n).subscribe((r) => {
        //     this.loadAll();
        // });
    }

    transition() {
        this.router.navigate(['/sales-unit-requirement'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/sales-unit-requirement', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principalService.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSalesUnitRequirements();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['filterBy']) {
            console.log('aaaa', this.filterBy);
            this.loadAll();
        }

        if (changes['idRequest']) {
            console.log('bbbb', this.idRequest);
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
        this.newPersonal.unsubscribe();
        this.newOrganization.unsubscribe();
    }

    trackId(index: number, item: SalesUnitRequirement) {
        return item.idRequirement;
    }

    registerChangeInSalesUnitRequirements() {
        this.eventSubscriber = this.eventManager.subscribe('salesUnitRequirementListModification', (response) => this.loadAll());
        this.newOrganization = this.organizationCustomerService.newCustomer.subscribe((r) => {
            this.addNewItem(r);
        });
        this.newPersonal = this.personalCustomerService.newCustomer.subscribe((r) => {
            this.addNewItem(r);
        });
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        console.log('data', data);
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.salesUnitRequirements = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    public cancel(salesUnitRequirement: SalesUnitRequirement): void {
        this.confirmationService.confirm({
            header : 'Confirmation',
            message : 'Do you want to cancel this VSO Draft?',
            accept: () => {
                console.log('aaaa');
                this.salesUnitRequirementService.changeStatus(salesUnitRequirement, BaseConstants.Status.STATUS_CANCEL).subscribe(
                    (res) => {
                        this.loadAll();
                    }
                )
            }
        });
    }

    executeProcess(data) {
        this.salesUnitRequirementService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.salesUnitRequirementService.update(event.data)
                .subscribe((res: SalesUnitRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.salesUnitRequirementService.create(event.data)
                .subscribe((res: SalesUnitRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: SalesUnitRequirement) {
        this.toasterService.showToaster('info', 'SalesUnitRequirement Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }
}
