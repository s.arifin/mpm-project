import { Component, OnInit, OnDestroy } from '@angular/core';
import { MotorService, Motor } from '../motor';
import { ResponseWrapper, CommonUtilService, Principal } from '../../shared';
import { Subscription, Observable } from 'rxjs/Rx';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesUnitRequirement, SalesUnitRequirementService } from './';
import { SalesBooking, SalesBookingService } from '../sales-booking';
import { LoadingService } from '../../layouts';
import { Feature, FeatureService } from '../feature';
import { ConfirmationService } from 'primeng/primeng';
import { UnitDocumentMessage, UnitDocumentMessageService } from '../unit-document-message';

import { Mokita } from '../mokita/mokita.model';
import { MokitaService } from '../mokita/mokita.service';

import * as BaseConstant from '../../shared/constants/base.constants';
import * as SalesUnitRequirementConstant from '../../shared/constants/sales-unit-requirement.constants';
import * as _ from 'lodash';
// nuse
import { Customer, CustomerCService, CustomerAX } from '../customer';
import { AxCustomer, AxCustomerService } from '../axcustomer';
@Component({
    selector: 'jhi-sales-unit-requirement-check-availability',
    templateUrl: './sales-unit-requirement-check-availability.component.html'
})

export class SalesUnitRequirementCheckAvailabilityComponent implements OnInit, OnDestroy {
    protected subscription: Subscription;

    public salesBooking: SalesBooking;
    public motors: Array<Motor>;
    public salesUnitRequirement: SalesUnitRequirement;
    public years: Array<number>;
    public features: Array<Feature>;
    public search: boolean;
    public qtyAvailable: number;
    public selectedMotor: Motor;
    public selectedFeature: Feature;
    public isAvailable: boolean;
    public idSur: string;
    public unitDocumentMessage: UnitDocumentMessage;
    public udmResponse: UnitDocumentMessage[];
    public statusCheckAvailability: string;
    public isSave: boolean;
    public motorsForSelect: Array<Object>;
    public fetchMotor: boolean;
    mokita: Mokita;

    // nuse
    customer: Customer;
    axCustomer: AxCustomer;
    customerAX: CustomerAX;

    constructor(
        protected commontUtilService: CommonUtilService,
        protected motorService: MotorService,
        protected route: ActivatedRoute,
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected loadingService: LoadingService,
        protected featureService: FeatureService,
        protected salesBookingService: SalesBookingService,
        protected confirmationService: ConfirmationService,
        protected router: Router,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected principalService: Principal,
        protected customerCService: CustomerCService,
        protected axCustomerService: AxCustomerService,
        protected mokitaService: MokitaService,
    ) {
        this.mokita = new Mokita();
        this.idSur = null;
        this.isAvailable = false;
        this.qtyAvailable = 0;
        this.search = false;
        this.salesBooking = new SalesBooking;
        this.selectedMotor = new Motor;
        this.selectedFeature = new Feature;
        this.unitDocumentMessage = new UnitDocumentMessage;
        this.udmResponse = new Array<UnitDocumentMessage>();
        this.fetchMotor = true;
    }

    ngOnInit() {
        this.getYears();
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.loadingService.loadingStart();

                this.idSur = params['id'];
                this.checkHaveUnitDocumentMessage(this.idSur).then(
                    () => {
                        this.loadSur(this.idSur).then(
                            () => {
                                const sur: SalesUnitRequirement = this.salesUnitRequirement;
                                this.salesBooking = this.mappingSalesUnitRequirementToSalesBooking(sur);

                                this.motorService.find(sur.productId).subscribe(
                                    (motor: Motor) => {
                                        this.selectedMotor = motor;
                                        this.featureService.find(sur.colorId).subscribe(
                                            (feature: Feature) => {
                                                this.selectedFeature = feature;
                                                this.checkAvailability();
                                            }
                                        )
                                    }
                                )
                            }
                        ).catch(
                            (err) => {
                                this.loadingService.loadingStop();
                                this.commontUtilService.showError(err);
                            }
                        )

                        this.loadMotor().catch(
                            (err) => {
                                this.loadingService.loadingStop();
                                this.commontUtilService.showError(err);
                            }
                        );
                    }
                )
            }
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    public getColorByIdProduct(idProduct: String): void {
        const selectedMotor: Motor = _.find(this.motors, function(e: Motor){
            return e.idProduct.toLowerCase() === idProduct.toLowerCase();
        });
        this.features = selectedMotor.features;
    }

    protected checkHaveUnitDocumentMessage(idReq: string): Promise<void> {
        return new Promise<void>(
            (resolve, reject) => {
                this.unitDocumentMessageService.getDataByIdRequirement(idReq).subscribe(
                    (res: ResponseWrapper) => {
                        const unitDocumentMessages: Array<UnitDocumentMessage> = res.json;
                        if (unitDocumentMessages.length > 0) {
                            for (let i = 0; i < unitDocumentMessages.length; i++) {
                                const each: UnitDocumentMessage = unitDocumentMessages[i];
                                if (each.currentApprovalStatus === BaseConstant.Status.STATUS_WAITING_FOR_APPROVAL) {
                                    this.router.navigate(['sales-unit-requirement/' + this.idSur + '/list-document-message']);
                                }
                            }
                        }
                        resolve();
                    }, (err) => {
                        reject(err);
                    }
                )
            }
        );
    }

    protected mappingSalesUnitRequirementToSalesBooking(data: SalesUnitRequirement): SalesBooking {
        let a: SalesBooking;

        a = new SalesBooking;
        a.idFeature = data.colorId;
        a.yearAssembly = data.productYear;
        a.idProduct = data.productId;
        a.idInternal = this.principalService.getIdInternal();

        return a;
    }

    public btnCheckUnit(): void {
        if (this.search) {
            this.salesBooking = new SalesBooking;
            this.search = false;
        } else {
            this.search = true;
        }
    }

    public getSelectedMotor(): void {
        const idProduct: string = this.salesBooking.idProduct;
        this.selectedMotor = _.find(this.motors, function(e: Motor) {
            return e.idProduct.toLowerCase() === idProduct.toLowerCase();
        });
        this.getSelectedColor();
    }

    protected convertMotorForSelect(data: Array<Motor>) {
        this.motorsForSelect = this.motorService.convertMotorForSelectPrimeNg(data);
    }

    public isDisableSelectMotor(sur: SalesUnitRequirement): boolean {
        return false;
    }

    public getSelectedColor(): void {
        const idFeature: number = this.salesBooking.idFeature;
        this.selectedFeature = _.find(this.selectedMotor.features, function(e: Feature) {
            return e.idFeature === idFeature;
        });
    }

    public checkAvailability(mode?: string): void {
        this.loadingService.loadingStart();
        this.salesBookingService.checkAvailability(this.salesBooking, this.principalService.getIdInternal()).subscribe(
            (res: ResponseWrapper) => {
                this.qtyAvailable = res.json;
                this.btnCheckUnit();

                if (mode === 'new') {
                    this.getSelectedMotor();
                }

                this.statusAvailable();
                this.loadingService.loadingStop();
            },
            (err) => {
                this.loadingService.loadingStop();
                this.commontUtilService.showError(err);
            }
        )
    }

    protected loadFeature(): Promise<void> {
        return new Promise<void>(
            (resolve, reject) => {
                this.featureService.query().subscribe(
                    (res) => {
                        this.features = res.json;
                        resolve();
                    },
                    (err) => {
                        reject(err);
                    }
                )
            }
        )
    }

    protected loadSur(id: string): Promise<void> {
        return new Promise<void>(
            (resolve, reject) => {
                this.salesUnitRequirementService.find(id).subscribe(
                    (res: SalesUnitRequirement) => {
                        this.salesUnitRequirement = res;
                        resolve();
                    },
                    (err) => {
                        reject(err)
                    }
                );
            }
        );
    }

    protected loadMotor(): Promise<void> {
        return new Promise<void>(
            (resolve, reject) => {
                this.motorService.query(
                    {
                        page: 0,
                        size: 1000,
                        sort: ['idProduct', 'asc']
                    }
                ).subscribe(
                    (res: ResponseWrapper) => {
                        this.motors = res.json;
                        this.fetchMotor = false;
                        this.convertMotorForSelect(res.json);
                        resolve();
                    },
                    (err) => {
                        reject(err);
                    }
                )
            }
        )
    }

    public getYears(): void {
        this.years = new Array<number>();
        this.years = this.salesUnitRequirementService.optionYearAssembly();
    }

    public statusAvailable(): void {
        this.statusCheckAvailability = 'Not Available';
        this.isAvailable = false;
        console.log('this.salesUnitRequirement.unitIndent', this.salesUnitRequirement.unitIndent);
        if (this.qtyAvailable > 0 && !this.salesUnitRequirement.unitIndent) {
            this.isAvailable = true;
            this.statusCheckAvailability = 'Available';
        }
    }

    public bookNow(): void {
        this.confirmationService.confirm({
            message: 'Are you sure do you want to book this motor ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.salesUnitRequirement.colorId = this.selectedFeature.idFeature;
                this.salesUnitRequirementService.update(this.salesUnitRequirement).subscribe(
                    (r) => {
                this.salesBooking.requirementId = this.idSur;
                this.salesBooking.idInternal = this.principalService.getIdInternal();
                this.salesBookingService.create(this.salesBooking).subscribe(
                    (res) => {
                        this.buildVSO(this.salesUnitRequirement).then(
                            (resolve) => {
                                this.customerCService.findByIdCustomer(this.salesUnitRequirement.customerId, 0).subscribe(
                                    (resCustomer: CustomerAX) => {
                                        this.customerAX = resCustomer;

                                        this.axCustomer = new AxCustomer();
                                        this.axCustomer.AccountNum = this.customerAX.idMPM;
                                        this.axCustomer.DataArea = this.salesUnitRequirement.internalId.substr(0, 3);
                                        this.axCustomer.CustGroup = '130-Retail';
                                        this.axCustomer.Name = this.customerAX.partyName;
                                        this.axCustomer.Dimensi1 = '1014';
                                        this.axCustomer.Dimensi2 = this.salesUnitRequirement.internalId.substr(0, 3);
                                        this.axCustomer.Dimensi3 = '000';
                                        this.axCustomer.Dimensi4 = '000';
                                        this.axCustomer.Dimensi5 = '00';
                                        this.axCustomer.Dimensi6 = '000';

                                        console.log('axCustomer:', this.axCustomer);
                                        this.axCustomerService.send(this.axCustomer).subscribe(
                                            (resWrapper: ResponseWrapper) => {
                                                console.log('success : ', resWrapper.json.Message);
                                            }
                                        );
                                    }
                                );
                                this.mokita.idreq = this.salesUnitRequirement.idRequirement;
                                this.mokita.idorderstatus = 10;
                                this.mokitaService.spk(this.mokita).subscribe((a) => {});
                                this.backToList();
                            }
                        ).catch(
                            (err) => {
                                this.loadingService.loadingStop();
                                this.commontUtilService.showError(err);
                            }
                        )
                    },
                    (err) => {
                        this.loadingService.loadingStop();
                        this.commontUtilService.showError(err);
                    }
                )
            }
        )
            }
        });
    }

    protected buildVSO(sur: SalesUnitRequirement): Promise<any> {
        return new Promise<any>(
            (resolve, reject) => {
                this.salesUnitRequirementService.executeProcess(SalesUnitRequirementConstant.BUILD_VSO, null, sur).subscribe(
                    (res) => {
                        resolve(true);
                    },
                    (err) => {
                        reject(err);
                    }
                )
            }
        );
    }

    public backOrder(): void {
        this.confirmationService.confirm({
            message: 'Are you sure do you want to back order?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.salesUnitRequirementService.executeProcess(117, null, this.salesUnitRequirement).subscribe(
                    (resolve) => {
                        this.backToList();
                    },
                    (err) => {
                        this.loadingService.loadingStop();
                        this.commontUtilService.showError(err);
                    }
                )
            }
        });
    }

    public checkIsDifferentUnit(): boolean {
        let a: boolean;
        a = false;

        if (this.salesUnitRequirement.productId !== this.salesBooking.idProduct) {
            a = true;
        }

        return a;
    }

    protected backToList(): void {
        this.router.navigate(['sales-unit-requirement']);
    }

    public changeUnit(): void {
        this.confirmationService.confirm({
            message: 'Are you sure do you want to change unit ? because all data will reset',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.salesUnitRequirementService.executeProcess(SalesUnitRequirementConstant.RESET_VSO_DRAFT, null, this.salesUnitRequirement).subscribe(
                    (res) => {
                        this.loadingService.loadingStop();
                        this.backToList();
                    },
                    (err) => {
                        this.loadingService.loadingStop();
                        this.commontUtilService.showError(err);
                    }
                )
            }
        })
    }

    public cancelSUR(): void {
        this.confirmationService.confirm({
            header : 'Confirmation',
            message : 'Do you want to cancel this VSO Draft?',
            accept: () => {
                this.salesUnitRequirementService.changeStatus(this.salesUnitRequirement, BaseConstant.Status.STATUS_CANCEL).subscribe(
                    (res) => {
                        this.backToList();
                    }
                )
            }
        });
    }

    public sendUnitMessage(): void {
        this.confirmationService.confirm({
            header : 'Confirmation',
            message : 'Do you want to notif korsal with this message?',
            accept: () => {
                this.unitDocumentMessage.idRequirement = this.salesUnitRequirement.idRequirement;
                this.unitDocumentMessageService.queryFilterBy({
                    idreq: this.salesUnitRequirement.idRequirement
                }).subscribe(
                    (res) => {
                        this.udmResponse = res.json;
                        console.log('udm sur =', this.udmResponse);
                        if (this.udmResponse.length > 0  ) {
                            this.router.navigate(['sales-unit-requirement/' + this.idSur + '/list-document-message']);
                        } else {
                            this.unitDocumentMessageService.create(this.unitDocumentMessage).subscribe(
                                (res1) => {
                                    this.checkHaveUnitDocumentMessage(this.idSur);
                                }
                            )
                        }
                    }
                );
            }
        });
    }

    public processIndent(): void {
        this.confirmationService.confirm({
            message: 'Are you sure do you want to indent this vso draft ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.salesUnitRequirementService.executeProcess(116, null, this.salesUnitRequirement).subscribe(
                    (res) => {
                        this.loadingService.loadingStop();
                        this.backToList();
                    },
                    (err) => {
                        this.loadingService.loadingStop();
                        this.commontUtilService.showError(err);
                    }
                )
            }
        })
    }
}
