import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SalesUnitRequirementComponent } from './sales-unit-requirement.component';
import { SalesUnitRequirementEditComponent } from './sales-unit-requirement-edit.component';
import { SalesUnitRequirementLovPopupComponent } from './sales-unit-requirement-as-lov.component';
import { SalesUnitRequirementPopupComponent } from './sales-unit-requirement-dialog.component';
import { SalesUnitRequirementDetailComponent } from './sales-unit-requirement-detail.component';
import { SalesUnitRequirementCheckAvailabilityComponent } from './sales-unit-requirement-check-availability.component';
import { SalesUnitRequirementStatusUnitDocumentMessageComponent } from './sales-unit-requirement-status-unit-document-message.component';
import { SalesUnitRequirementByStatusIndentComponent } from './sales-unit-requirement-by-status-indent.component';

@Injectable()
export class SalesUnitRequirementResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequirement,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const salesUnitRequirementRoute: Routes = [
    {
        path: 'sales-unit-requirement',
        component: SalesUnitRequirementComponent,
        resolve: {
            'pagingParams': SalesUnitRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'indent',
        component: SalesUnitRequirementByStatusIndentComponent,
        resolve: {
            'pagingParams': SalesUnitRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.indent'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sales-unit-requirement/:id/list-document-message',
        component: SalesUnitRequirementStatusUnitDocumentMessageComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sales-unit-requirement/:id/check-availability',
        component: SalesUnitRequirementCheckAvailabilityComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sales-unit-requirement/:id/detail',
        component: SalesUnitRequirementDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const salesUnitRequirementPopupRoute: Routes = [
    {
        path: 'sales-unit-requirement-lov',
        component: SalesUnitRequirementLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sales-unit-requirement-new',
        component: SalesUnitRequirementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sales-unit-requirement/:id/edit',
        component: SalesUnitRequirementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sales-unit-requirement-popup-new/:idRequest',
        component: SalesUnitRequirementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sales-unit-requirement/:id/popup-edit',
        component: SalesUnitRequirementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
