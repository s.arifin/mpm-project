import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';
import { Subject } from 'rxjs/Subject';
import { SalesUnitRequirement } from './sales-unit-requirement.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { LeasingTenorProvide } from '../leasing-tenor-provide';
import { SaleTypeService } from '../sale-type';

import { PostalAddress } from '../postal-address';
import { Person } from '../person';
import * as BaseConstant from '../../shared/constants/base.constants';
import { CurrencyPipe } from '@angular/common';

import * as _ from 'lodash';
import * as moment from 'moment';
import { Organization } from '../organization';
import { createOrdersParameterOption } from '../orders/orders-parameter.util';
import { createSalesUnitRequirementParameterOption } from './sales-unit-requirement-parameter.util';

@Injectable()
export class SalesUnitRequirementService {
    protected itemValues: SalesUnitRequirement[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl = SERVER_API_URL + 'api/sales-unit-requirements';
    protected resourceApproveLeasingUrl = SERVER_API_URL + 'api/leasing-approval';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/sales-unit-requirements';
    private resourceCUrl = process.env.API_C_URL + '/api/ax_generalledger/';
    private resourceCUrl2 = process.env.API_C_URL + '/api/sales-unit-requirements/';

    public notSelectLeasingTenorProvideMessage: String = 'Please select down payment';

    constructor(
        protected http: Http,
        protected currencyPipe: CurrencyPipe,
        protected saleTypeService: SaleTypeService
    ) { }

    public getDataByStatusAndStatusApproval(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idinternal', req.idinternal);
        options.params.set('idstatusapproval', req.idstatusapproval);
        options.params.set('idstatustype', req.idstatustype);

        return this.http.get(this.resourceUrl + '/by-status-and-approval', options)
            .map((res: Response) => this.convertResponse(res));
    }

    public findByApprovalDiskonPelanggaranWilayahByProspect(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idprospect', req.idprospect);
        options.params.set('idstatustype', req.idstatustype);
        options.params.set('idinternal', req.idinternal);

        const url = this.resourceUrl + '/by-approval-diskon-and-pelanggaran-wilayah-by-prospect';
        return this.http.get(url, options)
            .map((res: Response) => this.convertResponse(res));
    }

    public findByApprovalDiskonPelanggaranWilayahBySales(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idstatustype', req.idstatustype);
        options.params.set('idinternal', req.idinternal);
        return this.http.get(this.resourceUrl + '/by-approval-diskon-and-pelanggaran-wilayah-by-sales', options)
            .map((res: Response) => this.convertResponse(res));
    }

    public findByRequestAndInternal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        const url = this.resourceUrl + '/by-internal-and-request';
        return this.http.get(url, options).map((res: Response) => this.convertResponse(res));
    }

    create(salesUnitRequirement: SalesUnitRequirement): Observable<SalesUnitRequirement> {
        const copy = this.convert(salesUnitRequirement);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(salesUnitRequirement: SalesUnitRequirement): Observable<SalesUnitRequirement> {
        const copy = this.convert(salesUnitRequirement);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    createGL(salesUnitRequirement: SalesUnitRequirement): Observable<any> {
        const copy = this.convert(salesUnitRequirement);
        return this.http.post(this.resourceCUrl + '/CreateJournal/?TranCode=103&IdInput=' + salesUnitRequirement.idRequirement, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<SalesUnitRequirement> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findC(id: any): Observable<SalesUnitRequirement> {
        return this.http.get(`${this.resourceCUrl2}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryApprovalLeasing(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceApproveLeasingUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryApprovalLeasingRequestAttachment(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceApproveLeasingUrl + '/request-attachment', options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(salesUnitRequirement: SalesUnitRequirement, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(salesUnitRequirement);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findEmail(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(SERVER_API_URL + 'api/sales-unit-requirements/email', options)
        .map((res: Response) => this.convertResponse(res));
    }

    queryByName(req?: any): Observable<ResponseWrapper> {
        const options = createSalesUnitRequirementParameterOption(req);
        return this.http.get(this.resourceUrl + '/byname', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, salesUnitRequirement: SalesUnitRequirement): Observable<SalesUnitRequirement> {
        const copy = this.convert(salesUnitRequirement);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeApproveMail(sur: SalesUnitRequirement): Observable<SalesUnitRequirement> {
        const copy = this.convert(sur);
        return this.http.post(SERVER_API_URL + 'api/sales-unit-requirements/approve/email', sur).map((res: Response) => {
            return res.json();
        });
    }

    executeNotApproveMail(sur: SalesUnitRequirement): Observable<SalesUnitRequirement> {
        const copy = this.convert(sur);
        return this.http.post(SERVER_API_URL + 'api/sales-unit-requirements/not-approve/email' , sur).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, salesUnitRequirements: SalesUnitRequirement[]): Observable<SalesUnitRequirement[]> {
        const copy = this.convertList(salesUnitRequirements);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    public findByApprovalDiskonPelanggaranWilayahByKorsal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        // options.params.set('idprospect', req.idprospect);
        options.params.set('idstatustype', req.idstatustype);
        options.params.set('idinternal', req.idinternal);

        const url = this.resourceUrl + '/by-approval-diskon-and-pelanggaran-wilayah-by-korsal';
        return this.http.get(url, options)
            .map((res: Response) => this.convertResponse(res));
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected ifNumberNull(value?: number): number {
        if (value) {
            return value;
        } else {
            return 0;
        }
    }

    public fnCountTotalSubsidi(salesUnitRequirement: SalesUnitRequirement): number {
        const subsahm = this.ifNumberNull(salesUnitRequirement.subsahm);
        const subsmd = this.ifNumberNull(salesUnitRequirement.subsmd);
        const subsown = this.ifNumberNull(salesUnitRequirement.subsown);
        const subsfincomp = this.ifNumberNull(salesUnitRequirement.subsfincomp);
        const makelarfee = this.ifNumberNull(salesUnitRequirement.brokerFee);
        return (subsahm + subsmd + subsown + subsfincomp + makelarfee);
    }

    public fnGetSelectedLeasingTenorProvide(collection: LeasingTenorProvide[], data: string): LeasingTenorProvide {
        let objLeasingTenorProvide: LeasingTenorProvide;
        objLeasingTenorProvide = new LeasingTenorProvide();

        objLeasingTenorProvide = _.find(collection, function(e) {
            return e.idLeasingProvide === data;
        });
        return objLeasingTenorProvide;
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convert(jsonResponse[i]));
        }
        this.pushItems(result);
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(salesUnitRequirement: SalesUnitRequirement): SalesUnitRequirement {
        if (salesUnitRequirement === null || salesUnitRequirement === {}) {
            return {};
        }
        // const copy: SalesUnitRequirement = Object.assign({}, salesUnitRequirement);
        const copy: SalesUnitRequirement = JSON.parse(JSON.stringify(salesUnitRequirement));
        return copy;
    }

    protected convertList(salesUnitRequirements: SalesUnitRequirement[]): SalesUnitRequirement[] {
        const copy: SalesUnitRequirement[] = salesUnitRequirements;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: SalesUnitRequirement[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    public optionYearAssembly(): Array<number> {
        const date = new Date();

        let years: Array<number>;
        years = new Array<number>();

        const startYear = date.getFullYear() - 5;

        for (let i = startYear; i <= (date.getFullYear() + 1 ); i++) {
            years.push(i);
        }

        return years;
    }

    public notCompleteData(sur: SalesUnitRequirement): string {
        let strContent: string;
        strContent = '';

        if (sur.productId === null) {
            strContent += '<li>Belum pilih motor</li>';
        }

        if (sur.colorId === null) {
            strContent += '<li>Belum pilih warna</li>';
        }

        if (sur.saleTypeId === null) {
            strContent += '<li>Belum pilih metode pembayaran</li>';
        } else {
            if (this.saleTypeService.checkIfCredit(sur.saleTypeId) && sur.leasingCompanyId === null) {
                strContent += '<li>Belum pilih leasing</li>';
            }

            if (this.saleTypeService.checkIfCredit(sur.saleTypeId) && sur.creditTenor === null) {
                strContent += '<li>Belum masukkan tenor</li>';
            }
            if (this.saleTypeService.checkIfCredit(sur.saleTypeId) && sur.creditDownPayment === null) {
                strContent += '<li>Belum masukkan down payment</li>';
            }
            if (this.saleTypeService.checkIfCredit(sur.saleTypeId) && sur.creditInstallment === null) {
                strContent += '<li>Belum masukkan cicilan</li>';
            }
        }

        // validasi Customer
        if (sur.personCustomer === null || sur.personCustomer === undefined) {
            sur.personCustomer = new Person;
        }

        strContent += this.validationCustomer(sur);

        // validasi STNK
        if (sur.personOwner === null || sur.personCustomer === undefined) {
            sur.personOwner = new Person;
        }

        strContent += this.validationSTNK(sur);

        if (strContent !== '') {
            strContent = '<ul>' + strContent + '</ul>';
            strContent = '<div>Daftar data yang belum lengkap: </div>' + strContent;
        }

        return strContent;
    }

    public validationCustomer(sur: SalesUnitRequirement): String {
        let a: String = '';

        if (this.saleTypeService.checkIfCredit(sur.saleTypeId)) {
            const person: Person = sur.personCustomer;
            if (this.emptyFirstName(person)) {
                a += '<li>Customer - Masukkan firstname</li>';
            }

            if (this.emptyNIK(person)) {
                a += '<li>Customer - Masukkan NIK</li>';
            }

            if (this.emptyDOB(person)) {
                a += '<li>Customer - Masukkan tanggal lahir</li>';
            }

            if (this.emptyNoHP(person)) {
                a += '<li>Customer - Masukkan nomor handphone</li>';
            }

            if (this.emptyAddress(person.postalAddress)) {
                a += '<li>Customer - Masukkan alamat</li>';
            }
        }

        return a;
    }

    public validationSTNK(sur: SalesUnitRequirement): String {
        let a: String = '';

        if (sur.waitStnk) {

        } else {
            const postalAddress: PostalAddress = sur.personOwner.postalAddress;
            const person: Person = sur.personOwner;
            if (this.saleTypeService.checkIfCredit(sur.saleTypeId)) {
                if (this.emptyCity(postalAddress)) {
                    a += '<li>STNK - Pilih Kota</li>';
                }

                if (this.emptyDOB(person)) {
                    a += '<li>STNK - Pilih tanggal lahir</li>';
                }

                if (this.emptyPhoneNumber(person)) {
                    a += '<li>STNK - Masukkan no HP / no Telp</li>';
                }

                if (this.emptyNIK(person)) {
                    a += '<li>STNK - Masukkan no KTP</li>';
                }

                // if (this.emptyNoTelp(person)) {
                //     a += '<li>STNK - Masukkan no telp</li>';
                // }

                // if (this.emptyNPWP(person)) {
               //      a += '<li>STNK - Masukkan NPWP</li>';
               // }

                if (this.emptyReligion(person)) {
                    a += '<li>STNK - pilih agama</li>'
                }
            } else {
                if (this.emptyCity(postalAddress)) {
                    a += '<li>STNK - Pilih Kota</li>';
                }
            }
        }

        return a;
    }

    public emptyAddress(postalAddress: PostalAddress): boolean {
        const address: string = postalAddress.address1;

        let a: boolean;
        a = false;

        if (address === null || address === '' || address === undefined) {
            a = true;
        }

        return a;
    }

    public emptyFirstName(person: Person): boolean {
        const firstName: string = person.firstName;

        let a: boolean;
        a = false;

        if (firstName === null || firstName === '' || firstName === undefined) {
            a = true;
        }

        return a;
    }

    public emptyReligion(person: Person): boolean {
        const religi: string = person.religionTypeId;

        let a: boolean;
        a = false;

        if (religi === null || religi === '' || religi === undefined) {
            a = true;
        }

        return a;
    }

    public emptyNPWP(person: Person ): boolean {
        const npwp: string = person.taxIdNumber;

        let a: boolean;
        a = false;

        if (npwp === null || npwp === '' || npwp === undefined) {
            a = true;
        }

        return a;
    }

    public emptyNoTelp(person: Person ): boolean {
        const noTelp: string = person.phone;

        let a: boolean;
        a = false;

        if (noTelp === null || noTelp === '' || noTelp === undefined) {
            a = true;
        }

        return a;
    }

    public emptyPhoneNumber(person: Person): boolean {
        const postalAddress: PostalAddress = person.postalAddress;

        let a: boolean;
        a = false;
        if (this.emptyNoHP(person) && this.emptyNoTelp(postalAddress)) {
            a = true;
        }

        return a;
    }

    public emptyCity(postalAddress: PostalAddress): boolean {
        const cityId: string = postalAddress.cityId;

        let a: boolean;
        a = false;

        if (cityId === null || cityId === '' || cityId === undefined) {
            a = true;
        }

        return a;
    }

    public emptyDOB(person: Person): boolean {
        const tglLahir: string = person.dob;

        let a: boolean;
        a = false;

        if (tglLahir === null || tglLahir === '' || tglLahir === undefined) {
            a = true;
        }

        return a;
    }

    public emptyNoHP(person: Person): boolean {
        const hpSatu: string = person.cellPhone1;
       // const hpDua: string = person.cellPhone2;

        let a: boolean;
        a = false;

       // if (hpSatu === null || hpSatu === '' || hpDua === null || hpDua === '' || hpSatu === undefined || hpDua === undefined) {
            if (hpSatu === null || hpSatu === '' || hpSatu === undefined ) {
            a = true;
        }

        return a;
    }

    public emptyNIK(person: Person): boolean {
        const NIK: string = person.personalIdNumber;

        let a: boolean;
        a = false;

        if (NIK === null || NIK === '' || NIK === undefined) {
            a = true;
        }

        return a;
    }

    public emptyOrganizationName(organization: Organization): Boolean {
        const name: string = organization.name;

        let a: Boolean = false;
        if (name === null || name === '' || name === undefined) {
            a = true;
        }

        return a;
    }

    public emptyOrganizationPhone(organization: Organization): Boolean {
        const phone: String = organization.officePhone;

        let a: Boolean = false;
        if (phone === null || phone === '' || phone === undefined) {
            a = true;
        }

        return a;
    }

    public countRemainingDownPaymentForCredit(sur: SalesUnitRequirement, totalReceipt: number): string {
        let a: string;
        a = '';

        const productPrice = sur.unitPrice;
        if (productPrice !== null) {
            const totalSubsidi = this.fnCountTotalSubsidi(sur);
            let creditDownPayment: number = sur.creditDownPayment;
            if (creditDownPayment === null || creditDownPayment === undefined) {
                creditDownPayment = 0;
            }

            const totalPaymented = creditDownPayment - (totalReceipt + totalSubsidi);
            a = this.currencyPipe.transform(totalPaymented , 'IDR', true);
        } else {
            a = 'Please select motor';
        }

        return a;
    }

    public generateLabelApproval(sur: SalesUnitRequirement): string {
        let a: string;
        a = '<ul>';

        if (sur.approvalDiscount === BaseConstant.Status.STATUS_NOT_REQUIRED) {
            a += '<li>approval diskon tidak diperlukan</li>';
        } else if (sur.approvalDiscount === BaseConstant.Status.STATUS_APPROVED) {
            a += '<li>approval diskon sudah di approve</li>';
        } else if (sur.approvalDiscount === BaseConstant.Status.STATUS_REJECTED) {
            a += '<li>approval diskon tidak di approve</li>';
        } else if (sur.approvalDiscount === BaseConstant.Status.STATUS_WAITING_FOR_APPROVAL) {
            a += '<li>approval diskon menunggu untuk di approve</li>';
        }else {
            a += '<li>approval diskon terjadi error</li>';
        }

        if (sur.approvalTerritorial === BaseConstant.Status.STATUS_NOT_REQUIRED) {
            a += '<li>approval wilayah tidak diperlukan</li>';
        } else if (sur.approvalTerritorial === BaseConstant.Status.STATUS_APPROVED) {
            a += '<li>approval wilayah sudah di approve</li>';
        } else if (sur.approvalTerritorial === BaseConstant.Status.STATUS_REJECTED) {
            a += '<li>approval wilayah tidak di approve</li>';
        } else if (sur.approvalTerritorial === BaseConstant.Status.STATUS_WAITING_FOR_APPROVAL) {
            a += '<li>approval wilayah menunggu untuk di approve</li>';
        }else {
            a += '<li>approval wilayah terjadi error</li>';
        }

        if (sur.approvalLeasing === BaseConstant.Status.STATUS_REJECTED) {
            a += '<li>Not Approve Leasing = </li>' + sur.leasingnote;
        }
        a += '</ul>';

        return a;
    }

    public generateLabelApprovalLeasing(sur: SalesUnitRequirement): string {
        let a: string;
        let b: string;
        a = '<ul>';

        if (sur.approvalLeasing === BaseConstant.Status.STATUS_REJECTED) {
            if (sur.leasingnote === null || sur.leasingnote === '' || sur.leasingnote === undefined) {
                a += '<li>Note = </li>';
            }else {
                b = sur.leasingnote;
                a += '<li>Note = </li>' + b;
            }
        }
        a += '</ul>';

        return a;
    }

    public checkIfOrganization(idRequestType: number): object {
        let a: object;
        a = {
            customer : 'organization',
            stnk : 'organization'
        };

        if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP) {
            a = {
                customer : 'organization',
                stnk : 'organization'
            };
        } else if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP_REKANAN) {
            a = {
                customer : 'organization',
                stnk : 'organization'
            };
        } else if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP_INTERNAL) {
            a = {
                customer : 'organization',
                stnk : 'organization'
            };
        } else if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP_PERSON) {
            a = {
                customer : 'organization',
                stnk : 'person'
            };
        } else if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_DIRECT) {
            a = {
                customer : 'person',
                stnk : 'person'
            }
        }

        return a;
    }

    public checkCustomer(idRequestType: number): string {
        let a: string;
        a = null;

        const rs: object = this.checkIfOrganization(idRequestType);
        a = rs['customer'];

        return a;
    }

    public checkSTNK(idRequestType: number): string {
        let a: string;
        a = null;

        const rs: object = this.checkIfOrganization(idRequestType);
        a = rs['stnk'];

        return a;
    }
    // process(params?: any, req?: any): Observable<any> {
    //     const options = createRequestOption(req);
    //     return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
    //         return res.json();
    //     });
    // }

    queryApprovalLeasingFilter(req?: any): Observable<ResponseWrapper> {
        const options = createOrdersParameterOption(req);
        return this.http.get(this.resourceUrl + '/filter-approval-leasing', options)
            .map((res: Response) => this.convertResponse(res));
    }
}
