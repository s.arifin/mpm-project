import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {BookingSlotStandard} from './booking-slot-standard.model';
import {BookingSlotStandardPopupService} from './booking-slot-standard-popup.service';
import {BookingSlotStandardService} from './booking-slot-standard.service';
import {ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-booking-slot-standard-dialog',
    templateUrl: './booking-slot-standard-dialog.component.html'
})
export class BookingSlotStandardDialogComponent implements OnInit {

    bookingSlotStandard: BookingSlotStandard;
    isSaving: boolean;

    internals: Internal[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected bookingSlotStandardService: BookingSlotStandardService,
        protected internalService: InternalService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.bookingSlotStandard.idBookSlotStd !== undefined) {
            this.subscribeToSaveResponse(
                this.bookingSlotStandardService.update(this.bookingSlotStandard));
        } else {
            this.subscribeToSaveResponse(
                this.bookingSlotStandardService.create(this.bookingSlotStandard));
        }
    }

    protected subscribeToSaveResponse(result: Observable<BookingSlotStandard>) {
        result.subscribe((res: BookingSlotStandard) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: BookingSlotStandard) {
        this.eventManager.broadcast({ name: 'bookingSlotStandardListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'bookingSlotStandard saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'bookingSlotStandard Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }
}

@Component({
    selector: 'jhi-booking-slot-standard-popup',
    template: ''
})
export class BookingSlotStandardPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected bookingSlotStandardPopupService: BookingSlotStandardPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.bookingSlotStandardPopupService
                    .open(BookingSlotStandardDialogComponent as Component, params['id']);
            } else {
                this.bookingSlotStandardPopupService
                    .open(BookingSlotStandardDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
