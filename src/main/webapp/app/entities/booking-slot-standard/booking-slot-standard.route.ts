import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { BookingSlotStandardComponent } from './booking-slot-standard.component';
import { BookingSlotStandardPopupComponent } from './booking-slot-standard-dialog.component';

@Injectable()
export class BookingSlotStandardResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'internal.idInternal,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const bookingSlotStandardRoute: Routes = [
    {
        path: 'booking-slot-standard',
        component: BookingSlotStandardComponent,
        resolve: {
            'pagingParams': BookingSlotStandardResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.bookingSlotStandard.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const bookingSlotStandardPopupRoute: Routes = [
    {
        path: 'booking-slot-standard-new',
        component: BookingSlotStandardPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.bookingSlotStandard.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'booking-slot-standard/:id/edit',
        component: BookingSlotStandardPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.bookingSlotStandard.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
