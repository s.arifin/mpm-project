import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BookingSlotStandard } from './booking-slot-standard.model';
import { BookingSlotStandardService } from './booking-slot-standard.service';

@Injectable()
export class BookingSlotStandardPopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected bookingSlotStandardService: BookingSlotStandardService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.bookingSlotStandardService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.bookingSlotStandardModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new BookingSlotStandard();
                    this.ngbModalRef = this.bookingSlotStandardModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    bookingSlotStandardModalRef(component: Component, bookingSlotStandard: BookingSlotStandard): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.bookingSlotStandard = bookingSlotStandard;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
