import { BaseEntity } from './../../shared';

export class BookingSlotStandard implements BaseEntity {
    constructor(
        public id?: any,
        public idBookSlotStd?: any,
        public description?: string,
        public capacity?: number,
        public startHour?: number,
        public startMinute?: number,
        public endHour?: number,
        public endMinute?: number,
        public internalId?: any,
    ) {
    }
}
