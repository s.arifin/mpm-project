import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { BookingSlotStandard } from './booking-slot-standard.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class BookingSlotStandardService {

    protected resourceUrl = 'api/booking-slot-standards';
    protected resourceSearchUrl = 'api/_search/booking-slot-standards';

    constructor(protected http: Http) { }

    create(bookingSlotStandard: BookingSlotStandard): Observable<BookingSlotStandard> {
        const copy = this.convert(bookingSlotStandard);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(bookingSlotStandard: BookingSlotStandard): Observable<BookingSlotStandard> {
        const copy = this.convert(bookingSlotStandard);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<BookingSlotStandard> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(bookingSlotStandard: any, id: Number): Observable<String> {
        const copy = this.convert(bookingSlotStandard);
        return this.http.post(`${this.resourceUrl}/execute/${id}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(bookingSlotStandard: BookingSlotStandard): BookingSlotStandard {
        const copy: BookingSlotStandard = Object.assign({}, bookingSlotStandard);
        return copy;
    }
}
