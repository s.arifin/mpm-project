import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { WorkOrder } from './work-order.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class WorkOrderService {

    private resourceUrl = 'api/work-orders';
    private resourceSearchUrl = 'api/_search/work-orders';

    constructor(private http: Http) { }

    create(workOrder: WorkOrder): Observable<WorkOrder> {
        const copy = this.convert(workOrder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(workOrder: WorkOrder): Observable<WorkOrder> {
        const copy = this.convert(workOrder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<WorkOrder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(workOrder: any, id: Number): Observable<String> {
        const copy = this.convert(workOrder);
        return this.http.post(`${this.resourceUrl}/execute/${id}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(workOrder: WorkOrder): WorkOrder {
        const copy: WorkOrder = Object.assign({}, workOrder);
        return copy;
    }
}
