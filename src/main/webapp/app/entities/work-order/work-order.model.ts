import { BaseEntity } from './../../shared';

export class WorkOrder implements BaseEntity {
    constructor(
        public id?: any,
        public idRequirement?: any,
        public vehicleNumber?: string,
        public customerId?: any,
        public mechanicId?: any,
        public vehicleId?: any,
    ) {
    }
}
