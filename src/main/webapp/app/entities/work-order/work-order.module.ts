import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    WorkOrderService,
    WorkOrderPopupService,
    WorkOrderComponent,
    WorkOrderDetailComponent,
    WorkOrderDialogComponent,
    WorkOrderPopupComponent,
    workOrderRoute,
    workOrderPopupRoute,
    WorkOrderResolvePagingParams,
    WorkOrderEditComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
         CheckboxModule,
         RadioButtonModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule
        } from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...workOrderRoute,
    ...workOrderPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,

        CheckboxModule,
        RadioButtonModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule
    ],
    exports: [
        WorkOrderComponent,
        WorkOrderDetailComponent,
        WorkOrderEditComponent
    ],
    declarations: [
        WorkOrderComponent,
        WorkOrderDetailComponent,
        WorkOrderDialogComponent,
        WorkOrderPopupComponent,
        WorkOrderEditComponent
    ],
    entryComponents: [
        WorkOrderComponent,
        WorkOrderDialogComponent,
        WorkOrderPopupComponent,
        WorkOrderEditComponent
    ],
    providers: [
        WorkOrderService,
        WorkOrderPopupService,
        WorkOrderResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmWorkOrderModule {}
