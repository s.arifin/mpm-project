import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {WorkOrder} from './work-order.model';
import {WorkOrderPopupService} from './work-order-popup.service';
import {WorkOrderService} from './work-order.service';
import {ToasterService} from '../../shared';
import { Customer, CustomerService } from '../customer';
import { Mechanic, MechanicService } from '../mechanic';
import { VehicleIdentification, VehicleIdentificationService } from '../vehicle-identification';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-work-order-dialog',
    templateUrl: './work-order-dialog.component.html'
})
export class WorkOrderDialogComponent implements OnInit {

    workOrder: WorkOrder;
    isSaving: boolean;

    customers: Customer[];

    mechanics: Mechanic[];

    vehicleidentifications: VehicleIdentification[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private workOrderService: WorkOrderService,
        private customerService: CustomerService,
        private mechanicService: MechanicService,
        private vehicleIdentificationService: VehicleIdentificationService,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.mechanicService.query()
            .subscribe((res: ResponseWrapper) => { this.mechanics = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.vehicleIdentificationService.query()
            .subscribe((res: ResponseWrapper) => { this.vehicleidentifications = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.workOrder.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.workOrderService.update(this.workOrder));
        } else {
            this.subscribeToSaveResponse(
                this.workOrderService.create(this.workOrder));
        }
    }

    private subscribeToSaveResponse(result: Observable<WorkOrder>) {
        result.subscribe((res: WorkOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: WorkOrder) {
        this.eventManager.broadcast({ name: 'workOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'workOrder saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'workOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }

    trackMechanicById(index: number, item: Mechanic) {
        return item.idPartyRole;
    }

    trackVehicleIdentificationById(index: number, item: VehicleIdentification) {
        return item.idVehicleIdentification;
    }
}

@Component({
    selector: 'jhi-work-order-popup',
    template: ''
})
export class WorkOrderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private workOrderPopupService: WorkOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.workOrderPopupService
                    .open(WorkOrderDialogComponent as Component, params['id']);
            } else {
                this.workOrderPopupService
                    .open(WorkOrderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
