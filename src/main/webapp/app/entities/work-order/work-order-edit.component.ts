import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Subscription} from 'rxjs/Rx';
import {Observable} from 'rxjs/Rx';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {WorkOrder} from './work-order.model';
import {WorkOrderPopupService} from './work-order-popup.service';
import {WorkOrderService} from './work-order.service';
import {ToasterService} from '../../shared';
import { Customer, CustomerService } from '../customer';
import { Mechanic, MechanicService } from '../mechanic';
import { VehicleIdentification, VehicleIdentificationService } from '../vehicle-identification';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-work-order-edit',
    templateUrl: './work-order-edit.component.html'
})
export class WorkOrderEditComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    workOrder: WorkOrder;
    isSaving: boolean;

    customers: Customer[];

    mechanics: Mechanic[];

    vehicleidentifications: VehicleIdentification[];

    newCustomer: Boolean = true;
    workOrders: WorkOrder[];
    itemsPerPage: any;
    totalItems: any;
    selectedValue: any;
    selectedValues: any;
    loadDataLazy(event) {}
    updateRowData(event) {}

    constructor(
        private alertService: JhiAlertService,
        private workOrderService: WorkOrderService,
        private customerService: CustomerService,
        private mechanicService: MechanicService,
        private vehicleIdentificationService: VehicleIdentificationService,
        private route: ActivatedRoute,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
        this.workOrder = new WorkOrder();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.mechanicService.query()
            .subscribe((res: ResponseWrapper) => { this.mechanics = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.vehicleIdentificationService.query()
            .subscribe((res: ResponseWrapper) => { this.vehicleidentifications = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.workOrderService.find(id).subscribe((workOrder) => {
            this.workOrder = workOrder;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.workOrder.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.workOrderService.update(this.workOrder));
        } else {
            this.subscribeToSaveResponse(
                this.workOrderService.create(this.workOrder));
        }
    }

    private subscribeToSaveResponse(result: Observable<WorkOrder>) {
        result.subscribe((res: WorkOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: WorkOrder) {
        this.eventManager.broadcast({ name: 'workOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'workOrder saved !');
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'workOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }

    trackMechanicById(index: number, item: Mechanic) {
        return item.idPartyRole;
    }

    trackVehicleIdentificationById(index: number, item: VehicleIdentification) {
        return item.idVehicleIdentification;
    }
}
