import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { SalesBooking } from './sales-booking.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import { SalesUnitRequirement } from '../sales-unit-requirement';

import * as moment from 'moment';

@Injectable()
export class SalesBookingService {
    protected itemValues: Array<SalesBooking>;
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/sales-bookings';
    protected resourceSearchUrl = 'api/_search/sales-bookings';

    constructor(protected http: Http) { }

    public checkAvailability(salesBooking: SalesBooking, idInternal?: string): Observable<ResponseWrapper> {
        const year = salesBooking.yearAssembly;
        const httpUrl = this.resourceUrl + '/check-availability/' + idInternal + '/' + salesBooking.idProduct  + '/' + salesBooking.idFeature + '/' + year;

        return this.http.get(httpUrl).map((res: Response) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    public create(salesBooking: SalesBooking): Observable<SalesBooking> {
        const copy = this.convert(salesBooking);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateIntroduction) {
            entity.dateIntroduction = new Date(entity.dateIntroduction);
        }
    }

    protected convert(salesBooking: SalesBooking): SalesBooking {
        if (salesBooking === null || salesBooking === {}) {
            return {};
        }

        const copy: SalesBooking = Object.assign({}, salesBooking);

        // copy.dateIntroduction = this.dateUtils.toDate(motor.dateIntroduction);
        return copy;
    }
}
