import { BaseEntity } from './../shared-component';

export class SalesBooking implements BaseEntity {
    public id?: any;
    public idInternal?: string;
    public requirementId?: string;
    public idProduct?: string;
    public idFeature?: number;
    public dtFrom?: Date;
    public dtThru?: Date;
    public note?: string;
    public yearAssembly?: number;
    public onHand?: boolean;
    public inTransit?: boolean;
}
