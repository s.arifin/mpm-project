import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { SalesBookingService } from './';

@NgModule({
    providers: [
        SalesBookingService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmSalesBookingModule {}
