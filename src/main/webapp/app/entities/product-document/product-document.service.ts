import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';

import { ProductDocument } from './product-document.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ProductDocumentService {
    protected itemValues: ProductDocument[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/product-documents';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/product-documents';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(productDocument: ProductDocument): Observable<ProductDocument> {
        const copy = this.convert(productDocument);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(productDocument: ProductDocument): Observable<ProductDocument> {
        const copy = this.convert(productDocument);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<ProductDocument> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findByIdProduct(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idProduct', req.idProduct);
        return this.http.get(this.resourceUrl + '/by-product', options)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, productDocument: ProductDocument): Observable<ProductDocument> {
        const copy = this.convert(productDocument);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, productDocuments: ProductDocument[]): Observable<ProductDocument[]> {
        const copy = this.convertList(productDocuments);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        console.log('res', res);
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateCreate) {
            entity.dateCreate = new Date(entity.dateCreate);
        }
    }

    protected convert(productDocument: ProductDocument): ProductDocument {
        if (productDocument === null || productDocument === {}) {
            return {};
        }
        // const copy: ProductDocument = Object.assign({}, productDocument);
        const copy: ProductDocument = JSON.parse(JSON.stringify(productDocument));

        // copy.dateCreate = this.dateUtils.toDate(productDocument.dateCreate);
        return copy;
    }

    protected convertList(productDocuments: ProductDocument[]): ProductDocument[] {
        const copy: ProductDocument[] = productDocuments;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ProductDocument[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
