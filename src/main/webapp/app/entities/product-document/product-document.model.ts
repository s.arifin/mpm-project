import { BaseEntity } from './../../shared';

import { Documents } from '../documents/documents.model';

export class ProductDocument extends Documents {
    constructor(
        public contentContentType?: string,
        public content?: any,
        public note?: string,
        public productId?: any,
    ) {
        super();
    }
}
