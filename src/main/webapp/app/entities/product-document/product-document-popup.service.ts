import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { ProductDocument } from './product-document.model';
import { ProductDocumentService } from './product-document.service';

@Injectable()
export class ProductDocumentPopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected productDocumentService: ProductDocumentService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, idProduct?: number, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.productDocumentService.find(id).subscribe((data) => {
                    // if (data.dateCreate) {
                    //    data.dateCreate = this.datePipe
                    //        .transform(data.dateCreate, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.productDocumentModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else if (idProduct) {
                setTimeout(
                    () => {
                        const data = new ProductDocument();
                        data.productId = idProduct;
                        this.ngbModalRef = this.productDocumentModalRef(component, data);
                        resolve(this.ngbModalRef);
                    }, 0
                );
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new ProductDocument();
                    this.ngbModalRef = this.productDocumentModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    productDocumentModalRef(component: Component, productDocument: ProductDocument): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.productDocument = productDocument;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.productDocumentLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    productDocumentLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
