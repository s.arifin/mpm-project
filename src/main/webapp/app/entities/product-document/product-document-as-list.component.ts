import {Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges} from '@angular/core';
import {Response} from '@angular/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Rx';
import {JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import {ProductDocument} from './product-document.model';
import {ProductDocumentService} from './product-document.service';
import {ITEMS_PER_PAGE, Principal, ResponseWrapper} from '../../shared';
import {PaginationConfig} from '../../blocks/config/uib-pagination.config';

import {LazyLoadEvent} from 'primeng/primeng';
import {ToasterService} from '../../shared/alert/toaster.service';
import {ConfirmationService} from 'primeng/primeng';

@Component({
    selector: 'jhi-product-document-as-list',
    templateUrl: './product-document-as-list.component.html'
})
export class ProductDocumentAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() filterBy: any;

    @Input()
    idProduct: string;

    readonly: boolean;
    currentAccount: any;
    productDocuments: ProductDocument[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        protected productDocumentService: ProductDocumentService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected dataUtils: JhiDataUtils,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idDocument';
        this.reverse = 'asc';
    }

    loadAll() {
        this.productDocumentService.findByIdProduct({
            idProduct: this.idProduct,
            query: this.filterBy,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/product-document'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        if (this.idProduct !== undefined) {
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/product-document', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idProduct']) {
            if (this.idProduct !== undefined) {
                this.loadAll();
                this.registerChangeInProductDocuments();
            }
        }
    }

    ngOnDestroy() {

    }

    trackId(index: number, item: ProductDocument) {
        return item.idDocument;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    registerChangeInProductDocuments() {
        this.eventSubscriber = this.eventManager.subscribe('productDocumentListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idDocument') {
            result.push('idDocument');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.productDocuments = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.productDocumentService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }

        if (this.idProduct !== undefined) {
            this.loadAll();
        }
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.productDocumentService.update(event.data)
                .subscribe((res: ProductDocument) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.productDocumentService.create(event.data)
                .subscribe((res: ProductDocument) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: ProductDocument) {
        this.toasterService.showToaster('info', 'ProductDocument Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    public delete(id: any, idx: number): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.productDocumentService.delete(id).subscribe(
                    (response) => {
                        this.eventManager.broadcast({
                            name: 'productDocumentListModification',
                            content: 'Deleted an Product Document'
                        });
                        // this.loadAll();
                    },
                    (res) => {
                        this.onError(res);
                    }
                );
            }
        });
    }
}
