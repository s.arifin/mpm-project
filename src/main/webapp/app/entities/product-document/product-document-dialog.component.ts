import {Component, OnInit, OnDestroy, ElementRef} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService, JhiDataUtils} from 'ng-jhipster';

import {ProductDocument} from './product-document.model';
import {ProductDocumentPopupService} from './product-document-popup.service';
import {ProductDocumentService} from './product-document.service';
import {ToasterService} from '../../shared';
import { Product, ProductService } from '../product';
import { DocumentType, DocumentTypeService } from '../document-type';
import { ResponseWrapper } from '../../shared';

import * as DocumentTypeConstants from '../../shared/constants/document-type.constants';

@Component({
    selector: 'jhi-product-document-dialog',
    templateUrl: './product-document-dialog.component.html'
})
export class ProductDocumentDialogComponent implements OnInit {

    productDocument: ProductDocument;
    isSaving: boolean;

    products: Product[];

    documenttypes: DocumentType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected dataUtils: JhiDataUtils,
        protected alertService: JhiAlertService,
        protected productDocumentService: ProductDocumentService,
        protected productService: ProductService,
        protected documentTypeService: DocumentTypeService,
        protected elementRef: ElementRef,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.productDocument.dateCreate = new Date();

        this.isSaving = false;
        this.documentTypeService.findByIdParent(DocumentTypeConstants.PRODUCT)
            .subscribe(
                (res: ResponseWrapper) => {
                    console.log('res', res);
                    this.documenttypes = res.json;
                },
            (res: ResponseWrapper) => this.onError(res.json));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.productDocument, this.elementRef, field, fieldContentType, idInput);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.productDocument.idDocument !== undefined) {
            this.subscribeToSaveResponse(
                this.productDocumentService.update(this.productDocument));
        } else {
            this.subscribeToSaveResponse(
                this.productDocumentService.create(this.productDocument));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProductDocument>) {
        result.subscribe((res: ProductDocument) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ProductDocument) {
        this.eventManager.broadcast({ name: 'productDocumentListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'productDocument saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'productDocument Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackProductById(index: number, item: Product) {
        return item.idProduct;
    }

    trackDocumentTypeById(index: number, item: DocumentType) {
        return item.idDocumentType;
    }
}

@Component({
    selector: 'jhi-product-document-popup',
    template: ''
})
export class ProductDocumentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected productDocumentPopupService: ProductDocumentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.productDocumentPopupService
                    .open(ProductDocumentDialogComponent as Component, null, params['id']);
            } else if (params['idproduct']) {
                this.productDocumentPopupService
                    .open(ProductDocumentDialogComponent as Component, params['idproduct'], null);
            } else {
                this.productDocumentPopupService
                    .open(ProductDocumentDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
