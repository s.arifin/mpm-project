import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {QuoteItem} from './quote-item.model';
import {QuoteItemPopupService} from './quote-item-popup.service';
import {QuoteItemService} from './quote-item.service';
import {ToasterService} from '../../shared';
import { Quote, QuoteService } from '../quote';
import { Product, ProductService } from '../product';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-quote-item-dialog',
    templateUrl: './quote-item-dialog.component.html'
})
export class QuoteItemDialogComponent implements OnInit {

    quoteItem: QuoteItem;
    isSaving: boolean;

    quotes: Quote[];

    products: Product[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected quoteItemService: QuoteItemService,
        protected quoteService: QuoteService,
        protected productService: ProductService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.quoteService.query()
            .subscribe((res: ResponseWrapper) => { this.quotes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.quoteItem.idQuoteItem !== undefined) {
            this.subscribeToSaveResponse(
                this.quoteItemService.update(this.quoteItem));
        } else {
            this.subscribeToSaveResponse(
                this.quoteItemService.create(this.quoteItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<QuoteItem>) {
        result.subscribe((res: QuoteItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: QuoteItem) {
        this.eventManager.broadcast({ name: 'quoteItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'quoteItem saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'quoteItem Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackQuoteById(index: number, item: Quote) {
        return item.idQuote;
    }

    trackProductById(index: number, item: Product) {
        return item.idProduct;
    }
}

@Component({
    selector: 'jhi-quote-item-popup',
    template: ''
})
export class QuoteItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected quoteItemPopupService: QuoteItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.quoteItemPopupService
                    .open(QuoteItemDialogComponent as Component, params['id']);
            } else {
                this.quoteItemPopupService
                    .open(QuoteItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
