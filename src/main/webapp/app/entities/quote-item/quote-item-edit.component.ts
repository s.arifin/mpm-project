import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { QuoteItem } from './quote-item.model';
import { QuoteItemService } from './quote-item.service';
import { ToasterService} from '../../shared';
import { Quote, QuoteService } from '../quote';
import { Product, ProductService } from '../product';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-quote-item-edit',
    templateUrl: './quote-item-edit.component.html'
})
export class QuoteItemEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    quoteItem: QuoteItem;
    isSaving: boolean;

    quotes: Quote[];

    products: Product[];

    constructor(
        protected alertService: JhiAlertService,
        protected quoteItemService: QuoteItemService,
        protected quoteService: QuoteService,
        protected productService: ProductService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.quoteItem = new QuoteItem();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.quoteService.query()
            .subscribe((res: ResponseWrapper) => { this.quotes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.quoteItemService.find(id).subscribe((quoteItem) => {
            this.quoteItem = quoteItem;
        });
    }

    previousState() {
        this.router.navigate(['quote-item']);
    }

    save() {
        this.isSaving = true;
        if (this.quoteItem.idQuoteItem !== undefined) {
            this.subscribeToSaveResponse(
                this.quoteItemService.update(this.quoteItem));
        } else {
            this.subscribeToSaveResponse(
                this.quoteItemService.create(this.quoteItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<QuoteItem>) {
        result.subscribe((res: QuoteItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: QuoteItem) {
        this.eventManager.broadcast({ name: 'quoteItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'quoteItem saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'quoteItem Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackQuoteById(index: number, item: Quote) {
        return item.idQuote;
    }

    trackProductById(index: number, item: Product) {
        return item.idProduct;
    }
}
