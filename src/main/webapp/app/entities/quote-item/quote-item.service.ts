import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { QuoteItem } from './quote-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class QuoteItemService {
    protected itemValues: QuoteItem[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/quote-items';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/quote-items';

    constructor(protected http: Http) { }

    create(quoteItem: QuoteItem): Observable<QuoteItem> {
        const copy = this.convert(quoteItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(quoteItem: QuoteItem): Observable<QuoteItem> {
        const copy = this.convert(quoteItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<QuoteItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, quoteItem: QuoteItem): Observable<QuoteItem> {
        const copy = this.convert(quoteItem);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, quoteItems: QuoteItem[]): Observable<QuoteItem[]> {
        const copy = this.convertList(quoteItems);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(quoteItem: QuoteItem): QuoteItem {
        if (quoteItem === null || quoteItem === {}) {
            return {};
        }
        // const copy: QuoteItem = Object.assign({}, quoteItem);
        const copy: QuoteItem = JSON.parse(JSON.stringify(quoteItem));
        return copy;
    }

    protected convertList(quoteItems: QuoteItem[]): QuoteItem[] {
        const copy: QuoteItem[] = quoteItems;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: QuoteItem[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
