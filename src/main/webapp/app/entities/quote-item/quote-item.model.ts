import { BaseEntity } from './../../shared';

export class QuoteItem implements BaseEntity {
    constructor(
        public id?: any,
        public idQuoteItem?: any,
        public unitPrice?: number,
        public qty?: number,
        public quoteId?: any,
        public productId?: any,
    ) {
    }
}
