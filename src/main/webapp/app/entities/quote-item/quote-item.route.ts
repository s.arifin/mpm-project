import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { QuoteItemComponent } from './quote-item.component';
import { QuoteItemEditComponent } from './quote-item-edit.component';
import { QuoteItemPopupComponent } from './quote-item-dialog.component';

@Injectable()
export class QuoteItemResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idQuoteItem,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const quoteItemRoute: Routes = [
    {
        path: 'quote-item',
        component: QuoteItemComponent,
        resolve: {
            'pagingParams': QuoteItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.quoteItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const quoteItemPopupRoute: Routes = [
    {
        path: 'quote-item-new',
        component: QuoteItemEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.quoteItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'quote-item/:id/edit',
        component: QuoteItemEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.quoteItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'quote-item-popup-new',
        component: QuoteItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.quoteItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'quote-item/:id/popup-edit',
        component: QuoteItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.quoteItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
