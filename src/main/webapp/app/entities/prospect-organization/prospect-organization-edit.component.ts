import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { PartyCategory } from '../party-category/party-category.model';
import { PartyCategoryService } from '../party-category/party-category.service';
import { ProspectOrganization } from './prospect-organization.model';
import { ProspectOrganizationService } from './prospect-organization.service';
import { ToasterService} from '../../shared';
import { Organization, OrganizationService } from '../organization';
import { Internal, InternalService } from '../internal';
import { SalesBroker, SalesBrokerService } from '../sales-broker';
import { ProspectSource, ProspectSourceService } from '../prospect-source';
import { EventType, EventTypeService } from '../event-type';
import { Facility, FacilityService } from '../facility';
import { Salesman, SalesmanService } from '../salesman';
import { Suspect, SuspectService } from '../suspect';
import {QuoteItemEditComponent, QuoteItemService} from '../quote-item'

import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-prospect-organization-edit',
    templateUrl: './prospect-organization-edit.component.html'
})
export class ProspectOrganizationEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    prospectOrganization: ProspectOrganization;
    isSaving: boolean;
    // featureService: FeatureService;
    idProspect: string = null;

    organizations: Organization[];
    internals: Internal[];
    salesbrokers: SalesBroker[];
    prospectsources: ProspectSource[];
    eventtypes: EventType[];
    facilities: Facility[];
    salesmen: Salesman[];
    customertype: string;
    index = 0;
    partycategory: PartyCategory;
    PROSPECT_EVENT_TYPE = 1;

    constructor(
        protected alertService: JhiAlertService,
        protected prospectOrganizationService: ProspectOrganizationService,
        protected organizationService: OrganizationService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected internalService: InternalService,
        protected salesBrokerService: SalesBrokerService,
        protected prospectSourceService: ProspectSourceService,
        protected eventTypeService: EventTypeService,
        protected facilityService: FacilityService,
        protected salesmanService: SalesmanService,
        protected suspectService: SuspectService,
        protected partyCategoryService: PartyCategoryService
    ) {
        this.prospectOrganization = new ProspectOrganization();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idProspect = params['id'];
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.organizationService.query()
        .subscribe((res: ResponseWrapper) => { this.organizations = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
        .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.salesBrokerService.query()
        //     .subscribe((res: ResponseWrapper) => { this.salesbrokers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.partyCategoryService.query()
            .subscribe((res: ResponseWrapper) => { this.partycategory = res.json.filter((o) => o.idCategory === 11 || o.idCategory === 12) ;
                console.log('party', res.json)
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.prospectSourceService.query()
            .subscribe((res: ResponseWrapper) => { this.prospectsources = res.json;
                console.log('res.json', res.json); }, (res: ResponseWrapper) => this.onError(res.json));
        this.eventTypeService.query({idparent: this.PROSPECT_EVENT_TYPE})
            .subscribe((res: ResponseWrapper) => { this.eventtypes = res.json ;
                // .filter((o) => o.idEventType === 13 || o.idEventType === 14);
                console.log('res.jsoneventtype  ', res.json);
            }, (res: ResponseWrapper) => this.onError(res.json));
        // this.facilityService.query()
        //     .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesmanService.query()
            .subscribe((res: ResponseWrapper) => { this.salesmen = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.index = 0;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.prospectOrganizationService.find(id).subscribe((prospectOrganization) => {
            if ( prospectOrganization.organization === null ) {
                prospectOrganization.organization = new Organization();
            } else {
                prospectOrganization.organization.dateOrganizationEstablish = new Date(prospectOrganization.organization.dateOrganizationEstablish);
            }
            this.prospectOrganization = prospectOrganization;
            console.log('Check Detail:', prospectOrganization.organization.idParty);
        });
    }

    previousState() {
        this.router.navigate(['prospect-organization']);
    }

    addQuotation() {
    }

    save() {
        this.isSaving = true;
        console.log('this.prospectOrganization', this.prospectOrganization);
        console.log('this.organization', this.organizations)
        if (this.prospectOrganization.idProspect !== undefined) {
            this.subscribeToSaveResponse(this.prospectOrganizationService.update(this.prospectOrganization));
        } else {
            this.subscribeToSaveResponse(
                this.prospectOrganizationService.create(this.prospectOrganization));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProspectOrganization>) {
        result.subscribe((res: ProspectOrganization) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ProspectOrganization) {
        this.eventManager.broadcast({ name: 'prospectOrganizationListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'prospectOrganization saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'prospectOrganization Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackOrganizationById(index: number, item: Organization) {
        // console.log('index', index);
        // console.log('item', item);
        return item.idParty;
    }

    trackPartyById(index: number, item: PartyCategory) {
        // console.log('index', index);
        // console.log('item', item);
        return item.idCategory;
    }

    trackProspectSourceById(index: number, item: ProspectSource) {
        return item.idProspectSource;
    }

    openNext() {
        this.index = (this.index === 1) ? 0 : this.index + 1;
    }

    openPrev() {
        this.index = (this.index === 0) ? 1 : this.index - 1;
    }
    // quotation() {
    //     this.router.navigate(['quote-item-popup-new']);
    // }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idPartyRole;
    }

}
