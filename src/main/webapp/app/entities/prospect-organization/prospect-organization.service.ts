import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { ProspectOrganization } from './prospect-organization.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ProspectOrganizationService {
    protected itemValues: ProspectOrganization[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/prospect-organizations';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/prospect-organizations';

    constructor(protected http: Http) { }

    create(prospectOrganization: ProspectOrganization): Observable<ProspectOrganization> {
        const copy = this.convert(prospectOrganization);
        console.log('copy organization prospect', copy);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(prospectOrganization: ProspectOrganization): Observable<ProspectOrganization> {
        const copy = this.convert(prospectOrganization);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<ProspectOrganization> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idstatustype', req.idstatustype);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(prospectOrganization: ProspectOrganization, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(prospectOrganization);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, prospectOrganization: ProspectOrganization): Observable<ProspectOrganization> {
        const copy = this.convert(prospectOrganization);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, prospectOrganizations: ProspectOrganization[]): Observable<ProspectOrganization[]> {
        const copy = this.convertList(prospectOrganizations);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(prospectOrganization: ProspectOrganization): ProspectOrganization {
        if (prospectOrganization === null || prospectOrganization === {}) {
            return {};
        }
        // const copy: ProspectOrganization = Object.assign({}, prospectOrganization);
        const copy: ProspectOrganization = JSON.parse(JSON.stringify(prospectOrganization));
        return copy;
    }

    protected convertList(prospectOrganizations: ProspectOrganization[]): ProspectOrganization[] {
        const copy: ProspectOrganization[] = prospectOrganizations;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ProspectOrganization[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
