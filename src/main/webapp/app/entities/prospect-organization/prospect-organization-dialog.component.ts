import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {ProspectOrganization} from './prospect-organization.model';
import {ProspectOrganizationPopupService} from './prospect-organization-popup.service';
import {ProspectOrganizationService} from './prospect-organization.service';
import {ToasterService} from '../../shared';
import { Organization, OrganizationService } from '../organization';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-prospect-organization-dialog',
    templateUrl: './prospect-organization-dialog.component.html'
})
export class ProspectOrganizationDialogComponent implements OnInit {

    prospectOrganization: ProspectOrganization;
    isSaving: boolean;

    organizations: Organization[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected prospectOrganizationService: ProspectOrganizationService,
        protected organizationService: OrganizationService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.organizationService.query()
            .subscribe((res: ResponseWrapper) => { this.organizations = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.prospectOrganization.idProspect !== undefined) {
            this.subscribeToSaveResponse(
                this.prospectOrganizationService.update(this.prospectOrganization));
        } else {
            this.subscribeToSaveResponse(
                this.prospectOrganizationService.create(this.prospectOrganization));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProspectOrganization>) {
        result.subscribe((res: ProspectOrganization) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ProspectOrganization) {
        this.eventManager.broadcast({ name: 'prospectOrganizationListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'prospectOrganization saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'prospectOrganization Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackOrganizationById(index: number, item: Organization) {
        return item.idParty;
    }
}

@Component({
    selector: 'jhi-prospect-organization-popup',
    template: ''
})
export class ProspectOrganizationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected prospectOrganizationPopupService: ProspectOrganizationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
                this.prospectOrganizationPopupService
                    .open(ProspectOrganizationDialogComponent as Component);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
