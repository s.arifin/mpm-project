import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    ProspectOrganizationService,
    ProspectOrganizationPopupService,
    ProspectOrganizationComponent,
    ProspectOrganizationDialogComponent,
    ProspectOrganizationPopupComponent,
    prospectOrganizationRoute,
    prospectOrganizationPopupRoute,
    ProspectOrganizationResolvePagingParams,
    ProspectOrganizationGridComponent,
    ProspectOrganizationEditComponent,
    ProspectOrganizationStepComponent,
    ProspectOrganizationQuotationDialogComponent,
    ProspectOrganizationQuotationPopupComponent,
    ProspectOrganizationLinkDetailComponent
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {MpmQuoteItemModule} from '../quote-item/quote-item.module';

import {
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         RadioButtonModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule,
         TooltipModule
        } from 'primeng/primeng';

import { WizardModule } from 'ng2-archwizard';

import { MpmSharedEntityModule } from '../shared-entity.module';

import {MpmProspectOrganizationDetailsModule} from '../prospect-organization-details/prospect-organization-details.module';

const ENTITY_STATES = [
    ...prospectOrganizationRoute,
    ...prospectOrganizationPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        MpmQuoteItemModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        WizardModule,
        RadioButtonModule,
        TooltipModule,
        MpmProspectOrganizationDetailsModule
    ],
    exports: [
        ProspectOrganizationComponent,
        ProspectOrganizationGridComponent,
        ProspectOrganizationEditComponent,
        ProspectOrganizationStepComponent,
        ProspectOrganizationQuotationDialogComponent,
        ProspectOrganizationLinkDetailComponent,
        ProspectOrganizationQuotationPopupComponent
    ],
    declarations: [
        ProspectOrganizationComponent,
        ProspectOrganizationDialogComponent,
        ProspectOrganizationPopupComponent,
        ProspectOrganizationGridComponent,
        ProspectOrganizationEditComponent,
        ProspectOrganizationStepComponent,
        ProspectOrganizationLinkDetailComponent,
        ProspectOrganizationQuotationDialogComponent,
        ProspectOrganizationQuotationPopupComponent
    ],
    entryComponents: [
        ProspectOrganizationComponent,
        ProspectOrganizationDialogComponent,
        ProspectOrganizationPopupComponent,
        ProspectOrganizationGridComponent,
        ProspectOrganizationEditComponent,
        ProspectOrganizationStepComponent,
        ProspectOrganizationLinkDetailComponent,
        ProspectOrganizationQuotationDialogComponent,
        ProspectOrganizationQuotationPopupComponent
    ],
    providers: [
        ProspectOrganizationService,
        ProspectOrganizationPopupService,
        ProspectOrganizationResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmProspectOrganizationModule {}
