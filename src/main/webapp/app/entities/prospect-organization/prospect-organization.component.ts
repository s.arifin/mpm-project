import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ProspectOrganization } from './prospect-organization.model';
import { ProspectOrganizationService } from './prospect-organization.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import * as ProspectOrganizationConstant from '../../shared/constants/prospect-organization.constants'

@Component({
    selector: 'jhi-prospect-organization',
    templateUrl: './prospect-organization.component.html'
})
export class ProspectOrganizationComponent implements OnInit, OnDestroy {

    // currentAccount: any;
    prospectOrganizations: ProspectOrganization[];
    // error: any;
    // success: any;
    // eventSubscriber: Subscription;
    // currentSearch: string;
    // routeData: any;
    // links: any;
    // totalItems: any;
    // queryCount: any;
    // itemsPerPage: any;
    // page: any;
    // predicate: any;
    // previousPage: any;
    // reverse: any;
    upload: boolean;
    statusTypes: any[];

    dateKirim1: Date;
    dateKirim2: Date;

    statusNew: Number;
    statusLow: Number;
    statusMedium: Number;
    statusHot: Number;

    uploadProspectOrganization: ProspectOrganization;

    constructor(
        protected prospectOrganizationService: ProspectOrganizationService,
        // protected confirmationService: ConfirmationService,
        // protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        // protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        // protected paginationUtil: JhiPaginationUtil,
        // protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        // protected ProspectOrganization: new ProspectOrganization
    ) {
        // this.itemsPerPage = ITEMS_PER_PAGE;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        // });
        // this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.uploadProspectOrganization = new ProspectOrganization();
        this.statusNew = ProspectOrganizationConstant.STATUS_NEW;
        this.statusLow = ProspectOrganizationConstant.STATUS_LOW;
        this.statusMedium = ProspectOrganizationConstant.STATUS_MEDIUM;
        this.statusHot = ProspectOrganizationConstant.STATUS_HOT;
    }

    // loadAll() {
    //     if (this.currentSearch) {
    //         this.prospectOrganizationService.search({
    //             page: this.page - 1,
    //             query: this.currentSearch,
    //             size: this.itemsPerPage,
    //             sort: this.sort()}).subscribe(
    //                 (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
    //                 (res: ResponseWrapper) => this.onError(res.json)
    //             );
    //         return;
    //     }
    //     this.prospectOrganizationService.query({
    //         page: this.page - 1,
    //         size: this.itemsPerPage,
    //         sort: this.sort()}).subscribe(
    //         (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
    //         (res: ResponseWrapper) => this.onError(res.json)
    //     );
    // }

    // loadPage(page: number) {
    //     if (page !== this.previousPage) {
    //         this.previousPage = page;
    //         this.transition();
    //     }
    // }

    // transition() {
    //     this.router.navigate(['/prospect-organization'], {queryParams:
    //         {
    //             page: this.page,
    //             size: this.itemsPerPage,
    //             search: this.currentSearch,
    //             sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
    //         }
    //     });
    //     this.loadAll();
    // }

    // clear() {
    //     this.page = 0;
    //     this.currentSearch = '';
    //     this.router.navigate(['/prospect-organization', {
    //         page: this.page,
    //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
    //     }]);
    //     this.loadAll();
    // }

    // search(query) {
    //     if (!query) {
    //         return this.clear();
    //     }
    //     this.page = 0;
    //     this.currentSearch = query;
    //     this.router.navigate(['/prospect-organization', {
    //         search: this.currentSearch,
    //         page: this.page,
    //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
    //     }]);
    //     this.loadAll();
    // }

    ngOnInit() {
        // this.loadAll();
        // this.principal.identity(true).then((account) => {
        //     this.currentAccount = account;
        // });
        // this.registerChangeInProspectOrganizations();
        this.upload = false;
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    trackStatusTypeById() {
    }

    // trackId(index: number, item: ProspectOrganization) {
    //     return item.idProspect;
    // }

    // registerChangeInProspectOrganizations() {
    //     this.eventSubscriber = this.eventManager.subscribe('prospectOrganizationListModification', (response) => this.loadAll());
    // }

    // sort() {
    //     const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    //     if (this.predicate !== 'idProspect') {
    //         result.push('idProspect');
    //     }
    //     return result;
    // }

    // protected onSuccess(data, headers) {
    //     console.log('data', data);
    //     this.links = this.parseLinks.parse(headers.get('link'));
    //     this.totalItems = headers.get('X-Total-Count');
    //     this.queryCount = this.totalItems;
    //     // this.page = pagingParams.page;
    //     this.prospectOrganizations = data;
    // }

    // protected onError(error) {
    //     this.alertService.error(error.message, null, null);
    // }

    // executeProcess(data) {
    //     this.prospectOrganizationService.executeProcess(0, null, data).subscribe(
    //        (value) => console.log('this: ', value),
    //        (err) => console.log(err),
    //        () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
    //     );
    // }

    // loadDataLazy(event: LazyLoadEvent) {
    //     this.itemsPerPage = event.rows;
    //     this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

    //     if (event.sortField !== undefined) {
    //         this.predicate = event.sortField;
    //         this.reverse = event.sortOrder;
    //     }
    //     this.loadAll();
    // }
    // uploadfx(idParty) {
    //     // console.log('idparty', idParty);
    //     this.uploadProspectOrganization.organization.idParty = idParty
    //     this.upload = true;
    // }
    back() {
        this.upload = false;
    }

    // updateRowData(event) {
    //     if (event.data.id !== undefined) {
    //         this.prospectOrganizationService.update(event.data)
    //             .subscribe((res: ProspectOrganization) =>
    //                 this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
    //     } else {
    //         this.prospectOrganizationService.create(event.data)
    //             .subscribe((res: ProspectOrganization) =>
    //                 this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
    //     }
    // }

    // protected onRowDataSaveSuccess(result: ProspectOrganization) {
    //     this.toasterService.showToaster('info', 'ProspectOrganization Saved', 'Data saved..');
    // }

    // protected onRowDataSaveError(error) {
    //     try {
    //         error.json();
    //     } catch (exception) {
    //         error.message = error.text();
    //     }
    //     this.onError(error);
    // }

    // delete(id: any) {
    //     this.confirmationService.confirm({
    //         message: 'Are you sure that you want to delete?',
    //         header: 'Confirmation',
    //         icon: 'fa fa-question-circle',
    //         accept: () => {
    //             this.prospectOrganizationService.delete(id).subscribe((response) => {
    //             this.eventManager.broadcast({
    //                 name: 'prospectOrganizationListModification',
    //                 content: 'Deleted an prospectOrganization'
    //                 });
    //             });
    //         }
    //     });
    // }
}
