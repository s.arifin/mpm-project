import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import { ProspectOrganization } from './prospect-organization.model';
import { ProspectOrganizationPopupService} from './prospect-organization-popup.service';
import {ToasterService} from '../../shared';
import { Party, PartyService } from '../party';
import { Feature, FeatureService } from '../feature';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-prospect-organization-quotation-dialog',
    templateUrl: './prospect-organization-quotation.component.html'
})
export class ProspectOrganizationQuotationDialogComponent implements OnInit {

   isSaving: boolean;

    parties: Party[];
    feature: Feature;
    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected partyService: PartyService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected  featureService: FeatureService
    ) {
    }

    ngOnInit() {
        this.featureService.query()
        .subscribe((res: ResponseWrapper) => { this.feature = res.json;
        console.log('feature', res.json); }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'prospectOrganization Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
    // trackPartyById(index: number, item: PartyCategory) {
    //     // console.log('index', index);
    //     // console.log('item', item);
    //     return item.idCategory;
    // }

}

@Component({
    selector: 'jhi-prospect-organization-quotation-popup',
    template: ''
})
export class ProspectOrganizationQuotationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected prospectOrganizationPopupService: ProspectOrganizationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.prospectOrganizationPopupService
            .open(ProspectOrganizationQuotationDialogComponent as Component);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
