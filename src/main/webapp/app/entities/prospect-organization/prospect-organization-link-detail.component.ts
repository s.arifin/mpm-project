import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ProspectOrganization } from './prospect-organization.model';
import { ProspectOrganizationService } from './prospect-organization.service';
import { Salesman, SalesmanService} from '../salesman';
import { ProspectOrganizationDetails, ProspectOrganizationDetailsService } from '../prospect-organization-details'
import { Person, PersonService } from '../person';
import { Motor, MotorService } from '../motor';
import { Feature, FeatureService } from '../feature';
import { Organization } from '../organization';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import * as ProspectOrganizationConstant from '../../shared/constants/prospect-organization.constants'

@Component({
    selector: 'jhi-prospect-organization-link-detail',
    templateUrl: './prospect-organization-link-detail.component.html'
})
export class ProspectOrganizationLinkDetailComponent implements OnInit, OnDestroy {
    protected subscription: Subscription;
    currentAccount: any;
    prospectOrganizationDetails: ProspectOrganizationDetails[];
    ProspectOrganizationDetailsRow: ProspectOrganizationDetails;
    prospectOrganization: ProspectOrganization;
    people: Person;
    motors: Motor;
    features: Feature;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any= 1;
    predicate: any= 'idProspectOrganizationDetail';
    previousPage: any;
    reverse: any= false;
    idProspect: any;
    details: boolean;
    indexAccordion: number;
    index: number;
    isSaving: boolean;

    constructor(
        protected prospectOrganizationDetailsService: ProspectOrganizationDetailsService,
        protected prospectOrganizationService: ProspectOrganizationService,
        protected confirmationService: ConfirmationService,
        protected motorService: MotorService,
        protected personService: PersonService,
        protected featureService: FeatureService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.indexAccordion = 0;
        this.ProspectOrganizationDetailsRow = new ProspectOrganizationDetails();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            // this.page = data['pagingParams'].page;
            // this.previousPage = data['pagingParams'].page;
            // this.reverse = data['pagingParams'].ascending;
            // this.predicate = '';
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.prospectOrganizationDetailsService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.prospectOrganizationDetailsService.query({
            idProspect: this.idProspect,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/prospect-organization-details'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/prospect-organization-details', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/prospect-organization-details', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.subscription = this.activatedRoute.params.subscribe((params) => {
            if (params['id']) {
                this.idProspect = params['id'];
                // this.load(params['id']);
            }
        });
        this.personService.query()
        .subscribe((res: ResponseWrapper) => { this.people = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
            this.motorService.query()
        .subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
            this.featureService.query()
        .subscribe((res: ResponseWrapper) => { this.features = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.getProspectOrganization();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInProspectOrganizationDetails();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ProspectOrganizationDetails) {
        return item.idProspectOrganizationDetail;
    }

    registerChangeInProspectOrganizationDetails() {
        this.eventSubscriber = this.eventManager.subscribe('prospectOrganizationDetailsListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idProspectOrganizationDetail') {
            result.push('idProspectOrganizationDetail');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.prospectOrganizationDetails = new Array<ProspectOrganizationDetails>();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.prospectOrganizationDetails = data;
        this.details = true;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.prospectOrganizationDetailsService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    lanjutSUR() {
        this.prospectOrganizationDetailsService.executeListProcess(102, null, this.prospectOrganizationDetails).subscribe(
            (value) => console.log('this: ', value),
            (err) => console.log(err),
            () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        )
        console.log('details', this.prospectOrganizationDetails);
    }
    getProspectOrganization() {
        this.prospectOrganizationService.find(this.idProspect).subscribe((prospectOrganization) => {
            this.prospectOrganization = prospectOrganization;
            console.log('Check Detail:', prospectOrganization);
        });
    }
    previousState() {
        this.details = false;
    }
    editRowDetails(paramID, status) {
        if (status === 2) {
            this.prospectOrganizationDetailsService.find(paramID).subscribe((prospectDetails) => {
                this.ProspectOrganizationDetailsRow =  prospectDetails;
                this.details = false;
            })
        } else if (status === 1) {
            this.ProspectOrganizationDetailsRow = new ProspectOrganizationDetails();
            this.details = false;
        }
    }

    save() {
        console.log('detail row', this.ProspectOrganizationDetailsRow);
        if (this.ProspectOrganizationDetailsRow.id !== undefined) {
            this.prospectOrganizationDetailsService.update(this.ProspectOrganizationDetailsRow)
                .subscribe((res: ProspectOrganizationDetails) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.ProspectOrganizationDetailsRow.idProspect = this.idProspect;
            this.prospectOrganizationDetailsService.create(this.ProspectOrganizationDetailsRow)
                .subscribe((res: ProspectOrganizationDetails) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: ProspectOrganizationDetails) {
        this.details = true;
        this.toasterService.showToaster('info', 'ProspectOrganizationDetails Saved', 'Data saved..');
        this.loadAll();
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.prospectOrganizationDetailsService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'prospectOrganizationDetailsListModification',
                    content: 'Deleted an prospectOrganizationDetails'
                    });
                });
            }
        });
    }

    // protected subscription: Subscription;
    // prospectOrganization: ProspectOrganization;
    // salesman: Salesman;
    // prospectOrganizationDetailsGrid: ProspectOrganizationDetails[];

    // prospectOrganizationDetails: ProspectOrganizationDetails;

    // uploadProspectOrganization: ProspectOrganization;

    // idProspect: any;

    // people: Person[];

    // motors: Motor[];

    // features: Feature[];

    // details: boolean;
    // currentAccount: any;
    // error: any;
    // success: any;
    // eventSubscriber: Subscription;
    // currentSearch: string;
    // routeData: any;
    // links: any;
    // totalItems: any;
    // queryCount: any;
    // itemsPerPage: any;
    // page: any= 1;
    // predicate: any= 'idProspectOrganizationDetail';
    // previousPage: any;
    // reverse: any= false;
    // isSaving: boolean;

    // constructor(
    //     protected route: ActivatedRoute,
    //     protected salesmanServices: SalesmanService,
    //     protected prospectOrganizationService: ProspectOrganizationService,
    //     protected prospectOrganizationDetailsService: ProspectOrganizationDetailsService,
    //     protected parseLinks: JhiParseLinks,
    //     protected confirmationService: ConfirmationService,
    //     protected personService: PersonService,
    //     protected motorService: MotorService,
    //     protected featureService: FeatureService,
    //     protected alertService: JhiAlertService,
    //     protected principal: Principal,
    //     protected router: Router,
    //     protected eventManager: JhiEventManager,
    //     protected toasterService: ToasterService,
    // ) {

    // }

    // ngOnInit() {
    //     this.subscription = this.route.params.subscribe((params) => {
    //         if (params['id']) {
    //             this.idProspect = params['id'];
    //             this.loadDetails(params['id']);
    //         }
    //     });
    //     this.personService.query()
    //         .subscribe((res: ResponseWrapper) => { this.people = res.json; }, (res: ResponseWrapper) => this.onErrorDetails(res.json));
    //     this.motorService.query()
    //         .subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onErrorDetails(res.json));
    //     this.featureService.query()
    //         .subscribe((res: ResponseWrapper) => { this.features = res.json; }, (res: ResponseWrapper) => this.onErrorDetails(res.json));
    //         // this.isSaving = false;
    // }

    // loadDetails(id) {
    //     this.prospectOrganizationService.find(id).subscribe((prospectOrganization) => {
    //         this.prospectOrganization = prospectOrganization;
    //         this.loadProspectOrganizationDetails(this.prospectOrganization.idProspect);
    //         console.log('Check Detail:', prospectOrganization);
    //     });
    // }

    // lanjutSUR() {
    //     console.log('prospectdetail', this.prospectOrganizationDetails)
    // }

    // trackStatusTypeById() {
    // }

    // back() {
    // }

    // ----------------------------detail component------------------------------------
    // loadProspectOrganizationDetails(paramID) {
    //     if (this.currentSearch) {
    //         this.prospectOrganizationDetailsService.search({
    //             page: this.page - 1,
    //             query: this.currentSearch,
    //             size: this.itemsPerPage,
    //             sort: this.sort()}).subscribe(
    //                 (res: ResponseWrapper) => this.onSuccessDetails(res.json, res.headers),
    //                 (res: ResponseWrapper) => this.onErrorDetails(res.json)
    //             );
    //         return;
    //     }
    //     this.prospectOrganizationDetailsService.query({
    //         idProspect: this.idProspect,
    //         page: this.page - 1,
    //         size: this.itemsPerPage,
    //         sort: this.sort()}).subscribe(
    //         (res: ResponseWrapper) => this.onSuccessDetails(res.json, res.headers),
    //         (res: ResponseWrapper) => this.onErrorDetails(res.json)
    //     );
    // }

    // trackId(index: number, item: ProspectOrganizationDetails) {
    //     return item.idProspectOrganizationDetail;
    // }

    // sort() {
    //     const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    //     if (this.predicate !== 'idProspectOrganizationDetail') {
    //         result.push('idProspectOrganizationDetail');
    //     }
    //     return result;
    // }

    // protected onSuccessDetails(data, headers) {
    //     this.prospectOrganizationDetailsGrid = new Array< ProspectOrganizationDetails>();
    //     this.links = this.parseLinks.parse(headers.get('link'));
    //     this.totalItems = headers.get('X-Total-Count');
    //     this.queryCount = this.totalItems;
    //     // this.page = pagingParams.page;
    //     this.prospectOrganizationDetailsGrid = data;
    //     console.log('chectdetail org', this.prospectOrganizationDetailsGrid);
    //     this.details = true;
    // }

    // protected onErrorDetails(error) {
    //     this.alertService.error(error.message, null, null);
    // }

    // executeProcess(data) {
    //     this.prospectOrganizationDetailsService.executeProcess(0, null, data).subscribe(
    //        (value) => console.log('this: ', value),
    //        (err) => console.log(err),
    //        () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
    //     );
    // }

    // loadDataLazy(event: LazyLoadEvent) {
    //     this.itemsPerPage = event.rows;
    //     this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

    //     if (event.sortField !== undefined) {
    //         this.predicate = event.sortField;
    //         this.reverse = event.sortOrder;
    //     }
    //     // this.loadAll();
    // }

    // updateRowData(event) {
    //     if (event.data.id !== undefined) {
    //         this.prospectOrganizationDetailsService.update(event.data)
    //             .subscribe((res: ProspectOrganizationDetails) =>
    //                 this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
    //     } else {
    //         this.prospectOrganizationDetailsService.create(event.data)
    //             .subscribe((res: ProspectOrganizationDetails) =>
    //                 this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
    //     }
    // }

    // protected onRowDataSaveSuccess(result: ProspectOrganizationDetails) {
    //     this.toasterService.showToaster('info', 'ProspectOrganizationDetails Saved', 'Data saved..');
    // }

    // protected onRowDataSaveError(error) {
    //     try {
    //         error.json();
    //     } catch (exception) {
    //         error.message = error.text();
    //     }
    //     this.onError(error);
    // }

    // delete(id: any) {
    //     this.confirmationService.confirm({
    //         message: 'Are you sure that you want to delete?',
    //         header: 'Confirmation',
    //         icon: 'fa fa-question-circle',
    //         accept: () => {
    //             this.prospectOrganizationDetailsService.delete(id).subscribe((response) => {
    //             this.eventManager.broadcast({
    //                 name: 'prospectOrganizationDetailsListModification',
    //                 content: 'Deleted an prospectOrganizationDetails'
    //                 });
    //             });
    //         }
    //     });
    // }
    // -----------------------------------------prospect detail edit

    // load(id) {
    //     this.prospectOrganizationDetailsService.find(id).subscribe((prospectOrganizationDetails) => {
    //         this.prospectOrganizationDetails = prospectOrganizationDetails;
    //     });
    // }

    // previousState() {
    //     this.router.navigate(['prospect-organization-details/' + this.prospectOrganizationDetails.idProspect ]);
    // }

    // save() {
    //     this.isSaving = true;
    //     if (this.prospectOrganizationDetails.idProspectOrganizationDetail !== undefined) {
    //         this.subscribeToSaveResponse(
    //             this.prospectOrganizationDetailsService.update(this.prospectOrganizationDetails));
    //     } else {
    //         this.subscribeToSaveResponse(
    //             this.prospectOrganizationDetailsService.create(this.prospectOrganizationDetails));
    //     }
    // }

    // protected subscribeToSaveResponse(result: Observable<ProspectOrganizationDetails>) {
    //     result.subscribe((res: ProspectOrganizationDetails) =>
    //         this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    // }

    // protected onSaveSuccess(result: ProspectOrganizationDetails) {
    //     this.eventManager.broadcast({ name: 'prospectOrganizationDetailsListModification', content: 'OK'});
    //     this.toasterService.showToaster('info', 'Save', 'prospectOrganizationDetails saved !');
    //     this.isSaving = false;
    //     this.previousState();
    // }

    // protected onSaveError(error) {
    //     try {
    //         error.json();
    //     } catch (exception) {
    //         error.message = error.text();
    //     }
    //     this.isSaving = false;
    //     this.onError(error);
    // }

    // protected onError(error) {
    //     this.toasterService.showToaster('warning', 'prospectOrganizationDetails Changed', error.message);
    //     this.alertService.error(error.message, null, null);
    // }

    trackPersonById(index: number, item: Person) {
        return item.idParty;
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }
}
