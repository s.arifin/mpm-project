import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ProspectOrganization } from './prospect-organization.model';
import { ProspectOrganizationService } from './prospect-organization.service';
import { CommunicationEventProspect, CommunicationEventProspectService } from '../communication-event-prospect';

import { ToasterService} from '../../shared';
import { Organization, OrganizationService } from '../organization';
import { ResponseWrapper} from '../../shared';
import { FollowUpQuestion } from '../shared-component';
import { ConfirmationService } from 'primeng/primeng';

import * as ProspectOrganizationConstant from '../../shared/constants/prospect-organization.constants'

@Component({
    selector: 'jhi-prospect-step',
    templateUrl: './prospect-organization-step.component.html',

})
export class ProspectOrganizationStepComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    protected prospectOrganization: ProspectOrganization;

    answer: FollowUpQuestion;
    idCommunicationEventProspect: any;

    FOLLOW_UP_BY_PHONE = '151';
    FOLLOW_UP_BY_VISIT = '152';
    FOLLOW_UP_BY_WALKIN = '153';
    BACK = 'prospect-organization';

    constructor(
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected prospectOrganizationService: ProspectOrganizationService,
        protected communicationEventProspectService: CommunicationEventProspectService,
        protected route: ActivatedRoute,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.prospectOrganization = new ProspectOrganization();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });

        this.answer = new FollowUpQuestion();

    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.prospectOrganizationService.find(id).subscribe((prospectOrganization) => {
            this.prospectOrganization = prospectOrganization;
            if ((this.prospectOrganization.prospectCount + 1) === 3) {
            }
        });
    }

    previousState() {
        this.router.navigate([this.BACK]);
    }

    save() {
        console.log('answer', this.answer);
        if (this.answer.answer02 === 'no' ) {
            this.doNotConnected();
        } else if (this.answer.answer03 === 'yes') {
            this.doInterest();
        } else if (this.answer.answer03 === 'no') {
            this.doNotInterest();
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProspectOrganization>) {
        result.subscribe((res: ProspectOrganization) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected subscribeToSaveResponseFollowUp(result: Observable<CommunicationEventProspect>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe((res: CommunicationEventProspect) => {
                    this.idCommunicationEventProspect = res.idCommunicationEvent;
                    resolve();
                });
            }
        )
    }

    protected onSaveSuccess(result: ProspectOrganization) {
        this.eventManager.broadcast({ name: 'prospectOrganizationListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'prospectOrganization saved !');
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'prospect Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    goToFormInterest() {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to process to interest form?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.router.navigate(['communication-event-prospect/' + this.idCommunicationEventProspect + '/2' + '/interest-form']);
            }
        });
    }

    doInterest() {
        this.prospectOrganization.prospectCount = 0;

        const followUpResult = new CommunicationEventProspect();
        followUpResult.idProspect = this.prospectOrganization.idProspect;
        followUpResult.eventTypeId = this.answer.answer01;
        followUpResult.interest = true;

        this.subscribeToSaveResponseFollowUp(this.communicationEventProspectService.create(followUpResult)).then(
            () => {this.goToFormInterest(); }
        );
        this.prospectOrganizationService.update(this.prospectOrganization);
    }

    doNotInterest() {

        const followUpResult = new CommunicationEventProspect();
        followUpResult.eventTypeId = this.answer.answer01;
        followUpResult.idProspect = this.prospectOrganization.idProspect;
        followUpResult.interest = false;
        if (this.answer.answer04 !== 'Lainnya') {
            followUpResult.note = 'Tidak Tertarik Karena ' + this.answer.answer04;
        } else {
            followUpResult.note = 'Tidak Tertarik Karena' + this.answer.note;
        }

        this.subscribeToSaveResponseFollowUp(this.communicationEventProspectService.create(followUpResult));
        this.subscribeToSaveResponse(this.prospectOrganizationService.executeProcess(103, null, this.prospectOrganization));
    }

    doNotConnected() {
        let process = null;
        if ((this.prospectOrganization.prospectCount + 1) === 3) {
            process = this.prospectOrganizationService.executeProcess(103, null, this.prospectOrganization)
        } else {
            this.prospectOrganization.prospectCount += 1;
            process = this.prospectOrganizationService.update(this.prospectOrganization);
        }
        const followUpResult = new CommunicationEventProspect();
        followUpResult.eventTypeId = this.answer.answer01;
        followUpResult.idProspect = this.prospectOrganization.idProspect;
        this.subscribeToSaveResponseFollowUp(this.communicationEventProspectService.create(followUpResult));
        this.subscribeToSaveResponse(process);
    }

    enterStep(e) {
        this.answer = new FollowUpQuestion();
    }

}
