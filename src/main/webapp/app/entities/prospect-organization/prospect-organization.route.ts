import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ProspectOrganizationComponent } from './prospect-organization.component';
import { ProspectOrganizationEditComponent } from './prospect-organization-edit.component';
import { ProspectOrganizationStepComponent } from './prospect-organization-step.component';
import { ProspectOrganizationQuotationPopupComponent} from './prospect-organization-quotation.component';
import { ProspectOrganizationPopupComponent } from './prospect-organization-dialog.component';
import { ProspectOrganizationLinkDetailComponent} from './prospect-organization-link-detail.component';
import { ProspectOrganizationDetailsEditComponent} from '../prospect-organization-details';

@Injectable()
export class ProspectOrganizationResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idProspect,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const prospectOrganizationRoute: Routes = [
    {
        path: 'prospect-organization',
        component: ProspectOrganizationComponent,
        resolve: {
            'pagingParams': ProspectOrganizationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganization.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const prospectOrganizationPopupRoute: Routes = [
    {
        path: 'prospect-organization-new',
        component: ProspectOrganizationEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganization.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-organization/:id/prospect-organization-details/:id/edit',
        component: ProspectOrganizationDetailsEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganizationDetails.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-organization/:id/link-detail',
        component: ProspectOrganizationLinkDetailComponent,
        resolve: {
            'pagingParams': ProspectOrganizationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganization.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-organization/:id/prospect-organization-details-new',
        component: ProspectOrganizationDetailsEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganizationDetails.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-organization/:id/edit',
        component: ProspectOrganizationEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganization.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-organization-popup-new',
        component: ProspectOrganizationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'prospect-organization/:id/popup-edit',
        component: ProspectOrganizationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'prospect-organization/:id/step',
        component: ProspectOrganizationStepComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospect.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-organization-quotation',
        component: ProspectOrganizationQuotationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectOrganization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
];
