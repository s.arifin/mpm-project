import { BaseEntity } from './../shared-component';
import { PostalAddress } from './../postal-address';
import { Organization } from './../organization';
import { Person } from './../person';

export class ProspectOrganization implements BaseEntity {
    constructor(
        public id?: any,
        public idProspect?: any,
        public organizationId?: any,
        public organization?: any,
        public prospectNumber?: any,
        public prospectCount?: number,
        public dealerId?: any,
        public brokerId?: any,
        public prospectSourceId?: any,
        public eventTypeId?: any,
        public customerType?: any,
        public facilityId?: any,
        public salesmanId?: any,
        public person?: any,
        public pic?: any,
        public addressTDP?: any
    ) {
        this.organization = new Organization();
        this.pic = new Person();
        this.addressTDP = new PostalAddress();
        this.prospectCount = 0;
    }
}
