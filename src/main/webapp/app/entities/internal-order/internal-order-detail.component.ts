import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { InternalOrder } from './internal-order.model';
import { InternalOrderService } from './internal-order.service';

@Component({
    selector: 'jhi-internal-order-detail',
    templateUrl: './internal-order-detail.component.html'
})
export class InternalOrderDetailComponent implements OnInit, OnDestroy {

    internalOrder: InternalOrder;
    protected subscription: Subscription;
    protected eventSubscriber: Subscription;

    constructor(
        protected eventManager: JhiEventManager,
        protected internalOrderService: InternalOrderService,
        protected route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInInternalOrders();
    }

    load(id) {
        this.internalOrderService.find(id).subscribe((internalOrder) => {
            this.internalOrder = internalOrder;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInInternalOrders() {
        this.eventSubscriber = this.eventManager.subscribe(
            'internalOrderListModification',
            (response) => this.load(this.internalOrder.id)
        );
    }
}
