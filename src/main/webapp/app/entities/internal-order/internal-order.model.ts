import { BaseEntity } from './../../shared';

export class InternalOrder implements BaseEntity {
    constructor(
        public id?: any,
        public idOrder?: any,
        public internalFromId?: any,
        public internalToId?: any,
    ) {
    }
}
