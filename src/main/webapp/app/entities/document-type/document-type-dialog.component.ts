import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {DocumentType} from './document-type.model';
import {DocumentTypePopupService} from './document-type-popup.service';
import {DocumentTypeService} from './document-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-document-type-dialog',
    templateUrl: './document-type-dialog.component.html'
})
export class DocumentTypeDialogComponent implements OnInit {

    documentType: DocumentType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected documentTypeService: DocumentTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.documentType.idDocumentType !== undefined) {
            this.subscribeToSaveResponse(
                this.documentTypeService.update(this.documentType));
        } else {
            this.subscribeToSaveResponse(
                this.documentTypeService.create(this.documentType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<DocumentType>) {
        result.subscribe((res: DocumentType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: DocumentType) {
        this.eventManager.broadcast({ name: 'documentTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'documentType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'documentType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-document-type-popup',
    template: ''
})
export class DocumentTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected documentTypePopupService: DocumentTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.documentTypePopupService
                    .open(DocumentTypeDialogComponent as Component, params['id']);
            } else {
                this.documentTypePopupService
                    .open(DocumentTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
