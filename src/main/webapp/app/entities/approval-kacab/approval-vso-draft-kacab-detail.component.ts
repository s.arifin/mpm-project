import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApprovalService } from '../approval';
import { AccountService } from '../../shared/auth/account.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { LazyLoadEvent } from 'primeng/primeng';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ConfirmationService } from 'primeng/primeng';

import { SalesUnitRequirement, SalesUnitRequirementService } from '../sales-unit-requirement';

import * as BaseConstants from '../../shared/constants/base.constants';
import * as SalesUnitRequirementConstant from '../../shared/constants/sales-unit-requirement.constants';

@Component({
    selector: 'jhi-approval-vso-draft-kacab-detail',
    templateUrl: './approval-vso-draft-kacab-detail.component.html'
})
export class ApprovalVSODraftKacabDetailComponent implements OnInit {
    isApprovalSubsidi: boolean;
    isApprovalTerritorialViolation: boolean;

    salesUnitRequirementId: String;

    salesUnitRequirement: SalesUnitRequirement;

    baseConstants: any;

    constructor(
        protected route: ActivatedRoute,
        protected alertService: JhiAlertService,
        protected router: Router,
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected confirmationService: ConfirmationService
    ) {
        this.isApprovalTerritorialViolation = false;
        this.isApprovalSubsidi = false;
        this.baseConstants = BaseConstants;
        this.salesUnitRequirement = new SalesUnitRequirement;
    }

    ngOnInit() {
        this.route.params.subscribe(
            (params) => {
                if (params['id']) {
                    this.salesUnitRequirementId = params['id'];
                    this.loadSalesUnitRequirement();
                }
            }
        )
    }

    protected loadSalesUnitRequirement(): void {
        this.salesUnitRequirementService.find(this.salesUnitRequirementId).subscribe(
            (data) => {
                this.salesUnitRequirement = data;
            }
        )
    }

    public confirmApprove(): void {
        this.confirmationService.confirm({
            header : 'Confirmation',
            message : 'Do you want to submit this <b>Approval</b>?',
            accept: () => {

                if (this.salesUnitRequirement.approvalDiscount === BaseConstants.Status.STATUS_WAITING_FOR_APPROVAL) {
                    if (this.isApprovalSubsidi) {
                        this.salesUnitRequirement.approvalDiscount = BaseConstants.Status.STATUS_APPROVED;
                    } else {
                        this.salesUnitRequirement.approvalDiscount = BaseConstants.Status.STATUS_REJECTED;
                    }
                }

                if (this.salesUnitRequirement.currentStatus === BaseConstants.Status.STATUS_TERRITORIAL_VIOLATION) {
                    if (this.isApprovalTerritorialViolation) {
                        this.salesUnitRequirement.currentStatus = BaseConstants.Status.STATUS_APPROVED
                    } else {
                        this.salesUnitRequirement.currentStatus = BaseConstants.Status.STATUS_REJECTED;
                    }
                }
                console.log('a', this.salesUnitRequirement);
                this.salesUnitRequirementService.executeProcess(SalesUnitRequirementConstant.APPROVAL_KACAB, null, this.salesUnitRequirement).subscribe(
                    (res) => {
                        this.previousState();
                    }
                );
            }
        });
    }

    public previousState(): void {
        this.router.navigate(['approval-kacab/vso-draft']);
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
