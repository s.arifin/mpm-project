import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';
import {MpmSharedEntityModule} from '../shared-entity.module';
import {MpmSharedModule} from '../../shared';
import {
    ApprovalVSODraftKacabComponent,
    approvalKacabRoute,
    ApprovalKacabResolvePagingParams,
    ApprovalVSODraftKacabDetailComponent,
    ApprovalKacabComponent,
    ApprovalRequestProductKacabComponent,
    ApprovalRequestProductKacabDetailComponent,
    ApprovalPelanggaranWilayahComponent,
    ApprovalPelanggaranWilayahDetailComponent
} from './';

import { MpmApprovalModule } from '../approval';

import { MpmSalesUnitRequirementModule } from '../sales-unit-requirement';

import {
    DataListModule,
    PanelModule,
    ButtonModule,
    InputSwitchModule,
    FieldsetModule,
    ConfirmDialogModule,
    ConfirmationService
} from 'primeng/primeng';

import {DataTableModule} from 'primeng/primeng';

const ENTITY_STATES = [
    ...approvalKacabRoute
];

@NgModule({
    imports : [
        MpmSharedEntityModule,
        MpmSalesUnitRequirementModule,
        ConfirmDialogModule,
        PanelModule,
        ButtonModule,
        FieldsetModule,
        InputSwitchModule,
        MpmSharedModule,
        MpmApprovalModule,
        DataListModule,
        DataTableModule,
        RouterModule.forChild(ENTITY_STATES),
    ],
    exports: [
        ApprovalRequestProductKacabComponent,
        ApprovalKacabComponent,
        ApprovalVSODraftKacabDetailComponent,
        ApprovalVSODraftKacabComponent,
        ApprovalPelanggaranWilayahComponent,
        ApprovalPelanggaranWilayahDetailComponent
    ],
    declarations : [
        ApprovalRequestProductKacabDetailComponent,
        ApprovalRequestProductKacabComponent,
        ApprovalKacabComponent,
        ApprovalVSODraftKacabDetailComponent,
        ApprovalVSODraftKacabComponent,
        ApprovalPelanggaranWilayahComponent,
        ApprovalPelanggaranWilayahDetailComponent
    ],
    entryComponents : [
        ApprovalRequestProductKacabDetailComponent,
        ApprovalRequestProductKacabComponent,
        ApprovalKacabComponent,
        ApprovalVSODraftKacabDetailComponent,
        ApprovalVSODraftKacabComponent,
        ApprovalPelanggaranWilayahComponent,
        ApprovalPelanggaranWilayahDetailComponent
    ],
    providers : [
        ApprovalKacabResolvePagingParams
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmApprovalKacabModule {}
