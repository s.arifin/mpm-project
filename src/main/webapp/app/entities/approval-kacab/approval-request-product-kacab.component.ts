import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApprovalService } from '../approval';
import { AccountService } from '../../shared/auth/account.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { LazyLoadEvent } from 'primeng/primeng';
import { RequestProductService, RequestProduct } from '../request-product';
import * as BaseConstant from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-approval-request-product-kacab',
    templateUrl: './approval-request-product-kacab.component.html'
})

export class ApprovalRequestProductKacabComponent implements OnInit {
    public requestProducts: Array<RequestProduct>;

    constructor(
        protected requestProductService: RequestProductService
    ) {
    }

    ngOnInit() {
        this.loadRequestProduct();
    }

    protected loadRequestProduct(): void {
        const obj = {
            idStatusType: BaseConstant.Status.STATUS_WAITING_FOR_APPROVAL,
            page: 0,
            size: 10000
        };
        this.requestProductService.queryFilterBy(obj).subscribe(
            (res) => {
                console.log('res.json', res.json);
                this.requestProducts = res.json;
            }
        )
    }
}
