import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ApprovalVSODraftKacabComponent } from './approval-vso-draft-kacab.component';
import { ApprovalVSODraftKacabDetailComponent } from './approval-vso-draft-kacab-detail.component';
import { ApprovalKacabComponent } from './approval-kacab.component';
import { ApprovalRequestProductKacabComponent } from './approval-request-product-kacab.component';
import { ApprovalRequestProductKacabDetailComponent} from './approval-request-product-kacab-detail.component';
import { ApprovalPelanggaranWilayahComponent} from './approval-pelanggaran-wilayah.component';
import { ApprovalPelanggaranWilayahDetailComponent} from './approval-pelanggaran-wilayah-detail.component';

@Injectable()
export class ApprovalKacabResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequirement,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const approvalKacabRoute: Routes = [
    {
        path: 'approval-kacab',
        component: ApprovalKacabComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval.kacab'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-kacab/request-product',
        component: ApprovalRequestProductKacabComponent,
        resolve: {
            'pagingParams': ApprovalKacabResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval.kacab-vso-draft'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path : 'approval-kacab/pelanggaran-wilayah',
        component : ApprovalPelanggaranWilayahComponent,
        resolve: {
            'pagingParams': ApprovalKacabResolvePagingParams
        },
        data : {
            authorities : ['ROLE_USER'],
            pageTitle : 'mpmApp.approval.kacab-pelanggaran-wilayah'
        },
        canActivate : [UserRouteAccessService]
    },
    {
        path: 'approval-kacab/request-product-detail/:idRequest',
        component: ApprovalRequestProductKacabDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval.kacab-vso-draft'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-kacab/vso-draft',
        component: ApprovalVSODraftKacabComponent,
        resolve: {
            'pagingParams': ApprovalKacabResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval.kacab-vso-draft'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path : 'approval-kacab/vso-draft/:id/detail',
        component : ApprovalVSODraftKacabDetailComponent,
        data : {
            authorities : ['ROLE_USER'],
            pageTitle : 'mpmApp.approval.kacab-vso-draft'
        },
        canActivate : [UserRouteAccessService]
    },
    {
        path : 'approval-kacab/pelanggaran-wilayah/:id/detail',
        component : ApprovalPelanggaranWilayahDetailComponent,
        data : {
            authorities : ['ROLE_USER'],
            pageTitle : 'mpmApp.approval.kacab-vso-draft'
        },
        canActivate : [UserRouteAccessService]
    }
];
