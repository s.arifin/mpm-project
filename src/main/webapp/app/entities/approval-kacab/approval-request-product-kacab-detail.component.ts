import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApprovalService } from '../approval';
import { AccountService } from '../../shared/auth/account.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../shared';
import { ConfirmationService } from 'primeng/primeng';
import { JhiEventManager, JhiAlertService, JhiParseLinks, JhiPaginationUtil, JhiLanguageService } from 'ng-jhipster';
import { RequestProductService, RequestProduct } from '../request-product';
import { RequestItemService, RequestItem } from '../request-item';
import { LoadingService } from '../../layouts';

import * as RequestItemConstant from '../../shared/constants/request-item.constants';
import * as BaseConstant from '../../shared/constants/base.constants';
import * as _ from 'lodash';

@Component({
    selector: 'jhi-approval-request-product-kacab-detail',
    templateUrl: './approval-request-product-kacab-detail.component.html'
})

export class ApprovalRequestProductKacabDetailComponent implements OnInit {
    public requestProduct: RequestProduct;
    public requestItems: Array<RequestItem>;
    public isSave: boolean;

    protected page: number;
    protected predicate: string;
    protected reverse: string;

    constructor(
        protected requestProductService: RequestProductService,
        protected requestItemService: RequestItemService,
        protected route: ActivatedRoute,
        protected commonUtilService: CommonUtilService,
        protected loadingService: LoadingService,
        protected alertService: JhiAlertService,
        protected confirmationService: ConfirmationService,
        protected router: Router,
    ) {
        this.isSave = false;
        this.requestProduct = new RequestProduct;
        this.page = 1;
        this.predicate = 'idRequestItem';
        this.reverse = 'asc';
    }

    ngOnInit() {
        this.route.params.subscribe(
            (param) => {
                const idRequest: string = param['idRequest'];
                this.loadRequestProduct(idRequest);
                this.loadRequestItem(idRequest);
            }
        );
    }

    protected loadRequestProduct(param: string): void {
        this.requestProductService.find(param).subscribe(
            (res) => {
                console.log('res', res);
                this.requestProduct = res;
            },
            (err) => {
                this.commonUtilService.showError(err);
            }
        )
    }

    protected loadRequestItem(param: string): void {
        this.requestItemService.queryFilterBy({
            filterName: 'relation',
            idRequest: param,
            page: this.page - 1,
            size: 10000,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => {
                const _requestItems: Array<RequestItem> = this.setQtyTransfer(res.json);
                this.requestItems = _requestItems;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    protected setQtyTransfer(data: Array<RequestItem>): Array<RequestItem> {
        const _data: Array<RequestItem> = _.map(data, function(e: RequestItem) {
            if (e.qtyTransfer === null) {
                e.qtyTransfer = e.qtyReq;
                return e;
            }
            return e;
        });

        return _data;
    }

    protected onError(error): void {
        this.alertService.error(error.message, null, null);
    }

    protected sort(): any {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequestItem') {
            result.push('idRequestItem');
        }
        return result;
    }

    public save(): void {
        this.loadingService.loadingStart();
        this.requestItemService.executeListProcess(RequestItemConstant.BULK_SAVE, null, this.requestItems).subscribe(
            (res) => {
                this.loadingService.loadingStop();
                this.isSave = true;
            },
            (err) => {
                this.loadingService.loadingStop();
                this.commonUtilService.showError(err);
            }
        )
    }

    protected previousState() {
        this.router.navigate(['approval-kacab/request-product']);
    }

    public processRequest(mode: string): void {
        if (mode === 'approve') {
            this.confirmationService.confirm({
                message: 'Are you sure that you want to approve this request?',
                header: 'Confirmation',
                icon: 'fa fa-question-circle',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.requestProductService.changeStatus(this.requestProduct, BaseConstant.Status.STATUS_APPROVED).subscribe(
                        (res) => {
                            this.loadingService.loadingStop();
                            this.previousState();
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            });
        } else if (mode === 'reject') {
            this.confirmationService.confirm({
                message: 'Are you sure that you want to not approve this request?',
                header: 'Confirmation',
                icon: 'fa fa-question-circle',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.requestProductService.changeStatus(this.requestProduct, BaseConstant.Status.STATUS_REJECTED).subscribe(
                        (res) => {
                            this.loadingService.loadingStop();
                            this.previousState();
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            });
        } else {
            this.loadingService.loadingStop();
        }
    }
}
