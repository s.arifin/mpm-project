import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesUnitRequirement } from '../sales-unit-requirement';
import { ApprovalService } from '../approval';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { LazyLoadEvent } from 'primeng/primeng';

@Component({
    selector: 'jhi-approval-vso-draft-kacab',
    templateUrl: './approval-vso-draft-kacab.component.html'
})

export class ApprovalVSODraftKacabComponent implements OnInit {
    salesUnitRequirements: SalesUnitRequirement[];
    totalItems: any;
    queryCount: any;
    links: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    currentSearch: string;
    routeData: any;

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected approvalService: ApprovalService,
        protected principal: Principal
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    ngOnInit() {
        this.loadData();
    }

    loadData() {
        const obj: Object = {
            page: this.page - 1,
            size: this.itemsPerPage,
            idinternal : this.principal.getIdInternal(),
            sort : this.sort()
        };
        this.approvalService.getApprovalKacabForDiscount(obj).subscribe(
            (res) => {
                this.salesUnitRequirements = res.json;
                console.log('aaaa', this.salesUnitRequirements);
            }
        )
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }
}
