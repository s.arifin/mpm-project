import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Prospect } from './prospect.model';
import { ProspectService } from './prospect.service';
import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { SalesBroker, SalesBrokerService } from '../sales-broker';
import { ProspectSource, ProspectSourceService } from '../prospect-source';
import { EventType, EventTypeService } from '../event-type';
import { Facility, FacilityService } from '../facility';
import { Salesman, SalesmanService } from '../salesman';
import { Suspect, SuspectService } from '../suspect';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-prospect-edit',
    templateUrl: './prospect-edit.component.html'
})
export class ProspectEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    prospect: Prospect;
    isSaving: boolean;

    internals: Internal[];

    salesbrokers: SalesBroker[];

    prospectsources: ProspectSource[];

    eventtypes: EventType[];

    facilities: Facility[];

    salesmen: Salesman[];

    suspects: Suspect[];

    constructor(
        protected alertService: JhiAlertService,
        protected prospectService: ProspectService,
        protected internalService: InternalService,
        protected salesBrokerService: SalesBrokerService,
        protected prospectSourceService: ProspectSourceService,
        protected eventTypeService: EventTypeService,
        protected facilityService: FacilityService,
        protected salesmanService: SalesmanService,
        protected suspectService: SuspectService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.prospect = new Prospect();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesBrokerService.query()
            .subscribe((res: ResponseWrapper) => { this.salesbrokers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.prospectSourceService.query()
            .subscribe((res: ResponseWrapper) => { this.prospectsources = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.eventTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.eventtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesmanService.query()
            .subscribe((res: ResponseWrapper) => { this.salesmen = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.prospectService.find(id).subscribe((prospect) => {
            this.prospect = prospect;
        });
    }

    previousState() {
        this.router.navigate(['prospect']);
    }

    save() {
        this.isSaving = true;
        if (this.prospect.idProspect !== undefined) {
            this.subscribeToSaveResponse(
                this.prospectService.update(this.prospect));
        } else {
            this.subscribeToSaveResponse(
                this.prospectService.create(this.prospect));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Prospect>) {
        result.subscribe((res: Prospect) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Prospect) {
        this.eventManager.broadcast({ name: 'prospectListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'prospect saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'prospect Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackSalesBrokerById(index: number, item: SalesBroker) {
        return item.idPartyRole;
    }

    trackProspectSourceById(index: number, item: ProspectSource) {
        return item.idProspectSource;
    }

    trackEventTypeById(index: number, item: EventType) {
        return item.idEventType;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idPartyRole;
    }

}
