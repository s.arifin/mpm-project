export class ProspectParameter {
    constructor(
        public status?: Array<Number>,
        public salesmanId?: String,
        public workTypeId?: Number,
        public districtId?: String,
        public saleTypeId?: Number
    ) {
        this.status = [10, 51, 52, 53];
        this.salesmanId = null;
        this.workTypeId = null;
        this.districtId = null;
        this.saleTypeId = null;
    }
}
