import { BaseEntity } from './../../shared';
import { Salesman } from '../salesman';

export class Prospect implements BaseEntity {
    constructor(
        public id?: any,
        public idProspect?: any,
        public prospectNumber?: string,
        public prospectCount?: number,
        public dateFollowUp?: Date,
        public dealerId?: any,
        public brokerId?: any,
        public brokerName?: any,
        public prospectSourceId?: any,
        public prospectSourceDescription?: any,
        public eventTypeId?: any,
        public eventLocation?: any,
        public facilityId?: any,
        public teamLeaderId?: any,
        public teamLeaderName?: any,
        public salesmanId?: any,
        public suspectId?: any,
        public currentStatus?: any,
        public salesmanName?: any,
        public salesCoordinatorName?: any ,
        public salesman?: Salesman,
    ) {
        this.prospectCount = 0;
        this.salesman = new Salesman()
    }
}
