import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {Prospect} from './prospect.model';
import {ProspectPopupService} from './prospect-popup.service';
import {ProspectService} from './prospect.service';
import {ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { SalesBroker, SalesBrokerService } from '../sales-broker';
import { ProspectSource, ProspectSourceService } from '../prospect-source';
import { EventType, EventTypeService } from '../event-type';
import { Facility, FacilityService } from '../facility';
import { Salesman, SalesmanService } from '../salesman';
import { Suspect, SuspectService } from '../suspect';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-prospect-dialog',
    templateUrl: './prospect-dialog.component.html'
})
export class ProspectDialogComponent implements OnInit {

    prospect: Prospect;
    isSaving: boolean;

    internals: Internal[];

    salesbrokers: SalesBroker[];

    prospectsources: ProspectSource[];

    eventtypes: EventType[];

    facilities: Facility[];

    salesmen: Salesman[];

    suspects: Suspect[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected prospectService: ProspectService,
        protected internalService: InternalService,
        protected salesBrokerService: SalesBrokerService,
        protected prospectSourceService: ProspectSourceService,
        protected eventTypeService: EventTypeService,
        protected facilityService: FacilityService,
        protected salesmanService: SalesmanService,
        protected suspectService: SuspectService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesBrokerService.query()
            .subscribe((res: ResponseWrapper) => { this.salesbrokers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.prospectSourceService.query()
            .subscribe((res: ResponseWrapper) => { this.prospectsources = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.eventTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.eventtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesmanService.query()
            .subscribe((res: ResponseWrapper) => { this.salesmen = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.prospect.idProspect !== undefined) {
            this.subscribeToSaveResponse(
                this.prospectService.update(this.prospect));
        } else {
            this.subscribeToSaveResponse(
                this.prospectService.create(this.prospect));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Prospect>) {
        result.subscribe((res: Prospect) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: Prospect) {
        this.eventManager.broadcast({ name: 'prospectListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'prospect saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'prospect Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackSalesBrokerById(index: number, item: SalesBroker) {
        return item.idPartyRole;
    }

    trackProspectSourceById(index: number, item: ProspectSource) {
        return item.idProspectSource;
    }

    trackEventTypeById(index: number, item: EventType) {
        return item.idEventType;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idPartyRole;
    }

}

@Component({
    selector: 'jhi-prospect-popup',
    template: ''
})
export class ProspectPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected prospectPopupService: ProspectPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.prospectPopupService
                    .open(ProspectDialogComponent as Component, params['id']);
            } else {
                this.prospectPopupService
                    .open(ProspectDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
