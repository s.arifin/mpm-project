import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ProspectComponent } from './prospect.component';
import { ProspectEditComponent } from './prospect-edit.component';
import { ProspectPopupComponent } from './prospect-dialog.component';

@Injectable()
export class ProspectResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idProspect,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const prospectRoute: Routes = [
    {
        path: 'prospect',
        component: ProspectComponent,
        resolve: {
            'pagingParams': ProspectResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospect.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const prospectPopupRoute: Routes = [
    {
        path: 'prospect-new',
        component: ProspectEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospect.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect/:id/edit',
        component: ProspectEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospect.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-popup-new',
        component: ProspectPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospect.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'prospect/:id/popup-edit',
        component: ProspectPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospect.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
