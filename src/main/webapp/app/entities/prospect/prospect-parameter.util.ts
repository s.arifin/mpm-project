import { URLSearchParams, BaseRequestOptions } from '@angular/http';

export const createProspectParameterOption = (req?: any): BaseRequestOptions => {
    const options: BaseRequestOptions = new BaseRequestOptions();
    if (req) {
        const params: URLSearchParams = new URLSearchParams();
        params.set('page', req.page);
        params.set('size', req.size);
        if (req.sort) {
            params.paramsMap.set('sort', req.sort);
        }
        params.set('query', req.query);

        params.set('status', req.prospectPTO.status);
        params.set('salesmanId', req.prospectPTO.salesmanId);
        params.set('workTypeId', req.prospectPTO.workTypeId);
        params.set('districtId', req.prospectPTO.districtId);
        params.set('saleTypeId', req.prospectPTO.saleTypeId);

        options.params = params;
    }
    return options;
}
