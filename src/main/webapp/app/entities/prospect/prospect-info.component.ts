import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ProspectSource, ProspectSourceService } from '../prospect-source';

@Component({
    selector: 'jhi-prospect-info',
    templateUrl: './prospect-info.component.html'
})
export class ProspectInfoComponent implements OnChanges {
    @Input()
    idProspectSource: number;

    public prospectSource: ProspectSource;

    constructor(
        protected prospectSourceService: ProspectSourceService
    ) {
        this.prospectSource = new ProspectSource();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idProspectSource']) {
            if (this.idProspectSource !== undefined) {
                this.loadData();
            }
        }
    }

    loadData() {
        this.prospectSourceService.find(this.idProspectSource).subscribe(
            (res) => {
                this.prospectSource = res;
                console.log('aaa', this.prospectSource);
            }
        )
    }
}
