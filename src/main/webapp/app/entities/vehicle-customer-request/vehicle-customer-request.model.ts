import { BaseEntity } from './../../shared';

export class VehicleCustomerRequest implements BaseEntity {
    constructor(
        public id?: any,
        public idRequest?: any,
        public currentStatus?: number,
        public requestNumber?: string,
        public dateCreate?: any,
        public dateRequest?: any,
        public description?: string,
        public customerOrder?: string,
        public idInternal?: any,
        public internalId?: any,
        public customerId?: any,
        public requestTypeId?: any,
        public qtyToShipment?: number,
        public salesBrokerId?: string,
        public salesBrokerCode?: string,
        public salesBrokerName?: string,
        public selectedKorsal?: string,
        public selectedSalesman?: string,
        public idSalesman?: any,
    ) {
        this.dateCreate = new Date();
        this.dateRequest = new Date();
        this.requestTypeId = 101;
    }
}

export class Subsidi {
    constructor(
        public subsidiAHM?: number,
        public subsidiMD?: number
    ) {

    }
}
