import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule} from '../../shared';
import {
    SurRequestCustomerComponent,
    VehicleCustomerRequestService,
    VehicleCustomerRequestPopupService,
    VehicleCustomerRequestComponent,
    VehicleCustomerRequestDialogComponent,
    VehicleCustomerRequestPopupComponent,
    vehicleCustomerRequestRoute,
    vehicleCustomerRequestPopupRoute,
    VehicleCustomerRequestResolvePagingParams,
    VehicleCustomerRequestEditComponent,
    SurRequestAsListComponent,
    SurRequestEditComponent,
    SalesUnitRequirementResolve,
    SurPersonComponent,
    SurOrganizationComponent,
    OrganizationViewgcComponent

} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         InputSwitchModule,
         InputTextModule,
         InputTextareaModule,
         DataScrollerModule,
         PaginatorModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         FieldsetModule,
         ScheduleModule,
         ChartModule,
         DragDropModule,
         LightboxModule
} from 'primeng/primeng';

import { DataTableModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ButtonModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { MpmSharedEntityModule } from '../shared-entity.module';
import { MpmSalesUnitRequirementModule  } from '../sales-unit-requirement/sales-unit-requirement.module';

const ENTITY_STATES = [
    ...vehicleCustomerRequestRoute,
    ...vehicleCustomerRequestPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        MpmSharedEntityModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSalesUnitRequirementModule,
        CommonModule,
        TabsModule.forRoot(),
        TooltipModule.forRoot(),
        InputTextareaModule,
        ButtonModule,
        InputSwitchModule,
        DataListModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        DataTableModule,
        SliderModule,
        RadioButtonModule,
    ],
    exports: [
        VehicleCustomerRequestComponent,
        VehicleCustomerRequestEditComponent,
        SurRequestAsListComponent,
        SurRequestEditComponent,
        SurPersonComponent,
        SurOrganizationComponent,
        OrganizationViewgcComponent

    ],
    declarations: [
        SurRequestCustomerComponent,
        VehicleCustomerRequestComponent,
        VehicleCustomerRequestDialogComponent,
        VehicleCustomerRequestPopupComponent,
        VehicleCustomerRequestEditComponent,
        SurRequestAsListComponent,
        SurRequestEditComponent,
        SurPersonComponent,
        SurOrganizationComponent,
        OrganizationViewgcComponent

    ],
    entryComponents: [
        SurRequestCustomerComponent,
        VehicleCustomerRequestComponent,
        VehicleCustomerRequestDialogComponent,
        VehicleCustomerRequestPopupComponent,
        VehicleCustomerRequestEditComponent,
        SurRequestAsListComponent,
        SurRequestEditComponent,
        SurPersonComponent,
        SurOrganizationComponent,
        OrganizationViewgcComponent
    ],
    providers: [
        VehicleCustomerRequestService,
        VehicleCustomerRequestPopupService,
        VehicleCustomerRequestResolvePagingParams,
        SalesUnitRequirementResolve,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmVehicleCustomerRequestModule {}
