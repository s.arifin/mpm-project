import { Organization } from './../organization/organization.model';
import { CustomerCService } from './../customer/customer-c.service';
import { Party } from './../party/party.model';
import { Customer } from './../customer/customer.model';
import { Component, OnChanges, OnInit, SimpleChanges, Input } from '@angular/core';
import { Person, PersonService } from '../person';
// import { PersonalCustomerService } from '../personal-customer';
import { SalesUnitRequirementService, SalesUnitRequirement } from '../sales-unit-requirement';
import { LoadingService } from '../../layouts';
import { CommonUtilService, ToasterService } from '../../shared';
import { ConfirmationService } from 'primeng/primeng';
import { CustomerService } from '../customer';

// import { Organization, OrganizationService } from '../organization';
// import { OrganizationCustomer, OrganizationCustomerService } from '../shared-component';
import { PersonalCustomerService, PersonalCustomer, OrganizationCustomer, OrganizationCustomerService } from './../shared-component';

@Component({
    selector: 'jhi-sur-organization',
    templateUrl: './sur-organization.component.html'
})

export class SurOrganizationComponent implements OnChanges, OnInit {
    @Input()
    public salesUnitRequirement: SalesUnitRequirement;

    @Input()
    public idRequestType: number;

    public ktpNumber: string;
    public isSameWithCustomer: boolean;
    public organizationCustomer: OrganizationCustomer;
    public disable: boolean;
    personalCustomer: PersonalCustomer;
    protected surReady: boolean;
    protected idReTypeReady: boolean

    constructor(
        protected toasterService: ToasterService,
        protected commonUtilService: CommonUtilService,
        protected loadingService: LoadingService,
        protected personService: PersonService,
        protected customerService: CustomerService,
        protected personalCustomerService: PersonalCustomerService,
        protected confirmationService: ConfirmationService,
        protected organizationCustomerService: OrganizationCustomerService,
        protected salesUnitRequirementService: SalesUnitRequirementService
    ) {
        // this.isSameWithCustomer = false;
        this.disable = false;
        this.surReady = false;
        this.idReTypeReady = false;
        this.personalCustomer = new PersonalCustomer();
        this.organizationCustomer = new OrganizationCustomer();
    }

    ngOnChanges(change: SimpleChanges) {
        if (change['salesUnitRequirement']) {
        }
        if (change['salesUnitRequirement']) {
            this.idReTypeReady = true;
            this.load();
        }

        if (change['idRequestType']) {
            this.idReTypeReady = true;
            this.load();
        }
    }

    ngOnInit() {
     }

     protected load(): void {
        // if (this.idReTypeReady && this.surReady) {
            const custId: string = this.salesUnitRequirement.customerId;
            const orgaOwner: string = this.salesUnitRequirement.organizationOwner.idParty;
            if (custId !== null && custId !== undefined) {
                const buyer: string = this.checkCustomer();
                if (buyer === 'organization') {
                    this.organizationCustomerService.find(custId).subscribe(
                        (res: OrganizationCustomer) => {
                            // this.salesUnitRequirement.customerId = res.idCustomer;
                            // this.organizationCustomer = res;
                            console.log('res.cariparty customer : ', res.partyId);
                            console.log('cariparty personowner : ', this.salesUnitRequirement.organizationOwner.idParty);
                            if (res.partyId === orgaOwner) {
                                this.disable = true;
                                this.isSameWithCustomer = true;
                                console.log('disable :', this.disable)
                            }
                        },
                        (err) => {
                            this.commonUtilService.showError(err);
                        }
                    )
                } else if (custId !== null && custId !== undefined) {
                    this.personalCustomerService.find(custId).subscribe(
                        (res: PersonalCustomer) => {
                            if (res.partyId !== orgaOwner) {
                                this.disable = false;
                                this.isSameWithCustomer = false;
                                console.log('disable :', this.disable)
                            }
                        },
                        (err) => {
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            }
        // }
    }

    public onChangeSameWithCustomer(e): void {
        const checked: boolean = e.checked;
        if (checked) {
            this.disable = true;
            this.organizationCustomerService.find(this.salesUnitRequirement.customerId).subscribe(
                (res) => {
                    let newOrganization: Organization;
                    newOrganization = res.organization;

                    if (newOrganization.dob != null) {
                        newOrganization.dob = new Date(newOrganization.dob);
                    }

                    this.salesUnitRequirement.organizationOwner = newOrganization;
                }
            )
        } else {
            this.salesUnitRequirement.organizationOwner = new Organization();
            this.disable = false;

        }
     }

     public createNewPerson(customerId): void {
        this.confirmationService.confirm({
            message: 'Are you sure do you want to change STNK data ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                try {
                    this.salesUnitRequirement.personOwner = new Person();
                } finally {
                    this.loadingService.loadingStop();
                }
            }
        })
     }

    //  getPersonalCustomer(salesUnitRequirement: SalesUnitRequirement): void {
    //     this.personalCustomerService.find(salesUnitRequirement.customerId).subscribe(
    //         (res) => {
    //             if (res.person.idParty === this.salesUnitRequirement.personOwner) {
    //                 this.disable = true;
    //                 console.log('id partynya ', this.salesUnitRequirement.personOwner);
    //             }
    //             this.disable = false;
    //         }
    //     )
    // }
    // getOrganizationCustomer(id: string): void {
    //     this.organizationCustomerService.find(id).subscribe(
    //         (res) => {
    //             this.organizationCustomer = res;
    //             console.log('getOrganizationCustomer', res);
    //         }
    //     )
    // }

     public findPersonByKTP(): void {
        let msg: string = null;
        this.loadingService.loadingStart();
        try {
            const obj: object = {
                personalIdNumber : this.ktpNumber
            }
            this.personService.findByPersonalIdNumber(obj).subscribe(
                (person: Person) => {
                    if (person.idParty === undefined) {
                        msg = 'Data yang anda cari tidak ditemukan';
                    } else {
                        this.salesUnitRequirement.personOwner = person;
                    }
                },
                (err) => {
                    this.commonUtilService.showError(err);
                }
            )
        } finally {
            this.loadingService.loadingStop();
            if (msg) {
                this.toasterService.showToaster('warn', 'Info', msg);
            }
        }
     }

     public checkCustomer(): string {
        return this.salesUnitRequirementService.checkCustomer(this.idRequestType);
    }
}
