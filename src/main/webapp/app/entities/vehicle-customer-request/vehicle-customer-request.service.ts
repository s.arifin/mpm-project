import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';
import { LeasingTenorProvide } from '../leasing-tenor-provide';
import { SaleTypeService } from '../sale-type';
import { PostalAddress } from '../postal-address';
import { Person } from '../person';

import { VehicleCustomerRequest } from './vehicle-customer-request.model';
import { SalesUnitRequirement, SalesUnitRequirementService } from '../sales-unit-requirement';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as BaseConstant from '../../shared/constants/base.constants';
import { Organization } from '../organization';

@Injectable()
export class VehicleCustomerRequestService {
   protected itemValues: VehicleCustomerRequest[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/vehicle-customer-requests';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/vehicle-customer-requests';

   constructor(
    protected http: Http,
    protected dateUtils: JhiDateUtils,
    protected saleTypeService: SaleTypeService,
    protected salesUnitRequirementService: SalesUnitRequirementService
) { }

   create(vehicleCustomerRequest: VehicleCustomerRequest): Observable<VehicleCustomerRequest> {
       const copy = this.convert(vehicleCustomerRequest);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(vehicleCustomerRequest: VehicleCustomerRequest): Observable<VehicleCustomerRequest> {
       const copy = this.convert(vehicleCustomerRequest);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<VehicleCustomerRequest> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   changeStatus(vehicleCustomerRequest: VehicleCustomerRequest, id: Number): Observable<VehicleCustomerRequest> {
       const copy = this.convert(vehicleCustomerRequest);
       return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
           .map((res: Response) => this.convertItemFromServer(res.json));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: VehicleCustomerRequest, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: VehicleCustomerRequest[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to VehicleCustomerRequest.
    */
   protected convertItemFromServer(json: any): VehicleCustomerRequest {
       const entity: VehicleCustomerRequest = Object.assign(new VehicleCustomerRequest(), json);
       if (entity.dateCreate) {
           entity.dateCreate = new Date(entity.dateCreate);
       }
       if (entity.dateRequest) {
           entity.dateRequest = new Date(entity.dateRequest);
       }
       return entity;
   }

   /**
    * Convert a VehicleCustomerRequest to a JSON which can be sent to the server.
    */
   protected convert(vehicleCustomerRequest: VehicleCustomerRequest): VehicleCustomerRequest {
       if (vehicleCustomerRequest === null || vehicleCustomerRequest === {}) {
           return {};
       }
       // const copy: VehicleCustomerRequest = Object.assign({}, vehicleCustomerRequest);
       const copy: VehicleCustomerRequest = JSON.parse(JSON.stringify(vehicleCustomerRequest));

       // copy.dateCreate = this.dateUtils.toDate(vehicleCustomerRequest.dateCreate);

       // copy.dateRequest = this.dateUtils.toDate(vehicleCustomerRequest.dateRequest);
       return copy;
   }

   public showSalesBrokerOrOrganization(idRequestType: number): string {
        let a: string;
        a = '';

        if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_DIRECT) {
            a = 'salesbroker';
        } else if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP || idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP_PERSON) {
            a = 'organization';
        }

        return a;
   }

   protected convertList(vehicleCustomerRequests: VehicleCustomerRequest[]): VehicleCustomerRequest[] {
       const copy: VehicleCustomerRequest[] = vehicleCustomerRequests;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: VehicleCustomerRequest[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }

    public validationSubmitToVSO(vehicleCustomerRequest: VehicleCustomerRequest, salesUnitRequirements: SalesUnitRequirement[]): string {
            let a: string;
            a = '';

            if (salesUnitRequirements.length > 0) {
                for (let i = 0; i < salesUnitRequirements.length; i++) {
                    const each: SalesUnitRequirement = salesUnitRequirements[i];
                    const sequence = i + 1;

                    if (each.productId === null) {
                        a += '<li>Pembelian - Belum pilih motor - pesanan nomor ' + sequence + '</li>';
                    }

                    if (each.colorId === null) {
                        a += '<li>Pembelian - Belum pilih warna - pesanan nomor ' + sequence + '</li>';
                    }

                    if (each.saleTypeId === null) {
                        a += '<li>Pembayaran - Belum pilih metode pembayaran - nomor pesanan ' + sequence + '</li>';
                    } else {
                        if (this.saleTypeService.checkIfCredit(each.saleTypeId) && each.leasingCompanyId === null) {
                            a += '<li>Pembayaran - Belum pilih leasing - nomor pesanan ' + sequence + '</li>';
                        }

                        if (this.saleTypeService.checkIfCredit(each.saleTypeId) && each.creditTenor === null) {
                            a += '<li>Pembayaran - Belum masukkan tenor - nomor pesanan ' + sequence + '</li>';
                        }
                        if (this.saleTypeService.checkIfCredit(each.saleTypeId) && each.creditDownPayment === null) {
                            a += '<li>Pembayaran - Belum masukkan down payment - nomor pesanan ' + sequence + '</li>';
                        }
                        if (this.saleTypeService.checkIfCredit(each.saleTypeId) && each.creditInstallment === null) {
                            a += '<li>Pembayaran - Belum masukkan cicilan - nomor pesanan ' + sequence + '</li>';
                        }
                    }

                    const buyer: object = this.salesUnitRequirementService.checkIfOrganization(+vehicleCustomerRequest.requestTypeId);

                    if (buyer['customer'] === 'customer') {
                        a += this.validationCustomer(each, sequence);
                        // validasi Customer
                        if (each.personCustomer === null || each.personCustomer === undefined) {
                            each.personCustomer = new Person;
                        }
                    }

                    if (buyer['stnk'] === 'organization') {
                        a += this.validationSTNKOrganization(each, sequence);
                    } else if (buyer['stnk'] === 'person') {
                        a += this.validationSTNKPerson(each, sequence);
                    }
                }
                if (a !== '') {
                    a = '<ul>' + a + '</ul>';
                }
            } else {
                a += 'Anda belum memasukkan pesanan anda';
            }

            return a;
    }

    protected validationCustomer(sur: SalesUnitRequirement, sequence: number): String {
        let a: String = '';

        if (this.saleTypeService.checkIfCredit(sur.saleTypeId)) {
            const person: Person = sur.personCustomer;
            if (this.salesUnitRequirementService.emptyFirstName(person)) {
                a += '<li>Pembeli - Masukkan firstname - nomor pesanan ' + sequence + '</li>';
            }

            if (this.salesUnitRequirementService.emptyNIK(person)) {
                a += '<li>Pembeli - Masukkan NIK - nomor pesanan ' + sequence + '</li>';
            }

            if (this.salesUnitRequirementService.emptyDOB(person)) {
                a += '<li>Pembeli - Masukkan tanggal lahir - nomor pesanan ' + sequence + '</li>';
            }

            if (this.salesUnitRequirementService.emptyNoHP(person)) {
                a += '<li>Pembeli - Masukkan nomor handphone - nomor pesanan ' + sequence + '</li>';
            }

            if (this.salesUnitRequirementService.emptyAddress(person.postalAddress)) {
                a += '<li>Pembeli - Masukkan alamat - nomor pesanan ' + sequence + '</li>';
            }
        }

        return a;
    }

    protected validationSTNKOrganization(sur: SalesUnitRequirement, sequence: number): String {
        let a: String = '';
        a = '';

        if (!sur.waitStnk) {
            const postalAddress: PostalAddress = sur.organizationOwner.postalAddress;
            const organization: Organization = sur.organizationOwner;

            if (this.salesUnitRequirementService.emptyOrganizationName(organization)) {
                a += '<li>STNK - Masukkan nama perusahaan - nomor pesanan ' + sequence + '</li>';
            }

            if (this.salesUnitRequirementService.emptyOrganizationPhone(organization)) {
                a += '<li>STNK - Masukkan no telp perusahaan - nomor pesanan ' + sequence + '</li>';
            }
        }

        return a;
    }

    protected validationSTNKPerson(sur: SalesUnitRequirement, sequence: number): String {
        let a: String = '';

        if (sur.waitStnk) {

        } else {
            const postalAddress: PostalAddress = sur.personOwner.postalAddress;
            const person: Person = sur.personOwner;
            if (this.saleTypeService.checkIfCredit(sur.saleTypeId)) {
                if (this.salesUnitRequirementService.emptyCity(postalAddress)) {
                    a += '<li>STNK - Pilih Kota - nomor pesanan ' + sequence + '</li>';
                }

                if (this.salesUnitRequirementService.emptyDOB(person)) {
                    a += '<li>STNK - Pilih tanggal lahir - nomor pesanan ' + sequence + '</li>';
                }

                if (this.salesUnitRequirementService.emptyPhoneNumber(person)) {
                    a += '<li>STNK - Masukkan no HP / no Telp - nomor pesanan ' + sequence + '</li>';
                }

                if (this.salesUnitRequirementService.emptyNIK(person)) {
                    a += '<li>STNK - Masukkan no KTP - nomor pesanan ' + sequence + '</li>';
                }

                if (this.salesUnitRequirementService.emptyReligion(person)) {
                    a += '<li>STNK - pilih agama - nomor pesanan ' + sequence + '</li>'
                }
            } else {
                if (this.salesUnitRequirementService.emptyCity(postalAddress)) {
                    a += '<li>STNK - Pilih Kota - nomor pesanan ' + sequence + '</li>';
                }
            }
        }

        return a;
    }
}
