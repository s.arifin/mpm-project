// import { OrganizationCustomer } from './../organization-customer/organization-customer.model';
// import { PersonalCustomer } from './../personal-customer/personal-customer.model';
import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { Person, PersonService } from '../person';
// import { PersonalCustomerService } from '../personal-customer';
import { SalesUnitRequirementService, SalesUnitRequirement } from '../sales-unit-requirement';
import { LoadingService } from '../../layouts';
import { CommonUtilService, ToasterService } from '../../shared';
import { ConfirmationService } from 'primeng/primeng';
import { PersonalCustomerService, PersonalCustomer, OrganizationCustomer, OrganizationCustomerService } from './../shared-component';

@Component({
    selector: 'jhi-sur-person',
    templateUrl: './sur-person.component.html'
})

export class SurPersonComponent implements OnChanges {
    @Input()
    public salesUnitRequirement: SalesUnitRequirement;

    @Input()
    public idRequestType: number;

    public ktpNumber: string;
    public isSameWithCustomer: boolean;
    public tutup: boolean;

    constructor(
        protected toasterService: ToasterService,
        protected commonUtilService: CommonUtilService,
        protected loadingService: LoadingService,
        protected personService: PersonService,
        protected personalCustomerService: PersonalCustomerService,
        protected confirmationService: ConfirmationService,
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected organizationCustomerService: OrganizationCustomerService,
    ) {
        this.isSameWithCustomer = false;
        this.tutup = false;
    }

    ngOnChanges(change: SimpleChanges) {
        if (change['salesUnitRequirement']) {
        }
        if (change['salesUnitRequirement']) {
            // this.idReTypeReady = true;
            this.load();
        }

        if (change['idRequestType']) {
            // this.idReTypeReady = true;
            this.load();
        }
    }

    protected load(): void {
        // if (this.idReTypeReady && this.surReady) {
            const custId: string = this.salesUnitRequirement.customerId;
            const personOwner: string = this.salesUnitRequirement.personOwner.idParty;
            if (custId !== null && custId !== undefined) {
                const buyer: string = this.checkCustomer();
                const owner: string = this.checkSTNK();
                if (buyer === 'person' && owner === 'person') {
                    this.personalCustomerService.find(custId).subscribe(
                        (res: PersonalCustomer) => {
                            console.log('res.cariparty customer : ', res.partyId);
                            console.log('cariparty personowner : ', this.salesUnitRequirement.personOwner.idParty);
                            if (res.partyId === personOwner) {
                                this.tutup = true;
                                this.isSameWithCustomer = true;
                                console.log('disable :', this.tutup)
                            }
                        },
                        (err) => {
                            this.commonUtilService.showError(err);
                        }
                    )
                }  else {
                    this.tutup = false;
                    this.isSameWithCustomer = false;
                }
                // else if (custId !== null && custId !== undefined) {
                //     this.personalCustomerService.find(custId).subscribe(
                //         (res: OrganizationCustomer) => {
                //             if (res.partyId !== personOwner) {
                //                 this.tutup = false;
                //                 this.isSameWithCustomer = false;
                //                 console.log('disable :', this.tutup)
                //             }
                //         },
                //         (err) => {
                //             this.commonUtilService.showError(err);
                //         }
                //     )
                // }
            }
        // }
    }

    public onChangeSameWithCustomer(e): void {
        console.log('salesUnitRequirement', this.salesUnitRequirement);
        const checked: boolean = e.checked;
        if (checked) {
            this.tutup = true;
            this.personalCustomerService.find(this.salesUnitRequirement.customerId).subscribe(
                (res) => {
                    let newPerson: Person;
                    newPerson = res.person;

                    if (newPerson.dob != null) {
                        newPerson.dob = new Date(newPerson.dob);
                    }

                    this.salesUnitRequirement.personOwner = newPerson;
                }
            )
        } else {
            this.salesUnitRequirement.personOwner = new Person();
            this.tutup = false;
        }
     }

     public disableSearchButton(): boolean {
         if (this.ktpNumber === '' || this.ktpNumber === undefined) {
             return true;
         } else {
             return false;
         }
     }

     public createNewPerson(): void {
        this.confirmationService.confirm({
            message: 'Are you sure do you want to change STNK data ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                try {
                    this.salesUnitRequirement.personOwner = new Person();
                } finally {
                    this.loadingService.loadingStop();
                }
            }
        })
     }

     public findPersonByKTP(): void {
        let msg: string = null;
        this.loadingService.loadingStart();
        try {
            const obj: object = {
                personalIdNumber : this.ktpNumber
            }
            this.personService.findByPersonalIdNumber(obj).subscribe(
                (person: Person) => {
                    if (person.idParty === undefined) {
                        msg = 'Data yang anda cari tidak ditemukan';
                    } else {
                        this.salesUnitRequirement.personOwner = person;
                    }
                },
                (err) => {
                    this.commonUtilService.showError(err);
                }
            )
        } finally {
            this.loadingService.loadingStop();
            if (msg) {
                this.toasterService.showToaster('warn', 'Info', msg);
            }
        }
     }
     public checkCustomer(): string {
        return this.salesUnitRequirementService.checkCustomer(this.idRequestType);
    }
    public checkSTNK(): string {
        return this.salesUnitRequirementService.checkSTNK(this.idRequestType);
    }
}
