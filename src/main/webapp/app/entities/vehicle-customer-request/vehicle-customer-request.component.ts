import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { VehicleCustomerRequest } from './vehicle-customer-request.model';
import { VehicleCustomerRequestService } from './vehicle-customer-request.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import { PersonalCustomerService } from '../personal-customer';
import { OrganizationCustomerService } from '../organization-customer';
import { SalesBrokerService } from '../sales-broker';

import * as BaseConstant from '../../shared/constants/base.constants';

@Component({
   selector: 'jhi-vehicle-customer-request',
   templateUrl: './vehicle-customer-request.component.html'
})
export class VehicleCustomerRequestComponent implements OnInit, OnDestroy {

    currentAccount: any;
    vehicleCustomerRequests: VehicleCustomerRequest[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    idInternal: string;

    newPersonal: Subscription;
    newOrganization: Subscription;

    constructor(
        protected vehicleCustomerRequestService: VehicleCustomerRequestService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected personalCustomerService: PersonalCustomerService,
        protected organizationCustomerService: OrganizationCustomerService,
        protected salesBrokerService: SalesBrokerService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
                    this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
    }

    loadAll() {
        if (this.currentSearch) {
            this.vehicleCustomerRequestService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.vehicleCustomerRequestService.queryFilterBy({
            idInternal: this.idInternal,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/vehicle-customer-request'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/vehicle-customer-request', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/vehicle-customer-request', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

   ngOnInit() {
        this.idInternal = this.principal.getIdInternal();
       this.loadAll();
       this.principal.identity(true).then((account) => {
           this.currentAccount = account;
       });
       this.registerChangeInVehicleCustomerRequests();
   }

   ngOnDestroy() {
       this.eventManager.destroy(this.eventSubscriber);
       this.newOrganization.unsubscribe();
       this.newPersonal.unsubscribe();
   }

    trackId(index: number, item: VehicleCustomerRequest) {
       return item.idRequest;
    }

    addNewGroup(customer) {
        const n: VehicleCustomerRequest = new VehicleCustomerRequest();
        n.customerId = customer.idCustomer;
        n.idInternal = this.idInternal;
        n.requestTypeId = 101;
        this.vehicleCustomerRequestService.create(n).subscribe((r) => {
            this.loadAll();
        });
    }

    addNewPersonal(customer) {
        const n: VehicleCustomerRequest = new VehicleCustomerRequest();
        n.customerId = customer.idCustomer;
        n.idInternal = this.idInternal;
        n.requestTypeId = 102;
        this.vehicleCustomerRequestService.create(n).subscribe((r) => {
            this.loadAll();
        });
    }

    registerChangeInVehicleCustomerRequests() {
       this.eventSubscriber = this.eventManager.subscribe('vehicleCustomerRequestListModification', (response) => this.loadAll());
        this.newOrganization = this.organizationCustomerService.newCustomer.subscribe((r) => {
            this.addNewGroup(r);
        });
        this.newPersonal = this.personalCustomerService.newCustomer.subscribe((r) => {
            this.addNewPersonal(r);
        });
    }

   sort() {
       const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
       if (this.predicate !== 'idRequest') {
           result.push('idRequest');
       }
       return result;
   }

   protected onSuccess(data, headers) {
       this.links = this.parseLinks.parse(headers.get('link'));
       this.totalItems = headers.get('X-Total-Count');
       this.queryCount = this.totalItems;
       // this.page = pagingParams.page;
       this.vehicleCustomerRequests = data;
   }

   protected onError(error) {
       this.jhiAlertService.error(error.message, null, null);
   }

   executeProcess(item) {
       this.vehicleCustomerRequestService.executeProcess(item).subscribe(
          (value) => console.log('this: ', value),
          (err) => console.log(err),
          () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
       );
   }

   loadDataLazy(event: LazyLoadEvent) {
       this.itemsPerPage = event.rows;
       this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
       this.previousPage = this.page;

       if (event.sortField !== undefined) {
           this.predicate = event.sortField;
           this.reverse = event.sortOrder;
       }
       this.loadAll();
   }

   updateRowData(event) {
       if (event.data.id !== undefined) {
           this.vehicleCustomerRequestService.update(event.data)
               .subscribe((res: VehicleCustomerRequest) =>
                   this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
       } else {
           this.vehicleCustomerRequestService.create(event.data)
               .subscribe((res: VehicleCustomerRequest) =>
                   this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
       }
   }

   protected onRowDataSaveSuccess(result: VehicleCustomerRequest) {
       this.toaster.showToaster('info', 'VehicleCustomerRequest Saved', 'Data saved..');
   }

   protected onRowDataSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.onError(error);
   }

   delete(data: VehicleCustomerRequest) {
       this.confirmationService.confirm({
           message: 'Are you sure that you want to delete?',
           header: 'Confirmation',
           icon: 'fa fa-question-circle',
           accept: () => {
               this.vehicleCustomerRequestService.changeStatus(data, BaseConstant.Status.STATUS_CANCEL).subscribe(
                   (Response) => {
                    this.eventManager.broadcast({
                        name: 'vehicleCustomerRequestListModification',
                        content: 'Deleted an vehicleCustomerRequest'
                    });
                   }
               )
            //    this.vehicleCustomerRequestService.delete(id).subscribe((response) => {
            //        this.eventManager.broadcast({
            //            name: 'vehicleCustomerRequestListModification',
            //            content: 'Deleted an vehicleCustomerRequest'
            //        });
            //    });
           }
       });
   }

   process() {
       this.vehicleCustomerRequestService.process({}).subscribe((r) => {
           console.log('result', r);
           this.toaster.showToaster('info', 'Data Processed', 'processed.....');
       });
   }

}
