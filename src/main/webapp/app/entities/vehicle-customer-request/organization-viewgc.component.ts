import { Component, Input, OnInit, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { PostalAddress } from '../postal-address';

import { Organization, OrganizationService } from '../organization';
import { OrganizationCustomer, OrganizationCustomerService } from '../shared-component';

@Component({
    selector: 'jhi-organization-viewgc',
    templateUrl: './organization-viewgc.component.html'
})
export class OrganizationViewgcComponent implements OnInit, OnDestroy, OnChanges {

    @Input() organization: Organization = new Organization();
    @Input() readonly: boolean;

    constructor(
        protected organizationCustomerService: OrganizationCustomerService,
    ) {
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['organization']) {
            console.log('organization view global', this.organization);
        }
    }

    ngOnDestroy() {
    }

    getProvince(province: any) {
        this.organization.postalAddress.provinceId = province;
    }

    getDistrict(district: any) {
        this.organization.postalAddress.districtId = district;
    }

    getCity(city: any) {
        this.organization.postalAddress.cityId = city;
    }

    getVillage(village: any ) {
        this.organization.postalAddress.villageId = village;
    }
}
