import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { VehicleCustomerRequestComponent } from './vehicle-customer-request.component';
import { VehicleCustomerRequestEditComponent } from './vehicle-customer-request-edit.component';
import { VehicleCustomerRequestPopupComponent } from './vehicle-customer-request-dialog.component';
import { SurRequestEditComponent } from './sur-request-edit.component';
import { SalesUnitRequirement, SalesUnitRequirementService } from '../sales-unit-requirement';
import { Organization } from '../organization';
import { Person } from '../person';

@Injectable()
export class VehicleCustomerRequestResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'requestNumber,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

@Injectable()
export class SalesUnitRequirementResolve implements Resolve<any> {
    constructor(protected service: SalesUnitRequirementService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        const idRequest = route.params['idRequest'] ? route.params['idRequest'] : null;
        const idInternal = route.params['idInternal'] ? route.params['idInternal'] : null;
        const idRequestType = route.params['idRequestType'] ? route.params['idRequestType'] : null;
        const idCustomer = route.params['idCustomer'] ? route.params['idCustomer'] : null;
        const idSalesBroker = route.params['idSalesBroker'] ? route.params['idSalesBroker'] : null;

        if (id) {
            return this.service.find(id);
        }
        const result: SalesUnitRequirement = new SalesUnitRequirement();

        if (idRequest) {
            result.idRequest = idRequest;
        }

        if (idInternal) {
            result.internalId = idInternal;
        }

        if (idSalesBroker) {
            result.brokerId = idSalesBroker;
        }

        if (idRequestType) {
            result.idReqTyp = +idRequestType;

            const buyer: object = this.service.checkIfOrganization(+idRequestType);
            if (buyer['customer'] === 'organization') {
                result.customerId = idCustomer;
            }

            if (buyer['stnk'] === 'organization') {
                result.personOwner = undefined;
            } else {
                result.organizationOwner = undefined;
            }
        }

        return result;
    }
}

export const vehicleCustomerRequestRoute: Routes = [
    {
        path: 'vehicle-customer-request',
        component: VehicleCustomerRequestComponent,
        resolve: {
            'pagingParams': VehicleCustomerRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleCustomerRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sur-request-new/:idInternal/:idRequestType/:idRequest/:idSalesBroker/direct',
        component: SurRequestEditComponent,
        resolve: {
            salesUnitRequirement: SalesUnitRequirementResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sur-request-new/:idInternal/:idRequestType/:idRequest/:idCustomer/group',
        component: SurRequestEditComponent,
        resolve: {
            salesUnitRequirement: SalesUnitRequirementResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sur-request-edit/:id',
        component: SurRequestEditComponent,
        resolve: {
            salesUnitRequirement: SalesUnitRequirementResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sur-request/:route/:page/:id/edit',
        component: SurRequestEditComponent,
        resolve: {
            salesUnitRequirement: SalesUnitRequirementResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];

export const vehicleCustomerRequestPopupRoute: Routes = [
    {
        path: 'vehicle-customer-request-popup-new/:idInternal',
        component: VehicleCustomerRequestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleCustomerRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'vehicle-customer-request-new',
        component: VehicleCustomerRequestEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleCustomerRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vehicle-customer-request/:id/edit',
        component: VehicleCustomerRequestEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleCustomerRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vehicle-customer-request/:route/:page/:id/edit',
        component: VehicleCustomerRequestEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleCustomerRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vehicle-customer-request/:id/popup-edit',
        component: VehicleCustomerRequestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleCustomerRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
