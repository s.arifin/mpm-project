import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { SalesUnitRequirement } from '../sales-unit-requirement/sales-unit-requirement.model';
import { SalesUnitRequirementService } from '../sales-unit-requirement/sales-unit-requirement.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { SurOrganizationComponent } from './sur-organization.component';
import { LoadingService } from '../../layouts';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { PersonalCustomerService, PersonalCustomer, OrganizationCustomer, OrganizationCustomerService } from './../shared-component';

@Component({
    selector: 'jhi-sur-request-as-list',
    templateUrl: './sur-request-as-list.component.html'
})
export class SurRequestAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input()
    public idCustomer: string;

    @Input() idSalesman: string;
    @Input() idInternal: string;
    @Input() idSaleType: number;
    @Input() idBillTo: string;
    @Input() idShipTo: string;
    @Input() idSalesBroker: string;
    @Input() idFeature: number;
    @Input() idMotor: string;
    @Input() idRequest: string;

    @Input()
    public idRequestType: number;

    currentAccount: any;
    salesUnitRequirements: SalesUnitRequirement[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    personalCustomer: PersonalCustomer;
    organizationCustomer: OrganizationCustomer;
    public salesUnitRequirement: SalesUnitRequirement;
    val1: number;

    constructor(
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        protected personalCustomerService: PersonalCustomerService,
        protected organizationCustomerService: OrganizationCustomerService
    ) {
        this.idCustomer = null;
        this.idSalesBroker = null;
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'requirementNumber';
        this.reverse = 'asc';
        this.first = 0;
        this.personalCustomer = new PersonalCustomer();
        this.organizationCustomer = new OrganizationCustomer();
    }

    loadAll() {
        this.salesUnitRequirementService.queryFilterBy({
            idCustomer: this.idCustomer,
            idSalesman: this.idSalesman,
            idInternal: this.idInternal,
            idSaleType: this.idSaleType,
            idBillTo: this.idBillTo,
            idShipTo: this.idShipTo,
            idSalesBroker: this.idSalesBroker,
            idFeature: this.idFeature,
            idMotor: this.idMotor,
            idRequest: this.idRequest,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/sales-unit-requirement', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSalesUnitRequirements();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idCustomer']) {
            this.loadAll();
        }
        if (changes['idSalesman']) {
            this.loadAll();
        }
        if (changes['idInternal']) {
            this.loadAll();
        }
        if (changes['idSaleType']) {
            this.loadAll();
        }
        if (changes['idBillTo']) {
            this.loadAll();
        }
        if (changes['idShipTo']) {
            this.loadAll();
        }
        if (changes['idSalesBroker']) {
            this.loadAll();
        }
        if (changes['idFeature']) {
            this.loadAll();
        }
        if (changes['idMotor']) {
            this.loadAll();
        }
        if (changes['idRequest']) {
            this.loadAll();
        }
    }

    public disableBtnAddSUR(): boolean {
        let a: boolean;
        a = true;

        if (this.idCustomer !== null || this.idSalesBroker !== null) {
            a = false;
        }

        return a;
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SalesUnitRequirement) {
        return item.idRequirement;
    }

    registerChangeInSalesUnitRequirements() {
        this.eventSubscriber = this.eventManager.subscribe('salesUnitRequirementListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.salesUnitRequirements = data;
    }

    protected onError(error) {
        console.log('error', error);
        this.alertService.error(error.message, null, null);
    }

    executeProcess(id: number, param: string, item: SalesUnitRequirement) {
        this.salesUnitRequirementService.executeProcess(id, param, item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    previousState() {
        this.router.navigate(['vehicle-customer-request', 0, 1, this.salesUnitRequirement.idRequest, 'edit']);
    }

    duplicateData(data: SalesUnitRequirement) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to duplicate?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
            for (let i = 0; i < this.val1; i++) {
                this.loadingService.loadingStart();
                this.loadingService.loadingStart();
                this.salesUnitRequirementService.executeProcess(110, null, data)
                .subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'salesUnitRequirementListModification',
                        content: 'Duplicate an salesUnitRequirement'
                    });
                    this.loadAll();
                    this.loadingService.loadingStop();
                    this.val1 = 0;
                });
            }
        }
        });
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.subscribeToSaveResponse(
                this.salesUnitRequirementService.update(event.data));
        } else {
            this.subscribeToSaveResponse(
                this.salesUnitRequirementService.create(event.data));
        }
    }

    protected subscribeToSaveResponse(result: Observable<SalesUnitRequirement>) {
        result.subscribe((res: SalesUnitRequirement) => this.onSaveSuccess(res));
    }

    protected onSaveSuccess(result: SalesUnitRequirement) {
        this.eventManager.broadcast({ name: 'salesUnitRequirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'salesUnitRequirement saved !');
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.salesUnitRequirementService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'salesUnitRequirementListModification',
                        content: 'Deleted an salesUnitRequirement'
                    });
                });
            }
        });
    }

    newData() {
        if (this.idCustomer !== null && this.idCustomer !== undefined) {
            this.router.navigate(['sur-request-new', this.idInternal, this.idRequestType, this.idRequest, this.idCustomer, 'group' ]);
        } else if (this.idSalesBroker !== null && this.idSalesBroker !== undefined) {
            this.router.navigate(['sur-request-new', this.idInternal, this.idRequestType, this.idRequest, this.idSalesBroker, 'direct' ]);
        }
    }

    editData(id: string) {
        this.router.navigate(['sur-request-edit', id ]);
    }

}
