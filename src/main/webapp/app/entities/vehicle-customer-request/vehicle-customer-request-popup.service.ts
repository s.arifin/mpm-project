import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { VehicleCustomerRequest } from './vehicle-customer-request.model';
import { VehicleCustomerRequestService } from './vehicle-customer-request.service';

@Injectable()
export class VehicleCustomerRequestPopupService {
    protected ngbModalRef: NgbModalRef;

    idInternal: any;
    idCustomer: any;
    idRequestType: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected vehicleCustomerRequestService: VehicleCustomerRequestService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.vehicleCustomerRequestService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.vehicleCustomerRequestModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new VehicleCustomerRequest();
                    data.idInternal = this.idInternal;
                    data.customerId = this.idCustomer;
                    data.requestTypeId = null;
                    // data.requestTypeId = this.idRequestType;
                    this.ngbModalRef = this.vehicleCustomerRequestModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    vehicleCustomerRequestModalRef(component: Component, vehicleCustomerRequest: VehicleCustomerRequest): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.vehicleCustomerRequest = vehicleCustomerRequest;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idCustomer = this.idCustomer;
        modalRef.componentInstance.idRequestType = this.idRequestType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.vehicleCustomerRequestLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    vehicleCustomerRequestLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idCustomer = this.idCustomer;
        modalRef.componentInstance.idRequestType = this.idRequestType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
