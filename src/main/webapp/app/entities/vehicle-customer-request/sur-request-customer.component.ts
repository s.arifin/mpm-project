import { Component, OnChanges, SimpleChanges, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { PersonalCustomer, PersonalCustomerService } from '../personal-customer';
import { SalesUnitRequirementService, SalesUnitRequirement } from '../sales-unit-requirement';
import { CommonUtilService } from '../../shared';
import { OrganizationCustomer, OrganizationCustomerService } from '../organization-customer';

@Component({
    selector: 'jhi-sur-request-customer',
    templateUrl: './sur-request-customer.component.html'
 })

 export class SurRequestCustomerComponent implements OnChanges, OnDestroy, OnInit {
    @Input()
    public idRequestType: number;

    @Input()
    public salesUnitRequirement: SalesUnitRequirement;

    public personalCustomer: PersonalCustomer;
    public organizationCustomer: OrganizationCustomer;
    public readonly: boolean;

    protected surReady: boolean;
    protected idReTypeReady: boolean
    protected subscriptionPersonalCustomer: Subscription;

    constructor(
        protected personalCustomerService: PersonalCustomerService,
        protected organizationCustomerService: OrganizationCustomerService,
        protected commonUtilService: CommonUtilService,
        protected salesUnitRequirementService: SalesUnitRequirementService
    ) {
        this.surReady = false;
        this.idReTypeReady = false;
        this.readonly = true;
        this.personalCustomer = new PersonalCustomer();
        this.organizationCustomer = new OrganizationCustomer();
    }

    ngOnInit() {
        this.registerPersonalCustomer();
    }

    ngOnChanges(change: SimpleChanges) {
        if (change['salesUnitRequirement']) {
            this.surReady = true;
            this.load();
        }

        if (change['idRequestType']) {
            this.idReTypeReady = true;
            this.load();
        }
    }

    ngOnDestroy() {
        this.subscriptionPersonalCustomer.unsubscribe();
    }

    protected registerPersonalCustomer(): void {
        this.subscriptionPersonalCustomer = this.personalCustomerService.newCustomer.subscribe(
            (res: PersonalCustomer) => {
                this.salesUnitRequirement.customerId = res.idCustomer;
                this.load();
            }
        );
    }

    protected load(): void {
        if (this.idReTypeReady && this.surReady) {
            const custId: string = this.salesUnitRequirement.customerId;
            if (custId !== null && custId !== undefined) {
                const buyer: string = this.checkCustomer();
                if (buyer === 'organization') {
                    this.organizationCustomerService.find(custId).subscribe(
                        (res: OrganizationCustomer) => {
                            this.salesUnitRequirement.customerId = res.idCustomer;
                            this.organizationCustomer = res;
                        },
                        (err) => {
                            this.commonUtilService.showError(err);
                        }
                    )
                } else if (buyer === 'person') {
                    this.personalCustomerService.find(custId).subscribe(
                        (res: PersonalCustomer) => {
                            this.personalCustomer = res;
                        },
                        (err) => {
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            }
        }
    }

    public checkCustomer(): string {
        return this.salesUnitRequirementService.checkCustomer(this.idRequestType);
    }
 }
