import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import * as BaseConstant from '../../shared/constants/base.constants';

import { VehicleCustomerRequest } from './vehicle-customer-request.model';
import { VehicleCustomerRequestPopupService } from './vehicle-customer-request-popup.service';
import { VehicleCustomerRequestService } from './vehicle-customer-request.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { Customer, CustomerService } from '../customer';
import { RequestType, RequestTypeService } from '../request-type';
import { ResponseWrapper, Principal } from '../../shared';
import { Salesman, SalesmanService} from '../salesman';

import { SalesBrokerService, SalesBroker } from '../sales-broker';
import { OrganizationCustomerService, OrganizationCustomer } from '../organization-customer';
import { CommonUtilService } from '../../shared/utils/common.service';

@Component({
    selector: 'jhi-vehicle-customer-request-dialog',
    templateUrl: './vehicle-customer-request-dialog.component.html'
})
export class VehicleCustomerRequestDialogComponent implements OnInit {

    vehicleCustomerRequest: VehicleCustomerRequest;
    isSaving: boolean;
//    idInternal: any;
    idCustomer: any;
    idRequestType: any;
    internals: Internal[];
    customers: Customer[];
    requesttypes: RequestType[];
    korsals: Salesman[];
    salesmans: Salesman[];

    public organizationCustomers: Array<OrganizationCustomer>;
    public showSalesBroker: boolean;
    public showSalesman: boolean;
    public showOrganization: boolean;
    public notSelectedRequestType: boolean;
    public brokers: Array<SalesBroker>;

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected vehicleCustomerRequestService: VehicleCustomerRequestService,
        protected internalService: InternalService,
        protected customerService: CustomerService,
        protected organizationCustomerService: OrganizationCustomerService,
        protected requestTypeService: RequestTypeService,
        protected eventManager: JhiEventManager,
        protected principal: Principal,
        protected toaster: ToasterService,
        protected salesBrokerService: SalesBrokerService,
        protected commonUtilService: CommonUtilService,
        protected salesmanService: SalesmanService
    ) {
        this.showSalesBroker = false;
        this.showOrganization = false;
        this.showSalesman = false;
        this.notSelectedRequestType = true;
        this.korsals = [];
        this.salesmans = [];
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.customerService.query({size: 99999})
        //     .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.loadOrganizationCustomers();
        this.salesBrokerService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => {
                this.brokers = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json));
        this.loadRequestType();
        this.loadKorsal()
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    protected loadOrganizationCustomers(): void {
        this.organizationCustomerService.query({size: 99999}).subscribe(
            (res: ResponseWrapper) => {
                this.organizationCustomers = res.json;
            },
            (err) => {
                this.commonUtilService.showError(err);
            }
        )
    }

    protected  loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('tarik data korsal', res.json);
                this.korsals = res.json;
            }
        )
    }

    public selectSalesByKorsal(): void {
        const obj: object = {
            idCoordinator : this.vehicleCustomerRequest.selectedKorsal,
            size : 9999
        }
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper,
            ) => {
                console.log('selected Sales' , res.json);
                this.salesmans = res.json;
            }
        )
    }

    protected resetShowCustomerAndSalesBroker(): void {
        this.showSalesBroker = false;
        this.showOrganization = false;
        this.showSalesman = false;
        this.notSelectedRequestType = true;
    }

    public selectRequestType(idRequestType: number): void {
        this.resetShowCustomerAndSalesBroker();
        if (this.vehicleCustomerRequestService.showSalesBrokerOrOrganization(idRequestType) === 'salesbroker') {
            this.showSalesBroker = true;
            this.showSalesman = true;
            this.notSelectedRequestType = false;
            this.vehicleCustomerRequest.customerId = null;
        } else if (this.vehicleCustomerRequestService.showSalesBrokerOrOrganization(idRequestType) === 'organization') {
            this.vehicleCustomerRequest.salesBrokerId = null
            this.showOrganization = true;
            this.showSalesman = true;
            this.notSelectedRequestType = false;
        } else {
            this.vehicleCustomerRequest.salesBrokerId = null;
            this.vehicleCustomerRequest.customerId = null;
        }
    }

    protected loadRequestType() {
        this.requestTypeService.query()
            .subscribe((res: ResponseWrapper) => {
                const rs: Array<RequestType> = new Array<RequestType>();
                let arr: Array<RequestType> = new Array<RequestType>();
                arr = res.json;

                arr.forEach(
                    (each: RequestType) => {
                        const idRequestType: number = each.idRequestType;
                        if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_DIRECT || idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP || idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP_PERSON
                            || idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP_REKANAN || idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP_INTERNAL) {
                            rs.push(each);
                        }
                    }
                )

                this.requesttypes = rs;
            }, (res: ResponseWrapper) => this.onError(res.json));
    }

    save() {
        this.isSaving = true;
        if (this.vehicleCustomerRequest.idRequest !== undefined) {
            this.subscribeToSaveResponse(
                this.vehicleCustomerRequestService.update(this.vehicleCustomerRequest));
        } else {
            this.subscribeToSaveResponse(
                this.vehicleCustomerRequestService.create(this.vehicleCustomerRequest));
        }
    }

    protected subscribeToSaveResponse(result: Observable<VehicleCustomerRequest>) {
        result.subscribe((res: VehicleCustomerRequest) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: VehicleCustomerRequest) {
        this.eventManager.broadcast({ name: 'vehicleCustomerRequestListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vehicleCustomerRequest saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'vehicleCustomerRequest Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }

    trackRequestTypeById(index: number, item: RequestType) {
        return item.idRequestType;
    }
}

@Component({
    selector: 'jhi-vehicle-customer-request-popup',
    template: ''
})
export class VehicleCustomerRequestPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected vehicleCustomerRequestPopupService: VehicleCustomerRequestPopupService
    ) {}

    ngOnInit() {
        this.vehicleCustomerRequestPopupService.idInternal = undefined;
        this.vehicleCustomerRequestPopupService.idCustomer = undefined;
        this.vehicleCustomerRequestPopupService.idRequestType = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vehicleCustomerRequestPopupService
                    .open(VehicleCustomerRequestDialogComponent as Component, params['id']);
            } else if ( params['idInternal'] ) {
                this.vehicleCustomerRequestPopupService.idInternal = params['idInternal'];
                this.vehicleCustomerRequestPopupService
                    .open(VehicleCustomerRequestDialogComponent as Component);
            } else {
                this.vehicleCustomerRequestPopupService
                    .open(VehicleCustomerRequestDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
