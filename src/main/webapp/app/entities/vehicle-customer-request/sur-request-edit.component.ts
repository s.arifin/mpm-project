import { Party } from './../party/party.model';
import { Component, OnInit, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { CurrencyPipe, DatePipe } from '@angular/common';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SalesUnitRequirementService, SalesUnitRequirement, Subsidi } from '../sales-unit-requirement';
import { ToasterService} from '../../shared';
import { Customer, CustomerService } from '../customer';
import { Salesman, SalesmanService } from '../salesman';
import { Internal, InternalService } from '../internal';
import { SaleType, SaleTypeService } from '../sale-type';
import { BillTo, BillToService } from '../bill-to';
import { ShipTo, ShipToService } from '../ship-to';
import { SalesBroker, SalesBrokerService } from '../sales-broker';
import { Feature, FeatureService } from '../feature';
import { Motor, MotorService } from '../motor';
import { ResponseWrapper } from '../../shared';
import { ConfirmationService } from 'primeng/primeng';
import { LeasingTenorProvideService, LeasingTenorProvide } from '../leasing-tenor-provide';
import { LeasingCompany, LeasingCompanyService } from '../leasing-company';

import { CommonUtilService, Principal} from '../../shared';
import { Person } from '../person';
import { PriceComponentService, PriceComponent } from '../price-component';
import { PersonalCustomerService } from '../../entities/personal-customer/personal-customer.service';
import { LoadingService } from '../../layouts/loading/loading.service';
import { Receipt, ReceiptService } from '../receipt';
import { OrganizationCustomer, OrganizationCustomerService } from '../shared-component';
import { RuleIndentService } from '../rule-indent';
import { RuleHotItemService } from '../rule-hot-item'

import * as reqType from '../../shared/constants/requirement-type.constants';
import * as SalesUnitRequirementConstants from '../../shared/constants/sales-unit-requirement.constants';
import * as PriceTypeConstants from '../../shared/constants/price-type.constants';
import * as BaseConstants from '../../shared/constants/base.constants';
import * as ProspectSourceConstants from '../../shared/constants/prospect-source.constants';
import * as _ from 'lodash';

@Component({
   selector: 'jhi-sur-request-edit',
   templateUrl: './sur-request-edit.component.html'
})
export class SurRequestEditComponent implements OnInit, OnDestroy, OnChanges {

    protected subscription: Subscription;
    salesUnitRequirement: SalesUnitRequirement;
    isSaving: boolean;
    idRequirement: any;
    paramPage: number;
    routeId: number;
    public isCredit: boolean;

    customers: Customer[];
    salesmen: Salesman[];
    internals: Internal[];
    saletypes: SaleType[];
    billtos: BillTo[];
    shiptos: ShipTo[];
    salesbrokers: SalesBroker[];
    features: Feature[];
    motors: Motor[];
    years: Array<number>;

    protected leasingData: Observable<any>;
    protected totalSubsidi: number;
    protected BBNPrice: number;
    protected HETPrice: number;
    protected subscriptionReceipt: Subscription;

    public subsidi: Subsidi;
    public fetchMotor: boolean;
    public organizationCustomer: OrganizationCustomer;
    public isCountReceipt: boolean;
    public totalReceipt: number;
    public installment: number;
    public remainingPayment: any;
    public remainingDownPayment: any;
    public isSameWithCustomer: boolean;
    public isReadOnlyDocument: boolean;
    public salesUnitRequirementId: number;
    public firstPrice: number;
    public idRequestType: number;

    isDisabledSubsidiFinCo: boolean;
    leasingCompany: LeasingCompany;
    leasingCompanies: LeasingCompany[];

    tempLeasingTenorProvides: LeasingTenorProvide[];
    leasingTenorProvides: LeasingTenorProvide[];
    selectedLeasingTenorProvide?: LeasingTenorProvide;
    tenors: Array<number>;
    public selectedMotor: any;
    public selectedTenor?: number = null;
    public motorsForSelect: Array<Object>;

constructor(
    protected alertService: JhiAlertService,
    protected salesUnitRequirementService: SalesUnitRequirementService,
    protected customerService: CustomerService,
    protected salesmanService: SalesmanService,
    protected internalService: InternalService,
    protected saleTypeService: SaleTypeService,
    protected billToService: BillToService,
    protected shipToService: ShipToService,
    protected salesBrokerService: SalesBrokerService,
    protected featureService: FeatureService,
    protected motorService: MotorService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected toaster: ToasterService,
    protected confirmationService: ConfirmationService,
    protected commontUtilService: CommonUtilService,
    protected organizationCustomerService: OrganizationCustomerService,
    protected priceComponentService: PriceComponentService,
    protected personalCustomerService: PersonalCustomerService,
    protected currencyPipe: CurrencyPipe,
    protected datePipe: DatePipe,
    protected leasingTenorProvideService: LeasingTenorProvideService,
    protected leasingCompanyService: LeasingCompanyService,
    protected receiptService: ReceiptService,
    protected loadingService: LoadingService,
    protected principalService: Principal,
    protected ruleIndentService: RuleIndentService,
    protected ruleHotItemService: RuleHotItemService
) {
    this.fetchMotor = true;
    this.salesUnitRequirement = new SalesUnitRequirement();
    this.routeId = 0;
    this.remainingPayment = '';
    this.remainingDownPayment = '';
    this.totalSubsidi = 0;
    this.totalReceipt = 0;
    this.BBNPrice = 0;
    this.HETPrice = 0;
    this.installment = 0;
    this.tempLeasingTenorProvides = new Array<LeasingTenorProvide>();
    this.isSameWithCustomer = false;
    this.isReadOnlyDocument = false;
    this.isCountReceipt = false;
    this.selectedLeasingTenorProvide = null;
    this.isCredit = false;
    this.motorsForSelect = new Array<Object>();
        this.subsidi = new Subsidi;
        this.subsidi.subsidiAHM = 0;
        this.subsidi.subsidiMD = 0;
}

ngOnChanges(change: SimpleChanges) {
    if (change['idReq']) {
        console.log('apa ini', this.salesUnitRequirementId)
        this.load(this.salesUnitRequirementId);
    }
}

ngOnInit() {
        this.getYear();
        this.route.data.subscribe(({ salesUnitRequirement }) => {
            this.salesUnitRequirement = salesUnitRequirement.body ? salesUnitRequirement.body : salesUnitRequirement;

            this.idRequestType = this.salesUnitRequirement.idReqTyp;

            const customerId: string = this.salesUnitRequirement.customerId;
            this.mappingSURWithCustomer(customerId).then(
                () => {
                    if (this.salesUnitRequirement.idRequirement !== null && this.salesUnitRequirement.idRequirement !== undefined) {
                        this.selectedMotor = this.salesUnitRequirement.productId;
                        console.log('ini id motor : ' , this.selectedMotor);
                        this.loadMotor().then(
                            () => {
                                this.selectMotor();
                            }
                        );
                    } else {
                        this.loadMotor();
                    }
                }
            )
        });
        this.isSaving = false;
        this.registerReceipt();
        this.leasingCompanyService.query()
        .subscribe((res: ResponseWrapper) => {this.leasingCompanies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.saleTypeService.query({size: 100})
            .subscribe((res: ResponseWrapper) => { this.saletypes = this.commontUtilService.getDataByParent(res.json, 1); }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipToService.query()
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesBrokerService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.salesbrokers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // console.log('cek stnk', this.checkSTNK());
        }

protected mappingSURWithCustomer(idCustomer: string): Promise<void> {
    return new Promise<void>(
        (resolve, reject) => {
            if (idCustomer !== null && idCustomer !== undefined) {
                this.customerService.find(idCustomer).subscribe(
                    (res: Customer) => {
                        this.salesUnitRequirement.partyId = res.partyId;
                        resolve();
                    }
                )
            }else {
                resolve();
            }
        }
    );
}

ngOnDestroy() {
    // this.subscription.unsubscribe();
    this.subscriptionReceipt.unsubscribe();
}

loadSUR() {
    this.salesUnitRequirementService.find(this.idRequirement).subscribe((salesUnitRequirement) => {
        this.salesUnitRequirement = salesUnitRequirement;
    });
}

public checkCustomer(): string {
    return this.salesUnitRequirementService.checkCustomer(this.salesUnitRequirement.idReqTyp);
}

public checkSTNK(): string {
    const hasil = this.salesUnitRequirementService.checkSTNK(this.salesUnitRequirement.idReqTyp);
    return hasil;
}

previousState() {
    this.router.navigate(['vehicle-customer-request', 0, 1, this.salesUnitRequirement.idRequest, 'edit']);
}

save() {
    this.isSaving = true;
    if (this.salesUnitRequirement.idRequirement !== undefined) {
        this.subscribeToSaveResponse(
            this.salesUnitRequirementService.update(this.salesUnitRequirement));
    } else {
        this.subscribeToSaveResponse(
            this.salesUnitRequirementService.create(this.salesUnitRequirement));
    }
}

protected subscribeToSaveResponse(result: Observable<SalesUnitRequirement>) {
    result.subscribe((res: SalesUnitRequirement) =>
        this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
}

protected onSaveSuccess(result: SalesUnitRequirement) {
    this.eventManager.broadcast({ name: 'salesUnitRequirementListModification', content: 'OK'});
    this.toaster.showToaster('info', 'Save', 'salesUnitRequirement saved !');
    this.isSaving = false;
    this.previousState();
}

protected onSaveError(error) {
    try {
        error.json();
    } catch (exception) {
        error.message = error.text();
    }
    this.isSaving = false;
    this.onError(error);
}

protected onError(error) {
    this.toaster.showToaster('warning', 'salesUnitRequirement Changed', error.message);
    this.alertService.error(error.message, null, null);
}

protected convertMotorForSelect(data: Array<Motor>) {
    this.motorsForSelect = this.motorService.convertMotorForSelectPrimeNg(data);
}

trackCustomerById(index: number, item: Customer) {
    return item.idCustomer;
}

activated() {
    this.salesUnitRequirementService.changeStatus(this.salesUnitRequirement, 11).delay(1000).subscribe((r) => {
        this.loadSUR();
        this.toaster.showToaster('info', 'Data Activated', 'Activated.....');
    });
}

approved() {
    this.salesUnitRequirementService.changeStatus(this.salesUnitRequirement, 12).delay(1000).subscribe((r) => {
        this.loadSUR();
        this.toaster.showToaster('info', 'Data Approved', 'Approved.....');
    });
}

completed() {
    this.salesUnitRequirementService.changeStatus(this.salesUnitRequirement, 17).delay(1000).subscribe((r) => {
        this.eventManager.broadcast({
            name: 'salesUnitRequirementListModification',
            content: 'Completed an salesUnitRequirement'
            });
            this.toaster.showToaster('info', 'Data Completed', 'Completed.....');
            this.previousState();
        });
}

canceled() {
    this.confirmationService.confirm({
        message: 'Are you sure that you want to cancel?',
        header: 'Confirmation',
        icon: 'fa fa-question-circle',
        accept: () => {
            this.salesUnitRequirementService.changeStatus(this.salesUnitRequirement, 13).delay(1000).subscribe((r) => {
                this.eventManager.broadcast({
                    name: 'salesUnitRequirementListModification',
                    content: 'Cancel an salesUnitRequirement'
                    });
                    this.toaster.showToaster('info', 'Data salesUnitRequirement cancel', 'Cancel salesUnitRequirement.....');
                    this.previousState();
                });
            }
        });
}

    public getYear(): void {
        this.years = new Array();
        this.years = this.salesUnitRequirementService.optionYearAssembly();
    }

    public isDisabledSubsidiDealer(sur: SalesUnitRequirement): boolean {
        if (sur.approvalDiscount === BaseConstants.Status.STATUS_WAITING_FOR_APPROVAL || sur.approvalDiscount === BaseConstants.Status.STATUS_APPROVED) {
            return true;
        } else {
            return false;
        }
    }

    protected resetLeasing(): void {
        this.salesUnitRequirement.leasingCompanyId = null;
        // this.selectLeasing();
        this.remainingDownPayment = this.salesUnitRequirementService.notSelectLeasingTenorProvideMessage;
    }

    public selectMotor(isSelect?: boolean): void {
        this.salesUnitRequirement.productId = this.selectedMotor;
        this.BBNPrice = this.salesUnitRequirement.bbnprice;
        this.HETPrice = this.salesUnitRequirement.hetprice;
        console.log('ini bbn ', this.BBNPrice);
        console.log('ini het ', this.HETPrice);
        this.getColorByMotor(this.salesUnitRequirement.productId);
        this.setIsHotItemAndIsIndent(this.salesUnitRequirement.productId);
        if (isSelect) {// dijalankan ketika melakukan pemilihan motor
            this.resetLeasing();
            const obj = {
                idproduct : this.salesUnitRequirement.productId,
                idinternal : this.salesUnitRequirement.internalId
            };
            this.motorService.getPrice(obj).subscribe(
                (res) => {
                    const priceComponents: PriceComponent[] = res.json;

                    // binding for subsidi AHM
                    const priceComponentSubsidiAHM: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.SUBSIDI_AHM]);
                    this.salesUnitRequirement.subsahm = 0;
                    if (priceComponentSubsidiAHM !== undefined) {
                    //     this.salesUnitRequirement.subsahm = priceComponentSubsidiAHM.price;
                        this.subsidi.subsidiAHM = priceComponentSubsidiAHM.price;
                    }

                    // binding for subsidi main dealer
                    const priceComponentSubsidiMD: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.SUBSIDI_MD]);
                    this.salesUnitRequirement.subsmd = 0;
                    if (priceComponentSubsidiMD !== undefined) {
                    //     this.salesUnitRequirement.subsmd = priceComponentSubsidiMD.price;
                        this.subsidi.subsidiMD = priceComponentSubsidiMD.price;
                    }

                    // binding for BBN price
                    const priceComponentBBN: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.BBN]);

                    this.salesUnitRequirement.bbnprice = 0;
                    if (priceComponentBBN !== undefined) {
                        this.BBNPrice = priceComponentBBN.price;
                        this.salesUnitRequirement.bbnprice = priceComponentBBN.price;
                    }

                    // binding for HET price
                    const priceComponentHET: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.HET]);
                    this.salesUnitRequirement.hetprice = 0;

                    if (priceComponentHET !== undefined) {
                        this.HETPrice = priceComponentHET.price;
                        this.salesUnitRequirement.hetprice = priceComponentHET.price;
                    }
                    // binding for unit price
                    this.salesUnitRequirement.unitPrice = this.priceComponentService.countMotorPrice(priceComponents);

                    this.firstPrice = this.priceComponentService.countMotorPrice(priceComponents);

                    this.setMinPayment();
                }, (err) => {
                   this.commontUtilService.showError(err);
                }
            );
        }
    }

    public setMinPayment() {
        const saleTypeId: number = this.salesUnitRequirement.saleTypeId;
        if (this.saleTypeService.checkIfCash(saleTypeId)) {
            this.salesUnitRequirement.minPayment = this.salesUnitRequirement.unitPrice;
        } else if (this.saleTypeService.checkIfCredit(saleTypeId)) {
            this.salesUnitRequirement.minPayment = this.salesUnitRequirement.creditDownPayment;
        } else if (this.saleTypeService.checkIfOffTheRoad(saleTypeId)) {
            this.salesUnitRequirement.minPayment = this.salesUnitRequirement.hetprice;
        }
    }

    public isDisableSelectMotor(sur: SalesUnitRequirement): boolean {
        return false;
    }

    public disableMakelarFee(): boolean {
        let isDisable: boolean;
        isDisable = true;

        if (this.salesUnitRequirement.prospectSourceId === ProspectSourceConstants.MAKELAR) {
            isDisable = false;
        }

        return isDisable;
    }

    public trackLeasingCompanyById(index: number, item: LeasingCompany): String {
        return item.idLeasingCompany;
    }

    public trackSalesmanById(index: number, item: Salesman) {
        return item.idPartyRole;
    }

    public trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    public trackSaleTypeById(index: number, item: SaleType) {
        return item.idSaleType;
    }

    public trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }

    public trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }

    public trackSalesBrokerById(index: number, item: SalesBroker) {
        return item.idPartyRole;
    }

    public trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    public trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }

    public trackLeasingTenorProvideById(index: number, item: LeasingTenorProvide) {
        return item.idLeasingProvide;
    }

    protected registerReceipt(): void {
        this.subscriptionReceipt = this.receiptService.values.subscribe(
            (res) => {
                this.countReceipt(res);
            }
        );
    }

    public countReceipt(res): void {
        this.totalReceipt = this.receiptService.countTotalReceipt(res);
        this.countRemainingPayment();
        this.setRequirementNumber();
    }

    protected setRequirementNumber(): void {
        if (this.salesUnitRequirement.requirementNumber === null || this.salesUnitRequirement.requirementNumber === undefined) {
            this.salesUnitRequirementService.find(this.salesUnitRequirement.idRequirement).subscribe(
                (res: SalesUnitRequirement) => {
                    this.salesUnitRequirement.requirementNumber = res.requirementNumber;
                },
                (err) => {
                    this.commontUtilService.showError(err);
                })
        }
    }

    public countTotalSubsidi(): void {
        this.totalSubsidi = this.salesUnitRequirementService.fnCountTotalSubsidi(this.salesUnitRequirement);
    }

    protected loadMotor(): Promise<Motor[]> {
        return new Promise<Motor[]>(
            (resolve) => {
                this.motorService.query({
                    page: 0,
                    size: 10000,
                    sort: ['idProduct', 'asc']
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.fetchMotor = false;
                        this.convertMotorForSelect(res.json);
                        this.motors = res.json;
                    },
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => {
                        resolve();
                });
            }
        );
    }

    filterSaleType(data: SaleType[], parentId?: number): Array<SaleType> {
        const _arr = Array();
        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const obj = data[i];
                if (obj.parentId === parentId) {
                    _arr.push(obj);
                }
            }
        }
        return _arr;
    }

    protected getMotorPriceByIdProduct(_idproduct: string): void {
        const obj = {
            idproduct : _idproduct,
            idInternal : this.salesUnitRequirement.internalId
        };
        this.motorService.getPrice(obj).subscribe(
            (res) => {
                const priceComponents: PriceComponent[] = res.json;

                // binding for BBN price
                const priceComponentBBN: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.BBN]);
                if (priceComponentBBN !== undefined) {
                    this.BBNPrice = priceComponentBBN.price;
                }

                // binding for HET price
                const priceComponentHET: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.HET]);
                if (priceComponentHET !== undefined) {
                    this.HETPrice = priceComponentHET.price;
                }

                this.firstPrice = this.priceComponentService.countMotorPrice(priceComponents);
            }
        )
    }

    protected setIsHotItemAndIsIndent(productId?: String): void {
        if (productId !== null) {
            const selectedProductId = this.salesUnitRequirement.productId;
            const motor: Motor = _.find(this.motors, function(e: Motor) {
                return e.idProduct === selectedProductId;
            });

            this.salesUnitRequirement.unitIndent = false;
            this.salesUnitRequirement.unitHotItem = false;

            // check indent
            this.ruleIndentService.checkByIdInternalAndIdProduct(
                {
                    idProduct: productId,
                    idInternal: this.principalService.getIdInternal()
                }
            ).subscribe(
                (res: Boolean) => {
                    if (res) {
                        this.salesUnitRequirement.unitIndent = true;
                    }
                }
            )

            // check hot item
            this.ruleHotItemService.checkByIdProductAndIdInternal(
                {
                    idproduct: productId,
                    idinternal: this.principalService.getIdInternal()
                }
            ).subscribe(
                (res: Boolean) => {
                    if (res) {
                        this.salesUnitRequirement.unitHotItem = true;
                    }
                }
            )
        }
    }

    public selectTenor(tenor: number): Promise<void> {
        return new Promise<void>(
            (resolve) => {
                this.selectedTenor = tenor;
                this.leasingTenorProvides = _.filter(this.tempLeasingTenorProvides, function(e) {
                    return e.tenor === tenor;
                });
                this.installment = 0;
                this.remainingDownPayment = this.salesUnitRequirementService.notSelectLeasingTenorProvideMessage;
                resolve();
            }
        )
    }

    public selectDownPayment(data: string): void {
        this.installment = 0;
        const objLeasingTenor: LeasingTenorProvide =  this.salesUnitRequirementService.fnGetSelectedLeasingTenorProvide(this.tempLeasingTenorProvides, data);

        this.installment = objLeasingTenor.installment;
        this.salesUnitRequirement.minPayment = objLeasingTenor.downPayment;
        this.countRemainingDownPayment();
    }

    public selectSaleType(): void {
        const saleTypeId = this.salesUnitRequirement.saleTypeId;
        this.countMotorPriceBySaleType(saleTypeId);
        if (this.saleTypeService.checkIfCash(saleTypeId)) { // Cash
            this.salesUnitRequirement.subsfincomp = 0;
            this.isCredit = false;
            this.salesUnitRequirement.leasingCompanyId = null;
            this.salesUnitRequirement.leasingTenorProvideId = null;
            this.salesUnitRequirement.creditTenor = 0;
            this.salesUnitRequirement.creditInstallment = 0;
            this.salesUnitRequirement.creditDownPayment = 0;
            this.salesUnitRequirement.unitPrice = this.HETPrice + this.BBNPrice;
            this.countRemainingPayment();

            this.salesUnitRequirement.bbnprice = this.BBNPrice;
            console.log('ini bbn gesh', this.BBNPrice)
            console.log('ini het gesh', this.HETPrice)
        } else if (this.saleTypeService.checkIfCredit(saleTypeId)) { // Credit Sales
            this.isCredit = true;
            this.countRemainingDownPayment();
            this.salesUnitRequirement.bbnprice = this.BBNPrice;
            this.salesUnitRequirement.unitPrice = this.HETPrice + this.BBNPrice;
            console.log('ini pilih credit', this.salesUnitRequirement.bbnprice)
        } else if (this.saleTypeService.checkIfOffTheRoad(saleTypeId)) { // Off The Road
            this.salesUnitRequirement.subsfincomp = 0;
            this.isCredit = false;
            this.salesUnitRequirement.leasingCompanyId = null;
            this.salesUnitRequirement.leasingTenorProvideId = null;
            this.salesUnitRequirement.creditTenor = 0;
            this.salesUnitRequirement.creditInstallment = 0;
            this.salesUnitRequirement.creditDownPayment = 0;
            this.salesUnitRequirement.bbnprice = 0;
            this.salesUnitRequirement.unitPrice = this.HETPrice;
            console.log('ini pilih offtheroad', this.salesUnitRequirement.bbnprice);
            this.countRemainingPayment();
        } else {
            this.isCredit = false;
            this.salesUnitRequirement.subsfincomp = 0;
        }
        this.setMinPayment();
    }

    protected checkIfDataDiffInternal(sur: SalesUnitRequirement): void {
        const internalId: String = this.principalService.getIdInternal();
        if (sur.internalId !== internalId) {
            this.previousState();
        }
    }

    load(id): void {
        this.loadingService.loadingStart();
        this.salesUnitRequirementService.find(id).subscribe((salesUnitRequirement: SalesUnitRequirement) => {
            this.selectedMotor = salesUnitRequirement.productId;
            console.log('cari idproduct', salesUnitRequirement.productId);
            this.checkIfDataDiffInternal(salesUnitRequirement);

            if (salesUnitRequirement.personOwner === null) {
                salesUnitRequirement.personOwner = new Person();
                salesUnitRequirement.personOwner.dob = null;
            }

            if (salesUnitRequirement.productId !== null) {
                salesUnitRequirement.productId = salesUnitRequirement.productId.toUpperCase();
                // this.getMotorPriceByIdProduct(salesUnitRequirement.productId);

                this.HETPrice = salesUnitRequirement.hetprice;
                this.BBNPrice = salesUnitRequirement.bbnprice;
                console.log('cari harga HET', this.HETPrice);
                console.log('cari harga BBN', this.BBNPrice);
                salesUnitRequirement.hetprice = this.HETPrice;
                salesUnitRequirement.bbnprice = this.BBNPrice;
                salesUnitRequirement.unitPrice = (this.HETPrice + this.BBNPrice);
            }

            if (salesUnitRequirement.idReqTyp === reqType.ORGANIZATION_CUSTOMER) {
                this.getDataByOrganization(salesUnitRequirement);
            } else if (salesUnitRequirement.idReqTyp === null) {
                this.getDataByPersonal(salesUnitRequirement);
            }
        });
    }

    protected getDataByOrganization(salesUnitRequirement: SalesUnitRequirement): void {
        this.organizationCustomerService.find(salesUnitRequirement.customerId).subscribe(
            (res) => {
                this.organizationCustomer = res;
                salesUnitRequirement.partyId = res.partyId;
                this.salesUnitRequirement = salesUnitRequirement;
                this.loadMotor().then(
                    () => {
                        this.selectMotor();
                        this.selectSaleType();
                    }
                )
                this.loadingService.loadingStop();
                // const saleTypeId: number = this.salesUnitRequirement.saleTypeId;
                // const selLeasingTenorProvide: string = this.salesUnitRequirement.leasingTenorProvideId;
                // if (this.saleTypeService.checkIfCredit(saleTypeId) && selLeasingTenorProvide) {
                    // get detail selected leasing tenor provide
                    // this.leasingTenorProvideService.find(this.salesUnitRequirement.leasingTenorProvideId).subscribe(
                    //     (resSelectedLeasingProvide) => {
                    //         this.selectedLeasingTenorProvide = resSelectedLeasingProvide;
                            // get all leasing
                            // this.selectLeasing().then(
                            //     () => {
                            //         // set selected tenor
                            //         this.selectedTenor = this.selectedLeasingTenorProvide.tenor;
                            //         this.selectTenor(this.selectedTenor).then(
                            //             () => {
                            //                 this.selectDownPayment(this.salesUnitRequirement.leasingTenorProvideId);
                            //                 this.loadingService.loadingStop();
                            //             }
                            //         );
                            //     }
                            // );
                        // }
                    // )
                // } else {

                // }
            },
            (err) => {
                this.commontUtilService.showError(err);
                this.loadingService.loadingStop();
            }
        )
    }

    protected getDataByPersonal(salesUnitRequirement: SalesUnitRequirement): void {
        this.personalCustomerService.find(salesUnitRequirement.customerId).subscribe(
            (res) => {
                salesUnitRequirement.partyId = res.partyId;
                this.salesUnitRequirement = salesUnitRequirement;
                this.loadMotor().then(
                    () => {
                        this.selectMotor();
                        this.selectSaleType();
                    }
                )
                this.loadingService.loadingStop();
                // const saleTypeId: number = this.salesUnitRequirement.saleTypeId;
                // const selLeasingTenorProvide: string = this.salesUnitRequirement.leasingTenorProvideId;
                // if (this.saleTypeService.checkIfCredit(saleTypeId) && selLeasingTenorProvide) {
                //     // get detail selected leasing tenor provide
                //     this.leasingTenorProvideService.find(this.salesUnitRequirement.leasingTenorProvideId).subscribe(
                //         (resSelectedLeasingProvide) => {
                //             this.selectedLeasingTenorProvide = resSelectedLeasingProvide;
                            // get all leasing
                            // this.selectLeasing().then(
                            //     () => {
                            //         // set selected tenor
                            //         this.selectedTenor = this.selectedLeasingTenorProvide.tenor;
                            //         this.selectTenor(this.selectedTenor).then(
                            //             () => {
                            //                 this.selectDownPayment(this.salesUnitRequirement.leasingTenorProvideId);
                            //                 this.loadingService.loadingStop();
                            //             }
                            //         );
                            //     }
                            // );
                //         }
                //     )
                // } else {

                // }
            },
            (err) => {
                this.commontUtilService.showError(err);
                this.loadingService.loadingStop();
            }
        )
    }

    protected countMotorPriceBySaleType(saleTypeId: number): void {
        if (this.saleTypeService.checkIfOffTheRoad(saleTypeId)) {
            this.salesUnitRequirement.unitPrice = this.firstPrice - this.salesUnitRequirement.bbnprice;
        } else {
        if ( this.salesUnitRequirement.unitPrice !== (this.salesUnitRequirement.bbnprice + this.salesUnitRequirement.hetprice)) {
            this.salesUnitRequirement.unitPrice = this.firstPrice;
        }
        }
        this.setMinPayment();
    }

    public countSubsidiDealer(): void {
        const saleTypeId = this.salesUnitRequirement.saleTypeId;
        if (this.saleTypeService.checkIfCash(saleTypeId) || saleTypeId === this.saleTypeService.checkIfOffTheRoad(saleTypeId)) { // Cash
            this.countRemainingPayment();
        } else if (this.saleTypeService.checkIfCredit(saleTypeId)) { // Credit Sales
            this.countRemainingDownPayment();
        }
    }

    // public countRemainingDownPayment(): void {
    //     const productPrice = this.salesUnitRequirement.unitPrice;
    //     if (productPrice !== null) {
    //         this.countTotalSubsidi();
    //         // const objLeasingTenor: LeasingTenorProvide = this.salesUnitRequirementService.fnGetSelectedLeasingTenorProvide(this.tempLeasingTenorProvides, data);
    //         let creditDownPayment: number = this.salesUnitRequirement.creditDownPayment;
    //         if (creditDownPayment === null || creditDownPayment === undefined) {
    //             creditDownPayment = 0;
    //         }

    //         let subsFinComp: number = this.salesUnitRequirement.subsfincomp;
    //         if (subsFinComp === null || subsFinComp === undefined) {
    //             subsFinComp = 0;
    //         }

    //         const totalPaymented = this.salesUnitRequirement.creditDownPayment - (this.totalReceipt + subsFinComp);
    //         // this.remainingDownPayment = this.currencyPipe.transform(objLeasingTenor.downPayment - totalPaymented, 'IDR', true);
    //         this.remainingDownPayment = this.currencyPipe.transform(totalPaymented , 'IDR', true);
    //         this.salesUnitRequirement.downPayment = this.totalSubsidi + this.totalReceipt;
    //     } else {
    //         this.remainingDownPayment = 'Please select motor';
    //     }
    // }

    public countRemainingDownPayment(): void {
        const productPrice = this.salesUnitRequirement.unitPrice;
        this.remainingDownPayment = this.salesUnitRequirementService.countRemainingDownPaymentForCredit(this.salesUnitRequirement, this.totalReceipt);
        if (productPrice !== null) {
            this.countTotalSubsidi();
            this.salesUnitRequirement.downPayment = this.totalSubsidi + this.totalReceipt;
        }
        this.setMinPayment();
        this.countRemainingPayment();
    }

    public countRemainingPayment(): void {
        const productPrice = this.salesUnitRequirement.unitPrice;
        console.log('ini unit pricenya :' , productPrice)
        if (productPrice !== null) {
            this.countTotalSubsidi();
            this.remainingPayment = this.currencyPipe.transform(productPrice - this.totalSubsidi - this.totalReceipt, 'IDR', true);

            this.salesUnitRequirement.downPayment = this.totalSubsidi + this.totalReceipt;
        } else {
            this.remainingPayment = 'Please select motor';
        }
    }

    public onChangeSameWithCustomer(e): void {
    const checked: boolean = e.checked;
    if (checked) {
            this.personalCustomerService.find(this.salesUnitRequirement.customerId).subscribe(
                (res) => {
                    let newPerson: Person;
                    newPerson = res.person;

                    if (newPerson.dob != null) {
                        newPerson.dob = new Date(newPerson.dob);
                    }

                    this.salesUnitRequirement.personOwner = newPerson;
                }
            )
    } else {
            this.salesUnitRequirement.personOwner = new Person();
    }
    }

    protected getColorByMotor(idProduct: string): void {
        if (idProduct !== null) {
            this.features = new Array<Feature>();
            const selectedProduct: Motor = _.find(this.motors, function(e) {
                return e.idProduct.toLowerCase() === idProduct.toLowerCase();
            });

            this.features = selectedProduct.features;
        }
    }

    protected checkPastTenorProvide(): Promise<void> {
        return new Promise<void>(
            (resolve) => {
                const tenorProvId = this.salesUnitRequirement.leasingTenorProvideId;

                let selectedLeasingTenorProvide: LeasingTenorProvide;
                selectedLeasingTenorProvide = this.salesUnitRequirementService.fnGetSelectedLeasingTenorProvide(this.tempLeasingTenorProvides, tenorProvId);

                if (selectedLeasingTenorProvide === undefined && this.selectedLeasingTenorProvide !== null) {
                    this.tempLeasingTenorProvides.push(this.selectedLeasingTenorProvide);
                }
                resolve();
            }
        )
    }

    protected getTenor(data: any): void {
        if (data.length > 0) {
            const leasingTenorOrderByTenor: LeasingTenorProvide[] = _.orderBy(data, ['tenor'], ['asc']);
            this.tenors = _.map(_.uniqBy(leasingTenorOrderByTenor, 'tenor'), function(e) {
                return e.tenor;
            });
        }
    }

    public onMotorChange(motor?: string): void {
        // Update Warna Motor
        this.getColorByMotor(motor);
        this.setIsHotItemAndIsIndent(motor);
        if (motor) {// dijalankan ketika melakukan pemilihan motor
            const obj = {
                idproduct : motor,
                idinternal : this.salesUnitRequirement.internalId
            };
            this.motorService.getPrice(obj).subscribe(
                (res) => {
                    const priceComponents: PriceComponent[] = res.json;

                    // binding for subsidi AHM
                    const priceComponentSubsidiAHM: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.SUBSIDI_AHM]);

                    this.salesUnitRequirement.subsahm = 0;
                    if (priceComponentSubsidiAHM !== undefined) {
                        this.salesUnitRequirement.subsahm = priceComponentSubsidiAHM.price;
                    }
                    // binding for subsidi main dealer
                    const priceComponentSubsidiMD: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.SUBSIDI_MD]);
                    this.salesUnitRequirement.subsmd = 0;
                    if (priceComponentSubsidiMD !== undefined) {
                        this.salesUnitRequirement.subsmd = priceComponentSubsidiMD.price;
                    }

                    // binding for BBN price
                    const priceComponentBBN: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.BBN]);
                    this.salesUnitRequirement.bbnprice = 0;
                    if (priceComponentBBN !== undefined) {
                        this.BBNPrice = priceComponentBBN.price;
                        this.salesUnitRequirement.bbnprice = priceComponentBBN.price;
                    }

                    // binding for HET price
                    const priceComponentHET: PriceComponent = _.find(priceComponents, ['idPriceType', PriceTypeConstants.HET]);
                    this.salesUnitRequirement.hetprice = 0;
                    if (priceComponentHET !== undefined) {
                        this.HETPrice = priceComponentHET.price;
                        this.salesUnitRequirement.hetprice = priceComponentHET.price;
                    }
                    // binding for unit price
                    this.salesUnitRequirement.unitPrice = this.priceComponentService.countMotorPrice(priceComponents);
                    this.firstPrice = this.priceComponentService.countMotorPrice(priceComponents);
                    this.setMinPayment();
                }
            );
        }
    }

}
