import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { VehicleCustomerRequest } from './vehicle-customer-request.model';
import { VehicleCustomerRequestService } from './vehicle-customer-request.service';
import { ToasterService, CommonUtilService} from '../../shared';
import { ReportUtilService} from '../../shared/report/report-util.service';
import { Internal, InternalService } from '../internal';
import { OrganizationCustomer, OrganizationCustomerService } from '../organization-customer';
import { SalesBroker, SalesBrokerService } from '../sales-broker';
import { RequestType, RequestTypeService } from '../request-type';
import { ResponseWrapper } from '../../shared';
import { ConfirmationService } from 'primeng/primeng';
import { SalesUnitRequirementService, SalesUnitRequirement } from '../sales-unit-requirement';
import { Salesman, SalesmanService} from '../salesman';

@Component({
   selector: 'jhi-vehicle-customer-request-edit',
   templateUrl: './vehicle-customer-request-edit.component.html'
})
export class VehicleCustomerRequestEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    protected subscriptionSalesUnitRequirement: Subscription;
    protected salesUnitRequirements: Array<SalesUnitRequirement>;

    vehicleCustomerRequest: VehicleCustomerRequest;
    isSaving: boolean;
    idRequest: any;
    paramPage: number;
    routeId: number;
    internals: Internal[];
    customers: OrganizationCustomer[];
    brokers: SalesBroker[];
    requesttypes: RequestType[];
    salesmans: Salesman[];

    public displayInformation: boolean;
    public information: string;
    public organizationCustomers: Array<OrganizationCustomer>;

    constructor(
        protected alertService: JhiAlertService,
        protected vehicleCustomerRequestService: VehicleCustomerRequestService,
        protected internalService: InternalService,
        protected customerService: OrganizationCustomerService,
        protected requestTypeService: RequestTypeService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected reportUtilService: ReportUtilService,
        protected toaster: ToasterService,
        protected confirmationService: ConfirmationService,
        protected salesBrokerService: SalesBrokerService,
        protected organizationCustomerService: OrganizationCustomerService,
        protected commonUtilService: CommonUtilService,
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected salesmanService: SalesmanService
    ) {
        this.vehicleCustomerRequest = new VehicleCustomerRequest();
        this.routeId = 0;
        this.displayInformation = false;
        this.salesUnitRequirements = new Array<SalesUnitRequirement>();
        this.salesmans = [];
    }

    ngOnInit() {
        this.registerSalesUnitRequirement();
        this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idRequest = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
        });
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => {
                this.internals = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json));
        this.loadOrganizationCustomers();
        this.requestTypeService.query()
            .subscribe((res: ResponseWrapper) => {
                this.requesttypes = res.json;
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesBrokerService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => {
                this.brokers = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json));
        this.salesmanService.query({size: 9999})
            .subscribe((res: ResponseWrapper) => {
                    this.salesmans = res.json;
                }, (res: ResponseWrapper) => this.onError(res.json));
            }

    protected registerSalesUnitRequirement(): void {
        this.subscriptionSalesUnitRequirement = this.salesUnitRequirementService.values.subscribe(
            (res) => {
                this.salesUnitRequirements = res;
            }
        )
    }

    protected loadOrganizationCustomers(): void {
        this.organizationCustomerService.query({size: 99999}).subscribe(
            (res: ResponseWrapper) => {
                    this.organizationCustomers = res.json;
            }, (err) => {
                    this.commonUtilService.showError(err);
            }
        )
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.subscriptionSalesUnitRequirement.unsubscribe();
    }

   load() {
       this.vehicleCustomerRequestService.find(this.idRequest).subscribe((vehicleCustomerRequest) => {
           this.vehicleCustomerRequest = vehicleCustomerRequest;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['vehicle-customer-request', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.vehicleCustomerRequest.idRequest !== undefined) {
           this.subscribeToSaveResponse(
               this.vehicleCustomerRequestService.update(this.vehicleCustomerRequest));
       } else {
           this.subscribeToSaveResponse(
               this.vehicleCustomerRequestService.create(this.vehicleCustomerRequest));
       }
   }

   protected subscribeToSaveResponse(result: Observable<VehicleCustomerRequest>) {
       result.subscribe((res: VehicleCustomerRequest) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: VehicleCustomerRequest) {
       this.eventManager.broadcast({ name: 'vehicleCustomerRequestListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'vehicleCustomerRequest saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'vehicleCustomerRequest Changed', error.message);
       this.alertService.error(error.message, null, null);
   }

   trackInternalById(index: number, item: Internal) {
       return item.idInternal;
   }

   trackCustomerById(index: number, item: OrganizationCustomer) {
       return item.idCustomer;
   }

   trackBrokerById(index: number, item: SalesBroker) {
       return item.idPartyRole;
   }

    trackRequestTypeById(index: number, item: RequestType) {
       return item.idRequestType;
    }

    print() {
        this.toaster.showToaster('info', 'Print Data', 'Printing.....');
        this.reportUtilService.viewFile('/api/report/sample/pdf');
    }

    activated() {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to submit this data to VSO?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                const filledForm: string = this.vehicleCustomerRequestService.validationSubmitToVSO(this.vehicleCustomerRequest, this.salesUnitRequirements);

                if (filledForm === '') {
                    this.vehicleCustomerRequestService.process(this.vehicleCustomerRequest, {action: 'completed'}).subscribe((r) => {this.load(); },
                    () => {},
                    () => {
                        this.eventManager.broadcast({
                            name: 'customerRequestItemListModification',
                            content: 'change an vehicleCustomerRequest'
                        });
                        this.previousState();
                        this.toaster.showToaster('info', 'Data Activated', 'Activated.....');
                    });
                } else {
                    this.information = filledForm;
                    this.displayInformation = true;
                }
            }
        });
    }

   approved() {
     this.vehicleCustomerRequestService.changeStatus(this.vehicleCustomerRequest, 12).subscribe((r) => {
        this.load();
        this.eventManager.broadcast({
            name: 'customerRequestItemListModification',
            content: 'change an vehicleCustomerRequest'
        });
        this.toaster.showToaster('info', 'Data Approved', 'Approved.....');
      });
   }

   public completed(): void {
        this.vehicleCustomerRequestService.changeStatus(this.vehicleCustomerRequest, 17).delay(1000).subscribe((r) => {
            this.eventManager.broadcast({
                name: 'vehicleCustomerRequestListModification',
                content: 'Completed an vehicleCustomerRequest'
            });
            this.toaster.showToaster('info', 'Data Completed', 'Completed.....');
            this.previousState();
        });
   }

   canceled() {
       this.confirmationService.confirm({
           message: 'Are you sure that you want to cancel?',
           header: 'Confirmation',
           icon: 'fa fa-question-circle',
           accept: () => {
               this.vehicleCustomerRequestService.changeStatus(this.vehicleCustomerRequest, 13).delay(1000).subscribe((r) => {
                   this.eventManager.broadcast({
                       name: 'vehicleCustomerRequestListModification',
                       content: 'Cancel an vehicleCustomerRequest'
                    });
                    this.toaster.showToaster('info', 'Data vehicleCustomerRequest cancel', 'Cancel vehicleCustomerRequest.....');
                    this.previousState();
                });
            }
        });
   }

}
