import { BaseEntity } from './../../shared';
import { PostalAddress } from './../postal-address';
import { PartyRole } from './../party-role';
import { Person } from './../person';

export class Driver extends PartyRole {
    constructor(
        public id?: any,
        public idDriver?: string,
        public person?: Person,
        public externalDriver?: boolean,
        public idVendor?: string,
    ) {
        super();
        this.person = new Person();
    }
}
