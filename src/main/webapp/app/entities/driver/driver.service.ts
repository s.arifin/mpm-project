import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Driver } from './driver.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DriverService {

    protected resourceUrl = SERVER_API_URL + 'api/drivers';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/drivers';
    protected resourceCUrl = process.env.API_C_URL + '/api/drivers';
    protected dummy = 'http://localhost:52374/api/drivers';

    constructor(protected http: Http) { }

    getDriverbyInternal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        return this.http.get(`${this.resourceCUrl}/GetDriverInternal`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    create(driver: Driver): Observable<Driver> {
        const copy = this.convert(driver);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(driver: Driver): Observable<Driver> {
        const copy = this.convert(driver);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<Driver> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterByC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/GetDriverInternal', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(driver: Driver): Driver {
        const copy: Driver = Object.assign({}, driver);
        return copy;
    }
}
