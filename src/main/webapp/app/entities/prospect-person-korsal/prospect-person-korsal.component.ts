import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ProspectPerson } from './prospect-person-korsal.model';
import { ProspectPersonKorsalService } from './prospect-person-korsal.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService, CommonUtilService} from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LazyLoadEvent } from 'primeng/primeng';
import { ConfirmationService } from 'primeng/primeng';
import { Salesman } from '../salesman';
import { SalesUnitRequirementStatusUnitDocumentMessageComponent } from '../sales-unit-requirement';
import { Person } from '../person';
import * as ProspectPersonConstant from '../../shared/constants/prospect-person.constants'

@Component({
    selector: 'jhi-prospect-person-korsal',
    templateUrl: './prospect-person-korsal.component.html'
})
export class ProspectPersonKorsalComponent implements OnInit, OnDestroy {

    @Input() idSalesman: any;
    @Input() idKorsal: any;

    protected subscription: Subscription;
    prospectPerson: ProspectPerson;
    statusNew: Number;
    statusLow: Number;
    statusMedium: Number;
    statusHot: Number;
    newProspect: Boolean = false;
    findProspect: Boolean = false;
    findPerson: Boolean = false;
    findNik: Boolean = false;
    name: String = null;
    cellPhone: String = null;
    nik: String = null;
    userSales: any;
    userKorsal: any;
    findProspectOtherSales: Boolean = false;
    salesFU: Boolean = false;
    idinternal: string;

    constructor(

        protected alertService: JhiAlertService,
        protected prospectPersonService: ProspectPersonKorsalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected commonUtilService: CommonUtilService,
        protected principal: Principal,
    ) {
        this.prospectPerson = new ProspectPerson();
        this.statusNew = ProspectPersonConstant.STATUS_NEW;
        this.statusLow = ProspectPersonConstant.STATUS_LOW;
        this.statusMedium = ProspectPersonConstant.STATUS_MEDIUM;
        this.statusHot = ProspectPersonConstant.STATUS_HOT;
        this.userSales = null;
        this.idinternal = this.principal.getIdInternal();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.userSales = params['id']
                console.log('ini parameter :' , params['id']);
            } else {
                this.prospectPersonService.values.subscribe(
                    (response) => {
                        if (response) {
                            this.prospectPerson = response[0];
                        }
                    }
                );
            }
        });
    }

    ngOnDestroy() {

    }

    clearOldData() {
        this.newProspect = false;
        this.findProspect = false;
        this.findPerson = false;
        this.findNik = false;
        this.name = null;
        this.cellPhone = null;
        this.nik = null;
    }

    previousState() {
        this.router.navigate(['/prospect-person-korsal/' + this.userSales + '/target']);
    }

    public doFollowUp(): void {
        this.clearOldData();
        this.router.navigate(['../prospect-person/' + this.prospectPerson.idProspect + '/step']);
    }

    public doEdit(): void {
        this.clearOldData();
        this.router.navigate(['../prospect-person/' + this.prospectPerson.idProspect + '/edit']);
    }

    public backToFind(): void {
        this.clearOldData();
        this.newProspect = true;
    }

    public trackpersonalIdNumber(index: number, item: Person): String {
        return item.personalIdNumber;
    }

    closeDialog() {
        this.findProspectOtherSales = false;
    }
}
