import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ProspectPersonKorsalComponent } from './prospect-person-korsal.component';
import { ProspectPersonKorsalEditComponent } from './prospect-person-korsal-edit.component';
import { ProspectPersonKorsalDetailComponent } from './prospect-person-korsal-detail.component';
import { ProspectPersonKorsalTargetComponent } from './prospect-person-korsal-target.component';

@Injectable()
export class ProspectPersonKorsalResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idProspect,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const prospectPersonKorsalRoute: Routes = [
    {
        path: 'prospect-person-korsal',
        component: ProspectPersonKorsalComponent,
        resolve: {
            'pagingParams': ProspectPersonKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    }

];

export const prospectPersonKorsalPopupRoute: Routes = [
    {
        path: 'prospect-person-korsal-new',
        component: ProspectPersonKorsalEditComponent,
        resolve: {
            'pagingParams': ProspectPersonKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    // {
    //     path: 'prospect-person-korsal/:id/edit',
    //     component: ProspectPersonKorsalEditComponent,
    //     data: {
    //         authorities: ['ROLE_USER'],
    //         pageTitle: 'mpmApp.prospectPerson.home.title'
    //     },
    //     canActivate: [UserRouteAccessService]
    // },
    {
        path: 'prospect-person-korsal/:id/detail',
        component: ProspectPersonKorsalDetailComponent,
        resolve: {
            'pagingParams': ProspectPersonKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-person-korsal/:id/edit',
        component: ProspectPersonKorsalComponent,
        resolve: {
            'pagingParams': ProspectPersonKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-person-korsal/:id/target',
        component: ProspectPersonKorsalTargetComponent,
        resolve: {
            'pagingParams': ProspectPersonKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    }

];
