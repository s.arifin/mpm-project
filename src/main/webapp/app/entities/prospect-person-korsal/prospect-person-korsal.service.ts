import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { ProspectPerson } from './prospect-person-korsal.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ProspectPersonKorsalService {
    protected itemValues: ProspectPerson[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/prospect-people';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/prospect-people';
    private resourceCUrl = process.env.API_C_URL + '/api/prospect';
    protected resourceBUrl = process.env.API_C_URL + '/api/prospect-people';

    constructor(protected http: Http) { }

    create(prospectPerson: ProspectPerson): Observable<ProspectPerson> {
        const copy = this.convert(prospectPerson);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    querytargetSales(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/TargetSales', options)
            .map((res: Response) => this.convertResponse(res));
    }

    querytargetSalesSummary(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/TargetSalesSummary', options)
            .map((res: Response) => this.convertResponse(res));
    }

    updateFollowUp(prospectPerson: ProspectPerson): Observable<ProspectPerson> {
        const copy = this.convert(prospectPerson);
        return this.http.put(this.resourceUrl + '/followUp', copy).map((res: Response) => {
            return res.json();
        });
    }

    update(prospectPerson: ProspectPerson): Observable<ProspectPerson> {
        const copy = this.convert(prospectPerson);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<ProspectPerson> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterByKorsal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterByKorsal', options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(prospectPerson: ProspectPerson, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(prospectPerson);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, prospectPerson: ProspectPerson): Observable<ProspectPerson> {
        const copy = this.convert(prospectPerson);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, prospectPersons: ProspectPerson[]): Observable<ProspectPerson[]> {
        const copy = this.convertList(prospectPersons);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(prospectPerson: ProspectPerson): ProspectPerson {
        if (prospectPerson === null || prospectPerson === {}) {
            return {};
        }
        // const copy: ProspectPerson = Object.assign({}, prospectPerson);
        const copy: ProspectPerson = JSON.parse(JSON.stringify(prospectPerson));
        return copy;
    }

    protected convertList(prospectPersons: ProspectPerson[]): ProspectPerson[] {
        const copy: ProspectPerson[] = prospectPersons;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ProspectPerson[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    getByFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceBUrl + '/filterbykorsal', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findC(id: any): Observable<ProspectPerson> {
        return this.http.get(`${this.resourceBUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    cekKorsal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/cekKorsal', options)
            .map((res: Response) => this.convertResponse(res));
    }
}
