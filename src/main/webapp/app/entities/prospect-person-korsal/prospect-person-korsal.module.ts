import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    ProspectPersonKorsalService,
    ProspectPersonKorsalPopupService,
    ProspectPersonKorsalComponent,
    prospectPersonKorsalRoute,
    prospectPersonKorsalPopupRoute,
    ProspectPersonKorsalResolvePagingParams,
    ProspectPersonKorsalGridComponent,
    ProspectPersonKorsalEditComponent,
    ProspectPersonKorsalDetailComponent,
    ProspectPersonKorsalTargetComponent,
} from '.';

import { CommonModule } from '@angular/common';

import { MpmSalesUnitRequirementModule } from '../sales-unit-requirement';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         RadioButtonModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule,
         TooltipModule
        } from 'primeng/primeng';

import { WizardModule } from 'ng2-archwizard';

import { MpmSharedEntityModule } from '../shared-entity.module';
import { MpmCommunicationEventProspectModule } from '../communication-event-prospect/communication-event-prospect.module';

const ENTITY_STATES = [
    ...prospectPersonKorsalRoute,
    ...prospectPersonKorsalPopupRoute,
];

@NgModule({
    imports: [
        MpmCommunicationEventProspectModule,
        MpmSalesUnitRequirementModule,
        MpmSharedModule,
        MpmSharedEntityModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        WizardModule,
        RadioButtonModule,
        TooltipModule
    ],
    exports: [
        ProspectPersonKorsalComponent,
        ProspectPersonKorsalGridComponent,
        ProspectPersonKorsalEditComponent,
        ProspectPersonKorsalDetailComponent,
        ProspectPersonKorsalTargetComponent
    ],
    declarations: [
        ProspectPersonKorsalComponent,
        ProspectPersonKorsalGridComponent,
        ProspectPersonKorsalEditComponent,
        ProspectPersonKorsalDetailComponent,
        ProspectPersonKorsalTargetComponent
    ],
    entryComponents: [
        ProspectPersonKorsalComponent,
        ProspectPersonKorsalGridComponent,
        ProspectPersonKorsalEditComponent,
        ProspectPersonKorsalDetailComponent,
        ProspectPersonKorsalTargetComponent
    ],
    providers: [
        ProspectPersonKorsalService,
        ProspectPersonKorsalPopupService,
        ProspectPersonKorsalResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmProspectPersonKorsalModule {}
