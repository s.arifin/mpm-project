import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ProspectPerson, SummaryTargetSales, SummaryTargetSalesHistory  } from './prospect-person-korsal.model';
import { ProspectPersonKorsalService } from './prospect-person-korsal.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService, CommonUtilService} from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LazyLoadEvent } from 'primeng/primeng';
import { ConfirmationService } from 'primeng/primeng';
import { Salesman } from '../salesman';
import { LoadingService} from '../../layouts/loading/loading.service';
import { SalesUnitRequirementStatusUnitDocumentMessageComponent } from '../sales-unit-requirement';
import { Person } from '../person';
import * as ProspectPersonConstant from '../../shared/constants/prospect-person.constants'

@Component({
    selector: 'jhi-prospect-person-korsal-target',
    templateUrl: './prospect-person-korsal-target.component.html'
})
export class ProspectPersonKorsalTargetComponent implements OnInit, OnDestroy {

    @Input() idSalesman: any;
    @Input() idKorsal: any;

    protected subscription: Subscription;
    userSales: any;
    userKorsal: any;
    findProspectOtherSales: Boolean = false;
    salesFU: Boolean = false;
    targetSales: SummaryTargetSales[];
    targetSalesSummary: SummaryTargetSalesHistory[];
    page: any;
    itemsPerPage: any;
    links: any;
    totalItems: any;
    queryCount: any;
    routeData: any;
    previousPage: any;
    reverse: any;
    predicate: any;
    first: number;
    isKorsal: number;
    salesman: Salesman;

    constructor(

        protected alertService: JhiAlertService,
        protected prospectPersonService: ProspectPersonKorsalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected commonUtilService: CommonUtilService,
        protected principal: Principal,
        protected loadingService: LoadingService,
        protected parseLinks: JhiParseLinks,
        protected activatedRoute: ActivatedRoute,
    ) {
        this.targetSales = new Array<SummaryTargetSales>();
        this.targetSalesSummary = new Array<SummaryTargetSalesHistory>();
        this.userSales = null;
        this.isKorsal = null;
        this.first = (this.page - 1) * this.itemsPerPage;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.userSales = params['id']
                console.log('ini parameter :' , params['id']);
            } else {
                this.prospectPersonService.values.subscribe(
                    (response) => {
                        if (response) {
                            this.targetSales = response[0];
                        }
                    }
                );
            }
            this.loadAll();
        });
    }

    loadAll() {
        console.log('ini username : ', this.userSales);
        // this.loadingService.loadingStart();
        this.prospectPersonService.querytargetSales({
            username: this.userSales,
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );

        this.prospectPersonService.querytargetSalesSummary({
            username: this.userSales,
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess2(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );

        this.prospectPersonService.cekKorsal({
            username: this.principal.getUserLogin()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess3(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;
        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    protected onSuccess(data, headers) {
        if (data !== null) {
            this.totalItems = data[0].TotalData;
            // this.totalItems = headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            console.log('hasil get', data[0].TotalData, data);
            this.targetSales = data;
        }
    }

    protected onSuccess2(data, headers) {
        if (data !== null) {
            this.totalItems = data[0].TotalData;
            // this.totalItems = headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            console.log('hasil get', data[0].TotalData, data);
            this.targetSalesSummary = data;
        }
    }

    protected onSuccess3(data, headers) {
        if (data !== null) {
            this.totalItems = headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            this.salesman = data;
            console.log('inikorsal', this.salesman);
            if (this.salesman.coordinator === true) {
                this.isKorsal = 1
            }else {
                this.isKorsal = 2
            }
        }
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    ngOnDestroy() {

    }

    previousState() {
        this.router.navigate(['../prospect-korsal-list-sales']);
    }

    public trackpersonalIdNumber(index: number, item: Person): String {
        return item.personalIdNumber;
    }

    closeDialog() {
        this.findProspectOtherSales = false;
    }
}
