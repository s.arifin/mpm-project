import { BaseEntity } from '../shared-component';
import { Person } from '../person'
import { Prospect } from '../prospect/prospect.model'
import { CommunicationEvent } from '../communication-event/communication-event.model'

export class ProspectPerson extends Prospect {
    constructor(
        public person?: any,
        public fName?: any,
        public lName?: any,
        public phone?: any,
        public cellphone1?: any,
        public cellphone2?: any,
        public facilityName?: any,
        public internName?: any,
        public personalIdNumber?: any,
        public prospectNumber?: any,
        public telp?: number,
        public kunjungan?: number,
        public walkin?: number,
        public assign?: Date,
        public followup?: Date,
        public reqdrop?: number,
        public userName?: any,

    ) {
        super();
        this.person = new Person();
    }
}

export class SummaryTargetSales implements BaseEntity {
    constructor(
        public id?: Number,
        public TotalData?: Number,
        public idSalesman?: any,
        public capaiSales?: any,
        public nilaiTargetSales?: any,
        public hasilCapai?: any,
        public namaSalesman?: any,
    ) {

    }
}

export class SummaryTargetSalesHistory implements BaseEntity {
    constructor(
        public id?: Number,
        public TotalData?: Number,
        public idsalesman?: any,
        public namasales?: any,
        public targetsales?: any,
        public capaiansales?: any,
        public persentase?: any,
        public bulanket?: any,
        public tahun?: any,
    ) {

    }
}
