import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { StockOpnameItem } from './stock-opname-item.model';
import { StockOpnameItemService } from './stock-opname-item.service';

@Injectable()
export class StockOpnameItemPopupService {
    protected ngbModalRef: NgbModalRef;
    idStockOpname: any;
    idProduct: any;
    idContainer: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected stockOpnameItemService: StockOpnameItemService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.stockOpnameItemService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.stockOpnameItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new StockOpnameItem();
                    data.stockOpnameId = this.idStockOpname;
                    data.productId = this.idProduct;
                    data.containerId = this.idContainer;
                    this.ngbModalRef = this.stockOpnameItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    stockOpnameItemModalRef(component: Component, stockOpnameItem: StockOpnameItem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.stockOpnameItem = stockOpnameItem;
        modalRef.componentInstance.idStockOpname = this.idStockOpname;
        modalRef.componentInstance.idProduct = this.idProduct;
        modalRef.componentInstance.idContainer = this.idContainer;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.stockOpnameItemLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    stockOpnameItemLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idStockOpname = this.idStockOpname;
        modalRef.componentInstance.idProduct = this.idProduct;
        modalRef.componentInstance.idContainer = this.idContainer;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
