import { BaseEntity } from './../../shared';

export class StockOpnameItem implements BaseEntity {
    constructor(
        public id?: any,
        public idStockopnameItem?: any,
        public itemDescription?: string,
        public qty?: number,
        public qtyCount?: number,
        public tagNumber?: string,
        public hasChecked?: boolean,
        public het?: number,
        public stockOpnameId?: any,
        public productId?: any,
        public idProduct?: any,
        public containerId?: any,
    ) {
        this.hasChecked = false;
    }
}
