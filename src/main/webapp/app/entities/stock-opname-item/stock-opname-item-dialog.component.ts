import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { StockOpnameItem } from './stock-opname-item.model';
import { StockOpnameItemPopupService } from './stock-opname-item-popup.service';
import { StockOpnameItemService } from './stock-opname-item.service';
import { ToasterService } from '../../shared';
import { StockOpname, StockOpnameService } from '../stock-opname';
import { Product, ProductService } from '../product';
import { Container, ContainerService } from '../container';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-stock-opname-item-dialog',
    templateUrl: './stock-opname-item-dialog.component.html'
})
export class StockOpnameItemDialogComponent implements OnInit {

    stockOpnameItem: StockOpnameItem;
    isSaving: boolean;
    idStockOpname: any;
    idProduct: any;
    idContainer: any;

    stockopnames: StockOpname[];

    products: Product[];

    containers: Container[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected stockOpnameItemService: StockOpnameItemService,
        protected stockOpnameService: StockOpnameService,
        protected productService: ProductService,
        protected containerService: ContainerService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.stockOpnameService.query()
            .subscribe((res: ResponseWrapper) => { this.stockopnames = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.containerService.query()
            .subscribe((res: ResponseWrapper) => { this.containers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.stockOpnameItem.idStockopnameItem !== undefined) {
            this.subscribeToSaveResponse(
                this.stockOpnameItemService.update(this.stockOpnameItem));
        } else {
            this.subscribeToSaveResponse(
                this.stockOpnameItemService.create(this.stockOpnameItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<StockOpnameItem>) {
        result.subscribe((res: StockOpnameItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: StockOpnameItem) {
        this.eventManager.broadcast({ name: 'stockOpnameItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'stockOpnameItem saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'stockOpnameItem Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackStockOpnameById(index: number, item: StockOpname) {
        return item.idStockOpname;
    }

    trackProductById(index: number, item: Product) {
        return item.idProduct;
    }

    trackContainerById(index: number, item: Container) {
        return item.idContainer;
    }
}

@Component({
    selector: 'jhi-stock-opname-item-popup',
    template: ''
})
export class StockOpnameItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected stockOpnameItemPopupService: StockOpnameItemPopupService
    ) {}

    ngOnInit() {
        this.stockOpnameItemPopupService.idStockOpname = undefined;
        this.stockOpnameItemPopupService.idProduct = undefined;
        this.stockOpnameItemPopupService.idContainer = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.stockOpnameItemPopupService
                    .open(StockOpnameItemDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.stockOpnameItemPopupService.idStockOpname = params['parent'];
                this.stockOpnameItemPopupService
                    .open(StockOpnameItemDialogComponent as Component);
            } else {
                this.stockOpnameItemPopupService
                    .open(StockOpnameItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
