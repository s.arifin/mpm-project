import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { LazyLoadEvent, AutoComplete } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import * as _ from 'lodash';

import { ProductService, Product } from '../product/index';
import { ContainerService, Container } from '../container/index';
import { StockOpnameItem } from '.';

@Component({
    selector: 'jhi-stock-opname-item-input',
    templateUrl: './stock-opname-item-input.component.html',
    styles: [
        `
        .progress {
            position: relative;
        }
        .progress span {
            position: absolute;
            display: block;
            width: 100%;
            color: black;
        }
    `
    ]
})
export class StockOpnameItemInputComponent implements OnInit, OnDestroy {
    @ViewChild('acTag') acTag: AutoComplete;
    @ViewChild('acPart') acPart: AutoComplete;
    @ViewChild('formInput') formInput: ElementRef;
    @Input() set soItemsInput(value: any) {
        console.log('Data:', value);
        this.hitungStatusInput(value);
        this.hitungStatusAmount(value);
        this.stockOpnameItems = this.configStockOpnameItems(value);
        console.log('stockOpnameItems', this.stockOpnameItems)
    };
    @Input() statusOpname: number;
    @Output() inputData = new EventEmitter();

    radioInput: any = '1';
    inputTemuan: any = {
        tag_no: '',
        parts_no: '',
        parts_name: '',
        qtyCount: 0,
        location: ''
    };
    stockOpnameItems: StockOpnameItem[];
    statusInput: any;
    statusInputAll: any;
    statusInputFiltered: any;
    statusAmount: any;
    statusAmountSelisih: any;
    statusAmountSystem: any;

    dataFilterTag: any[] = [];
    dataFilterPart: any[] = [];
    dataFilterLocation: any[] = [];

    dataAllProduct: Product[];
    dataAllContainer: Container[];

    protected productSubs: Subscription;
    protected containerSubs: Subscription;

    constructor(
        public productService: ProductService,
        public containerService: ContainerService, // public asListService: StockOpnameItemAsListComponent
        public toaster: ToasterService
    ) { }

    configStockOpnameItems(data: any) {
        if (data !== undefined) {
            console.log('Data Item', data);
            for (let i = 0; i < data.length; i++) {
                data[i].idProduct = data[i].productId;
            }
            return data;
        }
    }

    addInputTemu() {
        console.log('stockOpnameItems', this.stockOpnameItems);
        console.log('inputTemuan', this.inputTemuan)
        if (this.radioInput === '1') {
            let data: any = _.cloneDeep(_.filter(this.stockOpnameItems, { productId: this.inputTemuan.parts_no.productId }));
            if (data.length > 0) {
                this.sendInputTemu(data[0]);
            } else {
                data = _.cloneDeep(_.filter(this.stockOpnameItems, { tagNumber: this.inputTemuan.tag_no.tagNumber }));
                if (data.length > 0) {
                    this.sendInputTemu(data[0]);
                } else {
                    this.toaster.showToaster('error', 'Not Found', 'Item not found')
                }
            }
        } else {
            const data: StockOpnameItem = {};
            data.productId = this.inputTemuan.parts_no.idProduct;
            data.containerId = this.inputTemuan.location.idContainer;
            data.stockOpnameId = this.stockOpnameItems[0].stockOpnameId;
            this.sendInputTemu(data);
        }
    }

    sendInputTemu(data) {
        console.log('FilteredData', data);
        if (this.radioInput === '1') { data.tag_no = this.inputTemuan.tagNumber; };
        data.qtyCount = this.inputTemuan.qtyCount;
        data.hasChecked = true;
        data = { 'data': data };
        console.log('rowdata', data);
        this.inputData.emit(data);
        this.formInput.nativeElement.reset();
    }

    resetInputTemu() {
        this.formInput.nativeElement.reset();
    }

    ngOnInit() {
        // Load All Parts
        this.productSubs = this.productService.query().subscribe((res) => {
            console.log('PartRes', res.json);
            this.dataAllProduct = res.json;
        });

        // Load All Container/Location
        this.containerSubs = this.containerService.query().subscribe((res) => {
            console.log('ContRes', res.json);
            this.dataAllContainer = res.json;
        });
    }

    ngOnDestroy() { }

    loadAll() { }

    filterTag(e) {
        console.log('Query:', e.query);
        this.dataFilterTag = _.cloneDeep(
            _.filter(this.stockOpnameItems, function(item) {
                return item.tagNumber.toLowerCase().indexOf(e.query.toLowerCase()) > -1;
            })
        );
        console.log('dataFilterTag', this.dataFilterTag);
    }

    filterPart(e) {
        console.log('Query:', e.query);
        console.log('radioInput', this.radioInput)
        if ((this.radioInput === '1')) {
            console.log('stockOpnameItems', this.stockOpnameItems)
            this.dataFilterPart = _.cloneDeep(_.filter(this.stockOpnameItems, function(item) {
                return item.idProduct.toLowerCase().indexOf(e.query.toLowerCase()) > -1;
            }));
            console.log('dataFilterPart1', this.dataFilterPart);
        } else {
            // this.productSubs = this.productService.query({ query: e.query }).subscribe((res) => {
            //     console.log('Res', res.json);
            //     this.dataFilterPart = res.json;
            // });
            console.log('dataAllProduct', this.dataAllProduct)
            this.dataFilterPart = _.cloneDeep(_.filter(this.dataAllProduct, function(item) {
                return item.idProduct.toLowerCase().indexOf(e.query.toLowerCase()) > -1;
            }));
            console.log('dataFilterPart2', this.dataFilterPart);
        };
    }

    filterLocation(e) {
        console.log('Query:', e.query);
        this.dataFilterLocation = _.cloneDeep(_.filter(this.dataAllContainer, function(item) {
            return item.idContainer.toLowerCase().indexOf(e.query.toLowerCase()) > -1;
        }));
        console.log('dataFilterLocation', this.dataFilterPart);
    }

    setPart() {
        console.log('setPart', this.inputTemuan);
        if (this.radioInput === '1') {
            this.inputTemuan.parts_name = this.inputTemuan.parts_no.productName;
            this.acTag.inputEL.nativeElement.value = this.inputTemuan.parts_no.tagNumber;
        } else {
            this.inputTemuan.parts_name = this.inputTemuan.parts_no.name;
        }
    }
    setTag() {
        console.log('setTag', this.inputTemuan);
        this.inputTemuan.parts_name = this.inputTemuan.tag_no.productName;
        this.acPart.inputEL.nativeElement.value = this.inputTemuan.tag_no.idProduct;
    }

    hitungStatusInput(data: any) {
        if (data !== undefined) {
            this.statusInputAll = data.length;
            this.statusInputFiltered = _.cloneDeep(
                _.filter(data, { hasChecked: true })
            ).length;
            this.statusInput =
                this.statusInputFiltered / this.statusInputAll * 100 + '%';
        }
    }

    hitungStatusAmount(data: any) {
        if (data !== undefined) {
            console.log('statusData', data);
            this.statusAmountSelisih = 0;
            this.statusAmountSystem = 0;
            for (let i = 0; i < data.length; i++) {
                if (data[i].hasChecked === true) {
                    this.statusAmountSelisih += (data[i].qty - data[i].qtyCount) * data[i].het;
                }
                this.statusAmountSystem += data[i].qty * data[i].het;
            }
            console.log('statusAmountSelisih', this.statusAmountSelisih);
            console.log('statusAmountSystem', this.statusAmountSystem);
            this.statusAmount =
                this.statusAmountSelisih / this.statusAmountSystem * 100 + '%';
        }
    }
}
