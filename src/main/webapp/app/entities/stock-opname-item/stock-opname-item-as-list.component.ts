import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { StockOpnameItem } from './stock-opname-item.model';
import { StockOpnameItemService } from './stock-opname-item.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import * as _ from 'lodash';

@Component({
    selector: 'jhi-stock-opname-item-as-list',
    templateUrl: './stock-opname-item-as-list.component.html'
})
export class StockOpnameItemAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idStockOpname: any;
    @Input() idProduct: any;
    @Input() idContainer: any;
    @Input() statusOpname: number;
    @Output() soItemsAsList = new EventEmitter<string>();

    currentAccount: any;
    stockOpnameItems: StockOpnameItem[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    @Input() set inputData(value: any) {
        if (value !== undefined) {
            console.log('InputDataAsList', value);
            this.updateRowData(value);
            this.loadAll();
        }
    };

    constructor(
        protected stockOpnameItemService: StockOpnameItemService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'container.containerCode';
        this.reverse = 'asc';
        this.first = 0;
        console.log('ID STock:', this.idStockOpname);
    }

    loadAll() {
        // console.log('ID STock:', this.idStockOpname);
        // this.stockOpnameItemService.query().subscribe(
        //     (res: ResponseWrapper) => {
        //         console.log('load stock opname:', res);
        //         this.onSuccess(res.json, res.headers)
        //     },
        //     (res: ResponseWrapper) => this.onError(res.json)
        // );
        this.stockOpnameItemService.queryFilterBy({
            // filterName: 'relation',
            idStockOpname: this.idStockOpname,
            // idProduct: this.idProduct,
            // idContainer: this.idContainer,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => {
                console.log('load stock opname:', res);
                this.onSuccess(res.json, res.headers)
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/stock-opname-item', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        // this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInStockOpnameItems();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idStockOpname']) {
        }
        if (changes['idProduct']) {
        }
        if (changes['idContainer']) {
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: StockOpnameItem) {
        return item.idStockopnameItem;
    }

    registerChangeInStockOpnameItems() {
        this.eventSubscriber = this.eventManager.subscribe('stockOpnameItemListModification', (response) => {
            console.log('register change:', response);
            this.loadAll();
        });
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idStockopnameItem') {
            result.push('product.idProduct');
            result.push('idStockopnameItem');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.stockOpnameItems = data;
        this.soItemsAsList.emit(data);
        console.log('item data', data);
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.stockOpnameItemService.executeProcess(item).subscribe(
            (value) => console.log('this: ', value),
            (err) => console.log(err),
            () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        console.log('gridData', event.data);
        if (event.data.id !== undefined) {
            this.subscribeToSaveResponse(
                this.stockOpnameItemService.update(event.data));
        } else {
            this.subscribeToSaveResponse(
                this.stockOpnameItemService.create(event.data));
        }
    }

    protected subscribeToSaveResponse(result: Observable<StockOpnameItem>) {
        result.subscribe((res: StockOpnameItem) => this.onSaveSuccess(res));
    }

    protected onSaveSuccess(result: StockOpnameItem) {
        this.eventManager.broadcast({ name: 'stockOpnameItemListModification', content: 'OK' });
        this.toasterService.showToaster('info', 'Save', 'stockOpnameItem saved !');
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.stockOpnameItemService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'stockOpnameItemListModification',
                        content: 'Deleted an stockOpnameItem'
                    });
                });
            }
        });
    }
}
