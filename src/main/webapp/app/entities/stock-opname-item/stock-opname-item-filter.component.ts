import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

// Custom
import { InternalService } from '../internal/internal.service';
import { ContainerService } from '../container/container.service';
import { ProductService } from '../product/product.service';
import { StockOpnameTypeService } from '../stock-opname-type/stock-opname-type.service';
import * as _ from 'lodash';

@Component({
    selector: 'jhi-stock-opname-item-filter',
    templateUrl: './stock-opname-item-filter.component.html'
})
export class StockOpnameItemFilterComponent implements OnInit, OnDestroy {

    public formFilter: FormGroup;
    public Dealer: AbstractControl;
    public TypeOpname: AbstractControl;
    public Gudang: AbstractControl;
    public Rak: AbstractControl;
    public Parts: AbstractControl;
    public Tanggal: AbstractControl;

    internalSubscriber: Subscription;
    containerSubscriber: Subscription;
    productSubscriber: Subscription;

    dealerList: any[] = [];
    partList: any[] = [];

    @Output() dataFilter = new EventEmitter<string>();

    constructor(
        fb: FormBuilder,
        protected internalService: InternalService,
        protected containerService: ContainerService,
        protected productService: ProductService
    ) {
        this.formFilter = fb.group({
            Dealer: ['', Validators.compose([])],
            TypeOpname: ['', Validators.compose([])],
            Gudang: ['', Validators.compose([])],
            Rak: ['', Validators.compose([])],
            Parts: ['', Validators.compose([])],
            Tanggal: ['', Validators.compose([])],
        });

        this.Dealer = this.formFilter.controls['Dealer'];
        this.TypeOpname = this.formFilter.controls['TypeOpname'];
        this.Gudang = this.formFilter.controls['Gudang'];
        this.Rak = this.formFilter.controls['Rak'];
        this.Parts = this.formFilter.controls['Parts'];
        this.Tanggal = this.formFilter.controls['Tanggal'];

    }

    loadParams() {

        // Get Current Dealer
        this.internalSubscriber = this.internalService.query().subscribe((res) => {
            for (const entry of res.json) {
                const data = {
                    label: entry.organization.name,
                    value: entry.organization.name
                };
                this.dealerList.push(data);
            }
            this.formFilter.controls['Dealer'].setValue(res.json[2].organization.name);
        })
        // End Get Current Dealer

        // Get Rak
        this.containerSubscriber = this.containerService.query().subscribe((res) => {
            console.log('Rak:', res);
        })
        // End Rak

    }

    searchPart(e) {
        // Get Parts
        this.productSubscriber = this.productService.query({query: e.query}).subscribe((res) => {
            this.partList = res.json;
        })
        // End Parts
    }

    filterOpname() {
        console.log('Filter:', this.formFilter.value);
        this.dataFilter.emit(this.formFilter.value);
    }

    ngOnInit() {
        this.loadParams();
    }

    ngOnDestroy() {
        this.internalSubscriber.unsubscribe();
        this.containerSubscriber.unsubscribe();
        this.productSubscriber.unsubscribe();
    }
}
