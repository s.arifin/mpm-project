import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { StockOpnameItemComponent } from './stock-opname-item.component';
import { StockOpnameItemPopupComponent } from './stock-opname-item-dialog.component';

@Injectable()
export class StockOpnameItemResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idStockopnameItem,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const stockOpnameItemRoute: Routes = [
    {
        path: 'stock-opname-item',
        component: StockOpnameItemComponent,
        resolve: {
            'pagingParams': StockOpnameItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpnameItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const stockOpnameItemPopupRoute: Routes = [
    {
        path: 'stock-opname-item-popup-new-list/:parent',
        component: StockOpnameItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpnameItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stock-opname-item-new',
        component: StockOpnameItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpnameItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stock-opname-item/:id/edit',
        component: StockOpnameItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpnameItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
