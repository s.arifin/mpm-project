import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { StockOpnameItem } from './stock-opname-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class StockOpnameItemService {
    protected itemValues: StockOpnameItem[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl = SERVER_API_URL + 'api/stock-opname-items';
    protected resourceSearchUrl = SERVER_API_URL +
        'api/_search/stock-opname-items';

    constructor(protected http: Http) { }

    create(stockOpnameItem: StockOpnameItem): Observable<StockOpnameItem> {
        const copy = this.convert(stockOpnameItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(stockOpnameItem: StockOpnameItem): Observable<StockOpnameItem> {
        const copy = this.convert(stockOpnameItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<StockOpnameItem> {
        return this.http
            .get(`${this.resourceUrl}/${id}`)
            .map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http
            .get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        console.log('query req', req);
        const options = createRequestOption(req);
        console.log('query opt', options);
        options.params.set('idStockOpname', req.idStockOpname);
        return this.http
            .get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http
            .post(`${this.resourceUrl}/process`, params, options)
            .map((res: Response) => {
                return res.json();
            });
    }

    executeProcess(item?: StockOpnameItem, req?: any): Observable<Response> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, item, options);
    }

    executeListProcess(
        items?: StockOpnameItem[],
        req?: any
    ): Observable<Response> {
        const options = createRequestOption(req);
        return this.http.post(
            `${this.resourceUrl}/execute-list`,
            items,
            options
        );
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http
            .get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to StockOpnameItem.
     */
    protected convertItemFromServer(json: any): StockOpnameItem {
        const entity: StockOpnameItem = Object.assign(
            new StockOpnameItem(),
            json
        );
        return entity;
    }

    /**
     * Convert a StockOpnameItem to a JSON which can be sent to the server.
     */
    protected convert(stockOpnameItem: StockOpnameItem): StockOpnameItem {
        if (stockOpnameItem === null || stockOpnameItem === {}) {
            return {};
        }
        // const copy: StockOpnameItem = Object.assign({}, stockOpnameItem);
        const copy: StockOpnameItem = JSON.parse(
            JSON.stringify(stockOpnameItem)
        );
        return copy;
    }

    protected convertList(
        stockOpnameItems: StockOpnameItem[]
    ): StockOpnameItem[] {
        const copy: StockOpnameItem[] = stockOpnameItems;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: StockOpnameItem[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
