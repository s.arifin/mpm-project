import { Component, OnInit } from '@angular/core';
import { LoadingService } from '../../layouts/loading';
import { ConvertUtilService } from '../../shared/index';
import {CommonUtilService, ResponseWrapper} from '../../shared';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { UploadInvoice, BillingDisbursementService } from './index';

// nuse
import { AxPosting, AxPostingService } from '../axposting';
import { AxPostingLine } from '../axposting-line';
import { DatePipe } from '@angular/common';
import { BillingDisbursementC, BillingDisbursementCService } from '../billing-disbursement-c';

import * as BillingDisbursementConstant from '../../shared/constants/billing-disbursement.constants';

@Component({
    selector: 'jhi-billing-disbursement-upload',
    templateUrl: './billing-disbursement-upload.component.html'
})

export class BillingDisbursementUploadComponent implements OnInit {
    public listUploadInvoice: Array<UploadInvoice>;
    public isSaving: boolean;

    // nuse
    protected axPosting: AxPosting;
    protected axPostingLine: AxPostingLine;
    protected billingDisbursementC: BillingDisbursementC;
    protected jumlah: number;
    protected ppn: number;
    protected discount: number;
    protected ongkosAngkut: number;
    protected unitPrice: number;

    constructor(
        protected billingDisubrsementService: BillingDisbursementService,
        protected loadingService: LoadingService,
        protected convertUtilService: ConvertUtilService,
        protected commonUtilService: CommonUtilService,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected axPostingService: AxPostingService,
        protected datePipe: DatePipe,
        protected billingDisbursementCService: BillingDisbursementCService,
    ) {
        this.listUploadInvoice = new Array<UploadInvoice>();
    }

    ngOnInit() {
        this.loadingService.loadingStart();
        this.loadingService.loadingStop();
        this.isSaving = false;
    }

    public disableSave(): boolean {
        if (this.listUploadInvoice.length === 0 || this.isSaving === true) {
            return true;
        }
        return false;
    }

    public save(): void {
        this.loadingService.loadingStart();
        this.isSaving = true;

        const objectBody: Object = {
            items: this.listUploadInvoice
        };

        const objectHeader: Object = {
            execute: 'UploadInvoice'
        };

        this.billingDisubrsementService.executeList(objectBody, objectHeader).subscribe(
            (res) => {
                if (res) {
                    this.onSuccessUpload();
                    console.log('isi res : ', res);
                    console.log('object body', objectBody);
                    this.jumlah = 0;
                    this.ppn = 0;
                    this.ongkosAngkut = 0;
                    this.discount = 0;

                    this.listUploadInvoice.forEach((item) => {
                        console.log('each item : ', item);
                        this.jumlah = this.jumlah + parseInt(item.totalAmount.toString(), 10);
                        this.ppn = this.ppn + parseInt(item.ppn.toString(), 10);
                        this.discount = this.discount + parseInt(item.unitDiscount.toString(), 10);
                        this.unitPrice = this.unitPrice + parseInt(item.unitPrice.toString(), 10);
                    });

                    if (isNaN(this.unitPrice)) {
                        this.unitPrice = 0;
                        console.log('masuk NaN');
                    }

                    console.log('Jumlah ;', this.jumlah);
                    console.log('PPN ;', this.ppn);
                    console.log('UNit Price : ', this.unitPrice);
                    this.billingDisbursementCService.findByInvoiceId(this.listUploadInvoice[0].invoiceNumber).subscribe(
                        (resBilDis) => {
                            this.billingDisbursementC = resBilDis;

                            const today = new Date();
                            const myaxPostingLine = [];
                            console.log('billing disbursement c : ', this.billingDisbursementC);
                            this.axPosting = new AxPosting();
                            this.axPosting.AutoPosting = 'FALSE';
                            this.axPosting.AutoSettlement = 'FALSE';
                            this.axPosting.DataArea = this.billingDisbursementC.internalId.substr(0, 3);
                            this.axPosting.Guid = this.billingDisbursementC.idBilling;
                            this.axPosting.JournalName = 'AP-Upload Unit';
                            this.axPosting.Name = 'Receiving Invoice ' + this.billingDisbursementC.vendorInvoice;
                            this.axPosting.TransDate = this.billingDisbursementC.dateIssued;
                            this.axPosting.TransactionType = 'Unit';

                            // debit
                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = '114411';
                            this.axPostingLine.AccountType = 'Ledger';
                            this.axPostingLine.AmountCurDebit = this.billingDisbursementC.totalUnitPrice.toString();
                            this.axPostingLine.AmountCurCredit = '0';
                            this.axPostingLine.Company = this.billingDisbursementC.internalId.substr(0, 3);
                            this.axPostingLine.DMSNum = this.listUploadInvoice[0].poNumber;
                            this.axPostingLine.Description = 'intransit ' + this.billingDisbursementC.vendorInvoice;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDisbursementC.internalId.substr(0, 3);
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = '000';
                            this.axPostingLine.Dimension5 = '00';
                            this.axPostingLine.Dimension6 = '000';
                            this.axPostingLine.DueDate = this.billingDisbursementC.dateIssued;
                            this.axPostingLine.Invoice = this.billingDisbursementC.vendorInvoice;
                            this.axPostingLine.Payment = '';
                            myaxPostingLine.push(this.axPostingLine);

                            if (this.ongkosAngkut > 0) {
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = '613402';
                                this.axPostingLine.AccountType = 'Ledger';
                                this.axPostingLine.AmountCurDebit = this.ongkosAngkut.toString();
                                this.axPostingLine.AmountCurCredit = '0';
                                this.axPostingLine.Company = this.billingDisbursementC.internalId.substr(0, 3);
                                this.axPostingLine.DMSNum = this.listUploadInvoice[0].poNumber;
                                this.axPostingLine.Description = 'biaya angkut ' + this.billingDisbursementC.vendorInvoice;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDisbursementC.internalId.substr(0, 3);
                                this.axPostingLine.Dimension3 = '012';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.billingDisbursementC.dateIssued;
                                this.axPostingLine.Invoice = this.billingDisbursementC.vendorInvoice;
                                this.axPostingLine.Payment = '';
                                myaxPostingLine.push(this.axPostingLine);
                            }

                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = '115101';
                            this.axPostingLine.AccountType = 'Ledger';
                            this.axPostingLine.AmountCurDebit = this.billingDisbursementC.totalPPN.toString();
                            this.axPostingLine.AmountCurCredit = '0';
                            this.axPostingLine.Company = this.billingDisbursementC.internalId.substr(0, 3);
                            this.axPostingLine.DMSNum = this.listUploadInvoice[0].poNumber;
                            this.axPostingLine.Description = 'PPN ' + this.billingDisbursementC.vendorInvoice;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDisbursementC.internalId.substr(0, 3);
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = '000';
                            this.axPostingLine.Dimension5 = '00';
                            this.axPostingLine.Dimension6 = '000';
                            this.axPostingLine.DueDate = this.billingDisbursementC.dateIssued;
                            this.axPostingLine.Invoice = this.billingDisbursementC.vendorInvoice;
                            this.axPostingLine.Payment = '';
                            myaxPostingLine.push(this.axPostingLine);

                            if (this.billingDisbursementC.totalDiscount > 0) {
                            // credit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = '114811';
                                this.axPostingLine.AccountType = 'Ledger';
                                this.axPostingLine.AmountCurDebit = '0';
                                this.axPostingLine.AmountCurCredit = this.billingDisbursementC.totalDiscount.toString();
                                this.axPostingLine.Company = this.billingDisbursementC.internalId.substr(0, 3);
                                this.axPostingLine.DMSNum = this.listUploadInvoice[0].poNumber;
                                this.axPostingLine.Description = 'PV/Discount ' + this.billingDisbursementC.vendorInvoice;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDisbursementC.internalId.substr(0, 3);
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.billingDisbursementC.dateIssued;
                                this.axPostingLine.Invoice = this.billingDisbursementC.vendorInvoice;
                                this.axPostingLine.Payment = '';
                                myaxPostingLine.push(this.axPostingLine);
                            }
                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = this.billingDisbursementC.vendorId;
                            this.axPostingLine.AccountType = 'Vend';
                            this.axPostingLine.AmountCurDebit = '0';
                            this.axPostingLine.AmountCurCredit = this.billingDisbursementC.totalAmount.toString();
                            this.axPostingLine.Company = this.billingDisbursementC.internalId.substr(0, 3);
                            this.axPostingLine.DMSNum = this.listUploadInvoice[0].poNumber;
                            this.axPostingLine.Description = 'AP ' + this.billingDisbursementC.vendorInvoice;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDisbursementC.internalId.substr(0, 3);
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = '000';
                            this.axPostingLine.Dimension5 = '00';
                            this.axPostingLine.Dimension6 = '000';
                            this.axPostingLine.DueDate = this.billingDisbursementC.dateIssued;
                            this.axPostingLine.Invoice = this.billingDisbursementC.vendorInvoice;
                            this.axPostingLine.Payment = '';
                            myaxPostingLine.push(this.axPostingLine);

                            this.axPosting.LedgerJournalLine = myaxPostingLine;
                            console.log('axPosting data :', this.axPosting);
                            this.axPostingService.send(this.axPosting).subscribe(
                                (resaxPosting: ResponseWrapper) =>
                                    console.log('Success : ', resaxPosting.json.Message),
                                (resaxPosting: ResponseWrapper) => {
                                    console.log('error ax posting : ', resaxPosting.json.Message);
                                }
                            );
                        }
                    );
                }
            },
            (err) => {
                this.loadingService.loadingStop();
                this.commonUtilService.showError(err);
                this.isSaving = false;
            }
        )
    }

    protected onSuccessUpload(): void {
        this.eventManager.broadcast({ name: 'billingDisbursementListModification', content: 'OK'});
        this.loadingService.loadingStop();
        this.isSaving = false;
        this.previousState();
    }

    protected previousState(): void {
        this.router.navigate(['billing-disbursement']);
    }

    public onFileChange(event: any): void {
        const tempListUploadInvoice = new Array<UploadInvoice>();

        this.listUploadInvoice = new Array<UploadInvoice>();
        this.convertUtilService.csvToArray(event, ';').then(
            (res) => {
                for (let i = 0; i < res.length; i++) {
                    const each = res[i];
                    console.log('each', each);
                    const uploadInv = new UploadInvoice();

                    uploadInv.invoiceNumber = each[0];
                    uploadInv.dateCreate = new Date(each[1]);
                    uploadInv.dateIssued = new Date(each[1]);
                    uploadInv.idProduct = each[2]
                    uploadInv.idColor = each[3];
                    uploadInv.customerCode = each[4];
                    uploadInv.mainDealerCode = each[5];
                    uploadInv.qty = each[6];
                    uploadInv.totalAmount = each[7];
                    uploadInv.ppn = each[8];
                    uploadInv.unitPrice = each[9];
                    uploadInv.unitDiscount = each[10];
                    uploadInv.poNumber = each[11];
                    uploadInv.dueDate = new Date(each[12]);

                    tempListUploadInvoice.push(uploadInv);
                }

                this.listUploadInvoice = tempListUploadInvoice;
            },
            (rej) => {
                console.log('reject');
            }
        )
    }
}
