import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { BillingDisbursement } from './billing-disbursement.model';
import { BillingDisbursementPopupService } from './billing-disbursement-popup.service';
import { BillingDisbursementService } from './billing-disbursement.service';
import { ToasterService, CommonUtilService} from '../../shared';
import { BillingType, BillingTypeService } from '../billing-type';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { Vendor, VendorService } from '../vendor';
import { ResponseWrapper } from '../../shared';
import { ITEMS_PER_PAGE, Principal } from '../../shared';

@Component({
    selector: 'jhi-billing-disbursement-dialog',
    templateUrl: './billing-disbursement-dialog.component.html'
})
export class BillingDisbursementDialogComponent implements OnInit {

    billingDisbursement: BillingDisbursement;
    isSaving: boolean;
    idBillingType: any;
    idInternal: any;
    idBillTo: any;
    idBillFrom: any;
    idVendor: any;

    billingtypes: BillingType[];

    internals: Internal[];

    billtos: BillTo[];

    vendors: Vendor[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected billingDisbursementService: BillingDisbursementService,
        protected billingTypeService: BillingTypeService,
        protected internalService: InternalService,
        protected billToService: BillToService,
        protected vendorService: VendorService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected commonUtilService: CommonUtilService,
        private principal: Principal
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.billingTypeService.query({size : 99999})
            .subscribe((res: ResponseWrapper) => { this.billingtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.internalService.query({size: 99999})
        //     .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.vendorService.query({size: 99999})
        //     .subscribe((res: ResponseWrapper) => { this.vendors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.loadVendor();
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;

        const objectBody: Object = {
            item: this.billingDisbursement
        };

        const objectHeader: Object = {
            execute: 'BuildAndValidateBillingDisbursement'
        };
        if (this.billingDisbursement.dateIssued != null && this.billingDisbursement.dateDue != null && this.billingDisbursement.vendorId != null) {
            this.billingDisbursement.internalId = this.principal.getIdInternal();
            this.subscribeToSaveResponse(
                this.billingDisbursementService.execute(objectBody, objectHeader));
        } else {
            if (this.billingDisbursement.dateIssued == null) {
                this.toaster.showToaster('info', 'save', 'Please Select Tanggal Invoice!')
            } else if (this.billingDisbursement.dateDue == null) {
                this.toaster.showToaster('info', 'save', 'Please Select Date Due!')
            } else if (this.billingDisbursement.vendorId == null) {
                this.toaster.showToaster('info', 'save', 'Please Select Main Dealer!')
            }
        }
    }

    protected subscribeToSaveResponse(result: Observable<BillingDisbursement>) {
        result.subscribe((res: BillingDisbursement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: BillingDisbursement) {
        this.eventManager.broadcast({ name: 'billingDisbursementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'billingDisbursement saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error: any) {
        this.commonUtilService.showError(error);
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'billingDisbursement Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackBillingTypeById(index: number, item: BillingType) {
        return item.idBillingType;
    }

    trackInternalById(index: number, item: Internal) {
        return item.organization.name;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }

    trackVendorById(index: number, item: Vendor) {
        return item.organization.name;
    }

    private loadVendor(): Promise<Vendor[]> {
        return new Promise<Vendor[]>(
            (resolve) => {
                this.vendorService.queryFilterByMainDealer({
                    idInternal: this.principal.getIdInternal(),
                    sort: ['idVendor', 'asc'],
                    size : 3000}).subscribe(
                    (res: ResponseWrapper) => {
                        // this.fetchMotor = false;
                        // this.convertVendorForSelect(res.json);
                        this.vendors = res.json;
                        console.log('vendor nih', this.vendors)
                    },
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => {
                });
                resolve();
            }
        );
    }
}

@Component({
    selector: 'jhi-billing-disbursement-popup',
    template: ''
})
export class BillingDisbursementPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected billingDisbursementPopupService: BillingDisbursementPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.billingDisbursementPopupService
                    .open(BillingDisbursementDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.billingDisbursementPopupService.parent = params['parent'];
                this.billingDisbursementPopupService
                    .open(BillingDisbursementDialogComponent as Component);
            } else {
                this.billingDisbursementPopupService
                    .open(BillingDisbursementDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
