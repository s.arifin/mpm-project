import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { BillingDisbursement } from './billing-disbursement.model';
import { BillingDisbursementService } from './billing-disbursement.service';
import { ToasterService, Principal } from '../../shared';
import { BillingType, BillingTypeService } from '../billing-type';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { Vendor, VendorService } from '../vendor';
import { ResponseWrapper } from '../../shared';

import { ConfirmationService } from 'primeng/primeng';

// nuse
import { AxPosting, AxPostingService } from '../axposting';
import { AxPostingLine } from '../axposting-line';
import { DatePipe } from '@angular/common';
import { BillingDisbursementC, BillingDisbursementCService } from '../billing-disbursement-c';

@Component({
    selector: 'jhi-billing-disbursement-edit',
    templateUrl: './billing-disbursement-edit.component.html'
})
export class BillingDisbursementEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    billingDisbursement: BillingDisbursement;
    isSaving: boolean;
    idBilling: any;
    paramPage: number;
    routeId: number;
    currentInternal: string;
    totFreightcost: number;

    billingtypes: BillingType[];
    internals: Internal[];
    billtos: BillTo[];
    vendors: Vendor[];
    dateCreate: Date;
    dateDue: Date;
    dateIssued: any;

    // nuse
    protected axPosting: AxPosting;
    protected axPostingLine: AxPostingLine;
    protected billingDisbursementC: BillingDisbursementC;

    constructor(
        protected alertService: JhiAlertService,
        protected billingDisbursementService: BillingDisbursementService,
        protected billingTypeService: BillingTypeService,
        protected internalService: InternalService,
        protected billToService: BillToService,
        protected vendorService: VendorService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected principal: Principal,
        protected confirmationService: ConfirmationService,
        protected axPostingService: AxPostingService,
        protected datePipe: DatePipe,
        protected billingDisbursementCService: BillingDisbursementCService,
    ) {
        this.billingDisbursement = new BillingDisbursement();
        this.routeId = 0;
        this.totFreightcost = 0;
        this.currentInternal = principal.getIdInternal();
        this.dateCreate = new Date();
        this.dateDue = new Date();
        // this.dateIssued = new Date();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idBilling = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        this.billingTypeService.query({size: 999999})
            .subscribe((res: ResponseWrapper) => { this.billingtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query({size: 999999})
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query({size: 999999})
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.vendorService.query()
        //     .subscribe((res: ResponseWrapper) => { this.vendors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.loadVendor();
    }
    // onChange() {
    //     this.hitungHargaInputManualheader()
    // }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.billingDisbursementService.find(this.idBilling).subscribe((billingDisbursement) => {
            this.billingDisbursement = billingDisbursement;
            this.hitungHargaInputManualheader();
            if (this.billingDisbursement.internalId === null || this.billingDisbursement.internalId === undefined ) {
                this.billingDisbursement.internalId = this.currentInternal;
            }
            this.calculate(billingDisbursement);
            console.log('billing = ', this.billingDisbursement);
            this.dateCreate = this.billingDisbursement.dateCreate;
            this.dateDue = this.billingDisbursement.dateDue;
            this.dateIssued = new Date (this.billingDisbursement.dateIssued);
            console.log('date = ', this.dateIssued);
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['./', { page: this.paramPage }]);
        }
    }

    save() {
        this.isSaving = true;
        if (this.billingDisbursement.idBilling !== undefined) {
            this.subscribeToSaveResponse(
                this.billingDisbursementService.update(this.billingDisbursement));
        } else {
            this.subscribeToSaveResponse(
                this.billingDisbursementService.create(this.billingDisbursement));
        }
    }

    protected subscribeToSaveResponse(result: Observable<BillingDisbursement>) {
        result.subscribe((res: BillingDisbursement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: BillingDisbursement) {
        this.eventManager.broadcast({ name: 'billingDisbursementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'billingDisbursement saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'billingDisbursement Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackBillingTypeById(index: number, item: BillingType) {
        return item.idBillingType;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }

    trackVendorById(index: number, item: Vendor) {
        return item.idVendor;
    }

    hitungHargaInputManualheader() {
        console.log('masuk ngga neehh');
        this.totFreightcost = (this.billingDisbursement.freightCost);
    }

    private loadVendor(): Promise<Vendor[]> {
        return new Promise<Vendor[]>(
            (resolve) => {
                this.vendorService.queryFilterByMainDealer({
                    idInternal: this.principal.getIdInternal(),
                    sort: ['idVendor', 'asc'],
                    size : 3000}).subscribe(
                    (res: ResponseWrapper) => {
                        // this.fetchMotor = false;
                        // this.convertVendorForSelect(res.json);
                        this.vendors = res.json;
                        console.log('vendor nih', this.vendors)
                    },
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => {
                });
                resolve();
            }
        );
    }
    activated() {
        this.billingDisbursementService.changeStatus(this.billingDisbursement, 11)
        .subscribe((r) => {
           this.load();
           this.toaster.showToaster('info', 'Data Activated', 'Activated.....');
         });
      }

      approved() {
        this.billingDisbursementService.changeStatus(this.billingDisbursement, 12)
        .subscribe((r) => {
           this.load();
           this.toaster.showToaster('info', 'Data Approved', 'Approved.....');
         });
      }

      completed() {
          this.billingDisbursementService.changeStatus(this.billingDisbursement, 17)
          .subscribe((r) => {
              this.eventManager.broadcast({
                  name: 'billingDisbursementListModification',
                  content: 'Completed an billingDisbursement'
               });
               this.toaster.showToaster('info', 'Data Completed', 'Completed.....');
               this.AXJournal(this.billingDisbursement);
               this.previousState();
           });
      }

      canceled() {
          this.confirmationService.confirm({
              message: 'Are you sure that you want to cancel?',
              header: 'Confirmation',
              icon: 'fa fa-question-circle',
              accept: () => {
                  this.billingDisbursementService.changeStatus(this.billingDisbursement, 13)
                  .subscribe((r) => {
                      this.eventManager.broadcast({
                          name: 'billingDisbursementListModification',
                          content: 'Cancel an billingDisbursement'
                       });
                       this.toaster.showToaster('info', 'Data billingDisbursement cancel', 'Cancel billingDisbursement.....');
                       this.previousState();
                   });
               }
           });
      }

      calculate(id: BillingDisbursement) {
          this.billingDisbursementService.calData(10, null, id)
          .subscribe(
              (res) => {
                  this.billingDisbursement = res;
                  console.log('hasil calculate', res)
              }
        )
      }

    protected AXJournal(BillingDis: BillingDisbursement) {
        this.billingDisbursementCService.findByInvoiceId(BillingDis.vendorInvoice).subscribe(
            (resBilDis) => {
                this.billingDisbursementC = resBilDis;

                const today = new Date();
                const myaxPostingLine = [];
                console.log('billing disbursement c : ', this.billingDisbursementC);
                this.axPosting = new AxPosting();
                this.axPosting.AutoPosting = 'FALSE';
                this.axPosting.AutoSettlement = 'FALSE';
                this.axPosting.DataArea = this.billingDisbursementC.internalId.substr(0, 3);
                this.axPosting.Guid = this.billingDisbursementC.idBilling;
                this.axPosting.JournalName = 'AP-Upload Unit';
                this.axPosting.Name = 'Receiving Invoice ' + this.billingDisbursementC.vendorInvoice;
                this.axPosting.TransDate = this.billingDisbursementC.dateIssued;
                this.axPosting.TransactionType = 'Unit';

                // debit
                this.axPostingLine = new AxPostingLine();
                this.axPostingLine.AccountNum = '114411';
                this.axPostingLine.AccountType = 'Ledger';
                this.axPostingLine.AmountCurDebit = this.billingDisbursementC.totalUnitPrice.toString();
                this.axPostingLine.AmountCurCredit = '0';
                this.axPostingLine.Company = this.billingDisbursementC.internalId.substr(0, 3);
                this.axPostingLine.DMSNum = this.billingDisbursementC.orderNumber;
                this.axPostingLine.Description = 'intransit ' + this.billingDisbursementC.vendorInvoice;
                this.axPostingLine.Dimension1 = '1014';
                this.axPostingLine.Dimension2 = this.billingDisbursementC.internalId.substr(0, 3);
                this.axPostingLine.Dimension3 = '000';
                this.axPostingLine.Dimension4 = '000';
                this.axPostingLine.Dimension5 = '00';
                this.axPostingLine.Dimension6 = '000';
                this.axPostingLine.DueDate = this.billingDisbursementC.dateIssued;
                this.axPostingLine.Invoice = this.billingDisbursementC.vendorInvoice;
                this.axPostingLine.Payment = '';
                myaxPostingLine.push(this.axPostingLine);

                if (this.billingDisbursementC.shippingFee > 0) {
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = '613402';
                    this.axPostingLine.AccountType = 'Ledger';
                    this.axPostingLine.AmountCurDebit = this.billingDisbursementC.shippingFee.toString();
                    this.axPostingLine.AmountCurCredit = '0';
                    this.axPostingLine.Company = this.billingDisbursementC.internalId.substr(0, 3);
                    this.axPostingLine.DMSNum = this.billingDisbursementC.orderNumber;
                    this.axPostingLine.Description = 'biaya angkut ' + this.billingDisbursementC.vendorInvoice;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDisbursementC.internalId.substr(0, 3);
                    this.axPostingLine.Dimension3 = '012';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.billingDisbursementC.dateIssued;
                    this.axPostingLine.Invoice = this.billingDisbursementC.vendorInvoice;
                    this.axPostingLine.Payment = '';
                    myaxPostingLine.push(this.axPostingLine);
                }

                this.axPostingLine = new AxPostingLine();
                this.axPostingLine.AccountNum = '115101';
                this.axPostingLine.AccountType = 'Ledger';
                this.axPostingLine.AmountCurDebit = this.billingDisbursementC.totalPPN.toString();
                this.axPostingLine.AmountCurCredit = '0';
                this.axPostingLine.Company = this.billingDisbursementC.internalId.substr(0, 3);
                this.axPostingLine.DMSNum = this.billingDisbursementC.orderNumber;
                this.axPostingLine.Description = 'PPN ' + this.billingDisbursementC.vendorInvoice;
                this.axPostingLine.Dimension1 = '1014';
                this.axPostingLine.Dimension2 = this.billingDisbursementC.internalId.substr(0, 3);
                this.axPostingLine.Dimension3 = '000';
                this.axPostingLine.Dimension4 = '000';
                this.axPostingLine.Dimension5 = '00';
                this.axPostingLine.Dimension6 = '000';
                this.axPostingLine.DueDate = this.billingDisbursementC.dateIssued;
                this.axPostingLine.Invoice = this.billingDisbursementC.vendorInvoice;
                this.axPostingLine.Payment = '';
                myaxPostingLine.push(this.axPostingLine);

                if (this.billingDisbursementC.totalDiscount > 0) {
                // credit
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountNum = '114811';
                    this.axPostingLine.AccountType = 'Ledger';
                    this.axPostingLine.AmountCurDebit = '0';
                    this.axPostingLine.AmountCurCredit = this.billingDisbursementC.totalDiscount.toString();
                    this.axPostingLine.Company = this.billingDisbursementC.internalId.substr(0, 3);
                    this.axPostingLine.DMSNum = this.billingDisbursementC.orderNumber;
                    this.axPostingLine.Description = 'PV/Discount ' + this.billingDisbursementC.vendorInvoice;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.billingDisbursementC.internalId.substr(0, 3);
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.billingDisbursementC.dateIssued;
                    this.axPostingLine.Invoice = this.billingDisbursementC.vendorInvoice;
                    this.axPostingLine.Payment = '';
                    myaxPostingLine.push(this.axPostingLine);
                }
                this.axPostingLine = new AxPostingLine();
                this.axPostingLine.AccountNum = this.billingDisbursementC.vendorId;
                this.axPostingLine.AccountType = 'Vend';
                this.axPostingLine.AmountCurDebit = '0';
                this.axPostingLine.AmountCurCredit = this.billingDisbursementC.totalAmount.toString();
                this.axPostingLine.Company = this.billingDisbursementC.internalId.substr(0, 3);
                this.axPostingLine.DMSNum = this.billingDisbursementC.orderNumber;
                this.axPostingLine.Description = 'AP ' + this.billingDisbursementC.vendorInvoice;
                this.axPostingLine.Dimension1 = '1014';
                this.axPostingLine.Dimension2 = this.billingDisbursementC.internalId.substr(0, 3);
                this.axPostingLine.Dimension3 = '000';
                this.axPostingLine.Dimension4 = '000';
                this.axPostingLine.Dimension5 = '00';
                this.axPostingLine.Dimension6 = '000';
                this.axPostingLine.DueDate = this.billingDisbursementC.dateIssued;
                this.axPostingLine.Invoice = this.billingDisbursementC.vendorInvoice;
                this.axPostingLine.Payment = '';
                myaxPostingLine.push(this.axPostingLine);

                this.axPosting.LedgerJournalLine = myaxPostingLine;
                console.log('axPosting data :', this.axPosting);
                this.axPostingService.send(this.axPosting).subscribe(
                    (resaxPosting: ResponseWrapper) =>
                        console.log('Success : ', resaxPosting.json.Message),
                    (resaxPosting: ResponseWrapper) => {
                        console.log('error ax posting : ', resaxPosting.json.Message);
                    }
                );
            }
        );
    }
}
