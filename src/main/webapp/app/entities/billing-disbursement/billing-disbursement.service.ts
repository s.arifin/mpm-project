import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';
import { BillingDisbursement } from './billing-disbursement.model';
import { AbstractEntityService } from '../../shared/base/abstract-entity.service';
import { Observable } from 'rxjs';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { Response } from '@angular/http';

@Injectable()
export class BillingDisbursementService extends AbstractEntityService<BillingDisbursement> {

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) {
        super(http, dateUtils);
        this.resourceUrl =  SERVER_API_URL + 'api/billing-disbursements';
        this.resourceSearchUrl = SERVER_API_URL + 'api/_search/billing-disbursements';
    }

    protected convertItemFromServer(json: any): BillingDisbursement {
        const entity: BillingDisbursement = Object.assign(new BillingDisbursement(), json);
        if (entity.dateCreate) {
            entity.dateCreate = new Date(entity.dateCreate);
        }
        if (entity.dateDue) {
            entity.dateDue = new Date(entity.dateDue);
        }
        return entity;
    }

    protected convert(billingDisbursement: BillingDisbursement): BillingDisbursement {
        if (billingDisbursement === null || billingDisbursement === {}) {
            return {};
        }
        const copy: BillingDisbursement = JSON.parse(JSON.stringify(billingDisbursement));
        return copy;
    }

    calData(id: Number, param: String, billingDisbursement: BillingDisbursement): Observable<BillingDisbursement> {
        const copy = this.convert(billingDisbursement);
        return this.http.post(`${this.resourceUrl}/calData/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }
}
