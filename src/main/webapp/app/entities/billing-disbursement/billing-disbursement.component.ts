import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { BillingDisbursement } from './billing-disbursement.model';
import { BillingDisbursementService } from './billing-disbursement.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BillingDisbursementDialogComponent } from './billing-disbursement-dialog.component';
import { BillingDisbursementEditComponent } from './billing-disbursement-edit.component';

@Component({
    selector: 'jhi-billing-disbursement',
    templateUrl: './billing-disbursement.component.html'
})
export class BillingDisbursementComponent implements OnInit, OnDestroy {

    currentAccount: any;
    billingDisbursements: BillingDisbursement[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: Number;
    currentInternal: string;

    constructor(
        protected billingDisbursementService: BillingDisbursementService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toasterService: ToasterService,
        protected modalService: NgbModal
    ) {
        this.currentInternal = principal.getIdInternal();
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
                    this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
    }

    loadAll() {
        if (this.currentSearch) {
            this.billingDisbursementService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.billingDisbursementService.queryFilterBy({
            idInternal : this.currentInternal,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/billing-disbursement'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/billing-disbursement', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/billing-disbursement', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInBillingDisbursements();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: BillingDisbursement) {
        return item.idBilling;
    }

    registerChangeInBillingDisbursements() {
        this.eventSubscriber = this.eventManager.subscribe('billingDisbursementListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idBilling') {
            result.push('idBilling');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.billingDisbursements = data;
    }

    protected onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.billingDisbursementService.execute(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.billingDisbursementService.update(event.data)
                .subscribe((res: BillingDisbursement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.billingDisbursementService.create(event.data)
                .subscribe((res: BillingDisbursement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: BillingDisbursement) {
        this.toasterService.showToaster('info', 'BillingDisbursement Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.billingDisbursementService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'billingDisbursementListModification',
                        content: 'Deleted an billingDisbursement'
                    });
                });
            }
        });
    }

    process() {
        this.billingDisbursementService.process({}).subscribe((r) => {
            console.log('result', r);
            this.toasterService.showToaster('info', 'Data Processed', 'processed.....');
        });
    }

    getCurrentStatusDesc(data): string {
        let result = '';

        switch (data) {
            case 10:
                result = 'DRAFT';
                break;
            case 11:
                result = 'OPEN';
                break;
            default:
                result = 'NONE'
          }
        return result;
    }

    buildReindex() {
        this.billingDisbursementService.process({command: 'buildIndex'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }

    openAdd() {
        const modalRef = this.modalService.open(BillingDisbursementDialogComponent, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.billingDisbursement = new BillingDisbursement();
        modalRef.componentInstance.billingDisbursement.billingTypeId = 12;
        modalRef.result.then((result) => {}, (reason) => {});
    }

    openEdit(item) {
        const modalRef = this.modalService.open(BillingDisbursementEditComponent, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.billingDisbursement = item;
        modalRef.result.then((result) => {}, (reason) => {});
    }

}
