import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { BillingDisbursement } from './billing-disbursement.model';
import { BillingDisbursementService } from './billing-disbursement.service';

@Injectable()
export class BillingDisbursementPopupService {
    protected ngbModalRef: NgbModalRef;
    idBillingType: any;
    idInternal: any;
    idBillTo: any;
    idBillFrom: any;
    idVendor: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected billingDisbursementService: BillingDisbursementService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.billingDisbursementService.find(id).subscribe((data) => {
                    // if (data.dateCreate) {
                    //    data.dateCreate = this.datePipe
                    //        .transform(data.dateCreate, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    // if (data.dateDue) {
                    //    data.dateDue = this.datePipe
                    //        .transform(data.dateDue, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.billingDisbursementModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new BillingDisbursement();
                    data.billingTypeId = this.idBillingType;
                    data.internalId = this.idInternal;
                    data.billToId = this.idBillTo;
                    data.billFromId = this.idBillFrom;
                    data.vendorId = this.idVendor;
                    this.ngbModalRef = this.billingDisbursementModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    billingDisbursementModalRef(component: Component, billingDisbursement: BillingDisbursement): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.billingDisbursement = billingDisbursement;
        modalRef.componentInstance.idBillingType = this.idBillingType;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idBillTo = this.idBillTo;
        modalRef.componentInstance.idBillFrom = this.idBillFrom;
        modalRef.componentInstance.idVendor = this.idVendor;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.billingDisbursementLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    billingDisbursementLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
