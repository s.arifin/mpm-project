import { Billing } from '../billing/billing.model';

export class BillingDisbursement extends Billing {
    constructor(
        public vendorInvoice?: string,
        public vendorId?: any,
        public vendorName?: any,
        public dateIssued?: Date,
        public discountLine?: number,
        public freightCost?: number,
        public totalFeightCost?: number
    ) {
        super();
    }
}

export class UploadInvoice {
    constructor(
        public invoiceNumber?: string,
        public dateCreate?: Date,
        public idProduct?: string,
        public idColor?: string,
        public customerCode?: string,
        public mainDealerCode?: string,
        public qty?: number,
        public totalAmount?: number,
        public ppn?: number,
        public unitPrice?: number,
        public unitDiscount?: number,
        public poNumber?: string,
        public dueDate?: Date,
        public dateIssued?: Date,
        public discountLine?: number,
        public freightCost?: number,
        public totalFeightCost?: number
    ) {
        this.dateCreate = new Date();
    }
}
