import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { BillingDisbursementComponent } from './billing-disbursement.component';
import { BillingDisbursementEditComponent } from './billing-disbursement-edit.component';
import { BillingDisbursementPopupComponent } from './billing-disbursement-dialog.component';
import { BillingDisbursementUploadComponent } from './billing-disbursement-upload.component';

@Injectable()
export class BillingDisbursementResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'vendorInvoice,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const billingDisbursementRoute: Routes = [
    {
        path: '',
        component: BillingDisbursementComponent,
        resolve: {
            'pagingParams': BillingDisbursementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingDisbursement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const billingDisbursementPopupRoute: Routes = [
    {
        path: 'popup-new',
        component: BillingDisbursementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingDisbursement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'new',
        component: BillingDisbursementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingDisbursement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'upload',
        component: BillingDisbursementUploadComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingDisbursement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: BillingDisbursementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingDisbursement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':route/:page/:id/edit',
        component: BillingDisbursementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingDisbursement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/popup-edit',
        component: BillingDisbursementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingDisbursement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
