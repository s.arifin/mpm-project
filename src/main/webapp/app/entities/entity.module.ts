import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { MpmPurposeTypeModule } from './purpose-type/purpose-type.module';
import { MpmPeriodTypeModule } from './period-type/period-type.module';
import { MpmReligionTypeModule } from './religion-type/religion-type.module';
import { MpmWorkTypeModule } from './work-type/work-type.module';
import { MpmUomTypeModule } from './uom-type/uom-type.module';
import { MpmUomModule } from './uom/uom.module';
import { MpmOrganizationModule } from './organization/organization.module';
import { MpmFeatureTypeModule } from './feature-type/feature-type.module';
import { MpmFeatureModule } from './feature/feature.module';
import { MpmGoodModule } from './good/good.module';
import { MpmFacilityTypeModule } from './facility-type/facility-type.module';
import { MpmFacilityModule } from './facility/facility.module';
// import { MpmVendorModule } from './vendor/vendor.module';
import { MpmRelationTypeModule } from './relation-type/relation-type.module';
import { MpmPartyRelationshipModule } from './party-relationship/party-relationship.module';
import { MpmPersonalCustomerModule } from './personal-customer/personal-customer.module';
import { MpmOrganizationCustomerModule } from './organization-customer/organization-customer.module';
import { MpmParentOrganizationModule } from './parent-organization/parent-organization.module';
// import { MpmBranchModule } from './branch/branch.module';
import { MpmRequirementModule } from './requirement/requirement.module';
import { MpmUnitReqInternalModule } from './unit-req-internal/unit-req-internal.module';
import { MpmOrdersModule } from './orders/orders.module';
import { MpmOrderItemModule } from './order-item/order-item.module';
// import { MpmPurchaseOrderModule } from './purchase-order/purchase-order.module';
import { MpmShipmentModule } from './shipment/shipment.module';
import { MpmShipmentItemModule } from './shipment-item/shipment-item.module';
import { MpmBillingModule } from './billing/billing.module';
import { MpmBillingItemModule } from './billing-item/billing-item.module';
import { MpmPaymentModule } from './payment/payment.module';
import { MpmMotorModule } from './motor/motor.module';
import { MpmProductTypeModule } from './product-type/product-type.module';
import { MpmServiceModule } from './service/service.module';
import { MpmContainerTypeModule } from './container-type/container-type.module';
import { MpmContainerModule } from './container/container.module';
import { MpmShipmentPackageModule } from './shipment-package/shipment-package.module';
import { MpmShipmentReceiptModule } from './shipment-receipt/shipment-receipt.module';
import { MpmInventoryItemModule } from './inventory-item/inventory-item.module';
import { MpmCustomerTypeModule } from './customer-type/customer-type.module';
import { MpmSalesSourceModule } from './sales-source/sales-source.module';
import { MpmProspectModule } from './prospect/prospect.module';
import { MpmSalesmanModule } from './salesman/salesman.module';
import { MpmSuspectTypeModule } from './suspect-type/suspect-type.module';
import { MpmSuspectModule } from './suspect/suspect.module';
import { MpmPartyCategoryModule } from './party-category/party-category.module';
import { MpmProductCategoryModule } from './product-category/product-category.module';
import { MpmCalendarTypeModule } from './calendar-type/calendar-type.module';
import { MpmStandardCalendarModule } from './standard-calendar/standard-calendar.module';
import { MpmRemPartModule } from './rem-part/rem-part.module';
import { MpmBrokerTypeModule } from './broker-type/broker-type.module'
import { MpmSalesBrokerModule } from './sales-broker/sales-broker.module';
import { MpmMechanicModule } from './mechanic/mechanic.module';
import { MpmWorkOrderBookingModule } from './work-order-booking/work-order-booking.module';
import { MpmBookingSlotModule } from './booking-slot/booking-slot.module';
import { MpmBookingSlotStandardModule } from './booking-slot-standard/booking-slot-standard.module';
import { MpmBookingTypeModule } from './booking-type/booking-type.module';
import { MpmWorkOrderModule } from './work-order/work-order.module';
import { MpmWorkProductRequirementModule } from './work-product-requirement/work-product-requirement.module';
import { MpmWorkServiceRequirementModule } from './work-service-requirement/work-service-requirement.module';
import { MpmWorkRequirementModule } from './work-requirement/work-requirement.module';
import { MpmVehicleModule } from './vehicle/vehicle.module';
import { MpmVehicleIdentificationModule } from './vehicle-identification/vehicle-identification.module';
import { MpmEventTypeModule } from './event-type/event-type.module';
import { MpmBaseCalendarModule } from './base-calendar/base-calendar.module';
import { MpmPriceComponentModule } from './price-component/price-component.module';
import { MpmWorkEffortTypeModule } from './work-effort-type/work-effort-type.module';
import { MpmWeServiceTypeModule } from './we-service-type/we-service-type.module';
import { MpmBookingListViewModule } from './booking-list-view/booking-list-view.module';
import { MpmAssociationTypeModule } from './association-type/association-type.module';
import { MpmOrderTypeModule } from './order-type/order-type.module';
import { MpmBillingTypeModule } from './billing-type/billing-type.module';
import { MpmPaymentApplicationModule } from './payment-application/payment-application.module';
import { MpmWorkEffortModule } from './work-effort/work-effort.module';
import { MpmVendorProductModule } from './vendor-product/vendor-product.module';
import { MpmRequirementTypeModule } from './requirement-type/requirement-type.module';
import { MpmProspectSourceEventModule } from './prospect-source-event/prospect-source-event.module';
import { MpmProspectSourceModule } from './prospect-source/prospect-source.module';
import { MpmProductAssociationModule } from './product-association/product-association.module';
import { MpmPaymentTypeModule } from './payment-type/payment-type.module';
import { MpmGoodContainerModule } from './good-container/good-container.module';
import { MpmPositionTypeModule } from './position-type/position-type.module';
import { MpmPositionModule } from './position/position.module';
import { MpmPositionFullfillmentModule } from './position-fullfillment/position-fullfillment.module';
import { MpmPositionReportingStructureModule } from './position-reporting-structure/position-reporting-structure.module';
import { MpmContactMechanismModule } from './contact-mechanism/contact-mechanism.module';
import { MpmShipToModule } from './ship-to/ship-to.module';
import { MpmBillToModule } from './bill-to/bill-to.module';
import { MpmVehicleSalesOrderModule } from './vehicle-sales-order/vehicle-sales-order.module';
import { MpmPartSalesOrderModule } from './part-sales-order/part-sales-order.module';
import { MpmRegularSalesOrderModule } from './regular-sales-order/regular-sales-order.module';
import { MpmSalesOrderModule } from './sales-order/sales-order.module';
import { MpmVehiclePurchaseOrderModule } from './vehicle-purchase-order/vehicle-purchase-order.module';
import { MpmProductPurchaseOrderModule } from './product-purchase-order/product-purchase-order.module';
import { MpmReasonTypeModule } from './reason-type/reason-type.module';
import { MpmMotorDueReminderModule } from './motor-due-reminder/motor-due-reminder.module';
import { MpmServiceReminderModule } from './service-reminder/service-reminder.module';
import { MpmDealerReminderTypeModule } from './dealer-reminder-type/dealer-reminder-type.module';
import { MpmDealerClaimTypeModule } from './dealer-claim-type/dealer-claim-type.module';
import { MpmGeoBoundaryTypeModule } from './geo-boundary-type/geo-boundary-type.module';
import { MpmInternalOrderModule } from './internal-order/internal-order.module';
import { MpmSaleTypeModule } from './sale-type/sale-type.module';
import { MpmLeasingCompanyModule } from './leasing-company/leasing-company.module';
import { MpmSalesUnitRequirementModule } from './sales-unit-requirement/sales-unit-requirement.module';
import { MpmProspectPersonModule } from './prospect-person/prospect-person.module';
import { MpmProspectPersonKorsalModule } from './prospect-person-korsal/prospect-person-korsal.module';
import { MpmProspectOrganizationModule } from './prospect-organization/prospect-organization.module';
import { MpmReceiptModule } from './receipt/receipt.module';
import { MpmDisbursementModule } from './disbursement/disbursement.module';
import { MpmDocumentTypeModule } from './document-type/document-type.module';
import { MpmDocumentsModule } from './documents/documents.module';
import { MpmPartyDocumentModule } from './party-document/party-document.module';
import { MpmInventoryMovementModule } from './inventory-movement/inventory-movement.module';
import { MpmPickingSlipModule } from './picking-slip/picking-slip.module';
// import { MpmVehicleDocumentRequirementModule } from './vehicle-document-requirement/vehicle-document-requirement.module';
import { MpmShipmentIncomingModule } from './shipment-incoming/shipment-incoming.module';
import { MpmShipmentOutgoingModule } from './shipment-outgoing/shipment-outgoing.module';
import { MpmGeoBoundaryModule } from './geo-boundary/geo-boundary.module';
import { MpmProvinceModule } from './province/province.module';
import { MpmCityModule } from './city/city.module';
import { MpmDistrictModule } from './district/district.module';
import { MpmVillageModule } from './village/village.module';
import { MpmSuspectPersonModule } from './suspect-person/suspect-person.module';
import { MpmSuspectOrganizationModule } from './suspect-organization/suspect-organization.module';
import { MpmSalesUnitLeasingModule } from './sales-unit-leasing/sales-unit-leasing.module';
import { MpmPaymentMethodTypeModule } from './payment-method-type/payment-method-type.module';
import { MpmShipmentPackageTypeModule } from './shipment-package-type/shipment-package-type.module';
import { MpmLeasingTenorProvideModule } from './leasing-tenor-provide/leasing-tenor-provide.module';
import { MpmLeasingTypeModule } from './leasing-type/leasing-type.module';
import { MpmCommunicationEventModule } from './communication-event/communication-event.module';
import { MpmBillingReceiptModule } from './billing-receipt/billing-receipt.module';
import { MpmVehicleSalesBillingModule } from './vehicle-sales-billing/vehicle-sales-billing.module';
import { MpmCommunicationEventProspectModule } from './communication-event-prospect/communication-event-prospect.module';
import { MpmQuoteModule } from './quote/quote.module';
import { MpmQuoteItemModule } from './quote-item/quote-item.module';
// import { MpmUnitDeliverableModule } from './unit-deliverable/unit-deliverable.module';
import { MpmProductDocumentModule } from './product-document/product-document.module';
import { MpmProspectKorsalModule } from './prospect-korsal/prospect-korsal.module';
import { MpmCommunicationEventCDBModule } from './communication-event-cdb/communication-event-cdb.module';
import { MpmSimulasiCreditModule } from './simulasi-credit/simulasi-credit.module';
import { MpmApprovalKacabModule } from './approval-kacab/approval-kacab.module';
import { MpmApprovalModule } from './approval/approval.module';
import { MpmApprovalKorsalModule } from './approval-korsal/approval-korsal.module';
// import { MessagePipe } from '../shared/pipe/message.pipe';
import { MpmRuleSalesDiscountModule } from './rule-sales-discount/rule-sales-discount.module';
import { MpmUnitAccesoriesMapperModule } from './unit-accesories-mapper/unit-accesories-mapper.module';
import { MpmVendorTypeModule } from './vendor-type/vendor-type.module';
import { MpmShipmentTypeModule } from './shipment-type/shipment-type.module';
import { MpmDriverModule } from './driver/driver.module';
import { MpmDeliveryOrderModule } from './delivery-order/delivery-order.module';
import { MpmDeliveryOrderDriverModule } from './delivery-order-driver/delivery-order-driver.module';
import { MpmMotorCatalogModule } from './motor-catalog/motor-catalog.module';
import { MpmRuleHotItemModule } from './rule-hot-item/rule-hot-item.module';
import { MpmCommunicationEventDeliveryModule } from './communication-event-delivery/communication-event-delivery.module';
import { MpmMemosModule } from './memos/memos.module';
import { MpmUserMediatorModule } from './user-mediator/user-mediator.module';
import { MpmMemoTypeModule } from './memo-type/memo-type.module';
import { MpmStockOpnameTypeModule } from './stock-opname-type/stock-opname-type.module';
import { MpmStockOpnameModule } from './stock-opname/stock-opname.module';
import { MpmStockOpnameItemModule } from './stock-opname-item/stock-opname-item.module';
// import { MpmBillingDisbursementModule } from './billing-disbursement/billing-disbursement.module';
import { MpmProspectOrganizationDetailsModule } from './prospect-organization-details/prospect-organization-details.module';
import { MpmMovingSlipModule } from './moving-slip/moving-slip.module';
import { MpmProductShipmentReceiptModule } from './product-shipment-receipt/product-shipment-receipt.module';
import { MpmOrderShipmentItemModule } from './order-shipment-item/order-shipment-item.module';
import { MpmOrderBillingItemModule } from './order-billing-item/order-billing-item.module';
import { MpmReturnPurchaseOrderModule } from './return-purchase-order/return-purchase-order.module';
import { MpmReturnSalesOrderModule } from './return-sales-order/return-sales-order.module';
import { MpmProductSalesOrderModule } from './product-sales-order/product-sales-order.module';
import { MpmProductPackageReceiptModule } from './product-package-receipt/product-package-receipt.module';
import { MpmShipmentBillingItemModule } from './shipment-billing-item/shipment-billing-item.module';
import { MpmUnitShipmentReceiptModule } from './unit-shipment-receipt/unit-shipment-receipt.module';
// import { MpmPackageReceiptModule } from './package-receipt/package-receipt.module';
import { MpmProductShipmentIncomingModule } from './product-shipment-incoming/product-shipment-incoming.module';
import { MpmPostalAddressModule } from './postal-address/postal-address.module';
import { MpmPaymentMethodModule } from './payment-method/payment-method.module';
import { MpmOrderPaymentModule } from './order-payment/order-payment.module';
import { MpmRequirementPaymentModule } from './requirement-payment/requirement-payment.module';
import { MpmFacilityContactMechanismModule } from './facility-contact-mechanism/facility-contact-mechanism.module';
import { MpmPartyFacilityPurposeModule } from './party-facility-purpose/party-facility-purpose.module';
import { MpmUomConversionModule } from './uom-conversion/uom-conversion.module';
import { MpmProductCategoryTypeModule } from './product-category-type/product-category-type.module';
import { MpmPartyCategoryTypeModule } from './party-category-type/party-category-type.module';
import { MpmRequestTypeModule } from './request-type/request-type.module';
import { MpmRequestModule } from './request/request.module';
import { MpmRequestProductModule } from './request-product/request-product.module';
import { MpmRequestItemModule } from './request-item/request-item.module';
import { MpmUnitDocumentMessageModule } from './unit-document-message/unit-document-message.module';
import { MpmSalesBookingModule } from './sales-booking/sales-booking.module';
import { MpmVehicleCustomerRequestModule } from './vehicle-customer-request/vehicle-customer-request.module';
import { MpmCustomerRequestItemModule } from './customer-request-item/customer-request-item.module';
import { MpmRequestRequirementModule } from './request-requirement/request-requirement.module';
import { MpmRuleIndentModule } from './rule-indent/rule-indent.module';
import { MpmOrderRequestItemModule } from './order-request-item/order-request-item.module';
import { MpmRequestUnitInternalModule } from './request-unit-internal/request-unit-internal.module';
import { MpmPreRequestUnitInternalModule } from './pre-request-unit-internal/pre-request-unit-internal.module';
import { MpmReportModule } from './report/report.module';
import { MpmUnitPreparationModule } from './unit-preparation/unit-preparation.module';
import { MpmUnitShipmentOutgoingModule } from './unit-shipment-outgoing/unit-shipment-outgoing.module';
import { MpmApprovalLeasingModule } from './approval-leasing/approval-leasing.module';
import { MpmGeneralUploadModule } from './general-upload/general-upload.module';
// nuse
import { MpmAxPostingModule } from './axposting/axposting.module';
import { MpmAxPostingLineModule } from './axposting-line/axposting-line.module';
import { MpmAxCustomerModule} from './axcustomer';
import { MpmBillingDisbursementCModule } from './billing-disbursement-c';
import { MpmShipmentReceiptCModule } from './shipment-receipt-c';
import { MpmVehicleDocumentRequirementCModule } from './vehicle-document-requirement-c';
import { MpmVendorRelationshipModule } from './vendor-relationship/vendor-relationship.module';
import { MpmCustomerRelationshipModule } from './customer-relationship/customer-relationship.module';
// import { MpmFeatureApplicableModule } from './feature-applicable/feature-applicable.module';
import { MpmVehicleRegistrationModule } from './vehicle-registration/vehicle-registration.module';
// import { MpmMaterialPromoMainDealerModule } from './material-promo/material-promo-main-dealer.module';
import { MpmMasterAksesorisModule } from './master-aksesoris/master-aksesoris.module';
import { MpmApprovalKacabReceiveModule } from './approval-kacab-receive/approval-kacab-receive.module';
import { MpmApprovalKacabRequestModule } from './approval-kacab-request/approval-kacab-request.module';
import { MpmMappingAccessoriesModule } from './mapping-accessories/mapping-accessories.module';
import { MpmTransferUnitModule } from './transfer-unit/transfer-unit.module';
import { MpmPackageAssociationModule } from './package-association/package-association.module';
import { MpmTransferUnitRequestModule } from './transfer-unit-request/transfer-unit-request.module';
import { MpmMaterialPromoModule } from './material-promo/material-promo.module';
import { MpmMasterMaterialPromoModule } from './master-material-promo/master-material-promo.module';
import { MpmPelanggaranLuarWilayahModule } from './pelanggaran-luar-wilayah/pelanggaran-luar-wilayah.module';
import { MpmMokitaModule } from './mokita/mokita.module';
import { MpmSalesTargetModule } from './sales-target/sales-target.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        MpmApprovalLeasingModule,
        MpmReportModule,
        MpmPreRequestUnitInternalModule,
        MpmApprovalKacabModule,
        MpmPurposeTypeModule,
        MpmPeriodTypeModule,
        MpmReligionTypeModule,
        MpmWorkTypeModule,
        MpmUomTypeModule,
        MpmUomModule,
        MpmOrganizationModule,
        MpmFeatureTypeModule,
        MpmFeatureModule,
        MpmGoodModule,
        MpmFacilityTypeModule,
        MpmFacilityModule,
        // MpmVendorModule,
        MpmRelationTypeModule,
        MpmPartyRelationshipModule,
        MpmPersonalCustomerModule,
        MpmOrganizationCustomerModule,
        MpmParentOrganizationModule,
        // MpmBranchModule,
        MpmRequirementModule,
        MpmUnitReqInternalModule,
        MpmOrdersModule,
        MpmOrderItemModule,
        // MpmPurchaseOrderModule,
        MpmShipmentModule,
        MpmShipmentItemModule,
        MpmBillingModule,
        MpmBillingItemModule,
        MpmPaymentModule,
        MpmMotorModule,
        MpmProductTypeModule,
        MpmServiceModule,
        MpmContainerTypeModule,
        MpmContainerModule,
        MpmShipmentPackageModule,
        MpmShipmentReceiptModule,
        MpmInventoryItemModule,
        MpmCustomerTypeModule,
        MpmSalesSourceModule,
        MpmProspectModule,
        MpmSalesmanModule,
        MpmSuspectTypeModule,
        MpmSuspectModule,
        MpmPartyCategoryModule,
        MpmProductCategoryModule,
        MpmCalendarTypeModule,
        MpmStandardCalendarModule,
        MpmRemPartModule,
        MpmBrokerTypeModule,
        MpmSalesBrokerModule,
        MpmMechanicModule,
        MpmWorkOrderBookingModule,
        MpmBookingSlotModule,
        MpmBookingSlotStandardModule,
        MpmBookingTypeModule,
        MpmWorkOrderModule,
        MpmWorkProductRequirementModule,
        MpmWorkServiceRequirementModule,
        MpmWorkRequirementModule,
        MpmVehicleModule,
        MpmVehicleIdentificationModule,
        MpmEventTypeModule,
        MpmBaseCalendarModule,
        MpmPriceComponentModule,
        MpmWorkEffortTypeModule,
        MpmWeServiceTypeModule,
        MpmBookingListViewModule,
        MpmAssociationTypeModule,
        MpmOrderTypeModule,
        MpmBillingTypeModule,
        // MpmPackageReceiptModule,
        // MpmAksesorisModule,
        MpmPaymentApplicationModule,
        MpmWorkEffortModule,
        MpmVendorProductModule,
        MpmRequirementTypeModule,
        MpmProspectSourceEventModule,
        MpmProspectSourceModule,
        MpmProductAssociationModule,
        MpmPaymentTypeModule,
        MpmGoodContainerModule,
        MpmPositionTypeModule,
        MpmPositionModule,
        MpmPositionFullfillmentModule,
        MpmPositionReportingStructureModule,
        MpmContactMechanismModule,
        MpmShipToModule,
        MpmBillToModule,
        MpmVehicleSalesOrderModule,
        MpmPartSalesOrderModule,
        MpmRegularSalesOrderModule,
        MpmSalesOrderModule,
        MpmVehiclePurchaseOrderModule,
        MpmProductPurchaseOrderModule,
        MpmReasonTypeModule,
        MpmMotorDueReminderModule,
        MpmServiceReminderModule,
        MpmDealerReminderTypeModule,
        MpmDealerClaimTypeModule,
        MpmGeoBoundaryTypeModule,
        MpmInternalOrderModule,
        MpmSaleTypeModule,
        MpmLeasingCompanyModule,
        MpmLeasingTypeModule,
        MpmSalesUnitRequirementModule,
        MpmProspectPersonModule,
        MpmProspectPersonKorsalModule,
        MpmProspectOrganizationModule,
        MpmReceiptModule,
        MpmDisbursementModule,
        MpmDocumentTypeModule,
        MpmDocumentsModule,
        MpmPartyDocumentModule,
        MpmInventoryMovementModule,
        MpmPickingSlipModule,
        // MpmVehicleDocumentRequirementModule,
        MpmShipmentIncomingModule,
        MpmShipmentOutgoingModule,
        MpmGeoBoundaryModule,
        MpmProvinceModule,
        MpmCityModule,
        MpmDistrictModule,
        MpmVillageModule,
        MpmSuspectPersonModule,
        MpmSuspectOrganizationModule,
        MpmSalesUnitLeasingModule,
        MpmPaymentMethodTypeModule,
        MpmShipmentPackageTypeModule,
        MpmLeasingTenorProvideModule,
        MpmCommunicationEventModule,
        MpmBillingReceiptModule,
        MpmVehicleSalesBillingModule,
        MpmCommunicationEventProspectModule,
        MpmQuoteModule,
        MpmQuoteItemModule,
        // MpmUnitDeliverableModule,
        MpmProductDocumentModule,
        MpmProspectKorsalModule,
        MpmCommunicationEventCDBModule,
        MpmSimulasiCreditModule,
        MpmApprovalModule,
        MpmRuleSalesDiscountModule,
        MpmApprovalKorsalModule,
        MpmUnitAccesoriesMapperModule,
        MpmVendorTypeModule,
        MpmShipmentTypeModule,
        MpmDriverModule,
        MpmDeliveryOrderModule,
        MpmDeliveryOrderDriverModule,
        MpmMotorCatalogModule,
        MpmRuleHotItemModule,
        MpmCommunicationEventDeliveryModule,
        MpmMemosModule,
        MpmUserMediatorModule,
        MpmMemoTypeModule,
        MpmStockOpnameTypeModule,
        MpmStockOpnameModule,
        MpmStockOpnameItemModule,
        // MpmBillingDisbursementModule,
        MpmProspectOrganizationDetailsModule,
        MpmMovingSlipModule,
        MpmProductShipmentReceiptModule,
        MpmOrderShipmentItemModule,
        MpmOrderBillingItemModule,
        MpmReturnPurchaseOrderModule,
        MpmReturnSalesOrderModule,
        MpmProductSalesOrderModule,
        MpmProductPackageReceiptModule,
        MpmShipmentBillingItemModule,
        MpmUnitShipmentReceiptModule,
        MpmProductShipmentIncomingModule,
        MpmPostalAddressModule,
        MpmPaymentMethodModule,
        MpmOrderPaymentModule,
        MpmRequirementPaymentModule,
        MpmFacilityContactMechanismModule,
        MpmPartyFacilityPurposeModule,
        MpmUomConversionModule,
        MpmProductCategoryTypeModule,
        MpmPartyCategoryTypeModule,
        MpmRequestTypeModule,
        MpmRequestModule,
        MpmRequestProductModule,
        MpmRequestItemModule,
        MpmUnitDocumentMessageModule,
        MpmSalesBookingModule,
        MpmRequestRequirementModule,
        MpmRuleIndentModule,
        MpmVehicleCustomerRequestModule,
        MpmOrderRequestItemModule,
        MpmCustomerRequestItemModule,
        MpmRequestUnitInternalModule,
        MpmUnitPreparationModule,
        MpmUnitShipmentOutgoingModule,
        MpmGeneralUploadModule,
        // MpmMaterialPromoMainDealerModule,
        MpmMasterAksesorisModule,
        MpmMappingAccessoriesModule,
        // nuse
        MpmAxPostingModule,
        MpmAxPostingLineModule,
        MpmAxCustomerModule,
        MpmBillingDisbursementCModule,
        MpmVendorRelationshipModule,
        MpmCustomerRelationshipModule,
        MpmShipmentReceiptCModule,
        MpmVehicleDocumentRequirementCModule,
        // MpmFeatureApplicableModule,
        // MpmVehicleRegistrationModule,
        MpmApprovalKacabReceiveModule,
        MpmApprovalKacabRequestModule,
        MpmTransferUnitModule,
        MpmPackageAssociationModule,
        MpmTransferUnitRequestModule,
        MpmMaterialPromoModule,
        MpmMasterMaterialPromoModule,
        MpmPelanggaranLuarWilayahModule,
        MpmMokitaModule,
        MpmSalesTargetModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmEntityModule {}
