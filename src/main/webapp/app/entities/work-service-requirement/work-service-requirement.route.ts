import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { WorkServiceRequirementComponent } from './work-service-requirement.component';
import { WorkServiceRequirementPopupComponent } from './work-service-requirement-dialog.component';

@Injectable()
export class WorkServiceRequirementResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idWe,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const workServiceRequirementRoute: Routes = [
    {
        path: 'work-service-requirement',
        component: WorkServiceRequirementComponent,
        resolve: {
            'pagingParams': WorkServiceRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workServiceRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const workServiceRequirementPopupRoute: Routes = [
    {
        path: 'work-service-requirement-new',
        component: WorkServiceRequirementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workServiceRequirement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'work-service-requirement/:id/edit',
        component: WorkServiceRequirementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workServiceRequirement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
