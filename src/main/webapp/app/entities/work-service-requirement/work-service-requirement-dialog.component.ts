import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {WorkServiceRequirement} from './work-service-requirement.model';
import {WorkServiceRequirementPopupService} from './work-service-requirement-popup.service';
import {WorkServiceRequirementService} from './work-service-requirement.service';
import {ToasterService} from '../../shared';
import { WorkRequirement, WorkRequirementService } from '../work-requirement';
import { WorkOrderBooking, WorkOrderBookingService } from '../work-order-booking';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-work-service-requirement-dialog',
    templateUrl: './work-service-requirement-dialog.component.html'
})
export class WorkServiceRequirementDialogComponent implements OnInit {

    workServiceRequirement: WorkServiceRequirement;
    isSaving: boolean;

    workrequirements: WorkRequirement[];

    workorderbookings: WorkOrderBooking[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private workServiceRequirementService: WorkServiceRequirementService,
        private workRequirementService: WorkRequirementService,
        private workOrderBookingService: WorkOrderBookingService,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.workRequirementService.query()
            .subscribe((res: ResponseWrapper) => { this.workrequirements = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.workOrderBookingService.query()
            .subscribe((res: ResponseWrapper) => { this.workorderbookings = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.workServiceRequirement.idWe !== undefined) {
            this.subscribeToSaveResponse(
                this.workServiceRequirementService.update(this.workServiceRequirement));
        } else {
            this.subscribeToSaveResponse(
                this.workServiceRequirementService.create(this.workServiceRequirement));
        }
    }

    private subscribeToSaveResponse(result: Observable<WorkServiceRequirement>) {
        result.subscribe((res: WorkServiceRequirement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: WorkServiceRequirement) {
        this.eventManager.broadcast({ name: 'workServiceRequirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'workServiceRequirement saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'workServiceRequirement Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackWorkRequirementById(index: number, item: WorkRequirement) {
        return item.idRequirement;
    }

    trackWorkOrderBookingById(index: number, item: WorkOrderBooking) {
        return item.idBooking;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-work-service-requirement-popup',
    template: ''
})
export class WorkServiceRequirementPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private workServiceRequirementPopupService: WorkServiceRequirementPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.workServiceRequirementPopupService
                    .open(WorkServiceRequirementDialogComponent as Component, params['id']);
            } else {
                this.workServiceRequirementPopupService
                    .open(WorkServiceRequirementDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
