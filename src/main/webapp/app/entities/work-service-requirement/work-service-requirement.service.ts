import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { WorkServiceRequirement } from './work-service-requirement.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class WorkServiceRequirementService {
    private itemValues: WorkServiceRequirement[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = 'api/work-service-requirements';
    private resourceSearchUrl = 'api/_search/work-service-requirements';

    constructor(private http: Http) { }

    create(workServiceRequirement: WorkServiceRequirement): Observable<WorkServiceRequirement> {
        const copy = this.convert(workServiceRequirement);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(workServiceRequirement: WorkServiceRequirement): Observable<WorkServiceRequirement> {
        const copy = this.convert(workServiceRequirement);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<WorkServiceRequirement> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, workServiceRequirement: WorkServiceRequirement): Observable<WorkServiceRequirement> {
        const copy = this.convert(workServiceRequirement);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, workServiceRequirements: WorkServiceRequirement[]): Observable<WorkServiceRequirement[]> {
        const copy = this.convertList(workServiceRequirements);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(workServiceRequirement: WorkServiceRequirement): WorkServiceRequirement {
        if (workServiceRequirement === null || workServiceRequirement === {}) {
            return {};
        }
        // const copy: WorkServiceRequirement = Object.assign({}, workServiceRequirement);
        const copy: WorkServiceRequirement = JSON.parse(JSON.stringify(workServiceRequirement));
        return copy;
    }

    private convertList(workServiceRequirements: WorkServiceRequirement[]): WorkServiceRequirement[] {
        const copy: WorkServiceRequirement[] = workServiceRequirements;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: WorkServiceRequirement[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
