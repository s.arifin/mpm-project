import { BaseEntity } from './../../shared';

export class WorkServiceRequirement implements BaseEntity {
    constructor(
        public id?: any,
        public idWe?: any,
        public requirements?: any,
        public bookingId?: any,
    ) {
    }
}
