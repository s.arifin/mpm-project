import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Subscription} from 'rxjs/Rx';
import {Observable} from 'rxjs/Rx';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {WorkRequirement} from './work-requirement.model';
import {WorkRequirementService} from './work-requirement.service';
import {ToasterService} from '../../shared';
import { WorkServiceRequirement, WorkServiceRequirementService } from '../work-service-requirement';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-work-requirement-edit',
    templateUrl: './work-requirement-edit.component.html'
})
export class WorkRequirementEditComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    workRequirement: WorkRequirement;
    isSaving: boolean;

    workservicerequirements: WorkServiceRequirement[];

    constructor(
        private alertService: JhiAlertService,
        private workRequirementService: WorkRequirementService,
        private workServiceRequirementService: WorkServiceRequirementService,
        private route: ActivatedRoute,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
        this.workRequirement = new WorkRequirement();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.workServiceRequirementService.query()
            .subscribe((res: ResponseWrapper) => { this.workservicerequirements = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.workRequirementService.find(id).subscribe((workRequirement) => {
            this.workRequirement = workRequirement;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.workRequirement.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.workRequirementService.update(this.workRequirement));
        } else {
            this.subscribeToSaveResponse(
                this.workRequirementService.create(this.workRequirement));
        }
    }

    private subscribeToSaveResponse(result: Observable<WorkRequirement>) {
        result.subscribe((res: WorkRequirement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: WorkRequirement) {
        this.eventManager.broadcast({ name: 'workRequirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'workRequirement saved !');
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'workRequirement Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackWorkServiceRequirementById(index: number, item: WorkServiceRequirement) {
        return item.idWe;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
