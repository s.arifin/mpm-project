export * from './work-requirement.model';
export * from './work-requirement.service';
export * from './work-requirement.component';
export * from './work-requirement.route';
