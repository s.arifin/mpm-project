import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    WorkRequirementService,
    WorkRequirementComponent,
    workRequirementRoute,
    workRequirementPopupRoute,
    WorkRequirementResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...workRequirementRoute,
    ...workRequirementPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        WorkRequirementComponent,
    ],
    entryComponents: [
        WorkRequirementComponent,
    ],
    providers: [
        WorkRequirementService,
        WorkRequirementResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmWorkRequirementModule {}
