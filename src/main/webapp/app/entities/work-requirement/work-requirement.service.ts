import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { WorkRequirement } from './work-requirement.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class WorkRequirementService {

    private resourceUrl = 'api/work-requirements';
    private resourceSearchUrl = 'api/_search/work-requirements';

    constructor(private http: Http) { }

    create(workRequirement: WorkRequirement): Observable<WorkRequirement> {
        const copy = this.convert(workRequirement);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(workRequirement: WorkRequirement): Observable<WorkRequirement> {
        const copy = this.convert(workRequirement);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<WorkRequirement> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(workRequirement: WorkRequirement): WorkRequirement {
        const copy: WorkRequirement = Object.assign({}, workRequirement);
        return copy;
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
