import { BaseEntity, Requirement } from './../shared-component';

export class WorkRequirement extends Requirement {
    constructor(
        public services?: BaseEntity[],
    ) {
        super();
    }
}
