import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

// import { ReportService } from './report.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { ReportUtilService } from '../../shared/report/report-util.service';
import * as moment from 'moment';
import { Facility } from '../shared-component/entity/facility.model';
import { FacilityService } from '../facility/facility.service';
// import { Organization } from '../organization/organization.model';
// import { OrganizationService } from '../organization/organization.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
import { Report } from './report.model';
import { VehicleDocumentRequirementCService } from '../vehicle-document-requirement-c/index';

@Component({
    selector: 'jhi-report-posting-jurnal',
    templateUrl: './report-posting-jurnal.component.html'
})
export class ReportPostingJurnalComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    jurnalName: string;
    reportType: string;
    nd: string;
    types: any;

    // organizations: Organization[];
    // organization: Organization;
    // organisasi: any;
    internals: Internal[];
    internal: string;
    journals: Report[];
    journal: string;
    journal1: string;

    constructor(
        protected reportUtilService: ReportUtilService,
        // protected organizationService: OrganizationService,
        protected internalService: InternalService,
        private vehicleDocumentRequirementCService: VehicleDocumentRequirementCService,
    ) {

        this.types = [
            {value: '1', label: 'Unit'},
            {value: '2', label: 'Part'},
            {value: '3', label: 'Service'},
        ];

        this.journal = '';
        this.reportType = '';
        this.internal = '';
        this.nd = '';

    }

    ngOnInit() {
        // this.organizationService.query()
        //     .subscribe((res: ResponseWrapper) => { this.organizations = res.json});
        //     console.log('nc :', this.organization);

        this.internalService.query({size: 999999})
        .subscribe((res: ResponseWrapper) => {
            this.internals = res.json;
            console.log('internal :', this.internals)
        });

        this.vehicleDocumentRequirementCService.reportType()
        .subscribe((res: ResponseWrapper) => {
            this.journals = res.json;
            console.log('journal :', this.journals)
        });
    }

    // trackorganizationById(index: number, item: Organization) {
    //     return item.idParty;
    // }

    trackinternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackinternalByType(index: number, item: Report) {
        return item.description;
    }

    print(from: Date, thru: Date, nd: Internal) {
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const waktu = '23:59:59.000';
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        // const sampai = new Date (thru);
        this.reportUtilService.viewFile('/api/report/salesbookunit/pdf', { date_from: dari, date_thru: sampai, namacabang: nd.idInternal });
        this.reportUtilService.downloadFile('/api/report/salesbookunit/xlsx', { date_from: dari, date_thru: sampai, namacabang: nd.idInternal });
        console.log('from :', dari);
        console.log('thrue :', sampai);
        console.log('nd :', nd);
    }

    printC(from: Date, thru: Date, type: string, journal: string, nd: string) {

        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const waktu = '23:59:59.000';
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        // const sampai = new Date (thru);

        const filter_data = 'pdLogStartDate:' + dari + '|pdLogEndDate:' + sampai + '|pcJournalType:' + type + '|pcJournalName:' + journal + '|pcIdInternal:' + nd;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/Posting_Jurnal1/pdf', { filterData: filter_data });
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/Posting_Jurnal1/xlsx', { filterData: filter_data });
    }

}
