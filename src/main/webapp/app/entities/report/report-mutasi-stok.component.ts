import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ReportService } from './report.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Organization } from '../organization/organization.model';
import { OrganizationService } from '../organization/organization.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';

@Component({
    selector: 'jhi-report-mutasi-stok',
    templateUrl: './report-mutasi-stok.component.html'
})
export class ReportMutasiStokComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    organizations: Organization[];
    organization: Organization;
    organisasi: any;
    internals: Internal[];
    internal: Internal;
    selectedInternal: any;

    constructor(
        protected reportUtilService: ReportUtilService,
        // protected organizationService: OrganizationService,
        protected internalService: InternalService,
        protected principal: Principal,
    ) {

    }

    ngOnInit() {
        // this.internalService.find(this.principal.getIdInternal)
        // .subscribe((res) => {
        //     this.selectedInternal = res.idInternal;
        //     console.log('internal :', this.internals)
        // });

        this.internalService.query({size: 999999})
        .subscribe((res: ResponseWrapper) => {
            this.internals = res.json;
            console.log('internal :', this.internals)
        });
    }

    trackinternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    print(period: Date, pcIdInternal: Internal) {
        const dari = period.getFullYear() + '-' + (period.getMonth() + 1) + '-' + period.getDate();
        const waktu = '23:59:59.000';
        // const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        // const sampai = new Date (thru);
        const filter_data = 'pdInputDate:' + dari + '|pcIdInternal:' + pcIdInternal.idInternal;
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/reportMutasiStock/pdf', { filterData: filter_data });
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/reportMutasiStock/xlsx', { filterData: filter_data });
        // this.reportUtilService.downloadFile('/api/report/bpkb_on_hand/xlsx', { date_from: dari, date_thru: sampai, namacabang: nd.idInternal });
        console.log('from :', dari);
    }
}
