
import { BaseEntity } from './../../shared';

export class Report implements BaseEntity {
    constructor(
        public id?: any,
        public idType?: any,
        public description?: string,
    ) {
    }
}
