import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ReportRefundComponent } from './report-refund.component';
import { ReportDriverActivityComponent} from './report-driver-activity.component';
import { ReportStokBPKBComponent} from './report-stok-bpkb.component';
import { ReportStokSTNKComponent} from './report-stok-stnk.component';
import { ReportMutasiStokComponent } from './report-mutasi-stok.component';
import { ReportPengeluaranUnitComponent } from './report-pengeluaran-unit.component';
import { ReportTrialBalanceComponent } from './report-trial-balance.component';
import { ReportPenerimaanComponent } from './report-penerimaan.component';
import { ReportPembelianComponent} from './report-pembelian.component';
import { ReportQueryStokComponent } from './report-query-stok.component';
import { ReportKegiatanHarianComponent } from './report-kegiatan-harian.component';
import { ReportFollowUpDetailComponent } from './report-follow-up-detail.component';
import { ReportSuspectToRatioComponent } from './report-suspect-to-ratio.component';
import { ReportProfitPerSalesComponent} from './report-profit-per-sales.component';
import { ReportSalesBookingUnitComponent } from './report-sales-booking-unit.component';
import { ReportProspectToBPKBComponent } from './report-prospect-to-bpkb.component';
import { ReportBukuTamuComponent} from './report-buku-tamu.component';
import { ReportIndentUnitComponent } from './report-indent-unit.component';
import { ReportPostingJurnalComponent } from './report-posting-jurnal.component';

export const reportRoute: Routes = [
    {
        path: 'report-indent-unit',
        component: ReportIndentUnitComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-refund',
        component: ReportRefundComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-driver-activity',
        component: ReportDriverActivityComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-stok-bpkb',
        component: ReportStokBPKBComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-stok-stnk',
        component: ReportStokSTNKComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-mutasi-stok',
        component: ReportMutasiStokComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-pengeluaran-unit',
        component: ReportPengeluaranUnitComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-trial-balance',
        component: ReportTrialBalanceComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-penerimaan',
        component: ReportPenerimaanComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-pembelian',
        component: ReportPembelianComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-query-stok',
        component: ReportQueryStokComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-kegiatan-harian',
        component: ReportKegiatanHarianComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-follow-up-detail',
        component: ReportFollowUpDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-suspect-to-ratio',
        component: ReportSuspectToRatioComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-buku-tamu',
        component: ReportBukuTamuComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-profit-per-sales',
        component: ReportProfitPerSalesComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-sales-booking-unit',
        component: ReportSalesBookingUnitComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },

    {
        path: 'report-posting-jurnal',
        component: ReportPostingJurnalComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'report-prospect-to-bpkb',
        component: ReportProspectToBPKBComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.remPart.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
