import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    ReportService,
    ReportIndentUnitComponent,
    reportRoute,
    ReportRefundComponent,
    ReportDriverActivityComponent,
    ReportStokBPKBComponent,
    ReportStokSTNKComponent,
    ReportMutasiStokComponent,
    ReportPengeluaranUnitComponent,
    ReportTrialBalanceComponent,
    ReportPenerimaanComponent,
    ReportPembelianComponent,
    ReportQueryStokComponent,
    ReportKegiatanHarianComponent,
    ReportFollowUpDetailComponent,
    ReportSuspectToRatioComponent,
    ReportProfitPerSalesComponent,
    ReportSalesBookingUnitComponent,
    ReportProspectToBPKBComponent,
    ReportBukuTamuComponent,
    ReportPostingJurnalComponent
} from './';

import { CommonModule } from '@angular/common';

import {CurrencyMaskModule} from 'ng2-currency-mask';

import {
        CheckboxModule,
        InputTextModule,
        CalendarModule,
        DropdownModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        DialogModule,
        ConfirmDialogModule,
        ConfirmationService,
        GrowlModule,
        DataGridModule,
        SharedModule,
        AccordionModule,
        TabViewModule,
        FieldsetModule,
        ScheduleModule,
        PanelModule,
        ListboxModule,
        SelectItem,
        MenuItem,
        Header,
        Footer} from 'primeng/primeng';

const ENTITY_STATES = [
    ...reportRoute
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        DataGridModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        CurrencyMaskModule
    ],
    exports: [
    ],
    declarations: [
        ReportIndentUnitComponent,
        ReportRefundComponent,
        ReportDriverActivityComponent,
        ReportStokBPKBComponent,
        ReportStokSTNKComponent,
        ReportMutasiStokComponent,
        ReportPengeluaranUnitComponent,
        ReportTrialBalanceComponent,
        ReportPenerimaanComponent,
        ReportPembelianComponent,
        ReportQueryStokComponent,
        ReportKegiatanHarianComponent,
        ReportFollowUpDetailComponent,
        ReportSuspectToRatioComponent,
        ReportProfitPerSalesComponent,
        ReportSalesBookingUnitComponent,
        ReportProspectToBPKBComponent,
        ReportBukuTamuComponent,
        ReportPostingJurnalComponent
    ],
    entryComponents: [
        ReportIndentUnitComponent,
        ReportRefundComponent,
        ReportDriverActivityComponent,
        ReportStokBPKBComponent,
        ReportStokSTNKComponent,
        ReportMutasiStokComponent,
        ReportPengeluaranUnitComponent,
        ReportTrialBalanceComponent,
        ReportPenerimaanComponent,
        ReportPembelianComponent,
        ReportQueryStokComponent,
        ReportKegiatanHarianComponent,
        ReportFollowUpDetailComponent,
        ReportSuspectToRatioComponent,
        ReportProfitPerSalesComponent,
        ReportSalesBookingUnitComponent,
        ReportProspectToBPKBComponent,
        ReportBukuTamuComponent,
        ReportPostingJurnalComponent
    ],
    providers: [
        ReportService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmReportModule {}
