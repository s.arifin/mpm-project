import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ReportService } from './report.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { Organization } from '../organization/organization.model';
import { OrganizationService } from '../organization/organization.service';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';

@Component({
    selector: 'jhi-report-penerimaan',
    templateUrl: './report-penerimaan.component.html'
})
export class ReportPenerimaanComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    organizations: Organization[];
    organization: Organization;
    organisasi: any;
    internals: Internal[];
    internal: Internal;
    jenis: number;

    constructor(
        protected reportUtilService: ReportUtilService,
        protected internalService: InternalService,
    ) {

    }

    ngOnInit() {
        this.internalService.query({size: 999999})
        .subscribe((res: ResponseWrapper) => {
            this.internals = res.json;
            console.log('internal :', this.internals)
        });
    }

    trackinternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    print(from: Date, thru: Date, nd: Internal, jenis: number) {
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const waktu = '23:59:59.000';
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        // const sampai = new Date (thru);
        const filter_data = 'date_from:' + dari + '|date_thru:' + sampai + '|namacabang:' + nd.idInternal + '|type:' + jenis;

        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/PenerimaanUnit_Purchase/pdf', { filterData: filter_data });
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/PenerimaanUnit_Purchase/xlsx', { filterData: filter_data});
        console.log('from :', dari);
        console.log('thrue :', sampai);
        console.log('nd :', nd);

    }

    // print() {
    //     this.reportUtilService.downloadFile('/api/report/driver_activity/xlsx', {coba: 1234});
    // }
}
