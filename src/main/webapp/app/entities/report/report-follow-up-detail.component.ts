import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ReportService } from './report.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { now } from 'moment';
// import { OrganizationService } from '../organization/organization.service';
// import { Organization } from '../organization/organization.model';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';

@Component({
    selector: 'jhi-follow-up-detail',
    templateUrl: './report-follow-up-detail.component.html'
})
export class ReportFollowUpDetailComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    internals: Internal[];
    internal: Internal;

    constructor(
        protected reportUtilService: ReportUtilService,
        // // protected organizationService: OrganizationService,
        protected internalService: InternalService,
    ) {

    }

    ngOnInit() {
        this.internalService.query({size: 999999})
        .subscribe((res: ResponseWrapper) => {
            this.internals = res.json;
            console.log('internal :', this.internals)
        });
    }

    trackinternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    print(from: Date, thru: Date, nd: Internal) {
        const waktu = '23:59:59.000';
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;

        const filter_data = 'date_from:' + dari + '|date_thru:' + sampai + '|namacabang:' + nd.idInternal;
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/follow_up_detail/xlsx', { filterData: filter_data });
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/follow_up_detail/pdf', { filterData: filter_data });
        console.log('from :', dari);
        console.log('from :', sampai);
        console.log('nd :', nd);
    }
}
