import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ReportService } from './report.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';

@Component({
    selector: 'jhi-report-prospect-to-bpkb',
    templateUrl: './report-prospect-to-bpkb.component.html'
})
export class ReportProspectToBPKBComponent implements OnInit {
    tgl1: Date;
    tgl2: Date;
    organisasi: any;
    internals: Internal[];
    internal: Internal;
    mot_finance: boolean;
    mot_prospect: boolean;

    constructor(
        protected reportUtilService: ReportUtilService,
        // protected organizationService: OrganizationService,
        protected internalService: InternalService,
        private toasterService: ToasterService,
    ) {
        this.mot_finance = false;
        this.mot_finance = false;
    }

    ngOnInit() {
        this.internalService.query({size: 999999})
        .subscribe((res: ResponseWrapper) => {
            this.internals = res.json;
            console.log('internal :', this.internals)
        });
    }

    trackinternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    printfin(from: Date, thru: Date, nd: Internal) {
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const waktu = '23:59:59.000';
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        // const sampai = new Date (thru);
            this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
            // this.reportUtilService.viewFile('/api/report/MOT_SP_Finance/pdf', {dtcreate1: dari, dtcreate2: sampai, internalid: nd.idInternal});
            this.reportUtilService.downloadFile('/api/report/MOT_SP_Finance/xlsx', { dtcreate1: dari, dtcreate2: sampai, internalid: nd.idInternal });

        console.log('from :', dari);
        console.log('thrue :', sampai);
        console.log('nd :', nd.idInternal);
    }

    printfinC(from: Date, thru: Date, nd: Internal) {
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const waktu = '23:59:59.000';
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        // const sampai = new Date (thru);

        const filter_data = 'dtcreate1:' + dari + '|dtcreate2:' + sampai + '|internalid:' + nd.idInternal;

        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/MOT_SP_Finance/xlsx', { filterData: filter_data });
    }

    printpros(from: Date, thru: Date, nd: Internal) {
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const waktu = '23:59:59.000';
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        // const sampai = new Date (thru);
            this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
            console.log('cosole2:');
            // this.reportUtilService.viewFile('/api/report/MOT_SP_Prospect/pdf', {dtcreate1: dari, dtcreate2: sampai, internalid: nd.idInternal});
            this.reportUtilService.downloadFile('/api/report/MOT_SP_Prospect/xlsx', { dtcreate1: dari, dtcreate2: sampai, internalid: nd.idInternal });

        console.log('from :', dari);
        console.log('thrue :', sampai);
        console.log('nd :', nd.idInternal);
    }

    printprosC(from: Date, thru: Date, nd: Internal) {
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const waktu = '23:59:59.000';
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        // const sampai = new Date (thru);

        const filter_data = 'dtcreate1:' + dari + '|dtcreate2:' + sampai + '|internalid:' + nd.idInternal;

        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/MOT_SP_Prospect/xlsx', { filterData: filter_data });
    }

    printunit(from: Date, thru: Date, nd: Internal) {
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const waktu = '23:59:59.000';
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        // const sampai = new Date (thru);
            this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
            console.log('cosole2:');
            // this.reportUtilService.viewFile('/api/report/MOT_SP_Prospect/pdf', {dtcreate1: dari, dtcreate2: sampai, internalid: nd.idInternal});
            this.reportUtilService.downloadFile('/api/report/motunit/xlsx', { dtcreate1: dari, dtcreate2: sampai, internalid: nd.idInternal });
            this.reportUtilService.viewFile('/api/report/motunit/pdf', { dtcreate1: dari, dtcreate2: sampai, internalid: nd.idInternal });
        console.log('from :', dari);
        console.log('thrue :', sampai);
        console.log('nd :', nd.idInternal);
    }

    printunitC(from: Date, thru: Date, nd: Internal) {
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const waktu = '23:59:59.000';
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        // const sampai = new Date (thru);

        const filter_data = 'dtcreate1:' + dari + '|dtcreate2:' + sampai + '|internalid:' + nd.idInternal;

        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/motunit/xlsx', { filterData: filter_data });
        this.reportUtilService.viewFile(process.env.API_C_URL + '/api/report/motunit/pdf', { filterData: filter_data });
    }

    printpenjualan(from: Date, thru: Date, nd: Internal) {
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const waktu = '23:59:59.000';
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        // const sampai = new Date (thru);
            this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
            console.log('cosole2:');
            // this.reportUtilService.viewFile('/api/report/MOT_SP_Prospect/pdf', {dtcreate1: dari, dtcreate2: sampai, internalid: nd.idInternal});
            this.reportUtilService.downloadFile('/api/report/MOT_SP_Penjualan1/xlsx', { dtcreate1: dari, dtcreate2: sampai, internalid: nd.idInternal });

        console.log('from :', dari);
        console.log('thrue :', sampai);
        console.log('nd :', nd.idInternal);
    }

    printpenjualanC(from: Date, thru: Date, nd: Internal) {
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const waktu = '23:59:59.000';
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        // const sampai = new Date (thru);

        const filter_data = 'dtcreate1:' + dari + '|dtcreate2:' + sampai + '|internalid:' + nd.idInternal;

        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/MOT_SP_Penjualan1/xlsx', { filterData: filter_data });
    }

    printpenjualan2(from: Date, thru: Date, nd: Internal) {
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const waktu = '23:59:59.000';
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        // const sampai = new Date (thru);
            this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
            console.log('cosole2:');
            // this.reportUtilService.viewFile('/api/report/MOT_SP_Prospect/pdf', {dtcreate1: dari, dtcreate2: sampai, internalid: nd.idInternal});
            this.reportUtilService.downloadFile('/api/report/MOT_SP_Penjualan2/xlsx', { dtcreate1: dari, dtcreate2: sampai, internalid: nd.idInternal });

        console.log('from :', dari);
        console.log('thrue :', sampai);
        console.log('nd :', nd.idInternal);
    }

    printpenjualan2C(from: Date, thru: Date, nd: Internal) {
        const dari = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        const waktu = '23:59:59.000';
        const sampai = thru.getFullYear() + '-' + (thru.getMonth() + 1) + '-' + thru.getDate() + ' ' + waktu;
        // const sampai = new Date (thru);

        const filter_data = 'dtcreate1:' + dari + '|dtcreate2:' + sampai + '|internalid:' + nd.idInternal;

        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.downloadFile(process.env.API_C_URL + '/api/report/MOT_SP_Penjualan2/xlsx', { filterData: filter_data });
    }
}
