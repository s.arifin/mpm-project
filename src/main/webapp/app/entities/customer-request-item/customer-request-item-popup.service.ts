import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CustomerRequestItem } from './customer-request-item.model';
import { CustomerRequestItemService } from './customer-request-item.service';

@Injectable()
export class CustomerRequestItemPopupService {
    protected ngbModalRef: NgbModalRef;
    idRequest: any;
    idProduct: any;
    idFeature: any;
    idCustomer: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected customerRequestItemService: CustomerRequestItemService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.customerRequestItemService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.customerRequestItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new CustomerRequestItem();
                    data.requestId = this.idRequest;
                    data.idProduct = this.idProduct;
                    data.idFeature = this.idFeature;
                    data.customerId = this.idCustomer;
                    this.ngbModalRef = this.customerRequestItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    customerRequestItemModalRef(component: Component, customerRequestItem: CustomerRequestItem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.customerRequestItem = customerRequestItem;
        modalRef.componentInstance.idRequest = this.idRequest;
        modalRef.componentInstance.idProduct = this.idProduct;
        modalRef.componentInstance.idFeature = this.idFeature;
        modalRef.componentInstance.idCustomer = this.idCustomer;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.customerRequestItemLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    customerRequestItemLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idRequest = this.idRequest;
        modalRef.componentInstance.idProduct = this.idProduct;
        modalRef.componentInstance.idFeature = this.idFeature;
        modalRef.componentInstance.idCustomer = this.idCustomer;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
