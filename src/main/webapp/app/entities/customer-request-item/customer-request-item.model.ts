import { BaseEntity } from './../../shared';

export class CustomerRequestItem implements BaseEntity {
    constructor(
        public id?: any,
        public idRequestItem?: any,
        public description?: string,
        public qty?: number,
        public qtyDelivered?: number,
        public unitPrice?: number,
        public bbn?: number,
        public idFrame?: string,
        public idMachine?: string,
        public sequence?: number,
        public requestId?: any,
        public idProduct?: any,
        public idFeature?: any,
        public customerId?: any,
        public idColor?: any,
        public currentStatus?: number,
    ) {
    }
}
