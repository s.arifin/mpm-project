import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { CustomerRequestItem } from './customer-request-item.model';
import { CustomerRequestItemService } from './customer-request-item.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import { PersonalCustomerService } from '../personal-customer';
import { OrganizationCustomerService } from '../organization-customer';

@Component({
    selector: 'jhi-customer-request-item-as-list',
    templateUrl: './customer-request-item-as-list.component.html'
})
export class CustomerRequestItemAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idRequest: any;
    @Input() idProduct: any;
    @Input() idFeature: any;
    @Input() idCustomer: any;

    currentAccount: any;
    customerRequestItems: CustomerRequestItem[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    newPersonal: Subscription;
    newOrganization: Subscription;

    constructor(
        protected customerRequestItemService: CustomerRequestItemService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService,
        protected personalCustomerService: PersonalCustomerService,
        protected organizationCustomerService: OrganizationCustomerService
    ) {
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'customer.idCustomer';
        this.reverse = 'asc';
        this.first = 0;
    }

    loadAll() {
        this.customerRequestItemService.queryFilterBy({
            idRequest: this.idRequest,
            idProduct: this.idProduct,
            idFeature: this.idFeature,
            idCustomer: this.idCustomer,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/customer-request-item', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCustomerRequestItems();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idRequest']) {
            this.loadAll();
        }
        if (changes['idProduct']) {
            this.loadAll();
        }
        if (changes['idFeature']) {
            this.loadAll();
        }
        if (changes['idCustomer']) {
            this.loadAll();
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
        this.newOrganization.unsubscribe();
        this.newPersonal.unsubscribe();
    }

    trackId(index: number, item: CustomerRequestItem) {
        return item.idRequestItem;
    }

    addNewItem(customer) {
        const n: CustomerRequestItem = new CustomerRequestItem();
        n.customerId = customer.idCustomer;
        n.requestId = this.idRequest;
        this.customerRequestItemService.create(n).subscribe((r) => {
            this.loadAll();
        });
    }

    registerChangeInCustomerRequestItems() {
        this.eventSubscriber = this.eventManager.subscribe('customerRequestItemListModification', (response) => this.loadAll());
        this.newOrganization = this.organizationCustomerService.newCustomer.subscribe((r) => {
            this.addNewItem(r);
        });
        this.newPersonal = this.personalCustomerService.newCustomer.subscribe((r) => {
            this.addNewItem(r);
        });
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequestItem') {
            result.push('idRequestItem');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.customerRequestItems = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.customerRequestItemService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.subscribeToSaveResponse(
                this.customerRequestItemService.update(event.data));
        } else {
            this.subscribeToSaveResponse(
                this.customerRequestItemService.create(event.data));
        }
    }

    protected subscribeToSaveResponse(result: Observable<CustomerRequestItem>) {
        result.subscribe((res: CustomerRequestItem) => this.onSaveSuccess(res));
    }

    protected onSaveSuccess(result: CustomerRequestItem) {
        this.eventManager.broadcast({ name: 'customerRequestItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'customerRequestItem saved !');
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.customerRequestItemService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'customerRequestItemListModification',
                        content: 'Deleted an customerRequestItem'
                    });
                });
            }
        });
    }

    public sendToCustomer(item: CustomerRequestItem) {
        this.confirmationService.confirm({
            message: 'Anda akan mengirim motor ke Konsumen ?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.customerRequestItemService.executeProcess(item, {command: 'shipToCustomer'}).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'customerRequestItemListModification',
                        content: 'Update an customerRequestItem'
                    });
                    this.eventManager.broadcast({
                        name: 'vehicleCustomerRequestListModification',
                        content: 'Update an customerRequestItem'
                    });
                });
            }
        });
    }

}
