import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { CustomerRequestItem } from './customer-request-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CustomerRequestItemService {
   protected itemValues: CustomerRequestItem[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/customer-request-items';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/customer-request-items';

   constructor(protected http: Http) { }

   create(customerRequestItem: CustomerRequestItem): Observable<CustomerRequestItem> {
       const copy = this.convert(customerRequestItem);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(customerRequestItem: CustomerRequestItem): Observable<CustomerRequestItem> {
       const copy = this.convert(customerRequestItem);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<CustomerRequestItem> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: CustomerRequestItem, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: CustomerRequestItem[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to CustomerRequestItem.
    */
   protected convertItemFromServer(json: any): CustomerRequestItem {
       const entity: CustomerRequestItem = Object.assign(new CustomerRequestItem(), json);
       return entity;
   }

   /**
    * Convert a CustomerRequestItem to a JSON which can be sent to the server.
    */
   protected convert(customerRequestItem: CustomerRequestItem): CustomerRequestItem {
       if (customerRequestItem === null || customerRequestItem === {}) {
           return {};
       }
       // const copy: CustomerRequestItem = Object.assign({}, customerRequestItem);
       const copy: CustomerRequestItem = JSON.parse(JSON.stringify(customerRequestItem));
       return copy;
   }

   protected convertList(customerRequestItems: CustomerRequestItem[]): CustomerRequestItem[] {
       const copy: CustomerRequestItem[] = customerRequestItems;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: CustomerRequestItem[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
