import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CustomerRequestItem } from './customer-request-item.model';
import { CustomerRequestItemPopupService } from './customer-request-item-popup.service';
import { CustomerRequestItemService } from './customer-request-item.service';
import { ToasterService } from '../../shared';
import { Request, RequestService } from '../request';
import { Product, ProductService } from '../product';
import { Feature, FeatureService } from '../feature';
import { Customer, CustomerService } from '../customer';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-customer-request-item-dialog',
    templateUrl: './customer-request-item-dialog.component.html'
})
export class CustomerRequestItemDialogComponent implements OnInit {

    customerRequestItem: CustomerRequestItem;
    isSaving: boolean;
    idRequest: any;
    idProduct: any;
    idFeature: any;
    idCustomer: any;

    requests: Request[];

    products: Product[];

    features: Feature[];

    customers: Customer[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected customerRequestItemService: CustomerRequestItemService,
        protected requestService: RequestService,
        protected productService: ProductService,
        protected featureService: FeatureService,
        protected customerService: CustomerService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.requestService.query()
            .subscribe((res: ResponseWrapper) => { this.requests = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.featureService.query()
            .subscribe((res: ResponseWrapper) => { this.features = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.customerRequestItem.idRequestItem !== undefined) {
            this.subscribeToSaveResponse(
                this.customerRequestItemService.update(this.customerRequestItem));
        } else {
            this.subscribeToSaveResponse(
                this.customerRequestItemService.create(this.customerRequestItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<CustomerRequestItem>) {
        result.subscribe((res: CustomerRequestItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: CustomerRequestItem) {
        this.eventManager.broadcast({ name: 'customerRequestItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'customerRequestItem saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'customerRequestItem Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRequestById(index: number, item: Request) {
        return item.idRequest;
    }

    trackProductById(index: number, item: Product) {
        return item.idProduct;
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }
}

@Component({
    selector: 'jhi-customer-request-item-popup',
    template: ''
})
export class CustomerRequestItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected customerRequestItemPopupService: CustomerRequestItemPopupService
    ) {}

    ngOnInit() {
        this.customerRequestItemPopupService.idRequest = undefined;
        this.customerRequestItemPopupService.idProduct = undefined;
        this.customerRequestItemPopupService.idFeature = undefined;
        this.customerRequestItemPopupService.idCustomer = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.customerRequestItemPopupService
                    .open(CustomerRequestItemDialogComponent as Component, params['id']);
            } else if ( params['parent'] && params['idCustomer']) {
                this.customerRequestItemPopupService.idRequest = params['parent'];
                this.customerRequestItemPopupService.idCustomer = params['idCustomer'];
                this.customerRequestItemPopupService
                    .open(CustomerRequestItemDialogComponent as Component);
            } else {
                this.customerRequestItemPopupService
                    .open(CustomerRequestItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
