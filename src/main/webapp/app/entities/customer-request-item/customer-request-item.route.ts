import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { CustomerRequestItemComponent } from './customer-request-item.component';
import { CustomerRequestItemPopupComponent } from './customer-request-item-dialog.component';

@Injectable()
export class CustomerRequestItemResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequestItem,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const customerRequestItemRoute: Routes = [
    {
        path: 'customer-request-item',
        component: CustomerRequestItemComponent,
        resolve: {
            'pagingParams': CustomerRequestItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.customerRequestItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const customerRequestItemPopupRoute: Routes = [
    {
        path: 'customer-request-item-popup-new-list/:parent/:idCustomer',
        component: CustomerRequestItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.customerRequestItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'customer-request-item-new',
        component: CustomerRequestItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.customerRequestItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'customer-request-item/:id/edit',
        component: CustomerRequestItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.customerRequestItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
