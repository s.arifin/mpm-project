import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PositionReportingStructureComponent } from './position-reporting-structure.component';
import { PositionReportingStructureLovPopupComponent } from './position-reporting-structure-as-lov.component';
import { PositionReportingStructurePopupComponent } from './position-reporting-structure-dialog.component';

@Injectable()
export class PositionReportingStructureResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idPositionStructure,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const positionReportingStructureRoute: Routes = [
    {
        path: 'position-reporting-structure',
        component: PositionReportingStructureComponent,
        resolve: {
            'pagingParams': PositionReportingStructureResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.positionReportingStructure.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const positionReportingStructurePopupRoute: Routes = [
    {
        path: 'position-reporting-structure-lov',
        component: PositionReportingStructureLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.positionReportingStructure.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'position-reporting-structure-new',
        component: PositionReportingStructurePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.positionReportingStructure.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'position-reporting-structure/:id/edit',
        component: PositionReportingStructurePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.positionReportingStructure.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
