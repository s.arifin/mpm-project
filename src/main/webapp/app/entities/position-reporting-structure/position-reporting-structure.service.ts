import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { JhiDateUtils } from 'ng-jhipster';

import { PositionReportingStructure } from './position-reporting-structure.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class PositionReportingStructureService {
    protected itemValues: PositionReportingStructure[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/position-reporting-structures';
    protected resourceSearchUrl = 'api/_search/position-reporting-structures';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(positionReportingStructure: PositionReportingStructure): Observable<PositionReportingStructure> {
        const copy = this.convert(positionReportingStructure);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(positionReportingStructure: PositionReportingStructure): Observable<PositionReportingStructure> {
        const copy = this.convert(positionReportingStructure);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<PositionReportingStructure> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(positionReportingStructure: PositionReportingStructure, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(positionReportingStructure);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, positionReportingStructure: PositionReportingStructure): Observable<PositionReportingStructure> {
        const copy = this.convert(positionReportingStructure);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, positionReportingStructures: PositionReportingStructure[]): Observable<PositionReportingStructure[]> {
        const copy = this.convertList(positionReportingStructures);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateFrom) {
            entity.dateFrom = new Date(entity.dateFrom);
        }
        if (entity.dateThru) {
            entity.dateThru = new Date(entity.dateThru);
        }
    }

    protected convert(positionReportingStructure: PositionReportingStructure): PositionReportingStructure {
        if (positionReportingStructure === null || positionReportingStructure === {}) {
            return {};
        }
        // const copy: PositionReportingStructure = Object.assign({}, positionReportingStructure);
        const copy: PositionReportingStructure = JSON.parse(JSON.stringify(positionReportingStructure));

        // copy.dateFrom = this.dateUtils.toDate(positionReportingStructure.dateFrom);

        // copy.dateThru = this.dateUtils.toDate(positionReportingStructure.dateThru);
        return copy;
    }

    protected convertList(positionReportingStructures: PositionReportingStructure[]): PositionReportingStructure[] {
        const copy: PositionReportingStructure[] = positionReportingStructures;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: PositionReportingStructure[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
