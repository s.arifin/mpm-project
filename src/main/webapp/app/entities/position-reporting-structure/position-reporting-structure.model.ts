import { BaseEntity } from './../../shared';

export class PositionReportingStructure implements BaseEntity {
    constructor(
        public id?: any,
        public idPositionStructure?: any,
        public dateFrom?: any,
        public dateThru?: any,
        public positionFromId?: any,
        public positionToId?: any,
    ) {
    }
}
