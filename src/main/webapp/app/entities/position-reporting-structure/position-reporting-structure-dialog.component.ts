import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {PositionReportingStructure} from './position-reporting-structure.model';
import {PositionReportingStructurePopupService} from './position-reporting-structure-popup.service';
import {PositionReportingStructureService} from './position-reporting-structure.service';
import {ToasterService} from '../../shared';
import { Position, PositionService } from '../position';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-position-reporting-structure-dialog',
    templateUrl: './position-reporting-structure-dialog.component.html'
})
export class PositionReportingStructureDialogComponent implements OnInit {

    positionReportingStructure: PositionReportingStructure;
    isSaving: boolean;

    positions: Position[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected positionReportingStructureService: PositionReportingStructureService,
        protected positionService: PositionService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.positionService.query()
            .subscribe((res: ResponseWrapper) => { this.positions = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.positionReportingStructure.idPositionStructure !== undefined) {
            this.subscribeToSaveResponse(
                this.positionReportingStructureService.update(this.positionReportingStructure));
        } else {
            this.subscribeToSaveResponse(
                this.positionReportingStructureService.create(this.positionReportingStructure));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PositionReportingStructure>) {
        result.subscribe((res: PositionReportingStructure) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: PositionReportingStructure) {
        this.eventManager.broadcast({ name: 'positionReportingStructureListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'positionReportingStructure saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'positionReportingStructure Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackPositionById(index: number, item: Position) {
        return item.idPosition;
    }
}

@Component({
    selector: 'jhi-position-reporting-structure-popup',
    template: ''
})
export class PositionReportingStructurePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected positionReportingStructurePopupService: PositionReportingStructurePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.positionReportingStructurePopupService
                    .open(PositionReportingStructureDialogComponent as Component, params['id']);
            } else {
                this.positionReportingStructurePopupService
                    .open(PositionReportingStructureDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
