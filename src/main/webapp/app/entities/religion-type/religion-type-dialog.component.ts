import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {ReligionType} from './religion-type.model';
import {ReligionTypePopupService} from './religion-type-popup.service';
import {ReligionTypeService} from './religion-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-religion-type-dialog',
    templateUrl: './religion-type-dialog.component.html'
})
export class ReligionTypeDialogComponent implements OnInit {

    religionType: ReligionType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected religionTypeService: ReligionTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.religionType.idReligionType !== undefined) {
            this.subscribeToSaveResponse(
                this.religionTypeService.update(this.religionType));
        } else {
            this.subscribeToSaveResponse(
                this.religionTypeService.create(this.religionType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ReligionType>) {
        result.subscribe((res: ReligionType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ReligionType) {
        this.eventManager.broadcast({ name: 'religionTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'religionType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'religionType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-religion-type-popup',
    template: ''
})
export class ReligionTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected religionTypePopupService: ReligionTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.religionTypePopupService
                    .open(ReligionTypeDialogComponent as Component, params['id']);
            } else {
                this.religionTypePopupService
                    .open(ReligionTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
