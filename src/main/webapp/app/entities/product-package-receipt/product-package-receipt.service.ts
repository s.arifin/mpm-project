import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ProductPackageReceipt } from './product-package-receipt.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ProductPackageReceiptService {
    protected itemValues: ProductPackageReceipt[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/product-package-receipts';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/product-package-receipts';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(productPackageReceipt: ProductPackageReceipt): Observable<ProductPackageReceipt> {
        const copy = this.convert(productPackageReceipt);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(productPackageReceipt: ProductPackageReceipt): Observable<ProductPackageReceipt> {
        const copy = this.convert(productPackageReceipt);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<ProductPackageReceipt> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(productPackageReceipt: ProductPackageReceipt, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(productPackageReceipt);
        console.log('id', id);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<ProductPackageReceipt> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<ProductPackageReceipt[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ProductPackageReceipt.
     */
    protected convertItemFromServer(json: any): ProductPackageReceipt {
        const entity: ProductPackageReceipt = Object.assign(new ProductPackageReceipt(), json);
        if (entity.dateCreated) {
            entity.dateCreated = new Date(entity.dateCreated);
        }
        return entity;
    }

    /**
     * Convert a ProductPackageReceipt to a JSON which can be sent to the server.
     */
    protected convert(productPackageReceipt: ProductPackageReceipt): ProductPackageReceipt {
        if (productPackageReceipt === null || productPackageReceipt === {}) {
            return {};
        }
        // const copy: ProductPackageReceipt = Object.assign({}, productPackageReceipt);
        const copy: ProductPackageReceipt = JSON.parse(JSON.stringify(productPackageReceipt));

        // copy.dateCreated = this.dateUtils.toDate(productPackageReceipt.dateCreated);
        return copy;
    }

    protected convertList(productPackageReceipts: ProductPackageReceipt[]): ProductPackageReceipt[] {
        const copy: ProductPackageReceipt[] = productPackageReceipts;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ProductPackageReceipt[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
