import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProductPackageReceipt } from './product-package-receipt.model';
import { ProductPackageReceiptService } from './product-package-receipt.service';
import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { ShipTo, ShipToService } from '../ship-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-product-package-receipt-edit',
    templateUrl: './product-package-receipt-edit.component.html'
})
export class ProductPackageReceiptEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    productPackageReceipt: ProductPackageReceipt;
    isSaving: boolean;
    idPackage: any;
    paramPage: number;
    routeId: number;
    internals: Internal[];
    shiptos: ShipTo[];

    idShipTo: any;
    idShipFrom: any;

    constructor(
        protected alertService: JhiAlertService,
        protected productPackageReceiptService: ProductPackageReceiptService,
        protected internalService: InternalService,
        protected shipToService: ShipToService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.productPackageReceipt = new ProductPackageReceipt();
        this.routeId = 0;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idPackage = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipToService.query()
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.productPackageReceiptService.find(this.idPackage).subscribe((productPackageReceipt) => {
            this.productPackageReceipt = productPackageReceipt;
            this.idShipFrom = this.productPackageReceipt.shipFromId;
            this.idShipTo   = this.productPackageReceipt.internalId;
            console.log('Check', this.idShipFrom, this.idShipTo);
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['product-package-receipt', { page: this.paramPage }]);
        }
    }

    process() {
        this.productPackageReceiptService.changeStatus(this.productPackageReceipt, 17).subscribe(() => {
            this.previousState();
        });
    }

    save() {
        this.isSaving = true;
        if (this.productPackageReceipt.idPackage !== undefined) {
            this.subscribeToSaveResponse(
                this.productPackageReceiptService.update(this.productPackageReceipt));
        } else {
            this.subscribeToSaveResponse(
                this.productPackageReceiptService.create(this.productPackageReceipt));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProductPackageReceipt>) {
        result.subscribe((res: ProductPackageReceipt) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ProductPackageReceipt) {
        this.eventManager.broadcast({ name: 'productPackageReceiptListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'productPackageReceipt saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'productPackageReceipt Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }
}
