import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProductPackageReceipt } from './product-package-receipt.model';
import { ProductPackageReceiptPopupService } from './product-package-receipt-popup.service';
import { ProductPackageReceiptService } from './product-package-receipt.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { ShipTo, ShipToService } from '../ship-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-product-package-receipt-dialog',
    templateUrl: './product-package-receipt-dialog.component.html'
})
export class ProductPackageReceiptDialogComponent implements OnInit {

    productPackageReceipt: ProductPackageReceipt;
    isSaving: boolean;
    idInternal: any;
    idShipFrom: any;

    internals: Internal[];

    shiptos: ShipTo[];

    constructor(
        public activeModal: NgbActiveModal,
        protected router: Router,
        protected jhiAlertService: JhiAlertService,
        protected productPackageReceiptService: ProductPackageReceiptService,
        protected internalService: InternalService,
        protected shipToService: ShipToService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipToService.query()
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.productPackageReceipt.idPackage !== undefined) {
            this.subscribeToSaveResponse(
                this.productPackageReceiptService.update(this.productPackageReceipt));
        } else {
            this.subscribeToSaveResponse(
                this.productPackageReceiptService.create(this.productPackageReceipt));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProductPackageReceipt>) {
        result.subscribe((res: ProductPackageReceipt) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ProductPackageReceipt) {
        this.eventManager.broadcast({ name: 'productPackageReceiptListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'productPackageReceipt saved !');
        this.isSaving = false;
        this.router.navigate(['/product-package-receipt/0/1/' + this.productPackageReceipt.idPackage + '/edit']);
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'productPackageReceipt Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }
}

@Component({
    selector: 'jhi-product-package-receipt-popup',
    template: ''
})
export class ProductPackageReceiptPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected productPackageReceiptPopupService: ProductPackageReceiptPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.productPackageReceiptPopupService
                    .open(ProductPackageReceiptDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.productPackageReceiptPopupService.parent = params['parent'];
                this.productPackageReceiptPopupService
                    .open(ProductPackageReceiptDialogComponent as Component);
            } else {
                this.productPackageReceiptPopupService
                    .open(ProductPackageReceiptDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
