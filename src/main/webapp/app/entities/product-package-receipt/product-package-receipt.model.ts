import { BaseEntity } from './../../shared';

export class ProductPackageReceipt implements BaseEntity {
    constructor(
        public id?: any,
        public idPackage?: any,
        public dateCreated?: any,
        public documentNumber?: string,
        public internalId?: any,
        public shipFromId?: any,
    ) {
    }
}
