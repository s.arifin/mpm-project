import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ProductPackageReceiptComponent } from './product-package-receipt.component';
import { ProductPackageReceiptEditComponent } from './product-package-receipt-edit.component';
import { ProductPackageReceiptPopupComponent } from './product-package-receipt-dialog.component';

@Injectable()
export class ProductPackageReceiptResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idPackage,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const productPackageReceiptRoute: Routes = [
    {
        path: 'product-package-receipt',
        component: ProductPackageReceiptComponent,
        resolve: {
            'pagingParams': ProductPackageReceiptResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productPackageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const productPackageReceiptPopupRoute: Routes = [
    {
        path: 'product-package-receipt-popup-new-list/:parent',
        component: ProductPackageReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productPackageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-package-receipt-popup-new',
        component: ProductPackageReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productPackageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-package-receipt-new',
        component: ProductPackageReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productPackageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-package-receipt/:id/edit',
        component: ProductPackageReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productPackageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-package-receipt/:route/:page/:id/edit',
        component: ProductPackageReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productPackageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-package-receipt/:id/popup-edit',
        component: ProductPackageReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productPackageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
