import { Observable } from 'rxjs/Rx';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ProspectPerson } from '../prospect-person/prospect-person.model';
import { ProspectKorsalService } from './prospect-korsal.service';

import { Salesman, SalesmanService } from '../salesman';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-prospect-tl-request-drop',
    templateUrl: './prospect-tl-request-drop.component.html'
})
export class ProspectTLRequestDropComponent implements OnInit, OnDestroy {

    currentAccount: any;
    prospectKorsals: ProspectPerson[];
    selected: ProspectPerson[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    selectedSales: any;
    isRequestDrop: Boolean = false;
    isReject: Boolean = false;
    isCompletedDrop: Boolean = false;
    isCompletedReject: Boolean = false;

    constructor(
        protected prospectKorsalService: ProspectKorsalService,
        protected salesmanService: SalesmanService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected loadingService: LoadingService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.selected = new Array <ProspectPerson>();
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.prospectKorsalService.search({
                page: this.page - 1,
                query: this.currentSearch,
                idstatustype: 54,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.prospectKorsalService.query({
            idstatustype: 54,
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/prospect-korsal'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/prospect-korsal', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/prospect-korsal', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInProspectKorsals();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ProspectPerson) {
        return item.idProspect;
    }

    registerChangeInProspectKorsals() {
        this.eventSubscriber = this.eventManager.subscribe('prospectKorsalListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idProspect') {
            result.push('idProspect');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.prospectKorsals = new Array<ProspectPerson>();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.prospectKorsals = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    protected subscribeToSaveResponse(result: Observable<ProspectPerson[]>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe(
                    (res: ProspectPerson[]) => {
                        resolve();
                    }
                );
            }
        );
    }

    drop() {
        this.isRequestDrop = true;
    }

    reject() {
        this.isReject = true;
    }

    confirmDrop() {
        this.isRequestDrop = false;
        this.loadingService.loadingStart();
        this.subscribeToSaveResponse(
            this.prospectKorsalService.executeListProcess(103, this.selectedSales, this.selected)
        ).then(
            () => {
                // this.eventManager.broadcast({
                //     name: 'prospectKorsalListModification',
                //     content: 'prospect has dropped'
                // });
                this.selected = new Array<ProspectPerson>();
                this.loadAll();
                this.loadingService.loadingStop();
                this.isCompletedDrop = true;
            }
        );
    }

    confirmReject() {
        this.isReject = false;
        this.loadingService.loadingStart();
        this.subscribeToSaveResponse(
            this.prospectKorsalService.executeListProcess(104, this.selectedSales, this.selected)
        ).then(
            () => {
                // this.eventManager.broadcast({
                //     name: 'prospectKorsalListModification',
                //     content: 'prospect has dropped'
                // });
                this.selected = new Array<ProspectPerson>();
                this.loadAll();
                this.loadingService.loadingStop();
                this.isCompletedReject = true;
            }
        );
    }
}
