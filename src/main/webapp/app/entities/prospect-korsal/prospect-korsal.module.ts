import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    ProspectKorsalService,
    ProspectKorsalPopupService,
    ProspectKorsalComponent,
    ProspectTLComponent,
    prospectKorsalRoute,
    prospectKorsalPopupRoute,
    ProspectKorsalResolvePagingParams,
    ProspectKorsalListSalesComponent,
    ProspectKorsalRequestDropComponent,
    ProspectTLRequestDropComponent,
    ProspectKorsalListTeamleadComponent,
    ProspectKorsalListTeamleadSalesComponent,
    ProspectKorsalTargetComponent,
    ProspectKorsalGridListComponent,
    ProspectKorsalGridListDetailsComponent,
    ProspectKorsalGridListTLComponent,
    ProspectKorsalGridListDetailsTLComponent,
    ProspectKorsalGridListSalesTLComponent,
    ProspectTlGridListDetailsComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule
        } from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...prospectKorsalRoute,
    ...prospectKorsalPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,

        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule
    ],
    exports: [
        ProspectKorsalComponent,
        ProspectTLComponent,
        ProspectKorsalListSalesComponent,
        ProspectKorsalRequestDropComponent,
        ProspectTLRequestDropComponent,
        ProspectKorsalListTeamleadComponent,
        ProspectKorsalListTeamleadSalesComponent,
        ProspectKorsalTargetComponent,
        ProspectKorsalGridListComponent,
        ProspectKorsalGridListDetailsComponent,
        ProspectKorsalGridListTLComponent,
        ProspectKorsalGridListDetailsTLComponent,
        ProspectKorsalGridListSalesTLComponent,
        ProspectTlGridListDetailsComponent,
    ],
    declarations: [
        ProspectKorsalComponent,
        ProspectTLComponent,
        ProspectKorsalListSalesComponent,
        ProspectKorsalRequestDropComponent,
        ProspectTLRequestDropComponent,
        ProspectKorsalListTeamleadComponent,
        ProspectKorsalListTeamleadSalesComponent,
        ProspectKorsalTargetComponent,
        ProspectKorsalGridListComponent,
        ProspectKorsalGridListDetailsComponent,
        ProspectKorsalGridListTLComponent,
        ProspectKorsalGridListDetailsTLComponent,
        ProspectKorsalGridListSalesTLComponent,
        ProspectTlGridListDetailsComponent,
    ],
    entryComponents: [
        ProspectKorsalComponent,
        ProspectTLComponent,
        ProspectKorsalListSalesComponent,
        ProspectKorsalRequestDropComponent,
        ProspectTLRequestDropComponent,
        ProspectKorsalListTeamleadComponent,
        ProspectKorsalListTeamleadSalesComponent,
        ProspectKorsalTargetComponent,
        ProspectKorsalGridListComponent,
        ProspectKorsalGridListDetailsComponent,
        ProspectKorsalGridListTLComponent,
        ProspectKorsalGridListDetailsTLComponent,
        ProspectKorsalGridListSalesTLComponent,
        ProspectTlGridListDetailsComponent,
    ],
    providers: [
        ProspectKorsalService,
        ProspectKorsalPopupService,
        ProspectKorsalResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmProspectKorsalModule {}
