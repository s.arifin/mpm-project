import { Observable, Subscription } from 'rxjs/Rx';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ProspectPerson } from '../prospect-person/prospect-person.model';
import { ProspectKorsalService } from './prospect-korsal.service';

import { Salesman, SalesmanService } from '../salesman';
import { WorkType, WorkTypeService } from '../work-type';
import { SaleType, SaleTypeService } from '../sale-type';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';
import { count } from 'rxjs/operator/count';

@Component({
    selector: 'jhi-prospect-korsal',
    templateUrl: './prospect-korsal.component.html'
})
export class ProspectKorsalComponent implements OnInit, OnDestroy {

    currentAccount: any;
    prospectKorsals: ProspectPerson[];
    selected: ProspectPerson[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    total: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    selectedWorkType: any;
    selectedSalesman: any;
    selectedSaleType: any;
    selectedDistrict: any;
    korsname: any;
    korsnames: any;
    namekors: Salesman[];
    salesmen: Salesman[];
    teamLeader: Salesman[];
    worktypes: WorkType[];
    saletypes: SaleType[];
    isCompleted: Boolean = false;
    isDrop: Boolean = false;
    isAssignTeamLeader: Boolean = false;
    isAssignSalesman: Boolean = false;
    isFilter: Boolean = false;
    isTeamLeader: Boolean = false;
    isSalesman: Boolean = false;
    display = false;
    displaySearch = false;
    selectedSales: any;
    selectedTeamLeader: any;
    nullValue = 'none';
    selectedTL: any;

    constructor(
        protected prospectKorsalService: ProspectKorsalService,
        protected salesmanService: SalesmanService,
        protected workTypeService: WorkTypeService,
        protected saleTypeService: SaleTypeService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected loadingService: LoadingService
    ) {
        this.selected = new Array<ProspectPerson>();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
    }

    loadAll() {
        if (this.isFilter === true) {
            this.prospectKorsalService.filter({
                page: this.page - 1,
                size: this.itemsPerPage,
                idinternal: this.principal.getIdInternal(),
                korsal: this.principal.getUserLogin().replace('.', '$'),
                salesman: this.selectedSalesman != null ? this.selectedSalesman : 'null',
                worktype: this.selectedWorkType != null ? this.selectedWorkType : 'null',
                district: this.selectedDistrict != null ? this.selectedDistrict : 'null',
                saletype: this.selectedSaleType != null ? this.selectedSaleType : 'null',
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => {
                        this.onSuccess(res.json, res.headers);
                    },
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.prospectKorsalService.getAll({
            page: this.page - 1,
            size: this.itemsPerPage,
            idinternal: this.principal.getIdInternal(),
            korsal: this.principal.getUserLogin().replace('.', '$'),
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );

    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/prospect-korsal'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();

        this.salesmanService.filter(
            {
                username: this.principal.getUserLogin().replace('.', '$')
            }
            ).subscribe(
            (res: ResponseWrapper) => {
                this.salesmen = res.json; if (this.teamLeader.length > 0) {
                    this.isSalesman = true;
                }
                console.log('cok = ', this.isSalesman);
            }
        )

        this.salesmanService.getTeamLeader(
            {
                idinternal: this.principal.getIdInternal(),
                username: this.principal.getUserLogin().replace('.', '$')
            }
            ).subscribe(
            (res: ResponseWrapper) => {
                this.teamLeader = res.json;
                if (this.teamLeader.length > 0) {
                    this.isTeamLeader = true;
                }
                console.log('cok = ', this.isTeamLeader);
            }
        )

        this.workTypeService.getAll()
            .subscribe((res: ResponseWrapper) => { this.worktypes = res.json; });
        this.saleTypeService.getAll()
            .subscribe((res: ResponseWrapper) => { this.saletypes = res.json; });
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInProspectKorsals();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ProspectPerson) {
        return item.idProspect;
    }

    registerChangeInProspectKorsals() {
        this.eventSubscriber = this.eventManager.subscribe('prospectKorsalListModification', (response) => this.loadAll());
    }

    clear() {
        this.page = 1;
        this.isFilter = false;
        this.router.navigate(['/prospect-korsal', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idProspect') {
            result.push('idProspect');
        }
        return result;
    }

    search() {
        this.page = 1;
        this.isFilter = true;
        this.displaySearch = false;
        this.router.navigate(['/prospect-korsal', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    doSearch() {
        this.displaySearch = true;
    }

    protected onSuccess(data, headers) {
        this.prospectKorsals = new Array<ProspectPerson>();
        this.totalItems = data[0].TotalData;
        this.queryCount = this.totalItems;
        this.prospectKorsals = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    protected subscribeToSaveResponse(result: Observable<ProspectPerson[]>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe(
                    (res: ProspectPerson[]) => {
                        resolve();
                    }
                );
            }
        );
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idSalesman;
    }

    trackWorkTypeById(index: number, item: WorkType) {
        return item.idWorkType;
    }

    getDistrict(district: any) {
        this.selectedDistrict = district;
    }

    assign() {
        this.selectedSales = null;
        this.selectedTeamLeader = null;
        this.display = true;
    }

    drop() {
        this.prospectKorsalService.drops(this.selected).subscribe((response) => {
            this.total = response.length;
            this.selected = new Array<ProspectPerson>();
        })
        this.loadAll();
        this.loadingService.loadingStop();
        this.isDrop = true;
    }

    assignTeamleader() {
        this.prospectKorsalService.assignTeamLeader(this.selectedTeamLeader, this.principal.getUserLogin().replace(/\./g, '$'), this.selected).subscribe((response) => {
            this.total = response.length;
            this.selected = new Array<ProspectPerson>();
            this.isAssignTeamLeader = true;
        });
    }

    assignSalesman() {
        this.prospectKorsalService.assignSalesman(this.selectedSales, this.principal.getUserLogin().replace(/\./g, '$'), this.selected).subscribe((response) => {
            this.total = response.length;
            this.selected = new Array<ProspectPerson>();
            this.isAssignSalesman = true;
        });

    }

    please() {
        this.salesmanService.filterTL(this.selectedTeamLeader).subscribe((res) => {
        this.namekors = res.json;
        this.korsname = this.namekors[0].name;
        console.log('prospect prsn' , this.korsname);
        });
    }

    pleases()  {
        this.salesmanService.filterid(this.selectedSales).subscribe((res) => {
        this.namekors = res.json;
        this.korsnames = this.namekors[0].name;
        console.log('prospect prsn' , this.korsnames);
        });
    }

}
