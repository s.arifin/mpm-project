import { Observable } from 'rxjs/Rx';
import { Component, OnInit, OnDestroy , Input} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ProspectPersonKorsalService } from '../prospect-person-korsal/prospect-person-korsal.service';
import { ProspectPerson } from '../prospect-person/prospect-person.model';
import { ProspectKorsalService } from './prospect-korsal.service';
import { SummaryTargetSales, SummaryTargetSalesHistory  } from '../prospect-person-korsal/prospect-person-korsal.model';
import { Salesman, SalesmanService } from '../salesman';
import { SummaryProspect } from '../shared-component';

import { LoadingService } from '../../layouts/loading/loading.service';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { Prospect } from '../prospect/index';

@Component({
    selector: 'jhi-prospect-korsal-grid-list-details',
    templateUrl: './prospect-korsal-grid-list-details.component.html'
})
export class ProspectKorsalGridListDetailsComponent implements OnInit, OnDestroy {

    @Input() idSalesman: any;
    @Input() idKorsal: any;
    protected subscription: Subscription;

    currentAccount: any;
    prospectKorsals: ProspectPerson[];
    error: any;
    success: any;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    detailModal: boolean;
    userSales: any;
    userKorsal: any;
    loading: Boolean = false;
    isCompleted: Boolean = false;
    display = false;
    selectedSales: any;
    targetSales: SummaryTargetSales[];
    selected: ProspectPerson[];

    constructor(
        protected prospectKorsalService: ProspectKorsalService,
        protected prospectPersonService: ProspectPersonKorsalService,
        protected salesmanService: SalesmanService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected route: ActivatedRoute,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected loadingService: LoadingService
    ) {
        this.selected = new Array<ProspectPerson>();
        this.prospectKorsals = new Array<ProspectPerson>();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.userSales = null;
        this.previousPage = 0;
        this.reverse = true;
        this.detailModal = false;
        this.predicate = 'idPartyRole';
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.idSalesman = null;
    }

    loadAll() {
        if (this.currentSearch) {
            this.prospectKorsalService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.prospectKorsalService.getAllGridListDetails({
            page: this.page - 1,
            size: this.itemsPerPage,
            idinternal: this.principal.getIdInternal(),
            teamleader: this.userSales.replace(/\./g, '$'),
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    showDetail(idsalesman: any) {
        // this.detail

        console.log('ID_Salesman', idsalesman);

        // this.vehicleDocumentRequirementService.find(idRequirement).subscribe((response) => {
        //     console.log('response', response);
        //     console.log('response.personOwner', response.personOwner);
        //     // this.vehicleDocumentRequirement = response;
        //     this.vehicleDocumentRequirement = response;
        //     // this.personOwner = response.personOwner;
        //     // this.postalAddress = response.personOwner.postalAddress;
        //     // console.log('-----> vehicleDocumentRequirement:', this.vehicleDocumentRequirement);
        //     // console.log('-----> salesUnitRequirement:', this.vehicleDocumentRequirement.salesUnitRequirement);
        // });
        this.idSalesman = idsalesman;
        this.detailModal = true;
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/prospect-korsal-list-sales'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/prospect-korsal-list-sales', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/prospect-korsal-list-sales', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.userSales = params['id']
                console.log('ini parameter :' , params['id']);
            } else {
                this.prospectPersonService.values.subscribe(
                    (response) => {
                        if (response) {
                            this.targetSales = response[0];
                        }
                    }
                );
            }
            this.loadAll();
        });
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: ProspectPerson) {
        return item.idProspect;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idPartyRole') {
            result.push('idPartyRole');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.prospectKorsals = new Array<ProspectPerson>();
        // this.links = this.parseLinks.parse(headers.get('link'));
        // this.totalItems = headers.get('X-Total-Count');
        this.totalItems = data[0].TotalData;
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.prospectKorsals = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    protected subscribeToSaveResponse(result: Observable<ProspectPerson[]>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe(
                    (res: ProspectPerson[]) => {
                        resolve();
                    }
                );
            }
        );
    }

    assign() {
        // this.display = false;
        // this.loadingService.loadingStart();
        this.subscribeToSaveResponse(
            this.prospectKorsalService.assignSLS(this.prospectKorsals[0].salesmanId, this.principal.getUserLogin().replace(/\./g, '$'), this.prospectKorsals)
            ).then(
                () => {
                    this.prospectKorsals = new Array<ProspectPerson>();
                    this.loadAll();
                    this.loadingService.loadingStop();
                    this.isCompleted = true;
                    this.router.navigate(['/prospect-korsal']);

            }
        );
    }

    delete() {
        this.subscribeToSaveResponse(
            this.prospectKorsalService.reAssignSLS(this.prospectKorsals[0].salesmanId, this.principal.getUserLogin().replace(/\./g, '$'), this.selected)
            ).then(
                () => {
                    this.prospectKorsals = new Array<ProspectPerson>();
                    this.loadAll();
                    this.loadingService.loadingStop();
                    this.isCompleted = true;

            }
        );
    }
}
