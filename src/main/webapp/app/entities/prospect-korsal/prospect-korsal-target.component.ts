import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, ResolveStart } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProspectPerson  } from '../prospect-person-korsal/prospect-person-korsal.model';
import { Internal, InternalService } from '../internal';
import { SalesBroker, SalesBrokerService } from '../sales-broker';
import { ProspectSource, ProspectSourceService } from '../prospect-source';
import { EventType, EventTypeService } from '../event-type';
import { Facility, FacilityService } from '../facility';
import { Salesman, SalesmanService } from '../salesman';
import { Suspect, SuspectService } from '../suspect';
import { ProspectPersonService } from '../prospect-person/prospect-person.service';
import { ToasterService} from '../../shared';
import { Person, PersonService } from '../person';
import { ResponseWrapper } from '../../shared';
import { ProspectKorsalService } from './prospect-korsal.service';
import { SuspectPerson, SuspectPersonContainer , SuspectPersonService } from '../suspect-person';
import { resolveSrv } from 'dns';

@Component({
    selector: 'jhi-prospect-korsal-target',
    templateUrl: './prospect-korsal-target.component.html'
})
export class ProspectKorsalTargetComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    salesBrokerSubscriber: Subscription;
    prospectPerson: ProspectPerson;
    suspectPersonRO: SuspectPerson;
    suspectPersonSegmentasi: SuspectPerson;
    isSaving: boolean;

    people: Person[];
    internals: Internal[];
    salesbrokers: SalesBroker[];
    prospectsources: ProspectSource[];
    eventtypes: EventType[];
    facilities: Facility[];
    salesmen: Salesman[];
    suspects: Suspect[];
    username: any;
    dateassignto: any;

    PROSPECT_EVENT_TYPE = 1;
    SALES_BROKER__TYPE_MAKELAR = 10;
    SALES_BROKER_TYPE_REFFERENCE = 11;
    SALES_BROKER_TYPE_REFFERAL = 12;
    SALES_BROKER_TYPE_OTHER = 99;

    constructor(
        protected alertService: JhiAlertService,
        protected prospectPersonService: ProspectKorsalService,
        protected suspectPersonService: SuspectPersonService,
        protected personService: PersonService,
        protected internalService: InternalService,
        protected salesBrokerService: SalesBrokerService,
        protected prospectSourceService: ProspectSourceService,
        protected eventTypeService: EventTypeService,
        protected facilityService: FacilityService,
        protected salesmanService: SalesmanService,
        protected suspectService: SuspectService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.prospectPerson = new ProspectPerson();
        this.suspectPersonSegmentasi = new SuspectPerson();
        this.suspectPersonRO = new SuspectPerson();
        this.username = null;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            } else {
                this.prospectPersonService.values.subscribe(
                    (response) => {
                        if (response) {
                            this.prospectPerson = response[0];
                        }
                    }
                );
            }
        });
        this.isSaving = false;
        this.salesBrokerSubscriber = this.salesBrokerService.values.subscribe(
            (response) => {
               if (response) {
                    this.getChosenSalesBroker(response);
               }
        });

        this.personService.query()
            .subscribe((res: ResponseWrapper) => { this.people = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.prospectSourceService.query()
            .subscribe((res: ResponseWrapper) => { this.prospectsources = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.eventTypeService.query({idparent: this.PROSPECT_EVENT_TYPE})
            .subscribe((res: ResponseWrapper) => {this.eventtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesmanService.query()
            .subscribe((res: ResponseWrapper) => { this.salesmen = res.json.filter((d) => d.coordinator === false); }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.salesBrokerSubscriber.unsubscribe();
    }

    load(id) {
        this.prospectPersonService.findId(id).subscribe((prospectPerson) => {
            this.prospectPerson = prospectPerson;
        });

        this.suspectPersonService.findRepeatOrder(id).subscribe((suspectPerson) => {
            if (suspectPerson == null) {
                this.suspectPersonService.findSegmentasi(id).subscribe((suspectPersonSegmen) => {
                    if (suspectPersonSegmen != null) {
                        this.suspectPersonSegmentasi = suspectPersonSegmen;
                    }
                });
            } else {
                this.suspectPersonRO = suspectPerson;
                console.log('tes data', this.suspectPersonRO.lastAssignDate);
            }
        });
    }

    // assignTo() {
    //     this.display = false;
    //     this.suspectPersonService.creat (this.principal.getIdInternal(), this.selectedCoordinatorSales, this.principal.getUserLogin().replace('.', '$'), this.selected).subscribe((response) => {
    //         {       this.salesmanService.filterid(this.selectedCoordinatorSales).subscribe((res) => {
    //                 this.namekors = res.json;
    //                 console.log('nama == ', this.namekors[0].name);
    //                 this.korsname = this.namekors[0].name;
    //                 console.log('cuuux == ', this.korsname);
    //                 this.smsGateway = true;
    //             });
    //         }
    //         this.total = response.length;
    //         console.log(response.length);
    //         this.smsGateway = true;
    //     });
    // }

    removeSales() {
        this.prospectPerson.brokerName = null;
        this.prospectPerson.brokerId = null;
    }

    previousState() {
        this.router.navigate(['../prospect-korsal']);
    }

    save() {
        this.isSaving = true;
        if (this.prospectPerson.idProspect !== undefined) {
            this.subscribeToSaveResponse(
                this.prospectPersonService.update(this.prospectPerson));
        } else {
            this.subscribeToSaveResponse(
                this.prospectPersonService.create(this.prospectPerson));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProspectPerson>) {
        result.subscribe((res: ProspectPerson) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ProspectPerson) {
        this.eventManager.broadcast({ name: 'prospectPersonListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'prospectPerson saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'prospectPerson Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    getChosenSalesBroker(item: SalesBroker) {
        if (item) {
            this.prospectPerson.brokerId = item.idPartyRole;
            this.prospectPerson.brokerName = item.partyName;
        }
    }

    trackPersonById(index: number, item: Person) {
        return item.idParty;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackSalesBrokerById(index: number, item: SalesBroker) {
        return item.idPartyRole;
    }

    trackProspectSourceById(index: number, item: ProspectSource) {
        return item.idProspectSource;
    }

    trackEventTypeById(index: number, item: EventType) {
        return item.idEventType;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idPartyRole;
    }
}
