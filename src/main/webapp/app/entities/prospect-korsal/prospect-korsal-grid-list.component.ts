import { Observable } from 'rxjs/Rx';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ProspectPerson } from '../prospect-person/prospect-person.model';
import { ProspectKorsalService } from './prospect-korsal.service';

import { Salesman, SalesmanService } from '../salesman';
import { SummaryProspect } from '../shared-component';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { Prospect } from '../prospect/index';

@Component({
    selector: 'jhi-prospect-korsal-grid-list',
    templateUrl: './prospect-korsal-grid-list.component.html'
})
export class ProspectKorsalGridListComponent implements OnInit, OnDestroy {

    currentAccount: any;
    prospectKorsals: SummaryProspect[];
    error: any;
    success: any;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    detailModal: boolean;

    loading: Boolean = false;
    isCompleted: Boolean = false;
    display = false;
    selectedSales: any;
    idSalesman: any;

    constructor(
        protected prospectKorsalService: ProspectKorsalService,
        protected salesmanService: SalesmanService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.previousPage = 0;
        this.reverse = true;
        this.detailModal = false;
        this.predicate = 'idPartyRole';
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.idSalesman = null;
    }

    loadAll() {
        if (this.currentSearch) {
            this.prospectKorsalService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.prospectKorsalService.listSales({
            korsal: this.principal.getUserLogin(),
            idinternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    showDetail(idsalesman: any) {
        // this.detail

        console.log('ID_Salesman', idsalesman);

        // this.vehicleDocumentRequirementService.find(idRequirement).subscribe((response) => {
        //     console.log('response', response);
        //     console.log('response.personOwner', response.personOwner);
        //     // this.vehicleDocumentRequirement = response;
        //     this.vehicleDocumentRequirement = response;
        //     // this.personOwner = response.personOwner;
        //     // this.postalAddress = response.personOwner.postalAddress;
        //     // console.log('-----> vehicleDocumentRequirement:', this.vehicleDocumentRequirement);
        //     // console.log('-----> salesUnitRequirement:', this.vehicleDocumentRequirement.salesUnitRequirement);
        // });
        this.idSalesman = idsalesman;
        this.detailModal = true;
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/prospect-korsal-list-sales'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/prospect-korsal-list-sales', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/prospect-korsal-list-sales', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
            console.log('account --->', this.currentAccount.login);
            console.log('korsal--->', this.principal.getUserLogin());
            console.log('internal--->', this.principal.getIdInternal());
        });
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: ProspectPerson) {
        return item.idProspect;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idPartyRole') {
            result.push('idPartyRole');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        if (data !== null) {
            this.totalItems = data[0].TotalData;
            // this.totalItems = headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            console.log('hasil get', data[0].TotalData, data);
            this.prospectKorsals = data;
        }
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    protected subscribeToSaveResponse(result: Observable<ProspectPerson[]>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe(
                    (res: ProspectPerson[]) => {
                        resolve();
                    }
                );
            }
        );
    }
}
