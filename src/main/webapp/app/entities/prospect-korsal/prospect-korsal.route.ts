import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ProspectKorsalComponent } from './prospect-korsal.component';
import { ProspectTLComponent } from './prospect-tl.component';
import { ProspectKorsalListSalesComponent} from './prospect-korsal-list-sales.component';
import { ProspectKorsalListTeamleadComponent} from './prospect-korsal-list-teamlead.component';
import { ProspectKorsalListTeamleadSalesComponent} from './prospect-korsal-list-teamleadsales.component';
import { ProspectKorsalRequestDropComponent } from './prospect-korsal-request-drop.component';
import { ProspectTLRequestDropComponent} from './prospect-tl-request-drop.component';
import { ProspectKorsalTargetComponent } from './prospect-korsal-target.component';
import { ProspectKorsalGridListComponent} from './prospect-korsal-grid-list.component';
import { ProspectKorsalGridListDetailsComponent} from './prospect-korsal-grid-list-details.component';
import { ProspectKorsalGridListTLComponent} from './prospect-korsal-grid-list-tl.component';
import { ProspectKorsalGridListDetailsTLComponent} from './prospect-korsal-grid-list-details-tl.component';
import { ProspectKorsalGridListSalesTLComponent} from './prospect-korsal-grid-list-sales-tl.component';
import { ProspectTlGridListDetailsComponent} from './prospect-tl-grid-list-details.component';

@Injectable()
export class ProspectKorsalResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idProspect,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const prospectKorsalRoute: Routes = [
    {
        path: 'prospect-korsal',
        component: ProspectKorsalComponent,
        resolve: {
            'pagingParams': ProspectKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectKorsal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-tl',
        component: ProspectTLComponent,
        resolve: {
            'pagingParams': ProspectKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectKorsal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-korsal-list-sales',
        component: ProspectKorsalListSalesComponent,
        resolve: {
            'pagingParams': ProspectKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectKorsal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-korsal-list-teamlead',
        component: ProspectKorsalListTeamleadComponent,
        resolve: {
            'pagingParams': ProspectKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectKorsal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-korsal-list-teamleadsales',
        component: ProspectKorsalListTeamleadSalesComponent,
        resolve: {
            'pagingParams': ProspectKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectKorsal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-korsal-request-drop',
        component: ProspectKorsalRequestDropComponent,
        resolve: {
            'pagingParams': ProspectKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectKorsal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-tl-request-drop',
        component: ProspectTLRequestDropComponent,
        resolve: {
            'pagingParams': ProspectKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectKorsal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-korsal/:id/detail',
        component: ProspectKorsalTargetComponent,
        resolve: {
            'pagingParams': ProspectKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-korsal-grid-list',
        component: ProspectKorsalGridListComponent,
        resolve: {
            'pagingParams': ProspectKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectKorsal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-korsal-grid-list/:id/details',
        component: ProspectKorsalGridListDetailsComponent,
        resolve: {
            'pagingParams': ProspectKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectKorsal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-korsal-grid-list-tl',
        component: ProspectKorsalGridListTLComponent,
        resolve: {
            'pagingParams': ProspectKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectKorsal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-korsal-grid-list/:id/details-tl',
        component: ProspectKorsalGridListDetailsTLComponent,
        resolve: {
            'pagingParams': ProspectKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectKorsal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-korsal-grid-list-sales-tl',
        component: ProspectKorsalGridListSalesTLComponent,
        resolve: {
            'pagingParams': ProspectKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectKorsal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-tl-grid-list/:id/details',
        component: ProspectTlGridListDetailsComponent,
        resolve: {
            'pagingParams': ProspectKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectKorsal.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const prospectKorsalPopupRoute: Routes = [];
