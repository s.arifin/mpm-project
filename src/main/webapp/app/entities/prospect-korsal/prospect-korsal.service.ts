import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { ProspectPerson } from '../prospect-person/prospect-person.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { createProspectParameterOption } from '../prospect/prospect-parameter.util';

import * as moment from 'moment';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Injectable()
export class ProspectKorsalService {
    protected itemValues: ProspectPerson[];
    protected message: String;
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);
    messages: BehaviorSubject<any> = new BehaviorSubject<any>(this.message);

    protected resourceUrl = SERVER_API_URL + 'api/prospect-people';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/prospect-people';
    private resourceCUrl = process.env.API_C_URL + '/api/prospect';
    private resourceCAUrl = process.env.API_C_URL + '/api/supect';
    private resourcePPUrl = process.env.API_C_URL + '/api/prospect-people-list';

    constructor(protected http: Http) { }

    create(prospectKorsal: ProspectPerson): Observable<ProspectPerson> {
        const copy = this.convert(prospectKorsal);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(prospectKorsal: ProspectPerson): Observable<ProspectPerson> {
        const copy = this.convert(prospectKorsal);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<ProspectPerson> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findId(id: any): Observable<ProspectPerson> {
        return this.http.get(`${this.resourcePPUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-korsal', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAll(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/by-korsal', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllGridListDetails(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/by-grid-list-sales-details', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllTL(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/by-teamleader', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllGridListTLDetails(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/by-teamleader-grid-list-sales-details', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryTL(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idstatustype', req.idstatustype);
        return this.http.get(this.resourceUrl + '/by-teamleader', options)
            .map((res: Response) => this.convertResponse(res));
    }

    listSales(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCAUrl + '/KorsalList', options)
            .map((res: Response) => this.convertResponse(res));
    }

    listSalesDetails(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCAUrl + '/KorsalListDetails', options)
            .map((res: Response) => this.convertResponse(res));
    }

    listTeamleader(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCAUrl + '/teamleader', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.get(`${this.resourceCAUrl}/${id}`);
    }

    listTeamlead(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCAUrl + '/KorsalTeamlead', options)
            .map((res: Response) => this.convertResponse(res));
    }

    listTeamleadSales(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCAUrl + '/KorsalTeamleadSales', options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(prospectKorsal: ProspectPerson, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(prospectKorsal);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    reDelete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, prospectKorsal: ProspectPerson): Observable<ProspectPerson> {
        const copy = this.convert(prospectKorsal);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, prospectKorsals: ProspectPerson[]): Observable<ProspectPerson[]> {
        const copy = this.convertList(prospectKorsals);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    drops(prospectPersons: ProspectPerson[]): Observable<ProspectPerson[]> {
        const copy = this.convertList(prospectPersons);
        return this.http.post(this.resourceCUrl + '/drop-grid-list', copy).map((res: Response) => {
            // this.message = res.headers.toJSON()['x-mpmapp-alert'][0];
            this.messages.next(this.message);
            return res.json();
        });
    }

    assignTeamLeader(idteamleader: String, loginName: string, prospectPersons: ProspectPerson[]): Observable<ProspectPerson[]> {
        const copy = this.convertList(prospectPersons);
        return this.http.post(this.resourceCUrl + '/assign-teamleader-grid-list/' + idteamleader + '/' + loginName, copy).map((res: Response) => {
            // this.message = res.headers.toJSON()['x-mpmapp-alert'][0];
            this.messages.next(this.message);
            return res.json();
        });
    }

    assignTL(idteamleader: String, loginName: string, prospectPersons: ProspectPerson[]): Observable<ProspectPerson[]> {
        const copy = this.convertList(prospectPersons);
        return this.http.post(this.resourceCUrl + '/assign-teamleader-grid-list-re/' + idteamleader + '/' + loginName, copy).map((res: Response) => {
            // this.message = res.headers.toJSON()['x-mpmapp-alert'][0];
            this.messages.next(this.message);
            return res.json();
        });
    }

    reAssignTL(idteamleader: String, loginName: string, prospectPersons: ProspectPerson[]): Observable<ProspectPerson[]> {
        const copy = this.convertList(prospectPersons);
        return this.http.post(this.resourceCUrl + '/re-assign-teamleader/' + idteamleader + '/' + loginName, copy).map((res: Response) => {
            // this.message = res.headers.toJSON()['x-mpmapp-alert'][0];
            this.messages.next(this.message);
            return res.json();
        });
    }

    assignSalesman(idsalesman: String, loginName: string, prospectPersons: ProspectPerson[]): Observable<ProspectPerson[]> {
        const copy = this.convertList(prospectPersons);
        return this.http.post(this.resourceCUrl + '/assign-salesman-grid-list/' + idsalesman + '/' + loginName, copy).map((res: Response) => {
            // this.message = res.headers.toJSON()['x-mpmapp-alert'][0];
            this.messages.next(this.message);
            return res.json();
        });
    }

    assignSLS(idteamleader: String, loginName: string, prospectPersons: ProspectPerson[]): Observable<ProspectPerson[]> {
        const copy = this.convertList(prospectPersons);
        return this.http.post(this.resourceCUrl + '/assign-salesman-grid-list-re/' + idteamleader + '/' + loginName, copy).map((res: Response) => {
            // this.message = res.headers.toJSON()['x-mpmapp-alert'][0];
            this.messages.next(this.message);
            return res.json();
        });
    }

    reAssignSLS(idteamleader: String, loginName: string, prospectPersons: ProspectPerson[]): Observable<ProspectPerson[]> {
        const copy = this.convertList(prospectPersons);
        return this.http.post(this.resourceCUrl + '/re-assign-salesman-grid-list/' + idteamleader + '/' + loginName, copy).map((res: Response) => {
            // this.message = res.headers.toJSON()['x-mpmapp-alert'][0];
            this.messages.next(this.message);
            return res.json();
        });
    }

    reAssignTT(idsal: string, idteamleader: String, loginName: string, prospectPersons: ProspectPerson[]): Observable<ProspectPerson[]> {
        const copy = this.convertList(prospectPersons);
        return this.http.post(this.resourceCUrl + '/re-assign-tl-grid-list/' + idsal + '/' + idteamleader + '/' + loginName, copy).map((res: Response) => {
            // this.message = res.headers.toJSON()['x-mpmapp-alert'][0];
            this.messages.next(this.message);
            return res.json();
        });
    }

    filter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/by-korsal/filter', options)
            .map((res: any) => this.convertResponse(res));
    }

    filterTL(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/by-teamleader/filter', options)
            .map((res: any) => this.convertResponse(res));
    }

    filterBy(req?: any): Observable<ResponseWrapper> {
        const options = createProspectParameterOption(req);
        return this.http.get(this.resourceUrl + '/korsal-filter', options)
            .map((res: any) => this.convertResponse(res));
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(prospectKorsal: ProspectPerson): ProspectPerson {
        if (prospectKorsal === null || prospectKorsal === {}) {
            return {};
        }
        // const copy: ProspectKorsal = Object.assign({}, prospectKorsal);
        const copy: ProspectPerson = JSON.parse(JSON.stringify(prospectKorsal));
        return copy;
    }

    protected convertList(prospectKorsals: ProspectPerson[]): ProspectPerson[] {
        const copy: ProspectPerson[] = prospectKorsals;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ProspectPerson[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
