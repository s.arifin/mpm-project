import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { RequestComponent } from './request.component';
import { RequestEditComponent } from './request-edit.component';
import { RequestPopupComponent } from './request-dialog.component';

@Injectable()
export class RequestResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequest,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const requestRoute: Routes = [
    {
        path: 'request',
        component: RequestComponent,
        resolve: {
            'pagingParams': RequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.request.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const requestPopupRoute: Routes = [
    {
        path: 'request-popup-new',
        component: RequestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.request.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'request-new',
        component: RequestEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.request.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request/:id/edit',
        component: RequestEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.request.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request/:id/edit/:page',
        component: RequestEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.request.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request/:id/popup-edit',
        component: RequestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.request.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
