import { BaseEntity } from './../../shared';

export class Request implements BaseEntity {
    constructor(
        public id?: any,
        public idRequest?: any,
        public requestNumber?: string,
        public dateCreate?: any,
        public description?: string,
        public details?: any,
        public requestTypeId?: any,
        public currentStatus?: number
    ) {
    }
}

export class CustomRequestNote implements BaseEntity {
    constructor(
        public id?: any,
        public idreqnot?: any,
        public reqnotnumber?: any,
        public dtcreated?: any,
        public marketname?: any,
        public idproduct?: any,
        public idfeature?: any,
        public color?: any,
        public cabang?: any,
        public createdby?: any,
        public status?: any,
        public idstatustype?: any,
        public QtyReq?: any,
        public QtyApproved?: any,
        public idreqnotdest?: any,
        public idinternalsrc?: any,
        public idinternaldest?: any,
        public TotalData?: any,
        public idframe?: any,
        public idmachine?: any,
        public unitprice?: any
    ) {
    }
}
