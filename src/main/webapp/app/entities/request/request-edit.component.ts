import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Request } from './request.model';
import { RequestService } from './request.service';
import { ToasterService} from '../../shared';
import { RequestType, RequestTypeService } from '../request-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-request-edit',
    templateUrl: './request-edit.component.html'
})
export class RequestEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    request: Request;
    isSaving: boolean;
    idRequest: any;
    paramPage: number;

    requesttypes: RequestType[];

    constructor(
        protected alertService: JhiAlertService,
        protected requestService: RequestService,
        protected requestTypeService: RequestTypeService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.request = new Request();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idRequest = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
        });
        this.isSaving = false;
        this.requestTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.requesttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.requestService.find(this.idRequest).subscribe((request) => {
            this.request = request;
        });
    }

    previousState() {
        this.router.navigate(['request', { page: this.paramPage }]);
    }

    save() {
        this.isSaving = true;
        if (this.request.idRequest !== undefined) {
            this.subscribeToSaveResponse(
                this.requestService.update(this.request));
        } else {
            this.subscribeToSaveResponse(
                this.requestService.create(this.request));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Request>) {
        result.subscribe((res: Request) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Request) {
        this.eventManager.broadcast({ name: 'requestListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'request saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'request Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackRequestTypeById(index: number, item: RequestType) {
        return item.idRequestType;
    }
}
