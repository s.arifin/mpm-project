import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Request } from './request.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class RequestService {
    protected itemValues: Request[];
    values: Subject<any> = new Subject<any>();

    protected resourceCUrl = process.env.API_C_URL + '/api/requestNote';
    protected resourceUrl =  SERVER_API_URL + 'api/requests';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/requests';
    protected dummy = 'http://localhost:52374/api/requestNote';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    queryFilterByinternalC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }
    detail(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/detail', options)
            .map((res: Response) => this.convertResponse(res));
    }
    // notAvailable(req?: any): Observable<ResponseWrapper> {
    //     const options = createRequestOption(req);
    //     console.log('logg', options);
    //     options.headers.append('Content-Type', 'application/json');
    //     return this.http.post(this.dummy + '/notAvailable', options)
    //         .map((res: Response) => this.convertResponse(res));
    // }
    notAvailable(customUnitModel: object): Observable<ResponseWrapper> {
        const copy = JSON.stringify(customUnitModel);

        const options: BaseRequestOptions = new BaseRequestOptions();
        // const params: URLSearchParams = new URLSearchParams();
        // params.set('query', copy);
        // options.headers = new Headers;
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl + '/notAvailable'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    create(request: Request): Observable<Request> {
        const copy = this.convert(request);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(request: Request): Observable<Request> {
        const copy = this.convert(request);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<Request> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(request: Request, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(request);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<Request> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<Request[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Request.
     */
    protected convertItemFromServer(json: any): Request {
        const entity: Request = Object.assign(new Request(), json);
        if (entity.dateCreate) {
            entity.dateCreate = new Date(entity.dateCreate);
        }
        return entity;
    }

    /**
     * Convert a Request to a JSON which can be sent to the server.
     */
    protected convert(request: Request): Request {
        if (request === null || request === {}) {
            return {};
        }
        // const copy: Request = Object.assign({}, request);
        const copy: Request = JSON.parse(JSON.stringify(request));

        // copy.dateCreate = this.dateUtils.toDate(request.dateCreate);
        return copy;
    }

    protected convertList(requests: Request[]): Request[] {
        const copy: Request[] = requests;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: Request[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
