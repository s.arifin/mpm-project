import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Request } from './request.model';
import { RequestPopupService } from './request-popup.service';
import { RequestService } from './request.service';
import { ToasterService } from '../../shared';
import { RequestType, RequestTypeService } from '../request-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-request-dialog',
    templateUrl: './request-dialog.component.html'
})
export class RequestDialogComponent implements OnInit {

    request: Request;
    isSaving: boolean;
    idRequestType: any;

    requesttypes: RequestType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected requestService: RequestService,
        protected requestTypeService: RequestTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.requestTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.requesttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.request.idRequest !== undefined) {
            this.subscribeToSaveResponse(
                this.requestService.update(this.request));
        } else {
            this.subscribeToSaveResponse(
                this.requestService.create(this.request));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Request>) {
        result.subscribe((res: Request) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: Request) {
        this.eventManager.broadcast({ name: 'requestListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'request saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'request Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRequestTypeById(index: number, item: RequestType) {
        return item.idRequestType;
    }
}

@Component({
    selector: 'jhi-request-popup',
    template: ''
})
export class RequestPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected requestPopupService: RequestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.requestPopupService
                    .open(RequestDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.requestPopupService.parent = params['parent'];
                this.requestPopupService
                    .open(RequestDialogComponent as Component);
            } else {
                this.requestPopupService
                    .open(RequestDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
