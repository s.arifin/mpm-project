import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CommunicationEventProspectComponent } from './communication-event-prospect.component';
import { CommunicationEventProspectEditComponent } from './communication-event-prospect-edit.component';
import { CommunicationEventProspectInterestFormComponent } from './communication-event-prospect-interest-form.component';
import { CommunicationEventProspectPopupComponent } from './communication-event-prospect-dialog.component';

@Injectable()
export class CommunicationEventProspectResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idCommunicationEvent,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const communicationEventProspectRoute: Routes = [
    {
        path: 'communication-event-prospect',
        component: CommunicationEventProspectComponent,
        resolve: {
            'pagingParams': CommunicationEventProspectResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventProspect.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const communicationEventProspectPopupRoute: Routes = [
    {
        path: 'communication-event-prospect-new',
        component: CommunicationEventProspectEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventProspect.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'communication-event-prospect/:id/edit',
        component: CommunicationEventProspectEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventProspect.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'communication-event-prospect/:id/:case/interest-form',
        component: CommunicationEventProspectInterestFormComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventProspect.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'communication-event-prospect-popup-new',
        component: CommunicationEventProspectPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventProspect.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'communication-event-prospect/:id/popup-edit',
        component: CommunicationEventProspectPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventProspect.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
