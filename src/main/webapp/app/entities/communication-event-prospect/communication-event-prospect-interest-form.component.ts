import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CommunicationEventProspect } from './communication-event-prospect.model';
import { CommunicationEventProspectService } from './communication-event-prospect.service';
import { ToasterService} from '../../shared';
import { SaleType, SaleTypeService } from '../sale-type';
import { Feature} from '../feature';
import { Motor, MotorService } from '../motor';
import { ResponseWrapper } from '../../shared';
import * as _ from 'lodash';

@Component({
    selector: 'jhi-communication-event-prospect-interest-form',
    templateUrl: './communication-event-prospect-interest-form.component.html'
})
export class CommunicationEventProspectInterestFormComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    communicationEventProspect: CommunicationEventProspect;
    isSaving: boolean;

    saletypes: SaleType[];

    features: Feature[];

    motors: Motor[];

    case: number;

    purchasePlans: Array<object> = [
        {label : '< 2 Minggu', value : '< 2 Minggu'},
        {label : '< 1 Bulan', value : '< 1 Bulan'},
        {label : '> 1 Bulan', value : '> 1 Bulan'},
    ];

    constructor(
        protected alertService: JhiAlertService,
        protected communicationEventProspectService: CommunicationEventProspectService,
        protected saleTypeService: SaleTypeService,
        protected motorService: MotorService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.communicationEventProspect = new CommunicationEventProspect();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
            this.case = params['case'] ;
        });
        this.isSaving = false;
        this.saleTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.saletypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query({
            page: 0,
            size: 10000,
            sort: ['idProduct', 'asc']
        }).subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.communicationEventProspectService.find(id).subscribe((communicationEventProspect) => {
            this.communicationEventProspect = communicationEventProspect;
        });
    }

    previousState() {
        if (this.case.toString() === '101') {
            this.router.navigate(['prospect-person']);
        } else if (this.case.toString() === '102') {
            this.router.navigate(['prospect-organization']);
        }
    }

    save() {
        this.isSaving = true;
        if (this.communicationEventProspect.idCommunicationEvent !== undefined) {
            this.subscribeToSaveResponse(
                this.communicationEventProspectService.executeProcess(this.case, null, this.communicationEventProspect));
                // this.communicationEventProspectService.update(this.communicationEventProspect, this.case));
        }
    }

    disableInterestForm(): Boolean {
        if (this.communicationEventProspect.purchasePlan == null ||
            this.communicationEventProspect.purchasePlan === undefined ||
            this.communicationEventProspect.qty == null ||
            this.communicationEventProspect.qty === undefined ||
            this.communicationEventProspect.qty <= 0) {
            return true
        } else { return false };
    }

    public selectMotor(isSelect?: boolean): void {
        this.getColorByMotor(this.communicationEventProspect.productId);
    }

    protected getColorByMotor(idProduct: string): void {
        if (idProduct !== null) {
            this.features = new Array<Feature>();
            const selectedProduct: Motor = _.find(this.motors, function(e) {
                return e.idProduct.toLowerCase() === idProduct.toLowerCase();
            });
            this.features = selectedProduct.features;
        }
    }

    protected subscribeToSaveResponse(result: Observable<CommunicationEventProspect>) {
        result.subscribe((res: CommunicationEventProspect) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: CommunicationEventProspect) {
        this.eventManager.broadcast({ name: 'communicationEventProspectListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'communicationEventProspect saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'communicationEventProspect Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}
