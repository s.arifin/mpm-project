import { BaseEntity } from './../../shared';

export class CommunicationEventProspect implements BaseEntity {
    constructor(
        public id?: any,
        public idCommunicationEvent?: any,
        public note?: string,
        public purposeId?: any,
        public idProspect?: string,
        public interest?: boolean,
        public qty?: number,
        public eventTypeId?: any,
        public saleTypeId?: any,
        public saleTypeDescription?: any,
        public colorId?: any,
        public colorDescription?: any,
        public productId?: any,
        public productName?: any,
        public purchasePlan?: any,
        public currentStatus?: any,
        public walkInType?: any,
        public nextFollowUp?: any,
        public walkInYype?: any,
        public prospectNumber?: any,
    ) {
        this.purposeId = 101;
    }
}
