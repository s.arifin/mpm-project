import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CommunicationEventProspect } from './communication-event-prospect.model';
import { CommunicationEventProspectService } from './communication-event-prospect.service';
import { ToasterService} from '../../shared';
import { EventType, EventTypeService } from '../event-type';
import { SaleType, SaleTypeService } from '../sale-type';
import { Feature, FeatureService } from '../feature';
import { Motor, MotorService } from '../motor';
import { ResponseWrapper } from '../../shared';
import * as _ from 'lodash';

@Component({
    selector: 'jhi-communication-event-prospect-edit',
    templateUrl: './communication-event-prospect-edit.component.html'
})
export class CommunicationEventProspectEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    communicationEventProspect: CommunicationEventProspect;
    isSaving: boolean;

    eventtypes: EventType[];

    saletypes: SaleType[];

    features: Feature[];

    motors: Motor[];

    constructor(
        protected alertService: JhiAlertService,
        protected communicationEventProspectService: CommunicationEventProspectService,
        protected eventTypeService: EventTypeService,
        protected saleTypeService: SaleTypeService,
        protected featureService: FeatureService,
        protected motorService: MotorService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.communicationEventProspect = new CommunicationEventProspect();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.eventTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.eventtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.saleTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.saletypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query({
            page: 0,
            size: 10000,
            sort: ['idProduct', 'asc']
        }).subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.communicationEventProspectService.find(id).subscribe((communicationEventProspect) => {
            this.communicationEventProspect = communicationEventProspect;
        });
    }

    previousState() {
        this.router.navigate(['communication-event-prospect']);
    }

    save() {
        this.isSaving = true;
        if (this.communicationEventProspect.idCommunicationEvent !== undefined) {
            this.subscribeToSaveResponse(
                this.communicationEventProspectService.update(this.communicationEventProspect));
        } else {
            this.subscribeToSaveResponse(
                this.communicationEventProspectService.create(this.communicationEventProspect));
        }
    }

    public selectMotor(isSelect?: boolean): void {
        this.getColorByMotor(this.communicationEventProspect.productId);
    }

    protected getColorByMotor(idProduct: string): void {
        if (idProduct !== null) {
            this.features = new Array<Feature>();
            const selectedProduct: Motor = _.find(this.motors, function(e) {
                return e.idProduct.toLowerCase() === idProduct.toLowerCase();
            });
            this.features = selectedProduct.features;
        }
    }

    protected subscribeToSaveResponse(result: Observable<CommunicationEventProspect>) {
        result.subscribe((res: CommunicationEventProspect) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: CommunicationEventProspect) {
        this.eventManager.broadcast({ name: 'communicationEventProspectListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'communicationEventProspect saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'communicationEventProspect Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackEventTypeById(index: number, item: EventType) {
        return item.idEventType;
    }

    trackSaleTypeById(index: number, item: SaleType) {
        return item.idSaleType;
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }
}
