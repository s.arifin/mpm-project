import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { CommunicationEventProspect } from './communication-event-prospect.model';
import { CommunicationEventProspectService } from './communication-event-prospect.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import { LoadingService } from '../../layouts/loading/loading.service';

// nuse
import { ProspectPerson, ProspectPersonService} from '../prospect-person';
import { Customer, CustomerCService} from '../customer';
import { AxCustomer, AxCustomerService } from '../axcustomer';

@Component({
    selector: 'jhi-communication-event-prospect-as-list',
    templateUrl: './communication-event-prospect-as-list.component.html'
})
export class CommunicationEventProspectAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idProspect: any;
    @Input() usedFor: string;
    @Input() hasConfirmDialog: boolean;

    currentAccount: any;
    communicationEventProspects: CommunicationEventProspect[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    goodSubs: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    isEmpty: boolean;
    // nuse
    customer: Customer;
    prospectPerson: ProspectPerson;
    axCustomer: AxCustomer;

    public isDetail: boolean;

    constructor(
        protected communicationEventProspectService: CommunicationEventProspectService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService,
        protected loadingService: LoadingService,
        // nuse
        protected customerCService: CustomerCService,
        protected prospectPersonService: ProspectPersonService,
        protected axCustomerService: AxCustomerService
    ) {
        this.hasConfirmDialog = false;
        this.isDetail = false;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idCommunicationEvent';
        this.reverse = 'asc';
        this.first = 0;
    }

    loadAll() {
        this.communicationEventProspectService.queryFilterBy({
            idProspect: this.idProspect,
            interest: true,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/communication-event-prospect', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.isEmpty = false;
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCommunicationEventProspects();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idProspect']) {
            this.loadAll();
        }

        if (changes['usedFor']) {
            if (this.usedFor === 'detail') {
                this.isDetail = true;
            }
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: CommunicationEventProspect) {
        return item.idCommunicationEvent;
    }

    registerChangeInCommunicationEventProspects() {
        this.eventSubscriber = this.eventManager.subscribe('communicationEventProspectListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idCommunicationEvent') {
            result.push('idCommunicationEvent');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.communicationEventProspects = new Array<CommunicationEventProspect>();

        if (data.length !== 0) {
            this.isEmpty = true;
        }

        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.communicationEventProspects = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    processToSalesUnitRequirement(item: CommunicationEventProspect): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to process to VSO Draft?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.subscribeToSaveResponse(
                    this.communicationEventProspectService.executeProcess(103, null, item)
                ).then(
                    () => {
                        this.loadingService.loadingStop();
                        this.eventManager.broadcast({
                            name: 'communicationEventProspectListModification',
                            content: 'Completed'
                        });
                });
            }
        });
    }

    protected subscribeToSaveResponse(result: Observable<CommunicationEventProspect>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe(
                    (res: CommunicationEventProspect) => {
                        resolve();
                    }
                );
            }
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
}
