import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { CommunicationEventProspect } from './communication-event-prospect.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class CommunicationEventProspectService {
    protected itemValues: CommunicationEventProspect[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/communication-event-prospects';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/communication-event-prospects';

    constructor(protected http: Http) { }

    public getPurchasePlan(): Array<object> {
        return [
            {label : '< 2 Minggu', value : '< 2 Minggu'},
            {label : '< 1 Bulan', value : '< 1 Bulan'},
            {label : '> 1 Bulan', value : '> 1 Bulan'},
        ];
    }

    create(communicationEventProspect: CommunicationEventProspect): Observable<CommunicationEventProspect> {
        const copy = this.convert(communicationEventProspect);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(communicationEventProspect: CommunicationEventProspect): Observable<CommunicationEventProspect> {
        const copy = this.convert(communicationEventProspect);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<CommunicationEventProspect> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idprospect', req.idprospect);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(communicationEventProspect: CommunicationEventProspect, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(communicationEventProspect);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, communicationEventProspect: CommunicationEventProspect): Observable<CommunicationEventProspect> {
        const copy = this.convert(communicationEventProspect);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, communicationEventProspects: CommunicationEventProspect[]): Observable<CommunicationEventProspect[]> {
        const copy = this.convertList(communicationEventProspects);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(communicationEventProspect: CommunicationEventProspect): CommunicationEventProspect {
        if (communicationEventProspect === null || communicationEventProspect === {}) {
            return {};
        }
        // const copy: CommunicationEventProspect = Object.assign({}, communicationEventProspect);
        const copy: CommunicationEventProspect = JSON.parse(JSON.stringify(communicationEventProspect));
        return copy;
    }

    protected convertList(communicationEventProspects: CommunicationEventProspect[]): CommunicationEventProspect[] {
        const copy: CommunicationEventProspect[] = communicationEventProspects;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: CommunicationEventProspect[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
