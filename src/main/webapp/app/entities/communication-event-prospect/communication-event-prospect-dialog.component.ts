import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {CommunicationEventProspect} from './communication-event-prospect.model';
import {CommunicationEventProspectPopupService} from './communication-event-prospect-popup.service';
import {CommunicationEventProspectService} from './communication-event-prospect.service';
import {ToasterService} from '../../shared';
import { EventType, EventTypeService } from '../event-type';
import { SaleType, SaleTypeService } from '../sale-type';
import { Feature, FeatureService } from '../feature';
import { Motor, MotorService } from '../motor';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-communication-event-prospect-dialog',
    templateUrl: './communication-event-prospect-dialog.component.html'
})
export class CommunicationEventProspectDialogComponent implements OnInit {

    communicationEventProspect: CommunicationEventProspect;
    isSaving: boolean;

    eventtypes: EventType[];

    saletypes: SaleType[];

    features: Feature[];

    motors: Motor[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected communicationEventProspectService: CommunicationEventProspectService,
        protected eventTypeService: EventTypeService,
        protected saleTypeService: SaleTypeService,
        protected featureService: FeatureService,
        protected motorService: MotorService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.eventTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.eventtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.saleTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.saletypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.featureService.query()
            .subscribe((res: ResponseWrapper) => { this.features = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query()
            .subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.communicationEventProspect.idCommunicationEvent !== undefined) {
            this.subscribeToSaveResponse(
                this.communicationEventProspectService.update(this.communicationEventProspect));
        } else {
            this.subscribeToSaveResponse(
                this.communicationEventProspectService.create(this.communicationEventProspect));
        }
    }

    protected subscribeToSaveResponse(result: Observable<CommunicationEventProspect>) {
        result.subscribe((res: CommunicationEventProspect) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: CommunicationEventProspect) {
        this.eventManager.broadcast({ name: 'communicationEventProspectListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'communicationEventProspect saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'communicationEventProspect Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackEventTypeById(index: number, item: EventType) {
        return item.idEventType;
    }

    trackSaleTypeById(index: number, item: SaleType) {
        return item.idSaleType;
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }
}

@Component({
    selector: 'jhi-communication-event-prospect-popup',
    template: ''
})
export class CommunicationEventProspectPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected communicationEventProspectPopupService: CommunicationEventProspectPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.communicationEventProspectPopupService
                    .open(CommunicationEventProspectDialogComponent as Component, params['id']);
            } else {
                this.communicationEventProspectPopupService
                    .open(CommunicationEventProspectDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
