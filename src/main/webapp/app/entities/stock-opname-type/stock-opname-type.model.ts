import { BaseEntity } from './../../shared';

export class StockOpnameType implements BaseEntity {
    constructor(
        public id?: number,
        public idStockOpnameType?: number,
        public description?: string,
    ) {
    }
}
