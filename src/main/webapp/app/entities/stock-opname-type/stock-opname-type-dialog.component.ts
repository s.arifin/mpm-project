import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { StockOpnameType } from './stock-opname-type.model';
import { StockOpnameTypePopupService } from './stock-opname-type-popup.service';
import { StockOpnameTypeService } from './stock-opname-type.service';
import { ToasterService } from '../../shared';

@Component({
    selector: 'jhi-stock-opname-type-dialog',
    templateUrl: './stock-opname-type-dialog.component.html'
})
export class StockOpnameTypeDialogComponent implements OnInit {

    stockOpnameType: StockOpnameType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected stockOpnameTypeService: StockOpnameTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.stockOpnameType.idStockOpnameType !== undefined) {
            this.subscribeToSaveResponse(
                this.stockOpnameTypeService.update(this.stockOpnameType));
        } else {
            this.subscribeToSaveResponse(
                this.stockOpnameTypeService.create(this.stockOpnameType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<StockOpnameType>) {
        result.subscribe((res: StockOpnameType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: StockOpnameType) {
        this.eventManager.broadcast({ name: 'stockOpnameTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'stockOpnameType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-stock-opname-type-popup',
    template: ''
})
export class StockOpnameTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected stockOpnameTypePopupService: StockOpnameTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.stockOpnameTypePopupService
                    .open(StockOpnameTypeDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.stockOpnameTypePopupService.parent = params['parent'];
                this.stockOpnameTypePopupService
                    .open(StockOpnameTypeDialogComponent as Component);
            } else {
                this.stockOpnameTypePopupService
                    .open(StockOpnameTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
