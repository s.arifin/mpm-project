import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { StockOpnameType } from './stock-opname-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class StockOpnameTypeService {
    protected itemValues: StockOpnameType[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/stock-opname-types';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/stock-opname-types';

    constructor(protected http: Http) { }

    create(stockOpnameType: StockOpnameType): Observable<StockOpnameType> {
        const copy = this.convert(stockOpnameType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(stockOpnameType: StockOpnameType): Observable<StockOpnameType> {
        const copy = this.convert(stockOpnameType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<StockOpnameType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: StockOpnameType, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: StockOpnameType[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to StockOpnameType.
     */
    protected convertItemFromServer(json: any): StockOpnameType {
        const entity: StockOpnameType = Object.assign(new StockOpnameType(), json);
        return entity;
    }

    /**
     * Convert a StockOpnameType to a JSON which can be sent to the server.
     */
    protected convert(stockOpnameType: StockOpnameType): StockOpnameType {
        if (stockOpnameType === null || stockOpnameType === {}) {
            return {};
        }
        // const copy: StockOpnameType = Object.assign({}, stockOpnameType);
        const copy: StockOpnameType = JSON.parse(JSON.stringify(stockOpnameType));
        return copy;
    }

    protected convertList(stockOpnameTypes: StockOpnameType[]): StockOpnameType[] {
        const copy: StockOpnameType[] = stockOpnameTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: StockOpnameType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
