import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { StockOpnameTypeComponent } from './stock-opname-type.component';
import { StockOpnameTypePopupComponent } from './stock-opname-type-dialog.component';

@Injectable()
export class StockOpnameTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idStockOpnameType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const stockOpnameTypeRoute: Routes = [
    {
        path: 'stock-opname-type',
        component: StockOpnameTypeComponent,
        resolve: {
            'pagingParams': StockOpnameTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpnameType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const stockOpnameTypePopupRoute: Routes = [
    {
        path: 'stock-opname-type-new',
        component: StockOpnameTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpnameType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stock-opname-type/:id/edit',
        component: StockOpnameTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.stockOpnameType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
