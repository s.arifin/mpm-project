import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { RuleIndentComponent } from './rule-indent.component';
import { RuleIndentEditComponent } from './rule-indent-edit.component';
import { RuleIndentPopupComponent } from './rule-indent-dialog.component';

@Injectable()
export class RuleIndentResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRule,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const ruleIndentRoute: Routes = [
    {
        path: 'rule-indent',
        component: RuleIndentComponent,
        resolve: {
            'pagingParams': RuleIndentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.ruleIndent.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ruleIndentPopupRoute: Routes = [
    {
        path: 'rule-indent-popup-new-list/:parent',
        component: RuleIndentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.ruleIndent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rule-indent-popup-new',
        component: RuleIndentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.ruleIndent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rule-indent-new',
        component: RuleIndentEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.ruleIndent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'rule-indent/:id/edit',
        component: RuleIndentEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.ruleIndent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'rule-indent/:id/edit/:page',
        component: RuleIndentEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.ruleIndent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'rule-indent/:id/popup-edit',
        component: RuleIndentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.ruleIndent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
