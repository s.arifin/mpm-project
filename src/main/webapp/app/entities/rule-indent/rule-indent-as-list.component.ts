import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { RuleIndent } from './rule-indent.model';
import { RuleIndentService } from './rule-indent.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-rule-indent-as-list',
    templateUrl: './rule-indent-as-list.component.html'
})
export class RuleIndentAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input()
    idProduct: String;

    currentAccount: any;
    ruleIndents: RuleIndent[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: Number;

    constructor(
        protected ruleIndentService: RuleIndentService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idRule';
        this.reverse = 'asc';
    }

    loadAll() {
        this.ruleIndentService.findAllRuleIndentByIdProduct({
            idProduct: this.idProduct,
            // idInternal: this.principal.getIdInternal(),
            idInternal: this.principalService.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/rule-indent', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principalService.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInRuleIndents();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idProduct']) {
            if (this.idProduct !== undefined) {
                this.loadAll();
                this.registerChangeInRuleIndents();
            }
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: RuleIndent) {
        return item.idRule;
    }

    registerChangeInRuleIndents() {
        this.eventSubscriber = this.eventManager.subscribe('ruleIndentListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRule') {
            result.push('idRule');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.ruleIndents = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.ruleIndentService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.ruleIndentService.update(event.data)
                .subscribe((res: RuleIndent) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.ruleIndentService.create(event.data)
                .subscribe((res: RuleIndent) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: RuleIndent) {
        this.toasterService.showToaster('info', 'RuleIndent Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.ruleIndentService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'ruleIndentListModification',
                        content: 'Deleted an ruleIndent'
                    });
                });
            }
        });
    }
}
