import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RuleIndent } from './rule-indent.model';
import { RuleIndentPopupService } from './rule-indent-popup.service';
import { RuleIndentService } from './rule-indent.service';
import { ToasterService, Principal } from '../../shared';
import * as RuleTypeConstant from '../../shared/constants/rule-type.constants';

@Component({
    selector: 'jhi-rule-indent-dialog',
    templateUrl: './rule-indent-dialog.component.html'
})
export class RuleIndentDialogComponent implements OnInit {
    public data: any;
    public ruleIndent: RuleIndent;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected ruleIndentService: RuleIndentService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected principalService: Principal
    ) {
    }

    ngOnInit() {
        this.ruleIndent.idInternal = this.principalService.getIdInternal();
        this.ruleIndent.idProduct = this.data;
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.ruleIndent.idRule !== undefined) {
            this.subscribeToSaveResponse(
                this.ruleIndentService.update(this.ruleIndent));
        } else {
            this.ruleIndent.idRuleType = RuleTypeConstant.INDENT;
            this.subscribeToSaveResponse(
                this.ruleIndentService.create(this.ruleIndent));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RuleIndent>) {
        result.subscribe((res: RuleIndent) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: RuleIndent) {
        this.eventManager.broadcast({ name: 'ruleIndentListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'ruleIndent saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-rule-indent-popup',
    template: ''
})
export class RuleIndentPopupComponent implements OnInit, OnDestroy {
    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected ruleIndentPopupService: RuleIndentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.ruleIndentPopupService
                    .open(RuleIndentDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.ruleIndentPopupService.parent = params['parent'];
                this.ruleIndentPopupService
                    .open(RuleIndentDialogComponent as Component);
            } else {
                this.ruleIndentPopupService
                    .open(RuleIndentDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
