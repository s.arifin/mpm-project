import { BaseEntity } from './../../shared';
import { Rules } from '../rules/rules.model';

export class RuleIndent extends Rules {
    constructor(
        public idRule?: number,
        public idProduct?: string,
        public minPayment?: number,
    ) {
        super();
    }
}
