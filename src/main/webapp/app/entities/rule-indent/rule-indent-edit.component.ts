import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RuleIndent } from './rule-indent.model';
import { RuleIndentService } from './rule-indent.service';
import { ToasterService} from '../../shared';

@Component({
    selector: 'jhi-rule-indent-edit',
    templateUrl: './rule-indent-edit.component.html'
})
export class RuleIndentEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    ruleIndent: RuleIndent;
    isSaving: boolean;
    idRule: any;
    paramPage: number;

    constructor(
        protected alertService: JhiAlertService,
        protected ruleIndentService: RuleIndentService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.ruleIndent = new RuleIndent();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idRule = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
        });
        this.isSaving = false;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.ruleIndentService.find(this.idRule).subscribe((ruleIndent) => {
            this.ruleIndent = ruleIndent;
        });
    }

    previousState() {
        this.router.navigate(['rule-indent', { page: this.paramPage }]);
    }

    save() {
        this.isSaving = true;
        if (this.ruleIndent.idRule !== undefined) {
            this.subscribeToSaveResponse(
                this.ruleIndentService.update(this.ruleIndent));
        } else {
            this.subscribeToSaveResponse(
                this.ruleIndentService.create(this.ruleIndent));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RuleIndent>) {
        result.subscribe((res: RuleIndent) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: RuleIndent) {
        this.eventManager.broadcast({ name: 'ruleIndentListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'ruleIndent saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'ruleIndent Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}
