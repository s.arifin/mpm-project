import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { RuleIndent } from './rule-indent.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class RuleIndentService {
    protected itemValues: RuleIndent[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/rule-indents';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/rule-indents';

    constructor(protected http: Http) { }

    public findAllRuleIndentByIdProduct(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idProduct', req.idProduct);
        options.params.set('idInternal', req.idInternal);
        return this.http.get(this.resourceUrl + '/by-product', options)
            .map(
                (res: Response) => this.convertResponse(res)
            )
    }

    public checkByIdInternalAndIdProduct(req?: any): Observable<Boolean> {
        const options = createRequestOption(req);
        options.params.set('idProduct', req.idProduct);
        options.params.set('idInternal', req.idInternal);
        return this.http.get(this.resourceUrl + '/check-by-product-and-internal' , options)
            .map((res: Response) => {
                return res.json();
            });
    }

    create(ruleIndent: RuleIndent): Observable<RuleIndent> {
        const copy = this.convert(ruleIndent);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(ruleIndent: RuleIndent): Observable<RuleIndent> {
        const copy = this.convert(ruleIndent);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<RuleIndent> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<RuleIndent> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<RuleIndent[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to RuleIndent.
     */
    protected convertItemFromServer(json: any): RuleIndent {
        const entity: RuleIndent = Object.assign(new RuleIndent(), json);
        return entity;
    }

    /**
     * Convert a RuleIndent to a JSON which can be sent to the server.
     */
    protected convert(ruleIndent: RuleIndent): RuleIndent {
        if (ruleIndent === null || ruleIndent === {}) {
            return {};
        }
        // const copy: RuleIndent = Object.assign({}, ruleIndent);
        const copy: RuleIndent = JSON.parse(JSON.stringify(ruleIndent));
        return copy;
    }

    protected convertList(ruleIndents: RuleIndent[]): RuleIndent[] {
        const copy: RuleIndent[] = ruleIndents;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: RuleIndent[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
