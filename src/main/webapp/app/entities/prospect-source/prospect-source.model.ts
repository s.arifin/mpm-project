import { BaseEntity } from './../../shared';

export class ProspectSource implements BaseEntity {
    constructor(
        public id?: number,
        public idProspectSource?: number,
        public description?: string,
    ) {
    }
}
