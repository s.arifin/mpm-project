import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {ProspectSource} from './prospect-source.model';
import {ProspectSourcePopupService} from './prospect-source-popup.service';
import {ProspectSourceService} from './prospect-source.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-prospect-source-dialog',
    templateUrl: './prospect-source-dialog.component.html'
})
export class ProspectSourceDialogComponent implements OnInit {

    prospectSource: ProspectSource;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected prospectSourceService: ProspectSourceService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.prospectSource.idProspectSource !== undefined) {
            this.subscribeToSaveResponse(
                this.prospectSourceService.update(this.prospectSource));
        } else {
            this.subscribeToSaveResponse(
                this.prospectSourceService.create(this.prospectSource));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProspectSource>) {
        result.subscribe((res: ProspectSource) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ProspectSource) {
        this.eventManager.broadcast({ name: 'prospectSourceListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'prospectSource saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'prospectSource Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-prospect-source-popup',
    template: ''
})
export class ProspectSourcePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected prospectSourcePopupService: ProspectSourcePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.prospectSourcePopupService
                    .open(ProspectSourceDialogComponent as Component, params['id']);
            } else {
                this.prospectSourcePopupService
                    .open(ProspectSourceDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
