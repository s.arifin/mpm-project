import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LeasingCompany } from './leasing-company.model';
import { LeasingCompanyService } from './leasing-company.service';
import { ToasterService} from '../../shared';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-leasing-company-edit',
    templateUrl: './leasing-company-edit.component.html'
})
export class LeasingCompanyEditComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    leasingCompany: LeasingCompany;
    isSaving: boolean;

    constructor(
        private alertService: JhiAlertService,
        private leasingCompanyService: LeasingCompanyService,
        private route: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
        this.leasingCompany = new LeasingCompany();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.leasingCompanyService.find(id).subscribe((leasingCompany) => {
            this.leasingCompany = leasingCompany;
            console.log('this.leasingCompany', this.leasingCompany);
        });
    }

    previousState() {
        this.router.navigate(['leasing-company']);
    }

    save() {
        this.isSaving = true;
        if (this.leasingCompany.id !== undefined) {
            this.subscribeToSaveResponse(
                this.leasingCompanyService.update(this.leasingCompany));
        } else {
            this.subscribeToSaveResponse(
                this.leasingCompanyService.create(this.leasingCompany));
        }
    }

    private subscribeToSaveResponse(result: Observable<LeasingCompany>) {
        result.subscribe((res: LeasingCompany) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: LeasingCompany) {
        this.eventManager.broadcast({ name: 'leasingCompanyListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'leasingCompany saved !');
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'leasingCompany Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}
