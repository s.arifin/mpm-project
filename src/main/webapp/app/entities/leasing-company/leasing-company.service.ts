import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { LeasingCompany } from './leasing-company.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class LeasingCompanyService {
    private itemValues: LeasingCompany[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceJAVAUrl = SERVER_API_URL + 'api/leasing-companies';
    private resourceSearchUrl = process.env.API_C_URL + '/api/_search/leasing-companies';
    private resourceUrl = process.env.API_C_URL + '/api/leasing-companies';

    constructor(private http: Http) { }

    create(leasingCompany: LeasingCompany): Observable<LeasingCompany> {
        const copy = this.convert(leasingCompany);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(leasingCompany: LeasingCompany): Observable<LeasingCompany> {
        const copy = this.convert(leasingCompany);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<LeasingCompany> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceJAVAUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryNet(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAll(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllDropdown(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/getAll')
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, leasingCompany: LeasingCompany): Observable<LeasingCompany> {
        const copy = this.convert(leasingCompany);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, leasingCompanys: LeasingCompany[]): Observable<LeasingCompany[]> {
        const copy = this.convertList(leasingCompanys);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(leasingCompany: LeasingCompany): LeasingCompany {
        if (leasingCompany === null || leasingCompany === {}) {
            return {};
        }
        // const copy: LeasingCompany = Object.assign({}, leasingCompany);
        const copy: LeasingCompany = JSON.parse(JSON.stringify(leasingCompany));
        return copy;
    }

    private convertList(leasingCompanys: LeasingCompany[]): LeasingCompany[] {
        const copy: LeasingCompany[] = leasingCompanys;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: LeasingCompany[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
