import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Subscription } from 'rxjs/Rx';

import { LeasingCompany } from './leasing-company.model';
import { LeasingCompanyService } from './leasing-company.service';

@Component({
    selector: 'jhi-leasing-company-view',
    templateUrl: './leasing-company-view.component.html'
})
export class LeasingCompanyViewComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    leasingCompany: LeasingCompany;

    constructor(
        private leasingCompanyService: LeasingCompanyService,
        private route: ActivatedRoute,
        private router: Router,
    ) {
        this.leasingCompany = new LeasingCompany();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.leasingCompanyService.find(id).subscribe((leasingCompany) => {
            this.leasingCompany = leasingCompany;
        });
    }

    previousState() {
        this.router.navigate(['leasing-company']);
    }
}
