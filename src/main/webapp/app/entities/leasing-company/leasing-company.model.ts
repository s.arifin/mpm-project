import { BaseEntity } from './../shared-component';
import { Organization } from './../organization';
import { PartyRole } from './../party-role';

export class LeasingCompany extends PartyRole {
    constructor(
        public id?: any,
        public idPartyRole?: any,
        public idLeasingCompany?: string,
        public idLeasingAhm?: string,
        public idLeasingFinCoy?: string,
        public organization?: Organization
    ) {
        super();
        this.organization = new Organization();
    }
}
