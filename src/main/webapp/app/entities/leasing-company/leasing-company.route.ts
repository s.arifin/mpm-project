import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { LeasingCompanyComponent } from './leasing-company.component';
import { LeasingCompanyEditComponent } from './leasing-company-edit.component';
import { LeasingCompanyViewComponent } from './leasing-company-view.component';
import { LeasingCompanyLovPopupComponent } from './leasing-company-as-lov.component';
import { LeasingCompanyPopupComponent } from './leasing-company-dialog.component';

@Injectable()
export class LeasingCompanyResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idPartyRole,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const leasingCompanyRoute: Routes = [
    {
        path: 'leasing-company',
        component: LeasingCompanyComponent,
        resolve: {
            'pagingParams': LeasingCompanyResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.leasingCompany.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const leasingCompanyPopupRoute: Routes = [
    {
        path: 'leasing-company-lov',
        component: LeasingCompanyLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.leasingCompany.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'leasing-company-new',
        component: LeasingCompanyEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.leasingCompany.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'leasing-company/:id/edit',
        component: LeasingCompanyEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.leasingCompany.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'leasing-company/:id/view',
        component: LeasingCompanyViewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.leasingCompany.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'leasing-company-popup-new',
        component: LeasingCompanyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.leasingCompany.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'leasing-company/:id/popup-edit',
        component: LeasingCompanyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.leasingCompany.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
