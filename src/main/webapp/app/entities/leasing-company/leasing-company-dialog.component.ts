import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {LeasingCompany} from './leasing-company.model';
import {LeasingCompanyPopupService} from './leasing-company-popup.service';
import {LeasingCompanyService} from './leasing-company.service';
import {ToasterService} from '../../shared';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-leasing-company-dialog',
    templateUrl: './leasing-company-dialog.component.html'
})
export class LeasingCompanyDialogComponent implements OnInit {

    leasingCompany: LeasingCompany;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private leasingCompanyService: LeasingCompanyService,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.leasingCompany.id !== undefined) {
            this.subscribeToSaveResponse(
                this.leasingCompanyService.update(this.leasingCompany));
        } else {
            this.subscribeToSaveResponse(
                this.leasingCompanyService.create(this.leasingCompany));
        }
    }

    private subscribeToSaveResponse(result: Observable<LeasingCompany>) {
        result.subscribe((res: LeasingCompany) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: LeasingCompany) {
        this.eventManager.broadcast({ name: 'leasingCompanyListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'leasingCompany saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.toaster.showToaster('warning', 'leasingCompany Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-leasing-company-popup',
    template: ''
})
export class LeasingCompanyPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private leasingCompanyPopupService: LeasingCompanyPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.leasingCompanyPopupService
                    .open(LeasingCompanyDialogComponent as Component, params['id']);
            } else {
                this.leasingCompanyPopupService
                    .open(LeasingCompanyDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
