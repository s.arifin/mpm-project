import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { RuleSalesDiscountComponent } from './rule-sales-discount.component';
import { RuleSalesDiscountPopupComponent } from './rule-sales-discount-dialog.component';

@Injectable()
export class RuleSalesDiscountResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRule,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const ruleSalesDiscountRoute: Routes = [
    {
        path: 'rule-sales-discount',
        component: RuleSalesDiscountComponent,
        resolve: {
            'pagingParams': RuleSalesDiscountResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.ruleSalesDiscount.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ruleSalesDiscountPopupRoute: Routes = [
    {
        path: 'rule-sales-discount-new',
        component: RuleSalesDiscountPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.ruleSalesDiscount.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rule-sales-discount/:id/edit',
        component: RuleSalesDiscountPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.ruleSalesDiscount.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
