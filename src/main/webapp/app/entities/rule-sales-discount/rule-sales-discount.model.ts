import { BaseEntity } from './../../shared';
import { Rules } from '../rules';

export class RuleSalesDiscount extends Rules {
    constructor(
        public idRule?: number,
        public role?: string,
        public maxAmount?: number,
        public sequenceNumber?: number,
    ) {
        super();
    }
}
