import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';

import { RuleSalesDiscount } from './rule-sales-discount.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class RuleSalesDiscountService {
    protected itemValues: RuleSalesDiscount[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/rule-sales-discounts';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/rule-sales-discounts';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(ruleSalesDiscount: RuleSalesDiscount): Observable<RuleSalesDiscount> {
        const copy = this.convert(ruleSalesDiscount);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(ruleSalesDiscount: RuleSalesDiscount): Observable<RuleSalesDiscount> {
        const copy = this.convert(ruleSalesDiscount);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<RuleSalesDiscount> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findAllByIdInternal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idinternal', req.idinternal);

        const url = this.resourceUrl + '/by-internal';
        return this.http.get(url, options)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, ruleSalesDiscount: RuleSalesDiscount): Observable<RuleSalesDiscount> {
        const copy = this.convert(ruleSalesDiscount);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, ruleSalesDiscounts: RuleSalesDiscount[]): Observable<RuleSalesDiscount[]> {
        const copy = this.convertList(ruleSalesDiscounts);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateFrom) {
            entity.dateFrom = new Date(entity.dateFrom);
        }
        if (entity.dateThru) {
            entity.dateThru = new Date(entity.dateThru);
        }
    }

    protected convert(ruleSalesDiscount: RuleSalesDiscount): RuleSalesDiscount {
        if (ruleSalesDiscount === null || ruleSalesDiscount === {}) {
            return {};
        }
        // const copy: RuleSalesDiscount = Object.assign({}, ruleSalesDiscount);
        const copy: RuleSalesDiscount = JSON.parse(JSON.stringify(ruleSalesDiscount));

        // copy.dateFrom = this.dateUtils.toDate(ruleSalesDiscount.dateFrom);

        // copy.dateThru = this.dateUtils.toDate(ruleSalesDiscount.dateThru);
        return copy;
    }

    protected convertList(ruleSalesDiscounts: RuleSalesDiscount[]): RuleSalesDiscount[] {
        const copy: RuleSalesDiscount[] = ruleSalesDiscounts;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: RuleSalesDiscount[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
