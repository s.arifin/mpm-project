import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';
import { Principal } from '../../shared';
import {RuleSalesDiscount} from './rule-sales-discount.model';
import {RuleSalesDiscountPopupService} from './rule-sales-discount-popup.service';
import {RuleSalesDiscountService} from './rule-sales-discount.service';
import {ToasterService} from '../../shared';
import * as RULE_TYPE_CONSTANTS from '../../shared/constants/rule-type.constants';
import * as BASE_CONSTANTS from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-rule-sales-discount-dialog',
    templateUrl: './rule-sales-discount-dialog.component.html'
})
export class RuleSalesDiscountDialogComponent implements OnInit {

    ruleSalesDiscount: RuleSalesDiscount;
    isSaving: boolean;
    roles: object;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected ruleSalesDiscountService: RuleSalesDiscountService,
        protected eventManager: JhiEventManager,
        protected principalService: Principal,
        protected toaster: ToasterService
    ) {
        this.roles = BASE_CONSTANTS.AUTHORITY_BY_SEGMENT.SUBSIDI;
    }

    ngOnInit() {
        this.isSaving = false;
        if (this.ruleSalesDiscount.idRule === undefined) {
            this.initNewData();
        } else {
            this.ruleSalesDiscount.dateFrom = new Date(this.ruleSalesDiscount.dateFrom);
            this.ruleSalesDiscount.dateThru = new Date(this.ruleSalesDiscount.dateThru);
        }
    }

    protected initNewData(): void {
        const date = new Date();

        this.ruleSalesDiscount = new RuleSalesDiscount();
        this.ruleSalesDiscount.idRuleType = RULE_TYPE_CONSTANTS.DISCOUNT;
        this.ruleSalesDiscount.dateFrom = new Date(date.setHours(0, 0, 0));
        this.ruleSalesDiscount.dateThru = new Date(date.setHours(23, 59, 59));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.ruleSalesDiscount.idRule !== undefined) {
            this.subscribeToSaveResponse(
                this.ruleSalesDiscountService.update(this.ruleSalesDiscount));
        } else {
            this.ruleSalesDiscount.idInternal = this.principalService.getIdInternal();
            console.log('this.ruleSalesDiscount', this.ruleSalesDiscount);
            this.subscribeToSaveResponse(
                this.ruleSalesDiscountService.create(this.ruleSalesDiscount));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RuleSalesDiscount>) {
        result.subscribe((res: RuleSalesDiscount) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: RuleSalesDiscount) {
        this.eventManager.broadcast({ name: 'ruleSalesDiscountListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'ruleSalesDiscount saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'ruleSalesDiscount Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-rule-sales-discount-popup',
    template: ''
})
export class RuleSalesDiscountPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected ruleSalesDiscountPopupService: RuleSalesDiscountPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.ruleSalesDiscountPopupService
                    .open(RuleSalesDiscountDialogComponent as Component, params['id']);
            } else {
                this.ruleSalesDiscountPopupService
                    .open(RuleSalesDiscountDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
