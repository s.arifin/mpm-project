import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { RuleSalesDiscount } from './rule-sales-discount.model';
import { RuleSalesDiscountService } from './rule-sales-discount.service';

@Injectable()
export class RuleSalesDiscountPopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected ruleSalesDiscountService: RuleSalesDiscountService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.ruleSalesDiscountService.find(id).subscribe((data) => {
                    // if (data.dateFrom) {
                    //    data.dateFrom = this.datePipe
                    //        .transform(data.dateFrom, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    // if (data.dateThru) {
                    //    data.dateThru = this.datePipe
                    //        .transform(data.dateThru, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.ruleSalesDiscountModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new RuleSalesDiscount();
                    this.ngbModalRef = this.ruleSalesDiscountModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    ruleSalesDiscountModalRef(component: Component, ruleSalesDiscount: RuleSalesDiscount): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.ruleSalesDiscount = ruleSalesDiscount;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.ruleSalesDiscountLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    ruleSalesDiscountLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
