import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import { MpmSharedModule, JhiLanguageHelper } from '../../shared';
import { JhiLanguageService } from 'ng-jhipster';
import { customHttpProvider } from '../../blocks/interceptor/http.provider';
import { CommonModule } from '@angular/common';

import {
    UnitDeliverableService,
    UnitDeliverablePopupService,
    UnitDeliverableComponent,
    UnitDeliverableDialogComponent,
    UnitDeliverablePopupComponent,
    unitDeliverableRoute,
    unitDeliverablePopupRoute,
    UnitDeliverableEditComponent,
} from './';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule
        } from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...unitDeliverableRoute,
    ...unitDeliverablePopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,

        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule
    ],
    exports: [
        UnitDeliverableComponent,
        UnitDeliverableEditComponent
    ],
    declarations: [
        UnitDeliverableComponent,
        UnitDeliverableDialogComponent,
        UnitDeliverablePopupComponent,
        UnitDeliverableEditComponent
    ],
    entryComponents: [
        UnitDeliverableComponent,
        UnitDeliverableDialogComponent,
        UnitDeliverablePopupComponent,
        UnitDeliverableEditComponent
    ],
    providers: [
        UnitDeliverablePopupService,
        customHttpProvider(),
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmUnitDeliverableModule {
    constructor(protected languageService: JhiLanguageService, protected languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => this.languageService.changeLanguage(languageKey));
    }
}
