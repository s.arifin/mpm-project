import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {UnitDeliverable} from './unit-deliverable.model';
import {UnitDeliverablePopupService} from './unit-deliverable-popup.service';
import {UnitDeliverableService} from './unit-deliverable.service';
import {ToasterService} from '../../shared';
import { VehicleDocumentRequirement, VehicleDocumentRequirementService } from '../vehicle-document-requirement';
import { StatusType, StatusTypeService } from '../status-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-unit-deliverable-dialog',
    templateUrl: './unit-deliverable-dialog.component.html'
})
export class UnitDeliverableDialogComponent implements OnInit {

    unitDeliverable: UnitDeliverable;
    isSaving: boolean;

    vehicledocumentrequirements: VehicleDocumentRequirement[];

    statustypes: StatusType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected unitDeliverableService: UnitDeliverableService,
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected statusTypeService: StatusTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.vehicleDocumentRequirementService.query()
            .subscribe((res: ResponseWrapper) => { this.vehicledocumentrequirements = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.statusTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.statustypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.unitDeliverable.idDeliverable !== undefined) {
            this.subscribeToSaveResponse(
                this.unitDeliverableService.update(this.unitDeliverable));
        } else {
            this.subscribeToSaveResponse(
                this.unitDeliverableService.create(this.unitDeliverable));
        }
    }

    protected subscribeToSaveResponse(result: Observable<UnitDeliverable>) {
        result.subscribe((res: UnitDeliverable) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: UnitDeliverable) {
        this.eventManager.broadcast({ name: 'unitDeliverableListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'unitDeliverable saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'unitDeliverable Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackVehicleDocumentRequirementById(index: number, item: VehicleDocumentRequirement) {
        return item.idRequirement;
    }

    trackStatusTypeById(index: number, item: StatusType) {
        return item.idStatusType;
    }
}

@Component({
    selector: 'jhi-unit-deliverable-popup',
    template: ''
})
export class UnitDeliverablePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected unitDeliverablePopupService: UnitDeliverablePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.unitDeliverablePopupService
                    .open(UnitDeliverableDialogComponent as Component, params['id']);
            } else {
                this.unitDeliverablePopupService
                    .open(UnitDeliverableDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
