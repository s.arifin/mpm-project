import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';

import { UnitDeliverable } from './unit-deliverable.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { createUnitDeliverableParameterOption } from './unit-deliverable-parameter.util';
import { SummaryRefferal } from '../vehicle-document-requirement/summary-refferal.model';

@Injectable()
export class UnitDeliverableService {
    protected itemValues: UnitDeliverable[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);
    valuesSummaryRefferal: {};
    protected enheritancedata = new BehaviorSubject<any>(this.valuesSummaryRefferal);
    passingData = this.enheritancedata.asObservable();

    protected resourceUrl = SERVER_API_URL + 'api/unit-deliverables';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/unit-deliverables';
    protected resourceSearcSTNKhUrl = SERVER_API_URL + 'api/_search/unit-deliverables-stnk';
    protected resourceSearchBPKBUrl = SERVER_API_URL + 'api/_search/unit-deliverables-bpkb';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(unitDeliverable: UnitDeliverable): Observable<UnitDeliverable> {
        const copy = this.convert(unitDeliverable);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(unitDeliverable: UnitDeliverable): Observable<UnitDeliverable> {
        const copy = this.convert(unitDeliverable);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    // revisi(req?: any): Observable<any> {
    //     const options = createRequestOption(req);
    //     return this.http.get(`${this.resourceUrl}/revisi`, options).map((res: Response) => {
    //         return res.json();
    //     });
    // }

    find(id: any): Observable<UnitDeliverable> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    revisi(id: any): Observable<UnitDeliverable> {
        const url = this.resourceUrl + '/revisi';
        return this.http.get(`${url}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idDeliverableType', req.idDeliverableType);
        options.params.set('idInternal', req.idInternal);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idDeliverableType', req.idDeliverableType);
        options.params.set('idInternal', req.idInternal);
        options.params.set('idLeasing', req.idLeasing);
        return this.http.get(this.resourceUrl + '/filterleasing', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryCustom(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idDeliverableType', req.idDeliverableType);
        options.params.set('idInternal', req.idInternal);
        options.params.set('idStatusType', req.idStatusType);
        return this.http.get(this.resourceUrl + '/offtheroad', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, unitDeliverable: UnitDeliverable): Observable<UnitDeliverable> {
        const copy = this.convert(unitDeliverable);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeRevisi(id: Number, param: String, unitDeliverable: UnitDeliverable): Observable<UnitDeliverable> {
        const copy = this.convert(unitDeliverable);
        return this.http.post(`${this.resourceUrl}/revisi/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, unitDeliverables: UnitDeliverable[]): Observable<UnitDeliverable[]> {
        const copy = this.convertList(unitDeliverables);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    searchSTNK(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearcSTNKhUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    searchBPKB(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchBPKBUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateReceipt) {
            entity.dateReceipt = new Date(entity.dateReceipt);
        }
        if (entity.dateDelivery) {
            entity.dateDelivery = new Date(entity.dateDelivery);
        }
    }

    protected convert(unitDeliverable: UnitDeliverable): UnitDeliverable {
        if (unitDeliverable === null || unitDeliverable === {}) {
            return {};
        }
        // const copy: UnitDeliverable = Object.assign({}, unitDeliverable);
        const copy: UnitDeliverable = JSON.parse(JSON.stringify(unitDeliverable));

        // copy.dateReceipt = this.dateUtils.toDate(unitDeliverable.dateReceipt);

        // copy.dateDelivery = this.dateUtils.toDate(unitDeliverable.dateDelivery);
        return copy;
    }

    protected convertList(unitDeliverables: UnitDeliverable[]): UnitDeliverable[] {
        const copy: UnitDeliverable[] = unitDeliverables;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: UnitDeliverable[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    listReferences(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/list-references/`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    listReferencesLov(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/list-references-lov/`, options)
            .map((res: Response) => this.convertResponse(res));
    }
    listReferencesLov1(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/list-references-lov1/`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createUnitDeliverableParameterOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    // findRefference(req?: any): Observable<SummaryRefferal> {
    //     const options = createRequestOption(req);
    //     return this.http.get(`${this.resourceUrl}/findref/`, options)
    //         .map((res: Response) => {
    //             const jsonResponse = res.json();
    //             return jsonResponse;
    //         });
    // }

    execute(params?: any, req?: any): Observable<UnitDeliverable> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/executefind`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    queryByidReqandIdDeltype(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idDeliverableType', req.idDeliverableType);
        options.params.set('idReq', req.idReq);
        return this.http.get(this.resourceUrl + '/findByIdReqandIdDeltype/', options)
            .map((res: Response) => this.convertResponse(res));
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    setPrint(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/setprint`, options).map((res: Response) => {
            return res.json();
        });
    }

    passingCustomData(data) {
        this.enheritancedata.next(data);
        return data;
    }

}
