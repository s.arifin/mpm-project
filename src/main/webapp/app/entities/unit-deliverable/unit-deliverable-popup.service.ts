import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { UnitDeliverable } from './unit-deliverable.model';
import { UnitDeliverableService } from './unit-deliverable.service';

@Injectable()
export class UnitDeliverablePopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected unitDeliverableService: UnitDeliverableService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.unitDeliverableService.find(id).subscribe((data) => {
                    // if (data.dateReceipt) {
                    //    data.dateReceipt = this.datePipe
                    //        .transform(data.dateReceipt, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    // if (data.dateDelivery) {
                    //    data.dateDelivery = this.datePipe
                    //        .transform(data.dateDelivery, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.unitDeliverableModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new UnitDeliverable();
                    this.ngbModalRef = this.unitDeliverableModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    unitDeliverableModalRef(component: Component, unitDeliverable: UnitDeliverable): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.unitDeliverable = unitDeliverable;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.unitDeliverableLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    unitDeliverableLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
