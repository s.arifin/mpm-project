import { BaseEntity } from './../../shared';
import { VehicleDocumentRequirement } from '../vehicle-document-requirement/vehicle-document-requirement.model';

export class UnitDeliverable implements BaseEntity {
    constructor(
        public id?: any,
        public idDeliverable?: any,
        public idDeliverableType?: number,
        public description?: string,
        public dateReceipt?: any,
        public dateDelivery?: any,
        public bastNumber?: string,
        public name?: string,
        public identityNumber?: string,
        public cellPhone?: string,
        public vehicleDocumentRequirementId?: any,
        public vehicleDocumentRequirement?: VehicleDocumentRequirement,
        public vndStatusTypeId?: any,
        public conStatusTypeId?: any,
        public refNumber?: string,
        public refDate?: any,
        public receiptQty?: any,
        public receiptNominal?: number,
        public costHandling?: any,
        public costOther?: any,
        public fincoyName?: any,
        public dateBastSales?: any,

        public platNomor?: any,
        public notice?: any,
        public stnk?: any,
        public bpkb?: any,
        public bpkbNumber?: any,

        public idframe?: any,
        public idmachine?: any,
        public requestpoliceid?: any,
        public customerName?: any,
        public stnkNama?: any,
        public delivStnk?: any,
        public delivNotice?: any,
        public delivPlatNomor?: any,
        public salesName?: any,
        public ivuNumber?: any,
        public vendorName?: any,
    ) {
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
    }
}
