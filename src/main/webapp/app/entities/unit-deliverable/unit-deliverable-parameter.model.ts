export class UnitDeliverableParameters {
    constructor(
        public status?: Array<Number>,
        public statusNotIn?: Array<Number>,
        public orderDateFrom?: any,
        public orderDateThru?: any,
        public leasingCompanyId?: String,
        public coordinatorSalesId?: String,
        public salesmanId?: String,
        public unitStatus?: Number,
        public internalId?: String,
        public idDeliverableType?: Number,
        public dataParam?: String
    ) {
        this.status = [];
        this.statusNotIn = [];
        this.orderDateFrom = null;
        this.orderDateThru = null;
        this.leasingCompanyId = null;
        this.coordinatorSalesId = null;
        this.salesmanId = null;
        this.unitStatus = null;
        this.internalId = null;
        this.idDeliverableType = null;
        this.dataParam = null;
    }
}
