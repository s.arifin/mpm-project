import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UnitDeliverable } from './unit-deliverable.model';
import { UnitDeliverableService } from './unit-deliverable.service';
import { ToasterService} from '../../shared';
import { VehicleDocumentRequirement, VehicleDocumentRequirementService } from '../vehicle-document-requirement';
import { StatusType, StatusTypeService } from '../status-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-unit-deliverable-edit',
    templateUrl: './unit-deliverable-edit.component.html'
})
export class UnitDeliverableEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    unitDeliverable: UnitDeliverable;
    isSaving: boolean;

    vehicledocumentrequirements: VehicleDocumentRequirement[];

    statustypes: StatusType[];

    constructor(
        protected alertService: JhiAlertService,
        protected unitDeliverableService: UnitDeliverableService,
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected statusTypeService: StatusTypeService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.unitDeliverable = new UnitDeliverable();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.vehicleDocumentRequirementService.query()
            .subscribe((res: ResponseWrapper) => { this.vehicledocumentrequirements = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.statusTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.statustypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.unitDeliverableService.find(id).subscribe((unitDeliverable) => {
            this.unitDeliverable = unitDeliverable;
        });
    }

    previousState() {
        this.router.navigate(['unit-deliverable']);
    }

    save() {
        this.isSaving = true;
        if (this.unitDeliverable.idDeliverable !== undefined) {
            this.subscribeToSaveResponse(
                this.unitDeliverableService.update(this.unitDeliverable));
        } else {
            this.subscribeToSaveResponse(
                this.unitDeliverableService.create(this.unitDeliverable));
        }
    }

    protected subscribeToSaveResponse(result: Observable<UnitDeliverable>) {
        result.subscribe((res: UnitDeliverable) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: UnitDeliverable) {
        this.eventManager.broadcast({ name: 'unitDeliverableListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'unitDeliverable saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'unitDeliverable Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackVehicleDocumentRequirementById(index: number, item: VehicleDocumentRequirement) {
        return item.idRequirement;
    }

    trackStatusTypeById(index: number, item: StatusType) {
        return item.idStatusType;
    }
}
