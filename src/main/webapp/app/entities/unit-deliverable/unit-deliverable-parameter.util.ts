import { URLSearchParams, BaseRequestOptions } from '@angular/http';

export const createUnitDeliverableParameterOption = (req?: any): BaseRequestOptions => {
    const options: BaseRequestOptions = new BaseRequestOptions();
    if (req) {
        const params: URLSearchParams = new URLSearchParams();
        params.set('page', req.page);
        params.set('size', req.size);
        if (req.sort) {
            params.paramsMap.set('sort', req.sort);
        }
        params.set('query', req.query);

        params.set('status', req.unitDeliverablePTO.status);
        params.set('statusNotIn', req.unitDeliverablePTO.statusNotIn);
        params.set('orderDateFrom', req.unitDeliverablePTO.orderDateFrom);
        params.set('orderDateThru', req.unitDeliverablePTO.orderDateThru);
        params.set('leasingCompanyId', req.unitDeliverablePTO.leasingCompanyId);
        params.set('coordinatorSalesId', req.unitDeliverablePTO.coordinatorSalesId);
        params.set('salesmanId', req.unitDeliverablePTO.salesmanId);
        params.set('internalId', req.unitDeliverablePTO.internalId);
        params.set('idDeliverableType', req.unitDeliverablePTO.idDeliverableType);
        params.set('dataParam' , req.unitDeliverablePTO.dataParam);

        options.params = params;
    }
    return options;
}
