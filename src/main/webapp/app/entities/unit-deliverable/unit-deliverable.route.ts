import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UnitDeliverableComponent } from './unit-deliverable.component';
import { UnitDeliverableEditComponent } from './unit-deliverable-edit.component';
import { UnitDeliverablePopupComponent } from './unit-deliverable-dialog.component';
import { UnitDeliverableResolvePagingParams } from '../shared-component/services/share-paging-params';

export const unitDeliverableRoute: Routes = [
    {
        path: '',
        component: UnitDeliverableComponent,
        resolve: {
            'pagingParams': UnitDeliverableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitDeliverable.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const unitDeliverablePopupRoute: Routes = [
    {
        path: 'new',
        component: UnitDeliverableEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitDeliverable.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: UnitDeliverableEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitDeliverable.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'popup-new',
        component: UnitDeliverablePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitDeliverable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: ':id/popup-edit',
        component: UnitDeliverablePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitDeliverable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
