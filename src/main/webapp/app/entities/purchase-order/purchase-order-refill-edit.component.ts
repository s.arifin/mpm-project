import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PurchaseOrder } from './purchase-order.model';
import { PurchaseOrderService } from './purchase-order.service';
import { ToasterService} from '../../shared';
import { Vendor, VendorService } from '../vendor';
import { Internal, InternalService } from '../internal';
import { OrderType, OrderTypeService } from '../order-type';
import { StatusType, StatusTypeService } from '../status-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-purchase-order-refill-edit',
    templateUrl: './purchase-order-refill-edit.component.html'
})
export class PurchaseOrderRefillEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    purchaseOrder: PurchaseOrder;
    isSaving: boolean;

    vendors: Vendor[];

    internals: Internal[];

    orderTypes: OrderType[];

    statusTypes: StatusType[];

    purchaseOrders: PurchaseOrder[];
    itemsPerPage: any;
    totalItems: any;

    constructor(
        protected alertService: JhiAlertService,
        protected purchaseOrderService: PurchaseOrderService,
        protected vendorService: VendorService,
        protected internalService: InternalService,
        protected orderTypeService: OrderTypeService,
        protected statusTypeService: StatusTypeService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.purchaseOrder = new PurchaseOrder();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.vendorService.query()
            .subscribe((res: ResponseWrapper) => { this.vendors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.orderTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.orderTypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.statusTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.statusTypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.purchaseOrderService.find(id).subscribe((purchaseOrder) => {
            if (purchaseOrder.dateOrder) {
                purchaseOrder.dateOrder = new Date(purchaseOrder.dateOrder);
            }
            this.purchaseOrder = purchaseOrder;
        });
    }

    previousState() {
        this.router.navigate(['/purchase-order']);
    }

    save() {
        this.isSaving = true;
        if (this.purchaseOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.purchaseOrderService.update(this.purchaseOrder));
        } else {
            this.subscribeToSaveResponse(
                this.purchaseOrderService.create(this.purchaseOrder));
        }
    }

    cancel() {
        this.eventManager.broadcast({ name: 'purchaseOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'PO Canceled !');
    }

    protected subscribeToSaveResponse(result: Observable<PurchaseOrder>) {
        result.subscribe((res: PurchaseOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: PurchaseOrder) {
        this.eventManager.broadcast({ name: 'purchaseOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'purchaseOrder saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'purchaseOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackVendorById(index: number, item: Vendor) {
        return item.idVendor;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackOrderTypeById(index: number, item: OrderType) {
        return item.idOrderType;
    }

    trackStatusTypeById(index: number, item: StatusType) {
        return item.idStatusType;
    }

    suggestParts() {
        console.log('Suggesting parts... ');
    }
}
