import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PurchaseOrder } from './purchase-order.model';
import { PurchaseOrderService } from './purchase-order.service';
import { ToasterService} from '../../shared';
import { Vendor, VendorService } from '../vendor';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-purchase-order-edit',
    templateUrl: './purchase-order-edit.component.html'
})
export class PurchaseOrderEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    purchaseOrder: PurchaseOrder;
    isSaving: boolean;
    idOrder: any;
    paramPage: number;
    routeId: number;

    vendors: Vendor[];

    internals: Internal[];

    billtos: BillTo[];

    constructor(
        protected alertService: JhiAlertService,
        protected purchaseOrderService: PurchaseOrderService,
        protected vendorService: VendorService,
        protected internalService: InternalService,
        protected billToService: BillToService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.purchaseOrder = new PurchaseOrder();
        this.routeId = 0;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idOrder = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        this.vendorService.query()
            .subscribe((res: ResponseWrapper) => { this.vendors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.purchaseOrderService.find(this.idOrder).subscribe((purchaseOrder) => {
            this.purchaseOrder = purchaseOrder;
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['purchase-order', { page: this.paramPage }]);
        }
    }

    save() {
        this.isSaving = true;
        if (this.purchaseOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.purchaseOrderService.update(this.purchaseOrder));
        } else {
            this.subscribeToSaveResponse(
                this.purchaseOrderService.create(this.purchaseOrder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PurchaseOrder>) {
        result.subscribe((res: PurchaseOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: PurchaseOrder) {
        this.eventManager.broadcast({ name: 'purchaseOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'purchaseOrder saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'purchaseOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackVendorById(index: number, item: Vendor) {
        return item.idVendor;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}
