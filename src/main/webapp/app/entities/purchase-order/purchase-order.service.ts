import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { PurchaseOrder } from './purchase-order.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PurchaseOrderService {
    protected itemValues: PurchaseOrder[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/purchase-orders';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/purchase-orders';

    constructor(protected http: Http) { }

    create(purchaseOrder: PurchaseOrder): Observable<PurchaseOrder> {
        const copy = this.convert(purchaseOrder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(purchaseOrder: PurchaseOrder): Observable<PurchaseOrder> {
        const copy = this.convert(purchaseOrder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<PurchaseOrder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(purchaseOrder: PurchaseOrder, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(purchaseOrder);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<PurchaseOrder> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<PurchaseOrder[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PurchaseOrder.
     */
    protected convertItemFromServer(json: any): PurchaseOrder {
        const entity: PurchaseOrder = Object.assign(new PurchaseOrder(), json);
        return entity;
    }

    /**
     * Convert a PurchaseOrder to a JSON which can be sent to the server.
     */
    protected convert(purchaseOrder: PurchaseOrder): PurchaseOrder {
        if (purchaseOrder === null || purchaseOrder === {}) {
            return {};
        }
        // const copy: PurchaseOrder = Object.assign({}, purchaseOrder);
        const copy: PurchaseOrder = JSON.parse(JSON.stringify(purchaseOrder));
        return copy;
    }

    protected convertList(purchaseOrders: PurchaseOrder[]): PurchaseOrder[] {
        const copy: PurchaseOrder[] = purchaseOrders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: PurchaseOrder[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
