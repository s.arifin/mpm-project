import { BaseEntity } from './../../shared';

export class PurchaseOrder implements BaseEntity {
    constructor(
        public id?: any,
        public idOrder?: any,
        public vendorId?: any,
        public internalId?: any,
        public billToId?: any,
        public dateOrder?: any,
    ) {
    }
}
