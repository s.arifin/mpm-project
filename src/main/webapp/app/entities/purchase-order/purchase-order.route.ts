import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PurchaseOrderComponent } from './purchase-order.component';
import { PurchaseOrderEditComponent } from './purchase-order-edit.component';
import { PurchaseOrderPopupComponent } from './purchase-order-dialog.component';

@Injectable()
export class PurchaseOrderResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'orderNumber,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const purchaseOrderRoute: Routes = [
    {
        path: '',
        component: PurchaseOrderComponent,
        resolve: {
            'pagingParams': PurchaseOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const purchaseOrderPopupRoute: Routes = [
    {
        path: 'popup-new',
        component: PurchaseOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'new',
        component: PurchaseOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: PurchaseOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':route/:page/:id/edit',
        component: PurchaseOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/popup-edit',
        component: PurchaseOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.purchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
