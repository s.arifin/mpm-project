import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserMediatorComponent } from './user-mediator.component';
import { UserMediatorPopupComponent } from './user-mediator-dialog.component';

@Injectable()
export class UserMediatorResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'userName,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const userMediatorRoute: Routes = [
    {
        path: 'user-mediator',
        component: UserMediatorComponent,
        resolve: {
            'pagingParams': UserMediatorResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.userMediator.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const userMediatorPopupRoute: Routes = [
    {
        path: 'user-mediator-new',
        component: UserMediatorPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.userMediator.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'user-mediator/:id/edit',
        component: UserMediatorPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.userMediator.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
