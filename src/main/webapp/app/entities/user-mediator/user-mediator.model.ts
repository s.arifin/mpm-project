import { BaseEntity } from './../shared-component';
import { Person } from './../person';

export class UserMediator implements BaseEntity {
    constructor(
        public id?: any,
        public idUserMediator?: any,
        public userName?: string,
        public email?: string,
        public internalId?: any,
        public person?: Person,
        public positions?: any,
    ) {
        this.person = new Person();
    }
}

export class UserMediatorNet {
    constructor(
        public id?: any,
        public idUserMediator?: any,
        public name?: string,
        public username?: string,
        public email?: string,
        public idinternal?: any,
        public idperson?: any,
        public positions?: any,
        public idusrmed?: any,
    ) {}
}
