import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { UserMediator } from './user-mediator.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UserMediatorService {
    protected itemValues: UserMediator[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/user-mediators';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/user-mediators';
    protected resourceCUrl = process.env.API_C_URL + '/api/user_mediator';

    constructor(protected http: Http) { }

    create(userMediator: UserMediator): Observable<UserMediator> {
        const copy = this.convert(userMediator);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(userMediator: UserMediator): Observable<UserMediator> {
        const copy = this.convert(userMediator);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<UserMediator> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryC(idInternal?: string): Observable<ResponseWrapper> {
        const options = new RequestOptions();
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');

        return this.http.get(this.resourceCUrl + '/getbyinternalid/?idinternal=' + idInternal, options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllByInternal(idinternal?: string): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrl + '/stockopname/by-idinternal-dropdown/' + idinternal)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(userMediator: UserMediator, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(userMediator);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, userMediator: UserMediator): Observable<UserMediator> {
        const copy = this.convert(userMediator);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, userMediators: UserMediator[]): Observable<UserMediator[]> {
        const copy = this.convertList(userMediators);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to UserMediator.
     */
    protected convertItemFromServer(json: any): UserMediator {
        const entity: UserMediator = Object.assign(new UserMediator(), json);
        return entity;
    }

    /**
     * Convert a UserMediator to a JSON which can be sent to the server.
     */
    protected convert(userMediator: UserMediator): UserMediator {
        if (userMediator === null || userMediator === {}) {
            return {};
        }
        // const copy: UserMediator = Object.assign({}, userMediator);
        const copy: UserMediator = JSON.parse(JSON.stringify(userMediator));
        return copy;
    }

    protected convertList(userMediators: UserMediator[]): UserMediator[] {
        const copy: UserMediator[] = userMediators;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: UserMediator[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
