import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UserMediator } from './user-mediator.model';
import { UserMediatorPopupService } from './user-mediator-popup.service';
import { UserMediatorService } from './user-mediator.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { Person, PersonService } from '../person';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-user-mediator-dialog',
    templateUrl: './user-mediator-dialog.component.html'
})
export class UserMediatorDialogComponent implements OnInit {

    userMediator: UserMediator;
    isSaving: boolean;
    internals: Internal[];
    people: Person[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected userMediatorService: UserMediatorService,
        protected internalService: InternalService,
        protected personService: PersonService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query({
            size: 1000
        })
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; console.log('zabata=>', this.internals); }, (res: ResponseWrapper) => this.onError(res.json));
        this.personService.query()
            .subscribe((res: ResponseWrapper) => { this.people = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.userMediator.idUserMediator !== undefined) {
            this.subscribeToSaveResponse(
                this.userMediatorService.update(this.userMediator));
        } else {
            this.subscribeToSaveResponse(
                this.userMediatorService.create(this.userMediator));
        }
    }

    protected subscribeToSaveResponse(result: Observable<UserMediator>) {
        result.subscribe((res: UserMediator) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: UserMediator) {
        this.eventManager.broadcast({ name: 'userMediatorListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'userMediator saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'userMediator Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackPersonById(index: number, item: Person) {
        return item.idParty;
    }
}

@Component({
    selector: 'jhi-user-mediator-popup',
    template: ''
})
export class UserMediatorPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected userMediatorPopupService: UserMediatorPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.userMediatorPopupService
                    .open(UserMediatorDialogComponent as Component, params['id']);
            } else {
                this.userMediatorPopupService
                    .open(UserMediatorDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
