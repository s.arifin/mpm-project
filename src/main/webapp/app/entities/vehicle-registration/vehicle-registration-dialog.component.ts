import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { VehicleRegistration } from './vehicle-registration.model';
import { VehicleRegistrationPopupService } from './vehicle-registration-popup.service';
import { VehicleRegistrationService } from './vehicle-registration.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { Vendor, VendorService } from '../vendor';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-vehicle-registration-dialog',
    templateUrl: './vehicle-registration-dialog.component.html'
})
export class VehicleRegistrationDialogComponent implements OnInit {

    vehicleRegistration: VehicleRegistration;
    isSaving: boolean;
    idInternal: any;
    idBillTo: any;
    idVendor: any;

    internals: Internal[];

    billtos: BillTo[];

    vendors: Vendor[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected vehicleRegistrationService: VehicleRegistrationService,
        protected internalService: InternalService,
        protected billToService: BillToService,
        protected vendorService: VendorService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected router: Router
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.vendorService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.vendors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.vehicleRegistration.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.vehicleRegistrationService.update(this.vehicleRegistration));
        } else {
            this.subscribeToSaveResponse(
                this.vehicleRegistrationService.create(this.vehicleRegistration));
        }
    }

    protected subscribeToSaveResponse(result: Observable<VehicleRegistration>) {
        result.subscribe((res: VehicleRegistration) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: VehicleRegistration) {
        this.eventManager.broadcast({ name: 'vehicleRegistrationListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vehicleRegistration saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'vehicleRegistration Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }

    trackVendorById(index: number, item: Vendor) {
        return item.idVendor;
    }
}

@Component({
    selector: 'jhi-vehicle-registration-popup',
    template: ''
})
export class VehicleRegistrationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected vehicleRegistrationPopupService: VehicleRegistrationPopupService
    ) {}

    ngOnInit() {
        this.vehicleRegistrationPopupService.idInternal = undefined;
        this.vehicleRegistrationPopupService.idBillTo = undefined;
        this.vehicleRegistrationPopupService.idVendor = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vehicleRegistrationPopupService
                    .open(VehicleRegistrationDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.vehicleRegistrationPopupService.parent = params['parent'];
                this.vehicleRegistrationPopupService
                    .open(VehicleRegistrationDialogComponent as Component);
            } else {
                this.vehicleRegistrationPopupService
                    .open(VehicleRegistrationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
