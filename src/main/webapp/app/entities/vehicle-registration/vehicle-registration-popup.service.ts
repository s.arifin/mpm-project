import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { VehicleRegistration } from './vehicle-registration.model';
import { VehicleRegistrationService } from './vehicle-registration.service';

@Injectable()
export class VehicleRegistrationPopupService {
    protected ngbModalRef: NgbModalRef;
    idInternal: any;
    idBillTo: any;
    idVendor: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected vehicleRegistrationService: VehicleRegistrationService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.vehicleRegistrationService.find(id).subscribe((data) => {
                    // if (data.dateCreate) {
                    //    data.dateCreate = this.datePipe
                    //        .transform(data.dateCreate, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    // if (data.dateRequired) {
                    //    data.dateRequired = this.datePipe
                    //        .transform(data.dateRequired, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.vehicleRegistrationModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new VehicleRegistration();
                    data.internalId = this.idInternal;
                    data.billToId = this.idBillTo;
                    data.vendorId = this.idVendor;
                    this.ngbModalRef = this.vehicleRegistrationModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    vehicleRegistrationModalRef(component: Component, vehicleRegistration: VehicleRegistration): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.vehicleRegistration = vehicleRegistration;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idBillTo = this.idBillTo;
        modalRef.componentInstance.idVendor = this.idVendor;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.vehicleRegistrationLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    vehicleRegistrationLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idBillTo = this.idBillTo;
        modalRef.componentInstance.idVendor = this.idVendor;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
