import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule, JhiLanguageHelper } from '../../shared';
import { JhiLanguageService } from 'ng-jhipster';
import {
    VehicleRegistrationService,
    VehicleRegistrationPopupService,
    VehicleRegistrationComponent,
    VehicleRegistrationDialogComponent,
    VehicleRegistrationPopupComponent,
    vehicleRegistrationRoute,
    vehicleRegistrationPopupRoute,
    VehicleRegistrationResolvePagingParams,
} from './';

import { CurrencyMaskModule } from 'ng2-currency-mask';
import { AutoCompleteModule,
         InputTextModule,
         InputTextareaModule,
         DataScrollerModule,
         PaginatorModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         FieldsetModule,
         ScheduleModule,
         ChartModule,
         DragDropModule,
         LightboxModule
} from 'primeng/primeng';

import { DataTableModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { StepsModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ButtonModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { MpmSharedEntityModule } from '../shared-entity.module';
import { customHttpProvider } from '../../blocks/interceptor/http.provider';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const ENTITY_STATES = [
    ...vehicleRegistrationRoute,
    ...vehicleRegistrationPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        MpmSharedEntityModule,
        NgbModule,
        RouterModule.forChild(ENTITY_STATES),
        InputTextareaModule,
        ButtonModule,
        DataListModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        DataTableModule,
        StepsModule,
        SliderModule,
        RadioButtonModule
    ],
    exports: [
        VehicleRegistrationComponent,
    ],
    declarations: [
        VehicleRegistrationComponent,
        VehicleRegistrationDialogComponent,
        VehicleRegistrationPopupComponent,
    ],
    entryComponents: [
        VehicleRegistrationComponent,
        VehicleRegistrationDialogComponent,
        VehicleRegistrationPopupComponent,
    ],
    providers: [
        VehicleRegistrationPopupService,
        VehicleRegistrationResolvePagingParams,
        customHttpProvider(),
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmVehicleRegistrationModule {
    constructor(protected languageService: JhiLanguageService, protected languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => this.languageService.changeLanguage(languageKey));
    }
}
