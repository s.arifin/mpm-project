import { BaseEntity } from './../../shared';

export class VehicleRegistration implements BaseEntity {
    constructor(
        public id?: any,
        public idRequirement?: any,
        public currentStatus?: number,
        public requirementNumber?: string,
        public description?: string,
        public dateCreate?: Date,
        public dateRequired?: Date,
        public note?: string,
        public bbn?: number,
        public otherCost?: number,
        public internalId?: string,
        public internalName?: string,
        public billToId?: string,
        public billToName?: string,
        public vendorId?: string,
        public vendorName?: string,
    ) {
    }
}
