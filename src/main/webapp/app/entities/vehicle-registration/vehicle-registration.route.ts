import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { VehicleRegistrationComponent } from './vehicle-registration.component';
import { VehicleRegistrationPopupComponent } from './vehicle-registration-dialog.component';

@Injectable()
export class VehicleRegistrationResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequirement,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const vehicleRegistrationRoute: Routes = [
    {
        path: '',
        component: VehicleRegistrationComponent,
        resolve: {
            'pagingParams': VehicleRegistrationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleRegistration.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'main',
        component: VehicleRegistrationComponent,
        resolve: {
            'pagingParams': VehicleRegistrationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleRegistration.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const vehicleRegistrationPopupRoute: Routes = [
    {
        path: 'new',
        component: VehicleRegistrationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleRegistration.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: ':id/edit',
        component: VehicleRegistrationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleRegistration.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
