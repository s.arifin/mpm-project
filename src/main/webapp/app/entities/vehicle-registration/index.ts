export * from './vehicle-registration.model';
export * from './vehicle-registration-popup.service';
export * from './vehicle-registration.service';
export * from './vehicle-registration-dialog.component';
export * from './vehicle-registration.component';
export * from './vehicle-registration.route';
