import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';
import { VehicleRegistration } from './vehicle-registration.model';
import { AbstractEntityService } from '../../shared/base/abstract-entity.service';

@Injectable()
export class VehicleRegistrationService extends AbstractEntityService<VehicleRegistration> {

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) {
        super(http, dateUtils);
        this.resourceUrl =  SERVER_API_URL + 'api/vehicle-registrations';
        this.resourceSearchUrl = SERVER_API_URL + 'api/_search/vehicle-registrations';
   }

   protected convertItemFromServer(vehicleRegistration: VehicleRegistration): VehicleRegistration {
       const copy: VehicleRegistration = Object.assign({}, vehicleRegistration);
       copy.dateCreate = new Date(vehicleRegistration.dateCreate);
       copy.dateRequired = new Date(vehicleRegistration.dateRequired);
       return copy;
   }

}
