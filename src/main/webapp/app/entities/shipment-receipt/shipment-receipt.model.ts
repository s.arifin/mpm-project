import { BaseEntity } from './../../shared';

export class ShipmentReceipt implements BaseEntity {
    constructor(
        public id?: any,
        public idReceipt?: any,
        public idProduct?: string,
        public idFeature?: number,
        public featureRefKey?: number,
        public featureDescriptionription?: number,
        public receiptCode?: string,
        public qtyAccept?: number,
        public qtyReject?: number,
        public itemDescription?: string,
        public dateReceipt?: any,
        public shipmentPackageId?: any,
        public shipmentItemId?: any,
        public orderItemId?: any,
        public inventoryItems?: any,
    ) {
    }
}
