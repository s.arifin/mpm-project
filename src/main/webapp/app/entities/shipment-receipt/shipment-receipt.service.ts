import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ShipmentReceipt } from './shipment-receipt.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ShipmentReceiptService {
    protected itemValues: ShipmentReceipt[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/shipment-receipts';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/shipment-receipts';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(shipmentReceipt: ShipmentReceipt): Observable<ShipmentReceipt> {
        const copy = this.convert(shipmentReceipt);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(shipmentReceipt: ShipmentReceipt): Observable<ShipmentReceipt> {
        const copy = this.convert(shipmentReceipt);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<ShipmentReceipt> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(shipmentReceipt: ShipmentReceipt, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(shipmentReceipt);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<ShipmentReceipt> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<ShipmentReceipt[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ShipmentReceipt.
     */
    protected convertItemFromServer(json: any): ShipmentReceipt {
        const entity: ShipmentReceipt = Object.assign(new ShipmentReceipt(), json);
        if (entity.dateReceipt) {
            entity.dateReceipt = new Date(entity.dateReceipt);
        }
        return entity;
    }

    /**
     * Convert a ShipmentReceipt to a JSON which can be sent to the server.
     */
    protected convert(shipmentReceipt: ShipmentReceipt): ShipmentReceipt {
        if (shipmentReceipt === null || shipmentReceipt === {}) {
            return {};
        }
        // const copy: ShipmentReceipt = Object.assign({}, shipmentReceipt);
        const copy: ShipmentReceipt = JSON.parse(JSON.stringify(shipmentReceipt));

        // copy.dateReceipt = this.dateUtils.toDate(shipmentReceipt.dateReceipt);
        return copy;
    }

    protected convertList(shipmentReceipts: ShipmentReceipt[]): ShipmentReceipt[] {
        const copy: ShipmentReceipt[] = shipmentReceipts;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ShipmentReceipt[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
