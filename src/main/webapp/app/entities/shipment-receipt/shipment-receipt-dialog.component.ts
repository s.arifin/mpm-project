import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ShipmentReceipt } from './shipment-receipt.model';
import { ShipmentReceiptPopupService } from './shipment-receipt-popup.service';
import { ShipmentReceiptService } from './shipment-receipt.service';
import { ToasterService } from '../../shared';
import { ShipmentPackage, ShipmentPackageService } from '../shipment-package';
import { ShipmentItem, ShipmentItemService } from '../shipment-item';
import { OrderItem, OrderItemService } from '../order-item';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-shipment-receipt-dialog',
    templateUrl: './shipment-receipt-dialog.component.html'
})
export class ShipmentReceiptDialogComponent implements OnInit {

    shipmentReceipt: ShipmentReceipt;
    isSaving: boolean;
    idShipmentPackage: any;
    idShipmentItem: any;
    idOrderItem: any;

    shipmentpackages: ShipmentPackage[];

    shipmentitems: ShipmentItem[];

    orderitems: OrderItem[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected shipmentReceiptService: ShipmentReceiptService,
        protected shipmentPackageService: ShipmentPackageService,
        protected shipmentItemService: ShipmentItemService,
        protected orderItemService: OrderItemService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.shipmentPackageService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmentpackages = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipmentItemService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmentitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.orderItemService.query()
            .subscribe((res: ResponseWrapper) => { this.orderitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.shipmentReceipt.idReceipt !== undefined) {
            this.subscribeToSaveResponse(
                this.shipmentReceiptService.update(this.shipmentReceipt));
        } else {
            this.subscribeToSaveResponse(
                this.shipmentReceiptService.create(this.shipmentReceipt));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ShipmentReceipt>) {
        result.subscribe((res: ShipmentReceipt) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ShipmentReceipt) {
        this.eventManager.broadcast({ name: 'shipmentReceiptListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'shipmentReceipt saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'shipmentReceipt Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackShipmentPackageById(index: number, item: ShipmentPackage) {
        return item.idPackage;
    }

    trackShipmentItemById(index: number, item: ShipmentItem) {
        return item.idShipmentItem;
    }

    trackOrderItemById(index: number, item: OrderItem) {
        return item.idOrderItem;
    }
}

@Component({
    selector: 'jhi-shipment-receipt-popup',
    template: ''
})
export class ShipmentReceiptPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected shipmentReceiptPopupService: ShipmentReceiptPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.shipmentReceiptPopupService
                    .open(ShipmentReceiptDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.shipmentReceiptPopupService.idShipmentPackage = params['parent'];
                this.shipmentReceiptPopupService
                    .open(ShipmentReceiptDialogComponent as Component);
            } else {
                this.shipmentReceiptPopupService
                    .open(ShipmentReceiptDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
