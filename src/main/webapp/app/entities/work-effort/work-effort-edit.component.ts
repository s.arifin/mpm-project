import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { WorkEffort } from './work-effort.model';
import { WorkEffortService } from './work-effort.service';
import { ToasterService} from '../../shared';
import { PaymentApplication, PaymentApplicationService } from '../payment-application';
import { Facility, FacilityService } from '../facility';
import { WorkEffortType, WorkEffortTypeService } from '../work-effort-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-work-effort-edit',
    templateUrl: './work-effort-edit.component.html'
})
export class WorkEffortEditComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    workEffort: WorkEffort;
    isSaving: boolean;

    paymentapplications: PaymentApplication[];

    facilities: Facility[];

    workefforttypes: WorkEffortType[];

    constructor(
        private alertService: JhiAlertService,
        private workEffortService: WorkEffortService,
        private paymentApplicationService: PaymentApplicationService,
        private facilityService: FacilityService,
        private workEffortTypeService: WorkEffortTypeService,
        private route: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
        this.workEffort = new WorkEffort();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.paymentApplicationService.query()
            .subscribe((res: ResponseWrapper) => { this.paymentapplications = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.workEffortTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.workefforttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.workEffortService.find(id).subscribe((workEffort) => {
            this.workEffort = workEffort;
        });
    }

    previousState() {
        this.router.navigate(['work-effort']);
    }

    save() {
        this.isSaving = true;
        if (this.workEffort.idWe !== undefined) {
            this.subscribeToSaveResponse(
                this.workEffortService.update(this.workEffort));
        } else {
            this.subscribeToSaveResponse(
                this.workEffortService.create(this.workEffort));
        }
    }

    private subscribeToSaveResponse(result: Observable<WorkEffort>) {
        result.subscribe((res: WorkEffort) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: WorkEffort) {
        this.eventManager.broadcast({ name: 'workEffortListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'workEffort saved !');
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'workEffort Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackPaymentApplicationById(index: number, item: PaymentApplication) {
        return item.idPaymentApplication;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackWorkEffortTypeById(index: number, item: WorkEffortType) {
        return item.idWeType;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
