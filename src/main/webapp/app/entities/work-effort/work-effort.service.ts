import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { WorkEffort } from './work-effort.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class WorkEffortService {
    private itemValues: WorkEffort[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = 'api/work-efforts';
    private resourceSearchUrl = 'api/_search/work-efforts';

    constructor(private http: Http) { }

    create(workEffort: WorkEffort): Observable<WorkEffort> {
        const copy = this.convert(workEffort);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(workEffort: WorkEffort): Observable<WorkEffort> {
        const copy = this.convert(workEffort);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<WorkEffort> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(workEffort: WorkEffort, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(workEffort);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, workEffort: WorkEffort): Observable<WorkEffort> {
        const copy = this.convert(workEffort);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, workEfforts: WorkEffort[]): Observable<WorkEffort[]> {
        const copy = this.convertList(workEfforts);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(workEffort: WorkEffort): WorkEffort {
        if (workEffort === null || workEffort === {}) {
            return {};
        }
        // const copy: WorkEffort = Object.assign({}, workEffort);
        const copy: WorkEffort = JSON.parse(JSON.stringify(workEffort));
        return copy;
    }

    private convertList(workEfforts: WorkEffort[]): WorkEffort[] {
        const copy: WorkEffort[] = workEfforts;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: WorkEffort[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
