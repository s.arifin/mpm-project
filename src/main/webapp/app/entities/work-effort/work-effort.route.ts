import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { WorkEffortComponent } from './work-effort.component';
import { WorkEffortEditComponent } from './work-effort-edit.component';
import { WorkEffortPopupComponent } from './work-effort-dialog.component';

@Injectable()
export class WorkEffortResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idWe,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const workEffortRoute: Routes = [
    {
        path: 'work-effort',
        component: WorkEffortComponent,
        resolve: {
            'pagingParams': WorkEffortResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workEffort.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const workEffortPopupRoute: Routes = [
    {
        path: 'work-effort-new',
        component: WorkEffortEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workEffort.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'work-effort/:id/edit',
        component: WorkEffortEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workEffort.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'work-effort-popup-new',
        component: WorkEffortPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workEffort.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'work-effort/:id/popup-edit',
        component: WorkEffortPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.workEffort.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
