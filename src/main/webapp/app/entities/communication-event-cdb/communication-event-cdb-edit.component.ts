import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CommunicationEventCDB } from './communication-event-cdb.model';
import { CommunicationEventCDBService } from './communication-event-cdb.service';
import { ToasterService} from '../../shared';

@Component({
    selector: 'jhi-communication-event-cdb-edit',
    templateUrl: './communication-event-cdb-edit.component.html'
})
export class CommunicationEventCDBEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    communicationEventCDB: CommunicationEventCDB;
    isSaving: boolean;

    constructor(
        protected alertService: JhiAlertService,
        protected communicationEventCDBService: CommunicationEventCDBService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.communicationEventCDB = new CommunicationEventCDB();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.communicationEventCDBService.find(id).subscribe((communicationEventCDB) => {
            this.communicationEventCDB = communicationEventCDB;
        });
    }

    previousState() {
        this.router.navigate(['communication-event-cdb']);
    }

    save() {
        this.isSaving = true;
        if (this.communicationEventCDB.idCommunicationEvent !== undefined) {
            this.subscribeToSaveResponse(
                this.communicationEventCDBService.update(this.communicationEventCDB));
        } else {
            this.subscribeToSaveResponse(
                this.communicationEventCDBService.create(this.communicationEventCDB));
        }
    }

    protected subscribeToSaveResponse(result: Observable<CommunicationEventCDB>) {
        result.subscribe((res: CommunicationEventCDB) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: CommunicationEventCDB) {
        this.eventManager.broadcast({ name: 'communicationEventCDBListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'communicationEventCDB saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'communicationEventCDB Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}
