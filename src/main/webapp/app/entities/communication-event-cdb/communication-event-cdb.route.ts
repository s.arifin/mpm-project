import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CommunicationEventCDBComponent } from './communication-event-cdb.component';
import { CommunicationEventCDBEditComponent } from './communication-event-cdb-edit.component';
import { CommunicationEventCDBPopupComponent } from './communication-event-cdb-dialog.component';

@Injectable()
export class CommunicationEventCDBResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idCommunicationEvent,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const communicationEventCDBRoute: Routes = [
    {
        path: 'communication-event-cdb',
        component: CommunicationEventCDBComponent,
        resolve: {
            'pagingParams': CommunicationEventCDBResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventCDB.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const communicationEventCDBPopupRoute: Routes = [
    {
        path: 'communication-event-cdb-new',
        component: CommunicationEventCDBEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventCDB.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'communication-event-cdb/:id/edit',
        component: CommunicationEventCDBEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventCDB.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'communication-event-cdb-popup-new',
        component: CommunicationEventCDBPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventCDB.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'communication-event-cdb/:id/popup-edit',
        component: CommunicationEventCDBPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEventCDB.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
