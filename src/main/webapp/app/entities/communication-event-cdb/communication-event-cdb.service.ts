import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { CommunicationEventCDB } from './communication-event-cdb.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class CommunicationEventCDBService {
    protected itemValues: CommunicationEventCDB[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/communication-event-cdbs';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/communication-event-cdbs';

    constructor(protected http: Http) { }

    create(communicationEventCDB: CommunicationEventCDB): Observable<CommunicationEventCDB> {
        const copy = this.convert(communicationEventCDB);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(communicationEventCDB: CommunicationEventCDB): Observable<CommunicationEventCDB> {
        const copy = this.convert(communicationEventCDB);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<CommunicationEventCDB> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryByIdCustomer(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-idcustomer', options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(communicationEventCDB: CommunicationEventCDB, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(communicationEventCDB);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, communicationEventCDB: CommunicationEventCDB): Observable<CommunicationEventCDB> {
        const copy = this.convert(communicationEventCDB);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, communicationEventCDBs: CommunicationEventCDB[]): Observable<CommunicationEventCDB[]> {
        const copy = this.convertList(communicationEventCDBs);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(communicationEventCDB: CommunicationEventCDB): CommunicationEventCDB {
        if (communicationEventCDB === null || communicationEventCDB === {}) {
            return {};
        }
        // const copy: CommunicationEventCDB = Object.assign({}, communicationEventCDB);
        const copy: CommunicationEventCDB = JSON.parse(JSON.stringify(communicationEventCDB));
        return copy;
    }

    protected convertList(communicationEventCDBs: CommunicationEventCDB[]): CommunicationEventCDB[] {
        const copy: CommunicationEventCDB[] = communicationEventCDBs;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: CommunicationEventCDB[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
