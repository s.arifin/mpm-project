import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {CommunicationEventCDB} from './communication-event-cdb.model';
import {CommunicationEventCDBPopupService} from './communication-event-cdb-popup.service';
import {CommunicationEventCDBService} from './communication-event-cdb.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-communication-event-cdb-dialog',
    templateUrl: './communication-event-cdb-dialog.component.html'
})
export class CommunicationEventCDBDialogComponent implements OnInit {

    communicationEventCDB: CommunicationEventCDB;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected communicationEventCDBService: CommunicationEventCDBService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.communicationEventCDB.idCommunicationEvent !== undefined) {
            this.subscribeToSaveResponse(
                this.communicationEventCDBService.update(this.communicationEventCDB));
        } else {
            this.subscribeToSaveResponse(
                this.communicationEventCDBService.create(this.communicationEventCDB));
        }
    }

    protected subscribeToSaveResponse(result: Observable<CommunicationEventCDB>) {
        result.subscribe((res: CommunicationEventCDB) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: CommunicationEventCDB) {
        this.eventManager.broadcast({ name: 'communicationEventCDBListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'communicationEventCDB saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'communicationEventCDB Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-communication-event-cdb-popup',
    template: ''
})
export class CommunicationEventCDBPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected communicationEventCDBPopupService: CommunicationEventCDBPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.communicationEventCDBPopupService
                    .open(CommunicationEventCDBDialogComponent as Component, params['id']);
            } else {
                this.communicationEventCDBPopupService
                    .open(CommunicationEventCDBDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
