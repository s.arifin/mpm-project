import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { FacilityType } from './facility-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class FacilityTypeService {
   protected itemValues: FacilityType[];
   values: Subject<any> = new Subject<any>();

//    protected resourceUrl =  process.env.API_C_URL + 'api/facility-types';
   protected resourceUrl = process.env.API_C_URL + '/api/facility-types';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/facility-types';

   constructor(protected http: Http) { }

   create(facilityType: FacilityType): Observable<FacilityType> {
       const copy = this.convert(facilityType);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(facilityType: FacilityType): Observable<FacilityType> {
       const copy = this.convert(facilityType);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<FacilityType> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

    getAll(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl)
            .map((res: Response) => this.convertResponse(res));
    }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: FacilityType, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: FacilityType[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to FacilityType.
    */
   protected convertItemFromServer(json: any): FacilityType {
       const entity: FacilityType = Object.assign(new FacilityType(), json);
       return entity;
   }

   /**
    * Convert a FacilityType to a JSON which can be sent to the server.
    */
   protected convert(facilityType: FacilityType): FacilityType {
       if (facilityType === null || facilityType === {}) {
           return {};
       }
       // const copy: FacilityType = Object.assign({}, facilityType);
       const copy: FacilityType = JSON.parse(JSON.stringify(facilityType));
       return copy;
   }

   protected convertList(facilityTypes: FacilityType[]): FacilityType[] {
       const copy: FacilityType[] = facilityTypes;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: FacilityType[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
