import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { FacilityType } from './facility-type.model';
import { FacilityTypePopupService } from './facility-type-popup.service';
import { FacilityTypeService } from './facility-type.service';
import { ToasterService } from '../../shared';

@Component({
    selector: 'jhi-facility-type-dialog',
    templateUrl: './facility-type-dialog.component.html'
})
export class FacilityTypeDialogComponent implements OnInit {

    facilityType: FacilityType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected facilityTypeService: FacilityTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.facilityType.idFacilityType !== undefined) {
            this.subscribeToSaveResponse(
                this.facilityTypeService.update(this.facilityType));
        } else {
            this.subscribeToSaveResponse(
                this.facilityTypeService.create(this.facilityType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<FacilityType>) {
        result.subscribe((res: FacilityType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: FacilityType) {
        this.eventManager.broadcast({ name: 'facilityTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'facilityType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-facility-type-popup',
    template: ''
})
export class FacilityTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected facilityTypePopupService: FacilityTypePopupService
    ) {}

    ngOnInit() {

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.facilityTypePopupService
                    .open(FacilityTypeDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.facilityTypePopupService.parent = params['parent'];
                this.facilityTypePopupService
                    .open(FacilityTypeDialogComponent as Component);
            } else {
                this.facilityTypePopupService
                    .open(FacilityTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
