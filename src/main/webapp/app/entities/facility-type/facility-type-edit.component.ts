import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { FacilityType } from './facility-type.model';
import { FacilityTypeService } from './facility-type.service';
import { ToasterService} from '../../shared';

@Component({
   selector: 'jhi-facility-type-edit',
   templateUrl: './facility-type-edit.component.html'
})
export class FacilityTypeEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   facilityType: FacilityType;
   isSaving: boolean;
   idFacilityType: any;
   paramPage: number;
   routeId: number;

   constructor(
       protected alertService: JhiAlertService,
       protected facilityTypeService: FacilityTypeService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected toaster: ToasterService
   ) {
       this.facilityType = new FacilityType();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idFacilityType = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.facilityTypeService.find(this.idFacilityType).subscribe((facilityType) => {
           this.facilityType = facilityType;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['facility-type', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.facilityType.idFacilityType !== undefined) {
            this.facilityTypeService.update(this.facilityType).subscribe(
                (res) => {
                            this.toaster.showToaster('info', 'Save', 'facilityType saved !');
                            this.isSaving = false;
                            this.previousState();
                        },
                (res: FacilityType) => {
                            this.toaster.showToaster('danger', 'Save', 'Error saved !');
                            this.isSaving = false;
                        }
                );
       } else {
            this.facilityTypeService.create(this.facilityType).subscribe(
                (res) => {
                            this.toaster.showToaster('info', 'Save', 'facilityType saved !');
                            this.isSaving = false;
                            this.previousState();
                        },
                (res) => {
                            this.toaster.showToaster('danger', 'Save', 'Error saved !');
                            this.isSaving = false;
                        }
                );
       }
   }

   protected subscribeToSaveResponse(result: Observable<FacilityType>) {
       result.subscribe((res: FacilityType) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: FacilityType) {
       this.eventManager.broadcast({ name: 'facilityTypeListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'facilityType saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'facilityType Changed', error.message);
       this.alertService.error(error.message, null, null);
   }
}
