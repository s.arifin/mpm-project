import { BaseEntity } from './../../shared';

export class FacilityType implements BaseEntity {
    constructor(
        public id?: number,
        public idFacilityType?: number,
        public description?: string,
    ) {
    }
}
