import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { FacilityTypeComponent } from './facility-type.component';
import { FacilityTypeEditComponent } from './facility-type-edit.component';
import { FacilityTypePopupComponent } from './facility-type-dialog.component';

@Injectable()
export class FacilityTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idFacilityType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const facilityTypeRoute: Routes = [
    {
        path: 'facility-type',
        component: FacilityTypeComponent,
        resolve: {
            'pagingParams': FacilityTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facilityType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const facilityTypePopupRoute: Routes = [
    {
        path: 'facility-type-popup-new',
        component: FacilityTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facilityType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'facility-type-new',
        component: FacilityTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facilityType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'facility-type/:id/edit',
        component: FacilityTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facilityType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'facility-type/:route/:page/:id/edit',
        component: FacilityTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facilityType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'facility-type/:id/popup-edit',
        component: FacilityTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.facilityType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
