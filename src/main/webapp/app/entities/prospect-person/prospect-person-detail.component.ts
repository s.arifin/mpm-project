import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProspectPerson } from './prospect-person.model';
import { Internal, InternalService } from '../internal';
import { SalesBroker, SalesBrokerService } from '../sales-broker';
import { ProspectSource, ProspectSourceService } from '../prospect-source';
import { EventType, EventTypeService } from '../event-type';
import { Facility, FacilityService } from '../facility';
import { Salesman, SalesmanService } from '../salesman';
import { Suspect, SuspectService } from '../suspect';
import { ProspectPersonService } from './prospect-person.service';
import { ToasterService} from '../../shared';
import { Person, PersonService } from '../person';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-prospect-person-detail',
    templateUrl: './prospect-person-detail.component.html'
})
export class ProspectPersonDetailComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    salesBrokerSubscriber: Subscription;
    prospectPerson: ProspectPerson;
    isSaving: boolean;

    people: Person[];
    internals: Internal[];
    salesbrokers: SalesBroker[];
    prospectsources: ProspectSource[];
    eventtypes: EventType[];
    facilities: Facility[];
    salesmen: Salesman[];
    suspects: Suspect[];

    PROSPECT_EVENT_TYPE = 1;
    SALES_BROKER__TYPE_MAKELAR = 10;
    SALES_BROKER_TYPE_REFFERENCE = 11;
    SALES_BROKER_TYPE_REFFERAL = 12;
    SALES_BROKER_TYPE_OTHER = 99;

    constructor(
        protected alertService: JhiAlertService,
        protected prospectPersonService: ProspectPersonService,
        protected personService: PersonService,
        protected internalService: InternalService,
        protected salesBrokerService: SalesBrokerService,
        protected prospectSourceService: ProspectSourceService,
        protected eventTypeService: EventTypeService,
        protected facilityService: FacilityService,
        protected salesmanService: SalesmanService,
        protected suspectService: SuspectService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.prospectPerson = new ProspectPerson();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            } else {
                this.prospectPersonService.values.subscribe(
                    (response) => {
                        if (response) {
                            this.prospectPerson = response[0];
                        }
                    }
                );
            }
        });
        this.isSaving = false;

        this.salesBrokerSubscriber = this.salesBrokerService.values.subscribe(
            (response) => {
               if (response) {
                    this.getChosenSalesBroker(response);
               }
        });

        this.personService.query()
            .subscribe((res: ResponseWrapper) => { this.people = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.prospectSourceService.query()
            .subscribe((res: ResponseWrapper) => { this.prospectsources = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.eventTypeService.query({idparent: this.PROSPECT_EVENT_TYPE})
            .subscribe((res: ResponseWrapper) => {this.eventtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.facilityService.query()
        //     .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesmanService.query()
            .subscribe((res: ResponseWrapper) => { this.salesmen = res.json.filter((d) => d.coordinator === false); }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.salesBrokerSubscriber.unsubscribe();
    }

    load(id) {
        this.prospectPersonService.find(id).subscribe((prospectPerson) => {
            this.prospectPerson = prospectPerson;
        });
    }

    removeSales() {
        this.prospectPerson.brokerName = null;
        this.prospectPerson.brokerId = null;
    }

    previousState() {
        this.router.navigate(['prospect-person']);
    }

    save() {
        this.isSaving = true;
        if (this.prospectPerson.idProspect !== undefined) {
            this.subscribeToSaveResponse(
                this.prospectPersonService.update(this.prospectPerson));
        } else {
            this.subscribeToSaveResponse(
                this.prospectPersonService.create(this.prospectPerson));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProspectPerson>) {
        result.subscribe((res: ProspectPerson) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ProspectPerson) {
        this.eventManager.broadcast({ name: 'prospectPersonListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'prospectPerson saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'prospectPerson Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    getChosenSalesBroker(item: SalesBroker) {
        if (item) {
            this.prospectPerson.brokerId = item.idPartyRole;
            this.prospectPerson.brokerName = item.partyName;
        }
    }

    trackPersonById(index: number, item: Person) {
        return item.idParty;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackSalesBrokerById(index: number, item: SalesBroker) {
        return item.idPartyRole;
    }

    trackProspectSourceById(index: number, item: ProspectSource) {
        return item.idProspectSource;
    }

    trackEventTypeById(index: number, item: EventType) {
        return item.idEventType;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idPartyRole;
    }
}
