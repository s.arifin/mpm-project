import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    ProspectPersonService,
    ProspectPersonPopupService,
    ProspectPersonComponent,
    prospectPersonRoute,
    prospectPersonPopupRoute,
    ProspectPersonResolvePagingParams,
    ProspectPersonGridComponent,
    ProspectPersonEditComponent,
    ProspectPersonDetailComponent,
    ProspectPersonStepComponent,
    ProspectPersonViewPersonComponent,
    ProspectPersonVSOLandingPageComponent,
    ProspectPersonVSOFormMinatComponent,
    ProspectPersonVSODetailProspectComponent,
    ProspectPersonVSOEditComponent
} from './';

import { CommonModule } from '@angular/common';

import { MpmSalesUnitRequirementModule } from '../sales-unit-requirement';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         RadioButtonModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule,
         TooltipModule
        } from 'primeng/primeng';

import { WizardModule } from 'ng2-archwizard';

import { MpmSharedEntityModule } from '../shared-entity.module';
import { MpmCommunicationEventProspectModule } from '../communication-event-prospect/communication-event-prospect.module';

const ENTITY_STATES = [
    ...prospectPersonRoute,
    ...prospectPersonPopupRoute,
];

@NgModule({
    imports: [
        MpmCommunicationEventProspectModule,
        MpmSalesUnitRequirementModule,
        MpmSharedModule,
        MpmSharedEntityModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        WizardModule,
        RadioButtonModule,
        TooltipModule
    ],
    exports: [
        ProspectPersonComponent,
        ProspectPersonGridComponent,
        ProspectPersonEditComponent,
        ProspectPersonDetailComponent,
        ProspectPersonStepComponent,
    ],
    declarations: [
        ProspectPersonViewPersonComponent,
        ProspectPersonVSOEditComponent,
        ProspectPersonVSODetailProspectComponent,
        ProspectPersonVSOFormMinatComponent,
        ProspectPersonVSOLandingPageComponent,
        ProspectPersonComponent,
        ProspectPersonGridComponent,
        ProspectPersonEditComponent,
        ProspectPersonDetailComponent,
        ProspectPersonStepComponent,
    ],
    entryComponents: [
        ProspectPersonViewPersonComponent,
        ProspectPersonVSOEditComponent,
        ProspectPersonVSODetailProspectComponent,
        ProspectPersonVSOFormMinatComponent,
        ProspectPersonVSOLandingPageComponent,
        ProspectPersonComponent,
        ProspectPersonGridComponent,
        ProspectPersonEditComponent,
        ProspectPersonDetailComponent,
        ProspectPersonStepComponent,
    ],
    providers: [
        ProspectPersonService,
        ProspectPersonPopupService,
        ProspectPersonResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmProspectPersonModule {}
