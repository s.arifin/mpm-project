import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrencyPipe, DatePipe } from '@angular/common';
import { Response } from '@angular/http';
import { CommunicationEventProspect } from '../communication-event-prospect'
import { CommunicationEventProspectService} from '../communication-event-prospect'
import { Subscription } from 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ProspectPerson } from './prospect-person.model';
import { ProspectPersonService } from './prospect-person.service';

import { ToasterService} from '../../shared';
import { Prospect, ProspectService } from '../prospect';
import { Person, PersonService } from '../person';
import { ResponseWrapper} from '../../shared';
import { FollowUpQuestion } from '../shared-component';
import { ConfirmationService } from 'primeng/primeng';

import * as ProspectPersonConstant from '../../shared/constants/prospect-person.constants'

@Component({
    selector: 'jhi-prospect-step',
    templateUrl: './prospect-person-step.component.html',

})
export class ProspectPersonStepComponent implements OnInit, OnDestroy {
    public displayConfirmNextForm: boolean;
    public displayFollowUp: boolean;
    public idProspect: string;
    protected subscription: Subscription;
    protected prospectPerson: ProspectPerson;
    communicationEventProspect: CommunicationEventProspect;
    answer: FollowUpQuestion;
    idCommunicationEventProspect: any;
    prospectNumber: any;
    isSaving: boolean;
    nextFollowUp: Date;
    FOLLOW_UP_BY_PHONE = '151';
    FOLLOW_UP_BY_VISIT = '152';
    FOLLOW_UP_BY_WALKIN = '153';
    FOLLOW_UP_BY_DEALER = '154';
    FOLLOW_UP_BY_EVENT = '155';
    FOLLOW_UP_BY_POS = '156';
    BACK = 'prospect-person';

    constructor(
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected prospectPersonService: ProspectPersonService,
        protected communicationEventProspectService: CommunicationEventProspectService,
        protected route: ActivatedRoute,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected datePipe: DatePipe,
    ) {
        this.displayFollowUp = true;
        this.displayConfirmNextForm = false;
        this.prospectPerson = new ProspectPerson();
        this.communicationEventProspect = new CommunicationEventProspect();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idProspect = params['id'];
                this.load(params['id']);
            }
        });

        this.answer = new FollowUpQuestion();
        this.isSaving = false;

    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.prospectPersonService.find(id).subscribe((prospectPerson) => {
            this.prospectPerson = prospectPerson;
            console.log('WAKWAU', this.prospectPerson);
        });
    }

    public previousState(): void {
        this.router.navigate([this.BACK]);
    }

    save() {
        this.isSaving = true;
        if (this.answer.answer02 === 'no' ) {
            this.doNotConnected();
            this.incrementProspectCount();
        } else if (this.answer.answer03 === 'yes') {
            this.doInterest();
        } else if (this.answer.answer03 === 'no') {
            this.doNotInterest();
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProspectPerson>, back: Boolean) {
        result.subscribe((res: ProspectPerson) =>
            this.onSaveSuccess(res, back), (res: Response) => this.onSaveError(res));
    }

    protected subscribeToSaveResponseFollowUp(result: Observable<CommunicationEventProspect>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe((res: CommunicationEventProspect) => {
                    this.idCommunicationEventProspect = res.idCommunicationEvent;
                    resolve();
                });
            }
        )
    }

    protected onSaveSuccess(result: ProspectPerson, back: Boolean) {
        this.isSaving = false;
        if (back === true) {
            this.eventManager.broadcast({ name: 'prospectPersonListModification', content: 'OK'});
            this.toaster.showToaster('info', 'Save', 'prospectPerson saved !');
            this.previousState();
        }
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'prospect Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    goToFormInterest() {
        this.confirmationService.confirm({
            message: 'Do you want to form interest?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.router.navigate(['communication-event-prospect/' + this.idCommunicationEventProspect + '/101/interest-form']);
            },
            reject: () => {
                this.previousState();
            }
        });
    }

    protected doInterest() {
        this.prospectPerson.prospectCount = 0;
        this.prospectPerson.dateFollowUp = new Date();
        const followUpResult = new CommunicationEventProspect();
        followUpResult.idProspect = this.prospectPerson.idProspect;
        followUpResult.eventTypeId = this.answer.answer01;
        followUpResult.walkInType = this.answer.answer05;
        followUpResult.interest = true;

        this.subscribeToSaveResponseFollowUp(this.communicationEventProspectService.create(followUpResult)).then(
            () => {
                this.subscribeToSaveResponse(this.prospectPersonService.updateFollowUp(this.prospectPerson), false);
                // this.goToFromOptionNextStep();
                this.goToVSOLandingPage();
            }
        );
    }

    protected goToVSOLandingPage(): void {
        this.displayFollowUp = false;
    }

    protected goToFromOptionNextStep(): void {
        this.displayConfirmNextForm = true;
    }

    public goToFormMinat(): void {
        this.router.navigate(['communication-event-prospect/' + this.idCommunicationEventProspect + '/101/interest-form']);
    }

    public goToDraftVSO(): void {
        this.displayConfirmNextForm = false;
        this.displayFollowUp = false;
    }

    protected doNotInterest() {

        const followUpResult = new CommunicationEventProspect();
        const dt = new Date();
        const zndt = this.datePipe.transform(dt.toString(), 'yyyy-MM-dd HH:mm:ss');

        followUpResult.eventTypeId = this.answer.answer01;
        followUpResult.idProspect = this.prospectPerson.idProspect;
        followUpResult.interest = false;
        if (this.answer.answer04 !== 'Lainnya') {
            followUpResult.note = 'Tidak Tertarik Karena ' + this.answer.answer04;
        } else {
            followUpResult.note = 'Tidak Tertarik Karena' + this.answer.note;
        }

        this.subscribeToSaveResponseFollowUp(this.communicationEventProspectService.create(followUpResult)).then(
            () => {
                this.subscribeToSaveResponse(this.prospectPersonService.executeProcess(103, zndt, this.prospectPerson), true);
            }
        );
    }

    protected doNotConnected() {
        const followUpResult = new CommunicationEventProspect();
        followUpResult.eventTypeId = this.answer.answer01;
        followUpResult.nextFollowUp = this.nextFollowUp;
        followUpResult.note = this.communicationEventProspect.note;
        followUpResult.idProspect = this.prospectPerson.idProspect;
        this.subscribeToSaveResponseFollowUp(this.communicationEventProspectService.create(followUpResult));
    }

    enterStep(e) {
        this.answer = new FollowUpQuestion();
    }

    protected incrementProspectCount() {
        if ((this.prospectPerson.prospectCount + 1) === 3) {
            const dt = new Date();
            const zndt = this.datePipe.transform(dt.toString(), 'yyyy-MM-dd hh:mm:ss');
            this.subscribeToSaveResponse(this.prospectPersonService.executeProcess(103, zndt, this.prospectPerson), true);
        } else {
            const sum = this.prospectPerson.prospectCount + 1;
            this.prospectPerson.prospectCount = sum;
            this.prospectPerson.dateFollowUp = new Date();
            this.subscribeToSaveResponse(this.prospectPersonService.update(this.prospectPerson), true);
        }
    }

}
