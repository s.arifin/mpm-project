import { Component, OnInit, OnDestroy, DoCheck, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProspectPerson } from '../prospect-person.model';
import { Internal, InternalService } from '../../internal';
import { SalesBroker, SalesBrokerService } from '../../sales-broker';
import { ProspectSource, ProspectSourceService } from '../../prospect-source';
import { EventType, EventTypeService } from '../../event-type';
import { Facility, FacilityService } from '../../facility';
import { Salesman, SalesmanService } from '../../salesman';
import { Suspect, SuspectService } from '../../suspect';
import { ProspectPersonService } from '../prospect-person.service';
import { ResponseWrapper, ToasterService, Principal, CommonUtilService} from '../../../shared';
import { Person, PersonService } from '../../person';
import { LoadingService } from '../../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-prospect-person-vso-detail-prospect',
    templateUrl: './prospect-person-vso-detail-prospect.component.html'
})

export class ProspectPersonVSODetailProspectComponent implements OnInit, OnDestroy, DoCheck, OnChanges {
    @Input()
    public idprospect: string;

    @Output()
    public fnAfterSave = new EventEmitter();

    private salesBrokerSubscriber: Subscription;

    public isDetail: boolean;
    public prospectPerson: ProspectPerson;
    public findProspectPerson: ProspectPerson;
    public isInvalid: Boolean = true;
    public findProspect: Boolean = false;
    public error: Boolean = false;
    public people: Person[];
    public internals: Internal[];
    public salesbrokers: SalesBroker[];
    public prospectsources: ProspectSource[];
    public eventtypes: EventType[];
    public facilities: Facility[];
    public salesmen: Salesman[];
    public suspects: Suspect[];

    public indexAccordion: number;
    public index: number;
    public PROSPECT_EVENT_TYPE: Number = 1;
    public SALES_BROKER__TYPE_MAKELAR: Number = 10;
    public SALES_BROKER_TYPE_REFFERENCE: Number = 11;
    public SALES_BROKER_TYPE_REFFERAL: Number = 12;
    public SALES_BROKER_TYPE_OTHER: Number = 99;

    constructor(
        private alertService: JhiAlertService,
        private prospectPersonService: ProspectPersonService,
        private personService: PersonService,
        private internalService: InternalService,
        private salesBrokerService: SalesBrokerService,
        private prospectSourceService: ProspectSourceService,
        private eventTypeService: EventTypeService,
        private facilityService: FacilityService,
        private salesmanService: SalesmanService,
        private suspectService: SuspectService,
        private route: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toaster: ToasterService,
        private loadingService: LoadingService,
        private commonUtilService:  CommonUtilService,
        private principal: Principal
    ) {
        this.isDetail = true;
        this.index = 0;
        this.indexAccordion = 0;
        this.prospectPerson = new ProspectPerson();
    }

    ngOnInit() {
        this.salesBrokerSubscriber = this.salesBrokerService.values.subscribe(
            (response) => {
               if (response) {
                this.getChosenSalesBroker(response);
               }
        });

        this.personService.query()
            .subscribe((res: ResponseWrapper) => { this.people = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.prospectSourceService.query()
            .subscribe((res: ResponseWrapper) => { this.prospectsources = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.eventTypeService.query({idparent: this.PROSPECT_EVENT_TYPE})
            .subscribe((res: ResponseWrapper) => {this.eventtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.facilityService.query()
        //     .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngDoCheck() {
        if (this.prospectPerson.person.firstName == null ||
            this.prospectPerson.person.firstName === '' ||
            this.prospectPerson.person.cellPhone1 == null ||
            this.prospectPerson.person.cellPhone1 === '' ||
            !this.prospectPerson.prospectSourceId) {

            this.isInvalid = true;
        } else {
            this.isInvalid = false;
        }
    }

    ngOnChanges(change: SimpleChanges) {
        if (change['idprospect']) {
            this.load(this.idprospect);
        }
    }

    ngOnDestroy() {
        this.salesBrokerSubscriber.unsubscribe();
    }

    public load(id): void {
        this.prospectPersonService.find(id).subscribe(
            (prospectPerson) => {
                if (prospectPerson) {
                    this.prospectPerson = prospectPerson;
                } else {
                    this.prospectPersonService.values.subscribe(
                        (response) => {
                            if (response) {
                                this.prospectPerson = response[0];
                            }
                        }
                    );
                }
            }
        );
    }

    public getChosenSalesBroker(item: SalesBroker) {
        if (item) {
            this.prospectPerson.brokerId = item.idPartyRole;
            this.prospectPerson.brokerName = item.person.name;
        }
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'prospectPerson Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackPersonById(index: number, item: Person) {
        return item.idParty;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackSalesBrokerById(index: number, item: SalesBroker) {
        return item.idPartyRole;
    }

    trackProspectSourceById(index: number, item: ProspectSource) {
        return item.idProspectSource;
    }

    trackEventTypeById(index: number, item: EventType) {
        return item.idEventType;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idPartyRole;
    }

    openNext() {
        if (this.index === 3) {
            this.indexAccordion = (this.indexAccordion === 1) ? 0 : this.indexAccordion + 1;
        } else {
            this.index = this.index + 1;
        }
    }

    openPrev() {
        this.index = 0;
        this.indexAccordion = (this.indexAccordion === 0) ? 1 : this.indexAccordion - 1;
    }

    public previousState() {
        this.fnAfterSave.emit();
    }

    public goToEdit() {
        if (this.isDetail) {
            this.isDetail = false;
        } else {
            this.isDetail = true;
            this.load(this.idprospect);
        }
    }
}
