import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CommunicationEventProspectService, CommunicationEventProspect } from '../../communication-event-prospect';
import { ResponseWrapper, CommonUtilService, ToasterService} from '../../../shared';
import { SaleType, SaleTypeService } from '../../sale-type';
import { Feature} from '../../feature';
import { Motor, MotorService } from '../../motor';
import { LoadingService } from '../../../layouts/loading/loading.service';

import * as _ from 'lodash';

@Component({
    selector: 'jhi-prospect-person-vso-form-minat',
    templateUrl: './prospect-person-vso-form-minat.component.html'
})
export class ProspectPersonVSOFormMinatComponent implements OnInit, OnChanges {
    @Input()
    public idprospect: string;

    @Input()
    public idCommunicationEventProspect: string;

    @Output()
    public fnAfterSave = new EventEmitter();

    public saletypes: SaleType[];
    public features: Feature[];
    public motors: Motor[];
    public selectedMotor: any;
    public listMotors = [{label: 'Please Select', value: null}];
    public purchasePlans: Array<object>;
    public communicationEventProspect: CommunicationEventProspect;

    constructor(
        private alertService: JhiAlertService,
        private communicationEventProspectService: CommunicationEventProspectService,
        private saleTypeService: SaleTypeService,
        private motorService: MotorService,
        private route: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toaster: ToasterService,
        private commontUtilService: CommonUtilService,
        private loadingService: LoadingService
    ) {
        this.purchasePlans = this.communicationEventProspectService.getPurchasePlan();
    }

    private resetMe() {
        this.communicationEventProspect = new CommunicationEventProspect();
        this.communicationEventProspect.productId = null;
        this.communicationEventProspect.colorId = null;
        this.communicationEventProspect.saleTypeId = null;
        this.communicationEventProspect.purchasePlan = null;
        this.selectedMotor = null;
    }

    ngOnInit() {
        this.resetMe();
        this.saleTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.saletypes = this.commontUtilService.getDataByParent(res.json, 1); },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            });
        this.motorService.query({
            page: 0,
            size: 10000,
            sort: ['idProduct', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.motors = res.json;
            this.motors.forEach((element) => {
                this.listMotors.push({
                    label: element.idProduct + ' - ' + element.description + ' - ' + element.name,
                    value: element.idProduct });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            });
    }

    ngOnChanges(change: SimpleChanges) {
        if (change['idCommunicationEventProspect']) {
            this.load(this.idCommunicationEventProspect);
        }
    }

    public disableInterestForm(): boolean {
        if (this.communicationEventProspect.purchasePlan == null ||
            this.communicationEventProspect.purchasePlan === undefined ||
            this.communicationEventProspect.qty == null ||
            this.communicationEventProspect.qty === undefined ||
            this.communicationEventProspect.qty <= 0) {
            return true
        } else { return false };
    }

    private load(id): void {
        this.communicationEventProspectService.find(id).subscribe((communicationEventProspect) => {
            this.communicationEventProspect = communicationEventProspect;
        });
    }

    public selectMotor(isSelect?: boolean): void {
        this.communicationEventProspect.productId = this.selectedMotor;
        this.getColorByMotor(this.communicationEventProspect.productId);
    }

    private getColorByMotor(idProduct: string): void {
        if (idProduct !== null) {
            this.features = new Array<Feature>();
            const selectedProduct: Motor = _.find(this.motors, function(e) {
                return e.idProduct.toLowerCase() === idProduct.toLowerCase();
            });
            this.features = selectedProduct.features;
        }
    }

    public previousState(): void {
        this.fnAfterSave.emit();
    }

    public save(): void {
        this.loadingService.loadingStart();
        if (this.communicationEventProspect.idCommunicationEvent !== undefined) {
            this.communicationEventProspectService.executeProcess(101, null, this.communicationEventProspect).subscribe(
                (res) => {
                    this.loadingService.loadingStop();
                    this.toaster.showToaster('info', 'Save', 'Form Minat sudah di submit');
                    this.fnAfterSave.emit();
                },
                (err) => {
                    this.commontUtilService.showError(err);
                    this.loadingService.loadingStop();
                }
            )
        } else {
            this.toaster.showToaster('error', 'Error', 'Error');
            this.loadingService.loadingStop();
        }
    }
}
