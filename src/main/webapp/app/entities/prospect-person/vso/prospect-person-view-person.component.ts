import { Component, Input, Output, OnInit, OnDestroy, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { Person, PersonService } from '../../person';
import { ReligionType, ReligionTypeService } from '../../religion-type';
import { WorkType, WorkTypeService } from '../../work-type';
import { ResponseWrapper, CommonUtilService } from '../../../shared';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../../layouts';

@Component({
    selector: 'jhi-prospect-person-view-person',
    templateUrl: './prospect-person-view-person.component.html'
})
export class ProspectPersonViewPersonComponent implements OnInit, OnDestroy, OnChanges {

    @Input()
    public person: Person = new Person();
    @Input()
    public readonly = false;
    @Input()
    public index = 0;
    @Input()
    public viewHiddenField: any = 0;
    @Output()
    public exist = new EventEmitter();

    religiontypes: ReligionType[];
    worktypes: WorkType[];
    personCust: Person[];
    selectedPerson: any;
    isNull: Boolean = false;

    public yearForCalendar: string

    genders: Array<object> = [
        {label : 'Pria', value : 'P'},
        {label : 'Wanita', value : 'W'}
    ];

    bloodTypes: Array<object> = [
        {label : 'O', value : 'O'},
        {label : 'A', value : 'A'},
        {label : 'B', value : 'B'},
        {label : 'AB', value : 'AB'}
    ];

    constructor(
        private workTypeService: WorkTypeService,
        private religionTypeService: ReligionTypeService,
        private personService: PersonService,
        private commonUtilService: CommonUtilService,
        private confirmationService: ConfirmationService,
        private loadingService: LoadingService,
    ) {
        this.yearForCalendar = this.commonUtilService.getYearRangeForCalendar();
        this.personCust = [];
    }

    ngOnInit() {
        this.religionTypeService.query({size: 100})
            .subscribe((res: ResponseWrapper) => { this.religiontypes = res.json; });
        this.workTypeService.query({size: 100})
            .subscribe((res: ResponseWrapper) => { this.worktypes = res.json; });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['person']) {
            console.log('this.person', this.person);
            if (this.person.dob !== null) {
                this.person.dob = new Date(this.person.dob);
            }
        }
    }

    ngOnDestroy() {
    }

    checkExistPersonByPersonalId(data: String, using: Boolean) {
        this.loadingService.loadingStart();
        this.selectedPerson = [];
        this.personService.findByPersonalIdNumberCustomer({
            personalIdNumber : data,
            size: 999
        }).subscribe(
            (res) => {
                this.loadingService.loadingStop();
                // this.exist.emit(res);
                this.personCust = res.json;
                console.log('cari nik customer = ', this.personCust)
                if (this.personCust.length > 0) {
                    this.confirmationService.confirm({
                        message: 'Data KTP Sudah ada di Dalam Database',
                        header: 'Confirmation',
                        icon: 'fa fa-question-circle',
                        acceptVisible: true,
                        rejectVisible: false
                    });
                    this.isNull = true;
                } else {
                    this.person = new Person();
                    this.isNull = false;
                }
            }
        );
    }

    findPerson() {
        if (this.selectedPerson !== null || this.selectedPerson !== undefined) {
            this.personService.find(this.selectedPerson).subscribe(
                (res) => {
                    this.exist.emit(res);
                },
                (err) => {
                    this.person = new Person();
                    this.isNull = false;
                    this.selectedPerson = [];
                }
            )
        } else {
            this.person = new Person();
        }
    }

    getProvince(province: any) {
        this.person.postalAddress.provinceId = province;
    }

    getDistrict(district: any) {
        this.person.postalAddress.districtId = district;
    }

    getCity(city: any) {
        this.person.postalAddress.cityId = city;
    }

    getVillage(village: any) {
        this.person.postalAddress.villageId = village;
    }

    trackWorkTypeById(index: number, item: WorkType) {
        return item.idWorkType;
    }

    trackReligionTypeById(index: number, item: ReligionType) {
        return item.idReligionType;
    }
}
