import { Component, OnInit, OnDestroy, DoCheck, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProspectPerson } from '../prospect-person.model';
import { Internal, InternalService } from '../../internal';
import { SalesBroker, SalesBrokerService } from '../../sales-broker';
import { ProspectSource, ProspectSourceService } from '../../prospect-source';
import { EventType, EventTypeService } from '../../event-type';
import { Facility, FacilityService } from '../../facility';
import { Salesman, SalesmanService } from '../../salesman';
import { Suspect, SuspectService } from '../../suspect';
import { ProspectPersonService } from '../prospect-person.service';
import { ToasterService, Principal, CommonUtilService, ResponseWrapper } from '../../../shared';
import { Person, PersonService } from '../../person';
import { LoadingService } from '../../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-prospect-person-vso-edit',
    templateUrl: './prospect-person-vso-edit.component.html'
})
export class ProspectPersonVSOEditComponent implements OnInit, OnDestroy, DoCheck, OnChanges {
    @Input()
    public idprospect: string;

    @Output()
    public fnAfterSave = new EventEmitter();

    private subscription: Subscription;
    salesBrokerSubscriber: Subscription;
    prospectPerson: ProspectPerson;
    findProspectPerson: ProspectPerson;
    isSaving: boolean;
    isInvalid = true;
    findProspect = false;
    error = false;

    people: Person[];
    internals: Internal[];
    salesbrokers: SalesBroker[];
    prospectsources: ProspectSource[];
    eventtypes: EventType[];
    facilities: Facility[];
    salesmen: Salesman[];
    suspects: Suspect[];

    indexAccordion = 0;
    index = 0;
    PROSPECT_EVENT_TYPE = 1;
    SALES_BROKER__TYPE_MAKELAR = 10;
    SALES_BROKER_TYPE_REFFERENCE = 11;
    SALES_BROKER_TYPE_REFFERAL = 12;
    SALES_BROKER_TYPE_OTHER = 99;

    constructor(
        private alertService: JhiAlertService,
        private prospectPersonService: ProspectPersonService,
        private personService: PersonService,
        private internalService: InternalService,
        private salesBrokerService: SalesBrokerService,
        private prospectSourceService: ProspectSourceService,
        private eventTypeService: EventTypeService,
        private facilityService: FacilityService,
        private salesmanService: SalesmanService,
        private suspectService: SuspectService,
        private route: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toaster: ToasterService,
        private loadingService: LoadingService,
        private commonUtilService:  CommonUtilService,
        private principal: Principal
    ) {
        this.prospectPerson = new ProspectPerson();
    }

    ngOnInit() {
        this.isSaving = false;

        this.salesBrokerSubscriber = this.salesBrokerService.values.subscribe(
            (response) => {
               if (response) {
                this.getChosenSalesBroker(response);
               }
        });

        this.personService.query()
            .subscribe((res: ResponseWrapper) => { this.people = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.prospectSourceService.query()
            .subscribe((res: ResponseWrapper) => { this.prospectsources = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.eventTypeService.query({idparent: this.PROSPECT_EVENT_TYPE})
            .subscribe((res: ResponseWrapper) => {this.eventtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngDoCheck() {
        if (this.prospectPerson.person.firstName == null ||
            this.prospectPerson.person.firstName === '' ||
            this.prospectPerson.person.cellPhone1 == null ||
            this.prospectPerson.person.cellPhone1 === '' ||
            !this.prospectPerson.prospectSourceId) {

            this.isInvalid = true;
        } else {
            this.isInvalid = false;
        }
    }

    ngOnChanges(change: SimpleChanges) {
        if (change['idprospect']) {
            this.load(this.idprospect);
        }
    }

    ngOnDestroy() {
        this.salesBrokerSubscriber.unsubscribe();
    }

    private load(id) {
        this.prospectPersonService.find(id).subscribe(
            (prospectPerson) => {
                if (prospectPerson) {
                    this.prospectPerson = prospectPerson;
                } else {
                    this.prospectPersonService.values.subscribe(
                        (response) => {
                            if (response) {
                                this.prospectPerson = response[0];
                            }
                        }
                    )
                }
            }
        );
    }

    removeSalesBroker() {
        this.prospectPerson.brokerName = null;
        this.prospectPerson.brokerId = null;
    }

    previousState() {
        this.fnAfterSave.emit();
    }

    save() {
        this.findProspect = false;
        this.loadingService.loadingStart();

        if (this.prospectPerson.idProspect !== undefined) {
            this.subscribeToSaveResponse(
                this.prospectPersonService.update(this.prospectPerson));
        } else {
            this.doCheckProspect().then(
                () => {
                    // HANYA SEMENTARA SEBELUM EXTRA ROLE JALAN
                    if (this.error === true) {
                        this.error = false;
                    } else if (this.findProspect === false) {
                        this.prospectPerson.dealerId = this.principal.getIdInternal();
                        this.subscribeToSaveResponse(
                            this.prospectPersonService.create(this.prospectPerson));
                    }
                }
            );
        }
    }

    private doCheckProspect(): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                this.prospectPersonService.executeProcess(105, this.principal.getIdInternal(), this.prospectPerson).subscribe(
                    (response) => {
                        if (response.idProspect != null) {
                            this.findProspectPerson = response;
                            this.loadingService.loadingStop();
                            this.findProspect = true;
                        }
                        resolve();
                    },
                    // HANYA SEMENTARA SEBELUM EXTRA ROLE JALAN
                    (err) => {
                        this.error = true;
                        this.commonUtilService.showError(err);
                        resolve();
                    });
            }
        );
    }

    public doFollowUp(): void {
        this.router.navigate(['../prospect-person/' + this.findProspectPerson.idProspect + '/step']);
    }

    public doEdit(): void {
        this.load(this.findProspectPerson.idProspect);
    }

    private subscribeToSaveResponse(result: Observable<ProspectPerson>) {
        result.subscribe((res: ProspectPerson) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ProspectPerson) {
        this.toaster.showToaster('info', 'Save', 'prospectPerson saved / updated !');
        this.isSaving = false;
        this.loadingService.loadingStop();
        this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'prospectPerson Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    getChosenSalesBroker(item: SalesBroker) {
        if (item) {
            this.prospectPerson.brokerId = item.idPartyRole;
            this.prospectPerson.brokerName = item.person.name;
        }
    }

    trackPersonById(index: number, item: Person) {
        return item.idParty;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackSalesBrokerById(index: number, item: SalesBroker) {
        return item.idPartyRole;
    }

    trackProspectSourceById(index: number, item: ProspectSource) {
        return item.idProspectSource;
    }

    trackEventTypeById(index: number, item: EventType) {
        return item.idEventType;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idPartyRole;
    }

    openNext() {
        if (this.index === 3) {
            this.indexAccordion = (this.indexAccordion === 1) ? 0 : this.indexAccordion + 1;
        } else {
            this.index = this.index + 1;
        }
    }

    openPrev() {
        this.index = 0;
        this.indexAccordion = (this.indexAccordion === 0) ? 1 : this.indexAccordion - 1;
    }
}
