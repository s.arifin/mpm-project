import { Component, Input } from '@angular/core';

@Component({
    selector: 'jhi-prospect-person-vso-landing-page',
    templateUrl: './prospect-person-vso-landing-page.component.html'
})
export class ProspectPersonVSOLandingPageComponent {
    @Input()
    public idprospect: string;

    @Input()
    public idCommunicationEventProspect: string;

    public showPage: string;
    constructor() {
        this.showPage = null;
    }

    public goToPage(dest?: string): void {
        this.showPage = dest;
    }
}
