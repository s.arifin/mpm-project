import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ProspectPersonComponent } from './prospect-person.component';
import { ProspectPersonEditComponent } from './prospect-person-edit.component';
import { ProspectPersonDetailComponent } from './prospect-person-detail.component';
import { ProspectPersonStepComponent } from './prospect-person-step.component';

@Injectable()
export class ProspectPersonResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idProspect,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const prospectPersonRoute: Routes = [
    {
        path: 'prospect-person',
        component: ProspectPersonComponent,
        resolve: {
            'pagingParams': ProspectPersonResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const prospectPersonPopupRoute: Routes = [
    {
        path: 'prospect-person-new',
        component: ProspectPersonEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-person/:id/edit',
        component: ProspectPersonEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-person/:id/detail',
        component: ProspectPersonDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectPerson.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'prospect-person/:id/step',
        component: ProspectPersonStepComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospect.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];
