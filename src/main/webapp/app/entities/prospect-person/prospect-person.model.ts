import { BaseEntity } from '../shared-component';
import { Person } from '../person'
import { Prospect } from '../prospect/prospect.model'
import { CommunicationEvent } from '../communication-event/communication-event.model'

export class ProspectPerson extends Prospect {
    constructor(
        public person?: any,
        public fName?: any,
        public lName?: any,
        public phone?: any,
        public cellphone1?: any,
        public cellphone2?: any,
        public facilityName?: any,
        public internName?: any,
        public personalIdNumber?: any,
        public prospectNumber?: any,

    ) {
        super();
        this.person = new Person();
    }
}
