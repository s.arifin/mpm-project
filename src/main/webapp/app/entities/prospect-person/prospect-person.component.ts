import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ProspectPerson } from './prospect-person.model';
import { ProspectPersonService } from './prospect-person.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService, CommonUtilService} from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LazyLoadEvent } from 'primeng/primeng';
import { ConfirmationService } from 'primeng/primeng';
import { Salesman } from '../salesman';
import { SalesUnitRequirementStatusUnitDocumentMessageComponent } from '../sales-unit-requirement';
import { Person } from '../person';
import * as ProspectPersonConstant from '../../shared/constants/prospect-person.constants'

@Component({
    selector: 'jhi-prospect-person',
    templateUrl: './prospect-person.component.html'
})
export class ProspectPersonComponent implements OnInit, OnDestroy {

    prospectPerson: ProspectPerson;
    statusNew: Number;
    statusLow: Number;
    statusMedium: Number;
    statusHot: Number;
    newProspect: Boolean = false;
    findProspect: Boolean = false;
    findPerson: Boolean = false;
    findNik: Boolean = false;
    name: String = null;
    cellPhone: String = null;
    nik: String = null;
    userSales: any;
    findProspectOtherSales: Boolean = false;
    salesFU: Boolean = false;

    constructor(
        protected alertService: JhiAlertService,
        protected prospectPersonService: ProspectPersonService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected commonUtilService: CommonUtilService,
        protected principal: Principal,
    ) {
        this.prospectPerson = new ProspectPerson();
        this.statusNew = ProspectPersonConstant.STATUS_NEW;
        this.statusLow = ProspectPersonConstant.STATUS_LOW;
        this.statusMedium = ProspectPersonConstant.STATUS_MEDIUM;
        this.statusHot = ProspectPersonConstant.STATUS_HOT;
        this.userSales = null;
    }

    ngOnInit() {

    }

    ngOnDestroy() {

    }

    clearOldData() {
        this.newProspect = false;
        this.findProspect = false;
        this.findPerson = false;
        this.findNik = false;
        this.name = null;
        this.cellPhone = null;
        this.nik = null;
    }

    doCheckProspect() {
        const checkProspect = new ProspectPerson();
        this.userSales = this.principal.getUserLogin();

        if (this.name.split(' ').length === 1) {
            checkProspect.person.firstName = this.name;
            checkProspect.person.lastName = '';
        } else {
            checkProspect.person.firstName = this.name.split(' ').slice(0, -1).join(' ');
            checkProspect.person.lastName = this.name.split(' ').slice(-1).join(' ');
        }

        checkProspect.person.cellPhone1 = this.cellPhone;

        this.prospectPersonService.executeProcess(105, this.principal.getIdInternal(), checkProspect).subscribe(
        (response) => {
            // Jika Prpspeck Ditemukan
            if (response.person.getIdInternal != null && response.person.getIdInternal !== undefined) {
                this.prospectPerson = response;
                if (this.prospectPerson.person.lastName == null) {
                    this.name = this.prospectPerson.person.firstName;
                } else {
                    this.name = this.prospectPerson.person.firstName + ' ' + this.prospectPerson.person.lastName;
                }
                this.cellPhone = this.prospectPerson.person.cellPhone1;
                this.findPerson = true;
                this.newProspect = false;
            // Jika Prospect Tidak Ditemukan Namun, Data Orang Ditemukan
            } else if (response.idProspect != null) {
                    this.prospectPerson = response;

                    if (this.prospectPerson.salesman.userName.toUpperCase() !== this.userSales.toUpperCase()) {
                        this.findProspectOtherSales = true;
                        this.salesFU = this.prospectPerson.salesman.partyName;
                        this.newProspect = false;
                    } else {
                        if (this.prospectPerson.person.lastName == null) {
                            this.name = this.prospectPerson.person.firstName;
                        } else {
                            this.name = this.prospectPerson.person.firstName + ' ' + this.prospectPerson.person.lastName;
                        }
                        this.cellPhone = this.prospectPerson.person.cellPhone1;
                        this.findProspect = true;
                        this.newProspect = false;
                    }
                } else if (response.idProspect != null) {
                    this.prospectPerson = response;
                    if (this.prospectPerson.person.lastName == null) {
                        this.name = this.prospectPerson.person.firstName;
                    } else {
                        this.name = this.prospectPerson.person.firstName + ' ' + this.prospectPerson.person.lastName;
                    }
                    this.cellPhone = this.prospectPerson.person.cellPhone1;
                    this.findNik = true;
                    this.newProspect = false;
            // Jika Data Tidak Ditemukan
            } else {
                const newProspectData = new Array<ProspectPerson>();
                newProspectData.push(checkProspect);
                this.prospectPersonService.pushItems(newProspectData);
                this.clearOldData();
                this.router.navigate(['../prospect-person-new']);
            }
        },
        (err) => {
            this.commonUtilService.showError(err);
        });
        // () => {
            // const newProspectData = new Array<ProspectPerson>();
            // newProspectData.push(checkProspect);
            // this.prospectPersonService.pushItems(newProspectData);
            // this.clearOldData();
            // this.router.navigate(['../prospect-person-new']);
        // });
    }

    public doFollowUp(): void {
        this.clearOldData();
        this.router.navigate(['../prospect-person/' + this.prospectPerson.idProspect + '/step']);
    }

    public doEdit(): void {
        this.clearOldData();
        this.router.navigate(['../prospect-person/' + this.prospectPerson.idProspect + '/edit']);
    }

    public backToFind(): void {
        this.clearOldData();
        this.newProspect = true;
    }

    public trackpersonalIdNumber(index: number, item: Person): String {
        return item.personalIdNumber;
    }

    closeDialog() {
        this.findProspectOtherSales = false;
    }
}
