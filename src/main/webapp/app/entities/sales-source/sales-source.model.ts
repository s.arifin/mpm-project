import { BaseEntity } from './../../shared';

export class SalesSource implements BaseEntity {
    constructor(
        public id?: number,
        public description?: string,
    ) {
    }
}
