import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {SalesSource} from './sales-source.model';
import {SalesSourcePopupService} from './sales-source-popup.service';
import {SalesSourceService} from './sales-source.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-sales-source-dialog',
    templateUrl: './sales-source-dialog.component.html'
})
export class SalesSourceDialogComponent implements OnInit {

    salesSource: SalesSource;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected salesSourceService: SalesSourceService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.salesSource.id !== undefined) {
            this.subscribeToSaveResponse(
                this.salesSourceService.update(this.salesSource));
        } else {
            this.subscribeToSaveResponse(
                this.salesSourceService.create(this.salesSource));
        }
    }

    protected subscribeToSaveResponse(result: Observable<SalesSource>) {
        result.subscribe((res: SalesSource) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: SalesSource) {
        this.eventManager.broadcast({ name: 'salesSourceListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'salesSource saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'salesSource Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-sales-source-popup',
    template: ''
})
export class SalesSourcePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected salesSourcePopupService: SalesSourcePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.salesSourcePopupService
                    .open(SalesSourceDialogComponent as Component, params['id']);
            } else {
                this.salesSourcePopupService
                    .open(SalesSourceDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
