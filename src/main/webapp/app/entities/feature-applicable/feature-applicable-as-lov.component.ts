import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { FeatureApplicable } from './feature-applicable.model';
import { FeatureApplicableService } from './feature-applicable.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FeatureApplicablePopupService } from './feature-applicable-popup.service';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';

@Component({
    selector: 'jhi-feature-applicable-as-lov',
    templateUrl: './feature-applicable-as-lov.component.html'
})
export class FeatureApplicableAsLovComponent implements OnInit, OnDestroy {

    currentAccount: any;
    idFeatureType: any;
    idFeature: any;
    idProduct: any;
    featureApplicables: FeatureApplicable[];
    selected: FeatureApplicable[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    filterName: string;

    constructor(
        public activeModal: NgbActiveModal,
        protected featureApplicableService: FeatureApplicableService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = 5;
        this.page = 1;
        this.predicate = 'product.idProduct';
        this.reverse = 'asc';
        this.first = 0;
        this.selected = [];
        this.filterName = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['filterName'] ?
                          this.activatedRoute.snapshot.params['filterName'] : 'none';
    }

    loadAll() {
        if (this.currentSearch) {
            this.featureApplicableService.search({
                // idFeatureType: 100,
                // idFeature: this.idFeature,
                // idProduct: this.idProduct,
                filterName: this.filterName,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.featureApplicableService.queryFilterBy({
            idFeatureType: 100,
            // idFeature: this.idFeature,
            // idProduct: this.idProduct,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.loadAll();
    }

    clear() {
        this.page = 1;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 1;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: FeatureApplicable) {
        return item.idApplicability;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idApplicability') {
            result.push('idApplicability');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.featureApplicables = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    pushData() {
        this.featureApplicableService.pushItems(this.selected);
        window.history.back();
        this.activeModal.dismiss('close');
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        window.history.back();
        this.activeModal.dismiss('cancel');
    }

}

@Component({
    selector: 'jhi-feature-applicable-lov-popup',
    template: ''
})
export class FeatureApplicableLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected featureApplicablePopupService: FeatureApplicablePopupService
    ) {}

    ngOnInit() {
        this.featureApplicablePopupService.idFeatureType = undefined;
        this.featureApplicablePopupService.idFeature = undefined;
        this.featureApplicablePopupService.idProduct = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            this.featureApplicablePopupService
                .load(FeatureApplicableAsLovComponent as Component);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
