import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule, JhiLanguageHelper, Principal } from '../../shared';
import { JhiLanguageService } from 'ng-jhipster';
import { customHttpProvider } from '../../blocks/interceptor/http.provider';

import {
    FeatureApplicableService,
    FeatureApplicablePopupService,
    FeatureApplicableComponent,
    FeatureApplicableDialogComponent,
    FeatureApplicablePopupComponent,
    featureApplicableRoute,
    featureApplicablePopupRoute,
    FeatureApplicableResolvePagingParams,
    FeatureApplicableAsLovComponent,
    FeatureApplicableLovPopupComponent,
} from './';

import { CurrencyMaskModule } from 'ng2-currency-mask';
import { AutoCompleteModule,
         InputTextModule,
         InputTextareaModule,
         DataScrollerModule,
         PaginatorModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         FieldsetModule,
         ScheduleModule,
         ChartModule,
         DragDropModule,
         LightboxModule
} from 'primeng/primeng';

import { DataTableModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { StepsModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ButtonModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...featureApplicableRoute,
    ...featureApplicablePopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        MpmSharedEntityModule,
        RouterModule.forChild(ENTITY_STATES),
        TabsModule.forRoot(),
        TooltipModule.forRoot(),
        InputTextareaModule,
        ButtonModule,
        DataListModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        DataTableModule,
        StepsModule,
        SliderModule,
        RadioButtonModule
    ],
    exports: [
        FeatureApplicableComponent,
        FeatureApplicableAsLovComponent,
        FeatureApplicableLovPopupComponent,
    ],
    declarations: [
        FeatureApplicableComponent,
        FeatureApplicableDialogComponent,
        FeatureApplicablePopupComponent,
        FeatureApplicableAsLovComponent,
        FeatureApplicableLovPopupComponent,
    ],
    entryComponents: [
        FeatureApplicableComponent,
        FeatureApplicableDialogComponent,
        FeatureApplicablePopupComponent,
        FeatureApplicableAsLovComponent,
        FeatureApplicableLovPopupComponent,
    ],
    providers: [
        FeatureApplicablePopupService,
        FeatureApplicableResolvePagingParams,
        customHttpProvider(),
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmFeatureApplicableModule {
    constructor(
        protected languageService: JhiLanguageService,
        protected languageHelper: JhiLanguageHelper,
        protected principal: Principal) {
        this.languageHelper.language
            .subscribe((languageKey: string) => this.languageService.changeLanguage(languageKey));
        this.principal.identity(true).then((account) => {});
    }
}
