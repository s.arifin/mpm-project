import { BaseEntity } from './../../shared';

export class FeatureApplicable implements BaseEntity {
    constructor(
        public id?: any,
        public idApplicability?: any,
        public boolValue?: boolean,
        public dateFrom?: any,
        public dateThru?: any,
        public featureTypeId?: any,
        public featureId?: any,
        public productId?: any,
    ) {
        this.boolValue = false;
    }
}
