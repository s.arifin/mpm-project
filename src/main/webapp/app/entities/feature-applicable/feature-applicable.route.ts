import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { FeatureApplicableComponent } from './feature-applicable.component';
import { FeatureApplicableLovPopupComponent } from './feature-applicable-as-lov.component';
import { FeatureApplicablePopupComponent } from './feature-applicable-dialog.component';

@Injectable()
export class FeatureApplicableResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idApplicability,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const featureApplicableRoute: Routes = [
    {
        path: '',
        component: FeatureApplicableComponent,
        resolve: {
            'pagingParams': FeatureApplicableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.featureApplicable.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const featureApplicablePopupRoute: Routes = [
    {
        path: 'lov',
        component: FeatureApplicableLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.featureApplicable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'popup-new-list/:parent',
        component: FeatureApplicablePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.featureApplicable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'new',
        component: FeatureApplicablePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.featureApplicable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: ':id/edit',
        component: FeatureApplicablePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.featureApplicable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
