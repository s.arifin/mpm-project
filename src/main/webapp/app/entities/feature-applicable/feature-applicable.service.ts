import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';
import { FeatureApplicable } from './feature-applicable.model';
import { AbstractEntityService } from '../../shared/base/abstract-entity.service';

@Injectable()
export class FeatureApplicableService extends AbstractEntityService<FeatureApplicable> {

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) {
        super(http, dateUtils);
        this.resourceUrl =  SERVER_API_URL + 'api/feature-applicables';
        this.resourceSearchUrl = SERVER_API_URL + 'api/_search/feature-applicables';
   }

   protected convertItemFromServer(featureApplicable: FeatureApplicable): FeatureApplicable {
       const copy: FeatureApplicable = Object.assign({}, featureApplicable);
       copy.dateFrom = new Date(featureApplicable.dateFrom);
       copy.dateThru = new Date(featureApplicable.dateThru);
       return copy;
   }

}
