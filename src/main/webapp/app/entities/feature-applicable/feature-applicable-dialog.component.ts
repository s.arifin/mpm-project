import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { FeatureApplicable } from './feature-applicable.model';
import { FeatureApplicablePopupService } from './feature-applicable-popup.service';
import { FeatureApplicableService } from './feature-applicable.service';
import { ToasterService } from '../../shared';
import { FeatureType, FeatureTypeService } from '../feature-type';
import { Feature, FeatureService } from '../feature';
import { Product, ProductService } from '../product';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-feature-applicable-dialog',
    templateUrl: './feature-applicable-dialog.component.html'
})
export class FeatureApplicableDialogComponent implements OnInit {

    featureApplicable: FeatureApplicable;
    isSaving: boolean;
    idFeatureType: any;
    idFeature: any;
    idProduct: any;

    featuretypes: FeatureType[];

    features: Feature[];

    products: Product[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected featureApplicableService: FeatureApplicableService,
        protected featureTypeService: FeatureTypeService,
        protected featureService: FeatureService,
        protected productService: ProductService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.featureTypeService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.featuretypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.featureService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.features = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.productService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.featureApplicable.idApplicability !== undefined) {
            this.subscribeToSaveResponse(
                this.featureApplicableService.update(this.featureApplicable));
        } else {
            this.subscribeToSaveResponse(
                this.featureApplicableService.create(this.featureApplicable));
        }
    }

    protected subscribeToSaveResponse(result: Observable<FeatureApplicable>) {
        result.subscribe((res: FeatureApplicable) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: FeatureApplicable) {
        this.eventManager.broadcast({ name: 'featureApplicableListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'featureApplicable saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
        window.history.back();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'featureApplicable Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackFeatureTypeById(index: number, item: FeatureType) {
        return item.idFeatureType;
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    trackProductById(index: number, item: Product) {
        return item.idProduct;
    }
}

@Component({
    selector: 'jhi-feature-applicable-popup',
    template: ''
})
export class FeatureApplicablePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected featureApplicablePopupService: FeatureApplicablePopupService
    ) {}

    ngOnInit() {
        this.featureApplicablePopupService.idFeatureType = undefined;
        this.featureApplicablePopupService.idFeature = undefined;
        this.featureApplicablePopupService.idProduct = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.featureApplicablePopupService
                    .open(FeatureApplicableDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.featureApplicablePopupService.parent = params['parent'];
                this.featureApplicablePopupService
                    .open(FeatureApplicableDialogComponent as Component);
            } else {
                this.featureApplicablePopupService
                    .open(FeatureApplicableDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
