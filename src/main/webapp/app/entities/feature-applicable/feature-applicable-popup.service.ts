import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { FeatureApplicable } from './feature-applicable.model';
import { FeatureApplicableService } from './feature-applicable.service';

@Injectable()
export class FeatureApplicablePopupService {
    protected ngbModalRef: NgbModalRef;
    idFeatureType: any;
    idFeature: any;
    idProduct: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected featureApplicableService: FeatureApplicableService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.featureApplicableService.find(id).subscribe((data) => {
                    // if (data.dateFrom) {
                    //    data.dateFrom = this.datePipe
                    //        .transform(data.dateFrom, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    // if (data.dateThru) {
                    //    data.dateThru = this.datePipe
                    //        .transform(data.dateThru, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.featureApplicableModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new FeatureApplicable();
                    data.featureTypeId = this.idFeatureType;
                    data.featureId = this.idFeature;
                    data.productId = this.idProduct;
                    this.ngbModalRef = this.featureApplicableModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    featureApplicableModalRef(component: Component, featureApplicable: FeatureApplicable): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.featureApplicable = featureApplicable;
        modalRef.componentInstance.idFeatureType = this.idFeatureType;
        modalRef.componentInstance.idFeature = this.idFeature;
        modalRef.componentInstance.idProduct = this.idProduct;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.featureApplicableLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    featureApplicableLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idFeatureType = this.idFeatureType;
        modalRef.componentInstance.idFeature = this.idFeature;
        modalRef.componentInstance.idProduct = this.idProduct;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
