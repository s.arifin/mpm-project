import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { FeatureApplicable } from './feature-applicable.model';
import { FeatureApplicableService } from './feature-applicable.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-feature-applicable-as-list',
    templateUrl: './feature-applicable-as-list.component.html'
})
export class FeatureApplicableAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idFeatureType: any;
    @Input() idFeature: any;
    @Input() idProduct: any;

    currentAccount: any;
    featureApplicables: FeatureApplicable[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        protected featureApplicableService: FeatureApplicableService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'idApplicability';
        this.reverse = 'asc';
        this.first = 0;
    }

    loadAll() {
        this.featureApplicableService.queryFilterBy({
            idFeatureType: this.idFeatureType,
            idFeature: this.idFeature,
            idProduct: this.idProduct,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/feature-applicable', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInFeatureApplicables();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idFeatureType']) {
            this.loadAll();
        }
        if (changes['idFeature']) {
            this.loadAll();
        }
        if (changes['idProduct']) {
            this.loadAll();
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: FeatureApplicable) {
        return item.idApplicability;
    }

    registerChangeInFeatureApplicables() {
        this.eventSubscriber = this.eventManager.subscribe('featureApplicableListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idApplicability') {
            result.push('idApplicability');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.featureApplicables = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.featureApplicableService.execute(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.subscribeToSaveResponse(
                this.featureApplicableService.update(event.data));
        } else {
            this.subscribeToSaveResponse(
                this.featureApplicableService.create(event.data));
        }
    }

    protected subscribeToSaveResponse(result: Observable<FeatureApplicable>) {
        result.subscribe((res: FeatureApplicable) => this.onSaveSuccess(res));
    }

    protected onSaveSuccess(result: FeatureApplicable) {
        this.eventManager.broadcast({ name: 'featureApplicableListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'featureApplicable saved !');
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.featureApplicableService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'featureApplicableListModification',
                        content: 'Deleted an featureApplicable'
                    });
                });
            }
        });
    }
}
