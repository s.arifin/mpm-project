import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Mokita } from './mokita.model';
import { ProspectPerson } from '../prospect-person/prospect-person.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class MokitaService {

    protected resourceCUrl = process.env.API_C_URL + '/api/mokita';

    constructor(protected http: Http) { }

    prospect(prospectKorsals: ProspectPerson[]): Observable<ResponseWrapper> {
        const copy = JSON.stringify(prospectKorsals);

        const options = new RequestOptions();
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(this.resourceCUrl + '/prospect-cancel', copy, options).map((res: Response) => {
            return res.json();
        });
    }

    spk(mokita: Mokita): Observable<Mokita> {
        const copy = this.convert(mokita);
        return this.http.post(this.resourceCUrl + '/spk', copy).map((res: Response) => {
            return res.json();
        });
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(mokita: Mokita): Mokita {
        const copy: Mokita = Object.assign({}, mokita);
        return copy;
    }
}
