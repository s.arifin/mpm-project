import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { MpmSharedModule } from '../../shared';
import {
    MokitaService,
} from './';

@NgModule({
    imports: [
        MpmSharedModule,
    ],
    exports: [
    ],
    declarations: [
    ],
    entryComponents: [
    ],
    providers: [
        MokitaService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMokitaModule {}
