import { BaseEntity } from './../../shared';

export class PreRequestUnitInternal implements BaseEntity {
    constructor(
        public id?: any,
        public idRequirement?: string,
        public dtCreated?: Date,
        public requirementNumber?: string,
        public productName?: string,
        public productCode?: string,
        public colorName?: string,
        public salesId?: string,
        public salesName?: string,
        public korsalName?: string
    ) {
    }
}
