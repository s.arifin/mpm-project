import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';
import {
    PreRequestUnitInternalComponent,
    PreRequestUnitInternal,
    preRequestUnitInternalRoute,
    PreRequestUnitInternalComponentResolvePagingParams
} from './';
import {MpmSharedModule} from '../../shared';

const ENTITY_STATES = [
    ...preRequestUnitInternalRoute,
];

import {DataTableModule} from 'primeng/primeng';

@NgModule({
    imports: [
        MpmSharedModule,
        DataTableModule,
        RouterModule.forChild(ENTITY_STATES),
    ],
    exports: [

    ],
    declarations: [
        PreRequestUnitInternalComponent
    ],
    entryComponents: [
        PreRequestUnitInternalComponent
    ],
    providers: [
        PreRequestUnitInternalComponentResolvePagingParams
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPreRequestUnitInternalModule {}
