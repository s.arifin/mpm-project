import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PreRequestUnitInternal } from './';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { Principal, ITEMS_PER_PAGE, ResponseWrapper } from '../../shared';
import { RequestUnitInternalService } from '../request-unit-internal/request-unit-internal.service';
import { CommonUtilService } from '../../shared';
import { LazyLoadEvent } from 'primeng/primeng';

@Component({
    selector: 'jhi-pre-request-unit-internal-component',
    templateUrl: './pre-request-unit-internal.component.html'
})

export class PreRequestUnitInternalComponent implements OnInit {

    public preRequestUnitInternals: Array<PreRequestUnitInternal>;
    public error: any;
    public success: any;
    public currentSearch: string;
    public routeData: any;
    public links: any;
    public totalItems: any;
    public queryCount: any;
    public itemsPerPage: any;
    public page: any;
    public predicate: any;
    public previousPage: any;
    public reverse: any;

    constructor(
        protected commontUtilService: CommonUtilService,
        protected principalService: Principal,
        protected requestUnitInternalService: RequestUnitInternalService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected parseLinks: JhiParseLinks,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
    }

    ngOnInit() {
        this.loadAll();
    }

    protected loadAll(): void {
        this.requestUnitInternalService.findPreRequestUnitInternal({
            page: this.page - 1,
            size: this.itemsPerPage,
            idinternal: this.principalService.getIdInternal(),
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            )
    }

    protected sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        console.log('data', data);
        this.preRequestUnitInternals = data;
    }

    protected onError(error) {
        this.commontUtilService.showError(error);
    }

    public loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
}
