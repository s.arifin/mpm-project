import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';
import { UserRouteAccessService } from '../../shared';

import {PreRequestUnitInternalComponent} from './pre-request-unit-internal.component';

@Injectable()
export class PreRequestUnitInternalComponentResolvePagingParams implements Resolve<any> {
    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequirement,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const preRequestUnitInternalRoute: Routes = [
    {
        path: 'pre-request-unit-internal',
        component: PreRequestUnitInternalComponent,
        resolve: {
            'pagingParams': PreRequestUnitInternalComponentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestUnitInternal.pre'
        },
        canActivate: [UserRouteAccessService]
    }
];
