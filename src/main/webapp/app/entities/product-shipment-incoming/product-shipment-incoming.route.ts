import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ProductShipmentIncomingComponent } from './product-shipment-incoming.component';
import { ProductShipmentIncomingEditComponent } from './product-shipment-incoming-edit.component';
import { ProductShipmentIncomingPopupComponent } from './product-shipment-incoming-dialog.component';

@Injectable()
export class ProductShipmentIncomingResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idShipment,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const productShipmentIncomingRoute: Routes = [
    {
        path: 'product-shipment-incoming',
        component: ProductShipmentIncomingComponent,
        resolve: {
            'pagingParams': ProductShipmentIncomingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productShipmentIncoming.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const productShipmentIncomingPopupRoute: Routes = [
    {
        path: 'product-shipment-incoming-popup-new',
        component: ProductShipmentIncomingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productShipmentIncoming.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-shipment-incoming-new',
        component: ProductShipmentIncomingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productShipmentIncoming.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-shipment-incoming/:id/edit',
        component: ProductShipmentIncomingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productShipmentIncoming.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-shipment-incoming/:route/:page/:id/edit',
        component: ProductShipmentIncomingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productShipmentIncoming.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-shipment-incoming/:id/popup-edit',
        component: ProductShipmentIncomingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productShipmentIncoming.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
