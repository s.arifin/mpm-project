import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ProductShipmentIncoming } from './product-shipment-incoming.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ProductShipmentIncomingService {
    protected itemValues: ProductShipmentIncoming[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/product-shipment-incomings';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/product-shipment-incomings';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(productShipmentIncoming: ProductShipmentIncoming): Observable<ProductShipmentIncoming> {
        const copy = this.convert(productShipmentIncoming);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(productShipmentIncoming: ProductShipmentIncoming): Observable<ProductShipmentIncoming> {
        const copy = this.convert(productShipmentIncoming);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<ProductShipmentIncoming> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(productShipmentIncoming: ProductShipmentIncoming, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(productShipmentIncoming);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<ProductShipmentIncoming> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<ProductShipmentIncoming[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ProductShipmentIncoming.
     */
    protected convertItemFromServer(json: any): ProductShipmentIncoming {
        const entity: ProductShipmentIncoming = Object.assign(new ProductShipmentIncoming(), json);
        if (entity.dateSchedulle) {
            entity.dateSchedulle = new Date(entity.dateSchedulle);
        }
        return entity;
    }

    /**
     * Convert a ProductShipmentIncoming to a JSON which can be sent to the server.
     */
    protected convert(productShipmentIncoming: ProductShipmentIncoming): ProductShipmentIncoming {
        if (productShipmentIncoming === null || productShipmentIncoming === {}) {
            return {};
        }
        // const copy: ProductShipmentIncoming = Object.assign({}, productShipmentIncoming);
        const copy: ProductShipmentIncoming = JSON.parse(JSON.stringify(productShipmentIncoming));

        // copy.dateSchedulle = this.dateUtils.toDate(productShipmentIncoming.dateSchedulle);
        return copy;
    }

    protected convertList(productShipmentIncomings: ProductShipmentIncoming[]): ProductShipmentIncoming[] {
        const copy: ProductShipmentIncoming[] = productShipmentIncomings;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ProductShipmentIncoming[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
