import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProductShipmentIncoming } from './product-shipment-incoming.model';
import { ProductShipmentIncomingPopupService } from './product-shipment-incoming-popup.service';
import { ProductShipmentIncomingService } from './product-shipment-incoming.service';
import { ToasterService } from '../../shared';
import { ShipmentType, ShipmentTypeService } from '../shipment-type';
import { ShipTo, ShipToService } from '../ship-to';
import { PostalAddress, PostalAddressService } from '../postal-address';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-product-shipment-incoming-dialog',
    templateUrl: './product-shipment-incoming-dialog.component.html'
})
export class ProductShipmentIncomingDialogComponent implements OnInit {

    productShipmentIncoming: ProductShipmentIncoming;
    isSaving: boolean;
    idShipmentType: any;
    idShipFrom: any;
    idShipTo: any;
    idAddressFrom: any;
    idAddressTo: any;

    shipmenttypes: ShipmentType[];

    shiptos: ShipTo[];

    postaladdresses: PostalAddress[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected productShipmentIncomingService: ProductShipmentIncomingService,
        protected shipmentTypeService: ShipmentTypeService,
        protected shipToService: ShipToService,
        protected postalAddressService: PostalAddressService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.shipmentTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmenttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipToService.query()
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.postalAddressService.query()
            .subscribe((res: ResponseWrapper) => { this.postaladdresses = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.productShipmentIncoming.idShipment !== undefined) {
            this.subscribeToSaveResponse(
                this.productShipmentIncomingService.update(this.productShipmentIncoming));
        } else {
            this.subscribeToSaveResponse(
                this.productShipmentIncomingService.create(this.productShipmentIncoming));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProductShipmentIncoming>) {
        result.subscribe((res: ProductShipmentIncoming) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ProductShipmentIncoming) {
        this.eventManager.broadcast({ name: 'productShipmentIncomingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'productShipmentIncoming saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'productShipmentIncoming Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackShipmentTypeById(index: number, item: ShipmentType) {
        return item.idShipmentType;
    }

    trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }

    trackPostalAddressById(index: number, item: PostalAddress) {
        return item.idContact;
    }
}

@Component({
    selector: 'jhi-product-shipment-incoming-popup',
    template: ''
})
export class ProductShipmentIncomingPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected productShipmentIncomingPopupService: ProductShipmentIncomingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.productShipmentIncomingPopupService
                    .open(ProductShipmentIncomingDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.productShipmentIncomingPopupService.parent = params['parent'];
                this.productShipmentIncomingPopupService
                    .open(ProductShipmentIncomingDialogComponent as Component);
            } else {
                this.productShipmentIncomingPopupService
                    .open(ProductShipmentIncomingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
