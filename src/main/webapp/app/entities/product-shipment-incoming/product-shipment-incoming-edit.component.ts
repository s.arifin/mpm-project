import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProductShipmentIncoming } from './product-shipment-incoming.model';
import { ProductShipmentIncomingService } from './product-shipment-incoming.service';
import { ToasterService} from '../../shared';
import { ShipmentType, ShipmentTypeService } from '../shipment-type';
import { ShipTo, ShipToService } from '../ship-to';
import { PostalAddress, PostalAddressService } from '../postal-address';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-product-shipment-incoming-edit',
    templateUrl: './product-shipment-incoming-edit.component.html'
})
export class ProductShipmentIncomingEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    productShipmentIncoming: ProductShipmentIncoming;
    isSaving: boolean;
    idShipment: any;
    paramPage: number;
    routeId: number;

    shipmenttypes: ShipmentType[];

    shiptos: ShipTo[];

    postaladdresses: PostalAddress[];

    constructor(
        protected alertService: JhiAlertService,
        protected productShipmentIncomingService: ProductShipmentIncomingService,
        protected shipmentTypeService: ShipmentTypeService,
        protected shipToService: ShipToService,
        protected postalAddressService: PostalAddressService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.productShipmentIncoming = new ProductShipmentIncoming();
        this.routeId = 0;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idShipment = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        this.shipmentTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmenttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipToService.query()
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.postalAddressService.query()
            .subscribe((res: ResponseWrapper) => { this.postaladdresses = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.productShipmentIncomingService.find(this.idShipment).subscribe((productShipmentIncoming) => {
            this.productShipmentIncoming = productShipmentIncoming;
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['product-shipment-incoming', { page: this.paramPage }]);
        }
    }

    save() {
        this.isSaving = true;
        if (this.productShipmentIncoming.idShipment !== undefined) {
            this.subscribeToSaveResponse(
                this.productShipmentIncomingService.update(this.productShipmentIncoming));
        } else {
            this.subscribeToSaveResponse(
                this.productShipmentIncomingService.create(this.productShipmentIncoming));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProductShipmentIncoming>) {
        result.subscribe((res: ProductShipmentIncoming) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ProductShipmentIncoming) {
        this.eventManager.broadcast({ name: 'productShipmentIncomingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'productShipmentIncoming saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'productShipmentIncoming Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackShipmentTypeById(index: number, item: ShipmentType) {
        return item.idShipmentType;
    }

    trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }

    trackPostalAddressById(index: number, item: PostalAddress) {
        return item.idContact;
    }
}
