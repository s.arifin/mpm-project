import { BaseEntity } from '../../shared';

export class PartyDocument implements BaseEntity {
    constructor(
        public id?: any,
        public idDocument?: any,
        public contentContentType?: string,
        public content?: any,
        public note?: string,
        public dateCreate?: any,
        public partyId?: any,
        public documentTypeId?: any,
    ) {
    }
}
