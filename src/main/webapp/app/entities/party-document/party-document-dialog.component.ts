import {Component, OnInit, OnDestroy, ElementRef} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService, JhiDataUtils} from 'ng-jhipster';

import {PartyDocument} from './party-document.model';
import {PartyDocumentPopupService} from './party-document-popup.service';
import {PartyDocumentService} from './party-document.service';
import {ToasterService} from '../../shared';
import { Party, PartyService } from '../party';
import { DocumentType, DocumentTypeService } from '../document-type';
import { ResponseWrapper } from '../../shared';
import * as DocumentTypeConstants from '../../shared/constants/document-type.constants';
import * as ConfigConstants from '../../shared/constants/config.constants';

@Component({
    selector: 'jhi-party-document-dialog',
    templateUrl: './party-document-dialog.component.html'
})
export class PartyDocumentDialogComponent implements OnInit {
    public isInsertFile: string;
    public maxFileSize: number;
    public isValidImage: boolean;

    partyDocument: PartyDocument;
    isSaving: boolean;

    parties: Party[];

    documenttypes: DocumentType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected dataUtils: JhiDataUtils,
        protected alertService: JhiAlertService,
        protected partyDocumentService: PartyDocumentService,
        protected partyService: PartyService,
        protected documentTypeService: DocumentTypeService,
        protected elementRef: ElementRef,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.isInsertFile = null;
        this.isValidImage = false;
        this.maxFileSize = ConfigConstants.MAX_POST_SIZE_IMAGE;
    }

    ngOnInit() {
        this.partyDocument.dateCreate = new Date();
        this.isSaving = false;
        this.partyService.query()
            .subscribe((res: ResponseWrapper) => { this.parties = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.documentTypeService.findByIdParent(DocumentTypeConstants.PERSON)
            .subscribe((res: ResponseWrapper) => { this.documenttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        const bytes: number = event.target.files[0].size;
        if (bytes < ConfigConstants.MAX_POST_SIZE_IMAGE) {
            this.dataUtils.setFileData(event, entity, field, isImage);
            this.isValidImage = false;
            this.isInsertFile = 'valid';
        } else {
            this.isValidImage = true;
            this.clearInputImage('content', 'contentContentType', 'fileImage');
            this.isInsertFile = null;
        }
    }

    public setImageData(event: any): void {
        this.setFileData(event, this.partyDocument, 'content', true);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.partyDocument, this.elementRef, field, fieldContentType, idInput);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.partyDocument.idDocument !== undefined) {
            this.subscribeToSaveResponse(
                this.partyDocumentService.update(this.partyDocument));
        } else {
            this.subscribeToSaveResponse(
                this.partyDocumentService.create(this.partyDocument));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PartyDocument>) {
        result.subscribe((res: PartyDocument) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: PartyDocument) {
        this.eventManager.broadcast({ name: 'partyDocumentListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'partyDocument saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'partyDocument Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackPartyById(index: number, item: Party) {
        return item.idParty;
    }

    trackDocumentTypeById(index: number, item: DocumentType) {
        return item.idDocumentType;
    }
}

@Component({
    selector: 'jhi-party-document-popup',
    template: ''
})
export class PartyDocumentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected partyDocumentPopupService: PartyDocumentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.partyDocumentPopupService
                    .open(PartyDocumentDialogComponent as Component, params['id'], 'edit');
            } else {
                if (params['idparty']) {
                    this.partyDocumentPopupService
                        .open(PartyDocumentDialogComponent as Component, params['idparty'], 'new');
                } else {
                    this.partyDocumentPopupService
                        .open(PartyDocumentDialogComponent as Component);
                }
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
