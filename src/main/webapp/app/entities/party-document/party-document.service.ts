import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';

import { PartyDocument } from './party-document.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class PartyDocumentService {
    protected itemValues: PartyDocument[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/party-documents';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/party-documents';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(partyDocument: PartyDocument): Observable<PartyDocument> {
        const copy = this.convert(partyDocument);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(partyDocument: PartyDocument): Observable<PartyDocument> {
        const copy = this.convert(partyDocument);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<PartyDocument> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterByEmail(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(SERVER_API_URL + 'api/party-documents/filterBy/email', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterByDocType(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterByDocType', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, partyDocument: PartyDocument): Observable<PartyDocument> {
        const copy = this.convert(partyDocument);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, partyDocuments: PartyDocument[]): Observable<PartyDocument[]> {
        const copy = this.convertList(partyDocuments);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateCreate) {
            entity.dateCreate = new Date(entity.dateCreate);
        }
    }

    protected convert(partyDocument: PartyDocument): PartyDocument {
        if (partyDocument === null || partyDocument === {}) {
            return {};
        }
        // const copy: PartyDocument = Object.assign({}, partyDocument);
        const copy: PartyDocument = JSON.parse(JSON.stringify(partyDocument));

        // copy.dateCreate = this.dateUtils.toDate(partyDocument.dateCreate);
        return copy;
    }

    protected convertList(partyDocuments: PartyDocument[]): PartyDocument[] {
        const copy: PartyDocument[] = partyDocuments;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: PartyDocument[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
