import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { Good } from './good.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class GoodService {
    protected itemValues: Good[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl = 'api/goods';
    protected resourceSearchUrl = 'api/_search/goods';
    protected resourceCUrl = process.env.API_C_URL + '/api/goods';
    protected dummy = 'http://localhost:52374/api/goods';

    constructor(protected http: Http) { }

    create(good: Good): Observable<Good> {
        const copy = this.convert(good);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(good: Good): Observable<Good> {
        const copy = this.convert(good);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<Good> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, good: Good): Observable<Good> {
        const copy = this.convert(good);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, goods: Good[]): Observable<Good[]> {
        const copy = this.convertList(goods);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(good: Good): Good {
        if (good === null || good === {}) {
            return {};
        }
        // const copy: Good = Object.assign({}, good);
        const copy: Good = JSON.parse(JSON.stringify(good));
        return copy;
    }

    protected convertList(goods: Good[]): Good[] {
        const copy: Good[] = goods;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: Good[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
        this.itemValues = [];
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    queryFilterByFeatureC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/byinternalfacility`, options)
        .map((res: Response) => this.convertResponse(res));
   }

   queryGetyearsC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/getyear`, options)
        .map((res: Response) => this.convertResponse(res));
   }

   queryGetFeatureC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/getfeature`, options)
        .map((res: Response) => this.convertResponse(res));
    }

    queryByInternalC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/byInternal`, options)
        .map((res: Response) => this.convertResponse(res));
   }

   queryYearsByProductC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/getyearByProduct`, options)
        .map((res: Response) => this.convertResponse(res));
   }

   queryGetFeatureByYearProductC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/getfeatureByYearProduct`, options)
        .map((res: Response) => this.convertResponse(res));
    }
}
