import { BaseEntity } from './../../shared';
import { Product } from './../product';

export class Good extends Product {
    constructor(
        public id?: any,
        public uomId?: any,
    ) {
        super();
    }
}
