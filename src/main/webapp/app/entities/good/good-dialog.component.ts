import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {Good} from './good.model';
import {GoodPopupService} from './good-popup.service';
import {GoodService} from './good.service';
import {ToasterService} from '../../shared';
import { Uom, UomService } from '../uom';
import { Feature, FeatureService } from '../feature';
import { ProductCategory, ProductCategoryService } from '../product-category';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-good-dialog',
    templateUrl: './good-dialog.component.html'
})
export class GoodDialogComponent implements OnInit {

    good: Good;
    isSaving: boolean;

    uoms: Uom[];

    features: Feature[];

    productcategories: ProductCategory[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected goodService: GoodService,
        protected uomService: UomService,
        protected featureService: FeatureService,
        protected productCategoryService: ProductCategoryService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.uomService.query()
            .subscribe((res: ResponseWrapper) => { this.uoms = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.featureService.query()
            .subscribe((res: ResponseWrapper) => { this.features = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.productCategoryService.query()
            .subscribe((res: ResponseWrapper) => { this.productcategories = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.good.idProduct !== undefined) {
            this.subscribeToSaveResponse(
                this.goodService.update(this.good));
        } else {
            this.subscribeToSaveResponse(
                this.goodService.create(this.good));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Good>) {
        result.subscribe((res: Good) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Good) {
        this.eventManager.broadcast({ name: 'goodListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'good saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'good Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackUomById(index: number, item: Uom) {
        return item.idUom;
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    trackProductCategoryById(index: number, item: ProductCategory) {
        return item.idCategory;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-good-popup',
    template: ''
})
export class GoodPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected goodPopupService: GoodPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.goodPopupService
                    .open(GoodDialogComponent as Component, params['id']);
            } else {
                this.goodPopupService
                    .open(GoodDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
