import { Component, Input, OnInit, OnDestroy, ViewChild, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ShipmentOutgoing } from './../shipment-outgoing.model';
import { ShipmentOutgoingMatching } from './../shipment-outgoing-matching.model';
import { ShipmentOutgoingService } from './../shipment-outgoing.service';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { VehicleSalesOrderService, VehicleSalesOrder } from '../../vehicle-sales-order';
import { PickingSlip, PickingSlipService } from '../../picking-slip';
import { CustomShipment } from './../custom-shipment.model';
import { ShipmentOutgoingParameters } from './../shipment-outgoing-parameter.model';

import * as ShipmentOutgoingConstrants from '../../../shared/constants/shipmentOutgoing.constrants'

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { ReportUtilService } from '../../../shared/report/report-util.service';

@Component({
    selector: 'jhi-shipment-outgoing-grid-delivery',
    templateUrl: './shipment-outgoing-grid-delivery.component.html'
})
export class ShipmentOutgoingGridDeliveryComponent implements OnInit, OnDestroy {

    @Input() idStatusType: number;

    @Input() forPrint: boolean;

    @Output()
    fnCallback = new EventEmitter();

    dataServiceSubscriber: Subscription;
    shipmentOutgoing: ShipmentOutgoing;
    customShipment: CustomShipment[];
    customShipments: CustomShipment;
    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    vso: VehicleSalesOrder;
    pickingSlip: PickingSlip;
    printBASTdialog: boolean;
    manualMatching: boolean;
    errorprint: boolean;
    rowdatatmp: CustomShipment;
    rowdatamatching: CustomShipment;
    bast_unit: boolean;
    bast_acc: boolean;
    bast_mat_prom: boolean;
    buttonVisible: boolean;
    selectedSalesman: string;
    filtered: any;
    idInternalHere: any;
    isFiltered: boolean;
    shipmentMatching: ShipmentOutgoingMatching[];
    shipmentOutgoingParameters: ShipmentOutgoingParameters;
    constructor(
        private shipmentOutgoingService: ShipmentOutgoingService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private loadingService: LoadingService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private vsoService: VehicleSalesOrderService,
        private pickingSlipService: PickingSlipService,
        private toasterService: ToasterService,
        private reportUtilService: ReportUtilService,
    ) {
        this.buttonVisible = true;
        this.bast_unit = false;
        this.bast_acc = false;
        this.bast_mat_prom = false;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        // this.pickingSlipService.query({
        //     idstatustype: 25,
        //     page: this.page - 1,
        //     size: this.itemsPerPage,
        //     // sort: this.sortPicking()
        //    }).subscribe(
        //     (res: ResponseWrapper) => this.onSuccess(res.json, res.headers, 2),
        //     (res: ResponseWrapper) => this.onError(res.json)
        // );
    }

    loadAll() {
        console.log('testload');
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.shipmentOutgoingService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers, 2),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }
        this.shipmentOutgoingService.queryFilterBy({
            idstatustype: this.idStatusType,
            idInternal: this.principal.getIdInternal(),
            queryFor: 'pickingdo',
            page: this.page - 1,
            size: this.itemsPerPage,
            // sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers, 2),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
            this.idInternalHere = this.principal.getIdInternal();
        });
        this.dataServiceSubscriber = this.shipmentOutgoingService.currentData.subscribe(
            (response) => {
                if (response === null) {
                    if (this.idStatusType == null || this.idStatusType === undefined) {
                        this.idStatusType = 25;
                    }
                    this.loadAll();
                }
            });
        this.registerChangeInShipmentOutgoings();
    }
    cancelDo(rowdata): Promise<any> {
        return new Promise<any>(
            (res) => {
                console.log('cancel', rowdata);
                // console.log('KIRIM_GUDANG', ShipmentOutgoingConstrants.KIRIM_GUDANG);
                this.shipmentOutgoingService.executeProcess(ShipmentOutgoingConstrants.CANCEL, null, rowdata).subscribe(
                    (value) => console.log('this: ', value),
                    (err) => console.log(err),
                    () => {
                        this.loadAll();
                        res()
                    }
                );
            }
        )
    }
    changesharetmp(datatmp) {
        this.shipmentOutgoingService.changeShareDataTmp(datatmp);

    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/shipment-outgoing'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/shipment-outgoing', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/shipment-outgoing', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
        this.dataServiceSubscriber.unsubscribe();
    }

    trackId(index: number, item: ShipmentOutgoing) {
        return item.idShipment;
    }

    registerChangeInShipmentOutgoings() {
        this.eventSubscriber = this.eventManager.subscribe('shipmentOutgoingListModification',
            (response) => {
                if (this.idStatusType == null || this.idStatusType === undefined) {
                    this.idStatusType = 25;
                }
                this.loadAll();
            }
        );
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idShipment') {
            result.push('idShipment');
        }
        return result;
    }
    sortPicking() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idSlip') {
            result.push('idSlip');
        }
        return result;
    }

    private onSuccess(data, headers, type) {
        this.loadingService.loadingStop();
        this.customShipment = new Array<CustomShipment>();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        // if (data !== null) {
        //     if (data.length > 0) {
        //             for (let i = 0; i < data.length; i++) {
        //                 const obj = data[i];
        //                 obj.dateSchedulle = new Date (obj.dateSchedulle)
        //             }
        //         } else {
        //             data.dateSchedulle = new Date (data.dateSchedulle)
        //         }
        // }
        this.customShipment = data;
        // if (type === 1) {
        //     this.deliveryOrder = data;
        // } else if (type === 2) {
        //     this.pickingSlip = data;
        // }
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.shipmentOutgoingService.executeProcess(0, null, data).subscribe(
            (value) => console.log('this: ', value),
            (err) => console.log(err),
            () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    goToDetail(delivery): Promise<any> {
        return new Promise<any>(
            (res) => {
                this.pickingSlipService.find(delivery).subscribe(
                    (data) => {
                        this.pickingSlip = data;
                        console.log('kirim detail', this.pickingSlip);
                        this.shipmentOutgoingService.changeShareDataTmp(this.pickingSlip);
                        this.router.navigate(['../', { outlets: { popup: ['shipment-outgoing-detail-delivery'] } }]);
                    }
                )
            }
        )
    }

    cancel() {
        this.printBASTdialog = false;
    }
    print(data: CustomShipment) {
        this.rowdatatmp = data;
        console.log('rowdatatmp', this.rowdatatmp);
        // if (data.acc1 !== true || data.acc2 !== true || data.acc3 !== true || data.acc4 !== true || data.acc5 !== true || data.acc6 !== true || data.acc7 !== true) {
        //     console.log('error');
        //     this.errorprint = true;
        //     this.printBASTdialog = true;
        // } else {
        console.log('fix');
        this.errorprint = false;
        this.printBASTdialog = true;
        // }
    }

    matchingManual(selected: CustomShipment): Promise<any> {
        return new Promise<any>(
            (res) => {
                console.log('kirim detail', selected);
                this.shipmentOutgoingService.changeShareDataTmp(selected);
                this.router.navigate(['../', { outlets: { popup: ['shipment-outgoing-manual-matching'] } }]);
                // this.rowdatamatching = data;
                // console.log('rowdatamatching', this.rowdatamatching);
                // this.manualMatching = true;
                // this.shipmentOutgoingService.passingCustomData(this.rowdatamatching);
            }
        )
    }

    printWithError() {
        console.log('errorprint');
        this.printBASTdialog = false;
    }
    printBAST() {
        this.buttonVisible = false;
        this.shipmentOutgoingService.queryCekCetakan({
            pidshipment: 'idshipment:' + this.rowdatatmp.idShipment,
            pinternal: 'idinternal:' + this.principal.getIdInternal()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccessCek(res, res.headers),
            (res: ResponseWrapper) => this.onErrorCek(res.json)
        );
    }
    doPrint(idframe, idInternal) {
        if (this.bast_unit === true) {
            this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
            if (this.idInternalHere === idInternal) {
                this.reportUtilService.viewFile('/api/report/bast_unit/pdf', { no_vdo: idframe });
            } else {
                this.reportUtilService.viewFile('/api/report/BAST_Unit_ke_Other_Dealer/pdf', { no_vdo: idframe });
            }
            console.log('cosole1:');
        }
        if (this.bast_acc === true) {
            this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
            console.log('cosole2:');
            if (this.idInternalHere === idInternal) {
                this.reportUtilService.viewFile('/api/report/bast_accesories/pdf', { no_vdo: idframe });
            } else {
                this.reportUtilService.viewFile('/api/report/bast_accesories_reqnot/pdf', { no_vdo: idframe });
            }
        }
        if (this.bast_mat_prom === true) {
            console.log('console3:');
            this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
            this.reportUtilService.viewFile('/api/report/bast_material_non_part/pdf', { no_vdo: idframe });
        }
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.shipmentOutgoingService.update(event.data)
                .subscribe((res: ShipmentOutgoing) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.shipmentOutgoingService.create(event.data)
                .subscribe((res: ShipmentOutgoing) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: ShipmentOutgoing) {
        this.toasterService.showToaster('info', 'ShipmentOutgoing Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.shipmentOutgoingService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'shipmentOutgoingListModification',
                        content: 'Deleted an shipmentOutgoing'
                    });
                });
            }
        });
    }

    filter() {
        this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered);
        // this.page = 0;
        this.shipmentOutgoingParameters = new ShipmentOutgoingParameters();
        this.shipmentOutgoingParameters.internalId = this.principal.getIdInternal();
        this.shipmentOutgoingParameters.idStatusType = 18;
        this.shipmentOutgoingParameters.queryFor = 'pickingdo';

        if (this.selectedSalesman !== null && this.selectedSalesman !== undefined) {
            this.shipmentOutgoingParameters.salesmanId = this.selectedSalesman;
        }

        if (this.filtered !== null && this.filtered !== undefined) {
            this.shipmentOutgoingParameters.dataParam = this.filtered;
        }

        this.shipmentOutgoingService.searchUnitPrep({
            shipmentOutgoingPTO: this.shipmentOutgoingParameters,
            idInternal: this.principal.getIdInternal(),
            // sort: this.sort()
        })
            .toPromise()
            .then((res) => {
                console.log('filter data isi nama == ', res)
                this.loadingService.loadingStop(),
                    this.onSuccessFilter(res.json, res.headers)
            }
            );

        console.log('tanggal 1', this.shipmentOutgoingParameters.orderDateFrom);
        console.log('tanggal 2', this.shipmentOutgoingParameters.orderDateThru);
        console.log('salesman 2', this.shipmentOutgoingParameters.salesmanId);
    }

    protected onSuccessFilter(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.customShipment = data;
        // if (data.length === 0) {
        //     this.unitDeliverables = [...this.unitDeliverables, this.unitDeliverable];
        // }
    }

    protected onSuccessCek(sdatas, headers) {
        this.shipmentOutgoingService.find(this.rowdatatmp.idShipment).subscribe(
            (data) => {
                this.shipmentOutgoing = data;
                this.vsoService.find(this.rowdatatmp.idOrder, this.principal.getIdInternal()).subscribe(
                    (data2) => {
                        this.vso = data2
                        this.shipmentOutgoingService.executeProcess(ShipmentOutgoingConstrants.PRINT, this.vso.internalId, this.shipmentOutgoing).subscribe(
                            (value) => {
                                console.log('value', value);
                                if (this.idStatusType == null || this.idStatusType === undefined) {
                                    this.idStatusType = 25;
                                }
                                this.doPrint(this.rowdatatmp.idframe, this.rowdatatmp.idInternalsrc);
                                // this.loadAll();
                                this.printBASTdialog = false;
                                this.fnCallback.emit();
                                this.shipmentOutgoingService.GLInventoryTransfer(this.rowdatatmp.idframe).subscribe(
                                    (resGL) => { },
                                    (err) => { }
                                );
                                // const obj = {
                                //     idorder : this.rowdatatmp.idOrder
                                // }
                                this.shipmentOutgoingService.buildPO({
                                    item: 'idframe:' + this.rowdatatmp.idframe
                                }).subscribe(
                                    (value1) => {
                                        this.shipmentOutgoingService.GLInventoryTransfer(this.rowdatatmp.idframe).subscribe(
                                            (resGL) => { },
                                            (err) => { }
                                        );
                                        console.log('this: ', value1);
                                    },
                                    (err) => console.log(err),
                                    () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
                                );
                            }
                        );
                    }
                )
            }
        )
    }
    protected onErrorCek(error) {
        // this.toaster.showToaster('info', 'save', 'Qty Input Melebihi Dari Qty Request')
        this.toasterService.showToaster('error', 'Error', 'Seluruh motor transfer unit harus sudah lolos unit preparation');
        this.buttonVisible = true;
    }

    buildReindex() {
        this.shipmentOutgoingService.process({ command: 'buildIndex' }).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }
}
