import { Component, Input, OnInit, OnDestroy, ViewChild, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ShipmentOutgoing } from './../shipment-outgoing.model';
import { ShipmentOutgoingService } from './../shipment-outgoing.service';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { VehicleSalesOrderService, VehicleSalesOrder } from '../../vehicle-sales-order';
import * as ShipmentOutgoingConstrants from '../../../shared/constants/shipmentOutgoing.constrants';
import {CustomShipment} from './../custom-shipment.model';
import {SalesUnitRequirement, SalesUnitRequirementService} from './../../sales-unit-requirement';
import { ShipmentOutgoingParameters } from './../shipment-outgoing-parameter.model';
import { ShipmentFilter } from './../shipment-filter.model';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-shipment-outgoing-grid-unit',
    templateUrl: './shipment-outgoing-grid-unit.component.html'
})
export class ShipmentOutgoingGridUnitComponent implements OnInit, OnDestroy {

    @Input() idStatusType: number;
    @Output() fnCallback = new EventEmitter();

    salesUnitRequirement: SalesUnitRequirement;
    shipmentOutgoingTMP: ShipmentOutgoing[];
    customShipment: CustomShipment[];
    selected: CustomShipment[];
    currentAccount: any;
    shipmentOutgoings: ShipmentOutgoing;
    unitPreparation: ShipmentOutgoing[];
    deliveryOrder: ShipmentOutgoing[];
    jadwalPengiriman: ShipmentOutgoing[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    vso: VehicleSalesOrder[];
    custShipment: CustomShipment;
    idInternalHere: any;
    shipmentFilter: ShipmentFilter;

    selectedSalesman: string;
    filtered: any;
    isFiltered: boolean;
    shipmentOutgoingParameters: ShipmentOutgoingParameters;

    constructor(
        private salesUnitRequirementService: SalesUnitRequirementService,
        private shipmentOutgoingService: ShipmentOutgoingService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private loadingService: LoadingService,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private vsoService: VehicleSalesOrderService,
        private toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.selected = new Array<CustomShipment>();
        this.shipmentOutgoingTMP = new Array<ShipmentOutgoing>()
        this.isFiltered = false;
        this.shipmentFilter = new ShipmentFilter();
    }

    loadAll() {
        this.loadingService.loadingStart();
        console.log('loadall');
        this.customShipment = new Array<CustomShipment>();
        this.selected = new Array<CustomShipment>();
        if (this.currentSearch) {
            this.shipmentOutgoingService.search({
                idstatustype: 10,
                idInternal: this.principal.getIdInternal(),
                queryFor: 'picking',
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                // sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.shipmentOutgoingService.queryFilterBy({
                idstatustype: 10,
                idInternal: this.principal.getIdInternal(),
                queryFor: 'picking',
                filterType: this.shipmentFilter.filterShipmentType,
                type: this.shipmentFilter.ShipmentType,
                page: this.page - 1,
                size: this.itemsPerPage,
                // sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json),
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/shipment-outgoing'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/shipment-outgoing', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/shipment-outgoing', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        console.log('ngoninit');
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
            this.idInternalHere = this.principal.getIdInternal();
        });
        this.registerChangeInShipmentOutgoings();
    }

    goToDetail(sur): Promise<any> {
        console.log('sur', sur);
        return new Promise<any>(
            (resolve) => {
                this.salesUnitRequirementService.find(sur).subscribe(
                    (data) => {
                        this.salesUnitRequirement = data;
                        console.log('kirim detail',  this.salesUnitRequirement);
                        this.shipmentOutgoingService.changeShareDataTmp( this.salesUnitRequirement);
                        this.router.navigate(['../shipment-outgoing/detail']);
                    }
                )
            }
        )
    }

    kirimGudang() {
        // if (this.shipmentOutgoings.deliveryAddress !== null && this.shipmentOutgoings.dateSchedulle !== null && this.shipmentOutgoings.dateSchedulle !== '-') {
            for (let i = 0; i < this.selected.length; i++) {
                if (this.selected[i].dateSchedulle !== null && this.selected[i].deliveryAddress !== null) {
                    this.shipmentOutgoingTMP.push({
                        idShipment : this.selected[i].idShipment
                    });
                    console.log('test');
                }else {
                    console.log('false');
                    this.toasterService.showToaster('info', 'Error', 'Alamat dan Tanggal Pengiriman Tidak Boleh Kosong');
                    return false;
                }
            }
            // console.log('true');
            this.shipmentOutgoingService.executeListProcess(101, null, this.shipmentOutgoingTMP).subscribe(
                (value) => {
                    console.log('this: ', value);
                    // this.loadAll();
                    this.fnCallback.emit();
                },
                (err) => console.log(err),
                () => this.toasterService.showToaster('info', 'Data Proces', 'Data Dikirim Ke Gudang')
            );
    }

    tabUnitprep() {
        console.log('tab unit prep click');
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: CustomShipment) {
        return item.idOrder;
    }

    registerChangeInShipmentOutgoings() {
        this.eventManager.subscribe('unitPrepFilter', (response) => {
            this.shipmentOutgoingService.passingData.subscribe(
                (filter) => {
                    this.shipmentFilter = filter;
                    this.filter();
                });
            console.log('sedang filter');
            console.log(this.shipmentFilter);
        });
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'orderNumber') {
            result.push('orderNumber');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // console.log('data', data);
        // console.log('header', headers);
        this.loadingService.loadingStop(),
        this.customShipment = new Array<CustomShipment>();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const obj = data[i];
                if (obj.dateSchedulle !== null) {
                    console.log('dateschedule', obj.dateSchedulle);
                    obj.dateSchedulle = new Date (obj.dateSchedulle)
                }
            }
        }
        this.customShipment = data;
        console.log('custom', data);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.shipmentOutgoingService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.shipmentOutgoingService.update(event.data)
                .subscribe((res: ShipmentOutgoing) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.shipmentOutgoingService.create(event.data)
                .subscribe((res: ShipmentOutgoing) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: ShipmentOutgoing) {
        this.toasterService.showToaster('info', 'ShipmentOutgoing Sent to Warehouse', 'Data Saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.shipmentOutgoingService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                         name: 'shipmentOutgoingListModification',
                         content: 'Deleted an shipmentOutgoing'
                    });
                });
            }
        });
    }

    filter() {
        this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered );
        // this.page = 0;
        this.shipmentOutgoingParameters = new ShipmentOutgoingParameters();
        this.shipmentOutgoingParameters.internalId = this.principal.getIdInternal();
        this.shipmentOutgoingParameters.idStatusType = 10;
        this.shipmentOutgoingParameters.queryFor = 'picking';
        this.shipmentOutgoingParameters.filterType = this.shipmentFilter.filterShipmentType;
        this.shipmentOutgoingParameters.type = this.shipmentFilter.ShipmentType;
        console.log('pertama', this.shipmentOutgoingParameters);
        if ( this.shipmentFilter.filterShipmentType === true ) {
            this.shipmentOutgoingParameters.reffNumber = 'RQN';
        } else {
            this.shipmentOutgoingParameters.reffNumber = '';
        }

        if (this.selectedSalesman !== null && this.selectedSalesman !== undefined) {
            this.shipmentOutgoingParameters.salesmanId = this.selectedSalesman;
        }

        if (this.filtered !== null && this.filtered !== undefined) {
            this.shipmentOutgoingParameters.dataParam = this.filtered;
        }
        console.log('setelah', this.shipmentOutgoingParameters);
        this.shipmentOutgoingService.searchUnitPrep({
            shipmentOutgoingPTO: this.shipmentOutgoingParameters,
            idInternal: this.principal.getIdInternal(),
            // sort: this.sort()
        })
            .toPromise()
            .then((res) => {
                console.log('filter data isi nama == ', res)
                this.loadingService.loadingStop(),
                this.onSuccessFilter(res.json, res.headers)
            }
        );

        console.log('tanggal 1', this.shipmentOutgoingParameters.orderDateFrom);
        console.log('tanggal 2', this.shipmentOutgoingParameters.orderDateThru);
        console.log('salesman 2', this.shipmentOutgoingParameters.salesmanId);
    }

    protected onSuccessFilter(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.customShipment = data;
        // if (data.length === 0) {
        //     this.unitDeliverables = [...this.unitDeliverables, this.unitDeliverable];
        // }
    }

    buildReindex() {
        this.shipmentOutgoingService.process({command: 'buildIndex'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }
}
