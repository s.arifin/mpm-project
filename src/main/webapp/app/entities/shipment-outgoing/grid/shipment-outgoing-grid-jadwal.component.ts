import { Component, Input, OnInit, OnDestroy, Output, ViewChild, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ShipmentOutgoing } from './../shipment-outgoing.model';
import { ShipmentOutgoingService } from './../shipment-outgoing.service';
import { LoadingService } from '../../../layouts/loading/loading.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { VehicleSalesOrderService, VehicleSalesOrder } from '../../vehicle-sales-order';
import { Driver, DriverService} from '../../driver';
import * as ShipmentOutgoingConstrants from '../../../shared/constants/shipmentOutgoing.constrants';
import {CustomShipment} from './../custom-shipment.model'
import { ShipmentOutgoingParameters } from './../shipment-outgoing-parameter.model';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-shipment-outgoing-grid-jadwal',
    templateUrl: './shipment-outgoing-grid-jadwal.component.html'
})
export class ShipmentOutgoingGridJadwalComponent implements OnInit, OnDestroy {

    @Input() idStatusType: any;

    @Output()
    fnCallback = new EventEmitter();

    customShipment: CustomShipment[];
    statusdriver: boolean ;
    currentAccount: any;
    shipmentOutgoings: ShipmentOutgoing;
    shipmentOutgoing: ShipmentOutgoing[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    vso: VehicleSalesOrder[];
    driver: Driver[];
    driversExternal: any[];
    driversInternal: any[];
    stat: number;
    internal: any;
    eksternal: any;
    // driverstatus: Driver;
    modalKonfirmasiDriver: boolean;
    buttonVisible: boolean;
    selected: CustomShipment[];
    idInternalHere: any;

    selectedSalesman: string;
    filtered: any;
    isFiltered: boolean;
    shipmentOutgoingParameters: ShipmentOutgoingParameters;

 constructor(
        private shipmentOutgoingService: ShipmentOutgoingService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private loadingService: LoadingService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private vsoService: VehicleSalesOrderService,
        private driverService: DriverService,
        private toasterService: ToasterService
    ) {
        this.buttonVisible = true;
        this.modalKonfirmasiDriver = false;
        this.stat = 1;
        this.driversExternal = new Array <Driver>();
        this.driversInternal = new Array <Driver>();
        this.customShipment = new Array<CustomShipment>();
        this.selected = new Array<CustomShipment>();
        this.driver = new Array<Driver>();
        this.shipmentOutgoing = new Array<ShipmentOutgoing>();
        this.shipmentOutgoings = new ShipmentOutgoing;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    private driverType(data) {
        console.log('driver', data);
        if (data.length > 0) {
            for (const i of data) {
                const obj = data[i];
                if (i.externalDriver !== true) {
                    this.driver.push(obj)
                }
            }
        }
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.shipmentOutgoingService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json),
                );
            return;
        }
        this.shipmentOutgoingService.queryFilterBy({
            idstatustype: this.idStatusType,
            queryFor: 'jadwal',
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            // sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
            this.idInternalHere = this.principal.getIdInternal();
            this.driverService.queryFilterBy({
                idInternal: this.principal.getIdInternal(),
                page: this.page - 1,
                size: this.itemsPerPage,
                // sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccessDriver(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        });
        this.registerChangeInShipmentOutgoings();
    }

    showModalKonfirmasiOther() {
        for ( let i = 0 ; i < this.selected.length; i++) {
            if (this.selected[i].currentStatus === 21 || this.selected[i].currentStatus === 23 ) {
                this.toasterService.showToaster('info', 'Data Proces', 'Please Check Your Selected Data')
                return;
            }
        }
        this.modalKonfirmasiDriver = true;
    }
    goToDetail(jadwal): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                this.shipmentOutgoingService.find(jadwal.idShipment).subscribe(
                    (data) => {
                        this.shipmentOutgoings = data;
                        this.shipmentOutgoings.dateSchedulle = new Date (this.shipmentOutgoings.dateSchedulle);
                        console.log('kirim detail',  this.shipmentOutgoings);
                        this.shipmentOutgoingService.changeShareDataTmp( this.shipmentOutgoings);
                        this.shipmentOutgoingService.passingCustomData(jadwal);
                        this.router.navigate(['../shipment-outgoing/jadwal-detail']);
                    }
                )
            }
        )
    }
    cancelDo(rowdata): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                console.log('cancel', rowdata);
                // console.log('KIRIM_GUDANG', ShipmentOutgoingConstrants.KIRIM_GUDANG);
                this.shipmentOutgoingService.executeProcess(ShipmentOutgoingConstrants.CANCEL, null, rowdata ).subscribe(
                    (value) => console.log('this: ', value),
                    (err) => console.log(err),
                    () => {
                        this.loadAll();
                        resolve()
                    }
                );
            }
        )
    }
    status() {
        if (this.stat === 2) {
            this.shipmentOutgoings.idDriver = null;
        } else {
            this.shipmentOutgoings.otherName = null;
        }
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/shipment-outgoing'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/shipment-outgoing', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/shipment-outgoing', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ShipmentOutgoing) {
        return item.idShipment;
    }

    registerChangeInShipmentOutgoings() {
        this.eventSubscriber = this.eventManager.subscribe('shipmentOutgoingListModification',
            (response) => {
                if (this.idStatusType == null || this.idStatusType === undefined) {
                    this.idStatusType = [21, 22, 23, 24];
                }
                this.loadAll();
            }
        );
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'shipmentNumber') {
            result.push('shipmentNumber');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.loadingService.loadingStop();
        this.customShipment = new Array<CustomShipment>();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        if (data !== null) {
            if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        const obj = data[i];
                        obj.dateSchedulle = new Date (obj.dateSchedulle)
                        // this.customShipment = [...this.customShipment, data];
                    }
            }
        }
        this.customShipment = data;
        // console.log(data)
        // return _arr;
    }
    private onSuccessDriver(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.driver = data;
        // console.log("drivernya=>>>>>>>>>>>>>>",this.drivers);

        for ( let i = 0 ; i < this.driver.length; i++) {

            if (this.driver[i].externalDriver === true ) {
                this.driversExternal.push(this.driver[i]);

            } else if (this.driver[i].externalDriver === false || this.driver[i].externalDriver == null) {
                this.driversInternal.push(this.driver[i]);
            }
        }
         console.log('drivernya', this.driversExternal);
         console.log('drivernyaint', this.driversInternal);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.shipmentOutgoingService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(data) {
        this.subscribeToSaveResponse(
            this.shipmentOutgoingService.update(this.shipmentOutgoings));
    }

    assignDriver(): Promise<any> {
        // console.log('rowdatanya', this.shipmentOutgoings);
        this.buttonVisible = false;
        return new Promise<any>(
            (resolve) => {
                console.log('selcted', this.selected);
                this.updateShipmentDriver(this.selected).then(
                    () => {
                        for (let i = 0; i < this.selected.length; i++) {
                            this.shipmentOutgoing.push({
                                idShipment : this.selected[i].idShipment
                            });
                            console.log('test');
                        }
                        this.shipmentOutgoingService.executeListProcess(ShipmentOutgoingConstrants.ASSIGN_DRIVER, null, this.shipmentOutgoing).subscribe(
                            (value) => {
                                console.log('this: ', value);

                                this.fnCallback.emit();
                            },
                            (err) => console.log(err),
                            () => this.toasterService.showToaster('info', 'Data Proces', 'Data Dikirim Ke Driver')
                        );
                    }
                )
            }
        )
    }
    updateShipmentDriver(datashipment): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                for (let i = 0; i < this.selected.length; i++) {
                    this.shipmentOutgoing.push({
                        idShipment : this.selected[i].idShipment,
                        idDriver : this.shipmentOutgoings.idDriver,
                        otherName : this.shipmentOutgoings.otherName,
                        deliveryOpt: this.shipmentOutgoings.deliveryOpt
                    });
                    console.log('test');
                }
                this.shipmentOutgoingService.executeListProcess(109, null,  this.shipmentOutgoing ).subscribe(
                    (value) => console.log('this: ', value),
                    (err) => console.log(err),
                    () => {
                        resolve()
                    }
                );
            }
        )
    }

    private subscribeToSaveResponse(result: Observable<ShipmentOutgoing>) {
        result.subscribe((res: ShipmentOutgoing) =>
            this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
    }

    private onRowDataSaveSuccess(result: ShipmentOutgoing) {
        this.toasterService.showToaster('info', 'ShipmentOutgoing Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.shipmentOutgoingService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'shipmentOutgoingListModification',
                    content: 'Deleted an shipmentOutgoing'
                    });
                });
            }
        });
    }
    trackDriverById(index: number, item: Driver) {
        // console.log('index', index);
        // console.log('item', item);
        return item.idPartyRole;
    }

    filter() {
        this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered );
        // this.page = 0;
        this.shipmentOutgoingParameters = new ShipmentOutgoingParameters();
        this.shipmentOutgoingParameters.internalId = this.principal.getIdInternal();
        this.shipmentOutgoingParameters.idStatusType = 24;
        this.shipmentOutgoingParameters.queryFor = 'jadwal';

        if (this.selectedSalesman !== null && this.selectedSalesman !== undefined) {
            this.shipmentOutgoingParameters.salesmanId = this.selectedSalesman;
        }

        if (this.filtered !== null && this.filtered !== undefined) {
            this.shipmentOutgoingParameters.dataParam = this.filtered;
        }

        this.shipmentOutgoingService.searchUnitPrep({
            shipmentOutgoingPTO: this.shipmentOutgoingParameters,
            idInternal: this.principal.getIdInternal(),
            // sort: this.sort()
        })
            .toPromise()
            .then((res) => {
                console.log('filter data isi nama == ', res)
                this.loadingService.loadingStop(),
                this.onSuccessFilter(res.json, res.headers)
            }
        );

        console.log('tanggal 1', this.shipmentOutgoingParameters.orderDateFrom);
        console.log('tanggal 2', this.shipmentOutgoingParameters.orderDateThru);
        console.log('salesman 2', this.shipmentOutgoingParameters.salesmanId);
    }

    protected onSuccessFilter(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.customShipment = data;
        // if (data.length === 0) {
        //     this.unitDeliverables = [...this.unitDeliverables, this.unitDeliverable];
        // }
    }

    buildReindex() {
        this.shipmentOutgoingService.process({command: 'buildIndex'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }
}
