import { BaseEntity } from './../../shared';

export class ShipmentFilter implements BaseEntity {
    constructor(
        public id?: any,
        public ShipmentType?: any,
        public filterShipmentType?: any,
    ) {
        this.ShipmentType = 0;
        this.filterShipmentType = false;
    }
}
