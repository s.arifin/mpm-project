import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import * as BaseConstant from '../../../shared/constants/base.constants';
import { Subscription, Observable } from 'rxjs/Rx';

import { SalesUnitRequirement } from '../../sales-unit-requirement/sales-unit-requirement.model';
import { PersonalCustomerService, PersonalCustomer, OrganizationCustomer, OrganizationCustomerService } from '../../shared-component';

@Component({
    selector: 'jhi-sales-unit-requirement-deal-detail-shipment-view',
    templateUrl: './sales-unit-requirement-deal-detail-shipment-view.component.html'
})

export class SalesUnitRequirementDealDetailShipmentViewComponent implements OnChanges {
    @Input()
    public salesUnitRequirement: SalesUnitRequirement;

    @Input()
    public viewDetailShipment: any = 0;

    personalCustomer: PersonalCustomer;
    organizationCustomer: OrganizationCustomer;
    readonlyDocument: Boolean = true;
    isOrganization: String;
    isPerson: String;

    constructor(
        private personalCustomerService: PersonalCustomerService,
        private organizationCustomerService: OrganizationCustomerService
    ) {
        this.personalCustomer = new PersonalCustomer();
        this.isOrganization = 'organization';
        this.isPerson = 'person';
    }

    ngOnChanges(changes: SimpleChanges ) {
        if (changes['salesUnitRequirement']) {
            if (this.salesUnitRequirement.customerId !== undefined) {
                if (this.salesUnitRequirement.idReqTyp === 101 || this.salesUnitRequirement.idReqTyp === 103 || this.salesUnitRequirement.idReqTyp === 104 || this.salesUnitRequirement.idReqTyp === 105 ) {
                    this.getOrganizationCustomer(this.salesUnitRequirement.customerId);
                } else {
                    this.getPersonalCustomer(this.salesUnitRequirement.customerId);
                }
            }
        }
    }

    getPersonalCustomer(id: string): void {
        this.personalCustomerService.find(id).subscribe(
            (res) => {
                if (res.person.dob !== null) {
                    res.person.dob = new Date(res.person.dob);
                }
                console.log('aaaa', res);
                this.personalCustomer = res;
            }
        )
    }
    getOrganizationCustomer(id: string): void {
        this.organizationCustomerService.find(id).subscribe(
            (res) => {
                this.organizationCustomer = res;
            }
        )
    }

    public showNotShipment(): boolean {
        let a: boolean;
        a = false;

        if (this.viewDetailShipment === 0) {
            a = true;
        }

        return a;
    }

    public showNameOrFullName(data: number): boolean {
        let a: boolean;
        a = false;

        if (data === 1 || data === 0) {
            a = true;
        }

        return a;
    }

    public checkCustomer(idRequestType: number): string {
        let a: string;
        a = null;

        const rs: object = this.checkIfOrganization(idRequestType);
        a = rs['customer'];
        return a;
    }

    public checkSTNK(idRequestType: number): string {
        let a: string;
        a = null;

        const rs: object = this.checkIfOrganization(idRequestType);
        a = rs['stnk'];

        return a;
    }

    public checkIfOrganization(idRequestType: number): object {
        let a: object;
        a = {
            customer : 'organization',
            stnk : 'organization'
        };

        if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP) {
            a = {
                customer : 'organization',
                stnk : 'organization'
            };
        } else if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP_REKANAN) {
            a = {
                customer : 'organization',
                stnk : 'organization'
            };
        } else if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP_INTERNAL) {
            a = {
                customer : 'organization',
                stnk : 'organization'
            };
        } else if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP_PERSON) {
            a = {
                customer : 'organization',
                stnk : 'person'
            };
        } else if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_DIRECT) {
            a = {
                customer : 'person',
                stnk : 'person'
            }
        }
        return a;
    }
}
