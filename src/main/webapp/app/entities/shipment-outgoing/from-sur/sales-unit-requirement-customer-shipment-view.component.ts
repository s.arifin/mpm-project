import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import * as BaseConstant from '../../../shared/constants/base.constants';
import { SalesUnitRequirement } from '../../sales-unit-requirement';
import { PersonalCustomerService, PersonalCustomer, OrganizationCustomer, OrganizationCustomerService } from '../../shared-component';

import * as reqType from '../../../shared/constants/requirement-type.constants';
import * as SalesUnitRequirementConstants from '../../../shared/constants/sales-unit-requirement.constants';
import { Organization } from '../../organization';

@Component({
    selector: 'jhi-sales-unit-requirement-customer-shipment-view',
    templateUrl: './sales-unit-requirement-customer-shipment-view.component.html'
})

export class SalesUnitRequirementCustomerViewShipmentComponent implements OnChanges {
    @Input()
    public salesUnitRequirement: SalesUnitRequirement =  new SalesUnitRequirement();
    @Input() organization: Organization;

    @Input()
    public viewDetailShipment: any = 0;

    personalCustomer: PersonalCustomer;
    organizationCustomer: OrganizationCustomer;
    readonlyDocument: Boolean = true;

    constructor(
        private personalCustomerService: PersonalCustomerService,
        private organizationCustomerService: OrganizationCustomerService
    ) {
        this.personalCustomer = new PersonalCustomer();
        this.organizationCustomer = new OrganizationCustomer();
        this.salesUnitRequirement = new  SalesUnitRequirement();
        this.organization = new Organization();
    }

    ngOnChanges(changes: SimpleChanges ) {
        if (changes['salesUnitRequirement']) {
            if (this.salesUnitRequirement.customerId !== undefined) {
                if (this.salesUnitRequirement.idReqTyp === 101 ||
                    this.salesUnitRequirement.idReqTyp === 103 ||
                    this.salesUnitRequirement.idReqTyp === 104 ||
                    this.salesUnitRequirement.idReqTyp === 105 ) {
                    this.getOrganizationCustomer(this.salesUnitRequirement.customerId);
                } else if (this.salesUnitRequirement.idReqTyp === 102 ||
                    this.salesUnitRequirement.idReqTyp === null ) {
                    this.getPersonalCustomer(this.salesUnitRequirement.customerId);
                }
            }
        }
        console.log('vso customer', this.salesUnitRequirement);
    }

    getPersonalCustomer(id: string): void {
        this.personalCustomerService.find(id).subscribe(
            (res) => {
                if (res.person.dob !== null) {
                    res.person.dob = new Date(res.person.dob);
                }
                console.log('aaaa', res);
                this.personalCustomer = res;
            }
        )
    }
    getOrganizationCustomer(id: string): void {
        this.organizationCustomerService.find(id).subscribe(
            (res) => {
                this.organizationCustomer = res;
                console.log('getOrganizationCustomer', res);
            }
        )
    }

    public showNotShipment(): boolean {
        let a: boolean;
        a = false;

        if (this.viewDetailShipment === 0) {
            a = true;
        }

        return a;
    }

    public showNameOrFullName(data: number): boolean {
        let a: boolean;
        a = false;

        if (data === 1 || data === 0) {
            a = true;
        }

        return a;
    }
};
