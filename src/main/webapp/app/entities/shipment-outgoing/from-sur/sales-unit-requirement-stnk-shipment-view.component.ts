import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import * as BaseConstant from '../../../shared/constants/base.constants';
import { Subscription, Observable } from 'rxjs/Rx';

import { SalesUnitRequirement } from '../../sales-unit-requirement/sales-unit-requirement.model';
import { PersonalCustomerService, PersonalCustomer, OrganizationCustomer, OrganizationCustomerService } from '../../shared-component';

@Component({
    selector: 'jhi-sales-unit-requirement-stnk-shipment-view',
    templateUrl: './sales-unit-requirement-stnk-shipment-view.component.html'
})

export class SalesUnitRequirementSTNKShipmentViewComponent implements OnChanges {
    @Input() salesUnitRequirement: SalesUnitRequirement;
    @Input() viewDetailShipment: any = 0;
    readonlyDocument: boolean;

    constructor() {
        this.readonlyDocument = true;
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['salesUnitRequirement']) {
            // if (this.salesUnitRequirement.idReqTyp === reqType.REQUIREMENT_GC ) {
            // }
        }
    }
}
