import { Component, OnInit, OnDestroy, DoCheck } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { SalesUnitRequirement, SalesUnitRequirementService} from './../sales-unit-requirement';
import {PersonalCustomer, PersonalCustomerService, OrganizationCustomer, OrganizationCustomerService} from '../shared-component';

import { ShipmentOutgoing } from './shipment-outgoing.model';
import { ShipmentOutgoingService } from './shipment-outgoing.service';
import { Shipment, ShipmentService} from './../shipment';
import { ToasterService, Principal} from '../../shared';
import * as BaseConstant from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-shipment-outgoing-detail',
    templateUrl: './shipment-outgoing-detail.component.html'
})

export class ShipmentOutgoingDetailComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    shipmentOutgoing: ShipmentOutgoing;
    isSaving: boolean;
    eventSubscriber: Subscription;
    salesUnitRequirement: SalesUnitRequirement;
    shipment: Shipment;
    currentAccount: any;
    address: any;
    textAreas: any;
    personalCustomer: PersonalCustomer;
    organizationCustomer: OrganizationCustomer;
    isReadOnlyDocument: boolean;
    noeditaddress: boolean;
    isOrganization: String;
    isPerson: String;
    internal: any;
    partyId: any;
    referenceNumber: any;

    constructor(
        protected personalCustomerService: PersonalCustomerService,
        private organizationCustomerService: OrganizationCustomerService,
        protected shipmentService: ShipmentService,
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected alertService: JhiAlertService,
        protected shipmentOutgoingService: ShipmentOutgoingService,
        protected route: ActivatedRoute,
        protected principal: Principal,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.shipmentOutgoing = new ShipmentOutgoing();
        this.noeditaddress = true;
        this.isOrganization = 'organization';
        this.isPerson = 'person';
    }

    ngOnInit() {
        // this.shipmentOutgoingService.currentData.subscribe( shipmentTMP => this.shipmentTMP = shipmentTMP)
        // this.shipmentTMP.dateCreate = new Date ( this.shipmentTMP.dateCreate);
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.subscription = this.route.params.subscribe((params) => {
            console.log('param', params);
            if (params['idship']) {
                this.load(params['idship'], 2)
            }
            if (params['idrek']) {
                this.load(params['idrek'], 1);
            }
            if (params['reffNumber']) {
                this.referenceNumber = params['reffNumber'];
            }
        });
        this.isSaving = false;
    }

    ngOnDestroy() {
    }

    load(paramID, status) {
        console.log('paramid', paramID);
        console.log('status', status);
        if (status === 1) {
            this.salesUnitRequirementService.find(paramID).subscribe(
                (data1) => {
                    this.salesUnitRequirement = data1;
                    this.internal = this.principal.getIdInternal();
                    if (this.salesUnitRequirement.idReqTyp === 102 || this.salesUnitRequirement.idReqTyp === null) {
                        this.personalCustomerService.find(this.salesUnitRequirement.customerId).subscribe(
                            (data) => {
                                this.personalCustomer = data;
                                console.log('detailperson',  this.personalCustomer);
                            }
                        )
                    } else if (this.salesUnitRequirement.idReqTyp === 101 || this.salesUnitRequirement.idReqTyp === 103 || this.salesUnitRequirement.idReqTyp === 104 || this.salesUnitRequirement.idReqTyp === 105) {
                        this.organizationCustomerService.find(this.salesUnitRequirement.customerId).subscribe(
                            (data) => {
                                this.organizationCustomer = data;
                                console.log('detailorganization :', this.organizationCustomer);
                            }
                        )
                    }
                }
            )
        } else if (status === 2) {
            this.shipmentOutgoingService.find(paramID).subscribe(
                (data) => {
                    if (data.dateSchedulle !== null) {
                        data.dateSchedulle = new Date(data.dateSchedulle);
                    }
                    this.shipmentOutgoing = data;
                }
            )
        }
    }

    previousState() {
        this.router.navigate(['shipment-outgoing']);
    }

    checkaddress() {
        if (this.salesUnitRequirement.idReqTyp === 102 || this.salesUnitRequirement.idReqTyp === null) {
                console.log('opt', this.shipmentOutgoing.deliveryOpt);
            console.log('personal', this.personalCustomer);
            if (this.shipmentOutgoing.deliveryOpt === 'opt1' && this.personalCustomer.person !== undefined) {
                this.noeditaddress = true;
                console.log('this.shipmentOutgoing.deliveryAddress', this.shipmentOutgoing.deliveryAddress);
                this.shipmentOutgoing.deliveryAddress = '';
                if (this.personalCustomer.person.postalAddress.villageName == null) {
                    this.personalCustomer.person.postalAddress.villageName = ' ';
                } else {
                    this.personalCustomer.person.postalAddress.villageName = this.personalCustomer.person.postalAddress.villageName;
                }
                if (this.personalCustomer.person.postalAddress.districtName == null) {
                    this.personalCustomer.person.postalAddress.districtName = ' ';
                } else {
                    this.personalCustomer.person.postalAddress.districtName = this.personalCustomer.person.postalAddress.districtName;
                }
                if (this.personalCustomer.person.postalAddress.cityName == null) {
                    this.personalCustomer.person.postalAddress.cityName = ' ';
                } else {
                    this.personalCustomer.person.postalAddress.cityName = this.personalCustomer.person.postalAddress.cityName;
                }
                if (this.personalCustomer.person.postalAddress.provinceName == null) {
                    this.personalCustomer.person.postalAddress.provinceName = ' ';
                } else {
                    this.personalCustomer.person.postalAddress.provinceName = this.personalCustomer.person.postalAddress.provinceName;
                }
                this.shipmentOutgoing.deliveryAddress = this.personalCustomer.person.postalAddress.address1 + ' ' +
                this.personalCustomer.person.postalAddress.villageName + ' ' +
                this.personalCustomer.person.postalAddress.districtName + ' ' +
                this.personalCustomer.person.postalAddress.cityName + ' ' +
                this.personalCustomer.person.postalAddress.provinceName;
            } else if (this.shipmentOutgoing.deliveryOpt === 'opt2' && this.personalCustomer.person !== undefined) {
                this.noeditaddress = true;
                this.shipmentOutgoing.deliveryAddress = '';
                if ( this.salesUnitRequirement.personOwner.postalAddress.villageName == null ) {
                    this.salesUnitRequirement.personOwner.postalAddress.villageName = ' ';
                } else {
                    this.salesUnitRequirement.personOwner.postalAddress.villageName = this.salesUnitRequirement.personOwner.postalAddress.villageName;
                }
                if ( this.salesUnitRequirement.personOwner.postalAddress.districtName == null ) {
                    this.salesUnitRequirement.personOwner.postalAddress.districtName = ' ';
                } else {
                    this.salesUnitRequirement.personOwner.postalAddress.districtName = this.salesUnitRequirement.personOwner.postalAddress.districtName;
                }
                if ( this.salesUnitRequirement.personOwner.postalAddress.cityName == null ) {
                    this.salesUnitRequirement.personOwner.postalAddress.cityName = ' ';
                } else {
                    this.salesUnitRequirement.personOwner.postalAddress.cityName = this.salesUnitRequirement.personOwner.postalAddress.cityName;
                }
                if ( this.salesUnitRequirement.personOwner.postalAddress.provinceName == null ) {
                    this.salesUnitRequirement.personOwner.postalAddress.provinceName = ' ';
                } else {
                    this.salesUnitRequirement.personOwner.postalAddress.provinceName = this.salesUnitRequirement.personOwner.postalAddress.provinceName;
                }
                this.shipmentOutgoing.deliveryAddress = this.salesUnitRequirement.personOwner.postalAddress.address1 + ' ' +
                this.salesUnitRequirement.personOwner.postalAddress.villageName + ' ' +
                this.salesUnitRequirement.personOwner.postalAddress.districtName + ' ' +
                this.salesUnitRequirement.personOwner.postalAddress.cityName + ' ' +
                this.salesUnitRequirement.personOwner.postalAddress.provinceName;
                console.log('this.shipmentOutgoing.deliveryAddress', this.shipmentOutgoing.deliveryAddress);
            } else if (this.shipmentOutgoing.deliveryOpt === 'opt3') {
                this.noeditaddress = false;
            }
        } else if (this.salesUnitRequirement.idReqTyp === 101 || this.salesUnitRequirement.idReqTyp === 103 || this.salesUnitRequirement.idReqTyp === 104 || this.salesUnitRequirement.idReqTyp === 105) {

            console.log('opt', this.shipmentOutgoing.deliveryOpt);
            console.log('personal', this.organizationCustomer);
            if (this.shipmentOutgoing.deliveryOpt === 'opt1' && this.organizationCustomer.organization !== undefined) {
                this.noeditaddress = true;
                console.log('this.shipmentOutgoing.deliveryAddress', this.shipmentOutgoing.deliveryAddress);
                this.shipmentOutgoing.deliveryAddress = '';
                if (this.organizationCustomer.organization.postalAddress.villageName == null) {
                    this.organizationCustomer.organization.postalAddress.villageName = ' ';
                } else {
                    this.organizationCustomer.organization.postalAddress.villageName = this.organizationCustomer.organization.postalAddress.villageName;
                }
                if (this.organizationCustomer.organization.postalAddress.districtName == null) {
                    this.organizationCustomer.organization.postalAddress.districtName = ' ';
                } else {
                    this.organizationCustomer.organization.postalAddress.districtName = this.organizationCustomer.organization.postalAddress.districtName;
                }
                if (this.organizationCustomer.organization.postalAddress.cityName == null) {
                    this.organizationCustomer.organization.postalAddress.cityName = ' ';
                } else {
                    this.organizationCustomer.organization.postalAddress.cityName = this.organizationCustomer.organization.postalAddress.cityName;
                }
                if (this.organizationCustomer.organization.postalAddress.provinceName == null) {
                    this.organizationCustomer.organization.postalAddress.provinceName = ' ';
                } else {
                    this.organizationCustomer.organization.postalAddress.provinceName = this.organizationCustomer.organization.postalAddress.provinceName;
                }
                this.shipmentOutgoing.deliveryAddress = this.organizationCustomer.organization.postalAddress.address1 + ' ' +
                this.organizationCustomer.organization.postalAddress.villageName + ' ' +
                this.organizationCustomer.organization.postalAddress.districtName + ' ' +
                this.organizationCustomer.organization.postalAddress.cityName + ' ' +
                this.organizationCustomer.organization.postalAddress.provinceName;
            } else if (this.shipmentOutgoing.deliveryOpt === 'opt2' && this.organizationCustomer.organization !== undefined) {
                this.noeditaddress = true;
                this.shipmentOutgoing.deliveryAddress = '';
                if ( this.salesUnitRequirement.organizationOwner.postalAddress.villageName == null ) {
                    this.salesUnitRequirement.organizationOwner.postalAddress.villageName = ' ';
                } else {
                    this.salesUnitRequirement.organizationOwner.postalAddress.villageName = this.salesUnitRequirement.organizationOwner.postalAddress.villageName;
                }
                if ( this.salesUnitRequirement.organizationOwner.postalAddress.districtName == null ) {
                    this.salesUnitRequirement.organizationOwner.postalAddress.districtName = ' ';
                } else {
                    this.salesUnitRequirement.organizationOwner.postalAddress.districtName = this.salesUnitRequirement.organizationOwner.postalAddress.districtName;
                }
                if ( this.salesUnitRequirement.organizationOwner.postalAddress.cityName == null ) {
                    this.salesUnitRequirement.organizationOwner.postalAddress.cityName = ' ';
                } else {
                    this.salesUnitRequirement.organizationOwner.postalAddress.cityName = this.salesUnitRequirement.organizationOwner.postalAddress.cityName;
                }
                if ( this.salesUnitRequirement.organizationOwner.postalAddress.provinceName == null ) {
                    this.salesUnitRequirement.organizationOwner.postalAddress.provinceName = ' ';
                } else {
                    this.salesUnitRequirement.organizationOwner.postalAddress.provinceName = this.salesUnitRequirement.organizationOwner.postalAddress.provinceName;
                }
                this.shipmentOutgoing.deliveryAddress = this.salesUnitRequirement.organizationOwner.postalAddress.address1 + ' ' +
                this.salesUnitRequirement.organizationOwner.postalAddress.villageName + ' ' +
                this.salesUnitRequirement.organizationOwner.postalAddress.districtName + ' ' +
                this.salesUnitRequirement.organizationOwner.postalAddress.cityName + ' ' +
                this.salesUnitRequirement.organizationOwner.postalAddress.provinceName;
                console.log('this.shipmentOutgoing.deliveryAddress', this.shipmentOutgoing.deliveryAddress);
            } else if (this.shipmentOutgoing.deliveryOpt === 'opt3') {
                this.noeditaddress = false;
            }
        }
    }

    save() {
        this.isSaving = true;
        this.shipmentOutgoingService.executeProcess(107, null, this.shipmentOutgoing).subscribe(
            (value) => {
                console.log('this: ', value);
                this.previousState();
            },
            (err) => console.log(err),
            () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
         );
    }

    protected subscribeToSaveResponse(result: Observable<ShipmentOutgoing>) {
        result.subscribe((res: ShipmentOutgoing) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ShipmentOutgoing) {
        this.eventManager.broadcast({ name: 'shipmentOutgoingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'shipmentOutgoing saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'shipmentOutgoing Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    public checkCustomer(idRequestType: number): string {
        let a: string;
        a = null;

        const rs: object = this.checkIfOrganization(idRequestType);
        a = rs['customer'];
        return a;
    }

    public checkSTNK(idRequestType: number): string {
        let a: string;
        a = null;

        const rs: object = this.checkIfOrganization(idRequestType);
        a = rs['stnk'];

        return a;
    }

    public checkIfOrganization(idRequestType: number): object {
        let a: object;
        a = {
            customer : 'organization',
            stnk : 'organization'
        };

        if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP) {
            a = {
                customer : 'organization',
                stnk : 'organization'
            };
        } else if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP_REKANAN) {
            a = {
                customer : 'organization',
                stnk : 'organization'
            };
        } else if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP_INTERNAL) {
            a = {
                customer : 'organization',
                stnk : 'organization'
            };
        } else if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_GROUP_PERSON) {
            a = {
                customer : 'organization',
                stnk : 'person'
            };
        } else if (idRequestType === BaseConstant.REQUEST_TYPE.VSO_DIRECT) {
            a = {
                customer : 'person',
                stnk : 'person'
            }
        } else if (!idRequestType) {
            a = {
                customer : 'person',
                stnk : 'person'
            }
        }
        return a;
    }
}
