import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';
import {SalesUnitRequirement, SalesUnitRequirementService} from './../sales-unit-requirement';

import {ShipmentOutgoing} from './shipment-outgoing.model';
import {ShipmentOutgoingPopupService} from './shipment-outgoing-popup.service';
import {ShipmentOutgoingService} from './shipment-outgoing.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-shipment-outgoing-dialog',
    templateUrl: './shipment-outgoing-dialog.component.html'
})
export class ShipmentOutgoingDialogComponent implements OnInit {

    shipmentOutgoing: ShipmentOutgoing;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected shipmentOutgoingService: ShipmentOutgoingService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.shipmentOutgoing.idShipment !== undefined) {
            this.subscribeToSaveResponse(
                this.shipmentOutgoingService.update(this.shipmentOutgoing));
        } else {
            this.subscribeToSaveResponse(
                this.shipmentOutgoingService.create(this.shipmentOutgoing));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ShipmentOutgoing>) {
        result.subscribe((res: ShipmentOutgoing) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ShipmentOutgoing) {
        this.eventManager.broadcast({ name: 'shipmentOutgoingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'shipmentOutgoing saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'shipmentOutgoing Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-shipment-outgoing-popup',
    template: ''
})
export class ShipmentOutgoingPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected shipmentOutgoingPopupService: ShipmentOutgoingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.shipmentOutgoingPopupService
                    .open(ShipmentOutgoingDialogComponent as Component, params['id']);
            } else {
                this.shipmentOutgoingPopupService
                    .open(ShipmentOutgoingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
