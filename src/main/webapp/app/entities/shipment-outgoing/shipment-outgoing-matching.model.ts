import { BaseEntity } from './../../shared';

export class ShipmentOutgoingMatching implements BaseEntity {
    constructor(
        public id?: any,
        public idinvite?: any,
        public idproduct?: any,
        public productName?: any,
        public idfeature?: any,
        public idframe?: any,
        public idmachine?: any,
        public idowner?: any,
        public dateCreate?: any,
        public qty?: any,
        public qtyBooking?: any,
        public idslip?: any,
        public oldidframe?: any,
        public reqnotNumber?: any
    ) {
        // this.driver = new Driver();
    }
}
