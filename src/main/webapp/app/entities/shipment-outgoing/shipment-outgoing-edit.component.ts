import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ShipmentOutgoing } from './shipment-outgoing.model';
import { ShipmentOutgoingService } from './shipment-outgoing.service';
import { ToasterService} from '../../shared';

@Component({
    selector: 'jhi-shipment-outgoing-edit',
    templateUrl: './shipment-outgoing-edit.component.html'
})
export class ShipmentOutgoingEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    shipmentOutgoing: ShipmentOutgoing;
    isSaving: boolean;

    constructor(
        protected alertService: JhiAlertService,
        protected shipmentOutgoingService: ShipmentOutgoingService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.shipmentOutgoing = new ShipmentOutgoing();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.shipmentOutgoingService.find(id).subscribe((shipmentOutgoing) => {
            this.shipmentOutgoing = shipmentOutgoing;
        });
    }

    previousState() {
        this.router.navigate(['shipment-outgoing']);
    }

    save() {
        this.isSaving = true;
        if (this.shipmentOutgoing.idShipment !== undefined) {
            this.subscribeToSaveResponse(
                this.shipmentOutgoingService.update(this.shipmentOutgoing));
        } else {
            this.subscribeToSaveResponse(
                this.shipmentOutgoingService.create(this.shipmentOutgoing));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ShipmentOutgoing>) {
        result.subscribe((res: ShipmentOutgoing) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ShipmentOutgoing) {
        this.eventManager.broadcast({ name: 'shipmentOutgoingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'shipmentOutgoing saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'shipmentOutgoing Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}
