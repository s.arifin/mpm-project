import { BaseEntity } from './../../shared';
import { Shipment } from '../shipment/shipment.model';
import { Driver } from '../driver';

export class ShipmentOutgoing extends Shipment {
    constructor(
        public id?: any,
        public idShipment?: any,
        public idDriver?: any,
        public otherName?: any,
        public deliveryOpt?: any,
        public deliveryAddress?: any,
        public reasonCancel?: any,
        protected jumlahprint?: any,
    ) {
        super();
        // this.driver = new Driver();
    }
}
