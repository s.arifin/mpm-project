import { BaseEntity } from './../../shared';

export class CustomShipment implements BaseEntity {
    constructor(
        public id?: any,
        public requirementNumber?: any, // sur
        public idframe?: any, // sur
        public dateSchedulle?: any, // shipment
        public saleTypeDescription?: any, // sur
        public salesmanName?: any, // sur
        public coordinatorSales?: any, // sur
        // korsal
        // kode tipe
        public codeType?: any,
        public colorDescription?: any,  // sur
        public acc1?: any, // picking slip
        public acc2?: any, // picking slip
        public acc3?: any, // picking slip
        public acc4?: any, // picking slip
        public acc5?: any, // picking slip
        public acc6?: any, // picking slip
        public acc7?: any, // picking slip
        public acc8?: any, // picking slip
        public acctambah1?: any, // picking slip
        public acctambah2?: any, // picking slip
        public promat1?: any, // picking slip
        public promat2?: any, // picking slip
        public shipmentNumber?: any, // nomor do
        public IVUNumber?: any,
        protected jumlahprint?: any,
        // alamat pengiriman
        public qty ?: any, // sur
        public currentStatus?: any, // shipment status
        public idShipment?: any, // id shipment
        public idRequirement?: any, // id sur
        public idOrder?: any, // id order,vso
        public idSlip?: any, // id picking slip
        public deliveryAddress?: any, // address shipment
        public billingNumber?: any, // nomor ivu
        public driverName?: any, // nama driver
        public idInventoryMovementType?: any,
        public refferenceNumber?: any,
        public idOrderItem?: any,
        public idInternalsrc?: any,
        public srcInternalName?: any,
        public yearAssembly?: any,
        // custom tanpa dari back-end
        public shipmentOutgoingdDescription?: any,
        public color?: any,
        public nameCustomer?: any,
        public noHp?: any,
        public date?: any,
        public hour?: any,
        public address?: any,
        public city?: any,
        public motor?: any,
        public idfeature?: any,
        public idMachine?: any,
    ) {
    }
}
