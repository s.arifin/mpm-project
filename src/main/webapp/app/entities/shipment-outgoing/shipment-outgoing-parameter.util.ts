import { URLSearchParams, BaseRequestOptions } from '@angular/http';

export const createShipmentOutgoingParameterOption = (req?: any): BaseRequestOptions => {
    const options: BaseRequestOptions = new BaseRequestOptions();
    if (req) {
        const params: URLSearchParams = new URLSearchParams();
        params.set('page', req.page);
        params.set('size', req.size);
        if (req.sort) {
            params.paramsMap.set('sort', req.sort);
        }
        params.set('query', req.query);

        params.set('reffNumber', req.shipmentOutgoingPTO.reffNumber);
        params.set('orderDateFrom', req.shipmentOutgoingPTO.orderDateFrom);
        params.set('orderDateThru', req.shipmentOutgoingPTO.orderDateThru);
        params.set('idFrame', req.shipmentOutgoingPTO.idFrame);
        params.set('coordinatorSalesId', req.shipmentOutgoingPTO.coordinatorSalesId);
        params.set('salesmanId', req.shipmentOutgoingPTO.salesmanId);
        params.set('color', req.shipmentOutgoingPTO.color);
        params.set('shipmentNumber', req.shipmentOutgoingPTO.shipmentNumber);
        params.set('shipmentAddress', req.shipmentOutgoingPTO.shipmentAddress);
        params.set('idStatusType', req.shipmentOutgoingPTO.idStatusType);
        params.set('internalId', req.shipmentOutgoingPTO.internalId);
        params.set('dataParam' , req.shipmentOutgoingPTO.dataParam);
        params.set('queryFor' , req.shipmentOutgoingPTO.queryFor);
        params.set('filterType' , req.shipmentOutgoingPTO.filterType);
        params.set('type' , req.shipmentOutgoingPTO.type);

        options.params = params;
    }
    return options;
}
