import { Component, Input, OnInit, OnDestroy, ViewChild, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { Salesman, SalesmanService } from '../salesman';
import { ShipmentOutgoing } from './shipment-outgoing.model';
import { ShipmentOutgoingService } from './shipment-outgoing.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import * as BaseConstant from '../../shared/constants/base.constants';
import { ShipmentOutgoingPopupService } from './shipment-outgoing-popup.service';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LoadingService } from '../../layouts/loading/loading.service';

import { LazyLoadEvent, Button } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { ShipmentOutgoingMatching } from './shipment-outgoing-matching.model';
import { CustomShipment } from './custom-shipment.model';
import { registerModuleFactory } from '@angular/core/src/linker/ng_module_factory_loader';

@Component({
    selector: 'jhi-shipment-outgoing-manual-matching',
    templateUrl: './shipment-outgoing-manual-matching.component.html'
})
export class ShipmentOutgoingManualMatchingComponent implements OnInit {

    currentAccount: any;
    shipmentOutgoings: ShipmentOutgoing[];
    shipmentMatching: ShipmentOutgoingMatching[];
    rowDataTmp: ShipmentOutgoingMatching[];
    customShipment: CustomShipment;
    date1: Date;
    date2: Date;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        protected shipmentOutgoingService: ShipmentOutgoingService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        private loadingService: LoadingService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        public activeModal: NgbActiveModal,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected salesmanService: SalesmanService,
        protected toasterService: ToasterService
    ) {
    }

    loadAll() {
        this.loadingService.loadingStart();
        this.shipmentOutgoingService.queryFindMatching({
            codetype: 'codeType:' + this.customShipment.codeType,
            feature: 'idFeature:' + this.customShipment.idfeature,
            internals: 'idInternal:' + this.principal.getIdInternal(),
            Year: 'yearAssembly:' + this.customShipment.yearAssembly
        }).subscribe(
            (res) => {
                this.shipmentMatching = res.json;
                this.loadingService.loadingStop();
            }
        );
    }

    pilih(data: ShipmentOutgoingMatching) {
        this.loadingService.loadingStart();
        data.idslip = this.customShipment.idSlip;
        data.oldidframe = this.customShipment.idframe;
        // const obj = {...data, ...{idslip: this.customShipment.idSlip}}
        this.shipmentOutgoingService.executeProcessMatching(data).subscribe(
            (res) => {
                this.loadingService.loadingStop();
                this.toasterService.showToaster('success', 'Sucess', 'Manual matching berhasil!');
                this.eventManager.broadcast({ name: 'shipmentOutgoingListModification', content: 'OK'});
                this.cancel()
            },
            (err) => {
                this.loadingService.loadingStop();
                this.toasterService.showToaster('error', 'Error', err.message);
            }
        )
    }

    previousState() {
        this.router.navigate(['shipment-outgoing']);
    }
    updateRowData(event) {
        // if (event.data.id !== undefined) {
        //     this.shipmentOutgoingService.update(event.data)
        //         .subscribe((res: ShipmentOutgoing) =>
        //             this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        // } else {
        //     this.shipmentOutgoingService.create(event.data)
        //         .subscribe((res: ShipmentOutgoing) =>
        //             this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        // }
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/shipment-outgoing', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.shipmentOutgoingService.currentData.subscribe(
            (res) => this.customShipment = res);
        this.loadAll();
    }

    cancel() {
        this.activeModal.dismiss('cancel');
        // this.router.navigate(['shipment-outgoing']);
    }
    save() {
    }

}

@Component({
    selector: 'jhi-shipment-outgoing-popup-manual-matching',
    template: ''
})
export class ShipmentOutgoingPopupManualMatchingComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected shipmentOutgoingPopupService: ShipmentOutgoingPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.shipmentOutgoingPopupService
                    .open(ShipmentOutgoingManualMatchingComponent as Component, params['id']);
            } else {
                this.shipmentOutgoingPopupService
                    .open(ShipmentOutgoingManualMatchingComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
