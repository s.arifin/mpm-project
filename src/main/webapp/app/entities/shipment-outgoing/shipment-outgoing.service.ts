import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { ShipmentOutgoing } from './shipment-outgoing.model';
import { ShipmentOutgoingMatching } from './shipment-outgoing-matching.model';
import { CustomShipment } from './custom-shipment.model';
import { Billing } from './../billing'
import { ResponseWrapper, createRequestOption } from '../../shared';
import { createShipmentOutgoingParameterOption } from './shipment-outgoing-parameter.util';

import * as moment from 'moment';

@Injectable()
export class ShipmentOutgoingService {
    valueTmp = {};
    valueShipmentCustom = {};
    protected itemValues: ShipmentOutgoing[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected dataSource = new BehaviorSubject<any>(this.valueTmp);
    currentData = this.dataSource.asObservable();

    protected enheritancedata = new BehaviorSubject<any>(this.valueShipmentCustom);
    passingData = this.enheritancedata.asObservable();

    protected resourceUrl = SERVER_API_URL + 'api/shipment-outgoings';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/shipment-outgoings';
    protected resourceSearchUrl2 = SERVER_API_URL + 'api/_search/shipment-outgoings-query';

    protected resourceCUrl = process.env.API_C_URL + '/api/PurchaseOrder';
    protected dummy = 'http://localhost:52374/api/PurchaseOrder';
    protected resourceCUrl2 = process.env.API_C_URL + '/api/shipment-outgoings';
    protected dummy2 = 'http://localhost:52374/api/shipment-outgoings';
    protected resourceAXUrl = process.env.API_C_URL + '/api/ax_generalledger';

    constructor(protected http: Http) { }

    buildPO(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }
    // buildPO(item: any): Observable<ResponseWrapper> {
    //     console.log('ini id ioder: ', item)
    //     const copy = JSON.stringify(item);
    //     const options: BaseRequestOptions = new BaseRequestOptions();
    //     options.headers.append('Content-Type', 'application/json');

    //     return this.http.post(`${this.resourceCUrl}`, copy, options).map((res: Response) => {
    //         return res.json();
    //     });
    // }

    create(shipmentOutgoing: ShipmentOutgoing): Observable<ShipmentOutgoing> {
        const copy = this.convert(shipmentOutgoing);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(shipmentOutgoing: ShipmentOutgoing): Observable<ShipmentOutgoing> {
        const copy = this.convert(shipmentOutgoing);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    updateC(shipmentOutgoing: ShipmentOutgoing): Observable<ShipmentOutgoing> {
        const copy = this.convert(shipmentOutgoing);
        return this.http.put(this.resourceCUrl2, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<ShipmentOutgoing> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findC(id: any): Observable<ShipmentOutgoing> {
        return this.http.get(`${this.resourceCUrl2}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    queryFindBilling(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/findBilling', options)
        .map((res: Response) => this.convertResponse(res));
    }

    queryFindBillingC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl2 + '/findBilling', options)
        .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        if (options) {options.params.set('idstatustype', req.idstatustype); }
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFindMatching(req?: any): Observable<ResponseWrapper> {
        // const copy = JSON.stringify(data);
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl2 + '/manualMatching', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFindMatchingPos(req?: any): Observable<ResponseWrapper> {
        // const copy = JSON.stringify(data);
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl2 + '/manualMatchingPos', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFindMatchingEvent(req?: any): Observable<ResponseWrapper> {
        // const copy = JSON.stringify(data);
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl2 + '/manualMatchingEvent', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryCekCetakan(req?: any): Observable<ResponseWrapper> {
        // const copy = JSON.stringify(data);
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl2 + '/cekCetakan', options)
            .map((res: Response) => this.convertResponse(res));
    }

    executeProcessMatching(query?: ShipmentOutgoingMatching): Observable<ResponseWrapper> {
        const copy = JSON.stringify(query);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');
        return this.http.post(`${this.resourceCUrl2}/executeMatching`, copy, options).map((res: Response) => this.convertResponse(res));
    }

    getTodayAssignDriver(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        // if (options) {options.params.set('idstatustype', req.idstatustype); }
        // console.log('idstatustype', options);
        return this.http.get(`${this.resourceUrl}/getToday-assign-driver/`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(shipmentOutgoing: ShipmentOutgoing, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(shipmentOutgoing);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, shipmentOutgoing: ShipmentOutgoing): Observable<ShipmentOutgoing> {
        const copy = this.convert(shipmentOutgoing);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeProcessC(id: Number, param: String, shipmentOutgoing: ShipmentOutgoing): Observable<ShipmentOutgoing> {
        const copy = this.convert(shipmentOutgoing);
        return this.http.post(`${this.resourceCUrl2}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, shipmentOutgoings: ShipmentOutgoing[]): Observable<ShipmentOutgoing[]> {
        const copy = this.convertList(shipmentOutgoings);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcessC(id: Number, param: String, shipmentOutgoings: ShipmentOutgoing[]): Observable<ShipmentOutgoing[]> {
        const copy = this.convertList(shipmentOutgoings);
        return this.http.post(`${this.resourceCUrl2}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    searchUnitPrep(req?: any): Observable<ResponseWrapper> {
        const options = createShipmentOutgoingParameterOption(req);
        return this.http.get(`${this.resourceSearchUrl2}/`, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(shipmentOutgoing: ShipmentOutgoing): ShipmentOutgoing {
        if (shipmentOutgoing === null || shipmentOutgoing === {}) {
            return {};
        }
        // const copy: ShipmentOutgoing = Object.assign({}, shipmentOutgoing);
        const copy: ShipmentOutgoing = JSON.parse(JSON.stringify(shipmentOutgoing));
        return copy;
    }

    protected convertList(shipmentOutgoings: ShipmentOutgoing[]): ShipmentOutgoing[] {
        const copy: ShipmentOutgoing[] = shipmentOutgoings;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    // protected convertCustom(customShipment: CustomShipment): CustomShipment {
    //     if (customShipment === null || customShipment === {}) {
    //         return {};
    //     }
    //     // const copy: ShipmentOutgoing = Object.assign({}, shipmentOutgoing);
    //     const copy: CustomShipment = JSON.parse(JSON.stringify(customShipment));
    //     return copy;
    // }

    // protected convertListCustom(customShipments: CustomShipment[]): CustomShipment[] {
    //     const copy: CustomShipment[] = customShipments;
    //     copy.forEach((item) => {
    //         item = this.convertCustom(item);
    //     });
    //     return copy;
    // }
    pushItems(data: ShipmentOutgoing[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
    changeShareDataTmp(datatmp) {
        console.log('surnya service', datatmp)
        this.dataSource.next(datatmp);
    }
    passingCustomData(custom) {
        this.enheritancedata.next(custom);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    GLInventoryTransfer(idframe: string): Observable<any> {
        return this.http.get(this.resourceAXUrl + '/GLInventoryTransferOut?idframe=' + idframe).map((res: Response) => {
            return res.json();
        });
    }

}
