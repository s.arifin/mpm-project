import {Component, OnInit, OnDestroy} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

import {Mechanic, MechanicService} from '../mechanic';
import {PickingSlip, PickingSlipService} from '../picking-slip'

import {ShipmentOutgoing} from './shipment-outgoing.model';
import {ShipmentOutgoingPopupService} from './shipment-outgoing-popup.service';
import {ShipmentOutgoingService} from './shipment-outgoing.service';
import {CustomShipment} from './custom-shipment.model';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-shipment-outgoing-dialog-detail-delivery',
    templateUrl: './shipment-outgoing-dialog-detail-delivery.component.html'
})
export class ShipmentOutgoingDialogDetailDeliveryComponent implements OnInit {

    pickingSlip: PickingSlip;
    mechanicInternal: Mechanic[];
    mechanicExternal: Mechanic[];

    stat = 1;
    mecInternal: any;
    mecExternal: any;

    constructor(
        protected pickingSlipService: PickingSlipService,
        protected mechanicService: MechanicService,
        protected router: Router,
        protected principal: Principal,
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected shipmentOutgoingService: ShipmentOutgoingService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.mechanicExternal = new Array <Mechanic>();
        this.mechanicInternal = new Array <Mechanic>();
    }

    ngOnInit() {
        this.shipmentOutgoingService.currentData.subscribe((pickingSlip) => this.pickingSlip = pickingSlip);
        this.mechanicService.queryFilterBy({
            idInternal : this.principal.getIdInternal(),
            size: 1000})
        .subscribe((res: ResponseWrapper) => this.mechanicType(res.json, res.headers ), (res: ResponseWrapper) => this.onError(res.json));
        // this.loadAll(this.pickingSlip);
    }

    loadAll(pickingSlip) {
        console.log('loadlall', pickingSlip);
        this.pickingSlipService.find(pickingSlip.idSlip).subscribe((data) => {
            this.pickingSlip = data
        })
    }

    protected mechanicType(data, header) {
        if (data.length > 0) {

            for (let i = 0; i < data.length; i++) {
                if ( data[i].external === 'true') {
                    this.mechanicExternal.push(data[i]);
                } else {
                    this.mechanicInternal.push(data[i]);
                }

            }

        }
    }
    trackMechanicById(index: number, item: Mechanic) {
        // console.log('index', index);
        return item.idPartyRole;
    }

    save() {
        this.subscribeToSaveResponse(
                this.pickingSlipService.update(this.pickingSlip));
                //  this.shipmentOutgoingService.update(this.pickingSlip));
     }

     protected subscribeToSaveResponse(result: Observable<PickingSlip>) {
         result.subscribe((res: PickingSlip) =>
             this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
     }

     protected onSaveSuccess(result: PickingSlip) {
         this.eventManager.broadcast({ name: 'shipmentOutgoingListModification', content: 'OK'});
         this.toaster.showToaster('info', 'Save', 'PickingSlip saved !');
         this.cancel();
     }

     protected onSaveError(error) {
         try {
             error.json();
         } catch (exception) {
             error.message = error.text();
         }
         this.onError(error);
     }

     protected onSuccess(data) {
         console.log('datanyadelivery', data);
        //  data.dateSchedulle = new Date(data.dateSchedulle)
         this.pickingSlip = data;
     }

     protected onError(error) {
         this.toaster.showToaster('warning', 'data changed', error.message);
         this.alertService.error(error.message, null, null);
     }

    cancel() {
        this.pickingSlip = {};
        this.shipmentOutgoingService.changeShareDataTmp(this.pickingSlip);
        this.activeModal.dismiss('cancel');
        // this.router.navigate(['shipment-outgoing']);
    }

    status() {
        if (this.stat === 1) {
            this.pickingSlip.mechanicinternal = '';
        } else {
            this.pickingSlip.mechanicexternal = '';
        }
    }

}

@Component({
    selector: 'jhi-shipment-outgoing-popup-detail-delivery',
    template: ''
})
export class ShipmentOutgoingPopupDetailDeliveryComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected shipmentOutgoingPopupService: ShipmentOutgoingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.shipmentOutgoingPopupService
                    .open(ShipmentOutgoingDialogDetailDeliveryComponent as Component, params['id']);
            } else {
                this.shipmentOutgoingPopupService
                    .open(ShipmentOutgoingDialogDetailDeliveryComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
