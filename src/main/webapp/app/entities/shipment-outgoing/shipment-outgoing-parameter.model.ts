export class ShipmentOutgoingParameters {
    constructor(
        public reffNumber?: any,
        public orderDateFrom?: any,
        public orderDateThru?: any,
        public idFrame?: String,
        public coordinatorSalesId?: String,
        public salesmanId?: String,
        public color?: String,
        public shipmentNumber?: String,
        public shipmentAddress?: String,
        public idStatusType?: Number,
        public internalId?: String,
        public dataParam?: String,
        public queryFor?: String,
        public filterType?: Boolean,
        public type?: Number
    ) {
        this.reffNumber = null;
        this.orderDateFrom = null;
        this.orderDateThru = null;
        this.idFrame = null;
        this.coordinatorSalesId = null;
        this.salesmanId = null;
        this.color = null;
        this.shipmentNumber = null;
        this.shipmentAddress = null;
        this.idStatusType = null;
        this.internalId = null;
        this.dataParam = null;
        this.queryFor = null;
        this.filterType = false;
        this.type = 0;
    }
}
