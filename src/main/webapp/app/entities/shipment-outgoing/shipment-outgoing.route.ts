import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ShipmentOutgoingComponent } from './shipment-outgoing.component';
import { ShipmentOutgoingDetailComponent } from './shipment-outgoing-detail.component';
import { ShipmentOutgoingJadwalDetailComponent } from './shipment-outgoing-jadwal-detail.component';
import { ShipmentOutgoingEditComponent } from './shipment-outgoing-edit.component';
import { ShipmentOutgoingPopupComponent } from './shipment-outgoing-dialog.component';
import { ShipmentOutgoingPopupDetailDeliveryComponent } from './shipment-outgoing-dialog-detail-delivery.component';
import { ShipmentOutgoingPopupManualMatchingComponent } from './shipment-outgoing-manual-matching.component';

@Injectable()
export class ShipmentOutgoingResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idShipment,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const shipmentOutgoingRoute: Routes = [
    {
        path: 'shipment-outgoing',
        component: ShipmentOutgoingComponent,
        resolve: {
            'pagingParams': ShipmentOutgoingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const shipmentOutgoingPopupRoute: Routes = [
    {
        path: 'shipment-outgoing/:idrek/:idship/:reffNumber/detail/shipment',
        component: ShipmentOutgoingDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'shipment-outgoing/:sur/detail',
        component: ShipmentOutgoingDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'shipment-outgoing/jadwal-detail',
        component: ShipmentOutgoingJadwalDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'shipment-outgoing-new',
        component: ShipmentOutgoingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'shipment-outgoing/:id/edit',
        component: ShipmentOutgoingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'shipment-outgoing-popup-new',
        component: ShipmentOutgoingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shipment-outgoing/:id/popup-edit',
        component: ShipmentOutgoingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shipment-outgoing-detail-delivery',
        component: ShipmentOutgoingPopupDetailDeliveryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shipment-outgoing-manual-matching',
        component: ShipmentOutgoingPopupManualMatchingComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
