import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import * as ShipmentOutgoingConstrants from '../../shared/constants/shipmentOutgoing.constrants';

import { ShipmentOutgoing } from './shipment-outgoing.model';
import { CustomShipment } from './custom-shipment.model';
import { ShipmentOutgoingService } from './shipment-outgoing.service';
import {SalesUnitRequirement, SalesUnitRequirementService} from '../sales-unit-requirement';
import {VehicleSalesOrder, VehicleSalesOrderService} from '../vehicle-sales-order';
import {Receipt, ReceiptService} from '../receipt';
import { Billing } from './../billing';

import { LoadingService } from '../../layouts/loading/loading.service';

import { ToasterService} from '../../shared';
import { billingDisbursementPopupRoute } from '../billing-disbursement';

@Component({
    selector: 'jhi-shipment-outgoing-jadwal-detail',
    templateUrl: './shipment-outgoing-jadwal-detail.component.html'
})

export class ShipmentOutgoingJadwalDetailComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    shipmentTMP: ShipmentOutgoing;
    customShipment: CustomShipment;
    vehicleSalesOrder: VehicleSalesOrder;
    page: any;
    private billing: Billing[];
    billingNumber: any;
    dateBilling: any;

    batalKirimDialog: boolean;
    statusPengirimanDialog: boolean;
    salesUnitRequirement: SalesUnitRequirement;
    receipt: Receipt;
    receipts: Receipt[];
    datas: any;
    statusPayment: boolean;
    messages: Array<Object> = [
        {
            severity: 'info',
            summary: 'Info',
            detail: 'Periksa Pembayaran'
        }
    ];

    constructor(
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected vehicleSalesOrderService: VehicleSalesOrderService,
        protected receiptService: ReceiptService,
        protected shipmentOutgoingService: ShipmentOutgoingService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected alertService: JhiAlertService,
        protected loadingService: LoadingService,
        protected toaster: ToasterService,
        protected principal: Principal,
    ) {
        this.batalKirimDialog = false;
        this.statusPengirimanDialog = false;
        this.datas = {
            dpmurni: 0,
            tandajadi: 0,
            tenor: null,
            angsuran: null,
            tanggalPembayaran: null,
            sisa: null,
            totalPembayaran: 0
        }
        this.statusPayment = false;
        this.vehicleSalesOrder = new VehicleSalesOrder();
        this.billing = new Array<Billing>();
        this.billingNumber = null;
        this.dateBilling = null;
    }

    ngOnInit() {
        this.shipmentOutgoingService.currentData.subscribe( (shipmentTMP) => this.shipmentTMP = shipmentTMP)
        this.shipmentOutgoingService.passingData.subscribe(
             (customShipment) => {
                this.customShipment = customShipment;
                this.shipmentOutgoingService.queryFindBilling({
                idOrderItem: this.customShipment.idOrderItem,
                page: this.page - 1,
                size: 1})
                .subscribe(
                    (databilling) => {
                    this.billing = databilling.json;
                    this.billingNumber = this.billing[0].billingNumber
                    this.dateBilling = this.billing[0].dateCreate;
                    console.log('dataBilling', this.billing);
                    console.log('dataBilling Number', this.billingNumber);
                    console.log('dataBilling Number', this.dateBilling);
                    }
                );
            }
        )
        this.vehicleSalesOrderService.find(this.customShipment.idOrder, this.principal.getIdInternal()).subscribe(
            (data) => {
            console.log('datanya', data);
            this.vehicleSalesOrder = data;
            console.log('datanya vso', this.vehicleSalesOrder);
            // this.loadingService.loadingStop();
            // if (this.orderItem.length > 0) {
            //     this.orderItem[0].idFrame = data.salesUnitRequirement.idframe;
            //     this.orderItem[0].color = data.salesUnitRequirement.colorDescription;
            // }
            }
        );
        // this.load(this.customShipment);
        // this.shipmentOutgoingService.currentData.subscribe(
        //     (res: ResponseWrapper) => this.onSuccess(res.json),
        //     (res: ResponseWrapper) => this.onError(res.json)
        // );
        // .subscribe((res: ResponseWrapper) => { this.shipmentTMP = res.json ;
            // .filter((o) => o.idEventType === 13 || o.idEventType === 14);
        //     console.log('res.jsoneventtype  ', res.json);
        // }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
    }

    load(customShipment) {
        this.loadingService.loadingStart();
        this.vehicleSalesOrderService.find(customShipment.idOrder, this.principal.getIdInternal()).subscribe(
            (data) => {
            console.log('datanya', data);
            this.vehicleSalesOrder = data;
            this.loadingService.loadingStop();
            // if (this.orderItem.length > 0) {
            //     this.orderItem[0].idFrame = data.salesUnitRequirement.idframe;
            //     this.orderItem[0].color = data.salesUnitRequirement.colorDescription;
            // }
            }
        );
    }

    cancelDO(rowdata): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                console.log('cancel', rowdata);
                // console.log('KIRIM_GUDANG', ShipmentOutgoingConstrants.KIRIM_GUDANG);
                this.shipmentOutgoingService.executeProcess(ShipmentOutgoingConstrants.CANCEL, null, rowdata ).subscribe(
                    (value) => console.log('this: ', value),
                    (err) => console.log(err),
                    () => {
                        this.previousState();
                        resolve()
                    }
                );
            }
        )
    }

    update(data) {
       this.subscribeToSaveResponse(
                this.shipmentOutgoingService.update(data));
    }

    showBatalKirimDialog() {
        this.batalKirimDialog = true;
    }

    cancelShipment(): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                // console.log('cancel', rowdata);
                // console.log('KIRIM_GUDANG', ShipmentOutgoingConstrants.KIRIM_GUDANG);
                this.shipmentOutgoingService.executeProcess(ShipmentOutgoingConstrants.CANCEL, null, this.shipmentTMP ).subscribe(
                    (value) => console.log('this: ', value),
                    (err) => console.log(err),
                    () => {
                        this.previousState();
                        resolve()
                    }
                );
            }
        )
    }

    completeShipment(): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                // console.log('cancel', rowdata);
                // console.log('KIRIM_GUDANG', ShipmentOutgoingConstrants.KIRIM_GUDANG);
                this.shipmentOutgoingService.executeProcess(ShipmentOutgoingConstrants.COMPLETE, null, this.shipmentTMP ).subscribe(
                    (value) => console.log('this: ', value),
                    (err) => console.log(err),
                    () => {
                        this.previousState();
                        resolve()
                    }
                );
            }
        )
    }

    checkPayment() {
        this.vehicleSalesOrderService.findSurByOrderId(this.customShipment.idOrder, this.principal.getIdInternal()).subscribe((sur) => {
            this.salesUnitRequirement = sur;
            console.log('this.salesUnitRequirement', this.salesUnitRequirement);
            this.receiptService.findByPaidFromId(this.salesUnitRequirement.customerId).subscribe(
                (res: ResponseWrapper) => {
                    console.log('res', res);
                    this.receipts = res.json;
                // this.receipts = receipt;
                for (const rec in this.receipts) {
                    // this.datas.dpmurni += this.receipts[id].amount;
                    if ( this.receipts[rec].amount !== 0 ) {
                        // this.datas.dpmurni += this.receipts[rec].amount;
                        this.datas.totalPembayaran += this.receipts[rec].amount;
                        console.log(rec);
                    }
                    // this.datas.
                }
                console.log('this.datas.totalPembayaran', this.datas.totalPembayaran);
                console.log('this.receipt', this.receipts);
            });
            if (this.salesUnitRequirement.minPayment >= this.datas.totalPembayaran) {
                console.log('lunas');
                this.statusPayment = true;
                this.toaster.showToaster('info', 'Payment', 'Pembayaran Lunas');
            }else {
                console.log('belum lunas');
                this.statusPayment = true; // sementara diloloskan sebelum ar dapat digunakan untuk cek pelunasan (permintaan pak yosep dan pak adith)
                // this.statusPayment = false;
                this.toaster.showToaster('info', 'Payment', 'Pembayaran Belum Lunas');
            }
        });
    }

    showStatusPengirimanDialog() {
        this.statusPengirimanDialog = true;
    }

    protected subscribeToSaveResponse(result: Observable<ShipmentOutgoing>) {
        result.subscribe((res: ShipmentOutgoing) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ShipmentOutgoing) {
        this.eventManager.broadcast({ name: 'ShipmentOutgoingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'ShipmentOutgoing saved !');
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    protected onSuccess(data) {
        console.log('datanyajadwal', data);
        data.dateSchedulle = new Date(data.dateSchedulle)
        this.shipmentTMP = data;
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'data changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    protected previousState() {
        this.shipmentTMP = {};
        this.shipmentOutgoingService.changeShareDataTmp(this.shipmentTMP);
        this.router.navigate(['shipment-outgoing']);
    }
}
