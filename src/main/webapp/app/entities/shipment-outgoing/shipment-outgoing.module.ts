import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';

import {MpmSalesUnitRequirementModule} from '../sales-unit-requirement/sales-unit-requirement.module';

import {MpmCommunicationEventDeliveryModule } from '../communication-event-delivery/communication-event-delivery.module';

import {
    ShipmentOutgoingService,
    ShipmentOutgoingPopupService,
    ShipmentOutgoingComponent,
    ShipmentOutgoingDialogComponent,
    ShipmentOutgoingPopupComponent,
    shipmentOutgoingRoute,
    shipmentOutgoingPopupRoute,
    ShipmentOutgoingResolvePagingParams,
    ShipmentOutgoingEditComponent,
    ShipmentOutgoingManualMatchingComponent,
    ShipmentOutgoingPopupManualMatchingComponent,
    ShipmentOutgoingDetailComponent,
    ShipmentOutgoingJadwalDetailComponent,
    ShipmentOutgoingDialogDetailDeliveryComponent,
    ShipmentOutgoingPopupDetailDeliveryComponent,
    ShipmentOutgoingGridUnitComponent,
    ShipmentOutgoingGridDeliveryComponent,
    ShipmentOutgoingGridJadwalComponent,
    SalesUnitRequirementCustomerViewShipmentComponent,
    SalesUnitRequirementDealDetailShipmentViewComponent,
    SalesUnitRequirementSTNKShipmentViewComponent,
    PartyDocumentAsListShipmentComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         MessagesModule,
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         InputSwitchModule,
         LightboxModule,
         RadioButtonModule
        } from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';
import { MessagePipe } from '../../shared/pipe/message.pipe';

const ENTITY_STATES = [
    ...shipmentOutgoingRoute,
    ...shipmentOutgoingPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        MessagesModule,
        MpmSalesUnitRequirementModule,
        MpmCommunicationEventDeliveryModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,

        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        InputSwitchModule,
        CurrencyMaskModule,
        RadioButtonModule
    ],
    exports: [
        ShipmentOutgoingComponent,
        ShipmentOutgoingEditComponent,
        ShipmentOutgoingDetailComponent,
        ShipmentOutgoingJadwalDetailComponent,
        ShipmentOutgoingGridUnitComponent,
        ShipmentOutgoingGridDeliveryComponent,
        ShipmentOutgoingGridJadwalComponent,
        SalesUnitRequirementCustomerViewShipmentComponent,
        SalesUnitRequirementDealDetailShipmentViewComponent,
        SalesUnitRequirementSTNKShipmentViewComponent,
        PartyDocumentAsListShipmentComponent,
        ShipmentOutgoingManualMatchingComponent,
    ],
    declarations: [
        ShipmentOutgoingComponent,
        ShipmentOutgoingDialogComponent,
        ShipmentOutgoingPopupComponent,
        ShipmentOutgoingEditComponent,
        ShipmentOutgoingDetailComponent,
        ShipmentOutgoingJadwalDetailComponent,
        ShipmentOutgoingDialogDetailDeliveryComponent,
        ShipmentOutgoingPopupDetailDeliveryComponent,
        ShipmentOutgoingGridUnitComponent,
        ShipmentOutgoingGridDeliveryComponent,
        ShipmentOutgoingGridJadwalComponent,
        SalesUnitRequirementCustomerViewShipmentComponent,
        SalesUnitRequirementDealDetailShipmentViewComponent,
        SalesUnitRequirementSTNKShipmentViewComponent,
        PartyDocumentAsListShipmentComponent,
        ShipmentOutgoingManualMatchingComponent,
        ShipmentOutgoingPopupManualMatchingComponent,
    ],
    entryComponents: [
        ShipmentOutgoingComponent,
        ShipmentOutgoingDialogComponent,
        ShipmentOutgoingPopupComponent,
        ShipmentOutgoingEditComponent,
        ShipmentOutgoingDetailComponent,
        ShipmentOutgoingJadwalDetailComponent,
        ShipmentOutgoingDialogDetailDeliveryComponent,
        ShipmentOutgoingPopupDetailDeliveryComponent,
        ShipmentOutgoingGridUnitComponent,
        ShipmentOutgoingGridDeliveryComponent,
        ShipmentOutgoingGridJadwalComponent,
        SalesUnitRequirementCustomerViewShipmentComponent,
        SalesUnitRequirementDealDetailShipmentViewComponent,
        SalesUnitRequirementSTNKShipmentViewComponent,
        PartyDocumentAsListShipmentComponent,
        ShipmentOutgoingManualMatchingComponent,
        ShipmentOutgoingPopupManualMatchingComponent,
    ],
    providers: [
        ShipmentOutgoingService,
        ShipmentOutgoingPopupService,
        ShipmentOutgoingResolvePagingParams
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmShipmentOutgoingModule {}
