import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { Salesman, SalesmanService} from '../salesman';
import { ShipmentOutgoing } from './shipment-outgoing.model';
import { ShipmentFilter } from './shipment-filter.model';
import { ShipmentOutgoingService } from './shipment-outgoing.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import * as BaseConstant from '../../shared/constants/base.constants';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-shipment-outgoing',
    templateUrl: './shipment-outgoing.component.html'
})
export class ShipmentOutgoingComponent implements OnInit, OnDestroy {

    currentAccount: any;
    shipmentOutgoings: ShipmentOutgoing[];
    date1: Date;
    date2: Date;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    statusFilter = [];
    statusFilterJadwal = [];
    selectedstatus: {};
    dataweb: any;
    korsals: Salesman[];
    salesmans: Salesman[];
    selectedKorsal: string;
    selectedSalesman: string;
    selectedType: any;
    shipmentFilter: ShipmentFilter;

    modalKonfirmasiOther: boolean;

    status_unit_prep = [];
    status_delivery = [];
    status_jadwal = [];
    status_filter= [];
    filterType = [];

    statusOne: boolean;
    statusTwo: boolean;
    statusThree: boolean;
    statusFour: boolean;
    statusFive: boolean;

    constructor(
        protected shipmentOutgoingService: ShipmentOutgoingService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected salesmanService: SalesmanService,
        protected toasterService: ToasterService
    ) {
        this.modalKonfirmasiOther = false;
        this.status_unit_prep = [10];
        this.status_delivery = [25];
        this.status_jadwal = [21, 22, 23, 24];
        this.status_filter = [24];
        this.statusOne = true;
        this.statusTwo = true;
        this.statusThree = true;
        this.statusFour = true;
        this.statusFive = true;
        this.filterType = [
            {typeId: 1, typeDeskripsi: 'Transfer Unit', },
            {typeId: 2, typeDeskripsi: 'Retail dan GC'}
        ]
        this.statusFilter = [
            {statusId: 1, statusDeskripsi: 'Delivery Order'},
            {statusId: 2, statusDeskripsi: 'Jadwal Pengiriman'}
        ]
        this.statusFilterJadwal = [
            {statusId: 1, statusDeskripsi: 'Print BAST'},
            {statusId: 2, statusDeskripsi: 'Assign To'},
            {statusId: 3, statusDeskripsi: 'Complete Shipment'},
            {statusId: 4, statusDeskripsi: 'Cancel Shipment'}
        ]
        this.korsals = [];
        this.salesmans = [];
        this.shipmentFilter = new ShipmentFilter();
        this.selectedType = null;
    }

    loadAll() {

    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/shipment-outgoing', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    filterUnitPrep() {
        if ( this.selectedType !== null ) {
            this.shipmentFilter.ShipmentType = this.selectedType;
            this.shipmentFilter.filterShipmentType = true;
            console.log( this.shipmentFilter );
            this.shipmentOutgoingService.passingCustomData(this.shipmentFilter);
            this.eventManager.broadcast({ name: 'unitPrepFilter', content: 'OK'});
        }
        if ( this.selectedType === null ) {
            this.toasterService.showToaster( 'info' , 'Gagal' , 'Tidal ada filter yang dipilih' );
        }
    }

    search() {
        console.log('xxx');
        // if (!query) {
        //     return this.clear();
        // }
        // this.page = 0;
        // this.currentSearch = query;
        // this.router.navigate(['/shipment-outgoing', {
        //     search: this.currentSearch,
        //     page: this.page,
        //     sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        // }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadKorsal();
    }

    ngOnDestroy() {
    }
    public doRefresh(): void {
        this.destroyComponentChild().then(
            (res) => {
                setTimeout(
                    () => {
                        this.buildComponentChild();
                }, 1000);
            }
        );
    }
    protected destroyComponentChild(): Promise<void> {
        return new Promise<void> (
            (resolve) => {
                this.statusOne = false;
                this.statusTwo = false;
                this.statusThree = false;
                this.statusFour = false;
                this.statusFive = false;
                resolve();
            }
        )
    }

    protected  loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('tarik data korsal', res.json);
                this.korsals = res.json;
            }
        )
    }

    public selectSalesByKorsal(): void {
        const obj: object = {
            idCoordinator : this.selectedKorsal
        }
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('selected Sales' , res.json);
                this.salesmans = res.json;
            }
        )
    }

    protected buildComponentChild(): void {
        this.statusOne = true;
        this.statusTwo = true;
        this.statusThree = true;
        this.statusFour = true;
        this.statusFive = true;
    }
    registerChangeInShipmentOutgoings() {
        this.eventSubscriber = this.eventManager.subscribe('shipmentOutgoingListModification',
            (response) => {
                this.doRefresh();
            }
        );
    }

    filterstatus() {
        console.log('selected', this.selectedstatus);
        this.dataweb = this.selectedstatus;
        if (this.selectedstatus = 1) {
            this.statusOne = true;
        } else if (this.selectedstatus = 2) {
            this.statusTwo = true;
        } else if (this.selectedstatus = 3) {
            this.statusThree = true;
        } else if (this.selectedstatus = 4) {
            this.statusFour = true;
        } else if (this.selectedstatus = null) {
            this.statusFive = true;
        }

    }
    trackStatusById(index: number, item ) {
        // console.log('index', index);
        // console.log('item', item);
        return item.statusId;
    }

    buildReindex() {
        this.shipmentOutgoingService.process({command: 'buildIndex'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }

}
