import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { UomConversion } from './uom-conversion.model';
import { UomConversionService } from './uom-conversion.service';

@Injectable()
export class UomConversionPopupService {
    protected ngbModalRef: NgbModalRef;
    idUomTo: any;
    idUomFrom: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected uomConversionService: UomConversionService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.uomConversionService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.uomConversionModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new UomConversion();
                    data.uomToId = this.idUomTo;
                    data.uomFromId = this.idUomFrom;
                    this.ngbModalRef = this.uomConversionModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    uomConversionModalRef(component: Component, uomConversion: UomConversion): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.uomConversion = uomConversion;
        modalRef.componentInstance.idUomTo = this.idUomTo;
        modalRef.componentInstance.idUomFrom = this.idUomFrom;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.uomConversionLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    uomConversionLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idUomTo = this.idUomTo;
        modalRef.componentInstance.idUomFrom = this.idUomFrom;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
