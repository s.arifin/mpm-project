import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UomConversion } from './uom-conversion.model';
import { UomConversionPopupService } from './uom-conversion-popup.service';
import { UomConversionService } from './uom-conversion.service';
import { ToasterService } from '../../shared';
import { Uom, UomService } from '../uom';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-uom-conversion-dialog',
    templateUrl: './uom-conversion-dialog.component.html'
})
export class UomConversionDialogComponent implements OnInit {

    uomConversion: UomConversion;
    isSaving: boolean;
    idUomTo: any;
    idUomFrom: any;

    uoms: Uom[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected uomConversionService: UomConversionService,
        protected uomService: UomService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.uomService.query()
            .subscribe((res: ResponseWrapper) => { this.uoms = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.uomConversion.idUomConversion !== undefined) {
            this.subscribeToSaveResponse(
                this.uomConversionService.update(this.uomConversion));
        } else {
            this.subscribeToSaveResponse(
                this.uomConversionService.create(this.uomConversion));
        }
    }

    protected subscribeToSaveResponse(result: Observable<UomConversion>) {
        result.subscribe((res: UomConversion) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: UomConversion) {
        this.eventManager.broadcast({ name: 'uomConversionListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'uomConversion saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'uomConversion Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUomById(index: number, item: Uom) {
        return item.idUom;
    }
}

@Component({
    selector: 'jhi-uom-conversion-popup',
    template: ''
})
export class UomConversionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected uomConversionPopupService: UomConversionPopupService
    ) {}

    ngOnInit() {
        this.uomConversionPopupService.idUomTo = undefined;
        this.uomConversionPopupService.idUomFrom = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.uomConversionPopupService
                    .open(UomConversionDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.uomConversionPopupService.parent = params['parent'];
                this.uomConversionPopupService
                    .open(UomConversionDialogComponent as Component);
            } else {
                this.uomConversionPopupService
                    .open(UomConversionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
