import { BaseEntity } from './../../shared';

export class UomConversion implements BaseEntity {
    constructor(
        public id?: number,
        public idUomConversion?: number,
        public factor?: number,
        public uomToId?: any,
        public uomFromId?: any,
    ) {
    }
}
