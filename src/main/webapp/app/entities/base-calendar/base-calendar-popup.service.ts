import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { BaseCalendar } from './base-calendar.model';
import { BaseCalendarService } from './base-calendar.service';

@Injectable()
export class BaseCalendarPopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected baseCalendarService: BaseCalendarService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.baseCalendarService.find(id).subscribe((data) => {
                    if (data.dateKey) {
                        data.dateKey = new Date(data.dateKey);
                    }
                    if (data.dateFrom) {
                        data.dateFrom = new Date(data.dateFrom);
                    }
                    if (data.dateThru) {
                        data.dateThru = new Date(data.dateThru);
                    }
                    this.ngbModalRef = this.baseCalendarModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new BaseCalendar();
                    this.ngbModalRef = this.baseCalendarModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    baseCalendarModalRef(component: Component, baseCalendar: BaseCalendar): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.baseCalendar = baseCalendar;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
