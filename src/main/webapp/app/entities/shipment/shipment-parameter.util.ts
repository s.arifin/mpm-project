import { URLSearchParams, BaseRequestOptions } from '@angular/http';

export const createShipmentParameterOption = (req?: any): BaseRequestOptions => {
    const options: BaseRequestOptions = new BaseRequestOptions();
    if (req) {
        const params: URLSearchParams = new URLSearchParams();
        params.set('page', req.page);
        params.set('size', req.size);
        if (req.sort) {
            params.paramsMap.set('sort', req.sort);
        }
        params.set('query', req.query);

        params.set('status', req.shipmentPTO.status);
        params.set('statusNotIn', req.shipmentPTO.statusNotIn);
        params.set('packageReceiptDateFrom', req.shipmentPTO.packageReceiptDateFrom);
        params.set('packageReceiptDateThru', req.shipmentPTO.packageReceiptDateThru);
        params.set('shipmentIncomingDateFrom', req.shipmentPTO.shipmentIncomingDateFrom);
        params.set('shipmentIncomingDateThru', req.shipmentPTO.shipmentIncomingDateThru);
        params.set('internalId', req.shipmentPTO.internalId);

        options.params = params;
    }
    return options;
}
