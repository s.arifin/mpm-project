import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Shipment } from './shipment.model';
import { ShipmentService } from './shipment.service';

@Injectable()
export class ShipmentPopupService {
    protected ngbModalRef: NgbModalRef;
    idShipmentType: any;
    idShipFrom: any;
    idShipTo: any;
    idAddressFrom: any;
    idAddressTo: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected shipmentService: ShipmentService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.shipmentService.find(id).subscribe((data) => {
                    // if (data.dateSchedulle) {
                    //    data.dateSchedulle = this.datePipe
                    //        .transform(data.dateSchedulle, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.shipmentModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new Shipment();
                    data.shipmentTypeId = this.idShipmentType;
                    data.shipFromId = this.idShipFrom;
                    data.shipToId = this.idShipTo;
                    data.addressFromId = this.idAddressFrom;
                    data.addressToId = this.idAddressTo;
                    this.ngbModalRef = this.shipmentModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    shipmentModalRef(component: Component, shipment: Shipment): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.shipment = shipment;
        modalRef.componentInstance.idShipmentType = this.idShipmentType;
        modalRef.componentInstance.idShipFrom = this.idShipFrom;
        modalRef.componentInstance.idShipTo = this.idShipTo;
        modalRef.componentInstance.idAddressFrom = this.idAddressFrom;
        modalRef.componentInstance.idAddressTo = this.idAddressTo;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.shipmentLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    shipmentLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
