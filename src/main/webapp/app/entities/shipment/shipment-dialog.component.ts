import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Shipment } from './shipment.model';
import { ShipmentPopupService } from './shipment-popup.service';
import { ShipmentService } from './shipment.service';
import { ToasterService } from '../../shared';
import { ShipmentType, ShipmentTypeService } from '../shipment-type';
import { ShipTo, ShipToService } from '../ship-to';
import { PostalAddress, PostalAddressService } from '../postal-address';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-shipment-dialog',
    templateUrl: './shipment-dialog.component.html'
})
export class ShipmentDialogComponent implements OnInit {

    shipment: Shipment;
    isSaving: boolean;
    idShipmentType: any;
    idShipFrom: any;
    idShipTo: any;
    idAddressFrom: any;
    idAddressTo: any;

    shipmenttypes: ShipmentType[];

    shiptos: ShipTo[];

    postaladdresses: PostalAddress[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected shipmentService: ShipmentService,
        protected shipmentTypeService: ShipmentTypeService,
        protected shipToService: ShipToService,
        protected postalAddressService: PostalAddressService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.shipmentTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmenttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipToService.query()
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.postalAddressService.query()
            .subscribe((res: ResponseWrapper) => { this.postaladdresses = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.shipment.idShipment !== undefined) {
            this.subscribeToSaveResponse(
                this.shipmentService.update(this.shipment));
        } else {
            this.subscribeToSaveResponse(
                this.shipmentService.create(this.shipment));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Shipment>) {
        result.subscribe((res: Shipment) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: Shipment) {
        this.eventManager.broadcast({ name: 'shipmentListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'shipment saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'shipment Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackShipmentTypeById(index: number, item: ShipmentType) {
        return item.idShipmentType;
    }

    trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }

    trackPostalAddressById(index: number, item: PostalAddress) {
        return item.idContact;
    }
}

@Component({
    selector: 'jhi-shipment-popup',
    template: ''
})
export class ShipmentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected shipmentPopupService: ShipmentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.shipmentPopupService
                    .open(ShipmentDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.shipmentPopupService.parent = params['parent'];
                this.shipmentPopupService
                    .open(ShipmentDialogComponent as Component);
            } else {
                this.shipmentPopupService
                    .open(ShipmentDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
