import { BaseEntity } from './../../shared';

export class Shipment implements BaseEntity {
    constructor(
        public id?: any,
        public idShipment?: any,
        public shipmentNumber?: string,
        public description?: string,
        public dateSchedulle?: any,
        public reason?: string,
        public details?: any,
        public shipmentTypeId?: any,
        public shipFromId?: any,
        public shipToId?: any,
        public addressFromId?: any,
        public addressToId?: any,
        public currentStatus?: number,
    ) {
    }
}
