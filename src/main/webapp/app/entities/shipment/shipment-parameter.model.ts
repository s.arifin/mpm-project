export class ShipmentParameter {
    constructor(
        public status?: Array<Number>,
        public statusNotIn?: Array<Number>,
        public packageReceiptDateFrom?: any,
        public packageReceiptDateThru?: any,
        public shipmentIncomingDateFrom?: any,
        public shipmentIncomingDateThru?: any,
        public internalId?: String
    ) {
        this.status = [10, 11];
        this.statusNotIn = [13, 17];
        this.packageReceiptDateFrom = null;
        this.packageReceiptDateThru = null;
        this.shipmentIncomingDateFrom = null;
        this.shipmentIncomingDateThru = null;
        this.internalId = null;
    }
}
