import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Shipment } from './shipment.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ShipmentService {
    protected itemValues: Shipment[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/shipments';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/shipments';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(shipment: Shipment): Observable<Shipment> {
        const copy = this.convert(shipment);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(shipment: Shipment): Observable<Shipment> {
        const copy = this.convert(shipment);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<Shipment> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(shipment: Shipment, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(shipment);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<Shipment> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<Shipment[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Shipment.
     */
    protected convertItemFromServer(json: any): Shipment {
        const entity: Shipment = Object.assign(new Shipment(), json);
        if (entity.dateSchedulle) {
            entity.dateSchedulle = new Date(entity.dateSchedulle);
        }
        return entity;
    }

    /**
     * Convert a Shipment to a JSON which can be sent to the server.
     */
    protected convert(shipment: Shipment): Shipment {
        if (shipment === null || shipment === {}) {
            return {};
        }
        // const copy: Shipment = Object.assign({}, shipment);
        const copy: Shipment = JSON.parse(JSON.stringify(shipment));

        // copy.dateSchedulle = this.dateUtils.toDate(shipment.dateSchedulle);
        return copy;
    }

    protected convertList(shipments: Shipment[]): Shipment[] {
        const copy: Shipment[] = shipments;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: Shipment[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
