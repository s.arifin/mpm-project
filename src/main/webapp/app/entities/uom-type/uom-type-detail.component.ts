import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import {UomType} from './uom-type.model';
import {UomTypeService} from './uom-type.service';

@Component({
    selector: 'jhi-uom-type-detail',
    templateUrl: './uom-type-detail.component.html'
})
export class UomTypeDetailComponent implements OnInit, OnDestroy {

    uomType: UomType;
    protected subscription: Subscription;
    protected eventSubscriber: Subscription;

    constructor(
        protected eventManager: JhiEventManager,
        protected uomTypeService: UomTypeService,
        protected route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUomTypes();
    }

    load(id) {
        this.uomTypeService.find(id).subscribe((uomType) => {
            this.uomType = uomType;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUomTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'uomTypeListModification',
            (response) => this.load(this.uomType.idUomType)
        );
    }
}
