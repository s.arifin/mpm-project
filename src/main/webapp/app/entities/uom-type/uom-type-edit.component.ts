import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UomType } from './uom-type.model';
import { UomTypeService } from './uom-type.service';
import { ToasterService} from '../../shared';

@Component({
   selector: 'jhi-uom-type-edit',
   templateUrl: './uom-type-edit.component.html'
})
export class UomTypeEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   uomType: UomType;
   isSaving: boolean;
   idUomType: any;
   paramPage: number;
   routeId: number;

   constructor(
       protected alertService: JhiAlertService,
       protected uomTypeService: UomTypeService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected toaster: ToasterService
   ) {
       this.uomType = new UomType();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idUomType = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.uomTypeService.find(this.idUomType).subscribe((uomType) => {
           this.uomType = uomType;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['uom-type', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.uomType.idUomType !== undefined) {
           this.subscribeToSaveResponse(
               this.uomTypeService.update(this.uomType));
       } else {
           this.subscribeToSaveResponse(
               this.uomTypeService.create(this.uomType));
       }
   }

   protected subscribeToSaveResponse(result: Observable<UomType>) {
       result.subscribe((res: UomType) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: UomType) {
       this.eventManager.broadcast({ name: 'uomTypeListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'uomType saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'uomType Changed', error.message);
       this.alertService.error(error.message, null, null);
   }
}
