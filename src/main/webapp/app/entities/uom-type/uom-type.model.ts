import { BaseEntity } from './../../shared';

export class UomType implements BaseEntity {
    constructor(
        public id?: number,
        public idUomType?: number,
        public description?: string,
    ) {
    }
}
