import { BaseEntity } from './../../shared';

export class GeneralUpload implements BaseEntity {
    constructor(
        public id?: any,
        public idGeneralUpload?: any,
        public currentStatus?: number,
        public dateCreate?: any,
        public description?: string,
        public valueContentType?: string,
        public value?: any,
        public internalId?: any,
        public purposeId?: any,
    ) {
    }
}
