import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PartyCategoryType } from './party-category-type.model';
import { PartyCategoryTypePopupService } from './party-category-type-popup.service';
import { PartyCategoryTypeService } from './party-category-type.service';
import { ToasterService } from '../../shared';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-party-category-type-dialog',
    templateUrl: './party-category-type-dialog.component.html'
})
export class PartyCategoryTypeDialogComponent implements OnInit {

    partyCategoryType: PartyCategoryType;
    isSaving: boolean;
    idParent: any;

    partycategorytypes: PartyCategoryType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected partyCategoryTypeService: PartyCategoryTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.partyCategoryTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.partycategorytypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.partyCategoryType.idCategoryType !== undefined) {
            this.subscribeToSaveResponse(
                this.partyCategoryTypeService.update(this.partyCategoryType));
        } else {
            this.subscribeToSaveResponse(
                this.partyCategoryTypeService.create(this.partyCategoryType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PartyCategoryType>) {
        result.subscribe((res: PartyCategoryType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: PartyCategoryType) {
        this.eventManager.broadcast({ name: 'partyCategoryTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'partyCategoryType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'partyCategoryType Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPartyCategoryTypeById(index: number, item: PartyCategoryType) {
        return item.idCategoryType;
    }
}

@Component({
    selector: 'jhi-party-category-type-popup',
    template: ''
})
export class PartyCategoryTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected partyCategoryTypePopupService: PartyCategoryTypePopupService
    ) {}

    ngOnInit() {
        this.partyCategoryTypePopupService.idParent = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.partyCategoryTypePopupService
                    .open(PartyCategoryTypeDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.partyCategoryTypePopupService.parent = params['parent'];
                this.partyCategoryTypePopupService
                    .open(PartyCategoryTypeDialogComponent as Component);
            } else {
                this.partyCategoryTypePopupService
                    .open(PartyCategoryTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
