import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { PartyCategoryType } from './party-category-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PartyCategoryTypeService {
   protected itemValues: PartyCategoryType[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/party-category-types';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/party-category-types';

   constructor(protected http: Http) { }

   create(partyCategoryType: PartyCategoryType): Observable<PartyCategoryType> {
       const copy = this.convert(partyCategoryType);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(partyCategoryType: PartyCategoryType): Observable<PartyCategoryType> {
       const copy = this.convert(partyCategoryType);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<PartyCategoryType> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: PartyCategoryType, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: PartyCategoryType[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to PartyCategoryType.
    */
   protected convertItemFromServer(json: any): PartyCategoryType {
       const entity: PartyCategoryType = Object.assign(new PartyCategoryType(), json);
       return entity;
   }

   /**
    * Convert a PartyCategoryType to a JSON which can be sent to the server.
    */
   protected convert(partyCategoryType: PartyCategoryType): PartyCategoryType {
       if (partyCategoryType === null || partyCategoryType === {}) {
           return {};
       }
       // const copy: PartyCategoryType = Object.assign({}, partyCategoryType);
       const copy: PartyCategoryType = JSON.parse(JSON.stringify(partyCategoryType));
       return copy;
   }

   protected convertList(partyCategoryTypes: PartyCategoryType[]): PartyCategoryType[] {
       const copy: PartyCategoryType[] = partyCategoryTypes;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: PartyCategoryType[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
