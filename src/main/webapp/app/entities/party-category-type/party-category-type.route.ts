import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PartyCategoryTypeComponent } from './party-category-type.component';
import { PartyCategoryTypeEditComponent } from './party-category-type-edit.component';
import { PartyCategoryTypePopupComponent } from './party-category-type-dialog.component';

@Injectable()
export class PartyCategoryTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idCategoryType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const partyCategoryTypeRoute: Routes = [
    {
        path: 'party-category-type',
        component: PartyCategoryTypeComponent,
        resolve: {
            'pagingParams': PartyCategoryTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategoryType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const partyCategoryTypePopupRoute: Routes = [
    {
        path: 'party-category-type-popup-new',
        component: PartyCategoryTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategoryType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'party-category-type-new',
        component: PartyCategoryTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategoryType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'party-category-type/:id/edit',
        component: PartyCategoryTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategoryType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'party-category-type/:route/:page/:id/edit',
        component: PartyCategoryTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategoryType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'party-category-type/:id/popup-edit',
        component: PartyCategoryTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyCategoryType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
