import { BaseEntity } from './../../shared';

export class PartyCategoryType implements BaseEntity {
    constructor(
        public id?: number,
        public idCategoryType?: number,
        public description?: string,
        public refKey?: string,
        public parentId?: any,
    ) {
    }
}
