import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PartyCategoryType } from './party-category-type.model';
import { PartyCategoryTypeService } from './party-category-type.service';
import { ToasterService} from '../../shared';
import { ResponseWrapper } from '../../shared';

@Component({
   selector: 'jhi-party-category-type-edit',
   templateUrl: './party-category-type-edit.component.html'
})
export class PartyCategoryTypeEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   partyCategoryType: PartyCategoryType;
   isSaving: boolean;
   idCategoryType: any;
   paramPage: number;
   routeId: number;

   partycategorytypes: PartyCategoryType[];

   constructor(
       protected alertService: JhiAlertService,
       protected partyCategoryTypeService: PartyCategoryTypeService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected toaster: ToasterService
   ) {
       this.partyCategoryType = new PartyCategoryType();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idCategoryType = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
       this.partyCategoryTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.partycategorytypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.partyCategoryTypeService.find(this.idCategoryType).subscribe((partyCategoryType) => {
           this.partyCategoryType = partyCategoryType;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['party-category-type', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.partyCategoryType.idCategoryType !== undefined) {
           this.subscribeToSaveResponse(
               this.partyCategoryTypeService.update(this.partyCategoryType));
       } else {
           this.subscribeToSaveResponse(
               this.partyCategoryTypeService.create(this.partyCategoryType));
       }
   }

   protected subscribeToSaveResponse(result: Observable<PartyCategoryType>) {
       result.subscribe((res: PartyCategoryType) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: PartyCategoryType) {
       this.eventManager.broadcast({ name: 'partyCategoryTypeListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'partyCategoryType saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'partyCategoryType Changed', error.message);
       this.alertService.error(error.message, null, null);
   }

   trackPartyCategoryTypeById(index: number, item: PartyCategoryType) {
       return item.idCategoryType;
   }
}
