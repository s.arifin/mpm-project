import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { GeoBoundaryComponent } from './geo-boundary.component';
import { GeoBoundaryPopupComponent } from './geo-boundary-dialog.component';

@Injectable()
export class GeoBoundaryResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idGeobou,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const geoBoundaryRoute: Routes = [
    {
        path: 'geo-boundary',
        component: GeoBoundaryComponent,
        resolve: {
            'pagingParams': GeoBoundaryResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.geoBoundary.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const geoBoundaryPopupRoute: Routes = [
    {
        path: 'geo-boundary-new',
        component: GeoBoundaryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.geoBoundary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'geo-boundary/:id/edit',
        component: GeoBoundaryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.geoBoundary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
