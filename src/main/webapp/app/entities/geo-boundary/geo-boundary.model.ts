import { BaseEntity } from './../../shared';

export class GeoBoundary implements BaseEntity {
    constructor(
        public id?: any,
        public idGeobou?: any,
        public geocode?: string,
        public description?: string,
        public geoBoundaries?: any,
        public geoBoundaryTypeId?: any,
    ) {
    }
}
