import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { GeoBoundary } from './geo-boundary.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class GeoBoundaryService {
    protected itemValues: GeoBoundary[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/geo-boundaries';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/geo-boundaries';

    constructor(protected http: Http) { }

    create(geoBoundary: GeoBoundary): Observable<GeoBoundary> {
        const copy = this.convert(geoBoundary);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(geoBoundary: GeoBoundary): Observable<GeoBoundary> {
        const copy = this.convert(geoBoundary);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<GeoBoundary> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, geoBoundary: GeoBoundary): Observable<GeoBoundary> {
        const copy = this.convert(geoBoundary);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, geoBoundarys: GeoBoundary[]): Observable<GeoBoundary[]> {
        const copy = this.convertList(geoBoundarys);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(geoBoundary: GeoBoundary): GeoBoundary {
        if (geoBoundary === null || geoBoundary === {}) {
            return {};
        }
        // const copy: GeoBoundary = Object.assign({}, geoBoundary);
        const copy: GeoBoundary = JSON.parse(JSON.stringify(geoBoundary));
        return copy;
    }

    protected convertList(geoBoundarys: GeoBoundary[]): GeoBoundary[] {
        const copy: GeoBoundary[] = geoBoundarys;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: GeoBoundary[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
