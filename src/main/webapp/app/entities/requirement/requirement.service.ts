import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { JhiDateUtils } from 'ng-jhipster';

import { Requirement } from './requirement.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class RequirementService {
    protected itemValues: Requirement[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/requirements';
    protected resourceSearchUrl = 'api/_search/requirements';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(requirement: Requirement): Observable<Requirement> {
        const copy = this.convert(requirement);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(requirement: Requirement): Observable<Requirement> {
        const copy = this.convert(requirement);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<Requirement> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(requirement: Requirement, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(requirement);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, requirement: Requirement): Observable<Requirement> {
        const copy = this.convert(requirement);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, requirements: Requirement[]): Observable<Requirement[]> {
        const copy = this.convertList(requirements);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateCreate) {
            entity.dateCreate = new Date(entity.dateCreate);
        }
        if (entity.dateRequired) {
            entity.dateRequired = new Date(entity.dateRequired);
        }
    }

    protected convert(requirement: Requirement): Requirement {
        if (requirement === null || requirement === {}) {
            return {};
        }
        // const copy: Requirement = Object.assign({}, requirement);
        const copy: Requirement = JSON.parse(JSON.stringify(requirement));

        // copy.dateCreate = this.dateUtils.toDate(requirement.dateCreate);

        // copy.dateRequired = this.dateUtils.toDate(requirement.dateRequired);
        return copy;
    }

    protected convertList(requirements: Requirement[]): Requirement[] {
        const copy: Requirement[] = requirements;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: Requirement[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
