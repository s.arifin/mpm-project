import { BaseEntity } from './../../shared';

export class Requirement implements BaseEntity {
    constructor(
        public id?: any,
        public idRequirement?: any,
        public requirementNumber?: string,
        public description?: string,
        public dateCreate?: any,
        public dateRequired?: any,
        public budget?: number,
        public qty?: number,
        public idReqTyp?: number,
        public qtyFilled?: number,
        public payments?: any,
        public facilityId?: any,
        public facilityDescription?: string,
        public parentId?: any,
        public parentDescription?: string,
        public currentStatus?: number
    ) {
    }
}
