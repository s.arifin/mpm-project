import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {Requirement} from './requirement.model';
import {RequirementPopupService} from './requirement-popup.service';
import {RequirementService} from './requirement.service';
import {ToasterService} from '../../shared';
import { OrderItem, OrderItemService } from '../order-item';
import { PaymentApplication, PaymentApplicationService } from '../payment-application';
import { Facility, FacilityService } from '../facility';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-requirement-dialog',
    templateUrl: './requirement-dialog.component.html'
})
export class RequirementDialogComponent implements OnInit {

    requirement: Requirement;
    isSaving: boolean;

    orderitems: OrderItem[];

    paymentapplications: PaymentApplication[];

    facilities: Facility[];

    requirements: Requirement[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected requirementService: RequirementService,
        protected orderItemService: OrderItemService,
        protected paymentApplicationService: PaymentApplicationService,
        protected facilityService: FacilityService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.orderItemService.query()
            .subscribe((res: ResponseWrapper) => { this.orderitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.paymentApplicationService.query()
            .subscribe((res: ResponseWrapper) => { this.paymentapplications = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.requirementService.query()
            .subscribe((res: ResponseWrapper) => { this.requirements = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.requirement.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.requirementService.update(this.requirement));
        } else {
            this.subscribeToSaveResponse(
                this.requirementService.create(this.requirement));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Requirement>) {
        result.subscribe((res: Requirement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Requirement) {
        this.eventManager.broadcast({ name: 'requirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'requirement saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'requirement Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackOrderItemById(index: number, item: OrderItem) {
        return item.idOrderItem;
    }

    trackPaymentApplicationById(index: number, item: PaymentApplication) {
        return item.idPaymentApplication;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackRequirementById(index: number, item: Requirement) {
        return item.idRequirement;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-requirement-popup',
    template: ''
})
export class RequirementPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected requirementPopupService: RequirementPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.requirementPopupService
                    .open(RequirementDialogComponent as Component, params['id']);
            } else {
                this.requirementPopupService
                    .open(RequirementDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
